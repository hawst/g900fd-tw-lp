.class Lcom/samsung/android/allshare/ImageViewerImpl$2;
.super Lcom/samsung/android/allshare/AllShareResponseHandler;
.source "ImageViewerImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/allshare/ImageViewerImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/allshare/ImageViewerImpl;


# direct methods
.method constructor <init>(Lcom/samsung/android/allshare/ImageViewerImpl;Landroid/os/Looper;)V
    .locals 0
    .param p2, "x0"    # Landroid/os/Looper;

    .prologue
    .line 222
    iput-object p1, p0, Lcom/samsung/android/allshare/ImageViewerImpl$2;->this$0:Lcom/samsung/android/allshare/ImageViewerImpl;

    invoke-direct {p0, p2}, Lcom/samsung/android/allshare/AllShareResponseHandler;-><init>(Landroid/os/Looper;)V

    return-void
.end method

.method private removeUri(Ljava/lang/String;)V
    .locals 5
    .param p1, "currentTrackUri"    # Ljava/lang/String;

    .prologue
    .line 309
    iget-object v4, p0, Lcom/samsung/android/allshare/ImageViewerImpl$2;->this$0:Lcom/samsung/android/allshare/ImageViewerImpl;

    # getter for: Lcom/samsung/android/allshare/ImageViewerImpl;->mPlayingContentUris:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/samsung/android/allshare/ImageViewerImpl;->access$200(Lcom/samsung/android/allshare/ImageViewerImpl;)Ljava/util/ArrayList;

    move-result-object v4

    if-eqz v4, :cond_0

    if-nez p1, :cond_1

    .line 321
    :cond_0
    :goto_0
    return-void

    .line 312
    :cond_1
    iget-object v4, p0, Lcom/samsung/android/allshare/ImageViewerImpl$2;->this$0:Lcom/samsung/android/allshare/ImageViewerImpl;

    # getter for: Lcom/samsung/android/allshare/ImageViewerImpl;->mPlayingContentUris:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/samsung/android/allshare/ImageViewerImpl;->access$200(Lcom/samsung/android/allshare/ImageViewerImpl;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/ArrayList;

    .line 313
    .local v3, "uris":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 314
    .local v2, "uri":Ljava/lang/String;
    invoke-virtual {p1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 315
    iget-object v4, p0, Lcom/samsung/android/allshare/ImageViewerImpl$2;->this$0:Lcom/samsung/android/allshare/ImageViewerImpl;

    # getter for: Lcom/samsung/android/allshare/ImageViewerImpl;->mPlayingContentUris:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/samsung/android/allshare/ImageViewerImpl;->access$200(Lcom/samsung/android/allshare/ImageViewerImpl;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_0
.end method


# virtual methods
.method public handleResponseMessage(Lcom/sec/android/allshare/iface/CVMessage;)V
    .locals 18
    .param p1, "cvm"    # Lcom/sec/android/allshare/iface/CVMessage;

    .prologue
    .line 226
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/allshare/iface/CVMessage;->getActionID()Ljava/lang/String;

    move-result-object v2

    .line 227
    .local v2, "actionID":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/allshare/iface/CVMessage;->getBundle()Landroid/os/Bundle;

    move-result-object v13

    .line 229
    .local v13, "resBundle":Landroid/os/Bundle;
    if-eqz v2, :cond_0

    if-nez v13, :cond_2

    .line 230
    :cond_0
    const-string v16, "ImageViewerImpl"

    const-string v17, "handleResponseMessage : actionID == null || resBundle == null"

    invoke-static/range {v16 .. v17}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 306
    :cond_1
    :goto_0
    return-void

    .line 234
    :cond_2
    sget-object v5, Lcom/samsung/android/allshare/ERROR;->FAIL:Lcom/samsung/android/allshare/ERROR;

    .line 235
    .local v5, "error":Lcom/samsung/android/allshare/ERROR;
    const-string v16, "BUNDLE_ENUM_ERROR"

    move-object/from16 v0, v16

    invoke-virtual {v13, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 236
    .local v8, "errorStr":Ljava/lang/String;
    if-eqz v8, :cond_3

    .line 237
    invoke-static {v8}, Lcom/samsung/android/allshare/ERROR;->stringToEnum(Ljava/lang/String;)Lcom/samsung/android/allshare/ERROR;

    move-result-object v5

    .line 239
    :cond_3
    const-string v16, "BUNDLE_LONG_CONTENT_INFO_STARTINGPOSITION"

    move-object/from16 v0, v16

    invoke-virtual {v13, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v6

    .line 242
    .local v6, "contentInfoStartingPosition":J
    new-instance v4, Lcom/samsung/android/allshare/media/ContentInfo$Builder;

    invoke-direct {v4}, Lcom/samsung/android/allshare/media/ContentInfo$Builder;-><init>()V

    .line 243
    .local v4, "cb":Lcom/samsung/android/allshare/media/ContentInfo$Builder;
    invoke-virtual {v4, v6, v7}, Lcom/samsung/android/allshare/media/ContentInfo$Builder;->setStartingPosition(J)Lcom/samsung/android/allshare/media/ContentInfo$Builder;

    move-result-object v4

    .line 244
    invoke-virtual {v4}, Lcom/samsung/android/allshare/media/ContentInfo$Builder;->build()Lcom/samsung/android/allshare/media/ContentInfo;

    move-result-object v10

    .line 246
    .local v10, "info":Lcom/samsung/android/allshare/media/ContentInfo;
    const-string v16, "BUNDLE_PARCELABLE_ITEM"

    move-object/from16 v0, v16

    invoke-virtual {v13, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v12

    check-cast v12, Landroid/os/Bundle;

    .line 248
    .local v12, "itemBundle":Landroid/os/Bundle;
    invoke-static {v12}, Lcom/samsung/android/allshare/ItemCreator;->fromBundle(Landroid/os/Bundle;)Lcom/samsung/android/allshare/Item;

    move-result-object v11

    .line 251
    .local v11, "item":Lcom/samsung/android/allshare/Item;
    const-string v16, "com.sec.android.allshare.action.ACTION_IMAGE_VIEWER_SHOW_LOCAL_CONTENT"

    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    const/16 v17, 0x1

    move/from16 v0, v16

    move/from16 v1, v17

    if-eq v0, v1, :cond_4

    const-string v16, "com.sec.android.allshare.action.ACTION_IMAGE_VIEWER_SHOW_LOCAL_CONTENT_URI"

    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    const/16 v17, 0x1

    move/from16 v0, v16

    move/from16 v1, v17

    if-eq v0, v1, :cond_4

    const-string v16, "com.sec.android.allshare.action.ACTION_IMAGE_VIEWER_SHOW_URI"

    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    const/16 v17, 0x1

    move/from16 v0, v16

    move/from16 v1, v17

    if-eq v0, v1, :cond_4

    const-string v16, "com.sec.android.allshare.action.ACTION_IMAGE_VIEWER_SHOW"

    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    const/16 v17, 0x1

    move/from16 v0, v16

    move/from16 v1, v17

    if-ne v0, v1, :cond_5

    .line 255
    :cond_4
    sget-object v16, Lcom/samsung/android/allshare/ERROR;->SUCCESS:Lcom/samsung/android/allshare/ERROR;

    move-object/from16 v0, v16

    invoke-virtual {v5, v0}, Lcom/samsung/android/allshare/ERROR;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_6

    .line 256
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/ImageViewerImpl$2;->this$0:Lcom/samsung/android/allshare/ImageViewerImpl;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    # setter for: Lcom/samsung/android/allshare/ImageViewerImpl;->mContentChangedNotified:Z
    invoke-static/range {v16 .. v17}, Lcom/samsung/android/allshare/ImageViewerImpl;->access$002(Lcom/samsung/android/allshare/ImageViewerImpl;Z)Z

    .line 280
    :cond_5
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/ImageViewerImpl$2;->this$0:Lcom/samsung/android/allshare/ImageViewerImpl;

    move-object/from16 v16, v0

    # getter for: Lcom/samsung/android/allshare/ImageViewerImpl;->mResponseListener:Lcom/samsung/android/allshare/media/ImageViewer$IImageViewerResponseListener;
    invoke-static/range {v16 .. v16}, Lcom/samsung/android/allshare/ImageViewerImpl;->access$500(Lcom/samsung/android/allshare/ImageViewerImpl;)Lcom/samsung/android/allshare/media/ImageViewer$IImageViewerResponseListener;

    move-result-object v16

    if-nez v16, :cond_b

    .line 281
    const-string v16, "ImageViewerImpl"

    const-string v17, "handleResponseMessage : mResponseListener == null"

    invoke-static/range {v16 .. v17}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 258
    :cond_6
    if-eqz v11, :cond_5

    .line 260
    const-string v16, "com.sec.android.allshare.action.ACTION_IMAGE_VIEWER_SHOW_URI"

    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    const/16 v17, 0x1

    move/from16 v0, v16

    move/from16 v1, v17

    if-eq v0, v1, :cond_7

    const-string v16, "com.sec.android.allshare.action.ACTION_IMAGE_VIEWER_SHOW"

    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    const/16 v17, 0x1

    move/from16 v0, v16

    move/from16 v1, v17

    if-ne v0, v1, :cond_8

    .line 262
    :cond_7
    invoke-virtual {v11}, Lcom/samsung/android/allshare/Item;->getURI()Landroid/net/Uri;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Lcom/samsung/android/allshare/ImageViewerImpl$2;->removeUri(Ljava/lang/String;)V

    goto :goto_1

    .line 264
    :cond_8
    const-string v16, "com.sec.android.allshare.action.ACTION_IMAGE_VIEWER_SHOW_LOCAL_CONTENT"

    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    const/16 v17, 0x1

    move/from16 v0, v16

    move/from16 v1, v17

    if-ne v0, v1, :cond_a

    .line 265
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 266
    .local v3, "bundle":Landroid/os/Bundle;
    instance-of v0, v11, Lcom/sec/android/allshare/iface/IBundleHolder;

    move/from16 v16, v0

    if-eqz v16, :cond_9

    move-object/from16 v16, v11

    .line 267
    check-cast v16, Lcom/sec/android/allshare/iface/IBundleHolder;

    invoke-interface/range {v16 .. v16}, Lcom/sec/android/allshare/iface/IBundleHolder;->getBundle()Landroid/os/Bundle;

    move-result-object v3

    .line 269
    :cond_9
    const-string v16, "BUNDLE_STRING_FILEPATH"

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 270
    .local v9, "filePath":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-direct {v0, v9}, Lcom/samsung/android/allshare/ImageViewerImpl$2;->removeUri(Ljava/lang/String;)V

    goto :goto_1

    .line 272
    .end local v3    # "bundle":Landroid/os/Bundle;
    .end local v9    # "filePath":Ljava/lang/String;
    :cond_a
    const-string v16, "com.sec.android.allshare.action.ACTION_IMAGE_VIEWER_SHOW_LOCAL_CONTENT_URI"

    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    const/16 v17, 0x1

    move/from16 v0, v16

    move/from16 v1, v17

    if-ne v0, v1, :cond_5

    .line 273
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/ImageViewerImpl$2;->this$0:Lcom/samsung/android/allshare/ImageViewerImpl;

    move-object/from16 v16, v0

    invoke-virtual {v11}, Lcom/samsung/android/allshare/Item;->getURI()Landroid/net/Uri;

    move-result-object v17

    # invokes: Lcom/samsung/android/allshare/ImageViewerImpl;->parseUriFilePath(Landroid/net/Uri;)Ljava/lang/String;
    invoke-static/range {v16 .. v17}, Lcom/samsung/android/allshare/ImageViewerImpl;->access$400(Lcom/samsung/android/allshare/ImageViewerImpl;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v9

    .line 274
    .restart local v9    # "filePath":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-direct {v0, v9}, Lcom/samsung/android/allshare/ImageViewerImpl$2;->removeUri(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 285
    .end local v9    # "filePath":Ljava/lang/String;
    :cond_b
    const-string v16, "com.sec.android.allshare.action.ACTION_IMAGE_VIEWER_SHOW"

    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    const/16 v17, 0x1

    move/from16 v0, v16

    move/from16 v1, v17

    if-eq v0, v1, :cond_c

    const-string v16, "com.sec.android.allshare.action.ACTION_IMAGE_VIEWER_SHOW_LOCAL_CONTENT"

    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    const/16 v17, 0x1

    move/from16 v0, v16

    move/from16 v1, v17

    if-eq v0, v1, :cond_c

    const-string v16, "com.sec.android.allshare.action.ACTION_IMAGE_VIEWER_SHOW_LOCAL_CONTENT_URI"

    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    const/16 v17, 0x1

    move/from16 v0, v16

    move/from16 v1, v17

    if-eq v0, v1, :cond_c

    const-string v16, "com.sec.android.allshare.action.ACTION_IMAGE_VIEWER_SHOW_URI"

    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    const/16 v17, 0x1

    move/from16 v0, v16

    move/from16 v1, v17

    if-ne v0, v1, :cond_e

    .line 291
    :cond_c
    if-nez v11, :cond_d

    .line 292
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/ImageViewerImpl$2;->this$0:Lcom/samsung/android/allshare/ImageViewerImpl;

    move-object/from16 v16, v0

    # getter for: Lcom/samsung/android/allshare/ImageViewerImpl;->mResponseListener:Lcom/samsung/android/allshare/media/ImageViewer$IImageViewerResponseListener;
    invoke-static/range {v16 .. v16}, Lcom/samsung/android/allshare/ImageViewerImpl;->access$500(Lcom/samsung/android/allshare/ImageViewerImpl;)Lcom/samsung/android/allshare/media/ImageViewer$IImageViewerResponseListener;

    move-result-object v16

    sget-object v17, Lcom/samsung/android/allshare/ERROR;->ITEM_NOT_EXIST:Lcom/samsung/android/allshare/ERROR;

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-interface {v0, v11, v10, v1}, Lcom/samsung/android/allshare/media/ImageViewer$IImageViewerResponseListener;->onShowResponseReceived(Lcom/samsung/android/allshare/Item;Lcom/samsung/android/allshare/media/ContentInfo;Lcom/samsung/android/allshare/ERROR;)V

    goto/16 :goto_0

    .line 294
    :cond_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/ImageViewerImpl$2;->this$0:Lcom/samsung/android/allshare/ImageViewerImpl;

    move-object/from16 v16, v0

    # getter for: Lcom/samsung/android/allshare/ImageViewerImpl;->mResponseListener:Lcom/samsung/android/allshare/media/ImageViewer$IImageViewerResponseListener;
    invoke-static/range {v16 .. v16}, Lcom/samsung/android/allshare/ImageViewerImpl;->access$500(Lcom/samsung/android/allshare/ImageViewerImpl;)Lcom/samsung/android/allshare/media/ImageViewer$IImageViewerResponseListener;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-interface {v0, v11, v10, v5}, Lcom/samsung/android/allshare/media/ImageViewer$IImageViewerResponseListener;->onShowResponseReceived(Lcom/samsung/android/allshare/Item;Lcom/samsung/android/allshare/media/ContentInfo;Lcom/samsung/android/allshare/ERROR;)V

    goto/16 :goto_0

    .line 295
    :cond_e
    const-string v16, "com.sec.android.allshare.action.ACTION_IMAGE_VIEWER_STOP"

    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    const/16 v17, 0x1

    move/from16 v0, v16

    move/from16 v1, v17

    if-ne v0, v1, :cond_f

    .line 296
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/ImageViewerImpl$2;->this$0:Lcom/samsung/android/allshare/ImageViewerImpl;

    move-object/from16 v16, v0

    # getter for: Lcom/samsung/android/allshare/ImageViewerImpl;->mResponseListener:Lcom/samsung/android/allshare/media/ImageViewer$IImageViewerResponseListener;
    invoke-static/range {v16 .. v16}, Lcom/samsung/android/allshare/ImageViewerImpl;->access$500(Lcom/samsung/android/allshare/ImageViewerImpl;)Lcom/samsung/android/allshare/media/ImageViewer$IImageViewerResponseListener;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-interface {v0, v5}, Lcom/samsung/android/allshare/media/ImageViewer$IImageViewerResponseListener;->onStopResponseReceived(Lcom/samsung/android/allshare/ERROR;)V

    goto/16 :goto_0

    .line 297
    :cond_f
    const-string v16, "com.sec.android.allshare.action.ACTION_IMAGE_VIEWER_REQUEST_GET_VIEWER_STATE"

    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_1

    .line 298
    const-string v16, "BUNDLE_STRING_IMAGE_VIEWEW_STATE"

    move-object/from16 v0, v16

    invoke-virtual {v13, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 300
    .local v14, "state":Ljava/lang/String;
    invoke-static {v14}, Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;->stringToEnum(Ljava/lang/String;)Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;

    move-result-object v15

    .line 302
    .local v15, "viewerState":Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/ImageViewerImpl$2;->this$0:Lcom/samsung/android/allshare/ImageViewerImpl;

    move-object/from16 v16, v0

    # getter for: Lcom/samsung/android/allshare/ImageViewerImpl;->mResponseListener:Lcom/samsung/android/allshare/media/ImageViewer$IImageViewerResponseListener;
    invoke-static/range {v16 .. v16}, Lcom/samsung/android/allshare/ImageViewerImpl;->access$500(Lcom/samsung/android/allshare/ImageViewerImpl;)Lcom/samsung/android/allshare/media/ImageViewer$IImageViewerResponseListener;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-interface {v0, v15, v5}, Lcom/samsung/android/allshare/media/ImageViewer$IImageViewerResponseListener;->onGetStateResponseReceived(Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;Lcom/samsung/android/allshare/ERROR;)V

    goto/16 :goto_0
.end method
