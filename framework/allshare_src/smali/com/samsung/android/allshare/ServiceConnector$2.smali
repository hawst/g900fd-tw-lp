.class final Lcom/samsung/android/allshare/ServiceConnector$2;
.super Ljava/lang/Object;
.source "ServiceConnector.java"

# interfaces
.implements Landroid/os/Handler$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/allshare/ServiceConnector;->createServiceProvider(Landroid/content/Context;Lcom/samsung/android/allshare/ServiceConnector$IServiceConnectEventListener;Ljava/lang/String;)Lcom/samsung/android/allshare/ERROR;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field private mListener:Lcom/samsung/android/allshare/ServiceConnector$IServiceConnectEventListener;

.field private mServiceProvider:Lcom/samsung/android/allshare/ServiceProvider;

.field final synthetic val$connector:Lcom/samsung/android/allshare/ServiceConnector$AllShareConnector;

.field final synthetic val$ctx:Landroid/content/Context;

.field final synthetic val$l:Lcom/samsung/android/allshare/ServiceConnector$IServiceConnectEventListener;

.field final synthetic val$serviceType:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/samsung/android/allshare/ServiceConnector$IServiceConnectEventListener;Landroid/content/Context;Lcom/samsung/android/allshare/ServiceConnector$AllShareConnector;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 375
    iput-object p1, p0, Lcom/samsung/android/allshare/ServiceConnector$2;->val$l:Lcom/samsung/android/allshare/ServiceConnector$IServiceConnectEventListener;

    iput-object p2, p0, Lcom/samsung/android/allshare/ServiceConnector$2;->val$ctx:Landroid/content/Context;

    iput-object p3, p0, Lcom/samsung/android/allshare/ServiceConnector$2;->val$connector:Lcom/samsung/android/allshare/ServiceConnector$AllShareConnector;

    iput-object p4, p0, Lcom/samsung/android/allshare/ServiceConnector$2;->val$serviceType:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 376
    iget-object v0, p0, Lcom/samsung/android/allshare/ServiceConnector$2;->val$l:Lcom/samsung/android/allshare/ServiceConnector$IServiceConnectEventListener;

    iput-object v0, p0, Lcom/samsung/android/allshare/ServiceConnector$2;->mListener:Lcom/samsung/android/allshare/ServiceConnector$IServiceConnectEventListener;

    .line 378
    iget-object v0, p0, Lcom/samsung/android/allshare/ServiceConnector$2;->val$ctx:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/android/allshare/ServiceConnector$2;->val$connector:Lcom/samsung/android/allshare/ServiceConnector$AllShareConnector;

    iget-object v2, p0, Lcom/samsung/android/allshare/ServiceConnector$2;->val$serviceType:Ljava/lang/String;

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/allshare/ServiceConnector$2;->createServiceProvierImpl(Landroid/content/Context;Lcom/samsung/android/allshare/ServiceConnector$AllShareConnector;Ljava/lang/String;)Lcom/samsung/android/allshare/ServiceProvider;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/allshare/ServiceConnector$2;->mServiceProvider:Lcom/samsung/android/allshare/ServiceProvider;

    return-void
.end method

.method private createServiceProvierImpl(Landroid/content/Context;Lcom/samsung/android/allshare/ServiceConnector$AllShareConnector;Ljava/lang/String;)Lcom/samsung/android/allshare/ServiceProvider;
    .locals 2
    .param p1, "ctx"    # Landroid/content/Context;
    .param p2, "connector"    # Lcom/samsung/android/allshare/ServiceConnector$AllShareConnector;
    .param p3, "serviceType"    # Ljava/lang/String;

    .prologue
    .line 423
    if-eqz p3, :cond_0

    const-string v0, "com.samsung.android.allshare.media"

    invoke-virtual {p3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 424
    new-instance v0, Lcom/samsung/android/allshare/ServiceConnector$ProfileConstData;

    const-string v1, "com.samsung.android.allshare.service.mediashare"

    invoke-direct {v0, v1}, Lcom/samsung/android/allshare/ServiceConnector$ProfileConstData;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2, v0}, Lcom/samsung/android/allshare/ServiceConnector$AllShareConnector;->setProfileConstData(Lcom/samsung/android/allshare/ServiceConnector$ProfileConstData;)V

    .line 426
    new-instance v0, Lcom/samsung/android/allshare/ServiceConnector$MediaServiceProviderImpl;

    invoke-direct {v0, p1, p2}, Lcom/samsung/android/allshare/ServiceConnector$MediaServiceProviderImpl;-><init>(Landroid/content/Context;Lcom/samsung/android/allshare/ServiceConnector$AllShareConnector;)V

    .line 433
    :goto_0
    return-object v0

    .line 427
    :cond_0
    if-eqz p3, :cond_1

    const-string v0, "com.samsung.android.allshare.file"

    invoke-virtual {p3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 428
    new-instance v0, Lcom/samsung/android/allshare/ServiceConnector$ProfileConstData;

    const-string v1, "com.samsung.android.allshare.service.fileshare"

    invoke-direct {v0, v1}, Lcom/samsung/android/allshare/ServiceConnector$ProfileConstData;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2, v0}, Lcom/samsung/android/allshare/ServiceConnector$AllShareConnector;->setProfileConstData(Lcom/samsung/android/allshare/ServiceConnector$ProfileConstData;)V

    .line 430
    new-instance v0, Lcom/samsung/android/allshare/ServiceConnector$FileServiceProviderImpl;

    invoke-direct {v0, p1, p2}, Lcom/samsung/android/allshare/ServiceConnector$FileServiceProviderImpl;-><init>(Landroid/content/Context;Lcom/samsung/android/allshare/ServiceConnector$AllShareConnector;)V

    goto :goto_0

    .line 433
    :cond_1
    new-instance v0, Lcom/samsung/android/allshare/ServiceConnector$ServiceProviderImpl;

    invoke-direct {v0, p1, p2}, Lcom/samsung/android/allshare/ServiceConnector$ServiceProviderImpl;-><init>(Landroid/content/Context;Lcom/samsung/android/allshare/ServiceConnector$AllShareConnector;)V

    goto :goto_0
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)Z
    .locals 6
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 384
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    if-eqz v3, :cond_0

    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    instance-of v3, v3, Lcom/samsung/android/allshare/IAllShareConnector$AllShareServiceState;

    if-nez v3, :cond_1

    .line 385
    :cond_0
    const/4 v3, 0x0

    .line 416
    :goto_0
    return v3

    .line 387
    :cond_1
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, Lcom/samsung/android/allshare/IAllShareConnector$AllShareServiceState;

    .line 389
    .local v2, "state":Lcom/samsung/android/allshare/IAllShareConnector$AllShareServiceState;
    sget-object v3, Lcom/samsung/android/allshare/ServiceConnector$3;->$SwitchMap$com$samsung$android$allshare$IAllShareConnector$AllShareServiceState:[I

    invoke-virtual {v2}, Lcom/samsung/android/allshare/IAllShareConnector$AllShareServiceState;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 416
    :goto_1
    const/4 v3, 0x1

    goto :goto_0

    .line 391
    :pswitch_0
    iget-object v3, p0, Lcom/samsung/android/allshare/ServiceConnector$2;->mServiceProvider:Lcom/samsung/android/allshare/ServiceProvider;

    check-cast v3, Lcom/samsung/android/allshare/ServiceConnector$IServiceStateSetter;

    sget-object v4, Lcom/samsung/android/allshare/ServiceConnector$ServiceState;->ENABLED:Lcom/samsung/android/allshare/ServiceConnector$ServiceState;

    invoke-interface {v3, v4}, Lcom/samsung/android/allshare/ServiceConnector$IServiceStateSetter;->setServiceState(Lcom/samsung/android/allshare/ServiceConnector$ServiceState;)V

    .line 394
    :try_start_0
    iget-object v3, p0, Lcom/samsung/android/allshare/ServiceConnector$2;->mListener:Lcom/samsung/android/allshare/ServiceConnector$IServiceConnectEventListener;

    iget-object v4, p0, Lcom/samsung/android/allshare/ServiceConnector$2;->mServiceProvider:Lcom/samsung/android/allshare/ServiceProvider;

    sget-object v5, Lcom/samsung/android/allshare/ServiceConnector$ServiceState;->ENABLED:Lcom/samsung/android/allshare/ServiceConnector$ServiceState;

    invoke-interface {v3, v4, v5}, Lcom/samsung/android/allshare/ServiceConnector$IServiceConnectEventListener;->onCreated(Lcom/samsung/android/allshare/ServiceProvider;Lcom/samsung/android/allshare/ServiceConnector$ServiceState;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_1

    .line 395
    :catch_0
    move-exception v0

    .line 396
    .local v0, "e":Ljava/lang/Exception;
    const-string v3, "ServiceConnector"

    const-string v4, "handleMessage Exception"

    invoke-static {v3, v4, v0}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_1

    .line 397
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v0

    .line 398
    .local v0, "e":Ljava/lang/Error;
    const-string v3, "ServiceConnector"

    const-string v4, "handleMessage Error"

    invoke-static {v3, v4, v0}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Error;)V

    goto :goto_1

    .line 403
    .end local v0    # "e":Ljava/lang/Error;
    :pswitch_1
    iget-object v3, p0, Lcom/samsung/android/allshare/ServiceConnector$2;->mServiceProvider:Lcom/samsung/android/allshare/ServiceProvider;

    check-cast v3, Lcom/samsung/android/allshare/ServiceConnector$IServiceStateSetter;

    sget-object v4, Lcom/samsung/android/allshare/ServiceConnector$ServiceState;->DISABLED:Lcom/samsung/android/allshare/ServiceConnector$ServiceState;

    invoke-interface {v3, v4}, Lcom/samsung/android/allshare/ServiceConnector$IServiceStateSetter;->setServiceState(Lcom/samsung/android/allshare/ServiceConnector$ServiceState;)V

    .line 406
    :try_start_1
    iget-object v3, p0, Lcom/samsung/android/allshare/ServiceConnector$2;->mListener:Lcom/samsung/android/allshare/ServiceConnector$IServiceConnectEventListener;

    iget-object v4, p0, Lcom/samsung/android/allshare/ServiceConnector$2;->mServiceProvider:Lcom/samsung/android/allshare/ServiceProvider;

    invoke-interface {v3, v4}, Lcom/samsung/android/allshare/ServiceConnector$IServiceConnectEventListener;->onDeleted(Lcom/samsung/android/allshare/ServiceProvider;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/Error; {:try_start_1 .. :try_end_1} :catch_3

    goto :goto_1

    .line 407
    :catch_2
    move-exception v0

    .line 408
    .local v0, "e":Ljava/lang/Exception;
    const-string v3, "ServiceConnector"

    const-string v4, ""

    invoke-static {v3, v4, v0}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_1

    .line 409
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_3
    move-exception v1

    .line 410
    .local v1, "err":Ljava/lang/Error;
    const-string v3, "ServiceConnector"

    const-string v4, ""

    invoke-static {v3, v4, v1}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Error;)V

    goto :goto_1

    .line 389
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
