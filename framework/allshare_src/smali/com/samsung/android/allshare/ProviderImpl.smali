.class final Lcom/samsung/android/allshare/ProviderImpl;
.super Lcom/samsung/android/allshare/media/Provider;
.source "ProviderImpl.java"

# interfaces
.implements Lcom/sec/android/allshare/iface/IBundleHolder;
.implements Lcom/sec/android/allshare/iface/IHandlerHolder;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/allshare/ProviderImpl$3;,
        Lcom/samsung/android/allshare/ProviderImpl$RootFolderItem;
    }
.end annotation


# static fields
.field private static final TAG_CLASS:Ljava/lang/String; = "ProviderImpl"

.field private static mRootFolderItem:Lcom/samsung/android/allshare/Item;


# instance fields
.field private mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

.field private mAllShareRespHandler:Lcom/samsung/android/allshare/AllShareResponseHandler;

.field private mBrowseResponseListener:Lcom/samsung/android/allshare/media/Provider$IProviderBrowseResponseListener;

.field private mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

.field private mEventHandler:Lcom/samsung/android/allshare/AllShareEventHandler;

.field private mIsSubscribed:Z

.field private mProviderEventListener:Lcom/samsung/android/allshare/media/Provider$IProviderEventListener;

.field private mReceiver:Lcom/samsung/android/allshare/media/Receiver;

.field private mSearchResponseListener:Lcom/samsung/android/allshare/media/Provider$IProviderSearchResponseListener;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const-wide/16 v4, -0x1

    const/4 v3, 0x0

    .line 66
    sput-object v3, Lcom/samsung/android/allshare/ProviderImpl;->mRootFolderItem:Lcom/samsung/android/allshare/Item;

    .line 73
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 74
    .local v0, "mFolderBunle":Landroid/os/Bundle;
    const-string v1, "BUNDLE_STRING_ITEM_TITLE"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Landroid/os/BaseBundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    const-string v1, "BUNDLE_PARCELABLE_ITEM_URI"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 76
    const-string v1, "BUNDLE_LONG_ITEM_DATE"

    invoke-virtual {v0, v1, v4, v5}, Landroid/os/BaseBundle;->putLong(Ljava/lang/String;J)V

    .line 77
    const-string v1, "BUNDLE_LONG_ITEM_FILE_SIZE"

    invoke-virtual {v0, v1, v4, v5}, Landroid/os/BaseBundle;->putLong(Ljava/lang/String;J)V

    .line 78
    const-string v1, "BUNDLE_STRING_OBJECT_ID"

    const-string v2, "0"

    invoke-virtual {v0, v1, v2}, Landroid/os/BaseBundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    new-instance v1, Lcom/samsung/android/allshare/ProviderImpl$RootFolderItem;

    invoke-direct {v1, v0}, Lcom/samsung/android/allshare/ProviderImpl$RootFolderItem;-><init>(Landroid/os/Bundle;)V

    sput-object v1, Lcom/samsung/android/allshare/ProviderImpl;->mRootFolderItem:Lcom/samsung/android/allshare/Item;

    .line 81
    return-void
.end method

.method constructor <init>(Lcom/samsung/android/allshare/IAllShareConnector;Lcom/samsung/android/allshare/DeviceImpl;)V
    .locals 5
    .param p1, "connector"    # Lcom/samsung/android/allshare/IAllShareConnector;
    .param p2, "deviceImpl"    # Lcom/samsung/android/allshare/DeviceImpl;

    .prologue
    const/4 v4, 0x0

    .line 108
    invoke-direct {p0}, Lcom/samsung/android/allshare/media/Provider;-><init>()V

    .line 56
    iput-object v4, p0, Lcom/samsung/android/allshare/ProviderImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    .line 58
    iput-object v4, p0, Lcom/samsung/android/allshare/ProviderImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    .line 60
    iput-object v4, p0, Lcom/samsung/android/allshare/ProviderImpl;->mBrowseResponseListener:Lcom/samsung/android/allshare/media/Provider$IProviderBrowseResponseListener;

    .line 62
    iput-object v4, p0, Lcom/samsung/android/allshare/ProviderImpl;->mSearchResponseListener:Lcom/samsung/android/allshare/media/Provider$IProviderSearchResponseListener;

    .line 64
    iput-object v4, p0, Lcom/samsung/android/allshare/ProviderImpl;->mProviderEventListener:Lcom/samsung/android/allshare/media/Provider$IProviderEventListener;

    .line 68
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/samsung/android/allshare/ProviderImpl;->mIsSubscribed:Z

    .line 132
    new-instance v2, Lcom/samsung/android/allshare/ProviderImpl$1;

    invoke-static {}, Lcom/samsung/android/allshare/ServiceConnector;->getMainLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v2, p0, v3}, Lcom/samsung/android/allshare/ProviderImpl$1;-><init>(Lcom/samsung/android/allshare/ProviderImpl;Landroid/os/Looper;)V

    iput-object v2, p0, Lcom/samsung/android/allshare/ProviderImpl;->mEventHandler:Lcom/samsung/android/allshare/AllShareEventHandler;

    .line 161
    new-instance v2, Lcom/samsung/android/allshare/ProviderImpl$2;

    invoke-static {}, Lcom/samsung/android/allshare/ServiceConnector;->getMainLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v2, p0, v3}, Lcom/samsung/android/allshare/ProviderImpl$2;-><init>(Lcom/samsung/android/allshare/ProviderImpl;Landroid/os/Looper;)V

    iput-object v2, p0, Lcom/samsung/android/allshare/ProviderImpl;->mAllShareRespHandler:Lcom/samsung/android/allshare/AllShareResponseHandler;

    .line 109
    if-nez p1, :cond_0

    .line 111
    const-string v2, "ProviderImpl"

    const-string v3, "Connection FAIL: AllShare Service Connector does not exist"

    invoke-static {v2, v3}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 130
    :goto_0
    return-void

    .line 115
    :cond_0
    iput-object p2, p0, Lcom/samsung/android/allshare/ProviderImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    .line 116
    iput-object p1, p0, Lcom/samsung/android/allshare/ProviderImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    .line 119
    invoke-virtual {p2}, Lcom/samsung/android/allshare/DeviceImpl;->getBundle()Landroid/os/Bundle;

    move-result-object v0

    .line 120
    .local v0, "bundle":Landroid/os/Bundle;
    if-nez v0, :cond_1

    .line 121
    iput-object v4, p0, Lcom/samsung/android/allshare/ProviderImpl;->mReceiver:Lcom/samsung/android/allshare/media/Receiver;

    .line 122
    const-string v2, "ProviderImpl"

    const-string v3, "bundle == null!"

    invoke-static {v2, v3}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 125
    :cond_1
    const-string v2, "BUNDLE_BOOLEAN_RECEIVERABLE"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 126
    .local v1, "isReceiverable":Ljava/lang/Boolean;
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 127
    new-instance v2, Lcom/samsung/android/allshare/ReceiverImpl;

    invoke-direct {v2, p1, p2}, Lcom/samsung/android/allshare/ReceiverImpl;-><init>(Lcom/samsung/android/allshare/IAllShareConnector;Lcom/samsung/android/allshare/DeviceImpl;)V

    iput-object v2, p0, Lcom/samsung/android/allshare/ProviderImpl;->mReceiver:Lcom/samsung/android/allshare/media/Receiver;

    goto :goto_0

    .line 129
    :cond_2
    iput-object v4, p0, Lcom/samsung/android/allshare/ProviderImpl;->mReceiver:Lcom/samsung/android/allshare/media/Receiver;

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/samsung/android/allshare/ProviderImpl;)Lcom/samsung/android/allshare/media/Provider$IProviderEventListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/ProviderImpl;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/samsung/android/allshare/ProviderImpl;->mProviderEventListener:Lcom/samsung/android/allshare/media/Provider$IProviderEventListener;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/android/allshare/ProviderImpl;)Lcom/samsung/android/allshare/media/Provider$IProviderSearchResponseListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/ProviderImpl;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/samsung/android/allshare/ProviderImpl;->mSearchResponseListener:Lcom/samsung/android/allshare/media/Provider$IProviderSearchResponseListener;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/android/allshare/ProviderImpl;)Lcom/samsung/android/allshare/media/Provider$IProviderBrowseResponseListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/ProviderImpl;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/samsung/android/allshare/ProviderImpl;->mBrowseResponseListener:Lcom/samsung/android/allshare/media/Provider$IProviderBrowseResponseListener;

    return-object v0
.end method


# virtual methods
.method public browse(Lcom/samsung/android/allshare/Item;II)V
    .locals 10
    .param p1, "parentFolderItem"    # Lcom/samsung/android/allshare/Item;
    .param p2, "startIndex"    # I
    .param p3, "requestCount"    # I

    .prologue
    const/4 v5, 0x0

    const/4 v2, -0x1

    .line 301
    iget-object v0, p0, Lcom/samsung/android/allshare/ProviderImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/allshare/ProviderImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    invoke-interface {v0}, Lcom/samsung/android/allshare/IAllShareConnector;->isAllShareServiceConnected()Z

    move-result v0

    if-nez v0, :cond_2

    .line 302
    :cond_0
    const-string v0, "ProviderImpl"

    const-string v1, "browse fail : SERVICE_NOT_CONNECTED"

    invoke-static {v0, v1}, Lcom/samsung/android/allshare/DLog;->i_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 303
    iget-object v0, p0, Lcom/samsung/android/allshare/ProviderImpl;->mBrowseResponseListener:Lcom/samsung/android/allshare/media/Provider$IProviderBrowseResponseListener;

    if-eqz v0, :cond_1

    .line 304
    iget-object v0, p0, Lcom/samsung/android/allshare/ProviderImpl;->mBrowseResponseListener:Lcom/samsung/android/allshare/media/Provider$IProviderBrowseResponseListener;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    const/4 v4, 0x0

    sget-object v6, Lcom/samsung/android/allshare/ERROR;->SERVICE_NOT_CONNECTED:Lcom/samsung/android/allshare/ERROR;

    move v3, v2

    invoke-interface/range {v0 .. v6}, Lcom/samsung/android/allshare/media/Provider$IProviderBrowseResponseListener;->onBrowseResponseReceived(Ljava/util/ArrayList;IILcom/samsung/android/allshare/Item;ZLcom/samsung/android/allshare/ERROR;)V

    .line 343
    .end local p1    # "parentFolderItem":Lcom/samsung/android/allshare/Item;
    :cond_1
    :goto_0
    return-void

    .line 309
    .restart local p1    # "parentFolderItem":Lcom/samsung/android/allshare/Item;
    :cond_2
    if-eqz p1, :cond_3

    if-eq p2, v2, :cond_3

    if-gtz p3, :cond_4

    .line 310
    :cond_3
    const-string v0, "ProviderImpl"

    const-string v1, "browse fail : INVALID_ARGUMENT"

    invoke-static {v0, v1}, Lcom/samsung/android/allshare/DLog;->i_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 311
    iget-object v0, p0, Lcom/samsung/android/allshare/ProviderImpl;->mBrowseResponseListener:Lcom/samsung/android/allshare/media/Provider$IProviderBrowseResponseListener;

    if-eqz v0, :cond_1

    .line 312
    iget-object v0, p0, Lcom/samsung/android/allshare/ProviderImpl;->mBrowseResponseListener:Lcom/samsung/android/allshare/media/Provider$IProviderBrowseResponseListener;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    sget-object v6, Lcom/samsung/android/allshare/ERROR;->INVALID_ARGUMENT:Lcom/samsung/android/allshare/ERROR;

    move v2, p2

    move v3, p3

    move-object v4, p1

    invoke-interface/range {v0 .. v6}, Lcom/samsung/android/allshare/media/Provider$IProviderBrowseResponseListener;->onBrowseResponseReceived(Ljava/util/ArrayList;IILcom/samsung/android/allshare/Item;ZLcom/samsung/android/allshare/ERROR;)V

    goto :goto_0

    .line 317
    :cond_4
    invoke-virtual {p1}, Lcom/samsung/android/allshare/Item;->getType()Lcom/samsung/android/allshare/Item$MediaType;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/allshare/Item$MediaType;->ITEM_FOLDER:Lcom/samsung/android/allshare/Item$MediaType;

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/Item$MediaType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 318
    const-string v0, "ProviderImpl"

    const-string v1, "browse fail : INVALID_ARGUMENT"

    invoke-static {v0, v1}, Lcom/samsung/android/allshare/DLog;->i_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 319
    iget-object v0, p0, Lcom/samsung/android/allshare/ProviderImpl;->mBrowseResponseListener:Lcom/samsung/android/allshare/media/Provider$IProviderBrowseResponseListener;

    if-eqz v0, :cond_1

    .line 320
    iget-object v0, p0, Lcom/samsung/android/allshare/ProviderImpl;->mBrowseResponseListener:Lcom/samsung/android/allshare/media/Provider$IProviderBrowseResponseListener;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    sget-object v6, Lcom/samsung/android/allshare/ERROR;->INVALID_ARGUMENT:Lcom/samsung/android/allshare/ERROR;

    move v2, p2

    move v3, p3

    move-object v4, p1

    invoke-interface/range {v0 .. v6}, Lcom/samsung/android/allshare/media/Provider$IProviderBrowseResponseListener;->onBrowseResponseReceived(Ljava/util/ArrayList;IILcom/samsung/android/allshare/Item;ZLcom/samsung/android/allshare/ERROR;)V

    goto :goto_0

    .line 325
    :cond_5
    new-instance v9, Lcom/sec/android/allshare/iface/CVMessage;

    invoke-direct {v9}, Lcom/sec/android/allshare/iface/CVMessage;-><init>()V

    .line 326
    .local v9, "req_msg":Lcom/sec/android/allshare/iface/CVMessage;
    const-string v0, "com.sec.android.allshare.action.ACTION_PROVIDER_REQUEST_ITEMS"

    invoke-virtual {v9, v0}, Lcom/sec/android/allshare/iface/CVMessage;->setActionID(Ljava/lang/String;)V

    .line 327
    new-instance v8, Landroid/os/Bundle;

    invoke-direct {v8}, Landroid/os/Bundle;-><init>()V

    .line 328
    .local v8, "req_bundle":Landroid/os/Bundle;
    const/4 v7, 0x0

    .line 329
    .local v7, "folderBundle":Landroid/os/Bundle;
    instance-of v0, p1, Lcom/sec/android/allshare/iface/IBundleHolder;

    if-eqz v0, :cond_6

    .line 330
    check-cast p1, Lcom/sec/android/allshare/iface/IBundleHolder;

    .end local p1    # "parentFolderItem":Lcom/samsung/android/allshare/Item;
    invoke-interface {p1}, Lcom/sec/android/allshare/iface/IBundleHolder;->getBundle()Landroid/os/Bundle;

    move-result-object v7

    .line 332
    :cond_6
    const-string v0, "BUNDLE_STRING_ID"

    invoke-virtual {p0}, Lcom/samsung/android/allshare/ProviderImpl;->getID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v8, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 333
    const-string v0, "BUNDLE_PARCELABLE_FOLDERITEM"

    invoke-virtual {v8, v0, v7}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 334
    const-string v0, "BUNDLE_INT_STARTINDEX"

    invoke-virtual {v8, v0, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 335
    const-string v0, "BUNDLE_INT_REQUESTCOUNT"

    invoke-virtual {v8, v0, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 337
    const-string v0, "ProviderImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "browse "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/samsung/android/allshare/ProviderImpl;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", index: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", count: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/allshare/DLog;->i_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 341
    invoke-virtual {v9, v8}, Lcom/sec/android/allshare/iface/CVMessage;->setBundle(Landroid/os/Bundle;)V

    .line 342
    iget-object v0, p0, Lcom/samsung/android/allshare/ProviderImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    iget-object v1, p0, Lcom/samsung/android/allshare/ProviderImpl;->mAllShareRespHandler:Lcom/samsung/android/allshare/AllShareResponseHandler;

    invoke-interface {v0, v9, v1}, Lcom/samsung/android/allshare/IAllShareConnector;->requestCVMAsync(Lcom/sec/android/allshare/iface/CVMessage;Lcom/samsung/android/allshare/AllShareResponseHandler;)J

    goto/16 :goto_0
.end method

.method public getBundle()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 423
    iget-object v0, p0, Lcom/samsung/android/allshare/ProviderImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    if-nez v0, :cond_0

    .line 424
    const/4 v0, 0x0

    .line 426
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/allshare/ProviderImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/DeviceImpl;->getBundle()Landroid/os/Bundle;

    move-result-object v0

    goto :goto_0
.end method

.method public getDeviceDomain()Lcom/samsung/android/allshare/Device$DeviceDomain;
    .locals 1

    .prologue
    .line 352
    iget-object v0, p0, Lcom/samsung/android/allshare/ProviderImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    if-nez v0, :cond_0

    .line 353
    sget-object v0, Lcom/samsung/android/allshare/Device$DeviceDomain;->UNKNOWN:Lcom/samsung/android/allshare/Device$DeviceDomain;

    .line 355
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/allshare/ProviderImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/DeviceImpl;->getDeviceDomain()Lcom/samsung/android/allshare/Device$DeviceDomain;

    move-result-object v0

    goto :goto_0
.end method

.method public getDeviceType()Lcom/samsung/android/allshare/Device$DeviceType;
    .locals 1

    .prologue
    .line 392
    iget-object v0, p0, Lcom/samsung/android/allshare/ProviderImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    if-nez v0, :cond_0

    .line 393
    sget-object v0, Lcom/samsung/android/allshare/Device$DeviceType;->UNKNOWN:Lcom/samsung/android/allshare/Device$DeviceType;

    .line 395
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/allshare/ProviderImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/DeviceImpl;->getDeviceType()Lcom/samsung/android/allshare/Device$DeviceType;

    move-result-object v0

    goto :goto_0
.end method

.method public getID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 360
    iget-object v0, p0, Lcom/samsung/android/allshare/ProviderImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    if-nez v0, :cond_0

    .line 361
    const-string v0, ""

    .line 363
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/allshare/ProviderImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/DeviceImpl;->getID()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getIPAddress()Ljava/lang/String;
    .locals 1

    .prologue
    .line 415
    iget-object v0, p0, Lcom/samsung/android/allshare/ProviderImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    if-nez v0, :cond_0

    .line 416
    const-string v0, ""

    .line 418
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/allshare/ProviderImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/DeviceImpl;->getIPAddress()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getIPAdress()Ljava/lang/String;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 410
    invoke-virtual {p0}, Lcom/samsung/android/allshare/ProviderImpl;->getIPAddress()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getIcon()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 368
    iget-object v0, p0, Lcom/samsung/android/allshare/ProviderImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    if-nez v0, :cond_0

    .line 369
    const/4 v0, 0x0

    .line 371
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/allshare/ProviderImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/DeviceImpl;->getIcon()Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method public getIconList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/Icon;",
            ">;"
        }
    .end annotation

    .prologue
    .line 376
    iget-object v0, p0, Lcom/samsung/android/allshare/ProviderImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    if-nez v0, :cond_0

    .line 377
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 379
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/allshare/ProviderImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/DeviceImpl;->getIconList()Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_0
.end method

.method public getModelName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 400
    iget-object v0, p0, Lcom/samsung/android/allshare/ProviderImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    if-nez v0, :cond_0

    .line 401
    const-string v0, ""

    .line 403
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/allshare/ProviderImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/DeviceImpl;->getModelName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getNIC()Ljava/lang/String;
    .locals 1

    .prologue
    .line 537
    iget-object v0, p0, Lcom/samsung/android/allshare/ProviderImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    if-nez v0, :cond_0

    .line 538
    const-string v0, ""

    .line 540
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/allshare/ProviderImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/DeviceImpl;->getNIC()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 384
    iget-object v0, p0, Lcom/samsung/android/allshare/ProviderImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    if-nez v0, :cond_0

    .line 385
    const-string v0, ""

    .line 387
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/allshare/ProviderImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/DeviceImpl;->getName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getP2pMacAddress()Ljava/lang/String;
    .locals 1

    .prologue
    .line 570
    const-string v0, ""

    return-object v0
.end method

.method public getReceiver()Lcom/samsung/android/allshare/media/Receiver;
    .locals 1

    .prologue
    .line 432
    iget-object v0, p0, Lcom/samsung/android/allshare/ProviderImpl;->mReceiver:Lcom/samsung/android/allshare/media/Receiver;

    return-object v0
.end method

.method public getRootFolder()Lcom/samsung/android/allshare/Item;
    .locals 1

    .prologue
    .line 287
    sget-object v0, Lcom/samsung/android/allshare/ProviderImpl;->mRootFolderItem:Lcom/samsung/android/allshare/Item;

    return-object v0
.end method

.method public isSearchable()Z
    .locals 2

    .prologue
    .line 292
    invoke-virtual {p0}, Lcom/samsung/android/allshare/ProviderImpl;->getBundle()Landroid/os/Bundle;

    move-result-object v0

    .line 293
    .local v0, "result":Landroid/os/Bundle;
    if-nez v0, :cond_0

    .line 294
    const/4 v1, 0x0

    .line 296
    :goto_0
    return v1

    :cond_0
    const-string v1, "BUNDLE_BOOLEAN_SEARCHABLE"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    goto :goto_0
.end method

.method public isSeekableOnPaused()Z
    .locals 1

    .prologue
    .line 557
    const/4 v0, 0x0

    return v0
.end method

.method public isWholeHomeAudio()Z
    .locals 1

    .prologue
    .line 562
    const/4 v0, 0x0

    return v0
.end method

.method public removeEventHandler()V
    .locals 4

    .prologue
    .line 545
    iget-object v0, p0, Lcom/samsung/android/allshare/ProviderImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    const-string v1, "com.sec.android.allshare.event.EVENT_DEVICE_SUBSCRIBE"

    invoke-virtual {p0}, Lcom/samsung/android/allshare/ProviderImpl;->getBundle()Landroid/os/Bundle;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/allshare/ProviderImpl;->mEventHandler:Lcom/samsung/android/allshare/AllShareEventHandler;

    invoke-interface {v0, v1, v2, v3}, Lcom/samsung/android/allshare/IAllShareConnector;->unsubscribeAllShareEvent(Ljava/lang/String;Landroid/os/Bundle;Lcom/samsung/android/allshare/AllShareEventHandler;)V

    .line 547
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/allshare/ProviderImpl;->mIsSubscribed:Z

    .line 549
    iget-object v0, p0, Lcom/samsung/android/allshare/ProviderImpl;->mReceiver:Lcom/samsung/android/allshare/media/Receiver;

    if-eqz v0, :cond_0

    .line 550
    iget-object v0, p0, Lcom/samsung/android/allshare/ProviderImpl;->mReceiver:Lcom/samsung/android/allshare/media/Receiver;

    check-cast v0, Lcom/samsung/android/allshare/ReceiverImpl;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/ReceiverImpl;->removeEventHandler()V

    .line 552
    :cond_0
    return-void
.end method

.method public search(Lcom/samsung/android/allshare/media/SearchCriteria;II)V
    .locals 12
    .param p1, "searchCriteria"    # Lcom/samsung/android/allshare/media/SearchCriteria;
    .param p2, "startIndex"    # I
    .param p3, "requestCount"    # I

    .prologue
    const/4 v2, -0x1

    const/4 v5, 0x0

    .line 460
    iget-object v0, p0, Lcom/samsung/android/allshare/ProviderImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/allshare/ProviderImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    invoke-interface {v0}, Lcom/samsung/android/allshare/IAllShareConnector;->isAllShareServiceConnected()Z

    move-result v0

    if-nez v0, :cond_2

    .line 461
    :cond_0
    const-string v0, "ProviderImpl"

    const-string v1, "search fail : SERVICE_NOT_CONNECTED"

    invoke-static {v0, v1}, Lcom/samsung/android/allshare/DLog;->i_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 462
    iget-object v0, p0, Lcom/samsung/android/allshare/ProviderImpl;->mSearchResponseListener:Lcom/samsung/android/allshare/media/Provider$IProviderSearchResponseListener;

    if-eqz v0, :cond_1

    .line 463
    iget-object v0, p0, Lcom/samsung/android/allshare/ProviderImpl;->mSearchResponseListener:Lcom/samsung/android/allshare/media/Provider$IProviderSearchResponseListener;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    sget-object v6, Lcom/samsung/android/allshare/ERROR;->SERVICE_NOT_CONNECTED:Lcom/samsung/android/allshare/ERROR;

    move v3, v2

    move-object v4, p1

    invoke-interface/range {v0 .. v6}, Lcom/samsung/android/allshare/media/Provider$IProviderSearchResponseListener;->onSearchResponseReceived(Ljava/util/ArrayList;IILcom/samsung/android/allshare/media/SearchCriteria;ZLcom/samsung/android/allshare/ERROR;)V

    .line 527
    :cond_1
    :goto_0
    return-void

    .line 468
    :cond_2
    if-nez p1, :cond_3

    .line 469
    const-string v0, "ProviderImpl"

    const-string v1, "search fail : INVALID_ARGUMENT (searchCriteria)"

    invoke-static {v0, v1}, Lcom/samsung/android/allshare/DLog;->i_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 470
    iget-object v0, p0, Lcom/samsung/android/allshare/ProviderImpl;->mSearchResponseListener:Lcom/samsung/android/allshare/media/Provider$IProviderSearchResponseListener;

    if-eqz v0, :cond_1

    .line 471
    iget-object v0, p0, Lcom/samsung/android/allshare/ProviderImpl;->mSearchResponseListener:Lcom/samsung/android/allshare/media/Provider$IProviderSearchResponseListener;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    sget-object v6, Lcom/samsung/android/allshare/ERROR;->INVALID_ARGUMENT:Lcom/samsung/android/allshare/ERROR;

    move v2, p2

    move v3, p3

    move-object v4, p1

    invoke-interface/range {v0 .. v6}, Lcom/samsung/android/allshare/media/Provider$IProviderSearchResponseListener;->onSearchResponseReceived(Ljava/util/ArrayList;IILcom/samsung/android/allshare/media/SearchCriteria;ZLcom/samsung/android/allshare/ERROR;)V

    goto :goto_0

    .line 476
    :cond_3
    if-eq p2, v2, :cond_4

    if-gtz p3, :cond_5

    .line 477
    :cond_4
    const-string v0, "ProviderImpl"

    const-string v1, "search fail : INVALID_ARGUMENT (index)"

    invoke-static {v0, v1}, Lcom/samsung/android/allshare/DLog;->i_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 478
    iget-object v0, p0, Lcom/samsung/android/allshare/ProviderImpl;->mSearchResponseListener:Lcom/samsung/android/allshare/media/Provider$IProviderSearchResponseListener;

    if-eqz v0, :cond_1

    .line 479
    iget-object v0, p0, Lcom/samsung/android/allshare/ProviderImpl;->mSearchResponseListener:Lcom/samsung/android/allshare/media/Provider$IProviderSearchResponseListener;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    sget-object v6, Lcom/samsung/android/allshare/ERROR;->INVALID_ARGUMENT:Lcom/samsung/android/allshare/ERROR;

    move v2, p2

    move v3, p3

    move-object v4, p1

    invoke-interface/range {v0 .. v6}, Lcom/samsung/android/allshare/media/Provider$IProviderSearchResponseListener;->onSearchResponseReceived(Ljava/util/ArrayList;IILcom/samsung/android/allshare/media/SearchCriteria;ZLcom/samsung/android/allshare/ERROR;)V

    goto :goto_0

    .line 484
    :cond_5
    new-instance v8, Lcom/sec/android/allshare/iface/CVMessage;

    invoke-direct {v8}, Lcom/sec/android/allshare/iface/CVMessage;-><init>()V

    .line 485
    .local v8, "req_msg":Lcom/sec/android/allshare/iface/CVMessage;
    const-string v0, "com.sec.android.allshare.action.ACTION_PROVIDER_REQUEST_SEARCHCRITERIA_ITEMS"

    invoke-virtual {v8, v0}, Lcom/sec/android/allshare/iface/CVMessage;->setActionID(Ljava/lang/String;)V

    .line 486
    new-instance v7, Landroid/os/Bundle;

    invoke-direct {v7}, Landroid/os/Bundle;-><init>()V

    .line 487
    .local v7, "req_bundle":Landroid/os/Bundle;
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 490
    .local v9, "typeList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    sget-object v0, Lcom/samsung/android/allshare/Item$MediaType;->ITEM_UNKNOWN:Lcom/samsung/android/allshare/Item$MediaType;

    invoke-virtual {p1, v0}, Lcom/samsung/android/allshare/media/SearchCriteria;->isMatchedItemType(Lcom/samsung/android/allshare/Item$MediaType;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 491
    const-string v0, "ProviderImpl"

    const-string v1, "search fail : INVALID_ARGUMENT (MediaType)"

    invoke-static {v0, v1}, Lcom/samsung/android/allshare/DLog;->i_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 492
    iget-object v0, p0, Lcom/samsung/android/allshare/ProviderImpl;->mSearchResponseListener:Lcom/samsung/android/allshare/media/Provider$IProviderSearchResponseListener;

    if-eqz v0, :cond_1

    .line 493
    iget-object v0, p0, Lcom/samsung/android/allshare/ProviderImpl;->mSearchResponseListener:Lcom/samsung/android/allshare/media/Provider$IProviderSearchResponseListener;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    sget-object v6, Lcom/samsung/android/allshare/ERROR;->INVALID_ARGUMENT:Lcom/samsung/android/allshare/ERROR;

    move v2, p2

    move v3, p3

    move-object v4, p1

    invoke-interface/range {v0 .. v6}, Lcom/samsung/android/allshare/media/Provider$IProviderSearchResponseListener;->onSearchResponseReceived(Ljava/util/ArrayList;IILcom/samsung/android/allshare/media/SearchCriteria;ZLcom/samsung/android/allshare/ERROR;)V

    goto :goto_0

    .line 498
    :cond_6
    sget-object v0, Lcom/samsung/android/allshare/Item$MediaType;->ITEM_AUDIO:Lcom/samsung/android/allshare/Item$MediaType;

    invoke-virtual {p1, v0}, Lcom/samsung/android/allshare/media/SearchCriteria;->isMatchedItemType(Lcom/samsung/android/allshare/Item$MediaType;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 499
    sget-object v0, Lcom/samsung/android/allshare/Item$MediaType;->ITEM_AUDIO:Lcom/samsung/android/allshare/Item$MediaType;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/Item$MediaType;->enumToString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 501
    :cond_7
    sget-object v0, Lcom/samsung/android/allshare/Item$MediaType;->ITEM_VIDEO:Lcom/samsung/android/allshare/Item$MediaType;

    invoke-virtual {p1, v0}, Lcom/samsung/android/allshare/media/SearchCriteria;->isMatchedItemType(Lcom/samsung/android/allshare/Item$MediaType;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 502
    sget-object v0, Lcom/samsung/android/allshare/Item$MediaType;->ITEM_VIDEO:Lcom/samsung/android/allshare/Item$MediaType;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/Item$MediaType;->enumToString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 504
    :cond_8
    sget-object v0, Lcom/samsung/android/allshare/Item$MediaType;->ITEM_IMAGE:Lcom/samsung/android/allshare/Item$MediaType;

    invoke-virtual {p1, v0}, Lcom/samsung/android/allshare/media/SearchCriteria;->isMatchedItemType(Lcom/samsung/android/allshare/Item$MediaType;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 505
    sget-object v0, Lcom/samsung/android/allshare/Item$MediaType;->ITEM_IMAGE:Lcom/samsung/android/allshare/Item$MediaType;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/Item$MediaType;->enumToString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 507
    :cond_9
    sget-object v0, Lcom/samsung/android/allshare/Item$MediaType;->ITEM_FOLDER:Lcom/samsung/android/allshare/Item$MediaType;

    invoke-virtual {p1, v0}, Lcom/samsung/android/allshare/media/SearchCriteria;->isMatchedItemType(Lcom/samsung/android/allshare/Item$MediaType;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 508
    sget-object v0, Lcom/samsung/android/allshare/Item$MediaType;->ITEM_FOLDER:Lcom/samsung/android/allshare/Item$MediaType;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/Item$MediaType;->enumToString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 511
    :cond_a
    const-string v0, "BUNDLE_STRING_ID"

    invoke-virtual {p0}, Lcom/samsung/android/allshare/ProviderImpl;->getID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 512
    const-string v0, "BUNDLE_STRING_SEARCHSTRING"

    invoke-virtual {p1}, Lcom/samsung/android/allshare/media/SearchCriteria;->getKeyword()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 513
    const-string v0, "BUNDLE_INT_STARTINDEX"

    invoke-virtual {v7, v0, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 514
    const-string v0, "BUNDLE_INT_REQUESTCOUNT"

    invoke-virtual {v7, v0, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 515
    const-string v0, "BUNDLE_STRING_ITEM_TYPE_ARRAYLIST"

    invoke-virtual {v7, v0, v9}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 517
    const-string v0, "ProviderImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "search "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/samsung/android/allshare/ProviderImpl;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", index: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", count: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/allshare/DLog;->i_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 519
    invoke-virtual {v8, v7}, Lcom/sec/android/allshare/iface/CVMessage;->setBundle(Landroid/os/Bundle;)V

    .line 520
    iget-object v0, p0, Lcom/samsung/android/allshare/ProviderImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    iget-object v1, p0, Lcom/samsung/android/allshare/ProviderImpl;->mAllShareRespHandler:Lcom/samsung/android/allshare/AllShareResponseHandler;

    invoke-interface {v0, v8, v1}, Lcom/samsung/android/allshare/IAllShareConnector;->requestCVMAsync(Lcom/sec/android/allshare/iface/CVMessage;Lcom/samsung/android/allshare/AllShareResponseHandler;)J

    move-result-wide v10

    .line 521
    .local v10, "retVal":J
    const-wide/16 v0, -0x1

    cmp-long v0, v0, v10

    if-nez v0, :cond_1

    .line 522
    const-string v0, "ProviderImpl"

    const-string v1, "Search fail : Error in sending request to Allshare Service"

    invoke-static {v0, v1}, Lcom/samsung/android/allshare/DLog;->i_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 523
    iget-object v0, p0, Lcom/samsung/android/allshare/ProviderImpl;->mSearchResponseListener:Lcom/samsung/android/allshare/media/Provider$IProviderSearchResponseListener;

    if-eqz v0, :cond_1

    .line 524
    iget-object v0, p0, Lcom/samsung/android/allshare/ProviderImpl;->mSearchResponseListener:Lcom/samsung/android/allshare/media/Provider$IProviderSearchResponseListener;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    sget-object v6, Lcom/samsung/android/allshare/ERROR;->FAIL:Lcom/samsung/android/allshare/ERROR;

    move v2, p2

    move v3, p3

    move-object v4, p1

    invoke-interface/range {v0 .. v6}, Lcom/samsung/android/allshare/media/Provider$IProviderSearchResponseListener;->onSearchResponseReceived(Ljava/util/ArrayList;IILcom/samsung/android/allshare/media/SearchCriteria;ZLcom/samsung/android/allshare/ERROR;)V

    goto/16 :goto_0
.end method

.method public setBrowseItemsResponseListener(Lcom/samsung/android/allshare/media/Provider$IProviderBrowseResponseListener;)V
    .locals 0
    .param p1, "l"    # Lcom/samsung/android/allshare/media/Provider$IProviderBrowseResponseListener;

    .prologue
    .line 347
    iput-object p1, p0, Lcom/samsung/android/allshare/ProviderImpl;->mBrowseResponseListener:Lcom/samsung/android/allshare/media/Provider$IProviderBrowseResponseListener;

    .line 348
    return-void
.end method

.method public setEventListener(Lcom/samsung/android/allshare/media/Provider$IProviderEventListener;)V
    .locals 4
    .param p1, "l"    # Lcom/samsung/android/allshare/media/Provider$IProviderEventListener;

    .prologue
    .line 437
    iget-object v0, p0, Lcom/samsung/android/allshare/ProviderImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/allshare/ProviderImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    invoke-interface {v0}, Lcom/samsung/android/allshare/IAllShareConnector;->isAllShareServiceConnected()Z

    move-result v0

    if-nez v0, :cond_2

    .line 438
    :cond_0
    const-string v0, "ProviderImpl"

    const-string v1, "setEventListener error! AllShareService is not connected"

    invoke-static {v0, v1}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 455
    :cond_1
    :goto_0
    return-void

    .line 442
    :cond_2
    iput-object p1, p0, Lcom/samsung/android/allshare/ProviderImpl;->mProviderEventListener:Lcom/samsung/android/allshare/media/Provider$IProviderEventListener;

    .line 444
    iget-boolean v0, p0, Lcom/samsung/android/allshare/ProviderImpl;->mIsSubscribed:Z

    if-nez v0, :cond_3

    if-eqz p1, :cond_3

    .line 445
    iget-object v0, p0, Lcom/samsung/android/allshare/ProviderImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    const-string v1, "com.sec.android.allshare.event.EVENT_DEVICE_SUBSCRIBE"

    iget-object v2, p0, Lcom/samsung/android/allshare/ProviderImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    invoke-virtual {v2}, Lcom/samsung/android/allshare/DeviceImpl;->getBundle()Landroid/os/Bundle;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/allshare/ProviderImpl;->mEventHandler:Lcom/samsung/android/allshare/AllShareEventHandler;

    invoke-interface {v0, v1, v2, v3}, Lcom/samsung/android/allshare/IAllShareConnector;->subscribeAllShareEvent(Ljava/lang/String;Landroid/os/Bundle;Lcom/samsung/android/allshare/AllShareEventHandler;)Z

    .line 447
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/allshare/ProviderImpl;->mIsSubscribed:Z

    goto :goto_0

    .line 448
    :cond_3
    iget-boolean v0, p0, Lcom/samsung/android/allshare/ProviderImpl;->mIsSubscribed:Z

    if-eqz v0, :cond_1

    if-nez p1, :cond_1

    .line 449
    iget-object v0, p0, Lcom/samsung/android/allshare/ProviderImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    const-string v1, "com.sec.android.allshare.event.EVENT_DEVICE_SUBSCRIBE"

    iget-object v2, p0, Lcom/samsung/android/allshare/ProviderImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    invoke-virtual {v2}, Lcom/samsung/android/allshare/DeviceImpl;->getBundle()Landroid/os/Bundle;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/allshare/ProviderImpl;->mEventHandler:Lcom/samsung/android/allshare/AllShareEventHandler;

    invoke-interface {v0, v1, v2, v3}, Lcom/samsung/android/allshare/IAllShareConnector;->unsubscribeAllShareEvent(Ljava/lang/String;Landroid/os/Bundle;Lcom/samsung/android/allshare/AllShareEventHandler;)V

    .line 451
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/allshare/ProviderImpl;->mIsSubscribed:Z

    goto :goto_0
.end method

.method public setSearchResponseListener(Lcom/samsung/android/allshare/media/Provider$IProviderSearchResponseListener;)V
    .locals 0
    .param p1, "l"    # Lcom/samsung/android/allshare/media/Provider$IProviderSearchResponseListener;

    .prologue
    .line 531
    iput-object p1, p0, Lcom/samsung/android/allshare/ProviderImpl;->mSearchResponseListener:Lcom/samsung/android/allshare/media/Provider$IProviderSearchResponseListener;

    .line 533
    return-void
.end method
