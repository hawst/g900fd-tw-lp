.class Lcom/samsung/android/allshare/ServiceConnector$AllShareConnector$2;
.super Landroid/content/BroadcastReceiver;
.source "ServiceConnector.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/allshare/ServiceConnector$AllShareConnector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/allshare/ServiceConnector$AllShareConnector;


# direct methods
.method constructor <init>(Lcom/samsung/android/allshare/ServiceConnector$AllShareConnector;)V
    .locals 0

    .prologue
    .line 1028
    iput-object p1, p0, Lcom/samsung/android/allshare/ServiceConnector$AllShareConnector$2;->this$0:Lcom/samsung/android/allshare/ServiceConnector$AllShareConnector;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 1033
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 1034
    .local v0, "action":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 1035
    const-string v1, "AllShareConnector"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/samsung/android/allshare/ServiceConnector$AllShareConnector$2;->this$0:Lcom/samsung/android/allshare/ServiceConnector$AllShareConnector;

    # getter for: Lcom/samsung/android/allshare/ServiceConnector$AllShareConnector;->mSubscriberTag:Ljava/lang/String;
    invoke-static {v3}, Lcom/samsung/android/allshare/ServiceConnector$AllShareConnector;->access$100(Lcom/samsung/android/allshare/ServiceConnector$AllShareConnector;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " : intent.getAction() == null!"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 1052
    :goto_0
    return-void

    .line 1039
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/allshare/ServiceConnector$AllShareConnector$2;->this$0:Lcom/samsung/android/allshare/ServiceConnector$AllShareConnector;

    # getter for: Lcom/samsung/android/allshare/ServiceConnector$AllShareConnector;->mConstData:Lcom/samsung/android/allshare/ServiceConnector$ProfileConstData;
    invoke-static {v1}, Lcom/samsung/android/allshare/ServiceConnector$AllShareConnector;->access$600(Lcom/samsung/android/allshare/ServiceConnector$AllShareConnector;)Lcom/samsung/android/allshare/ServiceConnector$ProfileConstData;

    move-result-object v1

    iget-object v1, v1, Lcom/samsung/android/allshare/ServiceConnector$ProfileConstData;->START_MESSAGE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1040
    const-string v1, "AllShareConnector"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/samsung/android/allshare/ServiceConnector$AllShareConnector$2;->this$0:Lcom/samsung/android/allshare/ServiceConnector$AllShareConnector;

    # getter for: Lcom/samsung/android/allshare/ServiceConnector$AllShareConnector;->mSubscriberTag:Ljava/lang/String;
    invoke-static {v3}, Lcom/samsung/android/allshare/ServiceConnector$AllShareConnector;->access$100(Lcom/samsung/android/allshare/ServiceConnector$AllShareConnector;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " : onReceive AllShare Service Start message...^^"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/allshare/DLog;->d_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 1043
    iget-object v1, p0, Lcom/samsung/android/allshare/ServiceConnector$AllShareConnector$2;->this$0:Lcom/samsung/android/allshare/ServiceConnector$AllShareConnector;

    invoke-virtual {v1}, Lcom/samsung/android/allshare/ServiceConnector$AllShareConnector;->connect()V

    goto :goto_0

    .line 1044
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/allshare/ServiceConnector$AllShareConnector$2;->this$0:Lcom/samsung/android/allshare/ServiceConnector$AllShareConnector;

    # getter for: Lcom/samsung/android/allshare/ServiceConnector$AllShareConnector;->mConstData:Lcom/samsung/android/allshare/ServiceConnector$ProfileConstData;
    invoke-static {v1}, Lcom/samsung/android/allshare/ServiceConnector$AllShareConnector;->access$600(Lcom/samsung/android/allshare/ServiceConnector$AllShareConnector;)Lcom/samsung/android/allshare/ServiceConnector$ProfileConstData;

    move-result-object v1

    iget-object v1, v1, Lcom/samsung/android/allshare/ServiceConnector$ProfileConstData;->STOP_MESSAGE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1045
    const-string v1, "AllShareConnector"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/samsung/android/allshare/ServiceConnector$AllShareConnector$2;->this$0:Lcom/samsung/android/allshare/ServiceConnector$AllShareConnector;

    # getter for: Lcom/samsung/android/allshare/ServiceConnector$AllShareConnector;->mSubscriberTag:Ljava/lang/String;
    invoke-static {v3}, Lcom/samsung/android/allshare/ServiceConnector$AllShareConnector;->access$100(Lcom/samsung/android/allshare/ServiceConnector$AllShareConnector;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " : onReceive AllShare Service Stop message...^^"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/allshare/DLog;->d_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 1047
    iget-object v1, p0, Lcom/samsung/android/allshare/ServiceConnector$AllShareConnector$2;->this$0:Lcom/samsung/android/allshare/ServiceConnector$AllShareConnector;

    # invokes: Lcom/samsung/android/allshare/ServiceConnector$AllShareConnector;->onDisconnected()V
    invoke-static {v1}, Lcom/samsung/android/allshare/ServiceConnector$AllShareConnector;->access$500(Lcom/samsung/android/allshare/ServiceConnector$AllShareConnector;)V

    goto :goto_0

    .line 1049
    :cond_2
    const-string v1, "AllShareConnector"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/samsung/android/allshare/ServiceConnector$AllShareConnector$2;->this$0:Lcom/samsung/android/allshare/ServiceConnector$AllShareConnector;

    # getter for: Lcom/samsung/android/allshare/ServiceConnector$AllShareConnector;->mSubscriberTag:Ljava/lang/String;
    invoke-static {v3}, Lcom/samsung/android/allshare/ServiceConnector$AllShareConnector;->access$100(Lcom/samsung/android/allshare/ServiceConnector$AllShareConnector;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " : onReceive Unknown action - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method
