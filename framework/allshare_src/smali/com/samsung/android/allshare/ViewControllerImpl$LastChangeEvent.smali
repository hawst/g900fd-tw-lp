.class Lcom/samsung/android/allshare/ViewControllerImpl$LastChangeEvent;
.super Ljava/lang/Object;
.source "ViewControllerImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/allshare/ViewControllerImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "LastChangeEvent"
.end annotation


# instance fields
.field private mEventID:Ljava/lang/String;

.field private mPowerOff:Ljava/lang/String;

.field final synthetic this$0:Lcom/samsung/android/allshare/ViewControllerImpl;


# direct methods
.method constructor <init>(Lcom/samsung/android/allshare/ViewControllerImpl;)V
    .locals 0

    .prologue
    .line 784
    iput-object p1, p0, Lcom/samsung/android/allshare/ViewControllerImpl$LastChangeEvent;->this$0:Lcom/samsung/android/allshare/ViewControllerImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private clean()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 790
    iput-object v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl$LastChangeEvent;->mEventID:Ljava/lang/String;

    .line 792
    iput-object v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl$LastChangeEvent;->mPowerOff:Ljava/lang/String;

    .line 794
    return-void
.end method

.method private setValue(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 797
    if-eqz p1, :cond_0

    .line 798
    const-string v0, "EventID"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 799
    iput-object p2, p0, Lcom/samsung/android/allshare/ViewControllerImpl$LastChangeEvent;->mEventID:Ljava/lang/String;

    .line 804
    :cond_0
    :goto_0
    return-void

    .line 800
    :cond_1
    const-string v0, "PowerOFF"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 801
    iput-object p2, p0, Lcom/samsung/android/allshare/ViewControllerImpl$LastChangeEvent;->mPowerOff:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public getEventID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 807
    iget-object v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl$LastChangeEvent;->mEventID:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 808
    iget-object v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl$LastChangeEvent;->mEventID:Ljava/lang/String;

    .line 810
    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public getPowerOff()Ljava/lang/String;
    .locals 1

    .prologue
    .line 814
    iget-object v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl$LastChangeEvent;->mPowerOff:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 815
    iget-object v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl$LastChangeEvent;->mPowerOff:Ljava/lang/String;

    .line 817
    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public parseFromXML(Ljava/lang/String;)V
    .locals 8
    .param p1, "xml"    # Ljava/lang/String;

    .prologue
    .line 821
    invoke-direct {p0}, Lcom/samsung/android/allshare/ViewControllerImpl$LastChangeEvent;->clean()V

    .line 823
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v5

    if-gtz v5, :cond_2

    .line 824
    :cond_0
    const-string v5, "ViewControllerImpl"

    const-string v6, "[ViewControl] LastChangeEvent.parseFromXML() paramter xml is null or empty!"

    invoke-static {v5, v6}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 868
    :cond_1
    :goto_0
    return-void

    .line 829
    :cond_2
    invoke-static {}, Landroid/util/Xml;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v3

    .line 832
    .local v3, "parser":Lorg/xmlpull/v1/XmlPullParser;
    const/4 v2, 0x0

    .line 833
    .local v2, "key":Ljava/lang/String;
    const/4 v4, 0x0

    .line 835
    .local v4, "value":Ljava/lang/String;
    :try_start_0
    new-instance v5, Ljava/io/StringReader;

    invoke-direct {v5, p1}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-interface {v3, v5}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/Reader;)V

    .line 836
    invoke-interface {v3}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v1

    .line 838
    .local v1, "eventType":I
    :goto_1
    const/4 v5, 0x1

    if-eq v1, v5, :cond_1

    .line 839
    packed-switch v1, :pswitch_data_0

    .line 858
    :cond_3
    :goto_2
    :pswitch_0
    invoke-interface {v3}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v1

    goto :goto_1

    .line 844
    :pswitch_1
    invoke-interface {v3}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v2

    .line 845
    goto :goto_2

    .line 848
    :pswitch_2
    if-eqz v2, :cond_3

    if-eqz v4, :cond_3

    .line 849
    invoke-direct {p0, v2, v4}, Lcom/samsung/android/allshare/ViewControllerImpl$LastChangeEvent;->setValue(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_2

    .line 860
    .end local v1    # "eventType":I
    :catch_0
    move-exception v0

    .line 861
    .local v0, "e":Lorg/xmlpull/v1/XmlPullParserException;
    const-string v5, "ViewControllerImpl"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "[ViewControl] LastChangeEvent.parseFromXML() exception : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Lorg/xmlpull/v1/XmlPullParserException;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 854
    .end local v0    # "e":Lorg/xmlpull/v1/XmlPullParserException;
    .restart local v1    # "eventType":I
    :pswitch_3
    :try_start_1
    invoke-interface {v3}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;
    :try_end_1
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_2

    move-result-object v4

    goto :goto_2

    .line 863
    .end local v1    # "eventType":I
    :catch_1
    move-exception v0

    .line 864
    .local v0, "e":Ljava/io/IOException;
    const-string v5, "ViewControllerImpl"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "[ViewControl] LastChangeEvent.parseFromXML() exception : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 866
    .end local v0    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v5

    goto :goto_0

    .line 839
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
