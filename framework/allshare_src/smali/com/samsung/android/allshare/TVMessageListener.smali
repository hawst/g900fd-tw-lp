.class Lcom/samsung/android/allshare/TVMessageListener;
.super Ljava/lang/Thread;
.source "IAppControlAPI.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "TVMessageListener"


# instance fields
.field mBuf:Ljava/nio/ByteBuffer;

.field mCbuf:Ljava/nio/CharBuffer;

.field mContext:Landroid/content/Context;

.field private mEventListener:Lcom/samsung/android/allshare/IAppControlAPI$IControlEventListener;

.field mInput:Ljava/nio/channels/ReadableByteChannel;

.field mProtocol:I

.field mRebuf:[B

.field public mRunThread:Z

.field mSender:Lcom/samsung/android/allshare/TVMessageSender;

.field mSocket:Ljava/net/Socket;

.field mStrCamera:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/samsung/android/allshare/TVMessageSender;)V
    .locals 4
    .param p1, "sender"    # Lcom/samsung/android/allshare/TVMessageSender;

    .prologue
    const/16 v3, 0x200

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 523
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 484
    iput-object v1, p0, Lcom/samsung/android/allshare/TVMessageListener;->mSocket:Ljava/net/Socket;

    .line 492
    new-array v0, v3, [B

    iput-object v0, p0, Lcom/samsung/android/allshare/TVMessageListener;->mRebuf:[B

    .line 498
    const-string v0, "live_camera.jpg"

    iput-object v0, p0, Lcom/samsung/android/allshare/TVMessageListener;->mStrCamera:Ljava/lang/String;

    .line 500
    iput v2, p0, Lcom/samsung/android/allshare/TVMessageListener;->mProtocol:I

    .line 502
    iput-boolean v2, p0, Lcom/samsung/android/allshare/TVMessageListener;->mRunThread:Z

    .line 504
    iput-object v1, p0, Lcom/samsung/android/allshare/TVMessageListener;->mSender:Lcom/samsung/android/allshare/TVMessageSender;

    .line 510
    iput-object v1, p0, Lcom/samsung/android/allshare/TVMessageListener;->mEventListener:Lcom/samsung/android/allshare/IAppControlAPI$IControlEventListener;

    .line 525
    const/16 v0, 0x800

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/allshare/TVMessageListener;->mBuf:Ljava/nio/ByteBuffer;

    .line 526
    invoke-static {v3}, Ljava/nio/CharBuffer;->allocate(I)Ljava/nio/CharBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/allshare/TVMessageListener;->mCbuf:Ljava/nio/CharBuffer;

    .line 530
    const/4 v0, 0x1

    iput v0, p0, Lcom/samsung/android/allshare/TVMessageListener;->mProtocol:I

    .line 532
    iput-object p1, p0, Lcom/samsung/android/allshare/TVMessageListener;->mSender:Lcom/samsung/android/allshare/TVMessageSender;

    .line 534
    if-eqz p1, :cond_0

    .line 535
    iget-object v0, p1, Lcom/samsung/android/allshare/TVMessageSender;->mSocket:Ljava/net/Socket;

    iput-object v0, p0, Lcom/samsung/android/allshare/TVMessageListener;->mSocket:Ljava/net/Socket;

    .line 537
    :cond_0
    return-void
.end method


# virtual methods
.method public deliverMsgData(III)V
    .locals 3
    .param p1, "what"    # I
    .param p2, "arg1"    # I
    .param p3, "arg2"    # I

    .prologue
    .line 546
    new-instance v0, Lcom/samsung/android/allshare/EventSync;

    invoke-direct {v0}, Lcom/samsung/android/allshare/EventSync;-><init>()V

    .line 548
    .local v0, "isc":Lcom/samsung/android/allshare/EventSync;
    iput p1, v0, Lcom/samsung/android/allshare/EventSync;->mWhat:I

    .line 549
    iput p2, v0, Lcom/samsung/android/allshare/EventSync;->mArg1:I

    .line 550
    iput p3, v0, Lcom/samsung/android/allshare/EventSync;->mArg2:I

    .line 560
    iget-object v1, p0, Lcom/samsung/android/allshare/TVMessageListener;->mEventListener:Lcom/samsung/android/allshare/IAppControlAPI$IControlEventListener;

    if-eqz v1, :cond_0

    .line 561
    iget-object v1, p0, Lcom/samsung/android/allshare/TVMessageListener;->mEventListener:Lcom/samsung/android/allshare/IAppControlAPI$IControlEventListener;

    invoke-interface {v1, v0}, Lcom/samsung/android/allshare/IAppControlAPI$IControlEventListener;->controlEvent(Lcom/samsung/android/allshare/EventSync;)V

    .line 568
    :goto_0
    return-void

    .line 563
    :cond_0
    const-string v1, "TVMessageListener"

    const-string v2, "EventListener is null !!!"

    invoke-static {v1, v2}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public deliverMsgData(IIILjava/lang/String;)V
    .locals 3
    .param p1, "what"    # I
    .param p2, "arg1"    # I
    .param p3, "arg2"    # I
    .param p4, "str"    # Ljava/lang/String;

    .prologue
    .line 571
    new-instance v0, Lcom/samsung/android/allshare/EventSync;

    invoke-direct {v0}, Lcom/samsung/android/allshare/EventSync;-><init>()V

    .line 572
    .local v0, "isc":Lcom/samsung/android/allshare/EventSync;
    iput p1, v0, Lcom/samsung/android/allshare/EventSync;->mWhat:I

    .line 573
    iput p2, v0, Lcom/samsung/android/allshare/EventSync;->mArg1:I

    .line 574
    iput p3, v0, Lcom/samsung/android/allshare/EventSync;->mArg2:I

    .line 575
    iput-object p4, v0, Lcom/samsung/android/allshare/EventSync;->mStr:Ljava/lang/String;

    .line 584
    iget-object v1, p0, Lcom/samsung/android/allshare/TVMessageListener;->mEventListener:Lcom/samsung/android/allshare/IAppControlAPI$IControlEventListener;

    if-eqz v1, :cond_0

    .line 585
    iget-object v1, p0, Lcom/samsung/android/allshare/TVMessageListener;->mEventListener:Lcom/samsung/android/allshare/IAppControlAPI$IControlEventListener;

    invoke-interface {v1, v0}, Lcom/samsung/android/allshare/IAppControlAPI$IControlEventListener;->controlEvent(Lcom/samsung/android/allshare/EventSync;)V

    .line 591
    :goto_0
    return-void

    .line 587
    :cond_0
    const-string v1, "TVMessageListener"

    const-string v2, "EventListener is null !!!"

    invoke-static {v1, v2}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public run()V
    .locals 25

    .prologue
    .line 601
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/TVMessageListener;->mSocket:Ljava/net/Socket;

    move-object/from16 v22, v0

    if-nez v22, :cond_1

    .line 741
    :cond_0
    :goto_0
    return-void

    .line 603
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/TVMessageListener;->mSocket:Ljava/net/Socket;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Ljava/net/Socket;->isConnected()Z

    move-result v22

    if-eqz v22, :cond_0

    .line 607
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/TVMessageListener;->mSocket:Ljava/net/Socket;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Ljava/net/Socket;->getInputStream()Ljava/io/InputStream;

    move-result-object v22

    invoke-static/range {v22 .. v22}, Ljava/nio/channels/Channels;->newChannel(Ljava/io/InputStream;)Ljava/nio/channels/ReadableByteChannel;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/allshare/TVMessageListener;->mInput:Ljava/nio/channels/ReadableByteChannel;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 620
    :goto_1
    :try_start_1
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    if-ne v0, v1, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/TVMessageListener;->mInput:Ljava/nio/channels/ReadableByteChannel;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/TVMessageListener;->mBuf:Ljava/nio/ByteBuffer;

    move-object/from16 v23, v0

    invoke-interface/range {v22 .. v23}, Ljava/nio/channels/ReadableByteChannel;->read(Ljava/nio/ByteBuffer;)I

    move-result v11

    .local v11, "nRecv":I
    const/16 v22, -0x1

    move/from16 v0, v22

    if-eq v11, v0, :cond_0

    .line 627
    const-string v22, "TVMessageListener"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "TVMessageListener data received "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Lcom/samsung/android/allshare/DLog;->i_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 635
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/TVMessageListener;->mBuf:Ljava/nio/ByteBuffer;

    move-object/from16 v22, v0

    sget-object v23, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual/range {v22 .. v23}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 636
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/TVMessageListener;->mBuf:Ljava/nio/ByteBuffer;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    .line 638
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/TVMessageListener;->mBuf:Ljava/nio/ByteBuffer;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Ljava/nio/ByteBuffer;->get()B

    move-result v21

    .line 639
    .local v21, "tvStatus":B
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/TVMessageListener;->mBuf:Ljava/nio/ByteBuffer;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v20

    .line 641
    .local v20, "targetLen":S
    const/16 v22, 0x200

    move/from16 v0, v22

    new-array v15, v0, [B

    .line 642
    .local v15, "srebuf":[B
    const/16 v22, 0x200

    move/from16 v0, v20

    move/from16 v1, v22

    if-le v0, v1, :cond_2

    .line 643
    const-string v22, "TVMessageListener"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "targetLen = "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, " is larger than MaxSize:512,discard is this pack."

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Lcom/samsung/android/allshare/DLog;->i_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 645
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/TVMessageListener;->mBuf:Ljava/nio/ByteBuffer;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_1

    .line 735
    .end local v11    # "nRecv":I
    .end local v15    # "srebuf":[B
    .end local v20    # "targetLen":S
    .end local v21    # "tvStatus":B
    :catch_0
    move-exception v7

    .line 737
    .local v7, "e":Ljava/io/IOException;
    const-string v22, "TVMessageListener"

    const-string v23, ""

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    invoke-static {v0, v1, v7}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto/16 :goto_0

    .line 609
    .end local v7    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v7

    .line 611
    .restart local v7    # "e":Ljava/io/IOException;
    const-string v22, "TVMessageListener"

    const-string v23, ""

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    invoke-static {v0, v1, v7}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto/16 :goto_0

    .line 648
    .end local v7    # "e":Ljava/io/IOException;
    .restart local v11    # "nRecv":I
    .restart local v15    # "srebuf":[B
    .restart local v20    # "targetLen":S
    .restart local v21    # "tvStatus":B
    :cond_2
    :try_start_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/TVMessageListener;->mBuf:Ljava/nio/ByteBuffer;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    move-object/from16 v0, v22

    move/from16 v1, v23

    move/from16 v2, v20

    invoke-virtual {v0, v15, v1, v2}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;

    .line 652
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/TVMessageListener;->mBuf:Ljava/nio/ByteBuffer;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v6

    .line 653
    .local v6, "dataLen":S
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/TVMessageListener;->mBuf:Ljava/nio/ByteBuffer;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v12

    .line 655
    .local v12, "protocolId":S
    const-string v22, "TVMessageListener"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "tvStatus :"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, " targetLen :"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, " dataLen :"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, " protocolId :"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Lcom/samsung/android/allshare/DLog;->i_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 658
    sparse-switch v12, :sswitch_data_0

    .line 732
    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/TVMessageListener;->mBuf:Ljava/nio/ByteBuffer;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    goto/16 :goto_1

    .line 660
    :sswitch_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/TVMessageListener;->mBuf:Ljava/nio/ByteBuffer;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v14

    .line 661
    .local v14, "response":S
    const-string v22, "TVMessageListener"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "IAPP_REMOCON : response :"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Lcom/samsung/android/allshare/DLog;->i_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 670
    const/16 v22, 0x0

    const/16 v23, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v0, v1, v14, v2}, Lcom/samsung/android/allshare/TVMessageListener;->deliverMsgData(III)V

    goto :goto_2

    .line 674
    .end local v14    # "response":S
    :sswitch_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/TVMessageListener;->mBuf:Ljava/nio/ByteBuffer;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v13

    .line 675
    .local v13, "remoteType":I
    const-string v22, "TVMessageListener"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "IAPP_REMOTE_INPUT_TYPE : remoteType :"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Lcom/samsung/android/allshare/DLog;->i_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 676
    const/16 v22, 0xa

    const/16 v23, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v0, v1, v13, v2}, Lcom/samsung/android/allshare/TVMessageListener;->deliverMsgData(III)V

    goto :goto_2

    .line 680
    .end local v13    # "remoteType":I
    :sswitch_2
    const-string v22, "TVMessageListener"

    const-string v23, "IAPP_STATUS"

    invoke-static/range {v22 .. v23}, Lcom/samsung/android/allshare/DLog;->i_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 681
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/TVMessageListener;->mBuf:Ljava/nio/ByteBuffer;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v16

    .line 682
    .local v16, "statusType":S
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/TVMessageListener;->mBuf:Ljava/nio/ByteBuffer;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v17

    .line 683
    .local v17, "statusVal":S
    const/16 v22, 0xc8

    move-object/from16 v0, p0

    move/from16 v1, v22

    move/from16 v2, v16

    move/from16 v3, v17

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/allshare/TVMessageListener;->deliverMsgData(III)V

    goto/16 :goto_2

    .line 687
    .end local v16    # "statusType":S
    .end local v17    # "statusVal":S
    :sswitch_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/TVMessageListener;->mBuf:Ljava/nio/ByteBuffer;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v18

    .line 688
    .local v18, "stringLength":S
    const-string v22, "TVMessageListener"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "IAPP_KEYBOARD_SYNC : stringLength :"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Lcom/samsung/android/allshare/DLog;->i_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 690
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/TVMessageListener;->mBuf:Ljava/nio/ByteBuffer;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    move-object/from16 v0, v22

    move/from16 v1, v23

    move/from16 v2, v18

    invoke-virtual {v0, v15, v1, v2}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;

    .line 691
    new-instance v9, Ljava/lang/String;

    const/16 v22, 0x0

    move/from16 v0, v22

    move/from16 v1, v18

    invoke-direct {v9, v15, v0, v1}, Ljava/lang/String;-><init>([BII)V

    .line 693
    .local v9, "keySync":Ljava/lang/String;
    const-string v22, "AA=="

    move-object/from16 v0, v22

    invoke-virtual {v9, v0}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v22

    if-eqz v22, :cond_3

    .line 694
    const/16 v22, 0x3

    const/16 v23, 0x0

    const-string v24, ""

    move-object/from16 v0, p0

    move/from16 v1, v22

    move/from16 v2, v18

    move/from16 v3, v23

    move-object/from16 v4, v24

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/samsung/android/allshare/TVMessageListener;->deliverMsgData(IIILjava/lang/String;)V

    goto/16 :goto_2

    .line 698
    :cond_3
    const/16 v22, 0x200

    move/from16 v0, v22

    new-array v0, v0, [B

    move-object/from16 v19, v0

    .line 699
    .local v19, "sync":[B
    const/16 v22, 0x1

    move/from16 v0, v22

    invoke-static {v9, v0}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v19

    .line 701
    new-instance v10, Ljava/lang/String;

    move-object/from16 v0, v19

    invoke-direct {v10, v0}, Ljava/lang/String;-><init>([B)V

    .line 703
    .local v10, "keySync2":Ljava/lang/String;
    const/16 v22, 0x3

    const/16 v23, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v22

    move/from16 v2, v18

    move/from16 v3, v23

    invoke-virtual {v0, v1, v2, v3, v10}, Lcom/samsung/android/allshare/TVMessageListener;->deliverMsgData(IIILjava/lang/String;)V

    goto/16 :goto_2

    .line 707
    .end local v9    # "keySync":Ljava/lang/String;
    .end local v10    # "keySync2":Ljava/lang/String;
    .end local v18    # "stringLength":S
    .end local v19    # "sync":[B
    :sswitch_4
    const-string v22, "TVMessageListener"

    const-string v23, "IAPP_EXIT"

    invoke-static/range {v22 .. v23}, Lcom/samsung/android/allshare/DLog;->i_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 708
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/TVMessageListener;->mBuf:Ljava/nio/ByteBuffer;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v8

    .line 711
    .local v8, "exit":S
    const/16 v22, 0x12c

    const/16 v23, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v0, v1, v8, v2}, Lcom/samsung/android/allshare/TVMessageListener;->deliverMsgData(III)V

    goto/16 :goto_2

    .line 715
    .end local v8    # "exit":S
    :sswitch_5
    const-string v22, "TVMessageListener"

    const-string v23, "IAPP_AUTHENTICATION"

    invoke-static/range {v22 .. v23}, Lcom/samsung/android/allshare/DLog;->i_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 716
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/TVMessageListener;->mBuf:Ljava/nio/ByteBuffer;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v5

    .line 719
    .local v5, "authresponse":S
    const/16 v22, 0x64

    const/16 v23, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v0, v1, v5, v2}, Lcom/samsung/android/allshare/TVMessageListener;->deliverMsgData(III)V

    goto/16 :goto_2

    .line 723
    .end local v5    # "authresponse":S
    :sswitch_6
    const-string v22, "TVMessageListener"

    const-string v23, "IAPP_AUTHENTICATION_TIMEOUT"

    invoke-static/range {v22 .. v23}, Lcom/samsung/android/allshare/DLog;->i_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 724
    const/16 v22, 0x65

    const/16 v23, 0x0

    const/16 v24, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v22

    move/from16 v2, v23

    move/from16 v3, v24

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/allshare/TVMessageListener;->deliverMsgData(III)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_2

    .line 658
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x3 -> :sswitch_3
        0xa -> :sswitch_1
        0x64 -> :sswitch_5
        0x65 -> :sswitch_6
        0xc8 -> :sswitch_2
        0x12c -> :sswitch_4
    .end sparse-switch
.end method

.method public setOnEventListener(Lcom/samsung/android/allshare/IAppControlAPI$IControlEventListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/android/allshare/IAppControlAPI$IControlEventListener;

    .prologue
    .line 513
    iput-object p1, p0, Lcom/samsung/android/allshare/TVMessageListener;->mEventListener:Lcom/samsung/android/allshare/IAppControlAPI$IControlEventListener;

    .line 514
    return-void
.end method

.method public stopThread()V
    .locals 0

    .prologue
    .line 595
    return-void
.end method
