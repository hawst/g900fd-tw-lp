.class Lcom/samsung/android/allshare/ViewController2Impl$3;
.super Ljava/lang/Object;
.source "ViewController2Impl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/allshare/ViewController2Impl;->connect(Ljava/lang/String;I)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/allshare/ViewController2Impl;

.field final synthetic val$port:I

.field final synthetic val$serverIp:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/samsung/android/allshare/ViewController2Impl;Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 324
    iput-object p1, p0, Lcom/samsung/android/allshare/ViewController2Impl$3;->this$0:Lcom/samsung/android/allshare/ViewController2Impl;

    iput-object p2, p0, Lcom/samsung/android/allshare/ViewController2Impl$3;->val$serverIp:Ljava/lang/String;

    iput p3, p0, Lcom/samsung/android/allshare/ViewController2Impl$3;->val$port:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 328
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/allshare/ViewController2Impl$3;->this$0:Lcom/samsung/android/allshare/ViewController2Impl;

    new-instance v2, Ljava/net/Socket;

    iget-object v3, p0, Lcom/samsung/android/allshare/ViewController2Impl$3;->val$serverIp:Ljava/lang/String;

    iget v4, p0, Lcom/samsung/android/allshare/ViewController2Impl$3;->val$port:I

    invoke-direct {v2, v3, v4}, Ljava/net/Socket;-><init>(Ljava/lang/String;I)V

    # setter for: Lcom/samsung/android/allshare/ViewController2Impl;->mSocket:Ljava/net/Socket;
    invoke-static {v1, v2}, Lcom/samsung/android/allshare/ViewController2Impl;->access$302(Lcom/samsung/android/allshare/ViewController2Impl;Ljava/net/Socket;)Ljava/net/Socket;
    :try_end_0
    .catch Ljava/net/UnknownHostException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 337
    :goto_0
    return-void

    .line 329
    :catch_0
    move-exception v0

    .line 330
    .local v0, "e":Ljava/net/UnknownHostException;
    iget-object v1, p0, Lcom/samsung/android/allshare/ViewController2Impl$3;->this$0:Lcom/samsung/android/allshare/ViewController2Impl;

    # setter for: Lcom/samsung/android/allshare/ViewController2Impl;->mConnectResult:Z
    invoke-static {v1, v5}, Lcom/samsung/android/allshare/ViewController2Impl;->access$402(Lcom/samsung/android/allshare/ViewController2Impl;Z)Z

    .line 331
    invoke-virtual {v0}, Ljava/net/UnknownHostException;->printStackTrace()V

    goto :goto_0

    .line 332
    .end local v0    # "e":Ljava/net/UnknownHostException;
    :catch_1
    move-exception v0

    .line 333
    .local v0, "e":Ljava/io/IOException;
    iget-object v1, p0, Lcom/samsung/android/allshare/ViewController2Impl$3;->this$0:Lcom/samsung/android/allshare/ViewController2Impl;

    # setter for: Lcom/samsung/android/allshare/ViewController2Impl;->mConnectResult:Z
    invoke-static {v1, v5}, Lcom/samsung/android/allshare/ViewController2Impl;->access$402(Lcom/samsung/android/allshare/ViewController2Impl;Z)Z

    .line 334
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method
