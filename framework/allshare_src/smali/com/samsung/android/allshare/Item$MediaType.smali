.class public final enum Lcom/samsung/android/allshare/Item$MediaType;
.super Ljava/lang/Enum;
.source "Item.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/allshare/Item;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "MediaType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/android/allshare/Item$MediaType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/android/allshare/Item$MediaType;

.field public static final enum ITEM_AUDIO:Lcom/samsung/android/allshare/Item$MediaType;

.field public static final enum ITEM_FOLDER:Lcom/samsung/android/allshare/Item$MediaType;

.field public static final enum ITEM_IMAGE:Lcom/samsung/android/allshare/Item$MediaType;

.field public static final enum ITEM_UNKNOWN:Lcom/samsung/android/allshare/Item$MediaType;

.field public static final enum ITEM_VIDEO:Lcom/samsung/android/allshare/Item$MediaType;


# instance fields
.field private final enumString:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 70
    new-instance v0, Lcom/samsung/android/allshare/Item$MediaType;

    const-string v1, "ITEM_FOLDER"

    const-string v2, "ITEM_FOLDER"

    invoke-direct {v0, v1, v3, v2}, Lcom/samsung/android/allshare/Item$MediaType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/android/allshare/Item$MediaType;->ITEM_FOLDER:Lcom/samsung/android/allshare/Item$MediaType;

    .line 72
    new-instance v0, Lcom/samsung/android/allshare/Item$MediaType;

    const-string v1, "ITEM_AUDIO"

    const-string v2, "ITEM_AUDIO"

    invoke-direct {v0, v1, v4, v2}, Lcom/samsung/android/allshare/Item$MediaType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/android/allshare/Item$MediaType;->ITEM_AUDIO:Lcom/samsung/android/allshare/Item$MediaType;

    .line 74
    new-instance v0, Lcom/samsung/android/allshare/Item$MediaType;

    const-string v1, "ITEM_IMAGE"

    const-string v2, "ITEM_IMAGE"

    invoke-direct {v0, v1, v5, v2}, Lcom/samsung/android/allshare/Item$MediaType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/android/allshare/Item$MediaType;->ITEM_IMAGE:Lcom/samsung/android/allshare/Item$MediaType;

    .line 76
    new-instance v0, Lcom/samsung/android/allshare/Item$MediaType;

    const-string v1, "ITEM_VIDEO"

    const-string v2, "ITEM_VIDEO"

    invoke-direct {v0, v1, v6, v2}, Lcom/samsung/android/allshare/Item$MediaType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/android/allshare/Item$MediaType;->ITEM_VIDEO:Lcom/samsung/android/allshare/Item$MediaType;

    .line 78
    new-instance v0, Lcom/samsung/android/allshare/Item$MediaType;

    const-string v1, "ITEM_UNKNOWN"

    const-string v2, "ITEM_UNKNOWN"

    invoke-direct {v0, v1, v7, v2}, Lcom/samsung/android/allshare/Item$MediaType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/android/allshare/Item$MediaType;->ITEM_UNKNOWN:Lcom/samsung/android/allshare/Item$MediaType;

    .line 68
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/samsung/android/allshare/Item$MediaType;

    sget-object v1, Lcom/samsung/android/allshare/Item$MediaType;->ITEM_FOLDER:Lcom/samsung/android/allshare/Item$MediaType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/android/allshare/Item$MediaType;->ITEM_AUDIO:Lcom/samsung/android/allshare/Item$MediaType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/android/allshare/Item$MediaType;->ITEM_IMAGE:Lcom/samsung/android/allshare/Item$MediaType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/android/allshare/Item$MediaType;->ITEM_VIDEO:Lcom/samsung/android/allshare/Item$MediaType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/samsung/android/allshare/Item$MediaType;->ITEM_UNKNOWN:Lcom/samsung/android/allshare/Item$MediaType;

    aput-object v1, v0, v7

    sput-object v0, Lcom/samsung/android/allshare/Item$MediaType;->$VALUES:[Lcom/samsung/android/allshare/Item$MediaType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "enumStr"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 84
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 85
    iput-object p3, p0, Lcom/samsung/android/allshare/Item$MediaType;->enumString:Ljava/lang/String;

    .line 86
    return-void
.end method

.method public static stringToEnum(Ljava/lang/String;)Lcom/samsung/android/allshare/Item$MediaType;
    .locals 1
    .param p0, "enumStr"    # Ljava/lang/String;

    .prologue
    .line 97
    if-nez p0, :cond_0

    .line 98
    sget-object v0, Lcom/samsung/android/allshare/Item$MediaType;->ITEM_UNKNOWN:Lcom/samsung/android/allshare/Item$MediaType;

    .line 111
    :goto_0
    return-object v0

    .line 100
    :cond_0
    const-string v0, "ITEM_AUDIO"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 101
    sget-object v0, Lcom/samsung/android/allshare/Item$MediaType;->ITEM_AUDIO:Lcom/samsung/android/allshare/Item$MediaType;

    goto :goto_0

    .line 102
    :cond_1
    const-string v0, "ITEM_FOLDER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 103
    sget-object v0, Lcom/samsung/android/allshare/Item$MediaType;->ITEM_FOLDER:Lcom/samsung/android/allshare/Item$MediaType;

    goto :goto_0

    .line 104
    :cond_2
    const-string v0, "ITEM_IMAGE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 105
    sget-object v0, Lcom/samsung/android/allshare/Item$MediaType;->ITEM_IMAGE:Lcom/samsung/android/allshare/Item$MediaType;

    goto :goto_0

    .line 106
    :cond_3
    const-string v0, "ITEM_UNKNOWN"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 107
    sget-object v0, Lcom/samsung/android/allshare/Item$MediaType;->ITEM_UNKNOWN:Lcom/samsung/android/allshare/Item$MediaType;

    goto :goto_0

    .line 108
    :cond_4
    const-string v0, "ITEM_VIDEO"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 109
    sget-object v0, Lcom/samsung/android/allshare/Item$MediaType;->ITEM_VIDEO:Lcom/samsung/android/allshare/Item$MediaType;

    goto :goto_0

    .line 111
    :cond_5
    sget-object v0, Lcom/samsung/android/allshare/Item$MediaType;->ITEM_UNKNOWN:Lcom/samsung/android/allshare/Item$MediaType;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/android/allshare/Item$MediaType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 68
    const-class v0, Lcom/samsung/android/allshare/Item$MediaType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/allshare/Item$MediaType;

    return-object v0
.end method

.method public static values()[Lcom/samsung/android/allshare/Item$MediaType;
    .locals 1

    .prologue
    .line 68
    sget-object v0, Lcom/samsung/android/allshare/Item$MediaType;->$VALUES:[Lcom/samsung/android/allshare/Item$MediaType;

    invoke-virtual {v0}, [Lcom/samsung/android/allshare/Item$MediaType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/allshare/Item$MediaType;

    return-object v0
.end method


# virtual methods
.method public enumToString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/samsung/android/allshare/Item$MediaType;->enumString:Ljava/lang/String;

    return-object v0
.end method
