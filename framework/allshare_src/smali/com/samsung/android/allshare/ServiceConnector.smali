.class public Lcom/samsung/android/allshare/ServiceConnector;
.super Ljava/lang/Object;
.source "ServiceConnector.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/allshare/ServiceConnector$3;,
        Lcom/samsung/android/allshare/ServiceConnector$AllShareConnector;,
        Lcom/samsung/android/allshare/ServiceConnector$ProfileConstData;,
        Lcom/samsung/android/allshare/ServiceConnector$FileServiceProviderImpl;,
        Lcom/samsung/android/allshare/ServiceConnector$MediaServiceProviderImpl;,
        Lcom/samsung/android/allshare/ServiceConnector$ServiceProviderImpl;,
        Lcom/samsung/android/allshare/ServiceConnector$IServiceConnectorGetter;,
        Lcom/samsung/android/allshare/ServiceConnector$IServiceStateSetter;,
        Lcom/samsung/android/allshare/ServiceConnector$IServiceConnectEventListener;,
        Lcom/samsung/android/allshare/ServiceConnector$ServiceState;
    }
.end annotation


# static fields
.field private static final TAG_CONNECTOR:Ljava/lang/String; = "ServiceConnector"

.field private static mContextRef:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 166
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/allshare/ServiceConnector;->mContextRef:Ljava/lang/ref/WeakReference;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    .prologue
    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 77
    return-void
.end method

.method public static createServiceProvider(Landroid/content/Context;Lcom/samsung/android/allshare/ServiceConnector$IServiceConnectEventListener;)Lcom/samsung/android/allshare/ERROR;
    .locals 5
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "l"    # Lcom/samsung/android/allshare/ServiceConnector$IServiceConnectEventListener;

    .prologue
    .line 231
    const-string v2, "ServiceConnector"

    const-string v3, "createServiceProvider(v1)"

    invoke-static {v2, v3}, Lcom/samsung/android/allshare/DLog;->v_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 232
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 233
    :cond_0
    const-string v2, "ServiceConnector"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Context or ServiceConnectEventListener is null : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " || "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 235
    sget-object v2, Lcom/samsung/android/allshare/ERROR;->INVALID_ARGUMENT:Lcom/samsung/android/allshare/ERROR;

    .line 290
    :goto_0
    return-object v2

    .line 239
    :cond_1
    invoke-static {}, Lcom/samsung/android/allshare/DLog;->setAPIVersionTag()V

    .line 241
    new-instance v2, Ljava/lang/ref/WeakReference;

    invoke-direct {v2, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    sput-object v2, Lcom/samsung/android/allshare/ServiceConnector;->mContextRef:Ljava/lang/ref/WeakReference;

    .line 243
    new-instance v1, Lcom/samsung/android/allshare/ServiceConnector$AllShareConnector;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/samsung/android/allshare/ServiceConnector$AllShareConnector;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 245
    .local v1, "connector":Lcom/samsung/android/allshare/ServiceConnector$AllShareConnector;
    new-instance v0, Lcom/samsung/android/allshare/ServiceConnector$1;

    invoke-direct {v0, p1, p0, v1}, Lcom/samsung/android/allshare/ServiceConnector$1;-><init>(Lcom/samsung/android/allshare/ServiceConnector$IServiceConnectEventListener;Landroid/content/Context;Lcom/samsung/android/allshare/ServiceConnector$AllShareConnector;)V

    .line 287
    .local v0, "callback":Landroid/os/Handler$Callback;
    invoke-virtual {v1, v0}, Lcom/samsung/android/allshare/ServiceConnector$AllShareConnector;->setCallback(Landroid/os/Handler$Callback;)V

    .line 288
    invoke-virtual {v1}, Lcom/samsung/android/allshare/ServiceConnector$AllShareConnector;->connect()V

    .line 290
    sget-object v2, Lcom/samsung/android/allshare/ERROR;->SUCCESS:Lcom/samsung/android/allshare/ERROR;

    goto :goto_0
.end method

.method public static createServiceProvider(Landroid/content/Context;Lcom/samsung/android/allshare/ServiceConnector$IServiceConnectEventListener;Ljava/lang/String;)Lcom/samsung/android/allshare/ERROR;
    .locals 6
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "l"    # Lcom/samsung/android/allshare/ServiceConnector$IServiceConnectEventListener;
    .param p2, "serviceType"    # Ljava/lang/String;

    .prologue
    .line 341
    const-string v3, "ServiceConnector"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "createServiceProvider of "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/allshare/DLog;->v_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 342
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 343
    :cond_0
    const-string v3, "ServiceConnector"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Context or ServiceConnectEventListener is null : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " || "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 345
    sget-object v3, Lcom/samsung/android/allshare/ERROR;->INVALID_ARGUMENT:Lcom/samsung/android/allshare/ERROR;

    .line 440
    :goto_0
    return-object v3

    .line 348
    :cond_1
    invoke-static {}, Lcom/samsung/android/feature/FloatingFeature;->getInstance()Lcom/samsung/android/feature/FloatingFeature;

    move-result-object v2

    .line 349
    .local v2, "floatingFeature":Lcom/samsung/android/feature/FloatingFeature;
    if-eqz v2, :cond_2

    .line 350
    const-string v3, "ServiceConnector"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "ALLSHARE_CONFIG : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "SEC_FLOATING_FEATURE_ALLSHARE_CONFIG_VERSION"

    invoke-virtual {v2, v5}, Lcom/samsung/android/feature/FloatingFeature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/allshare/DLog;->i_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 354
    :cond_2
    invoke-static {}, Lcom/samsung/android/allshare/DLog;->setAPIVersionTag()V

    .line 371
    new-instance v3, Ljava/lang/ref/WeakReference;

    invoke-direct {v3, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    sput-object v3, Lcom/samsung/android/allshare/ServiceConnector;->mContextRef:Ljava/lang/ref/WeakReference;

    .line 373
    new-instance v1, Lcom/samsung/android/allshare/ServiceConnector$AllShareConnector;

    const/4 v3, 0x0

    invoke-direct {v1, p0, v3}, Lcom/samsung/android/allshare/ServiceConnector$AllShareConnector;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 375
    .local v1, "connector":Lcom/samsung/android/allshare/ServiceConnector$AllShareConnector;
    new-instance v0, Lcom/samsung/android/allshare/ServiceConnector$2;

    invoke-direct {v0, p1, p0, v1, p2}, Lcom/samsung/android/allshare/ServiceConnector$2;-><init>(Lcom/samsung/android/allshare/ServiceConnector$IServiceConnectEventListener;Landroid/content/Context;Lcom/samsung/android/allshare/ServiceConnector$AllShareConnector;Ljava/lang/String;)V

    .line 437
    .local v0, "callback":Landroid/os/Handler$Callback;
    invoke-virtual {v1, v0}, Lcom/samsung/android/allshare/ServiceConnector$AllShareConnector;->setCallback(Landroid/os/Handler$Callback;)V

    .line 438
    invoke-virtual {v1}, Lcom/samsung/android/allshare/ServiceConnector$AllShareConnector;->connect()V

    .line 440
    sget-object v3, Lcom/samsung/android/allshare/ERROR;->SUCCESS:Lcom/samsung/android/allshare/ERROR;

    goto :goto_0
.end method

.method public static deleteServiceProvider(Lcom/samsung/android/allshare/ServiceProvider;)V
    .locals 7
    .param p0, "provider"    # Lcom/samsung/android/allshare/ServiceProvider;

    .prologue
    .line 454
    if-nez p0, :cond_0

    .line 455
    const-string v4, "ServiceConnector"

    const-string v5, "deleteServiceProvider : ServiceProvider is null"

    invoke-static {v4, v5}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 473
    .end local p0    # "provider":Lcom/samsung/android/allshare/ServiceProvider;
    :goto_0
    return-void

    .line 459
    .restart local p0    # "provider":Lcom/samsung/android/allshare/ServiceProvider;
    :cond_0
    check-cast p0, Lcom/samsung/android/allshare/ServiceConnector$IServiceConnectorGetter;

    .end local p0    # "provider":Lcom/samsung/android/allshare/ServiceProvider;
    invoke-interface {p0}, Lcom/samsung/android/allshare/ServiceConnector$IServiceConnectorGetter;->getAllShareConnector()Lcom/samsung/android/allshare/ServiceConnector$AllShareConnector;

    move-result-object v0

    .line 462
    .local v0, "connector":Lcom/samsung/android/allshare/ServiceConnector$AllShareConnector;
    # getter for: Lcom/samsung/android/allshare/ServiceConnector$AllShareConnector;->mEventHandlerSet:Ljava/util/HashSet;
    invoke-static {v0}, Lcom/samsung/android/allshare/ServiceConnector$AllShareConnector;->access$000(Lcom/samsung/android/allshare/ServiceConnector$AllShareConnector;)Ljava/util/HashSet;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/HashSet;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/HashSet;

    .line 464
    .local v1, "copySet":Ljava/util/HashSet;, "Ljava/util/HashSet<Lcom/samsung/android/allshare/ServiceConnector$AllShareConnector$EventHandler;>;"
    invoke-virtual {v1}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 465
    .local v2, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/allshare/ServiceConnector$AllShareConnector$EventHandler;>;"
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 466
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/allshare/ServiceConnector$AllShareConnector$EventHandler;

    .line 467
    .local v3, "value":Lcom/samsung/android/allshare/ServiceConnector$AllShareConnector$EventHandler;
    iget-object v4, v3, Lcom/samsung/android/allshare/ServiceConnector$AllShareConnector$EventHandler;->mEventId:Ljava/lang/String;

    iget-object v5, v3, Lcom/samsung/android/allshare/ServiceConnector$AllShareConnector$EventHandler;->mBundle:Landroid/os/Bundle;

    iget-object v6, v3, Lcom/samsung/android/allshare/ServiceConnector$AllShareConnector$EventHandler;->mHanlder:Lcom/samsung/android/allshare/AllShareEventHandler;

    invoke-virtual {v0, v4, v5, v6}, Lcom/samsung/android/allshare/ServiceConnector$AllShareConnector;->unsubscribeAllShareEvent(Ljava/lang/String;Landroid/os/Bundle;Lcom/samsung/android/allshare/AllShareEventHandler;)V

    .line 468
    # getter for: Lcom/samsung/android/allshare/ServiceConnector$AllShareConnector;->mEventHandlerSet:Ljava/util/HashSet;
    invoke-static {v0}, Lcom/samsung/android/allshare/ServiceConnector$AllShareConnector;->access$000(Lcom/samsung/android/allshare/ServiceConnector$AllShareConnector;)Ljava/util/HashSet;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    goto :goto_1

    .line 472
    .end local v3    # "value":Lcom/samsung/android/allshare/ServiceConnector$AllShareConnector$EventHandler;
    :cond_1
    invoke-virtual {v0}, Lcom/samsung/android/allshare/ServiceConnector$AllShareConnector;->destroyInstance()V

    goto :goto_0
.end method

.method static getContext()Landroid/content/Context;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 174
    sget-object v2, Lcom/samsung/android/allshare/ServiceConnector;->mContextRef:Ljava/lang/ref/WeakReference;

    if-nez v2, :cond_1

    move-object v0, v1

    .line 181
    :cond_0
    :goto_0
    return-object v0

    .line 177
    :cond_1
    sget-object v2, Lcom/samsung/android/allshare/ServiceConnector;->mContextRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 178
    .local v0, "ctx":Landroid/content/Context;
    if-nez v0, :cond_0

    move-object v0, v1

    .line 179
    goto :goto_0
.end method

.method static getMainLooper()Landroid/os/Looper;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 476
    sget-object v2, Lcom/samsung/android/allshare/ServiceConnector;->mContextRef:Ljava/lang/ref/WeakReference;

    if-nez v2, :cond_1

    .line 482
    .local v0, "ctx":Landroid/content/Context;
    :cond_0
    :goto_0
    return-object v1

    .line 478
    .end local v0    # "ctx":Landroid/content/Context;
    :cond_1
    sget-object v2, Lcom/samsung/android/allshare/ServiceConnector;->mContextRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 479
    .restart local v0    # "ctx":Landroid/content/Context;
    if-eqz v0, :cond_0

    .line 482
    invoke-virtual {v0}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    goto :goto_0
.end method
