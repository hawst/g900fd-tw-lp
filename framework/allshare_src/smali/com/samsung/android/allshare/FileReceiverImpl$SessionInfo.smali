.class Lcom/samsung/android/allshare/FileReceiverImpl$SessionInfo;
.super Ljava/lang/Object;
.source "FileReceiverImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/allshare/FileReceiverImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SessionInfo"
.end annotation


# instance fields
.field private mCount:I

.field private mEventListener:Lcom/samsung/android/allshare/file/FileReceiver$IFileReceiverProgressUpdateEventListener;

.field private mResponseListener:Lcom/samsung/android/allshare/file/FileReceiver$IFileReceiverReceiveResponseListener;

.field private mState:I

.field final synthetic this$0:Lcom/samsung/android/allshare/FileReceiverImpl;


# direct methods
.method public constructor <init>(Lcom/samsung/android/allshare/FileReceiverImpl;ILcom/samsung/android/allshare/file/FileReceiver$IFileReceiverReceiveResponseListener;Lcom/samsung/android/allshare/file/FileReceiver$IFileReceiverProgressUpdateEventListener;)V
    .locals 2
    .param p2, "count"    # I
    .param p3, "responseListener"    # Lcom/samsung/android/allshare/file/FileReceiver$IFileReceiverReceiveResponseListener;
    .param p4, "eventListener"    # Lcom/samsung/android/allshare/file/FileReceiver$IFileReceiverProgressUpdateEventListener;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 64
    iput-object p1, p0, Lcom/samsung/android/allshare/FileReceiverImpl$SessionInfo;->this$0:Lcom/samsung/android/allshare/FileReceiverImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    iput v0, p0, Lcom/samsung/android/allshare/FileReceiverImpl$SessionInfo;->mCount:I

    .line 57
    iput-object v1, p0, Lcom/samsung/android/allshare/FileReceiverImpl$SessionInfo;->mResponseListener:Lcom/samsung/android/allshare/file/FileReceiver$IFileReceiverReceiveResponseListener;

    .line 59
    iput-object v1, p0, Lcom/samsung/android/allshare/FileReceiverImpl$SessionInfo;->mEventListener:Lcom/samsung/android/allshare/file/FileReceiver$IFileReceiverProgressUpdateEventListener;

    .line 61
    iput v0, p0, Lcom/samsung/android/allshare/FileReceiverImpl$SessionInfo;->mState:I

    .line 65
    iput p2, p0, Lcom/samsung/android/allshare/FileReceiverImpl$SessionInfo;->mCount:I

    .line 66
    iput-object p3, p0, Lcom/samsung/android/allshare/FileReceiverImpl$SessionInfo;->mResponseListener:Lcom/samsung/android/allshare/file/FileReceiver$IFileReceiverReceiveResponseListener;

    .line 67
    iput-object p4, p0, Lcom/samsung/android/allshare/FileReceiverImpl$SessionInfo;->mEventListener:Lcom/samsung/android/allshare/file/FileReceiver$IFileReceiverProgressUpdateEventListener;

    .line 68
    return-void
.end method


# virtual methods
.method public completed()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 83
    iget v1, p0, Lcom/samsung/android/allshare/FileReceiverImpl$SessionInfo;->mCount:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/samsung/android/allshare/FileReceiverImpl$SessionInfo;->mCount:I

    .line 84
    iget v1, p0, Lcom/samsung/android/allshare/FileReceiverImpl$SessionInfo;->mCount:I

    if-gtz v1, :cond_0

    .line 85
    iput v0, p0, Lcom/samsung/android/allshare/FileReceiverImpl$SessionInfo;->mCount:I

    .line 86
    const/4 v0, 0x1

    .line 89
    :cond_0
    return v0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 79
    iget v0, p0, Lcom/samsung/android/allshare/FileReceiverImpl$SessionInfo;->mCount:I

    return v0
.end method

.method public getEventListener()Lcom/samsung/android/allshare/file/FileReceiver$IFileReceiverProgressUpdateEventListener;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/samsung/android/allshare/FileReceiverImpl$SessionInfo;->mEventListener:Lcom/samsung/android/allshare/file/FileReceiver$IFileReceiverProgressUpdateEventListener;

    return-object v0
.end method

.method public getResponseListener()Lcom/samsung/android/allshare/file/FileReceiver$IFileReceiverReceiveResponseListener;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/samsung/android/allshare/FileReceiverImpl$SessionInfo;->mResponseListener:Lcom/samsung/android/allshare/file/FileReceiver$IFileReceiverReceiveResponseListener;

    return-object v0
.end method

.method public removed()Z
    .locals 2

    .prologue
    const/4 v1, -0x2

    .line 93
    iget v0, p0, Lcom/samsung/android/allshare/FileReceiverImpl$SessionInfo;->mState:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/samsung/android/allshare/FileReceiverImpl$SessionInfo;->mState:I

    .line 94
    iget v0, p0, Lcom/samsung/android/allshare/FileReceiverImpl$SessionInfo;->mState:I

    if-gt v0, v1, :cond_0

    .line 95
    iput v1, p0, Lcom/samsung/android/allshare/FileReceiverImpl$SessionInfo;->mState:I

    .line 96
    const/4 v0, 0x1

    .line 99
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
