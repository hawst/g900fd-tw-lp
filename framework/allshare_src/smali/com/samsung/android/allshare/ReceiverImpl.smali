.class Lcom/samsung/android/allshare/ReceiverImpl;
.super Lcom/samsung/android/allshare/media/Receiver;
.source "ReceiverImpl.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/allshare/ReceiverImpl$3;
    }
.end annotation


# static fields
.field private static final TAG_CLASS:Ljava/lang/String; = "ReceiverImpl"


# instance fields
.field private mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

.field private mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

.field mEventHandler:Lcom/samsung/android/allshare/AllShareEventHandler;

.field private mIsSubscribed:Z

.field private mProgressUpdateListener:Lcom/samsung/android/allshare/media/Receiver$IProgressUpdateEventListener;

.field private mReceiverResponseListener:Lcom/samsung/android/allshare/media/Receiver$IReceiverResponseListener;

.field mResponseHandler:Lcom/samsung/android/allshare/AllShareResponseHandler;


# direct methods
.method constructor <init>(Lcom/samsung/android/allshare/IAllShareConnector;Lcom/samsung/android/allshare/DeviceImpl;)V
    .locals 2
    .param p1, "connector"    # Lcom/samsung/android/allshare/IAllShareConnector;
    .param p2, "deviceImpl"    # Lcom/samsung/android/allshare/DeviceImpl;

    .prologue
    const/4 v0, 0x0

    .line 50
    invoke-direct {p0}, Lcom/samsung/android/allshare/media/Receiver;-><init>()V

    .line 40
    iput-object v0, p0, Lcom/samsung/android/allshare/ReceiverImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    .line 42
    iput-object v0, p0, Lcom/samsung/android/allshare/ReceiverImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    .line 44
    iput-object v0, p0, Lcom/samsung/android/allshare/ReceiverImpl;->mProgressUpdateListener:Lcom/samsung/android/allshare/media/Receiver$IProgressUpdateEventListener;

    .line 46
    iput-object v0, p0, Lcom/samsung/android/allshare/ReceiverImpl;->mReceiverResponseListener:Lcom/samsung/android/allshare/media/Receiver$IReceiverResponseListener;

    .line 48
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/allshare/ReceiverImpl;->mIsSubscribed:Z

    .line 61
    new-instance v0, Lcom/samsung/android/allshare/ReceiverImpl$1;

    invoke-static {}, Lcom/samsung/android/allshare/ServiceConnector;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/samsung/android/allshare/ReceiverImpl$1;-><init>(Lcom/samsung/android/allshare/ReceiverImpl;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/samsung/android/allshare/ReceiverImpl;->mEventHandler:Lcom/samsung/android/allshare/AllShareEventHandler;

    .line 112
    new-instance v0, Lcom/samsung/android/allshare/ReceiverImpl$2;

    invoke-static {}, Lcom/samsung/android/allshare/ServiceConnector;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/samsung/android/allshare/ReceiverImpl$2;-><init>(Lcom/samsung/android/allshare/ReceiverImpl;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/samsung/android/allshare/ReceiverImpl;->mResponseHandler:Lcom/samsung/android/allshare/AllShareResponseHandler;

    .line 51
    if-nez p1, :cond_0

    .line 53
    const-string v0, "ReceiverImpl"

    const-string v1, "Connection FAIL: AllShare Service Connector does not exist"

    invoke-static {v0, v1}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    :goto_0
    return-void

    .line 57
    :cond_0
    iput-object p2, p0, Lcom/samsung/android/allshare/ReceiverImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    .line 58
    iput-object p1, p0, Lcom/samsung/android/allshare/ReceiverImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/samsung/android/allshare/ReceiverImpl;)Lcom/samsung/android/allshare/media/Receiver$IProgressUpdateEventListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/ReceiverImpl;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/samsung/android/allshare/ReceiverImpl;->mProgressUpdateListener:Lcom/samsung/android/allshare/media/Receiver$IProgressUpdateEventListener;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/android/allshare/ReceiverImpl;)Lcom/samsung/android/allshare/media/Receiver$IReceiverResponseListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/ReceiverImpl;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/samsung/android/allshare/ReceiverImpl;->mReceiverResponseListener:Lcom/samsung/android/allshare/media/Receiver$IReceiverResponseListener;

    return-object v0
.end method

.method private itemHandling(Lcom/samsung/android/allshare/Item;Ljava/lang/String;)V
    .locals 4
    .param p1, "item"    # Lcom/samsung/android/allshare/Item;
    .param p2, "actionID"    # Ljava/lang/String;

    .prologue
    .line 229
    iget-object v2, p0, Lcom/samsung/android/allshare/ReceiverImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/android/allshare/ReceiverImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    invoke-interface {v2}, Lcom/samsung/android/allshare/IAllShareConnector;->isAllShareServiceConnected()Z

    move-result v2

    if-nez v2, :cond_2

    .line 230
    :cond_0
    const-string v2, "com.sec.android.allshare.action.ACTION_RECEIVER_RECEIVE_BY_ITEM"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 231
    iget-object v2, p0, Lcom/samsung/android/allshare/ReceiverImpl;->mReceiverResponseListener:Lcom/samsung/android/allshare/media/Receiver$IReceiverResponseListener;

    sget-object v3, Lcom/samsung/android/allshare/ERROR;->SERVICE_NOT_CONNECTED:Lcom/samsung/android/allshare/ERROR;

    invoke-interface {v2, p1, v3}, Lcom/samsung/android/allshare/media/Receiver$IReceiverResponseListener;->onReceiveResponseReceived(Lcom/samsung/android/allshare/Item;Lcom/samsung/android/allshare/ERROR;)V

    .line 268
    .end local p1    # "item":Lcom/samsung/android/allshare/Item;
    :goto_0
    return-void

    .line 234
    .restart local p1    # "item":Lcom/samsung/android/allshare/Item;
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/allshare/ReceiverImpl;->mReceiverResponseListener:Lcom/samsung/android/allshare/media/Receiver$IReceiverResponseListener;

    sget-object v3, Lcom/samsung/android/allshare/ERROR;->SERVICE_NOT_CONNECTED:Lcom/samsung/android/allshare/ERROR;

    invoke-interface {v2, p1, v3}, Lcom/samsung/android/allshare/media/Receiver$IReceiverResponseListener;->onCancelResponseReceived(Lcom/samsung/android/allshare/Item;Lcom/samsung/android/allshare/ERROR;)V

    goto :goto_0

    .line 239
    :cond_2
    new-instance v1, Lcom/sec/android/allshare/iface/CVMessage;

    invoke-direct {v1}, Lcom/sec/android/allshare/iface/CVMessage;-><init>()V

    .line 240
    .local v1, "cvm":Lcom/sec/android/allshare/iface/CVMessage;
    invoke-virtual {v1, p2}, Lcom/sec/android/allshare/iface/CVMessage;->setActionID(Ljava/lang/String;)V

    .line 242
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 243
    .local v0, "bundle":Landroid/os/Bundle;
    instance-of v2, p1, Lcom/samsung/android/allshare/AudioItemImpl;

    if-eqz v2, :cond_3

    .line 244
    const-string v2, "BUNDLE_PARCELABLE_ITEM"

    check-cast p1, Lcom/samsung/android/allshare/AudioItemImpl;

    .end local p1    # "item":Lcom/samsung/android/allshare/Item;
    invoke-virtual {p1}, Lcom/samsung/android/allshare/AudioItemImpl;->getBundle()Landroid/os/Bundle;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 264
    :goto_1
    const-string v2, "BUNDLE_STRING_ID"

    invoke-virtual {p0}, Lcom/samsung/android/allshare/ReceiverImpl;->getID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 266
    invoke-virtual {v1, v0}, Lcom/sec/android/allshare/iface/CVMessage;->setBundle(Landroid/os/Bundle;)V

    .line 267
    iget-object v2, p0, Lcom/samsung/android/allshare/ReceiverImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    iget-object v3, p0, Lcom/samsung/android/allshare/ReceiverImpl;->mResponseHandler:Lcom/samsung/android/allshare/AllShareResponseHandler;

    invoke-interface {v2, v1, v3}, Lcom/samsung/android/allshare/IAllShareConnector;->requestCVMAsync(Lcom/sec/android/allshare/iface/CVMessage;Lcom/samsung/android/allshare/AllShareResponseHandler;)J

    goto :goto_0

    .line 246
    .restart local p1    # "item":Lcom/samsung/android/allshare/Item;
    :cond_3
    instance-of v2, p1, Lcom/samsung/android/allshare/VideoItemImpl;

    if-eqz v2, :cond_4

    .line 247
    const-string v2, "BUNDLE_PARCELABLE_ITEM"

    check-cast p1, Lcom/samsung/android/allshare/VideoItemImpl;

    .end local p1    # "item":Lcom/samsung/android/allshare/Item;
    invoke-virtual {p1}, Lcom/samsung/android/allshare/VideoItemImpl;->getBundle()Landroid/os/Bundle;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    goto :goto_1

    .line 249
    .restart local p1    # "item":Lcom/samsung/android/allshare/Item;
    :cond_4
    instance-of v2, p1, Lcom/samsung/android/allshare/ImageItemImpl;

    if-eqz v2, :cond_5

    .line 250
    const-string v2, "BUNDLE_PARCELABLE_ITEM"

    check-cast p1, Lcom/samsung/android/allshare/ImageItemImpl;

    .end local p1    # "item":Lcom/samsung/android/allshare/Item;
    invoke-virtual {p1}, Lcom/samsung/android/allshare/ImageItemImpl;->getBundle()Landroid/os/Bundle;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    goto :goto_1

    .line 252
    .restart local p1    # "item":Lcom/samsung/android/allshare/Item;
    :cond_5
    invoke-virtual {p1}, Lcom/samsung/android/allshare/Item;->getContentBuildType()Lcom/samsung/android/allshare/Item$ContentBuildType;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/allshare/Item$ContentBuildType;->LOCAL:Lcom/samsung/android/allshare/Item$ContentBuildType;

    invoke-virtual {v2, v3}, Lcom/samsung/android/allshare/Item$ContentBuildType;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 253
    const-string v2, "BUNDLE_PARCELABLE_ITEM_URI"

    invoke-virtual {p1}, Lcom/samsung/android/allshare/Item;->getURI()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 254
    const-string v2, "BUNDLE_STRING_ITEM_MIMETYPE"

    invoke-virtual {p1}, Lcom/samsung/android/allshare/Item;->getMimetype()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 255
    const-string v2, "BUNDLE_PARCELABLE_ITEM"

    check-cast p1, Lcom/sec/android/allshare/iface/IBundleHolder;

    .end local p1    # "item":Lcom/samsung/android/allshare/Item;
    invoke-interface {p1}, Lcom/sec/android/allshare/iface/IBundleHolder;->getBundle()Landroid/os/Bundle;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    goto :goto_1

    .line 258
    .restart local p1    # "item":Lcom/samsung/android/allshare/Item;
    :cond_6
    const-string v2, "com.sec.android.allshare.action.ACTION_RECEIVER_RECEIVE_BY_ITEM"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 259
    iget-object v2, p0, Lcom/samsung/android/allshare/ReceiverImpl;->mReceiverResponseListener:Lcom/samsung/android/allshare/media/Receiver$IReceiverResponseListener;

    sget-object v3, Lcom/samsung/android/allshare/ERROR;->INVALID_ARGUMENT:Lcom/samsung/android/allshare/ERROR;

    invoke-interface {v2, p1, v3}, Lcom/samsung/android/allshare/media/Receiver$IReceiverResponseListener;->onReceiveResponseReceived(Lcom/samsung/android/allshare/Item;Lcom/samsung/android/allshare/ERROR;)V

    goto :goto_1

    .line 261
    :cond_7
    iget-object v2, p0, Lcom/samsung/android/allshare/ReceiverImpl;->mReceiverResponseListener:Lcom/samsung/android/allshare/media/Receiver$IReceiverResponseListener;

    sget-object v3, Lcom/samsung/android/allshare/ERROR;->INVALID_ARGUMENT:Lcom/samsung/android/allshare/ERROR;

    invoke-interface {v2, p1, v3}, Lcom/samsung/android/allshare/media/Receiver$IReceiverResponseListener;->onCancelResponseReceived(Lcom/samsung/android/allshare/Item;Lcom/samsung/android/allshare/ERROR;)V

    goto :goto_1
.end method


# virtual methods
.method public cancel(Lcom/samsung/android/allshare/Item;)V
    .locals 1
    .param p1, "item"    # Lcom/samsung/android/allshare/Item;

    .prologue
    .line 190
    const-string v0, "com.sec.android.allshare.action.ACTION_RECEIVER_CANCEL_BY_ITEM"

    invoke-direct {p0, p1, v0}, Lcom/samsung/android/allshare/ReceiverImpl;->itemHandling(Lcom/samsung/android/allshare/Item;Ljava/lang/String;)V

    .line 191
    return-void
.end method

.method getBundle()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 271
    iget-object v0, p0, Lcom/samsung/android/allshare/ReceiverImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    if-nez v0, :cond_0

    .line 272
    const/4 v0, 0x0

    .line 274
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/allshare/ReceiverImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/DeviceImpl;->getBundle()Landroid/os/Bundle;

    move-result-object v0

    goto :goto_0
.end method

.method public getID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 221
    iget-object v0, p0, Lcom/samsung/android/allshare/ReceiverImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    if-nez v0, :cond_0

    .line 222
    const-string v0, ""

    .line 224
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/allshare/ReceiverImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/DeviceImpl;->getID()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method getItem(Landroid/os/Bundle;)Lcom/samsung/android/allshare/Item;
    .locals 5
    .param p1, "b"    # Landroid/os/Bundle;

    .prologue
    const/4 v2, 0x0

    .line 153
    if-nez p1, :cond_1

    .line 169
    :cond_0
    :goto_0
    return-object v2

    .line 155
    :cond_1
    const-string v3, "BUNDLE_STRING_ITEM_TYPE"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 156
    .local v1, "typeStr":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 159
    invoke-static {v1}, Lcom/samsung/android/allshare/Item$MediaType;->stringToEnum(Ljava/lang/String;)Lcom/samsung/android/allshare/Item$MediaType;

    move-result-object v0

    .line 161
    .local v0, "type":Lcom/samsung/android/allshare/Item$MediaType;
    sget-object v3, Lcom/samsung/android/allshare/ReceiverImpl$3;->$SwitchMap$com$samsung$android$allshare$Item$MediaType:[I

    invoke-virtual {v0}, Lcom/samsung/android/allshare/Item$MediaType;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    goto :goto_0

    .line 163
    :pswitch_0
    new-instance v2, Lcom/samsung/android/allshare/AudioItemImpl;

    invoke-direct {v2, p1}, Lcom/samsung/android/allshare/AudioItemImpl;-><init>(Landroid/os/Bundle;)V

    goto :goto_0

    .line 165
    :pswitch_1
    new-instance v2, Lcom/samsung/android/allshare/ImageItemImpl;

    invoke-direct {v2, p1}, Lcom/samsung/android/allshare/ImageItemImpl;-><init>(Landroid/os/Bundle;)V

    goto :goto_0

    .line 167
    :pswitch_2
    new-instance v2, Lcom/samsung/android/allshare/VideoItemImpl;

    invoke-direct {v2, p1}, Lcom/samsung/android/allshare/VideoItemImpl;-><init>(Landroid/os/Bundle;)V

    goto :goto_0

    .line 161
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method getItemType(Landroid/os/Bundle;)Lcom/samsung/android/allshare/Item$MediaType;
    .locals 2
    .param p1, "b"    # Landroid/os/Bundle;

    .prologue
    .line 174
    if-nez p1, :cond_0

    .line 175
    sget-object v1, Lcom/samsung/android/allshare/Item$MediaType;->ITEM_UNKNOWN:Lcom/samsung/android/allshare/Item$MediaType;

    .line 180
    :goto_0
    return-object v1

    .line 176
    :cond_0
    const-string v1, "BUNDLE_STRING_ITEM_TYPE"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 177
    .local v0, "typeStr":Ljava/lang/String;
    if-nez v0, :cond_1

    .line 178
    sget-object v1, Lcom/samsung/android/allshare/Item$MediaType;->ITEM_UNKNOWN:Lcom/samsung/android/allshare/Item$MediaType;

    goto :goto_0

    .line 180
    :cond_1
    invoke-static {v0}, Lcom/samsung/android/allshare/Item$MediaType;->stringToEnum(Ljava/lang/String;)Lcom/samsung/android/allshare/Item$MediaType;

    move-result-object v1

    goto :goto_0
.end method

.method public receive(Lcom/samsung/android/allshare/Item;)V
    .locals 1
    .param p1, "item"    # Lcom/samsung/android/allshare/Item;

    .prologue
    .line 185
    const-string v0, "com.sec.android.allshare.action.ACTION_RECEIVER_RECEIVE_BY_ITEM"

    invoke-direct {p0, p1, v0}, Lcom/samsung/android/allshare/ReceiverImpl;->itemHandling(Lcom/samsung/android/allshare/Item;Ljava/lang/String;)V

    .line 186
    return-void
.end method

.method public removeEventHandler()V
    .locals 4

    .prologue
    .line 278
    iget-object v0, p0, Lcom/samsung/android/allshare/ReceiverImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    const-string v1, "com.sec.android.allshare.event.EVENT_DEVICE_SUBSCRIBE"

    iget-object v2, p0, Lcom/samsung/android/allshare/ReceiverImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    invoke-virtual {v2}, Lcom/samsung/android/allshare/DeviceImpl;->getBundle()Landroid/os/Bundle;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/allshare/ReceiverImpl;->mEventHandler:Lcom/samsung/android/allshare/AllShareEventHandler;

    invoke-interface {v0, v1, v2, v3}, Lcom/samsung/android/allshare/IAllShareConnector;->unsubscribeAllShareEvent(Ljava/lang/String;Landroid/os/Bundle;Lcom/samsung/android/allshare/AllShareEventHandler;)V

    .line 280
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/allshare/ReceiverImpl;->mIsSubscribed:Z

    .line 281
    return-void
.end method

.method public setProgressUpdateEventListener(Lcom/samsung/android/allshare/media/Receiver$IProgressUpdateEventListener;)V
    .locals 4
    .param p1, "l"    # Lcom/samsung/android/allshare/media/Receiver$IProgressUpdateEventListener;

    .prologue
    .line 195
    iget-object v0, p0, Lcom/samsung/android/allshare/ReceiverImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/allshare/ReceiverImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    invoke-interface {v0}, Lcom/samsung/android/allshare/IAllShareConnector;->isAllShareServiceConnected()Z

    move-result v0

    if-nez v0, :cond_2

    .line 196
    :cond_0
    const-string v0, "ReceiverImpl"

    const-string v1, "setEventListener error! AllShareService is not connected"

    invoke-static {v0, v1}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 213
    :cond_1
    :goto_0
    return-void

    .line 200
    :cond_2
    iput-object p1, p0, Lcom/samsung/android/allshare/ReceiverImpl;->mProgressUpdateListener:Lcom/samsung/android/allshare/media/Receiver$IProgressUpdateEventListener;

    .line 202
    iget-boolean v0, p0, Lcom/samsung/android/allshare/ReceiverImpl;->mIsSubscribed:Z

    if-nez v0, :cond_3

    if-eqz p1, :cond_3

    .line 203
    iget-object v0, p0, Lcom/samsung/android/allshare/ReceiverImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    const-string v1, "com.sec.android.allshare.event.EVENT_DEVICE_SUBSCRIBE"

    iget-object v2, p0, Lcom/samsung/android/allshare/ReceiverImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    invoke-virtual {v2}, Lcom/samsung/android/allshare/DeviceImpl;->getBundle()Landroid/os/Bundle;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/allshare/ReceiverImpl;->mEventHandler:Lcom/samsung/android/allshare/AllShareEventHandler;

    invoke-interface {v0, v1, v2, v3}, Lcom/samsung/android/allshare/IAllShareConnector;->subscribeAllShareEvent(Ljava/lang/String;Landroid/os/Bundle;Lcom/samsung/android/allshare/AllShareEventHandler;)Z

    .line 205
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/allshare/ReceiverImpl;->mIsSubscribed:Z

    goto :goto_0

    .line 206
    :cond_3
    iget-boolean v0, p0, Lcom/samsung/android/allshare/ReceiverImpl;->mIsSubscribed:Z

    if-eqz v0, :cond_1

    if-nez p1, :cond_1

    .line 207
    iget-object v0, p0, Lcom/samsung/android/allshare/ReceiverImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    const-string v1, "com.sec.android.allshare.event.EVENT_DEVICE_SUBSCRIBE"

    iget-object v2, p0, Lcom/samsung/android/allshare/ReceiverImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    invoke-virtual {v2}, Lcom/samsung/android/allshare/DeviceImpl;->getBundle()Landroid/os/Bundle;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/allshare/ReceiverImpl;->mEventHandler:Lcom/samsung/android/allshare/AllShareEventHandler;

    invoke-interface {v0, v1, v2, v3}, Lcom/samsung/android/allshare/IAllShareConnector;->unsubscribeAllShareEvent(Ljava/lang/String;Landroid/os/Bundle;Lcom/samsung/android/allshare/AllShareEventHandler;)V

    .line 209
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/allshare/ReceiverImpl;->mIsSubscribed:Z

    goto :goto_0
.end method

.method public setReceiverResponseListener(Lcom/samsung/android/allshare/media/Receiver$IReceiverResponseListener;)V
    .locals 0
    .param p1, "l"    # Lcom/samsung/android/allshare/media/Receiver$IReceiverResponseListener;

    .prologue
    .line 217
    iput-object p1, p0, Lcom/samsung/android/allshare/ReceiverImpl;->mReceiverResponseListener:Lcom/samsung/android/allshare/media/Receiver$IReceiverResponseListener;

    .line 218
    return-void
.end method
