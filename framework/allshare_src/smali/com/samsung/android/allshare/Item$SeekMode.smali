.class public final enum Lcom/samsung/android/allshare/Item$SeekMode;
.super Ljava/lang/Enum;
.source "Item.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/allshare/Item;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "SeekMode"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/android/allshare/Item$SeekMode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/android/allshare/Item$SeekMode;

.field public static final enum ANY:Lcom/samsung/android/allshare/Item$SeekMode;

.field public static final enum BYTE:Lcom/samsung/android/allshare/Item$SeekMode;

.field public static final enum NONE:Lcom/samsung/android/allshare/Item$SeekMode;

.field public static final enum TIME:Lcom/samsung/android/allshare/Item$SeekMode;

.field public static final enum UNKNOWN:Lcom/samsung/android/allshare/Item$SeekMode;


# instance fields
.field private final enumString:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 155
    new-instance v0, Lcom/samsung/android/allshare/Item$SeekMode;

    const-string v1, "BYTE"

    const-string v2, "BYTE"

    invoke-direct {v0, v1, v3, v2}, Lcom/samsung/android/allshare/Item$SeekMode;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/android/allshare/Item$SeekMode;->BYTE:Lcom/samsung/android/allshare/Item$SeekMode;

    .line 157
    new-instance v0, Lcom/samsung/android/allshare/Item$SeekMode;

    const-string v1, "TIME"

    const-string v2, "TIME"

    invoke-direct {v0, v1, v4, v2}, Lcom/samsung/android/allshare/Item$SeekMode;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/android/allshare/Item$SeekMode;->TIME:Lcom/samsung/android/allshare/Item$SeekMode;

    .line 159
    new-instance v0, Lcom/samsung/android/allshare/Item$SeekMode;

    const-string v1, "ANY"

    const-string v2, "ANY"

    invoke-direct {v0, v1, v5, v2}, Lcom/samsung/android/allshare/Item$SeekMode;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/android/allshare/Item$SeekMode;->ANY:Lcom/samsung/android/allshare/Item$SeekMode;

    .line 161
    new-instance v0, Lcom/samsung/android/allshare/Item$SeekMode;

    const-string v1, "NONE"

    const-string v2, "NONE"

    invoke-direct {v0, v1, v6, v2}, Lcom/samsung/android/allshare/Item$SeekMode;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/android/allshare/Item$SeekMode;->NONE:Lcom/samsung/android/allshare/Item$SeekMode;

    .line 163
    new-instance v0, Lcom/samsung/android/allshare/Item$SeekMode;

    const-string v1, "UNKNOWN"

    const-string v2, "UNKNOWN"

    invoke-direct {v0, v1, v7, v2}, Lcom/samsung/android/allshare/Item$SeekMode;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/android/allshare/Item$SeekMode;->UNKNOWN:Lcom/samsung/android/allshare/Item$SeekMode;

    .line 153
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/samsung/android/allshare/Item$SeekMode;

    sget-object v1, Lcom/samsung/android/allshare/Item$SeekMode;->BYTE:Lcom/samsung/android/allshare/Item$SeekMode;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/android/allshare/Item$SeekMode;->TIME:Lcom/samsung/android/allshare/Item$SeekMode;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/android/allshare/Item$SeekMode;->ANY:Lcom/samsung/android/allshare/Item$SeekMode;

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/android/allshare/Item$SeekMode;->NONE:Lcom/samsung/android/allshare/Item$SeekMode;

    aput-object v1, v0, v6

    sget-object v1, Lcom/samsung/android/allshare/Item$SeekMode;->UNKNOWN:Lcom/samsung/android/allshare/Item$SeekMode;

    aput-object v1, v0, v7

    sput-object v0, Lcom/samsung/android/allshare/Item$SeekMode;->$VALUES:[Lcom/samsung/android/allshare/Item$SeekMode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "enumStr"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 169
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 170
    iput-object p3, p0, Lcom/samsung/android/allshare/Item$SeekMode;->enumString:Ljava/lang/String;

    .line 171
    return-void
.end method

.method public static stringToEnum(Ljava/lang/String;)Lcom/samsung/android/allshare/Item$SeekMode;
    .locals 1
    .param p0, "enumStr"    # Ljava/lang/String;

    .prologue
    .line 182
    if-nez p0, :cond_0

    .line 183
    sget-object v0, Lcom/samsung/android/allshare/Item$SeekMode;->UNKNOWN:Lcom/samsung/android/allshare/Item$SeekMode;

    .line 196
    :goto_0
    return-object v0

    .line 185
    :cond_0
    const-string v0, "ANY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 186
    sget-object v0, Lcom/samsung/android/allshare/Item$SeekMode;->ANY:Lcom/samsung/android/allshare/Item$SeekMode;

    goto :goto_0

    .line 187
    :cond_1
    const-string v0, "BYTE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 188
    sget-object v0, Lcom/samsung/android/allshare/Item$SeekMode;->BYTE:Lcom/samsung/android/allshare/Item$SeekMode;

    goto :goto_0

    .line 189
    :cond_2
    const-string v0, "NONE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 190
    sget-object v0, Lcom/samsung/android/allshare/Item$SeekMode;->NONE:Lcom/samsung/android/allshare/Item$SeekMode;

    goto :goto_0

    .line 191
    :cond_3
    const-string v0, "TIME"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 192
    sget-object v0, Lcom/samsung/android/allshare/Item$SeekMode;->TIME:Lcom/samsung/android/allshare/Item$SeekMode;

    goto :goto_0

    .line 193
    :cond_4
    const-string v0, "UNKNOWN"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 194
    sget-object v0, Lcom/samsung/android/allshare/Item$SeekMode;->UNKNOWN:Lcom/samsung/android/allshare/Item$SeekMode;

    goto :goto_0

    .line 196
    :cond_5
    sget-object v0, Lcom/samsung/android/allshare/Item$SeekMode;->UNKNOWN:Lcom/samsung/android/allshare/Item$SeekMode;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/android/allshare/Item$SeekMode;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 153
    const-class v0, Lcom/samsung/android/allshare/Item$SeekMode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/allshare/Item$SeekMode;

    return-object v0
.end method

.method public static values()[Lcom/samsung/android/allshare/Item$SeekMode;
    .locals 1

    .prologue
    .line 153
    sget-object v0, Lcom/samsung/android/allshare/Item$SeekMode;->$VALUES:[Lcom/samsung/android/allshare/Item$SeekMode;

    invoke-virtual {v0}, [Lcom/samsung/android/allshare/Item$SeekMode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/allshare/Item$SeekMode;

    return-object v0
.end method


# virtual methods
.method public enumToString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 176
    iget-object v0, p0, Lcom/samsung/android/allshare/Item$SeekMode;->enumString:Ljava/lang/String;

    return-object v0
.end method
