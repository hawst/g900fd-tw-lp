.class public final enum Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;
.super Ljava/lang/Enum;
.source "AVPlayer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/allshare/media/AVPlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "AVPlayerState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;

.field public static final enum BUFFERING:Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;

.field public static final enum CONTENT_CHANGED:Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;

.field public static final enum PAUSED:Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;

.field public static final enum PLAYING:Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;

.field public static final enum STOPPED:Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;

.field public static final enum UNKNOWN:Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;


# instance fields
.field private final enumString:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 182
    new-instance v0, Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;

    const-string v1, "STOPPED"

    const-string v2, "STOPPED"

    invoke-direct {v0, v1, v4, v2}, Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;->STOPPED:Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;

    .line 187
    new-instance v0, Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;

    const-string v1, "BUFFERING"

    const-string v2, "BUFFERING"

    invoke-direct {v0, v1, v5, v2}, Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;->BUFFERING:Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;

    .line 192
    new-instance v0, Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;

    const-string v1, "PLAYING"

    const-string v2, "PLAYING"

    invoke-direct {v0, v1, v6, v2}, Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;->PLAYING:Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;

    .line 197
    new-instance v0, Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;

    const-string v1, "PAUSED"

    const-string v2, "PAUSED"

    invoke-direct {v0, v1, v7, v2}, Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;->PAUSED:Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;

    .line 208
    new-instance v0, Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;

    const-string v1, "CONTENT_CHANGED"

    const-string v2, "CONTENT_CHANGED"

    invoke-direct {v0, v1, v8, v2}, Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;->CONTENT_CHANGED:Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;

    .line 213
    new-instance v0, Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;

    const-string v1, "UNKNOWN"

    const/4 v2, 0x5

    const-string v3, "UNKNOWN"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;->UNKNOWN:Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;

    .line 177
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;

    sget-object v1, Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;->STOPPED:Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;->BUFFERING:Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;->PLAYING:Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;

    aput-object v1, v0, v6

    sget-object v1, Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;->PAUSED:Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;

    aput-object v1, v0, v7

    sget-object v1, Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;->CONTENT_CHANGED:Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;->UNKNOWN:Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;->$VALUES:[Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "enumStr"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 219
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 220
    iput-object p3, p0, Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;->enumString:Ljava/lang/String;

    .line 221
    return-void
.end method

.method public static stringToEnum(Ljava/lang/String;)Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;
    .locals 1
    .param p0, "enumStr"    # Ljava/lang/String;

    .prologue
    .line 232
    if-nez p0, :cond_0

    .line 233
    sget-object v0, Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;->UNKNOWN:Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;

    .line 248
    :goto_0
    return-object v0

    .line 235
    :cond_0
    const-string v0, "BUFFERING"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 236
    sget-object v0, Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;->BUFFERING:Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;

    goto :goto_0

    .line 237
    :cond_1
    const-string v0, "CONTENT_CHANGED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 238
    sget-object v0, Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;->CONTENT_CHANGED:Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;

    goto :goto_0

    .line 239
    :cond_2
    const-string v0, "PLAYING"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 240
    sget-object v0, Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;->PLAYING:Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;

    goto :goto_0

    .line 241
    :cond_3
    const-string v0, "PAUSED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 242
    sget-object v0, Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;->PAUSED:Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;

    goto :goto_0

    .line 243
    :cond_4
    const-string v0, "STOPPED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 244
    sget-object v0, Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;->STOPPED:Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;

    goto :goto_0

    .line 245
    :cond_5
    const-string v0, "UNKNOWN"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 246
    sget-object v0, Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;->UNKNOWN:Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;

    goto :goto_0

    .line 248
    :cond_6
    sget-object v0, Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;->UNKNOWN:Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 177
    const-class v0, Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;

    return-object v0
.end method

.method public static values()[Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;
    .locals 1

    .prologue
    .line 177
    sget-object v0, Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;->$VALUES:[Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;

    invoke-virtual {v0}, [Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;

    return-object v0
.end method


# virtual methods
.method public enumToString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 226
    iget-object v0, p0, Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;->enumString:Ljava/lang/String;

    return-object v0
.end method
