.class public Lcom/samsung/android/allshare/media/SearchCriteria;
.super Ljava/lang/Object;
.source "SearchCriteria.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/allshare/media/SearchCriteria$1;,
        Lcom/samsung/android/allshare/media/SearchCriteria$Builder;
    }
.end annotation


# instance fields
.field private mKeyword:Ljava/lang/String;

.field private mMediaTypes:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/Item$MediaType;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/samsung/android/allshare/media/SearchCriteria$Builder;)V
    .locals 1
    .param p1, "builder"    # Lcom/samsung/android/allshare/media/SearchCriteria$Builder;

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    # getter for: Lcom/samsung/android/allshare/media/SearchCriteria$Builder;->mKeyword:Ljava/lang/String;
    invoke-static {p1}, Lcom/samsung/android/allshare/media/SearchCriteria$Builder;->access$000(Lcom/samsung/android/allshare/media/SearchCriteria$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/allshare/media/SearchCriteria;->mKeyword:Ljava/lang/String;

    .line 42
    # getter for: Lcom/samsung/android/allshare/media/SearchCriteria$Builder;->mMediaTypes:Ljava/util/ArrayList;
    invoke-static {p1}, Lcom/samsung/android/allshare/media/SearchCriteria$Builder;->access$100(Lcom/samsung/android/allshare/media/SearchCriteria$Builder;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/allshare/media/SearchCriteria;->mMediaTypes:Ljava/util/ArrayList;

    .line 43
    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/allshare/media/SearchCriteria$Builder;Lcom/samsung/android/allshare/media/SearchCriteria$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/android/allshare/media/SearchCriteria$Builder;
    .param p2, "x1"    # Lcom/samsung/android/allshare/media/SearchCriteria$1;

    .prologue
    .line 35
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/media/SearchCriteria;-><init>(Lcom/samsung/android/allshare/media/SearchCriteria$Builder;)V

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 11
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 58
    instance-of v9, p1, Lcom/samsung/android/allshare/media/SearchCriteria;

    if-eqz v9, :cond_0

    move-object v4, p1

    .line 59
    check-cast v4, Lcom/samsung/android/allshare/media/SearchCriteria;

    .line 60
    .local v4, "sc":Lcom/samsung/android/allshare/media/SearchCriteria;
    const/4 v3, 0x0

    .line 62
    .local v3, "ret":Z
    if-nez v4, :cond_1

    .line 107
    .end local v3    # "ret":Z
    .end local v4    # "sc":Lcom/samsung/android/allshare/media/SearchCriteria;
    :cond_0
    :goto_0
    return v7

    .line 66
    .restart local v3    # "ret":Z
    .restart local v4    # "sc":Lcom/samsung/android/allshare/media/SearchCriteria;
    :cond_1
    iget-object v9, v4, Lcom/samsung/android/allshare/media/SearchCriteria;->mKeyword:Ljava/lang/String;

    if-nez v9, :cond_2

    iget-object v9, p0, Lcom/samsung/android/allshare/media/SearchCriteria;->mKeyword:Ljava/lang/String;

    if-nez v9, :cond_0

    :cond_2
    iget-object v9, v4, Lcom/samsung/android/allshare/media/SearchCriteria;->mKeyword:Ljava/lang/String;

    if-eqz v9, :cond_3

    iget-object v9, p0, Lcom/samsung/android/allshare/media/SearchCriteria;->mKeyword:Ljava/lang/String;

    if-eqz v9, :cond_0

    .line 69
    :cond_3
    iget-object v9, v4, Lcom/samsung/android/allshare/media/SearchCriteria;->mKeyword:Ljava/lang/String;

    if-nez v9, :cond_7

    iget-object v9, p0, Lcom/samsung/android/allshare/media/SearchCriteria;->mKeyword:Ljava/lang/String;

    if-nez v9, :cond_7

    .line 70
    const/4 v3, 0x1

    .line 76
    :cond_4
    :goto_1
    if-eqz v3, :cond_0

    .line 78
    iget-object v9, v4, Lcom/samsung/android/allshare/media/SearchCriteria;->mMediaTypes:Ljava/util/ArrayList;

    if-eqz v9, :cond_a

    iget-object v9, p0, Lcom/samsung/android/allshare/media/SearchCriteria;->mMediaTypes:Ljava/util/ArrayList;

    if-eqz v9, :cond_a

    .line 79
    iget-object v9, p0, Lcom/samsung/android/allshare/media/SearchCriteria;->mMediaTypes:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 80
    .local v1, "itr1":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/allshare/Item$MediaType;>;"
    :cond_5
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_9

    .line 81
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/android/allshare/Item$MediaType;

    .line 82
    .local v5, "type1":Lcom/samsung/android/allshare/Item$MediaType;
    const/4 v0, 0x0

    .line 84
    .local v0, "isSame":Z
    iget-object v9, v4, Lcom/samsung/android/allshare/media/SearchCriteria;->mMediaTypes:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 85
    .local v2, "itr2":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/allshare/Item$MediaType;>;"
    :cond_6
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_8

    .line 86
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/android/allshare/Item$MediaType;

    .line 88
    .local v6, "type2":Lcom/samsung/android/allshare/Item$MediaType;
    if-ne v5, v6, :cond_6

    .line 89
    const/4 v0, 0x1

    goto :goto_2

    .line 71
    .end local v0    # "isSame":Z
    .end local v1    # "itr1":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/allshare/Item$MediaType;>;"
    .end local v2    # "itr2":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/allshare/Item$MediaType;>;"
    .end local v5    # "type1":Lcom/samsung/android/allshare/Item$MediaType;
    .end local v6    # "type2":Lcom/samsung/android/allshare/Item$MediaType;
    :cond_7
    iget-object v9, v4, Lcom/samsung/android/allshare/media/SearchCriteria;->mKeyword:Ljava/lang/String;

    iget-object v10, p0, Lcom/samsung/android/allshare/media/SearchCriteria;->mKeyword:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v9

    if-nez v9, :cond_4

    .line 72
    const/4 v3, 0x1

    goto :goto_1

    .line 93
    .restart local v0    # "isSame":Z
    .restart local v1    # "itr1":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/allshare/Item$MediaType;>;"
    .restart local v2    # "itr2":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/allshare/Item$MediaType;>;"
    .restart local v5    # "type1":Lcom/samsung/android/allshare/Item$MediaType;
    :cond_8
    if-nez v0, :cond_5

    goto :goto_0

    .end local v0    # "isSame":Z
    .end local v2    # "itr2":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/allshare/Item$MediaType;>;"
    .end local v5    # "type1":Lcom/samsung/android/allshare/Item$MediaType;
    :cond_9
    move v7, v8

    .line 98
    goto :goto_0

    .line 99
    .end local v1    # "itr1":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/allshare/Item$MediaType;>;"
    :cond_a
    iget-object v9, v4, Lcom/samsung/android/allshare/media/SearchCriteria;->mMediaTypes:Ljava/util/ArrayList;

    if-nez v9, :cond_0

    iget-object v9, p0, Lcom/samsung/android/allshare/media/SearchCriteria;->mMediaTypes:Ljava/util/ArrayList;

    if-nez v9, :cond_0

    move v7, v8

    .line 100
    goto :goto_0
.end method

.method public getKeyword()Ljava/lang/String;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/samsung/android/allshare/media/SearchCriteria;->mKeyword:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 114
    const/4 v0, 0x0

    .line 115
    .local v0, "code":I
    iget-object v3, p0, Lcom/samsung/android/allshare/media/SearchCriteria;->mKeyword:Ljava/lang/String;

    if-eqz v3, :cond_0

    .line 116
    iget-object v3, p0, Lcom/samsung/android/allshare/media/SearchCriteria;->mKeyword:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v3

    xor-int/2addr v0, v3

    .line 118
    :cond_0
    iget-object v3, p0, Lcom/samsung/android/allshare/media/SearchCriteria;->mMediaTypes:Ljava/util/ArrayList;

    if-eqz v3, :cond_1

    .line 119
    iget-object v3, p0, Lcom/samsung/android/allshare/media/SearchCriteria;->mMediaTypes:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/allshare/Item$MediaType;

    .line 120
    .local v2, "mediaType":Lcom/samsung/android/allshare/Item$MediaType;
    invoke-virtual {v2}, Lcom/samsung/android/allshare/Item$MediaType;->hashCode()I

    move-result v3

    xor-int/2addr v0, v3

    .line 121
    goto :goto_0

    .line 123
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "mediaType":Lcom/samsung/android/allshare/Item$MediaType;
    :cond_1
    return v0
.end method

.method public isMatchedItemType(Lcom/samsung/android/allshare/Item$MediaType;)Z
    .locals 4
    .param p1, "type"    # Lcom/samsung/android/allshare/Item$MediaType;

    .prologue
    const/4 v2, 0x0

    .line 134
    iget-object v3, p0, Lcom/samsung/android/allshare/media/SearchCriteria;->mMediaTypes:Ljava/util/ArrayList;

    if-nez v3, :cond_1

    .line 140
    :cond_0
    :goto_0
    return v2

    .line 137
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/allshare/media/SearchCriteria;->mMediaTypes:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/allshare/Item$MediaType;

    .line 138
    .local v1, "temp":Lcom/samsung/android/allshare/Item$MediaType;
    invoke-virtual {v1, p1}, Lcom/samsung/android/allshare/Item$MediaType;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 139
    const/4 v2, 0x1

    goto :goto_0
.end method
