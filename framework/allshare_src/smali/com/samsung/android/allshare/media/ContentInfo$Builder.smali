.class public Lcom/samsung/android/allshare/media/ContentInfo$Builder;
.super Ljava/lang/Object;
.source "ContentInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/allshare/media/ContentInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private mStartingPosition:J


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/android/allshare/media/ContentInfo$Builder;->mStartingPosition:J

    .line 73
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/allshare/media/ContentInfo$Builder;)J
    .locals 2
    .param p0, "x0"    # Lcom/samsung/android/allshare/media/ContentInfo$Builder;

    .prologue
    .line 65
    iget-wide v0, p0, Lcom/samsung/android/allshare/media/ContentInfo$Builder;->mStartingPosition:J

    return-wide v0
.end method


# virtual methods
.method public build()Lcom/samsung/android/allshare/media/ContentInfo;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 101
    iget-wide v2, p0, Lcom/samsung/android/allshare/media/ContentInfo$Builder;->mStartingPosition:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-gez v1, :cond_0

    .line 104
    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Lcom/samsung/android/allshare/media/ContentInfo;

    invoke-direct {v1, p0, v0}, Lcom/samsung/android/allshare/media/ContentInfo;-><init>(Lcom/samsung/android/allshare/media/ContentInfo$Builder;Lcom/samsung/android/allshare/media/ContentInfo$1;)V

    move-object v0, v1

    goto :goto_0
.end method

.method public setStartingPosition(J)Lcom/samsung/android/allshare/media/ContentInfo$Builder;
    .locals 1
    .param p1, "position"    # J

    .prologue
    .line 86
    iput-wide p1, p0, Lcom/samsung/android/allshare/media/ContentInfo$Builder;->mStartingPosition:J

    .line 87
    return-object p0
.end method
