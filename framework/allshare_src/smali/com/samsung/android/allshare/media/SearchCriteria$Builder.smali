.class public Lcom/samsung/android/allshare/media/SearchCriteria$Builder;
.super Ljava/lang/Object;
.source "SearchCriteria.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/allshare/media/SearchCriteria;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private mKeyword:Ljava/lang/String;

.field private mMediaTypes:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/Item$MediaType;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 188
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 189
    iget-object v0, p0, Lcom/samsung/android/allshare/media/SearchCriteria$Builder;->mKeyword:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 190
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/allshare/media/SearchCriteria$Builder;->mKeyword:Ljava/lang/String;

    .line 191
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/media/SearchCriteria$Builder;->mMediaTypes:Ljava/util/ArrayList;

    .line 192
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/allshare/media/SearchCriteria$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/media/SearchCriteria$Builder;

    .prologue
    .line 178
    iget-object v0, p0, Lcom/samsung/android/allshare/media/SearchCriteria$Builder;->mKeyword:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/android/allshare/media/SearchCriteria$Builder;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/media/SearchCriteria$Builder;

    .prologue
    .line 178
    iget-object v0, p0, Lcom/samsung/android/allshare/media/SearchCriteria$Builder;->mMediaTypes:Ljava/util/ArrayList;

    return-object v0
.end method


# virtual methods
.method public addItemType(Lcom/samsung/android/allshare/Item$MediaType;)Lcom/samsung/android/allshare/media/SearchCriteria$Builder;
    .locals 1
    .param p1, "type"    # Lcom/samsung/android/allshare/Item$MediaType;

    .prologue
    .line 224
    if-eqz p1, :cond_0

    .line 225
    iget-object v0, p0, Lcom/samsung/android/allshare/media/SearchCriteria$Builder;->mMediaTypes:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 226
    :cond_0
    return-object p0
.end method

.method public build()Lcom/samsung/android/allshare/media/SearchCriteria;
    .locals 2

    .prologue
    .line 240
    new-instance v0, Lcom/samsung/android/allshare/media/SearchCriteria;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/samsung/android/allshare/media/SearchCriteria;-><init>(Lcom/samsung/android/allshare/media/SearchCriteria$Builder;Lcom/samsung/android/allshare/media/SearchCriteria$1;)V

    return-object v0
.end method

.method public setKeyword(Ljava/lang/String;)Lcom/samsung/android/allshare/media/SearchCriteria$Builder;
    .locals 0
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 206
    iput-object p1, p0, Lcom/samsung/android/allshare/media/SearchCriteria$Builder;->mKeyword:Ljava/lang/String;

    .line 207
    return-object p0
.end method
