.class public final enum Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;
.super Ljava/lang/Enum;
.source "ImageViewer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/allshare/media/ImageViewer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ImageViewerState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;

.field public static final enum BUFFERING:Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;

.field public static final enum CONTENT_CHANGED:Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;

.field public static final enum SHOWING:Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;

.field public static final enum STOPPED:Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;

.field public static final enum UNKNOWN:Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;


# instance fields
.field private final enumString:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 123
    new-instance v0, Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;

    const-string v1, "STOPPED"

    const-string v2, "STOPPED"

    invoke-direct {v0, v1, v3, v2}, Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;->STOPPED:Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;

    .line 128
    new-instance v0, Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;

    const-string v1, "BUFFERING"

    const-string v2, "BUFFERING"

    invoke-direct {v0, v1, v4, v2}, Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;->BUFFERING:Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;

    .line 133
    new-instance v0, Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;

    const-string v1, "SHOWING"

    const-string v2, "SHOWING"

    invoke-direct {v0, v1, v5, v2}, Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;->SHOWING:Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;

    .line 144
    new-instance v0, Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;

    const-string v1, "CONTENT_CHANGED"

    const-string v2, "CONTENT_CHANGED"

    invoke-direct {v0, v1, v6, v2}, Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;->CONTENT_CHANGED:Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;

    .line 149
    new-instance v0, Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;

    const-string v1, "UNKNOWN"

    const-string v2, "UNKNOWN"

    invoke-direct {v0, v1, v7, v2}, Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;->UNKNOWN:Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;

    .line 118
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;

    sget-object v1, Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;->STOPPED:Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;->BUFFERING:Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;->SHOWING:Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;->CONTENT_CHANGED:Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;

    aput-object v1, v0, v6

    sget-object v1, Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;->UNKNOWN:Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;

    aput-object v1, v0, v7

    sput-object v0, Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;->$VALUES:[Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "enumStr"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 155
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 156
    iput-object p3, p0, Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;->enumString:Ljava/lang/String;

    .line 157
    return-void
.end method

.method public static stringToEnum(Ljava/lang/String;)Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;
    .locals 1
    .param p0, "enumStr"    # Ljava/lang/String;

    .prologue
    .line 168
    if-nez p0, :cond_0

    .line 169
    sget-object v0, Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;->UNKNOWN:Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;

    .line 182
    :goto_0
    return-object v0

    .line 171
    :cond_0
    const-string v0, "BUFFERING"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 172
    sget-object v0, Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;->BUFFERING:Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;

    goto :goto_0

    .line 173
    :cond_1
    const-string v0, "CONTENT_CHANGED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 174
    sget-object v0, Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;->CONTENT_CHANGED:Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;

    goto :goto_0

    .line 175
    :cond_2
    const-string v0, "SHOWING"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 176
    sget-object v0, Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;->SHOWING:Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;

    goto :goto_0

    .line 177
    :cond_3
    const-string v0, "STOPPED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 178
    sget-object v0, Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;->STOPPED:Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;

    goto :goto_0

    .line 179
    :cond_4
    const-string v0, "UNKNOWN"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 180
    sget-object v0, Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;->UNKNOWN:Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;

    goto :goto_0

    .line 182
    :cond_5
    sget-object v0, Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;->UNKNOWN:Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 118
    const-class v0, Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;

    return-object v0
.end method

.method public static values()[Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;
    .locals 1

    .prologue
    .line 118
    sget-object v0, Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;->$VALUES:[Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;

    invoke-virtual {v0}, [Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;

    return-object v0
.end method


# virtual methods
.method public enumToString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;->enumString:Ljava/lang/String;

    return-object v0
.end method
