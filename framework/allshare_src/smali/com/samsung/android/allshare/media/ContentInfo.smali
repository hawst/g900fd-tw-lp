.class public Lcom/samsung/android/allshare/media/ContentInfo;
.super Ljava/lang/Object;
.source "ContentInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/allshare/media/ContentInfo$Builder;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/samsung/android/allshare/media/ContentInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mStartingPosition:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 148
    new-instance v0, Lcom/samsung/android/allshare/media/ContentInfo$1;

    invoke-direct {v0}, Lcom/samsung/android/allshare/media/ContentInfo$1;-><init>()V

    sput-object v0, Lcom/samsung/android/allshare/media/ContentInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "src"    # Landroid/os/Parcel;

    .prologue
    .line 135
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 136
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/media/ContentInfo;->readFromParcel(Landroid/os/Parcel;)V

    .line 137
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/samsung/android/allshare/media/ContentInfo$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/samsung/android/allshare/media/ContentInfo$1;

    .prologue
    .line 36
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/media/ContentInfo;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method private constructor <init>(Lcom/samsung/android/allshare/media/ContentInfo$Builder;)V
    .locals 2
    .param p1, "builder"    # Lcom/samsung/android/allshare/media/ContentInfo$Builder;

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    # getter for: Lcom/samsung/android/allshare/media/ContentInfo$Builder;->mStartingPosition:J
    invoke-static {p1}, Lcom/samsung/android/allshare/media/ContentInfo$Builder;->access$000(Lcom/samsung/android/allshare/media/ContentInfo$Builder;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/android/allshare/media/ContentInfo;->mStartingPosition:J

    .line 46
    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/allshare/media/ContentInfo$Builder;Lcom/samsung/android/allshare/media/ContentInfo$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/android/allshare/media/ContentInfo$Builder;
    .param p2, "x1"    # Lcom/samsung/android/allshare/media/ContentInfo$1;

    .prologue
    .line 36
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/media/ContentInfo;-><init>(Lcom/samsung/android/allshare/media/ContentInfo$Builder;)V

    return-void
.end method

.method private readFromParcel(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "src"    # Landroid/os/Parcel;

    .prologue
    .line 132
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/android/allshare/media/ContentInfo;->mStartingPosition:J

    .line 133
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 115
    const/4 v0, 0x0

    return v0
.end method

.method public getStartingPosition()J
    .locals 2

    .prologue
    .line 55
    iget-wide v0, p0, Lcom/samsung/android/allshare/media/ContentInfo;->mStartingPosition:J

    return-wide v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 127
    iget-wide v0, p0, Lcom/samsung/android/allshare/media/ContentInfo;->mStartingPosition:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 128
    return-void
.end method
