.class Lcom/samsung/android/allshare/ReceiverImpl$1;
.super Lcom/samsung/android/allshare/AllShareEventHandler;
.source "ReceiverImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/allshare/ReceiverImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/allshare/ReceiverImpl;


# direct methods
.method constructor <init>(Lcom/samsung/android/allshare/ReceiverImpl;Landroid/os/Looper;)V
    .locals 0
    .param p2, "x0"    # Landroid/os/Looper;

    .prologue
    .line 61
    iput-object p1, p0, Lcom/samsung/android/allshare/ReceiverImpl$1;->this$0:Lcom/samsung/android/allshare/ReceiverImpl;

    invoke-direct {p0, p2}, Lcom/samsung/android/allshare/AllShareEventHandler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleEventMessage(Lcom/sec/android/allshare/iface/CVMessage;)V
    .locals 14
    .param p1, "cvm"    # Lcom/sec/android/allshare/iface/CVMessage;

    .prologue
    .line 65
    invoke-virtual {p1}, Lcom/sec/android/allshare/iface/CVMessage;->getActionID()Ljava/lang/String;

    move-result-object v0

    .line 66
    .local v0, "actionID":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/sec/android/allshare/iface/CVMessage;->getBundle()Landroid/os/Bundle;

    move-result-object v8

    .line 68
    .local v8, "bundle":Landroid/os/Bundle;
    iget-object v1, p0, Lcom/samsung/android/allshare/ReceiverImpl$1;->this$0:Lcom/samsung/android/allshare/ReceiverImpl;

    # getter for: Lcom/samsung/android/allshare/ReceiverImpl;->mProgressUpdateListener:Lcom/samsung/android/allshare/media/Receiver$IProgressUpdateEventListener;
    invoke-static {v1}, Lcom/samsung/android/allshare/ReceiverImpl;->access$000(Lcom/samsung/android/allshare/ReceiverImpl;)Lcom/samsung/android/allshare/media/Receiver$IProgressUpdateEventListener;

    move-result-object v1

    if-nez v1, :cond_1

    .line 109
    :cond_0
    :goto_0
    return-void

    .line 70
    :cond_1
    const-string v1, "BUNDLE_ENUM_ERROR"

    invoke-virtual {v8, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 71
    .local v11, "errStr":Ljava/lang/String;
    sget-object v7, Lcom/samsung/android/allshare/ERROR;->FAIL:Lcom/samsung/android/allshare/ERROR;

    .line 72
    .local v7, "error":Lcom/samsung/android/allshare/ERROR;
    if-nez v11, :cond_2

    .line 73
    sget-object v7, Lcom/samsung/android/allshare/ERROR;->SUCCESS:Lcom/samsung/android/allshare/ERROR;

    .line 77
    :goto_1
    const-string v1, "com.sec.android.allshare.event.EVENT_RECEIVER_PROGRESS_UPDATE_BY_ITEM"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 78
    const-string v1, "BUNDLE_LONG_PROGRESS"

    invoke-virtual {v8, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 79
    .local v2, "receivedSize":J
    const-string v1, "BUNDLE_LONG_TOTAL_SIZE"

    invoke-virtual {v8, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 80
    .local v4, "totalSize":J
    const-string v1, "BUNDLE_PARCELABLE_ITEM"

    invoke-virtual {v8, v1}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v12

    .line 82
    .local v12, "itemBundle":Landroid/os/Bundle;
    iget-object v1, p0, Lcom/samsung/android/allshare/ReceiverImpl$1;->this$0:Lcom/samsung/android/allshare/ReceiverImpl;

    invoke-virtual {v1, v12}, Lcom/samsung/android/allshare/ReceiverImpl;->getItem(Landroid/os/Bundle;)Lcom/samsung/android/allshare/Item;

    move-result-object v6

    .line 84
    .local v6, "item":Lcom/samsung/android/allshare/Item;
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/allshare/ReceiverImpl$1;->this$0:Lcom/samsung/android/allshare/ReceiverImpl;

    # getter for: Lcom/samsung/android/allshare/ReceiverImpl;->mProgressUpdateListener:Lcom/samsung/android/allshare/media/Receiver$IProgressUpdateEventListener;
    invoke-static {v1}, Lcom/samsung/android/allshare/ReceiverImpl;->access$000(Lcom/samsung/android/allshare/ReceiverImpl;)Lcom/samsung/android/allshare/media/Receiver$IProgressUpdateEventListener;

    move-result-object v1

    invoke-interface/range {v1 .. v7}, Lcom/samsung/android/allshare/media/Receiver$IProgressUpdateEventListener;->onProgressUpdated(JJLcom/samsung/android/allshare/Item;Lcom/samsung/android/allshare/ERROR;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 85
    :catch_0
    move-exception v9

    .line 86
    .local v9, "e":Ljava/lang/Exception;
    const-string v1, "ReceiverImpl"

    const-string v13, "mEventHandler(EVENT_RECEIVER_PROGRESS_UPDATE_BY_ITEM) Exception "

    invoke-static {v1, v13, v9}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0

    .line 75
    .end local v2    # "receivedSize":J
    .end local v4    # "totalSize":J
    .end local v6    # "item":Lcom/samsung/android/allshare/Item;
    .end local v9    # "e":Ljava/lang/Exception;
    .end local v12    # "itemBundle":Landroid/os/Bundle;
    :cond_2
    invoke-static {v11}, Lcom/samsung/android/allshare/ERROR;->stringToEnum(Ljava/lang/String;)Lcom/samsung/android/allshare/ERROR;

    move-result-object v7

    goto :goto_1

    .line 88
    .restart local v2    # "receivedSize":J
    .restart local v4    # "totalSize":J
    .restart local v6    # "item":Lcom/samsung/android/allshare/Item;
    .restart local v12    # "itemBundle":Landroid/os/Bundle;
    :catch_1
    move-exception v10

    .line 89
    .local v10, "err":Ljava/lang/Error;
    const-string v1, "ReceiverImpl"

    const-string v13, "mEventHandler(EVENT_RECEIVER_PROGRESS_UPDATE_BY_ITEM) Error "

    invoke-static {v1, v13, v10}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Error;)V

    goto :goto_0

    .line 93
    .end local v2    # "receivedSize":J
    .end local v4    # "totalSize":J
    .end local v6    # "item":Lcom/samsung/android/allshare/Item;
    .end local v10    # "err":Ljava/lang/Error;
    .end local v12    # "itemBundle":Landroid/os/Bundle;
    :cond_3
    const-string v1, "com.sec.android.allshare.event.EVENT_RECEIVER_COMPLETED_BY_ITEM"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 94
    const-string v1, "BUNDLE_PARCELABLE_ITEM"

    invoke-virtual {v8, v1}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v12

    .line 95
    .restart local v12    # "itemBundle":Landroid/os/Bundle;
    iget-object v1, p0, Lcom/samsung/android/allshare/ReceiverImpl$1;->this$0:Lcom/samsung/android/allshare/ReceiverImpl;

    invoke-virtual {v1, v12}, Lcom/samsung/android/allshare/ReceiverImpl;->getItem(Landroid/os/Bundle;)Lcom/samsung/android/allshare/Item;

    move-result-object v6

    .line 97
    .restart local v6    # "item":Lcom/samsung/android/allshare/Item;
    :try_start_1
    iget-object v1, p0, Lcom/samsung/android/allshare/ReceiverImpl$1;->this$0:Lcom/samsung/android/allshare/ReceiverImpl;

    # getter for: Lcom/samsung/android/allshare/ReceiverImpl;->mProgressUpdateListener:Lcom/samsung/android/allshare/media/Receiver$IProgressUpdateEventListener;
    invoke-static {v1}, Lcom/samsung/android/allshare/ReceiverImpl;->access$000(Lcom/samsung/android/allshare/ReceiverImpl;)Lcom/samsung/android/allshare/media/Receiver$IProgressUpdateEventListener;

    move-result-object v1

    invoke-interface {v1, v6, v7}, Lcom/samsung/android/allshare/media/Receiver$IProgressUpdateEventListener;->onCompleted(Lcom/samsung/android/allshare/Item;Lcom/samsung/android/allshare/ERROR;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/Error; {:try_start_1 .. :try_end_1} :catch_3

    goto :goto_0

    .line 98
    :catch_2
    move-exception v9

    .line 99
    .restart local v9    # "e":Ljava/lang/Exception;
    const-string v1, "ReceiverImpl"

    const-string v13, "mEventHandler(EVENT_RECEIVER_COMPLETED_BY_ITEM) Exception "

    invoke-static {v1, v13, v9}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0

    .line 101
    .end local v9    # "e":Ljava/lang/Exception;
    :catch_3
    move-exception v10

    .line 102
    .restart local v10    # "err":Ljava/lang/Error;
    const-string v1, "ReceiverImpl"

    const-string v13, "mEventHandler(EVENT_RECEIVER_COMPLETED_BY_ITEM) Error "

    invoke-static {v1, v13, v10}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Error;)V

    goto :goto_0
.end method
