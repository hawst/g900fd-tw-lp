.class public Lcom/samsung/android/allshare/extension/impl/WM7SearchMediaGetter;
.super Ljava/lang/Object;
.source "WM7SearchMediaGetter.java"

# interfaces
.implements Lcom/samsung/android/allshare/extension/impl/IMediaGetter;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/allshare/extension/impl/WM7SearchMediaGetter$FlatProviderConnectionInfo;,
        Lcom/samsung/android/allshare/extension/impl/WM7SearchMediaGetter$SearchRequest;
    }
.end annotation


# static fields
.field private static final DEFAULT_REQUEST_SIZE:I = 0x32


# instance fields
.field private mConns:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/extension/impl/WM7SearchMediaGetter$FlatProviderConnectionInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mProvider:Lcom/samsung/android/allshare/media/Provider;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/allshare/extension/impl/WM7SearchMediaGetter;->mProvider:Lcom/samsung/android/allshare/media/Provider;

    .line 34
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/extension/impl/WM7SearchMediaGetter;->mConns:Ljava/util/ArrayList;

    .line 60
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/allshare/extension/impl/WM7SearchMediaGetter;)Lcom/samsung/android/allshare/media/Provider;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/extension/impl/WM7SearchMediaGetter;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/samsung/android/allshare/extension/impl/WM7SearchMediaGetter;->mProvider:Lcom/samsung/android/allshare/media/Provider;

    return-object v0
.end method

.method static synthetic access$002(Lcom/samsung/android/allshare/extension/impl/WM7SearchMediaGetter;Lcom/samsung/android/allshare/media/Provider;)Lcom/samsung/android/allshare/media/Provider;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/extension/impl/WM7SearchMediaGetter;
    .param p1, "x1"    # Lcom/samsung/android/allshare/media/Provider;

    .prologue
    .line 29
    iput-object p1, p0, Lcom/samsung/android/allshare/extension/impl/WM7SearchMediaGetter;->mProvider:Lcom/samsung/android/allshare/media/Provider;

    return-object p1
.end method

.method static synthetic access$200(Lcom/samsung/android/allshare/extension/impl/WM7SearchMediaGetter;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/extension/impl/WM7SearchMediaGetter;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/samsung/android/allshare/extension/impl/WM7SearchMediaGetter;->mConns:Ljava/util/ArrayList;

    return-object v0
.end method

.method private startFlatSearching(Lcom/samsung/android/allshare/media/Provider;Ljava/util/ArrayList;Lcom/samsung/android/allshare/extension/FlatProvider$IFlatProviderConnection;)V
    .locals 9
    .param p1, "provider"    # Lcom/samsung/android/allshare/media/Provider;
    .param p3, "callback"    # Lcom/samsung/android/allshare/extension/FlatProvider$IFlatProviderConnection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/android/allshare/media/Provider;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/Item$MediaType;",
            ">;",
            "Lcom/samsung/android/allshare/extension/FlatProvider$IFlatProviderConnection;",
            ")V"
        }
    .end annotation

    .prologue
    .local p2, "types":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/Item$MediaType;>;"
    const/4 v8, 0x0

    .line 164
    invoke-interface {p3}, Lcom/samsung/android/allshare/extension/FlatProvider$IFlatProviderConnection;->onStart()V

    .line 166
    iput-object p1, p0, Lcom/samsung/android/allshare/extension/impl/WM7SearchMediaGetter;->mProvider:Lcom/samsung/android/allshare/media/Provider;

    .line 168
    if-nez p1, :cond_1

    .line 169
    sget-object v2, Lcom/samsung/android/allshare/ERROR;->INVALID_ARGUMENT:Lcom/samsung/android/allshare/ERROR;

    .line 170
    .local v2, "err":Lcom/samsung/android/allshare/ERROR;
    invoke-interface {p3, v2}, Lcom/samsung/android/allshare/extension/FlatProvider$IFlatProviderConnection;->onError(Lcom/samsung/android/allshare/ERROR;)V

    .line 267
    .end local v2    # "err":Lcom/samsung/android/allshare/ERROR;
    :cond_0
    :goto_0
    return-void

    .line 174
    :cond_1
    if-eqz p2, :cond_2

    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-nez v6, :cond_3

    .line 175
    :cond_2
    sget-object v2, Lcom/samsung/android/allshare/ERROR;->INVALID_ARGUMENT:Lcom/samsung/android/allshare/ERROR;

    .line 176
    .restart local v2    # "err":Lcom/samsung/android/allshare/ERROR;
    invoke-interface {p3, v2}, Lcom/samsung/android/allshare/extension/FlatProvider$IFlatProviderConnection;->onError(Lcom/samsung/android/allshare/ERROR;)V

    goto :goto_0

    .line 180
    .end local v2    # "err":Lcom/samsung/android/allshare/ERROR;
    :cond_3
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-ge v3, v6, :cond_5

    .line 181
    invoke-virtual {p2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/android/allshare/Item$MediaType;

    .line 182
    .local v5, "type":Lcom/samsung/android/allshare/Item$MediaType;
    sget-object v6, Lcom/samsung/android/allshare/Item$MediaType;->ITEM_AUDIO:Lcom/samsung/android/allshare/Item$MediaType;

    if-eq v5, v6, :cond_4

    sget-object v6, Lcom/samsung/android/allshare/Item$MediaType;->ITEM_VIDEO:Lcom/samsung/android/allshare/Item$MediaType;

    if-eq v5, v6, :cond_4

    sget-object v6, Lcom/samsung/android/allshare/Item$MediaType;->ITEM_IMAGE:Lcom/samsung/android/allshare/Item$MediaType;

    if-eq v5, v6, :cond_4

    .line 183
    sget-object v2, Lcom/samsung/android/allshare/ERROR;->INVALID_ARGUMENT:Lcom/samsung/android/allshare/ERROR;

    .line 184
    .restart local v2    # "err":Lcom/samsung/android/allshare/ERROR;
    invoke-interface {p3, v2}, Lcom/samsung/android/allshare/extension/FlatProvider$IFlatProviderConnection;->onError(Lcom/samsung/android/allshare/ERROR;)V

    goto :goto_0

    .line 180
    .end local v2    # "err":Lcom/samsung/android/allshare/ERROR;
    :cond_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 189
    .end local v5    # "type":Lcom/samsung/android/allshare/Item$MediaType;
    :cond_5
    new-instance v1, Lcom/samsung/android/allshare/extension/impl/WM7SearchMediaGetter$FlatProviderConnectionInfo;

    invoke-direct {v1, p0, p3, p1}, Lcom/samsung/android/allshare/extension/impl/WM7SearchMediaGetter$FlatProviderConnectionInfo;-><init>(Lcom/samsung/android/allshare/extension/impl/WM7SearchMediaGetter;Lcom/samsung/android/allshare/extension/FlatProvider$IFlatProviderConnection;Lcom/samsung/android/allshare/media/Provider;)V

    .line 190
    .local v1, "conn":Lcom/samsung/android/allshare/extension/impl/WM7SearchMediaGetter$FlatProviderConnectionInfo;
    iget-object v6, p0, Lcom/samsung/android/allshare/extension/impl/WM7SearchMediaGetter;->mConns:Ljava/util/ArrayList;

    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 193
    invoke-virtual {v1, p2, v8}, Lcom/samsung/android/allshare/extension/impl/WM7SearchMediaGetter$FlatProviderConnectionInfo;->addCurrentSearchRequest(Ljava/util/ArrayList;I)V

    .line 195
    new-instance v6, Lcom/samsung/android/allshare/extension/impl/WM7SearchMediaGetter$1;

    invoke-direct {v6, p0}, Lcom/samsung/android/allshare/extension/impl/WM7SearchMediaGetter$1;-><init>(Lcom/samsung/android/allshare/extension/impl/WM7SearchMediaGetter;)V

    invoke-virtual {p1, v6}, Lcom/samsung/android/allshare/media/Provider;->setSearchResponseListener(Lcom/samsung/android/allshare/media/Provider$IProviderSearchResponseListener;)V

    .line 262
    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/android/allshare/Item$MediaType;

    .line 263
    .restart local v5    # "type":Lcom/samsung/android/allshare/Item$MediaType;
    new-instance v0, Lcom/samsung/android/allshare/media/SearchCriteria$Builder;

    invoke-direct {v0}, Lcom/samsung/android/allshare/media/SearchCriteria$Builder;-><init>()V

    .line 264
    .local v0, "builder":Lcom/samsung/android/allshare/media/SearchCriteria$Builder;
    invoke-virtual {v0, v5}, Lcom/samsung/android/allshare/media/SearchCriteria$Builder;->addItemType(Lcom/samsung/android/allshare/Item$MediaType;)Lcom/samsung/android/allshare/media/SearchCriteria$Builder;

    .line 265
    invoke-virtual {v0}, Lcom/samsung/android/allshare/media/SearchCriteria$Builder;->build()Lcom/samsung/android/allshare/media/SearchCriteria;

    move-result-object v6

    const/16 v7, 0x32

    invoke-virtual {p1, v6, v8, v7}, Lcom/samsung/android/allshare/media/Provider;->search(Lcom/samsung/android/allshare/media/SearchCriteria;II)V

    goto :goto_2
.end method


# virtual methods
.method public cancel(Lcom/samsung/android/allshare/extension/FlatProvider$IFlatProviderConnection;)V
    .locals 3
    .param p1, "connection"    # Lcom/samsung/android/allshare/extension/FlatProvider$IFlatProviderConnection;

    .prologue
    .line 137
    iget-object v2, p0, Lcom/samsung/android/allshare/extension/impl/WM7SearchMediaGetter;->mConns:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 138
    .local v1, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/allshare/extension/impl/WM7SearchMediaGetter$FlatProviderConnectionInfo;>;"
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 139
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/allshare/extension/impl/WM7SearchMediaGetter$FlatProviderConnectionInfo;

    .line 140
    .local v0, "conn":Lcom/samsung/android/allshare/extension/impl/WM7SearchMediaGetter$FlatProviderConnectionInfo;
    # getter for: Lcom/samsung/android/allshare/extension/impl/WM7SearchMediaGetter$FlatProviderConnectionInfo;->mConn:Lcom/samsung/android/allshare/extension/FlatProvider$IFlatProviderConnection;
    invoke-static {v0}, Lcom/samsung/android/allshare/extension/impl/WM7SearchMediaGetter$FlatProviderConnectionInfo;->access$100(Lcom/samsung/android/allshare/extension/impl/WM7SearchMediaGetter$FlatProviderConnectionInfo;)Lcom/samsung/android/allshare/extension/FlatProvider$IFlatProviderConnection;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 141
    invoke-virtual {v0}, Lcom/samsung/android/allshare/extension/impl/WM7SearchMediaGetter$FlatProviderConnectionInfo;->setCancel()V

    .line 146
    .end local v0    # "conn":Lcom/samsung/android/allshare/extension/impl/WM7SearchMediaGetter$FlatProviderConnectionInfo;
    :cond_1
    invoke-interface {p1}, Lcom/samsung/android/allshare/extension/FlatProvider$IFlatProviderConnection;->onCancel()V

    .line 147
    return-void
.end method

.method public start(Lcom/samsung/android/allshare/media/Provider;Lcom/samsung/android/allshare/Item$MediaType;Lcom/samsung/android/allshare/extension/FlatProvider$IFlatProviderConnection;)V
    .locals 1
    .param p1, "provider"    # Lcom/samsung/android/allshare/media/Provider;
    .param p2, "type"    # Lcom/samsung/android/allshare/Item$MediaType;
    .param p3, "callback"    # Lcom/samsung/android/allshare/extension/FlatProvider$IFlatProviderConnection;

    .prologue
    .line 157
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 158
    .local v0, "types":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/Item$MediaType;>;"
    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 159
    invoke-direct {p0, p1, v0, p3}, Lcom/samsung/android/allshare/extension/impl/WM7SearchMediaGetter;->startFlatSearching(Lcom/samsung/android/allshare/media/Provider;Ljava/util/ArrayList;Lcom/samsung/android/allshare/extension/FlatProvider$IFlatProviderConnection;)V

    .line 160
    return-void
.end method

.method public start(Lcom/samsung/android/allshare/media/Provider;Ljava/util/ArrayList;Lcom/samsung/android/allshare/extension/FlatProvider$IFlatProviderConnection;)V
    .locals 0
    .param p1, "provider"    # Lcom/samsung/android/allshare/media/Provider;
    .param p3, "callback"    # Lcom/samsung/android/allshare/extension/FlatProvider$IFlatProviderConnection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/android/allshare/media/Provider;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/Item$MediaType;",
            ">;",
            "Lcom/samsung/android/allshare/extension/FlatProvider$IFlatProviderConnection;",
            ")V"
        }
    .end annotation

    .prologue
    .line 152
    .local p2, "types":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/Item$MediaType;>;"
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/allshare/extension/impl/WM7SearchMediaGetter;->startFlatSearching(Lcom/samsung/android/allshare/media/Provider;Ljava/util/ArrayList;Lcom/samsung/android/allshare/extension/FlatProvider$IFlatProviderConnection;)V

    .line 153
    return-void
.end method
