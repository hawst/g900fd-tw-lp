.class Lcom/samsung/android/allshare/extension/SECAVPlayer$4;
.super Ljava/lang/Object;
.source "SECAVPlayer.java"

# interfaces
.implements Lcom/samsung/android/allshare/media/AVPlayer$IAVPlayerVolumeResponseListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/allshare/extension/SECAVPlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/allshare/extension/SECAVPlayer;


# direct methods
.method constructor <init>(Lcom/samsung/android/allshare/extension/SECAVPlayer;)V
    .locals 0

    .prologue
    .line 1663
    iput-object p1, p0, Lcom/samsung/android/allshare/extension/SECAVPlayer$4;->this$0:Lcom/samsung/android/allshare/extension/SECAVPlayer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGetMuteResponseReceived(ZLcom/samsung/android/allshare/ERROR;)V
    .locals 4
    .param p1, "ret"    # Z
    .param p2, "error"    # Lcom/samsung/android/allshare/ERROR;

    .prologue
    const/4 v1, 0x0

    .line 1692
    iget-object v0, p0, Lcom/samsung/android/allshare/extension/SECAVPlayer$4;->this$0:Lcom/samsung/android/allshare/extension/SECAVPlayer;

    iput-boolean v1, v0, Lcom/samsung/android/allshare/extension/SECAVPlayer;->mRequestChangeMute:Z

    .line 1693
    sget-object v0, Lcom/samsung/android/allshare/ERROR;->SUCCESS:Lcom/samsung/android/allshare/ERROR;

    if-ne p2, v0, :cond_3

    .line 1694
    const-string v0, "SECAVPLAYER"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " onGetMuteResponseReceived - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/samsung/android/allshare/DLog;->i_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 1695
    iget-object v0, p0, Lcom/samsung/android/allshare/extension/SECAVPlayer$4;->this$0:Lcom/samsung/android/allshare/extension/SECAVPlayer;

    iget-boolean v0, v0, Lcom/samsung/android/allshare/extension/SECAVPlayer;->mChangeMute:Z

    if-eqz v0, :cond_0

    .line 1696
    iget-object v0, p0, Lcom/samsung/android/allshare/extension/SECAVPlayer$4;->this$0:Lcom/samsung/android/allshare/extension/SECAVPlayer;

    # getter for: Lcom/samsung/android/allshare/extension/SECAVPlayer;->mAVPlayer:Lcom/samsung/android/allshare/media/AVPlayer;
    invoke-static {v0}, Lcom/samsung/android/allshare/extension/SECAVPlayer;->access$900(Lcom/samsung/android/allshare/extension/SECAVPlayer;)Lcom/samsung/android/allshare/media/AVPlayer;

    move-result-object v2

    if-nez p1, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v2, v0}, Lcom/samsung/android/allshare/media/AVPlayer;->setMute(Z)V

    .line 1701
    :cond_0
    :goto_1
    iget-object v0, p0, Lcom/samsung/android/allshare/extension/SECAVPlayer$4;->this$0:Lcom/samsung/android/allshare/extension/SECAVPlayer;

    iput-boolean v1, v0, Lcom/samsung/android/allshare/extension/SECAVPlayer;->mChangeMute:Z

    .line 1702
    iget-object v0, p0, Lcom/samsung/android/allshare/extension/SECAVPlayer$4;->this$0:Lcom/samsung/android/allshare/extension/SECAVPlayer;

    # getter for: Lcom/samsung/android/allshare/extension/SECAVPlayer;->mAVPlayerVolumeResponseListener:Lcom/samsung/android/allshare/media/AVPlayer$IAVPlayerVolumeResponseListener;
    invoke-static {v0}, Lcom/samsung/android/allshare/extension/SECAVPlayer;->access$1900(Lcom/samsung/android/allshare/extension/SECAVPlayer;)Lcom/samsung/android/allshare/media/AVPlayer$IAVPlayerVolumeResponseListener;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1703
    iget-object v0, p0, Lcom/samsung/android/allshare/extension/SECAVPlayer$4;->this$0:Lcom/samsung/android/allshare/extension/SECAVPlayer;

    # getter for: Lcom/samsung/android/allshare/extension/SECAVPlayer;->mAVPlayerVolumeResponseListener:Lcom/samsung/android/allshare/media/AVPlayer$IAVPlayerVolumeResponseListener;
    invoke-static {v0}, Lcom/samsung/android/allshare/extension/SECAVPlayer;->access$1900(Lcom/samsung/android/allshare/extension/SECAVPlayer;)Lcom/samsung/android/allshare/media/AVPlayer$IAVPlayerVolumeResponseListener;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/samsung/android/allshare/media/AVPlayer$IAVPlayerVolumeResponseListener;->onGetMuteResponseReceived(ZLcom/samsung/android/allshare/ERROR;)V

    .line 1704
    :cond_1
    return-void

    :cond_2
    move v0, v1

    .line 1696
    goto :goto_0

    .line 1699
    :cond_3
    const-string v0, "SECAVPLAYER"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " onGetMuteResponseReceived - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/samsung/android/allshare/DLog;->i_api(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public onGetVolumeResponseReceived(ILcom/samsung/android/allshare/ERROR;)V
    .locals 3
    .param p1, "volume"    # I
    .param p2, "error"    # Lcom/samsung/android/allshare/ERROR;

    .prologue
    const/4 v1, 0x0

    .line 1666
    iget-object v0, p0, Lcom/samsung/android/allshare/extension/SECAVPlayer$4;->this$0:Lcom/samsung/android/allshare/extension/SECAVPlayer;

    iput-boolean v1, v0, Lcom/samsung/android/allshare/extension/SECAVPlayer;->mRequestVolume:Z

    .line 1667
    sget-object v0, Lcom/samsung/android/allshare/ERROR;->SUCCESS:Lcom/samsung/android/allshare/ERROR;

    if-ne p2, v0, :cond_1

    .line 1668
    const-string v0, "SECAVPLAYER"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " onGetVolumeResponseReceived - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/allshare/DLog;->i_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 1669
    iget-object v0, p0, Lcom/samsung/android/allshare/extension/SECAVPlayer$4;->this$0:Lcom/samsung/android/allshare/extension/SECAVPlayer;

    # invokes: Lcom/samsung/android/allshare/extension/SECAVPlayer;->setVolumeDelta(I)V
    invoke-static {v0, p1}, Lcom/samsung/android/allshare/extension/SECAVPlayer;->access$1700(Lcom/samsung/android/allshare/extension/SECAVPlayer;I)V

    .line 1675
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/allshare/extension/SECAVPlayer$4;->this$0:Lcom/samsung/android/allshare/extension/SECAVPlayer;

    # getter for: Lcom/samsung/android/allshare/extension/SECAVPlayer;->mAVPlayerVolumeResponseListener:Lcom/samsung/android/allshare/media/AVPlayer$IAVPlayerVolumeResponseListener;
    invoke-static {v0}, Lcom/samsung/android/allshare/extension/SECAVPlayer;->access$1900(Lcom/samsung/android/allshare/extension/SECAVPlayer;)Lcom/samsung/android/allshare/media/AVPlayer$IAVPlayerVolumeResponseListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1676
    iget-object v0, p0, Lcom/samsung/android/allshare/extension/SECAVPlayer$4;->this$0:Lcom/samsung/android/allshare/extension/SECAVPlayer;

    # getter for: Lcom/samsung/android/allshare/extension/SECAVPlayer;->mAVPlayerVolumeResponseListener:Lcom/samsung/android/allshare/media/AVPlayer$IAVPlayerVolumeResponseListener;
    invoke-static {v0}, Lcom/samsung/android/allshare/extension/SECAVPlayer;->access$1900(Lcom/samsung/android/allshare/extension/SECAVPlayer;)Lcom/samsung/android/allshare/media/AVPlayer$IAVPlayerVolumeResponseListener;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/samsung/android/allshare/media/AVPlayer$IAVPlayerVolumeResponseListener;->onGetVolumeResponseReceived(ILcom/samsung/android/allshare/ERROR;)V

    .line 1677
    :cond_0
    return-void

    .line 1671
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/allshare/extension/SECAVPlayer$4;->this$0:Lcom/samsung/android/allshare/extension/SECAVPlayer;

    # setter for: Lcom/samsung/android/allshare/extension/SECAVPlayer;->mVolumeDelta:I
    invoke-static {v0, v1}, Lcom/samsung/android/allshare/extension/SECAVPlayer;->access$1802(Lcom/samsung/android/allshare/extension/SECAVPlayer;I)I

    .line 1672
    const-string v0, "SECAVPLAYER"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " onGetVolumeResponseReceived - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/allshare/DLog;->i_api(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onSetMuteResponseReceived(ZLcom/samsung/android/allshare/ERROR;)V
    .locals 3
    .param p1, "ret"    # Z
    .param p2, "error"    # Lcom/samsung/android/allshare/ERROR;

    .prologue
    .line 1708
    sget-object v0, Lcom/samsung/android/allshare/ERROR;->SUCCESS:Lcom/samsung/android/allshare/ERROR;

    if-ne p2, v0, :cond_1

    .line 1709
    const-string v0, "SECAVPLAYER"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " onSetMuteResponseReceived - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/allshare/DLog;->i_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 1713
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/allshare/extension/SECAVPlayer$4;->this$0:Lcom/samsung/android/allshare/extension/SECAVPlayer;

    # getter for: Lcom/samsung/android/allshare/extension/SECAVPlayer;->mAVPlayerVolumeResponseListener:Lcom/samsung/android/allshare/media/AVPlayer$IAVPlayerVolumeResponseListener;
    invoke-static {v0}, Lcom/samsung/android/allshare/extension/SECAVPlayer;->access$1900(Lcom/samsung/android/allshare/extension/SECAVPlayer;)Lcom/samsung/android/allshare/media/AVPlayer$IAVPlayerVolumeResponseListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1714
    iget-object v0, p0, Lcom/samsung/android/allshare/extension/SECAVPlayer$4;->this$0:Lcom/samsung/android/allshare/extension/SECAVPlayer;

    # getter for: Lcom/samsung/android/allshare/extension/SECAVPlayer;->mAVPlayerVolumeResponseListener:Lcom/samsung/android/allshare/media/AVPlayer$IAVPlayerVolumeResponseListener;
    invoke-static {v0}, Lcom/samsung/android/allshare/extension/SECAVPlayer;->access$1900(Lcom/samsung/android/allshare/extension/SECAVPlayer;)Lcom/samsung/android/allshare/media/AVPlayer$IAVPlayerVolumeResponseListener;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/samsung/android/allshare/media/AVPlayer$IAVPlayerVolumeResponseListener;->onSetMuteResponseReceived(ZLcom/samsung/android/allshare/ERROR;)V

    .line 1715
    :cond_0
    return-void

    .line 1711
    :cond_1
    const-string v0, "SECAVPLAYER"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " onSetMuteResponseReceived - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/allshare/DLog;->i_api(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onSetVolumeResponseReceived(ILcom/samsung/android/allshare/ERROR;)V
    .locals 3
    .param p1, "volume"    # I
    .param p2, "error"    # Lcom/samsung/android/allshare/ERROR;

    .prologue
    .line 1681
    sget-object v0, Lcom/samsung/android/allshare/ERROR;->SUCCESS:Lcom/samsung/android/allshare/ERROR;

    if-ne p2, v0, :cond_1

    .line 1682
    const-string v0, "SECAVPLAYER"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " onSetVolumeResponseReceived - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/allshare/DLog;->i_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 1686
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/allshare/extension/SECAVPlayer$4;->this$0:Lcom/samsung/android/allshare/extension/SECAVPlayer;

    # getter for: Lcom/samsung/android/allshare/extension/SECAVPlayer;->mAVPlayerVolumeResponseListener:Lcom/samsung/android/allshare/media/AVPlayer$IAVPlayerVolumeResponseListener;
    invoke-static {v0}, Lcom/samsung/android/allshare/extension/SECAVPlayer;->access$1900(Lcom/samsung/android/allshare/extension/SECAVPlayer;)Lcom/samsung/android/allshare/media/AVPlayer$IAVPlayerVolumeResponseListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1687
    iget-object v0, p0, Lcom/samsung/android/allshare/extension/SECAVPlayer$4;->this$0:Lcom/samsung/android/allshare/extension/SECAVPlayer;

    # getter for: Lcom/samsung/android/allshare/extension/SECAVPlayer;->mAVPlayerVolumeResponseListener:Lcom/samsung/android/allshare/media/AVPlayer$IAVPlayerVolumeResponseListener;
    invoke-static {v0}, Lcom/samsung/android/allshare/extension/SECAVPlayer;->access$1900(Lcom/samsung/android/allshare/extension/SECAVPlayer;)Lcom/samsung/android/allshare/media/AVPlayer$IAVPlayerVolumeResponseListener;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/samsung/android/allshare/media/AVPlayer$IAVPlayerVolumeResponseListener;->onSetVolumeResponseReceived(ILcom/samsung/android/allshare/ERROR;)V

    .line 1688
    :cond_0
    return-void

    .line 1684
    :cond_1
    const-string v0, "SECAVPLAYER"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " onSetVolumeResponseReceived - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/allshare/DLog;->i_api(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
