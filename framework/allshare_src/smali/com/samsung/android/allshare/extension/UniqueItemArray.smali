.class public Lcom/samsung/android/allshare/extension/UniqueItemArray;
.super Ljava/util/ArrayList;
.source "UniqueItemArray.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/ArrayList",
        "<",
        "Lcom/samsung/android/allshare/Item;",
        ">;"
    }
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private mAccessLock:Ljava/lang/Object;

.field private mCurrentItems:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/samsung/android/allshare/Item;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/util/ArrayList;-><init>()V

    .line 33
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/extension/UniqueItemArray;->mCurrentItems:Ljava/util/HashMap;

    .line 35
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/extension/UniqueItemArray;->mAccessLock:Ljava/lang/Object;

    return-void
.end method

.method private generateKey(Lcom/samsung/android/allshare/Item;)Ljava/lang/String;
    .locals 4
    .param p1, "item"    # Lcom/samsung/android/allshare/Item;

    .prologue
    .line 155
    invoke-virtual {p1}, Lcom/samsung/android/allshare/Item;->getURI()Landroid/net/Uri;

    move-result-object v1

    .line 156
    .local v1, "uri":Landroid/net/Uri;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 157
    .local v0, "sb":Ljava/lang/StringBuilder;
    invoke-virtual {p1}, Lcom/samsung/android/allshare/Item;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/samsung/android/allshare/Item;->getType()Lcom/samsung/android/allshare/Item$MediaType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/allshare/Item$MediaType;->enumToString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 158
    if-eqz v1, :cond_0

    .line 159
    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 160
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method private isItemContained(Lcom/samsung/android/allshare/Item;)Z
    .locals 4
    .param p1, "item"    # Lcom/samsung/android/allshare/Item;

    .prologue
    .line 170
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/extension/UniqueItemArray;->generateKey(Lcom/samsung/android/allshare/Item;)Ljava/lang/String;

    move-result-object v0

    .line 172
    .local v0, "key":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/android/allshare/extension/UniqueItemArray;->mCurrentItems:Ljava/util/HashMap;

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 173
    iget-object v2, p0, Lcom/samsung/android/allshare/extension/UniqueItemArray;->mCurrentItems:Ljava/util/HashMap;

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/ref/WeakReference;

    .line 174
    .local v1, "ref":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lcom/samsung/android/allshare/Item;>;"
    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 175
    const/4 v2, 0x1

    .line 181
    .end local v1    # "ref":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lcom/samsung/android/allshare/Item;>;"
    :goto_0
    return v2

    .line 179
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/allshare/extension/UniqueItemArray;->mCurrentItems:Ljava/util/HashMap;

    new-instance v3, Ljava/lang/ref/WeakReference;

    invoke-direct {v3, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v2, v0, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 181
    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method


# virtual methods
.method public add(ILcom/samsung/android/allshare/Item;)V
    .locals 2
    .param p1, "index"    # I
    .param p2, "object"    # Lcom/samsung/android/allshare/Item;

    .prologue
    .line 49
    if-nez p2, :cond_0

    .line 57
    :goto_0
    return-void

    .line 53
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/allshare/extension/UniqueItemArray;->mAccessLock:Ljava/lang/Object;

    monitor-enter v1

    .line 54
    :try_start_0
    invoke-direct {p0, p2}, Lcom/samsung/android/allshare/extension/UniqueItemArray;->isItemContained(Lcom/samsung/android/allshare/Item;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 55
    invoke-super {p0, p1, p2}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 56
    :cond_1
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public bridge synthetic add(ILjava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # I
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 30
    check-cast p2, Lcom/samsung/android/allshare/Item;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/samsung/android/allshare/extension/UniqueItemArray;->add(ILcom/samsung/android/allshare/Item;)V

    return-void
.end method

.method public add(Lcom/samsung/android/allshare/Item;)Z
    .locals 3
    .param p1, "object"    # Lcom/samsung/android/allshare/Item;

    .prologue
    const/4 v0, 0x0

    .line 67
    if-nez p1, :cond_0

    .line 76
    :goto_0
    return v0

    .line 71
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/allshare/extension/UniqueItemArray;->mAccessLock:Ljava/lang/Object;

    monitor-enter v1

    .line 72
    :try_start_0
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/extension/UniqueItemArray;->isItemContained(Lcom/samsung/android/allshare/Item;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 73
    invoke-super {p0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-result v0

    monitor-exit v1

    goto :goto_0

    .line 74
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public bridge synthetic add(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 30
    check-cast p1, Lcom/samsung/android/allshare/Item;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/samsung/android/allshare/extension/UniqueItemArray;->add(Lcom/samsung/android/allshare/Item;)Z

    move-result v0

    return v0
.end method

.method public addAll(ILjava/util/Collection;)Z
    .locals 4
    .param p1, "index"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/Collection",
            "<+",
            "Lcom/samsung/android/allshare/Item;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .local p2, "collection":Ljava/util/Collection;, "Ljava/util/Collection<+Lcom/samsung/android/allshare/Item;>;"
    const/4 v2, 0x1

    .line 118
    if-nez p2, :cond_1

    .line 119
    const/4 v2, 0x0

    .line 132
    :cond_0
    :goto_0
    return v2

    .line 121
    :cond_1
    invoke-interface {p2}, Ljava/util/Collection;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 126
    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 127
    .local v0, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<+Lcom/samsung/android/allshare/Item;>;"
    :cond_2
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 128
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/allshare/Item;

    .line 129
    .local v1, "item":Lcom/samsung/android/allshare/Item;
    invoke-direct {p0, v1}, Lcom/samsung/android/allshare/extension/UniqueItemArray;->isItemContained(Lcom/samsung/android/allshare/Item;)Z

    move-result v3

    if-ne v3, v2, :cond_2

    .line 130
    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    goto :goto_1

    .line 132
    .end local v1    # "item":Lcom/samsung/android/allshare/Item;
    :cond_3
    invoke-super {p0, p1, p2}, Ljava/util/ArrayList;->addAll(ILjava/util/Collection;)Z

    move-result v2

    goto :goto_0
.end method

.method public addAll(Ljava/util/Collection;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<+",
            "Lcom/samsung/android/allshare/Item;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .local p1, "collection":Ljava/util/Collection;, "Ljava/util/Collection<+Lcom/samsung/android/allshare/Item;>;"
    const/4 v2, 0x1

    .line 88
    if-nez p1, :cond_1

    .line 89
    const/4 v2, 0x0

    .line 104
    :cond_0
    :goto_0
    return v2

    .line 91
    :cond_1
    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 96
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 97
    .local v0, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<+Lcom/samsung/android/allshare/Item;>;"
    :cond_2
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 98
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/allshare/Item;

    .line 99
    .local v1, "item":Lcom/samsung/android/allshare/Item;
    invoke-direct {p0, v1}, Lcom/samsung/android/allshare/extension/UniqueItemArray;->isItemContained(Lcom/samsung/android/allshare/Item;)Z

    move-result v3

    if-ne v3, v2, :cond_2

    .line 100
    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    goto :goto_1

    .line 104
    .end local v1    # "item":Lcom/samsung/android/allshare/Item;
    :cond_3
    invoke-super {p0, p1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    move-result v2

    goto :goto_0
.end method

.method public clear()V
    .locals 2

    .prologue
    .line 142
    iget-object v1, p0, Lcom/samsung/android/allshare/extension/UniqueItemArray;->mAccessLock:Ljava/lang/Object;

    monitor-enter v1

    .line 143
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/allshare/extension/UniqueItemArray;->mCurrentItems:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 144
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 145
    invoke-super {p0}, Ljava/util/ArrayList;->clear()V

    .line 146
    return-void

    .line 144
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
