.class public Lcom/samsung/android/allshare/extension/impl/BrowseMediaGetter;
.super Ljava/lang/Object;
.source "BrowseMediaGetter.java"

# interfaces
.implements Lcom/samsung/android/allshare/extension/impl/IMediaGetter;
.implements Lcom/samsung/android/allshare/media/Provider$IProviderBrowseResponseListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/allshare/extension/impl/BrowseMediaGetter$FlatProviderConnectionInfo;
    }
.end annotation


# static fields
.field private static final DEFAULT_BROWSE_REQUEST_SIZE:I = 0x2

.field private static final DEFAULT_REQUEST_SIZE:I = 0x32

.field private static final TAG_CLASS:Ljava/lang/String; = "BrowseMediaGetter"


# instance fields
.field private mConns:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/extension/impl/BrowseMediaGetter$FlatProviderConnectionInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/extension/impl/BrowseMediaGetter;->mConns:Ljava/util/ArrayList;

    .line 44
    return-void
.end method


# virtual methods
.method public cancel(Lcom/samsung/android/allshare/extension/FlatProvider$IFlatProviderConnection;)V
    .locals 3
    .param p1, "connection"    # Lcom/samsung/android/allshare/extension/FlatProvider$IFlatProviderConnection;

    .prologue
    .line 156
    iget-object v2, p0, Lcom/samsung/android/allshare/extension/impl/BrowseMediaGetter;->mConns:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 157
    .local v1, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/allshare/extension/impl/BrowseMediaGetter$FlatProviderConnectionInfo;>;"
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 158
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/allshare/extension/impl/BrowseMediaGetter$FlatProviderConnectionInfo;

    .line 159
    .local v0, "conn":Lcom/samsung/android/allshare/extension/impl/BrowseMediaGetter$FlatProviderConnectionInfo;
    # getter for: Lcom/samsung/android/allshare/extension/impl/BrowseMediaGetter$FlatProviderConnectionInfo;->mConn:Lcom/samsung/android/allshare/extension/FlatProvider$IFlatProviderConnection;
    invoke-static {v0}, Lcom/samsung/android/allshare/extension/impl/BrowseMediaGetter$FlatProviderConnectionInfo;->access$000(Lcom/samsung/android/allshare/extension/impl/BrowseMediaGetter$FlatProviderConnectionInfo;)Lcom/samsung/android/allshare/extension/FlatProvider$IFlatProviderConnection;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 160
    invoke-virtual {v0}, Lcom/samsung/android/allshare/extension/impl/BrowseMediaGetter$FlatProviderConnectionInfo;->setCancel()V

    .line 164
    .end local v0    # "conn":Lcom/samsung/android/allshare/extension/impl/BrowseMediaGetter$FlatProviderConnectionInfo;
    :cond_1
    invoke-interface {p1}, Lcom/samsung/android/allshare/extension/FlatProvider$IFlatProviderConnection;->onCancel()V

    .line 165
    return-void
.end method

.method public onBrowseResponseReceived(Ljava/util/ArrayList;IILcom/samsung/android/allshare/Item;ZLcom/samsung/android/allshare/ERROR;)V
    .locals 32
    .param p2, "requestedStartIndex"    # I
    .param p3, "requestedCount"    # I
    .param p4, "requestedFolderItem"    # Lcom/samsung/android/allshare/Item;
    .param p5, "endOfItems"    # Z
    .param p6, "err"    # Lcom/samsung/android/allshare/ERROR;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/Item;",
            ">;II",
            "Lcom/samsung/android/allshare/Item;",
            "Z",
            "Lcom/samsung/android/allshare/ERROR;",
            ")V"
        }
    .end annotation

    .prologue
    .line 200
    .local p1, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/Item;>;"
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->size()I

    move-result v19

    .line 202
    .local v19, "returnedCount":I
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v15

    .line 203
    .local v15, "itemIt":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/allshare/Item;>;"
    :cond_0
    :goto_0
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v29

    if-eqz v29, :cond_1

    .line 204
    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lcom/samsung/android/allshare/Item;

    .line 205
    .local v17, "obj":Lcom/samsung/android/allshare/Item;
    invoke-virtual/range {v17 .. v17}, Lcom/samsung/android/allshare/Item;->getType()Lcom/samsung/android/allshare/Item$MediaType;

    move-result-object v29

    sget-object v30, Lcom/samsung/android/allshare/Item$MediaType;->ITEM_UNKNOWN:Lcom/samsung/android/allshare/Item$MediaType;

    move-object/from16 v0, v29

    move-object/from16 v1, v30

    if-ne v0, v1, :cond_0

    .line 206
    invoke-interface {v15}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 210
    .end local v17    # "obj":Lcom/samsung/android/allshare/Item;
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/extension/impl/BrowseMediaGetter;->mConns:Ljava/util/ArrayList;

    move-object/from16 v29, v0

    invoke-virtual/range {v29 .. v29}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v14

    .line 212
    .local v14, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/allshare/extension/impl/BrowseMediaGetter$FlatProviderConnectionInfo;>;"
    if-nez p4, :cond_4

    .line 213
    const-class v29, Lcom/samsung/android/allshare/extension/impl/BrowseMediaGetter;

    invoke-virtual/range {v29 .. v29}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v29

    const-string v30, "onBrowseResponseReceived for requestedFolderItem == null"

    invoke-static/range {v29 .. v30}, Lcom/samsung/android/allshare/DLog;->d_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 220
    :cond_2
    :goto_1
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v29

    if-eqz v29, :cond_11

    .line 221
    new-instance v18, Ljava/util/ArrayList;

    invoke-direct/range {v18 .. v18}, Ljava/util/ArrayList;-><init>()V

    .line 222
    .local v18, "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/Item;>;"
    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/android/allshare/extension/impl/BrowseMediaGetter$FlatProviderConnectionInfo;

    .line 224
    .local v6, "conn":Lcom/samsung/android/allshare/extension/impl/BrowseMediaGetter$FlatProviderConnectionInfo;
    move-object/from16 v0, p4

    move/from16 v1, p2

    invoke-virtual {v6, v0, v1}, Lcom/samsung/android/allshare/extension/impl/BrowseMediaGetter$FlatProviderConnectionInfo;->isCurrentBrowseRequest(Lcom/samsung/android/allshare/Item;I)Z

    move-result v29

    if-eqz v29, :cond_2

    .line 226
    sget-object v29, Lcom/samsung/android/allshare/ERROR;->SUCCESS:Lcom/samsung/android/allshare/ERROR;

    move-object/from16 v0, p6

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/ERROR;->compareTo(Ljava/lang/Enum;)I

    move-result v29

    if-eqz v29, :cond_7

    .line 227
    const-class v29, Lcom/samsung/android/allshare/extension/impl/BrowseMediaGetter;

    invoke-virtual/range {v29 .. v29}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v29

    new-instance v30, Ljava/lang/StringBuilder;

    invoke-direct/range {v30 .. v30}, Ljava/lang/StringBuilder;-><init>()V

    const-string v31, "browse failed for "

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {p4 .. p4}, Lcom/samsung/android/allshare/Item;->getTitle()Ljava/lang/String;

    move-result-object v31

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    invoke-static/range {v29 .. v30}, Lcom/samsung/android/allshare/DLog;->d_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 230
    sget-object v29, Lcom/samsung/android/allshare/ERROR;->FAIL:Lcom/samsung/android/allshare/ERROR;

    move-object/from16 v0, p6

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/ERROR;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_6

    .line 231
    move-object/from16 v0, p4

    invoke-virtual {v6, v0}, Lcom/samsung/android/allshare/extension/impl/BrowseMediaGetter$FlatProviderConnectionInfo;->isRetry(Lcom/samsung/android/allshare/Item;)Z

    move-result v29

    if-nez v29, :cond_5

    .line 232
    move-object/from16 v0, p4

    invoke-virtual {v6, v0}, Lcom/samsung/android/allshare/extension/impl/BrowseMediaGetter$FlatProviderConnectionInfo;->setRetry(Lcom/samsung/android/allshare/Item;)V

    .line 233
    # getter for: Lcom/samsung/android/allshare/extension/impl/BrowseMediaGetter$FlatProviderConnectionInfo;->mFailedBrowseFolderStack:Ljava/util/Stack;
    invoke-static {v6}, Lcom/samsung/android/allshare/extension/impl/BrowseMediaGetter$FlatProviderConnectionInfo;->access$200(Lcom/samsung/android/allshare/extension/impl/BrowseMediaGetter$FlatProviderConnectionInfo;)Ljava/util/Stack;

    move-result-object v29

    move-object/from16 v0, v29

    move-object/from16 v1, p4

    invoke-virtual {v0, v1}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 234
    # getter for: Lcom/samsung/android/allshare/extension/impl/BrowseMediaGetter$FlatProviderConnectionInfo;->mFailedBrowseFolderStartIndexMap:Ljava/util/HashMap;
    invoke-static {v6}, Lcom/samsung/android/allshare/extension/impl/BrowseMediaGetter$FlatProviderConnectionInfo;->access$300(Lcom/samsung/android/allshare/extension/impl/BrowseMediaGetter$FlatProviderConnectionInfo;)Ljava/util/HashMap;

    move-result-object v29

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v30

    move-object/from16 v0, v29

    move-object/from16 v1, p4

    move-object/from16 v2, v30

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 236
    const-class v29, Lcom/samsung/android/allshare/extension/impl/BrowseMediaGetter;

    invoke-virtual/range {v29 .. v29}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v29

    new-instance v30, Ljava/lang/StringBuilder;

    invoke-direct/range {v30 .. v30}, Ljava/lang/StringBuilder;-><init>()V

    const-string v31, "failed browse added "

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {p4 .. p4}, Lcom/samsung/android/allshare/Item;->getTitle()Ljava/lang/String;

    move-result-object v31

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    const-string v31, ", start "

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    move-object/from16 v0, v30

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    invoke-static/range {v29 .. v30}, Lcom/samsung/android/allshare/DLog;->d_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 299
    :cond_3
    :goto_2
    invoke-virtual {v6}, Lcom/samsung/android/allshare/extension/impl/BrowseMediaGetter$FlatProviderConnectionInfo;->isCanceled()Z

    move-result v29

    if-eqz v29, :cond_c

    .line 300
    const-class v29, Lcom/samsung/android/allshare/extension/impl/BrowseMediaGetter;

    invoke-virtual/range {v29 .. v29}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v29

    const-string v30, "conn is canceled.."

    invoke-static/range {v29 .. v30}, Lcom/samsung/android/allshare/DLog;->d_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 301
    invoke-interface {v14}, Ljava/util/Iterator;->remove()V

    goto/16 :goto_1

    .line 216
    .end local v6    # "conn":Lcom/samsung/android/allshare/extension/impl/BrowseMediaGetter$FlatProviderConnectionInfo;
    .end local v18    # "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/Item;>;"
    :cond_4
    const-class v29, Lcom/samsung/android/allshare/extension/impl/BrowseMediaGetter;

    invoke-virtual/range {v29 .. v29}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v29

    new-instance v30, Ljava/lang/StringBuilder;

    invoke-direct/range {v30 .. v30}, Ljava/lang/StringBuilder;-><init>()V

    const-string v31, "onBrowseResponseReceived for "

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {p4 .. p4}, Lcom/samsung/android/allshare/Item;->getTitle()Ljava/lang/String;

    move-result-object v31

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    const-string v31, ", start idx = "

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    move-object/from16 v0, v30

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v30

    const-string v31, ", req count "

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    move-object/from16 v0, v30

    move/from16 v1, p3

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v30

    const-string v31, ", endOfItems"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    move-object/from16 v0, v30

    move/from16 v1, p5

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    invoke-static/range {v29 .. v30}, Lcom/samsung/android/allshare/DLog;->d_api(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 240
    .restart local v6    # "conn":Lcom/samsung/android/allshare/extension/impl/BrowseMediaGetter$FlatProviderConnectionInfo;
    .restart local v18    # "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/Item;>;"
    :cond_5
    const-class v29, Lcom/samsung/android/allshare/extension/impl/BrowseMediaGetter;

    invoke-virtual/range {v29 .. v29}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v29

    const-string v30, "failed again, no more try"

    invoke-static/range {v29 .. v30}, Lcom/samsung/android/allshare/DLog;->d_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 242
    move-object/from16 v0, p4

    invoke-virtual {v6, v0}, Lcom/samsung/android/allshare/extension/impl/BrowseMediaGetter$FlatProviderConnectionInfo;->removeRetry(Lcom/samsung/android/allshare/Item;)V

    goto :goto_2

    .line 245
    :cond_6
    move-object/from16 v0, p6

    invoke-virtual {v6, v0}, Lcom/samsung/android/allshare/extension/impl/BrowseMediaGetter$FlatProviderConnectionInfo;->onError(Lcom/samsung/android/allshare/ERROR;)V

    goto/16 :goto_1

    .line 249
    :cond_7
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v13

    .local v13, "i$":Ljava/util/Iterator;
    :cond_8
    :goto_3
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v29

    if-eqz v29, :cond_3

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/samsung/android/allshare/Item;

    .line 250
    .local v12, "i":Lcom/samsung/android/allshare/Item;
    invoke-virtual {v6, v12}, Lcom/samsung/android/allshare/extension/impl/BrowseMediaGetter$FlatProviderConnectionInfo;->isMathchingItem(Lcom/samsung/android/allshare/Item;)Z

    move-result v29

    if-eqz v29, :cond_9

    .line 251
    invoke-virtual {v12}, Lcom/samsung/android/allshare/Item;->getTitle()Ljava/lang/String;

    move-result-object v24

    .line 252
    .local v24, "srcName":Ljava/lang/String;
    invoke-virtual {v12}, Lcom/samsung/android/allshare/Item;->getExtension()Ljava/lang/String;

    move-result-object v23

    .line 253
    .local v23, "srcExt":Ljava/lang/String;
    invoke-virtual {v12}, Lcom/samsung/android/allshare/Item;->getFileSize()J

    move-result-wide v26

    .line 254
    .local v26, "srcSize":J
    invoke-virtual {v12}, Lcom/samsung/android/allshare/Item;->getChannelNr()Ljava/lang/String;

    move-result-object v22

    .line 256
    .local v22, "srcChannelNr":Ljava/lang/String;
    invoke-virtual {v12}, Lcom/samsung/android/allshare/Item;->getResolution()Ljava/lang/String;

    move-result-object v25

    .line 258
    .local v25, "srcResolution":Ljava/lang/String;
    invoke-virtual {v12}, Lcom/samsung/android/allshare/Item;->getArtist()Ljava/lang/String;

    move-result-object v21

    .line 259
    .local v21, "srcArtist":Ljava/lang/String;
    invoke-virtual {v12}, Lcom/samsung/android/allshare/Item;->getAlbumTitle()Ljava/lang/String;

    move-result-object v20

    .line 261
    .local v20, "srcAlbum":Ljava/lang/String;
    invoke-virtual {v12}, Lcom/samsung/android/allshare/Item;->getBitrate()I

    move-result v29

    move/from16 v0, v29

    int-to-long v4, v0

    .line 262
    .local v4, "bitrate":J
    invoke-virtual {v12}, Lcom/samsung/android/allshare/Item;->getDuration()J

    move-result-wide v10

    .line 264
    .local v10, "duration":J
    const-wide/16 v8, 0x0

    .line 266
    .local v8, "date":J
    invoke-virtual {v12}, Lcom/samsung/android/allshare/Item;->getDate()Ljava/util/Date;

    move-result-object v29

    if-eqz v29, :cond_a

    .line 267
    invoke-virtual {v12}, Lcom/samsung/android/allshare/Item;->getDate()Ljava/util/Date;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/util/Date;->getTime()J

    move-result-wide v8

    .line 272
    :goto_4
    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v29

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, v29

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, v29

    move-wide/from16 v1, v26

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, v29

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, v29

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, v29

    invoke-virtual {v0, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, v29

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, v29

    invoke-virtual {v0, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, v29

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, v29

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    .line 274
    .local v16, "key":Ljava/lang/String;
    # getter for: Lcom/samsung/android/allshare/extension/impl/BrowseMediaGetter$FlatProviderConnectionInfo;->mItems:Ljava/util/HashMap;
    invoke-static {v6}, Lcom/samsung/android/allshare/extension/impl/BrowseMediaGetter$FlatProviderConnectionInfo;->access$400(Lcom/samsung/android/allshare/extension/impl/BrowseMediaGetter$FlatProviderConnectionInfo;)Ljava/util/HashMap;

    move-result-object v29

    move-object/from16 v0, v29

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v29

    if-nez v29, :cond_b

    .line 275
    # getter for: Lcom/samsung/android/allshare/extension/impl/BrowseMediaGetter$FlatProviderConnectionInfo;->mItems:Ljava/util/HashMap;
    invoke-static {v6}, Lcom/samsung/android/allshare/extension/impl/BrowseMediaGetter$FlatProviderConnectionInfo;->access$400(Lcom/samsung/android/allshare/extension/impl/BrowseMediaGetter$FlatProviderConnectionInfo;)Ljava/util/HashMap;

    move-result-object v29

    move-object/from16 v0, v29

    move-object/from16 v1, v16

    move-object/from16 v2, v16

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 276
    move-object/from16 v0, v18

    invoke-virtual {v0, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 290
    .end local v4    # "bitrate":J
    .end local v8    # "date":J
    .end local v10    # "duration":J
    .end local v16    # "key":Ljava/lang/String;
    .end local v20    # "srcAlbum":Ljava/lang/String;
    .end local v21    # "srcArtist":Ljava/lang/String;
    .end local v22    # "srcChannelNr":Ljava/lang/String;
    .end local v23    # "srcExt":Ljava/lang/String;
    .end local v24    # "srcName":Ljava/lang/String;
    .end local v25    # "srcResolution":Ljava/lang/String;
    .end local v26    # "srcSize":J
    :cond_9
    :goto_5
    invoke-virtual {v12}, Lcom/samsung/android/allshare/Item;->getType()Lcom/samsung/android/allshare/Item$MediaType;

    move-result-object v29

    sget-object v30, Lcom/samsung/android/allshare/Item$MediaType;->ITEM_FOLDER:Lcom/samsung/android/allshare/Item$MediaType;

    move-object/from16 v0, v29

    move-object/from16 v1, v30

    if-ne v0, v1, :cond_8

    .line 291
    const-class v29, Lcom/samsung/android/allshare/extension/impl/BrowseMediaGetter;

    invoke-virtual/range {v29 .. v29}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v29

    new-instance v30, Ljava/lang/StringBuilder;

    invoke-direct/range {v30 .. v30}, Ljava/lang/StringBuilder;-><init>()V

    const-string v31, "container stacking + "

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual {v12}, Lcom/samsung/android/allshare/Item;->getTitle()Ljava/lang/String;

    move-result-object v31

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    invoke-static/range {v29 .. v30}, Lcom/samsung/android/allshare/DLog;->d_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 293
    # getter for: Lcom/samsung/android/allshare/extension/impl/BrowseMediaGetter$FlatProviderConnectionInfo;->mFutureBrowseFolderStack:Ljava/util/Stack;
    invoke-static {v6}, Lcom/samsung/android/allshare/extension/impl/BrowseMediaGetter$FlatProviderConnectionInfo;->access$500(Lcom/samsung/android/allshare/extension/impl/BrowseMediaGetter$FlatProviderConnectionInfo;)Ljava/util/Stack;

    move-result-object v29

    move-object/from16 v0, v29

    invoke-virtual {v0, v12}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_3

    .line 269
    .restart local v4    # "bitrate":J
    .restart local v8    # "date":J
    .restart local v10    # "duration":J
    .restart local v20    # "srcAlbum":Ljava/lang/String;
    .restart local v21    # "srcArtist":Ljava/lang/String;
    .restart local v22    # "srcChannelNr":Ljava/lang/String;
    .restart local v23    # "srcExt":Ljava/lang/String;
    .restart local v24    # "srcName":Ljava/lang/String;
    .restart local v25    # "srcResolution":Ljava/lang/String;
    .restart local v26    # "srcSize":J
    :cond_a
    const-string v29, "BrowseMediaGetter"

    new-instance v30, Ljava/lang/StringBuilder;

    invoke-direct/range {v30 .. v30}, Ljava/lang/StringBuilder;-><init>()V

    const-string v31, "item("

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    move-object/from16 v0, v30

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    move-object/from16 v0, v30

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    const-string v31, ") srcDate is 0"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    invoke-static/range {v29 .. v30}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_4

    .line 278
    .restart local v16    # "key":Ljava/lang/String;
    :cond_b
    const-class v29, Lcom/samsung/android/allshare/extension/impl/BrowseMediaGetter;

    invoke-virtual/range {v29 .. v29}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v29

    new-instance v30, Ljava/lang/StringBuilder;

    invoke-direct/range {v30 .. v30}, Ljava/lang/StringBuilder;-><init>()V

    const-string v31, "Duplicated content !!!, title : "

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual {v12}, Lcom/samsung/android/allshare/Item;->getTitle()Ljava/lang/String;

    move-result-object v31

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    const-string v31, ", ext= "

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    move-object/from16 v0, v30

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    const-string v31, ", size= "

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    move-object/from16 v0, v30

    move-wide/from16 v1, v26

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v30

    const-string v31, ", resolution= "

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    move-object/from16 v0, v30

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    const-string v31, ", artist= "

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    move-object/from16 v0, v30

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    const-string v31, ", album= "

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    move-object/from16 v0, v30

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    const-string v31, ", bitrate= "

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    move-object/from16 v0, v30

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v30

    const-string v31, ", duration= "

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    move-object/from16 v0, v30

    invoke-virtual {v0, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v30

    const-string v31, ", date= "

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    move-object/from16 v0, v30

    invoke-virtual {v0, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v30

    const-string v31, ", channelNr= "

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    move-object/from16 v0, v30

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    invoke-static/range {v29 .. v30}, Lcom/samsung/android/allshare/DLog;->d_api(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_5

    .line 305
    .end local v4    # "bitrate":J
    .end local v8    # "date":J
    .end local v10    # "duration":J
    .end local v12    # "i":Lcom/samsung/android/allshare/Item;
    .end local v13    # "i$":Ljava/util/Iterator;
    .end local v16    # "key":Ljava/lang/String;
    .end local v20    # "srcAlbum":Ljava/lang/String;
    .end local v21    # "srcArtist":Ljava/lang/String;
    .end local v22    # "srcChannelNr":Ljava/lang/String;
    .end local v23    # "srcExt":Ljava/lang/String;
    .end local v24    # "srcName":Ljava/lang/String;
    .end local v25    # "srcResolution":Ljava/lang/String;
    .end local v26    # "srcSize":J
    :cond_c
    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->size()I

    move-result v29

    if-lez v29, :cond_d

    .line 306
    move-object/from16 v0, v18

    invoke-virtual {v6, v0}, Lcom/samsung/android/allshare/extension/impl/BrowseMediaGetter$FlatProviderConnectionInfo;->onProgress(Ljava/util/ArrayList;)V

    .line 307
    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->clear()V

    .line 311
    :cond_d
    # getter for: Lcom/samsung/android/allshare/extension/impl/BrowseMediaGetter$FlatProviderConnectionInfo;->mCurrentBrowseMap:Ljava/util/HashMap;
    invoke-static {v6}, Lcom/samsung/android/allshare/extension/impl/BrowseMediaGetter$FlatProviderConnectionInfo;->access$600(Lcom/samsung/android/allshare/extension/impl/BrowseMediaGetter$FlatProviderConnectionInfo;)Ljava/util/HashMap;

    move-result-object v29

    move-object/from16 v0, v29

    move-object/from16 v1, p4

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 313
    const-class v29, Lcom/samsung/android/allshare/extension/impl/BrowseMediaGetter;

    invoke-virtual/range {v29 .. v29}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v29

    new-instance v30, Ljava/lang/StringBuilder;

    invoke-direct/range {v30 .. v30}, Ljava/lang/StringBuilder;-><init>()V

    const-string v31, "endOfItems=="

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    move-object/from16 v0, v30

    move/from16 v1, p5

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v30

    const-string v31, ", err="

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {p6 .. p6}, Lcom/samsung/android/allshare/ERROR;->enumToString()Ljava/lang/String;

    move-result-object v31

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    const-string v31, ", conn.mFutureBrowseFolderStack.size() ="

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    # getter for: Lcom/samsung/android/allshare/extension/impl/BrowseMediaGetter$FlatProviderConnectionInfo;->mFutureBrowseFolderStack:Ljava/util/Stack;
    invoke-static {v6}, Lcom/samsung/android/allshare/extension/impl/BrowseMediaGetter$FlatProviderConnectionInfo;->access$500(Lcom/samsung/android/allshare/extension/impl/BrowseMediaGetter$FlatProviderConnectionInfo;)Ljava/util/Stack;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/util/Stack;->size()I

    move-result v31

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v30

    const-string v31, ", conn.mFailedBrowseFolderStack.size() ="

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    # getter for: Lcom/samsung/android/allshare/extension/impl/BrowseMediaGetter$FlatProviderConnectionInfo;->mFailedBrowseFolderStack:Ljava/util/Stack;
    invoke-static {v6}, Lcom/samsung/android/allshare/extension/impl/BrowseMediaGetter$FlatProviderConnectionInfo;->access$200(Lcom/samsung/android/allshare/extension/impl/BrowseMediaGetter$FlatProviderConnectionInfo;)Ljava/util/Stack;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/util/Stack;->size()I

    move-result v31

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v30

    const-string v31, ", conn.mCurrentBrowseMap.size() ="

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    # getter for: Lcom/samsung/android/allshare/extension/impl/BrowseMediaGetter$FlatProviderConnectionInfo;->mCurrentBrowseMap:Ljava/util/HashMap;
    invoke-static {v6}, Lcom/samsung/android/allshare/extension/impl/BrowseMediaGetter$FlatProviderConnectionInfo;->access$600(Lcom/samsung/android/allshare/extension/impl/BrowseMediaGetter$FlatProviderConnectionInfo;)Ljava/util/HashMap;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/util/HashMap;->size()I

    move-result v31

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    invoke-static/range {v29 .. v30}, Lcom/samsung/android/allshare/DLog;->d_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 320
    if-nez p5, :cond_e

    sget-object v29, Lcom/samsung/android/allshare/ERROR;->SUCCESS:Lcom/samsung/android/allshare/ERROR;

    move-object/from16 v0, p6

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/ERROR;->compareTo(Ljava/lang/Enum;)I

    move-result v29

    if-nez v29, :cond_e

    .line 322
    const-class v29, Lcom/samsung/android/allshare/extension/impl/BrowseMediaGetter;

    invoke-virtual/range {v29 .. v29}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v29

    new-instance v30, Ljava/lang/StringBuilder;

    invoke-direct/range {v30 .. v30}, Ljava/lang/StringBuilder;-><init>()V

    const-string v31, "browse reamin contents "

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {p4 .. p4}, Lcom/samsung/android/allshare/Item;->getTitle()Ljava/lang/String;

    move-result-object v31

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    const-string v31, " idx = "

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    move-object/from16 v0, v30

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v30

    move-object/from16 v0, v30

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    invoke-static/range {v29 .. v30}, Lcom/samsung/android/allshare/DLog;->d_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 325
    add-int v29, p2, v19

    move-object/from16 v0, p4

    move/from16 v1, v29

    invoke-virtual {v6, v0, v1}, Lcom/samsung/android/allshare/extension/impl/BrowseMediaGetter$FlatProviderConnectionInfo;->setCurrentBrowseRequest(Lcom/samsung/android/allshare/Item;I)V

    .line 327
    # getter for: Lcom/samsung/android/allshare/extension/impl/BrowseMediaGetter$FlatProviderConnectionInfo;->mProvider:Lcom/samsung/android/allshare/media/Provider;
    invoke-static {v6}, Lcom/samsung/android/allshare/extension/impl/BrowseMediaGetter$FlatProviderConnectionInfo;->access$100(Lcom/samsung/android/allshare/extension/impl/BrowseMediaGetter$FlatProviderConnectionInfo;)Lcom/samsung/android/allshare/media/Provider;

    move-result-object v29

    add-int v30, p2, v19

    const/16 v31, 0x32

    move-object/from16 v0, v29

    move-object/from16 v1, p4

    move/from16 v2, v30

    move/from16 v3, v31

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/allshare/media/Provider;->browse(Lcom/samsung/android/allshare/Item;II)V

    goto/16 :goto_1

    .line 330
    :cond_e
    # getter for: Lcom/samsung/android/allshare/extension/impl/BrowseMediaGetter$FlatProviderConnectionInfo;->mFutureBrowseFolderStack:Ljava/util/Stack;
    invoke-static {v6}, Lcom/samsung/android/allshare/extension/impl/BrowseMediaGetter$FlatProviderConnectionInfo;->access$500(Lcom/samsung/android/allshare/extension/impl/BrowseMediaGetter$FlatProviderConnectionInfo;)Ljava/util/Stack;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/util/Stack;->size()I

    move-result v29

    if-lez v29, :cond_f

    .line 333
    :goto_6
    # getter for: Lcom/samsung/android/allshare/extension/impl/BrowseMediaGetter$FlatProviderConnectionInfo;->mFutureBrowseFolderStack:Ljava/util/Stack;
    invoke-static {v6}, Lcom/samsung/android/allshare/extension/impl/BrowseMediaGetter$FlatProviderConnectionInfo;->access$500(Lcom/samsung/android/allshare/extension/impl/BrowseMediaGetter$FlatProviderConnectionInfo;)Ljava/util/Stack;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/util/Stack;->size()I

    move-result v29

    if-lez v29, :cond_2

    # getter for: Lcom/samsung/android/allshare/extension/impl/BrowseMediaGetter$FlatProviderConnectionInfo;->mCurrentBrowseMap:Ljava/util/HashMap;
    invoke-static {v6}, Lcom/samsung/android/allshare/extension/impl/BrowseMediaGetter$FlatProviderConnectionInfo;->access$600(Lcom/samsung/android/allshare/extension/impl/BrowseMediaGetter$FlatProviderConnectionInfo;)Ljava/util/HashMap;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/util/HashMap;->size()I

    move-result v29

    const/16 v30, 0x2

    move/from16 v0, v29

    move/from16 v1, v30

    if-ge v0, v1, :cond_2

    .line 334
    # getter for: Lcom/samsung/android/allshare/extension/impl/BrowseMediaGetter$FlatProviderConnectionInfo;->mFutureBrowseFolderStack:Ljava/util/Stack;
    invoke-static {v6}, Lcom/samsung/android/allshare/extension/impl/BrowseMediaGetter$FlatProviderConnectionInfo;->access$500(Lcom/samsung/android/allshare/extension/impl/BrowseMediaGetter$FlatProviderConnectionInfo;)Ljava/util/Stack;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/android/allshare/Item;

    .line 335
    .local v7, "currentBrowseFolder":Lcom/samsung/android/allshare/Item;
    const/16 v29, 0x0

    move/from16 v0, v29

    invoke-virtual {v6, v7, v0}, Lcom/samsung/android/allshare/extension/impl/BrowseMediaGetter$FlatProviderConnectionInfo;->setCurrentBrowseRequest(Lcom/samsung/android/allshare/Item;I)V

    .line 336
    # getter for: Lcom/samsung/android/allshare/extension/impl/BrowseMediaGetter$FlatProviderConnectionInfo;->mProvider:Lcom/samsung/android/allshare/media/Provider;
    invoke-static {v6}, Lcom/samsung/android/allshare/extension/impl/BrowseMediaGetter$FlatProviderConnectionInfo;->access$100(Lcom/samsung/android/allshare/extension/impl/BrowseMediaGetter$FlatProviderConnectionInfo;)Lcom/samsung/android/allshare/media/Provider;

    move-result-object v29

    const/16 v30, 0x0

    const/16 v31, 0x32

    move-object/from16 v0, v29

    move/from16 v1, v30

    move/from16 v2, v31

    invoke-virtual {v0, v7, v1, v2}, Lcom/samsung/android/allshare/media/Provider;->browse(Lcom/samsung/android/allshare/Item;II)V

    .line 338
    const-class v29, Lcom/samsung/android/allshare/extension/impl/BrowseMediaGetter;

    invoke-virtual/range {v29 .. v29}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v29

    new-instance v30, Ljava/lang/StringBuilder;

    invoke-direct/range {v30 .. v30}, Ljava/lang/StringBuilder;-><init>()V

    const-string v31, "browse stacked container ["

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual {v7}, Lcom/samsung/android/allshare/Item;->getTitle()Ljava/lang/String;

    move-result-object v31

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    const-string v31, "], remained size = "

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    # getter for: Lcom/samsung/android/allshare/extension/impl/BrowseMediaGetter$FlatProviderConnectionInfo;->mFutureBrowseFolderStack:Ljava/util/Stack;
    invoke-static {v6}, Lcom/samsung/android/allshare/extension/impl/BrowseMediaGetter$FlatProviderConnectionInfo;->access$500(Lcom/samsung/android/allshare/extension/impl/BrowseMediaGetter$FlatProviderConnectionInfo;)Ljava/util/Stack;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/util/Stack;->size()I

    move-result v31

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    invoke-static/range {v29 .. v30}, Lcom/samsung/android/allshare/DLog;->d_api(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_6

    .line 344
    .end local v7    # "currentBrowseFolder":Lcom/samsung/android/allshare/Item;
    :cond_f
    # getter for: Lcom/samsung/android/allshare/extension/impl/BrowseMediaGetter$FlatProviderConnectionInfo;->mFailedBrowseFolderStack:Ljava/util/Stack;
    invoke-static {v6}, Lcom/samsung/android/allshare/extension/impl/BrowseMediaGetter$FlatProviderConnectionInfo;->access$200(Lcom/samsung/android/allshare/extension/impl/BrowseMediaGetter$FlatProviderConnectionInfo;)Ljava/util/Stack;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/util/Stack;->size()I

    move-result v29

    if-lez v29, :cond_10

    .line 346
    # getter for: Lcom/samsung/android/allshare/extension/impl/BrowseMediaGetter$FlatProviderConnectionInfo;->mFailedBrowseFolderStack:Ljava/util/Stack;
    invoke-static {v6}, Lcom/samsung/android/allshare/extension/impl/BrowseMediaGetter$FlatProviderConnectionInfo;->access$200(Lcom/samsung/android/allshare/extension/impl/BrowseMediaGetter$FlatProviderConnectionInfo;)Ljava/util/Stack;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/android/allshare/Item;

    .line 347
    .restart local v7    # "currentBrowseFolder":Lcom/samsung/android/allshare/Item;
    # getter for: Lcom/samsung/android/allshare/extension/impl/BrowseMediaGetter$FlatProviderConnectionInfo;->mFailedBrowseFolderStartIndexMap:Ljava/util/HashMap;
    invoke-static {v6}, Lcom/samsung/android/allshare/extension/impl/BrowseMediaGetter$FlatProviderConnectionInfo;->access$300(Lcom/samsung/android/allshare/extension/impl/BrowseMediaGetter$FlatProviderConnectionInfo;)Ljava/util/HashMap;

    move-result-object v29

    move-object/from16 v0, v29

    invoke-virtual {v0, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v28

    check-cast v28, Ljava/lang/Integer;

    .line 349
    .local v28, "startIndex":Ljava/lang/Integer;
    invoke-virtual/range {v28 .. v28}, Ljava/lang/Integer;->intValue()I

    move-result v29

    move/from16 v0, v29

    invoke-virtual {v6, v7, v0}, Lcom/samsung/android/allshare/extension/impl/BrowseMediaGetter$FlatProviderConnectionInfo;->setCurrentBrowseRequest(Lcom/samsung/android/allshare/Item;I)V

    .line 350
    const-class v29, Lcom/samsung/android/allshare/extension/impl/BrowseMediaGetter;

    invoke-virtual/range {v29 .. v29}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v29

    new-instance v30, Ljava/lang/StringBuilder;

    invoke-direct/range {v30 .. v30}, Ljava/lang/StringBuilder;-><init>()V

    const-string v31, "brwose failed container ["

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual {v7}, Lcom/samsung/android/allshare/Item;->getTitle()Ljava/lang/String;

    move-result-object v31

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    const-string v31, "], start "

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    move-object/from16 v0, v30

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v30

    const-string v31, ", remained size = "

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    # getter for: Lcom/samsung/android/allshare/extension/impl/BrowseMediaGetter$FlatProviderConnectionInfo;->mFailedBrowseFolderStack:Ljava/util/Stack;
    invoke-static {v6}, Lcom/samsung/android/allshare/extension/impl/BrowseMediaGetter$FlatProviderConnectionInfo;->access$200(Lcom/samsung/android/allshare/extension/impl/BrowseMediaGetter$FlatProviderConnectionInfo;)Ljava/util/Stack;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/util/Stack;->size()I

    move-result v31

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    invoke-static/range {v29 .. v30}, Lcom/samsung/android/allshare/DLog;->d_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 353
    # getter for: Lcom/samsung/android/allshare/extension/impl/BrowseMediaGetter$FlatProviderConnectionInfo;->mProvider:Lcom/samsung/android/allshare/media/Provider;
    invoke-static {v6}, Lcom/samsung/android/allshare/extension/impl/BrowseMediaGetter$FlatProviderConnectionInfo;->access$100(Lcom/samsung/android/allshare/extension/impl/BrowseMediaGetter$FlatProviderConnectionInfo;)Lcom/samsung/android/allshare/media/Provider;

    move-result-object v29

    invoke-virtual/range {v28 .. v28}, Ljava/lang/Integer;->intValue()I

    move-result v30

    const/16 v31, 0x32

    move-object/from16 v0, v29

    move/from16 v1, v30

    move/from16 v2, v31

    invoke-virtual {v0, v7, v1, v2}, Lcom/samsung/android/allshare/media/Provider;->browse(Lcom/samsung/android/allshare/Item;II)V

    goto/16 :goto_1

    .line 356
    .end local v7    # "currentBrowseFolder":Lcom/samsung/android/allshare/Item;
    .end local v28    # "startIndex":Ljava/lang/Integer;
    :cond_10
    # getter for: Lcom/samsung/android/allshare/extension/impl/BrowseMediaGetter$FlatProviderConnectionInfo;->mCurrentBrowseMap:Ljava/util/HashMap;
    invoke-static {v6}, Lcom/samsung/android/allshare/extension/impl/BrowseMediaGetter$FlatProviderConnectionInfo;->access$600(Lcom/samsung/android/allshare/extension/impl/BrowseMediaGetter$FlatProviderConnectionInfo;)Ljava/util/HashMap;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/util/HashMap;->size()I

    move-result v29

    if-gtz v29, :cond_2

    .line 357
    const-class v29, Lcom/samsung/android/allshare/extension/impl/BrowseMediaGetter;

    invoke-virtual/range {v29 .. v29}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v29

    const-string v30, "FINISHED "

    invoke-static/range {v29 .. v30}, Lcom/samsung/android/allshare/DLog;->d_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 358
    invoke-virtual {v6}, Lcom/samsung/android/allshare/extension/impl/BrowseMediaGetter$FlatProviderConnectionInfo;->onFinish()V

    goto/16 :goto_1

    .line 364
    .end local v6    # "conn":Lcom/samsung/android/allshare/extension/impl/BrowseMediaGetter$FlatProviderConnectionInfo;
    .end local v18    # "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/Item;>;"
    :cond_11
    return-void
.end method

.method public start(Lcom/samsung/android/allshare/media/Provider;Lcom/samsung/android/allshare/Item$MediaType;Lcom/samsung/android/allshare/extension/FlatProvider$IFlatProviderConnection;)V
    .locals 1
    .param p1, "provider"    # Lcom/samsung/android/allshare/media/Provider;
    .param p2, "type"    # Lcom/samsung/android/allshare/Item$MediaType;
    .param p3, "callback"    # Lcom/samsung/android/allshare/extension/FlatProvider$IFlatProviderConnection;

    .prologue
    .line 190
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 191
    .local v0, "types":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/Item$MediaType;>;"
    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 193
    invoke-virtual {p0, p1, v0, p3}, Lcom/samsung/android/allshare/extension/impl/BrowseMediaGetter;->start(Lcom/samsung/android/allshare/media/Provider;Ljava/util/ArrayList;Lcom/samsung/android/allshare/extension/FlatProvider$IFlatProviderConnection;)V

    .line 194
    return-void
.end method

.method public start(Lcom/samsung/android/allshare/media/Provider;Ljava/util/ArrayList;Lcom/samsung/android/allshare/extension/FlatProvider$IFlatProviderConnection;)V
    .locals 5
    .param p1, "provider"    # Lcom/samsung/android/allshare/media/Provider;
    .param p3, "connection"    # Lcom/samsung/android/allshare/extension/FlatProvider$IFlatProviderConnection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/android/allshare/media/Provider;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/Item$MediaType;",
            ">;",
            "Lcom/samsung/android/allshare/extension/FlatProvider$IFlatProviderConnection;",
            ")V"
        }
    .end annotation

    .prologue
    .local p2, "types":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/Item$MediaType;>;"
    const/4 v4, 0x0

    .line 170
    if-eqz p1, :cond_0

    invoke-virtual {p2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Lcom/samsung/android/allshare/Item$MediaType;->ITEM_UNKNOWN:Lcom/samsung/android/allshare/Item$MediaType;

    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Lcom/samsung/android/allshare/Item$MediaType;->ITEM_FOLDER:Lcom/samsung/android/allshare/Item$MediaType;

    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 172
    :cond_0
    sget-object v1, Lcom/samsung/android/allshare/ERROR;->INVALID_ARGUMENT:Lcom/samsung/android/allshare/ERROR;

    invoke-interface {p3, v1}, Lcom/samsung/android/allshare/extension/FlatProvider$IFlatProviderConnection;->onError(Lcom/samsung/android/allshare/ERROR;)V

    .line 186
    :goto_0
    return-void

    .line 177
    :cond_1
    invoke-interface {p3}, Lcom/samsung/android/allshare/extension/FlatProvider$IFlatProviderConnection;->onStart()V

    .line 178
    invoke-virtual {p1, p0}, Lcom/samsung/android/allshare/media/Provider;->setBrowseItemsResponseListener(Lcom/samsung/android/allshare/media/Provider$IProviderBrowseResponseListener;)V

    .line 180
    new-instance v0, Lcom/samsung/android/allshare/extension/impl/BrowseMediaGetter$FlatProviderConnectionInfo;

    invoke-direct {v0, p2, p3, p1}, Lcom/samsung/android/allshare/extension/impl/BrowseMediaGetter$FlatProviderConnectionInfo;-><init>(Ljava/util/ArrayList;Lcom/samsung/android/allshare/extension/FlatProvider$IFlatProviderConnection;Lcom/samsung/android/allshare/media/Provider;)V

    .line 182
    .local v0, "conn":Lcom/samsung/android/allshare/extension/impl/BrowseMediaGetter$FlatProviderConnectionInfo;
    iget-object v1, p0, Lcom/samsung/android/allshare/extension/impl/BrowseMediaGetter;->mConns:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 184
    invoke-virtual {p1}, Lcom/samsung/android/allshare/media/Provider;->getRootFolder()Lcom/samsung/android/allshare/Item;

    move-result-object v1

    invoke-virtual {v0, v1, v4}, Lcom/samsung/android/allshare/extension/impl/BrowseMediaGetter$FlatProviderConnectionInfo;->setCurrentBrowseRequest(Lcom/samsung/android/allshare/Item;I)V

    .line 185
    # getter for: Lcom/samsung/android/allshare/extension/impl/BrowseMediaGetter$FlatProviderConnectionInfo;->mProvider:Lcom/samsung/android/allshare/media/Provider;
    invoke-static {v0}, Lcom/samsung/android/allshare/extension/impl/BrowseMediaGetter$FlatProviderConnectionInfo;->access$100(Lcom/samsung/android/allshare/extension/impl/BrowseMediaGetter$FlatProviderConnectionInfo;)Lcom/samsung/android/allshare/media/Provider;

    move-result-object v1

    invoke-virtual {p1}, Lcom/samsung/android/allshare/media/Provider;->getRootFolder()Lcom/samsung/android/allshare/Item;

    move-result-object v2

    const/16 v3, 0x32

    invoke-virtual {v1, v2, v4, v3}, Lcom/samsung/android/allshare/media/Provider;->browse(Lcom/samsung/android/allshare/Item;II)V

    goto :goto_0
.end method
