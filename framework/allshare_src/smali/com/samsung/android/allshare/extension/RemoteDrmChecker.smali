.class public Lcom/samsung/android/allshare/extension/RemoteDrmChecker;
.super Ljava/lang/Object;
.source "RemoteDrmChecker.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/allshare/extension/RemoteDrmChecker$DrmCheckThread;
    }
.end annotation


# static fields
.field static final DRM_PROTECTED_ERROR:I = -0x1

.field static final DRM_PROTECTED_FALSE:I = 0x0

.field static final DRM_PROTECTED_TRUE:I = 0x1

.field static final OMA_PLUGIN_MIME:Ljava/lang/String; = "application/vnd.oma.drm.content"

.field static final PR_PLUGIN_MIME:Ljava/lang/String; = "audio/vnd.ms-playready.media.pya"

.field static final WMA_PLUGIN_MIME:Ljava/lang/String; = "audio/x-ms-wma"

.field static final WMV_PLUGIN_MIME:Ljava/lang/String; = "video/x-ms-wmv"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mIsDrmChecked:Z

.field private mIsDrmFile:I

.field private final mPath:Ljava/lang/String;

.field private final mTAG:Ljava/lang/String;

.field private final mTAG_CLASS:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 112
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 81
    const-string v0, "RemoteDrmChecker"

    iput-object v0, p0, Lcom/samsung/android/allshare/extension/RemoteDrmChecker;->mTAG:Ljava/lang/String;

    .line 83
    const-string v0, " "

    iput-object v0, p0, Lcom/samsung/android/allshare/extension/RemoteDrmChecker;->mTAG_CLASS:Ljava/lang/String;

    .line 85
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/Android/data/com.samsung.android.allshare/drmchecker/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/allshare/extension/RemoteDrmChecker;->mPath:Ljava/lang/String;

    .line 113
    iput-object p1, p0, Lcom/samsung/android/allshare/extension/RemoteDrmChecker;->mContext:Landroid/content/Context;

    .line 114
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/allshare/extension/RemoteDrmChecker;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/extension/RemoteDrmChecker;

    .prologue
    .line 80
    iget v0, p0, Lcom/samsung/android/allshare/extension/RemoteDrmChecker;->mIsDrmFile:I

    return v0
.end method

.method static synthetic access$002(Lcom/samsung/android/allshare/extension/RemoteDrmChecker;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/extension/RemoteDrmChecker;
    .param p1, "x1"    # I

    .prologue
    .line 80
    iput p1, p0, Lcom/samsung/android/allshare/extension/RemoteDrmChecker;->mIsDrmFile:I

    return p1
.end method

.method static synthetic access$100(Lcom/samsung/android/allshare/extension/RemoteDrmChecker;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/extension/RemoteDrmChecker;

    .prologue
    .line 80
    iget-object v0, p0, Lcom/samsung/android/allshare/extension/RemoteDrmChecker;->mPath:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/android/allshare/extension/RemoteDrmChecker;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/extension/RemoteDrmChecker;

    .prologue
    .line 80
    iget-object v0, p0, Lcom/samsung/android/allshare/extension/RemoteDrmChecker;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$302(Lcom/samsung/android/allshare/extension/RemoteDrmChecker;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/extension/RemoteDrmChecker;
    .param p1, "x1"    # Z

    .prologue
    .line 80
    iput-boolean p1, p0, Lcom/samsung/android/allshare/extension/RemoteDrmChecker;->mIsDrmChecked:Z

    return p1
.end method


# virtual methods
.method public isDrmFile(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    .locals 6
    .param p1, "selectedItemUri"    # Ljava/lang/String;
    .param p2, "mExtension"    # Ljava/lang/String;
    .param p3, "mimeType"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    const/4 v2, -0x1

    .line 147
    if-nez p2, :cond_0

    if-nez p3, :cond_0

    .line 148
    const-string v3, "RemoteDrmChecker"

    const-string v4, " Invalid arg."

    invoke-static {v3, v4}, Lcom/samsung/android/allshare/DLog;->i_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 180
    :goto_0
    return v2

    .line 151
    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_1

    const-string v4, "http"

    invoke-virtual {p1, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 152
    :cond_1
    const-string v3, "RemoteDrmChecker"

    const-string v4, " Invalid Url."

    invoke-static {v3, v4}, Lcom/samsung/android/allshare/DLog;->i_api(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 157
    :cond_2
    if-eqz p2, :cond_3

    invoke-virtual {p2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    const-string v5, ".wmv"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_4

    invoke-virtual {p2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    const-string v5, ".wma"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_4

    :cond_3
    if-eqz p3, :cond_6

    const-string v4, "video/x-ms-wmv"

    invoke-virtual {p3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_4

    const-string v4, "audio/x-ms-wma"

    invoke-virtual {p3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_4

    const-string v4, "audio/wav"

    invoke-virtual {p3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_4

    const-string v4, "audio/x-wav"

    invoke-virtual {p3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 162
    :cond_4
    iput-boolean v3, p0, Lcom/samsung/android/allshare/extension/RemoteDrmChecker;->mIsDrmChecked:Z

    .line 163
    new-instance v1, Lcom/samsung/android/allshare/extension/RemoteDrmChecker$DrmCheckThread;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/samsung/android/allshare/extension/RemoteDrmChecker$DrmCheckThread;-><init>(Lcom/samsung/android/allshare/extension/RemoteDrmChecker;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 165
    .local v1, "mDrmCheckThread":Lcom/samsung/android/allshare/extension/RemoteDrmChecker$DrmCheckThread;
    invoke-virtual {v1}, Lcom/samsung/android/allshare/extension/RemoteDrmChecker$DrmCheckThread;->start()V

    .line 168
    const-wide/16 v4, 0x1388

    :try_start_0
    invoke-virtual {v1, v4, v5}, Lcom/samsung/android/allshare/extension/RemoteDrmChecker$DrmCheckThread;->join(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 172
    :goto_1
    iget-boolean v3, p0, Lcom/samsung/android/allshare/extension/RemoteDrmChecker;->mIsDrmChecked:Z

    if-nez v3, :cond_5

    .line 173
    const-string v3, "RemoteDrmChecker"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " thread time out : isDrmFile = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/samsung/android/allshare/extension/RemoteDrmChecker;->mIsDrmFile:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 169
    :catch_0
    move-exception v0

    .line 170
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_1

    .line 176
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :cond_5
    const-string v2, "RemoteDrmChecker"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " returning isDrmFile = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/samsung/android/allshare/extension/RemoteDrmChecker;->mIsDrmFile:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/allshare/DLog;->i_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 177
    iget v2, p0, Lcom/samsung/android/allshare/extension/RemoteDrmChecker;->mIsDrmFile:I

    goto/16 :goto_0

    .line 179
    .end local v1    # "mDrmCheckThread":Lcom/samsung/android/allshare/extension/RemoteDrmChecker$DrmCheckThread;
    :cond_6
    const-string v2, "RemoteDrmChecker"

    const-string v4, " return FALSE, immediately."

    invoke-static {v2, v4}, Lcom/samsung/android/allshare/DLog;->i_api(Ljava/lang/String;Ljava/lang/String;)V

    move v2, v3

    .line 180
    goto/16 :goto_0
.end method
