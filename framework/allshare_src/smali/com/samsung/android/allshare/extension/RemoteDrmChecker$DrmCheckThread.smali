.class Lcom/samsung/android/allshare/extension/RemoteDrmChecker$DrmCheckThread;
.super Ljava/lang/Thread;
.source "RemoteDrmChecker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/allshare/extension/RemoteDrmChecker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DrmCheckThread"
.end annotation


# instance fields
.field private mExtension:Ljava/lang/String;

.field private mMimeType:Ljava/lang/String;

.field private mSelectedItemUri:Ljava/lang/String;

.field final synthetic this$0:Lcom/samsung/android/allshare/extension/RemoteDrmChecker;


# direct methods
.method public constructor <init>(Lcom/samsung/android/allshare/extension/RemoteDrmChecker;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p2, "selectedItemUri"    # Ljava/lang/String;
    .param p3, "mExtension"    # Ljava/lang/String;
    .param p4, "mimeType"    # Ljava/lang/String;

    .prologue
    .line 190
    iput-object p1, p0, Lcom/samsung/android/allshare/extension/RemoteDrmChecker$DrmCheckThread;->this$0:Lcom/samsung/android/allshare/extension/RemoteDrmChecker;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 191
    iput-object p2, p0, Lcom/samsung/android/allshare/extension/RemoteDrmChecker$DrmCheckThread;->mSelectedItemUri:Ljava/lang/String;

    .line 192
    iput-object p3, p0, Lcom/samsung/android/allshare/extension/RemoteDrmChecker$DrmCheckThread;->mExtension:Ljava/lang/String;

    .line 193
    iput-object p4, p0, Lcom/samsung/android/allshare/extension/RemoteDrmChecker$DrmCheckThread;->mMimeType:Ljava/lang/String;

    .line 194
    return-void
.end method


# virtual methods
.method public run()V
    .locals 24

    .prologue
    .line 197
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/extension/RemoteDrmChecker$DrmCheckThread;->this$0:Lcom/samsung/android/allshare/extension/RemoteDrmChecker;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    # setter for: Lcom/samsung/android/allshare/extension/RemoteDrmChecker;->mIsDrmFile:I
    invoke-static/range {v20 .. v21}, Lcom/samsung/android/allshare/extension/RemoteDrmChecker;->access$002(Lcom/samsung/android/allshare/extension/RemoteDrmChecker;I)I

    .line 199
    const/4 v3, 0x0

    .line 200
    .local v3, "client":Landroid/net/http/AndroidHttpClient;
    const/4 v7, 0x0

    .line 201
    .local v7, "entity":Lorg/apache/http/HttpEntity;
    const/4 v15, 0x0

    .line 202
    .local v15, "inStream":Ljava/io/InputStream;
    const/4 v11, 0x0

    .line 204
    .local v11, "fos":Ljava/io/FileOutputStream;
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "tempfile."

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v22

    invoke-static/range {v22 .. v23}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 205
    .local v10, "filename":Ljava/lang/String;
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/extension/RemoteDrmChecker$DrmCheckThread;->this$0:Lcom/samsung/android/allshare/extension/RemoteDrmChecker;

    move-object/from16 v21, v0

    # getter for: Lcom/samsung/android/allshare/extension/RemoteDrmChecker;->mPath:Ljava/lang/String;
    invoke-static/range {v21 .. v21}, Lcom/samsung/android/allshare/extension/RemoteDrmChecker;->access$100(Lcom/samsung/android/allshare/extension/RemoteDrmChecker;)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 207
    .local v9, "filePath":Ljava/lang/String;
    :try_start_0
    new-instance v4, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/extension/RemoteDrmChecker$DrmCheckThread;->this$0:Lcom/samsung/android/allshare/extension/RemoteDrmChecker;

    move-object/from16 v20, v0

    # getter for: Lcom/samsung/android/allshare/extension/RemoteDrmChecker;->mPath:Ljava/lang/String;
    invoke-static/range {v20 .. v20}, Lcom/samsung/android/allshare/extension/RemoteDrmChecker;->access$100(Lcom/samsung/android/allshare/extension/RemoteDrmChecker;)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-direct {v4, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 208
    .local v4, "dirPath":Ljava/io/File;
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v20

    if-nez v20, :cond_0

    .line 209
    const-string v20, "RemoteDrmChecker"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v21

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, " is not exist"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Lcom/samsung/android/allshare/DLog;->i_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 210
    invoke-virtual {v4}, Ljava/io/File;->mkdirs()Z

    .line 212
    :cond_0
    new-instance v16, Ljava/net/URL;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/extension/RemoteDrmChecker$DrmCheckThread;->mSelectedItemUri:Ljava/lang/String;

    move-object/from16 v20, v0

    move-object/from16 v0, v16

    move-object/from16 v1, v20

    invoke-direct {v0, v1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 214
    .local v16, "inputURL":Ljava/net/URL;
    invoke-static {v10}, Landroid/net/http/AndroidHttpClient;->newInstance(Ljava/lang/String;)Landroid/net/http/AndroidHttpClient;

    move-result-object v3

    .line 215
    if-nez v3, :cond_5

    .line 216
    const-string v20, "RemoteDrmChecker"

    const-string v21, " fail to newInstance AndroidHttpClient"

    invoke-static/range {v20 .. v21}, Lcom/samsung/android/allshare/DLog;->i_api(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_13
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_a
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 270
    if-eqz v3, :cond_1

    .line 271
    invoke-virtual {v3}, Landroid/net/http/AndroidHttpClient;->close()V

    .line 273
    :cond_1
    if-eqz v7, :cond_2

    .line 275
    :try_start_1
    invoke-interface {v7}, Lorg/apache/http/HttpEntity;->consumeContent()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 280
    :cond_2
    :goto_0
    if-eqz v11, :cond_3

    .line 282
    :try_start_2
    invoke-virtual {v11}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 287
    :cond_3
    :goto_1
    if-eqz v15, :cond_4

    .line 289
    :try_start_3
    invoke-virtual {v15}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    .line 302
    .end local v4    # "dirPath":Ljava/io/File;
    .end local v16    # "inputURL":Ljava/net/URL;
    :cond_4
    :goto_2
    return-void

    .line 276
    .restart local v4    # "dirPath":Ljava/io/File;
    .restart local v16    # "inputURL":Ljava/net/URL;
    :catch_0
    move-exception v6

    .line 277
    .local v6, "e":Ljava/io/IOException;
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 283
    .end local v6    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v6

    .line 284
    .restart local v6    # "e":Ljava/io/IOException;
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 290
    .end local v6    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v6

    .line 291
    .restart local v6    # "e":Ljava/io/IOException;
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 219
    .end local v6    # "e":Ljava/io/IOException;
    :cond_5
    :try_start_4
    new-instance v13, Lorg/apache/http/client/methods/HttpGet;

    invoke-virtual/range {v16 .. v16}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-direct {v13, v0}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    .line 221
    .local v13, "getRequest":Lorg/apache/http/client/methods/HttpGet;
    invoke-virtual {v3, v13}, Landroid/net/http/AndroidHttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v18

    .line 222
    .local v18, "response":Lorg/apache/http/HttpResponse;
    invoke-interface/range {v18 .. v18}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v7

    .line 224
    invoke-interface {v7}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v15

    .line 226
    new-instance v8, Ljava/io/File;

    invoke-direct {v8, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 227
    .local v8, "file":Ljava/io/File;
    new-instance v12, Ljava/io/FileOutputStream;

    invoke-direct {v12, v8}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_13
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_a
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 228
    .end local v11    # "fos":Ljava/io/FileOutputStream;
    .local v12, "fos":Ljava/io/FileOutputStream;
    const/16 v20, 0x2710

    :try_start_5
    move/from16 v0, v20

    new-array v2, v0, [B

    .line 230
    .local v2, "buf":[B
    const/16 v17, 0x0

    .line 231
    .local v17, "readCount":I
    const/4 v14, 0x0

    .local v14, "i":I
    :goto_3
    const/16 v20, 0x3

    move/from16 v0, v20

    if-ge v14, v0, :cond_7

    .line 232
    invoke-virtual {v15, v2}, Ljava/io/InputStream;->read([B)I

    move-result v17

    .line 233
    if-lez v17, :cond_6

    .line 234
    const/16 v20, 0x0

    move/from16 v0, v20

    move/from16 v1, v17

    invoke-virtual {v12, v2, v0, v1}, Ljava/io/FileOutputStream;->write([BII)V

    .line 231
    :cond_6
    add-int/lit8 v14, v14, 0x1

    goto :goto_3

    .line 236
    :cond_7
    invoke-virtual {v12}, Ljava/io/FileOutputStream;->close()V

    .line 239
    new-instance v5, Landroid/drm/DrmManagerClient;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/extension/RemoteDrmChecker$DrmCheckThread;->this$0:Lcom/samsung/android/allshare/extension/RemoteDrmChecker;

    move-object/from16 v20, v0

    # getter for: Lcom/samsung/android/allshare/extension/RemoteDrmChecker;->mContext:Landroid/content/Context;
    invoke-static/range {v20 .. v20}, Lcom/samsung/android/allshare/extension/RemoteDrmChecker;->access$200(Lcom/samsung/android/allshare/extension/RemoteDrmChecker;)Landroid/content/Context;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-direct {v5, v0}, Landroid/drm/DrmManagerClient;-><init>(Landroid/content/Context;)V

    .line 240
    .local v5, "drmClient":Landroid/drm/DrmManagerClient;
    const/16 v19, 0x0

    .line 242
    .local v19, "temp_isDrmFile":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/extension/RemoteDrmChecker$DrmCheckThread;->mExtension:Ljava/lang/String;

    move-object/from16 v20, v0

    if-eqz v20, :cond_8

    const-string v20, ".wma"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/extension/RemoteDrmChecker$DrmCheckThread;->mExtension:Ljava/lang/String;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-nez v20, :cond_9

    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/extension/RemoteDrmChecker$DrmCheckThread;->mMimeType:Ljava/lang/String;

    move-object/from16 v20, v0

    if-eqz v20, :cond_f

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/extension/RemoteDrmChecker$DrmCheckThread;->mMimeType:Ljava/lang/String;

    move-object/from16 v20, v0

    const-string v21, "wma"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v20

    if-nez v20, :cond_9

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/extension/RemoteDrmChecker$DrmCheckThread;->mMimeType:Ljava/lang/String;

    move-object/from16 v20, v0

    const-string v21, "wav"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v20

    if-eqz v20, :cond_f

    .line 245
    :cond_9
    const-string v20, "RemoteDrmChecker"

    const-string v21, " check DrmManagerClient - WMA_PLUGIN_MIME"

    invoke-static/range {v20 .. v21}, Lcom/samsung/android/allshare/DLog;->i_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 246
    const-string v20, "audio/x-ms-wma"

    move-object/from16 v0, v20

    invoke-virtual {v5, v9, v0}, Landroid/drm/DrmManagerClient;->canHandle(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v19

    .line 254
    :cond_a
    :goto_4
    const/16 v20, 0x1

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_12

    .line 255
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/extension/RemoteDrmChecker$DrmCheckThread;->this$0:Lcom/samsung/android/allshare/extension/RemoteDrmChecker;

    move-object/from16 v20, v0

    const/16 v21, 0x1

    # setter for: Lcom/samsung/android/allshare/extension/RemoteDrmChecker;->mIsDrmFile:I
    invoke-static/range {v20 .. v21}, Lcom/samsung/android/allshare/extension/RemoteDrmChecker;->access$002(Lcom/samsung/android/allshare/extension/RemoteDrmChecker;I)I

    .line 260
    :goto_5
    const-string v20, "RemoteDrmChecker"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "  === THIS IS DRM FILE ? "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/extension/RemoteDrmChecker$DrmCheckThread;->this$0:Lcom/samsung/android/allshare/extension/RemoteDrmChecker;

    move-object/from16 v22, v0

    # getter for: Lcom/samsung/android/allshare/extension/RemoteDrmChecker;->mIsDrmFile:I
    invoke-static/range {v22 .. v22}, Lcom/samsung/android/allshare/extension/RemoteDrmChecker;->access$000(Lcom/samsung/android/allshare/extension/RemoteDrmChecker;)I

    move-result v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Lcom/samsung/android/allshare/DLog;->i_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 261
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/extension/RemoteDrmChecker$DrmCheckThread;->this$0:Lcom/samsung/android/allshare/extension/RemoteDrmChecker;

    move-object/from16 v20, v0

    const/16 v21, 0x1

    # setter for: Lcom/samsung/android/allshare/extension/RemoteDrmChecker;->mIsDrmChecked:Z
    invoke-static/range {v20 .. v21}, Lcom/samsung/android/allshare/extension/RemoteDrmChecker;->access$302(Lcom/samsung/android/allshare/extension/RemoteDrmChecker;Z)Z
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_12
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 270
    if-eqz v3, :cond_b

    .line 271
    invoke-virtual {v3}, Landroid/net/http/AndroidHttpClient;->close()V

    .line 273
    :cond_b
    if-eqz v7, :cond_c

    .line 275
    :try_start_6
    invoke-interface {v7}, Lorg/apache/http/HttpEntity;->consumeContent()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_5

    .line 280
    :cond_c
    :goto_6
    if-eqz v12, :cond_d

    .line 282
    :try_start_7
    invoke-virtual {v12}, Ljava/io/FileOutputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_6

    .line 287
    :cond_d
    :goto_7
    if-eqz v15, :cond_1d

    .line 289
    :try_start_8
    invoke-virtual {v15}, Ljava/io/InputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_7

    move-object v11, v12

    .line 296
    .end local v2    # "buf":[B
    .end local v4    # "dirPath":Ljava/io/File;
    .end local v5    # "drmClient":Landroid/drm/DrmManagerClient;
    .end local v8    # "file":Ljava/io/File;
    .end local v12    # "fos":Ljava/io/FileOutputStream;
    .end local v13    # "getRequest":Lorg/apache/http/client/methods/HttpGet;
    .end local v14    # "i":I
    .end local v16    # "inputURL":Ljava/net/URL;
    .end local v17    # "readCount":I
    .end local v18    # "response":Lorg/apache/http/HttpResponse;
    .end local v19    # "temp_isDrmFile":Z
    .restart local v11    # "fos":Ljava/io/FileOutputStream;
    :cond_e
    :goto_8
    :try_start_9
    new-instance v20, Ljava/io/File;

    move-object/from16 v0, v20

    invoke-direct {v0, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v20 .. v20}, Ljava/io/File;->delete()Z
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_11

    .line 301
    :goto_9
    const-string v20, "RemoteDrmChecker"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "  DRM check end : isDrmFile = "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/extension/RemoteDrmChecker$DrmCheckThread;->this$0:Lcom/samsung/android/allshare/extension/RemoteDrmChecker;

    move-object/from16 v22, v0

    # getter for: Lcom/samsung/android/allshare/extension/RemoteDrmChecker;->mIsDrmFile:I
    invoke-static/range {v22 .. v22}, Lcom/samsung/android/allshare/extension/RemoteDrmChecker;->access$000(Lcom/samsung/android/allshare/extension/RemoteDrmChecker;)I

    move-result v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Lcom/samsung/android/allshare/DLog;->i_api(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 247
    .end local v11    # "fos":Ljava/io/FileOutputStream;
    .restart local v2    # "buf":[B
    .restart local v4    # "dirPath":Ljava/io/File;
    .restart local v5    # "drmClient":Landroid/drm/DrmManagerClient;
    .restart local v8    # "file":Ljava/io/File;
    .restart local v12    # "fos":Ljava/io/FileOutputStream;
    .restart local v13    # "getRequest":Lorg/apache/http/client/methods/HttpGet;
    .restart local v14    # "i":I
    .restart local v16    # "inputURL":Ljava/net/URL;
    .restart local v17    # "readCount":I
    .restart local v18    # "response":Lorg/apache/http/HttpResponse;
    .restart local v19    # "temp_isDrmFile":Z
    :cond_f
    :try_start_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/extension/RemoteDrmChecker$DrmCheckThread;->mExtension:Ljava/lang/String;

    move-object/from16 v20, v0

    if-eqz v20, :cond_10

    const-string v20, ".wmv"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/extension/RemoteDrmChecker$DrmCheckThread;->mExtension:Ljava/lang/String;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-nez v20, :cond_11

    :cond_10
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/extension/RemoteDrmChecker$DrmCheckThread;->mMimeType:Ljava/lang/String;

    move-object/from16 v20, v0

    if-eqz v20, :cond_a

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/extension/RemoteDrmChecker$DrmCheckThread;->mMimeType:Ljava/lang/String;

    move-object/from16 v20, v0

    const-string v21, "wmv"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v20

    if-eqz v20, :cond_a

    .line 249
    :cond_11
    const-string v20, "RemoteDrmChecker"

    const-string v21, " check DrmManagerClient - WMV_PLUGIN_MIME"

    invoke-static/range {v20 .. v21}, Lcom/samsung/android/allshare/DLog;->i_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 250
    const-string v20, "video/x-ms-wmv"

    move-object/from16 v0, v20

    invoke-virtual {v5, v9, v0}, Landroid/drm/DrmManagerClient;->canHandle(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v19

    goto/16 :goto_4

    .line 257
    :cond_12
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/extension/RemoteDrmChecker$DrmCheckThread;->this$0:Lcom/samsung/android/allshare/extension/RemoteDrmChecker;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    # setter for: Lcom/samsung/android/allshare/extension/RemoteDrmChecker;->mIsDrmFile:I
    invoke-static/range {v20 .. v21}, Lcom/samsung/android/allshare/extension/RemoteDrmChecker;->access$002(Lcom/samsung/android/allshare/extension/RemoteDrmChecker;I)I
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_3
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_12
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    goto/16 :goto_5

    .line 262
    .end local v2    # "buf":[B
    .end local v5    # "drmClient":Landroid/drm/DrmManagerClient;
    .end local v14    # "i":I
    .end local v17    # "readCount":I
    .end local v19    # "temp_isDrmFile":Z
    :catch_3
    move-exception v6

    move-object v11, v12

    .line 263
    .end local v4    # "dirPath":Ljava/io/File;
    .end local v8    # "file":Ljava/io/File;
    .end local v12    # "fos":Ljava/io/FileOutputStream;
    .end local v13    # "getRequest":Lorg/apache/http/client/methods/HttpGet;
    .end local v16    # "inputURL":Ljava/net/URL;
    .end local v18    # "response":Lorg/apache/http/HttpResponse;
    .restart local v6    # "e":Ljava/io/IOException;
    .restart local v11    # "fos":Ljava/io/FileOutputStream;
    :goto_a
    :try_start_b
    const-string v20, "RemoteDrmChecker"

    const-string v21, " IOException"

    invoke-static/range {v20 .. v21}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 264
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    .line 270
    if-eqz v3, :cond_13

    .line 271
    invoke-virtual {v3}, Landroid/net/http/AndroidHttpClient;->close()V

    .line 273
    :cond_13
    if-eqz v7, :cond_14

    .line 275
    :try_start_c
    invoke-interface {v7}, Lorg/apache/http/HttpEntity;->consumeContent()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_8

    .line 280
    :cond_14
    :goto_b
    if-eqz v11, :cond_15

    .line 282
    :try_start_d
    invoke-virtual {v11}, Ljava/io/FileOutputStream;->close()V
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_9

    .line 287
    :cond_15
    :goto_c
    if-eqz v15, :cond_e

    .line 289
    :try_start_e
    invoke-virtual {v15}, Ljava/io/InputStream;->close()V
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_4

    goto/16 :goto_8

    .line 290
    :catch_4
    move-exception v6

    .line 291
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_8

    .line 276
    .end local v6    # "e":Ljava/io/IOException;
    .end local v11    # "fos":Ljava/io/FileOutputStream;
    .restart local v2    # "buf":[B
    .restart local v4    # "dirPath":Ljava/io/File;
    .restart local v5    # "drmClient":Landroid/drm/DrmManagerClient;
    .restart local v8    # "file":Ljava/io/File;
    .restart local v12    # "fos":Ljava/io/FileOutputStream;
    .restart local v13    # "getRequest":Lorg/apache/http/client/methods/HttpGet;
    .restart local v14    # "i":I
    .restart local v16    # "inputURL":Ljava/net/URL;
    .restart local v17    # "readCount":I
    .restart local v18    # "response":Lorg/apache/http/HttpResponse;
    .restart local v19    # "temp_isDrmFile":Z
    :catch_5
    move-exception v6

    .line 277
    .restart local v6    # "e":Ljava/io/IOException;
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_6

    .line 283
    .end local v6    # "e":Ljava/io/IOException;
    :catch_6
    move-exception v6

    .line 284
    .restart local v6    # "e":Ljava/io/IOException;
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_7

    .line 290
    .end local v6    # "e":Ljava/io/IOException;
    :catch_7
    move-exception v6

    .line 291
    .restart local v6    # "e":Ljava/io/IOException;
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    move-object v11, v12

    .line 292
    .end local v12    # "fos":Ljava/io/FileOutputStream;
    .restart local v11    # "fos":Ljava/io/FileOutputStream;
    goto/16 :goto_8

    .line 276
    .end local v2    # "buf":[B
    .end local v4    # "dirPath":Ljava/io/File;
    .end local v5    # "drmClient":Landroid/drm/DrmManagerClient;
    .end local v8    # "file":Ljava/io/File;
    .end local v13    # "getRequest":Lorg/apache/http/client/methods/HttpGet;
    .end local v14    # "i":I
    .end local v16    # "inputURL":Ljava/net/URL;
    .end local v17    # "readCount":I
    .end local v18    # "response":Lorg/apache/http/HttpResponse;
    .end local v19    # "temp_isDrmFile":Z
    :catch_8
    move-exception v6

    .line 277
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_b

    .line 283
    :catch_9
    move-exception v6

    .line 284
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_c

    .line 265
    .end local v6    # "e":Ljava/io/IOException;
    :catch_a
    move-exception v6

    .line 266
    .local v6, "e":Ljava/lang/Exception;
    :goto_d
    :try_start_f
    const-string v20, "RemoteDrmChecker"

    const-string v21, " Exception"

    invoke-static/range {v20 .. v21}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 267
    invoke-virtual {v6}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_0

    .line 270
    if-eqz v3, :cond_16

    .line 271
    invoke-virtual {v3}, Landroid/net/http/AndroidHttpClient;->close()V

    .line 273
    :cond_16
    if-eqz v7, :cond_17

    .line 275
    :try_start_10
    invoke-interface {v7}, Lorg/apache/http/HttpEntity;->consumeContent()V
    :try_end_10
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_10} :catch_c

    .line 280
    .end local v6    # "e":Ljava/lang/Exception;
    :cond_17
    :goto_e
    if-eqz v11, :cond_18

    .line 282
    :try_start_11
    invoke-virtual {v11}, Ljava/io/FileOutputStream;->close()V
    :try_end_11
    .catch Ljava/io/IOException; {:try_start_11 .. :try_end_11} :catch_d

    .line 287
    :cond_18
    :goto_f
    if-eqz v15, :cond_e

    .line 289
    :try_start_12
    invoke-virtual {v15}, Ljava/io/InputStream;->close()V
    :try_end_12
    .catch Ljava/io/IOException; {:try_start_12 .. :try_end_12} :catch_b

    goto/16 :goto_8

    .line 290
    :catch_b
    move-exception v6

    .line 291
    .local v6, "e":Ljava/io/IOException;
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_8

    .line 276
    .local v6, "e":Ljava/lang/Exception;
    :catch_c
    move-exception v6

    .line 277
    .local v6, "e":Ljava/io/IOException;
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_e

    .line 283
    .end local v6    # "e":Ljava/io/IOException;
    :catch_d
    move-exception v6

    .line 284
    .restart local v6    # "e":Ljava/io/IOException;
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_f

    .line 270
    .end local v6    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v20

    :goto_10
    if-eqz v3, :cond_19

    .line 271
    invoke-virtual {v3}, Landroid/net/http/AndroidHttpClient;->close()V

    .line 273
    :cond_19
    if-eqz v7, :cond_1a

    .line 275
    :try_start_13
    invoke-interface {v7}, Lorg/apache/http/HttpEntity;->consumeContent()V
    :try_end_13
    .catch Ljava/io/IOException; {:try_start_13 .. :try_end_13} :catch_e

    .line 280
    :cond_1a
    :goto_11
    if-eqz v11, :cond_1b

    .line 282
    :try_start_14
    invoke-virtual {v11}, Ljava/io/FileOutputStream;->close()V
    :try_end_14
    .catch Ljava/io/IOException; {:try_start_14 .. :try_end_14} :catch_f

    .line 287
    :cond_1b
    :goto_12
    if-eqz v15, :cond_1c

    .line 289
    :try_start_15
    invoke-virtual {v15}, Ljava/io/InputStream;->close()V
    :try_end_15
    .catch Ljava/io/IOException; {:try_start_15 .. :try_end_15} :catch_10

    .line 292
    :cond_1c
    :goto_13
    throw v20

    .line 276
    :catch_e
    move-exception v6

    .line 277
    .restart local v6    # "e":Ljava/io/IOException;
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_11

    .line 283
    .end local v6    # "e":Ljava/io/IOException;
    :catch_f
    move-exception v6

    .line 284
    .restart local v6    # "e":Ljava/io/IOException;
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_12

    .line 290
    .end local v6    # "e":Ljava/io/IOException;
    :catch_10
    move-exception v6

    .line 291
    .restart local v6    # "e":Ljava/io/IOException;
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_13

    .line 297
    .end local v6    # "e":Ljava/io/IOException;
    :catch_11
    move-exception v6

    .line 298
    .local v6, "e":Ljava/lang/Exception;
    const-string v20, "RemoteDrmChecker"

    const-string v21, " fail to new File( filePath ).delete();"

    invoke-static/range {v20 .. v21}, Lcom/samsung/android/allshare/DLog;->i_api(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_9

    .line 270
    .end local v6    # "e":Ljava/lang/Exception;
    .end local v11    # "fos":Ljava/io/FileOutputStream;
    .restart local v4    # "dirPath":Ljava/io/File;
    .restart local v8    # "file":Ljava/io/File;
    .restart local v12    # "fos":Ljava/io/FileOutputStream;
    .restart local v13    # "getRequest":Lorg/apache/http/client/methods/HttpGet;
    .restart local v16    # "inputURL":Ljava/net/URL;
    .restart local v18    # "response":Lorg/apache/http/HttpResponse;
    :catchall_1
    move-exception v20

    move-object v11, v12

    .end local v12    # "fos":Ljava/io/FileOutputStream;
    .restart local v11    # "fos":Ljava/io/FileOutputStream;
    goto :goto_10

    .line 265
    .end local v11    # "fos":Ljava/io/FileOutputStream;
    .restart local v12    # "fos":Ljava/io/FileOutputStream;
    :catch_12
    move-exception v6

    move-object v11, v12

    .end local v12    # "fos":Ljava/io/FileOutputStream;
    .restart local v11    # "fos":Ljava/io/FileOutputStream;
    goto :goto_d

    .line 262
    .end local v4    # "dirPath":Ljava/io/File;
    .end local v8    # "file":Ljava/io/File;
    .end local v13    # "getRequest":Lorg/apache/http/client/methods/HttpGet;
    .end local v16    # "inputURL":Ljava/net/URL;
    .end local v18    # "response":Lorg/apache/http/HttpResponse;
    :catch_13
    move-exception v6

    goto/16 :goto_a

    .end local v11    # "fos":Ljava/io/FileOutputStream;
    .restart local v2    # "buf":[B
    .restart local v4    # "dirPath":Ljava/io/File;
    .restart local v5    # "drmClient":Landroid/drm/DrmManagerClient;
    .restart local v8    # "file":Ljava/io/File;
    .restart local v12    # "fos":Ljava/io/FileOutputStream;
    .restart local v13    # "getRequest":Lorg/apache/http/client/methods/HttpGet;
    .restart local v14    # "i":I
    .restart local v16    # "inputURL":Ljava/net/URL;
    .restart local v17    # "readCount":I
    .restart local v18    # "response":Lorg/apache/http/HttpResponse;
    .restart local v19    # "temp_isDrmFile":Z
    :cond_1d
    move-object v11, v12

    .end local v12    # "fos":Ljava/io/FileOutputStream;
    .restart local v11    # "fos":Ljava/io/FileOutputStream;
    goto/16 :goto_8
.end method
