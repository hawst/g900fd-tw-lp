.class Lcom/samsung/android/allshare/extension/SECVideoCaption$GetSECCaption;
.super Ljava/lang/Thread;
.source "SECVideoCaption.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/allshare/extension/SECVideoCaption;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GetSECCaption"
.end annotation


# instance fields
.field private mVideoURL:Ljava/lang/String;

.field final synthetic this$0:Lcom/samsung/android/allshare/extension/SECVideoCaption;


# direct methods
.method public constructor <init>(Lcom/samsung/android/allshare/extension/SECVideoCaption;Ljava/lang/String;)V
    .locals 1
    .param p2, "resourceURL"    # Ljava/lang/String;

    .prologue
    .line 99
    iput-object p1, p0, Lcom/samsung/android/allshare/extension/SECVideoCaption$GetSECCaption;->this$0:Lcom/samsung/android/allshare/extension/SECVideoCaption;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 100
    iput-object p2, p0, Lcom/samsung/android/allshare/extension/SECVideoCaption$GetSECCaption;->mVideoURL:Ljava/lang/String;

    .line 101
    const/4 v0, 0x0

    # setter for: Lcom/samsung/android/allshare/extension/SECVideoCaption;->mSubTitleURL:Ljava/lang/String;
    invoke-static {v0}, Lcom/samsung/android/allshare/extension/SECVideoCaption;->access$002(Ljava/lang/String;)Ljava/lang/String;

    .line 102
    return-void
.end method


# virtual methods
.method public run()V
    .locals 12

    .prologue
    .line 105
    const/4 v3, 0x0

    .line 106
    .local v3, "ext":Ljava/lang/String;
    const-string v10, "DMPVideoSubtitle"

    invoke-static {v10}, Landroid/net/http/AndroidHttpClient;->newInstance(Ljava/lang/String;)Landroid/net/http/AndroidHttpClient;

    move-result-object v1

    .line 107
    .local v1, "client":Landroid/net/http/AndroidHttpClient;
    new-instance v4, Lorg/apache/http/client/methods/HttpHead;

    iget-object v10, p0, Lcom/samsung/android/allshare/extension/SECVideoCaption$GetSECCaption;->mVideoURL:Ljava/lang/String;

    invoke-direct {v4, v10}, Lorg/apache/http/client/methods/HttpHead;-><init>(Ljava/lang/String;)V

    .line 108
    .local v4, "head":Lorg/apache/http/client/methods/HttpHead;
    const-string v10, "getCaptionInfo.sec"

    const-string v11, "1"

    invoke-virtual {v4, v10, v11}, Lorg/apache/http/client/methods/HttpHead;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    const-string v10, "getcontentFeatures.dlna.org"

    const-string v11, "1"

    invoke-virtual {v4, v10, v11}, Lorg/apache/http/client/methods/HttpHead;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    if-nez v1, :cond_0

    .line 140
    :goto_0
    return-void

    .line 115
    :cond_0
    :try_start_0
    invoke-virtual {v1, v4}, Landroid/net/http/AndroidHttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v9

    .line 116
    .local v9, "response":Lorg/apache/http/HttpResponse;
    if-eqz v9, :cond_3

    .line 117
    invoke-interface {v9}, Lorg/apache/http/HttpResponse;->getAllHeaders()[Lorg/apache/http/Header;

    move-result-object v6

    .line 118
    .local v6, "headers":[Lorg/apache/http/Header;
    move-object v0, v6

    .local v0, "arr$":[Lorg/apache/http/Header;
    array-length v8, v0

    .local v8, "len$":I
    const/4 v7, 0x0

    .local v7, "i$":I
    :goto_1
    if-ge v7, v8, :cond_3

    aget-object v5, v0, v7

    .line 119
    .local v5, "header":Lorg/apache/http/Header;
    invoke-interface {v5}, Lorg/apache/http/Header;->getName()Ljava/lang/String;

    move-result-object v10

    const-string v11, "CaptionInfo.sec"

    invoke-virtual {v10, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 120
    invoke-interface {v5}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v3

    .line 118
    :cond_1
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 124
    .end local v0    # "arr$":[Lorg/apache/http/Header;
    .end local v5    # "header":Lorg/apache/http/Header;
    .end local v6    # "headers":[Lorg/apache/http/Header;
    .end local v7    # "i$":I
    .end local v8    # "len$":I
    .end local v9    # "response":Lorg/apache/http/HttpResponse;
    :catch_0
    move-exception v2

    .line 125
    .local v2, "e":Ljava/io/IOException;
    const-string v10, "SECVideoCaption"

    const-string v11, "GetSECCaption : IOException"

    invoke-static {v10, v11, v2}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 126
    if-eqz v1, :cond_2

    .line 127
    invoke-virtual {v1}, Landroid/net/http/AndroidHttpClient;->close()V

    .line 128
    :cond_2
    const/4 v1, 0x0

    .line 136
    .end local v2    # "e":Ljava/io/IOException;
    :cond_3
    :goto_2
    if-eqz v1, :cond_4

    .line 137
    invoke-virtual {v1}, Landroid/net/http/AndroidHttpClient;->close()V

    .line 139
    :cond_4
    # setter for: Lcom/samsung/android/allshare/extension/SECVideoCaption;->mSubTitleURL:Ljava/lang/String;
    invoke-static {v3}, Lcom/samsung/android/allshare/extension/SECVideoCaption;->access$002(Ljava/lang/String;)Ljava/lang/String;

    goto :goto_0

    .line 129
    :catch_1
    move-exception v2

    .line 130
    .local v2, "e":Ljava/lang/IllegalArgumentException;
    const-string v10, "SECVideoCaption"

    const-string v11, "GetSECCaption : IllegalArgumentException"

    invoke-static {v10, v11, v2}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 131
    if-eqz v1, :cond_5

    .line 132
    invoke-virtual {v1}, Landroid/net/http/AndroidHttpClient;->close()V

    .line 133
    :cond_5
    const/4 v1, 0x0

    goto :goto_2
.end method
