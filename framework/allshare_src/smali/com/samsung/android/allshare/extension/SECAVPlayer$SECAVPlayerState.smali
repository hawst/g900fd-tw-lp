.class final enum Lcom/samsung/android/allshare/extension/SECAVPlayer$SECAVPlayerState;
.super Ljava/lang/Enum;
.source "SECAVPlayer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/allshare/extension/SECAVPlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "SECAVPlayerState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/android/allshare/extension/SECAVPlayer$SECAVPlayerState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/android/allshare/extension/SECAVPlayer$SECAVPlayerState;

.field public static final enum BUFFERING:Lcom/samsung/android/allshare/extension/SECAVPlayer$SECAVPlayerState;

.field public static final enum FINISHED:Lcom/samsung/android/allshare/extension/SECAVPlayer$SECAVPlayerState;

.field public static final enum PAUSE:Lcom/samsung/android/allshare/extension/SECAVPlayer$SECAVPlayerState;

.field public static final enum PLAYING:Lcom/samsung/android/allshare/extension/SECAVPlayer$SECAVPlayerState;

.field public static final enum STOPPED:Lcom/samsung/android/allshare/extension/SECAVPlayer$SECAVPlayerState;

.field public static final enum UNKNOWN:Lcom/samsung/android/allshare/extension/SECAVPlayer$SECAVPlayerState;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 215
    new-instance v0, Lcom/samsung/android/allshare/extension/SECAVPlayer$SECAVPlayerState;

    const-string v1, "STOPPED"

    invoke-direct {v0, v1, v3}, Lcom/samsung/android/allshare/extension/SECAVPlayer$SECAVPlayerState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/allshare/extension/SECAVPlayer$SECAVPlayerState;->STOPPED:Lcom/samsung/android/allshare/extension/SECAVPlayer$SECAVPlayerState;

    new-instance v0, Lcom/samsung/android/allshare/extension/SECAVPlayer$SECAVPlayerState;

    const-string v1, "FINISHED"

    invoke-direct {v0, v1, v4}, Lcom/samsung/android/allshare/extension/SECAVPlayer$SECAVPlayerState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/allshare/extension/SECAVPlayer$SECAVPlayerState;->FINISHED:Lcom/samsung/android/allshare/extension/SECAVPlayer$SECAVPlayerState;

    new-instance v0, Lcom/samsung/android/allshare/extension/SECAVPlayer$SECAVPlayerState;

    const-string v1, "BUFFERING"

    invoke-direct {v0, v1, v5}, Lcom/samsung/android/allshare/extension/SECAVPlayer$SECAVPlayerState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/allshare/extension/SECAVPlayer$SECAVPlayerState;->BUFFERING:Lcom/samsung/android/allshare/extension/SECAVPlayer$SECAVPlayerState;

    new-instance v0, Lcom/samsung/android/allshare/extension/SECAVPlayer$SECAVPlayerState;

    const-string v1, "PLAYING"

    invoke-direct {v0, v1, v6}, Lcom/samsung/android/allshare/extension/SECAVPlayer$SECAVPlayerState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/allshare/extension/SECAVPlayer$SECAVPlayerState;->PLAYING:Lcom/samsung/android/allshare/extension/SECAVPlayer$SECAVPlayerState;

    new-instance v0, Lcom/samsung/android/allshare/extension/SECAVPlayer$SECAVPlayerState;

    const-string v1, "PAUSE"

    invoke-direct {v0, v1, v7}, Lcom/samsung/android/allshare/extension/SECAVPlayer$SECAVPlayerState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/allshare/extension/SECAVPlayer$SECAVPlayerState;->PAUSE:Lcom/samsung/android/allshare/extension/SECAVPlayer$SECAVPlayerState;

    new-instance v0, Lcom/samsung/android/allshare/extension/SECAVPlayer$SECAVPlayerState;

    const-string v1, "UNKNOWN"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/allshare/extension/SECAVPlayer$SECAVPlayerState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/allshare/extension/SECAVPlayer$SECAVPlayerState;->UNKNOWN:Lcom/samsung/android/allshare/extension/SECAVPlayer$SECAVPlayerState;

    .line 214
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/samsung/android/allshare/extension/SECAVPlayer$SECAVPlayerState;

    sget-object v1, Lcom/samsung/android/allshare/extension/SECAVPlayer$SECAVPlayerState;->STOPPED:Lcom/samsung/android/allshare/extension/SECAVPlayer$SECAVPlayerState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/android/allshare/extension/SECAVPlayer$SECAVPlayerState;->FINISHED:Lcom/samsung/android/allshare/extension/SECAVPlayer$SECAVPlayerState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/android/allshare/extension/SECAVPlayer$SECAVPlayerState;->BUFFERING:Lcom/samsung/android/allshare/extension/SECAVPlayer$SECAVPlayerState;

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/android/allshare/extension/SECAVPlayer$SECAVPlayerState;->PLAYING:Lcom/samsung/android/allshare/extension/SECAVPlayer$SECAVPlayerState;

    aput-object v1, v0, v6

    sget-object v1, Lcom/samsung/android/allshare/extension/SECAVPlayer$SECAVPlayerState;->PAUSE:Lcom/samsung/android/allshare/extension/SECAVPlayer$SECAVPlayerState;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/samsung/android/allshare/extension/SECAVPlayer$SECAVPlayerState;->UNKNOWN:Lcom/samsung/android/allshare/extension/SECAVPlayer$SECAVPlayerState;

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/android/allshare/extension/SECAVPlayer$SECAVPlayerState;->$VALUES:[Lcom/samsung/android/allshare/extension/SECAVPlayer$SECAVPlayerState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 214
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/android/allshare/extension/SECAVPlayer$SECAVPlayerState;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 214
    const-class v0, Lcom/samsung/android/allshare/extension/SECAVPlayer$SECAVPlayerState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/allshare/extension/SECAVPlayer$SECAVPlayerState;

    return-object v0
.end method

.method public static values()[Lcom/samsung/android/allshare/extension/SECAVPlayer$SECAVPlayerState;
    .locals 1

    .prologue
    .line 214
    sget-object v0, Lcom/samsung/android/allshare/extension/SECAVPlayer$SECAVPlayerState;->$VALUES:[Lcom/samsung/android/allshare/extension/SECAVPlayer$SECAVPlayerState;

    invoke-virtual {v0}, [Lcom/samsung/android/allshare/extension/SECAVPlayer$SECAVPlayerState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/allshare/extension/SECAVPlayer$SECAVPlayerState;

    return-object v0
.end method
