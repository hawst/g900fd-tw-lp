.class public Lcom/samsung/android/allshare/extension/ItemExtractor;
.super Ljava/lang/Object;
.source "ItemExtractor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/allshare/extension/ItemExtractor$1;,
        Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;
    }
.end annotation


# static fields
.field private static final CLASS_TAG:Ljava/lang/String; = "ItemExtractor"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    return-void
.end method

.method public static create(Ljava/lang/String;)Lcom/samsung/android/allshare/Item;
    .locals 8
    .param p0, "seedString"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 343
    invoke-static {p0}, Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;->parseSeedStringUsingSplit(Ljava/lang/String;)Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;

    move-result-object v1

    .line 344
    .local v1, "seed":Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;
    if-nez v1, :cond_0

    .line 345
    const-string v5, "ItemExtractor"

    const-string v6, "create : return seed is null"

    invoke-static {v5, v6}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 382
    :goto_0
    return-object v4

    .line 349
    :cond_0
    invoke-virtual {v1}, Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;->getItemType()Ljava/lang/String;

    move-result-object v3

    .line 351
    .local v3, "typeString":Ljava/lang/String;
    invoke-static {v3}, Lcom/samsung/android/allshare/Item$MediaType;->stringToEnum(Ljava/lang/String;)Lcom/samsung/android/allshare/Item$MediaType;

    move-result-object v2

    .line 353
    .local v2, "type":Lcom/samsung/android/allshare/Item$MediaType;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 354
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v5, "BUNDLE_STRING_ITEM_TYPE"

    invoke-virtual {v2}, Lcom/samsung/android/allshare/Item$MediaType;->enumToString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 357
    const-string v5, "BUNDLE_STRING_OBJECT_ID"

    invoke-virtual {v1}, Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;->getObjectID()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 358
    const-string v5, "BUNDLE_STRING_ID"

    invoke-virtual {v1}, Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;->getProviderID()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 359
    const-string v5, "BUNDLE_STRING_ITEM_CONSTRUCTOR_KEY"

    const-string v6, "MEDIA_SERVER"

    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 360
    const-string v5, "BUNDLE_STRING_ITEM_TITLE"

    invoke-virtual {v1}, Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;->getTitle()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 361
    const-string v5, "BUNDLE_PARCELABLE_ITEM_URI"

    invoke-virtual {v1}, Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;->getItemUri()Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 362
    const-string v5, "BUNDLE_STRING_ITEM_MIMETYPE"

    invoke-virtual {v1}, Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;->getMimeType()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 363
    const-string v5, "BUNDLE_LONG_ITEM_FILE_SIZE"

    invoke-virtual {v1}, Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;->getFileSize()J

    move-result-wide v6

    invoke-virtual {v0, v5, v6, v7}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 365
    sget-object v5, Lcom/samsung/android/allshare/extension/ItemExtractor$1;->$SwitchMap$com$samsung$android$allshare$Item$MediaType:[I

    invoke-virtual {v2}, Lcom/samsung/android/allshare/Item$MediaType;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    .line 379
    const-string v5, "ItemExtractor"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "create : type is "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 367
    :pswitch_0
    const-string v4, "BUNDLE_LONG_AUDIO_ITEM_DURATION"

    invoke-virtual {v1}, Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;->getDuration()J

    move-result-wide v6

    invoke-virtual {v0, v4, v6, v7}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 368
    new-instance v4, Lcom/samsung/android/allshare/extension/impl/SimpleAudioItem;

    invoke-direct {v4, v0}, Lcom/samsung/android/allshare/extension/impl/SimpleAudioItem;-><init>(Landroid/os/Bundle;)V

    goto/16 :goto_0

    .line 370
    :pswitch_1
    new-instance v4, Lcom/samsung/android/allshare/extension/impl/SimpleImageItem;

    invoke-direct {v4, v0}, Lcom/samsung/android/allshare/extension/impl/SimpleImageItem;-><init>(Landroid/os/Bundle;)V

    goto/16 :goto_0

    .line 372
    :pswitch_2
    const-string v4, "BUNDLE_PARCELABLE_VIDEO_ITEM_SUBTITLE"

    invoke-virtual {v1}, Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;->getSubtitle()Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 374
    const-string v4, "BUNDLE_LONG_VIDEO_ITEM_DURATION"

    invoke-virtual {v1}, Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;->getDuration()J

    move-result-wide v6

    invoke-virtual {v0, v4, v6, v7}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 375
    new-instance v4, Lcom/samsung/android/allshare/extension/impl/SimpleVideoItem;

    invoke-direct {v4, v0}, Lcom/samsung/android/allshare/extension/impl/SimpleVideoItem;-><init>(Landroid/os/Bundle;)V

    goto/16 :goto_0

    .line 377
    :pswitch_3
    new-instance v4, Lcom/samsung/android/allshare/extension/impl/SimpleFolderItem;

    invoke-direct {v4, v0}, Lcom/samsung/android/allshare/extension/impl/SimpleFolderItem;-><init>(Landroid/os/Bundle;)V

    goto/16 :goto_0

    .line 365
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static extract(Lcom/samsung/android/allshare/Item;)Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;
    .locals 20
    .param p0, "item"    # Lcom/samsung/android/allshare/Item;

    .prologue
    .line 395
    if-nez p0, :cond_0

    .line 396
    const-string v17, "ItemExtractor"

    const-string v18, "extract : return item is null"

    invoke-static/range {v17 .. v18}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 397
    const/4 v11, 0x0

    .line 465
    :goto_0
    return-object v11

    .line 400
    :cond_0
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v9

    .line 401
    .local v9, "p":Landroid/os/Parcel;
    const/16 v17, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v9, v1}, Lcom/samsung/android/allshare/Item;->writeToParcel(Landroid/os/Parcel;I)V

    .line 403
    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-virtual {v9, v0}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 404
    invoke-virtual {v9}, Landroid/os/Parcel;->readBundle()Landroid/os/Bundle;

    move-result-object v2

    .line 405
    .local v2, "bundle":Landroid/os/Bundle;
    invoke-virtual {v9}, Landroid/os/Parcel;->recycle()V

    .line 407
    const-string v17, "BUNDLE_STRING_ITEM_TYPE"

    move-object/from16 v0, v17

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 408
    .local v15, "typeString":Ljava/lang/String;
    invoke-static {v15}, Lcom/samsung/android/allshare/Item$MediaType;->stringToEnum(Ljava/lang/String;)Lcom/samsung/android/allshare/Item$MediaType;

    move-result-object v14

    .line 410
    .local v14, "type":Lcom/samsung/android/allshare/Item$MediaType;
    const-string v17, "BUNDLE_STRING_OBJECT_ID"

    move-object/from16 v0, v17

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 411
    .local v8, "objId":Ljava/lang/String;
    const-string v17, "BUNDLE_STRING_ID"

    move-object/from16 v0, v17

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 412
    .local v10, "providerId":Ljava/lang/String;
    const-string v17, "BUNDLE_STRING_ITEM_CONSTRUCTOR_KEY"

    move-object/from16 v0, v17

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 413
    .local v3, "constructorKey":Ljava/lang/String;
    const-string v17, "BUNDLE_STRING_ITEM_TITLE"

    move-object/from16 v0, v17

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 414
    .local v13, "title":Ljava/lang/String;
    const-string v17, "BUNDLE_PARCELABLE_ITEM_URI"

    move-object/from16 v0, v17

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v16

    check-cast v16, Landroid/net/Uri;

    .line 415
    .local v16, "uri":Landroid/net/Uri;
    const/4 v12, 0x0

    .line 416
    .local v12, "subtitle":Landroid/net/Uri;
    const-string v17, "BUNDLE_STRING_ITEM_MIMETYPE"

    move-object/from16 v0, v17

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 417
    .local v7, "mime":Ljava/lang/String;
    const-string v17, "BUNDLE_LONG_ITEM_FILE_SIZE"

    move-object/from16 v0, v17

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v18

    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    .line 418
    .local v6, "filesize":Ljava/lang/Long;
    const-wide/16 v4, -0x1

    .line 420
    .local v4, "duration":J
    sget-object v17, Lcom/samsung/android/allshare/extension/ItemExtractor$1;->$SwitchMap$com$samsung$android$allshare$Item$MediaType:[I

    invoke-virtual {v14}, Lcom/samsung/android/allshare/Item$MediaType;->ordinal()I

    move-result v18

    aget v17, v17, v18

    packed-switch v17, :pswitch_data_0

    .line 435
    :goto_1
    :pswitch_0
    if-eqz v8, :cond_1

    invoke-virtual {v8}, Ljava/lang/String;->isEmpty()Z

    move-result v17

    if-nez v17, :cond_1

    if-eqz v10, :cond_1

    invoke-virtual {v10}, Ljava/lang/String;->isEmpty()Z

    move-result v17

    if-eqz v17, :cond_2

    .line 436
    :cond_1
    const-string v17, "ItemExtractor"

    const-string v18, "extract : return something is empty"

    invoke-static/range {v17 .. v18}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 437
    const/4 v11, 0x0

    goto/16 :goto_0

    .line 422
    :pswitch_1
    const-string v17, "BUNDLE_PARCELABLE_VIDEO_ITEM_SUBTITLE"

    move-object/from16 v0, v17

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v12

    .end local v12    # "subtitle":Landroid/net/Uri;
    check-cast v12, Landroid/net/Uri;

    .line 423
    .restart local v12    # "subtitle":Landroid/net/Uri;
    const-string v17, "BUNDLE_LONG_VIDEO_ITEM_DURATION"

    move-object/from16 v0, v17

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 424
    goto :goto_1

    .line 427
    :pswitch_2
    const-string v17, "BUNDLE_LONG_AUDIO_ITEM_DURATION"

    move-object/from16 v0, v17

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 428
    goto :goto_1

    .line 440
    :cond_2
    const-string v17, "MEDIA_SERVER"

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-nez v17, :cond_3

    .line 441
    const-string v17, "ItemExtractor"

    const-string v18, "ItemExtractor support only MEDIA_SERVER Item"

    invoke-static/range {v17 .. v18}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 442
    new-instance v17, Ljava/lang/IllegalArgumentException;

    const-string v18, "ItemExtractor support only MEDIA_SERVER Item"

    invoke-direct/range {v17 .. v18}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v17

    .line 445
    :cond_3
    const-string v17, ",@,#,"

    move-object/from16 v0, v17

    invoke-virtual {v8, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v17

    if-nez v17, :cond_4

    const-string v17, ",@,#,"

    move-object/from16 v0, v17

    invoke-virtual {v10, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v17

    if-nez v17, :cond_4

    const-string v17, ",@,#,"

    move-object/from16 v0, v17

    invoke-virtual {v13, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v17

    if-eqz v17, :cond_5

    .line 447
    :cond_4
    const-string v17, "ItemExtractor"

    const-string v18, "ItemExtractor doesn\'t suppport object id or provider id that contains DELIMITER"

    invoke-static/range {v17 .. v18}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 449
    const/4 v11, 0x0

    goto/16 :goto_0

    .line 454
    :cond_5
    new-instance v11, Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;

    const/16 v17, 0x0

    move-object/from16 v0, v17

    invoke-direct {v11, v0}, Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;-><init>(Lcom/samsung/android/allshare/extension/ItemExtractor$1;)V

    .line 455
    .local v11, "seed":Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/allshare/Item;->getType()Lcom/samsung/android/allshare/Item$MediaType;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lcom/samsung/android/allshare/Item$MediaType;->enumToString()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    # setter for: Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;->mItemType:Ljava/lang/String;
    invoke-static {v11, v0}, Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;->access$102(Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;Ljava/lang/String;)Ljava/lang/String;

    .line 456
    # setter for: Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;->mObjectId:Ljava/lang/String;
    invoke-static {v11, v8}, Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;->access$202(Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;Ljava/lang/String;)Ljava/lang/String;

    .line 457
    # setter for: Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;->mProviderId:Ljava/lang/String;
    invoke-static {v11, v10}, Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;->access$302(Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;Ljava/lang/String;)Ljava/lang/String;

    .line 458
    # setter for: Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;->mTitle:Ljava/lang/String;
    invoke-static {v11, v13}, Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;->access$402(Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;Ljava/lang/String;)Ljava/lang/String;

    .line 459
    # setter for: Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;->mSubtitle:Landroid/net/Uri;
    invoke-static {v11, v12}, Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;->access$502(Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;Landroid/net/Uri;)Landroid/net/Uri;

    .line 460
    # setter for: Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;->mDuration:J
    invoke-static {v11, v4, v5}, Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;->access$602(Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;J)J

    .line 461
    move-object/from16 v0, v16

    # setter for: Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;->mItemUri:Landroid/net/Uri;
    invoke-static {v11, v0}, Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;->access$702(Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;Landroid/net/Uri;)Landroid/net/Uri;

    .line 462
    # setter for: Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;->mMimeType:Ljava/lang/String;
    invoke-static {v11, v7}, Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;->access$802(Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;Ljava/lang/String;)Ljava/lang/String;

    .line 463
    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v18

    move-wide/from16 v0, v18

    # setter for: Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;->mFileSize:J
    invoke-static {v11, v0, v1}, Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;->access$902(Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;J)J

    goto/16 :goto_0

    .line 420
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
