.class public Lcom/samsung/android/allshare/extension/SECVideoCaption;
.super Ljava/lang/Object;
.source "SECVideoCaption.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/allshare/extension/SECVideoCaption$GetSECCaption;
    }
.end annotation


# static fields
.field private static mSubTitleURL:Ljava/lang/String;


# instance fields
.field private final TAG_CLASS:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    const-string v0, "SECVideoCaption"

    iput-object v0, p0, Lcom/samsung/android/allshare/extension/SECVideoCaption;->TAG_CLASS:Ljava/lang/String;

    .line 96
    return-void
.end method

.method static synthetic access$002(Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Ljava/lang/String;

    .prologue
    .line 63
    sput-object p0, Lcom/samsung/android/allshare/extension/SECVideoCaption;->mSubTitleURL:Ljava/lang/String;

    return-object p0
.end method


# virtual methods
.method public getSubTitleURL(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "resourceURL"    # Ljava/lang/String;

    .prologue
    .line 80
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 81
    :cond_0
    const/4 v2, 0x0

    .line 93
    :goto_0
    return-object v2

    .line 84
    :cond_1
    new-instance v1, Lcom/samsung/android/allshare/extension/SECVideoCaption$GetSECCaption;

    invoke-direct {v1, p0, p1}, Lcom/samsung/android/allshare/extension/SECVideoCaption$GetSECCaption;-><init>(Lcom/samsung/android/allshare/extension/SECVideoCaption;Ljava/lang/String;)V

    .line 85
    .local v1, "mGetSubTitleThread":Lcom/samsung/android/allshare/extension/SECVideoCaption$GetSECCaption;
    invoke-virtual {v1}, Lcom/samsung/android/allshare/extension/SECVideoCaption$GetSECCaption;->start()V

    .line 88
    const-wide/16 v2, 0xbb8

    :try_start_0
    invoke-virtual {v1, v2, v3}, Lcom/samsung/android/allshare/extension/SECVideoCaption$GetSECCaption;->join(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 93
    :goto_1
    sget-object v2, Lcom/samsung/android/allshare/extension/SECVideoCaption;->mSubTitleURL:Ljava/lang/String;

    goto :goto_0

    .line 89
    :catch_0
    move-exception v0

    .line 90
    .local v0, "e":Ljava/lang/InterruptedException;
    const-string v2, "SECVideoCaption"

    const-string v3, "getSubTitleURL : InterruptedException"

    invoke-static {v2, v3, v0}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_1
.end method
