.class Lcom/samsung/android/allshare/extension/SECDownloader$1;
.super Ljava/lang/Object;
.source "SECDownloader.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/allshare/extension/SECDownloader;->downloadRemains(Ljava/lang/String;Ljava/util/ArrayList;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/allshare/extension/SECDownloader;

.field final synthetic val$itemList:Ljava/util/ArrayList;

.field final synthetic val$serverName:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/samsung/android/allshare/extension/SECDownloader;Ljava/util/ArrayList;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 116
    iput-object p1, p0, Lcom/samsung/android/allshare/extension/SECDownloader$1;->this$0:Lcom/samsung/android/allshare/extension/SECDownloader;

    iput-object p2, p0, Lcom/samsung/android/allshare/extension/SECDownloader$1;->val$itemList:Ljava/util/ArrayList;

    iput-object p3, p0, Lcom/samsung/android/allshare/extension/SECDownloader$1;->val$serverName:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    .prologue
    .line 119
    const-string v7, "SECDownloader"

    const-string v8, "downloadRemains, Thread Start!!!"

    invoke-static {v7, v8}, Lcom/samsung/android/allshare/DLog;->i_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    const/16 v1, 0x32

    .line 123
    .local v1, "i":I
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 125
    .local v0, "bundleList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/os/Bundle;>;"
    :goto_0
    iget-object v7, p0, Lcom/samsung/android/allshare/extension/SECDownloader$1;->val$itemList:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-ge v1, v7, :cond_1

    .line 126
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v7

    const/16 v8, 0x32

    if-lt v7, v8, :cond_2

    .line 136
    :cond_1
    new-instance v4, Lcom/sec/android/allshare/iface/CVMessage;

    invoke-direct {v4}, Lcom/sec/android/allshare/iface/CVMessage;-><init>()V

    .line 137
    .local v4, "req_msg":Lcom/sec/android/allshare/iface/CVMessage;
    const-string v7, "com.sec.android.allshare.action.ACTION_DOWNLOAD_REQUEST"

    invoke-virtual {v4, v7}, Lcom/sec/android/allshare/iface/CVMessage;->setActionID(Ljava/lang/String;)V

    .line 139
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 140
    .local v3, "req_bundle":Landroid/os/Bundle;
    const-string v7, "BUNDLE_STRING_DEVICE_NAME"

    iget-object v8, p0, Lcom/samsung/android/allshare/extension/SECDownloader$1;->val$serverName:Ljava/lang/String;

    invoke-virtual {v3, v7, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    const-string v7, "BUNDLE_PARCELABLE_ARRAYLIST_CONTENT_URI"

    invoke-virtual {v3, v7, v0}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 143
    invoke-virtual {v4, v3}, Lcom/sec/android/allshare/iface/CVMessage;->setBundle(Landroid/os/Bundle;)V

    .line 145
    iget-object v7, p0, Lcom/samsung/android/allshare/extension/SECDownloader$1;->this$0:Lcom/samsung/android/allshare/extension/SECDownloader;

    # getter for: Lcom/samsung/android/allshare/extension/SECDownloader;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;
    invoke-static {v7}, Lcom/samsung/android/allshare/extension/SECDownloader;->access$000(Lcom/samsung/android/allshare/extension/SECDownloader;)Lcom/samsung/android/allshare/IAllShareConnector;

    move-result-object v7

    invoke-interface {v7, v4}, Lcom/samsung/android/allshare/IAllShareConnector;->requestCVMSync(Lcom/sec/android/allshare/iface/CVMessage;)Lcom/sec/android/allshare/iface/CVMessage;

    move-result-object v6

    .line 146
    .local v6, "res_msg":Lcom/sec/android/allshare/iface/CVMessage;
    if-nez v6, :cond_4

    .line 147
    const-string v7, "SECDownloader"

    const-string v8, "downloadRemains, res_msg is null"

    invoke-static {v7, v8}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 163
    :goto_1
    const-string v7, "SECDownloader"

    const-string v8, "downloadRemains, Thread End!!!"

    invoke-static {v7, v8}, Lcom/samsung/android/allshare/DLog;->i_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 164
    return-void

    .line 128
    .end local v3    # "req_bundle":Landroid/os/Bundle;
    .end local v4    # "req_msg":Lcom/sec/android/allshare/iface/CVMessage;
    .end local v6    # "res_msg":Lcom/sec/android/allshare/iface/CVMessage;
    :cond_2
    iget-object v7, p0, Lcom/samsung/android/allshare/extension/SECDownloader$1;->val$itemList:Ljava/util/ArrayList;

    invoke-virtual {v7, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/allshare/Item;

    .line 129
    .local v2, "item":Lcom/samsung/android/allshare/Item;
    if-eqz v2, :cond_3

    .line 130
    instance-of v7, v2, Lcom/sec/android/allshare/iface/IBundleHolder;

    if-eqz v7, :cond_3

    .line 131
    check-cast v2, Lcom/sec/android/allshare/iface/IBundleHolder;

    .end local v2    # "item":Lcom/samsung/android/allshare/Item;
    invoke-interface {v2}, Lcom/sec/android/allshare/iface/IBundleHolder;->getBundle()Landroid/os/Bundle;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 125
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 151
    .restart local v3    # "req_bundle":Landroid/os/Bundle;
    .restart local v4    # "req_msg":Lcom/sec/android/allshare/iface/CVMessage;
    .restart local v6    # "res_msg":Lcom/sec/android/allshare/iface/CVMessage;
    :cond_4
    invoke-virtual {v6}, Lcom/sec/android/allshare/iface/CVMessage;->getBundle()Landroid/os/Bundle;

    move-result-object v5

    .line 153
    .local v5, "res_bundle":Landroid/os/Bundle;
    if-nez v5, :cond_5

    .line 154
    const-string v7, "SECDownloader"

    const-string v8, "downloadRemains, res_bundle is null"

    invoke-static {v7, v8}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 158
    :cond_5
    iget-object v7, p0, Lcom/samsung/android/allshare/extension/SECDownloader$1;->val$itemList:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-ne v1, v7, :cond_0

    .line 159
    const-string v7, "SECDownloader"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "downloadRemains, finish size = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/samsung/android/allshare/DLog;->i_api(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method
