.class public Lcom/samsung/android/allshare/extension/FlatProvider;
.super Lcom/samsung/android/allshare/media/Provider;
.source "FlatProvider.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/allshare/extension/FlatProvider$IFlatProviderConnection;
    }
.end annotation


# instance fields
.field private mFlatBorwseWorker:Lcom/samsung/android/allshare/extension/impl/IMediaGetter;

.field private mProvider:Lcom/samsung/android/allshare/media/Provider;


# direct methods
.method public constructor <init>(Lcom/samsung/android/allshare/media/Provider;)V
    .locals 1
    .param p1, "decoratedProvider"    # Lcom/samsung/android/allshare/media/Provider;

    .prologue
    const/4 v0, 0x0

    .line 95
    invoke-direct {p0}, Lcom/samsung/android/allshare/media/Provider;-><init>()V

    .line 42
    iput-object v0, p0, Lcom/samsung/android/allshare/extension/FlatProvider;->mProvider:Lcom/samsung/android/allshare/media/Provider;

    .line 44
    iput-object v0, p0, Lcom/samsung/android/allshare/extension/FlatProvider;->mFlatBorwseWorker:Lcom/samsung/android/allshare/extension/impl/IMediaGetter;

    .line 96
    iput-object p1, p0, Lcom/samsung/android/allshare/extension/FlatProvider;->mProvider:Lcom/samsung/android/allshare/media/Provider;

    .line 97
    invoke-static {p0}, Lcom/samsung/android/allshare/extension/impl/MediaGetterManager;->createMediaGetter(Lcom/samsung/android/allshare/media/Provider;)Lcom/samsung/android/allshare/extension/impl/IMediaGetter;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/allshare/extension/FlatProvider;->mFlatBorwseWorker:Lcom/samsung/android/allshare/extension/impl/IMediaGetter;

    .line 98
    return-void
.end method


# virtual methods
.method public browse(Lcom/samsung/android/allshare/Item;II)V
    .locals 1
    .param p1, "parentFolderItem"    # Lcom/samsung/android/allshare/Item;
    .param p2, "startIndex"    # I
    .param p3, "requestCount"    # I

    .prologue
    .line 175
    iget-object v0, p0, Lcom/samsung/android/allshare/extension/FlatProvider;->mProvider:Lcom/samsung/android/allshare/media/Provider;

    invoke-virtual {v0, p1, p2, p3}, Lcom/samsung/android/allshare/media/Provider;->browse(Lcom/samsung/android/allshare/Item;II)V

    .line 176
    return-void
.end method

.method public cancelFlatBrowse(Lcom/samsung/android/allshare/extension/FlatProvider$IFlatProviderConnection;)V
    .locals 1
    .param p1, "conn"    # Lcom/samsung/android/allshare/extension/FlatProvider$IFlatProviderConnection;

    .prologue
    .line 149
    iget-object v0, p0, Lcom/samsung/android/allshare/extension/FlatProvider;->mFlatBorwseWorker:Lcom/samsung/android/allshare/extension/impl/IMediaGetter;

    invoke-interface {v0, p1}, Lcom/samsung/android/allshare/extension/impl/IMediaGetter;->cancel(Lcom/samsung/android/allshare/extension/FlatProvider$IFlatProviderConnection;)V

    .line 150
    return-void
.end method

.method public getDeviceDomain()Lcom/samsung/android/allshare/Device$DeviceDomain;
    .locals 1

    .prologue
    .line 187
    iget-object v0, p0, Lcom/samsung/android/allshare/extension/FlatProvider;->mProvider:Lcom/samsung/android/allshare/media/Provider;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/media/Provider;->getDeviceDomain()Lcom/samsung/android/allshare/Device$DeviceDomain;

    move-result-object v0

    return-object v0
.end method

.method public getDeviceType()Lcom/samsung/android/allshare/Device$DeviceType;
    .locals 1

    .prologue
    .line 199
    iget-object v0, p0, Lcom/samsung/android/allshare/extension/FlatProvider;->mProvider:Lcom/samsung/android/allshare/media/Provider;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/media/Provider;->getDeviceType()Lcom/samsung/android/allshare/Device$DeviceType;

    move-result-object v0

    return-object v0
.end method

.method public getID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 214
    iget-object v0, p0, Lcom/samsung/android/allshare/extension/FlatProvider;->mProvider:Lcom/samsung/android/allshare/media/Provider;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/media/Provider;->getID()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getIPAddress()Ljava/lang/String;
    .locals 1

    .prologue
    .line 243
    iget-object v0, p0, Lcom/samsung/android/allshare/extension/FlatProvider;->mProvider:Lcom/samsung/android/allshare/media/Provider;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/media/Provider;->getIPAddress()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getIPAdress()Ljava/lang/String;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 229
    invoke-virtual {p0}, Lcom/samsung/android/allshare/extension/FlatProvider;->getIPAddress()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getIcon()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 257
    iget-object v0, p0, Lcom/samsung/android/allshare/extension/FlatProvider;->mProvider:Lcom/samsung/android/allshare/media/Provider;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/media/Provider;->getIcon()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public getIconList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/Icon;",
            ">;"
        }
    .end annotation

    .prologue
    .line 271
    iget-object v0, p0, Lcom/samsung/android/allshare/extension/FlatProvider;->mProvider:Lcom/samsung/android/allshare/media/Provider;

    if-nez v0, :cond_0

    .line 272
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 274
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/allshare/extension/FlatProvider;->mProvider:Lcom/samsung/android/allshare/media/Provider;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/media/Provider;->getIconList()Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_0
.end method

.method public getModelName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 288
    iget-object v0, p0, Lcom/samsung/android/allshare/extension/FlatProvider;->mProvider:Lcom/samsung/android/allshare/media/Provider;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/media/Provider;->getModelName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getNIC()Ljava/lang/String;
    .locals 1

    .prologue
    .line 414
    iget-object v0, p0, Lcom/samsung/android/allshare/extension/FlatProvider;->mProvider:Lcom/samsung/android/allshare/media/Provider;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/media/Provider;->getNIC()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 303
    iget-object v0, p0, Lcom/samsung/android/allshare/extension/FlatProvider;->mProvider:Lcom/samsung/android/allshare/media/Provider;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/media/Provider;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getP2pMacAddress()Ljava/lang/String;
    .locals 1

    .prologue
    .line 436
    const-string v0, ""

    return-object v0
.end method

.method public getReceiver()Lcom/samsung/android/allshare/media/Receiver;
    .locals 1

    .prologue
    .line 317
    iget-object v0, p0, Lcom/samsung/android/allshare/extension/FlatProvider;->mProvider:Lcom/samsung/android/allshare/media/Provider;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/media/Provider;->getReceiver()Lcom/samsung/android/allshare/media/Receiver;

    move-result-object v0

    return-object v0
.end method

.method public getRootFolder()Lcom/samsung/android/allshare/Item;
    .locals 1

    .prologue
    .line 329
    iget-object v0, p0, Lcom/samsung/android/allshare/extension/FlatProvider;->mProvider:Lcom/samsung/android/allshare/media/Provider;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/media/Provider;->getRootFolder()Lcom/samsung/android/allshare/Item;

    move-result-object v0

    return-object v0
.end method

.method public isSearchable()Z
    .locals 1

    .prologue
    .line 342
    iget-object v0, p0, Lcom/samsung/android/allshare/extension/FlatProvider;->mProvider:Lcom/samsung/android/allshare/media/Provider;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/media/Provider;->isSearchable()Z

    move-result v0

    return v0
.end method

.method public isSeekableOnPaused()Z
    .locals 1

    .prologue
    .line 421
    const/4 v0, 0x0

    return v0
.end method

.method public isWholeHomeAudio()Z
    .locals 1

    .prologue
    .line 427
    const/4 v0, 0x0

    return v0
.end method

.method public search(Lcom/samsung/android/allshare/media/SearchCriteria;II)V
    .locals 1
    .param p1, "searchCriteria"    # Lcom/samsung/android/allshare/media/SearchCriteria;
    .param p2, "startIndex"    # I
    .param p3, "requestCount"    # I

    .prologue
    .line 376
    iget-object v0, p0, Lcom/samsung/android/allshare/extension/FlatProvider;->mProvider:Lcom/samsung/android/allshare/media/Provider;

    invoke-virtual {v0, p1, p2, p3}, Lcom/samsung/android/allshare/media/Provider;->search(Lcom/samsung/android/allshare/media/SearchCriteria;II)V

    .line 378
    return-void
.end method

.method public setBrowseItemsResponseListener(Lcom/samsung/android/allshare/media/Provider$IProviderBrowseResponseListener;)V
    .locals 1
    .param p1, "l"    # Lcom/samsung/android/allshare/media/Provider$IProviderBrowseResponseListener;

    .prologue
    .line 389
    iget-object v0, p0, Lcom/samsung/android/allshare/extension/FlatProvider;->mProvider:Lcom/samsung/android/allshare/media/Provider;

    invoke-virtual {v0, p1}, Lcom/samsung/android/allshare/media/Provider;->setBrowseItemsResponseListener(Lcom/samsung/android/allshare/media/Provider$IProviderBrowseResponseListener;)V

    .line 390
    return-void
.end method

.method public setEventListener(Lcom/samsung/android/allshare/media/Provider$IProviderEventListener;)V
    .locals 1
    .param p1, "l"    # Lcom/samsung/android/allshare/media/Provider$IProviderEventListener;

    .prologue
    .line 354
    iget-object v0, p0, Lcom/samsung/android/allshare/extension/FlatProvider;->mProvider:Lcom/samsung/android/allshare/media/Provider;

    invoke-virtual {v0, p1}, Lcom/samsung/android/allshare/media/Provider;->setEventListener(Lcom/samsung/android/allshare/media/Provider$IProviderEventListener;)V

    .line 355
    return-void
.end method

.method public setSearchResponseListener(Lcom/samsung/android/allshare/media/Provider$IProviderSearchResponseListener;)V
    .locals 1
    .param p1, "l"    # Lcom/samsung/android/allshare/media/Provider$IProviderSearchResponseListener;

    .prologue
    .line 402
    iget-object v0, p0, Lcom/samsung/android/allshare/extension/FlatProvider;->mProvider:Lcom/samsung/android/allshare/media/Provider;

    invoke-virtual {v0, p1}, Lcom/samsung/android/allshare/media/Provider;->setSearchResponseListener(Lcom/samsung/android/allshare/media/Provider$IProviderSearchResponseListener;)V

    .line 403
    return-void
.end method

.method startBrowseMedeaGetterFlatBrowse(Ljava/util/ArrayList;Lcom/samsung/android/allshare/extension/FlatProvider$IFlatProviderConnection;)V
    .locals 1
    .param p2, "conn"    # Lcom/samsung/android/allshare/extension/FlatProvider$IFlatProviderConnection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/Item$MediaType;",
            ">;",
            "Lcom/samsung/android/allshare/extension/FlatProvider$IFlatProviderConnection;",
            ")V"
        }
    .end annotation

    .prologue
    .line 124
    .local p1, "types":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/Item$MediaType;>;"
    iget-object v0, p0, Lcom/samsung/android/allshare/extension/FlatProvider;->mFlatBorwseWorker:Lcom/samsung/android/allshare/extension/impl/IMediaGetter;

    invoke-interface {v0, p0, p1, p2}, Lcom/samsung/android/allshare/extension/impl/IMediaGetter;->start(Lcom/samsung/android/allshare/media/Provider;Ljava/util/ArrayList;Lcom/samsung/android/allshare/extension/FlatProvider$IFlatProviderConnection;)V

    .line 125
    return-void
.end method

.method public startFlatBrowse(Lcom/samsung/android/allshare/Item$MediaType;Lcom/samsung/android/allshare/extension/FlatProvider$IFlatProviderConnection;)V
    .locals 1
    .param p1, "type"    # Lcom/samsung/android/allshare/Item$MediaType;
    .param p2, "conn"    # Lcom/samsung/android/allshare/extension/FlatProvider$IFlatProviderConnection;

    .prologue
    .line 111
    iget-object v0, p0, Lcom/samsung/android/allshare/extension/FlatProvider;->mFlatBorwseWorker:Lcom/samsung/android/allshare/extension/impl/IMediaGetter;

    invoke-interface {v0, p0, p1, p2}, Lcom/samsung/android/allshare/extension/impl/IMediaGetter;->start(Lcom/samsung/android/allshare/media/Provider;Lcom/samsung/android/allshare/Item$MediaType;Lcom/samsung/android/allshare/extension/FlatProvider$IFlatProviderConnection;)V

    .line 112
    return-void
.end method

.method public startFlatBrowse(Ljava/util/ArrayList;Lcom/samsung/android/allshare/extension/FlatProvider$IFlatProviderConnection;)V
    .locals 1
    .param p2, "conn"    # Lcom/samsung/android/allshare/extension/FlatProvider$IFlatProviderConnection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/Item$MediaType;",
            ">;",
            "Lcom/samsung/android/allshare/extension/FlatProvider$IFlatProviderConnection;",
            ")V"
        }
    .end annotation

    .prologue
    .line 137
    .local p1, "types":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/Item$MediaType;>;"
    iget-object v0, p0, Lcom/samsung/android/allshare/extension/FlatProvider;->mFlatBorwseWorker:Lcom/samsung/android/allshare/extension/impl/IMediaGetter;

    invoke-interface {v0, p0, p1, p2}, Lcom/samsung/android/allshare/extension/impl/IMediaGetter;->start(Lcom/samsung/android/allshare/media/Provider;Ljava/util/ArrayList;Lcom/samsung/android/allshare/extension/FlatProvider$IFlatProviderConnection;)V

    .line 138
    return-void
.end method
