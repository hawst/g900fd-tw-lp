.class public Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;
.super Ljava/lang/Object;
.source "ItemExtractor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/allshare/extension/ItemExtractor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Seed"
.end annotation


# static fields
.field private static final DELIMITER:Ljava/lang/String; = ",@,#,"

.field private static final FIELD_NUMBER:I = 0x9


# instance fields
.field private mDuration:J

.field private mFileSize:J

.field private mItemType:Ljava/lang/String;

.field private mItemUri:Landroid/net/Uri;

.field private mMimeType:Ljava/lang/String;

.field private mObjectId:Ljava/lang/String;

.field private mProviderId:Ljava/lang/String;

.field private mSubtitle:Landroid/net/Uri;

.field private mTitle:Ljava/lang/String;


# direct methods
.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;->mObjectId:Ljava/lang/String;

    .line 50
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;->mProviderId:Ljava/lang/String;

    .line 52
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;->mItemType:Ljava/lang/String;

    .line 54
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;->mTitle:Ljava/lang/String;

    .line 56
    iput-object v2, p0, Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;->mSubtitle:Landroid/net/Uri;

    .line 58
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;->mDuration:J

    .line 60
    iput-object v2, p0, Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;->mItemUri:Landroid/net/Uri;

    .line 62
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;->mMimeType:Ljava/lang/String;

    .line 64
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;->mFileSize:J

    .line 67
    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/allshare/extension/ItemExtractor$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/android/allshare/extension/ItemExtractor$1;

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;-><init>()V

    return-void
.end method

.method static synthetic access$102(Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 43
    iput-object p1, p0, Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;->mItemType:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$202(Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 43
    iput-object p1, p0, Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;->mObjectId:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$302(Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 43
    iput-object p1, p0, Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;->mProviderId:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$402(Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 43
    iput-object p1, p0, Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;->mTitle:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$502(Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;Landroid/net/Uri;)Landroid/net/Uri;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;
    .param p1, "x1"    # Landroid/net/Uri;

    .prologue
    .line 43
    iput-object p1, p0, Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;->mSubtitle:Landroid/net/Uri;

    return-object p1
.end method

.method static synthetic access$602(Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;J)J
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;
    .param p1, "x1"    # J

    .prologue
    .line 43
    iput-wide p1, p0, Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;->mDuration:J

    return-wide p1
.end method

.method static synthetic access$702(Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;Landroid/net/Uri;)Landroid/net/Uri;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;
    .param p1, "x1"    # Landroid/net/Uri;

    .prologue
    .line 43
    iput-object p1, p0, Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;->mItemUri:Landroid/net/Uri;

    return-object p1
.end method

.method static synthetic access$802(Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 43
    iput-object p1, p0, Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;->mMimeType:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$902(Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;J)J
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;
    .param p1, "x1"    # J

    .prologue
    .line 43
    iput-wide p1, p0, Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;->mFileSize:J

    return-wide p1
.end method

.method public static parseSeedString(Ljava/lang/String;)Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;
    .locals 21
    .param p0, "seedString"    # Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 184
    const-string v18, "ItemExtractor"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "parseSeedString : "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/samsung/android/allshare/DLog;->v_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 186
    new-instance v13, Ljava/util/StringTokenizer;

    const-string v18, ",@,#,"

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v13, v0, v1}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 187
    .local v13, "st":Ljava/util/StringTokenizer;
    invoke-virtual {v13}, Ljava/util/StringTokenizer;->countTokens()I

    move-result v2

    .line 189
    .local v2, "count":I
    const/16 v18, 0x9

    move/from16 v0, v18

    if-eq v2, v0, :cond_0

    .line 190
    const-string v18, "ItemExtractor"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "count : "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 191
    const/4 v12, 0x0

    .line 252
    :goto_0
    return-object v12

    .line 194
    :cond_0
    invoke-virtual {v13}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v17

    .line 195
    .local v17, "type":Ljava/lang/String;
    invoke-virtual {v13}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v11

    .line 196
    .local v11, "providerId":Ljava/lang/String;
    invoke-virtual {v13}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v10

    .line 198
    .local v10, "objectId":Ljava/lang/String;
    invoke-virtual {v13}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v16

    .line 199
    .local v16, "title":Ljava/lang/String;
    invoke-virtual {v13}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v15

    .line 200
    .local v15, "temp":Ljava/lang/String;
    const/4 v14, 0x0

    .line 202
    .local v14, "subtitle":Landroid/net/Uri;
    :try_start_0
    const-string v18, "null"

    move-object/from16 v0, v18

    invoke-virtual {v15, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v18

    if-eqz v18, :cond_1

    .line 203
    const/4 v14, 0x0

    .line 211
    :goto_1
    invoke-virtual {v13}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v15

    .line 212
    const-wide/16 v4, -0x1

    .line 214
    .local v4, "duration":J
    :try_start_1
    invoke-static {v15}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/Long;->longValue()J
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-wide v4

    .line 219
    :goto_2
    invoke-virtual {v13}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v15

    .line 220
    const/4 v8, 0x0

    .line 222
    .local v8, "itemUri":Landroid/net/Uri;
    :try_start_2
    const-string v18, "null"

    move-object/from16 v0, v18

    invoke-virtual {v15, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    move-result v18

    if-eqz v18, :cond_2

    .line 223
    const/4 v8, 0x0

    .line 231
    :goto_3
    invoke-virtual {v13}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v9

    .line 233
    .local v9, "mime":Ljava/lang/String;
    invoke-virtual {v13}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v15

    .line 234
    const-wide/16 v6, -0x1

    .line 236
    .local v6, "filesize":J
    :try_start_3
    invoke-static {v15}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/Long;->longValue()J
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3

    move-result-wide v6

    .line 241
    :goto_4
    new-instance v12, Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;

    invoke-direct {v12}, Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;-><init>()V

    .line 242
    .local v12, "seed":Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;
    move-object/from16 v0, v17

    iput-object v0, v12, Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;->mItemType:Ljava/lang/String;

    .line 243
    iput-object v11, v12, Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;->mProviderId:Ljava/lang/String;

    .line 244
    iput-object v10, v12, Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;->mObjectId:Ljava/lang/String;

    .line 245
    move-object/from16 v0, v16

    iput-object v0, v12, Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;->mTitle:Ljava/lang/String;

    .line 246
    iput-object v14, v12, Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;->mSubtitle:Landroid/net/Uri;

    .line 247
    iput-wide v4, v12, Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;->mDuration:J

    .line 248
    iput-object v8, v12, Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;->mItemUri:Landroid/net/Uri;

    .line 249
    iput-object v9, v12, Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;->mMimeType:Ljava/lang/String;

    .line 250
    iput-wide v6, v12, Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;->mFileSize:J

    goto :goto_0

    .line 205
    .end local v4    # "duration":J
    .end local v6    # "filesize":J
    .end local v8    # "itemUri":Landroid/net/Uri;
    .end local v9    # "mime":Ljava/lang/String;
    .end local v12    # "seed":Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;
    :cond_1
    :try_start_4
    invoke-static {v15}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    move-result-object v14

    goto :goto_1

    .line 207
    :catch_0
    move-exception v3

    .line 208
    .local v3, "e":Ljava/lang/Exception;
    const/4 v14, 0x0

    goto :goto_1

    .line 215
    .end local v3    # "e":Ljava/lang/Exception;
    .restart local v4    # "duration":J
    :catch_1
    move-exception v3

    .line 216
    .restart local v3    # "e":Ljava/lang/Exception;
    const-wide/16 v4, -0x1

    goto :goto_2

    .line 225
    .end local v3    # "e":Ljava/lang/Exception;
    .restart local v8    # "itemUri":Landroid/net/Uri;
    :cond_2
    :try_start_5
    invoke-static {v15}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2

    move-result-object v8

    goto :goto_3

    .line 227
    :catch_2
    move-exception v3

    .line 228
    .restart local v3    # "e":Ljava/lang/Exception;
    const/4 v8, 0x0

    goto :goto_3

    .line 237
    .end local v3    # "e":Ljava/lang/Exception;
    .restart local v6    # "filesize":J
    .restart local v9    # "mime":Ljava/lang/String;
    :catch_3
    move-exception v3

    .line 238
    .restart local v3    # "e":Ljava/lang/Exception;
    const-wide/16 v6, -0x1

    goto :goto_4
.end method

.method public static parseSeedStringUsingSplit(Ljava/lang/String;)Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;
    .locals 23
    .param p0, "seedString"    # Ljava/lang/String;

    .prologue
    .line 258
    if-nez p0, :cond_0

    .line 259
    const-string v20, "ItemExtractor"

    const-string v21, "seedString == null"

    invoke-static/range {v20 .. v21}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 260
    const/4 v14, 0x0

    .line 330
    :goto_0
    return-object v14

    .line 262
    :cond_0
    const-string v20, "ItemExtractor"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "parseSeedStringUsingSplit : "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Lcom/samsung/android/allshare/DLog;->v_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 264
    const-string v20, ",@,#,"

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v15

    .line 265
    .local v15, "seedMember":[Ljava/lang/String;
    array-length v2, v15

    .line 266
    .local v2, "count":I
    const/4 v8, 0x0

    .line 268
    .local v8, "index":I
    const/16 v20, 0x9

    move/from16 v0, v20

    if-eq v2, v0, :cond_1

    .line 269
    const-string v20, "ItemExtractor"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "count : "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 270
    const/4 v14, 0x0

    goto :goto_0

    .line 273
    :cond_1
    add-int/lit8 v9, v8, 0x1

    .end local v8    # "index":I
    .local v9, "index":I
    aget-object v19, v15, v8

    .line 274
    .local v19, "type":Ljava/lang/String;
    add-int/lit8 v8, v9, 0x1

    .end local v9    # "index":I
    .restart local v8    # "index":I
    aget-object v13, v15, v9

    .line 275
    .local v13, "providerId":Ljava/lang/String;
    add-int/lit8 v9, v8, 0x1

    .end local v8    # "index":I
    .restart local v9    # "index":I
    aget-object v12, v15, v8

    .line 277
    .local v12, "objectId":Ljava/lang/String;
    add-int/lit8 v8, v9, 0x1

    .end local v9    # "index":I
    .restart local v8    # "index":I
    aget-object v18, v15, v9

    .line 278
    .local v18, "title":Ljava/lang/String;
    add-int/lit8 v9, v8, 0x1

    .end local v8    # "index":I
    .restart local v9    # "index":I
    aget-object v17, v15, v8

    .line 279
    .local v17, "temp":Ljava/lang/String;
    const/16 v16, 0x0

    .line 281
    .local v16, "subtitle":Landroid/net/Uri;
    :try_start_0
    const-string v20, "null"

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v20

    if-eqz v20, :cond_2

    .line 282
    const/16 v16, 0x0

    .line 290
    :goto_1
    add-int/lit8 v8, v9, 0x1

    .end local v9    # "index":I
    .restart local v8    # "index":I
    aget-object v17, v15, v9

    .line 291
    const-wide/16 v4, -0x1

    .line 293
    .local v4, "duration":J
    :try_start_1
    invoke-static/range {v17 .. v17}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/Long;->longValue()J
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-wide v4

    .line 298
    :goto_2
    add-int/lit8 v9, v8, 0x1

    .end local v8    # "index":I
    .restart local v9    # "index":I
    aget-object v17, v15, v8

    .line 299
    const/4 v10, 0x0

    .line 301
    .local v10, "itemUri":Landroid/net/Uri;
    :try_start_2
    const-string v20, "null"

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    move-result v20

    if-eqz v20, :cond_3

    .line 302
    const/4 v10, 0x0

    .line 310
    :goto_3
    add-int/lit8 v8, v9, 0x1

    .end local v9    # "index":I
    .restart local v8    # "index":I
    aget-object v11, v15, v9

    .line 312
    .local v11, "mime":Ljava/lang/String;
    add-int/lit8 v9, v8, 0x1

    .end local v8    # "index":I
    .restart local v9    # "index":I
    aget-object v17, v15, v8

    .line 313
    const-wide/16 v6, -0x1

    .line 315
    .local v6, "filesize":J
    :try_start_3
    invoke-static/range {v17 .. v17}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/Long;->longValue()J
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3

    move-result-wide v6

    .line 319
    :goto_4
    new-instance v14, Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;

    invoke-direct {v14}, Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;-><init>()V

    .line 320
    .local v14, "seed":Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;
    move-object/from16 v0, v19

    iput-object v0, v14, Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;->mItemType:Ljava/lang/String;

    .line 321
    iput-object v13, v14, Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;->mProviderId:Ljava/lang/String;

    .line 322
    iput-object v12, v14, Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;->mObjectId:Ljava/lang/String;

    .line 323
    move-object/from16 v0, v18

    iput-object v0, v14, Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;->mTitle:Ljava/lang/String;

    .line 324
    move-object/from16 v0, v16

    iput-object v0, v14, Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;->mSubtitle:Landroid/net/Uri;

    .line 325
    iput-wide v4, v14, Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;->mDuration:J

    .line 326
    iput-object v10, v14, Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;->mItemUri:Landroid/net/Uri;

    .line 327
    iput-object v11, v14, Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;->mMimeType:Ljava/lang/String;

    .line 328
    iput-wide v6, v14, Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;->mFileSize:J

    goto/16 :goto_0

    .line 284
    .end local v4    # "duration":J
    .end local v6    # "filesize":J
    .end local v10    # "itemUri":Landroid/net/Uri;
    .end local v11    # "mime":Ljava/lang/String;
    .end local v14    # "seed":Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;
    :cond_2
    :try_start_4
    invoke-static/range {v17 .. v17}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    move-result-object v16

    goto :goto_1

    .line 286
    :catch_0
    move-exception v3

    .line 287
    .local v3, "e":Ljava/lang/Exception;
    const/16 v16, 0x0

    goto :goto_1

    .line 294
    .end local v3    # "e":Ljava/lang/Exception;
    .end local v9    # "index":I
    .restart local v4    # "duration":J
    .restart local v8    # "index":I
    :catch_1
    move-exception v3

    .line 295
    .restart local v3    # "e":Ljava/lang/Exception;
    const-wide/16 v4, -0x1

    goto :goto_2

    .line 304
    .end local v3    # "e":Ljava/lang/Exception;
    .end local v8    # "index":I
    .restart local v9    # "index":I
    .restart local v10    # "itemUri":Landroid/net/Uri;
    :cond_3
    :try_start_5
    invoke-static/range {v17 .. v17}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2

    move-result-object v10

    goto :goto_3

    .line 306
    :catch_2
    move-exception v3

    .line 307
    .restart local v3    # "e":Ljava/lang/Exception;
    const/4 v10, 0x0

    goto :goto_3

    .line 316
    .end local v3    # "e":Ljava/lang/Exception;
    .restart local v6    # "filesize":J
    .restart local v11    # "mime":Ljava/lang/String;
    :catch_3
    move-exception v3

    .line 317
    .restart local v3    # "e":Ljava/lang/Exception;
    const-wide/16 v6, -0x1

    goto :goto_4
.end method


# virtual methods
.method public getDuration()J
    .locals 2

    .prologue
    .line 144
    iget-wide v0, p0, Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;->mDuration:J

    return-wide v0
.end method

.method public getFileSize()J
    .locals 2

    .prologue
    .line 175
    iget-wide v0, p0, Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;->mFileSize:J

    return-wide v0
.end method

.method public getItemType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;->mItemType:Ljava/lang/String;

    return-object v0
.end method

.method public getItemUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;->mItemUri:Landroid/net/Uri;

    return-object v0
.end method

.method public getMimeType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 164
    iget-object v0, p0, Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;->mMimeType:Ljava/lang/String;

    return-object v0
.end method

.method public getObjectID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;->mObjectId:Ljava/lang/String;

    return-object v0
.end method

.method public getProviderID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;->mProviderId:Ljava/lang/String;

    return-object v0
.end method

.method public getSeedString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 77
    iget-object v2, p0, Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;->mSubtitle:Landroid/net/Uri;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;->mSubtitle:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;->mSubtitle:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-gtz v2, :cond_2

    :cond_0
    const-string v1, "null"

    .line 79
    .local v1, "subtitle":Ljava/lang/String;
    :goto_0
    iget-object v2, p0, Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;->mItemUri:Landroid/net/Uri;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;->mItemUri:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;->mItemUri:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-gtz v2, :cond_3

    :cond_1
    const-string v0, "null"

    .line 82
    .local v0, "itemUri":Ljava/lang/String;
    :goto_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;->mItemType:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ",@,#,"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;->mProviderId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ",@,#,"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;->mObjectId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ",@,#,"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;->mTitle:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ",@,#,"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ",@,#,"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v4, p0, Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;->mDuration:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ",@,#,"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ",@,#,"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;->mMimeType:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ",@,#,"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v4, p0, Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;->mFileSize:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2

    .line 77
    .end local v0    # "itemUri":Ljava/lang/String;
    .end local v1    # "subtitle":Ljava/lang/String;
    :cond_2
    iget-object v2, p0, Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;->mSubtitle:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 79
    .restart local v1    # "subtitle":Ljava/lang/String;
    :cond_3
    iget-object v2, p0, Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;->mItemUri:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public getSubtitle()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;->mSubtitle:Landroid/net/Uri;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;->mTitle:Ljava/lang/String;

    return-object v0
.end method
