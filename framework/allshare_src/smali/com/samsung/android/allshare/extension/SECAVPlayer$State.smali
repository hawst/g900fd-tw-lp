.class Lcom/samsung/android/allshare/extension/SECAVPlayer$State;
.super Ljava/lang/Object;
.source "SECAVPlayer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/allshare/extension/SECAVPlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "State"
.end annotation


# instance fields
.field private currentState:Lcom/samsung/android/allshare/extension/SECAVPlayer$SECAVPlayerState;

.field private mItemDuration:J

.field private mLastPos:J

.field private mMediaInfo:Lcom/samsung/android/allshare/media/MediaInfo;

.field private mPlayRequested:Z

.field final synthetic this$0:Lcom/samsung/android/allshare/extension/SECAVPlayer;


# direct methods
.method private constructor <init>(Lcom/samsung/android/allshare/extension/SECAVPlayer;)V
    .locals 2

    .prologue
    .line 255
    iput-object p1, p0, Lcom/samsung/android/allshare/extension/SECAVPlayer$State;->this$0:Lcom/samsung/android/allshare/extension/SECAVPlayer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 259
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/allshare/extension/SECAVPlayer$State;->mMediaInfo:Lcom/samsung/android/allshare/media/MediaInfo;

    .line 261
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/samsung/android/allshare/extension/SECAVPlayer$State;->mLastPos:J

    .line 263
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/allshare/extension/SECAVPlayer$State;->mPlayRequested:Z

    .line 265
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/android/allshare/extension/SECAVPlayer$State;->mItemDuration:J

    .line 267
    sget-object v0, Lcom/samsung/android/allshare/extension/SECAVPlayer$SECAVPlayerState;->UNKNOWN:Lcom/samsung/android/allshare/extension/SECAVPlayer$SECAVPlayerState;

    iput-object v0, p0, Lcom/samsung/android/allshare/extension/SECAVPlayer$State;->currentState:Lcom/samsung/android/allshare/extension/SECAVPlayer$SECAVPlayerState;

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/allshare/extension/SECAVPlayer;Lcom/samsung/android/allshare/extension/SECAVPlayer$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/android/allshare/extension/SECAVPlayer;
    .param p2, "x1"    # Lcom/samsung/android/allshare/extension/SECAVPlayer$1;

    .prologue
    .line 255
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/extension/SECAVPlayer$State;-><init>(Lcom/samsung/android/allshare/extension/SECAVPlayer;)V

    return-void
.end method

.method static synthetic access$100(Lcom/samsung/android/allshare/extension/SECAVPlayer$State;)J
    .locals 2
    .param p0, "x0"    # Lcom/samsung/android/allshare/extension/SECAVPlayer$State;

    .prologue
    .line 255
    invoke-direct {p0}, Lcom/samsung/android/allshare/extension/SECAVPlayer$State;->getLastPos()J

    move-result-wide v0

    return-wide v0
.end method

.method static synthetic access$1500(Lcom/samsung/android/allshare/extension/SECAVPlayer$State;J)V
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/extension/SECAVPlayer$State;
    .param p1, "x1"    # J

    .prologue
    .line 255
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/allshare/extension/SECAVPlayer$State;->setItemDuration(J)V

    return-void
.end method

.method static synthetic access$1600(Lcom/samsung/android/allshare/extension/SECAVPlayer$State;J)V
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/extension/SECAVPlayer$State;
    .param p1, "x1"    # J

    .prologue
    .line 255
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/allshare/extension/SECAVPlayer$State;->setNewPlayState(J)V

    return-void
.end method

.method static synthetic access$200(Lcom/samsung/android/allshare/extension/SECAVPlayer$State;)Lcom/samsung/android/allshare/media/MediaInfo;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/extension/SECAVPlayer$State;

    .prologue
    .line 255
    invoke-direct {p0}, Lcom/samsung/android/allshare/extension/SECAVPlayer$State;->getMediaInfo()Lcom/samsung/android/allshare/media/MediaInfo;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2000(Lcom/samsung/android/allshare/extension/SECAVPlayer$State;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/extension/SECAVPlayer$State;

    .prologue
    .line 255
    invoke-direct {p0}, Lcom/samsung/android/allshare/extension/SECAVPlayer$State;->isPlayRequested()Z

    move-result v0

    return v0
.end method

.method static synthetic access$300(Lcom/samsung/android/allshare/extension/SECAVPlayer$State;)J
    .locals 2
    .param p0, "x0"    # Lcom/samsung/android/allshare/extension/SECAVPlayer$State;

    .prologue
    .line 255
    invoke-direct {p0}, Lcom/samsung/android/allshare/extension/SECAVPlayer$State;->getItemDuration()J

    move-result-wide v0

    return-wide v0
.end method

.method static synthetic access$500(Lcom/samsung/android/allshare/extension/SECAVPlayer$State;)Lcom/samsung/android/allshare/extension/SECAVPlayer$SECAVPlayerState;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/extension/SECAVPlayer$State;

    .prologue
    .line 255
    iget-object v0, p0, Lcom/samsung/android/allshare/extension/SECAVPlayer$State;->currentState:Lcom/samsung/android/allshare/extension/SECAVPlayer$SECAVPlayerState;

    return-object v0
.end method

.method static synthetic access$502(Lcom/samsung/android/allshare/extension/SECAVPlayer$State;Lcom/samsung/android/allshare/extension/SECAVPlayer$SECAVPlayerState;)Lcom/samsung/android/allshare/extension/SECAVPlayer$SECAVPlayerState;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/extension/SECAVPlayer$State;
    .param p1, "x1"    # Lcom/samsung/android/allshare/extension/SECAVPlayer$SECAVPlayerState;

    .prologue
    .line 255
    iput-object p1, p0, Lcom/samsung/android/allshare/extension/SECAVPlayer$State;->currentState:Lcom/samsung/android/allshare/extension/SECAVPlayer$SECAVPlayerState;

    return-object p1
.end method

.method static synthetic access$600(Lcom/samsung/android/allshare/extension/SECAVPlayer$State;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/extension/SECAVPlayer$State;
    .param p1, "x1"    # Z

    .prologue
    .line 255
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/extension/SECAVPlayer$State;->setPlayRequested(Z)V

    return-void
.end method

.method static synthetic access$700(Lcom/samsung/android/allshare/extension/SECAVPlayer$State;J)V
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/extension/SECAVPlayer$State;
    .param p1, "x1"    # J

    .prologue
    .line 255
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/allshare/extension/SECAVPlayer$State;->setLastPos(J)V

    return-void
.end method

.method static synthetic access$800(Lcom/samsung/android/allshare/extension/SECAVPlayer$State;Lcom/samsung/android/allshare/media/MediaInfo;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/extension/SECAVPlayer$State;
    .param p1, "x1"    # Lcom/samsung/android/allshare/media/MediaInfo;

    .prologue
    .line 255
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/extension/SECAVPlayer$State;->setMediaInfo(Lcom/samsung/android/allshare/media/MediaInfo;)V

    return-void
.end method

.method private getItemDuration()J
    .locals 2

    .prologue
    .line 310
    iget-wide v0, p0, Lcom/samsung/android/allshare/extension/SECAVPlayer$State;->mItemDuration:J

    return-wide v0
.end method

.method private getLastPos()J
    .locals 2

    .prologue
    .line 298
    iget-wide v0, p0, Lcom/samsung/android/allshare/extension/SECAVPlayer$State;->mLastPos:J

    return-wide v0
.end method

.method private getMediaInfo()Lcom/samsung/android/allshare/media/MediaInfo;
    .locals 1

    .prologue
    .line 302
    iget-object v0, p0, Lcom/samsung/android/allshare/extension/SECAVPlayer$State;->mMediaInfo:Lcom/samsung/android/allshare/media/MediaInfo;

    return-object v0
.end method

.method private isPlayRequested()Z
    .locals 1

    .prologue
    .line 284
    iget-boolean v0, p0, Lcom/samsung/android/allshare/extension/SECAVPlayer$State;->mPlayRequested:Z

    return v0
.end method

.method private setItemDuration(J)V
    .locals 1
    .param p1, "duration"    # J

    .prologue
    .line 306
    iput-wide p1, p0, Lcom/samsung/android/allshare/extension/SECAVPlayer$State;->mItemDuration:J

    .line 307
    return-void
.end method

.method private setLastPos(J)V
    .locals 3
    .param p1, "pos"    # J

    .prologue
    .line 292
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-lez v0, :cond_0

    .line 293
    iput-wide p1, p0, Lcom/samsung/android/allshare/extension/SECAVPlayer$State;->mLastPos:J

    .line 295
    :cond_0
    return-void
.end method

.method private setMediaInfo(Lcom/samsung/android/allshare/media/MediaInfo;)V
    .locals 0
    .param p1, "info"    # Lcom/samsung/android/allshare/media/MediaInfo;

    .prologue
    .line 288
    iput-object p1, p0, Lcom/samsung/android/allshare/extension/SECAVPlayer$State;->mMediaInfo:Lcom/samsung/android/allshare/media/MediaInfo;

    .line 289
    return-void
.end method

.method private setNewPlayState(J)V
    .locals 4
    .param p1, "pos"    # J

    .prologue
    const-wide/16 v2, 0x0

    .line 272
    iput-wide v2, p0, Lcom/samsung/android/allshare/extension/SECAVPlayer$State;->mLastPos:J

    .line 273
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/allshare/extension/SECAVPlayer$State;->mMediaInfo:Lcom/samsung/android/allshare/media/MediaInfo;

    .line 274
    iput-wide v2, p0, Lcom/samsung/android/allshare/extension/SECAVPlayer$State;->mItemDuration:J

    .line 275
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/allshare/extension/SECAVPlayer$State;->mPlayRequested:Z

    .line 276
    sget-object v0, Lcom/samsung/android/allshare/extension/SECAVPlayer$SECAVPlayerState;->UNKNOWN:Lcom/samsung/android/allshare/extension/SECAVPlayer$SECAVPlayerState;

    iput-object v0, p0, Lcom/samsung/android/allshare/extension/SECAVPlayer$State;->currentState:Lcom/samsung/android/allshare/extension/SECAVPlayer$SECAVPlayerState;

    .line 277
    return-void
.end method

.method private setPlayRequested(Z)V
    .locals 0
    .param p1, "flag"    # Z

    .prologue
    .line 280
    iput-boolean p1, p0, Lcom/samsung/android/allshare/extension/SECAVPlayer$State;->mPlayRequested:Z

    .line 281
    return-void
.end method
