.class public Lcom/samsung/android/allshare/extension/DeviceExtractor$Seed;
.super Ljava/lang/Object;
.source "DeviceExtractor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/allshare/extension/DeviceExtractor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Seed"
.end annotation


# static fields
.field private static final DELIMITER:Ljava/lang/String; = "+"


# instance fields
.field private mInterface:Ljava/lang/String;

.field private mUUID:Ljava/lang/String;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/allshare/extension/DeviceExtractor$Seed;->mInterface:Ljava/lang/String;

    .line 33
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/allshare/extension/DeviceExtractor$Seed;->mUUID:Ljava/lang/String;

    .line 36
    return-void
.end method

.method public static parseSeedString(Ljava/lang/String;)Lcom/samsung/android/allshare/extension/DeviceExtractor$Seed;
    .locals 6
    .param p0, "seedString"    # Ljava/lang/String;

    .prologue
    .line 47
    new-instance v3, Ljava/util/StringTokenizer;

    const-string v5, "+"

    invoke-direct {v3, p0, v5}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 48
    .local v3, "st":Ljava/util/StringTokenizer;
    invoke-virtual {v3}, Ljava/util/StringTokenizer;->countTokens()I

    move-result v0

    .line 50
    .local v0, "count":I
    const/4 v5, 0x2

    if-eq v0, v5, :cond_0

    .line 51
    const/4 v2, 0x0

    .line 65
    :goto_0
    return-object v2

    .line 53
    :cond_0
    const-string v4, ""

    .line 54
    .local v4, "uuid":Ljava/lang/String;
    invoke-virtual {v3}, Ljava/util/StringTokenizer;->hasMoreElements()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 55
    invoke-virtual {v3}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v4

    .line 57
    :cond_1
    const-string v1, ""

    .line 58
    .local v1, "netInterface":Ljava/lang/String;
    invoke-virtual {v3}, Ljava/util/StringTokenizer;->hasMoreElements()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 59
    invoke-virtual {v3}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v1

    .line 61
    :cond_2
    new-instance v2, Lcom/samsung/android/allshare/extension/DeviceExtractor$Seed;

    invoke-direct {v2}, Lcom/samsung/android/allshare/extension/DeviceExtractor$Seed;-><init>()V

    .line 62
    .local v2, "seed":Lcom/samsung/android/allshare/extension/DeviceExtractor$Seed;
    iput-object v4, v2, Lcom/samsung/android/allshare/extension/DeviceExtractor$Seed;->mUUID:Ljava/lang/String;

    .line 63
    iput-object v1, v2, Lcom/samsung/android/allshare/extension/DeviceExtractor$Seed;->mInterface:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public getInterface()Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/samsung/android/allshare/extension/DeviceExtractor$Seed;->mInterface:Ljava/lang/String;

    return-object v0
.end method

.method public getUUID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/samsung/android/allshare/extension/DeviceExtractor$Seed;->mUUID:Ljava/lang/String;

    return-object v0
.end method
