.class Lcom/samsung/android/allshare/extension/impl/WM7SearchMediaGetter$SearchRequest;
.super Ljava/lang/Object;
.source "WM7SearchMediaGetter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/allshare/extension/impl/WM7SearchMediaGetter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SearchRequest"
.end annotation


# instance fields
.field private mSearchCriteria:Lcom/samsung/android/allshare/media/SearchCriteria;

.field private mStartIndex:I

.field final synthetic this$0:Lcom/samsung/android/allshare/extension/impl/WM7SearchMediaGetter;


# direct methods
.method public constructor <init>(Lcom/samsung/android/allshare/extension/impl/WM7SearchMediaGetter;Lcom/samsung/android/allshare/media/SearchCriteria;I)V
    .locals 1
    .param p2, "criteria"    # Lcom/samsung/android/allshare/media/SearchCriteria;
    .param p3, "startIndex"    # I

    .prologue
    .line 41
    iput-object p1, p0, Lcom/samsung/android/allshare/extension/impl/WM7SearchMediaGetter$SearchRequest;->this$0:Lcom/samsung/android/allshare/extension/impl/WM7SearchMediaGetter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/allshare/extension/impl/WM7SearchMediaGetter$SearchRequest;->mSearchCriteria:Lcom/samsung/android/allshare/media/SearchCriteria;

    .line 39
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/allshare/extension/impl/WM7SearchMediaGetter$SearchRequest;->mStartIndex:I

    .line 42
    iput-object p2, p0, Lcom/samsung/android/allshare/extension/impl/WM7SearchMediaGetter$SearchRequest;->mSearchCriteria:Lcom/samsung/android/allshare/media/SearchCriteria;

    .line 43
    iput p3, p0, Lcom/samsung/android/allshare/extension/impl/WM7SearchMediaGetter$SearchRequest;->mStartIndex:I

    .line 44
    return-void
.end method


# virtual methods
.method public isSame(Lcom/samsung/android/allshare/media/SearchCriteria;I)Z
    .locals 2
    .param p1, "criteria"    # Lcom/samsung/android/allshare/media/SearchCriteria;
    .param p2, "startIndex"    # I

    .prologue
    const/4 v0, 0x0

    .line 47
    if-nez p1, :cond_1

    .line 51
    :cond_0
    :goto_0
    return v0

    .line 49
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/allshare/extension/impl/WM7SearchMediaGetter$SearchRequest;->mSearchCriteria:Lcom/samsung/android/allshare/media/SearchCriteria;

    invoke-virtual {p1, v1}, Lcom/samsung/android/allshare/media/SearchCriteria;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/samsung/android/allshare/extension/impl/WM7SearchMediaGetter$SearchRequest;->mStartIndex:I

    if-ne v1, p2, :cond_0

    .line 50
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public updateSearchRequest(Lcom/samsung/android/allshare/media/SearchCriteria;I)V
    .locals 0
    .param p1, "criteria"    # Lcom/samsung/android/allshare/media/SearchCriteria;
    .param p2, "startIndex"    # I

    .prologue
    .line 55
    iput-object p1, p0, Lcom/samsung/android/allshare/extension/impl/WM7SearchMediaGetter$SearchRequest;->mSearchCriteria:Lcom/samsung/android/allshare/media/SearchCriteria;

    .line 56
    iput p2, p0, Lcom/samsung/android/allshare/extension/impl/WM7SearchMediaGetter$SearchRequest;->mStartIndex:I

    .line 57
    return-void
.end method
