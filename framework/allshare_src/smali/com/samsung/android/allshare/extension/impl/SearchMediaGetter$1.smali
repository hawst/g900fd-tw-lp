.class Lcom/samsung/android/allshare/extension/impl/SearchMediaGetter$1;
.super Ljava/lang/Object;
.source "SearchMediaGetter.java"

# interfaces
.implements Lcom/samsung/android/allshare/media/Provider$IProviderSearchResponseListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/allshare/extension/impl/SearchMediaGetter;->startFlatSearching(Lcom/samsung/android/allshare/media/Provider;Ljava/util/ArrayList;Lcom/samsung/android/allshare/extension/FlatProvider$IFlatProviderConnection;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/allshare/extension/impl/SearchMediaGetter;


# direct methods
.method constructor <init>(Lcom/samsung/android/allshare/extension/impl/SearchMediaGetter;)V
    .locals 0

    .prologue
    .line 154
    iput-object p1, p0, Lcom/samsung/android/allshare/extension/impl/SearchMediaGetter$1;->this$0:Lcom/samsung/android/allshare/extension/impl/SearchMediaGetter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSearchResponseReceived(Ljava/util/ArrayList;IILcom/samsung/android/allshare/media/SearchCriteria;ZLcom/samsung/android/allshare/ERROR;)V
    .locals 9
    .param p2, "requestedStartIndex"    # I
    .param p3, "requestedCount"    # I
    .param p4, "searchCriteria"    # Lcom/samsung/android/allshare/media/SearchCriteria;
    .param p5, "endOfItems"    # Z
    .param p6, "err"    # Lcom/samsung/android/allshare/ERROR;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/Item;",
            ">;II",
            "Lcom/samsung/android/allshare/media/SearchCriteria;",
            "Z",
            "Lcom/samsung/android/allshare/ERROR;",
            ")V"
        }
    .end annotation

    .prologue
    .local p1, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/Item;>;"
    const/4 v8, 0x1

    .line 160
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 162
    .local v4, "returnedCount":I
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 163
    .local v2, "itemIt":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/allshare/Item;>;"
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 164
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/allshare/Item;

    .line 165
    .local v3, "obj":Lcom/samsung/android/allshare/Item;
    invoke-virtual {v3}, Lcom/samsung/android/allshare/Item;->getType()Lcom/samsung/android/allshare/Item$MediaType;

    move-result-object v5

    sget-object v6, Lcom/samsung/android/allshare/Item$MediaType;->ITEM_UNKNOWN:Lcom/samsung/android/allshare/Item$MediaType;

    if-ne v5, v6, :cond_0

    .line 166
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 170
    .end local v3    # "obj":Lcom/samsung/android/allshare/Item;
    :cond_1
    sget-object v5, Lcom/samsung/android/allshare/ERROR;->FEATURE_NOT_SUPPORTED:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {p6, v5}, Lcom/samsung/android/allshare/ERROR;->compareTo(Ljava/lang/Enum;)I

    move-result v5

    if-nez v5, :cond_2

    .line 171
    const-class v5, Lcom/samsung/android/allshare/extension/impl/SearchMediaGetter;

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    const-string v6, "Feature Not Supported"

    invoke-static {v5, v6}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 173
    :cond_2
    iget-object v5, p0, Lcom/samsung/android/allshare/extension/impl/SearchMediaGetter$1;->this$0:Lcom/samsung/android/allshare/extension/impl/SearchMediaGetter;

    # getter for: Lcom/samsung/android/allshare/extension/impl/SearchMediaGetter;->mConns:Ljava/util/ArrayList;
    invoke-static {v5}, Lcom/samsung/android/allshare/extension/impl/SearchMediaGetter;->access$200(Lcom/samsung/android/allshare/extension/impl/SearchMediaGetter;)Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 174
    .local v1, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/allshare/extension/impl/SearchMediaGetter$FlatProviderConnectionInfo;>;"
    :cond_3
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_9

    .line 175
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/allshare/extension/impl/SearchMediaGetter$FlatProviderConnectionInfo;

    .line 177
    .local v0, "conn":Lcom/samsung/android/allshare/extension/impl/SearchMediaGetter$FlatProviderConnectionInfo;
    if-nez p4, :cond_4

    .line 178
    invoke-virtual {v0, p6}, Lcom/samsung/android/allshare/extension/impl/SearchMediaGetter$FlatProviderConnectionInfo;->onError(Lcom/samsung/android/allshare/ERROR;)V

    goto :goto_1

    .line 182
    :cond_4
    invoke-virtual {v0, p4, p2}, Lcom/samsung/android/allshare/extension/impl/SearchMediaGetter$FlatProviderConnectionInfo;->isCurrentSearchRequest(Lcom/samsung/android/allshare/media/SearchCriteria;I)Z

    move-result v5

    if-ne v5, v8, :cond_3

    .line 185
    sget-object v5, Lcom/samsung/android/allshare/ERROR;->SUCCESS:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {p6, v5}, Lcom/samsung/android/allshare/ERROR;->compareTo(Ljava/lang/Enum;)I

    move-result v5

    if-eqz v5, :cond_5

    .line 186
    invoke-virtual {v0, p6}, Lcom/samsung/android/allshare/extension/impl/SearchMediaGetter$FlatProviderConnectionInfo;->onError(Lcom/samsung/android/allshare/ERROR;)V

    goto :goto_1

    .line 190
    :cond_5
    invoke-virtual {v0}, Lcom/samsung/android/allshare/extension/impl/SearchMediaGetter$FlatProviderConnectionInfo;->isCanceled()Z

    move-result v5

    if-ne v5, v8, :cond_6

    .line 191
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_1

    .line 195
    :cond_6
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-eqz v5, :cond_7

    .line 196
    invoke-virtual {v0, p1}, Lcom/samsung/android/allshare/extension/impl/SearchMediaGetter$FlatProviderConnectionInfo;->onProgress(Ljava/util/ArrayList;)V

    .line 198
    :cond_7
    if-ne p5, v8, :cond_8

    .line 199
    invoke-virtual {v0}, Lcom/samsung/android/allshare/extension/impl/SearchMediaGetter$FlatProviderConnectionInfo;->onFinish()V

    goto :goto_1

    .line 201
    :cond_8
    add-int v5, p2, v4

    invoke-virtual {v0, p4, v5}, Lcom/samsung/android/allshare/extension/impl/SearchMediaGetter$FlatProviderConnectionInfo;->setCurrentSearchRequest(Lcom/samsung/android/allshare/media/SearchCriteria;I)V

    .line 203
    iget-object v5, p0, Lcom/samsung/android/allshare/extension/impl/SearchMediaGetter$1;->this$0:Lcom/samsung/android/allshare/extension/impl/SearchMediaGetter;

    # getter for: Lcom/samsung/android/allshare/extension/impl/SearchMediaGetter;->mProvider:Lcom/samsung/android/allshare/media/Provider;
    invoke-static {v5}, Lcom/samsung/android/allshare/extension/impl/SearchMediaGetter;->access$000(Lcom/samsung/android/allshare/extension/impl/SearchMediaGetter;)Lcom/samsung/android/allshare/media/Provider;

    move-result-object v5

    add-int v6, p2, v4

    const/16 v7, 0x32

    invoke-virtual {v5, p4, v6, v7}, Lcom/samsung/android/allshare/media/Provider;->search(Lcom/samsung/android/allshare/media/SearchCriteria;II)V

    goto :goto_1

    .line 209
    .end local v0    # "conn":Lcom/samsung/android/allshare/extension/impl/SearchMediaGetter$FlatProviderConnectionInfo;
    :cond_9
    return-void
.end method
