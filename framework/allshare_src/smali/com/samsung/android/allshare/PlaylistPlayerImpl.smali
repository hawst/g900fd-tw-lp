.class final Lcom/samsung/android/allshare/PlaylistPlayerImpl;
.super Lcom/samsung/android/allshare/media/PlaylistPlayer;
.source "PlaylistPlayerImpl.java"


# static fields
.field private static final MIN_TRACK_NUMBER:I = 0x1

.field private static final TAG_CLASS:Ljava/lang/String; = "PlaylistPlayerImpl"


# instance fields
.field private mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

.field mAllShareRespHandler:Lcom/samsung/android/allshare/AllShareResponseHandler;

.field mCurrentPlayingContentUriStrList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

.field mEventHandler:Lcom/samsung/android/allshare/AllShareEventHandler;

.field private mFilePath:Ljava/lang/String;

.field private mIsAutoSlideShowMode:Z

.field private mIsNavigateInPause:Z

.field private mIsSeekable:Z

.field private mIsSubscribed:Z

.field private mPlaylist:Lcom/samsung/android/allshare/media/Playlist;

.field private mPlaylistPlayerEventListener:Lcom/samsung/android/allshare/media/PlaylistPlayer$IPlaylistPlayerEventListener;

.field private mPlaylistPlayerResponseListener:Lcom/samsung/android/allshare/media/PlaylistPlayer$IPlaylistPlayerPlaybackResponseListener;

.field private mTrackNumber:I


# direct methods
.method constructor <init>(Lcom/samsung/android/allshare/IAllShareConnector;Lcom/samsung/android/allshare/DeviceImpl;)V
    .locals 4
    .param p1, "connector"    # Lcom/samsung/android/allshare/IAllShareConnector;
    .param p2, "deviceImpl"    # Lcom/samsung/android/allshare/DeviceImpl;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 73
    invoke-direct {p0}, Lcom/samsung/android/allshare/media/PlaylistPlayer;-><init>()V

    .line 49
    iput-object v2, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    .line 51
    iput-object v2, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    .line 53
    iput-object v2, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mPlaylistPlayerResponseListener:Lcom/samsung/android/allshare/media/PlaylistPlayer$IPlaylistPlayerPlaybackResponseListener;

    .line 55
    iput-object v2, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mPlaylistPlayerEventListener:Lcom/samsung/android/allshare/media/PlaylistPlayer$IPlaylistPlayerEventListener;

    .line 57
    const-string v1, ""

    iput-object v1, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mFilePath:Ljava/lang/String;

    .line 59
    iput-object v2, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mPlaylist:Lcom/samsung/android/allshare/media/Playlist;

    .line 61
    iput v3, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mTrackNumber:I

    .line 63
    iput-boolean v3, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mIsSeekable:Z

    .line 65
    iput-boolean v3, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mIsNavigateInPause:Z

    .line 67
    iput-boolean v3, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mIsAutoSlideShowMode:Z

    .line 69
    iput-boolean v3, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mIsSubscribed:Z

    .line 71
    iput-object v2, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mCurrentPlayingContentUriStrList:Ljava/util/ArrayList;

    .line 101
    new-instance v1, Lcom/samsung/android/allshare/PlaylistPlayerImpl$1;

    invoke-static {}, Lcom/samsung/android/allshare/ServiceConnector;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/samsung/android/allshare/PlaylistPlayerImpl$1;-><init>(Lcom/samsung/android/allshare/PlaylistPlayerImpl;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mEventHandler:Lcom/samsung/android/allshare/AllShareEventHandler;

    .line 169
    new-instance v1, Lcom/samsung/android/allshare/PlaylistPlayerImpl$2;

    invoke-static {}, Lcom/samsung/android/allshare/ServiceConnector;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/samsung/android/allshare/PlaylistPlayerImpl$2;-><init>(Lcom/samsung/android/allshare/PlaylistPlayerImpl;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mAllShareRespHandler:Lcom/samsung/android/allshare/AllShareResponseHandler;

    .line 74
    if-nez p1, :cond_0

    .line 75
    const-string v1, "PlaylistPlayerImpl"

    const-string v2, "Connection FAIL: AllShare Service Connector does not exist"

    invoke-static {v1, v2}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 91
    :goto_0
    return-void

    .line 79
    :cond_0
    iput-object p2, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    .line 80
    iput-object p1, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    .line 82
    invoke-virtual {p2}, Lcom/samsung/android/allshare/DeviceImpl;->getBundle()Landroid/os/Bundle;

    move-result-object v0

    .line 83
    .local v0, "bundle":Landroid/os/Bundle;
    if-nez v0, :cond_1

    .line 84
    const-string v1, "PlaylistPlayerImpl"

    const-string v2, "Cannot get bundle from deviceImpl"

    invoke-static {v1, v2}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 88
    :cond_1
    const-string v1, "BUNDLE_BOOLEAN_SEEKABLE"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mIsSeekable:Z

    .line 89
    const-string v1, "BUNDLE_BOOLEAN_NAVIGATE_IN_PAUSE"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mIsNavigateInPause:Z

    .line 90
    const-string v1, "BUNDLE_BOOLEAN_AUTO_SLIDE_SHOW"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mIsAutoSlideShowMode:Z

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/samsung/android/allshare/PlaylistPlayerImpl;)Lcom/samsung/android/allshare/media/PlaylistPlayer$IPlaylistPlayerEventListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/PlaylistPlayerImpl;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mPlaylistPlayerEventListener:Lcom/samsung/android/allshare/media/PlaylistPlayer$IPlaylistPlayerEventListener;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/android/allshare/PlaylistPlayerImpl;)Lcom/samsung/android/allshare/media/PlaylistPlayer$IPlaylistPlayerPlaybackResponseListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/PlaylistPlayerImpl;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mPlaylistPlayerResponseListener:Lcom/samsung/android/allshare/media/PlaylistPlayer$IPlaylistPlayerPlaybackResponseListener;

    return-object v0
.end method

.method private createFilePathForZoom(I)V
    .locals 4
    .param p1, "trackNumber"    # I

    .prologue
    .line 368
    iget-object v3, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mPlaylist:Lcom/samsung/android/allshare/media/Playlist;

    if-eqz v3, :cond_0

    if-gez p1, :cond_1

    .line 381
    :cond_0
    :goto_0
    return-void

    .line 370
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mPlaylist:Lcom/samsung/android/allshare/media/Playlist;

    invoke-virtual {v3}, Lcom/samsung/android/allshare/media/Playlist;->getPlaylist()Ljava/util/ArrayList;

    move-result-object v2

    .line 372
    .local v2, "itemList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/Item;>;"
    invoke-direct {p0, v2, p1}, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->getCursorFromSelectedItem(Ljava/util/ArrayList;I)Landroid/database/Cursor;

    move-result-object v0

    .line 374
    .local v0, "cur":Landroid/database/Cursor;
    if-eqz v0, :cond_0

    .line 376
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    .line 377
    const-string v3, "_data"

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 378
    .local v1, "idx":I
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mFilePath:Ljava/lang/String;

    .line 379
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 380
    iput p1, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mTrackNumber:I

    goto :goto_0
.end method

.method private getCursorFromSelectedItem(Ljava/util/ArrayList;I)Landroid/database/Cursor;
    .locals 9
    .param p2, "trackNumber"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/Item;",
            ">;I)",
            "Landroid/database/Cursor;"
        }
    .end annotation

    .prologue
    .local p1, "itemList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/Item;>;"
    const/4 v2, 0x0

    .line 384
    iget-object v3, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    invoke-interface {v3}, Lcom/samsung/android/allshare/IAllShareConnector;->isAllShareServiceConnected()Z

    move-result v3

    if-nez v3, :cond_1

    .line 411
    :cond_0
    :goto_0
    return-object v2

    .line 387
    :cond_1
    add-int/lit8 v8, p2, -0x1

    .line 389
    .local v8, "itemIndex":I
    if-ltz v8, :cond_2

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lt v8, v3, :cond_3

    .line 390
    :cond_2
    iput-object v2, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mFilePath:Ljava/lang/String;

    goto :goto_0

    .line 394
    :cond_3
    invoke-virtual {p1, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/android/allshare/Item;

    .line 395
    .local v7, "item":Lcom/samsung/android/allshare/Item;
    if-nez v7, :cond_4

    .line 396
    iput-object v2, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mFilePath:Ljava/lang/String;

    goto :goto_0

    .line 399
    :cond_4
    invoke-virtual {v7}, Lcom/samsung/android/allshare/Item;->getURI()Landroid/net/Uri;

    move-result-object v1

    .line 400
    .local v1, "uri":Landroid/net/Uri;
    if-nez v1, :cond_5

    .line 401
    iput-object v2, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mFilePath:Ljava/lang/String;

    goto :goto_0

    .line 404
    :cond_5
    iget-object v3, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    invoke-interface {v3}, Lcom/samsung/android/allshare/IAllShareConnector;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 405
    .local v0, "resolver":Landroid/content/ContentResolver;
    if-nez v0, :cond_6

    .line 406
    iput-object v2, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mFilePath:Ljava/lang/String;

    goto :goto_0

    :cond_6
    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    .line 410
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .local v6, "cur":Landroid/database/Cursor;
    move-object v2, v6

    .line 411
    goto :goto_0
.end method

.method private registerFilePath(Lcom/samsung/android/allshare/media/Playlist;Ljava/util/ArrayList;I)V
    .locals 5
    .param p1, "playlist"    # Lcom/samsung/android/allshare/media/Playlist;
    .param p3, "trackNumber"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/android/allshare/media/Playlist;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/Item;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .local p2, "itemList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/Item;>;"
    const/4 v2, 0x0

    .line 325
    iput-object p1, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mPlaylist:Lcom/samsung/android/allshare/media/Playlist;

    .line 326
    iput v2, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mTrackNumber:I

    .line 328
    invoke-direct {p0, p2, v2}, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->getCursorFromSelectedItem(Ljava/util/ArrayList;I)Landroid/database/Cursor;

    move-result-object v0

    .line 329
    .local v0, "cur":Landroid/database/Cursor;
    if-nez v0, :cond_0

    .line 330
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mFilePath:Ljava/lang/String;

    .line 339
    :goto_0
    return-void

    .line 333
    :cond_0
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    .line 334
    const-string v2, "_data"

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 335
    .local v1, "idx":I
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mFilePath:Ljava/lang/String;

    .line 336
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 338
    const-string v2, "PlaylistPlayerImpl"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Play:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mFilePath:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method getCurrentFilePath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mFilePath:Ljava/lang/String;

    return-object v0
.end method

.method public getPlayPosition()V
    .locals 5

    .prologue
    .line 517
    iget-object v2, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    invoke-interface {v2}, Lcom/samsung/android/allshare/IAllShareConnector;->isAllShareServiceConnected()Z

    move-result v2

    if-nez v2, :cond_1

    .line 518
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mPlaylistPlayerResponseListener:Lcom/samsung/android/allshare/media/PlaylistPlayer$IPlaylistPlayerPlaybackResponseListener;

    const/4 v3, -0x1

    sget-object v4, Lcom/samsung/android/allshare/ERROR;->SERVICE_NOT_CONNECTED:Lcom/samsung/android/allshare/ERROR;

    invoke-interface {v2, v3, v4}, Lcom/samsung/android/allshare/media/PlaylistPlayer$IPlaylistPlayerPlaybackResponseListener;->onGetPlayPositionResponseReceived(ILcom/samsung/android/allshare/ERROR;)V

    .line 531
    :goto_0
    return-void

    .line 523
    :cond_1
    new-instance v1, Lcom/sec/android/allshare/iface/CVMessage;

    invoke-direct {v1}, Lcom/sec/android/allshare/iface/CVMessage;-><init>()V

    .line 524
    .local v1, "req_msg":Lcom/sec/android/allshare/iface/CVMessage;
    const-string v2, "com.sec.android.allshare.action.ACTION_PLAYLIST_PLAYER_REQUEST_PLAY_POSITION"

    invoke-virtual {v1, v2}, Lcom/sec/android/allshare/iface/CVMessage;->setActionID(Ljava/lang/String;)V

    .line 525
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 527
    .local v0, "req_bundle":Landroid/os/Bundle;
    const-string v2, "BUNDLE_STRING_ID"

    iget-object v3, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    invoke-virtual {v3}, Lcom/samsung/android/allshare/DeviceImpl;->getID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 529
    invoke-virtual {v1, v0}, Lcom/sec/android/allshare/iface/CVMessage;->setBundle(Landroid/os/Bundle;)V

    .line 530
    iget-object v2, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    iget-object v3, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mAllShareRespHandler:Lcom/samsung/android/allshare/AllShareResponseHandler;

    invoke-interface {v2, v1, v3}, Lcom/samsung/android/allshare/IAllShareConnector;->requestCVMAsync(Lcom/sec/android/allshare/iface/CVMessage;Lcom/samsung/android/allshare/AllShareResponseHandler;)J

    goto :goto_0
.end method

.method public isSeekable()Z
    .locals 1

    .prologue
    .line 564
    iget-boolean v0, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mIsSeekable:Z

    return v0
.end method

.method public isSupportAudio()Z
    .locals 9

    .prologue
    const/4 v6, 0x0

    .line 642
    const/4 v5, 0x0

    .line 643
    .local v5, "result":Z
    iget-object v7, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    invoke-interface {v7}, Lcom/samsung/android/allshare/IAllShareConnector;->isAllShareServiceConnected()Z

    move-result v7

    if-nez v7, :cond_1

    .line 668
    :cond_0
    :goto_0
    return v6

    .line 646
    :cond_1
    new-instance v2, Lcom/sec/android/allshare/iface/CVMessage;

    invoke-direct {v2}, Lcom/sec/android/allshare/iface/CVMessage;-><init>()V

    .line 647
    .local v2, "req_msg":Lcom/sec/android/allshare/iface/CVMessage;
    const-string v7, "com.sec.android.allshare.action.ACTION_AV_PLAYER_IS_SUPPORT_AUDIO_PLAYLIST_PLAYER_SYNC"

    invoke-virtual {v2, v7}, Lcom/sec/android/allshare/iface/CVMessage;->setActionID(Ljava/lang/String;)V

    .line 649
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 650
    .local v1, "req_bundle":Landroid/os/Bundle;
    const-string v7, "BUNDLE_STRING_ID"

    iget-object v8, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    invoke-virtual {v8}, Lcom/samsung/android/allshare/DeviceImpl;->getID()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1, v7, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 651
    invoke-virtual {v2, v1}, Lcom/sec/android/allshare/iface/CVMessage;->setBundle(Landroid/os/Bundle;)V

    .line 653
    iget-object v7, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    invoke-interface {v7, v2}, Lcom/samsung/android/allshare/IAllShareConnector;->requestCVMSync(Lcom/sec/android/allshare/iface/CVMessage;)Lcom/sec/android/allshare/iface/CVMessage;

    move-result-object v4

    .line 654
    .local v4, "res_msg":Lcom/sec/android/allshare/iface/CVMessage;
    if-eqz v4, :cond_0

    .line 657
    invoke-virtual {v4}, Lcom/sec/android/allshare/iface/CVMessage;->getBundle()Landroid/os/Bundle;

    move-result-object v3

    .line 659
    .local v3, "res_bundle":Landroid/os/Bundle;
    if-eqz v3, :cond_0

    .line 663
    :try_start_0
    const-string v6, "BUNDLE_BOOLEAN_SUPPORT_AUDIO_PLAYLIST_PLAYER"

    invoke-virtual {v3, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    :goto_1
    move v6, v5

    .line 668
    goto :goto_0

    .line 665
    :catch_0
    move-exception v0

    .line 666
    .local v0, "e":Ljava/lang/Exception;
    const-string v6, "PlaylistPlayerImpl"

    const-string v7, "isSupportAudio Exception"

    invoke-static {v6, v7, v0}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_1
.end method

.method public isSupportImage()Z
    .locals 9

    .prologue
    const/4 v6, 0x0

    .line 611
    const/4 v5, 0x0

    .line 612
    .local v5, "result":Z
    iget-object v7, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    invoke-interface {v7}, Lcom/samsung/android/allshare/IAllShareConnector;->isAllShareServiceConnected()Z

    move-result v7

    if-nez v7, :cond_1

    .line 636
    :cond_0
    :goto_0
    return v6

    .line 615
    :cond_1
    new-instance v2, Lcom/sec/android/allshare/iface/CVMessage;

    invoke-direct {v2}, Lcom/sec/android/allshare/iface/CVMessage;-><init>()V

    .line 616
    .local v2, "req_msg":Lcom/sec/android/allshare/iface/CVMessage;
    const-string v7, "com.sec.android.allshare.action.ACTION_AV_PLAYER_IS_SUPPORT_IMAGE_PLAYLIST_PLAYER_SYNC"

    invoke-virtual {v2, v7}, Lcom/sec/android/allshare/iface/CVMessage;->setActionID(Ljava/lang/String;)V

    .line 618
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 619
    .local v1, "req_bundle":Landroid/os/Bundle;
    const-string v7, "BUNDLE_STRING_ID"

    iget-object v8, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    invoke-virtual {v8}, Lcom/samsung/android/allshare/DeviceImpl;->getID()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1, v7, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 620
    invoke-virtual {v2, v1}, Lcom/sec/android/allshare/iface/CVMessage;->setBundle(Landroid/os/Bundle;)V

    .line 622
    iget-object v7, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    invoke-interface {v7, v2}, Lcom/samsung/android/allshare/IAllShareConnector;->requestCVMSync(Lcom/sec/android/allshare/iface/CVMessage;)Lcom/sec/android/allshare/iface/CVMessage;

    move-result-object v4

    .line 623
    .local v4, "res_msg":Lcom/sec/android/allshare/iface/CVMessage;
    if-eqz v4, :cond_0

    .line 626
    invoke-virtual {v4}, Lcom/sec/android/allshare/iface/CVMessage;->getBundle()Landroid/os/Bundle;

    move-result-object v3

    .line 628
    .local v3, "res_bundle":Landroid/os/Bundle;
    if-eqz v3, :cond_0

    .line 632
    :try_start_0
    const-string v6, "BUNDLE_BOOLEAN_SUPPORT_PLAYLIST_PLAYER"

    invoke-virtual {v3, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    :goto_1
    move v6, v5

    .line 636
    goto :goto_0

    .line 633
    :catch_0
    move-exception v0

    .line 634
    .local v0, "e":Ljava/lang/Exception;
    const-string v6, "PlaylistPlayerImpl"

    const-string v7, "isSupportImage Exception"

    invoke-static {v6, v7, v0}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_1
.end method

.method public isSupportNavigateInPause()Z
    .locals 1

    .prologue
    .line 601
    iget-boolean v0, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mIsNavigateInPause:Z

    return v0
.end method

.method public isSupportSetAutoFlipMode()Z
    .locals 1

    .prologue
    .line 606
    iget-boolean v0, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mIsAutoSlideShowMode:Z

    return v0
.end method

.method public isSupportVideo()Z
    .locals 9

    .prologue
    const/4 v6, 0x0

    .line 674
    const/4 v5, 0x0

    .line 675
    .local v5, "result":Z
    iget-object v7, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    invoke-interface {v7}, Lcom/samsung/android/allshare/IAllShareConnector;->isAllShareServiceConnected()Z

    move-result v7

    if-nez v7, :cond_1

    .line 700
    :cond_0
    :goto_0
    return v6

    .line 678
    :cond_1
    new-instance v2, Lcom/sec/android/allshare/iface/CVMessage;

    invoke-direct {v2}, Lcom/sec/android/allshare/iface/CVMessage;-><init>()V

    .line 679
    .local v2, "req_msg":Lcom/sec/android/allshare/iface/CVMessage;
    const-string v7, "com.sec.android.allshare.action.ACTION_AV_PLAYER_IS_SUPPORT_VIDEO_PLAYLIST_PLAYER_SYNC"

    invoke-virtual {v2, v7}, Lcom/sec/android/allshare/iface/CVMessage;->setActionID(Ljava/lang/String;)V

    .line 681
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 682
    .local v1, "req_bundle":Landroid/os/Bundle;
    const-string v7, "BUNDLE_STRING_ID"

    iget-object v8, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    invoke-virtual {v8}, Lcom/samsung/android/allshare/DeviceImpl;->getID()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1, v7, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 683
    invoke-virtual {v2, v1}, Lcom/sec/android/allshare/iface/CVMessage;->setBundle(Landroid/os/Bundle;)V

    .line 685
    iget-object v7, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    invoke-interface {v7, v2}, Lcom/samsung/android/allshare/IAllShareConnector;->requestCVMSync(Lcom/sec/android/allshare/iface/CVMessage;)Lcom/sec/android/allshare/iface/CVMessage;

    move-result-object v4

    .line 686
    .local v4, "res_msg":Lcom/sec/android/allshare/iface/CVMessage;
    if-eqz v4, :cond_0

    .line 689
    invoke-virtual {v4}, Lcom/sec/android/allshare/iface/CVMessage;->getBundle()Landroid/os/Bundle;

    move-result-object v3

    .line 691
    .local v3, "res_bundle":Landroid/os/Bundle;
    if-eqz v3, :cond_0

    .line 695
    :try_start_0
    const-string v6, "BUNDLE_BOOLEAN_SUPPORT_VIDEO_PLAYLIST_PLAYER"

    invoke-virtual {v3, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    :goto_1
    move v6, v5

    .line 700
    goto :goto_0

    .line 697
    :catch_0
    move-exception v0

    .line 698
    .local v0, "e":Ljava/lang/Exception;
    const-string v6, "PlaylistPlayerImpl"

    const-string v7, "isSupportVideo Exception"

    invoke-static {v6, v7, v0}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_1
.end method

.method public next()V
    .locals 4

    .prologue
    .line 440
    iget-object v2, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    invoke-interface {v2}, Lcom/samsung/android/allshare/IAllShareConnector;->isAllShareServiceConnected()Z

    move-result v2

    if-nez v2, :cond_2

    .line 441
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mPlaylistPlayerResponseListener:Lcom/samsung/android/allshare/media/PlaylistPlayer$IPlaylistPlayerPlaybackResponseListener;

    sget-object v3, Lcom/samsung/android/allshare/ERROR;->SERVICE_NOT_CONNECTED:Lcom/samsung/android/allshare/ERROR;

    invoke-interface {v2, v3}, Lcom/samsung/android/allshare/media/PlaylistPlayer$IPlaylistPlayerPlaybackResponseListener;->onNextResponseReceived(Lcom/samsung/android/allshare/ERROR;)V

    .line 461
    :cond_1
    :goto_0
    return-void

    .line 445
    :cond_2
    new-instance v1, Lcom/sec/android/allshare/iface/CVMessage;

    invoke-direct {v1}, Lcom/sec/android/allshare/iface/CVMessage;-><init>()V

    .line 446
    .local v1, "req_msg":Lcom/sec/android/allshare/iface/CVMessage;
    const-string v2, "com.sec.android.allshare.action.ACTION_PLAYLIST_PLAYER_NEXT"

    invoke-virtual {v1, v2}, Lcom/sec/android/allshare/iface/CVMessage;->setActionID(Ljava/lang/String;)V

    .line 447
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 449
    .local v0, "req_bundle":Landroid/os/Bundle;
    const-string v2, "BUNDLE_STRING_ID"

    iget-object v3, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    invoke-virtual {v3}, Lcom/samsung/android/allshare/DeviceImpl;->getID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 451
    invoke-virtual {v1, v0}, Lcom/sec/android/allshare/iface/CVMessage;->setBundle(Landroid/os/Bundle;)V

    .line 452
    iget-object v2, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    iget-object v3, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mAllShareRespHandler:Lcom/samsung/android/allshare/AllShareResponseHandler;

    invoke-interface {v2, v1, v3}, Lcom/samsung/android/allshare/IAllShareConnector;->requestCVMAsync(Lcom/sec/android/allshare/iface/CVMessage;Lcom/samsung/android/allshare/AllShareResponseHandler;)J

    .line 454
    iget-object v2, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mPlaylist:Lcom/samsung/android/allshare/media/Playlist;

    if-eqz v2, :cond_1

    .line 455
    iget v2, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mTrackNumber:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mTrackNumber:I

    .line 456
    iget v2, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mTrackNumber:I

    iget-object v3, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mPlaylist:Lcom/samsung/android/allshare/media/Playlist;

    invoke-virtual {v3}, Lcom/samsung/android/allshare/media/Playlist;->getPlaylist()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lt v2, v3, :cond_3

    iget-object v2, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mPlaylist:Lcom/samsung/android/allshare/media/Playlist;

    invoke-virtual {v2}, Lcom/samsung/android/allshare/media/Playlist;->getPlaylist()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    :goto_1
    iput v2, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mTrackNumber:I

    .line 459
    iget v2, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mTrackNumber:I

    invoke-direct {p0, v2}, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->createFilePathForZoom(I)V

    goto :goto_0

    .line 456
    :cond_3
    iget v2, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mTrackNumber:I

    goto :goto_1
.end method

.method public pause()V
    .locals 4

    .prologue
    .line 483
    iget-object v2, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    invoke-interface {v2}, Lcom/samsung/android/allshare/IAllShareConnector;->isAllShareServiceConnected()Z

    move-result v2

    if-nez v2, :cond_1

    .line 484
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mPlaylistPlayerResponseListener:Lcom/samsung/android/allshare/media/PlaylistPlayer$IPlaylistPlayerPlaybackResponseListener;

    sget-object v3, Lcom/samsung/android/allshare/ERROR;->SERVICE_NOT_CONNECTED:Lcom/samsung/android/allshare/ERROR;

    invoke-interface {v2, v3}, Lcom/samsung/android/allshare/media/PlaylistPlayer$IPlaylistPlayerPlaybackResponseListener;->onPauseResponseReceived(Lcom/samsung/android/allshare/ERROR;)V

    .line 495
    :goto_0
    return-void

    .line 488
    :cond_1
    new-instance v1, Lcom/sec/android/allshare/iface/CVMessage;

    invoke-direct {v1}, Lcom/sec/android/allshare/iface/CVMessage;-><init>()V

    .line 489
    .local v1, "req_msg":Lcom/sec/android/allshare/iface/CVMessage;
    const-string v2, "com.sec.android.allshare.action.ACTION_PLAYLIST_PLAYER_PAUSE"

    invoke-virtual {v1, v2}, Lcom/sec/android/allshare/iface/CVMessage;->setActionID(Ljava/lang/String;)V

    .line 490
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 491
    .local v0, "req_bundle":Landroid/os/Bundle;
    const-string v2, "BUNDLE_STRING_ID"

    iget-object v3, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    invoke-virtual {v3}, Lcom/samsung/android/allshare/DeviceImpl;->getID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 493
    invoke-virtual {v1, v0}, Lcom/sec/android/allshare/iface/CVMessage;->setBundle(Landroid/os/Bundle;)V

    .line 494
    iget-object v2, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    iget-object v3, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mAllShareRespHandler:Lcom/samsung/android/allshare/AllShareResponseHandler;

    invoke-interface {v2, v1, v3}, Lcom/samsung/android/allshare/IAllShareConnector;->requestCVMAsync(Lcom/sec/android/allshare/iface/CVMessage;Lcom/samsung/android/allshare/AllShareResponseHandler;)J

    goto :goto_0
.end method

.method public play(Lcom/samsung/android/allshare/media/Playlist;I)V
    .locals 10
    .param p1, "playlist"    # Lcom/samsung/android/allshare/media/Playlist;
    .param p2, "trackNumber"    # I

    .prologue
    const/4 v9, 0x0

    .line 263
    iget-object v7, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    invoke-interface {v7}, Lcom/samsung/android/allshare/IAllShareConnector;->isAllShareServiceConnected()Z

    move-result v7

    if-nez v7, :cond_1

    .line 264
    :cond_0
    iget-object v7, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mPlaylistPlayerResponseListener:Lcom/samsung/android/allshare/media/PlaylistPlayer$IPlaylistPlayerPlaybackResponseListener;

    sget-object v8, Lcom/samsung/android/allshare/ERROR;->SERVICE_NOT_CONNECTED:Lcom/samsung/android/allshare/ERROR;

    invoke-interface {v7, p1, p2, v8}, Lcom/samsung/android/allshare/media/PlaylistPlayer$IPlaylistPlayerPlaybackResponseListener;->onPlayResponseReceived(Lcom/samsung/android/allshare/media/Playlist;ILcom/samsung/android/allshare/ERROR;)V

    .line 322
    :goto_0
    return-void

    .line 269
    :cond_1
    if-nez p1, :cond_2

    .line 270
    iget-object v7, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mPlaylistPlayerResponseListener:Lcom/samsung/android/allshare/media/PlaylistPlayer$IPlaylistPlayerPlaybackResponseListener;

    sget-object v8, Lcom/samsung/android/allshare/ERROR;->INVALID_ARGUMENT:Lcom/samsung/android/allshare/ERROR;

    invoke-interface {v7, p1, p2, v8}, Lcom/samsung/android/allshare/media/PlaylistPlayer$IPlaylistPlayerPlaybackResponseListener;->onPlayResponseReceived(Lcom/samsung/android/allshare/media/Playlist;ILcom/samsung/android/allshare/ERROR;)V

    .line 272
    iput-object v9, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mFilePath:Ljava/lang/String;

    goto :goto_0

    .line 276
    :cond_2
    const/4 v7, 0x1

    if-ge p2, v7, :cond_3

    .line 277
    iget-object v7, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mPlaylistPlayerResponseListener:Lcom/samsung/android/allshare/media/PlaylistPlayer$IPlaylistPlayerPlaybackResponseListener;

    sget-object v8, Lcom/samsung/android/allshare/ERROR;->INVALID_ARGUMENT:Lcom/samsung/android/allshare/ERROR;

    invoke-interface {v7, p1, p2, v8}, Lcom/samsung/android/allshare/media/PlaylistPlayer$IPlaylistPlayerPlaybackResponseListener;->onPlayResponseReceived(Lcom/samsung/android/allshare/media/Playlist;ILcom/samsung/android/allshare/ERROR;)V

    .line 279
    iput-object v9, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mFilePath:Ljava/lang/String;

    goto :goto_0

    .line 283
    :cond_3
    invoke-virtual {p1}, Lcom/samsung/android/allshare/media/Playlist;->getPlaylist()Ljava/util/ArrayList;

    move-result-object v3

    .line 284
    .local v3, "itemList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/Item;>;"
    if-eqz v3, :cond_4

    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_5

    .line 285
    :cond_4
    iget-object v7, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mPlaylistPlayerResponseListener:Lcom/samsung/android/allshare/media/PlaylistPlayer$IPlaylistPlayerPlaybackResponseListener;

    sget-object v8, Lcom/samsung/android/allshare/ERROR;->INVALID_ARGUMENT:Lcom/samsung/android/allshare/ERROR;

    invoke-interface {v7, p1, p2, v8}, Lcom/samsung/android/allshare/media/PlaylistPlayer$IPlaylistPlayerPlaybackResponseListener;->onPlayResponseReceived(Lcom/samsung/android/allshare/media/Playlist;ILcom/samsung/android/allshare/ERROR;)V

    .line 287
    iput-object v9, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mFilePath:Ljava/lang/String;

    goto :goto_0

    .line 291
    :cond_5
    iput-object v9, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mCurrentPlayingContentUriStrList:Ljava/util/ArrayList;

    .line 293
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 294
    .local v0, "bundleList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/os/Bundle;>;"
    const-string v4, ""

    .line 296
    .local v4, "mimeType":Ljava/lang/String;
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_6
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/allshare/Item;

    .line 297
    .local v2, "item":Lcom/samsung/android/allshare/Item;
    if-eqz v2, :cond_6

    .line 298
    invoke-virtual {v2}, Lcom/samsung/android/allshare/Item;->getMimetype()Ljava/lang/String;

    move-result-object v4

    .line 300
    instance-of v7, v2, Lcom/sec/android/allshare/iface/IBundleHolder;

    if-eqz v7, :cond_6

    .line 301
    check-cast v2, Lcom/sec/android/allshare/iface/IBundleHolder;

    .end local v2    # "item":Lcom/samsung/android/allshare/Item;
    invoke-interface {v2}, Lcom/sec/android/allshare/iface/IBundleHolder;->getBundle()Landroid/os/Bundle;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 306
    :cond_7
    new-instance v6, Lcom/sec/android/allshare/iface/CVMessage;

    invoke-direct {v6}, Lcom/sec/android/allshare/iface/CVMessage;-><init>()V

    .line 307
    .local v6, "req_msg":Lcom/sec/android/allshare/iface/CVMessage;
    const-string v7, "com.sec.android.allshare.action.ACTION_PLAYLIST_PLAYER_PLAY"

    invoke-virtual {v6, v7}, Lcom/sec/android/allshare/iface/CVMessage;->setActionID(Ljava/lang/String;)V

    .line 309
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 310
    .local v5, "req_bundle":Landroid/os/Bundle;
    const-string v7, "BUNDLE_PARCELABLE_ARRAYLIST_CONTENT_URI"

    invoke-virtual {v5, v7, v0}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 313
    const-string v7, "BUNDLE_STRING_ID"

    iget-object v8, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    invoke-virtual {v8}, Lcom/samsung/android/allshare/DeviceImpl;->getID()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v7, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 314
    const-string v7, "BUNDLE_STRING_MIME_TYPE"

    invoke-virtual {v5, v7, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 315
    const-string v7, "BUNDLE_INT_PLAYLIST_TRACKNUMBER"

    invoke-virtual {v5, v7, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 317
    invoke-virtual {v6, v5}, Lcom/sec/android/allshare/iface/CVMessage;->setBundle(Landroid/os/Bundle;)V

    .line 318
    iget-object v7, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    iget-object v8, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mAllShareRespHandler:Lcom/samsung/android/allshare/AllShareResponseHandler;

    invoke-interface {v7, v6, v8}, Lcom/samsung/android/allshare/IAllShareConnector;->requestCVMAsync(Lcom/sec/android/allshare/iface/CVMessage;Lcom/samsung/android/allshare/AllShareResponseHandler;)J

    .line 320
    invoke-direct {p0, p1, v3, p2}, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->registerFilePath(Lcom/samsung/android/allshare/media/Playlist;Ljava/util/ArrayList;I)V

    goto/16 :goto_0
.end method

.method public previous()V
    .locals 4

    .prologue
    .line 416
    iget-object v2, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    invoke-interface {v2}, Lcom/samsung/android/allshare/IAllShareConnector;->isAllShareServiceConnected()Z

    move-result v2

    if-nez v2, :cond_2

    .line 417
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mPlaylistPlayerResponseListener:Lcom/samsung/android/allshare/media/PlaylistPlayer$IPlaylistPlayerPlaybackResponseListener;

    sget-object v3, Lcom/samsung/android/allshare/ERROR;->SERVICE_NOT_CONNECTED:Lcom/samsung/android/allshare/ERROR;

    invoke-interface {v2, v3}, Lcom/samsung/android/allshare/media/PlaylistPlayer$IPlaylistPlayerPlaybackResponseListener;->onPreviousResponseReceived(Lcom/samsung/android/allshare/ERROR;)V

    .line 436
    :cond_1
    :goto_0
    return-void

    .line 421
    :cond_2
    new-instance v1, Lcom/sec/android/allshare/iface/CVMessage;

    invoke-direct {v1}, Lcom/sec/android/allshare/iface/CVMessage;-><init>()V

    .line 422
    .local v1, "req_msg":Lcom/sec/android/allshare/iface/CVMessage;
    const-string v2, "com.sec.android.allshare.action.ACTION_PLAYLIST_PLAYER_PERVIOUS"

    invoke-virtual {v1, v2}, Lcom/sec/android/allshare/iface/CVMessage;->setActionID(Ljava/lang/String;)V

    .line 423
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 425
    .local v0, "req_bundle":Landroid/os/Bundle;
    const-string v2, "BUNDLE_STRING_ID"

    iget-object v3, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    invoke-virtual {v3}, Lcom/samsung/android/allshare/DeviceImpl;->getID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 427
    invoke-virtual {v1, v0}, Lcom/sec/android/allshare/iface/CVMessage;->setBundle(Landroid/os/Bundle;)V

    .line 428
    iget-object v2, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    iget-object v3, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mAllShareRespHandler:Lcom/samsung/android/allshare/AllShareResponseHandler;

    invoke-interface {v2, v1, v3}, Lcom/samsung/android/allshare/IAllShareConnector;->requestCVMAsync(Lcom/sec/android/allshare/iface/CVMessage;Lcom/samsung/android/allshare/AllShareResponseHandler;)J

    .line 430
    iget-object v2, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mPlaylist:Lcom/samsung/android/allshare/media/Playlist;

    if-eqz v2, :cond_1

    .line 431
    iget v2, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mTrackNumber:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mTrackNumber:I

    .line 432
    iget v2, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mTrackNumber:I

    if-gez v2, :cond_3

    const/4 v2, 0x0

    :goto_1
    iput v2, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mTrackNumber:I

    .line 434
    iget v2, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mTrackNumber:I

    invoke-direct {p0, v2}, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->createFilePathForZoom(I)V

    goto :goto_0

    .line 432
    :cond_3
    iget v2, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mTrackNumber:I

    goto :goto_1
.end method

.method public removeEventHandler()V
    .locals 4

    .prologue
    .line 705
    iget-object v0, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    const-string v1, "com.sec.android.allshare.event.EVENT_DEVICE_SUBSCRIBE"

    iget-object v2, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    invoke-virtual {v2}, Lcom/samsung/android/allshare/DeviceImpl;->getBundle()Landroid/os/Bundle;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mEventHandler:Lcom/samsung/android/allshare/AllShareEventHandler;

    invoke-interface {v0, v1, v2, v3}, Lcom/samsung/android/allshare/IAllShareConnector;->unsubscribeAllShareEvent(Ljava/lang/String;Landroid/os/Bundle;Lcom/samsung/android/allshare/AllShareEventHandler;)V

    .line 707
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mIsSubscribed:Z

    .line 708
    return-void
.end method

.method public resume()V
    .locals 4

    .prologue
    .line 499
    iget-object v2, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    invoke-interface {v2}, Lcom/samsung/android/allshare/IAllShareConnector;->isAllShareServiceConnected()Z

    move-result v2

    if-nez v2, :cond_1

    .line 500
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mPlaylistPlayerResponseListener:Lcom/samsung/android/allshare/media/PlaylistPlayer$IPlaylistPlayerPlaybackResponseListener;

    sget-object v3, Lcom/samsung/android/allshare/ERROR;->SERVICE_NOT_CONNECTED:Lcom/samsung/android/allshare/ERROR;

    invoke-interface {v2, v3}, Lcom/samsung/android/allshare/media/PlaylistPlayer$IPlaylistPlayerPlaybackResponseListener;->onResumeResponseReceived(Lcom/samsung/android/allshare/ERROR;)V

    .line 513
    :goto_0
    return-void

    .line 504
    :cond_1
    new-instance v1, Lcom/sec/android/allshare/iface/CVMessage;

    invoke-direct {v1}, Lcom/sec/android/allshare/iface/CVMessage;-><init>()V

    .line 505
    .local v1, "req_msg":Lcom/sec/android/allshare/iface/CVMessage;
    const-string v2, "com.sec.android.allshare.action.ACTION_PLAYLIST_PLAYER_RESUME"

    invoke-virtual {v1, v2}, Lcom/sec/android/allshare/iface/CVMessage;->setActionID(Ljava/lang/String;)V

    .line 506
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 508
    .local v0, "req_bundle":Landroid/os/Bundle;
    const-string v2, "BUNDLE_STRING_ID"

    iget-object v3, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    invoke-virtual {v3}, Lcom/samsung/android/allshare/DeviceImpl;->getID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 510
    invoke-virtual {v1, v0}, Lcom/sec/android/allshare/iface/CVMessage;->setBundle(Landroid/os/Bundle;)V

    .line 511
    iget-object v2, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    iget-object v3, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mAllShareRespHandler:Lcom/samsung/android/allshare/AllShareResponseHandler;

    invoke-interface {v2, v1, v3}, Lcom/samsung/android/allshare/IAllShareConnector;->requestCVMAsync(Lcom/sec/android/allshare/iface/CVMessage;Lcom/samsung/android/allshare/AllShareResponseHandler;)J

    goto :goto_0
.end method

.method public seek(I)V
    .locals 4
    .param p1, "trackNumber"    # I

    .prologue
    .line 343
    iget-object v2, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    invoke-interface {v2}, Lcom/samsung/android/allshare/IAllShareConnector;->isAllShareServiceConnected()Z

    move-result v2

    if-nez v2, :cond_1

    .line 365
    :cond_0
    :goto_0
    return-void

    .line 346
    :cond_1
    const/4 v2, 0x1

    if-ge p1, v2, :cond_2

    .line 347
    iget-object v2, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mPlaylistPlayerResponseListener:Lcom/samsung/android/allshare/media/PlaylistPlayer$IPlaylistPlayerPlaybackResponseListener;

    sget-object v3, Lcom/samsung/android/allshare/ERROR;->INVALID_ARGUMENT:Lcom/samsung/android/allshare/ERROR;

    invoke-interface {v2, p1, v3}, Lcom/samsung/android/allshare/media/PlaylistPlayer$IPlaylistPlayerPlaybackResponseListener;->onSeekResponseReceived(ILcom/samsung/android/allshare/ERROR;)V

    goto :goto_0

    .line 353
    :cond_2
    new-instance v1, Lcom/sec/android/allshare/iface/CVMessage;

    invoke-direct {v1}, Lcom/sec/android/allshare/iface/CVMessage;-><init>()V

    .line 354
    .local v1, "mMessage":Lcom/sec/android/allshare/iface/CVMessage;
    const-string v2, "com.sec.android.allshare.action.ACTION_PLAYLIST_PLAYER_SEEK"

    invoke-virtual {v1, v2}, Lcom/sec/android/allshare/iface/CVMessage;->setActionID(Ljava/lang/String;)V

    .line 355
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 357
    .local v0, "mBundle":Landroid/os/Bundle;
    const-string v2, "BUNDLE_STRING_ID"

    iget-object v3, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    invoke-virtual {v3}, Lcom/samsung/android/allshare/DeviceImpl;->getID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 358
    const-string v2, "BUNDLE_INT_PLAYLIST_TRACKNUMBER"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 360
    invoke-virtual {v1, v0}, Lcom/sec/android/allshare/iface/CVMessage;->setBundle(Landroid/os/Bundle;)V

    .line 361
    iget-object v2, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    iget-object v3, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mAllShareRespHandler:Lcom/samsung/android/allshare/AllShareResponseHandler;

    invoke-interface {v2, v1, v3}, Lcom/samsung/android/allshare/IAllShareConnector;->requestCVMAsync(Lcom/sec/android/allshare/iface/CVMessage;Lcom/samsung/android/allshare/AllShareResponseHandler;)J

    .line 363
    add-int/lit8 v2, p1, -0x1

    invoke-direct {p0, v2}, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->createFilePathForZoom(I)V

    goto :goto_0
.end method

.method public setAutoFlipMode(Z)V
    .locals 4
    .param p1, "onoff"    # Z

    .prologue
    .line 569
    iget-object v2, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    invoke-interface {v2}, Lcom/samsung/android/allshare/IAllShareConnector;->isAllShareServiceConnected()Z

    move-result v2

    if-nez v2, :cond_1

    .line 581
    :cond_0
    :goto_0
    return-void

    .line 572
    :cond_1
    new-instance v1, Lcom/sec/android/allshare/iface/CVMessage;

    invoke-direct {v1}, Lcom/sec/android/allshare/iface/CVMessage;-><init>()V

    .line 573
    .local v1, "req_msg":Lcom/sec/android/allshare/iface/CVMessage;
    const-string v2, "com.sec.android.allshare.action.ACTION_PLAYLIST_PLAYER_SET_AUTO_FLIP_MODE"

    invoke-virtual {v1, v2}, Lcom/sec/android/allshare/iface/CVMessage;->setActionID(Ljava/lang/String;)V

    .line 574
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 576
    .local v0, "req_bundle":Landroid/os/Bundle;
    const-string v2, "BUNDLE_STRING_ID"

    iget-object v3, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    invoke-virtual {v3}, Lcom/samsung/android/allshare/DeviceImpl;->getID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 577
    const-string v2, "BUNDLE_BOOLEAN_AUTO_SLIDE_SHOW"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 579
    invoke-virtual {v1, v0}, Lcom/sec/android/allshare/iface/CVMessage;->setBundle(Landroid/os/Bundle;)V

    .line 580
    iget-object v2, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    iget-object v3, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mAllShareRespHandler:Lcom/samsung/android/allshare/AllShareResponseHandler;

    invoke-interface {v2, v1, v3}, Lcom/samsung/android/allshare/IAllShareConnector;->requestCVMAsync(Lcom/sec/android/allshare/iface/CVMessage;Lcom/samsung/android/allshare/AllShareResponseHandler;)J

    goto :goto_0
.end method

.method setCurrentFilePath(Ljava/lang/String;)V
    .locals 0
    .param p1, "strFilePath"    # Ljava/lang/String;

    .prologue
    .line 98
    iput-object p1, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mFilePath:Ljava/lang/String;

    .line 99
    return-void
.end method

.method public setPlaybackResponseListener(Lcom/samsung/android/allshare/media/PlaylistPlayer$IPlaylistPlayerPlaybackResponseListener;)V
    .locals 0
    .param p1, "l"    # Lcom/samsung/android/allshare/media/PlaylistPlayer$IPlaylistPlayerPlaybackResponseListener;

    .prologue
    .line 535
    iput-object p1, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mPlaylistPlayerResponseListener:Lcom/samsung/android/allshare/media/PlaylistPlayer$IPlaylistPlayerPlaybackResponseListener;

    .line 537
    return-void
.end method

.method public setPlaylistPlayerEventListener(Lcom/samsung/android/allshare/media/PlaylistPlayer$IPlaylistPlayerEventListener;)V
    .locals 4
    .param p1, "l"    # Lcom/samsung/android/allshare/media/PlaylistPlayer$IPlaylistPlayerEventListener;

    .prologue
    .line 541
    iget-object v0, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    invoke-interface {v0}, Lcom/samsung/android/allshare/IAllShareConnector;->isAllShareServiceConnected()Z

    move-result v0

    if-nez v0, :cond_2

    .line 542
    :cond_0
    const-string v0, "PlaylistPlayerImpl"

    const-string v1, "setEventListener error! AllShareService is not connected"

    invoke-static {v0, v1}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 560
    :cond_1
    :goto_0
    return-void

    .line 546
    :cond_2
    iput-object p1, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mPlaylistPlayerEventListener:Lcom/samsung/android/allshare/media/PlaylistPlayer$IPlaylistPlayerEventListener;

    .line 548
    iget-boolean v0, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mIsSubscribed:Z

    if-nez v0, :cond_3

    if-eqz p1, :cond_3

    .line 549
    iget-object v0, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    const-string v1, "com.sec.android.allshare.event.EVENT_DEVICE_SUBSCRIBE"

    iget-object v2, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    invoke-virtual {v2}, Lcom/samsung/android/allshare/DeviceImpl;->getBundle()Landroid/os/Bundle;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mEventHandler:Lcom/samsung/android/allshare/AllShareEventHandler;

    invoke-interface {v0, v1, v2, v3}, Lcom/samsung/android/allshare/IAllShareConnector;->subscribeAllShareEvent(Ljava/lang/String;Landroid/os/Bundle;Lcom/samsung/android/allshare/AllShareEventHandler;)Z

    .line 551
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mIsSubscribed:Z

    goto :goto_0

    .line 552
    :cond_3
    iget-boolean v0, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mIsSubscribed:Z

    if-eqz v0, :cond_1

    if-nez p1, :cond_1

    .line 553
    iget-object v0, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    const-string v1, "com.sec.android.allshare.event.EVENT_DEVICE_SUBSCRIBE"

    iget-object v2, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    invoke-virtual {v2}, Lcom/samsung/android/allshare/DeviceImpl;->getBundle()Landroid/os/Bundle;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mEventHandler:Lcom/samsung/android/allshare/AllShareEventHandler;

    invoke-interface {v0, v1, v2, v3}, Lcom/samsung/android/allshare/IAllShareConnector;->unsubscribeAllShareEvent(Ljava/lang/String;Landroid/os/Bundle;Lcom/samsung/android/allshare/AllShareEventHandler;)V

    .line 555
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mIsSubscribed:Z

    goto :goto_0
.end method

.method public setQuickNavigate(Z)V
    .locals 4
    .param p1, "onoff"    # Z

    .prologue
    .line 585
    iget-object v2, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    invoke-interface {v2}, Lcom/samsung/android/allshare/IAllShareConnector;->isAllShareServiceConnected()Z

    move-result v2

    if-nez v2, :cond_1

    .line 597
    :cond_0
    :goto_0
    return-void

    .line 588
    :cond_1
    new-instance v1, Lcom/sec/android/allshare/iface/CVMessage;

    invoke-direct {v1}, Lcom/sec/android/allshare/iface/CVMessage;-><init>()V

    .line 589
    .local v1, "req_msg":Lcom/sec/android/allshare/iface/CVMessage;
    const-string v2, "com.sec.android.allshare.action.ACTION_PLAYLIST_PLAYER_SET_QUICK_NAVIGATE"

    invoke-virtual {v1, v2}, Lcom/sec/android/allshare/iface/CVMessage;->setActionID(Ljava/lang/String;)V

    .line 590
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 592
    .local v0, "req_bundle":Landroid/os/Bundle;
    const-string v2, "BUNDLE_STRING_ID"

    iget-object v3, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    invoke-virtual {v3}, Lcom/samsung/android/allshare/DeviceImpl;->getID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 593
    const-string v2, "BUNDLE_BOOLEAN_NAVIGATE_IN_PAUSE"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 595
    invoke-virtual {v1, v0}, Lcom/sec/android/allshare/iface/CVMessage;->setBundle(Landroid/os/Bundle;)V

    .line 596
    iget-object v2, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    iget-object v3, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mAllShareRespHandler:Lcom/samsung/android/allshare/AllShareResponseHandler;

    invoke-interface {v2, v1, v3}, Lcom/samsung/android/allshare/IAllShareConnector;->requestCVMAsync(Lcom/sec/android/allshare/iface/CVMessage;Lcom/samsung/android/allshare/AllShareResponseHandler;)J

    goto :goto_0
.end method

.method public stop()V
    .locals 4

    .prologue
    .line 465
    iget-object v2, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    invoke-interface {v2}, Lcom/samsung/android/allshare/IAllShareConnector;->isAllShareServiceConnected()Z

    move-result v2

    if-nez v2, :cond_1

    .line 466
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mPlaylistPlayerResponseListener:Lcom/samsung/android/allshare/media/PlaylistPlayer$IPlaylistPlayerPlaybackResponseListener;

    sget-object v3, Lcom/samsung/android/allshare/ERROR;->SERVICE_NOT_CONNECTED:Lcom/samsung/android/allshare/ERROR;

    invoke-interface {v2, v3}, Lcom/samsung/android/allshare/media/PlaylistPlayer$IPlaylistPlayerPlaybackResponseListener;->onStopResponseReceived(Lcom/samsung/android/allshare/ERROR;)V

    .line 479
    :goto_0
    return-void

    .line 470
    :cond_1
    new-instance v1, Lcom/sec/android/allshare/iface/CVMessage;

    invoke-direct {v1}, Lcom/sec/android/allshare/iface/CVMessage;-><init>()V

    .line 471
    .local v1, "req_msg":Lcom/sec/android/allshare/iface/CVMessage;
    const-string v2, "com.sec.android.allshare.action.ACTION_PLAYLIST_PLAYER_STOP"

    invoke-virtual {v1, v2}, Lcom/sec/android/allshare/iface/CVMessage;->setActionID(Ljava/lang/String;)V

    .line 472
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 474
    .local v0, "req_bundle":Landroid/os/Bundle;
    const-string v2, "BUNDLE_STRING_ID"

    iget-object v3, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    invoke-virtual {v3}, Lcom/samsung/android/allshare/DeviceImpl;->getID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 476
    invoke-virtual {v1, v0}, Lcom/sec/android/allshare/iface/CVMessage;->setBundle(Landroid/os/Bundle;)V

    .line 477
    iget-object v2, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    iget-object v3, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mAllShareRespHandler:Lcom/samsung/android/allshare/AllShareResponseHandler;

    invoke-interface {v2, v1, v3}, Lcom/samsung/android/allshare/IAllShareConnector;->requestCVMAsync(Lcom/sec/android/allshare/iface/CVMessage;Lcom/samsung/android/allshare/AllShareResponseHandler;)J

    goto :goto_0
.end method
