.class public Lcom/samsung/android/allshare/Item$LocalContentBuilder;
.super Ljava/lang/Object;
.source "Item.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/allshare/Item;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "LocalContentBuilder"
.end annotation


# instance fields
.field private mFilepath:Ljava/lang/String;

.field private mMimetype:Ljava/lang/String;

.field private mSubtitlePath:Ljava/lang/String;

.field private mTitle:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "filepath"    # Ljava/lang/String;
    .param p2, "mimeType"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 619
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 602
    iput-object v0, p0, Lcom/samsung/android/allshare/Item$LocalContentBuilder;->mFilepath:Ljava/lang/String;

    .line 604
    iput-object v0, p0, Lcom/samsung/android/allshare/Item$LocalContentBuilder;->mMimetype:Ljava/lang/String;

    .line 606
    iput-object v0, p0, Lcom/samsung/android/allshare/Item$LocalContentBuilder;->mTitle:Ljava/lang/String;

    .line 608
    iput-object v0, p0, Lcom/samsung/android/allshare/Item$LocalContentBuilder;->mSubtitlePath:Ljava/lang/String;

    .line 620
    iput-object p1, p0, Lcom/samsung/android/allshare/Item$LocalContentBuilder;->mFilepath:Ljava/lang/String;

    .line 621
    iput-object p2, p0, Lcom/samsung/android/allshare/Item$LocalContentBuilder;->mMimetype:Ljava/lang/String;

    .line 622
    return-void
.end method

.method private checkFilePathValid(Ljava/lang/String;)Z
    .locals 13
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    const/4 v12, 0x1

    const/4 v2, 0x0

    const/4 v11, 0x0

    .line 693
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    move v0, v11

    .line 741
    :goto_0
    return v0

    .line 697
    :cond_1
    const-string v0, "content:"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 698
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 699
    .local v1, "uri":Landroid/net/Uri;
    if-nez v1, :cond_2

    move v0, v11

    .line 700
    goto :goto_0

    .line 703
    :cond_2
    invoke-static {}, Lcom/samsung/android/allshare/ServiceConnector;->getContext()Landroid/content/Context;

    move-result-object v8

    .line 704
    .local v8, "context":Landroid/content/Context;
    if-nez v8, :cond_3

    move v0, v11

    .line 705
    goto :goto_0

    .line 708
    :cond_3
    invoke-virtual {v8}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 710
    .local v7, "contentCursor":Landroid/database/Cursor;
    if-eqz v7, :cond_4

    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-ne v0, v12, :cond_4

    .line 711
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    move v0, v12

    .line 712
    goto :goto_0

    .line 715
    :cond_4
    if-eqz v7, :cond_5

    .line 716
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_5
    move v0, v11

    .line 719
    goto :goto_0

    .line 721
    .end local v1    # "uri":Landroid/net/Uri;
    .end local v7    # "contentCursor":Landroid/database/Cursor;
    .end local v8    # "context":Landroid/content/Context;
    :cond_6
    const/4 v6, 0x0

    .line 722
    .local v6, "absoluteFilePath":Ljava/lang/String;
    const-string v10, "file://"

    .line 724
    .local v10, "headString":Ljava/lang/String;
    const-string v0, "file:"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 725
    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v6

    .line 730
    :goto_1
    const-string v0, "/data/data"

    invoke-virtual {v6, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    move v0, v11

    .line 731
    goto :goto_0

    .line 727
    :cond_7
    move-object v6, p1

    goto :goto_1

    .line 733
    :cond_8
    new-instance v9, Ljava/io/File;

    invoke-direct {v9, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 735
    .local v9, "file":Ljava/io/File;
    if-eqz v9, :cond_9

    invoke-virtual {v9}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_9

    move v0, v12

    .line 736
    goto :goto_0

    :cond_9
    move v0, v11

    .line 741
    goto :goto_0
.end method


# virtual methods
.method public build()Lcom/samsung/android/allshare/Item;
    .locals 8

    .prologue
    const/4 v6, 0x0

    .line 665
    iget-object v0, p0, Lcom/samsung/android/allshare/Item$LocalContentBuilder;->mFilepath:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/samsung/android/allshare/Item$LocalContentBuilder;->checkFilePathValid(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 689
    :goto_0
    return-object v6

    .line 670
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/allshare/Item$LocalContentBuilder;->mSubtitlePath:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/samsung/android/allshare/Item$LocalContentBuilder;->checkFilePathValid(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 671
    iput-object v6, p0, Lcom/samsung/android/allshare/Item$LocalContentBuilder;->mSubtitlePath:Ljava/lang/String;

    .line 673
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/allshare/Item$LocalContentBuilder;->mFilepath:Ljava/lang/String;

    const-string v1, "content:"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 675
    new-instance v0, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;

    sget-object v1, Lcom/samsung/android/allshare/ItemCreator$ConstructorType;->LOCAL_CONTENT:Lcom/samsung/android/allshare/ItemCreator$ConstructorType;

    iget-object v2, p0, Lcom/samsung/android/allshare/Item$LocalContentBuilder;->mFilepath:Ljava/lang/String;

    iget-object v3, p0, Lcom/samsung/android/allshare/Item$LocalContentBuilder;->mTitle:Ljava/lang/String;

    iget-object v4, p0, Lcom/samsung/android/allshare/Item$LocalContentBuilder;->mSubtitlePath:Ljava/lang/String;

    iget-object v5, p0, Lcom/samsung/android/allshare/Item$LocalContentBuilder;->mMimetype:Ljava/lang/String;

    invoke-direct/range {v0 .. v6}, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;-><init>(Lcom/samsung/android/allshare/ItemCreator$ConstructorType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/Item$1;)V

    move-object v6, v0

    goto :goto_0

    .line 679
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/allshare/Item$LocalContentBuilder;->mMimetype:Ljava/lang/String;

    # invokes: Lcom/samsung/android/allshare/Item;->convertItemTypeFromMimeType(Ljava/lang/String;)Lcom/samsung/android/allshare/Item$MediaType;
    invoke-static {v0}, Lcom/samsung/android/allshare/Item;->access$100(Ljava/lang/String;)Lcom/samsung/android/allshare/Item$MediaType;

    move-result-object v7

    .line 680
    .local v7, "type":Lcom/samsung/android/allshare/Item$MediaType;
    sget-object v0, Lcom/samsung/android/allshare/Item$1;->$SwitchMap$com$samsung$android$allshare$Item$MediaType:[I

    invoke-virtual {v7}, Lcom/samsung/android/allshare/Item$MediaType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 686
    :pswitch_0
    new-instance v0, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;

    sget-object v1, Lcom/samsung/android/allshare/ItemCreator$ConstructorType;->LOCAL_CONTENT:Lcom/samsung/android/allshare/ItemCreator$ConstructorType;

    iget-object v2, p0, Lcom/samsung/android/allshare/Item$LocalContentBuilder;->mFilepath:Ljava/lang/String;

    iget-object v3, p0, Lcom/samsung/android/allshare/Item$LocalContentBuilder;->mTitle:Ljava/lang/String;

    iget-object v4, p0, Lcom/samsung/android/allshare/Item$LocalContentBuilder;->mSubtitlePath:Ljava/lang/String;

    iget-object v5, p0, Lcom/samsung/android/allshare/Item$LocalContentBuilder;->mMimetype:Ljava/lang/String;

    invoke-direct/range {v0 .. v6}, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;-><init>(Lcom/samsung/android/allshare/ItemCreator$ConstructorType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/Item$1;)V

    move-object v6, v0

    goto :goto_0

    .line 680
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public setSubtitle(Ljava/lang/String;)Lcom/samsung/android/allshare/Item$LocalContentBuilder;
    .locals 0
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    .line 650
    iput-object p1, p0, Lcom/samsung/android/allshare/Item$LocalContentBuilder;->mSubtitlePath:Ljava/lang/String;

    .line 651
    return-object p0
.end method

.method public setTitle(Ljava/lang/String;)Lcom/samsung/android/allshare/Item$LocalContentBuilder;
    .locals 0
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 634
    iput-object p1, p0, Lcom/samsung/android/allshare/Item$LocalContentBuilder;->mTitle:Ljava/lang/String;

    .line 635
    return-object p0
.end method
