.class Lcom/samsung/android/allshare/ItemCreator;
.super Ljava/lang/Object;
.source "ItemCreator.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/allshare/ItemCreator$1;,
        Lcom/samsung/android/allshare/ItemCreator$ConstructorType;
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    return-void
.end method

.method private static createMediaServerItem(Landroid/os/Bundle;)Lcom/samsung/android/allshare/Item;
    .locals 5
    .param p0, "itemBundle"    # Landroid/os/Bundle;

    .prologue
    .line 133
    const/4 v2, 0x0

    .line 134
    .local v2, "result":Lcom/samsung/android/allshare/Item;
    const-string v3, "BUNDLE_STRING_ITEM_TYPE"

    invoke-virtual {p0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 135
    .local v0, "itemType":Ljava/lang/String;
    invoke-static {v0}, Lcom/samsung/android/allshare/Item$MediaType;->stringToEnum(Ljava/lang/String;)Lcom/samsung/android/allshare/Item$MediaType;

    move-result-object v1

    .line 137
    .local v1, "mediaType":Lcom/samsung/android/allshare/Item$MediaType;
    sget-object v3, Lcom/samsung/android/allshare/ItemCreator$1;->$SwitchMap$com$samsung$android$allshare$Item$MediaType:[I

    invoke-virtual {v1}, Lcom/samsung/android/allshare/Item$MediaType;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 153
    const/4 v2, 0x0

    .line 155
    :goto_0
    :pswitch_0
    return-object v2

    .line 139
    :pswitch_1
    new-instance v2, Lcom/samsung/android/allshare/AudioItemImpl;

    .end local v2    # "result":Lcom/samsung/android/allshare/Item;
    invoke-direct {v2, p0}, Lcom/samsung/android/allshare/AudioItemImpl;-><init>(Landroid/os/Bundle;)V

    .line 140
    .restart local v2    # "result":Lcom/samsung/android/allshare/Item;
    goto :goto_0

    .line 142
    :pswitch_2
    new-instance v2, Lcom/samsung/android/allshare/ImageItemImpl;

    .end local v2    # "result":Lcom/samsung/android/allshare/Item;
    invoke-direct {v2, p0}, Lcom/samsung/android/allshare/ImageItemImpl;-><init>(Landroid/os/Bundle;)V

    .line 143
    .restart local v2    # "result":Lcom/samsung/android/allshare/Item;
    goto :goto_0

    .line 145
    :pswitch_3
    new-instance v2, Lcom/samsung/android/allshare/VideoItemImpl;

    .end local v2    # "result":Lcom/samsung/android/allshare/Item;
    invoke-direct {v2, p0}, Lcom/samsung/android/allshare/VideoItemImpl;-><init>(Landroid/os/Bundle;)V

    .line 146
    .restart local v2    # "result":Lcom/samsung/android/allshare/Item;
    goto :goto_0

    .line 148
    :pswitch_4
    new-instance v2, Lcom/samsung/android/allshare/FolderItemImpl;

    .end local v2    # "result":Lcom/samsung/android/allshare/Item;
    invoke-direct {v2, p0}, Lcom/samsung/android/allshare/FolderItemImpl;-><init>(Landroid/os/Bundle;)V

    .line 149
    .restart local v2    # "result":Lcom/samsung/android/allshare/Item;
    goto :goto_0

    .line 137
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
    .end packed-switch
.end method

.method static fromBundle(Landroid/os/Bundle;)Lcom/samsung/android/allshare/Item;
    .locals 14
    .param p0, "itemBundle"    # Landroid/os/Bundle;

    .prologue
    const/4 v11, 0x0

    .line 63
    const/4 v9, 0x0

    .line 65
    .local v9, "result":Lcom/samsung/android/allshare/Item;
    if-nez p0, :cond_1

    .line 129
    :cond_0
    :goto_0
    return-object v11

    .line 68
    :cond_1
    const-string v12, "BUNDLE_STRING_ITEM_CONSTRUCTOR_KEY"

    invoke-virtual {p0, v12}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 71
    .local v5, "itemConstructor":Ljava/lang/String;
    if-eqz v5, :cond_0

    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    move-result v12

    if-nez v12, :cond_0

    .line 74
    invoke-static {v5}, Lcom/samsung/android/allshare/ItemCreator$ConstructorType;->stringToEnum(Ljava/lang/String;)Lcom/samsung/android/allshare/ItemCreator$ConstructorType;

    move-result-object v1

    .line 76
    .local v1, "conType":Lcom/samsung/android/allshare/ItemCreator$ConstructorType;
    sget-object v11, Lcom/samsung/android/allshare/ItemCreator$1;->$SwitchMap$com$samsung$android$allshare$ItemCreator$ConstructorType:[I

    invoke-virtual {v1}, Lcom/samsung/android/allshare/ItemCreator$ConstructorType;->ordinal()I

    move-result v12

    aget v11, v11, v12

    packed-switch v11, :pswitch_data_0

    .line 126
    const/4 v9, 0x0

    :goto_1
    move-object v11, v9

    .line 129
    goto :goto_0

    .line 78
    :pswitch_0
    invoke-static {p0}, Lcom/samsung/android/allshare/ItemCreator;->createMediaServerItem(Landroid/os/Bundle;)Lcom/samsung/android/allshare/Item;

    move-result-object v9

    .line 79
    goto :goto_1

    .line 81
    :pswitch_1
    const-string v11, "BUNDLE_PARCELABLE_ITEM_URI"

    invoke-virtual {p0, v11}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v10

    check-cast v10, Landroid/net/Uri;

    .line 82
    .local v10, "uri":Landroid/net/Uri;
    const-string v11, "BUNDLE_STRING_ITEM_MIMETYPE"

    invoke-virtual {p0, v11}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 83
    .local v6, "mimeType":Ljava/lang/String;
    new-instance v0, Lcom/samsung/android/allshare/Item$WebContentBuilder;

    invoke-direct {v0, v10, v6}, Lcom/samsung/android/allshare/Item$WebContentBuilder;-><init>(Landroid/net/Uri;Ljava/lang/String;)V

    .line 84
    .local v0, "builder":Lcom/samsung/android/allshare/Item$WebContentBuilder;
    const-string v11, "BUNDLE_STRING_ITEM_TITLE"

    invoke-virtual {p0, v11}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v0, v11}, Lcom/samsung/android/allshare/Item$WebContentBuilder;->setTitle(Ljava/lang/String;)Lcom/samsung/android/allshare/Item$WebContentBuilder;

    move-result-object v0

    .line 86
    const-string v11, "BUNDLE_STRING_ITEM_SUBTITLE_PATH"

    invoke-virtual {p0, v11}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v0, v11}, Lcom/samsung/android/allshare/Item$WebContentBuilder;->setSubtitle(Ljava/lang/String;)Lcom/samsung/android/allshare/Item$WebContentBuilder;

    move-result-object v0

    .line 88
    const-string v11, "BUNDLE_STRING_ITEM_ALBUM_TITLE"

    invoke-virtual {p0, v11}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v0, v11}, Lcom/samsung/android/allshare/Item$WebContentBuilder;->setAlbumTitle(Ljava/lang/String;)Lcom/samsung/android/allshare/Item$WebContentBuilder;

    move-result-object v0

    .line 90
    const-string v11, "BUNDLE_STRING_ITEM_ARTIST"

    invoke-virtual {p0, v11}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v0, v11}, Lcom/samsung/android/allshare/Item$WebContentBuilder;->setArtist(Ljava/lang/String;)Lcom/samsung/android/allshare/Item$WebContentBuilder;

    move-result-object v0

    .line 92
    const-string v11, "BUNDLE_STRING_ITEM_GENRE"

    invoke-virtual {p0, v11}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v0, v11}, Lcom/samsung/android/allshare/Item$WebContentBuilder;->setGenre(Ljava/lang/String;)Lcom/samsung/android/allshare/Item$WebContentBuilder;

    move-result-object v0

    .line 94
    const-string v11, "BUNDLE_LONG_ITEM_DURATION"

    invoke-virtual {p0, v11}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v12

    invoke-virtual {v0, v12, v13}, Lcom/samsung/android/allshare/Item$WebContentBuilder;->setDuration(J)Lcom/samsung/android/allshare/Item$WebContentBuilder;

    move-result-object v0

    .line 96
    const-string v11, "BUNDLE_DATE_ITEM_DATE"

    invoke-virtual {p0, v11}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 98
    .local v2, "dateTime":J
    const-wide/16 v12, 0x0

    cmp-long v11, v2, v12

    if-lez v11, :cond_2

    .line 99
    new-instance v11, Ljava/util/Date;

    invoke-direct {v11, v2, v3}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v0, v11}, Lcom/samsung/android/allshare/Item$WebContentBuilder;->setDate(Ljava/util/Date;)Lcom/samsung/android/allshare/Item$WebContentBuilder;

    move-result-object v0

    .line 102
    :cond_2
    const-string v11, "BUNDLE_STRING_WEB_PLAY_MODE"

    invoke-virtual {p0, v11}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 103
    .local v8, "playModeStr":Ljava/lang/String;
    sget-object v4, Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;->UNKNOWN:Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;

    .line 104
    .local v4, "deliverymode":Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;
    if-eqz v8, :cond_3

    invoke-virtual {v8}, Ljava/lang/String;->isEmpty()Z

    move-result v11

    if-eqz v11, :cond_4

    .line 105
    :cond_3
    sget-object v4, Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;->UNKNOWN:Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;

    .line 110
    :goto_2
    invoke-virtual {v0, v4}, Lcom/samsung/android/allshare/Item$WebContentBuilder;->setDeliveryMode(Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;)Lcom/samsung/android/allshare/Item$WebContentBuilder;

    move-result-object v0

    .line 111
    invoke-virtual {v0}, Lcom/samsung/android/allshare/Item$WebContentBuilder;->build()Lcom/samsung/android/allshare/Item;

    move-result-object v9

    .line 112
    goto/16 :goto_1

    .line 107
    :cond_4
    invoke-static {v8}, Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;->stringToEnum(Ljava/lang/String;)Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;

    move-result-object v4

    goto :goto_2

    .line 115
    .end local v0    # "builder":Lcom/samsung/android/allshare/Item$WebContentBuilder;
    .end local v2    # "dateTime":J
    .end local v4    # "deliverymode":Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;
    .end local v6    # "mimeType":Ljava/lang/String;
    .end local v8    # "playModeStr":Ljava/lang/String;
    .end local v10    # "uri":Landroid/net/Uri;
    :pswitch_2
    const-string v11, "BUNDLE_STRING_FILEPATH"

    invoke-virtual {p0, v11}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 116
    .local v7, "path":Ljava/lang/String;
    const-string v11, "BUNDLE_STRING_ITEM_MIMETYPE"

    invoke-virtual {p0, v11}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 117
    .restart local v6    # "mimeType":Ljava/lang/String;
    new-instance v0, Lcom/samsung/android/allshare/Item$LocalContentBuilder;

    invoke-direct {v0, v7, v6}, Lcom/samsung/android/allshare/Item$LocalContentBuilder;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 118
    .local v0, "builder":Lcom/samsung/android/allshare/Item$LocalContentBuilder;
    const-string v11, "BUNDLE_STRING_ITEM_TITLE"

    invoke-virtual {p0, v11}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v0, v11}, Lcom/samsung/android/allshare/Item$LocalContentBuilder;->setTitle(Ljava/lang/String;)Lcom/samsung/android/allshare/Item$LocalContentBuilder;

    move-result-object v0

    .line 120
    const-string v11, "BUNDLE_STRING_ITEM_SUBTITLE_PATH"

    invoke-virtual {p0, v11}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v0, v11}, Lcom/samsung/android/allshare/Item$LocalContentBuilder;->setSubtitle(Ljava/lang/String;)Lcom/samsung/android/allshare/Item$LocalContentBuilder;

    move-result-object v0

    .line 122
    invoke-virtual {v0}, Lcom/samsung/android/allshare/Item$LocalContentBuilder;->build()Lcom/samsung/android/allshare/Item;

    move-result-object v9

    .line 123
    goto/16 :goto_1

    .line 76
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
