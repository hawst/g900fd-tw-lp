.class Lcom/samsung/android/allshare/FileReceiverImpl$5;
.super Ljava/lang/Object;
.source "FileReceiverImpl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/allshare/FileReceiverImpl;->receive(Ljava/util/ArrayList;Ljava/lang/String;Lcom/samsung/android/allshare/file/FileReceiver$IFileReceiverReceiveResponseListener;Lcom/samsung/android/allshare/file/FileReceiver$IFileReceiverProgressUpdateEventListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/allshare/FileReceiverImpl;

.field final synthetic val$filelist:Ljava/util/ArrayList;

.field final synthetic val$responseListener:Lcom/samsung/android/allshare/file/FileReceiver$IFileReceiverReceiveResponseListener;

.field final synthetic val$senderName:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/samsung/android/allshare/FileReceiverImpl;Lcom/samsung/android/allshare/file/FileReceiver$IFileReceiverReceiveResponseListener;Ljava/util/ArrayList;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 679
    iput-object p1, p0, Lcom/samsung/android/allshare/FileReceiverImpl$5;->this$0:Lcom/samsung/android/allshare/FileReceiverImpl;

    iput-object p2, p0, Lcom/samsung/android/allshare/FileReceiverImpl$5;->val$responseListener:Lcom/samsung/android/allshare/file/FileReceiver$IFileReceiverReceiveResponseListener;

    iput-object p3, p0, Lcom/samsung/android/allshare/FileReceiverImpl$5;->val$filelist:Ljava/util/ArrayList;

    iput-object p4, p0, Lcom/samsung/android/allshare/FileReceiverImpl$5;->val$senderName:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 682
    const-string v0, "FileReceiverImpl"

    const-string v1, "mRespHandler.postDelayed.mReceiveResponseListener.onReceiveResponseReceived() called.."

    invoke-static {v0, v1}, Lcom/samsung/android/allshare/DLog;->d_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 684
    iget-object v0, p0, Lcom/samsung/android/allshare/FileReceiverImpl$5;->val$responseListener:Lcom/samsung/android/allshare/file/FileReceiver$IFileReceiverReceiveResponseListener;

    iget-object v1, p0, Lcom/samsung/android/allshare/FileReceiverImpl$5;->this$0:Lcom/samsung/android/allshare/FileReceiverImpl;

    # getter for: Lcom/samsung/android/allshare/FileReceiverImpl;->mReceiver:Lcom/samsung/android/allshare/file/FileReceiver;
    invoke-static {v1}, Lcom/samsung/android/allshare/FileReceiverImpl;->access$100(Lcom/samsung/android/allshare/FileReceiverImpl;)Lcom/samsung/android/allshare/file/FileReceiver;

    move-result-object v1

    const-string v2, ""

    iget-object v3, p0, Lcom/samsung/android/allshare/FileReceiverImpl$5;->val$filelist:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/samsung/android/allshare/FileReceiverImpl$5;->val$senderName:Ljava/lang/String;

    sget-object v5, Lcom/samsung/android/allshare/ERROR;->FAIL:Lcom/samsung/android/allshare/ERROR;

    invoke-interface/range {v0 .. v5}, Lcom/samsung/android/allshare/file/FileReceiver$IFileReceiverReceiveResponseListener;->onReceiveResponseReceived(Lcom/samsung/android/allshare/file/FileReceiver;Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/String;Lcom/samsung/android/allshare/ERROR;)V

    .line 686
    return-void
.end method
