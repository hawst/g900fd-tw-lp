.class Lcom/samsung/android/allshare/TVMessageSender;
.super Ljava/lang/Thread;
.source "IAppControlAPI.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "TVMessageSender"


# instance fields
.field mBuf:Ljava/nio/ByteBuffer;

.field private mCIPORT:I

.field mControlAPI:Lcom/samsung/android/allshare/IAppControlAPI;

.field mDOutStream:Ljava/io/DataOutputStream;

.field public mDeviceClass:Ljava/lang/String;

.field mDeviceName:Ljava/lang/String;

.field public mExcutor:Ljava/util/concurrent/ExecutorService;

.field public mHandler:Landroid/os/Handler;

.field mLocalAddr:Ljava/lang/String;

.field mMacAddr:Ljava/lang/String;

.field mOutStream:Ljava/io/OutputStream;

.field mProtocol:I

.field private mRemoteAddr:Ljava/net/SocketAddress;

.field mServerIp:Ljava/lang/String;

.field public mSocket:Ljava/net/Socket;

.field mTargetDtvName:Ljava/lang/String;

.field mTargetName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/samsung/android/allshare/IAppControlAPI;)V
    .locals 2
    .param p1, "controlAPI"    # Lcom/samsung/android/allshare/IAppControlAPI;

    .prologue
    const/4 v1, 0x0

    .line 829
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 748
    const/16 v0, 0x7e4

    iput v0, p0, Lcom/samsung/android/allshare/TVMessageSender;->mCIPORT:I

    .line 750
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/allshare/TVMessageSender;->mProtocol:I

    .line 754
    iput-object v1, p0, Lcom/samsung/android/allshare/TVMessageSender;->mServerIp:Ljava/lang/String;

    .line 756
    iput-object v1, p0, Lcom/samsung/android/allshare/TVMessageSender;->mLocalAddr:Ljava/lang/String;

    .line 758
    iput-object v1, p0, Lcom/samsung/android/allshare/TVMessageSender;->mMacAddr:Ljava/lang/String;

    .line 760
    iput-object v1, p0, Lcom/samsung/android/allshare/TVMessageSender;->mDeviceName:Ljava/lang/String;

    .line 762
    iput-object v1, p0, Lcom/samsung/android/allshare/TVMessageSender;->mTargetName:Ljava/lang/String;

    .line 764
    iput-object v1, p0, Lcom/samsung/android/allshare/TVMessageSender;->mDeviceClass:Ljava/lang/String;

    .line 766
    iput-object v1, p0, Lcom/samsung/android/allshare/TVMessageSender;->mTargetDtvName:Ljava/lang/String;

    .line 770
    iput-object v1, p0, Lcom/samsung/android/allshare/TVMessageSender;->mOutStream:Ljava/io/OutputStream;

    .line 772
    iput-object v1, p0, Lcom/samsung/android/allshare/TVMessageSender;->mDOutStream:Ljava/io/DataOutputStream;

    .line 776
    invoke-static {}, Ljava/util/concurrent/Executors;->newCachedThreadPool()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/allshare/TVMessageSender;->mExcutor:Ljava/util/concurrent/ExecutorService;

    .line 778
    iput-object v1, p0, Lcom/samsung/android/allshare/TVMessageSender;->mControlAPI:Lcom/samsung/android/allshare/IAppControlAPI;

    .line 1323
    iput-object v1, p0, Lcom/samsung/android/allshare/TVMessageSender;->mHandler:Landroid/os/Handler;

    .line 832
    iput-object p1, p0, Lcom/samsung/android/allshare/TVMessageSender;->mControlAPI:Lcom/samsung/android/allshare/IAppControlAPI;

    .line 833
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/android/allshare/TVMessageSender;->mDeviceName:Ljava/lang/String;

    .line 834
    return-void
.end method

.method private createHandler()V
    .locals 1

    .prologue
    .line 1326
    new-instance v0, Lcom/samsung/android/allshare/TVMessageSender$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/allshare/TVMessageSender$1;-><init>(Lcom/samsung/android/allshare/TVMessageSender;)V

    iput-object v0, p0, Lcom/samsung/android/allshare/TVMessageSender;->mHandler:Landroid/os/Handler;

    .line 1460
    return-void
.end method


# virtual methods
.method public closeSender()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 849
    iget v1, p0, Lcom/samsung/android/allshare/TVMessageSender;->mProtocol:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 851
    iget-object v1, p0, Lcom/samsung/android/allshare/TVMessageSender;->mSocket:Ljava/net/Socket;

    if-eqz v1, :cond_1

    .line 852
    iget-object v1, p0, Lcom/samsung/android/allshare/TVMessageSender;->mSocket:Ljava/net/Socket;

    invoke-virtual {v1}, Ljava/net/Socket;->isConnected()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 854
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/allshare/TVMessageSender;->mSocket:Ljava/net/Socket;

    invoke-virtual {v1}, Ljava/net/Socket;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 860
    :cond_0
    :goto_0
    iput-object v3, p0, Lcom/samsung/android/allshare/TVMessageSender;->mSocket:Ljava/net/Socket;

    .line 863
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    .line 864
    iput-object v3, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    .line 865
    return-void

    .line 855
    :catch_0
    move-exception v0

    .line 857
    .local v0, "e":Ljava/io/IOException;
    const-string v1, "TVMessageSender"

    const-string v2, ""

    invoke-static {v1, v2, v0}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public initSender(Lcom/samsung/android/allshare/NetworkSocketInfo;)V
    .locals 6
    .param p1, "netinfo"    # Lcom/samsung/android/allshare/NetworkSocketInfo;

    .prologue
    const/4 v5, 0x1

    .line 781
    iget v2, p1, Lcom/samsung/android/allshare/NetworkSocketInfo;->mProtocol:I

    iput v2, p0, Lcom/samsung/android/allshare/TVMessageSender;->mProtocol:I

    .line 782
    iget-object v2, p1, Lcom/samsung/android/allshare/NetworkSocketInfo;->mIpAddress:Ljava/lang/String;

    iput-object v2, p0, Lcom/samsung/android/allshare/TVMessageSender;->mServerIp:Ljava/lang/String;

    .line 783
    iget v2, p1, Lcom/samsung/android/allshare/NetworkSocketInfo;->mPort:I

    iput v2, p0, Lcom/samsung/android/allshare/TVMessageSender;->mCIPORT:I

    .line 784
    iget-object v2, p1, Lcom/samsung/android/allshare/NetworkSocketInfo;->mDeviceClass:Ljava/lang/String;

    iput-object v2, p0, Lcom/samsung/android/allshare/TVMessageSender;->mDeviceClass:Ljava/lang/String;

    .line 785
    iget-object v2, p1, Lcom/samsung/android/allshare/NetworkSocketInfo;->mMacAddr:Ljava/lang/String;

    const/16 v3, 0x3a

    const/16 v4, 0x2d

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/allshare/TVMessageSender;->mMacAddr:Ljava/lang/String;

    .line 787
    iget v2, p0, Lcom/samsung/android/allshare/TVMessageSender;->mProtocol:I

    if-ne v2, v5, :cond_1

    .line 788
    iget-object v2, p0, Lcom/samsung/android/allshare/TVMessageSender;->mSocket:Ljava/net/Socket;

    if-eqz v2, :cond_0

    .line 790
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/allshare/TVMessageSender;->mSocket:Ljava/net/Socket;

    invoke-virtual {v2}, Ljava/net/Socket;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 794
    :goto_0
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/samsung/android/allshare/TVMessageSender;->mSocket:Ljava/net/Socket;

    .line 797
    :cond_0
    const/16 v2, 0x1000

    invoke-static {v2}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    .line 799
    new-instance v2, Ljava/net/Socket;

    invoke-direct {v2}, Ljava/net/Socket;-><init>()V

    iput-object v2, p0, Lcom/samsung/android/allshare/TVMessageSender;->mSocket:Ljava/net/Socket;

    .line 800
    new-instance v2, Ljava/net/InetSocketAddress;

    iget-object v3, p0, Lcom/samsung/android/allshare/TVMessageSender;->mServerIp:Ljava/lang/String;

    iget v4, p0, Lcom/samsung/android/allshare/TVMessageSender;->mCIPORT:I

    invoke-direct {v2, v3, v4}, Ljava/net/InetSocketAddress;-><init>(Ljava/lang/String;I)V

    iput-object v2, p0, Lcom/samsung/android/allshare/TVMessageSender;->mRemoteAddr:Ljava/net/SocketAddress;

    .line 803
    :try_start_1
    iget-object v2, p0, Lcom/samsung/android/allshare/TVMessageSender;->mSocket:Ljava/net/Socket;

    iget-object v3, p0, Lcom/samsung/android/allshare/TVMessageSender;->mRemoteAddr:Ljava/net/SocketAddress;

    const/16 v4, 0xbb8

    invoke-virtual {v2, v3, v4}, Ljava/net/Socket;->connect(Ljava/net/SocketAddress;I)V

    .line 804
    iget-object v2, p0, Lcom/samsung/android/allshare/TVMessageSender;->mSocket:Ljava/net/Socket;

    invoke-virtual {v2}, Ljava/net/Socket;->isConnected()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 805
    const-string v2, "TVMessageSender"

    const-string v3, "initSender : mSocket is connected."

    invoke-static {v2, v3}, Lcom/samsung/android/allshare/DLog;->i_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 807
    iget-object v2, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    sget-object v3, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 808
    iget-object v2, p0, Lcom/samsung/android/allshare/TVMessageSender;->mSocket:Ljava/net/Socket;

    invoke-virtual {v2}, Ljava/net/Socket;->getLocalAddress()Ljava/net/InetAddress;

    move-result-object v2

    invoke-virtual {v2}, Ljava/net/InetAddress;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/allshare/TVMessageSender;->mLocalAddr:Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 811
    :try_start_2
    iget-object v2, p0, Lcom/samsung/android/allshare/TVMessageSender;->mSocket:Ljava/net/Socket;

    invoke-virtual {v2}, Ljava/net/Socket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/allshare/TVMessageSender;->mOutStream:Ljava/io/OutputStream;

    .line 812
    new-instance v2, Ljava/io/DataOutputStream;

    iget-object v3, p0, Lcom/samsung/android/allshare/TVMessageSender;->mOutStream:Ljava/io/OutputStream;

    invoke-direct {v2, v3}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    iput-object v2, p0, Lcom/samsung/android/allshare/TVMessageSender;->mDOutStream:Ljava/io/DataOutputStream;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 827
    :cond_1
    :goto_1
    return-void

    .line 791
    :catch_0
    move-exception v0

    .line 792
    .local v0, "e":Ljava/io/IOException;
    const-string v2, "TVMessageSender"

    const-string v3, ""

    invoke-static {v2, v3, v0}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0

    .line 813
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v1

    .line 815
    .local v1, "e1":Ljava/io/IOException;
    :try_start_3
    const-string v2, "TVMessageSender"

    const-string v3, ""

    invoke-static {v2, v3, v1}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_1

    .line 821
    .end local v1    # "e1":Ljava/io/IOException;
    :catch_2
    move-exception v0

    .line 823
    .restart local v0    # "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 819
    .end local v0    # "e":Ljava/io/IOException;
    :cond_2
    :try_start_4
    const-string v2, "TVMessageSender"

    const-string v3, "initSender : mSocket connecting is failed."

    invoke-static {v2, v3}, Lcom/samsung/android/allshare/DLog;->i_api(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_1
.end method

.method public run()V
    .locals 0

    .prologue
    .line 838
    invoke-static {}, Landroid/os/Looper;->prepare()V

    .line 841
    invoke-direct {p0}, Lcom/samsung/android/allshare/TVMessageSender;->createHandler()V

    .line 843
    invoke-static {}, Landroid/os/Looper;->loop()V

    .line 845
    return-void
.end method

.method public sendAuthentication()V
    .locals 15

    .prologue
    const/4 v13, 0x0

    .line 898
    new-instance v10, Ljava/lang/StringBuffer;

    const/16 v12, 0x1000

    invoke-direct {v10, v12}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 899
    .local v10, "strBuffer":Ljava/lang/StringBuffer;
    const-string v12, "omnia.iapp.samsung"

    invoke-virtual {v10, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 900
    invoke-virtual {v10}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v12

    iput-object v12, p0, Lcom/samsung/android/allshare/TVMessageSender;->mTargetDtvName:Ljava/lang/String;

    .line 904
    iget-object v12, p0, Lcom/samsung/android/allshare/TVMessageSender;->mLocalAddr:Ljava/lang/String;

    invoke-virtual {v12}, Ljava/lang/String;->getBytes()[B

    move-result-object v12

    invoke-static {v12, v13}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v4

    .line 907
    .local v4, "ipAddr64":Ljava/lang/String;
    iget-object v12, p0, Lcom/samsung/android/allshare/TVMessageSender;->mMacAddr:Ljava/lang/String;

    invoke-virtual {v12}, Ljava/lang/String;->getBytes()[B

    move-result-object v12

    invoke-static {v12, v13}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v6

    .line 911
    .local v6, "macAddr64":Ljava/lang/String;
    iget-object v12, p0, Lcom/samsung/android/allshare/TVMessageSender;->mDeviceName:Ljava/lang/String;

    invoke-virtual {v12}, Ljava/lang/String;->getBytes()[B

    move-result-object v12

    invoke-static {v12, v13}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v2

    .line 917
    .local v2, "deviceName64":Ljava/lang/String;
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    .line 918
    .local v5, "ipLen":I
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v7

    .line 919
    .local v7, "macLen":I
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v8

    .line 920
    .local v8, "nameLen":I
    iget-object v12, p0, Lcom/samsung/android/allshare/TVMessageSender;->mTargetDtvName:Ljava/lang/String;

    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v11

    .line 922
    .local v11, "targetLen":I
    add-int v12, v5, v7

    add-int/2addr v12, v8

    add-int/lit8 v1, v12, 0x8

    .line 923
    .local v1, "dataLen":I
    add-int v12, v1, v11

    add-int/lit8 v9, v12, 0x5

    .line 925
    .local v9, "packetLen":I
    iget-object v12, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v12}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    .line 926
    iget-object v12, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v12, v13}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 927
    iget-object v12, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    int-to-short v13, v11

    invoke-virtual {v12, v13}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 928
    iget-object v12, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    iget-object v13, p0, Lcom/samsung/android/allshare/TVMessageSender;->mTargetDtvName:Ljava/lang/String;

    invoke-virtual {v13}, Ljava/lang/String;->getBytes()[B

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 929
    iget-object v12, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    int-to-short v13, v1

    invoke-virtual {v12, v13}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 931
    const/16 v0, 0x64

    .line 932
    .local v0, "ProtocolID":S
    iget-object v12, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v12, v0}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 934
    iget-object v12, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    int-to-short v13, v5

    invoke-virtual {v12, v13}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 935
    iget-object v12, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v4}, Ljava/lang/String;->getBytes()[B

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 936
    iget-object v12, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    int-to-short v13, v7

    invoke-virtual {v12, v13}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 937
    iget-object v12, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v6}, Ljava/lang/String;->getBytes()[B

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 938
    iget-object v12, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    int-to-short v13, v8

    invoke-virtual {v12, v13}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 939
    iget-object v12, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 940
    iget-object v12, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v12}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    .line 943
    :try_start_0
    iget-object v12, p0, Lcom/samsung/android/allshare/TVMessageSender;->mDOutStream:Ljava/io/DataOutputStream;

    iget-object v13, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v13}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v13

    const/4 v14, 0x0

    invoke-virtual {v12, v13, v14, v9}, Ljava/io/DataOutputStream;->write([BII)V

    .line 944
    iget-object v12, p0, Lcom/samsung/android/allshare/TVMessageSender;->mDOutStream:Ljava/io/DataOutputStream;

    invoke-virtual {v12}, Ljava/io/DataOutputStream;->flush()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 950
    :goto_0
    iget-object v12, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v12}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    .line 951
    return-void

    .line 945
    :catch_0
    move-exception v3

    .line 947
    .local v3, "e":Ljava/io/IOException;
    const-string v12, "TVMessageSender"

    const-string v13, ""

    invoke-static {v12, v13, v3}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public sendKeyboardEnd()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 868
    iget-object v5, p0, Lcom/samsung/android/allshare/TVMessageSender;->mTargetDtvName:Ljava/lang/String;

    if-nez v5, :cond_0

    .line 894
    :goto_0
    return-void

    .line 872
    :cond_0
    iget-object v5, p0, Lcom/samsung/android/allshare/TVMessageSender;->mTargetDtvName:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v4

    .line 873
    .local v4, "targetLen":I
    const/4 v1, 0x2

    .line 874
    .local v1, "dataLen":I
    add-int v5, v1, v4

    add-int/lit8 v3, v5, 0x5

    .line 875
    .local v3, "packetLen":I
    const-string v5, "TVMessageSender"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "sendDTVKeyboardEnd targetLen "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/android/allshare/DLog;->i_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 877
    iget-object v5, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    .line 878
    iget-object v5, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v5, v8}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 879
    iget-object v5, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    int-to-short v6, v4

    invoke-virtual {v5, v6}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 880
    iget-object v5, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    iget-object v6, p0, Lcom/samsung/android/allshare/TVMessageSender;->mTargetDtvName:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->getBytes()[B

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 881
    iget-object v5, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    int-to-short v6, v1

    invoke-virtual {v5, v6}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 883
    const/4 v0, 0x4

    .line 884
    .local v0, "ProtocolID":S
    iget-object v5, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v5, v0}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 887
    :try_start_0
    iget-object v5, p0, Lcom/samsung/android/allshare/TVMessageSender;->mDOutStream:Ljava/io/DataOutputStream;

    iget-object v6, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v6}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7, v3}, Ljava/io/DataOutputStream;->write([BII)V

    .line 888
    iget-object v5, p0, Lcom/samsung/android/allshare/TVMessageSender;->mDOutStream:Ljava/io/DataOutputStream;

    invoke-virtual {v5}, Ljava/io/DataOutputStream;->flush()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 893
    :goto_1
    iget-object v5, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    goto :goto_0

    .line 889
    :catch_0
    move-exception v2

    .line 891
    .local v2, "e":Ljava/io/IOException;
    const-string v5, "TVMessageSender"

    const-string v6, ""

    invoke-static {v5, v6, v2}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_1
.end method

.method public sendKeyboardString(ILjava/lang/String;)V
    .locals 12
    .param p1, "encoding"    # I
    .param p2, "keycode"    # Ljava/lang/String;

    .prologue
    const/4 v11, 0x0

    .line 1126
    iget-object v9, p0, Lcom/samsung/android/allshare/TVMessageSender;->mTargetDtvName:Ljava/lang/String;

    if-nez v9, :cond_1

    .line 1184
    :cond_0
    :goto_0
    return-void

    .line 1130
    :cond_1
    iget-object v9, p0, Lcom/samsung/android/allshare/TVMessageSender;->mSocket:Ljava/net/Socket;

    invoke-virtual {v9}, Ljava/net/Socket;->isConnected()Z

    move-result v9

    if-eqz v9, :cond_0

    .line 1133
    if-eqz p2, :cond_0

    .line 1136
    const/4 v4, 0x0

    .line 1138
    .local v4, "keyCode64":Ljava/lang/String;
    :try_start_0
    const-string v7, "UNICODE"

    .line 1139
    .local v7, "strEncoding":Ljava/lang/String;
    const/4 v9, 0x1

    if-ne p1, v9, :cond_3

    .line 1140
    const-string v7, "UTF-8"

    .line 1147
    :cond_2
    :goto_1
    invoke-virtual {p2, v7}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v9

    const/4 v10, 0x0

    invoke-static {v9, v10}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 1154
    if-eqz v4, :cond_0

    .line 1155
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    .line 1156
    .local v5, "keyLen":I
    iget-object v9, p0, Lcom/samsung/android/allshare/TVMessageSender;->mTargetDtvName:Ljava/lang/String;

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v8

    .line 1157
    .local v8, "targetLen":I
    add-int/lit8 v1, v5, 0x4

    .line 1158
    .local v1, "dataLen":I
    add-int v9, v1, v8

    add-int/lit8 v6, v9, 0x5

    .line 1160
    .local v6, "packetLen":I
    iget-object v9, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v9}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    .line 1161
    iget-object v9, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v9, v11}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 1162
    iget-object v9, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    int-to-short v10, v8

    invoke-virtual {v9, v10}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 1164
    iget-object v9, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    iget-object v10, p0, Lcom/samsung/android/allshare/TVMessageSender;->mTargetDtvName:Ljava/lang/String;

    invoke-virtual {v10}, Ljava/lang/String;->getBytes()[B

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 1165
    iget-object v9, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    int-to-short v10, v1

    invoke-virtual {v9, v10}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 1166
    const/4 v0, 0x1

    .line 1167
    .local v0, "ProtocolID":S
    iget-object v9, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v9, v0}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 1170
    iget-object v9, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    int-to-short v10, v5

    invoke-virtual {v9, v10}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 1171
    iget-object v9, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v4}, Ljava/lang/String;->getBytes()[B

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 1172
    iget-object v9, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v9}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    .line 1175
    :try_start_1
    iget-object v9, p0, Lcom/samsung/android/allshare/TVMessageSender;->mDOutStream:Ljava/io/DataOutputStream;

    iget-object v10, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v10}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v10

    const/4 v11, 0x0

    invoke-virtual {v9, v10, v11, v6}, Ljava/io/DataOutputStream;->write([BII)V

    .line 1176
    iget-object v9, p0, Lcom/samsung/android/allshare/TVMessageSender;->mDOutStream:Ljava/io/DataOutputStream;

    invoke-virtual {v9}, Ljava/io/DataOutputStream;->flush()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 1182
    :goto_2
    iget-object v9, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v9}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    goto :goto_0

    .line 1141
    .end local v0    # "ProtocolID":S
    .end local v1    # "dataLen":I
    .end local v5    # "keyLen":I
    .end local v6    # "packetLen":I
    .end local v8    # "targetLen":I
    :cond_3
    const/4 v9, 0x2

    if-ne p1, v9, :cond_2

    .line 1142
    :try_start_2
    const-string v7, "UTF-16"
    :try_end_2
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_1

    .line 1148
    .end local v7    # "strEncoding":Ljava/lang/String;
    :catch_0
    move-exception v3

    .line 1150
    .local v3, "e1":Ljava/io/UnsupportedEncodingException;
    const-string v9, "TVMessageSender"

    const-string v10, ""

    invoke-static {v9, v10, v3}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto/16 :goto_0

    .line 1177
    .end local v3    # "e1":Ljava/io/UnsupportedEncodingException;
    .restart local v0    # "ProtocolID":S
    .restart local v1    # "dataLen":I
    .restart local v5    # "keyLen":I
    .restart local v6    # "packetLen":I
    .restart local v7    # "strEncoding":Ljava/lang/String;
    .restart local v8    # "targetLen":I
    :catch_1
    move-exception v2

    .line 1179
    .local v2, "e":Ljava/io/IOException;
    const-string v9, "TVMessageSender"

    const-string v10, ""

    invoke-static {v9, v10, v2}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_2
.end method

.method public sendMouseCreate()V
    .locals 8

    .prologue
    const/4 v6, 0x0

    .line 1230
    iget-object v5, p0, Lcom/samsung/android/allshare/TVMessageSender;->mTargetDtvName:Ljava/lang/String;

    if-nez v5, :cond_0

    .line 1256
    :goto_0
    return-void

    .line 1234
    :cond_0
    iget-object v5, p0, Lcom/samsung/android/allshare/TVMessageSender;->mTargetDtvName:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v4

    .line 1235
    .local v4, "targetLen":I
    const/4 v1, 0x2

    .line 1236
    .local v1, "dataLen":I
    add-int v5, v1, v4

    add-int/lit8 v3, v5, 0x5

    .line 1239
    .local v3, "packetLen":I
    iget-object v5, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    .line 1240
    iget-object v5, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v5, v6}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 1241
    iget-object v5, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    int-to-short v6, v4

    invoke-virtual {v5, v6}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 1242
    iget-object v5, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    iget-object v6, p0, Lcom/samsung/android/allshare/TVMessageSender;->mTargetDtvName:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->getBytes()[B

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 1243
    iget-object v5, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    int-to-short v6, v1

    invoke-virtual {v5, v6}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 1245
    const/16 v0, 0xf

    .line 1246
    .local v0, "ProtocolID":S
    iget-object v5, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v5, v0}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 1249
    :try_start_0
    iget-object v5, p0, Lcom/samsung/android/allshare/TVMessageSender;->mDOutStream:Ljava/io/DataOutputStream;

    iget-object v6, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v6}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7, v3}, Ljava/io/DataOutputStream;->write([BII)V

    .line 1250
    iget-object v5, p0, Lcom/samsung/android/allshare/TVMessageSender;->mDOutStream:Ljava/io/DataOutputStream;

    invoke-virtual {v5}, Ljava/io/DataOutputStream;->flush()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1255
    :goto_1
    iget-object v5, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    goto :goto_0

    .line 1251
    :catch_0
    move-exception v2

    .line 1253
    .local v2, "e":Ljava/io/IOException;
    const-string v5, "TVMessageSender"

    const-string v6, ""

    invoke-static {v5, v6, v2}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_1
.end method

.method public sendMouseDestroy()V
    .locals 8

    .prologue
    const/4 v6, 0x0

    .line 1259
    iget-object v5, p0, Lcom/samsung/android/allshare/TVMessageSender;->mTargetDtvName:Ljava/lang/String;

    if-nez v5, :cond_0

    .line 1284
    :goto_0
    return-void

    .line 1263
    :cond_0
    iget-object v5, p0, Lcom/samsung/android/allshare/TVMessageSender;->mTargetDtvName:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v4

    .line 1264
    .local v4, "targetLen":I
    const/4 v1, 0x2

    .line 1265
    .local v1, "dataLen":I
    add-int v5, v1, v4

    add-int/lit8 v3, v5, 0x5

    .line 1268
    .local v3, "packetLen":I
    iget-object v5, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    .line 1269
    iget-object v5, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v5, v6}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 1270
    iget-object v5, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    int-to-short v6, v4

    invoke-virtual {v5, v6}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 1271
    iget-object v5, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    iget-object v6, p0, Lcom/samsung/android/allshare/TVMessageSender;->mTargetDtvName:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->getBytes()[B

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 1272
    iget-object v5, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    int-to-short v6, v1

    invoke-virtual {v5, v6}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 1274
    const/16 v0, 0x10

    .line 1275
    .local v0, "ProtocolID":S
    iget-object v5, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v5, v0}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 1278
    :try_start_0
    iget-object v5, p0, Lcom/samsung/android/allshare/TVMessageSender;->mDOutStream:Ljava/io/DataOutputStream;

    iget-object v6, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v6}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7, v3}, Ljava/io/DataOutputStream;->write([BII)V

    .line 1279
    iget-object v5, p0, Lcom/samsung/android/allshare/TVMessageSender;->mDOutStream:Ljava/io/DataOutputStream;

    invoke-virtual {v5}, Ljava/io/DataOutputStream;->flush()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1283
    :goto_1
    iget-object v5, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    goto :goto_0

    .line 1280
    :catch_0
    move-exception v2

    .line 1281
    .local v2, "e":Ljava/io/IOException;
    const-string v5, "TVMessageSender"

    const-string v6, ""

    invoke-static {v5, v6, v2}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_1
.end method

.method public sendMouseProcess(IIIIII)V
    .locals 8
    .param p1, "eventType"    # I
    .param p2, "x"    # I
    .param p3, "y"    # I
    .param p4, "dx"    # I
    .param p5, "dy"    # I
    .param p6, "Button"    # I

    .prologue
    const/4 v6, 0x0

    .line 1287
    iget-object v5, p0, Lcom/samsung/android/allshare/TVMessageSender;->mTargetDtvName:Ljava/lang/String;

    if-nez v5, :cond_0

    .line 1321
    :goto_0
    return-void

    .line 1292
    :cond_0
    iget-object v5, p0, Lcom/samsung/android/allshare/TVMessageSender;->mTargetDtvName:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v4

    .line 1293
    .local v4, "targetLen":I
    const/16 v1, 0x1a

    .line 1294
    .local v1, "dataLen":I
    add-int v5, v1, v4

    add-int/lit8 v3, v5, 0x5

    .line 1296
    .local v3, "packetLen":I
    iget-object v5, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    .line 1297
    iget-object v5, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v5, v6}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 1298
    iget-object v5, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    int-to-short v6, v4

    invoke-virtual {v5, v6}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 1299
    iget-object v5, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    iget-object v6, p0, Lcom/samsung/android/allshare/TVMessageSender;->mTargetDtvName:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->getBytes()[B

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 1300
    iget-object v5, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    int-to-short v6, v1

    invoke-virtual {v5, v6}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 1302
    const/16 v0, 0x11

    .line 1303
    .local v0, "ProtocolID":S
    iget-object v5, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v5, v0}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 1305
    iget-object v5, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v5, p1}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 1306
    iget-object v5, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v5, p2}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 1307
    iget-object v5, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v5, p3}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 1308
    iget-object v5, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v5, p4}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 1309
    iget-object v5, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v5, p5}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 1310
    iget-object v5, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v5, p6}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 1311
    iget-object v5, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    .line 1314
    :try_start_0
    iget-object v5, p0, Lcom/samsung/android/allshare/TVMessageSender;->mDOutStream:Ljava/io/DataOutputStream;

    iget-object v6, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v6}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7, v3}, Ljava/io/DataOutputStream;->write([BII)V

    .line 1315
    iget-object v5, p0, Lcom/samsung/android/allshare/TVMessageSender;->mDOutStream:Ljava/io/DataOutputStream;

    invoke-virtual {v5}, Ljava/io/DataOutputStream;->flush()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1319
    :goto_1
    iget-object v5, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    goto :goto_0

    .line 1316
    :catch_0
    move-exception v2

    .line 1317
    .local v2, "e":Ljava/io/IOException;
    const-string v5, "TVMessageSender"

    const-string v6, ""

    invoke-static {v5, v6, v2}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_1
.end method

.method public sendRemoteControlKey(Ljava/lang/String;I)V
    .locals 10
    .param p1, "keycode"    # Ljava/lang/String;
    .param p2, "mode"    # I

    .prologue
    const/4 v8, 0x0

    .line 1082
    iget-object v7, p0, Lcom/samsung/android/allshare/TVMessageSender;->mTargetDtvName:Ljava/lang/String;

    if-nez v7, :cond_1

    .line 1123
    :cond_0
    :goto_0
    return-void

    .line 1086
    :cond_1
    iget-object v7, p0, Lcom/samsung/android/allshare/TVMessageSender;->mSocket:Ljava/net/Socket;

    invoke-virtual {v7}, Ljava/net/Socket;->isConnected()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 1089
    if-eqz p1, :cond_0

    .line 1093
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v7

    invoke-static {v7, v8}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v3

    .line 1095
    .local v3, "keyCode64":Ljava/lang/String;
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    .line 1096
    .local v4, "keyLen":I
    iget-object v7, p0, Lcom/samsung/android/allshare/TVMessageSender;->mTargetDtvName:Ljava/lang/String;

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v6

    .line 1097
    .local v6, "targetLen":I
    add-int/lit8 v1, v4, 0x5

    .line 1098
    .local v1, "dataLen":I
    add-int v7, v1, v6

    add-int/lit8 v5, v7, 0x5

    .line 1100
    .local v5, "packetLen":I
    iget-object v7, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v7}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    .line 1101
    iget-object v7, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v7, v8}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 1102
    iget-object v7, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    int-to-short v8, v6

    invoke-virtual {v7, v8}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 1103
    iget-object v7, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    iget-object v8, p0, Lcom/samsung/android/allshare/TVMessageSender;->mTargetDtvName:Ljava/lang/String;

    invoke-virtual {v8}, Ljava/lang/String;->getBytes()[B

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 1105
    iget-object v7, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    int-to-short v8, v1

    invoke-virtual {v7, v8}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 1106
    const/4 v0, 0x0

    .line 1107
    .local v0, "ProtocolID":S
    iget-object v7, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v7, v0}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 1109
    iget-object v7, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    int-to-byte v8, p2

    invoke-virtual {v7, v8}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 1110
    iget-object v7, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    int-to-short v8, v4

    invoke-virtual {v7, v8}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 1111
    iget-object v7, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/lang/String;->getBytes()[B

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 1112
    iget-object v7, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v7}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    .line 1115
    :try_start_0
    iget-object v7, p0, Lcom/samsung/android/allshare/TVMessageSender;->mDOutStream:Ljava/io/DataOutputStream;

    iget-object v8, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v8}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v9, v5}, Ljava/io/DataOutputStream;->write([BII)V

    .line 1116
    iget-object v7, p0, Lcom/samsung/android/allshare/TVMessageSender;->mDOutStream:Ljava/io/DataOutputStream;

    invoke-virtual {v7}, Ljava/io/DataOutputStream;->flush()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1122
    :goto_1
    iget-object v7, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v7}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    goto :goto_0

    .line 1117
    :catch_0
    move-exception v2

    .line 1119
    .local v2, "e":Ljava/io/IOException;
    const-string v7, "TVMessageSender"

    const-string v8, ""

    invoke-static {v7, v8, v2}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_1
.end method

.method public sendTouchGestureEvent(SSBB)V
    .locals 8
    .param p1, "x"    # S
    .param p2, "y"    # S
    .param p3, "dx"    # B
    .param p4, "dy"    # B

    .prologue
    const/4 v7, 0x0

    .line 1040
    iget-object v5, p0, Lcom/samsung/android/allshare/TVMessageSender;->mTargetDtvName:Ljava/lang/String;

    if-nez v5, :cond_0

    .line 1079
    :goto_0
    return-void

    .line 1044
    :cond_0
    iget-object v5, p0, Lcom/samsung/android/allshare/TVMessageSender;->mTargetDtvName:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v4

    .line 1045
    .local v4, "targetLen":I
    const/16 v1, 0xe

    .line 1046
    .local v1, "dataLen":I
    add-int v5, v1, v4

    add-int/lit8 v3, v5, 0x5

    .line 1052
    .local v3, "packetLen":I
    iget-object v5, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    .line 1053
    iget-object v5, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v5, v7}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 1054
    iget-object v5, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    int-to-short v6, v4

    invoke-virtual {v5, v6}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 1055
    iget-object v5, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    iget-object v6, p0, Lcom/samsung/android/allshare/TVMessageSender;->mTargetDtvName:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->getBytes()[B

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 1056
    iget-object v5, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    int-to-short v6, v1

    invoke-virtual {v5, v6}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 1059
    const/4 v0, 0x5

    .line 1060
    .local v0, "ProtocolID":S
    iget-object v5, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v5, v0}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 1062
    iget-object v5, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    const/16 v6, -0xc

    invoke-virtual {v5, v6}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 1063
    iget-object v5, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v5, p1}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 1064
    iget-object v5, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v5, p2}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 1065
    iget-object v5, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v5, p4}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 1066
    iget-object v5, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    neg-int v6, p3

    int-to-byte v6, v6

    invoke-virtual {v5, v6}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 1067
    iget-object v5, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v5, v7}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 1068
    iget-object v5, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    .line 1071
    :try_start_0
    iget-object v5, p0, Lcom/samsung/android/allshare/TVMessageSender;->mDOutStream:Ljava/io/DataOutputStream;

    iget-object v6, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v6}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7, v3}, Ljava/io/DataOutputStream;->write([BII)V

    .line 1072
    iget-object v5, p0, Lcom/samsung/android/allshare/TVMessageSender;->mDOutStream:Ljava/io/DataOutputStream;

    invoke-virtual {v5}, Ljava/io/DataOutputStream;->flush()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1077
    :goto_1
    iget-object v5, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    goto :goto_0

    .line 1073
    :catch_0
    move-exception v2

    .line 1075
    .local v2, "e":Ljava/io/IOException;
    const-string v5, "TVMessageSender"

    const-string v6, ""

    invoke-static {v5, v6, v2}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_1
.end method

.method public sendTouchGestureSemanticEvent(IIIII)V
    .locals 8
    .param p1, "eventType"    # I
    .param p2, "distance"    # I
    .param p3, "degree"    # I
    .param p4, "x"    # I
    .param p5, "y"    # I

    .prologue
    const/4 v6, 0x0

    .line 956
    iget-object v5, p0, Lcom/samsung/android/allshare/TVMessageSender;->mTargetDtvName:Ljava/lang/String;

    if-nez v5, :cond_0

    .line 991
    :goto_0
    return-void

    .line 960
    :cond_0
    iget-object v5, p0, Lcom/samsung/android/allshare/TVMessageSender;->mTargetDtvName:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v4

    .line 961
    .local v4, "targetLen":I
    const/16 v1, 0x16

    .line 962
    .local v1, "dataLen":I
    add-int v5, v1, v4

    add-int/lit8 v3, v5, 0x5

    .line 964
    .local v3, "packetLen":I
    iget-object v5, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    .line 965
    iget-object v5, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v5, v6}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 966
    iget-object v5, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    int-to-short v6, v4

    invoke-virtual {v5, v6}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 967
    iget-object v5, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    iget-object v6, p0, Lcom/samsung/android/allshare/TVMessageSender;->mTargetDtvName:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->getBytes()[B

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 968
    iget-object v5, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    int-to-short v6, v1

    invoke-virtual {v5, v6}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 970
    const/16 v0, 0x8

    .line 971
    .local v0, "ProtocolID":S
    iget-object v5, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v5, v0}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 974
    iget-object v5, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v5, p1}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 975
    iget-object v5, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v5, p2}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 976
    iget-object v5, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v5, p3}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 977
    iget-object v5, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v5, p4}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 978
    iget-object v5, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v5, p5}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 980
    iget-object v5, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    .line 983
    :try_start_0
    iget-object v5, p0, Lcom/samsung/android/allshare/TVMessageSender;->mDOutStream:Ljava/io/DataOutputStream;

    iget-object v6, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v6}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7, v3}, Ljava/io/DataOutputStream;->write([BII)V

    .line 984
    iget-object v5, p0, Lcom/samsung/android/allshare/TVMessageSender;->mDOutStream:Ljava/io/DataOutputStream;

    invoke-virtual {v5}, Ljava/io/DataOutputStream;->flush()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 989
    :goto_1
    iget-object v5, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    goto :goto_0

    .line 985
    :catch_0
    move-exception v2

    .line 987
    .local v2, "e":Ljava/io/IOException;
    const-string v5, "TVMessageSender"

    const-string v6, ""

    invoke-static {v5, v6, v2}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_1
.end method

.method public sendTouchGuestureEvent2012(IIIIIII)V
    .locals 8
    .param p1, "eventType"    # I
    .param p2, "accellevel"    # I
    .param p3, "fingerid"    # I
    .param p4, "x"    # I
    .param p5, "y"    # I
    .param p6, "dx"    # I
    .param p7, "dy"    # I

    .prologue
    const/4 v6, 0x0

    .line 995
    iget-object v5, p0, Lcom/samsung/android/allshare/TVMessageSender;->mTargetDtvName:Ljava/lang/String;

    if-nez v5, :cond_0

    .line 1034
    :goto_0
    return-void

    .line 999
    :cond_0
    iget-object v5, p0, Lcom/samsung/android/allshare/TVMessageSender;->mTargetDtvName:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v4

    .line 1000
    .local v4, "targetLen":I
    const/16 v1, 0x1e

    .line 1001
    .local v1, "dataLen":I
    add-int v5, v1, v4

    add-int/lit8 v3, v5, 0x5

    .line 1003
    .local v3, "packetLen":I
    iget-object v5, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    .line 1004
    iget-object v5, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v5, v6}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 1005
    iget-object v5, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    int-to-short v6, v4

    invoke-virtual {v5, v6}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 1006
    iget-object v5, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    iget-object v6, p0, Lcom/samsung/android/allshare/TVMessageSender;->mTargetDtvName:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->getBytes()[B

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 1007
    iget-object v5, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    int-to-short v6, v1

    invoke-virtual {v5, v6}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 1009
    const/4 v0, 0x7

    .line 1010
    .local v0, "ProtocolID":S
    iget-object v5, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v5, v0}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 1013
    iget-object v5, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v5, p1}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 1014
    iget-object v5, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v5, p4}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 1015
    iget-object v5, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v5, p5}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 1016
    iget-object v5, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v5, p6}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 1017
    iget-object v5, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v5, p7}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 1018
    iget-object v5, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v5, p2}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 1019
    iget-object v5, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v5, p3}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 1023
    iget-object v5, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    .line 1026
    :try_start_0
    iget-object v5, p0, Lcom/samsung/android/allshare/TVMessageSender;->mDOutStream:Ljava/io/DataOutputStream;

    iget-object v6, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v6}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7, v3}, Ljava/io/DataOutputStream;->write([BII)V

    .line 1027
    iget-object v5, p0, Lcom/samsung/android/allshare/TVMessageSender;->mDOutStream:Ljava/io/DataOutputStream;

    invoke-virtual {v5}, Ljava/io/DataOutputStream;->flush()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1032
    :goto_1
    iget-object v5, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    goto :goto_0

    .line 1028
    :catch_0
    move-exception v2

    .line 1030
    .local v2, "e":Ljava/io/IOException;
    const-string v5, "TVMessageSender"

    const-string v6, ""

    invoke-static {v5, v6, v2}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_1
.end method

.method public setTouchGestureTouchMode(I)V
    .locals 8
    .param p1, "mode"    # I

    .prologue
    const/4 v6, 0x0

    .line 1191
    iget-object v5, p0, Lcom/samsung/android/allshare/TVMessageSender;->mTargetDtvName:Ljava/lang/String;

    if-nez v5, :cond_0

    .line 1227
    :goto_0
    return-void

    .line 1196
    :cond_0
    iget-object v5, p0, Lcom/samsung/android/allshare/TVMessageSender;->mTargetDtvName:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v4

    .line 1197
    .local v4, "targetLen":I
    const/4 v1, 0x6

    .line 1198
    .local v1, "dataLen":I
    add-int v5, v1, v4

    add-int/lit8 v3, v5, 0x5

    .line 1204
    .local v3, "packetLen":I
    iget-object v5, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    .line 1205
    iget-object v5, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v5, v6}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 1206
    iget-object v5, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    int-to-short v6, v4

    invoke-virtual {v5, v6}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 1207
    iget-object v5, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    iget-object v6, p0, Lcom/samsung/android/allshare/TVMessageSender;->mTargetDtvName:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->getBytes()[B

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 1208
    iget-object v5, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    int-to-short v6, v1

    invoke-virtual {v5, v6}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 1211
    const/16 v0, 0xb

    .line 1212
    .local v0, "ProtocolID":S
    iget-object v5, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v5, v0}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 1215
    iget-object v5, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v5, p1}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 1216
    iget-object v5, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    .line 1219
    :try_start_0
    iget-object v5, p0, Lcom/samsung/android/allshare/TVMessageSender;->mDOutStream:Ljava/io/DataOutputStream;

    iget-object v6, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v6}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7, v3}, Ljava/io/DataOutputStream;->write([BII)V

    .line 1220
    iget-object v5, p0, Lcom/samsung/android/allshare/TVMessageSender;->mDOutStream:Ljava/io/DataOutputStream;

    invoke-virtual {v5}, Ljava/io/DataOutputStream;->flush()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1225
    :goto_1
    iget-object v5, p0, Lcom/samsung/android/allshare/TVMessageSender;->mBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    goto :goto_0

    .line 1221
    :catch_0
    move-exception v2

    .line 1223
    .local v2, "e":Ljava/io/IOException;
    const-string v5, "TVMessageSender"

    const-string v6, ""

    invoke-static {v5, v6, v2}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_1
.end method
