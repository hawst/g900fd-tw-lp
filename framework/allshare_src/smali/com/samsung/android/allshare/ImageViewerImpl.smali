.class final Lcom/samsung/android/allshare/ImageViewerImpl;
.super Lcom/samsung/android/allshare/media/ImageViewer;
.source "ImageViewerImpl.java"

# interfaces
.implements Lcom/sec/android/allshare/iface/IBundleHolder;
.implements Lcom/sec/android/allshare/iface/IHandlerHolder;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/allshare/ImageViewerImpl$3;
    }
.end annotation


# static fields
.field private static final TAG_CLASS:Ljava/lang/String; = "ImageViewerImpl"


# instance fields
.field private mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

.field private mContentChangedNotified:Z

.field private mCurrentDMRUri:Ljava/lang/String;

.field private mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

.field mEventHandler:Lcom/samsung/android/allshare/AllShareEventHandler;

.field private mEventListener:Lcom/samsung/android/allshare/media/ImageViewer$IImageViewerEventListener;

.field private mIsSubscribed:Z

.field private mPlayingContentUris:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private mPlaylistPlayer:Lcom/samsung/android/allshare/media/PlaylistPlayer;

.field mResponseHandler:Lcom/samsung/android/allshare/AllShareResponseHandler;

.field private mResponseListener:Lcom/samsung/android/allshare/media/ImageViewer$IImageViewerResponseListener;

.field private mSlideShowPlayer:Lcom/samsung/android/allshare/media/SlideShowPlayer;

.field private mViewController:Lcom/samsung/android/allshare/media/ViewController;

.field private mViewController2:Lcom/samsung/android/allshare/media/ViewController2;


# direct methods
.method constructor <init>(Lcom/samsung/android/allshare/IAllShareConnector;Lcom/samsung/android/allshare/DeviceImpl;)V
    .locals 5
    .param p1, "connector"    # Lcom/samsung/android/allshare/IAllShareConnector;
    .param p2, "deviceImpl"    # Lcom/samsung/android/allshare/DeviceImpl;

    .prologue
    const/4 v4, 0x0

    .line 76
    invoke-direct {p0}, Lcom/samsung/android/allshare/media/ImageViewer;-><init>()V

    .line 52
    iput-object v4, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    .line 54
    iput-object v4, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    .line 56
    iput-object v4, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mResponseListener:Lcom/samsung/android/allshare/media/ImageViewer$IImageViewerResponseListener;

    .line 58
    iput-object v4, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mEventListener:Lcom/samsung/android/allshare/media/ImageViewer$IImageViewerEventListener;

    .line 62
    iput-object v4, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mViewController:Lcom/samsung/android/allshare/media/ViewController;

    .line 64
    iput-object v4, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mViewController2:Lcom/samsung/android/allshare/media/ViewController2;

    .line 66
    iput-object v4, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mSlideShowPlayer:Lcom/samsung/android/allshare/media/SlideShowPlayer;

    .line 68
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mIsSubscribed:Z

    .line 70
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mContentChangedNotified:Z

    .line 72
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mPlayingContentUris:Ljava/util/ArrayList;

    .line 74
    iput-object v4, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mCurrentDMRUri:Ljava/lang/String;

    .line 107
    new-instance v2, Lcom/samsung/android/allshare/ImageViewerImpl$1;

    invoke-static {}, Lcom/samsung/android/allshare/ServiceConnector;->getMainLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v2, p0, v3}, Lcom/samsung/android/allshare/ImageViewerImpl$1;-><init>(Lcom/samsung/android/allshare/ImageViewerImpl;Landroid/os/Looper;)V

    iput-object v2, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mEventHandler:Lcom/samsung/android/allshare/AllShareEventHandler;

    .line 221
    new-instance v2, Lcom/samsung/android/allshare/ImageViewerImpl$2;

    invoke-static {}, Lcom/samsung/android/allshare/ServiceConnector;->getMainLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v2, p0, v3}, Lcom/samsung/android/allshare/ImageViewerImpl$2;-><init>(Lcom/samsung/android/allshare/ImageViewerImpl;Landroid/os/Looper;)V

    iput-object v2, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mResponseHandler:Lcom/samsung/android/allshare/AllShareResponseHandler;

    .line 78
    if-nez p1, :cond_0

    .line 80
    const-string v2, "ImageViewerImpl"

    const-string v3, "Connection FAIL: AllShare Service Connector does not exist"

    invoke-static {v2, v3}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    :goto_0
    return-void

    .line 84
    :cond_0
    if-nez p2, :cond_1

    .line 85
    const-string v2, "ImageViewerImpl"

    const-string v3, "deviceImpl is null"

    invoke-static {v2, v3}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 89
    :cond_1
    iput-object p2, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    .line 90
    iput-object p1, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    .line 93
    invoke-virtual {p2}, Lcom/samsung/android/allshare/DeviceImpl;->getBundle()Landroid/os/Bundle;

    move-result-object v0

    .line 94
    .local v0, "bundle":Landroid/os/Bundle;
    if-nez v0, :cond_2

    .line 95
    const-string v2, "ImageViewerImpl"

    const-string v3, "bundle is null"

    invoke-static {v2, v3}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 99
    :cond_2
    const-string v2, "BUNDLE_BOOLEAN_SUPPORT_PLAYLIST_PLAYER"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 101
    .local v1, "isSupportPlaylist":Ljava/lang/Boolean;
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 102
    new-instance v2, Lcom/samsung/android/allshare/PlaylistPlayerImpl;

    invoke-direct {v2, p1, p2}, Lcom/samsung/android/allshare/PlaylistPlayerImpl;-><init>(Lcom/samsung/android/allshare/IAllShareConnector;Lcom/samsung/android/allshare/DeviceImpl;)V

    iput-object v2, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mPlaylistPlayer:Lcom/samsung/android/allshare/media/PlaylistPlayer;

    goto :goto_0

    .line 104
    :cond_3
    iput-object v4, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mPlaylistPlayer:Lcom/samsung/android/allshare/media/PlaylistPlayer;

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/samsung/android/allshare/ImageViewerImpl;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/ImageViewerImpl;

    .prologue
    .line 49
    iget-boolean v0, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mContentChangedNotified:Z

    return v0
.end method

.method static synthetic access$002(Lcom/samsung/android/allshare/ImageViewerImpl;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/ImageViewerImpl;
    .param p1, "x1"    # Z

    .prologue
    .line 49
    iput-boolean p1, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mContentChangedNotified:Z

    return p1
.end method

.method static synthetic access$100(Lcom/samsung/android/allshare/ImageViewerImpl;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/ImageViewerImpl;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mCurrentDMRUri:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$102(Lcom/samsung/android/allshare/ImageViewerImpl;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/ImageViewerImpl;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 49
    iput-object p1, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mCurrentDMRUri:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$200(Lcom/samsung/android/allshare/ImageViewerImpl;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/ImageViewerImpl;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mPlayingContentUris:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/android/allshare/ImageViewerImpl;)Lcom/samsung/android/allshare/media/ImageViewer$IImageViewerEventListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/ImageViewerImpl;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mEventListener:Lcom/samsung/android/allshare/media/ImageViewer$IImageViewerEventListener;

    return-object v0
.end method

.method static synthetic access$400(Lcom/samsung/android/allshare/ImageViewerImpl;Landroid/net/Uri;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/ImageViewerImpl;
    .param p1, "x1"    # Landroid/net/Uri;

    .prologue
    .line 49
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/ImageViewerImpl;->parseUriFilePath(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$500(Lcom/samsung/android/allshare/ImageViewerImpl;)Lcom/samsung/android/allshare/media/ImageViewer$IImageViewerResponseListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/ImageViewerImpl;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mResponseListener:Lcom/samsung/android/allshare/media/ImageViewer$IImageViewerResponseListener;

    return-object v0
.end method

.method private parseUriFilePath(Landroid/net/Uri;)Ljava/lang/String;
    .locals 8
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v2, 0x0

    .line 491
    if-nez p1, :cond_1

    .line 505
    :cond_0
    :goto_0
    return-object v2

    .line 493
    :cond_1
    invoke-static {}, Lcom/samsung/android/allshare/ServiceConnector;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 494
    .local v0, "cr":Landroid/content/ContentResolver;
    if-eqz v0, :cond_0

    move-object v1, p1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    .line 497
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 498
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_0

    .line 501
    const/4 v7, 0x0

    .line 502
    .local v7, "str":Ljava/lang/String;
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 503
    const/4 v1, 0x1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 504
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    move-object v2, v7

    .line 505
    goto :goto_0
.end method

.method private showLocalContentContentScheme(Lcom/samsung/android/allshare/Item;Lcom/samsung/android/allshare/media/ContentInfo;)V
    .locals 10
    .param p1, "item"    # Lcom/samsung/android/allshare/Item;
    .param p2, "ci"    # Lcom/samsung/android/allshare/media/ContentInfo;

    .prologue
    const/4 v2, 0x0

    .line 545
    const-string v3, "ImageViewerImpl"

    const-string v4, "showLocalContentContentScheme()"

    invoke-static {v3, v4}, Lcom/samsung/android/allshare/DLog;->v_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 546
    iget-object v3, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    invoke-interface {v3}, Lcom/samsung/android/allshare/IAllShareConnector;->isAllShareServiceConnected()Z

    move-result v3

    if-nez v3, :cond_1

    .line 547
    :cond_0
    const-string v2, "ImageViewerImpl"

    const-string v3, "showLocalContentContentScheme Fail :  SERVICE_NOT_CONNECTED "

    invoke-static {v2, v3}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 548
    iget-object v2, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mResponseListener:Lcom/samsung/android/allshare/media/ImageViewer$IImageViewerResponseListener;

    sget-object v3, Lcom/samsung/android/allshare/ERROR;->SERVICE_NOT_CONNECTED:Lcom/samsung/android/allshare/ERROR;

    invoke-interface {v2, p1, p2, v3}, Lcom/samsung/android/allshare/media/ImageViewer$IImageViewerResponseListener;->onShowResponseReceived(Lcom/samsung/android/allshare/Item;Lcom/samsung/android/allshare/media/ContentInfo;Lcom/samsung/android/allshare/ERROR;)V

    .line 614
    :goto_0
    return-void

    .line 552
    :cond_1
    if-nez p1, :cond_2

    .line 553
    const-string v2, "ImageViewerImpl"

    const-string v3, "showLocalContentContentScheme Fail :  Item does not exist "

    invoke-static {v2, v3}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 554
    iget-object v2, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mResponseListener:Lcom/samsung/android/allshare/media/ImageViewer$IImageViewerResponseListener;

    sget-object v3, Lcom/samsung/android/allshare/ERROR;->ITEM_NOT_EXIST:Lcom/samsung/android/allshare/ERROR;

    invoke-interface {v2, p1, p2, v3}, Lcom/samsung/android/allshare/media/ImageViewer$IImageViewerResponseListener;->onShowResponseReceived(Lcom/samsung/android/allshare/Item;Lcom/samsung/android/allshare/media/ContentInfo;Lcom/samsung/android/allshare/ERROR;)V

    goto :goto_0

    .line 558
    :cond_2
    invoke-virtual {p1}, Lcom/samsung/android/allshare/Item;->getURI()Landroid/net/Uri;

    move-result-object v1

    .line 559
    .local v1, "uri":Landroid/net/Uri;
    if-nez v1, :cond_3

    .line 560
    const-string v2, "ImageViewerImpl"

    const-string v3, "showLocalContentContentScheme Fail :  uri == null "

    invoke-static {v2, v3}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 561
    iget-object v2, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mResponseListener:Lcom/samsung/android/allshare/media/ImageViewer$IImageViewerResponseListener;

    sget-object v3, Lcom/samsung/android/allshare/ERROR;->SERVICE_NOT_CONNECTED:Lcom/samsung/android/allshare/ERROR;

    invoke-interface {v2, p1, p2, v3}, Lcom/samsung/android/allshare/media/ImageViewer$IImageViewerResponseListener;->onShowResponseReceived(Lcom/samsung/android/allshare/Item;Lcom/samsung/android/allshare/media/ContentInfo;Lcom/samsung/android/allshare/ERROR;)V

    goto :goto_0

    .line 565
    :cond_3
    iget-object v3, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    invoke-interface {v3}, Lcom/samsung/android/allshare/IAllShareConnector;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 566
    .local v0, "resolver":Landroid/content/ContentResolver;
    if-nez v0, :cond_4

    .line 567
    const-string v2, "ImageViewerImpl"

    const-string v3, "showLocalContentContentScheme Fail :  resolver == null "

    invoke-static {v2, v3}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 568
    iget-object v2, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mResponseListener:Lcom/samsung/android/allshare/media/ImageViewer$IImageViewerResponseListener;

    sget-object v3, Lcom/samsung/android/allshare/ERROR;->SERVICE_NOT_CONNECTED:Lcom/samsung/android/allshare/ERROR;

    invoke-interface {v2, p1, p2, v3}, Lcom/samsung/android/allshare/media/ImageViewer$IImageViewerResponseListener;->onShowResponseReceived(Lcom/samsung/android/allshare/Item;Lcom/samsung/android/allshare/media/ContentInfo;Lcom/samsung/android/allshare/ERROR;)V

    goto :goto_0

    :cond_4
    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    .line 572
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 574
    .local v7, "cur":Landroid/database/Cursor;
    if-nez v7, :cond_5

    .line 575
    const-string v2, "ImageViewerImpl"

    const-string v3, "showLocalContentContentScheme Fail :  INVALID_ARGUMENT (cur == null) "

    invoke-static {v2, v3}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 577
    iget-object v2, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mResponseListener:Lcom/samsung/android/allshare/media/ImageViewer$IImageViewerResponseListener;

    sget-object v3, Lcom/samsung/android/allshare/ERROR;->INVALID_ARGUMENT:Lcom/samsung/android/allshare/ERROR;

    invoke-interface {v2, p1, p2, v3}, Lcom/samsung/android/allshare/media/ImageViewer$IImageViewerResponseListener;->onShowResponseReceived(Lcom/samsung/android/allshare/Item;Lcom/samsung/android/allshare/media/ContentInfo;Lcom/samsung/android/allshare/ERROR;)V

    goto :goto_0

    .line 580
    :cond_5
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    .line 582
    const-string v2, "_data"

    invoke-interface {v7, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    .line 584
    .local v9, "idx":I
    if-gez v9, :cond_6

    .line 585
    const-string v2, "ImageViewerImpl"

    const-string v3, "showLocalContentContentScheme Fail :  INVALID_ARGUMENT(idx < 0)"

    invoke-static {v2, v3}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 586
    iget-object v2, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mResponseListener:Lcom/samsung/android/allshare/media/ImageViewer$IImageViewerResponseListener;

    sget-object v3, Lcom/samsung/android/allshare/ERROR;->INVALID_ARGUMENT:Lcom/samsung/android/allshare/ERROR;

    invoke-interface {v2, p1, p2, v3}, Lcom/samsung/android/allshare/media/ImageViewer$IImageViewerResponseListener;->onShowResponseReceived(Lcom/samsung/android/allshare/Item;Lcom/samsung/android/allshare/media/ContentInfo;Lcom/samsung/android/allshare/ERROR;)V

    .line 587
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 591
    :cond_6
    iget-object v2, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mPlaylistPlayer:Lcom/samsung/android/allshare/media/PlaylistPlayer;

    if-eqz v2, :cond_7

    .line 592
    iget-object v2, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mPlaylistPlayer:Lcom/samsung/android/allshare/media/PlaylistPlayer;

    check-cast v2, Lcom/samsung/android/allshare/PlaylistPlayerImpl;

    invoke-interface {v7, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->setCurrentFilePath(Ljava/lang/String;)V

    .line 594
    :cond_7
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 596
    new-instance v8, Lcom/sec/android/allshare/iface/CVMessage;

    invoke-direct {v8}, Lcom/sec/android/allshare/iface/CVMessage;-><init>()V

    .line 597
    .local v8, "cvm":Lcom/sec/android/allshare/iface/CVMessage;
    const-string v2, "com.sec.android.allshare.action.ACTION_IMAGE_VIEWER_SHOW_LOCAL_CONTENT_URI"

    invoke-virtual {v8, v2}, Lcom/sec/android/allshare/iface/CVMessage;->setActionID(Ljava/lang/String;)V

    .line 598
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 599
    .local v6, "bundle":Landroid/os/Bundle;
    const-string v2, "BUNDLE_STRING_ID"

    invoke-virtual {p0}, Lcom/samsung/android/allshare/ImageViewerImpl;->getID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 601
    instance-of v2, p1, Lcom/sec/android/allshare/iface/IBundleHolder;

    if-eqz v2, :cond_8

    .line 602
    const-string v3, "BUNDLE_PARCELABLE_ITEM"

    move-object v2, p1

    check-cast v2, Lcom/sec/android/allshare/iface/IBundleHolder;

    invoke-interface {v2}, Lcom/sec/android/allshare/iface/IBundleHolder;->getBundle()Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v6, v3, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 606
    :cond_8
    const-string v4, "BUNDLE_LONG_CONTENT_INFO_STARTINGPOSITION"

    if-eqz p2, :cond_9

    invoke-virtual {p2}, Lcom/samsung/android/allshare/media/ContentInfo;->getStartingPosition()J

    move-result-wide v2

    :goto_1
    invoke-virtual {v6, v4, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 609
    invoke-virtual {v8, v6}, Lcom/sec/android/allshare/iface/CVMessage;->setBundle(Landroid/os/Bundle;)V

    .line 611
    iget-object v2, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    iget-object v3, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mResponseHandler:Lcom/samsung/android/allshare/AllShareResponseHandler;

    invoke-interface {v2, v8, v3}, Lcom/samsung/android/allshare/IAllShareConnector;->requestCVMAsync(Lcom/sec/android/allshare/iface/CVMessage;Lcom/samsung/android/allshare/AllShareResponseHandler;)J

    .line 612
    const-string v2, "ImageViewerImpl"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "showLocalContentContentScheme : [ "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Lcom/samsung/android/allshare/Item;->getTitle()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ]  to "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/samsung/android/allshare/ImageViewerImpl;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " uri : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/allshare/DLog;->i_api(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 606
    :cond_9
    const-wide/16 v2, 0x0

    goto :goto_1
.end method

.method private showLocalContentFileScheme(Lcom/samsung/android/allshare/Item;Lcom/samsung/android/allshare/media/ContentInfo;)V
    .locals 7
    .param p1, "item"    # Lcom/samsung/android/allshare/Item;
    .param p2, "ci"    # Lcom/samsung/android/allshare/media/ContentInfo;

    .prologue
    .line 642
    const-string v4, "ImageViewerImpl"

    const-string v5, "showLocalContentFileScheme()"

    invoke-static {v4, v5}, Lcom/samsung/android/allshare/DLog;->v_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 643
    iget-object v4, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    invoke-interface {v4}, Lcom/samsung/android/allshare/IAllShareConnector;->isAllShareServiceConnected()Z

    move-result v4

    if-nez v4, :cond_1

    .line 644
    :cond_0
    const-string v4, "ImageViewerImpl"

    const-string v5, "showLocalContentFileScheme : SERVICE_NOT_CONNECTED"

    invoke-static {v4, v5}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 645
    iget-object v4, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mResponseListener:Lcom/samsung/android/allshare/media/ImageViewer$IImageViewerResponseListener;

    sget-object v5, Lcom/samsung/android/allshare/ERROR;->SERVICE_NOT_CONNECTED:Lcom/samsung/android/allshare/ERROR;

    invoke-interface {v4, p1, p2, v5}, Lcom/samsung/android/allshare/media/ImageViewer$IImageViewerResponseListener;->onShowResponseReceived(Lcom/samsung/android/allshare/Item;Lcom/samsung/android/allshare/media/ContentInfo;Lcom/samsung/android/allshare/ERROR;)V

    .line 683
    :goto_0
    return-void

    .line 649
    :cond_1
    const-string v1, ""

    .line 650
    .local v1, "filePath":Ljava/lang/String;
    const-string v2, ""

    .line 652
    .local v2, "mimeType":Ljava/lang/String;
    instance-of v4, p1, Lcom/sec/android/allshare/iface/IBundleHolder;

    if-eqz v4, :cond_2

    move-object v4, p1

    .line 653
    check-cast v4, Lcom/sec/android/allshare/iface/IBundleHolder;

    invoke-interface {v4}, Lcom/sec/android/allshare/iface/IBundleHolder;->getBundle()Landroid/os/Bundle;

    move-result-object v0

    .line 654
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v4, "BUNDLE_STRING_FILEPATH"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 655
    const-string v4, "BUNDLE_STRING_ITEM_MIMETYPE"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 658
    .end local v0    # "bundle":Landroid/os/Bundle;
    :cond_2
    iget-object v4, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mPlaylistPlayer:Lcom/samsung/android/allshare/media/PlaylistPlayer;

    if-eqz v4, :cond_3

    .line 659
    iget-object v4, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mPlaylistPlayer:Lcom/samsung/android/allshare/media/PlaylistPlayer;

    check-cast v4, Lcom/samsung/android/allshare/PlaylistPlayerImpl;

    invoke-virtual {v4, v1}, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->setCurrentFilePath(Ljava/lang/String;)V

    .line 661
    :cond_3
    new-instance v3, Lcom/sec/android/allshare/iface/CVMessage;

    invoke-direct {v3}, Lcom/sec/android/allshare/iface/CVMessage;-><init>()V

    .line 662
    .local v3, "req_msg":Lcom/sec/android/allshare/iface/CVMessage;
    const-string v4, "com.sec.android.allshare.action.ACTION_IMAGE_VIEWER_SHOW_LOCAL_CONTENT"

    invoke-virtual {v3, v4}, Lcom/sec/android/allshare/iface/CVMessage;->setActionID(Ljava/lang/String;)V

    .line 663
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 665
    .restart local v0    # "bundle":Landroid/os/Bundle;
    const-string v4, "BUNDLE_STRING_ID"

    invoke-virtual {p0}, Lcom/samsung/android/allshare/ImageViewerImpl;->getID()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 666
    const-string v4, "BUNDLE_STRING_FILEPATH"

    invoke-virtual {v0, v4, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 667
    const-string v4, "BUNDLE_STRING_ITEM_MIMETYPE"

    invoke-virtual {v0, v4, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 668
    const-string v4, "BUNDLE_STRING_TITLE"

    invoke-virtual {p1}, Lcom/samsung/android/allshare/Item;->getTitle()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 670
    instance-of v4, p1, Lcom/sec/android/allshare/iface/IBundleHolder;

    if-eqz v4, :cond_4

    .line 671
    const-string v5, "BUNDLE_PARCELABLE_ITEM"

    move-object v4, p1

    check-cast v4, Lcom/sec/android/allshare/iface/IBundleHolder;

    invoke-interface {v4}, Lcom/sec/android/allshare/iface/IBundleHolder;->getBundle()Landroid/os/Bundle;

    move-result-object v4

    invoke-virtual {v0, v5, v4}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 675
    :cond_4
    const-string v6, "BUNDLE_LONG_CONTENT_INFO_STARTINGPOSITION"

    if-eqz p2, :cond_5

    invoke-virtual {p2}, Lcom/samsung/android/allshare/media/ContentInfo;->getStartingPosition()J

    move-result-wide v4

    :goto_1
    invoke-virtual {v0, v6, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 678
    invoke-virtual {v3, v0}, Lcom/sec/android/allshare/iface/CVMessage;->setBundle(Landroid/os/Bundle;)V

    .line 680
    iget-object v4, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    iget-object v5, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mResponseHandler:Lcom/samsung/android/allshare/AllShareResponseHandler;

    invoke-interface {v4, v3, v5}, Lcom/samsung/android/allshare/IAllShareConnector;->requestCVMAsync(Lcom/sec/android/allshare/iface/CVMessage;Lcom/samsung/android/allshare/AllShareResponseHandler;)J

    .line 681
    const-string v4, "ImageViewerImpl"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "showLocalContentFileScheme : ["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p1}, Lcom/samsung/android/allshare/Item;->getTitle()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ") to "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p0}, Lcom/samsung/android/allshare/ImageViewerImpl;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p0}, Lcom/samsung/android/allshare/ImageViewerImpl;->getIPAddress()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ")"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/android/allshare/DLog;->i_api(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 675
    :cond_5
    const-wide/16 v4, 0x0

    goto :goto_1
.end method

.method private showMediaContent(Lcom/samsung/android/allshare/Item;Lcom/samsung/android/allshare/media/ContentInfo;)V
    .locals 5
    .param p1, "i"    # Lcom/samsung/android/allshare/Item;
    .param p2, "ci"    # Lcom/samsung/android/allshare/media/ContentInfo;

    .prologue
    .line 617
    const-string v2, "ImageViewerImpl"

    const-string v3, "showMediaContent()"

    invoke-static {v2, v3}, Lcom/samsung/android/allshare/DLog;->v_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 618
    iget-object v2, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    invoke-interface {v2}, Lcom/samsung/android/allshare/IAllShareConnector;->isAllShareServiceConnected()Z

    move-result v2

    if-nez v2, :cond_1

    .line 619
    :cond_0
    const-string v2, "ImageViewerImpl"

    const-string v3, "showMediaContent : SERVICE_NOT_CONNECTED"

    invoke-static {v2, v3}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 620
    iget-object v2, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mResponseListener:Lcom/samsung/android/allshare/media/ImageViewer$IImageViewerResponseListener;

    sget-object v3, Lcom/samsung/android/allshare/ERROR;->SERVICE_NOT_CONNECTED:Lcom/samsung/android/allshare/ERROR;

    invoke-interface {v2, p1, p2, v3}, Lcom/samsung/android/allshare/media/ImageViewer$IImageViewerResponseListener;->onShowResponseReceived(Lcom/samsung/android/allshare/Item;Lcom/samsung/android/allshare/media/ContentInfo;Lcom/samsung/android/allshare/ERROR;)V

    .line 639
    :goto_0
    return-void

    .line 624
    :cond_1
    new-instance v1, Lcom/sec/android/allshare/iface/CVMessage;

    invoke-direct {v1}, Lcom/sec/android/allshare/iface/CVMessage;-><init>()V

    .line 625
    .local v1, "cvm":Lcom/sec/android/allshare/iface/CVMessage;
    const-string v2, "com.sec.android.allshare.action.ACTION_IMAGE_VIEWER_SHOW"

    invoke-virtual {v1, v2}, Lcom/sec/android/allshare/iface/CVMessage;->setActionID(Ljava/lang/String;)V

    .line 626
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 627
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v2, "BUNDLE_STRING_ID"

    invoke-virtual {p0}, Lcom/samsung/android/allshare/ImageViewerImpl;->getID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 628
    instance-of v2, p1, Lcom/sec/android/allshare/iface/IBundleHolder;

    if-eqz v2, :cond_2

    .line 629
    const-string v3, "BUNDLE_PARCELABLE_ITEM"

    move-object v2, p1

    check-cast v2, Lcom/sec/android/allshare/iface/IBundleHolder;

    invoke-interface {v2}, Lcom/sec/android/allshare/iface/IBundleHolder;->getBundle()Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 632
    :cond_2
    const-string v4, "BUNDLE_LONG_CONTENT_INFO_STARTINGPOSITION"

    if-eqz p2, :cond_3

    invoke-virtual {p2}, Lcom/samsung/android/allshare/media/ContentInfo;->getStartingPosition()J

    move-result-wide v2

    :goto_1
    invoke-virtual {v0, v4, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 635
    invoke-virtual {v1, v0}, Lcom/sec/android/allshare/iface/CVMessage;->setBundle(Landroid/os/Bundle;)V

    .line 637
    iget-object v2, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    iget-object v3, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mResponseHandler:Lcom/samsung/android/allshare/AllShareResponseHandler;

    invoke-interface {v2, v1, v3}, Lcom/samsung/android/allshare/IAllShareConnector;->requestCVMAsync(Lcom/sec/android/allshare/iface/CVMessage;Lcom/samsung/android/allshare/AllShareResponseHandler;)J

    .line 638
    const-string v2, "ImageViewerImpl"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "showMediaContent : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Lcom/samsung/android/allshare/Item;->getTitle()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " to "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/samsung/android/allshare/ImageViewerImpl;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/allshare/DLog;->i_api(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 632
    :cond_3
    const-wide/16 v2, 0x0

    goto :goto_1
.end method

.method private showWebContent(Lcom/samsung/android/allshare/Item;Lcom/samsung/android/allshare/media/ContentInfo;)V
    .locals 5
    .param p1, "ai"    # Lcom/samsung/android/allshare/Item;
    .param p2, "ci"    # Lcom/samsung/android/allshare/media/ContentInfo;

    .prologue
    .line 511
    iget-object v2, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    invoke-interface {v2}, Lcom/samsung/android/allshare/IAllShareConnector;->isAllShareServiceConnected()Z

    move-result v2

    if-nez v2, :cond_1

    .line 512
    :cond_0
    const-string v2, "ImageViewerImpl"

    const-string v3, "showWebContent : SERVICE_NOT_CONNECTED"

    invoke-static {v2, v3}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 513
    iget-object v2, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mResponseListener:Lcom/samsung/android/allshare/media/ImageViewer$IImageViewerResponseListener;

    sget-object v3, Lcom/samsung/android/allshare/ERROR;->SERVICE_NOT_CONNECTED:Lcom/samsung/android/allshare/ERROR;

    invoke-interface {v2, p1, p2, v3}, Lcom/samsung/android/allshare/media/ImageViewer$IImageViewerResponseListener;->onShowResponseReceived(Lcom/samsung/android/allshare/Item;Lcom/samsung/android/allshare/media/ContentInfo;Lcom/samsung/android/allshare/ERROR;)V

    .line 542
    :goto_0
    return-void

    .line 517
    :cond_1
    if-nez p1, :cond_2

    .line 518
    const-string v2, "ImageViewerImpl"

    const-string v3, "showLocalContentContentScheme Fail :  Item does not exist "

    invoke-static {v2, v3}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 519
    iget-object v2, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mResponseListener:Lcom/samsung/android/allshare/media/ImageViewer$IImageViewerResponseListener;

    sget-object v3, Lcom/samsung/android/allshare/ERROR;->ITEM_NOT_EXIST:Lcom/samsung/android/allshare/ERROR;

    invoke-interface {v2, p1, p2, v3}, Lcom/samsung/android/allshare/media/ImageViewer$IImageViewerResponseListener;->onShowResponseReceived(Lcom/samsung/android/allshare/Item;Lcom/samsung/android/allshare/media/ContentInfo;Lcom/samsung/android/allshare/ERROR;)V

    goto :goto_0

    .line 523
    :cond_2
    new-instance v1, Lcom/sec/android/allshare/iface/CVMessage;

    invoke-direct {v1}, Lcom/sec/android/allshare/iface/CVMessage;-><init>()V

    .line 524
    .local v1, "cvm":Lcom/sec/android/allshare/iface/CVMessage;
    const-string v2, "com.sec.android.allshare.action.ACTION_IMAGE_VIEWER_SHOW_URI"

    invoke-virtual {v1, v2}, Lcom/sec/android/allshare/iface/CVMessage;->setActionID(Ljava/lang/String;)V

    .line 525
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 526
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v2, "BUNDLE_STRING_ID"

    invoke-virtual {p0}, Lcom/samsung/android/allshare/ImageViewerImpl;->getID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 528
    instance-of v2, p1, Lcom/sec/android/allshare/iface/IBundleHolder;

    if-eqz v2, :cond_3

    .line 529
    const-string v3, "BUNDLE_PARCELABLE_ITEM"

    move-object v2, p1

    check-cast v2, Lcom/sec/android/allshare/iface/IBundleHolder;

    invoke-interface {v2}, Lcom/sec/android/allshare/iface/IBundleHolder;->getBundle()Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 533
    :cond_3
    const-string v4, "BUNDLE_LONG_CONTENT_INFO_STARTINGPOSITION"

    if-eqz p2, :cond_4

    invoke-virtual {p2}, Lcom/samsung/android/allshare/media/ContentInfo;->getStartingPosition()J

    move-result-wide v2

    :goto_1
    invoke-virtual {v0, v4, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 536
    invoke-virtual {v1, v0}, Lcom/sec/android/allshare/iface/CVMessage;->setBundle(Landroid/os/Bundle;)V

    .line 538
    iget-object v2, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    iget-object v3, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mResponseHandler:Lcom/samsung/android/allshare/AllShareResponseHandler;

    invoke-interface {v2, v1, v3}, Lcom/samsung/android/allshare/IAllShareConnector;->requestCVMAsync(Lcom/sec/android/allshare/iface/CVMessage;Lcom/samsung/android/allshare/AllShareResponseHandler;)J

    .line 539
    const-string v2, "ImageViewerImpl"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "showWebContent : [ "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Lcom/samsung/android/allshare/Item;->getTitle()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ]  to "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/samsung/android/allshare/ImageViewerImpl;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " uri : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Lcom/samsung/android/allshare/Item;->getURI()Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/allshare/DLog;->i_api(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 533
    :cond_4
    const-wide/16 v2, 0x0

    goto :goto_1
.end method


# virtual methods
.method public getBundle()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 843
    iget-object v0, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    if-nez v0, :cond_0

    .line 844
    const/4 v0, 0x0

    .line 846
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/DeviceImpl;->getBundle()Landroid/os/Bundle;

    move-result-object v0

    goto :goto_0
.end method

.method public getDeviceDomain()Lcom/samsung/android/allshare/Device$DeviceDomain;
    .locals 1

    .prologue
    .line 769
    iget-object v0, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    if-nez v0, :cond_0

    .line 770
    sget-object v0, Lcom/samsung/android/allshare/Device$DeviceDomain;->UNKNOWN:Lcom/samsung/android/allshare/Device$DeviceDomain;

    .line 772
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/DeviceImpl;->getDeviceDomain()Lcom/samsung/android/allshare/Device$DeviceDomain;

    move-result-object v0

    goto :goto_0
.end method

.method public getDeviceType()Lcom/samsung/android/allshare/Device$DeviceType;
    .locals 2

    .prologue
    .line 810
    iget-object v1, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    if-nez v1, :cond_0

    .line 811
    sget-object v0, Lcom/samsung/android/allshare/Device$DeviceType;->UNKNOWN:Lcom/samsung/android/allshare/Device$DeviceType;

    .line 814
    :goto_0
    return-object v0

    .line 813
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    invoke-virtual {v1}, Lcom/samsung/android/allshare/DeviceImpl;->getDeviceType()Lcom/samsung/android/allshare/Device$DeviceType;

    move-result-object v0

    .line 814
    .local v0, "result":Lcom/samsung/android/allshare/Device$DeviceType;
    goto :goto_0
.end method

.method public getID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 777
    iget-object v0, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    if-nez v0, :cond_0

    .line 778
    const-string v0, ""

    .line 780
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/DeviceImpl;->getID()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getIPAddress()Ljava/lang/String;
    .locals 1

    .prologue
    .line 835
    iget-object v0, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    if-nez v0, :cond_0

    .line 836
    const-string v0, ""

    .line 838
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/DeviceImpl;->getIPAddress()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getIPAdress()Ljava/lang/String;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 830
    invoke-virtual {p0}, Lcom/samsung/android/allshare/ImageViewerImpl;->getIPAddress()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getIcon()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 785
    iget-object v0, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    if-nez v0, :cond_0

    .line 786
    const/4 v0, 0x0

    .line 788
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/DeviceImpl;->getIcon()Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method public getIconList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/Icon;",
            ">;"
        }
    .end annotation

    .prologue
    .line 793
    iget-object v0, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    if-nez v0, :cond_0

    .line 794
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 796
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/DeviceImpl;->getIconList()Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_0
.end method

.method public getModelName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 820
    iget-object v0, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    if-nez v0, :cond_0

    .line 821
    const-string v0, ""

    .line 823
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/DeviceImpl;->getModelName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getNIC()Ljava/lang/String;
    .locals 1

    .prologue
    .line 327
    iget-object v0, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    if-nez v0, :cond_0

    .line 328
    const-string v0, ""

    .line 330
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/DeviceImpl;->getNIC()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 801
    iget-object v0, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    if-nez v0, :cond_0

    .line 802
    const-string v0, ""

    .line 804
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/DeviceImpl;->getName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getP2pMacAddress()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1103
    iget-object v0, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    if-nez v0, :cond_0

    .line 1104
    const-string v0, ""

    .line 1106
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/DeviceImpl;->getP2pMacAddress()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getPlaylistPlayer()Lcom/samsung/android/allshare/media/PlaylistPlayer;
    .locals 1

    .prologue
    .line 852
    iget-object v0, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mPlaylistPlayer:Lcom/samsung/android/allshare/media/PlaylistPlayer;

    return-object v0
.end method

.method public getSlideShowPlayer()Lcom/samsung/android/allshare/media/SlideShowPlayer;
    .locals 9

    .prologue
    const/4 v6, 0x0

    .line 963
    iget-object v7, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    invoke-interface {v7}, Lcom/samsung/android/allshare/IAllShareConnector;->isAllShareServiceConnected()Z

    move-result v7

    if-nez v7, :cond_1

    .line 1002
    :cond_0
    :goto_0
    return-object v6

    .line 966
    :cond_1
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 967
    .local v2, "parmBundle":Landroid/os/Bundle;
    invoke-virtual {p0}, Lcom/samsung/android/allshare/ImageViewerImpl;->getBundle()Landroid/os/Bundle;

    move-result-object v0

    .line 968
    .local v0, "b":Landroid/os/Bundle;
    if-nez v0, :cond_2

    .line 969
    const-string v7, "ImageViewerImpl"

    const-string v8, "getSlideShowPlayer : bundle is Null"

    invoke-static {v7, v8}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 973
    :cond_2
    const-string v7, "BUNDLE_STRING_ID"

    invoke-virtual {p0}, Lcom/samsung/android/allshare/ImageViewerImpl;->getID()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v7, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 975
    new-instance v3, Lcom/sec/android/allshare/iface/CVMessage;

    invoke-direct {v3}, Lcom/sec/android/allshare/iface/CVMessage;-><init>()V

    .line 976
    .local v3, "req_msg":Lcom/sec/android/allshare/iface/CVMessage;
    const-string v7, "com.sec.android.allshare.action.ACTION_IMAGE_VIEWER_GET_SLIDESHOWPLAYER_SYNC"

    invoke-virtual {v3, v7}, Lcom/sec/android/allshare/iface/CVMessage;->setActionID(Ljava/lang/String;)V

    .line 977
    invoke-virtual {v3, v2}, Lcom/sec/android/allshare/iface/CVMessage;->setBundle(Landroid/os/Bundle;)V

    .line 979
    iget-object v7, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    invoke-interface {v7, v3}, Lcom/samsung/android/allshare/IAllShareConnector;->requestCVMSync(Lcom/sec/android/allshare/iface/CVMessage;)Lcom/sec/android/allshare/iface/CVMessage;

    move-result-object v5

    .line 981
    .local v5, "res_message":Lcom/sec/android/allshare/iface/CVMessage;
    if-nez v5, :cond_3

    .line 982
    const-string v7, "ImageViewerImpl"

    const-string v8, "res_message is Null"

    invoke-static {v7, v8}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 986
    :cond_3
    invoke-virtual {v5}, Lcom/sec/android/allshare/iface/CVMessage;->getBundle()Landroid/os/Bundle;

    move-result-object v4

    .line 988
    .local v4, "res_bundle":Landroid/os/Bundle;
    if-nez v4, :cond_4

    .line 989
    const-string v7, "ImageViewerImpl"

    const-string v8, "res_bundle is Null"

    invoke-static {v7, v8}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 994
    :cond_4
    const-string v7, "BUNDLE_BOOLEAN_SUPPORT_SPC_SLIDESHOW"

    invoke-virtual {v4, v7}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    .line 996
    .local v1, "isSupport":Z
    if-eqz v1, :cond_0

    .line 999
    iget-object v6, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mSlideShowPlayer:Lcom/samsung/android/allshare/media/SlideShowPlayer;

    if-nez v6, :cond_5

    .line 1000
    new-instance v6, Lcom/samsung/android/allshare/SlideShowPlayerImpl;

    iget-object v7, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    iget-object v8, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    invoke-direct {v6, v7, v8}, Lcom/samsung/android/allshare/SlideShowPlayerImpl;-><init>(Lcom/samsung/android/allshare/IAllShareConnector;Lcom/samsung/android/allshare/DeviceImpl;)V

    iput-object v6, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mSlideShowPlayer:Lcom/samsung/android/allshare/media/SlideShowPlayer;

    .line 1002
    :cond_5
    iget-object v6, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mSlideShowPlayer:Lcom/samsung/android/allshare/media/SlideShowPlayer;

    goto :goto_0
.end method

.method public getState()V
    .locals 5

    .prologue
    .line 1007
    iget-object v2, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    invoke-interface {v2}, Lcom/samsung/android/allshare/IAllShareConnector;->isAllShareServiceConnected()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1008
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mResponseListener:Lcom/samsung/android/allshare/media/ImageViewer$IImageViewerResponseListener;

    sget-object v3, Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;->UNKNOWN:Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;

    sget-object v4, Lcom/samsung/android/allshare/ERROR;->SERVICE_NOT_CONNECTED:Lcom/samsung/android/allshare/ERROR;

    invoke-interface {v2, v3, v4}, Lcom/samsung/android/allshare/media/ImageViewer$IImageViewerResponseListener;->onGetStateResponseReceived(Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;Lcom/samsung/android/allshare/ERROR;)V

    .line 1021
    :goto_0
    return-void

    .line 1013
    :cond_1
    new-instance v1, Lcom/sec/android/allshare/iface/CVMessage;

    invoke-direct {v1}, Lcom/sec/android/allshare/iface/CVMessage;-><init>()V

    .line 1014
    .local v1, "req_msg":Lcom/sec/android/allshare/iface/CVMessage;
    const-string v2, "com.sec.android.allshare.action.ACTION_IMAGE_VIEWER_REQUEST_GET_VIEWER_STATE"

    invoke-virtual {v1, v2}, Lcom/sec/android/allshare/iface/CVMessage;->setActionID(Ljava/lang/String;)V

    .line 1016
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1017
    .local v0, "req_bundle":Landroid/os/Bundle;
    const-string v2, "BUNDLE_STRING_ID"

    invoke-virtual {p0}, Lcom/samsung/android/allshare/ImageViewerImpl;->getID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1018
    invoke-virtual {v1, v0}, Lcom/sec/android/allshare/iface/CVMessage;->setBundle(Landroid/os/Bundle;)V

    .line 1020
    iget-object v2, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    iget-object v3, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mResponseHandler:Lcom/samsung/android/allshare/AllShareResponseHandler;

    invoke-interface {v2, v1, v3}, Lcom/samsung/android/allshare/IAllShareConnector;->requestCVMAsync(Lcom/sec/android/allshare/iface/CVMessage;Lcom/samsung/android/allshare/AllShareResponseHandler;)J

    goto :goto_0
.end method

.method public getViewController()Lcom/samsung/android/allshare/media/ViewController;
    .locals 13

    .prologue
    const/4 v10, 0x0

    .line 858
    iget-object v11, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    if-eqz v11, :cond_0

    iget-object v11, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    invoke-interface {v11}, Lcom/samsung/android/allshare/IAllShareConnector;->isAllShareServiceConnected()Z

    move-result v11

    if-nez v11, :cond_1

    .line 912
    :cond_0
    :goto_0
    return-object v10

    .line 861
    :cond_1
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 862
    .local v6, "parmBundle":Landroid/os/Bundle;
    invoke-virtual {p0}, Lcom/samsung/android/allshare/ImageViewerImpl;->getBundle()Landroid/os/Bundle;

    move-result-object v0

    .line 863
    .local v0, "b":Landroid/os/Bundle;
    if-nez v0, :cond_2

    .line 864
    const-string v11, "ImageViewerImpl"

    const-string v12, "getViewController : bundle is Null"

    invoke-static {v11, v12}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 868
    :cond_2
    const-string v11, "BUNDLE_STRING_DEVICE_ID"

    invoke-virtual {v0, v11}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 870
    .local v1, "deviceId":Ljava/lang/String;
    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v11

    if-nez v11, :cond_4

    .line 871
    :cond_3
    const-string v11, "ImageViewerImpl"

    const-string v12, "getViewController : deviceId is Null"

    invoke-static {v11, v12}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 875
    :cond_4
    const-string v11, "BUNDLE_STRING_DEVICE_ID"

    invoke-virtual {v6, v11, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 877
    new-instance v7, Lcom/sec/android/allshare/iface/CVMessage;

    invoke-direct {v7}, Lcom/sec/android/allshare/iface/CVMessage;-><init>()V

    .line 878
    .local v7, "req_msg":Lcom/sec/android/allshare/iface/CVMessage;
    const-string v11, "com.sec.android.allshare.action.ACTION_IMAGE_VIEWER_SET_VIEW_CONTROLLER_SYNC"

    invoke-virtual {v7, v11}, Lcom/sec/android/allshare/iface/CVMessage;->setActionID(Ljava/lang/String;)V

    .line 879
    invoke-virtual {v7, v6}, Lcom/sec/android/allshare/iface/CVMessage;->setBundle(Landroid/os/Bundle;)V

    .line 881
    iget-object v11, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    invoke-interface {v11, v7}, Lcom/samsung/android/allshare/IAllShareConnector;->requestCVMSync(Lcom/sec/android/allshare/iface/CVMessage;)Lcom/sec/android/allshare/iface/CVMessage;

    move-result-object v9

    .line 883
    .local v9, "res_message":Lcom/sec/android/allshare/iface/CVMessage;
    if-nez v9, :cond_5

    .line 884
    const-string v11, "ImageViewerImpl"

    const-string v12, "res_message is Null"

    invoke-static {v11, v12}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 888
    :cond_5
    invoke-virtual {v9}, Lcom/sec/android/allshare/iface/CVMessage;->getBundle()Landroid/os/Bundle;

    move-result-object v8

    .line 890
    .local v8, "res_bundle":Landroid/os/Bundle;
    if-nez v8, :cond_6

    .line 891
    const-string v11, "ImageViewerImpl"

    const-string v12, "res_bundle is Null"

    invoke-static {v11, v12}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 895
    :cond_6
    const-string v11, "BUNDLE_INT_TV_WIDTH_RESOLUTION"

    invoke-virtual {v8, v11}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    .line 896
    .local v5, "nTvWidth":I
    const-string v11, "BUNDLE_INT_TV_HEIGHT_RESOLUTION"

    invoke-virtual {v8, v11}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    .line 898
    .local v4, "nTvHeight":I
    const-string v11, "BUNDLE_BOOLEAN_ZOOMABLE"

    invoke-virtual {v8, v11}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    .line 899
    .local v3, "isZoomable":Z
    const-string v11, "BUNDLE_BOOLEAN_ROTATABLE"

    invoke-virtual {v8, v11}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    .line 901
    .local v2, "isRotatable":Z
    if-eqz v3, :cond_0

    if-eqz v2, :cond_0

    .line 908
    iget-object v10, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mViewController:Lcom/samsung/android/allshare/media/ViewController;

    if-nez v10, :cond_7

    .line 909
    new-instance v10, Lcom/samsung/android/allshare/ViewControllerImpl;

    iget-object v11, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    iget-object v12, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    invoke-direct {v10, v11, v12, v5, v4}, Lcom/samsung/android/allshare/ViewControllerImpl;-><init>(Lcom/samsung/android/allshare/IAllShareConnector;Lcom/samsung/android/allshare/DeviceImpl;II)V

    iput-object v10, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mViewController:Lcom/samsung/android/allshare/media/ViewController;

    .line 912
    :cond_7
    iget-object v10, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mViewController:Lcom/samsung/android/allshare/media/ViewController;

    goto/16 :goto_0
.end method

.method public getViewController2()Lcom/samsung/android/allshare/media/ViewController2;
    .locals 9

    .prologue
    const/4 v6, 0x0

    .line 918
    iget-object v7, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    invoke-interface {v7}, Lcom/samsung/android/allshare/IAllShareConnector;->isAllShareServiceConnected()Z

    move-result v7

    if-nez v7, :cond_1

    .line 957
    :cond_0
    :goto_0
    return-object v6

    .line 921
    :cond_1
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 922
    .local v2, "parmBundle":Landroid/os/Bundle;
    invoke-virtual {p0}, Lcom/samsung/android/allshare/ImageViewerImpl;->getBundle()Landroid/os/Bundle;

    move-result-object v0

    .line 923
    .local v0, "b":Landroid/os/Bundle;
    if-nez v0, :cond_2

    .line 924
    const-string v7, "ImageViewerImpl"

    const-string v8, "getViewController2 : bundle is Null"

    invoke-static {v7, v8}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 928
    :cond_2
    const-string v7, "BUNDLE_STRING_ID"

    invoke-virtual {p0}, Lcom/samsung/android/allshare/ImageViewerImpl;->getID()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v7, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 930
    new-instance v3, Lcom/sec/android/allshare/iface/CVMessage;

    invoke-direct {v3}, Lcom/sec/android/allshare/iface/CVMessage;-><init>()V

    .line 931
    .local v3, "req_msg":Lcom/sec/android/allshare/iface/CVMessage;
    const-string v7, "com.sec.android.allshare.action.ACTION_IMAGE_VIEWER_GET_VIEW_CONTROLLER2_SYNC"

    invoke-virtual {v3, v7}, Lcom/sec/android/allshare/iface/CVMessage;->setActionID(Ljava/lang/String;)V

    .line 932
    invoke-virtual {v3, v2}, Lcom/sec/android/allshare/iface/CVMessage;->setBundle(Landroid/os/Bundle;)V

    .line 934
    iget-object v7, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    invoke-interface {v7, v3}, Lcom/samsung/android/allshare/IAllShareConnector;->requestCVMSync(Lcom/sec/android/allshare/iface/CVMessage;)Lcom/sec/android/allshare/iface/CVMessage;

    move-result-object v5

    .line 936
    .local v5, "res_message":Lcom/sec/android/allshare/iface/CVMessage;
    if-nez v5, :cond_3

    .line 937
    const-string v7, "ImageViewerImpl"

    const-string v8, "res_message is Null"

    invoke-static {v7, v8}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 941
    :cond_3
    invoke-virtual {v5}, Lcom/sec/android/allshare/iface/CVMessage;->getBundle()Landroid/os/Bundle;

    move-result-object v4

    .line 943
    .local v4, "res_bundle":Landroid/os/Bundle;
    if-nez v4, :cond_4

    .line 944
    const-string v7, "ImageViewerImpl"

    const-string v8, "res_bundle is Null"

    invoke-static {v7, v8}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 949
    :cond_4
    const-string v7, "BUNDLE_BOOLEAN_SUPPORT_SPC_IMAGEZOOM"

    invoke-virtual {v4, v7}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    .line 951
    .local v1, "isSupport":Z
    if-eqz v1, :cond_0

    .line 954
    iget-object v6, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mViewController2:Lcom/samsung/android/allshare/media/ViewController2;

    if-nez v6, :cond_5

    .line 955
    new-instance v6, Lcom/samsung/android/allshare/ViewController2Impl;

    iget-object v7, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    iget-object v8, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    invoke-direct {v6, v7, v8}, Lcom/samsung/android/allshare/ViewController2Impl;-><init>(Lcom/samsung/android/allshare/IAllShareConnector;Lcom/samsung/android/allshare/DeviceImpl;)V

    iput-object v6, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mViewController2:Lcom/samsung/android/allshare/media/ViewController2;

    .line 957
    :cond_5
    iget-object v6, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mViewController2:Lcom/samsung/android/allshare/media/ViewController2;

    goto :goto_0
.end method

.method public getViewerState()Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;
    .locals 7

    .prologue
    .line 706
    iget-object v6, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    invoke-interface {v6}, Lcom/samsung/android/allshare/IAllShareConnector;->isAllShareServiceConnected()Z

    move-result v6

    if-nez v6, :cond_1

    .line 707
    :cond_0
    sget-object v6, Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;->UNKNOWN:Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;

    .line 733
    :goto_0
    return-object v6

    .line 709
    :cond_1
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 710
    .local v2, "parmBundle":Landroid/os/Bundle;
    invoke-virtual {p0}, Lcom/samsung/android/allshare/ImageViewerImpl;->getBundle()Landroid/os/Bundle;

    move-result-object v0

    .line 711
    .local v0, "b":Landroid/os/Bundle;
    if-nez v0, :cond_2

    .line 712
    sget-object v6, Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;->UNKNOWN:Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;

    goto :goto_0

    .line 713
    :cond_2
    const-string v6, "BUNDLE_STRING_ID"

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 714
    .local v1, "id":Ljava/lang/String;
    if-nez v1, :cond_3

    .line 715
    sget-object v6, Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;->UNKNOWN:Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;

    goto :goto_0

    .line 717
    :cond_3
    const-string v6, "BUNDLE_STRING_ID"

    invoke-virtual {v2, v6, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 719
    new-instance v3, Lcom/sec/android/allshare/iface/CVMessage;

    invoke-direct {v3}, Lcom/sec/android/allshare/iface/CVMessage;-><init>()V

    .line 720
    .local v3, "req_msg":Lcom/sec/android/allshare/iface/CVMessage;
    const-string v6, "com.sec.android.allshare.action.ACTION_IMAGE_VIEWER_GET_VIEWER_STATE_SYNC"

    invoke-virtual {v3, v6}, Lcom/sec/android/allshare/iface/CVMessage;->setActionID(Ljava/lang/String;)V

    .line 721
    invoke-virtual {v3, v2}, Lcom/sec/android/allshare/iface/CVMessage;->setBundle(Landroid/os/Bundle;)V

    .line 723
    iget-object v6, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    invoke-interface {v6, v3}, Lcom/samsung/android/allshare/IAllShareConnector;->requestCVMSync(Lcom/sec/android/allshare/iface/CVMessage;)Lcom/sec/android/allshare/iface/CVMessage;

    move-result-object v5

    .line 725
    .local v5, "res_message":Lcom/sec/android/allshare/iface/CVMessage;
    if-nez v5, :cond_4

    .line 726
    sget-object v6, Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;->UNKNOWN:Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;

    goto :goto_0

    .line 728
    :cond_4
    invoke-virtual {v5}, Lcom/sec/android/allshare/iface/CVMessage;->getBundle()Landroid/os/Bundle;

    move-result-object v4

    .line 730
    .local v4, "res_bundle":Landroid/os/Bundle;
    if-nez v4, :cond_5

    .line 731
    sget-object v6, Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;->UNKNOWN:Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;

    goto :goto_0

    .line 733
    :cond_5
    const-string v6, "BUNDLE_STRING_IMAGE_VIEWEW_STATE"

    invoke-virtual {v4, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;->stringToEnum(Ljava/lang/String;)Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;

    move-result-object v6

    goto :goto_0
.end method

.method public isRedirectSupportable()Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1085
    invoke-virtual {p0}, Lcom/samsung/android/allshare/ImageViewerImpl;->isSupportRedirect()Z

    move-result v0

    return v0
.end method

.method public isSeekableOnPaused()Z
    .locals 1

    .prologue
    .line 1091
    const/4 v0, 0x0

    return v0
.end method

.method public isSupportRedirect()Z
    .locals 10

    .prologue
    const/4 v7, 0x0

    .line 1046
    const/4 v6, 0x0

    .line 1047
    .local v6, "result":Z
    iget-object v8, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    if-eqz v8, :cond_0

    iget-object v8, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    invoke-interface {v8}, Lcom/samsung/android/allshare/IAllShareConnector;->isAllShareServiceConnected()Z

    move-result v8

    if-nez v8, :cond_1

    .line 1078
    :cond_0
    :goto_0
    return v7

    .line 1050
    :cond_1
    new-instance v3, Lcom/sec/android/allshare/iface/CVMessage;

    invoke-direct {v3}, Lcom/sec/android/allshare/iface/CVMessage;-><init>()V

    .line 1051
    .local v3, "req_msg":Lcom/sec/android/allshare/iface/CVMessage;
    const-string v8, "ACTION_IMAGE_VIEWER_IS_SUPPORT_REDIRECT_SYNC"

    invoke-virtual {v3, v8}, Lcom/sec/android/allshare/iface/CVMessage;->setActionID(Ljava/lang/String;)V

    .line 1053
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 1054
    .local v2, "req_bundle":Landroid/os/Bundle;
    const-string v8, "BUNDLE_STRING_ID"

    invoke-virtual {p0}, Lcom/samsung/android/allshare/ImageViewerImpl;->getID()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v2, v8, v9}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1055
    invoke-virtual {v3, v2}, Lcom/sec/android/allshare/iface/CVMessage;->setBundle(Landroid/os/Bundle;)V

    .line 1057
    iget-object v8, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    invoke-interface {v8, v3}, Lcom/samsung/android/allshare/IAllShareConnector;->requestCVMSync(Lcom/sec/android/allshare/iface/CVMessage;)Lcom/sec/android/allshare/iface/CVMessage;

    move-result-object v5

    .line 1058
    .local v5, "res_msg":Lcom/sec/android/allshare/iface/CVMessage;
    if-eqz v5, :cond_0

    .line 1061
    invoke-virtual {v5}, Lcom/sec/android/allshare/iface/CVMessage;->getBundle()Landroid/os/Bundle;

    move-result-object v4

    .line 1063
    .local v4, "res_bundle":Landroid/os/Bundle;
    if-eqz v4, :cond_0

    .line 1066
    const-string v8, "BUNDLE_ENUM_ERROR"

    invoke-virtual {v4, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1067
    .local v1, "err":Ljava/lang/String;
    if-eqz v1, :cond_2

    sget-object v8, Lcom/samsung/android/allshare/ERROR;->NOT_SUPPORTED_FRAMEWORK_VERSION:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v8}, Lcom/samsung/android/allshare/ERROR;->enumToString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 1068
    const-string v8, "ImageViewerImpl"

    const-string v9, " isRedirectSupportable() Exception : NOT_SUPPORTED_FRAMEWORK_VERSION"

    invoke-static {v8, v9}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1074
    :cond_2
    :try_start_0
    const-string v7, "BUNDLE_BOOLEAN_SUPPORT_REDIRECT"

    invoke-virtual {v4, v7}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v6

    :goto_1
    move v7, v6

    .line 1078
    goto :goto_0

    .line 1075
    :catch_0
    move-exception v0

    .line 1076
    .local v0, "e":Ljava/lang/Exception;
    const-string v7, "ImageViewerImpl"

    const-string v8, "isRedirectSupportable Exception"

    invoke-static {v7, v8, v0}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_1
.end method

.method public isWholeHomeAudio()Z
    .locals 1

    .prologue
    .line 1096
    const/4 v0, 0x0

    return v0
.end method

.method public prepare(Lcom/samsung/android/allshare/Item;)V
    .locals 5
    .param p1, "ai"    # Lcom/samsung/android/allshare/Item;

    .prologue
    .line 335
    iget-object v2, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    invoke-interface {v2}, Lcom/samsung/android/allshare/IAllShareConnector;->isAllShareServiceConnected()Z

    move-result v2

    if-nez v2, :cond_1

    .line 336
    :cond_0
    const-string v2, "ImageViewerImpl"

    const-string v3, "prepare : SERVICE_NOT_CONNECTED"

    invoke-static {v2, v3}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 361
    :goto_0
    return-void

    .line 340
    :cond_1
    if-nez p1, :cond_2

    .line 341
    const-string v2, "ImageViewerImpl"

    const-string v3, "prepare Fail :  Item does not exist "

    invoke-static {v2, v3}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 345
    :cond_2
    new-instance v1, Lcom/sec/android/allshare/iface/CVMessage;

    invoke-direct {v1}, Lcom/sec/android/allshare/iface/CVMessage;-><init>()V

    .line 346
    .local v1, "cvm":Lcom/sec/android/allshare/iface/CVMessage;
    const-string v2, "ACTION_IMAGE_VIEWER_PREPARE"

    invoke-virtual {v1, v2}, Lcom/sec/android/allshare/iface/CVMessage;->setActionID(Ljava/lang/String;)V

    .line 347
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 348
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v2, "BUNDLE_STRING_ID"

    invoke-virtual {p0}, Lcom/samsung/android/allshare/ImageViewerImpl;->getID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 350
    instance-of v2, p1, Lcom/sec/android/allshare/iface/IBundleHolder;

    if-eqz v2, :cond_3

    .line 351
    const-string v3, "BUNDLE_PARCELABLE_ITEM"

    move-object v2, p1

    check-cast v2, Lcom/sec/android/allshare/iface/IBundleHolder;

    invoke-interface {v2}, Lcom/sec/android/allshare/iface/IBundleHolder;->getBundle()Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 354
    :cond_3
    invoke-virtual {v1, v0}, Lcom/sec/android/allshare/iface/CVMessage;->setBundle(Landroid/os/Bundle;)V

    .line 356
    iget-object v2, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    iget-object v3, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mResponseHandler:Lcom/samsung/android/allshare/AllShareResponseHandler;

    invoke-interface {v2, v1, v3}, Lcom/samsung/android/allshare/IAllShareConnector;->requestCVMAsync(Lcom/sec/android/allshare/iface/CVMessage;Lcom/samsung/android/allshare/AllShareResponseHandler;)J

    .line 357
    const-string v2, "ImageViewerImpl"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "prepare : [ "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Lcom/samsung/android/allshare/Item;->getTitle()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " to "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/samsung/android/allshare/ImageViewerImpl;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " uri : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Lcom/samsung/android/allshare/Item;->getURI()Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ] "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/allshare/DLog;->i_api(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public removeEventHandler()V
    .locals 4

    .prologue
    .line 1025
    iget-object v0, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    const-string v1, "com.sec.android.allshare.event.EVENT_DEVICE_SUBSCRIBE"

    invoke-virtual {p0}, Lcom/samsung/android/allshare/ImageViewerImpl;->getBundle()Landroid/os/Bundle;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mEventHandler:Lcom/samsung/android/allshare/AllShareEventHandler;

    invoke-interface {v0, v1, v2, v3}, Lcom/samsung/android/allshare/IAllShareConnector;->unsubscribeAllShareEvent(Ljava/lang/String;Landroid/os/Bundle;Lcom/samsung/android/allshare/AllShareEventHandler;)V

    .line 1027
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mIsSubscribed:Z

    .line 1029
    iget-object v0, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mViewController:Lcom/samsung/android/allshare/media/ViewController;

    if-eqz v0, :cond_0

    .line 1030
    iget-object v0, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mViewController:Lcom/samsung/android/allshare/media/ViewController;

    check-cast v0, Lcom/samsung/android/allshare/ViewControllerImpl;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/ViewControllerImpl;->removeEventHandler()V

    .line 1032
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mViewController2:Lcom/samsung/android/allshare/media/ViewController2;

    if-eqz v0, :cond_1

    .line 1033
    iget-object v0, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mViewController2:Lcom/samsung/android/allshare/media/ViewController2;

    check-cast v0, Lcom/samsung/android/allshare/ViewController2Impl;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/ViewController2Impl;->removeEventHandler()V

    .line 1035
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mSlideShowPlayer:Lcom/samsung/android/allshare/media/SlideShowPlayer;

    if-eqz v0, :cond_2

    .line 1036
    iget-object v0, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mSlideShowPlayer:Lcom/samsung/android/allshare/media/SlideShowPlayer;

    check-cast v0, Lcom/samsung/android/allshare/SlideShowPlayerImpl;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/SlideShowPlayerImpl;->removeEventHandler()V

    .line 1038
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mPlaylistPlayer:Lcom/samsung/android/allshare/media/PlaylistPlayer;

    if-eqz v0, :cond_3

    .line 1039
    iget-object v0, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mPlaylistPlayer:Lcom/samsung/android/allshare/media/PlaylistPlayer;

    check-cast v0, Lcom/samsung/android/allshare/PlaylistPlayerImpl;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->removeEventHandler()V

    .line 1041
    :cond_3
    return-void
.end method

.method public setEventListener(Lcom/samsung/android/allshare/media/ImageViewer$IImageViewerEventListener;)V
    .locals 4
    .param p1, "l"    # Lcom/samsung/android/allshare/media/ImageViewer$IImageViewerEventListener;

    .prologue
    .line 745
    iget-object v0, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    invoke-interface {v0}, Lcom/samsung/android/allshare/IAllShareConnector;->isAllShareServiceConnected()Z

    move-result v0

    if-nez v0, :cond_2

    .line 746
    :cond_0
    const-string v0, "ImageViewerImpl"

    const-string v1, "setEventListener error! AllShareService is not connected"

    invoke-static {v0, v1}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 764
    :cond_1
    :goto_0
    return-void

    .line 750
    :cond_2
    const-string v0, "ImageViewerImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setEventListener to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/allshare/DLog;->d_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 751
    iput-object p1, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mEventListener:Lcom/samsung/android/allshare/media/ImageViewer$IImageViewerEventListener;

    .line 753
    iget-boolean v0, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mIsSubscribed:Z

    if-nez v0, :cond_3

    if-eqz p1, :cond_3

    .line 754
    iget-object v0, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    const-string v1, "com.sec.android.allshare.event.EVENT_DEVICE_SUBSCRIBE"

    iget-object v2, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    invoke-virtual {v2}, Lcom/samsung/android/allshare/DeviceImpl;->getBundle()Landroid/os/Bundle;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mEventHandler:Lcom/samsung/android/allshare/AllShareEventHandler;

    invoke-interface {v0, v1, v2, v3}, Lcom/samsung/android/allshare/IAllShareConnector;->subscribeAllShareEvent(Ljava/lang/String;Landroid/os/Bundle;Lcom/samsung/android/allshare/AllShareEventHandler;)Z

    .line 756
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mIsSubscribed:Z

    goto :goto_0

    .line 757
    :cond_3
    iget-boolean v0, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mIsSubscribed:Z

    if-eqz v0, :cond_1

    if-nez p1, :cond_1

    .line 758
    iget-object v0, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    const-string v1, "com.sec.android.allshare.event.EVENT_DEVICE_SUBSCRIBE"

    iget-object v2, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    invoke-virtual {v2}, Lcom/samsung/android/allshare/DeviceImpl;->getBundle()Landroid/os/Bundle;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mEventHandler:Lcom/samsung/android/allshare/AllShareEventHandler;

    invoke-interface {v0, v1, v2, v3}, Lcom/samsung/android/allshare/IAllShareConnector;->unsubscribeAllShareEvent(Ljava/lang/String;Landroid/os/Bundle;Lcom/samsung/android/allshare/AllShareEventHandler;)V

    .line 760
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mIsSubscribed:Z

    goto :goto_0
.end method

.method public setResponseListener(Lcom/samsung/android/allshare/media/ImageViewer$IImageViewerResponseListener;)V
    .locals 3
    .param p1, "l"    # Lcom/samsung/android/allshare/media/ImageViewer$IImageViewerResponseListener;

    .prologue
    .line 739
    const-string v0, "ImageViewerImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setResponseListener to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/allshare/DLog;->d_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 740
    iput-object p1, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mResponseListener:Lcom/samsung/android/allshare/media/ImageViewer$IImageViewerResponseListener;

    .line 741
    return-void
.end method

.method public show(Lcom/samsung/android/allshare/Item;Lcom/samsung/android/allshare/media/ContentInfo;)V
    .locals 17
    .param p1, "ai"    # Lcom/samsung/android/allshare/Item;
    .param p2, "ci"    # Lcom/samsung/android/allshare/media/ContentInfo;

    .prologue
    .line 365
    const-string v14, "ImageViewerImpl"

    const-string v15, "show() is called"

    invoke-static {v14, v15}, Lcom/samsung/android/allshare/DLog;->i_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 366
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/allshare/ImageViewerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    if-eqz v14, :cond_0

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/allshare/ImageViewerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    invoke-interface {v14}, Lcom/samsung/android/allshare/IAllShareConnector;->isAllShareServiceConnected()Z

    move-result v14

    if-nez v14, :cond_2

    .line 367
    :cond_0
    const-string v14, "ImageViewerImpl"

    const-string v15, "show : SERVICE_NOT_CONNECTED"

    invoke-static {v14, v15}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 368
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/allshare/ImageViewerImpl;->mResponseListener:Lcom/samsung/android/allshare/media/ImageViewer$IImageViewerResponseListener;

    sget-object v15, Lcom/samsung/android/allshare/ERROR;->SERVICE_NOT_CONNECTED:Lcom/samsung/android/allshare/ERROR;

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-interface {v14, v0, v1, v15}, Lcom/samsung/android/allshare/media/ImageViewer$IImageViewerResponseListener;->onShowResponseReceived(Lcom/samsung/android/allshare/Item;Lcom/samsung/android/allshare/media/ContentInfo;Lcom/samsung/android/allshare/ERROR;)V

    .line 488
    :cond_1
    :goto_0
    return-void

    .line 372
    :cond_2
    if-nez p1, :cond_3

    .line 373
    const-string v14, "ImageViewerImpl"

    const-string v15, "show : ai == null"

    invoke-static {v14, v15}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 374
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/allshare/ImageViewerImpl;->mResponseListener:Lcom/samsung/android/allshare/media/ImageViewer$IImageViewerResponseListener;

    if-eqz v14, :cond_1

    .line 375
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/allshare/ImageViewerImpl;->mResponseListener:Lcom/samsung/android/allshare/media/ImageViewer$IImageViewerResponseListener;

    sget-object v15, Lcom/samsung/android/allshare/ERROR;->INVALID_ARGUMENT:Lcom/samsung/android/allshare/ERROR;

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-interface {v14, v0, v1, v15}, Lcom/samsung/android/allshare/media/ImageViewer$IImageViewerResponseListener;->onShowResponseReceived(Lcom/samsung/android/allshare/Item;Lcom/samsung/android/allshare/media/ContentInfo;Lcom/samsung/android/allshare/ERROR;)V

    goto :goto_0

    .line 379
    :cond_3
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/allshare/Item;->getType()Lcom/samsung/android/allshare/Item$MediaType;

    move-result-object v11

    .line 380
    .local v11, "type":Lcom/samsung/android/allshare/Item$MediaType;
    if-nez v11, :cond_4

    .line 381
    const-string v14, "ImageViewerImpl"

    const-string v15, "show : type == null"

    invoke-static {v14, v15}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 382
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/allshare/ImageViewerImpl;->mResponseListener:Lcom/samsung/android/allshare/media/ImageViewer$IImageViewerResponseListener;

    if-eqz v14, :cond_1

    .line 383
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/allshare/ImageViewerImpl;->mResponseListener:Lcom/samsung/android/allshare/media/ImageViewer$IImageViewerResponseListener;

    sget-object v15, Lcom/samsung/android/allshare/ERROR;->INVALID_ARGUMENT:Lcom/samsung/android/allshare/ERROR;

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-interface {v14, v0, v1, v15}, Lcom/samsung/android/allshare/media/ImageViewer$IImageViewerResponseListener;->onShowResponseReceived(Lcom/samsung/android/allshare/Item;Lcom/samsung/android/allshare/media/ContentInfo;Lcom/samsung/android/allshare/ERROR;)V

    goto :goto_0

    .line 389
    :cond_4
    const/4 v14, 0x0

    move-object/from16 v0, p0

    iput-boolean v14, v0, Lcom/samsung/android/allshare/ImageViewerImpl;->mContentChangedNotified:Z

    .line 392
    sget-object v14, Lcom/samsung/android/allshare/ImageViewerImpl$3;->$SwitchMap$com$samsung$android$allshare$Item$MediaType:[I

    invoke-virtual {v11}, Lcom/samsung/android/allshare/Item$MediaType;->ordinal()I

    move-result v15

    aget v14, v14, v15

    packed-switch v14, :pswitch_data_0

    .line 396
    const-string v14, "ImageViewerImpl"

    const-string v15, "show : Invalid media type"

    invoke-static {v14, v15}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 397
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/allshare/ImageViewerImpl;->mResponseListener:Lcom/samsung/android/allshare/media/ImageViewer$IImageViewerResponseListener;

    if-eqz v14, :cond_1

    .line 398
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/allshare/ImageViewerImpl;->mResponseListener:Lcom/samsung/android/allshare/media/ImageViewer$IImageViewerResponseListener;

    sget-object v15, Lcom/samsung/android/allshare/ERROR;->INVALID_ARGUMENT:Lcom/samsung/android/allshare/ERROR;

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-interface {v14, v0, v1, v15}, Lcom/samsung/android/allshare/media/ImageViewer$IImageViewerResponseListener;->onShowResponseReceived(Lcom/samsung/android/allshare/Item;Lcom/samsung/android/allshare/media/ContentInfo;Lcom/samsung/android/allshare/ERROR;)V

    goto :goto_0

    .line 404
    :pswitch_0
    const-string v5, "MEDIA_SERVER"

    .line 405
    .local v5, "itemConstructorKey":Ljava/lang/String;
    move-object/from16 v0, p1

    instance-of v14, v0, Lcom/sec/android/allshare/iface/IBundleHolder;

    if-eqz v14, :cond_5

    move-object/from16 v14, p1

    .line 406
    check-cast v14, Lcom/sec/android/allshare/iface/IBundleHolder;

    invoke-interface {v14}, Lcom/sec/android/allshare/iface/IBundleHolder;->getBundle()Landroid/os/Bundle;

    move-result-object v2

    .line 407
    .local v2, "bundle":Landroid/os/Bundle;
    const-string v14, "BUNDLE_STRING_ITEM_CONSTRUCTOR_KEY"

    invoke-virtual {v2, v14}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 410
    .end local v2    # "bundle":Landroid/os/Bundle;
    :cond_5
    const-string v14, "MEDIA_SERVER"

    invoke-virtual {v5, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_a

    .line 412
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    .line 413
    .local v13, "uris":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/allshare/Item;->getResourceList()Ljava/util/ArrayList;

    move-result-object v8

    .line 414
    .local v8, "rsrcs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/Item$Resource;>;"
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/allshare/Item;->getThumbnail()Landroid/net/Uri;

    move-result-object v10

    .line 415
    .local v10, "thumb_uri":Landroid/net/Uri;
    if-eqz v10, :cond_6

    .line 416
    invoke-virtual {v10}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 418
    :cond_6
    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :cond_7
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_8

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/android/allshare/Item$Resource;

    .line 419
    .local v7, "rsrc":Lcom/samsung/android/allshare/Item$Resource;
    invoke-virtual {v7}, Lcom/samsung/android/allshare/Item$Resource;->getURI()Landroid/net/Uri;

    move-result-object v6

    .line 420
    .local v6, "rsc_uri":Landroid/net/Uri;
    if-eqz v6, :cond_7

    .line 421
    invoke-virtual {v6}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 424
    .end local v6    # "rsc_uri":Landroid/net/Uri;
    .end local v7    # "rsrc":Lcom/samsung/android/allshare/Item$Resource;
    :cond_8
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/allshare/Item;->getURI()Landroid/net/Uri;

    move-result-object v12

    .line 425
    .local v12, "uri":Landroid/net/Uri;
    if-eqz v12, :cond_9

    .line 426
    invoke-virtual {v12}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 428
    :cond_9
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/allshare/ImageViewerImpl;->mPlayingContentUris:Ljava/util/ArrayList;

    invoke-virtual {v14, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 429
    invoke-direct/range {p0 .. p2}, Lcom/samsung/android/allshare/ImageViewerImpl;->showMediaContent(Lcom/samsung/android/allshare/Item;Lcom/samsung/android/allshare/media/ContentInfo;)V

    goto/16 :goto_0

    .line 431
    .end local v4    # "i$":Ljava/util/Iterator;
    .end local v8    # "rsrcs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/Item$Resource;>;"
    .end local v10    # "thumb_uri":Landroid/net/Uri;
    .end local v12    # "uri":Landroid/net/Uri;
    .end local v13    # "uris":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_a
    const-string v14, "WEB_CONTENT"

    invoke-virtual {v5, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_f

    .line 433
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    .line 434
    .restart local v13    # "uris":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/allshare/Item;->getResourceList()Ljava/util/ArrayList;

    move-result-object v8

    .line 435
    .restart local v8    # "rsrcs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/Item$Resource;>;"
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/allshare/Item;->getThumbnail()Landroid/net/Uri;

    move-result-object v10

    .line 436
    .restart local v10    # "thumb_uri":Landroid/net/Uri;
    if-eqz v10, :cond_b

    .line 437
    invoke-virtual {v10}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 439
    :cond_b
    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .restart local v4    # "i$":Ljava/util/Iterator;
    :cond_c
    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_d

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/android/allshare/Item$Resource;

    .line 440
    .restart local v7    # "rsrc":Lcom/samsung/android/allshare/Item$Resource;
    invoke-virtual {v7}, Lcom/samsung/android/allshare/Item$Resource;->getURI()Landroid/net/Uri;

    move-result-object v6

    .line 441
    .restart local v6    # "rsc_uri":Landroid/net/Uri;
    if-eqz v6, :cond_c

    .line 442
    invoke-virtual {v6}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 445
    .end local v6    # "rsc_uri":Landroid/net/Uri;
    .end local v7    # "rsrc":Lcom/samsung/android/allshare/Item$Resource;
    :cond_d
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/allshare/Item;->getURI()Landroid/net/Uri;

    move-result-object v12

    .line 446
    .restart local v12    # "uri":Landroid/net/Uri;
    if-eqz v12, :cond_e

    .line 447
    invoke-virtual {v12}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 449
    :cond_e
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/allshare/ImageViewerImpl;->mPlayingContentUris:Ljava/util/ArrayList;

    invoke-virtual {v14, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 450
    invoke-direct/range {p0 .. p2}, Lcom/samsung/android/allshare/ImageViewerImpl;->showWebContent(Lcom/samsung/android/allshare/Item;Lcom/samsung/android/allshare/media/ContentInfo;)V

    goto/16 :goto_0

    .line 452
    .end local v4    # "i$":Ljava/util/Iterator;
    .end local v8    # "rsrcs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/Item$Resource;>;"
    .end local v10    # "thumb_uri":Landroid/net/Uri;
    .end local v12    # "uri":Landroid/net/Uri;
    .end local v13    # "uris":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_f
    const-string v14, "LOCAL_CONTENT"

    invoke-virtual {v5, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_14

    .line 453
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/allshare/Item;->getURI()Landroid/net/Uri;

    move-result-object v12

    .line 454
    .restart local v12    # "uri":Landroid/net/Uri;
    if-nez v12, :cond_10

    .line 455
    const-string v14, "ImageViewerImpl"

    const-string v15, "show : uri == null"

    invoke-static {v14, v15}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 456
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/allshare/ImageViewerImpl;->mResponseListener:Lcom/samsung/android/allshare/media/ImageViewer$IImageViewerResponseListener;

    if-eqz v14, :cond_1

    .line 457
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/allshare/ImageViewerImpl;->mResponseListener:Lcom/samsung/android/allshare/media/ImageViewer$IImageViewerResponseListener;

    sget-object v15, Lcom/samsung/android/allshare/ERROR;->INVALID_ARGUMENT:Lcom/samsung/android/allshare/ERROR;

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-interface {v14, v0, v1, v15}, Lcom/samsung/android/allshare/media/ImageViewer$IImageViewerResponseListener;->onShowResponseReceived(Lcom/samsung/android/allshare/Item;Lcom/samsung/android/allshare/media/ContentInfo;Lcom/samsung/android/allshare/ERROR;)V

    goto/16 :goto_0

    .line 461
    :cond_10
    invoke-virtual {v12}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v9

    .line 462
    .local v9, "scheme":Ljava/lang/String;
    const-string v14, "ImageViewerImpl"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "show : scheme = "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/samsung/android/allshare/DLog;->d_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 463
    const-string v14, "content"

    invoke-virtual {v9, v14}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v14

    if-eqz v14, :cond_11

    .line 464
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/allshare/Item;->getURI()Landroid/net/Uri;

    move-result-object v14

    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/samsung/android/allshare/ImageViewerImpl;->parseUriFilePath(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v3

    .line 465
    .local v3, "filePath":Ljava/lang/String;
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    .line 466
    .restart local v13    # "uris":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v13, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 467
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/allshare/ImageViewerImpl;->mPlayingContentUris:Ljava/util/ArrayList;

    invoke-virtual {v14, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 468
    invoke-direct/range {p0 .. p2}, Lcom/samsung/android/allshare/ImageViewerImpl;->showLocalContentContentScheme(Lcom/samsung/android/allshare/Item;Lcom/samsung/android/allshare/media/ContentInfo;)V

    goto/16 :goto_0

    .line 469
    .end local v3    # "filePath":Ljava/lang/String;
    .end local v13    # "uris":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_11
    const-string v14, "file"

    invoke-virtual {v9, v14}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v14

    if-eqz v14, :cond_13

    .line 470
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 471
    .restart local v2    # "bundle":Landroid/os/Bundle;
    move-object/from16 v0, p1

    instance-of v14, v0, Lcom/sec/android/allshare/iface/IBundleHolder;

    if-eqz v14, :cond_12

    move-object/from16 v14, p1

    .line 472
    check-cast v14, Lcom/sec/android/allshare/iface/IBundleHolder;

    invoke-interface {v14}, Lcom/sec/android/allshare/iface/IBundleHolder;->getBundle()Landroid/os/Bundle;

    move-result-object v2

    .line 474
    :cond_12
    const-string v14, "BUNDLE_STRING_FILEPATH"

    invoke-virtual {v2, v14}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 475
    .restart local v3    # "filePath":Ljava/lang/String;
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    .line 476
    .restart local v13    # "uris":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v13, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 477
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/allshare/ImageViewerImpl;->mPlayingContentUris:Ljava/util/ArrayList;

    invoke-virtual {v14, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 478
    invoke-direct/range {p0 .. p2}, Lcom/samsung/android/allshare/ImageViewerImpl;->showLocalContentFileScheme(Lcom/samsung/android/allshare/Item;Lcom/samsung/android/allshare/media/ContentInfo;)V

    goto/16 :goto_0

    .line 480
    .end local v2    # "bundle":Landroid/os/Bundle;
    .end local v3    # "filePath":Ljava/lang/String;
    .end local v13    # "uris":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_13
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/allshare/ImageViewerImpl;->mResponseListener:Lcom/samsung/android/allshare/media/ImageViewer$IImageViewerResponseListener;

    if-eqz v14, :cond_1

    .line 481
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/allshare/ImageViewerImpl;->mResponseListener:Lcom/samsung/android/allshare/media/ImageViewer$IImageViewerResponseListener;

    sget-object v15, Lcom/samsung/android/allshare/ERROR;->INVALID_ARGUMENT:Lcom/samsung/android/allshare/ERROR;

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-interface {v14, v0, v1, v15}, Lcom/samsung/android/allshare/media/ImageViewer$IImageViewerResponseListener;->onShowResponseReceived(Lcom/samsung/android/allshare/Item;Lcom/samsung/android/allshare/media/ContentInfo;Lcom/samsung/android/allshare/ERROR;)V

    goto/16 :goto_0

    .line 487
    .end local v9    # "scheme":Ljava/lang/String;
    .end local v12    # "uri":Landroid/net/Uri;
    :cond_14
    const-string v14, "ImageViewerImpl"

    const-string v15, "show : fail - INVALID ARG "

    invoke-static {v14, v15}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 392
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public stop()V
    .locals 5

    .prologue
    .line 687
    iget-object v2, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    invoke-interface {v2}, Lcom/samsung/android/allshare/IAllShareConnector;->isAllShareServiceConnected()Z

    move-result v2

    if-nez v2, :cond_1

    .line 688
    :cond_0
    const-string v2, "ImageViewerImpl"

    const-string v3, "stop : SERVICE_NOT_CONNECTED"

    invoke-static {v2, v3}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 689
    iget-object v2, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mResponseListener:Lcom/samsung/android/allshare/media/ImageViewer$IImageViewerResponseListener;

    sget-object v3, Lcom/samsung/android/allshare/ERROR;->SERVICE_NOT_CONNECTED:Lcom/samsung/android/allshare/ERROR;

    invoke-interface {v2, v3}, Lcom/samsung/android/allshare/media/ImageViewer$IImageViewerResponseListener;->onStopResponseReceived(Lcom/samsung/android/allshare/ERROR;)V

    .line 702
    :goto_0
    return-void

    .line 693
    :cond_1
    new-instance v1, Lcom/sec/android/allshare/iface/CVMessage;

    invoke-direct {v1}, Lcom/sec/android/allshare/iface/CVMessage;-><init>()V

    .line 694
    .local v1, "req_msg":Lcom/sec/android/allshare/iface/CVMessage;
    const-string v2, "com.sec.android.allshare.action.ACTION_IMAGE_VIEWER_STOP"

    invoke-virtual {v1, v2}, Lcom/sec/android/allshare/iface/CVMessage;->setActionID(Ljava/lang/String;)V

    .line 695
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 697
    .local v0, "req_bundle":Landroid/os/Bundle;
    const-string v2, "BUNDLE_STRING_ID"

    invoke-virtual {p0}, Lcom/samsung/android/allshare/ImageViewerImpl;->getID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 699
    invoke-virtual {v1, v0}, Lcom/sec/android/allshare/iface/CVMessage;->setBundle(Landroid/os/Bundle;)V

    .line 700
    iget-object v2, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    iget-object v3, p0, Lcom/samsung/android/allshare/ImageViewerImpl;->mResponseHandler:Lcom/samsung/android/allshare/AllShareResponseHandler;

    invoke-interface {v2, v1, v3}, Lcom/samsung/android/allshare/IAllShareConnector;->requestCVMAsync(Lcom/sec/android/allshare/iface/CVMessage;Lcom/samsung/android/allshare/AllShareResponseHandler;)J

    .line 701
    const-string v2, "ImageViewerImpl"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "stop : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/samsung/android/allshare/ImageViewerImpl;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/allshare/DLog;->i_api(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
