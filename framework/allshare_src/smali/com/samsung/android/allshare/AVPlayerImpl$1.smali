.class Lcom/samsung/android/allshare/AVPlayerImpl$1;
.super Lcom/samsung/android/allshare/AllShareEventHandler;
.source "AVPlayerImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/allshare/AVPlayerImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private mAVStateMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/samsung/android/allshare/AVPlayerImpl;


# direct methods
.method constructor <init>(Lcom/samsung/android/allshare/AVPlayerImpl;Landroid/os/Looper;)V
    .locals 3
    .param p2, "x0"    # Landroid/os/Looper;

    .prologue
    .line 692
    iput-object p1, p0, Lcom/samsung/android/allshare/AVPlayerImpl$1;->this$0:Lcom/samsung/android/allshare/AVPlayerImpl;

    invoke-direct {p0, p2}, Lcom/samsung/android/allshare/AllShareEventHandler;-><init>(Landroid/os/Looper;)V

    .line 693
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/AVPlayerImpl$1;->mAVStateMap:Ljava/util/HashMap;

    .line 695
    iget-object v0, p0, Lcom/samsung/android/allshare/AVPlayerImpl$1;->mAVStateMap:Ljava/util/HashMap;

    const-string v1, "com.sec.android.allshare.event.EVENT_RENDERER_STATE_BUFFERING"

    sget-object v2, Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;->BUFFERING:Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 696
    iget-object v0, p0, Lcom/samsung/android/allshare/AVPlayerImpl$1;->mAVStateMap:Ljava/util/HashMap;

    const-string v1, "com.sec.android.allshare.event.EVENT_RENDERER_STATE_PAUSED"

    sget-object v2, Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;->PAUSED:Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 697
    iget-object v0, p0, Lcom/samsung/android/allshare/AVPlayerImpl$1;->mAVStateMap:Ljava/util/HashMap;

    const-string v1, "com.sec.android.allshare.event.EVENT_RENDERER_STATE_STOPPED"

    sget-object v2, Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;->STOPPED:Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 698
    iget-object v0, p0, Lcom/samsung/android/allshare/AVPlayerImpl$1;->mAVStateMap:Ljava/util/HashMap;

    const-string v1, "com.sec.android.allshare.event.EVENT_RENDERER_STATE_PLAYING"

    sget-object v2, Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;->PLAYING:Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 699
    iget-object v0, p0, Lcom/samsung/android/allshare/AVPlayerImpl$1;->mAVStateMap:Ljava/util/HashMap;

    const-string v1, "com.sec.android.allshare.event.EVENT_RENDERER_STATE_NOMEDIA"

    sget-object v2, Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;->STOPPED:Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 700
    iget-object v0, p0, Lcom/samsung/android/allshare/AVPlayerImpl$1;->mAVStateMap:Ljava/util/HashMap;

    const-string v1, "com.sec.android.allshare.event.EVENT_RENDERER_STATE_CONTENT_CHANGED"

    sget-object v2, Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;->CONTENT_CHANGED:Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 702
    return-void
.end method

.method private isContains(Ljava/lang/String;Ljava/util/ArrayList;)Z
    .locals 4
    .param p1, "currentTrackUri"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .local p2, "playingContentUris":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v2, 0x0

    .line 785
    if-eqz p2, :cond_0

    if-nez p1, :cond_1

    .line 793
    :cond_0
    :goto_0
    return v2

    .line 788
    :cond_1
    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 789
    .local v1, "uri":Ljava/lang/String;
    invoke-virtual {p1, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 790
    const/4 v2, 0x1

    goto :goto_0
.end method

.method private notifyEvent(Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;Lcom/samsung/android/allshare/ERROR;)V
    .locals 3
    .param p1, "state"    # Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;
    .param p2, "error"    # Lcom/samsung/android/allshare/ERROR;

    .prologue
    .line 775
    iget-object v1, p0, Lcom/samsung/android/allshare/AVPlayerImpl$1;->this$0:Lcom/samsung/android/allshare/AVPlayerImpl;

    # getter for: Lcom/samsung/android/allshare/AVPlayerImpl;->mAVPlayerEventListener:Lcom/samsung/android/allshare/media/AVPlayer$IAVPlayerEventListener;
    invoke-static {v1}, Lcom/samsung/android/allshare/AVPlayerImpl;->access$300(Lcom/samsung/android/allshare/AVPlayerImpl;)Lcom/samsung/android/allshare/media/AVPlayer$IAVPlayerEventListener;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 777
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/allshare/AVPlayerImpl$1;->this$0:Lcom/samsung/android/allshare/AVPlayerImpl;

    # getter for: Lcom/samsung/android/allshare/AVPlayerImpl;->mAVPlayerEventListener:Lcom/samsung/android/allshare/media/AVPlayer$IAVPlayerEventListener;
    invoke-static {v1}, Lcom/samsung/android/allshare/AVPlayerImpl;->access$300(Lcom/samsung/android/allshare/AVPlayerImpl;)Lcom/samsung/android/allshare/media/AVPlayer$IAVPlayerEventListener;

    move-result-object v1

    invoke-interface {v1, p1, p2}, Lcom/samsung/android/allshare/media/AVPlayer$IAVPlayerEventListener;->onDeviceChanged(Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;Lcom/samsung/android/allshare/ERROR;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 782
    :cond_0
    :goto_0
    return-void

    .line 778
    :catch_0
    move-exception v0

    .line 779
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "AVPlayerImpl"

    const-string v2, "mEventHandler.notifyEvent Error"

    invoke-static {v1, v2, v0}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method


# virtual methods
.method public handleEventMessage(Lcom/sec/android/allshare/iface/CVMessage;)V
    .locals 10
    .param p1, "cvm"    # Lcom/sec/android/allshare/iface/CVMessage;

    .prologue
    .line 707
    :try_start_0
    sget-object v3, Lcom/samsung/android/allshare/ERROR;->FAIL:Lcom/samsung/android/allshare/ERROR;

    .line 708
    .local v3, "error":Lcom/samsung/android/allshare/ERROR;
    invoke-virtual {p1}, Lcom/sec/android/allshare/iface/CVMessage;->getBundle()Landroid/os/Bundle;

    move-result-object v5

    .line 709
    .local v5, "resBundle":Landroid/os/Bundle;
    const-string v7, "BUNDLE_ENUM_ERROR"

    invoke-virtual {v5, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 710
    .local v4, "errorStr":Ljava/lang/String;
    invoke-static {v4}, Lcom/samsung/android/allshare/ERROR;->stringToEnum(Ljava/lang/String;)Lcom/samsung/android/allshare/ERROR;

    move-result-object v3

    .line 712
    iget-object v7, p0, Lcom/samsung/android/allshare/AVPlayerImpl$1;->mAVStateMap:Ljava/util/HashMap;

    invoke-virtual {p1}, Lcom/sec/android/allshare/iface/CVMessage;->getActionID()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;

    .line 713
    .local v6, "state":Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;
    if-nez v6, :cond_0

    .line 714
    sget-object v6, Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;->UNKNOWN:Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;

    .line 717
    :cond_0
    sget-object v7, Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;->CONTENT_CHANGED:Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;

    invoke-virtual {v6, v7}, Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_9

    .line 718
    const-string v7, "BUNDLE_STRING_APP_ITEM_ID"

    invoke-virtual {v5, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 720
    .local v0, "currentTrackUri":Ljava/lang/String;
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_2

    .line 721
    :cond_1
    const-string v7, "AVPlayerImpl"

    const-string v8, "do not notify CONTENT_CHANGED event, currentTrackUri is null"

    invoke-static {v7, v8}, Lcom/samsung/android/allshare/DLog;->d_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 772
    .end local v0    # "currentTrackUri":Ljava/lang/String;
    .end local v3    # "error":Lcom/samsung/android/allshare/ERROR;
    .end local v4    # "errorStr":Ljava/lang/String;
    .end local v5    # "resBundle":Landroid/os/Bundle;
    .end local v6    # "state":Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;
    :goto_0
    return-void

    .line 725
    .restart local v0    # "currentTrackUri":Ljava/lang/String;
    .restart local v3    # "error":Lcom/samsung/android/allshare/ERROR;
    .restart local v4    # "errorStr":Ljava/lang/String;
    .restart local v5    # "resBundle":Landroid/os/Bundle;
    .restart local v6    # "state":Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;
    :cond_2
    iget-object v7, p0, Lcom/samsung/android/allshare/AVPlayerImpl$1;->this$0:Lcom/samsung/android/allshare/AVPlayerImpl;

    # getter for: Lcom/samsung/android/allshare/AVPlayerImpl;->mContentChangedNotified:Z
    invoke-static {v7}, Lcom/samsung/android/allshare/AVPlayerImpl;->access$000(Lcom/samsung/android/allshare/AVPlayerImpl;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 726
    const-string v7, "AVPlayerImpl"

    const-string v8, "do not notify CONTENT_CHANGED event yet"

    invoke-static {v7, v8}, Lcom/samsung/android/allshare/DLog;->d_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 727
    iget-object v7, p0, Lcom/samsung/android/allshare/AVPlayerImpl$1;->this$0:Lcom/samsung/android/allshare/AVPlayerImpl;

    # setter for: Lcom/samsung/android/allshare/AVPlayerImpl;->mCurrentDMRUri:Ljava/lang/String;
    invoke-static {v7, v0}, Lcom/samsung/android/allshare/AVPlayerImpl;->access$102(Lcom/samsung/android/allshare/AVPlayerImpl;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 766
    .end local v0    # "currentTrackUri":Ljava/lang/String;
    .end local v3    # "error":Lcom/samsung/android/allshare/ERROR;
    .end local v4    # "errorStr":Ljava/lang/String;
    .end local v5    # "resBundle":Landroid/os/Bundle;
    .end local v6    # "state":Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;
    :catch_0
    move-exception v1

    .line 767
    .local v1, "e":Ljava/lang/Exception;
    const-string v7, "AVPlayerImpl"

    const-string v8, "handleEventMessage Fail to notify event : Exception"

    invoke-static {v7, v8}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 731
    .end local v1    # "e":Ljava/lang/Exception;
    .restart local v0    # "currentTrackUri":Ljava/lang/String;
    .restart local v3    # "error":Lcom/samsung/android/allshare/ERROR;
    .restart local v4    # "errorStr":Ljava/lang/String;
    .restart local v5    # "resBundle":Landroid/os/Bundle;
    .restart local v6    # "state":Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;
    :cond_3
    :try_start_1
    iget-object v7, p0, Lcom/samsung/android/allshare/AVPlayerImpl$1;->this$0:Lcom/samsung/android/allshare/AVPlayerImpl;

    # getter for: Lcom/samsung/android/allshare/AVPlayerImpl;->mCurrentDMRUri:Ljava/lang/String;
    invoke-static {v7}, Lcom/samsung/android/allshare/AVPlayerImpl;->access$100(Lcom/samsung/android/allshare/AVPlayerImpl;)Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_4

    iget-object v7, p0, Lcom/samsung/android/allshare/AVPlayerImpl$1;->this$0:Lcom/samsung/android/allshare/AVPlayerImpl;

    # getter for: Lcom/samsung/android/allshare/AVPlayerImpl;->mCurrentDMRUri:Ljava/lang/String;
    invoke-static {v7}, Lcom/samsung/android/allshare/AVPlayerImpl;->access$100(Lcom/samsung/android/allshare/AVPlayerImpl;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 732
    const-string v7, "AVPlayerImpl"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "do not notify CONTENT_CHANGED event, mCurrentDMRUri is same as currentTrackUri "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/samsung/android/allshare/DLog;->d_api(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Error; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 768
    .end local v0    # "currentTrackUri":Ljava/lang/String;
    .end local v3    # "error":Lcom/samsung/android/allshare/ERROR;
    .end local v4    # "errorStr":Ljava/lang/String;
    .end local v5    # "resBundle":Landroid/os/Bundle;
    .end local v6    # "state":Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;
    :catch_1
    move-exception v2

    .line 769
    .local v2, "err":Ljava/lang/Error;
    const-string v7, "AVPlayerImpl"

    const-string v8, "handleEventMessage Error"

    invoke-static {v7, v8, v2}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Error;)V

    goto :goto_0

    .line 735
    .end local v2    # "err":Ljava/lang/Error;
    .restart local v0    # "currentTrackUri":Ljava/lang/String;
    .restart local v3    # "error":Lcom/samsung/android/allshare/ERROR;
    .restart local v4    # "errorStr":Ljava/lang/String;
    .restart local v5    # "resBundle":Landroid/os/Bundle;
    .restart local v6    # "state":Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;
    :cond_4
    :try_start_2
    const-string v7, "AVPlayerImpl"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "CONTENT_CHANGED, mCurrentDMRUri : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/samsung/android/allshare/AVPlayerImpl$1;->this$0:Lcom/samsung/android/allshare/AVPlayerImpl;

    # getter for: Lcom/samsung/android/allshare/AVPlayerImpl;->mCurrentDMRUri:Ljava/lang/String;
    invoke-static {v9}, Lcom/samsung/android/allshare/AVPlayerImpl;->access$100(Lcom/samsung/android/allshare/AVPlayerImpl;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "  currentTrackUri : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/samsung/android/allshare/DLog;->d_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 736
    iget-object v7, p0, Lcom/samsung/android/allshare/AVPlayerImpl$1;->this$0:Lcom/samsung/android/allshare/AVPlayerImpl;

    # getter for: Lcom/samsung/android/allshare/AVPlayerImpl;->mCurrentDMRUri:Ljava/lang/String;
    invoke-static {v7}, Lcom/samsung/android/allshare/AVPlayerImpl;->access$100(Lcom/samsung/android/allshare/AVPlayerImpl;)Ljava/lang/String;

    move-result-object v7

    if-nez v7, :cond_5

    .line 737
    const-string v7, "AVPlayerImpl"

    const-string v8, "do not notify CONTENT_CHANGED event, mCurrentDMRUri is null"

    invoke-static {v7, v8}, Lcom/samsung/android/allshare/DLog;->d_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 738
    iget-object v7, p0, Lcom/samsung/android/allshare/AVPlayerImpl$1;->this$0:Lcom/samsung/android/allshare/AVPlayerImpl;

    # setter for: Lcom/samsung/android/allshare/AVPlayerImpl;->mCurrentDMRUri:Ljava/lang/String;
    invoke-static {v7, v0}, Lcom/samsung/android/allshare/AVPlayerImpl;->access$102(Lcom/samsung/android/allshare/AVPlayerImpl;Ljava/lang/String;)Ljava/lang/String;

    goto/16 :goto_0

    .line 742
    :cond_5
    iget-object v7, p0, Lcom/samsung/android/allshare/AVPlayerImpl$1;->this$0:Lcom/samsung/android/allshare/AVPlayerImpl;

    # setter for: Lcom/samsung/android/allshare/AVPlayerImpl;->mCurrentDMRUri:Ljava/lang/String;
    invoke-static {v7, v0}, Lcom/samsung/android/allshare/AVPlayerImpl;->access$102(Lcom/samsung/android/allshare/AVPlayerImpl;Ljava/lang/String;)Ljava/lang/String;

    .line 744
    iget-object v7, p0, Lcom/samsung/android/allshare/AVPlayerImpl$1;->this$0:Lcom/samsung/android/allshare/AVPlayerImpl;

    # getter for: Lcom/samsung/android/allshare/AVPlayerImpl;->mPlayingContentUris:Ljava/util/ArrayList;
    invoke-static {v7}, Lcom/samsung/android/allshare/AVPlayerImpl;->access$200(Lcom/samsung/android/allshare/AVPlayerImpl;)Ljava/util/ArrayList;

    move-result-object v7

    if-eqz v7, :cond_6

    iget-object v7, p0, Lcom/samsung/android/allshare/AVPlayerImpl$1;->this$0:Lcom/samsung/android/allshare/AVPlayerImpl;

    # getter for: Lcom/samsung/android/allshare/AVPlayerImpl;->mPlayingContentUris:Ljava/util/ArrayList;
    invoke-static {v7}, Lcom/samsung/android/allshare/AVPlayerImpl;->access$200(Lcom/samsung/android/allshare/AVPlayerImpl;)Ljava/util/ArrayList;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_7

    .line 745
    :cond_6
    const-string v7, "AVPlayerImpl"

    const-string v8, "do not notify CONTENT_CHANGED event, mPlayingContentUris is null"

    invoke-static {v7, v8}, Lcom/samsung/android/allshare/DLog;->d_api(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 750
    :cond_7
    iget-object v7, p0, Lcom/samsung/android/allshare/AVPlayerImpl$1;->this$0:Lcom/samsung/android/allshare/AVPlayerImpl;

    # getter for: Lcom/samsung/android/allshare/AVPlayerImpl;->mPlayingContentUris:Ljava/util/ArrayList;
    invoke-static {v7}, Lcom/samsung/android/allshare/AVPlayerImpl;->access$200(Lcom/samsung/android/allshare/AVPlayerImpl;)Ljava/util/ArrayList;

    move-result-object v7

    invoke-direct {p0, v0, v7}, Lcom/samsung/android/allshare/AVPlayerImpl$1;->isContains(Ljava/lang/String;Ljava/util/ArrayList;)Z

    move-result v7

    if-eqz v7, :cond_8

    .line 751
    const-string v7, "AVPlayerImpl"

    const-string v8, "handleEventMessage: this is playing content."

    invoke-static {v7, v8}, Lcom/samsung/android/allshare/DLog;->d_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 752
    const-string v7, "AVPlayerImpl"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "do not notify CONTENT_CHANGED event, this is my="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/samsung/android/allshare/DLog;->i_api(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 758
    :cond_8
    iget-object v7, p0, Lcom/samsung/android/allshare/AVPlayerImpl$1;->this$0:Lcom/samsung/android/allshare/AVPlayerImpl;

    const/4 v8, 0x1

    # setter for: Lcom/samsung/android/allshare/AVPlayerImpl;->mContentChangedNotified:Z
    invoke-static {v7, v8}, Lcom/samsung/android/allshare/AVPlayerImpl;->access$002(Lcom/samsung/android/allshare/AVPlayerImpl;Z)Z

    .line 760
    const-string v7, "AVPlayerImpl"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Notify CONTENT_CHANGED event, mPlayingContentUris["

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/samsung/android/allshare/AVPlayerImpl$1;->this$0:Lcom/samsung/android/allshare/AVPlayerImpl;

    # getter for: Lcom/samsung/android/allshare/AVPlayerImpl;->mPlayingContentUris:Ljava/util/ArrayList;
    invoke-static {v9}, Lcom/samsung/android/allshare/AVPlayerImpl;->access$200(Lcom/samsung/android/allshare/AVPlayerImpl;)Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "] vs currentTrackUri["

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "]"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 764
    .end local v0    # "currentTrackUri":Ljava/lang/String;
    :cond_9
    invoke-direct {p0, v6, v3}, Lcom/samsung/android/allshare/AVPlayerImpl$1;->notifyEvent(Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;Lcom/samsung/android/allshare/ERROR;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Error; {:try_start_2 .. :try_end_2} :catch_1

    goto/16 :goto_0
.end method
