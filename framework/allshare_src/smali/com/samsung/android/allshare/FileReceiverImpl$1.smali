.class Lcom/samsung/android/allshare/FileReceiverImpl$1;
.super Ljava/lang/Object;
.source "FileReceiverImpl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/allshare/FileReceiverImpl;->cancel(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/allshare/FileReceiverImpl;

.field final synthetic val$sessionId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/samsung/android/allshare/FileReceiverImpl;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 152
    iput-object p1, p0, Lcom/samsung/android/allshare/FileReceiverImpl$1;->this$0:Lcom/samsung/android/allshare/FileReceiverImpl;

    iput-object p2, p0, Lcom/samsung/android/allshare/FileReceiverImpl$1;->val$sessionId:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 155
    iget-object v2, p0, Lcom/samsung/android/allshare/FileReceiverImpl$1;->val$sessionId:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 156
    const/4 v0, 0x0

    .line 157
    .local v0, "listener":Lcom/samsung/android/allshare/file/FileReceiver$IFileReceiverReceiveResponseListener;
    iget-object v2, p0, Lcom/samsung/android/allshare/FileReceiverImpl$1;->this$0:Lcom/samsung/android/allshare/FileReceiverImpl;

    iget-object v3, p0, Lcom/samsung/android/allshare/FileReceiverImpl$1;->val$sessionId:Ljava/lang/String;

    # invokes: Lcom/samsung/android/allshare/FileReceiverImpl;->getSessionKeyInfoMap(Ljava/lang/String;)Lcom/samsung/android/allshare/FileReceiverImpl$SessionInfo;
    invoke-static {v2, v3}, Lcom/samsung/android/allshare/FileReceiverImpl;->access$000(Lcom/samsung/android/allshare/FileReceiverImpl;Ljava/lang/String;)Lcom/samsung/android/allshare/FileReceiverImpl$SessionInfo;

    move-result-object v1

    .line 158
    .local v1, "tempSessionInfo":Lcom/samsung/android/allshare/FileReceiverImpl$SessionInfo;
    if-nez v1, :cond_1

    .line 159
    const-string v2, "FileReceiverImpl"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Null pointer Error!, sessionId="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/allshare/FileReceiverImpl$1;->val$sessionId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 172
    .end local v0    # "listener":Lcom/samsung/android/allshare/file/FileReceiver$IFileReceiverReceiveResponseListener;
    .end local v1    # "tempSessionInfo":Lcom/samsung/android/allshare/FileReceiverImpl$SessionInfo;
    :cond_0
    :goto_0
    return-void

    .line 163
    .restart local v0    # "listener":Lcom/samsung/android/allshare/file/FileReceiver$IFileReceiverReceiveResponseListener;
    .restart local v1    # "tempSessionInfo":Lcom/samsung/android/allshare/FileReceiverImpl$SessionInfo;
    :cond_1
    invoke-virtual {v1}, Lcom/samsung/android/allshare/FileReceiverImpl$SessionInfo;->getResponseListener()Lcom/samsung/android/allshare/file/FileReceiver$IFileReceiverReceiveResponseListener;

    move-result-object v0

    .line 166
    if-eqz v0, :cond_0

    .line 167
    iget-object v2, p0, Lcom/samsung/android/allshare/FileReceiverImpl$1;->this$0:Lcom/samsung/android/allshare/FileReceiverImpl;

    # getter for: Lcom/samsung/android/allshare/FileReceiverImpl;->mReceiver:Lcom/samsung/android/allshare/file/FileReceiver;
    invoke-static {v2}, Lcom/samsung/android/allshare/FileReceiverImpl;->access$100(Lcom/samsung/android/allshare/FileReceiverImpl;)Lcom/samsung/android/allshare/file/FileReceiver;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/allshare/FileReceiverImpl$1;->val$sessionId:Ljava/lang/String;

    sget-object v4, Lcom/samsung/android/allshare/ERROR;->FAIL:Lcom/samsung/android/allshare/ERROR;

    invoke-interface {v0, v2, v3, v4}, Lcom/samsung/android/allshare/file/FileReceiver$IFileReceiverReceiveResponseListener;->onCancelResponseReceived(Lcom/samsung/android/allshare/file/FileReceiver;Ljava/lang/String;Lcom/samsung/android/allshare/ERROR;)V

    .line 169
    iget-object v2, p0, Lcom/samsung/android/allshare/FileReceiverImpl$1;->this$0:Lcom/samsung/android/allshare/FileReceiverImpl;

    iget-object v3, p0, Lcom/samsung/android/allshare/FileReceiverImpl$1;->val$sessionId:Ljava/lang/String;

    # invokes: Lcom/samsung/android/allshare/FileReceiverImpl;->removeSessionKeyInfoMap(Ljava/lang/String;)V
    invoke-static {v2, v3}, Lcom/samsung/android/allshare/FileReceiverImpl;->access$200(Lcom/samsung/android/allshare/FileReceiverImpl;Ljava/lang/String;)V

    goto :goto_0
.end method
