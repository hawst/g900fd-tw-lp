.class final Lcom/samsung/android/allshare/FileReceiverImpl;
.super Lcom/samsung/android/allshare/file/FileReceiver;
.source "FileReceiverImpl.java"

# interfaces
.implements Lcom/sec/android/allshare/iface/IBundleHolder;
.implements Lcom/sec/android/allshare/iface/IHandlerHolder;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/allshare/FileReceiverImpl$INotifyProgressEvent;,
        Lcom/samsung/android/allshare/FileReceiverImpl$SessionInfo;
    }
.end annotation


# static fields
.field private static final TAG_CLASS:Ljava/lang/String; = "FileReceiverImpl"


# instance fields
.field private mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

.field private mCancelReq:Z

.field private mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

.field private mEventHandler:Lcom/samsung/android/allshare/AllShareEventHandler;

.field private mIsSubscribed:Z

.field private mProgressEventListener:Lcom/samsung/android/allshare/file/FileReceiver$IFileReceiverProgressUpdateEventListener;

.field private mReceiver:Lcom/samsung/android/allshare/file/FileReceiver;

.field private mRespHandler:Lcom/samsung/android/allshare/AllShareResponseHandler;

.field private mSessionKeyInfoMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/allshare/FileReceiverImpl$SessionInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mTimeKeyInfoMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/allshare/FileReceiverImpl$SessionInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/samsung/android/allshare/IAllShareConnector;Lcom/samsung/android/allshare/DeviceImpl;)V
    .locals 4
    .param p1, "connector"    # Lcom/samsung/android/allshare/IAllShareConnector;
    .param p2, "deviceImpl"    # Lcom/samsung/android/allshare/DeviceImpl;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 103
    invoke-direct {p0}, Lcom/samsung/android/allshare/file/FileReceiver;-><init>()V

    .line 36
    iput-object v2, p0, Lcom/samsung/android/allshare/FileReceiverImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    .line 38
    iput-object v2, p0, Lcom/samsung/android/allshare/FileReceiverImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    .line 41
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/allshare/FileReceiverImpl;->mTimeKeyInfoMap:Ljava/util/HashMap;

    .line 44
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/allshare/FileReceiverImpl;->mSessionKeyInfoMap:Ljava/util/HashMap;

    .line 46
    iput-object v2, p0, Lcom/samsung/android/allshare/FileReceiverImpl;->mReceiver:Lcom/samsung/android/allshare/file/FileReceiver;

    .line 49
    iput-boolean v3, p0, Lcom/samsung/android/allshare/FileReceiverImpl;->mCancelReq:Z

    .line 51
    iput-boolean v3, p0, Lcom/samsung/android/allshare/FileReceiverImpl;->mIsSubscribed:Z

    .line 317
    new-instance v1, Lcom/samsung/android/allshare/FileReceiverImpl$2;

    invoke-static {}, Lcom/samsung/android/allshare/ServiceConnector;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/samsung/android/allshare/FileReceiverImpl$2;-><init>(Lcom/samsung/android/allshare/FileReceiverImpl;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/samsung/android/allshare/FileReceiverImpl;->mRespHandler:Lcom/samsung/android/allshare/AllShareResponseHandler;

    .line 486
    new-instance v1, Lcom/samsung/android/allshare/FileReceiverImpl$3;

    invoke-static {}, Lcom/samsung/android/allshare/ServiceConnector;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/samsung/android/allshare/FileReceiverImpl$3;-><init>(Lcom/samsung/android/allshare/FileReceiverImpl;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/samsung/android/allshare/FileReceiverImpl;->mEventHandler:Lcom/samsung/android/allshare/AllShareEventHandler;

    .line 578
    new-instance v1, Lcom/samsung/android/allshare/FileReceiverImpl$4;

    invoke-direct {v1, p0}, Lcom/samsung/android/allshare/FileReceiverImpl$4;-><init>(Lcom/samsung/android/allshare/FileReceiverImpl;)V

    iput-object v1, p0, Lcom/samsung/android/allshare/FileReceiverImpl;->mProgressEventListener:Lcom/samsung/android/allshare/file/FileReceiver$IFileReceiverProgressUpdateEventListener;

    .line 104
    if-nez p1, :cond_0

    .line 106
    const-string v1, "FileReceiverImpl"

    const-string v2, "Connection FAIL: AllShare Service Connector does not exist"

    invoke-static {v1, v2}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    :goto_0
    return-void

    .line 110
    :cond_0
    invoke-interface {p1}, Lcom/samsung/android/allshare/IAllShareConnector;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 111
    .local v0, "cr":Landroid/content/ContentResolver;
    if-eqz v0, :cond_1

    .line 112
    const-string v1, "FileReceiverImpl"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ContentResolver : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/allshare/DLog;->d_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    :cond_1
    iput-object p2, p0, Lcom/samsung/android/allshare/FileReceiverImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    .line 114
    iput-object p1, p0, Lcom/samsung/android/allshare/FileReceiverImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    .line 117
    iget-object v1, p0, Lcom/samsung/android/allshare/FileReceiverImpl;->mProgressEventListener:Lcom/samsung/android/allshare/file/FileReceiver$IFileReceiverProgressUpdateEventListener;

    invoke-direct {p0, v1}, Lcom/samsung/android/allshare/FileReceiverImpl;->setProgressUpdateEventListener(Lcom/samsung/android/allshare/file/FileReceiver$IFileReceiverProgressUpdateEventListener;)V

    .line 118
    iput-object p0, p0, Lcom/samsung/android/allshare/FileReceiverImpl;->mReceiver:Lcom/samsung/android/allshare/file/FileReceiver;

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/samsung/android/allshare/FileReceiverImpl;Ljava/lang/String;)Lcom/samsung/android/allshare/FileReceiverImpl$SessionInfo;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/FileReceiverImpl;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/FileReceiverImpl;->getSessionKeyInfoMap(Ljava/lang/String;)Lcom/samsung/android/allshare/FileReceiverImpl$SessionInfo;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/android/allshare/FileReceiverImpl;)Lcom/samsung/android/allshare/file/FileReceiver;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/FileReceiverImpl;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/samsung/android/allshare/FileReceiverImpl;->mReceiver:Lcom/samsung/android/allshare/file/FileReceiver;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/android/allshare/FileReceiverImpl;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/FileReceiverImpl;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/FileReceiverImpl;->removeSessionKeyInfoMap(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$300(Lcom/samsung/android/allshare/FileReceiverImpl;Ljava/lang/String;)Lcom/samsung/android/allshare/FileReceiverImpl$SessionInfo;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/FileReceiverImpl;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/FileReceiverImpl;->getTimeKeyInfoMap(Ljava/lang/String;)Lcom/samsung/android/allshare/FileReceiverImpl$SessionInfo;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$400(Lcom/samsung/android/allshare/FileReceiverImpl;Ljava/lang/String;Lcom/samsung/android/allshare/FileReceiverImpl$SessionInfo;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/FileReceiverImpl;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Lcom/samsung/android/allshare/FileReceiverImpl$SessionInfo;

    .prologue
    .line 33
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/allshare/FileReceiverImpl;->putSessionKeyInfoMap(Ljava/lang/String;Lcom/samsung/android/allshare/FileReceiverImpl$SessionInfo;)V

    return-void
.end method

.method static synthetic access$500(Lcom/samsung/android/allshare/FileReceiverImpl;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/FileReceiverImpl;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/FileReceiverImpl;->removeTimeKeyInfoMap(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$600(Lcom/samsung/android/allshare/FileReceiverImpl;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/FileReceiverImpl;

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/samsung/android/allshare/FileReceiverImpl;->isCancelRequest()Z

    move-result v0

    return v0
.end method

.method static synthetic access$700(Lcom/samsung/android/allshare/FileReceiverImpl;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/FileReceiverImpl;

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/samsung/android/allshare/FileReceiverImpl;->releaseCancelRequest()V

    return-void
.end method

.method static synthetic access$800(Lcom/samsung/android/allshare/FileReceiverImpl;)Lcom/samsung/android/allshare/file/FileReceiver$IFileReceiverProgressUpdateEventListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/FileReceiverImpl;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/samsung/android/allshare/FileReceiverImpl;->mProgressEventListener:Lcom/samsung/android/allshare/file/FileReceiver$IFileReceiverProgressUpdateEventListener;

    return-object v0
.end method

.method private getSessionKeyInfoMap(Ljava/lang/String;)Lcom/samsung/android/allshare/FileReceiverImpl$SessionInfo;
    .locals 3
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 217
    const-string v0, "FileReceiverImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getSessionKeyInfoMap() called. key : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "size : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/allshare/FileReceiverImpl;->mSessionKeyInfoMap:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/allshare/DLog;->d_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 219
    iget-object v0, p0, Lcom/samsung/android/allshare/FileReceiverImpl;->mSessionKeyInfoMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/allshare/FileReceiverImpl$SessionInfo;

    return-object v0
.end method

.method private getTimeKeyInfoMap(Ljava/lang/String;)Lcom/samsung/android/allshare/FileReceiverImpl$SessionInfo;
    .locals 3
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 238
    const-string v0, "FileReceiverImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getTimeKeyInfoMap() called. key : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/allshare/DLog;->d_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 239
    iget-object v0, p0, Lcom/samsung/android/allshare/FileReceiverImpl;->mTimeKeyInfoMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/allshare/FileReceiverImpl$SessionInfo;

    return-object v0
.end method

.method private isCancelRequest()Z
    .locals 1

    .prologue
    .line 204
    iget-boolean v0, p0, Lcom/samsung/android/allshare/FileReceiverImpl;->mCancelReq:Z

    return v0
.end method

.method private putSessionKeyInfoMap(Ljava/lang/String;Lcom/samsung/android/allshare/FileReceiverImpl$SessionInfo;)V
    .locals 4
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "sessionInfo"    # Lcom/samsung/android/allshare/FileReceiverImpl$SessionInfo;

    .prologue
    .line 209
    const-string v1, "FileReceiverImpl"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "putSessionKeyInfoMap() called. key : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", count : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p2}, Lcom/samsung/android/allshare/FileReceiverImpl$SessionInfo;->getCount()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/allshare/DLog;->d_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 211
    new-instance v0, Lcom/samsung/android/allshare/FileReceiverImpl$SessionInfo;

    invoke-virtual {p2}, Lcom/samsung/android/allshare/FileReceiverImpl$SessionInfo;->getCount()I

    move-result v1

    invoke-virtual {p2}, Lcom/samsung/android/allshare/FileReceiverImpl$SessionInfo;->getResponseListener()Lcom/samsung/android/allshare/file/FileReceiver$IFileReceiverReceiveResponseListener;

    move-result-object v2

    invoke-virtual {p2}, Lcom/samsung/android/allshare/FileReceiverImpl$SessionInfo;->getEventListener()Lcom/samsung/android/allshare/file/FileReceiver$IFileReceiverProgressUpdateEventListener;

    move-result-object v3

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/samsung/android/allshare/FileReceiverImpl$SessionInfo;-><init>(Lcom/samsung/android/allshare/FileReceiverImpl;ILcom/samsung/android/allshare/file/FileReceiver$IFileReceiverReceiveResponseListener;Lcom/samsung/android/allshare/file/FileReceiver$IFileReceiverProgressUpdateEventListener;)V

    .line 213
    .local v0, "sessionKeyInfo":Lcom/samsung/android/allshare/FileReceiverImpl$SessionInfo;
    iget-object v1, p0, Lcom/samsung/android/allshare/FileReceiverImpl;->mSessionKeyInfoMap:Ljava/util/HashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 214
    return-void
.end method

.method private putTimeKeyInfoMap(Ljava/lang/String;Lcom/samsung/android/allshare/FileReceiverImpl$SessionInfo;)V
    .locals 4
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "sessionInfo"    # Lcom/samsung/android/allshare/FileReceiverImpl$SessionInfo;

    .prologue
    .line 230
    const-string v1, "FileReceiverImpl"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "putTimeKeyInfoMap() called. key : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", count : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p2}, Lcom/samsung/android/allshare/FileReceiverImpl$SessionInfo;->getCount()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/allshare/DLog;->d_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 232
    new-instance v0, Lcom/samsung/android/allshare/FileReceiverImpl$SessionInfo;

    invoke-virtual {p2}, Lcom/samsung/android/allshare/FileReceiverImpl$SessionInfo;->getCount()I

    move-result v1

    invoke-virtual {p2}, Lcom/samsung/android/allshare/FileReceiverImpl$SessionInfo;->getResponseListener()Lcom/samsung/android/allshare/file/FileReceiver$IFileReceiverReceiveResponseListener;

    move-result-object v2

    invoke-virtual {p2}, Lcom/samsung/android/allshare/FileReceiverImpl$SessionInfo;->getEventListener()Lcom/samsung/android/allshare/file/FileReceiver$IFileReceiverProgressUpdateEventListener;

    move-result-object v3

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/samsung/android/allshare/FileReceiverImpl$SessionInfo;-><init>(Lcom/samsung/android/allshare/FileReceiverImpl;ILcom/samsung/android/allshare/file/FileReceiver$IFileReceiverReceiveResponseListener;Lcom/samsung/android/allshare/file/FileReceiver$IFileReceiverProgressUpdateEventListener;)V

    .line 234
    .local v0, "timeKeyInfo":Lcom/samsung/android/allshare/FileReceiverImpl$SessionInfo;
    iget-object v1, p0, Lcom/samsung/android/allshare/FileReceiverImpl;->mTimeKeyInfoMap:Ljava/util/HashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 235
    return-void
.end method

.method private releaseCancelRequest()V
    .locals 1

    .prologue
    .line 200
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/allshare/FileReceiverImpl;->mCancelReq:Z

    .line 201
    return-void
.end method

.method private removeSessionKeyInfoMap(Ljava/lang/String;)V
    .locals 3
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 223
    iget-object v0, p0, Lcom/samsung/android/allshare/FileReceiverImpl;->mSessionKeyInfoMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 224
    const-string v0, "FileReceiverImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "removeSessionKeyInfoMap() called. key : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "size : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/allshare/FileReceiverImpl;->mTimeKeyInfoMap:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/allshare/DLog;->d_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 226
    return-void
.end method

.method private removeTimeKeyInfoMap(Ljava/lang/String;)V
    .locals 3
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/samsung/android/allshare/FileReceiverImpl;->mTimeKeyInfoMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 244
    const-string v0, "FileReceiverImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "removeTimeKeyInfoMap() called. key : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "size : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/allshare/FileReceiverImpl;->mTimeKeyInfoMap:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/allshare/DLog;->d_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 246
    return-void
.end method

.method private setCancelRequest()V
    .locals 1

    .prologue
    .line 196
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/allshare/FileReceiverImpl;->mCancelReq:Z

    .line 197
    return-void
.end method

.method private setProgressUpdateEventListener(Lcom/samsung/android/allshare/file/FileReceiver$IFileReceiverProgressUpdateEventListener;)V
    .locals 4
    .param p1, "listener"    # Lcom/samsung/android/allshare/file/FileReceiver$IFileReceiverProgressUpdateEventListener;

    .prologue
    .line 249
    iget-object v0, p0, Lcom/samsung/android/allshare/FileReceiverImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/allshare/FileReceiverImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    invoke-interface {v0}, Lcom/samsung/android/allshare/IAllShareConnector;->isAllShareServiceConnected()Z

    move-result v0

    if-nez v0, :cond_1

    .line 265
    :cond_0
    :goto_0
    return-void

    .line 252
    :cond_1
    iput-object p1, p0, Lcom/samsung/android/allshare/FileReceiverImpl;->mProgressEventListener:Lcom/samsung/android/allshare/file/FileReceiver$IFileReceiverProgressUpdateEventListener;

    .line 254
    iget-boolean v0, p0, Lcom/samsung/android/allshare/FileReceiverImpl;->mIsSubscribed:Z

    if-nez v0, :cond_2

    if-eqz p1, :cond_2

    .line 255
    iget-object v0, p0, Lcom/samsung/android/allshare/FileReceiverImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    const-string v1, "com.sec.android.allshare.event.EVENT_DEVICE_SUBSCRIBE"

    iget-object v2, p0, Lcom/samsung/android/allshare/FileReceiverImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    invoke-virtual {v2}, Lcom/samsung/android/allshare/DeviceImpl;->getBundle()Landroid/os/Bundle;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/allshare/FileReceiverImpl;->mEventHandler:Lcom/samsung/android/allshare/AllShareEventHandler;

    invoke-interface {v0, v1, v2, v3}, Lcom/samsung/android/allshare/IAllShareConnector;->subscribeAllShareEvent(Ljava/lang/String;Landroid/os/Bundle;Lcom/samsung/android/allshare/AllShareEventHandler;)Z

    .line 257
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/allshare/FileReceiverImpl;->mIsSubscribed:Z

    goto :goto_0

    .line 258
    :cond_2
    iget-boolean v0, p0, Lcom/samsung/android/allshare/FileReceiverImpl;->mIsSubscribed:Z

    if-eqz v0, :cond_0

    if-nez p1, :cond_0

    .line 259
    iget-object v0, p0, Lcom/samsung/android/allshare/FileReceiverImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    const-string v1, "com.sec.android.allshare.event.EVENT_DEVICE_SUBSCRIBE"

    iget-object v2, p0, Lcom/samsung/android/allshare/FileReceiverImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    invoke-virtual {v2}, Lcom/samsung/android/allshare/DeviceImpl;->getBundle()Landroid/os/Bundle;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/allshare/FileReceiverImpl;->mEventHandler:Lcom/samsung/android/allshare/AllShareEventHandler;

    invoke-interface {v0, v1, v2, v3}, Lcom/samsung/android/allshare/IAllShareConnector;->unsubscribeAllShareEvent(Ljava/lang/String;Landroid/os/Bundle;Lcom/samsung/android/allshare/AllShareEventHandler;)V

    .line 261
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/allshare/FileReceiverImpl;->mIsSubscribed:Z

    goto :goto_0
.end method


# virtual methods
.method public cancel(Ljava/lang/String;)V
    .locals 6
    .param p1, "sessionId"    # Ljava/lang/String;

    .prologue
    .line 151
    iget-object v2, p0, Lcom/samsung/android/allshare/FileReceiverImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/android/allshare/FileReceiverImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    invoke-interface {v2}, Lcom/samsung/android/allshare/IAllShareConnector;->isAllShareServiceConnected()Z

    move-result v2

    if-nez v2, :cond_2

    .line 152
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/allshare/FileReceiverImpl;->mRespHandler:Lcom/samsung/android/allshare/AllShareResponseHandler;

    new-instance v3, Lcom/samsung/android/allshare/FileReceiverImpl$1;

    invoke-direct {v3, p0, p1}, Lcom/samsung/android/allshare/FileReceiverImpl$1;-><init>(Lcom/samsung/android/allshare/FileReceiverImpl;Ljava/lang/String;)V

    const-wide/16 v4, 0x1

    invoke-virtual {v2, v3, v4, v5}, Lcom/samsung/android/allshare/AllShareResponseHandler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 192
    :cond_1
    :goto_0
    return-void

    .line 178
    :cond_2
    if-eqz p1, :cond_1

    .line 179
    new-instance v1, Lcom/sec/android/allshare/iface/CVMessage;

    invoke-direct {v1}, Lcom/sec/android/allshare/iface/CVMessage;-><init>()V

    .line 180
    .local v1, "req_msg":Lcom/sec/android/allshare/iface/CVMessage;
    const-string v2, "com.sec.android.allshare.action.ACTION_FILE_ARRAYLIST_RECEIVER_CANCEL"

    invoke-virtual {v1, v2}, Lcom/sec/android/allshare/iface/CVMessage;->setActionID(Ljava/lang/String;)V

    .line 182
    invoke-virtual {v1}, Lcom/sec/android/allshare/iface/CVMessage;->getBundle()Landroid/os/Bundle;

    move-result-object v0

    .line 183
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v2, "BUNDLE_STRING_ID"

    iget-object v3, p0, Lcom/samsung/android/allshare/FileReceiverImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    invoke-virtual {v3}, Lcom/samsung/android/allshare/DeviceImpl;->getID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 184
    const-string v2, "BUNDLE_STRING_SESSIONID"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 185
    const-string v2, "FileReceiverImpl"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "sessionID : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/allshare/DLog;->d_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 187
    iget-object v2, p0, Lcom/samsung/android/allshare/FileReceiverImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    iget-object v3, p0, Lcom/samsung/android/allshare/FileReceiverImpl;->mRespHandler:Lcom/samsung/android/allshare/AllShareResponseHandler;

    invoke-interface {v2, v1, v3}, Lcom/samsung/android/allshare/IAllShareConnector;->requestCVMAsync(Lcom/sec/android/allshare/iface/CVMessage;Lcom/samsung/android/allshare/AllShareResponseHandler;)J

    .line 190
    invoke-direct {p0}, Lcom/samsung/android/allshare/FileReceiverImpl;->setCancelRequest()V

    goto :goto_0
.end method

.method public getBundle()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 719
    iget-object v0, p0, Lcom/samsung/android/allshare/FileReceiverImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    if-nez v0, :cond_0

    .line 720
    const/4 v0, 0x0

    .line 722
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/allshare/FileReceiverImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/DeviceImpl;->getBundle()Landroid/os/Bundle;

    move-result-object v0

    goto :goto_0
.end method

.method public getDeviceDomain()Lcom/samsung/android/allshare/Device$DeviceDomain;
    .locals 1

    .prologue
    .line 269
    iget-object v0, p0, Lcom/samsung/android/allshare/FileReceiverImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/DeviceImpl;->getDeviceDomain()Lcom/samsung/android/allshare/Device$DeviceDomain;

    move-result-object v0

    return-object v0
.end method

.method public getDeviceType()Lcom/samsung/android/allshare/Device$DeviceType;
    .locals 1

    .prologue
    .line 274
    iget-object v0, p0, Lcom/samsung/android/allshare/FileReceiverImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/DeviceImpl;->getDeviceType()Lcom/samsung/android/allshare/Device$DeviceType;

    move-result-object v0

    return-object v0
.end method

.method public getID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 279
    iget-object v0, p0, Lcom/samsung/android/allshare/FileReceiverImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/DeviceImpl;->getID()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getIPAddress()Ljava/lang/String;
    .locals 1

    .prologue
    .line 291
    iget-object v0, p0, Lcom/samsung/android/allshare/FileReceiverImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/DeviceImpl;->getIPAddress()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getIPAdress()Ljava/lang/String;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 286
    invoke-virtual {p0}, Lcom/samsung/android/allshare/FileReceiverImpl;->getIPAddress()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getIcon()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 296
    iget-object v0, p0, Lcom/samsung/android/allshare/FileReceiverImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/DeviceImpl;->getIcon()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public getIconList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/Icon;",
            ">;"
        }
    .end annotation

    .prologue
    .line 301
    iget-object v0, p0, Lcom/samsung/android/allshare/FileReceiverImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    if-nez v0, :cond_0

    .line 302
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 304
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/allshare/FileReceiverImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/DeviceImpl;->getIconList()Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_0
.end method

.method public getModelName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 309
    iget-object v0, p0, Lcom/samsung/android/allshare/FileReceiverImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/DeviceImpl;->getModelName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getNIC()Ljava/lang/String;
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcom/samsung/android/allshare/FileReceiverImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    if-nez v0, :cond_0

    .line 124
    const-string v0, ""

    .line 126
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/allshare/FileReceiverImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/DeviceImpl;->getNIC()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 314
    iget-object v0, p0, Lcom/samsung/android/allshare/FileReceiverImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/DeviceImpl;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getP2pMacAddress()Ljava/lang/String;
    .locals 1

    .prologue
    .line 748
    const-string v0, ""

    return-object v0
.end method

.method public isSeekableOnPaused()Z
    .locals 1

    .prologue
    .line 735
    const/4 v0, 0x0

    return v0
.end method

.method public isWholeHomeAudio()Z
    .locals 1

    .prologue
    .line 741
    const/4 v0, 0x0

    return v0
.end method

.method public receive(Ljava/util/ArrayList;Ljava/lang/String;Lcom/samsung/android/allshare/file/FileReceiver$IFileReceiverReceiveResponseListener;Lcom/samsung/android/allshare/file/FileReceiver$IFileReceiverProgressUpdateEventListener;)V
    .locals 17
    .param p2, "senderName"    # Ljava/lang/String;
    .param p3, "responseListener"    # Lcom/samsung/android/allshare/file/FileReceiver$IFileReceiverReceiveResponseListener;
    .param p4, "eventListener"    # Lcom/samsung/android/allshare/file/FileReceiver$IFileReceiverProgressUpdateEventListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/io/File;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/samsung/android/allshare/file/FileReceiver$IFileReceiverReceiveResponseListener;",
            "Lcom/samsung/android/allshare/file/FileReceiver$IFileReceiverProgressUpdateEventListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 672
    .local p1, "filelist":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/allshare/FileReceiverImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    if-eqz v4, :cond_0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/allshare/FileReceiverImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    invoke-interface {v4}, Lcom/samsung/android/allshare/IAllShareConnector;->isAllShareServiceConnected()Z

    move-result v4

    if-nez v4, :cond_1

    .line 673
    :cond_0
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/allshare/FileReceiverImpl;->mReceiver:Lcom/samsung/android/allshare/file/FileReceiver;

    const-string v6, ""

    sget-object v9, Lcom/samsung/android/allshare/ERROR;->FAIL:Lcom/samsung/android/allshare/ERROR;

    move-object/from16 v4, p3

    move-object/from16 v7, p1

    move-object/from16 v8, p2

    invoke-interface/range {v4 .. v9}, Lcom/samsung/android/allshare/file/FileReceiver$IFileReceiverReceiveResponseListener;->onReceiveResponseReceived(Lcom/samsung/android/allshare/file/FileReceiver;Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/String;Lcom/samsung/android/allshare/ERROR;)V

    .line 715
    :goto_0
    return-void

    .line 678
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 679
    :cond_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/allshare/FileReceiverImpl;->mRespHandler:Lcom/samsung/android/allshare/AllShareResponseHandler;

    new-instance v5, Lcom/samsung/android/allshare/FileReceiverImpl$5;

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    invoke-direct {v5, v0, v1, v2, v3}, Lcom/samsung/android/allshare/FileReceiverImpl$5;-><init>(Lcom/samsung/android/allshare/FileReceiverImpl;Lcom/samsung/android/allshare/file/FileReceiver$IFileReceiverReceiveResponseListener;Ljava/util/ArrayList;Ljava/lang/String;)V

    const-wide/16 v6, 0x1

    invoke-virtual {v4, v5, v6, v7}, Lcom/samsung/android/allshare/AllShareResponseHandler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 691
    :cond_3
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 693
    .local v12, "filePathList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v13

    .local v13, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/io/File;

    .line 694
    .local v11, "file":Ljava/io/File;
    invoke-virtual {v11}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v12, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 697
    .end local v11    # "file":Ljava/io/File;
    :cond_4
    new-instance v14, Lcom/sec/android/allshare/iface/CVMessage;

    invoke-direct {v14}, Lcom/sec/android/allshare/iface/CVMessage;-><init>()V

    .line 698
    .local v14, "req_msg":Lcom/sec/android/allshare/iface/CVMessage;
    const-string v4, "com.sec.android.allshare.action.ACTION_FILE_ARRAYLIST_RECEIVER_RECEIVE"

    invoke-virtual {v14, v4}, Lcom/sec/android/allshare/iface/CVMessage;->setActionID(Ljava/lang/String;)V

    .line 700
    invoke-virtual {v14}, Lcom/sec/android/allshare/iface/CVMessage;->getBundle()Landroid/os/Bundle;

    move-result-object v10

    .line 701
    .local v10, "bundle":Landroid/os/Bundle;
    const-string v4, "BUNDLE_STRING_ID"

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/allshare/FileReceiverImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    invoke-virtual {v5}, Lcom/samsung/android/allshare/DeviceImpl;->getID()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v10, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 702
    const-string v4, "BUNDLE_STRING_NAME"

    move-object/from16 v0, p2

    invoke-virtual {v10, v4, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 703
    const-string v4, "BUNDLE_STRING_ARRAYLIST_FILE_PATH"

    invoke-virtual {v10, v4, v12}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 705
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v16

    .line 706
    .local v16, "timeKey":Ljava/lang/String;
    const-string v4, "BUNDLE_STRING_UNIQUEKEY"

    move-object/from16 v0, v16

    invoke-virtual {v10, v4, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 709
    new-instance v15, Lcom/samsung/android/allshare/FileReceiverImpl$SessionInfo;

    invoke-virtual {v12}, Ljava/util/ArrayList;->size()I

    move-result v4

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    move-object/from16 v2, p4

    invoke-direct {v15, v0, v4, v1, v2}, Lcom/samsung/android/allshare/FileReceiverImpl$SessionInfo;-><init>(Lcom/samsung/android/allshare/FileReceiverImpl;ILcom/samsung/android/allshare/file/FileReceiver$IFileReceiverReceiveResponseListener;Lcom/samsung/android/allshare/file/FileReceiver$IFileReceiverProgressUpdateEventListener;)V

    .line 711
    .local v15, "tempSessionInfo":Lcom/samsung/android/allshare/FileReceiverImpl$SessionInfo;
    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v1, v15}, Lcom/samsung/android/allshare/FileReceiverImpl;->putTimeKeyInfoMap(Ljava/lang/String;Lcom/samsung/android/allshare/FileReceiverImpl$SessionInfo;)V

    .line 713
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/allshare/FileReceiverImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/allshare/FileReceiverImpl;->mRespHandler:Lcom/samsung/android/allshare/AllShareResponseHandler;

    invoke-interface {v4, v14, v5}, Lcom/samsung/android/allshare/IAllShareConnector;->requestCVMAsync(Lcom/sec/android/allshare/iface/CVMessage;Lcom/samsung/android/allshare/AllShareResponseHandler;)J

    goto/16 :goto_0
.end method

.method public removeEventHandler()V
    .locals 4

    .prologue
    .line 727
    iget-object v0, p0, Lcom/samsung/android/allshare/FileReceiverImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    const-string v1, "com.sec.android.allshare.event.EVENT_DEVICE_SUBSCRIBE"

    invoke-virtual {p0}, Lcom/samsung/android/allshare/FileReceiverImpl;->getBundle()Landroid/os/Bundle;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/allshare/FileReceiverImpl;->mEventHandler:Lcom/samsung/android/allshare/AllShareEventHandler;

    invoke-interface {v0, v1, v2, v3}, Lcom/samsung/android/allshare/IAllShareConnector;->unsubscribeAllShareEvent(Ljava/lang/String;Landroid/os/Bundle;Lcom/samsung/android/allshare/AllShareEventHandler;)V

    .line 729
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/allshare/FileReceiverImpl;->mIsSubscribed:Z

    .line 730
    return-void
.end method
