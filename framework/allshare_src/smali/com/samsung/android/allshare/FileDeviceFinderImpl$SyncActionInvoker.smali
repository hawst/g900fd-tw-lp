.class Lcom/samsung/android/allshare/FileDeviceFinderImpl$SyncActionInvoker;
.super Ljava/lang/Object;
.source "FileDeviceFinderImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/allshare/FileDeviceFinderImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SyncActionInvoker"
.end annotation


# instance fields
.field private mMessage:Lcom/sec/android/allshare/iface/CVMessage;

.field final synthetic this$0:Lcom/samsung/android/allshare/FileDeviceFinderImpl;


# direct methods
.method private constructor <init>(Lcom/samsung/android/allshare/FileDeviceFinderImpl;)V
    .locals 1

    .prologue
    .line 390
    iput-object p1, p0, Lcom/samsung/android/allshare/FileDeviceFinderImpl$SyncActionInvoker;->this$0:Lcom/samsung/android/allshare/FileDeviceFinderImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 388
    new-instance v0, Lcom/sec/android/allshare/iface/CVMessage;

    invoke-direct {v0}, Lcom/sec/android/allshare/iface/CVMessage;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/FileDeviceFinderImpl$SyncActionInvoker;->mMessage:Lcom/sec/android/allshare/iface/CVMessage;

    .line 391
    return-void
.end method

.method private constructor <init>(Lcom/samsung/android/allshare/FileDeviceFinderImpl;Ljava/lang/String;)V
    .locals 1
    .param p2, "action_id"    # Ljava/lang/String;

    .prologue
    .line 393
    iput-object p1, p0, Lcom/samsung/android/allshare/FileDeviceFinderImpl$SyncActionInvoker;->this$0:Lcom/samsung/android/allshare/FileDeviceFinderImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 388
    new-instance v0, Lcom/sec/android/allshare/iface/CVMessage;

    invoke-direct {v0}, Lcom/sec/android/allshare/iface/CVMessage;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/FileDeviceFinderImpl$SyncActionInvoker;->mMessage:Lcom/sec/android/allshare/iface/CVMessage;

    .line 394
    iget-object v0, p0, Lcom/samsung/android/allshare/FileDeviceFinderImpl$SyncActionInvoker;->mMessage:Lcom/sec/android/allshare/iface/CVMessage;

    invoke-virtual {v0, p2}, Lcom/sec/android/allshare/iface/CVMessage;->setActionID(Ljava/lang/String;)V

    .line 395
    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/allshare/FileDeviceFinderImpl;Ljava/lang/String;Lcom/samsung/android/allshare/FileDeviceFinderImpl$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/android/allshare/FileDeviceFinderImpl;
    .param p2, "x1"    # Ljava/lang/String;
    .param p3, "x2"    # Lcom/samsung/android/allshare/FileDeviceFinderImpl$1;

    .prologue
    .line 387
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/allshare/FileDeviceFinderImpl$SyncActionInvoker;-><init>(Lcom/samsung/android/allshare/FileDeviceFinderImpl;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method invoke()Landroid/os/Bundle;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 406
    iget-object v2, p0, Lcom/samsung/android/allshare/FileDeviceFinderImpl$SyncActionInvoker;->this$0:Lcom/samsung/android/allshare/FileDeviceFinderImpl;

    # getter for: Lcom/samsung/android/allshare/FileDeviceFinderImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;
    invoke-static {v2}, Lcom/samsung/android/allshare/FileDeviceFinderImpl;->access$500(Lcom/samsung/android/allshare/FileDeviceFinderImpl;)Lcom/samsung/android/allshare/IAllShareConnector;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/android/allshare/FileDeviceFinderImpl$SyncActionInvoker;->this$0:Lcom/samsung/android/allshare/FileDeviceFinderImpl;

    # getter for: Lcom/samsung/android/allshare/FileDeviceFinderImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;
    invoke-static {v2}, Lcom/samsung/android/allshare/FileDeviceFinderImpl;->access$500(Lcom/samsung/android/allshare/FileDeviceFinderImpl;)Lcom/samsung/android/allshare/IAllShareConnector;

    move-result-object v2

    invoke-interface {v2}, Lcom/samsung/android/allshare/IAllShareConnector;->isAllShareServiceConnected()Z

    move-result v2

    if-nez v2, :cond_1

    .line 414
    :cond_0
    :goto_0
    return-object v1

    .line 410
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/allshare/FileDeviceFinderImpl$SyncActionInvoker;->this$0:Lcom/samsung/android/allshare/FileDeviceFinderImpl;

    # getter for: Lcom/samsung/android/allshare/FileDeviceFinderImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;
    invoke-static {v2}, Lcom/samsung/android/allshare/FileDeviceFinderImpl;->access$500(Lcom/samsung/android/allshare/FileDeviceFinderImpl;)Lcom/samsung/android/allshare/IAllShareConnector;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/allshare/FileDeviceFinderImpl$SyncActionInvoker;->mMessage:Lcom/sec/android/allshare/iface/CVMessage;

    invoke-interface {v2, v3}, Lcom/samsung/android/allshare/IAllShareConnector;->requestCVMSync(Lcom/sec/android/allshare/iface/CVMessage;)Lcom/sec/android/allshare/iface/CVMessage;

    move-result-object v0

    .line 411
    .local v0, "resMessage":Lcom/sec/android/allshare/iface/CVMessage;
    if-eqz v0, :cond_0

    .line 414
    invoke-virtual {v0}, Lcom/sec/android/allshare/iface/CVMessage;->getBundle()Landroid/os/Bundle;

    move-result-object v1

    goto :goto_0
.end method

.method putString(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 398
    iget-object v0, p0, Lcom/samsung/android/allshare/FileDeviceFinderImpl$SyncActionInvoker;->mMessage:Lcom/sec/android/allshare/iface/CVMessage;

    invoke-virtual {v0}, Lcom/sec/android/allshare/iface/CVMessage;->getBundle()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 399
    return-void
.end method

.method putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 402
    .local p2, "value":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/samsung/android/allshare/FileDeviceFinderImpl$SyncActionInvoker;->mMessage:Lcom/sec/android/allshare/iface/CVMessage;

    invoke-virtual {v0}, Lcom/sec/android/allshare/iface/CVMessage;->getBundle()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 403
    return-void
.end method
