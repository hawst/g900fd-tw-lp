.class Lcom/samsung/android/allshare/ViewController2Impl$1;
.super Lcom/samsung/android/allshare/AllShareResponseHandler;
.source "ViewController2Impl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/allshare/ViewController2Impl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/allshare/ViewController2Impl;


# direct methods
.method constructor <init>(Lcom/samsung/android/allshare/ViewController2Impl;Landroid/os/Looper;)V
    .locals 0
    .param p2, "x0"    # Landroid/os/Looper;

    .prologue
    .line 157
    iput-object p1, p0, Lcom/samsung/android/allshare/ViewController2Impl$1;->this$0:Lcom/samsung/android/allshare/ViewController2Impl;

    invoke-direct {p0, p2}, Lcom/samsung/android/allshare/AllShareResponseHandler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleResponseMessage(Lcom/sec/android/allshare/iface/CVMessage;)V
    .locals 9
    .param p1, "cvm"    # Lcom/sec/android/allshare/iface/CVMessage;

    .prologue
    const/4 v8, 0x1

    .line 161
    invoke-virtual {p1}, Lcom/sec/android/allshare/iface/CVMessage;->getActionID()Ljava/lang/String;

    move-result-object v0

    .line 162
    .local v0, "actionID":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/sec/android/allshare/iface/CVMessage;->getBundle()Landroid/os/Bundle;

    move-result-object v4

    .line 164
    .local v4, "resBundle":Landroid/os/Bundle;
    if-eqz v0, :cond_0

    if-eqz v4, :cond_0

    iget-object v5, p0, Lcom/samsung/android/allshare/ViewController2Impl$1;->this$0:Lcom/samsung/android/allshare/ViewController2Impl;

    # getter for: Lcom/samsung/android/allshare/ViewController2Impl;->mResponseListener:Lcom/samsung/android/allshare/media/ViewController2$IViewController2ResponseListener;
    invoke-static {v5}, Lcom/samsung/android/allshare/ViewController2Impl;->access$000(Lcom/samsung/android/allshare/ViewController2Impl;)Lcom/samsung/android/allshare/media/ViewController2$IViewController2ResponseListener;

    move-result-object v5

    if-nez v5, :cond_1

    .line 191
    :cond_0
    :goto_0
    return-void

    .line 167
    :cond_1
    sget-object v1, Lcom/samsung/android/allshare/ERROR;->FAIL:Lcom/samsung/android/allshare/ERROR;

    .line 168
    .local v1, "error":Lcom/samsung/android/allshare/ERROR;
    const-string v5, "BUNDLE_ENUM_ERROR"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 169
    .local v2, "errorStr":Ljava/lang/String;
    if-eqz v2, :cond_2

    .line 170
    invoke-static {v2}, Lcom/samsung/android/allshare/ERROR;->stringToEnum(Ljava/lang/String;)Lcom/samsung/android/allshare/ERROR;

    move-result-object v1

    .line 173
    :cond_2
    const/4 v3, 0x0

    .line 174
    .local v3, "portNum":I
    const-string v5, "com.sec.android.allshare.action.ACTION_VIEWCONTROLLER_REQUEST_GET_ZOOM_PORT"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-ne v5, v8, :cond_3

    .line 175
    sget-object v5, Lcom/samsung/android/allshare/ERROR;->SUCCESS:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v1, v5}, Lcom/samsung/android/allshare/ERROR;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 176
    const-string v5, "BUNDLE_INT_ZOOM_PORT_NUMBER"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    .line 178
    const-string v5, "ViewController2Impl"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "ZoomPort : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/android/allshare/DLog;->i_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 185
    :cond_3
    iget-object v5, p0, Lcom/samsung/android/allshare/ViewController2Impl$1;->this$0:Lcom/samsung/android/allshare/ViewController2Impl;

    iget-object v6, p0, Lcom/samsung/android/allshare/ViewController2Impl$1;->this$0:Lcom/samsung/android/allshare/ViewController2Impl;

    # getter for: Lcom/samsung/android/allshare/ViewController2Impl;->mIPAddress:Ljava/lang/String;
    invoke-static {v6}, Lcom/samsung/android/allshare/ViewController2Impl;->access$100(Lcom/samsung/android/allshare/ViewController2Impl;)Ljava/lang/String;

    move-result-object v6

    # invokes: Lcom/samsung/android/allshare/ViewController2Impl;->connect(Ljava/lang/String;I)Z
    invoke-static {v5, v6, v3}, Lcom/samsung/android/allshare/ViewController2Impl;->access$200(Lcom/samsung/android/allshare/ViewController2Impl;Ljava/lang/String;I)Z

    move-result v5

    if-ne v5, v8, :cond_5

    .line 186
    iget-object v5, p0, Lcom/samsung/android/allshare/ViewController2Impl$1;->this$0:Lcom/samsung/android/allshare/ViewController2Impl;

    # getter for: Lcom/samsung/android/allshare/ViewController2Impl;->mResponseListener:Lcom/samsung/android/allshare/media/ViewController2$IViewController2ResponseListener;
    invoke-static {v5}, Lcom/samsung/android/allshare/ViewController2Impl;->access$000(Lcom/samsung/android/allshare/ViewController2Impl;)Lcom/samsung/android/allshare/media/ViewController2$IViewController2ResponseListener;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/android/allshare/ViewController2Impl$1;->this$0:Lcom/samsung/android/allshare/ViewController2Impl;

    sget-object v7, Lcom/samsung/android/allshare/ERROR;->SUCCESS:Lcom/samsung/android/allshare/ERROR;

    invoke-interface {v5, v6, v7}, Lcom/samsung/android/allshare/media/ViewController2$IViewController2ResponseListener;->onConnectResponseReceived(Lcom/samsung/android/allshare/media/ViewController2;Lcom/samsung/android/allshare/ERROR;)V

    goto :goto_0

    .line 180
    :cond_4
    iget-object v5, p0, Lcom/samsung/android/allshare/ViewController2Impl$1;->this$0:Lcom/samsung/android/allshare/ViewController2Impl;

    # getter for: Lcom/samsung/android/allshare/ViewController2Impl;->mResponseListener:Lcom/samsung/android/allshare/media/ViewController2$IViewController2ResponseListener;
    invoke-static {v5}, Lcom/samsung/android/allshare/ViewController2Impl;->access$000(Lcom/samsung/android/allshare/ViewController2Impl;)Lcom/samsung/android/allshare/media/ViewController2$IViewController2ResponseListener;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/android/allshare/ViewController2Impl$1;->this$0:Lcom/samsung/android/allshare/ViewController2Impl;

    invoke-interface {v5, v6, v1}, Lcom/samsung/android/allshare/media/ViewController2$IViewController2ResponseListener;->onConnectResponseReceived(Lcom/samsung/android/allshare/media/ViewController2;Lcom/samsung/android/allshare/ERROR;)V

    goto :goto_0

    .line 189
    :cond_5
    iget-object v5, p0, Lcom/samsung/android/allshare/ViewController2Impl$1;->this$0:Lcom/samsung/android/allshare/ViewController2Impl;

    # getter for: Lcom/samsung/android/allshare/ViewController2Impl;->mResponseListener:Lcom/samsung/android/allshare/media/ViewController2$IViewController2ResponseListener;
    invoke-static {v5}, Lcom/samsung/android/allshare/ViewController2Impl;->access$000(Lcom/samsung/android/allshare/ViewController2Impl;)Lcom/samsung/android/allshare/media/ViewController2$IViewController2ResponseListener;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/android/allshare/ViewController2Impl$1;->this$0:Lcom/samsung/android/allshare/ViewController2Impl;

    sget-object v7, Lcom/samsung/android/allshare/ERROR;->FAIL:Lcom/samsung/android/allshare/ERROR;

    invoke-interface {v5, v6, v7}, Lcom/samsung/android/allshare/media/ViewController2$IViewController2ResponseListener;->onConnectResponseReceived(Lcom/samsung/android/allshare/media/ViewController2;Lcom/samsung/android/allshare/ERROR;)V

    goto :goto_0
.end method
