.class final Lcom/samsung/android/allshare/ViewControllerImpl;
.super Lcom/samsung/android/allshare/media/ViewController;
.source "ViewControllerImpl.java"

# interfaces
.implements Lcom/sec/android/allshare/iface/IBundleHolder;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/allshare/ViewControllerImpl$LastChangeEvent;
    }
.end annotation


# static fields
.field private static final EVENT_CONTROL_MULTI_TOUCH_BEGIN:I = 0x25

.field private static final EVENT_CONTROL_MULTI_TOUCH_END:I = 0x26

.field private static final EVENT_CONTROL_MULTI_TOUCH_MOVE:I = 0x27

.field private static final EVENT_CONTROL_TOUCH_GESTURE_MOVE:I = 0xd

.field private static final EVENT_CONTROL_TOUCH_GESTURE_UP:I = 0xc

.field private static final MODE_TOUCH:I = 0x0

.field private static final TAG:Ljava/lang/String; = "ViewControllerImpl"


# instance fields
.field private mAbsZoomRate:F

.field private mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

.field private mAllShareEventHandler:Lcom/samsung/android/allshare/AllShareEventHandler;

.field private mControlEventListener:Lcom/samsung/android/allshare/IAppControlAPI$IControlEventListener;

.field private mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

.field private mDeviceName:Ljava/lang/String;

.field private mEventHandler:Landroid/os/Handler;

.field private mEventListener:Lcom/samsung/android/allshare/media/ViewController$IEventListener;

.field private mIAppComponent:Lcom/samsung/android/allshare/IAppControlAPI;

.field private mIPAddress:Ljava/lang/String;

.field private mImageHeight:F

.field private mImageRatio:F

.field private mImageWidth:F

.field private mIsConnected:Z

.field private mIsSubscribed:Z

.field private mMACAddress:Ljava/lang/String;

.field private mMarginX:I

.field private mMarginY:I

.field private mMobileHeight:F

.field private mMobilePhoneRatio:F

.field private mMobileWidth:F

.field private mOrgCenterX:I

.field private mOrgCenterY:I

.field private mOrgX:I

.field private mOrgY:I

.field private mPrevAbsZoomRate:F

.field private mPrevAngle:I

.field private mRelativeZoomRate:F

.field private mResponseListener:Lcom/samsung/android/allshare/media/ViewController$IResponseListener;

.field private mTvHeight:F

.field private mTvOrgX:F

.field private mTvOrgX0:F

.field private mTvOrgY:F

.field private mTvOrgY0:F

.field private mTvRatio:F

.field private mTvWidth:F

.field private mZoomedImageHeight:F

.field private mZoomedImageWidth:F


# direct methods
.method constructor <init>(Lcom/samsung/android/allshare/IAllShareConnector;Lcom/samsung/android/allshare/DeviceImpl;II)V
    .locals 13
    .param p1, "connector"    # Lcom/samsung/android/allshare/IAllShareConnector;
    .param p2, "deviceImpl"    # Lcom/samsung/android/allshare/DeviceImpl;
    .param p3, "tvWidth"    # I
    .param p4, "tvHeight"    # I

    .prologue
    .line 128
    invoke-direct {p0}, Lcom/samsung/android/allshare/media/ViewController;-><init>()V

    .line 57
    const/4 v8, 0x0

    iput-object v8, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mIAppComponent:Lcom/samsung/android/allshare/IAppControlAPI;

    .line 59
    const/4 v8, 0x0

    iput-object v8, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    .line 61
    const/4 v8, 0x0

    iput-object v8, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mEventListener:Lcom/samsung/android/allshare/media/ViewController$IEventListener;

    .line 63
    const/4 v8, 0x0

    iput-object v8, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mResponseListener:Lcom/samsung/android/allshare/media/ViewController$IResponseListener;

    .line 65
    const/4 v8, 0x0

    iput-object v8, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    .line 67
    const/4 v8, 0x0

    iput-object v8, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mMACAddress:Ljava/lang/String;

    .line 69
    const/4 v8, 0x0

    iput-object v8, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mIPAddress:Ljava/lang/String;

    .line 71
    const/4 v8, 0x0

    iput-object v8, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mDeviceName:Ljava/lang/String;

    .line 73
    const/4 v8, 0x0

    iput v8, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mTvWidth:F

    .line 75
    const/4 v8, 0x0

    iput v8, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mTvHeight:F

    .line 77
    const/4 v8, 0x0

    iput v8, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mImageWidth:F

    .line 79
    const/4 v8, 0x0

    iput v8, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mImageHeight:F

    .line 81
    const/4 v8, 0x0

    iput-boolean v8, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mIsConnected:Z

    .line 83
    const/4 v8, 0x0

    iput-boolean v8, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mIsSubscribed:Z

    .line 85
    const/high16 v8, 0x3f800000    # 1.0f

    iput v8, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mRelativeZoomRate:F

    .line 87
    const/4 v8, 0x0

    iput v8, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mImageRatio:F

    .line 89
    const/4 v8, 0x0

    iput v8, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mMobilePhoneRatio:F

    .line 91
    const/4 v8, 0x0

    iput v8, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mMarginX:I

    .line 93
    const/4 v8, 0x0

    iput v8, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mMarginY:I

    .line 95
    const/4 v8, 0x0

    iput v8, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mOrgCenterX:I

    .line 97
    const/4 v8, 0x0

    iput v8, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mOrgCenterY:I

    .line 99
    const/4 v8, 0x0

    iput v8, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mOrgX:I

    .line 101
    const/4 v8, 0x0

    iput v8, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mOrgY:I

    .line 103
    const/4 v8, 0x0

    iput v8, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mTvOrgX:F

    .line 105
    const/4 v8, 0x0

    iput v8, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mTvOrgY:F

    .line 107
    const/4 v8, 0x0

    iput v8, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mTvOrgX0:F

    .line 109
    const/4 v8, 0x0

    iput v8, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mTvOrgY0:F

    .line 111
    const/4 v8, 0x0

    iput v8, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mTvRatio:F

    .line 113
    const/4 v8, 0x0

    iput v8, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mZoomedImageWidth:F

    .line 115
    const/4 v8, 0x0

    iput v8, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mZoomedImageHeight:F

    .line 117
    const/4 v8, 0x0

    iput v8, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mMobileWidth:F

    .line 119
    const/4 v8, 0x0

    iput v8, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mMobileHeight:F

    .line 121
    const/high16 v8, 0x3f800000    # 1.0f

    iput v8, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mAbsZoomRate:F

    .line 123
    const/high16 v8, 0x3f800000    # 1.0f

    iput v8, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mPrevAbsZoomRate:F

    .line 125
    const/4 v8, 0x0

    iput v8, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mPrevAngle:I

    .line 661
    new-instance v8, Lcom/samsung/android/allshare/ViewControllerImpl$1;

    invoke-static {}, Lcom/samsung/android/allshare/ServiceConnector;->getMainLooper()Landroid/os/Looper;

    move-result-object v9

    invoke-direct {v8, p0, v9}, Lcom/samsung/android/allshare/ViewControllerImpl$1;-><init>(Lcom/samsung/android/allshare/ViewControllerImpl;Landroid/os/Looper;)V

    iput-object v8, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mAllShareEventHandler:Lcom/samsung/android/allshare/AllShareEventHandler;

    .line 685
    new-instance v8, Lcom/samsung/android/allshare/ViewControllerImpl$2;

    invoke-direct {v8, p0}, Lcom/samsung/android/allshare/ViewControllerImpl$2;-><init>(Lcom/samsung/android/allshare/ViewControllerImpl;)V

    iput-object v8, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mControlEventListener:Lcom/samsung/android/allshare/IAppControlAPI$IControlEventListener;

    .line 703
    new-instance v8, Lcom/samsung/android/allshare/ViewControllerImpl$3;

    invoke-direct {v8, p0}, Lcom/samsung/android/allshare/ViewControllerImpl$3;-><init>(Lcom/samsung/android/allshare/ViewControllerImpl;)V

    iput-object v8, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mEventHandler:Landroid/os/Handler;

    .line 129
    if-nez p1, :cond_0

    .line 131
    const-string v8, "ViewControllerImpl"

    const-string v9, "Connection FAIL: AllShare Service Connector does not exist"

    invoke-static {v8, v9}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 196
    :goto_0
    return-void

    .line 135
    :cond_0
    invoke-static {}, Lcom/samsung/android/allshare/ServiceConnector;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 137
    .local v2, "context":Landroid/content/Context;
    if-eqz v2, :cond_2

    .line 139
    const-string v8, "connectivity"

    invoke-virtual {v2, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/ConnectivityManager;

    .line 141
    .local v1, "conManager":Landroid/net/ConnectivityManager;
    const/16 v8, 0xd

    invoke-virtual {v1, v8}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v5

    .line 147
    .local v5, "networkInfo":Landroid/net/NetworkInfo;
    const-string v8, "wifi"

    invoke-virtual {v2, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/net/wifi/WifiManager;

    .line 148
    .local v7, "wifiManager":Landroid/net/wifi/WifiManager;
    invoke-virtual {v7}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v8

    invoke-virtual {v8}, Landroid/net/wifi/WifiInfo;->getMacAddress()Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mMACAddress:Ljava/lang/String;

    .line 150
    if-eqz v5, :cond_2

    invoke-virtual {v5}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v8

    const/4 v9, 0x1

    if-ne v8, v9, :cond_2

    .line 151
    iget-object v8, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mMACAddress:Ljava/lang/String;

    if-eqz v8, :cond_2

    .line 152
    iget-object v8, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mMACAddress:Ljava/lang/String;

    const-string v9, ":"

    invoke-virtual {v8, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    .line 160
    .local v6, "strMACPart":[Ljava/lang/String;
    array-length v8, v6

    const/4 v9, 0x6

    if-lt v8, v9, :cond_1

    .line 161
    const/4 v8, 0x0

    aget-object v8, v6, v8

    const/16 v9, 0x10

    invoke-static {v8, v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v3

    .line 162
    .local v3, "nTempMAC1":I
    const/4 v8, 0x4

    aget-object v8, v6, v8

    const/16 v9, 0x10

    invoke-static {v8, v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v4

    .line 164
    .local v4, "nTempMAC2":I
    or-int/lit8 v3, v3, 0x2

    .line 165
    xor-int/lit16 v4, v4, 0x80

    .line 167
    const/4 v8, 0x0

    const-string v9, "%02x"

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v6, v8

    .line 168
    const/4 v8, 0x4

    const-string v9, "%02x"

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v6, v8

    .line 172
    .end local v3    # "nTempMAC1":I
    .end local v4    # "nTempMAC2":I
    :cond_1
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v9, 0x0

    aget-object v9, v6, v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ":"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const/4 v9, 0x1

    aget-object v9, v6, v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ":"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const/4 v9, 0x2

    aget-object v9, v6, v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ":"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const/4 v9, 0x3

    aget-object v9, v6, v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ":"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const/4 v9, 0x4

    aget-object v9, v6, v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ":"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const/4 v9, 0x5

    aget-object v9, v6, v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mMACAddress:Ljava/lang/String;

    .line 185
    .end local v1    # "conManager":Landroid/net/ConnectivityManager;
    .end local v5    # "networkInfo":Landroid/net/NetworkInfo;
    .end local v6    # "strMACPart":[Ljava/lang/String;
    .end local v7    # "wifiManager":Landroid/net/wifi/WifiManager;
    :cond_2
    iput-object p1, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    .line 187
    new-instance v8, Lcom/samsung/android/allshare/IAppControlAPI;

    invoke-direct {v8}, Lcom/samsung/android/allshare/IAppControlAPI;-><init>()V

    iput-object v8, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mIAppComponent:Lcom/samsung/android/allshare/IAppControlAPI;

    .line 189
    iput-object p2, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    .line 190
    iget-object v8, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    invoke-virtual {v8}, Lcom/samsung/android/allshare/DeviceImpl;->getIPAddress()Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mIPAddress:Ljava/lang/String;

    .line 191
    iget-object v8, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    invoke-virtual {v8}, Lcom/samsung/android/allshare/DeviceImpl;->getName()Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mDeviceName:Ljava/lang/String;

    .line 193
    move/from16 v0, p3

    int-to-float v8, v0

    iput v8, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mTvWidth:F

    .line 194
    move/from16 v0, p4

    int-to-float v8, v0

    iput v8, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mTvHeight:F

    .line 195
    iget v8, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mTvWidth:F

    iget v9, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mTvHeight:F

    div-float/2addr v8, v9

    iput v8, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mTvRatio:F

    goto/16 :goto_0
.end method

.method static synthetic access$000(Lcom/samsung/android/allshare/ViewControllerImpl;)Lcom/samsung/android/allshare/media/ViewController$IEventListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/ViewControllerImpl;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mEventListener:Lcom/samsung/android/allshare/media/ViewController$IEventListener;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/android/allshare/ViewControllerImpl;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/ViewControllerImpl;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mEventHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$202(Lcom/samsung/android/allshare/ViewControllerImpl;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/ViewControllerImpl;
    .param p1, "x1"    # Z

    .prologue
    .line 42
    iput-boolean p1, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mIsConnected:Z

    return p1
.end method

.method static synthetic access$300(Lcom/samsung/android/allshare/ViewControllerImpl;)Lcom/samsung/android/allshare/media/ViewController$IResponseListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/ViewControllerImpl;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mResponseListener:Lcom/samsung/android/allshare/media/ViewController$IResponseListener;

    return-object v0
.end method

.method static synthetic access$400(Lcom/samsung/android/allshare/ViewControllerImpl;)Lcom/samsung/android/allshare/IAppControlAPI;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/ViewControllerImpl;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mIAppComponent:Lcom/samsung/android/allshare/IAppControlAPI;

    return-object v0
.end method

.method private calcMargin()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/high16 v3, 0x40000000    # 2.0f

    .line 457
    iget v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mMobilePhoneRatio:F

    iget v1, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mImageRatio:F

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_0

    .line 458
    iget v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mMobileWidth:F

    iget v1, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mImageWidth:F

    iget v2, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mMobileHeight:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mImageHeight:F

    div-float/2addr v1, v2

    sub-float/2addr v0, v1

    div-float/2addr v0, v3

    float-to-int v0, v0

    iput v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mMarginX:I

    .line 459
    iput v4, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mMarginY:I

    .line 464
    :goto_0
    return-void

    .line 461
    :cond_0
    iput v4, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mMarginX:I

    .line 462
    iget v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mMobileHeight:F

    iget v1, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mImageHeight:F

    iget v2, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mMobileWidth:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mImageWidth:F

    div-float/2addr v1, v2

    sub-float/2addr v0, v1

    div-float/2addr v0, v3

    float-to-int v0, v0

    iput v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mMarginY:I

    goto :goto_0
.end method

.method private calcMobileResolution()V
    .locals 5

    .prologue
    .line 398
    invoke-static {}, Lcom/samsung/android/allshare/ServiceConnector;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 399
    .local v0, "context":Landroid/content/Context;
    if-nez v0, :cond_0

    .line 415
    :goto_0
    return-void

    .line 402
    :cond_0
    const-string v3, "window"

    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/WindowManager;

    invoke-interface {v3}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    .line 409
    .local v1, "display":Landroid/view/Display;
    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2}, Landroid/graphics/Point;-><init>()V

    .line 410
    .local v2, "outSize":Landroid/graphics/Point;
    invoke-virtual {v1, v2}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 411
    iget v3, v2, Landroid/graphics/Point;->x:I

    int-to-float v3, v3

    iput v3, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mMobileWidth:F

    .line 412
    iget v3, v2, Landroid/graphics/Point;->y:I

    int-to-float v3, v3

    iput v3, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mMobileHeight:F

    .line 414
    iget v3, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mMobileWidth:F

    iget v4, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mMobileHeight:F

    div-float/2addr v3, v4

    iput v3, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mMobilePhoneRatio:F

    goto :goto_0
.end method

.method private calcOrgImageResolution(II)V
    .locals 2
    .param p1, "sourceWidth"    # I
    .param p2, "sourceHeight"    # I

    .prologue
    .line 418
    int-to-float v0, p1

    iput v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mImageWidth:F

    .line 419
    int-to-float v0, p2

    iput v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mImageHeight:F

    .line 420
    iget v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mImageWidth:F

    iget v1, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mImageHeight:F

    div-float/2addr v0, v1

    iput v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mImageRatio:F

    .line 421
    return-void
.end method

.method private clacZoomedImageSize()V
    .locals 2

    .prologue
    .line 393
    iget v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mZoomedImageWidth:F

    iget v1, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mRelativeZoomRate:F

    mul-float/2addr v0, v1

    iput v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mZoomedImageWidth:F

    .line 394
    iget v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mZoomedImageHeight:F

    iget v1, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mRelativeZoomRate:F

    mul-float/2addr v0, v1

    iput v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mZoomedImageHeight:F

    .line 395
    return-void
.end method

.method private getMagicRate()F
    .locals 5

    .prologue
    .line 425
    const/4 v1, 0x0

    .line 426
    .local v1, "tempWidth":F
    const/4 v0, 0x0

    .line 428
    .local v0, "magicRate":F
    iget v2, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mTvHeight:F

    iget v3, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mImageHeight:F

    cmpl-float v2, v2, v3

    if-ltz v2, :cond_1

    iget v2, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mTvWidth:F

    iget v3, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mImageWidth:F

    cmpl-float v2, v2, v3

    if-ltz v2, :cond_1

    .line 429
    iget v2, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mMobilePhoneRatio:F

    iget v3, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mImageRatio:F

    cmpl-float v2, v2, v3

    if-ltz v2, :cond_0

    .line 430
    iget v2, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mImageWidth:F

    iget v3, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mMobileHeight:F

    iget v4, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mImageWidth:F

    mul-float/2addr v3, v4

    iget v4, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mImageHeight:F

    div-float/2addr v3, v4

    div-float v0, v2, v3

    .line 451
    :goto_0
    const-string v2, "ViewControllerImpl"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[zoom] magicRate : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/allshare/DLog;->i_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 453
    return v0

    .line 432
    :cond_0
    iget v2, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mImageWidth:F

    iget v3, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mMobileWidth:F

    div-float v0, v2, v3

    goto :goto_0

    .line 433
    :cond_1
    iget v2, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mTvRatio:F

    iget v3, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mImageRatio:F

    cmpl-float v2, v2, v3

    if-ltz v2, :cond_3

    .line 434
    iget v2, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mMobilePhoneRatio:F

    iget v3, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mImageRatio:F

    cmpl-float v2, v2, v3

    if-ltz v2, :cond_2

    .line 435
    iget v2, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mTvHeight:F

    iget v3, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mMobileHeight:F

    div-float v0, v2, v3

    goto :goto_0

    .line 437
    :cond_2
    iget v2, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mTvHeight:F

    iget v3, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mImageWidth:F

    mul-float/2addr v2, v3

    iget v3, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mImageHeight:F

    div-float v1, v2, v3

    .line 438
    iget v2, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mMobileWidth:F

    div-float v0, v1, v2

    goto :goto_0

    .line 442
    :cond_3
    iget v2, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mMobilePhoneRatio:F

    iget v3, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mImageRatio:F

    cmpl-float v2, v2, v3

    if-ltz v2, :cond_4

    .line 443
    iget v2, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mTvWidth:F

    iget v3, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mMobileWidth:F

    div-float v0, v2, v3

    goto :goto_0

    .line 445
    :cond_4
    iget v2, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mTvWidth:F

    iget v3, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mImageHeight:F

    mul-float/2addr v2, v3

    iget v3, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mImageWidth:F

    div-float v1, v2, v3

    .line 446
    iget v2, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mMobileHeight:F

    div-float v0, v1, v2

    goto :goto_0
.end method

.method private initZoom()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 351
    iput v1, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mImageWidth:F

    .line 352
    iput v1, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mImageHeight:F

    .line 353
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mRelativeZoomRate:F

    .line 354
    iput v1, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mImageRatio:F

    .line 355
    iput v1, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mMobilePhoneRatio:F

    .line 356
    iput v2, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mMarginX:I

    .line 357
    iput v2, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mMarginY:I

    .line 358
    iput v2, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mOrgCenterX:I

    .line 359
    iput v2, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mOrgCenterY:I

    .line 360
    iput v2, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mOrgX:I

    .line 361
    iput v2, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mOrgY:I

    .line 362
    iput v1, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mTvOrgX:F

    .line 363
    iput v1, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mTvOrgY:F

    .line 364
    iput v1, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mTvOrgX0:F

    .line 365
    iput v1, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mTvOrgY0:F

    .line 366
    return-void
.end method

.method private initZoomedImageSize(FF)V
    .locals 2
    .param p1, "mobileWidth"    # F
    .param p2, "mobileHeight"    # F

    .prologue
    .line 382
    iget v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mMobilePhoneRatio:F

    iget v1, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mImageRatio:F

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_0

    .line 383
    iget v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mImageRatio:F

    mul-float/2addr v0, p2

    iput v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mZoomedImageWidth:F

    .line 384
    iput p2, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mZoomedImageHeight:F

    .line 390
    :goto_0
    return-void

    .line 387
    :cond_0
    iput p1, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mZoomedImageWidth:F

    .line 388
    iget v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mImageRatio:F

    mul-float/2addr v0, p1

    iput v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mZoomedImageHeight:F

    goto :goto_0
.end method

.method private innerDisconnect()V
    .locals 2

    .prologue
    .line 238
    iget-object v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mIAppComponent:Lcom/samsung/android/allshare/IAppControlAPI;

    if-eqz v0, :cond_0

    .line 240
    iget-object v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mIAppComponent:Lcom/samsung/android/allshare/IAppControlAPI;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/IAppControlAPI;->sendMouseDestroy()V

    .line 241
    iget-object v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mIAppComponent:Lcom/samsung/android/allshare/IAppControlAPI;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/IAppControlAPI;->stopControl()Z

    .line 244
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/allshare/ViewControllerImpl;->isConnected()Z

    move-result v0

    iput-boolean v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mIsConnected:Z

    .line 246
    iget-object v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mEventListener:Lcom/samsung/android/allshare/media/ViewController$IEventListener;

    if-eqz v0, :cond_1

    .line 247
    iget-boolean v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mIsConnected:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 248
    iget-object v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mEventListener:Lcom/samsung/android/allshare/media/ViewController$IEventListener;

    sget-object v1, Lcom/samsung/android/allshare/ERROR;->SUCCESS:Lcom/samsung/android/allshare/ERROR;

    invoke-interface {v0, p0, v1}, Lcom/samsung/android/allshare/media/ViewController$IEventListener;->onDisconnected(Lcom/samsung/android/allshare/media/ViewController;Lcom/samsung/android/allshare/ERROR;)V

    .line 254
    :cond_1
    :goto_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mIsConnected:Z

    .line 255
    return-void

    .line 250
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mEventListener:Lcom/samsung/android/allshare/media/ViewController$IEventListener;

    sget-object v1, Lcom/samsung/android/allshare/ERROR;->SERVICE_NOT_CONNECTED:Lcom/samsung/android/allshare/ERROR;

    invoke-interface {v0, p0, v1}, Lcom/samsung/android/allshare/media/ViewController$IEventListener;->onDisconnected(Lcom/samsung/android/allshare/media/ViewController;Lcom/samsung/android/allshare/ERROR;)V

    goto :goto_0
.end method


# virtual methods
.method public connect()V
    .locals 5

    .prologue
    .line 210
    iget-object v1, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    invoke-interface {v1}, Lcom/samsung/android/allshare/IAllShareConnector;->isAllShareServiceConnected()Z

    move-result v1

    if-nez v1, :cond_2

    .line 211
    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/allshare/ViewControllerImpl;->innerDisconnect()V

    .line 213
    iget-object v1, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mResponseListener:Lcom/samsung/android/allshare/media/ViewController$IResponseListener;

    if-eqz v1, :cond_1

    .line 214
    iget-object v1, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mResponseListener:Lcom/samsung/android/allshare/media/ViewController$IResponseListener;

    sget-object v2, Lcom/samsung/android/allshare/ERROR;->SERVICE_NOT_CONNECTED:Lcom/samsung/android/allshare/ERROR;

    invoke-interface {v1, p0, v2}, Lcom/samsung/android/allshare/media/ViewController$IResponseListener;->onConnectResponseReceived(Lcom/samsung/android/allshare/media/ViewController;Lcom/samsung/android/allshare/ERROR;)V

    .line 235
    :cond_1
    :goto_0
    return-void

    .line 221
    :cond_2
    iget-boolean v1, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mIsConnected:Z

    if-nez v1, :cond_1

    .line 222
    iget-object v1, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mIAppComponent:Lcom/samsung/android/allshare/IAppControlAPI;

    invoke-virtual {v1}, Lcom/samsung/android/allshare/IAppControlAPI;->stopControl()Z

    move-result v0

    .line 223
    .local v0, "bRet":Z
    if-nez v0, :cond_3

    .line 224
    iget-object v1, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mResponseListener:Lcom/samsung/android/allshare/media/ViewController$IResponseListener;

    sget-object v2, Lcom/samsung/android/allshare/ERROR;->FAIL:Lcom/samsung/android/allshare/ERROR;

    invoke-interface {v1, p0, v2}, Lcom/samsung/android/allshare/media/ViewController$IResponseListener;->onConnectResponseReceived(Lcom/samsung/android/allshare/media/ViewController;Lcom/samsung/android/allshare/ERROR;)V

    goto :goto_0

    .line 229
    :cond_3
    iget-object v1, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mIAppComponent:Lcom/samsung/android/allshare/IAppControlAPI;

    iget-object v2, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mControlEventListener:Lcom/samsung/android/allshare/IAppControlAPI$IControlEventListener;

    invoke-virtual {v1, v2}, Lcom/samsung/android/allshare/IAppControlAPI;->setOnEventListener(Lcom/samsung/android/allshare/IAppControlAPI$IControlEventListener;)V

    .line 230
    iget-object v1, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mIAppComponent:Lcom/samsung/android/allshare/IAppControlAPI;

    iget-object v2, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mMACAddress:Ljava/lang/String;

    iget-object v3, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mIPAddress:Ljava/lang/String;

    iget-object v4, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mDeviceName:Ljava/lang/String;

    invoke-virtual {v1, v2, v3, v4}, Lcom/samsung/android/allshare/IAppControlAPI;->startControl(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    .line 231
    if-nez v0, :cond_1

    .line 232
    iget-object v1, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mResponseListener:Lcom/samsung/android/allshare/media/ViewController$IResponseListener;

    sget-object v2, Lcom/samsung/android/allshare/ERROR;->FAIL:Lcom/samsung/android/allshare/ERROR;

    invoke-interface {v1, p0, v2}, Lcom/samsung/android/allshare/media/ViewController$IResponseListener;->onConnectResponseReceived(Lcom/samsung/android/allshare/media/ViewController;Lcom/samsung/android/allshare/ERROR;)V

    goto :goto_0
.end method

.method public disconnect()V
    .locals 2

    .prologue
    .line 259
    iget-object v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    invoke-interface {v0}, Lcom/samsung/android/allshare/IAllShareConnector;->isAllShareServiceConnected()Z

    move-result v0

    if-nez v0, :cond_3

    .line 260
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mResponseListener:Lcom/samsung/android/allshare/media/ViewController$IResponseListener;

    if-eqz v0, :cond_1

    .line 261
    iget-object v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mResponseListener:Lcom/samsung/android/allshare/media/ViewController$IResponseListener;

    sget-object v1, Lcom/samsung/android/allshare/ERROR;->SERVICE_NOT_CONNECTED:Lcom/samsung/android/allshare/ERROR;

    invoke-interface {v0, p0, v1}, Lcom/samsung/android/allshare/media/ViewController$IResponseListener;->onDisconnectResponseReceived(Lcom/samsung/android/allshare/media/ViewController;Lcom/samsung/android/allshare/ERROR;)V

    .line 265
    :cond_1
    invoke-direct {p0}, Lcom/samsung/android/allshare/ViewControllerImpl;->innerDisconnect()V

    .line 285
    :cond_2
    :goto_0
    return-void

    .line 270
    :cond_3
    iget-boolean v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mIsConnected:Z

    if-nez v0, :cond_5

    .line 271
    iget-object v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mResponseListener:Lcom/samsung/android/allshare/media/ViewController$IResponseListener;

    if-eqz v0, :cond_4

    .line 272
    iget-object v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mResponseListener:Lcom/samsung/android/allshare/media/ViewController$IResponseListener;

    sget-object v1, Lcom/samsung/android/allshare/ERROR;->INVALID_DEVICE:Lcom/samsung/android/allshare/ERROR;

    invoke-interface {v0, p0, v1}, Lcom/samsung/android/allshare/media/ViewController$IResponseListener;->onDisconnectResponseReceived(Lcom/samsung/android/allshare/media/ViewController;Lcom/samsung/android/allshare/ERROR;)V

    .line 276
    :cond_4
    invoke-direct {p0}, Lcom/samsung/android/allshare/ViewControllerImpl;->innerDisconnect()V

    goto :goto_0

    .line 280
    :cond_5
    invoke-direct {p0}, Lcom/samsung/android/allshare/ViewControllerImpl;->innerDisconnect()V

    .line 282
    iget-object v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mResponseListener:Lcom/samsung/android/allshare/media/ViewController$IResponseListener;

    if-eqz v0, :cond_2

    .line 283
    iget-object v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mResponseListener:Lcom/samsung/android/allshare/media/ViewController$IResponseListener;

    sget-object v1, Lcom/samsung/android/allshare/ERROR;->SUCCESS:Lcom/samsung/android/allshare/ERROR;

    invoke-interface {v0, p0, v1}, Lcom/samsung/android/allshare/media/ViewController$IResponseListener;->onDisconnectResponseReceived(Lcom/samsung/android/allshare/media/ViewController;Lcom/samsung/android/allshare/ERROR;)V

    goto :goto_0
.end method

.method public getBundle()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 289
    const/4 v0, 0x0

    return-object v0
.end method

.method public getViewHeight()I
    .locals 1

    .prologue
    .line 302
    iget-object v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    invoke-interface {v0}, Lcom/samsung/android/allshare/IAllShareConnector;->isAllShareServiceConnected()Z

    move-result v0

    if-nez v0, :cond_1

    .line 303
    :cond_0
    const/4 v0, -0x1

    .line 305
    :goto_0
    return v0

    :cond_1
    iget v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mTvHeight:F

    float-to-int v0, v0

    goto :goto_0
.end method

.method public getViewWidth()I
    .locals 1

    .prologue
    .line 294
    iget-object v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    invoke-interface {v0}, Lcom/samsung/android/allshare/IAllShareConnector;->isAllShareServiceConnected()Z

    move-result v0

    if-nez v0, :cond_1

    .line 295
    :cond_0
    const/4 v0, -0x1

    .line 297
    :goto_0
    return v0

    :cond_1
    iget v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mTvWidth:F

    float-to-int v0, v0

    goto :goto_0
.end method

.method public isConnected()Z
    .locals 1

    .prologue
    .line 310
    iget-object v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    invoke-interface {v0}, Lcom/samsung/android/allshare/IAllShareConnector;->isAllShareServiceConnected()Z

    move-result v0

    if-nez v0, :cond_1

    .line 311
    :cond_0
    const/4 v0, 0x0

    .line 313
    :goto_0
    return v0

    :cond_1
    iget-boolean v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mIsConnected:Z

    goto :goto_0
.end method

.method public move(IIZ)V
    .locals 13
    .param p1, "cx"    # I
    .param p2, "cy"    # I
    .param p3, "isReleased"    # Z

    .prologue
    .line 612
    iget-object v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    invoke-interface {v0}, Lcom/samsung/android/allshare/IAllShareConnector;->isAllShareServiceConnected()Z

    move-result v0

    if-nez v0, :cond_1

    .line 659
    :cond_0
    :goto_0
    return-void

    .line 615
    :cond_1
    iget-boolean v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mIsConnected:Z

    if-eqz v0, :cond_0

    .line 616
    const/4 v10, 0x0

    .line 617
    .local v10, "mobileWidth":I
    const/4 v9, 0x0

    .line 618
    .local v9, "mobileHeight":I
    const/4 v12, 0x0

    .line 619
    .local v12, "widthRatio":F
    const/4 v8, 0x0

    .line 621
    .local v8, "heightRatio":F
    invoke-static {}, Lcom/samsung/android/allshare/ServiceConnector;->getContext()Landroid/content/Context;

    move-result-object v6

    .line 622
    .local v6, "context":Landroid/content/Context;
    if-eqz v6, :cond_0

    .line 625
    const-string v0, "window"

    invoke-virtual {v6, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v7

    .line 633
    .local v7, "display":Landroid/view/Display;
    new-instance v11, Landroid/graphics/Point;

    invoke-direct {v11}, Landroid/graphics/Point;-><init>()V

    .line 634
    .local v11, "outSize":Landroid/graphics/Point;
    invoke-virtual {v7, v11}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 636
    iget v10, v11, Landroid/graphics/Point;->x:I

    .line 637
    iget v9, v11, Landroid/graphics/Point;->y:I

    .line 639
    iget v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mTvWidth:F

    int-to-float v1, v10

    div-float v12, v0, v1

    .line 640
    iget v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mTvHeight:F

    int-to-float v1, v9

    div-float v8, v0, v1

    .line 642
    int-to-float v0, p1

    mul-float/2addr v0, v12

    float-to-int v2, v0

    .line 643
    .local v2, "tvX":I
    int-to-float v0, p2

    mul-float/2addr v0, v8

    float-to-int v3, v0

    .line 646
    .local v3, "tvY":I
    if-eqz p3, :cond_2

    .line 647
    iget-object v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mIAppComponent:Lcom/samsung/android/allshare/IAppControlAPI;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/IAppControlAPI;->setTouchGestureTouchMode(I)V

    .line 648
    iget-object v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mIAppComponent:Lcom/samsung/android/allshare/IAppControlAPI;

    const/16 v1, 0xc

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/allshare/IAppControlAPI;->sendTouchGestureEvent(IIIII)V

    .line 650
    iget-object v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mIAppComponent:Lcom/samsung/android/allshare/IAppControlAPI;

    const/16 v1, 0xd

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/allshare/IAppControlAPI;->sendTouchGestureEvent(IIIII)V

    goto :goto_0

    .line 655
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mIAppComponent:Lcom/samsung/android/allshare/IAppControlAPI;

    const/16 v1, 0xd

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/allshare/IAppControlAPI;->sendTouchGestureEvent(IIIII)V

    goto :goto_0
.end method

.method public removeEventHandler()V
    .locals 4

    .prologue
    .line 872
    iget-object v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    const-string v1, "com.sec.android.allshare.event.EVENT_DEVICE_SUBSCRIBE"

    iget-object v2, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    invoke-virtual {v2}, Lcom/samsung/android/allshare/DeviceImpl;->getBundle()Landroid/os/Bundle;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mAllShareEventHandler:Lcom/samsung/android/allshare/AllShareEventHandler;

    invoke-interface {v0, v1, v2, v3}, Lcom/samsung/android/allshare/IAllShareConnector;->unsubscribeAllShareEvent(Ljava/lang/String;Landroid/os/Bundle;Lcom/samsung/android/allshare/AllShareEventHandler;)V

    .line 874
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mIsSubscribed:Z

    .line 875
    return-void
.end method

.method public setEventListener(Lcom/samsung/android/allshare/media/ViewController$IEventListener;)V
    .locals 4
    .param p1, "listener"    # Lcom/samsung/android/allshare/media/ViewController$IEventListener;

    .prologue
    .line 318
    iget-object v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    invoke-interface {v0}, Lcom/samsung/android/allshare/IAllShareConnector;->isAllShareServiceConnected()Z

    move-result v0

    if-nez v0, :cond_2

    .line 319
    :cond_0
    const-string v0, "ViewControllerImpl"

    const-string v1, "setEventListener error! AllShareService is not connected"

    invoke-static {v0, v1}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 336
    :cond_1
    :goto_0
    return-void

    .line 323
    :cond_2
    iput-object p1, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mEventListener:Lcom/samsung/android/allshare/media/ViewController$IEventListener;

    .line 325
    iget-boolean v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mIsSubscribed:Z

    if-nez v0, :cond_3

    if-eqz p1, :cond_3

    .line 326
    iget-object v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    const-string v1, "com.sec.android.allshare.event.EVENT_DEVICE_SUBSCRIBE"

    iget-object v2, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    invoke-virtual {v2}, Lcom/samsung/android/allshare/DeviceImpl;->getBundle()Landroid/os/Bundle;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mAllShareEventHandler:Lcom/samsung/android/allshare/AllShareEventHandler;

    invoke-interface {v0, v1, v2, v3}, Lcom/samsung/android/allshare/IAllShareConnector;->subscribeAllShareEvent(Ljava/lang/String;Landroid/os/Bundle;Lcom/samsung/android/allshare/AllShareEventHandler;)Z

    .line 328
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mIsSubscribed:Z

    goto :goto_0

    .line 329
    :cond_3
    iget-boolean v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mIsSubscribed:Z

    if-eqz v0, :cond_1

    if-nez p1, :cond_1

    .line 330
    iget-object v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    const-string v1, "com.sec.android.allshare.event.EVENT_DEVICE_SUBSCRIBE"

    iget-object v2, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    invoke-virtual {v2}, Lcom/samsung/android/allshare/DeviceImpl;->getBundle()Landroid/os/Bundle;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mAllShareEventHandler:Lcom/samsung/android/allshare/AllShareEventHandler;

    invoke-interface {v0, v1, v2, v3}, Lcom/samsung/android/allshare/IAllShareConnector;->unsubscribeAllShareEvent(Ljava/lang/String;Landroid/os/Bundle;Lcom/samsung/android/allshare/AllShareEventHandler;)V

    .line 332
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mIsSubscribed:Z

    goto :goto_0
.end method

.method public setResponseListener(Lcom/samsung/android/allshare/media/ViewController$IResponseListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/android/allshare/media/ViewController$IResponseListener;

    .prologue
    .line 879
    iput-object p1, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mResponseListener:Lcom/samsung/android/allshare/media/ViewController$IResponseListener;

    .line 880
    return-void
.end method

.method public setViewAngle(I)V
    .locals 13
    .param p1, "angle"    # I

    .prologue
    const-wide/16 v2, 0x0

    const/4 v4, 0x0

    .line 340
    iget-object v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    invoke-interface {v0}, Lcom/samsung/android/allshare/IAllShareConnector;->isAllShareServiceConnected()Z

    move-result v0

    if-nez v0, :cond_1

    .line 348
    :cond_0
    :goto_0
    return-void

    .line 343
    :cond_1
    iget-boolean v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mIsConnected:Z

    if-eqz v0, :cond_0

    .line 344
    iget-object v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mIAppComponent:Lcom/samsung/android/allshare/IAppControlAPI;

    const/16 v1, 0x25

    move v5, v4

    move v6, v4

    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/allshare/IAppControlAPI;->sendMultiTouchEvent(IDIII)V

    .line 345
    iget-object v6, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mIAppComponent:Lcom/samsung/android/allshare/IAppControlAPI;

    const/16 v7, 0x27

    const-wide/high16 v8, 0x4059000000000000L    # 100.0

    move v10, p1

    move v11, v4

    move v12, v4

    invoke-virtual/range {v6 .. v12}, Lcom/samsung/android/allshare/IAppControlAPI;->sendMultiTouchEvent(IDIII)V

    .line 346
    iget-object v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mIAppComponent:Lcom/samsung/android/allshare/IAppControlAPI;

    const/16 v1, 0x26

    move v5, v4

    move v6, v4

    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/allshare/IAppControlAPI;->sendMultiTouchEvent(IDIII)V

    goto :goto_0
.end method

.method public zoom(IIII)V
    .locals 7
    .param p1, "cx"    # I
    .param p2, "cy"    # I
    .param p3, "zoomPercent"    # I
    .param p4, "angle"    # I

    .prologue
    .line 370
    iget-object v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    invoke-interface {v0}, Lcom/samsung/android/allshare/IAllShareConnector;->isAllShareServiceConnected()Z

    move-result v0

    if-nez v0, :cond_1

    .line 379
    :cond_0
    :goto_0
    return-void

    .line 373
    :cond_1
    iget-boolean v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mIsConnected:Z

    if-eqz v0, :cond_0

    .line 374
    iget-object v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mIAppComponent:Lcom/samsung/android/allshare/IAppControlAPI;

    const/16 v1, 0x25

    const-wide/16 v2, 0x0

    const/4 v4, 0x0

    move v5, p1

    move v6, p2

    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/allshare/IAppControlAPI;->sendMultiTouchEvent(IDIII)V

    .line 375
    iget-object v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mIAppComponent:Lcom/samsung/android/allshare/IAppControlAPI;

    const/16 v1, 0x27

    int-to-double v2, p3

    move v4, p4

    move v5, p1

    move v6, p2

    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/allshare/IAppControlAPI;->sendMultiTouchEvent(IDIII)V

    .line 377
    iget-object v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mIAppComponent:Lcom/samsung/android/allshare/IAppControlAPI;

    const/16 v1, 0x26

    const-wide/16 v2, 0x0

    const/4 v4, 0x0

    move v5, p1

    move v6, p2

    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/allshare/IAppControlAPI;->sendMultiTouchEvent(IDIII)V

    goto :goto_0
.end method

.method public zoom(IIIIII)V
    .locals 9
    .param p1, "cx"    # I
    .param p2, "cy"    # I
    .param p3, "zoomPercent"    # I
    .param p4, "angle"    # I
    .param p5, "sourceWidth"    # I
    .param p6, "sourceHeight"    # I

    .prologue
    .line 468
    const-string v0, "ViewControllerImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[zoom] cx : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " cy : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " zoomPercent : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " angle : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/allshare/DLog;->i_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 471
    iget-object v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    invoke-interface {v0}, Lcom/samsung/android/allshare/IAllShareConnector;->isAllShareServiceConnected()Z

    move-result v0

    if-nez v0, :cond_1

    .line 608
    :cond_0
    :goto_0
    return-void

    .line 474
    :cond_1
    iget v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mImageWidth:F

    float-to-int v0, v0

    if-ne v0, p5, :cond_2

    iget v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mImageHeight:F

    float-to-int v0, v0

    if-ne v0, p6, :cond_2

    iget v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mPrevAngle:I

    if-eq v0, p4, :cond_3

    .line 475
    :cond_2
    const-string v0, "ViewControllerImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[zoom] mImageWidth : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mImageWidth:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mImageHeight : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mImageHeight:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " sourceWidth : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " sourceHeight : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/allshare/DLog;->i_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 478
    invoke-direct {p0}, Lcom/samsung/android/allshare/ViewControllerImpl;->initZoom()V

    .line 479
    invoke-direct {p0}, Lcom/samsung/android/allshare/ViewControllerImpl;->calcMobileResolution()V

    .line 480
    invoke-direct {p0, p5, p6}, Lcom/samsung/android/allshare/ViewControllerImpl;->calcOrgImageResolution(II)V

    .line 481
    iget v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mMobileWidth:F

    iget v1, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mMobileHeight:F

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/allshare/ViewControllerImpl;->initZoomedImageSize(FF)V

    .line 483
    const-string v0, "ViewControllerImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[zoom] mTvWidth : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mTvWidth:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mTvHeight : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mTvHeight:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mImageWidth : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mImageWidth:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mImageHeight : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mImageHeight:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/allshare/DLog;->i_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 486
    iget v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mTvHeight:F

    iget v1, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mImageHeight:F

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_b

    iget v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mTvWidth:F

    iget v1, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mImageWidth:F

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_b

    .line 487
    iget v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mTvWidth:F

    iget v1, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mImageWidth:F

    sub-float/2addr v0, v1

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    iput v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mTvOrgX0:F

    .line 488
    iget v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mTvHeight:F

    iget v1, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mImageHeight:F

    sub-float/2addr v0, v1

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    iput v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mTvOrgY0:F

    .line 497
    :goto_1
    const-string v0, "ViewControllerImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[zoom] mTvOrgX0 : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mTvOrgX0:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mTvOrgY0 : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mTvOrgY0:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mTvOrgX : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mTvOrgX:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mTvOrgY : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mTvOrgY:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/allshare/DLog;->i_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 500
    iget v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mTvOrgX0:F

    iput v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mTvOrgX:F

    .line 501
    iget v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mTvOrgY0:F

    iput v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mTvOrgY:F

    .line 503
    iput p4, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mPrevAngle:I

    .line 505
    invoke-direct {p0}, Lcom/samsung/android/allshare/ViewControllerImpl;->calcMargin()V

    .line 506
    iget v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mMarginX:I

    iput v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mOrgX:I

    .line 507
    iget v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mMarginY:I

    iput v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mOrgY:I

    .line 510
    :cond_3
    iget-boolean v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mIsConnected:Z

    if-eqz v0, :cond_0

    .line 511
    invoke-direct {p0}, Lcom/samsung/android/allshare/ViewControllerImpl;->calcMobileResolution()V

    .line 513
    const/4 v7, 0x0

    .line 514
    .local v7, "isDoubleTapPinch":Z
    if-eqz p3, :cond_4

    iget v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mPrevAngle:I

    if-eq v0, p4, :cond_d

    .line 515
    :cond_4
    const/16 v0, 0x64

    if-ge p3, v0, :cond_5

    .line 516
    const/4 p3, 0x0

    .line 518
    :cond_5
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mRelativeZoomRate:F

    .line 519
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mPrevAbsZoomRate:F

    .line 520
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mAbsZoomRate:F

    .line 521
    const/4 v7, 0x1

    .line 522
    iget v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mMobileWidth:F

    iget v1, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mMobileHeight:F

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/allshare/ViewControllerImpl;->initZoomedImageSize(FF)V

    .line 524
    invoke-direct {p0}, Lcom/samsung/android/allshare/ViewControllerImpl;->calcMargin()V

    .line 525
    iget v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mMarginX:I

    iput v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mOrgX:I

    .line 526
    iget v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mMarginY:I

    iput v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mOrgY:I

    .line 547
    :cond_6
    :goto_2
    invoke-direct {p0}, Lcom/samsung/android/allshare/ViewControllerImpl;->clacZoomedImageSize()V

    .line 549
    const-string v0, "ViewControllerImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[zoom] mMobileWidth : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mMobileWidth:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mMobileHeight : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mMobileHeight:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mMobilePhoneRatio : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mMobilePhoneRatio:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mRelativeZoomRate : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mRelativeZoomRate:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/allshare/DLog;->i_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 553
    invoke-direct {p0}, Lcom/samsung/android/allshare/ViewControllerImpl;->getMagicRate()F

    move-result v8

    .line 555
    .local v8, "magicRate":F
    invoke-direct {p0}, Lcom/samsung/android/allshare/ViewControllerImpl;->calcMargin()V

    .line 557
    iget v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mOrgX:I

    sub-int v0, p1, v0

    iput v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mOrgCenterX:I

    .line 558
    iget v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mOrgY:I

    sub-int v0, p2, v0

    iput v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mOrgCenterY:I

    .line 560
    iget v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mOrgCenterX:I

    if-gez v0, :cond_7

    .line 561
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mOrgCenterX:I

    .line 563
    :cond_7
    iget v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mOrgCenterY:I

    if-gez v0, :cond_8

    .line 564
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mOrgCenterY:I

    .line 566
    :cond_8
    const-string v0, "ViewControllerImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[zoom] mMarginX : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mMarginX:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mMarginY : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mMarginY:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mOrgX : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mOrgX:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mOrgY : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mOrgY:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mOrgCenterX : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mOrgCenterX:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mOrgCenterY : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mOrgCenterY:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/allshare/DLog;->i_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 570
    const/4 v5, 0x0

    .line 571
    .local v5, "tvZoomCenterX":I
    const/4 v6, 0x0

    .line 573
    .local v6, "tvZoomCenterY":I
    if-eqz v7, :cond_f

    .line 574
    iget v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mTvWidth:F

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    float-to-int v5, v0

    .line 575
    iget v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mTvHeight:F

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    float-to-int v6, v0

    .line 581
    :goto_3
    iget v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mZoomedImageWidth:F

    iget v1, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mMobileWidth:F

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_9

    .line 582
    iget v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mTvWidth:F

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    float-to-int v5, v0

    .line 585
    :cond_9
    iget v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mZoomedImageHeight:F

    iget v1, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mMobileHeight:F

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_a

    .line 586
    iget v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mTvHeight:F

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    float-to-int v6, v0

    .line 588
    :cond_a
    const-string v0, "ViewControllerImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[zoom] tvCenterX : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " tvCenterY : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mTvOrgX : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mTvOrgX:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mTvOrgY : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mTvOrgY:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mOrgCenterX : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mOrgCenterX:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mOrgCenterY : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mOrgCenterY:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/allshare/DLog;->i_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 592
    iget v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mOrgX:I

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mOrgCenterX:I

    int-to-float v1, v1

    iget v2, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mRelativeZoomRate:F

    const/high16 v3, 0x3f800000    # 1.0f

    sub-float/2addr v2, v3

    mul-float/2addr v1, v2

    sub-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mOrgX:I

    .line 593
    iget v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mOrgY:I

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mOrgCenterY:I

    int-to-float v1, v1

    iget v2, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mRelativeZoomRate:F

    const/high16 v3, 0x3f800000    # 1.0f

    sub-float/2addr v2, v3

    mul-float/2addr v1, v2

    sub-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mOrgY:I

    .line 595
    iget-object v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mIAppComponent:Lcom/samsung/android/allshare/IAppControlAPI;

    const/16 v1, 0x25

    const-wide/16 v2, 0x0

    const/4 v4, 0x0

    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/allshare/IAppControlAPI;->sendMultiTouchEvent(IDIII)V

    .line 597
    iget-object v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mIAppComponent:Lcom/samsung/android/allshare/IAppControlAPI;

    const/16 v1, 0x27

    int-to-double v2, p3

    move v4, p4

    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/allshare/IAppControlAPI;->sendMultiTouchEvent(IDIII)V

    .line 599
    iget-object v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mIAppComponent:Lcom/samsung/android/allshare/IAppControlAPI;

    const/16 v1, 0x26

    const-wide/16 v2, 0x0

    const/4 v4, 0x0

    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/allshare/IAppControlAPI;->sendMultiTouchEvent(IDIII)V

    .line 602
    iget v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mPrevAbsZoomRate:F

    iget v1, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mRelativeZoomRate:F

    mul-float/2addr v0, v1

    iput v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mPrevAbsZoomRate:F

    .line 604
    const-string v0, "ViewControllerImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[zoom] mOrgX : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mOrgX:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mOrgY : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mOrgY:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mTvOrgX : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mTvOrgX:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mTvOrgY : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mTvOrgY:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/allshare/DLog;->i_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 606
    const-string v0, "ViewControllerImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[zoom] mPrevAbsZoomRate : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mPrevAbsZoomRate:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/allshare/DLog;->i_api(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 489
    .end local v5    # "tvZoomCenterX":I
    .end local v6    # "tvZoomCenterY":I
    .end local v7    # "isDoubleTapPinch":Z
    .end local v8    # "magicRate":F
    :cond_b
    iget v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mTvRatio:F

    iget v1, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mImageRatio:F

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_c

    .line 490
    iget v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mTvWidth:F

    iget v1, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mImageWidth:F

    iget v2, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mImageHeight:F

    div-float/2addr v1, v2

    iget v2, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mTvHeight:F

    mul-float/2addr v1, v2

    sub-float/2addr v0, v1

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    float-to-int v0, v0

    int-to-float v0, v0

    iput v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mTvOrgX0:F

    .line 491
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mTvOrgY0:F

    goto/16 :goto_1

    .line 493
    :cond_c
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mTvOrgX0:F

    .line 494
    iget v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mTvHeight:F

    iget v1, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mImageHeight:F

    iget v2, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mImageWidth:F

    div-float/2addr v1, v2

    iget v2, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mTvWidth:F

    mul-float/2addr v1, v2

    sub-float/2addr v0, v1

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    float-to-int v0, v0

    int-to-float v0, v0

    iput v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mTvOrgY0:F

    goto/16 :goto_1

    .line 528
    .restart local v7    # "isDoubleTapPinch":Z
    :cond_d
    int-to-float v0, p3

    const/high16 v1, 0x42c80000    # 100.0f

    div-float/2addr v0, v1

    iput v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mRelativeZoomRate:F

    .line 529
    iget v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mAbsZoomRate:F

    iget v1, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mRelativeZoomRate:F

    mul-float/2addr v0, v1

    iput v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mAbsZoomRate:F

    .line 531
    iget v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mAbsZoomRate:F

    const/high16 v1, 0x3f800000    # 1.0f

    cmpg-float v0, v0, v1

    if-gez v0, :cond_6

    .line 532
    const/16 v0, 0x64

    if-ge p3, v0, :cond_e

    .line 533
    const/4 p3, 0x0

    .line 535
    :cond_e
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mRelativeZoomRate:F

    .line 536
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mPrevAbsZoomRate:F

    .line 537
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mAbsZoomRate:F

    .line 538
    const/4 v7, 0x1

    .line 539
    iget v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mMobileWidth:F

    iget v1, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mMobileHeight:F

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/allshare/ViewControllerImpl;->initZoomedImageSize(FF)V

    .line 541
    invoke-direct {p0}, Lcom/samsung/android/allshare/ViewControllerImpl;->calcMargin()V

    .line 542
    iget v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mMarginX:I

    iput v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mOrgX:I

    .line 543
    iget v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mMarginY:I

    iput v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mOrgY:I

    goto/16 :goto_2

    .line 577
    .restart local v5    # "tvZoomCenterX":I
    .restart local v6    # "tvZoomCenterY":I
    .restart local v8    # "magicRate":F
    :cond_f
    iget v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mOrgCenterX:I

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mPrevAbsZoomRate:F

    div-float/2addr v0, v1

    mul-float/2addr v0, v8

    iget v1, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mTvOrgX:F

    add-float/2addr v0, v1

    float-to-int v5, v0

    .line 578
    iget v0, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mOrgCenterY:I

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mPrevAbsZoomRate:F

    div-float/2addr v0, v1

    mul-float/2addr v0, v8

    iget v1, p0, Lcom/samsung/android/allshare/ViewControllerImpl;->mTvOrgY:F

    add-float/2addr v0, v1

    float-to-int v6, v0

    goto/16 :goto_3
.end method
