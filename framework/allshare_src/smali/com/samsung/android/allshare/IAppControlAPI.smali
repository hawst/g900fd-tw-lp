.class Lcom/samsung/android/allshare/IAppControlAPI;
.super Ljava/lang/Object;
.source "IAppControlAPI.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/allshare/IAppControlAPI$IControlEventListener;
    }
.end annotation


# static fields
.field private static final DTVSERVER_PORT:I = 0xd6d8

.field private static final TAG:Ljava/lang/String; = "IAppControlAPI"


# instance fields
.field private bIsRunningConnection:Z

.field private mDeviceId:Ljava/lang/String;

.field private mEventListener:Lcom/samsung/android/allshare/IAppControlAPI$IControlEventListener;

.field public mExcutor:Ljava/util/concurrent/ExecutorService;

.field private mLock:Ljava/lang/Object;

.field private mServerIp:Ljava/lang/String;

.field private mSetIpaddress:I

.field mTvMsgListener:Lcom/samsung/android/allshare/TVMessageListener;

.field mTvMsgSender:Lcom/samsung/android/allshare/TVMessageSender;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 93
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/allshare/IAppControlAPI;->mServerIp:Ljava/lang/String;

    .line 52
    iput-object v1, p0, Lcom/samsung/android/allshare/IAppControlAPI;->mTvMsgSender:Lcom/samsung/android/allshare/TVMessageSender;

    .line 54
    iput-object v1, p0, Lcom/samsung/android/allshare/IAppControlAPI;->mTvMsgListener:Lcom/samsung/android/allshare/TVMessageListener;

    .line 56
    iput v2, p0, Lcom/samsung/android/allshare/IAppControlAPI;->mSetIpaddress:I

    .line 60
    iput-boolean v2, p0, Lcom/samsung/android/allshare/IAppControlAPI;->bIsRunningConnection:Z

    .line 62
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/IAppControlAPI;->mLock:Ljava/lang/Object;

    .line 64
    invoke-static {}, Ljava/util/concurrent/Executors;->newCachedThreadPool()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/allshare/IAppControlAPI;->mExcutor:Ljava/util/concurrent/ExecutorService;

    .line 75
    iput-object v1, p0, Lcom/samsung/android/allshare/IAppControlAPI;->mEventListener:Lcom/samsung/android/allshare/IAppControlAPI$IControlEventListener;

    .line 94
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/allshare/IAppControlAPI;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/IAppControlAPI;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/samsung/android/allshare/IAppControlAPI;->mLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/android/allshare/IAppControlAPI;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/IAppControlAPI;

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/samsung/android/allshare/IAppControlAPI;->createControlReceiver()V

    return-void
.end method

.method static synthetic access$202(Lcom/samsung/android/allshare/IAppControlAPI;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/IAppControlAPI;
    .param p1, "x1"    # Z

    .prologue
    .line 44
    iput-boolean p1, p0, Lcom/samsung/android/allshare/IAppControlAPI;->bIsRunningConnection:Z

    return p1
.end method

.method static synthetic access$300(Lcom/samsung/android/allshare/IAppControlAPI;)Lcom/samsung/android/allshare/IAppControlAPI$IControlEventListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/IAppControlAPI;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/samsung/android/allshare/IAppControlAPI;->mEventListener:Lcom/samsung/android/allshare/IAppControlAPI$IControlEventListener;

    return-object v0
.end method

.method private createControlReceiver()V
    .locals 1

    .prologue
    .line 221
    iget-object v0, p0, Lcom/samsung/android/allshare/IAppControlAPI;->mTvMsgListener:Lcom/samsung/android/allshare/TVMessageListener;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/allshare/IAppControlAPI;->mTvMsgSender:Lcom/samsung/android/allshare/TVMessageSender;

    if-eqz v0, :cond_0

    .line 222
    iget-object v0, p0, Lcom/samsung/android/allshare/IAppControlAPI;->mTvMsgSender:Lcom/samsung/android/allshare/TVMessageSender;

    invoke-direct {p0, v0}, Lcom/samsung/android/allshare/IAppControlAPI;->initControlReceiver(Lcom/samsung/android/allshare/TVMessageSender;)V

    .line 224
    :cond_0
    return-void
.end method

.method private initControl(Ljava/lang/String;)V
    .locals 1
    .param p1, "Deviceid"    # Ljava/lang/String;

    .prologue
    .line 113
    iput-object p1, p0, Lcom/samsung/android/allshare/IAppControlAPI;->mDeviceId:Ljava/lang/String;

    .line 118
    new-instance v0, Lcom/samsung/android/allshare/TVMessageSender;

    invoke-direct {v0, p0}, Lcom/samsung/android/allshare/TVMessageSender;-><init>(Lcom/samsung/android/allshare/IAppControlAPI;)V

    iput-object v0, p0, Lcom/samsung/android/allshare/IAppControlAPI;->mTvMsgSender:Lcom/samsung/android/allshare/TVMessageSender;

    .line 119
    iget-object v0, p0, Lcom/samsung/android/allshare/IAppControlAPI;->mTvMsgSender:Lcom/samsung/android/allshare/TVMessageSender;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/TVMessageSender;->start()V

    .line 121
    return-void
.end method

.method private initControlReceiver(Lcom/samsung/android/allshare/TVMessageSender;)V
    .locals 2
    .param p1, "tl"    # Lcom/samsung/android/allshare/TVMessageSender;

    .prologue
    .line 129
    new-instance v0, Lcom/samsung/android/allshare/TVMessageListener;

    invoke-direct {v0, p1}, Lcom/samsung/android/allshare/TVMessageListener;-><init>(Lcom/samsung/android/allshare/TVMessageSender;)V

    iput-object v0, p0, Lcom/samsung/android/allshare/IAppControlAPI;->mTvMsgListener:Lcom/samsung/android/allshare/TVMessageListener;

    .line 130
    iget-object v0, p0, Lcom/samsung/android/allshare/IAppControlAPI;->mTvMsgListener:Lcom/samsung/android/allshare/TVMessageListener;

    iget-object v1, p0, Lcom/samsung/android/allshare/IAppControlAPI;->mEventListener:Lcom/samsung/android/allshare/IAppControlAPI$IControlEventListener;

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/TVMessageListener;->setOnEventListener(Lcom/samsung/android/allshare/IAppControlAPI$IControlEventListener;)V

    .line 131
    iget-object v0, p0, Lcom/samsung/android/allshare/IAppControlAPI;->mTvMsgListener:Lcom/samsung/android/allshare/TVMessageListener;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/TVMessageListener;->start()V

    .line 132
    return-void
.end method

.method private sendMsgToDevice(Landroid/os/Message;)V
    .locals 1
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 139
    iget-object v0, p0, Lcom/samsung/android/allshare/IAppControlAPI;->mTvMsgSender:Lcom/samsung/android/allshare/TVMessageSender;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/allshare/IAppControlAPI;->mTvMsgSender:Lcom/samsung/android/allshare/TVMessageSender;

    iget-object v0, v0, Lcom/samsung/android/allshare/TVMessageSender;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 140
    iget-object v0, p0, Lcom/samsung/android/allshare/IAppControlAPI;->mTvMsgSender:Lcom/samsung/android/allshare/TVMessageSender;

    iget-object v0, v0, Lcom/samsung/android/allshare/TVMessageSender;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 142
    :cond_0
    return-void
.end method

.method private setIpaddress(Ljava/lang/String;I)V
    .locals 1
    .param p1, "ipaddress"    # Ljava/lang/String;
    .param p2, "port"    # I

    .prologue
    .line 103
    iput-object p1, p0, Lcom/samsung/android/allshare/IAppControlAPI;->mServerIp:Ljava/lang/String;

    .line 105
    const/4 v0, 0x1

    iput v0, p0, Lcom/samsung/android/allshare/IAppControlAPI;->mSetIpaddress:I

    .line 106
    return-void
.end method


# virtual methods
.method public addControlEventListener(Lcom/samsung/android/allshare/IAppControlAPI$IControlEventListener;)Z
    .locals 2
    .param p1, "listener"    # Lcom/samsung/android/allshare/IAppControlAPI$IControlEventListener;

    .prologue
    .line 253
    iget-object v0, p0, Lcom/samsung/android/allshare/IAppControlAPI;->mTvMsgListener:Lcom/samsung/android/allshare/TVMessageListener;

    if-eqz v0, :cond_0

    .line 255
    iget-object v0, p0, Lcom/samsung/android/allshare/IAppControlAPI;->mTvMsgListener:Lcom/samsung/android/allshare/TVMessageListener;

    iget-object v1, p0, Lcom/samsung/android/allshare/IAppControlAPI;->mEventListener:Lcom/samsung/android/allshare/IAppControlAPI$IControlEventListener;

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/TVMessageListener;->setOnEventListener(Lcom/samsung/android/allshare/IAppControlAPI$IControlEventListener;)V

    .line 256
    const/4 v0, 0x1

    .line 258
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public removeControl()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 280
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 281
    .local v0, "msg":Landroid/os/Message;
    if-eqz v0, :cond_0

    .line 282
    iput v2, v0, Landroid/os/Message;->what:I

    .line 285
    :cond_0
    invoke-direct {p0, v0}, Lcom/samsung/android/allshare/IAppControlAPI;->sendMsgToDevice(Landroid/os/Message;)V

    .line 286
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/android/allshare/IAppControlAPI;->mTvMsgListener:Lcom/samsung/android/allshare/TVMessageListener;

    .line 288
    iput v2, p0, Lcom/samsung/android/allshare/IAppControlAPI;->mSetIpaddress:I

    .line 289
    return-void
.end method

.method public removeControlEventListener(Lcom/samsung/android/allshare/IAppControlAPI$IControlEventListener;)Z
    .locals 1
    .param p1, "listener"    # Lcom/samsung/android/allshare/IAppControlAPI$IControlEventListener;

    .prologue
    .line 272
    const/4 v0, 0x1

    return v0
.end method

.method public sendAuthentication()V
    .locals 2

    .prologue
    .line 341
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 342
    .local v0, "msg":Landroid/os/Message;
    const/16 v1, 0x34

    iput v1, v0, Landroid/os/Message;->what:I

    .line 343
    invoke-direct {p0, v0}, Lcom/samsung/android/allshare/IAppControlAPI;->sendMsgToDevice(Landroid/os/Message;)V

    .line 344
    return-void
.end method

.method public sendIntCommand(II)V
    .locals 1
    .param p1, "command_type"    # I
    .param p2, "keybutton_cmd"    # I

    .prologue
    .line 396
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 397
    .local v0, "msg":Landroid/os/Message;
    iput p1, v0, Landroid/os/Message;->what:I

    .line 398
    iput p2, v0, Landroid/os/Message;->arg1:I

    .line 400
    invoke-direct {p0, v0}, Lcom/samsung/android/allshare/IAppControlAPI;->sendMsgToDevice(Landroid/os/Message;)V

    .line 401
    return-void
.end method

.method public sendKeyEvent(I)V
    .locals 2
    .param p1, "keycode"    # I

    .prologue
    .line 351
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 352
    .local v0, "msg":Landroid/os/Message;
    const/4 v1, 0x1

    iput v1, v0, Landroid/os/Message;->what:I

    .line 353
    iput p1, v0, Landroid/os/Message;->arg1:I

    .line 355
    invoke-direct {p0, v0}, Lcom/samsung/android/allshare/IAppControlAPI;->sendMsgToDevice(Landroid/os/Message;)V

    .line 356
    return-void
.end method

.method public sendKeyboardEnd()V
    .locals 2

    .prologue
    .line 444
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 445
    .local v0, "msg":Landroid/os/Message;
    const/16 v1, 0x8

    iput v1, v0, Landroid/os/Message;->what:I

    .line 447
    invoke-direct {p0, v0}, Lcom/samsung/android/allshare/IAppControlAPI;->sendMsgToDevice(Landroid/os/Message;)V

    .line 448
    return-void
.end method

.method public sendKeyboardEvent(Landroid/os/Message;)V
    .locals 1
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 363
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 364
    .local v0, "m_msg":Landroid/os/Message;
    move-object v0, p1

    .line 365
    invoke-direct {p0, v0}, Lcom/samsung/android/allshare/IAppControlAPI;->sendMsgToDevice(Landroid/os/Message;)V

    .line 366
    return-void
.end method

.method public sendKeyboardStringCommand(Ljava/lang/String;I)V
    .locals 2
    .param p1, "keycode"    # Ljava/lang/String;
    .param p2, "encoding"    # I

    .prologue
    .line 374
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 375
    .local v0, "msg":Landroid/os/Message;
    const/4 v1, 0x1

    iput v1, v0, Landroid/os/Message;->what:I

    .line 376
    iput p2, v0, Landroid/os/Message;->arg1:I

    .line 377
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 379
    invoke-direct {p0, v0}, Lcom/samsung/android/allshare/IAppControlAPI;->sendMsgToDevice(Landroid/os/Message;)V

    .line 380
    return-void
.end method

.method public sendMouseCreate()V
    .locals 2

    .prologue
    .line 451
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 452
    .local v0, "msg":Landroid/os/Message;
    const/16 v1, 0xf

    iput v1, v0, Landroid/os/Message;->what:I

    .line 454
    invoke-direct {p0, v0}, Lcom/samsung/android/allshare/IAppControlAPI;->sendMsgToDevice(Landroid/os/Message;)V

    .line 455
    return-void
.end method

.method public sendMouseDestroy()V
    .locals 2

    .prologue
    .line 458
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 459
    .local v0, "msg":Landroid/os/Message;
    const/16 v1, 0x10

    iput v1, v0, Landroid/os/Message;->what:I

    .line 461
    invoke-direct {p0, v0}, Lcom/samsung/android/allshare/IAppControlAPI;->sendMsgToDevice(Landroid/os/Message;)V

    .line 462
    return-void
.end method

.method public sendMouseProcess(IIIIII)V
    .locals 3
    .param p1, "eventType"    # I
    .param p2, "x"    # I
    .param p3, "y"    # I
    .param p4, "dx"    # I
    .param p5, "dy"    # I
    .param p6, "button"    # I

    .prologue
    .line 465
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v1

    .line 467
    .local v1, "msg":Landroid/os/Message;
    const/4 v2, 0x2

    iput v2, v1, Landroid/os/Message;->what:I

    .line 469
    new-instance v0, Lcom/samsung/android/allshare/EventMouse;

    invoke-direct {v0}, Lcom/samsung/android/allshare/EventMouse;-><init>()V

    .line 470
    .local v0, "ieventMouse":Lcom/samsung/android/allshare/EventMouse;
    iput p1, v0, Lcom/samsung/android/allshare/EventMouse;->mType:I

    .line 471
    iput p2, v0, Lcom/samsung/android/allshare/EventMouse;->mX:I

    .line 472
    iput p3, v0, Lcom/samsung/android/allshare/EventMouse;->mY:I

    .line 473
    iput p4, v0, Lcom/samsung/android/allshare/EventMouse;->mDX:I

    .line 474
    iput p5, v0, Lcom/samsung/android/allshare/EventMouse;->mDY:I

    .line 475
    iput p6, v0, Lcom/samsung/android/allshare/EventMouse;->mButton:I

    .line 476
    iput-object v0, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 478
    invoke-direct {p0, v1}, Lcom/samsung/android/allshare/IAppControlAPI;->sendMsgToDevice(Landroid/os/Message;)V

    .line 479
    return-void
.end method

.method public sendMultiTouchEvent(IDIII)V
    .locals 4
    .param p1, "eventType"    # I
    .param p2, "zoomRate"    # D
    .param p4, "angle"    # I
    .param p5, "cx"    # I
    .param p6, "cy"    # I

    .prologue
    .line 425
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v1

    .line 427
    .local v1, "msg":Landroid/os/Message;
    const/4 v2, 0x6

    iput v2, v1, Landroid/os/Message;->what:I

    .line 429
    new-instance v0, Lcom/samsung/android/allshare/EventTouch;

    invoke-direct {v0}, Lcom/samsung/android/allshare/EventTouch;-><init>()V

    .line 430
    .local v0, "ieventTouch":Lcom/samsung/android/allshare/EventTouch;
    iput p1, v0, Lcom/samsung/android/allshare/EventTouch;->mType:I

    .line 431
    double-to-int v2, p2

    iput v2, v0, Lcom/samsung/android/allshare/EventTouch;->mDistance:I

    .line 432
    iput p4, v0, Lcom/samsung/android/allshare/EventTouch;->mDegree:I

    .line 433
    iput p5, v0, Lcom/samsung/android/allshare/EventTouch;->mX:I

    .line 434
    iput p6, v0, Lcom/samsung/android/allshare/EventTouch;->mY:I

    .line 436
    iput-object v0, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 438
    invoke-direct {p0, v1}, Lcom/samsung/android/allshare/IAppControlAPI;->sendMsgToDevice(Landroid/os/Message;)V

    .line 440
    return-void
.end method

.method public sendRemoteControlKey(Ljava/lang/String;I)V
    .locals 2
    .param p1, "keycode"    # Ljava/lang/String;
    .param p2, "mode"    # I

    .prologue
    .line 383
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 384
    .local v0, "msg":Landroid/os/Message;
    const/16 v1, 0xe

    iput v1, v0, Landroid/os/Message;->what:I

    .line 385
    iput p2, v0, Landroid/os/Message;->arg1:I

    .line 386
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 388
    invoke-direct {p0, v0}, Lcom/samsung/android/allshare/IAppControlAPI;->sendMsgToDevice(Landroid/os/Message;)V

    .line 389
    return-void
.end method

.method public sendSocketIsNotConnectedEvent()V
    .locals 2

    .prologue
    .line 78
    new-instance v0, Lcom/samsung/android/allshare/EventSync;

    invoke-direct {v0}, Lcom/samsung/android/allshare/EventSync;-><init>()V

    .line 79
    .local v0, "event":Lcom/samsung/android/allshare/EventSync;
    const/16 v1, 0x270f

    iput v1, v0, Lcom/samsung/android/allshare/EventSync;->mWhat:I

    .line 81
    iget-object v1, p0, Lcom/samsung/android/allshare/IAppControlAPI;->mEventListener:Lcom/samsung/android/allshare/IAppControlAPI$IControlEventListener;

    if-eqz v1, :cond_0

    .line 82
    iget-object v1, p0, Lcom/samsung/android/allshare/IAppControlAPI;->mEventListener:Lcom/samsung/android/allshare/IAppControlAPI$IControlEventListener;

    invoke-interface {v1, v0}, Lcom/samsung/android/allshare/IAppControlAPI$IControlEventListener;->controlEvent(Lcom/samsung/android/allshare/EventSync;)V

    .line 84
    :cond_0
    return-void
.end method

.method public sendTouchGestureEvent(IIIII)V
    .locals 3
    .param p1, "nType"    # I
    .param p2, "x"    # I
    .param p3, "y"    # I
    .param p4, "dx"    # I
    .param p5, "dy"    # I

    .prologue
    .line 404
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v1

    .line 406
    .local v1, "msg":Landroid/os/Message;
    const/4 v2, 0x5

    iput v2, v1, Landroid/os/Message;->what:I

    .line 408
    new-instance v0, Lcom/samsung/android/allshare/EventTouch;

    invoke-direct {v0}, Lcom/samsung/android/allshare/EventTouch;-><init>()V

    .line 409
    .local v0, "ieventTouch":Lcom/samsung/android/allshare/EventTouch;
    iput p1, v0, Lcom/samsung/android/allshare/EventTouch;->mType:I

    .line 410
    iput p2, v0, Lcom/samsung/android/allshare/EventTouch;->mX:I

    .line 411
    iput p3, v0, Lcom/samsung/android/allshare/EventTouch;->mY:I

    .line 412
    iput p4, v0, Lcom/samsung/android/allshare/EventTouch;->mDX:I

    .line 413
    iput p5, v0, Lcom/samsung/android/allshare/EventTouch;->mDY:I

    .line 414
    iput-object v0, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 416
    invoke-direct {p0, v1}, Lcom/samsung/android/allshare/IAppControlAPI;->sendMsgToDevice(Landroid/os/Message;)V

    .line 417
    return-void
.end method

.method public setOnEventListener(Lcom/samsung/android/allshare/IAppControlAPI$IControlEventListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/android/allshare/IAppControlAPI$IControlEventListener;

    .prologue
    .line 87
    iput-object p1, p0, Lcom/samsung/android/allshare/IAppControlAPI;->mEventListener:Lcom/samsung/android/allshare/IAppControlAPI$IControlEventListener;

    .line 88
    return-void
.end method

.method public setTouchGestureTouchMode(I)V
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 329
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 330
    .local v0, "msg":Landroid/os/Message;
    const/16 v1, 0x9

    iput v1, v0, Landroid/os/Message;->what:I

    .line 331
    iput p1, v0, Landroid/os/Message;->arg1:I

    .line 332
    invoke-direct {p0, v0}, Lcom/samsung/android/allshare/IAppControlAPI;->sendMsgToDevice(Landroid/os/Message;)V

    .line 333
    return-void
.end method

.method public startControl(Lcom/samsung/android/allshare/NetworkSocketInfo;Ljava/lang/String;)V
    .locals 2
    .param p1, "netinfo"    # Lcom/samsung/android/allshare/NetworkSocketInfo;
    .param p2, "devicdid"    # Ljava/lang/String;

    .prologue
    .line 151
    iget-object v0, p1, Lcom/samsung/android/allshare/NetworkSocketInfo;->mIpAddress:Ljava/lang/String;

    iget v1, p1, Lcom/samsung/android/allshare/NetworkSocketInfo;->mPort:I

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/allshare/IAppControlAPI;->setIpaddress(Ljava/lang/String;I)V

    .line 153
    invoke-direct {p0, p2}, Lcom/samsung/android/allshare/IAppControlAPI;->initControl(Ljava/lang/String;)V

    .line 154
    iget-object v0, p0, Lcom/samsung/android/allshare/IAppControlAPI;->mTvMsgSender:Lcom/samsung/android/allshare/TVMessageSender;

    if-eqz v0, :cond_1

    .line 156
    iget-object v0, p0, Lcom/samsung/android/allshare/IAppControlAPI;->mTvMsgListener:Lcom/samsung/android/allshare/TVMessageListener;

    if-nez v0, :cond_0

    .line 157
    iget-object v0, p0, Lcom/samsung/android/allshare/IAppControlAPI;->mTvMsgSender:Lcom/samsung/android/allshare/TVMessageSender;

    invoke-direct {p0, v0}, Lcom/samsung/android/allshare/IAppControlAPI;->initControlReceiver(Lcom/samsung/android/allshare/TVMessageSender;)V

    .line 159
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/allshare/IAppControlAPI;->mExcutor:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/samsung/android/allshare/IAppControlAPI$1;

    invoke-direct {v1, p0, p1}, Lcom/samsung/android/allshare/IAppControlAPI$1;-><init>(Lcom/samsung/android/allshare/IAppControlAPI;Lcom/samsung/android/allshare/NetworkSocketInfo;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 168
    :cond_1
    return-void
.end method

.method public startControl(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 4
    .param p1, "mac"    # Ljava/lang/String;
    .param p2, "ipaddress"    # Ljava/lang/String;
    .param p3, "deviceid"    # Ljava/lang/String;

    .prologue
    const v2, 0xd6d8

    const/4 v1, 0x1

    .line 178
    iput-boolean v1, p0, Lcom/samsung/android/allshare/IAppControlAPI;->bIsRunningConnection:Z

    .line 180
    new-instance v0, Lcom/samsung/android/allshare/NetworkSocketInfo;

    invoke-direct {v0}, Lcom/samsung/android/allshare/NetworkSocketInfo;-><init>()V

    .line 183
    .local v0, "inet":Lcom/samsung/android/allshare/NetworkSocketInfo;
    invoke-direct {p0, p2, v2}, Lcom/samsung/android/allshare/IAppControlAPI;->setIpaddress(Ljava/lang/String;I)V

    .line 184
    iput-object p1, v0, Lcom/samsung/android/allshare/NetworkSocketInfo;->mMacAddr:Ljava/lang/String;

    .line 186
    iput-object p2, v0, Lcom/samsung/android/allshare/NetworkSocketInfo;->mIpAddress:Ljava/lang/String;

    .line 187
    iput v2, v0, Lcom/samsung/android/allshare/NetworkSocketInfo;->mPort:I

    .line 188
    iput v1, v0, Lcom/samsung/android/allshare/NetworkSocketInfo;->mProtocol:I

    .line 190
    invoke-direct {p0, p3}, Lcom/samsung/android/allshare/IAppControlAPI;->initControl(Ljava/lang/String;)V

    .line 192
    iget-object v2, p0, Lcom/samsung/android/allshare/IAppControlAPI;->mTvMsgSender:Lcom/samsung/android/allshare/TVMessageSender;

    if-eqz v2, :cond_0

    .line 194
    iget-object v2, p0, Lcom/samsung/android/allshare/IAppControlAPI;->mExcutor:Ljava/util/concurrent/ExecutorService;

    new-instance v3, Lcom/samsung/android/allshare/IAppControlAPI$2;

    invoke-direct {v3, p0, v0}, Lcom/samsung/android/allshare/IAppControlAPI$2;-><init>(Lcom/samsung/android/allshare/IAppControlAPI;Lcom/samsung/android/allshare/NetworkSocketInfo;)V

    invoke-interface {v2, v3}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 212
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public startControlClient(Lcom/samsung/android/allshare/NetworkSocketInfo;)I
    .locals 3
    .param p1, "netinfo"    # Lcom/samsung/android/allshare/NetworkSocketInfo;

    .prologue
    const/4 v0, 0x1

    .line 297
    iget-object v1, p1, Lcom/samsung/android/allshare/NetworkSocketInfo;->mIpAddress:Ljava/lang/String;

    iget v2, p1, Lcom/samsung/android/allshare/NetworkSocketInfo;->mPort:I

    invoke-direct {p0, v1, v2}, Lcom/samsung/android/allshare/IAppControlAPI;->setIpaddress(Ljava/lang/String;I)V

    .line 303
    iget v1, p1, Lcom/samsung/android/allshare/NetworkSocketInfo;->mProtocol:I

    if-ne v1, v0, :cond_0

    .line 304
    iget-object v1, p0, Lcom/samsung/android/allshare/IAppControlAPI;->mDeviceId:Ljava/lang/String;

    const-string v2, "D0000000001"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/allshare/IAppControlAPI;->mTvMsgSender:Lcom/samsung/android/allshare/TVMessageSender;

    if-eqz v1, :cond_0

    .line 305
    iget-object v1, p0, Lcom/samsung/android/allshare/IAppControlAPI;->mExcutor:Ljava/util/concurrent/ExecutorService;

    new-instance v2, Lcom/samsung/android/allshare/IAppControlAPI$3;

    invoke-direct {v2, p0, p1}, Lcom/samsung/android/allshare/IAppControlAPI$3;-><init>(Lcom/samsung/android/allshare/IAppControlAPI;Lcom/samsung/android/allshare/NetworkSocketInfo;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 322
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public stopControl()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 230
    iget-boolean v1, p0, Lcom/samsung/android/allshare/IAppControlAPI;->bIsRunningConnection:Z

    if-ne v1, v0, :cond_0

    .line 231
    const/4 v0, 0x0

    .line 241
    :goto_0
    return v0

    .line 234
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/allshare/IAppControlAPI;->removeControl()V

    .line 236
    iget-object v1, p0, Lcom/samsung/android/allshare/IAppControlAPI;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 237
    const/4 v2, 0x0

    :try_start_0
    iput-object v2, p0, Lcom/samsung/android/allshare/IAppControlAPI;->mTvMsgSender:Lcom/samsung/android/allshare/TVMessageSender;

    .line 238
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/samsung/android/allshare/IAppControlAPI;->mTvMsgListener:Lcom/samsung/android/allshare/TVMessageListener;

    .line 239
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
