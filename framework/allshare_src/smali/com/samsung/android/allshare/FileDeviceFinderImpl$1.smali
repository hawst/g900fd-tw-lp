.class Lcom/samsung/android/allshare/FileDeviceFinderImpl$1;
.super Lcom/samsung/android/allshare/AllShareEventHandler;
.source "FileDeviceFinderImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/allshare/FileDeviceFinderImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/allshare/FileDeviceFinderImpl;


# direct methods
.method constructor <init>(Lcom/samsung/android/allshare/FileDeviceFinderImpl;Landroid/os/Looper;)V
    .locals 0
    .param p2, "x0"    # Landroid/os/Looper;

    .prologue
    .line 98
    iput-object p1, p0, Lcom/samsung/android/allshare/FileDeviceFinderImpl$1;->this$0:Lcom/samsung/android/allshare/FileDeviceFinderImpl;

    invoke-direct {p0, p2}, Lcom/samsung/android/allshare/AllShareEventHandler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleEventMessage(Lcom/sec/android/allshare/iface/CVMessage;)V
    .locals 14
    .param p1, "cvm"    # Lcom/sec/android/allshare/iface/CVMessage;

    .prologue
    .line 103
    invoke-virtual {p1}, Lcom/sec/android/allshare/iface/CVMessage;->getEventID()Ljava/lang/String;

    move-result-object v8

    .line 105
    .local v8, "evt_id":Ljava/lang/String;
    const/4 v9, 0x0

    .line 108
    .local v9, "listener":Lcom/samsung/android/allshare/DeviceFinder$IDeviceFinderEventListener;
    :try_start_0
    iget-object v11, p0, Lcom/samsung/android/allshare/FileDeviceFinderImpl$1;->this$0:Lcom/samsung/android/allshare/FileDeviceFinderImpl;

    # getter for: Lcom/samsung/android/allshare/FileDeviceFinderImpl;->mDiscoveryListenerMap:Ljava/util/HashMap;
    invoke-static {v11}, Lcom/samsung/android/allshare/FileDeviceFinderImpl;->access$000(Lcom/samsung/android/allshare/FileDeviceFinderImpl;)Ljava/util/HashMap;

    move-result-object v11

    invoke-virtual {v11, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    move-object v0, v11

    check-cast v0, Lcom/samsung/android/allshare/DeviceFinder$IDeviceFinderEventListener;

    move-object v9, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 113
    :goto_0
    # getter for: Lcom/samsung/android/allshare/FileDeviceFinderImpl;->mDeviceEventToDeviceTypeMap:Ljava/util/HashMap;
    invoke-static {}, Lcom/samsung/android/allshare/FileDeviceFinderImpl;->access$100()Ljava/util/HashMap;

    move-result-object v11

    invoke-virtual {v11, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/allshare/Device$DeviceType;

    .line 114
    .local v3, "deviceType":Lcom/samsung/android/allshare/Device$DeviceType;
    invoke-virtual {p1}, Lcom/sec/android/allshare/iface/CVMessage;->getBundle()Landroid/os/Bundle;

    move-result-object v10

    .line 115
    .local v10, "msgBundle":Landroid/os/Bundle;
    const-string v11, "BUNDLE_STRING_TYPE"

    invoke-virtual {v10, v11}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 116
    .local v7, "eventType":Ljava/lang/String;
    const-string v11, "BUNDLE_PARCELABLE_DEVICE"

    invoke-virtual {v10, v11}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Landroid/os/Bundle;

    .line 118
    .local v2, "deviceBundle":Landroid/os/Bundle;
    if-nez v2, :cond_1

    .line 119
    const-string v11, "FileDeviceFinderImpl"

    const-string v12, "mEventHandler.handleEventMessage : deviceBundle is null"

    invoke-static {v11, v12}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 157
    :cond_0
    :goto_1
    return-void

    .line 109
    .end local v2    # "deviceBundle":Landroid/os/Bundle;
    .end local v3    # "deviceType":Lcom/samsung/android/allshare/Device$DeviceType;
    .end local v7    # "eventType":Ljava/lang/String;
    .end local v10    # "msgBundle":Landroid/os/Bundle;
    :catch_0
    move-exception v4

    .line 110
    .local v4, "e":Ljava/lang/Exception;
    const-string v11, "FileDeviceFinderImpl"

    const-string v12, "mEventHandler.handleEventMessage : Exception"

    invoke-static {v11, v12, v4}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0

    .line 123
    .end local v4    # "e":Ljava/lang/Exception;
    .restart local v2    # "deviceBundle":Landroid/os/Bundle;
    .restart local v3    # "deviceType":Lcom/samsung/android/allshare/Device$DeviceType;
    .restart local v7    # "eventType":Ljava/lang/String;
    .restart local v10    # "msgBundle":Landroid/os/Bundle;
    :cond_1
    iget-object v11, p0, Lcom/samsung/android/allshare/FileDeviceFinderImpl$1;->this$0:Lcom/samsung/android/allshare/FileDeviceFinderImpl;

    # invokes: Lcom/samsung/android/allshare/FileDeviceFinderImpl;->getDeviceFromMap(Landroid/os/Bundle;Lcom/samsung/android/allshare/Device$DeviceType;)Lcom/samsung/android/allshare/Device;
    invoke-static {v11, v2, v3}, Lcom/samsung/android/allshare/FileDeviceFinderImpl;->access$200(Lcom/samsung/android/allshare/FileDeviceFinderImpl;Landroid/os/Bundle;Lcom/samsung/android/allshare/Device$DeviceType;)Lcom/samsung/android/allshare/Device;

    move-result-object v1

    .line 124
    .local v1, "device":Lcom/samsung/android/allshare/Device;
    if-nez v1, :cond_2

    .line 125
    const-string v11, "FileDeviceFinderImpl"

    const-string v12, "mEventHandler.handleEventMessage : device is null"

    invoke-static {v11, v12}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 129
    :cond_2
    const-string v11, "ADDED"

    invoke-virtual {v11, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 131
    if-eqz v9, :cond_0

    .line 132
    :try_start_1
    sget-object v11, Lcom/samsung/android/allshare/ERROR;->SUCCESS:Lcom/samsung/android/allshare/ERROR;

    invoke-interface {v9, v3, v1, v11}, Lcom/samsung/android/allshare/DeviceFinder$IDeviceFinderEventListener;->onDeviceAdded(Lcom/samsung/android/allshare/Device$DeviceType;Lcom/samsung/android/allshare/Device;Lcom/samsung/android/allshare/ERROR;)V

    .line 133
    const-string v11, "FileDeviceFinderImpl"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "[ADDED] "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/samsung/android/allshare/DLog;->i_api(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Error; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_1

    .line 135
    :catch_1
    move-exception v4

    .line 136
    .restart local v4    # "e":Ljava/lang/Exception;
    const-string v11, "FileDeviceFinderImpl"

    const-string v12, "[ADDED] Exception"

    invoke-static {v11, v12, v4}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_1

    .line 137
    .end local v4    # "e":Ljava/lang/Exception;
    :catch_2
    move-exception v5

    .line 138
    .local v5, "err":Ljava/lang/Error;
    const-string v11, "FileDeviceFinderImpl"

    const-string v12, "[ADDED] Error"

    invoke-static {v11, v12, v5}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Error;)V

    goto :goto_1

    .line 140
    .end local v5    # "err":Ljava/lang/Error;
    :cond_3
    const-string v11, "REMOVED"

    invoke-virtual {v11, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 142
    :try_start_2
    iget-object v11, p0, Lcom/samsung/android/allshare/FileDeviceFinderImpl$1;->this$0:Lcom/samsung/android/allshare/FileDeviceFinderImpl;

    # invokes: Lcom/samsung/android/allshare/FileDeviceFinderImpl;->removeDeviceFromMap(Landroid/os/Bundle;Lcom/samsung/android/allshare/Device$DeviceType;)V
    invoke-static {v11, v2, v3}, Lcom/samsung/android/allshare/FileDeviceFinderImpl;->access$300(Lcom/samsung/android/allshare/FileDeviceFinderImpl;Landroid/os/Bundle;Lcom/samsung/android/allshare/Device$DeviceType;)V

    .line 143
    const-string v11, "BUNDLE_ENUM_ERROR"

    invoke-virtual {v10, v11}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Lcom/samsung/android/allshare/ERROR;->stringToEnum(Ljava/lang/String;)Lcom/samsung/android/allshare/ERROR;

    move-result-object v6

    .line 144
    .local v6, "error":Lcom/samsung/android/allshare/ERROR;
    if-eqz v9, :cond_0

    .line 145
    invoke-interface {v9, v3, v1, v6}, Lcom/samsung/android/allshare/DeviceFinder$IDeviceFinderEventListener;->onDeviceRemoved(Lcom/samsung/android/allshare/Device$DeviceType;Lcom/samsung/android/allshare/Device;Lcom/samsung/android/allshare/ERROR;)V

    .line 146
    const-string v11, "FileDeviceFinderImpl"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "[REMOVED] "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/samsung/android/allshare/DLog;->i_api(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/lang/Error; {:try_start_2 .. :try_end_2} :catch_4

    goto/16 :goto_1

    .line 148
    .end local v6    # "error":Lcom/samsung/android/allshare/ERROR;
    :catch_3
    move-exception v4

    .line 149
    .restart local v4    # "e":Ljava/lang/Exception;
    const-string v11, "FileDeviceFinderImpl"

    const-string v12, "[REMOVED] Exception"

    invoke-static {v11, v12, v4}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto/16 :goto_1

    .line 150
    .end local v4    # "e":Ljava/lang/Exception;
    :catch_4
    move-exception v5

    .line 151
    .restart local v5    # "err":Ljava/lang/Error;
    const-string v11, "FileDeviceFinderImpl"

    const-string v12, "[REMOVED] Exception"

    invoke-static {v11, v12, v5}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Error;)V

    goto/16 :goto_1

    .line 154
    .end local v5    # "err":Ljava/lang/Error;
    :cond_4
    const-string v11, "FileDeviceFinderImpl"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "mEventHandler.handleEventMessage : eventType="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1
.end method
