.class public final enum Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;
.super Ljava/lang/Enum;
.source "Item.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/allshare/Item$WebContentBuilder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "DeliveryMode"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;

.field public static final enum REDIRECT:Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;

.field public static final enum RELAY:Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;

.field public static final enum UNKNOWN:Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;


# instance fields
.field private final enumString:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1136
    new-instance v0, Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;

    const-string v1, "RELAY"

    const-string v2, "RELAY"

    invoke-direct {v0, v1, v3, v2}, Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;->RELAY:Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;

    .line 1142
    new-instance v0, Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;

    const-string v1, "REDIRECT"

    const-string v2, "REDIRECT"

    invoke-direct {v0, v1, v4, v2}, Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;->REDIRECT:Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;

    .line 1146
    new-instance v0, Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;

    const-string v1, "UNKNOWN"

    const-string v2, "UNKNOWN"

    invoke-direct {v0, v1, v5, v2}, Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;->UNKNOWN:Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;

    .line 1131
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;

    sget-object v1, Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;->RELAY:Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;->REDIRECT:Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;->UNKNOWN:Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;

    aput-object v1, v0, v5

    sput-object v0, Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;->$VALUES:[Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "enumStr"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1152
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1153
    iput-object p3, p0, Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;->enumString:Ljava/lang/String;

    .line 1154
    return-void
.end method

.method public static stringToEnum(Ljava/lang/String;)Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;
    .locals 1
    .param p0, "enumStr"    # Ljava/lang/String;

    .prologue
    .line 1165
    if-nez p0, :cond_0

    .line 1166
    sget-object v0, Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;->UNKNOWN:Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;

    .line 1175
    :goto_0
    return-object v0

    .line 1168
    :cond_0
    const-string v0, "REDIRECT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1169
    sget-object v0, Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;->REDIRECT:Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;

    goto :goto_0

    .line 1170
    :cond_1
    const-string v0, "RELAY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1171
    sget-object v0, Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;->RELAY:Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;

    goto :goto_0

    .line 1172
    :cond_2
    const-string v0, "UNKNOWN"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1173
    sget-object v0, Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;->UNKNOWN:Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;

    goto :goto_0

    .line 1175
    :cond_3
    sget-object v0, Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;->UNKNOWN:Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 1131
    const-class v0, Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;

    return-object v0
.end method

.method public static values()[Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;
    .locals 1

    .prologue
    .line 1131
    sget-object v0, Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;->$VALUES:[Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;

    invoke-virtual {v0}, [Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;

    return-object v0
.end method


# virtual methods
.method public enumToString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1159
    iget-object v0, p0, Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;->enumString:Ljava/lang/String;

    return-object v0
.end method
