.class Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;
.super Lcom/samsung/android/allshare/Item;
.source "Item.java"

# interfaces
.implements Lcom/sec/android/allshare/iface/IBundleHolder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/allshare/Item;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "BuilderGeneratedItem"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mAlbumTitle:Ljava/lang/String;

.field private mArtist:Ljava/lang/String;

.field private mConType:Lcom/samsung/android/allshare/ItemCreator$ConstructorType;

.field private mDate:Ljava/util/Date;

.field private mDeliveryMode:Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;

.field private mDuration:J

.field private mGenre:Ljava/lang/String;

.field private mItemFilepath:Ljava/lang/String;

.field private mItemMimetype:Ljava/lang/String;

.field private mItemTitle:Ljava/lang/String;

.field private mSubtitlePath:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 967
    new-instance v0, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem$1;

    invoke-direct {v0}, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem$1;-><init>()V

    sput-object v0, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "src"    # Landroid/os/Parcel;

    .prologue
    const/4 v1, 0x0

    .line 979
    invoke-direct {p0}, Lcom/samsung/android/allshare/Item;-><init>()V

    .line 753
    sget-object v0, Lcom/samsung/android/allshare/ItemCreator$ConstructorType;->UNKNOWN:Lcom/samsung/android/allshare/ItemCreator$ConstructorType;

    iput-object v0, p0, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;->mConType:Lcom/samsung/android/allshare/ItemCreator$ConstructorType;

    .line 755
    sget-object v0, Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;->UNKNOWN:Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;

    iput-object v0, p0, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;->mDeliveryMode:Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;

    .line 759
    iput-object v1, p0, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;->mArtist:Ljava/lang/String;

    .line 761
    iput-object v1, p0, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;->mAlbumTitle:Ljava/lang/String;

    .line 763
    iput-object v1, p0, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;->mGenre:Ljava/lang/String;

    .line 765
    iput-object v1, p0, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;->mDate:Ljava/util/Date;

    .line 767
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;->mDuration:J

    .line 980
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;->readFromParcel(Landroid/os/Parcel;)V

    .line 981
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/samsung/android/allshare/Item$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/samsung/android/allshare/Item$1;

    .prologue
    .line 746
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method private constructor <init>(Lcom/samsung/android/allshare/ItemCreator$ConstructorType;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;)V
    .locals 2
    .param p1, "conType"    # Lcom/samsung/android/allshare/ItemCreator$ConstructorType;
    .param p2, "uri"    # Landroid/net/Uri;
    .param p3, "title"    # Ljava/lang/String;
    .param p4, "mimeType"    # Ljava/lang/String;
    .param p5, "subtitlePath"    # Ljava/lang/String;
    .param p6, "deliveryMode"    # Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;

    .prologue
    const/4 v1, 0x0

    .line 780
    invoke-direct {p0}, Lcom/samsung/android/allshare/Item;-><init>()V

    .line 753
    sget-object v0, Lcom/samsung/android/allshare/ItemCreator$ConstructorType;->UNKNOWN:Lcom/samsung/android/allshare/ItemCreator$ConstructorType;

    iput-object v0, p0, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;->mConType:Lcom/samsung/android/allshare/ItemCreator$ConstructorType;

    .line 755
    sget-object v0, Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;->UNKNOWN:Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;

    iput-object v0, p0, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;->mDeliveryMode:Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;

    .line 759
    iput-object v1, p0, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;->mArtist:Ljava/lang/String;

    .line 761
    iput-object v1, p0, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;->mAlbumTitle:Ljava/lang/String;

    .line 763
    iput-object v1, p0, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;->mGenre:Ljava/lang/String;

    .line 765
    iput-object v1, p0, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;->mDate:Ljava/util/Date;

    .line 767
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;->mDuration:J

    .line 781
    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;->mItemFilepath:Ljava/lang/String;

    .line 782
    iput-object p4, p0, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;->mItemMimetype:Ljava/lang/String;

    .line 783
    iput-object p3, p0, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;->mItemTitle:Ljava/lang/String;

    .line 784
    iput-object p1, p0, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;->mConType:Lcom/samsung/android/allshare/ItemCreator$ConstructorType;

    .line 785
    iput-object p6, p0, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;->mDeliveryMode:Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;

    .line 786
    iput-object p5, p0, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;->mSubtitlePath:Ljava/lang/String;

    .line 787
    return-void
.end method

.method private constructor <init>(Lcom/samsung/android/allshare/ItemCreator$ConstructorType;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;J)V
    .locals 3
    .param p1, "conType"    # Lcom/samsung/android/allshare/ItemCreator$ConstructorType;
    .param p2, "uri"    # Landroid/net/Uri;
    .param p3, "title"    # Ljava/lang/String;
    .param p4, "mimeType"    # Ljava/lang/String;
    .param p5, "subtitlePath"    # Ljava/lang/String;
    .param p6, "deliveryMode"    # Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;
    .param p7, "artist"    # Ljava/lang/String;
    .param p8, "albumTitle"    # Ljava/lang/String;
    .param p9, "genre"    # Ljava/lang/String;
    .param p10, "date"    # Ljava/util/Date;
    .param p11, "duration"    # J

    .prologue
    const/4 v1, 0x0

    .line 791
    invoke-direct {p0}, Lcom/samsung/android/allshare/Item;-><init>()V

    .line 753
    sget-object v0, Lcom/samsung/android/allshare/ItemCreator$ConstructorType;->UNKNOWN:Lcom/samsung/android/allshare/ItemCreator$ConstructorType;

    iput-object v0, p0, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;->mConType:Lcom/samsung/android/allshare/ItemCreator$ConstructorType;

    .line 755
    sget-object v0, Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;->UNKNOWN:Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;

    iput-object v0, p0, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;->mDeliveryMode:Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;

    .line 759
    iput-object v1, p0, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;->mArtist:Ljava/lang/String;

    .line 761
    iput-object v1, p0, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;->mAlbumTitle:Ljava/lang/String;

    .line 763
    iput-object v1, p0, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;->mGenre:Ljava/lang/String;

    .line 765
    iput-object v1, p0, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;->mDate:Ljava/util/Date;

    .line 767
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;->mDuration:J

    .line 792
    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;->mItemFilepath:Ljava/lang/String;

    .line 793
    iput-object p4, p0, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;->mItemMimetype:Ljava/lang/String;

    .line 794
    iput-object p3, p0, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;->mItemTitle:Ljava/lang/String;

    .line 795
    iput-object p1, p0, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;->mConType:Lcom/samsung/android/allshare/ItemCreator$ConstructorType;

    .line 796
    iput-object p6, p0, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;->mDeliveryMode:Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;

    .line 797
    iput-object p5, p0, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;->mSubtitlePath:Ljava/lang/String;

    .line 798
    iput-object p7, p0, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;->mArtist:Ljava/lang/String;

    .line 799
    iput-object p8, p0, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;->mAlbumTitle:Ljava/lang/String;

    .line 800
    iput-object p9, p0, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;->mGenre:Ljava/lang/String;

    .line 801
    iput-object p10, p0, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;->mDate:Ljava/util/Date;

    .line 802
    iput-wide p11, p0, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;->mDuration:J

    .line 803
    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/allshare/ItemCreator$ConstructorType;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;JLcom/samsung/android/allshare/Item$1;)V
    .locals 1
    .param p1, "x0"    # Lcom/samsung/android/allshare/ItemCreator$ConstructorType;
    .param p2, "x1"    # Landroid/net/Uri;
    .param p3, "x2"    # Ljava/lang/String;
    .param p4, "x3"    # Ljava/lang/String;
    .param p5, "x4"    # Ljava/lang/String;
    .param p6, "x5"    # Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;
    .param p7, "x6"    # Ljava/lang/String;
    .param p8, "x7"    # Ljava/lang/String;
    .param p9, "x8"    # Ljava/lang/String;
    .param p10, "x9"    # Ljava/util/Date;
    .param p11, "x10"    # J
    .param p13, "x11"    # Lcom/samsung/android/allshare/Item$1;

    .prologue
    .line 746
    invoke-direct/range {p0 .. p12}, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;-><init>(Lcom/samsung/android/allshare/ItemCreator$ConstructorType;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;J)V

    return-void
.end method

.method private constructor <init>(Lcom/samsung/android/allshare/ItemCreator$ConstructorType;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/Item$WebContentBuilder$PlayMode;)V
    .locals 2
    .param p1, "conType"    # Lcom/samsung/android/allshare/ItemCreator$ConstructorType;
    .param p2, "uri"    # Landroid/net/Uri;
    .param p3, "title"    # Ljava/lang/String;
    .param p4, "mimeType"    # Ljava/lang/String;
    .param p5, "subtitlePath"    # Ljava/lang/String;
    .param p6, "playMode"    # Lcom/samsung/android/allshare/Item$WebContentBuilder$PlayMode;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 808
    invoke-direct {p0}, Lcom/samsung/android/allshare/Item;-><init>()V

    .line 753
    sget-object v0, Lcom/samsung/android/allshare/ItemCreator$ConstructorType;->UNKNOWN:Lcom/samsung/android/allshare/ItemCreator$ConstructorType;

    iput-object v0, p0, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;->mConType:Lcom/samsung/android/allshare/ItemCreator$ConstructorType;

    .line 755
    sget-object v0, Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;->UNKNOWN:Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;

    iput-object v0, p0, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;->mDeliveryMode:Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;

    .line 759
    iput-object v1, p0, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;->mArtist:Ljava/lang/String;

    .line 761
    iput-object v1, p0, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;->mAlbumTitle:Ljava/lang/String;

    .line 763
    iput-object v1, p0, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;->mGenre:Ljava/lang/String;

    .line 765
    iput-object v1, p0, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;->mDate:Ljava/util/Date;

    .line 767
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;->mDuration:J

    .line 809
    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;->mItemFilepath:Ljava/lang/String;

    .line 810
    iput-object p4, p0, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;->mItemMimetype:Ljava/lang/String;

    .line 811
    iput-object p3, p0, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;->mItemTitle:Ljava/lang/String;

    .line 812
    iput-object p1, p0, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;->mConType:Lcom/samsung/android/allshare/ItemCreator$ConstructorType;

    .line 813
    iput-object p5, p0, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;->mSubtitlePath:Ljava/lang/String;

    .line 815
    sget-object v0, Lcom/samsung/android/allshare/Item$WebContentBuilder$PlayMode;->REDIRECT:Lcom/samsung/android/allshare/Item$WebContentBuilder$PlayMode;

    if-ne p6, v0, :cond_0

    .line 816
    sget-object v0, Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;->REDIRECT:Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;

    iput-object v0, p0, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;->mDeliveryMode:Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;

    .line 821
    :goto_0
    return-void

    .line 817
    :cond_0
    sget-object v0, Lcom/samsung/android/allshare/Item$WebContentBuilder$PlayMode;->RELAY:Lcom/samsung/android/allshare/Item$WebContentBuilder$PlayMode;

    if-ne p6, v0, :cond_1

    .line 818
    sget-object v0, Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;->RELAY:Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;

    iput-object v0, p0, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;->mDeliveryMode:Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;

    goto :goto_0

    .line 820
    :cond_1
    sget-object v0, Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;->UNKNOWN:Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;

    iput-object v0, p0, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;->mDeliveryMode:Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;

    goto :goto_0
.end method

.method private constructor <init>(Lcom/samsung/android/allshare/ItemCreator$ConstructorType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "conType"    # Lcom/samsung/android/allshare/ItemCreator$ConstructorType;
    .param p2, "filepath"    # Ljava/lang/String;
    .param p3, "title"    # Ljava/lang/String;
    .param p4, "subtitlePath"    # Ljava/lang/String;
    .param p5, "mimeType"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 770
    invoke-direct {p0}, Lcom/samsung/android/allshare/Item;-><init>()V

    .line 753
    sget-object v0, Lcom/samsung/android/allshare/ItemCreator$ConstructorType;->UNKNOWN:Lcom/samsung/android/allshare/ItemCreator$ConstructorType;

    iput-object v0, p0, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;->mConType:Lcom/samsung/android/allshare/ItemCreator$ConstructorType;

    .line 755
    sget-object v0, Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;->UNKNOWN:Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;

    iput-object v0, p0, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;->mDeliveryMode:Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;

    .line 759
    iput-object v1, p0, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;->mArtist:Ljava/lang/String;

    .line 761
    iput-object v1, p0, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;->mAlbumTitle:Ljava/lang/String;

    .line 763
    iput-object v1, p0, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;->mGenre:Ljava/lang/String;

    .line 765
    iput-object v1, p0, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;->mDate:Ljava/util/Date;

    .line 767
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;->mDuration:J

    .line 771
    iput-object p2, p0, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;->mItemFilepath:Ljava/lang/String;

    .line 772
    iput-object p5, p0, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;->mItemMimetype:Ljava/lang/String;

    .line 773
    iput-object p3, p0, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;->mItemTitle:Ljava/lang/String;

    .line 774
    iput-object p1, p0, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;->mConType:Lcom/samsung/android/allshare/ItemCreator$ConstructorType;

    .line 775
    sget-object v0, Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;->UNKNOWN:Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;

    iput-object v0, p0, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;->mDeliveryMode:Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;

    .line 776
    iput-object p4, p0, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;->mSubtitlePath:Ljava/lang/String;

    .line 777
    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/allshare/ItemCreator$ConstructorType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/Item$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/android/allshare/ItemCreator$ConstructorType;
    .param p2, "x1"    # Ljava/lang/String;
    .param p3, "x2"    # Ljava/lang/String;
    .param p4, "x3"    # Ljava/lang/String;
    .param p5, "x4"    # Ljava/lang/String;
    .param p6, "x5"    # Lcom/samsung/android/allshare/Item$1;

    .prologue
    .line 746
    invoke-direct/range {p0 .. p5}, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;-><init>(Lcom/samsung/android/allshare/ItemCreator$ConstructorType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private readFromParcel(Landroid/os/Parcel;)V
    .locals 3
    .param p1, "src"    # Landroid/os/Parcel;

    .prologue
    .line 956
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;->mItemFilepath:Ljava/lang/String;

    .line 957
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;->mItemMimetype:Ljava/lang/String;

    .line 958
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;->mItemTitle:Ljava/lang/String;

    .line 959
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 960
    .local v0, "conType":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 961
    .local v1, "deliveryMode":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;->mSubtitlePath:Ljava/lang/String;

    .line 963
    invoke-static {v0}, Lcom/samsung/android/allshare/ItemCreator$ConstructorType;->stringToEnum(Ljava/lang/String;)Lcom/samsung/android/allshare/ItemCreator$ConstructorType;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;->mConType:Lcom/samsung/android/allshare/ItemCreator$ConstructorType;

    .line 964
    invoke-static {v1}, Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;->stringToEnum(Ljava/lang/String;)Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;->mDeliveryMode:Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;

    .line 965
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 941
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 985
    if-ne p0, p1, :cond_1

    .line 995
    :cond_0
    :goto_0
    return v1

    .line 987
    :cond_1
    if-eqz p1, :cond_2

    instance-of v3, p1, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;

    if-nez v3, :cond_3

    :cond_2
    move v1, v2

    .line 988
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 990
    check-cast v0, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;

    .line 992
    .local v0, "item":Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;
    invoke-virtual {p0}, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;->getURI()Landroid/net/Uri;

    move-result-object v3

    if-nez v3, :cond_4

    .line 993
    invoke-virtual {v0}, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;->getURI()Landroid/net/Uri;

    move-result-object v3

    if-eqz v3, :cond_0

    move v1, v2

    goto :goto_0

    .line 995
    :cond_4
    invoke-virtual {p0}, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;->getURI()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0}, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;->getURI()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method

.method public getAlbumTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 878
    iget-object v0, p0, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;->mAlbumTitle:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;->mAlbumTitle:Ljava/lang/String;

    goto :goto_0
.end method

.method public getArtist()Ljava/lang/String;
    .locals 1

    .prologue
    .line 883
    iget-object v0, p0, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;->mArtist:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;->mArtist:Ljava/lang/String;

    goto :goto_0
.end method

.method public getBitrate()I
    .locals 1

    .prologue
    .line 1057
    const/4 v0, -0x1

    return v0
.end method

.method public getBundle()Landroid/os/Bundle;
    .locals 4

    .prologue
    .line 858
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 859
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v1, "BUNDLE_STRING_ITEM_TYPE"

    invoke-virtual {p0}, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;->getType()Lcom/samsung/android/allshare/Item$MediaType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/allshare/Item$MediaType;->enumToString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 860
    const-string v1, "BUNDLE_STRING_ITEM_TITLE"

    iget-object v2, p0, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;->mItemTitle:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 861
    const-string v1, "BUNDLE_STRING_FILEPATH"

    iget-object v2, p0, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;->mItemFilepath:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 862
    const-string v1, "BUNDLE_PARCELABLE_ITEM_URI"

    invoke-virtual {p0}, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;->getURI()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 863
    const-string v1, "BUNDLE_STRING_ITEM_MIMETYPE"

    iget-object v2, p0, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;->mItemMimetype:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 864
    const-string v1, "BUNDLE_STRING_ITEM_CONSTRUCTOR_KEY"

    iget-object v2, p0, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;->mConType:Lcom/samsung/android/allshare/ItemCreator$ConstructorType;

    invoke-virtual {v2}, Lcom/samsung/android/allshare/ItemCreator$ConstructorType;->enumToString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 865
    const-string v1, "BUNDLE_STRING_WEB_PLAY_MODE"

    iget-object v2, p0, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;->mDeliveryMode:Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;

    invoke-virtual {v2}, Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;->enumToString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 866
    const-string v1, "BUNDLE_STRING_ITEM_SUBTITLE_PATH"

    iget-object v2, p0, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;->mSubtitlePath:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 867
    const-string v1, "BUNDLE_STRING_ITEM_ARTIST"

    iget-object v2, p0, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;->mArtist:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 868
    const-string v1, "BUNDLE_STRING_ITEM_ALBUM_TITLE"

    iget-object v2, p0, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;->mAlbumTitle:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 869
    const-string v1, "BUNDLE_STRING_ITEM_GENRE"

    iget-object v2, p0, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;->mGenre:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 870
    const-string v1, "BUNDLE_DATE_ITEM_DATE"

    iget-object v2, p0, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;->mDate:Ljava/util/Date;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;->mDate:Ljava/util/Date;

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    :goto_0
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 871
    const-string v1, "BUNDLE_LONG_ITEM_DURATION"

    iget-wide v2, p0, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;->mDuration:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 873
    return-object v0

    .line 870
    :cond_0
    const-wide/16 v2, 0x0

    goto :goto_0
.end method

.method public getChannelNr()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1068
    const-string v0, ""

    return-object v0
.end method

.method public getContentBuildType()Lcom/samsung/android/allshare/Item$ContentBuildType;
    .locals 2

    .prologue
    .line 1010
    sget-object v0, Lcom/samsung/android/allshare/Item$1;->$SwitchMap$com$samsung$android$allshare$ItemCreator$ConstructorType:[I

    iget-object v1, p0, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;->mConType:Lcom/samsung/android/allshare/ItemCreator$ConstructorType;

    invoke-virtual {v1}, Lcom/samsung/android/allshare/ItemCreator$ConstructorType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1022
    sget-object v0, Lcom/samsung/android/allshare/Item$ContentBuildType;->UNKNOWN:Lcom/samsung/android/allshare/Item$ContentBuildType;

    :goto_0
    return-object v0

    .line 1012
    :pswitch_0
    sget-object v0, Lcom/samsung/android/allshare/Item$ContentBuildType;->LOCAL:Lcom/samsung/android/allshare/Item$ContentBuildType;

    goto :goto_0

    .line 1014
    :pswitch_1
    sget-object v0, Lcom/samsung/android/allshare/Item$ContentBuildType;->PROVIDER:Lcom/samsung/android/allshare/Item$ContentBuildType;

    goto :goto_0

    .line 1016
    :pswitch_2
    sget-object v0, Lcom/samsung/android/allshare/Item$ContentBuildType;->WEB:Lcom/samsung/android/allshare/Item$ContentBuildType;

    goto :goto_0

    .line 1018
    :pswitch_3
    sget-object v0, Lcom/samsung/android/allshare/Item$ContentBuildType;->UNKNOWN:Lcom/samsung/android/allshare/Item$ContentBuildType;

    goto :goto_0

    .line 1010
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public getDate()Ljava/util/Date;
    .locals 1

    .prologue
    .line 825
    iget-object v0, p0, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;->mDate:Ljava/util/Date;

    return-object v0
.end method

.method public getDuration()J
    .locals 2

    .prologue
    .line 893
    iget-wide v0, p0, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;->mDuration:J

    return-wide v0
.end method

.method public getExtension()Ljava/lang/String;
    .locals 1

    .prologue
    .line 936
    const-string v0, ""

    return-object v0
.end method

.method public getFileSize()J
    .locals 2

    .prologue
    .line 926
    const-wide/16 v0, -0x1

    return-wide v0
.end method

.method public getGenre()Ljava/lang/String;
    .locals 1

    .prologue
    .line 888
    iget-object v0, p0, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;->mGenre:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;->mGenre:Ljava/lang/String;

    goto :goto_0
.end method

.method public getLocation()Landroid/location/Location;
    .locals 1

    .prologue
    .line 913
    const/4 v0, 0x0

    return-object v0
.end method

.method public getMimetype()Ljava/lang/String;
    .locals 1

    .prologue
    .line 931
    iget-object v0, p0, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;->mItemMimetype:Ljava/lang/String;

    return-object v0
.end method

.method public getResolution()Ljava/lang/String;
    .locals 1

    .prologue
    .line 908
    const-string v0, ""

    return-object v0
.end method

.method public getResourceList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/Item$Resource;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1063
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    return-object v0
.end method

.method public getSeekMode()Lcom/samsung/android/allshare/Item$SeekMode;
    .locals 1

    .prologue
    .line 1052
    sget-object v0, Lcom/samsung/android/allshare/Item$SeekMode;->BYTE:Lcom/samsung/android/allshare/Item$SeekMode;

    return-object v0
.end method

.method public getSubtitle()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 918
    iget-object v0, p0, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;->mSubtitlePath:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 919
    const/4 v0, 0x0

    .line 921
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;->mSubtitlePath:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method public getSubtitleList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/Subtitle;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1045
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    return-object v0
.end method

.method public getThumbnail()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 903
    const/4 v0, 0x0

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 830
    iget-object v0, p0, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;->mItemTitle:Ljava/lang/String;

    return-object v0
.end method

.method public getType()Lcom/samsung/android/allshare/Item$MediaType;
    .locals 1

    .prologue
    .line 835
    iget-object v0, p0, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;->mItemMimetype:Ljava/lang/String;

    # invokes: Lcom/samsung/android/allshare/Item;->convertItemTypeFromMimeType(Ljava/lang/String;)Lcom/samsung/android/allshare/Item$MediaType;
    invoke-static {v0}, Lcom/samsung/android/allshare/Item;->access$100(Ljava/lang/String;)Lcom/samsung/android/allshare/Item$MediaType;

    move-result-object v0

    return-object v0
.end method

.method public getURI()Landroid/net/Uri;
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 840
    iget-object v4, p0, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;->mItemFilepath:Ljava/lang/String;

    if-nez v4, :cond_1

    move-object v2, v3

    .line 852
    :cond_0
    :goto_0
    return-object v2

    .line 844
    :cond_1
    :try_start_0
    iget-object v4, p0, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;->mItemFilepath:Ljava/lang/String;

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 845
    .local v2, "uri":Landroid/net/Uri;
    invoke-virtual {v2}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    .line 847
    .local v1, "scheme":Ljava/lang/String;
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 848
    :cond_2
    new-instance v4, Ljava/io/File;

    iget-object v5, p0, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;->mItemFilepath:Ljava/lang/String;

    invoke-direct {v4, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v4}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    goto :goto_0

    .line 851
    .end local v1    # "scheme":Ljava/lang/String;
    .end local v2    # "uri":Landroid/net/Uri;
    :catch_0
    move-exception v0

    .local v0, "e":Ljava/lang/Exception;
    move-object v2, v3

    .line 852
    goto :goto_0
.end method

.method public getWebContentDeliveryMode()Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;
    .locals 1

    .prologue
    .line 1027
    iget-object v0, p0, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;->mDeliveryMode:Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;

    return-object v0
.end method

.method public getWebContentPlayMode()Lcom/samsung/android/allshare/Item$WebContentBuilder$PlayMode;
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1034
    iget-object v0, p0, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;->mDeliveryMode:Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;

    sget-object v1, Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;->REDIRECT:Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;

    if-ne v0, v1, :cond_0

    .line 1035
    sget-object v0, Lcom/samsung/android/allshare/Item$WebContentBuilder$PlayMode;->REDIRECT:Lcom/samsung/android/allshare/Item$WebContentBuilder$PlayMode;

    .line 1039
    :goto_0
    return-object v0

    .line 1036
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;->mDeliveryMode:Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;

    sget-object v1, Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;->RELAY:Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;

    if-ne v0, v1, :cond_1

    .line 1037
    sget-object v0, Lcom/samsung/android/allshare/Item$WebContentBuilder$PlayMode;->RELAY:Lcom/samsung/android/allshare/Item$WebContentBuilder$PlayMode;

    goto :goto_0

    .line 1039
    :cond_1
    sget-object v0, Lcom/samsung/android/allshare/Item$WebContentBuilder$PlayMode;->UNKNOWN:Lcom/samsung/android/allshare/Item$WebContentBuilder$PlayMode;

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 1001
    invoke-virtual {p0}, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;->getURI()Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1002
    invoke-virtual {p0}, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;->getURI()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->hashCode()I

    move-result v0

    .line 1004
    :goto_0
    return v0

    :cond_0
    const-string v0, ""

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public isRootFolder()Z
    .locals 1

    .prologue
    .line 898
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 946
    iget-object v0, p0, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;->mItemFilepath:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 947
    iget-object v0, p0, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;->mItemMimetype:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 948
    iget-object v0, p0, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;->mItemTitle:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 949
    iget-object v0, p0, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;->mConType:Lcom/samsung/android/allshare/ItemCreator$ConstructorType;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/ItemCreator$ConstructorType;->enumToString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 950
    iget-object v0, p0, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;->mDeliveryMode:Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;->enumToString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 951
    iget-object v0, p0, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;->mSubtitlePath:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 952
    return-void
.end method
