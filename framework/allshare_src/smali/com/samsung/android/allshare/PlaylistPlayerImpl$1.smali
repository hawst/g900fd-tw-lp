.class Lcom/samsung/android/allshare/PlaylistPlayerImpl$1;
.super Lcom/samsung/android/allshare/AllShareEventHandler;
.source "PlaylistPlayerImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/allshare/PlaylistPlayerImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private mPlaylistStateMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/allshare/media/PlaylistPlayer$PlayerState;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/samsung/android/allshare/PlaylistPlayerImpl;


# direct methods
.method constructor <init>(Lcom/samsung/android/allshare/PlaylistPlayerImpl;Landroid/os/Looper;)V
    .locals 3
    .param p2, "x0"    # Landroid/os/Looper;

    .prologue
    .line 101
    iput-object p1, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl$1;->this$0:Lcom/samsung/android/allshare/PlaylistPlayerImpl;

    invoke-direct {p0, p2}, Lcom/samsung/android/allshare/AllShareEventHandler;-><init>(Landroid/os/Looper;)V

    .line 102
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl$1;->mPlaylistStateMap:Ljava/util/HashMap;

    .line 104
    iget-object v0, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl$1;->mPlaylistStateMap:Ljava/util/HashMap;

    const-string v1, "com.sec.android.allshare.event.EVENT_RENDERER_STATE_BUFFERING"

    sget-object v2, Lcom/samsung/android/allshare/media/PlaylistPlayer$PlayerState;->BUFFERING:Lcom/samsung/android/allshare/media/PlaylistPlayer$PlayerState;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 106
    iget-object v0, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl$1;->mPlaylistStateMap:Ljava/util/HashMap;

    const-string v1, "com.sec.android.allshare.event.EVENT_RENDERER_STATE_PAUSED"

    sget-object v2, Lcom/samsung/android/allshare/media/PlaylistPlayer$PlayerState;->PAUSED:Lcom/samsung/android/allshare/media/PlaylistPlayer$PlayerState;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 107
    iget-object v0, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl$1;->mPlaylistStateMap:Ljava/util/HashMap;

    const-string v1, "com.sec.android.allshare.event.EVENT_RENDERER_STATE_STOPPED"

    sget-object v2, Lcom/samsung/android/allshare/media/PlaylistPlayer$PlayerState;->STOPPED:Lcom/samsung/android/allshare/media/PlaylistPlayer$PlayerState;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 108
    iget-object v0, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl$1;->mPlaylistStateMap:Ljava/util/HashMap;

    const-string v1, "com.sec.android.allshare.event.EVENT_RENDERER_STATE_PLAYING"

    sget-object v2, Lcom/samsung/android/allshare/media/PlaylistPlayer$PlayerState;->PLAYING:Lcom/samsung/android/allshare/media/PlaylistPlayer$PlayerState;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 109
    iget-object v0, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl$1;->mPlaylistStateMap:Ljava/util/HashMap;

    const-string v1, "com.sec.android.allshare.event.EVENT_RENDERER_STATE_CONTENT_CHANGED"

    sget-object v2, Lcom/samsung/android/allshare/media/PlaylistPlayer$PlayerState;->CONTENT_CHANGED:Lcom/samsung/android/allshare/media/PlaylistPlayer$PlayerState;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 111
    return-void
.end method

.method private isContains(Ljava/lang/String;)Z
    .locals 4
    .param p1, "fullUri"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 157
    if-nez p1, :cond_1

    .line 165
    :cond_0
    :goto_0
    return v2

    .line 160
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl$1;->this$0:Lcom/samsung/android/allshare/PlaylistPlayerImpl;

    iget-object v3, v3, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mCurrentPlayingContentUriStrList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 161
    .local v1, "uri":Ljava/lang/String;
    invoke-virtual {p1, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 162
    const/4 v2, 0x1

    goto :goto_0
.end method

.method private notifyEvent(Lcom/samsung/android/allshare/media/PlaylistPlayer$PlayerState;Lcom/samsung/android/allshare/ERROR;)V
    .locals 3
    .param p1, "state"    # Lcom/samsung/android/allshare/media/PlaylistPlayer$PlayerState;
    .param p2, "error"    # Lcom/samsung/android/allshare/ERROR;

    .prologue
    .line 146
    iget-object v1, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl$1;->this$0:Lcom/samsung/android/allshare/PlaylistPlayerImpl;

    # getter for: Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mPlaylistPlayerEventListener:Lcom/samsung/android/allshare/media/PlaylistPlayer$IPlaylistPlayerEventListener;
    invoke-static {v1}, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->access$000(Lcom/samsung/android/allshare/PlaylistPlayerImpl;)Lcom/samsung/android/allshare/media/PlaylistPlayer$IPlaylistPlayerEventListener;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 148
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl$1;->this$0:Lcom/samsung/android/allshare/PlaylistPlayerImpl;

    # getter for: Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mPlaylistPlayerEventListener:Lcom/samsung/android/allshare/media/PlaylistPlayer$IPlaylistPlayerEventListener;
    invoke-static {v1}, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->access$000(Lcom/samsung/android/allshare/PlaylistPlayerImpl;)Lcom/samsung/android/allshare/media/PlaylistPlayer$IPlaylistPlayerEventListener;

    move-result-object v1

    invoke-interface {v1, p1, p2}, Lcom/samsung/android/allshare/media/PlaylistPlayer$IPlaylistPlayerEventListener;->onPlaylistPlayerStateChanged(Lcom/samsung/android/allshare/media/PlaylistPlayer$PlayerState;Lcom/samsung/android/allshare/ERROR;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 154
    :cond_0
    :goto_0
    return-void

    .line 149
    :catch_0
    move-exception v0

    .line 150
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "PlaylistPlayerImpl"

    const-string v2, "mEventHandler.notifyEvent Exception"

    invoke-static {v1, v2, v0}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method


# virtual methods
.method public handleEventMessage(Lcom/sec/android/allshare/iface/CVMessage;)V
    .locals 8
    .param p1, "cvm"    # Lcom/sec/android/allshare/iface/CVMessage;

    .prologue
    .line 115
    const/4 v5, 0x0

    .line 117
    .local v5, "state":Lcom/samsung/android/allshare/media/PlaylistPlayer$PlayerState;
    :try_start_0
    iget-object v6, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl$1;->mPlaylistStateMap:Ljava/util/HashMap;

    invoke-virtual {p1}, Lcom/sec/android/allshare/iface/CVMessage;->getActionID()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    move-object v0, v6

    check-cast v0, Lcom/samsung/android/allshare/media/PlaylistPlayer$PlayerState;

    move-object v5, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 122
    :goto_0
    if-nez v5, :cond_1

    .line 143
    :cond_0
    :goto_1
    return-void

    .line 118
    :catch_0
    move-exception v1

    .line 119
    .local v1, "e":Ljava/lang/Exception;
    const-string v6, "PlaylistPlayerImpl"

    const-string v7, "mEventHandler.handleEventMessage Exception "

    invoke-static {v6, v7, v1}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0

    .line 124
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_1
    invoke-virtual {p1}, Lcom/sec/android/allshare/iface/CVMessage;->getBundle()Landroid/os/Bundle;

    move-result-object v4

    .line 125
    .local v4, "resBundle":Landroid/os/Bundle;
    if-eqz v4, :cond_0

    .line 127
    const-string v6, "BUNDLE_ENUM_ERROR"

    invoke-virtual {v4, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 129
    .local v2, "errStr":Ljava/lang/String;
    if-nez v2, :cond_2

    .line 130
    sget-object v3, Lcom/samsung/android/allshare/ERROR;->FAIL:Lcom/samsung/android/allshare/ERROR;

    .line 134
    .local v3, "error":Lcom/samsung/android/allshare/ERROR;
    :goto_2
    sget-object v6, Lcom/samsung/android/allshare/media/PlaylistPlayer$PlayerState;->CONTENT_CHANGED:Lcom/samsung/android/allshare/media/PlaylistPlayer$PlayerState;

    invoke-virtual {v5, v6}, Lcom/samsung/android/allshare/media/PlaylistPlayer$PlayerState;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 135
    iget-object v6, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl$1;->this$0:Lcom/samsung/android/allshare/PlaylistPlayerImpl;

    iget-object v6, v6, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mCurrentPlayingContentUriStrList:Ljava/util/ArrayList;

    if-eqz v6, :cond_0

    const-string v6, "BUNDLE_STRING_APP_ITEM_ID"

    invoke-virtual {v4, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/samsung/android/allshare/PlaylistPlayerImpl$1;->isContains(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 137
    iget-object v6, p0, Lcom/samsung/android/allshare/PlaylistPlayerImpl$1;->this$0:Lcom/samsung/android/allshare/PlaylistPlayerImpl;

    const/4 v7, 0x0

    iput-object v7, v6, Lcom/samsung/android/allshare/PlaylistPlayerImpl;->mCurrentPlayingContentUriStrList:Ljava/util/ArrayList;

    .line 138
    invoke-direct {p0, v5, v3}, Lcom/samsung/android/allshare/PlaylistPlayerImpl$1;->notifyEvent(Lcom/samsung/android/allshare/media/PlaylistPlayer$PlayerState;Lcom/samsung/android/allshare/ERROR;)V

    goto :goto_1

    .line 132
    .end local v3    # "error":Lcom/samsung/android/allshare/ERROR;
    :cond_2
    invoke-static {v2}, Lcom/samsung/android/allshare/ERROR;->stringToEnum(Ljava/lang/String;)Lcom/samsung/android/allshare/ERROR;

    move-result-object v3

    .restart local v3    # "error":Lcom/samsung/android/allshare/ERROR;
    goto :goto_2

    .line 141
    :cond_3
    invoke-direct {p0, v5, v3}, Lcom/samsung/android/allshare/PlaylistPlayerImpl$1;->notifyEvent(Lcom/samsung/android/allshare/media/PlaylistPlayer$PlayerState;Lcom/samsung/android/allshare/ERROR;)V

    goto :goto_1
.end method
