.class final Lcom/samsung/android/allshare/ViewController2Impl;
.super Lcom/samsung/android/allshare/media/ViewController2;
.source "ViewController2Impl.java"

# interfaces
.implements Lcom/sec/android/allshare/iface/IBundleHolder;


# static fields
.field private static final TAG:Ljava/lang/String; = "ViewController2Impl"


# instance fields
.field private mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

.field private mAllShareEventHandler:Lcom/samsung/android/allshare/AllShareEventHandler;

.field private mConnectResult:Z

.field private mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

.field private mDos:Ljava/io/DataOutputStream;

.field private mEventListener:Lcom/samsung/android/allshare/media/ViewController2$IViewController2EventListener;

.field private mExcutor:Ljava/util/concurrent/ExecutorService;

.field private mIPAddress:Ljava/lang/String;

.field private mIsConnected:Z

.field private mIsSubscribed:Z

.field private mMACAddress:Ljava/lang/String;

.field private mPacketSize:I

.field mResponseHandler:Lcom/samsung/android/allshare/AllShareResponseHandler;

.field private mResponseListener:Lcom/samsung/android/allshare/media/ViewController2$IViewController2ResponseListener;

.field private mSocket:Ljava/net/Socket;


# direct methods
.method constructor <init>(Lcom/samsung/android/allshare/IAllShareConnector;Lcom/samsung/android/allshare/DeviceImpl;)V
    .locals 13
    .param p1, "connector"    # Lcom/samsung/android/allshare/IAllShareConnector;
    .param p2, "deviceImpl"    # Lcom/samsung/android/allshare/DeviceImpl;

    .prologue
    const/16 v9, 0xd

    const/4 v12, 0x4

    const/4 v11, 0x1

    const/4 v10, 0x0

    const/4 v7, 0x0

    .line 65
    invoke-direct {p0}, Lcom/samsung/android/allshare/media/ViewController2;-><init>()V

    .line 39
    iput-object v7, p0, Lcom/samsung/android/allshare/ViewController2Impl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    .line 41
    iput-object v7, p0, Lcom/samsung/android/allshare/ViewController2Impl;->mEventListener:Lcom/samsung/android/allshare/media/ViewController2$IViewController2EventListener;

    .line 43
    iput-object v7, p0, Lcom/samsung/android/allshare/ViewController2Impl;->mResponseListener:Lcom/samsung/android/allshare/media/ViewController2$IViewController2ResponseListener;

    .line 45
    iput-object v7, p0, Lcom/samsung/android/allshare/ViewController2Impl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    .line 47
    iput-object v7, p0, Lcom/samsung/android/allshare/ViewController2Impl;->mMACAddress:Ljava/lang/String;

    .line 49
    iput-object v7, p0, Lcom/samsung/android/allshare/ViewController2Impl;->mIPAddress:Ljava/lang/String;

    .line 51
    iput-boolean v10, p0, Lcom/samsung/android/allshare/ViewController2Impl;->mIsConnected:Z

    .line 53
    iput-boolean v10, p0, Lcom/samsung/android/allshare/ViewController2Impl;->mIsSubscribed:Z

    .line 55
    iput-object v7, p0, Lcom/samsung/android/allshare/ViewController2Impl;->mSocket:Ljava/net/Socket;

    .line 57
    iput-object v7, p0, Lcom/samsung/android/allshare/ViewController2Impl;->mDos:Ljava/io/DataOutputStream;

    .line 59
    iput v9, p0, Lcom/samsung/android/allshare/ViewController2Impl;->mPacketSize:I

    .line 61
    invoke-static {}, Ljava/util/concurrent/Executors;->newCachedThreadPool()Ljava/util/concurrent/ExecutorService;

    move-result-object v7

    iput-object v7, p0, Lcom/samsung/android/allshare/ViewController2Impl;->mExcutor:Ljava/util/concurrent/ExecutorService;

    .line 63
    iput-boolean v11, p0, Lcom/samsung/android/allshare/ViewController2Impl;->mConnectResult:Z

    .line 156
    new-instance v7, Lcom/samsung/android/allshare/ViewController2Impl$1;

    invoke-static {}, Lcom/samsung/android/allshare/ServiceConnector;->getMainLooper()Landroid/os/Looper;

    move-result-object v8

    invoke-direct {v7, p0, v8}, Lcom/samsung/android/allshare/ViewController2Impl$1;-><init>(Lcom/samsung/android/allshare/ViewController2Impl;Landroid/os/Looper;)V

    iput-object v7, p0, Lcom/samsung/android/allshare/ViewController2Impl;->mResponseHandler:Lcom/samsung/android/allshare/AllShareResponseHandler;

    .line 254
    new-instance v7, Lcom/samsung/android/allshare/ViewController2Impl$2;

    invoke-static {}, Lcom/samsung/android/allshare/ServiceConnector;->getMainLooper()Landroid/os/Looper;

    move-result-object v8

    invoke-direct {v7, p0, v8}, Lcom/samsung/android/allshare/ViewController2Impl$2;-><init>(Lcom/samsung/android/allshare/ViewController2Impl;Landroid/os/Looper;)V

    iput-object v7, p0, Lcom/samsung/android/allshare/ViewController2Impl;->mAllShareEventHandler:Lcom/samsung/android/allshare/AllShareEventHandler;

    .line 66
    if-nez p1, :cond_0

    .line 68
    const-string v7, "ViewController2Impl"

    const-string v8, "Connection FAIL: AllShare Service Connector does not exist"

    invoke-static {v7, v8}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 126
    :goto_0
    return-void

    .line 72
    :cond_0
    invoke-static {}, Lcom/samsung/android/allshare/ServiceConnector;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 74
    .local v1, "context":Landroid/content/Context;
    if-eqz v1, :cond_2

    .line 76
    const-string v7, "connectivity"

    invoke-virtual {v1, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 78
    .local v0, "conManager":Landroid/net/ConnectivityManager;
    invoke-virtual {v0, v9}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v4

    .line 84
    .local v4, "networkInfo":Landroid/net/NetworkInfo;
    const-string v7, "wifi"

    invoke-virtual {v1, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/net/wifi/WifiManager;

    .line 85
    .local v6, "wifiManager":Landroid/net/wifi/WifiManager;
    invoke-virtual {v6}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v7

    invoke-virtual {v7}, Landroid/net/wifi/WifiInfo;->getMacAddress()Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/samsung/android/allshare/ViewController2Impl;->mMACAddress:Ljava/lang/String;

    .line 87
    if-eqz v4, :cond_2

    invoke-virtual {v4}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v7

    if-ne v7, v11, :cond_2

    .line 88
    iget-object v7, p0, Lcom/samsung/android/allshare/ViewController2Impl;->mMACAddress:Ljava/lang/String;

    if-eqz v7, :cond_2

    .line 89
    iget-object v7, p0, Lcom/samsung/android/allshare/ViewController2Impl;->mMACAddress:Ljava/lang/String;

    const-string v8, ":"

    invoke-virtual {v7, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 97
    .local v5, "strMACPart":[Ljava/lang/String;
    array-length v7, v5

    const/4 v8, 0x6

    if-lt v7, v8, :cond_1

    .line 98
    aget-object v7, v5, v10

    const/16 v8, 0x10

    invoke-static {v7, v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v2

    .line 99
    .local v2, "nTempMAC1":I
    aget-object v7, v5, v12

    const/16 v8, 0x10

    invoke-static {v7, v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v3

    .line 101
    .local v3, "nTempMAC2":I
    or-int/lit8 v2, v2, 0x2

    .line 102
    xor-int/lit16 v3, v3, 0x80

    .line 104
    const-string v7, "%02x"

    new-array v8, v11, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v10

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v10

    .line 105
    const-string v7, "%02x"

    new-array v8, v11, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v10

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v12

    .line 109
    .end local v2    # "nTempMAC1":I
    .end local v3    # "nTempMAC2":I
    :cond_1
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    aget-object v8, v5, v10

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ":"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    aget-object v8, v5, v11

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ":"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const/4 v8, 0x2

    aget-object v8, v5, v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ":"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const/4 v8, 0x3

    aget-object v8, v5, v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ":"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    aget-object v8, v5, v12

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ":"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const/4 v8, 0x5

    aget-object v8, v5, v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/samsung/android/allshare/ViewController2Impl;->mMACAddress:Ljava/lang/String;

    .line 122
    .end local v0    # "conManager":Landroid/net/ConnectivityManager;
    .end local v4    # "networkInfo":Landroid/net/NetworkInfo;
    .end local v5    # "strMACPart":[Ljava/lang/String;
    .end local v6    # "wifiManager":Landroid/net/wifi/WifiManager;
    :cond_2
    iput-object p1, p0, Lcom/samsung/android/allshare/ViewController2Impl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    .line 124
    iput-object p2, p0, Lcom/samsung/android/allshare/ViewController2Impl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    .line 125
    iget-object v7, p0, Lcom/samsung/android/allshare/ViewController2Impl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    invoke-virtual {v7}, Lcom/samsung/android/allshare/DeviceImpl;->getIPAddress()Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/samsung/android/allshare/ViewController2Impl;->mIPAddress:Ljava/lang/String;

    goto/16 :goto_0
.end method

.method static synthetic access$000(Lcom/samsung/android/allshare/ViewController2Impl;)Lcom/samsung/android/allshare/media/ViewController2$IViewController2ResponseListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/ViewController2Impl;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/samsung/android/allshare/ViewController2Impl;->mResponseListener:Lcom/samsung/android/allshare/media/ViewController2$IViewController2ResponseListener;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/android/allshare/ViewController2Impl;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/ViewController2Impl;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/samsung/android/allshare/ViewController2Impl;->mIPAddress:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/android/allshare/ViewController2Impl;Ljava/lang/String;I)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/ViewController2Impl;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # I

    .prologue
    .line 36
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/allshare/ViewController2Impl;->connect(Ljava/lang/String;I)Z

    move-result v0

    return v0
.end method

.method static synthetic access$302(Lcom/samsung/android/allshare/ViewController2Impl;Ljava/net/Socket;)Ljava/net/Socket;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/ViewController2Impl;
    .param p1, "x1"    # Ljava/net/Socket;

    .prologue
    .line 36
    iput-object p1, p0, Lcom/samsung/android/allshare/ViewController2Impl;->mSocket:Ljava/net/Socket;

    return-object p1
.end method

.method static synthetic access$402(Lcom/samsung/android/allshare/ViewController2Impl;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/ViewController2Impl;
    .param p1, "x1"    # Z

    .prologue
    .line 36
    iput-boolean p1, p0, Lcom/samsung/android/allshare/ViewController2Impl;->mConnectResult:Z

    return p1
.end method

.method private connect(Ljava/lang/String;I)Z
    .locals 2
    .param p1, "serverIp"    # Ljava/lang/String;
    .param p2, "port"    # I

    .prologue
    .line 322
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/allshare/ViewController2Impl;->mSocket:Ljava/net/Socket;

    .line 324
    iget-object v0, p0, Lcom/samsung/android/allshare/ViewController2Impl;->mExcutor:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/samsung/android/allshare/ViewController2Impl$3;

    invoke-direct {v1, p0, p1, p2}, Lcom/samsung/android/allshare/ViewController2Impl$3;-><init>(Lcom/samsung/android/allshare/ViewController2Impl;Ljava/lang/String;I)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 340
    iget-boolean v0, p0, Lcom/samsung/android/allshare/ViewController2Impl;->mConnectResult:Z

    iput-boolean v0, p0, Lcom/samsung/android/allshare/ViewController2Impl;->mIsConnected:Z

    .line 342
    iget-boolean v0, p0, Lcom/samsung/android/allshare/ViewController2Impl;->mConnectResult:Z

    return v0
.end method

.method private float2bytes(F)[B
    .locals 4
    .param p1, "value"    # F

    .prologue
    .line 356
    const/4 v2, 0x4

    new-array v0, v2, [B

    .line 358
    .local v0, "array":[B
    invoke-static {p1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    .line 360
    .local v1, "intBits":I
    const/4 v2, 0x0

    and-int/lit16 v3, v1, 0xff

    shr-int/lit8 v3, v3, 0x0

    int-to-byte v3, v3

    aput-byte v3, v0, v2

    .line 361
    const/4 v2, 0x1

    const v3, 0xff00

    and-int/2addr v3, v1

    shr-int/lit8 v3, v3, 0x8

    int-to-byte v3, v3

    aput-byte v3, v0, v2

    .line 362
    const/4 v2, 0x2

    const/high16 v3, 0xff0000

    and-int/2addr v3, v1

    shr-int/lit8 v3, v3, 0x10

    int-to-byte v3, v3

    aput-byte v3, v0, v2

    .line 363
    const/4 v2, 0x3

    const/high16 v3, -0x1000000

    and-int/2addr v3, v1

    shr-int/lit8 v3, v3, 0x18

    int-to-byte v3, v3

    aput-byte v3, v0, v2

    .line 365
    return-object v0
.end method

.method private int2bytes(I)[B
    .locals 3
    .param p1, "i"    # I

    .prologue
    .line 346
    const/4 v1, 0x4

    new-array v0, v1, [B

    .line 347
    .local v0, "dest":[B
    const/4 v1, 0x3

    and-int/lit16 v2, p1, 0xff

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 348
    const/4 v1, 0x2

    shr-int/lit8 v2, p1, 0x8

    and-int/lit16 v2, v2, 0xff

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 349
    const/4 v1, 0x1

    shr-int/lit8 v2, p1, 0x10

    and-int/lit16 v2, v2, 0xff

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 350
    const/4 v1, 0x0

    shr-int/lit8 v2, p1, 0x19

    and-int/lit16 v2, v2, 0xff

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 351
    return-object v0
.end method


# virtual methods
.method public connect()V
    .locals 4

    .prologue
    .line 137
    iget-object v2, p0, Lcom/samsung/android/allshare/ViewController2Impl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/android/allshare/ViewController2Impl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    invoke-interface {v2}, Lcom/samsung/android/allshare/IAllShareConnector;->isAllShareServiceConnected()Z

    move-result v2

    if-nez v2, :cond_2

    .line 139
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/allshare/ViewController2Impl;->mResponseListener:Lcom/samsung/android/allshare/media/ViewController2$IViewController2ResponseListener;

    if-eqz v2, :cond_1

    .line 140
    iget-object v2, p0, Lcom/samsung/android/allshare/ViewController2Impl;->mResponseListener:Lcom/samsung/android/allshare/media/ViewController2$IViewController2ResponseListener;

    sget-object v3, Lcom/samsung/android/allshare/ERROR;->SERVICE_NOT_CONNECTED:Lcom/samsung/android/allshare/ERROR;

    invoke-interface {v2, p0, v3}, Lcom/samsung/android/allshare/media/ViewController2$IViewController2ResponseListener;->onConnectResponseReceived(Lcom/samsung/android/allshare/media/ViewController2;Lcom/samsung/android/allshare/ERROR;)V

    .line 154
    :cond_1
    :goto_0
    return-void

    .line 147
    :cond_2
    new-instance v1, Lcom/sec/android/allshare/iface/CVMessage;

    invoke-direct {v1}, Lcom/sec/android/allshare/iface/CVMessage;-><init>()V

    .line 148
    .local v1, "cvm":Lcom/sec/android/allshare/iface/CVMessage;
    const-string v2, "com.sec.android.allshare.action.ACTION_VIEWCONTROLLER_REQUEST_GET_ZOOM_PORT"

    invoke-virtual {v1, v2}, Lcom/sec/android/allshare/iface/CVMessage;->setActionID(Ljava/lang/String;)V

    .line 149
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 150
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v2, "BUNDLE_STRING_ID"

    invoke-virtual {p0}, Lcom/samsung/android/allshare/ViewController2Impl;->getID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 151
    invoke-virtual {v1, v0}, Lcom/sec/android/allshare/iface/CVMessage;->setBundle(Landroid/os/Bundle;)V

    .line 153
    iget-object v2, p0, Lcom/samsung/android/allshare/ViewController2Impl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    iget-object v3, p0, Lcom/samsung/android/allshare/ViewController2Impl;->mResponseHandler:Lcom/samsung/android/allshare/AllShareResponseHandler;

    invoke-interface {v2, v1, v3}, Lcom/samsung/android/allshare/IAllShareConnector;->requestCVMAsync(Lcom/sec/android/allshare/iface/CVMessage;Lcom/samsung/android/allshare/AllShareResponseHandler;)J

    goto :goto_0
.end method

.method public disconnect()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 196
    iput-boolean v1, p0, Lcom/samsung/android/allshare/ViewController2Impl;->mIsConnected:Z

    .line 198
    iget-object v0, p0, Lcom/samsung/android/allshare/ViewController2Impl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/allshare/ViewController2Impl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    invoke-interface {v0}, Lcom/samsung/android/allshare/IAllShareConnector;->isAllShareServiceConnected()Z

    move-result v0

    if-nez v0, :cond_2

    .line 199
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/allshare/ViewController2Impl;->mResponseListener:Lcom/samsung/android/allshare/media/ViewController2$IViewController2ResponseListener;

    if-eqz v0, :cond_1

    .line 200
    iget-object v0, p0, Lcom/samsung/android/allshare/ViewController2Impl;->mResponseListener:Lcom/samsung/android/allshare/media/ViewController2$IViewController2ResponseListener;

    sget-object v1, Lcom/samsung/android/allshare/ERROR;->SERVICE_NOT_CONNECTED:Lcom/samsung/android/allshare/ERROR;

    invoke-interface {v0, p0, v1}, Lcom/samsung/android/allshare/media/ViewController2$IViewController2ResponseListener;->onDisconnectResponseReceived(Lcom/samsung/android/allshare/media/ViewController2;Lcom/samsung/android/allshare/ERROR;)V

    .line 211
    :cond_1
    :goto_0
    return-void

    .line 207
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/allshare/ViewController2Impl;->mResponseListener:Lcom/samsung/android/allshare/media/ViewController2$IViewController2ResponseListener;

    if-eqz v0, :cond_1

    .line 208
    iput-boolean v1, p0, Lcom/samsung/android/allshare/ViewController2Impl;->mIsConnected:Z

    .line 209
    iget-object v0, p0, Lcom/samsung/android/allshare/ViewController2Impl;->mResponseListener:Lcom/samsung/android/allshare/media/ViewController2$IViewController2ResponseListener;

    sget-object v1, Lcom/samsung/android/allshare/ERROR;->SUCCESS:Lcom/samsung/android/allshare/ERROR;

    invoke-interface {v0, p0, v1}, Lcom/samsung/android/allshare/media/ViewController2$IViewController2ResponseListener;->onDisconnectResponseReceived(Lcom/samsung/android/allshare/media/ViewController2;Lcom/samsung/android/allshare/ERROR;)V

    goto :goto_0
.end method

.method public getBundle()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 215
    const/4 v0, 0x0

    return-object v0
.end method

.method public getID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lcom/samsung/android/allshare/ViewController2Impl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    if-nez v0, :cond_0

    .line 130
    const-string v0, ""

    .line 132
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/allshare/ViewController2Impl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/DeviceImpl;->getID()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public isConnected()Z
    .locals 2

    .prologue
    .line 220
    iget-object v0, p0, Lcom/samsung/android/allshare/ViewController2Impl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/allshare/ViewController2Impl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    invoke-interface {v0}, Lcom/samsung/android/allshare/IAllShareConnector;->isAllShareServiceConnected()Z

    move-result v0

    if-nez v0, :cond_1

    .line 221
    :cond_0
    const-string v0, "ViewController2Impl"

    const-string v1, "setEventListener error! AllShareService is not connected"

    invoke-static {v0, v1}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 222
    const/4 v0, 0x0

    .line 225
    :goto_0
    return v0

    :cond_1
    iget-boolean v0, p0, Lcom/samsung/android/allshare/ViewController2Impl;->mIsConnected:Z

    goto :goto_0
.end method

.method public removeEventHandler()V
    .locals 4

    .prologue
    .line 369
    iget-object v0, p0, Lcom/samsung/android/allshare/ViewController2Impl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    const-string v1, "com.sec.android.allshare.event.EVENT_DEVICE_SUBSCRIBE"

    iget-object v2, p0, Lcom/samsung/android/allshare/ViewController2Impl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    invoke-virtual {v2}, Lcom/samsung/android/allshare/DeviceImpl;->getBundle()Landroid/os/Bundle;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/allshare/ViewController2Impl;->mAllShareEventHandler:Lcom/samsung/android/allshare/AllShareEventHandler;

    invoke-interface {v0, v1, v2, v3}, Lcom/samsung/android/allshare/IAllShareConnector;->unsubscribeAllShareEvent(Ljava/lang/String;Landroid/os/Bundle;Lcom/samsung/android/allshare/AllShareEventHandler;)V

    .line 371
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/allshare/ViewController2Impl;->mIsSubscribed:Z

    .line 372
    return-void
.end method

.method public setEventListener(Lcom/samsung/android/allshare/media/ViewController2$IViewController2EventListener;)V
    .locals 4
    .param p1, "listener"    # Lcom/samsung/android/allshare/media/ViewController2$IViewController2EventListener;

    .prologue
    .line 296
    iget-object v0, p0, Lcom/samsung/android/allshare/ViewController2Impl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/allshare/ViewController2Impl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    invoke-interface {v0}, Lcom/samsung/android/allshare/IAllShareConnector;->isAllShareServiceConnected()Z

    move-result v0

    if-nez v0, :cond_2

    .line 297
    :cond_0
    const-string v0, "ViewController2Impl"

    const-string v1, "setEventListener error! AllShareService is not connected"

    invoke-static {v0, v1}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 314
    :cond_1
    :goto_0
    return-void

    .line 301
    :cond_2
    iput-object p1, p0, Lcom/samsung/android/allshare/ViewController2Impl;->mEventListener:Lcom/samsung/android/allshare/media/ViewController2$IViewController2EventListener;

    .line 303
    iget-boolean v0, p0, Lcom/samsung/android/allshare/ViewController2Impl;->mIsSubscribed:Z

    if-nez v0, :cond_3

    if-eqz p1, :cond_3

    .line 304
    iget-object v0, p0, Lcom/samsung/android/allshare/ViewController2Impl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    const-string v1, "com.sec.android.allshare.event.EVENT_DEVICE_SUBSCRIBE"

    iget-object v2, p0, Lcom/samsung/android/allshare/ViewController2Impl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    invoke-virtual {v2}, Lcom/samsung/android/allshare/DeviceImpl;->getBundle()Landroid/os/Bundle;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/allshare/ViewController2Impl;->mAllShareEventHandler:Lcom/samsung/android/allshare/AllShareEventHandler;

    invoke-interface {v0, v1, v2, v3}, Lcom/samsung/android/allshare/IAllShareConnector;->subscribeAllShareEvent(Ljava/lang/String;Landroid/os/Bundle;Lcom/samsung/android/allshare/AllShareEventHandler;)Z

    .line 306
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/allshare/ViewController2Impl;->mIsSubscribed:Z

    goto :goto_0

    .line 307
    :cond_3
    iget-boolean v0, p0, Lcom/samsung/android/allshare/ViewController2Impl;->mIsSubscribed:Z

    if-eqz v0, :cond_1

    if-nez p1, :cond_1

    .line 308
    iget-object v0, p0, Lcom/samsung/android/allshare/ViewController2Impl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    const-string v1, "com.sec.android.allshare.event.EVENT_DEVICE_SUBSCRIBE"

    iget-object v2, p0, Lcom/samsung/android/allshare/ViewController2Impl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    invoke-virtual {v2}, Lcom/samsung/android/allshare/DeviceImpl;->getBundle()Landroid/os/Bundle;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/allshare/ViewController2Impl;->mAllShareEventHandler:Lcom/samsung/android/allshare/AllShareEventHandler;

    invoke-interface {v0, v1, v2, v3}, Lcom/samsung/android/allshare/IAllShareConnector;->unsubscribeAllShareEvent(Ljava/lang/String;Landroid/os/Bundle;Lcom/samsung/android/allshare/AllShareEventHandler;)V

    .line 310
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/allshare/ViewController2Impl;->mIsSubscribed:Z

    goto :goto_0
.end method

.method public setResponseListener(Lcom/samsung/android/allshare/media/ViewController2$IViewController2ResponseListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/android/allshare/media/ViewController2$IViewController2ResponseListener;

    .prologue
    .line 318
    iput-object p1, p0, Lcom/samsung/android/allshare/ViewController2Impl;->mResponseListener:Lcom/samsung/android/allshare/media/ViewController2$IViewController2ResponseListener;

    .line 319
    return-void
.end method

.method public setViewAngle(I)V
    .locals 6
    .param p1, "angle"    # I

    .prologue
    const/4 v4, 0x4

    const/4 v5, 0x0

    .line 230
    iget-object v3, p0, Lcom/samsung/android/allshare/ViewController2Impl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/samsung/android/allshare/ViewController2Impl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    invoke-interface {v3}, Lcom/samsung/android/allshare/IAllShareConnector;->isAllShareServiceConnected()Z

    move-result v3

    if-nez v3, :cond_1

    .line 252
    :cond_0
    :goto_0
    return-void

    .line 233
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/allshare/ViewController2Impl;->mSocket:Ljava/net/Socket;

    if-eqz v3, :cond_0

    .line 236
    new-array v1, v4, [B

    .line 237
    .local v1, "buf1":[B
    iget v3, p0, Lcom/samsung/android/allshare/ViewController2Impl;->mPacketSize:I

    new-array v0, v3, [B

    .line 239
    .local v0, "buf":[B
    aput-byte v4, v0, v5

    .line 240
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/ViewController2Impl;->int2bytes(I)[B

    move-result-object v1

    .line 242
    const/4 v3, 0x1

    array-length v4, v1

    invoke-static {v1, v5, v0, v3, v4}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 245
    :try_start_0
    new-instance v3, Ljava/io/DataOutputStream;

    iget-object v4, p0, Lcom/samsung/android/allshare/ViewController2Impl;->mSocket:Ljava/net/Socket;

    invoke-virtual {v4}, Ljava/net/Socket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    iput-object v3, p0, Lcom/samsung/android/allshare/ViewController2Impl;->mDos:Ljava/io/DataOutputStream;

    .line 246
    iget-object v3, p0, Lcom/samsung/android/allshare/ViewController2Impl;->mDos:Ljava/io/DataOutputStream;

    const/4 v4, 0x0

    iget v5, p0, Lcom/samsung/android/allshare/ViewController2Impl;->mPacketSize:I

    invoke-virtual {v3, v0, v4, v5}, Ljava/io/DataOutputStream;->write([BII)V

    .line 247
    iget-object v3, p0, Lcom/samsung/android/allshare/ViewController2Impl;->mDos:Ljava/io/DataOutputStream;

    invoke-virtual {v3}, Ljava/io/DataOutputStream;->flush()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 248
    :catch_0
    move-exception v2

    .line 250
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method public zoom(IIF)V
    .locals 8
    .param p1, "posX"    # I
    .param p2, "posY"    # I
    .param p3, "zoomRatio"    # F

    .prologue
    const/4 v6, 0x4

    const/4 v7, 0x0

    .line 267
    iget-object v5, p0, Lcom/samsung/android/allshare/ViewController2Impl;->mSocket:Ljava/net/Socket;

    if-nez v5, :cond_0

    .line 292
    :goto_0
    return-void

    .line 270
    :cond_0
    new-array v1, v6, [B

    .line 271
    .local v1, "buf1":[B
    new-array v2, v6, [B

    .line 272
    .local v2, "buf2":[B
    new-array v3, v6, [B

    .line 273
    .local v3, "buf3":[B
    iget v5, p0, Lcom/samsung/android/allshare/ViewController2Impl;->mPacketSize:I

    new-array v0, v5, [B

    .line 275
    .local v0, "buf":[B
    const/16 v5, 0x9

    aput-byte v5, v0, v7

    .line 276
    int-to-float v5, p1

    invoke-direct {p0, v5}, Lcom/samsung/android/allshare/ViewController2Impl;->float2bytes(F)[B

    move-result-object v1

    .line 277
    int-to-float v5, p2

    invoke-direct {p0, v5}, Lcom/samsung/android/allshare/ViewController2Impl;->float2bytes(F)[B

    move-result-object v2

    .line 278
    invoke-direct {p0, p3}, Lcom/samsung/android/allshare/ViewController2Impl;->float2bytes(F)[B

    move-result-object v3

    .line 280
    const/4 v5, 0x1

    array-length v6, v1

    invoke-static {v1, v7, v0, v5, v6}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 281
    array-length v5, v1

    add-int/lit8 v5, v5, 0x1

    array-length v6, v2

    invoke-static {v2, v7, v0, v5, v6}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 282
    array-length v5, v1

    array-length v6, v2

    add-int/2addr v5, v6

    add-int/lit8 v5, v5, 0x1

    array-length v6, v3

    invoke-static {v3, v7, v0, v5, v6}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 285
    :try_start_0
    new-instance v5, Ljava/io/DataOutputStream;

    iget-object v6, p0, Lcom/samsung/android/allshare/ViewController2Impl;->mSocket:Ljava/net/Socket;

    invoke-virtual {v6}, Ljava/net/Socket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    iput-object v5, p0, Lcom/samsung/android/allshare/ViewController2Impl;->mDos:Ljava/io/DataOutputStream;

    .line 286
    iget-object v5, p0, Lcom/samsung/android/allshare/ViewController2Impl;->mDos:Ljava/io/DataOutputStream;

    const/4 v6, 0x0

    iget v7, p0, Lcom/samsung/android/allshare/ViewController2Impl;->mPacketSize:I

    invoke-virtual {v5, v0, v6, v7}, Ljava/io/DataOutputStream;->write([BII)V

    .line 287
    iget-object v5, p0, Lcom/samsung/android/allshare/ViewController2Impl;->mDos:Ljava/io/DataOutputStream;

    invoke-virtual {v5}, Ljava/io/DataOutputStream;->flush()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 288
    :catch_0
    move-exception v4

    .line 290
    .local v4, "e":Ljava/io/IOException;
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method
