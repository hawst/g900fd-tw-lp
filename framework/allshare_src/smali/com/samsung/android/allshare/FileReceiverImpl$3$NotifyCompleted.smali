.class Lcom/samsung/android/allshare/FileReceiverImpl$3$NotifyCompleted;
.super Ljava/lang/Object;
.source "FileReceiverImpl.java"

# interfaces
.implements Lcom/samsung/android/allshare/FileReceiverImpl$INotifyProgressEvent;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/allshare/FileReceiverImpl$3;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "NotifyCompleted"
.end annotation


# instance fields
.field final synthetic this$1:Lcom/samsung/android/allshare/FileReceiverImpl$3;


# direct methods
.method constructor <init>(Lcom/samsung/android/allshare/FileReceiverImpl$3;)V
    .locals 0

    .prologue
    .line 536
    iput-object p1, p0, Lcom/samsung/android/allshare/FileReceiverImpl$3$NotifyCompleted;->this$1:Lcom/samsung/android/allshare/FileReceiverImpl$3;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onNotifyEvent(Lcom/samsung/android/allshare/file/FileReceiver$IFileReceiverProgressUpdateEventListener;Landroid/os/Bundle;)V
    .locals 6
    .param p1, "listener"    # Lcom/samsung/android/allshare/file/FileReceiver$IFileReceiverProgressUpdateEventListener;
    .param p2, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 541
    sget-object v0, Lcom/samsung/android/allshare/ERROR;->FAIL:Lcom/samsung/android/allshare/ERROR;

    .line 547
    .local v0, "error":Lcom/samsung/android/allshare/ERROR;
    const-string v5, "BUNDLE_STRING_FILE_PATH"

    invoke-virtual {p2, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 548
    .local v3, "path":Ljava/lang/String;
    const-string v5, "BUNDLE_ENUM_ERROR"

    invoke-virtual {p2, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 549
    .local v1, "errorStr":Ljava/lang/String;
    const-string v5, "BUNDLE_STRING_SESSIONID"

    invoke-virtual {p2, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 551
    .local v4, "sessionId":Ljava/lang/String;
    if-nez v1, :cond_0

    .line 552
    sget-object v0, Lcom/samsung/android/allshare/ERROR;->FAIL:Lcom/samsung/android/allshare/ERROR;

    .line 556
    :goto_0
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 557
    .local v2, "file":Ljava/io/File;
    iget-object v5, p0, Lcom/samsung/android/allshare/FileReceiverImpl$3$NotifyCompleted;->this$1:Lcom/samsung/android/allshare/FileReceiverImpl$3;

    iget-object v5, v5, Lcom/samsung/android/allshare/FileReceiverImpl$3;->this$0:Lcom/samsung/android/allshare/FileReceiverImpl;

    # getter for: Lcom/samsung/android/allshare/FileReceiverImpl;->mReceiver:Lcom/samsung/android/allshare/file/FileReceiver;
    invoke-static {v5}, Lcom/samsung/android/allshare/FileReceiverImpl;->access$100(Lcom/samsung/android/allshare/FileReceiverImpl;)Lcom/samsung/android/allshare/file/FileReceiver;

    move-result-object v5

    invoke-interface {p1, v5, v4, v2, v0}, Lcom/samsung/android/allshare/file/FileReceiver$IFileReceiverProgressUpdateEventListener;->onCompleted(Lcom/samsung/android/allshare/file/FileReceiver;Ljava/lang/String;Ljava/io/File;Lcom/samsung/android/allshare/ERROR;)V

    .line 558
    return-void

    .line 554
    .end local v2    # "file":Ljava/io/File;
    :cond_0
    invoke-static {v1}, Lcom/samsung/android/allshare/ERROR;->stringToEnum(Ljava/lang/String;)Lcom/samsung/android/allshare/ERROR;

    move-result-object v0

    goto :goto_0
.end method
