.class public Lcom/sec/ims/android/internal/Utils;
.super Ljava/lang/Object;
.source "Utils.java"


# static fields
.field private static final EMERGENCY_ROUTE_ID:I = 0x5a

.field private static final LOG_TAG:Ljava/lang/String;

.field private static mNetworkCreated:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const-class v0, Lcom/sec/ims/android/internal/Utils;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/ims/android/internal/Utils;->LOG_TAG:Ljava/lang/String;

    .line 27
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/ims/android/internal/Utils;->mNetworkCreated:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 21
    sget-object v0, Lcom/sec/ims/android/internal/Utils;->LOG_TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100()Z
    .locals 1

    .prologue
    .line 21
    sget-boolean v0, Lcom/sec/ims/android/internal/Utils;->mNetworkCreated:Z

    return v0
.end method

.method static synthetic access$102(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 21
    sput-boolean p0, Lcom/sec/ims/android/internal/Utils;->mNetworkCreated:Z

    return p0
.end method

.method public static updatePCSCFAddressToRouteTable(Ljava/util/List;[Ljava/lang/String;Ljava/util/Set;Ljava/lang/String;)V
    .locals 2
    .param p1, "intfName"    # [Ljava/lang/String;
    .param p3, "gateWayAddress"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;[",
            "Ljava/lang/String;",
            "Ljava/util/Set",
            "<",
            "Ljava/net/InetAddress;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 32
    .local p0, "pcscfIpList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p2, "localIpAddressSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/net/InetAddress;>;"
    if-nez p1, :cond_0

    .line 33
    sget-object v0, Lcom/sec/ims/android/internal/Utils;->LOG_TAG:Ljava/lang/String;

    const-string v1, "updatePcscfAddressToRouteTable-invalid InterfaceName passed,returning"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 34
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Exception while addRoute for Emergency.Valid Scenario???"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 37
    :cond_0
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/sec/ims/android/internal/Utils$1;

    invoke-direct {v1, p0, p1, p3, p2}, Lcom/sec/ims/android/internal/Utils$1;-><init>(Ljava/util/List;[Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 120
    return-void
.end method
