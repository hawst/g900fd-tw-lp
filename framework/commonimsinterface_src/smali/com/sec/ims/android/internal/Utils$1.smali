.class final Lcom/sec/ims/android/internal/Utils$1;
.super Ljava/lang/Object;
.source "Utils.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/ims/android/internal/Utils;->updatePCSCFAddressToRouteTable(Ljava/util/List;[Ljava/lang/String;Ljava/util/Set;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$gateWayAddress:Ljava/lang/String;

.field final synthetic val$intfName:[Ljava/lang/String;

.field final synthetic val$localIpAddressSet:Ljava/util/Set;

.field final synthetic val$pcscfIpList:Ljava/util/List;


# direct methods
.method constructor <init>(Ljava/util/List;[Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V
    .locals 0

    .prologue
    .line 37
    iput-object p1, p0, Lcom/sec/ims/android/internal/Utils$1;->val$pcscfIpList:Ljava/util/List;

    iput-object p2, p0, Lcom/sec/ims/android/internal/Utils$1;->val$intfName:[Ljava/lang/String;

    iput-object p3, p0, Lcom/sec/ims/android/internal/Utils$1;->val$gateWayAddress:Ljava/lang/String;

    iput-object p4, p0, Lcom/sec/ims/android/internal/Utils$1;->val$localIpAddressSet:Ljava/util/Set;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 23

    .prologue
    .line 41
    const-string v18, "network_management"

    invoke-static/range {v18 .. v18}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Landroid/os/INetworkManagementService$Stub;->asInterface(Landroid/os/IBinder;)Landroid/os/INetworkManagementService;

    move-result-object v15

    .line 43
    .local v15, "networkManagementService":Landroid/os/INetworkManagementService;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/ims/android/internal/Utils$1;->val$pcscfIpList:Ljava/util/List;

    move-object/from16 v18, v0

    invoke-interface/range {v18 .. v18}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .local v7, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v18

    if-eqz v18, :cond_5

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Ljava/lang/String;

    .line 44
    .local v16, "pcscfAddress":Ljava/lang/String;
    # getter for: Lcom/sec/ims/android/internal/Utils;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/ims/android/internal/Utils;->access$000()Ljava/lang/String;

    move-result-object v18

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "updatePcscfAddressToRouteTable addRoute-pcscfAddress = "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, " IntfName = "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/ims/android/internal/Utils$1;->val$intfName:[Ljava/lang/String;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    aget-object v20, v20, v21

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 46
    :try_start_0
    invoke-static/range {v16 .. v16}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v8

    .line 47
    .local v8, "inetAddress":Ljava/net/InetAddress;
    const/16 v17, 0x0

    .line 48
    .local v17, "routeInfo":Landroid/net/RouteInfo;
    instance-of v0, v8, Ljava/net/Inet4Address;

    move/from16 v18, v0

    if-eqz v18, :cond_2

    .line 49
    new-instance v9, Landroid/net/LinkAddress;

    const/16 v18, 0x20

    move/from16 v0, v18

    invoke-direct {v9, v8, v0}, Landroid/net/LinkAddress;-><init>(Ljava/net/InetAddress;I)V

    .line 50
    .local v9, "linkAddress":Landroid/net/LinkAddress;
    new-instance v17, Landroid/net/RouteInfo;

    .end local v17    # "routeInfo":Landroid/net/RouteInfo;
    const/16 v18, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/ims/android/internal/Utils$1;->val$intfName:[Ljava/lang/String;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    aget-object v19, v19, v20

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-direct {v0, v9, v1, v2}, Landroid/net/RouteInfo;-><init>(Landroid/net/LinkAddress;Ljava/net/InetAddress;Ljava/lang/String;)V

    .line 55
    .end local v9    # "linkAddress":Landroid/net/LinkAddress;
    .restart local v17    # "routeInfo":Landroid/net/RouteInfo;
    :cond_1
    :goto_1
    if-eqz v17, :cond_0

    .line 56
    sget v18, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v19, 0x13

    move/from16 v0, v18

    move/from16 v1, v19

    if-gt v0, v1, :cond_3

    .line 57
    invoke-virtual {v15}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v18

    const-string v19, "addRoute"

    const/16 v20, 0x2

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Class;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    const-class v22, Ljava/lang/String;

    aput-object v22, v20, v21

    const/16 v21, 0x1

    const-class v22, Landroid/net/RouteInfo;

    aput-object v22, v20, v21

    invoke-virtual/range {v18 .. v20}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v10

    .line 58
    .local v10, "method":Ljava/lang/reflect/Method;
    const/16 v18, 0x2

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/ims/android/internal/Utils$1;->val$intfName:[Ljava/lang/String;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    aget-object v20, v20, v21

    aput-object v20, v18, v19

    const/16 v19, 0x1

    aput-object v17, v18, v19

    move-object/from16 v0, v18

    invoke-virtual {v10, v15, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/net/UnknownHostException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_4

    goto/16 :goto_0

    .line 83
    .end local v8    # "inetAddress":Ljava/net/InetAddress;
    .end local v10    # "method":Ljava/lang/reflect/Method;
    .end local v17    # "routeInfo":Landroid/net/RouteInfo;
    :catch_0
    move-exception v4

    .line 84
    .local v4, "e":Ljava/lang/IllegalArgumentException;
    # getter for: Lcom/sec/ims/android/internal/Utils;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/ims/android/internal/Utils;->access$000()Ljava/lang/String;

    move-result-object v18

    const-string v19, "IllegalArgumentException caught while addRoute for Emergency"

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 51
    .end local v4    # "e":Ljava/lang/IllegalArgumentException;
    .restart local v8    # "inetAddress":Ljava/net/InetAddress;
    .restart local v17    # "routeInfo":Landroid/net/RouteInfo;
    :cond_2
    :try_start_1
    instance-of v0, v8, Ljava/net/Inet6Address;

    move/from16 v18, v0

    if-eqz v18, :cond_1

    .line 52
    new-instance v9, Landroid/net/LinkAddress;

    const/16 v18, 0x80

    move/from16 v0, v18

    invoke-direct {v9, v8, v0}, Landroid/net/LinkAddress;-><init>(Ljava/net/InetAddress;I)V

    .line 53
    .restart local v9    # "linkAddress":Landroid/net/LinkAddress;
    new-instance v17, Landroid/net/RouteInfo;

    .end local v17    # "routeInfo":Landroid/net/RouteInfo;
    const/16 v18, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/ims/android/internal/Utils$1;->val$intfName:[Ljava/lang/String;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    aget-object v19, v19, v20

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-direct {v0, v9, v1, v2}, Landroid/net/RouteInfo;-><init>(Landroid/net/LinkAddress;Ljava/net/InetAddress;Ljava/lang/String;)V

    .restart local v17    # "routeInfo":Landroid/net/RouteInfo;
    goto :goto_1

    .line 62
    .end local v9    # "linkAddress":Landroid/net/LinkAddress;
    :cond_3
    const v14, 0xea60

    .line 64
    .local v14, "networkId":I
    # getter for: Lcom/sec/ims/android/internal/Utils;->mNetworkCreated:Z
    invoke-static {}, Lcom/sec/ims/android/internal/Utils;->access$100()Z

    move-result v18

    if-nez v18, :cond_4

    .line 65
    # getter for: Lcom/sec/ims/android/internal/Utils;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/ims/android/internal/Utils;->access$000()Ljava/lang/String;

    move-result-object v18

    const-string v19, "createPhyNetwork"

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 66
    invoke-virtual {v15}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v18

    const-string v19, "createPhysicalNetwork"

    const/16 v20, 0x1

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Class;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    sget-object v22, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v22, v20, v21

    invoke-virtual/range {v18 .. v20}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v12

    .line 68
    .local v12, "methodCreatePhyNetwork":Ljava/lang/reflect/Method;
    const/16 v18, 0x1

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    const v20, 0xea60

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    aput-object v20, v18, v19

    move-object/from16 v0, v18

    invoke-virtual {v12, v15, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 70
    const/16 v18, 0x1

    # setter for: Lcom/sec/ims/android/internal/Utils;->mNetworkCreated:Z
    invoke-static/range {v18 .. v18}, Lcom/sec/ims/android/internal/Utils;->access$102(Z)Z

    .line 72
    # getter for: Lcom/sec/ims/android/internal/Utils;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/ims/android/internal/Utils;->access$000()Ljava/lang/String;

    move-result-object v18

    const-string v19, "addInterface"

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 73
    invoke-virtual {v15}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v18

    const-string v19, "addInterfaceToNetwork"

    const/16 v20, 0x2

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Class;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    const-class v22, Ljava/lang/String;

    aput-object v22, v20, v21

    const/16 v21, 0x1

    sget-object v22, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v22, v20, v21

    invoke-virtual/range {v18 .. v20}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v13

    .line 75
    .local v13, "methodaddInterface":Ljava/lang/reflect/Method;
    const/16 v18, 0x2

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/ims/android/internal/Utils$1;->val$intfName:[Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v20

    aput-object v20, v18, v19

    const/16 v19, 0x1

    const v20, 0xea60

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    aput-object v20, v18, v19

    move-object/from16 v0, v18

    invoke-virtual {v13, v15, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 78
    .end local v12    # "methodCreatePhyNetwork":Ljava/lang/reflect/Method;
    .end local v13    # "methodaddInterface":Ljava/lang/reflect/Method;
    :cond_4
    # getter for: Lcom/sec/ims/android/internal/Utils;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/ims/android/internal/Utils;->access$000()Ljava/lang/String;

    move-result-object v18

    const-string v19, "addRoute"

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 79
    invoke-virtual {v15}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v18

    const-string v19, "addRoute"

    const/16 v20, 0x2

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Class;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    sget-object v22, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v22, v20, v21

    const/16 v21, 0x1

    const-class v22, Landroid/net/RouteInfo;

    aput-object v22, v20, v21

    invoke-virtual/range {v18 .. v20}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v11

    .line 80
    .local v11, "methodAddRoute":Ljava/lang/reflect/Method;
    const/16 v18, 0x2

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    const v20, 0xea60

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    aput-object v20, v18, v19

    const/16 v19, 0x1

    aput-object v17, v18, v19

    move-object/from16 v0, v18

    invoke-virtual {v11, v15, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/net/UnknownHostException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_1 .. :try_end_1} :catch_4

    goto/16 :goto_0

    .line 85
    .end local v8    # "inetAddress":Ljava/net/InetAddress;
    .end local v11    # "methodAddRoute":Ljava/lang/reflect/Method;
    .end local v14    # "networkId":I
    .end local v17    # "routeInfo":Landroid/net/RouteInfo;
    :catch_1
    move-exception v4

    .line 86
    .local v4, "e":Ljava/net/UnknownHostException;
    # getter for: Lcom/sec/ims/android/internal/Utils;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/ims/android/internal/Utils;->access$000()Ljava/lang/String;

    move-result-object v18

    const-string v19, "UnknownHostException caught while addRoute for Emergency"

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 87
    .end local v4    # "e":Ljava/net/UnknownHostException;
    :catch_2
    move-exception v4

    .line 88
    .local v4, "e":Ljava/lang/NoSuchMethodException;
    # getter for: Lcom/sec/ims/android/internal/Utils;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/ims/android/internal/Utils;->access$000()Ljava/lang/String;

    move-result-object v18

    const-string v19, "NoSuchMethodException caught while addRoute for Emergency"

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 89
    .end local v4    # "e":Ljava/lang/NoSuchMethodException;
    :catch_3
    move-exception v4

    .line 90
    .local v4, "e":Ljava/lang/IllegalAccessException;
    # getter for: Lcom/sec/ims/android/internal/Utils;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/ims/android/internal/Utils;->access$000()Ljava/lang/String;

    move-result-object v18

    const-string v19, "IllegalAccessException caught while addRoute for Emergency"

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 91
    .end local v4    # "e":Ljava/lang/IllegalAccessException;
    :catch_4
    move-exception v4

    .line 92
    .local v4, "e":Ljava/lang/reflect/InvocationTargetException;
    # getter for: Lcom/sec/ims/android/internal/Utils;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/ims/android/internal/Utils;->access$000()Ljava/lang/String;

    move-result-object v18

    const-string v19, "InvocationTargetException caught while addRoute for Emergency"

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 95
    .end local v4    # "e":Ljava/lang/reflect/InvocationTargetException;
    .end local v16    # "pcscfAddress":Ljava/lang/String;
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/ims/android/internal/Utils$1;->val$gateWayAddress:Ljava/lang/String;

    move-object/from16 v18, v0

    if-nez v18, :cond_7

    .line 96
    # getter for: Lcom/sec/ims/android/internal/Utils;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/ims/android/internal/Utils;->access$000()Ljava/lang/String;

    move-result-object v18

    const-string v19, "updatePcscfAddressToRouteTable gateWayAddress passed is null, returning"

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 116
    :cond_6
    :goto_2
    return-void

    .line 101
    :cond_7
    :try_start_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/ims/android/internal/Utils$1;->val$gateWayAddress:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v6

    .line 102
    .local v6, "gatewayInetAddress":Ljava/net/InetAddress;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/ims/android/internal/Utils$1;->val$localIpAddressSet:Ljava/util/Set;

    move-object/from16 v18, v0

    invoke-interface/range {v18 .. v18}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_3
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v18

    if-eqz v18, :cond_6

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/net/InetAddress;

    .line 103
    .local v5, "epdnLocalIp":Ljava/net/InetAddress;
    # getter for: Lcom/sec/ims/android/internal/Utils;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/ims/android/internal/Utils;->access$000()Ljava/lang/String;

    move-result-object v18

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "updatePcscfAddressToRouteTable replaceSrcRoute-epdnLocalIp = "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, " IntfName = "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/ims/android/internal/Utils$1;->val$intfName:[Ljava/lang/String;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    aget-object v20, v20, v21

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "gateWayAddress = "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/ims/android/internal/Utils$1;->val$gateWayAddress:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 105
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/ims/android/internal/Utils$1;->val$intfName:[Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    aget-object v18, v18, v19

    invoke-virtual {v5}, Ljava/net/InetAddress;->getAddress()[B

    move-result-object v19

    invoke-virtual {v6}, Ljava/net/InetAddress;->getAddress()[B

    move-result-object v20

    const/16 v21, 0x5a

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    move-object/from16 v2, v20

    move/from16 v3, v21

    invoke-interface {v15, v0, v1, v2, v3}, Landroid/os/INetworkManagementService;->replaceSrcRoute(Ljava/lang/String;[B[BI)Z
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_5
    .catch Ljava/net/UnknownHostException; {:try_start_2 .. :try_end_2} :catch_6

    goto :goto_3

    .line 110
    .end local v5    # "epdnLocalIp":Ljava/net/InetAddress;
    .end local v6    # "gatewayInetAddress":Ljava/net/InetAddress;
    :catch_5
    move-exception v4

    .line 111
    .local v4, "e":Landroid/os/RemoteException;
    # getter for: Lcom/sec/ims/android/internal/Utils;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/ims/android/internal/Utils;->access$000()Ljava/lang/String;

    move-result-object v18

    const-string v19, "RemoteException caught while replaceSrcRoute for Emergency"

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 112
    .end local v4    # "e":Landroid/os/RemoteException;
    :catch_6
    move-exception v4

    .line 113
    .local v4, "e":Ljava/net/UnknownHostException;
    # getter for: Lcom/sec/ims/android/internal/Utils;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/ims/android/internal/Utils;->access$000()Ljava/lang/String;

    move-result-object v18

    const-string v19, "UnknownHostException caught while replaceSrcRoute for Emergency"

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2
.end method
