.class public abstract Lcom/sec/ims/android/internal/IIMSCoreService$Stub;
.super Landroid/os/Binder;
.source "IIMSCoreService.java"

# interfaces
.implements Lcom/sec/ims/android/internal/IIMSCoreService;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/ims/android/internal/IIMSCoreService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/ims/android/internal/IIMSCoreService$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.sec.ims.android.internal.IIMSCoreService"

.field static final TRANSACTION_handleActionsOverEpdg:I = 0x3

.field static final TRANSACTION_onEPDGHandoverToLTEFailed:I = 0x2

.field static final TRANSACTION_onSSACInfoChanged:I = 0x5

.field static final TRANSACTION_sendInitialRegister:I = 0x4

.field static final TRANSACTION_setRegistrationFeatureTags:I = 0x1


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 18
    const-string v0, "com.sec.ims.android.internal.IIMSCoreService"

    invoke-virtual {p0, p0, v0}, Lcom/sec/ims/android/internal/IIMSCoreService$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 19
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/sec/ims/android/internal/IIMSCoreService;
    .locals 2
    .param p0, "obj"    # Landroid/os/IBinder;

    .prologue
    .line 26
    if-nez p0, :cond_0

    .line 27
    const/4 v0, 0x0

    .line 33
    :goto_0
    return-object v0

    .line 29
    :cond_0
    const-string v1, "com.sec.ims.android.internal.IIMSCoreService"

    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 30
    .local v0, "iin":Landroid/os/IInterface;
    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/sec/ims/android/internal/IIMSCoreService;

    if-eqz v1, :cond_1

    .line 31
    check-cast v0, Lcom/sec/ims/android/internal/IIMSCoreService;

    goto :goto_0

    .line 33
    :cond_1
    new-instance v0, Lcom/sec/ims/android/internal/IIMSCoreService$Stub$Proxy;

    .end local v0    # "iin":Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/sec/ims/android/internal/IIMSCoreService$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 37
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 4
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 41
    sparse-switch p1, :sswitch_data_0

    .line 85
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v2

    :goto_0
    return v2

    .line 45
    :sswitch_0
    const-string v3, "com.sec.ims.android.internal.IIMSCoreService"

    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 50
    :sswitch_1
    const-string v3, "com.sec.ims.android.internal.IIMSCoreService"

    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 52
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 54
    .local v0, "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    move-result-object v1

    .line 55
    .local v1, "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p0, v0, v1}, Lcom/sec/ims/android/internal/IIMSCoreService$Stub;->setRegistrationFeatureTags(Ljava/lang/String;Ljava/util/List;)V

    goto :goto_0

    .line 60
    .end local v0    # "_arg0":Ljava/lang/String;
    .end local v1    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :sswitch_2
    const-string v3, "com.sec.ims.android.internal.IIMSCoreService"

    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 61
    invoke-virtual {p0}, Lcom/sec/ims/android/internal/IIMSCoreService$Stub;->onEPDGHandoverToLTEFailed()V

    goto :goto_0

    .line 66
    :sswitch_3
    const-string v3, "com.sec.ims.android.internal.IIMSCoreService"

    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 67
    invoke-virtual {p0}, Lcom/sec/ims/android/internal/IIMSCoreService$Stub;->handleActionsOverEpdg()V

    goto :goto_0

    .line 72
    :sswitch_4
    const-string v3, "com.sec.ims.android.internal.IIMSCoreService"

    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 74
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 75
    .local v0, "_arg0":I
    invoke-virtual {p0, v0}, Lcom/sec/ims/android/internal/IIMSCoreService$Stub;->sendInitialRegister(I)V

    goto :goto_0

    .line 80
    .end local v0    # "_arg0":I
    :sswitch_5
    const-string v3, "com.sec.ims.android.internal.IIMSCoreService"

    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 81
    invoke-virtual {p0}, Lcom/sec/ims/android/internal/IIMSCoreService$Stub;->onSSACInfoChanged()V

    goto :goto_0

    .line 41
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
