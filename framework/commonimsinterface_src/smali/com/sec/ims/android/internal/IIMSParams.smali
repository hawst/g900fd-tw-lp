.class public Lcom/sec/ims/android/internal/IIMSParams;
.super Ljava/lang/Object;
.source "IIMSParams.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/ims/android/internal/IIMSParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mAlertInfo:Ljava/lang/String;

.field private mAudioCodec:Ljava/lang/String;

.field private mCodecBitRate:I

.field private mFullModeRegistered:I

.field private mHistoryInfo:Ljava/lang/String;

.field private mIndicationFlag:I

.field private mIsConferenceSupported:Ljava/lang/String;

.field private mIsFocus:Ljava/lang/String;

.field private mIsVMSCall:Ljava/lang/String;

.field private mIsVideoActive:I

.field private mLocalVideoRTCPPort:Ljava/lang/String;

.field private mLocalVideoRTPPort:Ljava/lang/String;

.field private mModifySupported:Ljava/lang/String;

.field private mNumberPlus:Ljava/lang/String;

.field private mPLettering:Ljava/lang/String;

.field private mParticipantChangedInNWayConferenceCall:Ljava/lang/String;

.field private mPhotoRing:Ljava/lang/String;

.field private mReasonCode:Ljava/lang/String;

.field private mRemoteVideoRTCPPort:Ljava/lang/String;

.field private mRemoteVideoRTPPort:Ljava/lang/String;

.field private mRetryAfter:I

.field private mTTYModeType:Ljava/lang/String;

.field private mUSSDData:[B

.field private mVideoOrientation:I

.field private misHDIcon:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 119
    new-instance v0, Lcom/sec/ims/android/internal/IIMSParams$1;

    invoke-direct {v0}, Lcom/sec/ims/android/internal/IIMSParams$1;-><init>()V

    sput-object v0, Lcom/sec/ims/android/internal/IIMSParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/ims/android/internal/IIMSParams;->mPLettering:Ljava/lang/String;

    .line 43
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/ims/android/internal/IIMSParams;->mHistoryInfo:Ljava/lang/String;

    .line 44
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/ims/android/internal/IIMSParams;->mModifySupported:Ljava/lang/String;

    .line 45
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/ims/android/internal/IIMSParams;->mAudioCodec:Ljava/lang/String;

    .line 46
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/ims/android/internal/IIMSParams;->mNumberPlus:Ljava/lang/String;

    .line 47
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/ims/android/internal/IIMSParams;->mReasonCode:Ljava/lang/String;

    .line 48
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/ims/android/internal/IIMSParams;->mTTYModeType:Ljava/lang/String;

    .line 49
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/ims/android/internal/IIMSParams;->mParticipantChangedInNWayConferenceCall:Ljava/lang/String;

    .line 50
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/ims/android/internal/IIMSParams;->mIsConferenceSupported:Ljava/lang/String;

    .line 51
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/ims/android/internal/IIMSParams;->mIsFocus:Ljava/lang/String;

    .line 52
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/ims/android/internal/IIMSParams;->mIsVMSCall:Ljava/lang/String;

    .line 53
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/ims/android/internal/IIMSParams;->mPhotoRing:Ljava/lang/String;

    .line 54
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/ims/android/internal/IIMSParams;->mLocalVideoRTPPort:Ljava/lang/String;

    .line 55
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/ims/android/internal/IIMSParams;->mLocalVideoRTCPPort:Ljava/lang/String;

    .line 56
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/ims/android/internal/IIMSParams;->mRemoteVideoRTPPort:Ljava/lang/String;

    .line 57
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/ims/android/internal/IIMSParams;->mRemoteVideoRTCPPort:Ljava/lang/String;

    .line 60
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 62
    .local v0, "ussdDataLength":I
    if-lez v0, :cond_0

    .line 63
    new-array v1, v0, [B

    iput-object v1, p0, Lcom/sec/ims/android/internal/IIMSParams;->mUSSDData:[B

    .line 64
    iget-object v1, p0, Lcom/sec/ims/android/internal/IIMSParams;->mUSSDData:[B

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readByteArray([B)V

    .line 69
    :goto_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/sec/ims/android/internal/IIMSParams;->mIndicationFlag:I

    .line 70
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/sec/ims/android/internal/IIMSParams;->misHDIcon:I

    .line 71
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/sec/ims/android/internal/IIMSParams;->mFullModeRegistered:I

    .line 72
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/sec/ims/android/internal/IIMSParams;->mCodecBitRate:I

    .line 73
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/sec/ims/android/internal/IIMSParams;->mRetryAfter:I

    .line 74
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/ims/android/internal/IIMSParams;->mAlertInfo:Ljava/lang/String;

    .line 75
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/sec/ims/android/internal/IIMSParams;->mVideoOrientation:I

    .line 76
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/sec/ims/android/internal/IIMSParams;->mIsVideoActive:I

    .line 77
    return-void

    .line 66
    :cond_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/ims/android/internal/IIMSParams;->mUSSDData:[B

    goto :goto_0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 116
    const/4 v0, 0x0

    return v0
.end method

.method public getAlertInfo()Ljava/lang/String;
    .locals 1

    .prologue
    .line 306
    iget-object v0, p0, Lcom/sec/ims/android/internal/IIMSParams;->mAlertInfo:Ljava/lang/String;

    return-object v0
.end method

.method public getAudioCodec()Ljava/lang/String;
    .locals 1

    .prologue
    .line 155
    iget-object v0, p0, Lcom/sec/ims/android/internal/IIMSParams;->mAudioCodec:Ljava/lang/String;

    return-object v0
.end method

.method public getCodecBitRate()I
    .locals 1

    .prologue
    .line 274
    iget v0, p0, Lcom/sec/ims/android/internal/IIMSParams;->mCodecBitRate:I

    return v0
.end method

.method public getErrorReasonCode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 171
    iget-object v0, p0, Lcom/sec/ims/android/internal/IIMSParams;->mReasonCode:Ljava/lang/String;

    return-object v0
.end method

.method public getHistoryInfo()Ljava/lang/String;
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lcom/sec/ims/android/internal/IIMSParams;->mHistoryInfo:Ljava/lang/String;

    return-object v0
.end method

.method public getIndicationFlag()I
    .locals 1

    .prologue
    .line 250
    iget v0, p0, Lcom/sec/ims/android/internal/IIMSParams;->mIndicationFlag:I

    return v0
.end method

.method public getIsConferenceSupported()Ljava/lang/String;
    .locals 1

    .prologue
    .line 195
    iget-object v0, p0, Lcom/sec/ims/android/internal/IIMSParams;->mIsConferenceSupported:Ljava/lang/String;

    return-object v0
.end method

.method public getIsFocus()Ljava/lang/String;
    .locals 1

    .prologue
    .line 203
    iget-object v0, p0, Lcom/sec/ims/android/internal/IIMSParams;->mIsFocus:Ljava/lang/String;

    return-object v0
.end method

.method public getIsVMSCall()Ljava/lang/String;
    .locals 1

    .prologue
    .line 290
    iget-object v0, p0, Lcom/sec/ims/android/internal/IIMSParams;->mIsVMSCall:Ljava/lang/String;

    return-object v0
.end method

.method public getLocalVideoRTCPPort()Ljava/lang/String;
    .locals 1

    .prologue
    .line 219
    iget-object v0, p0, Lcom/sec/ims/android/internal/IIMSParams;->mLocalVideoRTCPPort:Ljava/lang/String;

    return-object v0
.end method

.method public getLocalVideoRTPPort()Ljava/lang/String;
    .locals 1

    .prologue
    .line 211
    iget-object v0, p0, Lcom/sec/ims/android/internal/IIMSParams;->mLocalVideoRTPPort:Ljava/lang/String;

    return-object v0
.end method

.method public getModifyHeader()Ljava/lang/String;
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lcom/sec/ims/android/internal/IIMSParams;->mModifySupported:Ljava/lang/String;

    return-object v0
.end method

.method public getNumberPlus()Ljava/lang/String;
    .locals 1

    .prologue
    .line 163
    iget-object v0, p0, Lcom/sec/ims/android/internal/IIMSParams;->mNumberPlus:Ljava/lang/String;

    return-object v0
.end method

.method public getPLettering()Ljava/lang/String;
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lcom/sec/ims/android/internal/IIMSParams;->mPLettering:Ljava/lang/String;

    return-object v0
.end method

.method public getParticipantChangedInNWayConferenceCall()Ljava/lang/String;
    .locals 1

    .prologue
    .line 187
    iget-object v0, p0, Lcom/sec/ims/android/internal/IIMSParams;->mParticipantChangedInNWayConferenceCall:Ljava/lang/String;

    return-object v0
.end method

.method public getPhotoRing()Ljava/lang/String;
    .locals 1

    .prologue
    .line 298
    iget-object v0, p0, Lcom/sec/ims/android/internal/IIMSParams;->mPhotoRing:Ljava/lang/String;

    return-object v0
.end method

.method public getRemoteVideoRTCPPort()Ljava/lang/String;
    .locals 1

    .prologue
    .line 235
    iget-object v0, p0, Lcom/sec/ims/android/internal/IIMSParams;->mRemoteVideoRTCPPort:Ljava/lang/String;

    return-object v0
.end method

.method public getRemoteVideoRTPPort()Ljava/lang/String;
    .locals 1

    .prologue
    .line 227
    iget-object v0, p0, Lcom/sec/ims/android/internal/IIMSParams;->mRemoteVideoRTPPort:Ljava/lang/String;

    return-object v0
.end method

.method public getRetryAfter()I
    .locals 1

    .prologue
    .line 282
    iget v0, p0, Lcom/sec/ims/android/internal/IIMSParams;->mRetryAfter:I

    return v0
.end method

.method public getUSSDData()[B
    .locals 1

    .prologue
    .line 242
    iget-object v0, p0, Lcom/sec/ims/android/internal/IIMSParams;->mUSSDData:[B

    return-object v0
.end method

.method public getVideoOrientation()I
    .locals 1

    .prologue
    .line 314
    iget v0, p0, Lcom/sec/ims/android/internal/IIMSParams;->mVideoOrientation:I

    return v0
.end method

.method public getisFullModeRegistered()I
    .locals 1

    .prologue
    .line 266
    iget v0, p0, Lcom/sec/ims/android/internal/IIMSParams;->mFullModeRegistered:I

    return v0
.end method

.method public getisHDIcon()I
    .locals 1

    .prologue
    .line 258
    iget v0, p0, Lcom/sec/ims/android/internal/IIMSParams;->misHDIcon:I

    return v0
.end method

.method public getttyType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 179
    iget-object v0, p0, Lcom/sec/ims/android/internal/IIMSParams;->mTTYModeType:Ljava/lang/String;

    return-object v0
.end method

.method public isVideoActive()I
    .locals 1

    .prologue
    .line 322
    iget v0, p0, Lcom/sec/ims/android/internal/IIMSParams;->mIsVideoActive:I

    return v0
.end method

.method public setAlertInfo(Ljava/lang/String;)V
    .locals 0
    .param p1, "alertInfo"    # Ljava/lang/String;

    .prologue
    .line 310
    iput-object p1, p0, Lcom/sec/ims/android/internal/IIMSParams;->mAlertInfo:Ljava/lang/String;

    .line 311
    return-void
.end method

.method public setAudioCodec(Ljava/lang/String;)V
    .locals 0
    .param p1, "codec"    # Ljava/lang/String;

    .prologue
    .line 159
    iput-object p1, p0, Lcom/sec/ims/android/internal/IIMSParams;->mAudioCodec:Ljava/lang/String;

    .line 160
    return-void
.end method

.method public setCodecBitRate(I)V
    .locals 0
    .param p1, "bitrate"    # I

    .prologue
    .line 278
    iput p1, p0, Lcom/sec/ims/android/internal/IIMSParams;->mCodecBitRate:I

    .line 279
    return-void
.end method

.method public setErrorReasonCode(Ljava/lang/String;)V
    .locals 0
    .param p1, "reason"    # Ljava/lang/String;

    .prologue
    .line 175
    iput-object p1, p0, Lcom/sec/ims/android/internal/IIMSParams;->mReasonCode:Ljava/lang/String;

    .line 176
    return-void
.end method

.method public setHistoryInfo(Ljava/lang/String;)V
    .locals 0
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 143
    iput-object p1, p0, Lcom/sec/ims/android/internal/IIMSParams;->mHistoryInfo:Ljava/lang/String;

    .line 144
    return-void
.end method

.method public setIndicationFlag(I)V
    .locals 0
    .param p1, "flag"    # I

    .prologue
    .line 254
    iput p1, p0, Lcom/sec/ims/android/internal/IIMSParams;->mIndicationFlag:I

    .line 255
    return-void
.end method

.method public setIsConferenceSupported(Ljava/lang/String;)V
    .locals 0
    .param p1, "isConfSupported"    # Ljava/lang/String;

    .prologue
    .line 199
    iput-object p1, p0, Lcom/sec/ims/android/internal/IIMSParams;->mIsConferenceSupported:Ljava/lang/String;

    .line 200
    return-void
.end method

.method public setIsFocus(Ljava/lang/String;)V
    .locals 0
    .param p1, "isFocus"    # Ljava/lang/String;

    .prologue
    .line 207
    iput-object p1, p0, Lcom/sec/ims/android/internal/IIMSParams;->mIsFocus:Ljava/lang/String;

    .line 208
    return-void
.end method

.method public setIsVMSCall(Ljava/lang/String;)V
    .locals 0
    .param p1, "isVMSCall"    # Ljava/lang/String;

    .prologue
    .line 294
    iput-object p1, p0, Lcom/sec/ims/android/internal/IIMSParams;->mIsVMSCall:Ljava/lang/String;

    .line 295
    return-void
.end method

.method public setLocalVideoRTCPPort(Ljava/lang/String;)V
    .locals 0
    .param p1, "port"    # Ljava/lang/String;

    .prologue
    .line 223
    iput-object p1, p0, Lcom/sec/ims/android/internal/IIMSParams;->mLocalVideoRTCPPort:Ljava/lang/String;

    .line 224
    return-void
.end method

.method public setLocalVideoRTPPort(Ljava/lang/String;)V
    .locals 0
    .param p1, "port"    # Ljava/lang/String;

    .prologue
    .line 215
    iput-object p1, p0, Lcom/sec/ims/android/internal/IIMSParams;->mLocalVideoRTPPort:Ljava/lang/String;

    .line 216
    return-void
.end method

.method public setModifyHeader(Ljava/lang/String;)V
    .locals 0
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 151
    iput-object p1, p0, Lcom/sec/ims/android/internal/IIMSParams;->mModifySupported:Ljava/lang/String;

    .line 152
    return-void
.end method

.method public setNumberPlus(Ljava/lang/String;)V
    .locals 0
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 167
    iput-object p1, p0, Lcom/sec/ims/android/internal/IIMSParams;->mNumberPlus:Ljava/lang/String;

    .line 168
    return-void
.end method

.method public setPLettering(Ljava/lang/String;)V
    .locals 0
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 135
    iput-object p1, p0, Lcom/sec/ims/android/internal/IIMSParams;->mPLettering:Ljava/lang/String;

    .line 136
    return-void
.end method

.method public setParticipantChangedInNWayConferenceCall(Ljava/lang/String;)V
    .locals 0
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 191
    iput-object p1, p0, Lcom/sec/ims/android/internal/IIMSParams;->mParticipantChangedInNWayConferenceCall:Ljava/lang/String;

    .line 192
    return-void
.end method

.method public setPhotoRing(Ljava/lang/String;)V
    .locals 0
    .param p1, "photoRing"    # Ljava/lang/String;

    .prologue
    .line 302
    iput-object p1, p0, Lcom/sec/ims/android/internal/IIMSParams;->mPhotoRing:Ljava/lang/String;

    .line 303
    return-void
.end method

.method public setRemoteVideoRTCPPort(Ljava/lang/String;)V
    .locals 0
    .param p1, "port"    # Ljava/lang/String;

    .prologue
    .line 239
    iput-object p1, p0, Lcom/sec/ims/android/internal/IIMSParams;->mRemoteVideoRTCPPort:Ljava/lang/String;

    .line 240
    return-void
.end method

.method public setRemoteVideoRTPPort(Ljava/lang/String;)V
    .locals 0
    .param p1, "port"    # Ljava/lang/String;

    .prologue
    .line 231
    iput-object p1, p0, Lcom/sec/ims/android/internal/IIMSParams;->mRemoteVideoRTPPort:Ljava/lang/String;

    .line 232
    return-void
.end method

.method public setRetryAfter(I)V
    .locals 0
    .param p1, "retryAfter"    # I

    .prologue
    .line 286
    iput p1, p0, Lcom/sec/ims/android/internal/IIMSParams;->mRetryAfter:I

    .line 287
    return-void
.end method

.method public setUSSDData([B)V
    .locals 0
    .param p1, "data"    # [B

    .prologue
    .line 246
    iput-object p1, p0, Lcom/sec/ims/android/internal/IIMSParams;->mUSSDData:[B

    .line 247
    return-void
.end method

.method public setVideoActive(I)V
    .locals 0
    .param p1, "active"    # I

    .prologue
    .line 326
    iput p1, p0, Lcom/sec/ims/android/internal/IIMSParams;->mIsVideoActive:I

    .line 327
    return-void
.end method

.method public setVideoOrientation(I)V
    .locals 0
    .param p1, "videoOrientation"    # I

    .prologue
    .line 318
    iput p1, p0, Lcom/sec/ims/android/internal/IIMSParams;->mVideoOrientation:I

    .line 319
    return-void
.end method

.method public setisFullModeRegistered(I)V
    .locals 0
    .param p1, "flag"    # I

    .prologue
    .line 270
    iput p1, p0, Lcom/sec/ims/android/internal/IIMSParams;->mFullModeRegistered:I

    .line 271
    return-void
.end method

.method public setisHDIcon(I)V
    .locals 0
    .param p1, "flag"    # I

    .prologue
    .line 262
    iput p1, p0, Lcom/sec/ims/android/internal/IIMSParams;->misHDIcon:I

    .line 263
    return-void
.end method

.method public setttyType(Ljava/lang/String;)V
    .locals 0
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 183
    iput-object p1, p0, Lcom/sec/ims/android/internal/IIMSParams;->mTTYModeType:Ljava/lang/String;

    .line 184
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 331
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "IIMSParams [mPLettering="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/ims/android/internal/IIMSParams;->mPLettering:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mHistoryInfo="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/ims/android/internal/IIMSParams;->mHistoryInfo:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mModifySupported="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/ims/android/internal/IIMSParams;->mModifySupported:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mAudioCodec="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/ims/android/internal/IIMSParams;->mAudioCodec:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mNumberPlus="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/ims/android/internal/IIMSParams;->mNumberPlus:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mReasonCode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/ims/android/internal/IIMSParams;->mReasonCode:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mTTYModeType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/ims/android/internal/IIMSParams;->mTTYModeType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mParticipantChangedInNWayConferenceCall="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/ims/android/internal/IIMSParams;->mParticipantChangedInNWayConferenceCall:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mIsConferenceSupported="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/ims/android/internal/IIMSParams;->mIsConferenceSupported:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mIsFocus="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/ims/android/internal/IIMSParams;->mIsFocus:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mIsVMSCall="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/ims/android/internal/IIMSParams;->mIsVMSCall:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mUSSDData="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/ims/android/internal/IIMSParams;->mUSSDData:[B

    invoke-static {v1}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mIndicationFlag="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/ims/android/internal/IIMSParams;->mIndicationFlag:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", misHDIcon="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/ims/android/internal/IIMSParams;->misHDIcon:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mFullModeRegistered="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/ims/android/internal/IIMSParams;->mFullModeRegistered:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mCodecBitRate="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/ims/android/internal/IIMSParams;->mCodecBitRate:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mPhotoRing="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/ims/android/internal/IIMSParams;->mPhotoRing:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mLocalVideoRTPPort="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/ims/android/internal/IIMSParams;->mLocalVideoRTPPort:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mLocalVideoRTCPPort="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/ims/android/internal/IIMSParams;->mLocalVideoRTCPPort:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mRemoteVideoRTPPort="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/ims/android/internal/IIMSParams;->mRemoteVideoRTPPort:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mRemoteVideoRTCPPort="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/ims/android/internal/IIMSParams;->mRemoteVideoRTCPPort:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mRetryAfter="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/ims/android/internal/IIMSParams;->mRetryAfter:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mAlertInfo="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/ims/android/internal/IIMSParams;->mAlertInfo:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mVideoOrientation="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/ims/android/internal/IIMSParams;->mVideoOrientation:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mIsVideoActive="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/ims/android/internal/IIMSParams;->mIsVideoActive:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 81
    iget-object v0, p0, Lcom/sec/ims/android/internal/IIMSParams;->mPLettering:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 82
    iget-object v0, p0, Lcom/sec/ims/android/internal/IIMSParams;->mHistoryInfo:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 83
    iget-object v0, p0, Lcom/sec/ims/android/internal/IIMSParams;->mModifySupported:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 84
    iget-object v0, p0, Lcom/sec/ims/android/internal/IIMSParams;->mAudioCodec:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 85
    iget-object v0, p0, Lcom/sec/ims/android/internal/IIMSParams;->mNumberPlus:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 86
    iget-object v0, p0, Lcom/sec/ims/android/internal/IIMSParams;->mReasonCode:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 87
    iget-object v0, p0, Lcom/sec/ims/android/internal/IIMSParams;->mTTYModeType:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 88
    iget-object v0, p0, Lcom/sec/ims/android/internal/IIMSParams;->mParticipantChangedInNWayConferenceCall:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 89
    iget-object v0, p0, Lcom/sec/ims/android/internal/IIMSParams;->mIsConferenceSupported:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 90
    iget-object v0, p0, Lcom/sec/ims/android/internal/IIMSParams;->mIsFocus:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 91
    iget-object v0, p0, Lcom/sec/ims/android/internal/IIMSParams;->mIsVMSCall:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 92
    iget-object v0, p0, Lcom/sec/ims/android/internal/IIMSParams;->mPhotoRing:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 93
    iget-object v0, p0, Lcom/sec/ims/android/internal/IIMSParams;->mLocalVideoRTPPort:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 94
    iget-object v0, p0, Lcom/sec/ims/android/internal/IIMSParams;->mLocalVideoRTCPPort:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 95
    iget-object v0, p0, Lcom/sec/ims/android/internal/IIMSParams;->mRemoteVideoRTPPort:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 96
    iget-object v0, p0, Lcom/sec/ims/android/internal/IIMSParams;->mRemoteVideoRTCPPort:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 98
    iget-object v0, p0, Lcom/sec/ims/android/internal/IIMSParams;->mUSSDData:[B

    if-nez v0, :cond_0

    .line 99
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 105
    :goto_0
    iget v0, p0, Lcom/sec/ims/android/internal/IIMSParams;->mIndicationFlag:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 106
    iget v0, p0, Lcom/sec/ims/android/internal/IIMSParams;->misHDIcon:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 107
    iget v0, p0, Lcom/sec/ims/android/internal/IIMSParams;->mFullModeRegistered:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 108
    iget v0, p0, Lcom/sec/ims/android/internal/IIMSParams;->mCodecBitRate:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 109
    iget v0, p0, Lcom/sec/ims/android/internal/IIMSParams;->mRetryAfter:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 110
    iget-object v0, p0, Lcom/sec/ims/android/internal/IIMSParams;->mAlertInfo:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 111
    iget v0, p0, Lcom/sec/ims/android/internal/IIMSParams;->mVideoOrientation:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 112
    iget v0, p0, Lcom/sec/ims/android/internal/IIMSParams;->mIsVideoActive:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 113
    return-void

    .line 101
    :cond_0
    iget-object v0, p0, Lcom/sec/ims/android/internal/IIMSParams;->mUSSDData:[B

    array-length v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 102
    iget-object v0, p0, Lcom/sec/ims/android/internal/IIMSParams;->mUSSDData:[B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    goto :goto_0
.end method
