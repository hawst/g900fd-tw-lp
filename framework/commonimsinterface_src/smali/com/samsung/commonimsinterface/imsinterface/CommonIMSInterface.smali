.class public Lcom/samsung/commonimsinterface/imsinterface/CommonIMSInterface;
.super Ljava/lang/Object;
.source "CommonIMSInterface.java"


# static fields
.field public static final BEARER_MOBILE:I = 0x1

.field public static final BEARER_WIFI:I = 0x2

.field private static final LOG_TAG:Ljava/lang/String;

.field public static final TOKEN_SERVICE_CALL:I = 0x0

.field public static final TOKEN_SERVICE_GENERAL:I = 0x7

.field public static final TOKEN_SERVICE_MEDIA:I = 0x4

.field public static final TOKEN_SERVICE_PRESENCE_EAB:I = 0x1

.field public static final TOKEN_SERVICE_PRESENCE_TMO:I = 0x3

.field public static final TOKEN_SERVICE_PRESENCE_UCE:I = 0x2

.field public static final TOKEN_SERVICE_PRES_EAB:I = 0x1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final TOKEN_SERVICE_PRES_TMO:I = 0x3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final TOKEN_SERVICE_PRES_UCE:I = 0x2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final TOKEN_SERVICE_SMS:I = 0x5

.field public static final TOKEN_SERVICE_SSCONFIG:I = 0x6

.field private static mIMSInterface:[Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    const-class v0, Lcom/samsung/commonimsinterface/imsinterface/CommonIMSInterface;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/commonimsinterface/imsinterface/CommonIMSInterface;->LOG_TAG:Ljava/lang/String;

    .line 20
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/Object;

    sput-object v0, Lcom/samsung/commonimsinterface/imsinterface/CommonIMSInterface;->mIMSInterface:[Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>(ILandroid/content/Context;)V
    .locals 0
    .param p1, "token"    # I
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    return-void
.end method

.method public static declared-synchronized getAvailableBearer(ILandroid/content/Context;)I
    .locals 5
    .param p0, "token"    # I
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 156
    const-class v2, Lcom/samsung/commonimsinterface/imsinterface/CommonIMSInterface;

    monitor-enter v2

    :try_start_0
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v1

    const-string v3, "CscFeature_IMS_EnableVoLTE"

    invoke-virtual {v1, v3}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 157
    sget-object v1, Lcom/samsung/commonimsinterface/imsinterface/CommonIMSInterface;->LOG_TAG:Ljava/lang/String;

    const-string v3, "VoLTE CSC feature disabled."

    invoke-static {v1, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 170
    :goto_0
    monitor-exit v2

    return v0

    .line 161
    :cond_0
    if-nez p1, :cond_1

    .line 162
    :try_start_1
    sget-object v1, Lcom/samsung/commonimsinterface/imsinterface/CommonIMSInterface;->LOG_TAG:Ljava/lang/String;

    const-string v3, "Invalid Argument."

    invoke-static {v1, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 156
    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1

    .line 166
    :cond_1
    :try_start_2
    invoke-static {p0, p1}, Lcom/samsung/commonimsinterface/imsinterface/CommonIMSInterface;->getAvailableBearerInternal(ILandroid/content/Context;)I

    move-result v0

    .line 168
    .local v0, "bearer":I
    sget-object v1, Lcom/samsung/commonimsinterface/imsinterface/CommonIMSInterface;->LOG_TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getAvailableBearer. token:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {p0}, Lcom/samsung/commonimsinterface/imscommon/IIMSConstants$TOKEN_TYPE;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " available:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v0}, Lcom/samsung/commonimsinterface/imscommon/IIMSConstants$BEARER_TYPE;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method private static getAvailableBearerInternal(ILandroid/content/Context;)I
    .locals 5
    .param p0, "token"    # I
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 179
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/CommonIMSInterface;->isVoLTETypeHybridQCT_QIMS()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 180
    const/4 v4, 0x7

    if-ne p0, v4, :cond_0

    .line 194
    :goto_0
    return v2

    :cond_0
    move v2, v3

    .line 183
    goto :goto_0

    .line 186
    :cond_1
    invoke-static {p1, v2}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getInstance(Landroid/content/Context;Z)Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    move-result-object v1

    .line 189
    .local v1, "service":Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;
    :try_start_0
    invoke-virtual {v1}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v2

    invoke-interface {v2, p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->getAvailableBearer(I)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    goto :goto_0

    .line 190
    :catch_0
    move-exception v0

    .line 191
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v2, Lcom/samsung/commonimsinterface/imsinterface/CommonIMSInterface;->LOG_TAG:Ljava/lang/String;

    const-string v4, "RemoteException on getAvailableBearer"

    invoke-static {v2, v4}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v2, v3

    .line 194
    goto :goto_0
.end method

.method public static declared-synchronized getInstance(ILandroid/content/Context;)Ljava/lang/Object;
    .locals 2
    .param p0, "token"    # I
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 60
    const-class v0, Lcom/samsung/commonimsinterface/imsinterface/CommonIMSInterface;

    monitor-enter v0

    const/4 v1, 0x1

    :try_start_0
    invoke-static {p0, p1, v1}, Lcom/samsung/commonimsinterface/imsinterface/CommonIMSInterface;->getInstance(ILandroid/content/Context;Z)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public static declared-synchronized getInstance(ILandroid/content/Context;Z)Ljava/lang/Object;
    .locals 5
    .param p0, "token"    # I
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "waitForReady"    # Z

    .prologue
    const/4 v0, 0x0

    .line 64
    const-class v1, Lcom/samsung/commonimsinterface/imsinterface/CommonIMSInterface;

    monitor-enter v1

    :try_start_0
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v2

    const-string v3, "CscFeature_IMS_EnableVoLTE"

    invoke-virtual {v2, v3}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 65
    sget-object v2, Lcom/samsung/commonimsinterface/imsinterface/CommonIMSInterface;->LOG_TAG:Ljava/lang/String;

    const-string v3, "VoLTE CSC feature disabled."

    invoke-static {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 132
    :goto_0
    monitor-exit v1

    return-object v0

    .line 69
    :cond_0
    if-nez p1, :cond_1

    .line 70
    :try_start_1
    sget-object v2, Lcom/samsung/commonimsinterface/imsinterface/CommonIMSInterface;->LOG_TAG:Ljava/lang/String;

    const-string v3, "Invalid Argument."

    invoke-static {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 64
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 74
    :cond_1
    :try_start_2
    sget-object v2, Lcom/samsung/commonimsinterface/imsinterface/CommonIMSInterface;->LOG_TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "waitForReady ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] Package ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 76
    sget-object v2, Lcom/samsung/commonimsinterface/imsinterface/CommonIMSInterface;->mIMSInterface:[Ljava/lang/Object;

    aget-object v2, v2, p0

    if-nez v2, :cond_8

    .line 77
    packed-switch p0, :pswitch_data_0

    .line 123
    sget-object v2, Lcom/samsung/commonimsinterface/imsinterface/CommonIMSInterface;->LOG_TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "No permission for creating token "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {p0}, Lcom/samsung/commonimsinterface/imscommon/IIMSConstants$TOKEN_TYPE;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " Id:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 79
    :pswitch_0
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/CommonIMSInterface;->isVoLTETypeHybridQCT_QIMS()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 80
    sget-object v0, Lcom/samsung/commonimsinterface/imsinterface/CommonIMSInterface;->mIMSInterface:[Ljava/lang/Object;

    new-instance v2, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralLibrary;

    invoke-direct {v2, p1}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralLibrary;-><init>(Landroid/content/Context;)V

    aput-object v2, v0, p0

    .line 127
    :goto_1
    sget-object v0, Lcom/samsung/commonimsinterface/imsinterface/CommonIMSInterface;->LOG_TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Interface for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {p0}, Lcom/samsung/commonimsinterface/imscommon/IIMSConstants$TOKEN_TYPE;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " has been Created!"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    :goto_2
    sget-object v0, Lcom/samsung/commonimsinterface/imsinterface/CommonIMSInterface;->mIMSInterface:[Ljava/lang/Object;

    aget-object v0, v0, p0

    goto/16 :goto_0

    .line 82
    :cond_2
    sget-object v0, Lcom/samsung/commonimsinterface/imsinterface/CommonIMSInterface;->mIMSInterface:[Ljava/lang/Object;

    new-instance v2, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;

    invoke-direct {v2, p1, p2}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;-><init>(Landroid/content/Context;Z)V

    aput-object v2, v0, p0

    goto :goto_1

    .line 86
    :pswitch_1
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/CommonIMSInterface;->isVoLTETypeHybridQCT_QIMS()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 87
    sget-object v0, Lcom/samsung/commonimsinterface/imsinterface/CommonIMSInterface;->mIMSInterface:[Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v2, v0, p0

    goto :goto_1

    .line 89
    :cond_3
    sget-object v0, Lcom/samsung/commonimsinterface/imsinterface/CommonIMSInterface;->mIMSInterface:[Ljava/lang/Object;

    new-instance v2, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;

    invoke-direct {v2, p1, p2}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;-><init>(Landroid/content/Context;Z)V

    aput-object v2, v0, p0

    goto :goto_1

    .line 95
    :pswitch_2
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/CommonIMSInterface;->isVoLTETypeHybridQCT_QIMS()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 96
    sget-object v0, Lcom/samsung/commonimsinterface/imsinterface/CommonIMSInterface;->mIMSInterface:[Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v2, v0, p0

    goto :goto_1

    .line 98
    :cond_4
    sget-object v0, Lcom/samsung/commonimsinterface/imsinterface/CommonIMSInterface;->mIMSInterface:[Ljava/lang/Object;

    new-instance v2, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForPresenceService;

    invoke-direct {v2, p1, p2}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForPresenceService;-><init>(Landroid/content/Context;Z)V

    aput-object v2, v0, p0

    goto :goto_1

    .line 102
    :pswitch_3
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/CommonIMSInterface;->isVoLTETypeHybridQCT_QIMS()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 103
    sget-object v0, Lcom/samsung/commonimsinterface/imsinterface/CommonIMSInterface;->mIMSInterface:[Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v2, v0, p0

    goto :goto_1

    .line 105
    :cond_5
    sget-object v0, Lcom/samsung/commonimsinterface/imsinterface/CommonIMSInterface;->mIMSInterface:[Ljava/lang/Object;

    new-instance v2, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;

    invoke-direct {v2, p1, p2}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;-><init>(Landroid/content/Context;Z)V

    aput-object v2, v0, p0

    goto :goto_1

    .line 109
    :pswitch_4
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/CommonIMSInterface;->isVoLTETypeHybridQCT_QIMS()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 110
    sget-object v0, Lcom/samsung/commonimsinterface/imsinterface/CommonIMSInterface;->mIMSInterface:[Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v2, v0, p0

    goto :goto_1

    .line 112
    :cond_6
    sget-object v0, Lcom/samsung/commonimsinterface/imsinterface/CommonIMSInterface;->mIMSInterface:[Ljava/lang/Object;

    new-instance v2, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForSMSService;

    invoke-direct {v2, p1, p2}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForSMSService;-><init>(Landroid/content/Context;Z)V

    aput-object v2, v0, p0

    goto/16 :goto_1

    .line 116
    :pswitch_5
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/CommonIMSInterface;->isVoLTETypeHybridQCT_QIMS()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 117
    sget-object v0, Lcom/samsung/commonimsinterface/imsinterface/CommonIMSInterface;->mIMSInterface:[Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v2, v0, p0

    goto/16 :goto_1

    .line 119
    :cond_7
    sget-object v0, Lcom/samsung/commonimsinterface/imsinterface/CommonIMSInterface;->mIMSInterface:[Ljava/lang/Object;

    new-instance v2, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForSSConfigService;

    invoke-direct {v2, p1, p2}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForSSConfigService;-><init>(Landroid/content/Context;Z)V

    aput-object v2, v0, p0

    goto/16 :goto_1

    .line 129
    :cond_8
    sget-object v0, Lcom/samsung/commonimsinterface/imsinterface/CommonIMSInterface;->LOG_TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Interface for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {p0}, Lcom/samsung/commonimsinterface/imscommon/IIMSConstants$TOKEN_TYPE;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " Already Exists!"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_2

    .line 77
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_0
    .end packed-switch
.end method

.method private static isKorOperator()Z
    .locals 2

    .prologue
    .line 197
    const-string v1, "ro.csc.sales_code"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 198
    .local v0, "csc":Ljava/lang/String;
    const-string v1, "LGT"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "LUC"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "LUO"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "KTT"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "KTC"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "KTO"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "SKT"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "SKC"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "SKO"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "KOO"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 201
    :cond_0
    const/4 v1, 0x1

    .line 203
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static declared-synchronized isServiceAvailable(ILandroid/content/Context;)Z
    .locals 5
    .param p0, "token"    # I
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 137
    const-class v2, Lcom/samsung/commonimsinterface/imsinterface/CommonIMSInterface;

    monitor-enter v2

    :try_start_0
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v1

    const-string v3, "CscFeature_IMS_EnableVoLTE"

    invoke-virtual {v1, v3}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 138
    sget-object v1, Lcom/samsung/commonimsinterface/imsinterface/CommonIMSInterface;->LOG_TAG:Ljava/lang/String;

    const-string v3, "VoLTE CSC feature disabled."

    invoke-static {v1, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 151
    :goto_0
    monitor-exit v2

    return v0

    .line 142
    :cond_0
    if-nez p1, :cond_1

    .line 143
    :try_start_1
    sget-object v1, Lcom/samsung/commonimsinterface/imsinterface/CommonIMSInterface;->LOG_TAG:Ljava/lang/String;

    const-string v3, "Invalid Argument."

    invoke-static {v1, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 137
    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1

    .line 147
    :cond_1
    :try_start_2
    invoke-static {p0, p1}, Lcom/samsung/commonimsinterface/imsinterface/CommonIMSInterface;->getAvailableBearerInternal(ILandroid/content/Context;)I

    move-result v1

    if-lez v1, :cond_2

    const/4 v0, 0x1

    .line 149
    .local v0, "available":Z
    :cond_2
    sget-object v1, Lcom/samsung/commonimsinterface/imsinterface/CommonIMSInterface;->LOG_TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "isServiceAvailable. token:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {p0}, Lcom/samsung/commonimsinterface/imscommon/IIMSConstants$TOKEN_TYPE;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " available:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public static isVoLTETypeHybridQCT_QIMS()Z
    .locals 2

    .prologue
    .line 175
    const-string v0, "IMS_HYBRID_QCT_QIMS"

    const-string v1, "IMS_HYBRID_QCT"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
