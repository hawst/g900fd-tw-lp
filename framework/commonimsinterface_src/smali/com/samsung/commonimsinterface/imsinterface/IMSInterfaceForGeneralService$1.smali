.class Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService$1;
.super Ljava/lang/Object;
.source "IMSInterfaceForGeneralService.java"

# interfaces
.implements Lcom/samsung/commonimsinterface/imscommon/IIMSServiceConnectionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;-><init>(Landroid/content/Context;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;


# direct methods
.method constructor <init>(Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;)V
    .locals 0

    .prologue
    .line 47
    iput-object p1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService$1;->this$0:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onConnected()V
    .locals 3

    .prologue
    .line 50
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string v2, "onConnected"

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 53
    :try_start_0
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService$1;->this$0:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;

    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;
    invoke-static {v1}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->access$200(Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;)Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService$1;->this$0:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;

    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->mIMSRemoteListenerListForConnectivity:Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;
    invoke-static {v2}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->access$100(Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;)Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->registerForConnectivityStateChange(Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;)V

    .line 54
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService$1;->this$0:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;

    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;
    invoke-static {v1}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->access$200(Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;)Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService$1;->this$0:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;

    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->mIMSRemoteListenerListForRegistration:Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;
    invoke-static {v2}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->access$300(Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;)Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->registerForRegisterStateChange(Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;)V

    .line 55
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService$1;->this$0:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;

    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;
    invoke-static {v1}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->access$200(Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;)Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService$1;->this$0:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;

    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->mIMSRemoteListenerListForSetting:Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;
    invoke-static {v2}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->access$400(Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;)Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->registerForSettingStateListener(Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;)V

    .line 56
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService$1;->this$0:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;

    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;
    invoke-static {v1}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->access$200(Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;)Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService$1;->this$0:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;

    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->mIMSRemoteListenerListForAPCS:Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;
    invoke-static {v2}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->access$500(Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;)Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->registerForAPCSStateChange(Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;)V

    .line 58
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService$1;->this$0:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;

    const/4 v2, 0x1

    # invokes: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->notifyServiceState(Z)V
    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->access$600(Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 62
    :goto_0
    return-void

    .line 59
    :catch_0
    move-exception v0

    .line 60
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public onDisconnected()V
    .locals 3

    .prologue
    .line 66
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string v2, "onDisconnected"

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    :try_start_0
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService$1;->this$0:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;

    const/4 v2, 0x0

    # invokes: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->notifyServiceState(Z)V
    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->access$600(Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 73
    :goto_0
    return-void

    .line 70
    :catch_0
    move-exception v0

    .line 71
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method
