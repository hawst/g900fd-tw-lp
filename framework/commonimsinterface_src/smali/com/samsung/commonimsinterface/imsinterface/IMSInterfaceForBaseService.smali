.class public Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;
.super Ljava/lang/Object;
.source "IMSInterfaceForBaseService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService$4;,
        Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService$GetService;,
        Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService$NotifyServiceStatus;
    }
.end annotation


# static fields
.field private static final IMS_SERVICE_WAIT_COUNT:I = 0x10

.field private static final IMS_SERVICE_WAIT_INTERNAL:I = 0x1f4

.field private static final LOG_TAG:Ljava/lang/String;

.field private static mInstance:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;


# instance fields
.field private mConnectionListenerList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/commonimsinterface/imscommon/IIMSServiceConnectionListener;",
            ">;"
        }
    .end annotation
.end field

.field private mDummyServiceStub:Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;

.field private mIMSRemoteListenerList:Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;

.field private mIMSService:Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

.field protected mIMSServiceConnectionListener:Lcom/samsung/commonimsinterface/imscommon/IIMSServiceConnectionListener;

.field private mIMSServiceConnectionManager:Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;

.field protected mIMSServiceReadyListener:Lcom/samsung/commonimsinterface/imscommon/IIMSListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const-class v0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;

    .line 33
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->mInstance:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Z)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "waitForReady"    # Z

    .prologue
    const/4 v0, 0x0

    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object v0, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->mIMSRemoteListenerList:Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;

    .line 35
    iput-object v0, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->mIMSService:Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    .line 36
    iput-object v0, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->mIMSServiceConnectionManager:Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;

    .line 37
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->mConnectionListenerList:Ljava/util/List;

    .line 90
    new-instance v0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService$1;

    invoke-direct {v0, p0}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService$1;-><init>(Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;)V

    iput-object v0, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->mIMSServiceConnectionListener:Lcom/samsung/commonimsinterface/imscommon/IIMSServiceConnectionListener;

    .line 121
    new-instance v0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService$2;

    invoke-direct {v0, p0}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService$2;-><init>(Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;)V

    iput-object v0, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->mIMSServiceReadyListener:Lcom/samsung/commonimsinterface/imscommon/IIMSListener;

    .line 251
    new-instance v0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService$3;

    invoke-direct {v0, p0}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService$3;-><init>(Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;)V

    iput-object v0, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->mDummyServiceStub:Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;

    .line 58
    new-instance v0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;

    invoke-direct {v0, p1}, Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->mIMSRemoteListenerList:Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;

    .line 59
    new-instance v0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;

    invoke-direct {v0, p1}, Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->mIMSServiceConnectionManager:Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;

    .line 61
    invoke-virtual {p0, p2}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->startService(Z)V

    .line 62
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;)Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->mIMSService:Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    return-object v0
.end method

.method static synthetic access$102(Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;)Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;
    .param p1, "x1"    # Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    .prologue
    .line 25
    iput-object p1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->mIMSService:Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    return-object p1
.end method

.method static synthetic access$200(Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;)Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->mIMSRemoteListenerList:Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;

    return-object v0
.end method

.method public static declared-synchronized getInstance(Landroid/content/Context;Z)Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "waitForReady"    # Z

    .prologue
    .line 65
    const-class v1, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->mInstance:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    if-nez v0, :cond_0

    .line 66
    new-instance v0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-direct {v0, p0, p1}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;-><init>(Landroid/content/Context;Z)V

    sput-object v0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->mInstance:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    .line 69
    :cond_0
    sget-object v0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->mInstance:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 65
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public declared-synchronized addServiceConnectionListener(Lcom/samsung/commonimsinterface/imscommon/IIMSServiceConnectionListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/samsung/commonimsinterface/imscommon/IIMSServiceConnectionListener;

    .prologue
    .line 85
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->mConnectionListenerList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 87
    sget-object v0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService$NotifyServiceStatus;->ADDED_LISTENER:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService$NotifyServiceStatus;

    invoke-virtual {p0, v0}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->notifyServiceState(Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService$NotifyServiceStatus;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 88
    monitor-exit p0

    return-void

    .line 85
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected getIMSService(Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService$GetService;)Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;
    .locals 7
    .param p1, "reason"    # Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService$GetService;

    .prologue
    .line 201
    const/4 v0, 0x0

    .line 202
    .local v0, "binder":Landroid/os/IBinder;
    const/4 v3, 0x0

    .line 204
    .local v3, "service":Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;
    const/4 v2, 0x0

    .local v2, "index":I
    :goto_0
    const/16 v4, 0x10

    if-ge v2, v4, :cond_4

    .line 205
    if-nez v0, :cond_0

    .line 206
    const-string v4, "ims"

    invoke-static {v4}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    .line 208
    if-eqz v0, :cond_0

    .line 209
    invoke-static {v0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v3

    .line 213
    :cond_0
    if-eqz v3, :cond_1

    .line 214
    sget-object v4, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService$4;->$SwitchMap$com$samsung$commonimsinterface$imsinterface$IMSInterfaceForBaseService$GetService:[I

    invoke-virtual {p1}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService$GetService;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 237
    :cond_1
    :goto_1
    :try_start_0
    sget-object v5, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "getIMSService ["

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, "] Waiting for Service Ready "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const/16 v4, 0xa

    if-ge v2, v4, :cond_2

    const-string v4, " "

    :goto_2
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, "... "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, "Connected"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " ["

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    if-eqz v3, :cond_3

    const/4 v4, 0x1

    :goto_3
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, "] "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v5, v4}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 241
    const-wide/16 v4, 0x1f4

    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1

    .line 204
    :goto_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 217
    :pswitch_0
    :try_start_1
    invoke-interface {v3}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->isIMSServiceReady()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 218
    sget-object v4, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "getIMSService ["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "] : Service Ready!"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    move-object v4, v3

    .line 248
    :goto_5
    return-object v4

    .line 221
    :catch_0
    move-exception v1

    .line 222
    .local v1, "e":Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1

    .line 227
    .end local v1    # "e":Landroid/os/RemoteException;
    :pswitch_1
    sget-object v4, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "getIMSService ["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "] : Service Connected!"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move-object v4, v3

    .line 228
    goto :goto_5

    .line 237
    :cond_2
    :try_start_2
    const-string v4, ""
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_1

    goto/16 :goto_2

    :cond_3
    const/4 v4, 0x0

    goto :goto_3

    .line 242
    :catch_1
    move-exception v1

    .line 243
    .local v1, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v1}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_4

    .line 247
    .end local v1    # "e":Ljava/lang/InterruptedException;
    :cond_4
    sget-object v4, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "getIMSService ["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "] : IMS Service Is Not Ready Yet!"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 248
    const/4 v4, 0x0

    goto :goto_5

    .line 214
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;
    .locals 2

    .prologue
    .line 74
    :try_start_0
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->mIMSService:Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->mIMSService:Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    invoke-interface {v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->isIMSServiceReady()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 75
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->mIMSService:Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 81
    :goto_0
    return-object v1

    .line 77
    :catch_0
    move-exception v0

    .line 78
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    .line 81
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->mDummyServiceStub:Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;

    goto :goto_0
.end method

.method protected declared-synchronized notifyServiceState(Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService$NotifyServiceStatus;)V
    .locals 6
    .param p1, "reason"    # Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService$NotifyServiceStatus;

    .prologue
    .line 144
    monitor-enter p0

    :try_start_0
    sget-object v3, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "notifyServiceState : Reason ["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "] "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    sget-object v3, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService$4;->$SwitchMap$com$samsung$commonimsinterface$imsinterface$IMSInterfaceForBaseService$NotifyServiceStatus:[I

    invoke-virtual {p1}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService$NotifyServiceStatus;->ordinal()I

    move-result v4

    aget v3, v3, v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    packed-switch v3, :pswitch_data_0

    .line 181
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 148
    :pswitch_0
    :try_start_1
    iget-object v3, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->mConnectionListenerList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/commonimsinterface/imscommon/IIMSServiceConnectionListener;

    .line 149
    .local v2, "listener":Lcom/samsung/commonimsinterface/imscommon/IIMSServiceConnectionListener;
    invoke-interface {v2}, Lcom/samsung/commonimsinterface/imscommon/IIMSServiceConnectionListener;->onConnected()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 144
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "listener":Lcom/samsung/commonimsinterface/imscommon/IIMSServiceConnectionListener;
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3

    .line 155
    :pswitch_1
    :try_start_2
    iget-object v3, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->mConnectionListenerList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .restart local v1    # "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/commonimsinterface/imscommon/IIMSServiceConnectionListener;

    .line 156
    .restart local v2    # "listener":Lcom/samsung/commonimsinterface/imscommon/IIMSServiceConnectionListener;
    invoke-interface {v2}, Lcom/samsung/commonimsinterface/imscommon/IIMSServiceConnectionListener;->onDisconnected()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    .line 162
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "listener":Lcom/samsung/commonimsinterface/imscommon/IIMSServiceConnectionListener;
    :pswitch_2
    :try_start_3
    iget-object v3, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->mIMSService:Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->mIMSService:Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    invoke-interface {v3}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->isIMSServiceReady()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 163
    iget-object v3, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->mConnectionListenerList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .restart local v1    # "i$":Ljava/util/Iterator;
    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/commonimsinterface/imscommon/IIMSServiceConnectionListener;

    .line 164
    .restart local v2    # "listener":Lcom/samsung/commonimsinterface/imscommon/IIMSServiceConnectionListener;
    invoke-interface {v2}, Lcom/samsung/commonimsinterface/imscommon/IIMSServiceConnectionListener;->onConnected()V
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_3

    .line 171
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "listener":Lcom/samsung/commonimsinterface/imscommon/IIMSServiceConnectionListener;
    :catch_0
    move-exception v0

    .line 172
    .local v0, "e":Landroid/os/RemoteException;
    :try_start_4
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 167
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_1
    :try_start_5
    iget-object v3, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->mConnectionListenerList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .restart local v1    # "i$":Ljava/util/Iterator;
    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/commonimsinterface/imscommon/IIMSServiceConnectionListener;

    .line 168
    .restart local v2    # "listener":Lcom/samsung/commonimsinterface/imscommon/IIMSServiceConnectionListener;
    invoke-interface {v2}, Lcom/samsung/commonimsinterface/imscommon/IIMSServiceConnectionListener;->onDisconnected()V
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_4

    .line 146
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected startService(Z)V
    .locals 6
    .param p1, "waitForReady"    # Z

    .prologue
    .line 184
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->mIMSService:Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    if-nez v0, :cond_0

    .line 185
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->mIMSServiceConnectionManager:Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;

    const-string v1, "com.samsung.commonimsinterface.imsstub.common.IMSService"

    const-string v2, "com.samsung.commonimsservice"

    const-string v3, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->mIMSServiceConnectionListener:Lcom/samsung/commonimsinterface/imscommon/IIMSServiceConnectionListener;

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;->startIMSService(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/samsung/commonimsinterface/imscommon/IIMSServiceConnectionListener;)V

    .line 192
    :cond_0
    if-eqz p1, :cond_1

    .line 193
    sget-object v0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService$GetService;->WAIT_FOR_READY:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService$GetService;

    invoke-virtual {p0, v0}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getIMSService(Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService$GetService;)Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->mIMSService:Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    .line 196
    :cond_1
    sget-object v0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "startService : WaitForReady ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "IMSService"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->mIMSService:Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 198
    return-void
.end method
