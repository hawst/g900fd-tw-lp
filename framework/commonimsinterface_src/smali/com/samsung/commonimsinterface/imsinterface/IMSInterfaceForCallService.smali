.class public Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;
.super Ljava/lang/Object;
.source "IMSInterfaceForCallService.java"

# interfaces
.implements Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCall;


# static fields
.field private static final LOG_TAG:Ljava/lang/String;


# instance fields
.field private mIMSRemoteListenerList:Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;

.field private mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const-class v0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Z)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "waitForReady"    # Z

    .prologue
    const/4 v0, 0x0

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object v0, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->mIMSRemoteListenerList:Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;

    .line 24
    iput-object v0, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    .line 33
    invoke-static {p1, p2}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getInstance(Landroid/content/Context;Z)Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    .line 34
    new-instance v0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;

    invoke-direct {v0, p1}, Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->mIMSRemoteListenerList:Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;

    .line 36
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    new-instance v1, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService$1;

    invoke-direct {v1, p0}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService$1;-><init>(Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;)V

    invoke-virtual {v0, v1}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->addServiceConnectionListener(Lcom/samsung/commonimsinterface/imscommon/IIMSServiceConnectionListener;)V

    .line 53
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 17
    sget-object v0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->LOG_TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;)Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;

    .prologue
    .line 17
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->mIMSRemoteListenerList:Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;)Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;

    .prologue
    .line 17
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    return-object v0
.end method


# virtual methods
.method public acceptChangeRequest(I)V
    .locals 4
    .param p1, "sessionHashCode"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 392
    sget-object v1, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->LOG_TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "acceptChangeRequest() with sessionHashCode : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 395
    :try_start_0
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v1}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v1

    invoke-interface {v1, p1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->acceptChangeRequest(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 399
    return-void

    .line 396
    :catch_0
    move-exception v0

    .line 397
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public acceptChangeRequest(II)V
    .locals 4
    .param p1, "sessionHashCode"    # I
    .param p2, "callType"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 403
    sget-object v1, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->LOG_TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "acceptChangeRequest() with sessionHashCode : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 406
    :try_start_0
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v1}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v1

    invoke-interface {v1, p1, p2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->acceptChangeRequestWithCallType(II)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 410
    return-void

    .line 407
    :catch_0
    move-exception v0

    .line 408
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public addParticipantToNWayConferenceCall(II)V
    .locals 4
    .param p1, "confSessionHashCode"    # I
    .param p2, "sessionHashCodeToBeAdded"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 900
    sget-object v1, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->LOG_TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "addParticipantToNWayConferenceCall() with confsessionHashCode : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " with sessionHashCodeToBeAdded : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 903
    :try_start_0
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v1}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v1

    invoke-interface {v1, p1, p2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->addParticipantToNWayConferenceCall(II)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 907
    return-void

    .line 904
    :catch_0
    move-exception v0

    .line 905
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public addUserForConferenceCall(Ljava/lang/String;II)I
    .locals 4
    .param p1, "dialString"    # Ljava/lang/String;
    .param p2, "sessionHashCode"    # I
    .param p3, "type"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 675
    sget-object v1, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->LOG_TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "addUserForConferenceCall() with dialString : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " with sessionHashCode : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " with type : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 678
    :try_start_0
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v1}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v1

    invoke-interface {v1, p2, p1, p3}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->addUserForConferenceCall(ILjava/lang/String;I)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    return v1

    .line 679
    :catch_0
    move-exception v0

    .line 680
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public answerCall(I)V
    .locals 4
    .param p1, "sessionHashCode"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 196
    sget-object v1, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->LOG_TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "answerCall() with sessionHashCode : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 198
    :try_start_0
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v1}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v1

    invoke-interface {v1, p1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->answerCall(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 202
    return-void

    .line 199
    :catch_0
    move-exception v0

    .line 200
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public answerCall(II)V
    .locals 4
    .param p1, "sessionHashCode"    # I
    .param p2, "callType"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 213
    sget-object v1, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->LOG_TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "answerCall() with sessionHashCode : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " callType : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 215
    :try_start_0
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v1}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v1

    invoke-interface {v1, p1, p2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->answerCallWithCallType(II)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 219
    return-void

    .line 216
    :catch_0
    move-exception v0

    .line 217
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public answerCallAudioOnly(I)V
    .locals 4
    .param p1, "sessionHashCode"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 228
    sget-object v1, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->LOG_TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "answerCallAudioOnly() with sessionHashCode : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 230
    :try_start_0
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v1}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v1

    invoke-interface {v1, p1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->answerCallAudioOnly(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 234
    return-void

    .line 231
    :catch_0
    move-exception v0

    .line 232
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public blacklistCheckDone()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 562
    sget-object v1, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->LOG_TAG:Ljava/lang/String;

    const-string v2, "blacklistCheckDone()"

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 565
    :try_start_0
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v1}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v1

    invoke-interface {v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->send180Ringing()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 569
    return-void

    .line 566
    :catch_0
    move-exception v0

    .line 567
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public changeCall(II)V
    .locals 4
    .param p1, "sessionHashCode"    # I
    .param p2, "type"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 374
    sget-object v1, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->LOG_TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "changeCall() with sessionHashCode : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " with type "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 377
    :try_start_0
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v1}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v1

    invoke-interface {v1, p1, p2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->changeCall(II)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 381
    return-void

    .line 378
    :catch_0
    move-exception v0

    .line 379
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public changeEPDGAudioPath(I)V
    .locals 4
    .param p1, "direction"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 997
    sget-object v1, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->LOG_TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "changeEPDGAudioPath() with direction : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1000
    :try_start_0
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v1}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v1

    invoke-interface {v1, p1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->changeEPDGAudioPath(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1004
    return-void

    .line 1001
    :catch_0
    move-exception v0

    .line 1002
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public deRegisterForCallStateChange(Lcom/samsung/commonimsinterface/imscommon/IIMSListener;)V
    .locals 3
    .param p1, "listener"    # Lcom/samsung/commonimsinterface/imscommon/IIMSListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 64
    sget-object v0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->LOG_TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "deRegisterForCallStateChange() with listener : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->mIMSRemoteListenerList:Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;

    invoke-virtual {v0, p1}, Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;->removeListener(Lcom/samsung/commonimsinterface/imscommon/IIMSListener;)Z

    .line 67
    return-void
.end method

.method public downgradeCall(II)V
    .locals 4
    .param p1, "sessionHashCode"    # I
    .param p2, "type"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 353
    sget-object v1, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->LOG_TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "downgradeCall() with sessionHashCode : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " with type "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 356
    :try_start_0
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v1}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v1

    invoke-interface {v1, p1, p2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->downgradeCall(II)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 360
    return-void

    .line 357
    :catch_0
    move-exception v0

    .line 358
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public emergencyDeregister()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 872
    sget-object v1, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->LOG_TAG:Ljava/lang/String;

    const-string v2, "emergencyDeregister"

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 875
    :try_start_0
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v1}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v1

    invoke-interface {v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->emergencyDeregister()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 879
    return-void

    .line 876
    :catch_0
    move-exception v0

    .line 877
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public emergencyRegister()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 858
    sget-object v1, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->LOG_TAG:Ljava/lang/String;

    const-string v2, "emergencyRegister"

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 861
    :try_start_0
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v1}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v1

    invoke-interface {v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->emergencyRegister()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 865
    return-void

    .line 862
    :catch_0
    move-exception v0

    .line 863
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public endCall(I)V
    .locals 4
    .param p1, "sessionHashCode"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 243
    sget-object v1, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->LOG_TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "endCall() with sessionHashCode : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 246
    :try_start_0
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v1}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v1

    invoke-interface {v1, p1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->endCall(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 250
    return-void

    .line 247
    :catch_0
    move-exception v0

    .line 248
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public endCall(II)V
    .locals 4
    .param p1, "sessionHashCode"    # I
    .param p2, "reason"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 254
    sget-object v1, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->LOG_TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "endCall() with sessionHashCode : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " with reason : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 257
    :try_start_0
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v1}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v1

    invoke-interface {v1, p1, p2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->endCallWithReason(II)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 261
    return-void

    .line 258
    :catch_0
    move-exception v0

    .line 259
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public getCurrentLatchedNetwork()Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 735
    sget-object v2, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->LOG_TAG:Ljava/lang/String;

    const-string v3, "getCurrentLatchedNetwork()"

    invoke-static {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 737
    const/4 v1, 0x0

    .line 740
    .local v1, "result":Ljava/lang/String;
    :try_start_0
    iget-object v2, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v2}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v2

    invoke-interface {v2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->getCurrentLatchedNetwork()Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 745
    return-object v1

    .line 741
    :catch_0
    move-exception v0

    .line 742
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v2, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public getMaxVolume(I)I
    .locals 5
    .param p1, "sessionHashCode"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 655
    sget-object v2, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->LOG_TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getMaxVolume() with sessionHashCode : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 657
    const/4 v1, -0x1

    .line 660
    .local v1, "result":I
    :try_start_0
    iget-object v2, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v2}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v2

    invoke-interface {v2, p1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->getMaxVolume(I)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 665
    return v1

    .line 661
    :catch_0
    move-exception v0

    .line 662
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v2, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public getParticipantListForNWayConferenceCall(I)[Ljava/lang/String;
    .locals 5
    .param p1, "sessionHashCode"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 937
    sget-object v2, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->LOG_TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getParticipantListForNWayConferenceCall() with sessionHashCode : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 939
    const/4 v1, 0x0

    .line 942
    .local v1, "result":[Ljava/lang/String;
    :try_start_0
    iget-object v2, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v2}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v2

    invoke-interface {v2, p1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->getParticipantSessionIdListForNWayConferenceCall(I)[Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 947
    return-object v1

    .line 943
    :catch_0
    move-exception v0

    .line 944
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v2, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public getParticipantListSizeForNWayConferenceCall(I)I
    .locals 5
    .param p1, "sessionHashCode"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 922
    sget-object v2, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->LOG_TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getParticipantListSizeForNWayConferenceCall() with sessionHashCode : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 924
    const/4 v1, -0x1

    .line 927
    .local v1, "result":I
    :try_start_0
    iget-object v2, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v2}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v2

    invoke-interface {v2, p1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->getParticipantListSizeForNWayConferenceCall(I)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 932
    return v1

    .line 928
    :catch_0
    move-exception v0

    .line 929
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v2, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public getParticipantPeerURIFromNWayConferenceCall(II)Ljava/lang/String;
    .locals 5
    .param p1, "sessionHashCode"    # I
    .param p2, "position"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 952
    sget-object v2, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->LOG_TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getParticipantPeerURIFromNWayConferenceCall() with sessionHashCode : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " with position : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 954
    const/4 v1, 0x0

    .line 957
    .local v1, "result":Ljava/lang/String;
    :try_start_0
    iget-object v2, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v2}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v2

    invoke-interface {v2, p1, p2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->getParticipantPeerURIFromNWayConferenceCall(II)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 962
    return-object v1

    .line 958
    :catch_0
    move-exception v0

    .line 959
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v2, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public getTtyMode()I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 542
    sget-object v2, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->LOG_TAG:Ljava/lang/String;

    const-string v3, "getTtyMode()"

    invoke-static {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 544
    const/4 v1, -0x1

    .line 547
    .local v1, "result":I
    :try_start_0
    iget-object v2, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v2}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v2

    invoke-interface {v2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->getTtyMode()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 552
    return v1

    .line 548
    :catch_0
    move-exception v0

    .line 549
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v2, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public handleHOFail(I)V
    .locals 4
    .param p1, "sessionHashCode"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 686
    sget-object v1, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->LOG_TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "handleHOFail() with sessionHashCode : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 689
    :try_start_0
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v1}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v1

    invoke-interface {v1, p1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->sendReInvite(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 693
    return-void

    .line 690
    :catch_0
    move-exception v0

    .line 691
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public holdCall(I)V
    .locals 4
    .param p1, "sessionHashCode"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 300
    sget-object v1, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->LOG_TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "holdCall() with sessionHashCode : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 303
    :try_start_0
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v1}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v1

    invoke-interface {v1, p1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->holdCall(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 307
    return-void

    .line 304
    :catch_0
    move-exception v0

    .line 305
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public indicateSrvccHoStarted(I)V
    .locals 4
    .param p1, "sessionHashCode"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 697
    sget-object v1, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->LOG_TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "indicateSrvccHoStarted() with sessionHashCode : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 700
    :try_start_0
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v1}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v1

    invoke-interface {v1, p1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->indicateSrvccHoStarted(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 704
    return-void

    .line 701
    :catch_0
    move-exception v0

    .line 702
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public isCallBarred(I)Z
    .locals 5
    .param p1, "callType"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 1008
    sget-object v2, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->LOG_TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "isCallBarred() with callType : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1010
    const/4 v1, 0x0

    .line 1013
    .local v1, "result":Z
    :try_start_0
    iget-object v2, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v2}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v2

    invoke-interface {v2, p1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->isCallBarred(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1018
    return v1

    .line 1014
    :catch_0
    move-exception v0

    .line 1015
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v2, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public isDeviceOnEHRPD()Z
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 788
    sget-object v2, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->LOG_TAG:Ljava/lang/String;

    const-string v3, "isDeviceOnEHRPD()"

    invoke-static {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 790
    const/4 v1, 0x0

    .line 793
    .local v1, "result":Z
    :try_start_0
    iget-object v2, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v2}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v2

    invoke-interface {v2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->isOnEHRPD()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 798
    return v1

    .line 794
    :catch_0
    move-exception v0

    .line 795
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v2, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public isDeviceOnLTE()Z
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 769
    sget-object v2, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->LOG_TAG:Ljava/lang/String;

    const-string v3, "isDeviceOnLTE()"

    invoke-static {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 771
    const/4 v1, 0x0

    .line 774
    .local v1, "result":Z
    :try_start_0
    iget-object v2, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v2}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v2

    invoke-interface {v2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->isOnLTE()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 779
    return v1

    .line 775
    :catch_0
    move-exception v0

    .line 776
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v2, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public isIMSEnabledOnWifi()Z
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 750
    sget-object v2, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->LOG_TAG:Ljava/lang/String;

    const-string v3, "isIMSEnabledOnWifi()"

    invoke-static {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 752
    const/4 v1, 0x0

    .line 755
    .local v1, "result":Z
    :try_start_0
    iget-object v2, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v2}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v2

    invoke-interface {v2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->isIMSEnabledOnWifi()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 760
    return v1

    .line 756
    :catch_0
    move-exception v0

    .line 757
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v2, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public isImsForbidden()Z
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 724
    sget-object v1, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->LOG_TAG:Ljava/lang/String;

    const-string v2, "isImsForbidden()"

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 727
    :try_start_0
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v1}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v1

    invoke-interface {v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->isImsForbidden()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    return v1

    .line 728
    :catch_0
    move-exception v0

    .line 729
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public isInEPDGCall()Z
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 967
    sget-object v2, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->LOG_TAG:Ljava/lang/String;

    const-string v3, "isInEPDGCall()"

    invoke-static {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 969
    const/4 v1, 0x0

    .line 972
    .local v1, "result":Z
    :try_start_0
    iget-object v2, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v2}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v2

    invoke-interface {v2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->isInEPDGCall()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 977
    return v1

    .line 973
    :catch_0
    move-exception v0

    .line 974
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v2, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public isMuted(I)Z
    .locals 5
    .param p1, "sessionHashCode"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 588
    sget-object v2, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->LOG_TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "isMuted() with sessionHashCode : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 590
    const/4 v1, 0x0

    .line 593
    .local v1, "result":Z
    :try_start_0
    iget-object v2, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v2}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v2

    invoke-interface {v2, p1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->isMuted(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 598
    return v1

    .line 594
    :catch_0
    move-exception v0

    .line 595
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v2, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public makeMediaCall(Ljava/lang/String;I)I
    .locals 3
    .param p1, "peerURI"    # Ljava/lang/String;
    .param p2, "type"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 155
    sget-object v0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->LOG_TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "makeMediaCall() with peerURI : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " with type : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 157
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->makeMediaCall(Ljava/lang/String;ILjava/lang/String;)I

    move-result v0

    return v0
.end method

.method public makeMediaCall(Ljava/lang/String;ILjava/lang/String;)I
    .locals 6
    .param p1, "peerURI"    # Ljava/lang/String;
    .param p2, "type"    # I
    .param p3, "CLI"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 82
    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->makeMediaCall(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public makeMediaCall(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)I
    .locals 7
    .param p1, "peerURI"    # Ljava/lang/String;
    .param p2, "type"    # I
    .param p3, "CLI"    # Ljava/lang/String;
    .param p4, "letteringText"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 99
    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v6, v5

    invoke-virtual/range {v0 .. v6}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->makeMediaCall(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public makeMediaCall(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    .locals 7
    .param p1, "peerURI"    # Ljava/lang/String;
    .param p2, "type"    # I
    .param p3, "CLI"    # Ljava/lang/String;
    .param p4, "letteringText"    # Ljava/lang/String;
    .param p5, "typeOfEmergencyService"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 114
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v6, p5

    invoke-virtual/range {v0 .. v6}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->makeMediaCall(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public makeMediaCall(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    .locals 10
    .param p1, "peerURI"    # Ljava/lang/String;
    .param p2, "type"    # I
    .param p3, "CLI"    # Ljava/lang/String;
    .param p4, "letteringText"    # Ljava/lang/String;
    .param p5, "alertInfo"    # Ljava/lang/String;
    .param p6, "typeOfEmergencyService"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 130
    sget-object v0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->LOG_TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "makeMediaCall() with type : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    const/4 v9, -0x1

    .line 135
    .local v9, "result":I
    :try_start_0
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v0}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v0

    const/16 v4, 0xe10

    move-object v1, p1

    move-object v2, p3

    move v3, p2

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p6

    invoke-interface/range {v0 .. v7}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->makeCall(Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v9

    .line 140
    return v9

    .line 136
    :catch_0
    move-exception v8

    .line 137
    .local v8, "e":Landroid/os/RemoteException;
    new-instance v0, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v8}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public manualDeregister()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 572
    sget-object v1, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->LOG_TAG:Ljava/lang/String;

    const-string v2, "manualDeregister()"

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 575
    :try_start_0
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v1}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v1

    invoke-interface {v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->manualDeregisterForCall()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 579
    return-void

    .line 576
    :catch_0
    move-exception v0

    .line 577
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public registerForCallStateChange(Lcom/samsung/commonimsinterface/imscommon/IIMSListener;)V
    .locals 3
    .param p1, "listener"    # Lcom/samsung/commonimsinterface/imscommon/IIMSListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 57
    sget-object v0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->LOG_TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "registerForCallStateChange() with listener : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->mIMSRemoteListenerList:Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;

    invoke-virtual {v0, p1}, Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;->addListener(Lcom/samsung/commonimsinterface/imscommon/IIMSListener;)Z

    .line 60
    return-void
.end method

.method public rejectCall(I)V
    .locals 3
    .param p1, "sessionHashCode"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 270
    sget-object v0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->LOG_TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "rejectCall() with sessionHashCode : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 272
    invoke-virtual {p0, p1}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->endCall(I)V

    .line 273
    return-void
.end method

.method public rejectCall(II)V
    .locals 4
    .param p1, "sessionHashCode"    # I
    .param p2, "reason"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 283
    sget-object v1, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->LOG_TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "rejectCall() with sessionHashCode : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " with reason : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 286
    :try_start_0
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v1}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v1

    invoke-interface {v1, p1, p2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->rejectCall(II)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 290
    return-void

    .line 287
    :catch_0
    move-exception v0

    .line 288
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public rejectChangeRequest(I)V
    .locals 4
    .param p1, "sessionHashCode"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 420
    sget-object v1, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->LOG_TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "rejectChangeRequest() with sessionHashCode : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 423
    :try_start_0
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v1}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v1

    invoke-interface {v1, p1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->rejectChangeRequest(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 427
    return-void

    .line 424
    :catch_0
    move-exception v0

    .line 425
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public removeParticipantFromNWayConferenceCall(II)V
    .locals 4
    .param p1, "sessionHashCode"    # I
    .param p2, "removedSessionHashCode"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 911
    sget-object v1, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->LOG_TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "removeParticipantFromNWayConferenceCall() with sessionHashCode : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " with removeSessionHashCode : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 914
    :try_start_0
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v1}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v1

    invoke-interface {v1, p1, p2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->removeParticipantFromNWayConferenceCall(II)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 918
    return-void

    .line 915
    :catch_0
    move-exception v0

    .line 916
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public resumeCall(I)V
    .locals 4
    .param p1, "sessionHashCode"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 317
    sget-object v1, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->LOG_TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "resumeCall() with sessionHashCode : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 320
    :try_start_0
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v1}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v1

    invoke-interface {v1, p1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->resumeCall(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 324
    return-void

    .line 321
    :catch_0
    move-exception v0

    .line 322
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public sendDtmf(II)Z
    .locals 4
    .param p1, "sessionHashCode"    # I
    .param p2, "code"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 437
    sget-object v1, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->LOG_TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "sendDtmf() with sessionHashCode : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " with code : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 440
    :try_start_0
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v1}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v1

    invoke-interface {v1, p1, p2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->sendDtmf(II)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 445
    const/4 v1, 0x1

    return v1

    .line 441
    :catch_0
    move-exception v0

    .line 442
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public sendTtyData(I[B)V
    .locals 4
    .param p1, "sessionHashCode"    # I
    .param p2, "data"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 512
    sget-object v2, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->LOG_TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "sendTtyData() with sessionHashCode : "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " with data length : "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    if-nez p2, :cond_0

    const-string v1, "null"

    :goto_0
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 515
    :try_start_0
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v1}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v1

    invoke-interface {v1, p1, p2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->sendTtyData(I[B)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 519
    return-void

    :cond_0
    move-object v1, p2

    .line 512
    goto :goto_0

    .line 516
    :catch_0
    move-exception v0

    .line 517
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public sendUSSD(III[B)I
    .locals 8
    .param p1, "ussdType"    # I
    .param p2, "dcs"    # I
    .param p3, "length"    # I
    .param p4, "ussdURI"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 172
    sget-object v0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->LOG_TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "sendUSSD() with ussdType : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " with dcs : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " with length : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " with ussdURI : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p4}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 177
    const/4 v7, -0x1

    .line 180
    .local v7, "result":I
    :try_start_0
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v0}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v0

    const/16 v5, 0xe10

    move v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p4

    invoke-interface/range {v0 .. v5}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->sendUSSD(III[BI)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v7

    .line 185
    return v7

    .line 181
    :catch_0
    move-exception v6

    .line 182
    .local v6, "e":Landroid/os/RemoteException;
    new-instance v0, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v6}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setAutoResponse(II)V
    .locals 4
    .param p1, "flag"    # I
    .param p2, "code"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 713
    sget-object v1, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->LOG_TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setAutoResponse() with flag : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " with code : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 716
    :try_start_0
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v1}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v1

    invoke-interface {v1, p1, p2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->setAutoResponse(II)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 720
    return-void

    .line 717
    :catch_0
    move-exception v0

    .line 718
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public setEmergencyPdnInfo([Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p1, "intfName"    # [Ljava/lang/String;
    .param p2, "pcscf"    # [Ljava/lang/String;
    .param p3, "gwAddress"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 806
    sget-object v4, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->LOG_TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "setEmergencyPdnInfo() with inftName.length : "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    if-nez p1, :cond_0

    const-string v3, "null"

    :goto_0
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " with pcscf.length : "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    if-nez p2, :cond_1

    const-string v3, "null"

    :goto_1
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 808
    const/4 v1, 0x0

    .line 809
    .local v1, "errorMessage":Ljava/lang/String;
    const/4 v2, 0x0

    .line 811
    .local v2, "index":I
    if-nez p1, :cond_2

    if-nez p2, :cond_2

    .line 812
    sget-object v3, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->LOG_TAG:Ljava/lang/String;

    const-string v4, "setEmergencyPdnInfo() : EPDN Down!!"

    invoke-static {v3, v4}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 815
    :try_start_0
    iget-object v3, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v3}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-interface {v3, v4, v5, v6}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->setEmergencyPdnInfo([Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 851
    :goto_2
    return-void

    .line 806
    .end local v1    # "errorMessage":Ljava/lang/String;
    .end local v2    # "index":I
    :cond_0
    array-length v3, p1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    goto :goto_0

    :cond_1
    array-length v3, p2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    goto :goto_1

    .line 816
    .restart local v1    # "errorMessage":Ljava/lang/String;
    .restart local v2    # "index":I
    :catch_0
    move-exception v0

    .line 817
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v3, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 823
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_2
    if-nez p1, :cond_3

    .line 824
    const-string v1, "intfName is null"

    .line 825
    sget-object v3, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->LOG_TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "setEmergencyPdnInfo() "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 829
    :cond_3
    if-nez p2, :cond_4

    .line 830
    const-string v1, "pcscf is null"

    .line 831
    sget-object v3, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->LOG_TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "setEmergencyPdnInfo() "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 836
    :cond_4
    const/4 v2, 0x0

    :goto_3
    array-length v3, p1

    if-ge v2, v3, :cond_5

    .line 837
    sget-object v3, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->LOG_TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "intfName ["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "] : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget-object v5, p1, v2

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 836
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 840
    :cond_5
    const/4 v2, 0x0

    :goto_4
    array-length v3, p2

    if-ge v2, v3, :cond_6

    .line 841
    sget-object v3, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->LOG_TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "pcscf ["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "] : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget-object v5, p2, v2

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 840
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 844
    :cond_6
    sget-object v3, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->LOG_TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "gw : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 847
    :try_start_1
    iget-object v3, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v3}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v3

    invoke-interface {v3, p1, p2, p3}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->setEmergencyPdnInfo([Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_2

    .line 848
    :catch_1
    move-exception v0

    .line 849
    .restart local v0    # "e":Landroid/os/RemoteException;
    new-instance v3, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v3
.end method

.method public setSpeakerMode(IZ)V
    .locals 4
    .param p1, "sessionHashCode"    # I
    .param p2, "speakerMode"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 623
    sget-object v1, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->LOG_TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setSpeakerMode() with sessionHashCode : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " with speakerMode : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 626
    :try_start_0
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v1}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v1

    invoke-interface {v1, p1, p2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->setSpeakerMode(IZ)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 630
    return-void

    .line 627
    :catch_0
    move-exception v0

    .line 628
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public setTtyMode(I)V
    .locals 4
    .param p1, "ttyMode"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 527
    sget-object v1, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->LOG_TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setTtyMode() with ttyMode : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 530
    :try_start_0
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v1}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v1

    invoke-interface {v1, p1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->setTtyMode(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 534
    return-void

    .line 531
    :catch_0
    move-exception v0

    .line 532
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public setVolume(II)V
    .locals 4
    .param p1, "sessionHashCode"    # I
    .param p2, "index"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 639
    sget-object v1, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->LOG_TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setVolume() with sessionHashCode : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " with index : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 642
    :try_start_0
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v1}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v1

    invoke-interface {v1, p1, p2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->setVolume(II)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 646
    return-void

    .line 643
    :catch_0
    move-exception v0

    .line 644
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public startDtmf(II)Z
    .locals 4
    .param p1, "sessionHashCode"    # I
    .param p2, "code"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 456
    sget-object v1, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->LOG_TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "startDtmf() with sessionHashCode : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " with code : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 459
    :try_start_0
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v1}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v1

    invoke-interface {v1, p1, p2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->startDtmf(II)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 464
    const/4 v1, 0x1

    return v1

    .line 460
    :catch_0
    move-exception v0

    .line 461
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public startNWayConferenceCall(III)I
    .locals 5
    .param p1, "heldSessionHashCode"    # I
    .param p2, "activeSessionHashCode"    # I
    .param p3, "callType"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 883
    sget-object v2, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->LOG_TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "startNWayConferenceCall() with heldsessionHashCode : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " with activesessionHashCode : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " with callType : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 887
    const/4 v1, -0x1

    .line 890
    .local v1, "result":I
    :try_start_0
    iget-object v2, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v2}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v2

    invoke-interface {v2, p1, p2, p3}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->startNWayConferenceCall(III)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 895
    return v1

    .line 891
    :catch_0
    move-exception v0

    .line 892
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v2, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public stopDtmf(I)Z
    .locals 4
    .param p1, "sessionHashCode"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 474
    sget-object v1, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->LOG_TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "stopDtmf() with sessionHashCode : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 477
    :try_start_0
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v1}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v1

    invoke-interface {v1, p1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->stopDtmf(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 482
    const/4 v1, 0x1

    return v1

    .line 478
    :catch_0
    move-exception v0

    .line 479
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public toggleMute(I)V
    .locals 4
    .param p1, "sessionHashCode"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 607
    sget-object v1, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->LOG_TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "toggleMute() with sessionHashCode : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 610
    :try_start_0
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v1}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v1

    invoke-interface {v1, p1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->toggleMute(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 614
    return-void

    .line 611
    :catch_0
    move-exception v0

    .line 612
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public updateSSACInfo(ZIIII)V
    .locals 7
    .param p1, "status"    # Z
    .param p2, "voiceFactor"    # I
    .param p3, "voiceTime"    # I
    .param p4, "videoFactor"    # I
    .param p5, "videoTime"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 982
    sget-object v0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->LOG_TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "updateSSACInfo :  status ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " voiceFactor "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " voiceTime "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " videoFactor "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " videoTime "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 989
    :try_start_0
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v0}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v0

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-interface/range {v0 .. v5}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->updateSSACInfo(ZIIII)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 993
    return-void

    .line 990
    :catch_0
    move-exception v6

    .line 991
    .local v6, "e":Landroid/os/RemoteException;
    new-instance v0, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v6}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public upgradeCall(II)V
    .locals 4
    .param p1, "sessionHashCode"    # I
    .param p2, "type"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 335
    sget-object v1, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->LOG_TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "upgradeCall() with sessionHashCode : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " with type "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 338
    :try_start_0
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v1}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v1

    invoke-interface {v1, p1, p2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->upgradeCall(II)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 342
    return-void

    .line 339
    :catch_0
    move-exception v0

    .line 340
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public voiceRecord(IILjava/lang/String;)V
    .locals 4
    .param p1, "command"    # I
    .param p2, "sessionHashCode"    # I
    .param p3, "fileName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 495
    sget-object v1, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->LOG_TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "voiceRecord() with command : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " with sessionHashCode : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " with fileName : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 498
    :try_start_0
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v1}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v1

    invoke-interface {v1, p1, p2, p3}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->voiceRecord(IILjava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 502
    return-void

    .line 499
    :catch_0
    move-exception v0

    .line 500
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v1
.end method
