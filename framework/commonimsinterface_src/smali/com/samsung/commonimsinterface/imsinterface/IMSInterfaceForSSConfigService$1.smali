.class Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForSSConfigService$1;
.super Ljava/lang/Object;
.source "IMSInterfaceForSSConfigService.java"

# interfaces
.implements Lcom/samsung/commonimsinterface/imscommon/IIMSServiceConnectionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForSSConfigService;-><init>(Landroid/content/Context;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForSSConfigService;


# direct methods
.method constructor <init>(Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForSSConfigService;)V
    .locals 0

    .prologue
    .line 33
    iput-object p1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForSSConfigService$1;->this$0:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForSSConfigService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onConnected()V
    .locals 3

    .prologue
    .line 36
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForSSConfigService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForSSConfigService;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string v2, "onConnected"

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 39
    :try_start_0
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForSSConfigService$1;->this$0:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForSSConfigService;

    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForSSConfigService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;
    invoke-static {v1}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForSSConfigService;->access$200(Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForSSConfigService;)Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForSSConfigService$1;->this$0:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForSSConfigService;

    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForSSConfigService;->mIMSRemoteListenerList:Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;
    invoke-static {v2}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForSSConfigService;->access$100(Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForSSConfigService;)Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->registerForSSConfigStateChange(Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 43
    :goto_0
    return-void

    .line 40
    :catch_0
    move-exception v0

    .line 41
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public onDisconnected()V
    .locals 2

    .prologue
    .line 47
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForSSConfigService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForSSConfigService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "onDisconnected"

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 48
    return-void
.end method
