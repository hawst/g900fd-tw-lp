.class Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService$1;
.super Ljava/lang/Object;
.source "IMSInterfaceForMediaService.java"

# interfaces
.implements Lcom/samsung/commonimsinterface/imscommon/IIMSServiceConnectionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;-><init>(Landroid/content/Context;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;


# direct methods
.method constructor <init>(Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;)V
    .locals 0

    .prologue
    .line 43
    iput-object p1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService$1;->this$0:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onConnected()V
    .locals 3

    .prologue
    .line 46
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string v2, "onConnected"

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 49
    :try_start_0
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService$1;->this$0:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;

    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;
    invoke-static {v1}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->access$200(Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;)Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService$1;->this$0:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;

    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mIMSRemoteListenerList:Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;
    invoke-static {v2}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->access$100(Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;)Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->registerForMediaStateChange(Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 53
    :goto_0
    return-void

    .line 50
    :catch_0
    move-exception v0

    .line 51
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public onDisconnected()V
    .locals 2

    .prologue
    .line 57
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "onDisconnected"

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    return-void
.end method
