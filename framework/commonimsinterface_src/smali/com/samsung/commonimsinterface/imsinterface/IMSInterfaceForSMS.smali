.class public interface abstract Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForSMS;
.super Ljava/lang/Object;
.source "IMSInterfaceForSMS.java"


# virtual methods
.method public abstract deRegisterForSMSStateChange(Lcom/samsung/commonimsinterface/imscommon/IIMSListener;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation
.end method

.method public abstract registerForSMSStateChange(Lcom/samsung/commonimsinterface/imscommon/IIMSListener;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation
.end method

.method public abstract sendDeliverReport([B)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation
.end method

.method public abstract sendRPSMMA(Ljava/lang/String;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation
.end method

.method public abstract sendSMSOverIMS(Ljava/lang/String;Ljava/lang/String;[BI)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation
.end method

.method public abstract sendSMSResponse(ZI)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation
.end method
