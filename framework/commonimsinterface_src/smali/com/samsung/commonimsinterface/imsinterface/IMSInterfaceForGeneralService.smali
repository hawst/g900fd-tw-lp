.class public Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;
.super Ljava/lang/Object;
.source "IMSInterfaceForGeneralService.java"

# interfaces
.implements Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneral;


# static fields
.field private static final LOG_TAG:Ljava/lang/String;


# instance fields
.field private mIMSRemoteListenerListForAPCS:Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;

.field private mIMSRemoteListenerListForConnectivity:Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;

.field private mIMSRemoteListenerListForRegistration:Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;

.field private mIMSRemoteListenerListForService:Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;

.field private mIMSRemoteListenerListForSetting:Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;

.field private mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const-class v0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Z)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "waitForReady"    # Z

    .prologue
    const/4 v0, 0x0

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object v0, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    .line 27
    iput-object v0, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->mIMSRemoteListenerListForService:Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;

    .line 28
    iput-object v0, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->mIMSRemoteListenerListForConnectivity:Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;

    .line 29
    iput-object v0, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->mIMSRemoteListenerListForRegistration:Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;

    .line 30
    iput-object v0, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->mIMSRemoteListenerListForSetting:Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;

    .line 31
    iput-object v0, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->mIMSRemoteListenerListForAPCS:Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;

    .line 40
    invoke-static {p1, p2}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getInstance(Landroid/content/Context;Z)Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    .line 41
    new-instance v0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;

    invoke-direct {v0, p1}, Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->mIMSRemoteListenerListForService:Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;

    .line 42
    new-instance v0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;

    invoke-direct {v0, p1}, Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->mIMSRemoteListenerListForConnectivity:Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;

    .line 43
    new-instance v0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;

    invoke-direct {v0, p1}, Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->mIMSRemoteListenerListForRegistration:Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;

    .line 44
    new-instance v0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;

    invoke-direct {v0, p1}, Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->mIMSRemoteListenerListForSetting:Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;

    .line 45
    new-instance v0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;

    invoke-direct {v0, p1}, Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->mIMSRemoteListenerListForAPCS:Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;

    .line 47
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    new-instance v1, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService$1;

    invoke-direct {v1, p0}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService$1;-><init>(Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;)V

    invoke-virtual {v0, v1}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->addServiceConnectionListener(Lcom/samsung/commonimsinterface/imscommon/IIMSServiceConnectionListener;)V

    .line 75
    return-void
.end method

.method private _getIMSSettingValues([ILjava/util/ArrayList;I)Ljava/lang/Object;
    .locals 7
    .param p1, "fields"    # [I
    .param p3, "transactionId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([I",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;I)",
            "Ljava/lang/Object;"
        }
    .end annotation

    .prologue
    .line 712
    .local p2, "xList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    sget-object v4, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->LOG_TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "_getIMSSettingValues() xList ["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "], transactionId["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 714
    :try_start_0
    iget-object v4, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v4}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v4

    invoke-interface {v4, p1, p2, p3}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->getIMSSettingValues([ILjava/util/List;I)Lcom/samsung/commonimsinterface/imscommon/IMSParameter;

    move-result-object v3

    .line 716
    .local v3, "parameter":Lcom/samsung/commonimsinterface/imscommon/IMSParameter;
    if-eqz v3, :cond_1

    .line 717
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lez v4, :cond_0

    .line 718
    const/4 v1, 0x0

    .line 719
    .local v1, "bundle":Landroid/os/Bundle;
    const-string v4, "settingsvalue"

    invoke-virtual {v3, v4}, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    .line 731
    .end local v1    # "bundle":Landroid/os/Bundle;
    .end local v3    # "parameter":Lcom/samsung/commonimsinterface/imscommon/IMSParameter;
    :goto_0
    return-object v1

    .line 722
    .restart local v3    # "parameter":Lcom/samsung/commonimsinterface/imscommon/IMSParameter;
    :cond_0
    const/4 v0, 0x0

    .line 723
    .local v0, "array":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/lang/String;>;"
    const-string v4, "settingsvalue"

    invoke-virtual {v3, v4}, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;->getSparseStringArray(Ljava/lang/String;)Landroid/util/SparseArray;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    move-object v1, v0

    .line 724
    goto :goto_0

    .line 726
    .end local v0    # "array":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/lang/String;>;"
    .end local v3    # "parameter":Lcom/samsung/commonimsinterface/imscommon/IMSParameter;
    :catch_0
    move-exception v2

    .line 727
    .local v2, "e":Landroid/os/RemoteException;
    invoke-virtual {v2}, Landroid/os/RemoteException;->printStackTrace()V

    .line 731
    .end local v2    # "e":Landroid/os/RemoteException;
    :cond_1
    :goto_1
    const/4 v1, 0x0

    goto :goto_0

    .line 728
    :catch_1
    move-exception v2

    .line 729
    .local v2, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v2}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_1
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->LOG_TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;)Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->mIMSRemoteListenerListForConnectivity:Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;)Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;)Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->mIMSRemoteListenerListForRegistration:Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;

    return-object v0
.end method

.method static synthetic access$400(Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;)Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->mIMSRemoteListenerListForSetting:Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;

    return-object v0
.end method

.method static synthetic access$500(Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;)Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->mIMSRemoteListenerListForAPCS:Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;

    return-object v0
.end method

.method static synthetic access$600(Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;
    .param p1, "x1"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 20
    invoke-direct {p0, p1}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->notifyServiceState(Z)V

    return-void
.end method

.method private notifyServiceState(Z)V
    .locals 5
    .param p1, "ready"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x7

    .line 78
    sget-object v0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->LOG_TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "notifyServiceState ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    if-eqz p1, :cond_0

    .line 81
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->mIMSRemoteListenerListForService:Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;

    const/16 v1, 0x3fd

    invoke-virtual {v0, v3, v1, v4}, Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;->onReceive(IILcom/samsung/commonimsinterface/imscommon/IMSParameter;)V

    .line 89
    :goto_0
    return-void

    .line 85
    :cond_0
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->mIMSRemoteListenerListForService:Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;

    const/16 v1, 0x3fe

    invoke-virtual {v0, v3, v1, v4}, Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;->onReceive(IILcom/samsung/commonimsinterface/imscommon/IMSParameter;)V

    goto :goto_0
.end method


# virtual methods
.method public abortIMSSettingTransaction(I)V
    .locals 3
    .param p1, "transactionId"    # I

    .prologue
    .line 817
    sget-object v1, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->LOG_TAG:Ljava/lang/String;

    const-string v2, "abortIMSSettingTransaction()"

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 819
    :try_start_0
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v1}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v1

    invoke-interface {v1, p1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->abortIMSSettingTransaction(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    .line 825
    :goto_0
    return-void

    .line 820
    :catch_0
    move-exception v0

    .line 821
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 822
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 823
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0
.end method

.method public deRegisterForAPCSStateChange(Lcom/samsung/commonimsinterface/imscommon/IIMSListener;)V
    .locals 3
    .param p1, "listener"    # Lcom/samsung/commonimsinterface/imscommon/IIMSListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 167
    sget-object v0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->LOG_TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "deRegisterForApcsStateChange() with listener : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 169
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->mIMSRemoteListenerListForAPCS:Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;

    invoke-virtual {v0, p1}, Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;->removeListener(Lcom/samsung/commonimsinterface/imscommon/IIMSListener;)Z

    .line 170
    return-void
.end method

.method public deRegisterForConnectivityStateChange(Lcom/samsung/commonimsinterface/imscommon/IIMSListener;)V
    .locals 3
    .param p1, "listener"    # Lcom/samsung/commonimsinterface/imscommon/IIMSListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 126
    sget-object v0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->LOG_TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "deRegisterForConnectivityStateChange() with listener : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 128
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->mIMSRemoteListenerListForConnectivity:Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;

    invoke-virtual {v0, p1}, Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;->removeListener(Lcom/samsung/commonimsinterface/imscommon/IIMSListener;)Z

    .line 129
    return-void
.end method

.method public deRegisterForRegisterStateChange(Lcom/samsung/commonimsinterface/imscommon/IIMSListener;)V
    .locals 3
    .param p1, "listener"    # Lcom/samsung/commonimsinterface/imscommon/IIMSListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 139
    sget-object v0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->LOG_TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "deRegisterForRegisterStateChange() with listener : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->mIMSRemoteListenerListForRegistration:Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;

    invoke-virtual {v0, p1}, Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;->removeListener(Lcom/samsung/commonimsinterface/imscommon/IIMSListener;)Z

    .line 142
    return-void
.end method

.method public deRegisterForServiceStateChange(Lcom/samsung/commonimsinterface/imscommon/IIMSListener;)V
    .locals 3
    .param p1, "listener"    # Lcom/samsung/commonimsinterface/imscommon/IIMSListener;

    .prologue
    .line 114
    sget-object v0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->LOG_TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "deRegisterForServiceStateChange() with listener : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 115
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->mIMSRemoteListenerListForService:Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;

    invoke-virtual {v0, p1}, Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;->removeListener(Lcom/samsung/commonimsinterface/imscommon/IIMSListener;)Z

    .line 116
    return-void
.end method

.method public deRegisterForSettingStateListener(Lcom/samsung/commonimsinterface/imscommon/IIMSListener;)V
    .locals 3
    .param p1, "listener"    # Lcom/samsung/commonimsinterface/imscommon/IIMSListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 153
    sget-object v0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->LOG_TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "deRegisterForSettingStateListener() with listener : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->mIMSRemoteListenerListForSetting:Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;

    invoke-virtual {v0, p1}, Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;->removeListener(Lcom/samsung/commonimsinterface/imscommon/IIMSListener;)Z

    .line 156
    return-void
.end method

.method public disableWiFiCalling()V
    .locals 3

    .prologue
    .line 519
    sget-object v1, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->LOG_TAG:Ljava/lang/String;

    const-string v2, "disableWiFiCalling()"

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 522
    :try_start_0
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v1}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v1

    invoke-interface {v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->disableWiFiCalling()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    .line 528
    :goto_0
    return-void

    .line 523
    :catch_0
    move-exception v0

    .line 524
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 525
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 526
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0
.end method

.method public enableWiFiCalling()V
    .locals 3

    .prologue
    .line 506
    sget-object v1, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->LOG_TAG:Ljava/lang/String;

    const-string v2, "enableWiFiCalling()"

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 509
    :try_start_0
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v1}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v1

    invoke-interface {v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->enableWiFiCalling()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    .line 515
    :goto_0
    return-void

    .line 510
    :catch_0
    move-exception v0

    .line 511
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 512
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 513
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0
.end method

.method public endIMSSettingTransaction(I)V
    .locals 3
    .param p1, "transactionId"    # I

    .prologue
    .line 805
    sget-object v1, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->LOG_TAG:Ljava/lang/String;

    const-string v2, "endIMSSettingTransaction()"

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 807
    :try_start_0
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v1}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v1

    invoke-interface {v1, p1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->endIMSSettingTransaction(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    .line 813
    :goto_0
    return-void

    .line 808
    :catch_0
    move-exception v0

    .line 809
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 810
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 811
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0
.end method

.method public forceRestart()Z
    .locals 4

    .prologue
    .line 489
    sget-object v2, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->LOG_TAG:Ljava/lang/String;

    const-string v3, "forceRestart()"

    invoke-static {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 491
    const/4 v1, 0x0

    .line 494
    .local v1, "result":Z
    :try_start_0
    iget-object v2, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v2}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v2

    invoke-interface {v2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->forceRestart()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v1

    .line 501
    :goto_0
    return v1

    .line 495
    :catch_0
    move-exception v0

    .line 496
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 497
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 498
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0
.end method

.method public getErrorCode(I)I
    .locals 4
    .param p1, "networkType"    # I

    .prologue
    .line 378
    sget-object v2, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->LOG_TAG:Ljava/lang/String;

    const-string v3, "getErrorCode()"

    invoke-static {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 380
    const/4 v1, -0x1

    .line 383
    .local v1, "result":I
    :try_start_0
    iget-object v2, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v2}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v2

    invoke-interface {v2, p1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->getErrorCode(I)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v1

    .line 390
    :goto_0
    return v1

    .line 384
    :catch_0
    move-exception v0

    .line 385
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 386
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 387
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0
.end method

.method public getFeatureMask()I
    .locals 4

    .prologue
    .line 361
    sget-object v2, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->LOG_TAG:Ljava/lang/String;

    const-string v3, "getFeatureMask()"

    invoke-static {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 363
    const/4 v1, -0x1

    .line 366
    .local v1, "result":I
    :try_start_0
    iget-object v2, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v2}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v2

    invoke-interface {v2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->getFeatureMask()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v1

    .line 373
    :goto_0
    return v1

    .line 367
    :catch_0
    move-exception v0

    .line 368
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 369
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 370
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0
.end method

.method public getIMSSettingValues(Ljava/util/ArrayList;)Landroid/os/Bundle;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Landroid/os/Bundle;"
        }
    .end annotation

    .prologue
    .line 687
    .local p1, "xListUri":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v0, -0x1

    invoke-virtual {p0, p1, v0}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->getIMSSettingValues(Ljava/util/ArrayList;I)Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method public getIMSSettingValues(Ljava/util/ArrayList;I)Landroid/os/Bundle;
    .locals 4
    .param p2, "transactionId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;I)",
            "Landroid/os/Bundle;"
        }
    .end annotation

    .prologue
    .local p1, "xListUri":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v0, 0x0

    .line 692
    const/4 v2, 0x0

    .line 693
    .local v2, "param":Lcom/samsung/commonimsinterface/imscommon/IMSParameter;
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-gtz v3, :cond_1

    .line 708
    :cond_0
    :goto_0
    return-object v0

    .line 697
    :cond_1
    :try_start_0
    iget-object v3, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v3}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v3

    invoke-interface {v3, p1, p2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->getIMSSettingValuesWithUri(Ljava/util/List;I)Lcom/samsung/commonimsinterface/imscommon/IMSParameter;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v2

    .line 703
    :goto_1
    if-eqz v2, :cond_0

    .line 706
    const/4 v0, 0x0

    .line 707
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v3, "settingsvalue"

    invoke-virtual {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 708
    goto :goto_0

    .line 698
    .end local v0    # "bundle":Landroid/os/Bundle;
    :catch_0
    move-exception v1

    .line 699
    .local v1, "e":Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1

    .line 700
    .end local v1    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v1

    .line 701
    .local v1, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v1}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_1
.end method

.method public getIMSSettingValues([ILjava/util/ArrayList;)Landroid/os/Bundle;
    .locals 1
    .param p1, "fields"    # [I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([I",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Landroid/os/Bundle;"
        }
    .end annotation

    .prologue
    .line 671
    .local p2, "xList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v0, -0x1

    invoke-direct {p0, p1, p2, v0}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->_getIMSSettingValues([ILjava/util/ArrayList;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    return-object v0
.end method

.method public getIMSSettingValues([ILjava/util/ArrayList;I)Landroid/os/Bundle;
    .locals 1
    .param p1, "fields"    # [I
    .param p3, "transactionId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([I",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;I)",
            "Landroid/os/Bundle;"
        }
    .end annotation

    .prologue
    .line 682
    .local p2, "xList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->_getIMSSettingValues([ILjava/util/ArrayList;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    return-object v0
.end method

.method public getIMSSettingValues([I)Landroid/util/SparseArray;
    .locals 2
    .param p1, "fields"    # [I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([I)",
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 666
    const/4 v0, 0x0

    const/4 v1, -0x1

    invoke-direct {p0, p1, v0, v1}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->_getIMSSettingValues([ILjava/util/ArrayList;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/SparseArray;

    return-object v0
.end method

.method public getIMSSettingValues([II)Landroid/util/SparseArray;
    .locals 1
    .param p1, "fields"    # [I
    .param p2, "transactionId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([II)",
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 677
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->_getIMSSettingValues([ILjava/util/ArrayList;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/SparseArray;

    return-object v0
.end method

.method public getServiceStatus()Z
    .locals 4

    .prologue
    .line 395
    sget-object v2, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->LOG_TAG:Ljava/lang/String;

    const-string v3, "getServiceStatus()"

    invoke-static {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 397
    const/4 v1, 0x0

    .line 400
    .local v1, "result":Z
    :try_start_0
    iget-object v2, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v2}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v2

    invoke-interface {v2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->getServiceStatus()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v1

    .line 407
    :goto_0
    return v1

    .line 401
    :catch_0
    move-exception v0

    .line 402
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 403
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 404
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0
.end method

.method public handleActionsOverEpdg()V
    .locals 3

    .prologue
    .line 558
    sget-object v1, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->LOG_TAG:Ljava/lang/String;

    const-string v2, "handleActionsOverEpdg()"

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 560
    :try_start_0
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v1}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v1

    invoke-interface {v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->handleActionsOverEpdg()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    .line 566
    :goto_0
    return-void

    .line 561
    :catch_0
    move-exception v0

    .line 562
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 563
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 564
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0
.end method

.method public isDeregisteredToMobile()Z
    .locals 4

    .prologue
    .line 225
    sget-object v2, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->LOG_TAG:Ljava/lang/String;

    const-string v3, "isDeregisteredToMobile()"

    invoke-static {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 227
    const/4 v1, 0x0

    .line 230
    .local v1, "result":Z
    :try_start_0
    iget-object v2, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v2}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v2

    invoke-interface {v2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->isDeregisteredToMobile()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v1

    .line 237
    :goto_0
    return v1

    .line 231
    :catch_0
    move-exception v0

    .line 232
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 233
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 234
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0
.end method

.method public isDeregisteredToWiFi()Z
    .locals 4

    .prologue
    .line 293
    sget-object v2, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->LOG_TAG:Ljava/lang/String;

    const-string v3, "isDeregisteredToWiFi()"

    invoke-static {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 295
    const/4 v1, 0x0

    .line 298
    .local v1, "result":Z
    :try_start_0
    iget-object v2, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v2}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v2

    invoke-interface {v2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->isDeregisteredToWiFi()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v1

    .line 305
    :goto_0
    return v1

    .line 299
    :catch_0
    move-exception v0

    .line 300
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 301
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 302
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0
.end method

.method public isDeregisteringToMobile()Z
    .locals 4

    .prologue
    .line 242
    sget-object v2, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->LOG_TAG:Ljava/lang/String;

    const-string v3, "isDeregisteringToMobile()"

    invoke-static {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 244
    const/4 v1, 0x0

    .line 247
    .local v1, "result":Z
    :try_start_0
    iget-object v2, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v2}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v2

    invoke-interface {v2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->isDeregisteringToMobile()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v1

    .line 254
    :goto_0
    return v1

    .line 248
    :catch_0
    move-exception v0

    .line 249
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 250
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 251
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0
.end method

.method public isDeregisteringToWiFi()Z
    .locals 4

    .prologue
    .line 310
    sget-object v2, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->LOG_TAG:Ljava/lang/String;

    const-string v3, "isDeregisteringToWiFi()"

    invoke-static {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 312
    const/4 v1, 0x0

    .line 315
    .local v1, "result":Z
    :try_start_0
    iget-object v2, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v2}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v2

    invoke-interface {v2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->isDeregisteringToWiFi()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v1

    .line 322
    :goto_0
    return v1

    .line 316
    :catch_0
    move-exception v0

    .line 317
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 318
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 319
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0
.end method

.method public isDisablingMobileData()Z
    .locals 4

    .prologue
    .line 344
    sget-object v2, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->LOG_TAG:Ljava/lang/String;

    const-string v3, "isDisablingMobileData()"

    invoke-static {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 346
    const/4 v1, 0x0

    .line 349
    .local v1, "result":Z
    :try_start_0
    iget-object v2, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v2}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v2

    invoke-interface {v2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->isDisablingMobileData()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v1

    .line 356
    :goto_0
    return v1

    .line 350
    :catch_0
    move-exception v0

    .line 351
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 352
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 353
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0
.end method

.method public isEABFeatureEnabled()Z
    .locals 8

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 913
    const/4 v2, 0x0

    .line 914
    .local v2, "retVal":Z
    new-array v1, v5, [I

    const/16 v7, 0x5f

    aput v7, v1, v6

    .line 915
    .local v1, "reqField":[I
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->getIMSSettingValues([I)Landroid/util/SparseArray;

    move-result-object v3

    .line 916
    .local v3, "sp":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/lang/String;>;"
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Landroid/util/SparseArray;->size()I

    move-result v7

    if-gtz v7, :cond_1

    .line 928
    :cond_0
    :goto_0
    return v6

    .line 919
    :cond_1
    invoke-virtual {v3, v6}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 920
    .local v4, "value":Ljava/lang/String;
    if-eqz v4, :cond_0

    .line 924
    :try_start_0
    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v7

    if-eqz v7, :cond_2

    move v2, v5

    :goto_1
    move v6, v2

    .line 928
    goto :goto_0

    :cond_2
    move v2, v6

    .line 924
    goto :goto_1

    .line 925
    :catch_0
    move-exception v0

    .line 926
    .local v0, "e":Ljava/lang/NumberFormatException;
    goto :goto_0
.end method

.method public isEABMenuShow()Z
    .locals 4

    .prologue
    .line 885
    const/4 v1, 0x0

    .line 886
    .local v1, "result":Z
    sget-object v2, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->LOG_TAG:Ljava/lang/String;

    const-string v3, "isEABMenuShow()"

    invoke-static {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 888
    :try_start_0
    iget-object v2, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v2}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v2

    invoke-interface {v2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->isEABMenuShow()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v1

    .line 894
    :goto_0
    return v1

    .line 889
    :catch_0
    move-exception v0

    .line 890
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 891
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 892
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0
.end method

.method public isEnablingMobileData()Z
    .locals 4

    .prologue
    .line 327
    sget-object v2, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->LOG_TAG:Ljava/lang/String;

    const-string v3, "isEnablingMobileData()"

    invoke-static {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 329
    const/4 v1, 0x0

    .line 332
    .local v1, "result":Z
    :try_start_0
    iget-object v2, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v2}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v2

    invoke-interface {v2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->isEnablingMobileData()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v1

    .line 339
    :goto_0
    return v1

    .line 333
    :catch_0
    move-exception v0

    .line 334
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 335
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 336
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0
.end method

.method public isIMSServiceReady()Z
    .locals 5

    .prologue
    .line 93
    const/4 v1, 0x0

    .line 96
    .local v1, "result":Z
    :try_start_0
    iget-object v2, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v2}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v2

    invoke-interface {v2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->isIMSServiceReady()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 101
    :goto_0
    sget-object v2, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->LOG_TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "isIMSServiceReady ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 103
    return v1

    .line 97
    :catch_0
    move-exception v0

    .line 98
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public isLTEVideoCallEnabled()Z
    .locals 4

    .prologue
    .line 638
    const/4 v1, 0x0

    .line 639
    .local v1, "result":Z
    sget-object v2, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->LOG_TAG:Ljava/lang/String;

    const-string v3, "isLTEVideoCallEnabled()"

    invoke-static {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 642
    :try_start_0
    iget-object v2, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v2}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v2

    invoke-interface {v2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->isLTEVideoCallEnabled()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v1

    .line 649
    :goto_0
    return v1

    .line 643
    :catch_0
    move-exception v0

    .line 644
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 645
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 646
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0
.end method

.method public isLTEVideoCallMenuShow()Z
    .locals 4

    .prologue
    .line 871
    const/4 v1, 0x0

    .line 872
    .local v1, "result":Z
    sget-object v2, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->LOG_TAG:Ljava/lang/String;

    const-string v3, "isLTEVideoCallMenuShow()"

    invoke-static {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 874
    :try_start_0
    iget-object v2, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v2}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v2

    invoke-interface {v2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->isLTEVideoCallMenuShow()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v1

    .line 880
    :goto_0
    return v1

    .line 875
    :catch_0
    move-exception v0

    .line 876
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 877
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 878
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0
.end method

.method public isLimitedMode(I)Z
    .locals 4
    .param p1, "networkType"    # I

    .prologue
    .line 829
    const/4 v1, 0x0

    .line 830
    .local v1, "result":Z
    sget-object v2, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->LOG_TAG:Ljava/lang/String;

    const-string v3, "isLimitedMode()"

    invoke-static {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 833
    :try_start_0
    iget-object v2, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v2}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v2

    invoke-interface {v2, p1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->isLimitedMode(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v1

    .line 840
    :goto_0
    return v1

    .line 834
    :catch_0
    move-exception v0

    .line 835
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 836
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 837
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0
.end method

.method public isRegistered()Z
    .locals 4

    .prologue
    .line 174
    sget-object v2, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->LOG_TAG:Ljava/lang/String;

    const-string v3, "isRegistered()"

    invoke-static {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 176
    const/4 v1, 0x0

    .line 179
    .local v1, "result":Z
    :try_start_0
    iget-object v2, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v2}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v2

    invoke-interface {v2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->isRegistered()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v1

    .line 186
    :goto_0
    return v1

    .line 180
    :catch_0
    move-exception v0

    .line 181
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 182
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 183
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0
.end method

.method public isRegisteredToMobile()Z
    .locals 4

    .prologue
    .line 191
    sget-object v2, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->LOG_TAG:Ljava/lang/String;

    const-string v3, "isRegisteredToMobile()"

    invoke-static {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 193
    const/4 v1, 0x0

    .line 196
    .local v1, "result":Z
    :try_start_0
    iget-object v2, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v2}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v2

    invoke-interface {v2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->isRegisteredToMobile()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v1

    .line 203
    :goto_0
    return v1

    .line 197
    :catch_0
    move-exception v0

    .line 198
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 199
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 200
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0
.end method

.method public isRegisteredToWiFi()Z
    .locals 4

    .prologue
    .line 259
    sget-object v2, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->LOG_TAG:Ljava/lang/String;

    const-string v3, "isRegisteredToWiFi()"

    invoke-static {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 261
    const/4 v1, 0x0

    .line 264
    .local v1, "result":Z
    :try_start_0
    iget-object v2, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v2}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v2

    invoke-interface {v2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->isRegisteredToWiFi()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v1

    .line 271
    :goto_0
    return v1

    .line 265
    :catch_0
    move-exception v0

    .line 266
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 267
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 268
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0
.end method

.method public isRegisteringToMobile()Z
    .locals 4

    .prologue
    .line 208
    sget-object v2, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->LOG_TAG:Ljava/lang/String;

    const-string v3, "isRegisteringToMobile()"

    invoke-static {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 210
    const/4 v1, 0x0

    .line 213
    .local v1, "result":Z
    :try_start_0
    iget-object v2, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v2}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v2

    invoke-interface {v2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->isRegisteringToMobile()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v1

    .line 220
    :goto_0
    return v1

    .line 214
    :catch_0
    move-exception v0

    .line 215
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 216
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 217
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0
.end method

.method public isRegisteringToWiFi()Z
    .locals 4

    .prologue
    .line 276
    sget-object v2, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->LOG_TAG:Ljava/lang/String;

    const-string v3, "isRegisteringToWiFi()"

    invoke-static {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 278
    const/4 v1, 0x0

    .line 281
    .local v1, "result":Z
    :try_start_0
    iget-object v2, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v2}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v2

    invoke-interface {v2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->isRegisteringToWiFi()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v1

    .line 288
    :goto_0
    return v1

    .line 282
    :catch_0
    move-exception v0

    .line 283
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 284
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 285
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0
.end method

.method public isVoLTEFeatureEnabled()Z
    .locals 4

    .prologue
    .line 596
    const/4 v1, 0x0

    .line 597
    .local v1, "result":Z
    sget-object v2, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->LOG_TAG:Ljava/lang/String;

    const-string v3, "isVoLTEFeatureEnabled()"

    invoke-static {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 600
    :try_start_0
    iget-object v2, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v2}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v2

    invoke-interface {v2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->isVoLTEFeatureEnabled()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v1

    .line 607
    :goto_0
    return v1

    .line 601
    :catch_0
    move-exception v0

    .line 602
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 603
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 604
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0
.end method

.method public isVoLTEMenuShow()Z
    .locals 4

    .prologue
    .line 857
    const/4 v1, 0x0

    .line 858
    .local v1, "result":Z
    sget-object v2, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->LOG_TAG:Ljava/lang/String;

    const-string v3, "isVoLTEMenuShow()"

    invoke-static {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 860
    :try_start_0
    iget-object v2, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v2}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v2

    invoke-interface {v2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->isVoLTEMenuShow()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v1

    .line 866
    :goto_0
    return v1

    .line 861
    :catch_0
    move-exception v0

    .line 862
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 863
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 864
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0
.end method

.method public manualDeregister()V
    .locals 3

    .prologue
    .line 425
    sget-object v1, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->LOG_TAG:Ljava/lang/String;

    const-string v2, "manualDeregister()"

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 428
    :try_start_0
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v1}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v1

    invoke-interface {v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->manualDeregister()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    .line 434
    :goto_0
    return-void

    .line 429
    :catch_0
    move-exception v0

    .line 430
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 431
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 432
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0
.end method

.method public manualRegister()V
    .locals 3

    .prologue
    .line 412
    sget-object v1, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->LOG_TAG:Ljava/lang/String;

    const-string v2, "manualRegister()"

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 415
    :try_start_0
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v1}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v1

    invoke-interface {v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->manualRegister()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    .line 421
    :goto_0
    return-void

    .line 416
    :catch_0
    move-exception v0

    .line 417
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 418
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 419
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0
.end method

.method public registerForAPCSStateChange(Lcom/samsung/commonimsinterface/imscommon/IIMSListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/samsung/commonimsinterface/imscommon/IIMSListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 160
    sget-object v0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->LOG_TAG:Ljava/lang/String;

    const-string v1, "registerForApcsStateChange()"

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 162
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->mIMSRemoteListenerListForAPCS:Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;

    invoke-virtual {v0, p1}, Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;->addListener(Lcom/samsung/commonimsinterface/imscommon/IIMSListener;)Z

    .line 163
    return-void
.end method

.method public registerForConnectivityStateChange(Lcom/samsung/commonimsinterface/imscommon/IIMSListener;)V
    .locals 3
    .param p1, "listener"    # Lcom/samsung/commonimsinterface/imscommon/IIMSListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 120
    sget-object v0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->LOG_TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "registerForConnectivityStateChange() with listener : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->mIMSRemoteListenerListForConnectivity:Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;

    invoke-virtual {v0, p1}, Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;->addListener(Lcom/samsung/commonimsinterface/imscommon/IIMSListener;)Z

    .line 122
    return-void
.end method

.method public registerForRegisterStateChange(Lcom/samsung/commonimsinterface/imscommon/IIMSListener;)V
    .locals 3
    .param p1, "listener"    # Lcom/samsung/commonimsinterface/imscommon/IIMSListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 133
    sget-object v0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->LOG_TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "registerForRegisterStateChange() with listener : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 134
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->mIMSRemoteListenerListForRegistration:Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;

    invoke-virtual {v0, p1}, Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;->addListener(Lcom/samsung/commonimsinterface/imscommon/IIMSListener;)Z

    .line 135
    return-void
.end method

.method public registerForServiceStateChange(Lcom/samsung/commonimsinterface/imscommon/IIMSListener;)V
    .locals 3
    .param p1, "listener"    # Lcom/samsung/commonimsinterface/imscommon/IIMSListener;

    .prologue
    .line 108
    sget-object v0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->LOG_TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "registerForServiceStateChange() with listener : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->mIMSRemoteListenerListForService:Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;

    invoke-virtual {v0, p1}, Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;->addListener(Lcom/samsung/commonimsinterface/imscommon/IIMSListener;)Z

    .line 110
    return-void
.end method

.method public registerForSettingStateListener(Lcom/samsung/commonimsinterface/imscommon/IIMSListener;[I)V
    .locals 3
    .param p1, "listener"    # Lcom/samsung/commonimsinterface/imscommon/IIMSListener;
    .param p2, "requiredFields"    # [I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 146
    sget-object v0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->LOG_TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "registerForSettingStateListener() with listener : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->mIMSRemoteListenerListForSetting:Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;->addListener(Lcom/samsung/commonimsinterface/imscommon/IIMSListener;[I)Z

    .line 149
    return-void
.end method

.method public sendDeregister()Z
    .locals 4

    .prologue
    .line 455
    sget-object v2, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->LOG_TAG:Ljava/lang/String;

    const-string v3, "sendDeregister()"

    invoke-static {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 457
    const/4 v1, 0x0

    .line 460
    .local v1, "result":Z
    :try_start_0
    iget-object v2, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v2}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v2

    const/4 v3, -0x1

    invoke-interface {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->sendDeregister(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v1

    .line 467
    :goto_0
    return v1

    .line 461
    :catch_0
    move-exception v0

    .line 462
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 463
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 464
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0
.end method

.method public sendDeregister(I)Z
    .locals 5
    .param p1, "cause"    # I

    .prologue
    .line 472
    sget-object v2, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->LOG_TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "sendDeregister() : cause is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 474
    const/4 v1, 0x0

    .line 477
    .local v1, "result":Z
    :try_start_0
    iget-object v2, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v2}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v2

    invoke-interface {v2, p1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->sendDeregister(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v1

    .line 484
    :goto_0
    return v1

    .line 478
    :catch_0
    move-exception v0

    .line 479
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 480
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 481
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0
.end method

.method public sendInitialRegister(I)V
    .locals 3
    .param p1, "networkType"    # I

    .prologue
    .line 545
    sget-object v1, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->LOG_TAG:Ljava/lang/String;

    const-string v2, "sendInitialRegister()"

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 548
    :try_start_0
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v1}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v1

    invoke-interface {v1, p1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->sendInitialRegister(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    .line 554
    :goto_0
    return-void

    .line 549
    :catch_0
    move-exception v0

    .line 550
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 551
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 552
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0
.end method

.method public sendRegister(I)Z
    .locals 5
    .param p1, "cause"    # I

    .prologue
    .line 438
    sget-object v2, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->LOG_TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "sendRegister() : cause is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 440
    const/4 v1, 0x0

    .line 443
    .local v1, "result":Z
    :try_start_0
    iget-object v2, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v2}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v2

    invoke-interface {v2, p1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->sendRegister(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v1

    .line 450
    :goto_0
    return v1

    .line 444
    :catch_0
    move-exception v0

    .line 445
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 446
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 447
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0
.end method

.method public setAllServicesDisabled(I)V
    .locals 0
    .param p1, "networkType"    # I

    .prologue
    .line 938
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->manualDeregister()V

    .line 939
    return-void
.end method

.method public setAllServicesEnabled(I)V
    .locals 0
    .param p1, "networkType"    # I

    .prologue
    .line 933
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->manualRegister()V

    .line 934
    return-void
.end method

.method public setEABFeatureDisable()V
    .locals 3

    .prologue
    .line 906
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    .line 907
    .local v0, "sp":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/lang/String;>;"
    const/16 v1, 0x5f

    const-string v2, "0"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 908
    invoke-virtual {p0, v0}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->updateIMSSettingValues(Landroid/util/SparseArray;)Z

    .line 909
    return-void
.end method

.method public setEABFeatureEnable()V
    .locals 3

    .prologue
    .line 899
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    .line 900
    .local v0, "sp":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/lang/String;>;"
    const/16 v1, 0x5f

    const-string v2, "1"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 901
    invoke-virtual {p0, v0}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->updateIMSSettingValues(Landroid/util/SparseArray;)Z

    .line 902
    return-void
.end method

.method public setLTEVideoCallDisable()V
    .locals 3

    .prologue
    .line 625
    sget-object v1, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->LOG_TAG:Ljava/lang/String;

    const-string v2, "setLTEVideoCallDisable()"

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 628
    :try_start_0
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v1}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v1

    invoke-interface {v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->setLTEVideoCallDisable()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    .line 634
    :goto_0
    return-void

    .line 629
    :catch_0
    move-exception v0

    .line 630
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 631
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 632
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0
.end method

.method public setLTEVideoCallEnable()V
    .locals 3

    .prologue
    .line 612
    sget-object v1, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->LOG_TAG:Ljava/lang/String;

    const-string v2, "setLTEVideoCallEnable()"

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 615
    :try_start_0
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v1}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v1

    invoke-interface {v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->setLTEVideoCallEnable()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    .line 621
    :goto_0
    return-void

    .line 616
    :catch_0
    move-exception v0

    .line 617
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 618
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 619
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0
.end method

.method public setRegistrationFeatureTags(Ljava/lang/String;[Ljava/lang/String;)V
    .locals 3
    .param p1, "serviceID"    # Ljava/lang/String;
    .param p2, "featureTags"    # [Ljava/lang/String;

    .prologue
    .line 532
    sget-object v1, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->LOG_TAG:Ljava/lang/String;

    const-string v2, "setRegistrationFeatureTags()"

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 535
    :try_start_0
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v1}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v1

    invoke-interface {v1, p1, p2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->setRegistrationFeatureTags(Ljava/lang/String;[Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    .line 541
    :goto_0
    return-void

    .line 536
    :catch_0
    move-exception v0

    .line 537
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 538
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 539
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0
.end method

.method public setThirdPartyMode(Z)V
    .locals 2
    .param p1, "flag"    # Z

    .prologue
    .line 655
    :try_start_0
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v1}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v1

    invoke-interface {v1, p1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->setThirdPartyMode(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    .line 661
    :goto_0
    return-void

    .line 656
    :catch_0
    move-exception v0

    .line 657
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 658
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 659
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0
.end method

.method public setVoLTEFeatureDisable()V
    .locals 3

    .prologue
    .line 583
    sget-object v1, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->LOG_TAG:Ljava/lang/String;

    const-string v2, "setVoLTEFeatureDisable()"

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 586
    :try_start_0
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v1}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v1

    invoke-interface {v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->setVoLTEFeatureDisable()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    .line 592
    :goto_0
    return-void

    .line 587
    :catch_0
    move-exception v0

    .line 588
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 589
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 590
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0
.end method

.method public setVoLTEFeatureEnable()V
    .locals 3

    .prologue
    .line 570
    sget-object v1, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->LOG_TAG:Ljava/lang/String;

    const-string v2, "setVoLTEFeatureEnable()"

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 573
    :try_start_0
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v1}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v1

    invoke-interface {v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->setVoLTEFeatureEnable()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    .line 579
    :goto_0
    return-void

    .line 574
    :catch_0
    move-exception v0

    .line 575
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 576
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 577
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0
.end method

.method public startIMSSettingTransaction()I
    .locals 3

    .prologue
    .line 792
    sget-object v1, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->LOG_TAG:Ljava/lang/String;

    const-string v2, "startIMSSettingTransaction()"

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 794
    :try_start_0
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v1}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v1

    invoke-interface {v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->startIMSSettingTransaction()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v1

    .line 800
    :goto_0
    return v1

    .line 795
    :catch_0
    move-exception v0

    .line 796
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    .line 800
    .end local v0    # "e":Landroid/os/RemoteException;
    :goto_1
    const/4 v1, -0x1

    goto :goto_0

    .line 797
    :catch_1
    move-exception v0

    .line 798
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_1
.end method

.method public startT3402TimerValue(I)V
    .locals 3
    .param p1, "nT3402TimerValue"    # I

    .prologue
    .line 845
    sget-object v1, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->LOG_TAG:Ljava/lang/String;

    const-string v2, "startT3402TimerValue()"

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 847
    :try_start_0
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v1}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v1

    invoke-interface {v1, p1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->startT3402TimerValue(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    .line 853
    :goto_0
    return-void

    .line 848
    :catch_0
    move-exception v0

    .line 849
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 850
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 851
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0
.end method

.method public updateIMSSettingValues(Landroid/util/SparseArray;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 735
    .local p1, "updateMap":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/lang/String;>;"
    const/4 v0, 0x0

    const/4 v1, -0x1

    invoke-virtual {p0, p1, v0, v1}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->updateIMSSettingValues(Landroid/util/SparseArray;Landroid/os/Bundle;I)Z

    move-result v0

    return v0
.end method

.method public updateIMSSettingValues(Landroid/util/SparseArray;I)Z
    .locals 1
    .param p2, "transactionId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/String;",
            ">;I)Z"
        }
    .end annotation

    .prologue
    .line 745
    .local p1, "updateMap":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/lang/String;>;"
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, p2}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->updateIMSSettingValues(Landroid/util/SparseArray;Landroid/os/Bundle;I)Z

    move-result v0

    return v0
.end method

.method public updateIMSSettingValues(Landroid/util/SparseArray;Landroid/os/Bundle;)Z
    .locals 1
    .param p2, "xList"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Landroid/os/Bundle;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 740
    .local p1, "updateMap":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/lang/String;>;"
    const/4 v0, -0x1

    invoke-virtual {p0, p1, p2, v0}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->updateIMSSettingValues(Landroid/util/SparseArray;Landroid/os/Bundle;I)Z

    move-result v0

    return v0
.end method

.method public updateIMSSettingValues(Landroid/util/SparseArray;Landroid/os/Bundle;I)Z
    .locals 8
    .param p2, "xList"    # Landroid/os/Bundle;
    .param p3, "transactionId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Landroid/os/Bundle;",
            "I)Z"
        }
    .end annotation

    .prologue
    .line 750
    .local p1, "updateMap":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/lang/String;>;"
    sget-object v5, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->LOG_TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "updateIMSSettingValues() xList ["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "], transactionId ["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 752
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 753
    .local v0, "bundle":Landroid/os/Bundle;
    const/4 v3, 0x0

    .line 755
    .local v3, "result":Z
    const/4 v2, 0x0

    .local v2, "index":I
    invoke-virtual {p1}, Landroid/util/SparseArray;->size()I

    move-result v4

    .local v4, "size":I
    :goto_0
    if-ge v2, v4, :cond_0

    .line 756
    invoke-virtual {p1, v2}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual {v0, v6, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 755
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 760
    :cond_0
    :try_start_0
    iget-object v5, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v5}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v5

    invoke-interface {v5, v0, p2, p3}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->updateIMSSettingValues(Landroid/os/Bundle;Landroid/os/Bundle;I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v3

    .line 767
    :goto_1
    return v3

    .line 761
    :catch_0
    move-exception v1

    .line 762
    .local v1, "e":Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1

    .line 763
    .end local v1    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v1

    .line 764
    .local v1, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v1}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_1
.end method

.method public updateIMSSettingValues(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p1, "xNodeUri"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 772
    const/4 v0, -0x1

    invoke-virtual {p0, p1, p2, v0}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->updateIMSSettingValues(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    return v0
.end method

.method public updateIMSSettingValues(Ljava/lang/String;Ljava/lang/String;I)Z
    .locals 4
    .param p1, "xNodeUri"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;
    .param p3, "transactionId"    # I

    .prologue
    .line 777
    const/4 v2, 0x0

    .line 779
    .local v2, "result":Z
    :try_start_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 780
    .local v0, "bundle":Landroid/os/Bundle;
    invoke-virtual {v0, p1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 781
    iget-object v3, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v3}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v3

    invoke-interface {v3, p1, p2, p3}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->updateIMSSettingValuesWithUri(Ljava/lang/String;Ljava/lang/String;I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v2

    .line 787
    .end local v0    # "bundle":Landroid/os/Bundle;
    :goto_0
    return v2

    .line 782
    :catch_0
    move-exception v1

    .line 783
    .local v1, "e":Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 784
    .end local v1    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v1

    .line 785
    .local v1, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v1}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0
.end method
