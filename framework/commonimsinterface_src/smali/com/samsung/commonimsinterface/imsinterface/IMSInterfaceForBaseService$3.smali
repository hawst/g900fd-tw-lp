.class Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService$3;
.super Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;
.source "IMSInterfaceForBaseService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;


# direct methods
.method constructor <init>(Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;)V
    .locals 0

    .prologue
    .line 251
    iput-object p1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService$3;->this$0:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-direct {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public abortIMSSettingTransaction(I)V
    .locals 2
    .param p1, "transactionId"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 315
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "abortIMSSettingTransaction:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 316
    return-void
.end method

.method public acceptChangeRequest(I)V
    .locals 2
    .param p1, "sessionHashCode"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1180
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "acceptChangeRequest:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1181
    return-void
.end method

.method public acceptChangeRequestWithCallType(II)V
    .locals 2
    .param p1, "sessionHashCode"    # I
    .param p2, "callType"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1184
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "acceptChangeRequestWithCallType:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1185
    return-void
.end method

.method public addParticipantToNWayConferenceCall(II)V
    .locals 2
    .param p1, "confSessionHashCode"    # I
    .param p2, "sessionHashCodeToBeAdded"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1175
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "addParticipantToNWayConferenceCall:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1176
    return-void
.end method

.method public addUserForConferenceCall(ILjava/lang/String;I)I
    .locals 2
    .param p1, "sessionHashCode"    # I
    .param p2, "dialString"    # Ljava/lang/String;
    .param p3, "type"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1169
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "addUserForConferenceCall:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1170
    const/4 v0, 0x0

    return v0
.end method

.method public answerCall(I)V
    .locals 2
    .param p1, "sessionHashCode"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1164
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "answerCall:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1165
    return-void
.end method

.method public answerCallAudioOnly(I)V
    .locals 2
    .param p1, "sessionHashCode"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1159
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "answerCallAudioOnly:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1160
    return-void
.end method

.method public answerCallWithCallType(II)V
    .locals 2
    .param p1, "sessionHashCode"    # I
    .param p2, "callType"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1154
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "answerCallWithCallType:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1155
    return-void
.end method

.method public captureSurfaceImage(ZI)V
    .locals 2
    .param p1, "isNearEnd"    # Z
    .param p2, "onGoingCallType"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1149
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "captureSurfaceImage:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1150
    return-void
.end method

.method public changeCall(II)V
    .locals 2
    .param p1, "sessionHashCode"    # I
    .param p2, "type"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1144
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "changeCall:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1145
    return-void
.end method

.method public changeEPDGAudioPath(I)V
    .locals 2
    .param p1, "direction"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1198
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "changeEPDGAudioPath is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1199
    return-void
.end method

.method public closeCamera()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1139
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "closeCamera:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1140
    return-void
.end method

.method public contactSvcCallFunction(IIILjava/lang/String;)V
    .locals 2
    .param p1, "funcId"    # I
    .param p2, "param1"    # I
    .param p3, "param2"    # I
    .param p4, "params"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1128
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "contactSvcCallFunction:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1129
    return-void
.end method

.method public deRegisterForAPCSStateChange(Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1123
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "deRegisterForAPCSStateChange:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1124
    return-void
.end method

.method public deRegisterForCallStateChange(Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1118
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "deRegisterForCallStateChange:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1119
    return-void
.end method

.method public deRegisterForConnectivityStateChange(Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1113
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "deRegisterForConnectivityStateChange:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1114
    return-void
.end method

.method public deRegisterForMediaStateChange(Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1108
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "deRegisterForMediaStateChange:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1109
    return-void
.end method

.method public deRegisterForPresenceStateChange(Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1103
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "deRegisterForPresenceStateChange:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1104
    return-void
.end method

.method public deRegisterForRegisterStateChange(Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1098
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "deRegisterForRegisterStateChange:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1099
    return-void
.end method

.method public deRegisterForSMSStateChange(Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1093
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "deRegisterForSMSStateChange:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1094
    return-void
.end method

.method public deRegisterForSSConfigStateChange(Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1088
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "deRegisterForSSConfigStateChange:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1089
    return-void
.end method

.method public deRegisterForServiceStateChange(Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;

    .prologue
    .line 265
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "deRegisterForServiceStateChange is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 266
    return-void
.end method

.method public deRegisterForSettingStateListener(Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1083
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "deRegisterForSettingStateListener:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1084
    return-void
.end method

.method public deinitSurface(Z)V
    .locals 2
    .param p1, "isNearEnd"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1078
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "deinitSurface:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1079
    return-void
.end method

.method public disableWiFiCalling()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1073
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "disableWiFiCalling:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1074
    return-void
.end method

.method public downgradeCall(II)V
    .locals 2
    .param p1, "sessionHashCode"    # I
    .param p2, "type"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1068
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "downgradeCall:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1069
    return-void
.end method

.method public emergencyDeregister()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1063
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "emergencyDeregister:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1064
    return-void
.end method

.method public emergencyRegister()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1058
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "emergencyRegister:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1059
    return-void
.end method

.method public enableWiFiCalling()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1053
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "enableWiFiCalling:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1054
    return-void
.end method

.method public endCall(I)V
    .locals 2
    .param p1, "sessionHashCode"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1048
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "endCall:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1049
    return-void
.end method

.method public endCallWithReason(II)V
    .locals 2
    .param p1, "sessionHashCode"    # I
    .param p2, "reason"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1043
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "endCallWithReason:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1044
    return-void
.end method

.method public endIMSSettingTransaction(I)V
    .locals 2
    .param p1, "transactionId"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 310
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "endIMSSettingTransaction:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 311
    return-void
.end method

.method public forceRestart()Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1037
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "forceRestart:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1038
    const/4 v0, 0x0

    return v0
.end method

.method public getAvailableBearer(I)I
    .locals 2
    .param p1, "token"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1209
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "getAvailableBearer is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1210
    const/4 v0, 0x0

    return v0
.end method

.method public getBackCameraId()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1031
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "getBackCameraId:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1032
    const/4 v0, 0x0

    return v0
.end method

.method public getCallBarring(II)I
    .locals 2
    .param p1, "ssClass"    # I
    .param p2, "cbType"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1025
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "getCallBarring:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1026
    const/4 v0, 0x0

    return v0
.end method

.method public getCallForwarding(II)I
    .locals 2
    .param p1, "ssClass"    # I
    .param p2, "cfType"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1019
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "getCallForwarding:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1020
    const/4 v0, 0x0

    return v0
.end method

.method public getCallType(I)I
    .locals 2
    .param p1, "sessionHashCode"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1013
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "getCallType:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1014
    const/4 v0, 0x0

    return v0
.end method

.method public getCallWaiting(I)I
    .locals 2
    .param p1, "ssClass"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1007
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "getCallWaiting:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1008
    const/4 v0, 0x0

    return v0
.end method

.method public getCameraDirection()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1001
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "getCameraDirection:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1002
    const/4 v0, 0x0

    return v0
.end method

.method public getCameraState()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 995
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "getCameraState:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 996
    const/4 v0, 0x0

    return v0
.end method

.method public getCurrentLatchedNetwork()Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 989
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "getCurrentLatchedNetwork:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 990
    const/4 v0, 0x0

    return-object v0
.end method

.method public getErrorCode(I)I
    .locals 2
    .param p1, "networkType"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 983
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "getErrorCode:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 984
    const/4 v0, 0x0

    return v0
.end method

.method public getFeatureMask()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 977
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "getFeatureMask:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 978
    const/4 v0, 0x0

    return v0
.end method

.method public getFrontCameraId()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 971
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "getFrontCameraId:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 972
    const/4 v0, 0x0

    return v0
.end method

.method public getIMSSettingFileEnumByUri(Ljava/util/List;)Lcom/samsung/commonimsinterface/imscommon/IMSParameter;
    .locals 2
    .param p1, "xList"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 287
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "getIMSSettingFileEnumByUri is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 288
    const/4 v0, 0x0

    return-object v0
.end method

.method public getIMSSettingValues([ILjava/util/List;I)Lcom/samsung/commonimsinterface/imscommon/IMSParameter;
    .locals 2
    .param p1, "fields"    # [I
    .param p2, "xList"    # Ljava/util/List;
    .param p3, "transactionId"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 965
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "getIMSSettingValues:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 966
    const/4 v0, 0x0

    return-object v0
.end method

.method public getIMSSettingValuesWithUri(Ljava/util/List;I)Lcom/samsung/commonimsinterface/imscommon/IMSParameter;
    .locals 1
    .param p1, "xListUri"    # Ljava/util/List;
    .param p2, "transactionId"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 294
    const/4 v0, 0x0

    return-object v0
.end method

.method public getMaxVolume(I)I
    .locals 2
    .param p1, "sessionHashCode"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 959
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "getMaxVolume:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 960
    const/4 v0, 0x0

    return v0
.end method

.method public getMaxZoom()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 953
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "getMaxZoom:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 954
    const/4 v0, 0x0

    return v0
.end method

.method public getNegotiatedFPS()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 947
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "getNegotiatedFPS:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 948
    const/4 v0, 0x0

    return v0
.end method

.method public getNegotiatedHeight()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 941
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "getNegotiatedHeight:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 942
    const/4 v0, 0x0

    return v0
.end method

.method public getNegotiatedWidth()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 935
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "getNegotiatedWidth:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 936
    const/4 v0, 0x0

    return v0
.end method

.method public getNumberOfCameras()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 929
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "getNumberOfCameras:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 930
    const/4 v0, 0x0

    return v0
.end method

.method public getParticipantListSizeForNWayConferenceCall(I)I
    .locals 2
    .param p1, "sessionHashCode"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 923
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "getParticipantListSizeForNWayConferenceCall:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 924
    const/4 v0, 0x0

    return v0
.end method

.method public getParticipantPeerURIFromNWayConferenceCall(II)Ljava/lang/String;
    .locals 2
    .param p1, "sessionHashCode"    # I
    .param p2, "position"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 917
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "getParticipantPeerURIFromNWayConferenceCall:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 918
    const/4 v0, 0x0

    return-object v0
.end method

.method public getParticipantSessionIdListForNWayConferenceCall(I)[Ljava/lang/String;
    .locals 2
    .param p1, "sessionHashCode"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 911
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "getParticipantSessionIdListForNWayConferenceCall:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 912
    const/4 v0, 0x0

    return-object v0
.end method

.method public getParticipantURIListForNWayConferenceCall(I)[Ljava/lang/String;
    .locals 2
    .param p1, "sessionHashCode"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 905
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "getParticipantURIListForNWayConferenceCall:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 906
    const/4 v0, 0x0

    return-object v0
.end method

.method public getRemoteCapabilitiesAndAvailability([Ljava/lang/String;Ljava/lang/String;)Z
    .locals 2
    .param p1, "uri"    # [Ljava/lang/String;
    .param p2, "serviceId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 893
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "getRemoteCapabilitiesAndAvailability:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 894
    const/4 v0, 0x0

    return v0
.end method

.method public getRemoteCapabilitiesAndAvailabilityPoll([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Z
    .locals 2
    .param p1, "uri"    # [Ljava/lang/String;
    .param p2, "serviceId"    # Ljava/lang/String;
    .param p3, "token"    # Ljava/lang/String;
    .param p4, "isListSubscribe"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 899
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "getRemoteCapabilitiesAndAvailability:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 900
    const/4 v0, 0x0

    return v0
.end method

.method public getServiceStatus()Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 887
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "getServiceStatus:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 888
    const/4 v0, 0x0

    return v0
.end method

.method public getSessionID(I)I
    .locals 2
    .param p1, "sessionHashCode"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 881
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "getSessionID:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 882
    const/4 v0, 0x0

    return v0
.end method

.method public getState(I)I
    .locals 2
    .param p1, "sessionHashCode"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 875
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "getState:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 876
    const/4 v0, 0x0

    return v0
.end method

.method public getTtyMode()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 412
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "getTtyMode:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 413
    const/4 v0, 0x0

    return v0
.end method

.method public getZoom()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 869
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "getZoom:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 870
    const/4 v0, 0x0

    return v0
.end method

.method public handleActionsOverEpdg()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 864
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "handleActionsOverEpdg:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 865
    return-void
.end method

.method public hashCode(I)I
    .locals 2
    .param p1, "sessionHashCode"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 858
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "hashCode:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 859
    const/4 v0, 0x0

    return v0
.end method

.method public holdCall(I)V
    .locals 2
    .param p1, "sessionHashCode"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 853
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "holdCall:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 854
    return-void
.end method

.method public holdVideo()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 848
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "holdVideo:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 849
    return-void
.end method

.method public indicateSrvccHoStarted(I)V
    .locals 2
    .param p1, "sessionHashCode"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 843
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "indicateSrvccHoStarted:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 844
    return-void
.end method

.method public isCallBarred(I)Z
    .locals 2
    .param p1, "callType"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1238
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "isCallBarred:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1239
    const/4 v0, 0x0

    return v0
.end method

.method public isDeregisteredToMobile()Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 837
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "isDeregisteredToMobile:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 838
    const/4 v0, 0x0

    return v0
.end method

.method public isDeregisteredToWiFi()Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 831
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "isDeregisteredToWiFi:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 832
    const/4 v0, 0x0

    return v0
.end method

.method public isDeregisteringToMobile()Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 825
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "isDeregisteringToMobile:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 826
    const/4 v0, 0x0

    return v0
.end method

.method public isDeregisteringToWiFi()Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 819
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "isDeregisteringToWiFi:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 820
    const/4 v0, 0x0

    return v0
.end method

.method public isDisablingMobileData()Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 813
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "isDisablingMobileData:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 814
    const/4 v0, 0x0

    return v0
.end method

.method public isEABMenuShow()Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1232
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "isVoLTEMenuShow is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1233
    const/4 v0, 0x0

    return v0
.end method

.method public isEnablingMobileData()Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 807
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "isEnablingMobileData:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 808
    const/4 v0, 0x0

    return v0
.end method

.method public isIMSEnabledOnWifi()Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 801
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "isIMSEnabledOnWifi:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 802
    const/4 v0, 0x0

    return v0
.end method

.method public isIMSServiceReady()Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 254
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "isIMSServiceReady is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 255
    const/4 v0, 0x0

    return v0
.end method

.method public isImsForbidden()Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 795
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "isImsForbidden:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 796
    const/4 v0, 0x0

    return v0
.end method

.method public isInCall(I)Z
    .locals 2
    .param p1, "sessionHashCode"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 789
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "isInCall:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 790
    const/4 v0, 0x0

    return v0
.end method

.method public isInEPDGCall()Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 783
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "isInEPDGCall:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 784
    const/4 v0, 0x0

    return v0
.end method

.method public isIncomingCallIntent(Landroid/content/Intent;)Z
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 777
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "isIncomingCallIntent:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 778
    const/4 v0, 0x0

    return v0
.end method

.method public isLTEVideoCallEnabled()Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 771
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "isLTEVideoCallEnabled:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 772
    const/4 v0, 0x0

    return v0
.end method

.method public isLTEVideoCallMenuShow()Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1226
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "isVoLTEMenuShow is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1227
    const/4 v0, 0x0

    return v0
.end method

.method public isLimitedMode(I)Z
    .locals 2
    .param p1, "networkType"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1203
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "isLimitedMode is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1204
    const/4 v0, 0x0

    return v0
.end method

.method public isMediaReadyToReceivePreview()Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 765
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "isMediaReadyToReceivePreview:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 766
    const/4 v0, 0x0

    return v0
.end method

.method public isMuted(I)Z
    .locals 2
    .param p1, "sessionHashCode"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 759
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "isMuted:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 760
    const/4 v0, 0x0

    return v0
.end method

.method public isOnEHRPD()Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 753
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "isOnEHRPD:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 754
    const/4 v0, 0x0

    return v0
.end method

.method public isOnHold(I)Z
    .locals 2
    .param p1, "sessionHashCode"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 747
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "isOnHold:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 748
    const/4 v0, 0x0

    return v0
.end method

.method public isOnLTE()Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 741
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "isOnLTE:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 742
    const/4 v0, 0x0

    return v0
.end method

.method public isProvisioned()Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1133
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "isProvisioned:IMSService is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1134
    const/4 v0, 0x0

    return v0
.end method

.method public isRegistered()Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 735
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "isRegistered:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 736
    const/4 v0, 0x0

    return v0
.end method

.method public isRegisteredToMobile()Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 729
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "isRegisteredToMobile:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 730
    const/4 v0, 0x0

    return v0
.end method

.method public isRegisteredToWiFi()Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 723
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "isRegisteredToWiFi:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 724
    const/4 v0, 0x0

    return v0
.end method

.method public isRegisteringToMobile()Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 717
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "isRegisteringToMobile:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 718
    const/4 v0, 0x0

    return v0
.end method

.method public isRegisteringToWiFi()Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 711
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "isRegisteringToWiFi:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 712
    const/4 v0, 0x0

    return v0
.end method

.method public isVoLTEFeatureEnabled()Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 705
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "isVoLTEFeatureEnabled:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 706
    const/4 v0, 0x0

    return v0
.end method

.method public isVoLTEMenuShow()Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1220
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "isVoLTEMenuShow is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1221
    const/4 v0, 0x0

    return v0
.end method

.method public makeCall(Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    .locals 2
    .param p1, "peerUri"    # Ljava/lang/String;
    .param p2, "CLI"    # Ljava/lang/String;
    .param p3, "type"    # I
    .param p4, "timeout"    # I
    .param p5, "letteringText"    # Ljava/lang/String;
    .param p6, "alertInfo"    # Ljava/lang/String;
    .param p7, "typeOfEmergencyService"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 699
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "makeCall:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 700
    const/4 v0, -0x1

    return v0
.end method

.method public manualDeregister()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 694
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "manualDeregister:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 695
    return-void
.end method

.method public manualDeregisterForCall()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 689
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "manualDeregisterForCall:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 690
    return-void
.end method

.method public manualRegister()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 684
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "manualRegister:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 685
    return-void
.end method

.method public mediaDeInit()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 679
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "mediaDeInit:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 680
    return-void
.end method

.method public mediaInit()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 674
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "mediaInit:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 675
    return-void
.end method

.method public mmSS_Svc_Api(IIILjava/lang/String;Ljava/lang/String;I)I
    .locals 2
    .param p1, "funcId"    # I
    .param p2, "param1"    # I
    .param p3, "param2"    # I
    .param p4, "param3"    # Ljava/lang/String;
    .param p5, "param4"    # Ljava/lang/String;
    .param p6, "param5"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 668
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "mmSS_Svc_Api:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 669
    const/4 v0, 0x0

    return v0
.end method

.method public openCamera(I)Z
    .locals 2
    .param p1, "cameraId"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 662
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "openCamera:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 663
    const/4 v0, 0x0

    return v0
.end method

.method public openCameraEx(II)Z
    .locals 2
    .param p1, "cameraId"    # I
    .param p2, "callType"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 656
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "openCameraEx:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 657
    const/4 v0, 0x0

    return v0
.end method

.method public publishCapabilitiesAndAvailability(Ljava/lang/String;Landroid/os/Bundle;)Z
    .locals 2
    .param p1, "serviceId"    # Ljava/lang/String;
    .param p2, "keyValuePairs"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 650
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "publishCapabilitiesAndAvailability:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 651
    const/4 v0, 0x0

    return v0
.end method

.method public registerForAPCSStateChange(Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 645
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "registerForAPCSStateChange:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 646
    return-void
.end method

.method public registerForCallStateChange(Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 640
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "registerForCallStateChange:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 641
    return-void
.end method

.method public registerForConnectivityStateChange(Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 635
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "registerForConnectivityStateChange:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 636
    return-void
.end method

.method public registerForMediaStateChange(Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 630
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "registerForMediaStateChange:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 631
    return-void
.end method

.method public registerForPresenceStateChange(Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 625
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "registerForPresenceStateChange:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 626
    return-void
.end method

.method public registerForRegisterStateChange(Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 620
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "registerForRegisterStateChange:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 621
    return-void
.end method

.method public registerForSMSStateChange(Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 615
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "registerForSMSStateChange:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 616
    return-void
.end method

.method public registerForSSConfigStateChange(Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 610
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "registerForSSConfigStateChange:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 611
    return-void
.end method

.method public registerForServiceStateChange(Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;

    .prologue
    .line 260
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "registerForServiceStateChange is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 261
    return-void
.end method

.method public registerForSettingStateListener(Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 605
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "registerForSettingStateListener:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 606
    return-void
.end method

.method public registerServiceCapabilityAvailabilityTemplate(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 2
    .param p1, "serviceId"    # Ljava/lang/String;
    .param p2, "pidfTemplate"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 599
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "registerServiceCapabilityAvailabilityTemplate:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 600
    const/4 v0, 0x0

    return v0
.end method

.method public rejectCall(II)V
    .locals 2
    .param p1, "sessionHashCode"    # I
    .param p2, "reason"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 594
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "rejectCall:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 595
    return-void
.end method

.method public rejectChangeRequest(I)V
    .locals 2
    .param p1, "sessionHashCode"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 589
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "rejectChangeRequest:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 590
    return-void
.end method

.method public removeParticipantFromNWayConferenceCall(II)V
    .locals 2
    .param p1, "sessionHashCode"    # I
    .param p2, "removedSessionHashCode"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 584
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "removeParticipantFromNWayConferenceCall:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 585
    return-void
.end method

.method public resetCameraID()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 579
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "resetCameraID:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 580
    return-void
.end method

.method public resumeCall(I)V
    .locals 2
    .param p1, "sessionHashCode"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 574
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "resumeCall:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 575
    return-void
.end method

.method public resumeVideo()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 569
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "resumeVideo:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 570
    return-void
.end method

.method public send180Ringing()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1193
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "send180Ringing is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1194
    return-void
.end method

.method public sendDeliverReport([B)V
    .locals 2
    .param p1, "data"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 559
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "sendDeliverReport:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 560
    return-void
.end method

.method public sendDeregister(I)Z
    .locals 2
    .param p1, "cause"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 553
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "sendDeregister:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 554
    const/4 v0, 0x0

    return v0
.end method

.method public sendDtmf(II)V
    .locals 2
    .param p1, "sessionHashCode"    # I
    .param p2, "code"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 542
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "sendDtmf:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 543
    return-void
.end method

.method public sendInitialRegister(I)V
    .locals 2
    .param p1, "networkType"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 537
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "sendInitialRegister:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 538
    return-void
.end method

.method public sendLiveVideo()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 532
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "sendLiveVideo:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 533
    return-void
.end method

.method public sendRPSMMA(Ljava/lang/String;)V
    .locals 2
    .param p1, "smscAddr"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 564
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "sendRPSMMA:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 565
    return-void
.end method

.method public sendReInvite(I)V
    .locals 2
    .param p1, "sessionHashCodesessionId"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 522
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "sendReInvite:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 523
    return-void
.end method

.method public sendRegister(I)Z
    .locals 2
    .param p1, "cause"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 547
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "sendRegister is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 548
    const/4 v0, 0x0

    return v0
.end method

.method public sendSMSOverIMS([BLjava/lang/String;Ljava/lang/String;I)V
    .locals 2
    .param p1, "pdu"    # [B
    .param p2, "destAddr"    # Ljava/lang/String;
    .param p3, "contentType"    # Ljava/lang/String;
    .param p4, "msgId"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 517
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "sendSMSOverIMS:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 518
    return-void
.end method

.method public sendSMSResponse(ZI)V
    .locals 2
    .param p1, "isSuccess"    # Z
    .param p2, "responseCode"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 512
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "sendSMSResponse:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 513
    return-void
.end method

.method public sendStillImage(Ljava/lang/String;ILjava/lang/String;I)V
    .locals 2
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "imageFormat"    # I
    .param p3, "frameSize"    # Ljava/lang/String;
    .param p4, "toFlip"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 507
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "sendStillImage:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 508
    return-void
.end method

.method public sendTtyData(I[B)V
    .locals 2
    .param p1, "sessionHashCode"    # I
    .param p2, "data"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 502
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "sendTtyData:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 503
    return-void
.end method

.method public sendUSSD(III[BI)I
    .locals 2
    .param p1, "ussdType"    # I
    .param p2, "dcs"    # I
    .param p3, "length"    # I
    .param p4, "ussdURI"    # [B
    .param p5, "timeout"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 496
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "sendUSSD:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 497
    const/4 v0, 0x0

    return v0
.end method

.method public setAutoResponse(II)V
    .locals 2
    .param p1, "flag"    # I
    .param p2, "code"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 491
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "setAutoResponse:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 492
    return-void
.end method

.method public setCallBarring(IIILjava/lang/String;)I
    .locals 2
    .param p1, "ssClass"    # I
    .param p2, "ssMode"    # I
    .param p3, "cbType"    # I
    .param p4, "password"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 485
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "setCallBarring:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 486
    const/4 v0, 0x0

    return v0
.end method

.method public setCallForwarding(IIIIIILjava/lang/String;)I
    .locals 2
    .param p1, "ssClass"    # I
    .param p2, "ssMode"    # I
    .param p3, "cfType"    # I
    .param p4, "noReplyTime"    # I
    .param p5, "cfURILength"    # I
    .param p6, "cfURIType"    # I
    .param p7, "cfURI"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 479
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "setCallForwarding:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 480
    const/4 v0, 0x0

    return v0
.end method

.method public setCallWaiting(II)I
    .locals 2
    .param p1, "ssClass"    # I
    .param p2, "mode"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 473
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "setCallWaiting:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 474
    const/4 v0, 0x0

    return v0
.end method

.method public setCameraDisplayOrientation()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 468
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "setCameraDisplayOrientation:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 469
    return-void
.end method

.method public setCameraDisplayOrientationEx(I)V
    .locals 2
    .param p1, "orientation"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 463
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "setCameraDisplayOrientationEx:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 464
    return-void
.end method

.method public setCameraEffect(I)V
    .locals 2
    .param p1, "value"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 527
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "setCameraEffect:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 528
    return-void
.end method

.method public setDisplay(Landroid/view/Surface;II)V
    .locals 2
    .param p1, "surface"    # Landroid/view/Surface;
    .param p2, "width"    # I
    .param p3, "height"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 458
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "setDisplay:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 459
    return-void
.end method

.method public setEmergencyPdnInfo([Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "intfName"    # [Ljava/lang/String;
    .param p2, "pcscf"    # [Ljava/lang/String;
    .param p3, "gwAddress"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 453
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "setEmergencyPdnInfo:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 454
    return-void
.end method

.method public setFarEndSurface(Landroid/view/Surface;II)V
    .locals 2
    .param p1, "surface"    # Landroid/view/Surface;
    .param p2, "width"    # I
    .param p3, "height"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 448
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "setFarEndSurface:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 449
    return-void
.end method

.method public setIsMediaReadyToReceivePreview(Z)V
    .locals 2
    .param p1, "flag"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 443
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "setIsMediaReadyToReceivePreview:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 444
    return-void
.end method

.method public setLTEVideoCallDisable()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 438
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "setLTEVideoCallDisable:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 439
    return-void
.end method

.method public setLTEVideoCallEnable()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 433
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "setLTEVideoCallEnable:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 434
    return-void
.end method

.method public setRegistrationFeatureTags(Ljava/lang/String;[Ljava/lang/String;)V
    .locals 2
    .param p1, "serviceID"    # Ljava/lang/String;
    .param p2, "featureTags"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 428
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "setRegistrationFeatureTags:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 429
    return-void
.end method

.method public setSpeakerMode(IZ)V
    .locals 2
    .param p1, "sessionHashCode"    # I
    .param p2, "speakerMode"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 423
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "setSpeakerMode:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 424
    return-void
.end method

.method public setThirdPartyMode(Z)V
    .locals 2
    .param p1, "flag"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 418
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, ":IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 419
    return-void
.end method

.method public setTtyMode(I)V
    .locals 2
    .param p1, "ttyMode"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 407
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "setTtyMode:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 408
    return-void
.end method

.method public setVoLTEFeatureDisable()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 402
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "setVoLTEFeatureDisable:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 403
    return-void
.end method

.method public setVoLTEFeatureEnable()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 397
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "setVoLTEFeatureEnable:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 398
    return-void
.end method

.method public setVolume(II)V
    .locals 2
    .param p1, "sessionHashCode"    # I
    .param p2, "index"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 392
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "setVolume:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 393
    return-void
.end method

.method public setZoom(I)V
    .locals 2
    .param p1, "value"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 387
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "setZoom:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 388
    return-void
.end method

.method public startAudio(I)V
    .locals 2
    .param p1, "sessionHashCode"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 382
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "startAudio:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 383
    return-void
.end method

.method public startCameraPreview(Landroid/view/Surface;)V
    .locals 2
    .param p1, "surface"    # Landroid/view/Surface;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 377
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "startCameraPreview:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 378
    return-void
.end method

.method public startDtmf(II)V
    .locals 2
    .param p1, "sessionHashCode"    # I
    .param p2, "code"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 372
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "startDtmf:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 373
    return-void
.end method

.method public startIMSSettingTransaction()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 304
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "startIMSSettingTransaction:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 305
    const/4 v0, -0x1

    return v0
.end method

.method public startNWayConferenceCall(III)I
    .locals 2
    .param p1, "heldSessionHashCode"    # I
    .param p2, "activeSessionHashCode"    # I
    .param p3, "callType"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 366
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "startNWayConferenceCall:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 367
    const/4 v0, 0x0

    return v0
.end method

.method public startRender(Z)V
    .locals 2
    .param p1, "isNearEnd"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 361
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "startRender:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 362
    return-void
.end method

.method public startT3402TimerValue(I)V
    .locals 2
    .param p1, "nT3402TimerValue"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1215
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "startT3402TimerValue is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1216
    return-void
.end method

.method public startVideo(I)V
    .locals 2
    .param p1, "sessionHashCode"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 356
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "startVideo:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 357
    return-void
.end method

.method public stopCameraPreview()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 351
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "stopCameraPreview:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 352
    return-void
.end method

.method public stopDtmf(I)V
    .locals 2
    .param p1, "sessionHashCode"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 346
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "stopDtmf:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 347
    return-void
.end method

.method public swapVideoSurface()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 341
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, ":IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 342
    return-void
.end method

.method public switchCamera()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 336
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "switchCamera:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 337
    return-void
.end method

.method public takeCall(Landroid/content/Intent;)V
    .locals 2
    .param p1, "incomingCallIntent"    # Landroid/content/Intent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 331
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "takeCall:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 332
    return-void
.end method

.method public toggleMute(I)V
    .locals 2
    .param p1, "sessionHashCode"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 326
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "toggleMute:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 327
    return-void
.end method

.method public unpublishCapabilitiesAndAvailability()Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 320
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "unpublishCapabilitiesAndAvailability:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 321
    const/4 v0, 0x0

    return v0
.end method

.method public updateIMSSettingValues(Landroid/os/Bundle;Landroid/os/Bundle;I)Z
    .locals 2
    .param p1, "updateMap"    # Landroid/os/Bundle;
    .param p2, "xList"    # Landroid/os/Bundle;
    .param p3, "transactionId"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 280
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "updateIMSSettingValues:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 281
    const/4 v0, 0x0

    return v0
.end method

.method public updateIMSSettingValuesWithUri(Ljava/lang/String;Ljava/lang/String;I)Z
    .locals 1
    .param p1, "xNodeUri"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;
    .param p3, "transactionId"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 299
    const/4 v0, 0x0

    return v0
.end method

.method public updateSSACInfo(ZIIII)V
    .locals 2
    .param p1, "status"    # Z
    .param p2, "voiceFactor"    # I
    .param p3, "voiceTime"    # I
    .param p4, "videoFactor"    # I
    .param p5, "videoTime"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1188
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "updateSSACInfo:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1189
    return-void
.end method

.method public upgradeCall(II)V
    .locals 2
    .param p1, "sessionHashCode"    # I
    .param p2, "type"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 275
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "upgradeCall:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 276
    return-void
.end method

.method public voiceRecord(IILjava/lang/String;)V
    .locals 2
    .param p1, "command"    # I
    .param p2, "sessionHashCode"    # I
    .param p3, "fileName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 270
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "voiceRecord:IMSSerivce is not available."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 271
    return-void
.end method
