.class public Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;
.super Ljava/lang/Object;
.source "IMSInterfaceForMediaService.java"

# interfaces
.implements Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMedia;


# static fields
.field private static final LOG_TAG:Ljava/lang/String;


# instance fields
.field private mCameraSurface:Landroid/view/Surface;

.field private mCameraSurfaceTexture:Landroid/graphics/SurfaceTexture;

.field private mFarEndSurface:Landroid/view/Surface;

.field private mFarEndSurfaceTexture:Landroid/graphics/SurfaceTexture;

.field private mIMSRemoteListenerList:Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;

.field private mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const-class v0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Z)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "waitForReady"    # Z

    .prologue
    const/4 v0, 0x0

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object v0, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    .line 25
    iput-object v0, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mIMSRemoteListenerList:Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;

    .line 27
    iput-object v0, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mFarEndSurface:Landroid/view/Surface;

    .line 28
    iput-object v0, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mFarEndSurfaceTexture:Landroid/graphics/SurfaceTexture;

    .line 29
    iput-object v0, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mCameraSurface:Landroid/view/Surface;

    .line 30
    iput-object v0, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mCameraSurfaceTexture:Landroid/graphics/SurfaceTexture;

    .line 40
    invoke-static {p1, p2}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getInstance(Landroid/content/Context;Z)Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    .line 41
    new-instance v0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;

    invoke-direct {v0, p1}, Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mIMSRemoteListenerList:Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;

    .line 43
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    new-instance v1, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService$1;

    invoke-direct {v1, p0}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService$1;-><init>(Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;)V

    invoke-virtual {v0, v1}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->addServiceConnectionListener(Lcom/samsung/commonimsinterface/imscommon/IIMSServiceConnectionListener;)V

    .line 60
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 18
    sget-object v0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->LOG_TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;)Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mIMSRemoteListenerList:Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;)Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    return-object v0
.end method


# virtual methods
.method public captureSurfaceImage(ZI)V
    .locals 4
    .param p1, "isNearEnd"    # Z
    .param p2, "onGoingCallType"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 705
    sget-object v1, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->LOG_TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "captureSurfaceImage isNearEnd:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " onGoingCallType:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 708
    :try_start_0
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v1}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v1

    invoke-interface {v1, p1, p2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->captureSurfaceImage(ZI)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    .line 714
    return-void

    .line 709
    :catch_0
    move-exception v0

    .line 710
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 711
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 712
    .local v0, "e":Ljava/lang/NullPointerException;
    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public closeCamera()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 364
    sget-object v1, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->LOG_TAG:Ljava/lang/String;

    const-string v2, "closeCamera"

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 367
    :try_start_0
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v1}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v1

    invoke-interface {v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->closeCamera()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    .line 373
    return-void

    .line 368
    :catch_0
    move-exception v0

    .line 369
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 370
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 371
    .local v0, "e":Ljava/lang/NullPointerException;
    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public deRegisterForMediaStateChange(Lcom/samsung/commonimsinterface/imscommon/IIMSListener;)V
    .locals 3
    .param p1, "listener"    # Lcom/samsung/commonimsinterface/imscommon/IIMSListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 71
    sget-object v0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->LOG_TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "deRegisterForMediaStateChange() with listener : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mIMSRemoteListenerList:Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;

    invoke-virtual {v0, p1}, Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;->removeListener(Lcom/samsung/commonimsinterface/imscommon/IIMSListener;)Z

    .line 74
    return-void
.end method

.method public deinitSurface(Z)V
    .locals 4
    .param p1, "isNearEnd"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 718
    sget-object v1, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->LOG_TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "deinitSurface isNearEnd:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 721
    :try_start_0
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v1}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v1

    invoke-interface {v1, p1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->deinitSurface(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    .line 727
    return-void

    .line 722
    :catch_0
    move-exception v0

    .line 723
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 724
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 725
    .local v0, "e":Ljava/lang/NullPointerException;
    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public getBackCameraId()I
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 405
    sget-object v2, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->LOG_TAG:Ljava/lang/String;

    const-string v3, "getBackCameraId"

    invoke-static {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 410
    :try_start_0
    iget-object v2, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v2}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v2

    invoke-interface {v2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->getBackCameraId()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v1

    .line 417
    .local v1, "iBackCameraId":I
    sget-object v2, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->LOG_TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getBackCameraId BackCameraId:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 419
    return v1

    .line 411
    .end local v1    # "iBackCameraId":I
    :catch_0
    move-exception v0

    .line 412
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v2, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 413
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 414
    .local v0, "e":Ljava/lang/NullPointerException;
    new-instance v2, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public getCameraDirection()I
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 535
    sget-object v2, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->LOG_TAG:Ljava/lang/String;

    const-string v3, "getCameraDirection"

    invoke-static {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 537
    const/4 v1, -0x1

    .line 540
    .local v1, "iDirection":I
    :try_start_0
    iget-object v2, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v2}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v2

    invoke-interface {v2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->getCameraDirection()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v1

    .line 547
    sget-object v2, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->LOG_TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getCameraDirection Direction:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 549
    return v1

    .line 541
    :catch_0
    move-exception v0

    .line 542
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v2, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 543
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 544
    .local v0, "e":Ljava/lang/NullPointerException;
    new-instance v2, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public getCameraParameters()Landroid/hardware/Camera$Parameters;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 383
    sget-object v0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->LOG_TAG:Ljava/lang/String;

    const-string v1, "getCameraParameters"

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 385
    const/4 v0, 0x0

    return-object v0
.end method

.method public getCameraPreviewSize(IZ)Landroid/hardware/Camera$Size;
    .locals 3
    .param p1, "targetSize"    # I
    .param p2, "isHeight"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 520
    sget-object v0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->LOG_TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getCameraPreviewSize targetSize:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " isHeight:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 522
    const/4 v0, 0x0

    return-object v0
.end method

.method public getCameraState()Lcom/samsung/commonimsinterface/imscommon/CameraState;
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 453
    sget-object v7, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->LOG_TAG:Ljava/lang/String;

    const-string v8, "getCameraState"

    invoke-static {v7, v8}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 455
    const/4 v4, 0x0

    .line 458
    .local v4, "result":Lcom/samsung/commonimsinterface/imscommon/CameraState;
    :try_start_0
    iget-object v7, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v7}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v7

    invoke-interface {v7}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->getCameraState()I

    move-result v5

    .line 460
    .local v5, "state":I
    invoke-static {}, Lcom/samsung/commonimsinterface/imscommon/CameraState;->values()[Lcom/samsung/commonimsinterface/imscommon/CameraState;

    move-result-object v0

    .local v0, "arr$":[Lcom/samsung/commonimsinterface/imscommon/CameraState;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v6, v0, v2

    .line 461
    .local v6, "value":Lcom/samsung/commonimsinterface/imscommon/CameraState;
    invoke-virtual {v6}, Lcom/samsung/commonimsinterface/imscommon/CameraState;->ordinal()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v7

    if-ne v7, v5, :cond_1

    .line 462
    move-object v4, v6

    .line 472
    .end local v6    # "value":Lcom/samsung/commonimsinterface/imscommon/CameraState;
    :cond_0
    sget-object v7, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->LOG_TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "getCameraState state:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 474
    return-object v4

    .line 460
    .restart local v6    # "value":Lcom/samsung/commonimsinterface/imscommon/CameraState;
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 466
    .end local v0    # "arr$":[Lcom/samsung/commonimsinterface/imscommon/CameraState;
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    .end local v5    # "state":I
    .end local v6    # "value":Lcom/samsung/commonimsinterface/imscommon/CameraState;
    :catch_0
    move-exception v1

    .line 467
    .local v1, "e":Landroid/os/RemoteException;
    new-instance v7, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v1}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 468
    .end local v1    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v1

    .line 469
    .local v1, "e":Ljava/lang/NullPointerException;
    new-instance v7, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v1}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v7
.end method

.method public getFrontCameraId()I
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 429
    sget-object v2, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->LOG_TAG:Ljava/lang/String;

    const-string v3, "getFrontCameraId"

    invoke-static {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 434
    :try_start_0
    iget-object v2, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v2}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v2

    invoke-interface {v2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->getFrontCameraId()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v1

    .line 441
    .local v1, "iFrontCameraId":I
    sget-object v2, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->LOG_TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getBackCameraId BackCameraId:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 443
    return v1

    .line 435
    .end local v1    # "iFrontCameraId":I
    :catch_0
    move-exception v0

    .line 436
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v2, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 437
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 438
    .local v0, "e":Ljava/lang/NullPointerException;
    new-instance v2, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public getMaxZoom()I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 755
    sget-object v1, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->LOG_TAG:Ljava/lang/String;

    const-string v2, "getMaxZoom"

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 757
    :try_start_0
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v1}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v1

    invoke-interface {v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->getMaxZoom()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v1

    return v1

    .line 758
    :catch_0
    move-exception v0

    .line 759
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 760
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 761
    .local v0, "e":Ljava/lang/NullPointerException;
    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public getNegotiatedFPS()I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 184
    sget-object v1, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->LOG_TAG:Ljava/lang/String;

    const-string v2, "getNegotiatedFPS"

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 186
    const/4 v0, 0x0

    .line 197
    .local v0, "iFps":I
    sget-object v1, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->LOG_TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getNegotiatedFPS FPS:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 199
    return v0
.end method

.method public getNegotiatedHeight()I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 138
    sget-object v1, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->LOG_TAG:Ljava/lang/String;

    const-string v2, "getNegotiatedHeight"

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 140
    const/4 v0, 0x0

    .line 151
    .local v0, "iHeight":I
    sget-object v1, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->LOG_TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getNegotiatedHeight Height:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 153
    return v0
.end method

.method public getNegotiatedWidth()I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 161
    sget-object v1, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->LOG_TAG:Ljava/lang/String;

    const-string v2, "getNegotiatedWidth"

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 163
    const/4 v0, 0x0

    .line 174
    .local v0, "iWidth":I
    sget-object v1, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->LOG_TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getNegotiatedWidth Width:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 176
    return v0
.end method

.method public getNumberOfCameras()I
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 247
    sget-object v2, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->LOG_TAG:Ljava/lang/String;

    const-string v3, "getNumberOfCameras"

    invoke-static {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 249
    const/4 v1, 0x0

    .line 252
    .local v1, "iNumberOfCameras":I
    :try_start_0
    iget-object v2, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v2}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v2

    invoke-interface {v2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->getNumberOfCameras()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v1

    .line 259
    sget-object v2, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->LOG_TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getNumberOfCameras iNumberOfCameras:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 261
    return v1

    .line 253
    :catch_0
    move-exception v0

    .line 254
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v2, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 255
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 256
    .local v0, "e":Ljava/lang/NullPointerException;
    new-instance v2, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public getZoom()I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 779
    sget-object v1, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->LOG_TAG:Ljava/lang/String;

    const-string v2, "getZoom"

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 781
    :try_start_0
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v1}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v1

    invoke-interface {v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->getZoom()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v1

    return v1

    .line 782
    :catch_0
    move-exception v0

    .line 783
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 784
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 785
    .local v0, "e":Ljava/lang/NullPointerException;
    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public holdVideo()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 731
    sget-object v1, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->LOG_TAG:Ljava/lang/String;

    const-string v2, "holdVideo"

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 733
    :try_start_0
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v1}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v1

    invoke-interface {v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->holdVideo()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    .line 739
    return-void

    .line 734
    :catch_0
    move-exception v0

    .line 735
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 736
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 737
    .local v0, "e":Ljava/lang/NullPointerException;
    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public isMediaReadyToReceivePreview()Z
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 207
    sget-object v1, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->LOG_TAG:Ljava/lang/String;

    const-string v2, "isMediaReadyToReceivePreview"

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 209
    const/4 v0, 0x0

    .line 220
    .local v0, "result":Z
    return v0
.end method

.method public mediaDeInit()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 97
    sget-object v1, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->LOG_TAG:Ljava/lang/String;

    const-string v2, "mediaDeInit"

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    :try_start_0
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v1}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v1

    invoke-interface {v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->mediaDeInit()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    .line 106
    return-void

    .line 101
    :catch_0
    move-exception v0

    .line 102
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 103
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 104
    .local v0, "e":Ljava/lang/NullPointerException;
    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public mediaInit()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 81
    sget-object v1, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->LOG_TAG:Ljava/lang/String;

    const-string v2, "mediaInit"

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    :try_start_0
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v1}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v1

    invoke-interface {v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->mediaInit()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    .line 90
    return-void

    .line 85
    :catch_0
    move-exception v0

    .line 86
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 87
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 88
    .local v0, "e":Ljava/lang/NullPointerException;
    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public openCamera(I)Z
    .locals 5
    .param p1, "cameraId"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 273
    sget-object v2, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->LOG_TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "openCamera cameraId:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 275
    const/4 v1, 0x0

    .line 278
    .local v1, "result":Z
    :try_start_0
    iget-object v2, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v2}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v2

    invoke-interface {v2, p1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->openCamera(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v1

    .line 285
    return v1

    .line 279
    :catch_0
    move-exception v0

    .line 280
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v2, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 281
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 282
    .local v0, "e":Ljava/lang/NullPointerException;
    new-instance v2, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public openCamera(II)Z
    .locals 5
    .param p1, "cameraId"    # I
    .param p2, "callType"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 297
    sget-object v2, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->LOG_TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "openCamera cameraId:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 299
    const/4 v1, 0x0

    .line 302
    .local v1, "result":Z
    :try_start_0
    iget-object v2, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v2}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v2

    invoke-interface {v2, p1, p2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->openCameraEx(II)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v1

    .line 309
    return v1

    .line 303
    :catch_0
    move-exception v0

    .line 304
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v2, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 305
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 306
    .local v0, "e":Ljava/lang/NullPointerException;
    new-instance v2, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public registerForMediaStateChange(Lcom/samsung/commonimsinterface/imscommon/IIMSListener;)V
    .locals 3
    .param p1, "listener"    # Lcom/samsung/commonimsinterface/imscommon/IIMSListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 64
    sget-object v0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->LOG_TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "registerForMediaStateChange() with listener : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mIMSRemoteListenerList:Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;

    invoke-virtual {v0, p1}, Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;->addListener(Lcom/samsung/commonimsinterface/imscommon/IIMSListener;)Z

    .line 67
    return-void
.end method

.method public resetCameraID()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 640
    sget-object v1, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->LOG_TAG:Ljava/lang/String;

    const-string v2, "resetCameraID"

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 643
    :try_start_0
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v1}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v1

    invoke-interface {v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->resetCameraID()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    .line 649
    return-void

    .line 644
    :catch_0
    move-exception v0

    .line 645
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 646
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 647
    .local v0, "e":Ljava/lang/NullPointerException;
    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public resumeVideo()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 743
    sget-object v1, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->LOG_TAG:Ljava/lang/String;

    const-string v2, "resumeVideo"

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 745
    :try_start_0
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v1}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v1

    invoke-interface {v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->resumeVideo()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    .line 751
    return-void

    .line 746
    :catch_0
    move-exception v0

    .line 747
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 748
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 749
    .local v0, "e":Ljava/lang/NullPointerException;
    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public sendLiveVideo()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 692
    sget-object v1, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->LOG_TAG:Ljava/lang/String;

    const-string v2, "sendLiveVideo"

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 695
    :try_start_0
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v1}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v1

    invoke-interface {v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->sendLiveVideo()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    .line 701
    return-void

    .line 696
    :catch_0
    move-exception v0

    .line 697
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 698
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 699
    .local v0, "e":Ljava/lang/NullPointerException;
    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public sendStillImage(Ljava/lang/String;ILjava/lang/String;)V
    .locals 4
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "imageFormat"    # I
    .param p3, "frameSize"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 653
    sget-object v1, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->LOG_TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "sendStillImage filePath:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " imageFormat:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " frameSize:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 656
    :try_start_0
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v1}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, p1, p2, p3, v2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->sendStillImage(Ljava/lang/String;ILjava/lang/String;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    .line 662
    return-void

    .line 657
    :catch_0
    move-exception v0

    .line 658
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 659
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 660
    .local v0, "e":Ljava/lang/NullPointerException;
    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public sendStillImage(Ljava/lang/String;ILjava/lang/String;I)V
    .locals 4
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "imageFormat"    # I
    .param p3, "frameSize"    # Ljava/lang/String;
    .param p4, "toFlip"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 666
    sget-object v1, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->LOG_TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "sendStillImage filePath:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " imageFormat:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " frameSize:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " toFlip:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 669
    :try_start_0
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v1}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v1

    invoke-interface {v1, p1, p2, p3, p4}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->sendStillImage(Ljava/lang/String;ILjava/lang/String;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    .line 675
    return-void

    .line 670
    :catch_0
    move-exception v0

    .line 671
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 672
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 673
    .local v0, "e":Ljava/lang/NullPointerException;
    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public setCameraDisplayOrientation()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 558
    sget-object v0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->LOG_TAG:Ljava/lang/String;

    const-string v1, "setCameraDisplayOrientation"

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 569
    sget-object v0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->LOG_TAG:Ljava/lang/String;

    const-string v1, "setCameraDisplayOrientation is not implemented."

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 570
    return-void
.end method

.method public setCameraDisplayOrientation(I)V
    .locals 4
    .param p1, "orientation"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 574
    sget-object v1, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->LOG_TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setCameraDisplayOrientation:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 577
    :try_start_0
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v1}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v1

    invoke-interface {v1, p1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->setCameraDisplayOrientationEx(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    .line 583
    return-void

    .line 578
    :catch_0
    move-exception v0

    .line 579
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 580
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 581
    .local v0, "e":Ljava/lang/NullPointerException;
    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public setCameraEffect(I)V
    .locals 3
    .param p1, "value"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 679
    sget-object v1, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->LOG_TAG:Ljava/lang/String;

    const-string v2, "setCameraEffect"

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 682
    :try_start_0
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v1}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v1

    invoke-interface {v1, p1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->setCameraEffect(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    .line 688
    return-void

    .line 683
    :catch_0
    move-exception v0

    .line 684
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 685
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 686
    .local v0, "e":Ljava/lang/NullPointerException;
    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public setCameraParameters(Landroid/hardware/Camera$Parameters;)V
    .locals 3
    .param p1, "parameters"    # Landroid/hardware/Camera$Parameters;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 395
    sget-object v0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->LOG_TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setCameraParameters parameters:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 396
    return-void
.end method

.method public setDisplay(Landroid/graphics/SurfaceTexture;)V
    .locals 5
    .param p1, "surfaceTexture"    # Landroid/graphics/SurfaceTexture;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 484
    sget-object v1, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->LOG_TAG:Ljava/lang/String;

    const-string v2, "setDisplay(SurfaceTexture)"

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 487
    :try_start_0
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mCameraSurfaceTexture:Landroid/graphics/SurfaceTexture;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mCameraSurfaceTexture:Landroid/graphics/SurfaceTexture;

    invoke-virtual {v1, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 488
    :cond_0
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mCameraSurface:Landroid/view/Surface;

    if-eqz v1, :cond_1

    .line 489
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mCameraSurface:Landroid/view/Surface;

    invoke-virtual {v1}, Landroid/view/Surface;->release()V

    .line 490
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mCameraSurface:Landroid/view/Surface;

    .line 493
    :cond_1
    if-eqz p1, :cond_2

    .line 494
    new-instance v1, Landroid/view/Surface;

    invoke-direct {v1, p1}, Landroid/view/Surface;-><init>(Landroid/graphics/SurfaceTexture;)V

    iput-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mCameraSurface:Landroid/view/Surface;

    .line 496
    :cond_2
    iput-object p1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mCameraSurfaceTexture:Landroid/graphics/SurfaceTexture;

    .line 499
    :cond_3
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v1}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mCameraSurface:Landroid/view/Surface;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-interface {v1, v2, v3, v4}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->setDisplay(Landroid/view/Surface;II)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    .line 505
    return-void

    .line 500
    :catch_0
    move-exception v0

    .line 501
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 502
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 503
    .local v0, "e":Ljava/lang/NullPointerException;
    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public setFarEndSurface(Landroid/graphics/SurfaceTexture;)V
    .locals 5
    .param p1, "surfaceTexture"    # Landroid/graphics/SurfaceTexture;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 110
    sget-object v1, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->LOG_TAG:Ljava/lang/String;

    const-string v2, "setFarEndSurface(SurfaceTexture)"

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    :try_start_0
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mFarEndSurfaceTexture:Landroid/graphics/SurfaceTexture;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mFarEndSurfaceTexture:Landroid/graphics/SurfaceTexture;

    invoke-virtual {v1, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 114
    :cond_0
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mFarEndSurface:Landroid/view/Surface;

    if-eqz v1, :cond_1

    .line 115
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mFarEndSurface:Landroid/view/Surface;

    invoke-virtual {v1}, Landroid/view/Surface;->release()V

    .line 116
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mFarEndSurface:Landroid/view/Surface;

    .line 119
    :cond_1
    if-eqz p1, :cond_2

    .line 120
    new-instance v1, Landroid/view/Surface;

    invoke-direct {v1, p1}, Landroid/view/Surface;-><init>(Landroid/graphics/SurfaceTexture;)V

    iput-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mFarEndSurface:Landroid/view/Surface;

    .line 122
    :cond_2
    iput-object p1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mFarEndSurfaceTexture:Landroid/graphics/SurfaceTexture;

    .line 125
    :cond_3
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v1}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mFarEndSurface:Landroid/view/Surface;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-interface {v1, v2, v3, v4}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->setFarEndSurface(Landroid/view/Surface;II)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    .line 131
    return-void

    .line 126
    :catch_0
    move-exception v0

    .line 127
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 128
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 129
    .local v0, "e":Ljava/lang/NullPointerException;
    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public setIsMediaReadyToReceivePreview(Z)V
    .locals 3
    .param p1, "flag"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 228
    sget-object v0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->LOG_TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setIsMediaReadyToReceivePreview flag:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 238
    return-void
.end method

.method public setZoom(I)V
    .locals 4
    .param p1, "value"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 767
    sget-object v1, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->LOG_TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setZoom value:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 769
    :try_start_0
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v1}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v1

    invoke-interface {v1, p1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->setZoom(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    .line 775
    return-void

    .line 770
    :catch_0
    move-exception v0

    .line 771
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 772
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 773
    .local v0, "e":Ljava/lang/NullPointerException;
    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public startCameraPreview(Landroid/graphics/SurfaceTexture;)V
    .locals 3
    .param p1, "surfaceTexture"    # Landroid/graphics/SurfaceTexture;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 319
    sget-object v1, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->LOG_TAG:Ljava/lang/String;

    const-string v2, "startCameraPreview(SurfaceTexture)"

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 322
    :try_start_0
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mCameraSurfaceTexture:Landroid/graphics/SurfaceTexture;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mCameraSurfaceTexture:Landroid/graphics/SurfaceTexture;

    invoke-virtual {v1, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 323
    :cond_0
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mCameraSurface:Landroid/view/Surface;

    if-eqz v1, :cond_1

    .line 324
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mCameraSurface:Landroid/view/Surface;

    invoke-virtual {v1}, Landroid/view/Surface;->release()V

    .line 325
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mCameraSurface:Landroid/view/Surface;

    .line 328
    :cond_1
    if-eqz p1, :cond_2

    .line 329
    new-instance v1, Landroid/view/Surface;

    invoke-direct {v1, p1}, Landroid/view/Surface;-><init>(Landroid/graphics/SurfaceTexture;)V

    iput-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mCameraSurface:Landroid/view/Surface;

    .line 331
    :cond_2
    iput-object p1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mCameraSurfaceTexture:Landroid/graphics/SurfaceTexture;

    .line 334
    :cond_3
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v1}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mCameraSurface:Landroid/view/Surface;

    invoke-interface {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->startCameraPreview(Landroid/view/Surface;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    .line 340
    return-void

    .line 335
    :catch_0
    move-exception v0

    .line 336
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 337
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 338
    .local v0, "e":Ljava/lang/NullPointerException;
    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public startRender(Z)V
    .locals 3
    .param p1, "isNearEnd"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 604
    sget-object v1, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->LOG_TAG:Ljava/lang/String;

    const-string v2, "startRender"

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 607
    :try_start_0
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v1}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v1

    invoke-interface {v1, p1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->startRender(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    .line 613
    return-void

    .line 608
    :catch_0
    move-exception v0

    .line 609
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 610
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 611
    .local v0, "e":Ljava/lang/NullPointerException;
    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public stopCameraPreview()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 348
    sget-object v1, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->LOG_TAG:Ljava/lang/String;

    const-string v2, "stopCameraPreview"

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 351
    :try_start_0
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v1}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v1

    invoke-interface {v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->stopCameraPreview()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    .line 357
    return-void

    .line 352
    :catch_0
    move-exception v0

    .line 353
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 354
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 355
    .local v0, "e":Ljava/lang/NullPointerException;
    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public swapSurface()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 622
    sget-object v1, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->LOG_TAG:Ljava/lang/String;

    const-string v2, "swapSurface"

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 625
    :try_start_0
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v1}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v1

    invoke-interface {v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->swapVideoSurface()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    .line 631
    return-void

    .line 626
    :catch_0
    move-exception v0

    .line 627
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 628
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 629
    .local v0, "e":Ljava/lang/NullPointerException;
    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public switchCamera()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 591
    sget-object v1, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->LOG_TAG:Ljava/lang/String;

    const-string v2, "switchCamera"

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 594
    :try_start_0
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v1}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v1

    invoke-interface {v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->switchCamera()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    .line 600
    return-void

    .line 595
    :catch_0
    move-exception v0

    .line 596
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 597
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 598
    .local v0, "e":Ljava/lang/NullPointerException;
    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v1
.end method
