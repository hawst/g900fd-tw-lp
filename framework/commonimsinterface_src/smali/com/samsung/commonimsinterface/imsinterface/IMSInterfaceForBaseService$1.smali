.class Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService$1;
.super Ljava/lang/Object;
.source "IMSInterfaceForBaseService.java"

# interfaces
.implements Lcom/samsung/commonimsinterface/imscommon/IIMSServiceConnectionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;


# direct methods
.method constructor <init>(Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;)V
    .locals 0

    .prologue
    .line 90
    iput-object p1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService$1;->this$0:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onConnected()V
    .locals 5

    .prologue
    .line 93
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Connected to IMS Service"

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 95
    iget-object v2, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService$1;->this$0:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    monitor-enter v2

    .line 97
    :try_start_0
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService$1;->this$0:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    iget-object v3, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService$1;->this$0:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    sget-object v4, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService$GetService;->WAIT_FOR_CONNECTED:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService$GetService;

    invoke-virtual {v3, v4}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getIMSService(Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService$GetService;)Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v3

    # setter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->mIMSService:Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;
    invoke-static {v1, v3}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$102(Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;)Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    .line 99
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService$1;->this$0:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->mIMSService:Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;
    invoke-static {v1}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$100(Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;)Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 100
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService$1;->this$0:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->mIMSRemoteListenerList:Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;
    invoke-static {v1}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$200(Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;)Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;

    move-result-object v1

    iget-object v3, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService$1;->this$0:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    iget-object v3, v3, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->mIMSServiceReadyListener:Lcom/samsung/commonimsinterface/imscommon/IIMSListener;

    invoke-virtual {v1, v3}, Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;->addListener(Lcom/samsung/commonimsinterface/imscommon/IIMSListener;)Z

    .line 101
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService$1;->this$0:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->mIMSService:Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;
    invoke-static {v1}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$100(Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;)Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v1

    iget-object v3, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService$1;->this$0:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->mIMSRemoteListenerList:Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;
    invoke-static {v3}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$200(Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;)Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;

    move-result-object v3

    invoke-interface {v1, v3}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->registerForServiceStateChange(Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 106
    :cond_0
    :goto_0
    :try_start_1
    monitor-exit v2

    .line 107
    return-void

    .line 103
    :catch_0
    move-exception v0

    .line 104
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 106
    .end local v0    # "e":Landroid/os/RemoteException;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public onDisconnected()V
    .locals 3

    .prologue
    .line 111
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Disconnected from IMS Service"

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService$1;->this$0:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    const/4 v1, 0x0

    # setter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->mIMSService:Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;
    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$102(Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;)Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    .line 115
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService$1;->this$0:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    monitor-enter v1

    .line 116
    :try_start_0
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService$1;->this$0:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    sget-object v2, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService$NotifyServiceStatus;->SERVICE_DISCONNECTED:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService$NotifyServiceStatus;

    invoke-virtual {v0, v2}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->notifyServiceState(Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService$NotifyServiceStatus;)V

    .line 117
    monitor-exit v1

    .line 118
    return-void

    .line 117
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
