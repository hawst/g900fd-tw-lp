.class public Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralLibrary;
.super Ljava/lang/Object;
.source "IMSInterfaceForGeneralLibrary.java"

# interfaces
.implements Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneral;


# static fields
.field private static final LOG_TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    const-class v0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralLibrary;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralLibrary;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    return-void
.end method


# virtual methods
.method public abortIMSSettingTransaction(I)V
    .locals 0
    .param p1, "transactionId"    # I

    .prologue
    .line 295
    return-void
.end method

.method public deRegisterForAPCSStateChange(Lcom/samsung/commonimsinterface/imscommon/IIMSListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/commonimsinterface/imscommon/IIMSListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 319
    return-void
.end method

.method public deRegisterForConnectivityStateChange(Lcom/samsung/commonimsinterface/imscommon/IIMSListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/commonimsinterface/imscommon/IIMSListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 303
    return-void
.end method

.method public deRegisterForRegisterStateChange(Lcom/samsung/commonimsinterface/imscommon/IIMSListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/commonimsinterface/imscommon/IIMSListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 311
    return-void
.end method

.method public deRegisterForServiceStateChange(Lcom/samsung/commonimsinterface/imscommon/IIMSListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/commonimsinterface/imscommon/IIMSListener;

    .prologue
    .line 44
    return-void
.end method

.method public deRegisterForSettingStateListener(Lcom/samsung/commonimsinterface/imscommon/IIMSListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/commonimsinterface/imscommon/IIMSListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 282
    return-void
.end method

.method public disableWiFiCalling()V
    .locals 0

    .prologue
    .line 181
    return-void
.end method

.method public enableWiFiCalling()V
    .locals 0

    .prologue
    .line 177
    return-void
.end method

.method public endIMSSettingTransaction(I)V
    .locals 0
    .param p1, "transactionId"    # I

    .prologue
    .line 291
    return-void
.end method

.method public forceRestart()Z
    .locals 1

    .prologue
    .line 172
    const/4 v0, 0x0

    return v0
.end method

.method public getErrorCode(I)I
    .locals 2
    .param p1, "networkType"    # I

    .prologue
    .line 137
    sget-object v0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralLibrary;->LOG_TAG:Ljava/lang/String;

    const-string v1, "getErrorCode()"

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 139
    const/4 v0, -0x1

    return v0
.end method

.method public getFeatureMask()I
    .locals 2

    .prologue
    .line 130
    sget-object v0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralLibrary;->LOG_TAG:Ljava/lang/String;

    const-string v1, "getFeatureMask()"

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralLibrary;->isRegistered()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getIMSSettingValues(Ljava/util/ArrayList;)Landroid/os/Bundle;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Landroid/os/Bundle;"
        }
    .end annotation

    .prologue
    .line 237
    .local p1, "xListUri":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v0, 0x0

    return-object v0
.end method

.method public getIMSSettingValues(Ljava/util/ArrayList;I)Landroid/os/Bundle;
    .locals 1
    .param p2, "transactionId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;I)",
            "Landroid/os/Bundle;"
        }
    .end annotation

    .prologue
    .line 242
    .local p1, "xListUri":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v0, 0x0

    return-object v0
.end method

.method public getIMSSettingValues([ILjava/util/ArrayList;)Landroid/os/Bundle;
    .locals 1
    .param p1, "fields"    # [I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([I",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Landroid/os/Bundle;"
        }
    .end annotation

    .prologue
    .line 222
    .local p2, "xList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v0, 0x0

    return-object v0
.end method

.method public getIMSSettingValues([ILjava/util/ArrayList;I)Landroid/os/Bundle;
    .locals 1
    .param p1, "fields"    # [I
    .param p3, "transactionId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([I",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;I)",
            "Landroid/os/Bundle;"
        }
    .end annotation

    .prologue
    .line 232
    .local p2, "xList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v0, 0x0

    return-object v0
.end method

.method public getIMSSettingValues([I)Landroid/util/SparseArray;
    .locals 1
    .param p1, "fields"    # [I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([I)",
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 217
    const/4 v0, 0x0

    return-object v0
.end method

.method public getIMSSettingValues([II)Landroid/util/SparseArray;
    .locals 1
    .param p1, "fields"    # [I
    .param p2, "transactionId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([II)",
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 227
    const/4 v0, 0x0

    return-object v0
.end method

.method public getServiceStatus()Z
    .locals 1

    .prologue
    .line 144
    const/4 v0, 0x0

    return v0
.end method

.method public handleActionsOverEpdg()V
    .locals 0

    .prologue
    .line 193
    return-void
.end method

.method public isDeregisteredToMobile()Z
    .locals 1

    .prologue
    .line 90
    const/4 v0, 0x0

    return v0
.end method

.method public isDeregisteredToWiFi()Z
    .locals 1

    .prologue
    .line 110
    const/4 v0, 0x0

    return v0
.end method

.method public isDeregisteringToMobile()Z
    .locals 1

    .prologue
    .line 95
    const/4 v0, 0x0

    return v0
.end method

.method public isDeregisteringToWiFi()Z
    .locals 1

    .prologue
    .line 115
    const/4 v0, 0x0

    return v0
.end method

.method public isDisablingMobileData()Z
    .locals 1

    .prologue
    .line 125
    const/4 v0, 0x0

    return v0
.end method

.method public isEABFeatureEnabled()Z
    .locals 1

    .prologue
    .line 365
    const/4 v0, 0x0

    return v0
.end method

.method public isEABMenuShow()Z
    .locals 1

    .prologue
    .line 352
    const/4 v0, 0x0

    return v0
.end method

.method public isEnablingMobileData()Z
    .locals 1

    .prologue
    .line 120
    const/4 v0, 0x0

    return v0
.end method

.method public isIMSServiceReady()Z
    .locals 1

    .prologue
    .line 33
    const/4 v0, 0x0

    return v0
.end method

.method public isLTEVideoCallEnabled()Z
    .locals 1

    .prologue
    .line 328
    const/4 v0, 0x0

    return v0
.end method

.method public isLTEVideoCallMenuShow()Z
    .locals 1

    .prologue
    .line 347
    const/4 v0, 0x0

    return v0
.end method

.method public isLimitedMode(I)Z
    .locals 1
    .param p1, "networkType"    # I

    .prologue
    .line 333
    const/4 v0, 0x0

    return v0
.end method

.method public isRegistered()Z
    .locals 2

    .prologue
    .line 48
    sget-object v0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralLibrary;->LOG_TAG:Ljava/lang/String;

    const-string v1, "isRegistered()"

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralLibrary;->isRegisteredToMobile()Z

    move-result v0

    return v0
.end method

.method public isRegisteredToMobile()Z
    .locals 4

    .prologue
    .line 55
    sget-object v1, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralLibrary;->LOG_TAG:Ljava/lang/String;

    const-string v2, "isRegisteredToMobile()"

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 76
    const-string v1, "persist.radio.ims.reg"

    const-string v2, "false"

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "true"

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    .line 78
    .local v0, "isRegistered":Z
    :goto_0
    sget-object v1, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralLibrary;->LOG_TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isRegisteredToMobile(): read from system property : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    return v0

    .line 76
    .end local v0    # "isRegistered":Z
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isRegisteredToWiFi()Z
    .locals 1

    .prologue
    .line 100
    const/4 v0, 0x0

    return v0
.end method

.method public isRegisteringToMobile()Z
    .locals 1

    .prologue
    .line 85
    const/4 v0, 0x0

    return v0
.end method

.method public isRegisteringToWiFi()Z
    .locals 1

    .prologue
    .line 105
    const/4 v0, 0x0

    return v0
.end method

.method public isVoLTEFeatureEnabled()Z
    .locals 1

    .prologue
    .line 323
    const/4 v0, 0x0

    return v0
.end method

.method public isVoLTEMenuShow()Z
    .locals 1

    .prologue
    .line 342
    const/4 v0, 0x0

    return v0
.end method

.method public manualDeregister()V
    .locals 0

    .prologue
    .line 153
    return-void
.end method

.method public manualRegister()V
    .locals 0

    .prologue
    .line 149
    return-void
.end method

.method public registerForAPCSStateChange(Lcom/samsung/commonimsinterface/imscommon/IIMSListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/commonimsinterface/imscommon/IIMSListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 315
    return-void
.end method

.method public registerForConnectivityStateChange(Lcom/samsung/commonimsinterface/imscommon/IIMSListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/commonimsinterface/imscommon/IIMSListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 299
    return-void
.end method

.method public registerForRegisterStateChange(Lcom/samsung/commonimsinterface/imscommon/IIMSListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/commonimsinterface/imscommon/IIMSListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 307
    return-void
.end method

.method public registerForServiceStateChange(Lcom/samsung/commonimsinterface/imscommon/IIMSListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/commonimsinterface/imscommon/IIMSListener;

    .prologue
    .line 39
    return-void
.end method

.method public registerForSettingStateListener(Lcom/samsung/commonimsinterface/imscommon/IIMSListener;[I)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/commonimsinterface/imscommon/IIMSListener;
    .param p2, "reqFields"    # [I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 278
    return-void
.end method

.method public sendDeregister()Z
    .locals 1

    .prologue
    .line 162
    const/4 v0, 0x0

    return v0
.end method

.method public sendDeregister(I)Z
    .locals 1
    .param p1, "cause"    # I

    .prologue
    .line 167
    const/4 v0, 0x0

    return v0
.end method

.method public sendInitialRegister(I)V
    .locals 0
    .param p1, "networkType"    # I

    .prologue
    .line 189
    return-void
.end method

.method public sendRegister(I)Z
    .locals 1
    .param p1, "cause"    # I

    .prologue
    .line 157
    const/4 v0, 0x0

    return v0
.end method

.method public setAllServicesDisabled(I)V
    .locals 0
    .param p1, "networkType"    # I

    .prologue
    .line 376
    return-void
.end method

.method public setAllServicesEnabled(I)V
    .locals 0
    .param p1, "networkType"    # I

    .prologue
    .line 371
    return-void
.end method

.method public setEABFeatureDisable()V
    .locals 0

    .prologue
    .line 361
    return-void
.end method

.method public setEABFeatureEnable()V
    .locals 0

    .prologue
    .line 357
    return-void
.end method

.method public setLTEVideoCallDisable()V
    .locals 0

    .prologue
    .line 209
    return-void
.end method

.method public setLTEVideoCallEnable()V
    .locals 0

    .prologue
    .line 205
    return-void
.end method

.method public setRegistrationFeatureTags(Ljava/lang/String;[Ljava/lang/String;)V
    .locals 0
    .param p1, "serviceID"    # Ljava/lang/String;
    .param p2, "featureTags"    # [Ljava/lang/String;

    .prologue
    .line 185
    return-void
.end method

.method public setThirdPartyMode(Z)V
    .locals 0
    .param p1, "flag"    # Z

    .prologue
    .line 213
    return-void
.end method

.method public setVoLTEFeatureDisable()V
    .locals 0

    .prologue
    .line 201
    return-void
.end method

.method public setVoLTEFeatureEnable()V
    .locals 0

    .prologue
    .line 197
    return-void
.end method

.method public startIMSSettingTransaction()I
    .locals 1

    .prologue
    .line 286
    const/4 v0, -0x1

    return v0
.end method

.method public startT3402TimerValue(I)V
    .locals 0
    .param p1, "nT3402TimerValue"    # I

    .prologue
    .line 338
    return-void
.end method

.method public updateIMSSettingValues(Landroid/util/SparseArray;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 257
    .local p1, "updateMap":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/lang/String;>;"
    const/4 v0, 0x0

    return v0
.end method

.method public updateIMSSettingValues(Landroid/util/SparseArray;I)Z
    .locals 1
    .param p2, "transactionId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/String;",
            ">;I)Z"
        }
    .end annotation

    .prologue
    .line 267
    .local p1, "updateMap":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/lang/String;>;"
    const/4 v0, 0x0

    return v0
.end method

.method public updateIMSSettingValues(Landroid/util/SparseArray;Landroid/os/Bundle;)Z
    .locals 1
    .param p2, "xList"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Landroid/os/Bundle;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 262
    .local p1, "updateMap":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/lang/String;>;"
    const/4 v0, 0x0

    return v0
.end method

.method public updateIMSSettingValues(Landroid/util/SparseArray;Landroid/os/Bundle;I)Z
    .locals 1
    .param p2, "xList"    # Landroid/os/Bundle;
    .param p3, "transactionId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Landroid/os/Bundle;",
            "I)Z"
        }
    .end annotation

    .prologue
    .line 272
    .local p1, "updateMap":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/lang/String;>;"
    const/4 v0, 0x0

    return v0
.end method

.method public updateIMSSettingValues(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p1, "xNodeUri"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 247
    const/4 v0, 0x0

    return v0
.end method

.method public updateIMSSettingValues(Ljava/lang/String;Ljava/lang/String;I)Z
    .locals 1
    .param p1, "xNodeUri"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;
    .param p3, "transactionId"    # I

    .prologue
    .line 252
    const/4 v0, 0x0

    return v0
.end method
