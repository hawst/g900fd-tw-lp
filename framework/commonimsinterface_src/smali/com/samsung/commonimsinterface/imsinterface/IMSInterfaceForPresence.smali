.class public interface abstract Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForPresence;
.super Ljava/lang/Object;
.source "IMSInterfaceForPresence.java"


# virtual methods
.method public abstract contactSvcCallFunction(IIILjava/lang/String;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation
.end method

.method public abstract deRegisterForPresenceNotifyUpdates(Lcom/samsung/commonimsinterface/imscommon/IIMSListener;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation
.end method

.method public abstract getRemoteCapabilitiesAndAvailability([Ljava/lang/String;Ljava/lang/String;)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation
.end method

.method public abstract getRemoteCapabilitiesAndAvailabilityPoll([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation
.end method

.method public abstract isProvisioned()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation
.end method

.method public abstract isRegistered()Z
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract publishCapabilitiesAndAvailability(Ljava/lang/String;Ljava/util/Hashtable;)Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Hashtable",
            "<**>;)Z"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation
.end method

.method public abstract registerForPresenceNotifyUpdates(Lcom/samsung/commonimsinterface/imscommon/IIMSListener;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation
.end method

.method public abstract registerServiceCapabilityAvailabilityTemplate(Ljava/lang/String;Ljava/lang/String;)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation
.end method

.method public abstract unpublishCapabilitiesAndAvailability()Z
.end method
