.class public Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForSSConfigService;
.super Ljava/lang/Object;
.source "IMSInterfaceForSSConfigService.java"

# interfaces
.implements Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForSSConfig;


# static fields
.field private static final LOG_TAG:Ljava/lang/String;


# instance fields
.field private mIMSRemoteListenerList:Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;

.field private mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    const-class v0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForSSConfigService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForSSConfigService;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Z)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "waitForReady"    # Z

    .prologue
    const/4 v0, 0x0

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object v0, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForSSConfigService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    .line 21
    iput-object v0, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForSSConfigService;->mIMSRemoteListenerList:Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;

    .line 30
    invoke-static {p1, p2}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getInstance(Landroid/content/Context;Z)Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForSSConfigService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    .line 31
    new-instance v0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;

    invoke-direct {v0, p1}, Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForSSConfigService;->mIMSRemoteListenerList:Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;

    .line 33
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForSSConfigService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    new-instance v1, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForSSConfigService$1;

    invoke-direct {v1, p0}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForSSConfigService$1;-><init>(Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForSSConfigService;)V

    invoke-virtual {v0, v1}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->addServiceConnectionListener(Lcom/samsung/commonimsinterface/imscommon/IIMSServiceConnectionListener;)V

    .line 50
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 14
    sget-object v0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForSSConfigService;->LOG_TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForSSConfigService;)Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForSSConfigService;

    .prologue
    .line 14
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForSSConfigService;->mIMSRemoteListenerList:Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForSSConfigService;)Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForSSConfigService;

    .prologue
    .line 14
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForSSConfigService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    return-object v0
.end method


# virtual methods
.method public deRegisterForSSConfigStateChange(Lcom/samsung/commonimsinterface/imscommon/IIMSListener;)V
    .locals 3
    .param p1, "listener"    # Lcom/samsung/commonimsinterface/imscommon/IIMSListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 61
    sget-object v0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForSSConfigService;->LOG_TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "deRegisterForSSConfigStateChange() with listener : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForSSConfigService;->mIMSRemoteListenerList:Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;

    invoke-virtual {v0, p1}, Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;->removeListener(Lcom/samsung/commonimsinterface/imscommon/IIMSListener;)Z

    .line 64
    return-void
.end method

.method public getCallBarring(II)I
    .locals 5
    .param p1, "ssClass"    # I
    .param p2, "cbType"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 191
    sget-object v2, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForSSConfigService;->LOG_TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getCallBarring() with ssClass : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " with cbType : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 193
    const/4 v1, 0x0

    .line 196
    .local v1, "result":I
    :try_start_0
    iget-object v2, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForSSConfigService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v2}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v2

    invoke-interface {v2, p1, p2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->getCallBarring(II)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 201
    return v1

    .line 197
    :catch_0
    move-exception v0

    .line 198
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v2, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public getCallForwarding(II)I
    .locals 5
    .param p1, "ssClass"    # I
    .param p2, "cfType"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 145
    sget-object v2, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForSSConfigService;->LOG_TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getCallForwarding() with ssClass : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " with cfType : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 147
    const/4 v1, 0x0

    .line 150
    .local v1, "result":I
    :try_start_0
    iget-object v2, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForSSConfigService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v2}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v2

    invoke-interface {v2, p1, p2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->getCallForwarding(II)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 155
    return v1

    .line 151
    :catch_0
    move-exception v0

    .line 152
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v2, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public getCallWaiting(I)I
    .locals 5
    .param p1, "ssClass"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 93
    sget-object v2, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForSSConfigService;->LOG_TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getCallWaiting() with ssClass : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 95
    const/4 v1, 0x0

    .line 98
    .local v1, "result":I
    :try_start_0
    iget-object v2, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForSSConfigService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v2}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v2

    invoke-interface {v2, p1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->getCallWaiting(I)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 103
    return v1

    .line 99
    :catch_0
    move-exception v0

    .line 100
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v2, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public mmSS_Svc_Api(IIILjava/lang/String;Ljava/lang/String;)I
    .locals 7
    .param p1, "funcId"    # I
    .param p2, "param1"    # I
    .param p3, "param2"    # I
    .param p4, "param3"    # Ljava/lang/String;
    .param p5, "param4"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 206
    const/16 v6, 0x11

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v6}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForSSConfigService;->mmSS_Svc_Api(IIILjava/lang/String;Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public mmSS_Svc_Api(IIILjava/lang/String;Ljava/lang/String;I)I
    .locals 9
    .param p1, "funcId"    # I
    .param p2, "param1"    # I
    .param p3, "param2"    # I
    .param p4, "param3"    # Ljava/lang/String;
    .param p5, "param4"    # Ljava/lang/String;
    .param p6, "param5"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 211
    sget-object v0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForSSConfigService;->LOG_TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mmSS_Svc_Api() with funcId : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " param1 : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " param2 : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " param3 : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " param4 : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " param5 : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 214
    const/4 v8, 0x0

    .line 217
    .local v8, "result":I
    :try_start_0
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForSSConfigService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v0}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v0

    move v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p4

    move-object v5, p5

    move v6, p6

    invoke-interface/range {v0 .. v6}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->mmSS_Svc_Api(IIILjava/lang/String;Ljava/lang/String;I)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v8

    .line 222
    :goto_0
    return v8

    .line 218
    :catch_0
    move-exception v7

    .line 219
    .local v7, "e":Landroid/os/RemoteException;
    invoke-virtual {v7}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public registerForSSConfigStateChange(Lcom/samsung/commonimsinterface/imscommon/IIMSListener;)V
    .locals 3
    .param p1, "listener"    # Lcom/samsung/commonimsinterface/imscommon/IIMSListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 54
    sget-object v0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForSSConfigService;->LOG_TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "registerForSSConfigStateChange() with listener : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 56
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForSSConfigService;->mIMSRemoteListenerList:Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;

    invoke-virtual {v0, p1}, Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;->addListener(Lcom/samsung/commonimsinterface/imscommon/IIMSListener;)Z

    .line 57
    return-void
.end method

.method public setCallBarring(IIILjava/lang/String;)I
    .locals 5
    .param p1, "ssClass"    # I
    .param p2, "ssMode"    # I
    .param p3, "cbType"    # I
    .param p4, "password"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 169
    sget-object v2, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForSSConfigService;->LOG_TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setCallBarring() with ssClass : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " with ssMode : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " with cbType : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " with password : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 174
    const/4 v1, 0x0

    .line 177
    .local v1, "result":I
    :try_start_0
    iget-object v2, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForSSConfigService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v2}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v2

    invoke-interface {v2, p1, p2, p3, p4}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->setCallBarring(IIILjava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 182
    return v1

    .line 178
    :catch_0
    move-exception v0

    .line 179
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v2, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public setCallForwarding(IIIIIILjava/lang/String;)I
    .locals 11
    .param p1, "ssClass"    # I
    .param p2, "ssMode"    # I
    .param p3, "cfType"    # I
    .param p4, "noReplyTime"    # I
    .param p5, "cfURILength"    # I
    .param p6, "cfURIType"    # I
    .param p7, "cfURI"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 120
    sget-object v1, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForSSConfigService;->LOG_TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setCallForwarding() with ssClass : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " with ssMode : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " with cfType : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " with noReplyTime : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " with cfURILength : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, p5

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " with cfURIType : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, p6

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " with cfURI : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p7

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 128
    const/4 v10, 0x0

    .line 131
    .local v10, "result":I
    :try_start_0
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForSSConfigService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v1}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v1

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move/from16 v6, p5

    move/from16 v7, p6

    move-object/from16 v8, p7

    invoke-interface/range {v1 .. v8}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->setCallForwarding(IIIIIILjava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v10

    .line 136
    return v10

    .line 132
    :catch_0
    move-exception v9

    .line 133
    .local v9, "e":Landroid/os/RemoteException;
    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v9}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public setCallWaiting(II)I
    .locals 5
    .param p1, "ssClass"    # I
    .param p2, "mode"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 75
    sget-object v2, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForSSConfigService;->LOG_TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setCallWaiting() with ssClass : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " with mode : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    const/4 v1, 0x0

    .line 80
    .local v1, "result":I
    :try_start_0
    iget-object v2, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForSSConfigService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v2}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v2

    invoke-interface {v2, p1, p2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->setCallWaiting(II)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 85
    return v1

    .line 81
    :catch_0
    move-exception v0

    .line 82
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v2, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v2
.end method
