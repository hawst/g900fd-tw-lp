.class public Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForPresenceService;
.super Ljava/lang/Object;
.source "IMSInterfaceForPresenceService.java"

# interfaces
.implements Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForPresence;


# static fields
.field private static final LOG_TAG:Ljava/lang/String;


# instance fields
.field private mIMSRemoteListenerList:Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;

.field private mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const-class v0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForPresenceService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForPresenceService;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Z)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "waitForReady"    # Z

    .prologue
    const/4 v0, 0x0

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object v0, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForPresenceService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    .line 23
    iput-object v0, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForPresenceService;->mIMSRemoteListenerList:Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;

    .line 32
    invoke-static {p1, p2}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getInstance(Landroid/content/Context;Z)Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForPresenceService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    .line 33
    new-instance v0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;

    invoke-direct {v0, p1}, Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForPresenceService;->mIMSRemoteListenerList:Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;

    .line 35
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForPresenceService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    new-instance v1, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForPresenceService$1;

    invoke-direct {v1, p0}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForPresenceService$1;-><init>(Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForPresenceService;)V

    invoke-virtual {v0, v1}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->addServiceConnectionListener(Lcom/samsung/commonimsinterface/imscommon/IIMSServiceConnectionListener;)V

    .line 52
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 16
    sget-object v0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForPresenceService;->LOG_TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForPresenceService;)Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForPresenceService;

    .prologue
    .line 16
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForPresenceService;->mIMSRemoteListenerList:Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForPresenceService;)Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForPresenceService;

    .prologue
    .line 16
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForPresenceService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    return-object v0
.end method


# virtual methods
.method public contactSvcCallFunction(IIILjava/lang/String;)V
    .locals 0
    .param p1, "funcId"    # I
    .param p2, "param1"    # I
    .param p3, "param2"    # I
    .param p4, "params"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 157
    return-void
.end method

.method public deRegisterForPresenceNotifyUpdates(Lcom/samsung/commonimsinterface/imscommon/IIMSListener;)V
    .locals 3
    .param p1, "listener"    # Lcom/samsung/commonimsinterface/imscommon/IIMSListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 63
    sget-object v0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForPresenceService;->LOG_TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "deRegisterForPresenceNotifyUpdates() with listener : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForPresenceService;->mIMSRemoteListenerList:Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;

    invoke-virtual {v0, p1}, Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;->removeListener(Lcom/samsung/commonimsinterface/imscommon/IIMSListener;)Z

    .line 66
    return-void
.end method

.method public getRemoteCapabilitiesAndAvailability([Ljava/lang/String;Ljava/lang/String;)Z
    .locals 4
    .param p1, "uri"    # [Ljava/lang/String;
    .param p2, "serviceId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 108
    const/4 v1, 0x0

    .line 111
    .local v1, "result":Z
    :try_start_0
    iget-object v2, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForPresenceService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v2}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v2

    invoke-interface {v2, p1, p2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->getRemoteCapabilitiesAndAvailability([Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 116
    return v1

    .line 112
    :catch_0
    move-exception v0

    .line 113
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v2, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public getRemoteCapabilitiesAndAvailabilityPoll([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Z
    .locals 4
    .param p1, "uri"    # [Ljava/lang/String;
    .param p2, "serviceId"    # Ljava/lang/String;
    .param p3, "token"    # Ljava/lang/String;
    .param p4, "isListSubscribe"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 121
    const/4 v1, 0x0

    .line 124
    .local v1, "result":Z
    :try_start_0
    iget-object v2, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForPresenceService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v2}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v2

    invoke-interface {v2, p1, p2, p3, p4}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->getRemoteCapabilitiesAndAvailabilityPoll([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 129
    return v1

    .line 125
    :catch_0
    move-exception v0

    .line 126
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v2, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public isProvisioned()Z
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 167
    const/4 v1, 0x0

    .line 170
    .local v1, "result":Z
    :try_start_0
    iget-object v2, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForPresenceService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v2}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v2

    invoke-interface {v2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->isProvisioned()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 175
    return v1

    .line 171
    :catch_0
    move-exception v0

    .line 172
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v2, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public isRegistered()Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 162
    const/4 v0, 0x0

    return v0
.end method

.method public publishCapabilitiesAndAvailability(Ljava/lang/String;Ljava/util/Hashtable;)Z
    .locals 4
    .param p1, "serviceId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Hashtable",
            "<**>;)Z"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 76
    .local p2, "keyValuePairs":Ljava/util/Hashtable;, "Ljava/util/Hashtable<**>;"
    const/4 v1, 0x0

    .line 79
    .local v1, "result":Z
    :try_start_0
    iget-object v2, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForPresenceService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v2}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v2

    invoke-static {p2}, Lcom/samsung/commonimsinterface/imscommon/IMSUtility;->convertHashtableToBundle(Ljava/util/Hashtable;)Landroid/os/Bundle;

    move-result-object v3

    invoke-interface {v2, p1, v3}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->publishCapabilitiesAndAvailability(Ljava/lang/String;Landroid/os/Bundle;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 84
    return v1

    .line 80
    :catch_0
    move-exception v0

    .line 81
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v2, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public registerForPresenceNotifyUpdates(Lcom/samsung/commonimsinterface/imscommon/IIMSListener;)V
    .locals 3
    .param p1, "listener"    # Lcom/samsung/commonimsinterface/imscommon/IIMSListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 56
    sget-object v0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForPresenceService;->LOG_TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "registerForPresenceNotifyUpdates() with listener : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForPresenceService;->mIMSRemoteListenerList:Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;

    invoke-virtual {v0, p1}, Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;->addListener(Lcom/samsung/commonimsinterface/imscommon/IIMSListener;)Z

    .line 59
    return-void
.end method

.method public registerServiceCapabilityAvailabilityTemplate(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 4
    .param p1, "serviceId"    # Ljava/lang/String;
    .param p2, "pidfTemplate"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 143
    const/4 v1, 0x0

    .line 146
    .local v1, "result":Z
    :try_start_0
    iget-object v2, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForPresenceService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v2}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v2

    invoke-interface {v2, p1, p2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->registerServiceCapabilityAvailabilityTemplate(Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 151
    return v1

    .line 147
    :catch_0
    move-exception v0

    .line 148
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v2, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public unpublishCapabilitiesAndAvailability()Z
    .locals 3

    .prologue
    .line 89
    const/4 v1, 0x0

    .line 92
    .local v1, "result":Z
    :try_start_0
    iget-object v2, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForPresenceService;->mService:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v2}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->getService()Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v2

    invoke-interface {v2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->unpublishCapabilitiesAndAvailability()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 97
    :goto_0
    return v1

    .line 93
    :catch_0
    move-exception v0

    .line 94
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method
