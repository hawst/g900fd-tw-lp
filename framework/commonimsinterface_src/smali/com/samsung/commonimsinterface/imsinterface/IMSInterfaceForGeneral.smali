.class public interface abstract Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneral;
.super Ljava/lang/Object;
.source "IMSInterfaceForGeneral.java"


# virtual methods
.method public abstract abortIMSSettingTransaction(I)V
.end method

.method public abstract deRegisterForAPCSStateChange(Lcom/samsung/commonimsinterface/imscommon/IIMSListener;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation
.end method

.method public abstract deRegisterForConnectivityStateChange(Lcom/samsung/commonimsinterface/imscommon/IIMSListener;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation
.end method

.method public abstract deRegisterForRegisterStateChange(Lcom/samsung/commonimsinterface/imscommon/IIMSListener;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation
.end method

.method public abstract deRegisterForServiceStateChange(Lcom/samsung/commonimsinterface/imscommon/IIMSListener;)V
.end method

.method public abstract deRegisterForSettingStateListener(Lcom/samsung/commonimsinterface/imscommon/IIMSListener;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation
.end method

.method public abstract disableWiFiCalling()V
.end method

.method public abstract enableWiFiCalling()V
.end method

.method public abstract endIMSSettingTransaction(I)V
.end method

.method public abstract forceRestart()Z
.end method

.method public abstract getErrorCode(I)I
.end method

.method public abstract getFeatureMask()I
.end method

.method public abstract getIMSSettingValues(Ljava/util/ArrayList;)Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Landroid/os/Bundle;"
        }
    .end annotation
.end method

.method public abstract getIMSSettingValues(Ljava/util/ArrayList;I)Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;I)",
            "Landroid/os/Bundle;"
        }
    .end annotation
.end method

.method public abstract getIMSSettingValues([ILjava/util/ArrayList;)Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([I",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Landroid/os/Bundle;"
        }
    .end annotation
.end method

.method public abstract getIMSSettingValues([ILjava/util/ArrayList;I)Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([I",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;I)",
            "Landroid/os/Bundle;"
        }
    .end annotation
.end method

.method public abstract getIMSSettingValues([I)Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([I)",
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getIMSSettingValues([II)Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([II)",
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getServiceStatus()Z
.end method

.method public abstract handleActionsOverEpdg()V
.end method

.method public abstract isDeregisteredToMobile()Z
.end method

.method public abstract isDeregisteredToWiFi()Z
.end method

.method public abstract isDeregisteringToMobile()Z
.end method

.method public abstract isDeregisteringToWiFi()Z
.end method

.method public abstract isDisablingMobileData()Z
.end method

.method public abstract isEABFeatureEnabled()Z
.end method

.method public abstract isEABMenuShow()Z
.end method

.method public abstract isEnablingMobileData()Z
.end method

.method public abstract isIMSServiceReady()Z
.end method

.method public abstract isLTEVideoCallEnabled()Z
.end method

.method public abstract isLTEVideoCallMenuShow()Z
.end method

.method public abstract isLimitedMode(I)Z
.end method

.method public abstract isRegistered()Z
.end method

.method public abstract isRegisteredToMobile()Z
.end method

.method public abstract isRegisteredToWiFi()Z
.end method

.method public abstract isRegisteringToMobile()Z
.end method

.method public abstract isRegisteringToWiFi()Z
.end method

.method public abstract isVoLTEFeatureEnabled()Z
.end method

.method public abstract isVoLTEMenuShow()Z
.end method

.method public abstract manualDeregister()V
.end method

.method public abstract manualRegister()V
.end method

.method public abstract registerForAPCSStateChange(Lcom/samsung/commonimsinterface/imscommon/IIMSListener;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation
.end method

.method public abstract registerForConnectivityStateChange(Lcom/samsung/commonimsinterface/imscommon/IIMSListener;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation
.end method

.method public abstract registerForRegisterStateChange(Lcom/samsung/commonimsinterface/imscommon/IIMSListener;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation
.end method

.method public abstract registerForServiceStateChange(Lcom/samsung/commonimsinterface/imscommon/IIMSListener;)V
.end method

.method public abstract registerForSettingStateListener(Lcom/samsung/commonimsinterface/imscommon/IIMSListener;[I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation
.end method

.method public abstract sendDeregister()Z
.end method

.method public abstract sendDeregister(I)Z
.end method

.method public abstract sendInitialRegister(I)V
.end method

.method public abstract sendRegister(I)Z
.end method

.method public abstract setAllServicesDisabled(I)V
.end method

.method public abstract setAllServicesEnabled(I)V
.end method

.method public abstract setEABFeatureDisable()V
.end method

.method public abstract setEABFeatureEnable()V
.end method

.method public abstract setLTEVideoCallDisable()V
.end method

.method public abstract setLTEVideoCallEnable()V
.end method

.method public abstract setRegistrationFeatureTags(Ljava/lang/String;[Ljava/lang/String;)V
.end method

.method public abstract setThirdPartyMode(Z)V
.end method

.method public abstract setVoLTEFeatureDisable()V
.end method

.method public abstract setVoLTEFeatureEnable()V
.end method

.method public abstract startIMSSettingTransaction()I
.end method

.method public abstract startT3402TimerValue(I)V
.end method

.method public abstract updateIMSSettingValues(Landroid/util/SparseArray;)Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation
.end method

.method public abstract updateIMSSettingValues(Landroid/util/SparseArray;I)Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/String;",
            ">;I)Z"
        }
    .end annotation
.end method

.method public abstract updateIMSSettingValues(Landroid/util/SparseArray;Landroid/os/Bundle;)Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Landroid/os/Bundle;",
            ")Z"
        }
    .end annotation
.end method

.method public abstract updateIMSSettingValues(Landroid/util/SparseArray;Landroid/os/Bundle;I)Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Landroid/os/Bundle;",
            "I)Z"
        }
    .end annotation
.end method

.method public abstract updateIMSSettingValues(Ljava/lang/String;Ljava/lang/String;)Z
.end method

.method public abstract updateIMSSettingValues(Ljava/lang/String;Ljava/lang/String;I)Z
.end method
