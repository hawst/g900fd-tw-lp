.class public interface abstract Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForSSConfig;
.super Ljava/lang/Object;
.source "IMSInterfaceForSSConfig.java"


# virtual methods
.method public abstract deRegisterForSSConfigStateChange(Lcom/samsung/commonimsinterface/imscommon/IIMSListener;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation
.end method

.method public abstract getCallBarring(II)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation
.end method

.method public abstract getCallForwarding(II)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation
.end method

.method public abstract getCallWaiting(I)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation
.end method

.method public abstract mmSS_Svc_Api(IIILjava/lang/String;Ljava/lang/String;)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation
.end method

.method public abstract mmSS_Svc_Api(IIILjava/lang/String;Ljava/lang/String;I)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation
.end method

.method public abstract registerForSSConfigStateChange(Lcom/samsung/commonimsinterface/imscommon/IIMSListener;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation
.end method

.method public abstract setCallBarring(IIILjava/lang/String;)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation
.end method

.method public abstract setCallForwarding(IIIIIILjava/lang/String;)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation
.end method

.method public abstract setCallWaiting(II)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation
.end method
