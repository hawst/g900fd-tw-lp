.class Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService$2;
.super Ljava/lang/Object;
.source "IMSInterfaceForBaseService.java"

# interfaces
.implements Lcom/samsung/commonimsinterface/imscommon/IIMSListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;


# direct methods
.method constructor <init>(Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;)V
    .locals 0

    .prologue
    .line 121
    iput-object p1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService$2;->this$0:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public declared-synchronized onReceive(IILcom/samsung/commonimsinterface/imscommon/IMSParameter;)V
    .locals 3
    .param p1, "token"    # I
    .param p2, "action"    # I
    .param p3, "parameter"    # Lcom/samsung/commonimsinterface/imscommon/IMSParameter;

    .prologue
    .line 124
    monitor-enter p0

    :try_start_0
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Received IMS Service Ready State ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 126
    packed-switch p2, :pswitch_data_0

    .line 140
    :goto_0
    monitor-exit p0

    return-void

    .line 128
    :pswitch_0
    :try_start_1
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService$2;->this$0:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    sget-object v1, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService$NotifyServiceStatus;->SYSTEM_READY:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService$NotifyServiceStatus;

    invoke-virtual {v0, v1}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->notifyServiceState(Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService$NotifyServiceStatus;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 124
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 132
    :pswitch_1
    :try_start_2
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService$2;->this$0:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    sget-object v1, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService$NotifyServiceStatus;->SYSTEM_NOT_READY:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService$NotifyServiceStatus;

    invoke-virtual {v0, v1}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->notifyServiceState(Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService$NotifyServiceStatus;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 126
    nop

    :pswitch_data_0
    .packed-switch 0x3fd
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
