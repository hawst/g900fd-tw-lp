.class final enum Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService$NotifyServiceStatus;
.super Ljava/lang/Enum;
.source "IMSInterfaceForBaseService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "NotifyServiceStatus"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService$NotifyServiceStatus;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService$NotifyServiceStatus;

.field public static final enum ADDED_LISTENER:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService$NotifyServiceStatus;

.field public static final enum SERVICE_DISCONNECTED:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService$NotifyServiceStatus;

.field public static final enum SYSTEM_NOT_READY:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService$NotifyServiceStatus;

.field public static final enum SYSTEM_READY:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService$NotifyServiceStatus;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 40
    new-instance v0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService$NotifyServiceStatus;

    const-string v1, "ADDED_LISTENER"

    invoke-direct {v0, v1, v2}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService$NotifyServiceStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService$NotifyServiceStatus;->ADDED_LISTENER:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService$NotifyServiceStatus;

    .line 41
    new-instance v0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService$NotifyServiceStatus;

    const-string v1, "SYSTEM_READY"

    invoke-direct {v0, v1, v3}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService$NotifyServiceStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService$NotifyServiceStatus;->SYSTEM_READY:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService$NotifyServiceStatus;

    .line 42
    new-instance v0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService$NotifyServiceStatus;

    const-string v1, "SYSTEM_NOT_READY"

    invoke-direct {v0, v1, v4}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService$NotifyServiceStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService$NotifyServiceStatus;->SYSTEM_NOT_READY:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService$NotifyServiceStatus;

    .line 43
    new-instance v0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService$NotifyServiceStatus;

    const-string v1, "SERVICE_DISCONNECTED"

    invoke-direct {v0, v1, v5}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService$NotifyServiceStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService$NotifyServiceStatus;->SERVICE_DISCONNECTED:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService$NotifyServiceStatus;

    .line 39
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService$NotifyServiceStatus;

    sget-object v1, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService$NotifyServiceStatus;->ADDED_LISTENER:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService$NotifyServiceStatus;

    aput-object v1, v0, v2

    sget-object v1, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService$NotifyServiceStatus;->SYSTEM_READY:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService$NotifyServiceStatus;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService$NotifyServiceStatus;->SYSTEM_NOT_READY:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService$NotifyServiceStatus;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService$NotifyServiceStatus;->SERVICE_DISCONNECTED:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService$NotifyServiceStatus;

    aput-object v1, v0, v5

    sput-object v0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService$NotifyServiceStatus;->$VALUES:[Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService$NotifyServiceStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 39
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService$NotifyServiceStatus;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 39
    const-class v0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService$NotifyServiceStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService$NotifyServiceStatus;

    return-object v0
.end method

.method public static values()[Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService$NotifyServiceStatus;
    .locals 1

    .prologue
    .line 39
    sget-object v0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService$NotifyServiceStatus;->$VALUES:[Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService$NotifyServiceStatus;

    invoke-virtual {v0}, [Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService$NotifyServiceStatus;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService$NotifyServiceStatus;

    return-object v0
.end method
