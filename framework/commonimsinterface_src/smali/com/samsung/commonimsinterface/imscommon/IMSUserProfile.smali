.class public Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;
.super Ljava/lang/Object;
.source "IMSUserProfile.java"

# interfaces
.implements Landroid/os/Parcelable;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile$Builder;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;",
            ">;"
        }
    .end annotation
.end field

.field private static final DEFAULT_PORT:I = 0x13c4

.field private static final LOG_TAG:Ljava/lang/String;

.field private static final TCP:Ljava/lang/String; = "TCP"

.field private static final UDP:Ljava/lang/String; = "UDP"

.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public available:I

.field private mAuthUserName:Ljava/lang/String;

.field private mAutoRegistration:Z

.field private transient mCallingUid:I

.field public transient mCapabilities:Landroid/os/Bundle;

.field private mDomain:Ljava/lang/String;

.field private mPassword:Ljava/lang/String;

.field private mPort:I

.field private mProfileName:Ljava/lang/String;

.field private mProtocol:Ljava/lang/String;

.field private mProxyAddress:Ljava/lang/String;

.field private mSendKeepAlive:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const-class v0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->LOG_TAG:Ljava/lang/String;

    .line 62
    new-instance v0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile$1;

    invoke-direct {v0}, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile$1;-><init>()V

    sput-object v0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 304
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    const-string v0, "UDP"

    iput-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mProtocol:Ljava/lang/String;

    .line 32
    const/16 v0, 0x13c4

    iput v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mPort:I

    .line 33
    iput-boolean v1, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mSendKeepAlive:Z

    .line 34
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mAutoRegistration:Z

    .line 35
    iput v1, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mCallingUid:I

    .line 305
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 3
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 307
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    const-string v0, "UDP"

    iput-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mProtocol:Ljava/lang/String;

    .line 32
    const/16 v0, 0x13c4

    iput v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mPort:I

    .line 33
    iput-boolean v1, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mSendKeepAlive:Z

    .line 34
    iput-boolean v2, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mAutoRegistration:Z

    .line 35
    iput v1, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mCallingUid:I

    .line 309
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mProxyAddress:Ljava/lang/String;

    .line 310
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mPassword:Ljava/lang/String;

    .line 311
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mDomain:Ljava/lang/String;

    .line 312
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mProtocol:Ljava/lang/String;

    .line 313
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mProfileName:Ljava/lang/String;

    .line 314
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mSendKeepAlive:Z

    .line 315
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_1

    :goto_1
    iput-boolean v1, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mAutoRegistration:Z

    .line 316
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mCallingUid:I

    .line 317
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mPort:I

    .line 318
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mAuthUserName:Ljava/lang/String;

    .line 319
    invoke-virtual {p1}, Landroid/os/Parcel;->readBundle()Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mCapabilities:Landroid/os/Bundle;

    .line 320
    return-void

    :cond_0
    move v0, v2

    .line 314
    goto :goto_0

    :cond_1
    move v1, v2

    .line 315
    goto :goto_1
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile$1;

    .prologue
    .line 19
    invoke-direct {p0, p1}, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile$1;

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;-><init>()V

    return-void
.end method

.method public constructor <init>(Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;)V
    .locals 2
    .param p1, "profile"    # Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;

    .prologue
    const/4 v1, 0x0

    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    const-string v0, "UDP"

    iput-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mProtocol:Ljava/lang/String;

    .line 32
    const/16 v0, 0x13c4

    iput v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mPort:I

    .line 33
    iput-boolean v1, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mSendKeepAlive:Z

    .line 34
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mAutoRegistration:Z

    .line 35
    iput v1, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mCallingUid:I

    .line 75
    invoke-virtual {p1}, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->getProxyAddress()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mProxyAddress:Ljava/lang/String;

    .line 76
    invoke-virtual {p1}, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->getPassword()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mPassword:Ljava/lang/String;

    .line 77
    invoke-virtual {p1}, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->getSipDomain()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mDomain:Ljava/lang/String;

    .line 78
    invoke-virtual {p1}, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->getProtocol()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mProtocol:Ljava/lang/String;

    .line 79
    invoke-virtual {p1}, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->getProfileName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mProfileName:Ljava/lang/String;

    .line 80
    invoke-virtual {p1}, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->getAuthUserName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mAuthUserName:Ljava/lang/String;

    .line 81
    invoke-virtual {p1}, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->getPort()I

    move-result v0

    iput v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mPort:I

    .line 82
    invoke-virtual {p1}, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->getSendKeepAlive()Z

    move-result v0

    iput-boolean v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mSendKeepAlive:Z

    .line 83
    invoke-virtual {p1}, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->getAutoRegistration()Z

    move-result v0

    iput-boolean v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mAutoRegistration:Z

    .line 84
    invoke-virtual {p1}, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->getCallingUid()I

    move-result v0

    iput v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mCallingUid:I

    .line 85
    invoke-virtual {p1}, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->getCapabilities()Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mCapabilities:Landroid/os/Bundle;

    .line 86
    invoke-virtual {p1}, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->getAvailability()I

    move-result v0

    iput v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->available:I

    .line 87
    return-void
.end method

.method static synthetic access$1002(Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;
    .param p1, "x1"    # Z

    .prologue
    .line 19
    iput-boolean p1, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mSendKeepAlive:Z

    return p1
.end method

.method static synthetic access$1102(Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;
    .param p1, "x1"    # Z

    .prologue
    .line 19
    iput-boolean p1, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mAutoRegistration:Z

    return p1
.end method

.method static synthetic access$200(Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 19
    invoke-virtual {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$302(Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;
    .param p1, "x1"    # I

    .prologue
    .line 19
    iput p1, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mPort:I

    return p1
.end method

.method static synthetic access$400()Ljava/lang/String;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->LOG_TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$502(Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 19
    iput-object p1, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mProfileName:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$602(Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 19
    iput-object p1, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mDomain:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$702(Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 19
    iput-object p1, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mAuthUserName:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$802(Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 19
    iput-object p1, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mPassword:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$902(Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 19
    iput-object p1, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mProtocol:Ljava/lang/String;

    return-object p1
.end method

.method private readResolve()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 497
    iget v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mPort:I

    if-nez v0, :cond_0

    .line 498
    const/16 v0, 0x13c4

    iput v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mPort:I

    .line 499
    :cond_0
    return-object p0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 340
    const/4 v0, 0x0

    return v0
.end method

.method public getAuthUserName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 404
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mAuthUserName:Ljava/lang/String;

    return-object v0
.end method

.method public getAutoRegistration()Z
    .locals 1

    .prologue
    .line 476
    iget-boolean v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mAutoRegistration:Z

    return v0
.end method

.method public getAvailability()I
    .locals 1

    .prologue
    .line 55
    iget v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->available:I

    return v0
.end method

.method public getCallingUid()I
    .locals 1

    .prologue
    .line 492
    iget v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mCallingUid:I

    return v0
.end method

.method public getCapabilities()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mCapabilities:Landroid/os/Bundle;

    return-object v0
.end method

.method public getDisplayName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 384
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mProfileName:Ljava/lang/String;

    return-object v0
.end method

.method public getMDN()Ljava/lang/String;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mProfileName:Ljava/lang/String;

    return-object v0
.end method

.method public getPassword()Ljava/lang/String;
    .locals 1

    .prologue
    .line 413
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mPassword:Ljava/lang/String;

    return-object v0
.end method

.method public getPort()I
    .locals 1

    .prologue
    .line 431
    iget v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mPort:I

    return v0
.end method

.method public getProfileName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 458
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mProfileName:Ljava/lang/String;

    return-object v0
.end method

.method public getProtocol()Ljava/lang/String;
    .locals 1

    .prologue
    .line 440
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mProtocol:Ljava/lang/String;

    return-object v0
.end method

.method public getProxyAddress()Ljava/lang/String;
    .locals 1

    .prologue
    .line 449
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mProxyAddress:Ljava/lang/String;

    return-object v0
.end method

.method public getSendKeepAlive()Z
    .locals 1

    .prologue
    .line 467
    iget-boolean v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mSendKeepAlive:Z

    return v0
.end method

.method public getSipDomain()Ljava/lang/String;
    .locals 1

    .prologue
    .line 422
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mDomain:Ljava/lang/String;

    return-object v0
.end method

.method public getUriString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 363
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "sip:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->getUserName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "@"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mDomain:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getUserName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 393
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mProfileName:Ljava/lang/String;

    return-object v0
.end method

.method public setCallingUid(I)V
    .locals 0
    .param p1, "uid"    # I

    .prologue
    .line 484
    iput p1, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mCallingUid:I

    .line 485
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 325
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mProxyAddress:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 326
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mPassword:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 327
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mDomain:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 328
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mProtocol:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 329
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mProfileName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 330
    iget-boolean v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mSendKeepAlive:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 331
    iget-boolean v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mAutoRegistration:Z

    if-eqz v0, :cond_1

    :goto_1
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 332
    iget v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mCallingUid:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 333
    iget v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mPort:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 334
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mAuthUserName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 335
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mCapabilities:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    .line 336
    return-void

    :cond_0
    move v0, v2

    .line 330
    goto :goto_0

    :cond_1
    move v1, v2

    .line 331
    goto :goto_1
.end method
