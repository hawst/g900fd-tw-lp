.class public interface abstract Lcom/samsung/commonimsinterface/imscommon/IIMSConstants;
.super Ljava/lang/Object;
.source "IIMSConstants.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/commonimsinterface/imscommon/IIMSConstants$TOKEN_TYPE;,
        Lcom/samsung/commonimsinterface/imscommon/IIMSConstants$BEARER_TYPE;,
        Lcom/samsung/commonimsinterface/imscommon/IIMSConstants$ERROR_CODE;,
        Lcom/samsung/commonimsinterface/imscommon/IIMSConstants$eCameraEffect;,
        Lcom/samsung/commonimsinterface/imscommon/IIMSConstants$SSCONFIG;,
        Lcom/samsung/commonimsinterface/imscommon/IIMSConstants$PRESENCE;,
        Lcom/samsung/commonimsinterface/imscommon/IIMSConstants$CALL;,
        Lcom/samsung/commonimsinterface/imscommon/IIMSConstants$GENERAL;
    }
.end annotation


# static fields
.field public static final ACTION_INET_DATA_CHANGE_STARTED:Ljava/lang/String; = "com.samsung.commonimsinterface.action.MOBILE_DATA_CHANGE"

.field public static final ACTION_PS_BARRED:Ljava/lang/String; = "com.android.intent.action.PSBARRED_FOR_VOLTE"

.field public static final EXTRA_DATA_ENABLED:Ljava/lang/String; = "dataEnabled"

.field public static final FIELD_SPEC_TYPE_IMS:Ljava/lang/String; = "IMS_PDN"

.field public static final FIELD_SPEC_TYPE_INTERNET:Ljava/lang/String; = "INTERNET_PDN"

.field public static final FIELD_SPEC_TYPE_WIFI:Ljava/lang/String; = "WIFI"
