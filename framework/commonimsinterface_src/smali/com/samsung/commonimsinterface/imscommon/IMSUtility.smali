.class public Lcom/samsung/commonimsinterface/imscommon/IMSUtility;
.super Ljava/lang/Object;
.source "IMSUtility.java"


# static fields
.field private static final LOG_TAG:Ljava/lang/String;

.field private static final MAX_USSD_LENGTH:I = 0xb6


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    const-class v0, Lcom/samsung/commonimsinterface/imscommon/IMSUtility;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/commonimsinterface/imscommon/IMSUtility;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static addStringIntoArrayListInSparseArray(Landroid/util/SparseArray;Ljava/lang/String;I)V
    .locals 1
    .param p1, "data"    # Ljava/lang/String;
    .param p2, "key"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;>;",
            "Ljava/lang/String;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 390
    .local p0, "sp":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/ArrayList<Ljava/lang/String;>;>;"
    if-nez p0, :cond_0

    .line 399
    :goto_0
    return-void

    .line 393
    :cond_0
    invoke-virtual {p0, p2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 394
    .local v0, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-nez v0, :cond_1

    .line 395
    new-instance v0, Ljava/util/ArrayList;

    .end local v0    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 396
    .restart local v0    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {p0, p2, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 398
    :cond_1
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public static addUniqeStringIntoArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 3
    .param p0, "str"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 375
    .local p1, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-nez p1, :cond_0

    .line 387
    :goto_0
    return-void

    .line 376
    :cond_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-nez v2, :cond_1

    .line 377
    invoke-virtual {p1, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 380
    :cond_1
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 381
    .local v1, "value":Ljava/lang/String;
    if-eqz v1, :cond_2

    .line 382
    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto :goto_0

    .line 386
    .end local v1    # "value":Ljava/lang/String;
    :cond_3
    invoke-virtual {p1, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public static bundleToString(Landroid/os/Bundle;)Ljava/lang/String;
    .locals 6
    .param p0, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 414
    if-nez p0, :cond_0

    .line 415
    const-string v4, ""

    .line 424
    :goto_0
    return-object v4

    .line 417
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 418
    .local v2, "strB":Ljava/lang/StringBuilder;
    const-string v3, ""

    .line 419
    .local v3, "value":Ljava/lang/String;
    invoke-virtual {p0}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 420
    .local v1, "key":Ljava/lang/String;
    if-eqz v1, :cond_1

    .line 421
    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 422
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " ["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "] ,"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 424
    .end local v1    # "key":Ljava/lang/String;
    :cond_2
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_0
.end method

.method public static clearArrayListInSparseArray(Landroid/util/SparseArray;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 402
    .local p0, "sp":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/ArrayList<Ljava/lang/String;>;>;"
    if-nez p0, :cond_0

    .line 411
    :goto_0
    return-void

    .line 405
    :cond_0
    const/4 v0, 0x0

    .local v0, "i":I
    invoke-virtual {p0}, Landroid/util/SparseArray;->size()I

    move-result v2

    .local v2, "n":I
    :goto_1
    if-ge v0, v2, :cond_2

    .line 406
    invoke-virtual {p0, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    .line 407
    .local v1, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-nez v1, :cond_1

    .line 405
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 408
    :cond_1
    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    goto :goto_2

    .line 410
    .end local v1    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_2
    invoke-virtual {p0}, Landroid/util/SparseArray;->clear()V

    goto :goto_0
.end method

.method private static clearObject(Ljava/lang/Object;)V
    .locals 11
    .param p0, "spVal"    # Ljava/lang/Object;

    .prologue
    .line 313
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v9

    .line 314
    .local v9, "type":Ljava/lang/String;
    const-string v10, "SparseArray"

    invoke-virtual {v10, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    move-object v8, p0

    .line 315
    check-cast v8, Landroid/util/SparseArray;

    .line 316
    .local v8, "sp":Landroid/util/SparseArray;, "Landroid/util/SparseArray<*>;"
    invoke-static {v8}, Lcom/samsung/commonimsinterface/imscommon/IMSUtility;->clearSparseArray(Landroid/util/SparseArray;)V

    .line 339
    .end local v8    # "sp":Landroid/util/SparseArray;, "Landroid/util/SparseArray<*>;"
    :cond_0
    :goto_0
    return-void

    .line 317
    :cond_1
    const-string v10, "ContentValues"

    invoke-virtual {v10, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    move-object v1, p0

    .line 318
    check-cast v1, Landroid/content/ContentValues;

    .line 319
    .local v1, "cv":Landroid/content/ContentValues;
    invoke-virtual {v1}, Landroid/content/ContentValues;->clear()V

    goto :goto_0

    .line 320
    .end local v1    # "cv":Landroid/content/ContentValues;
    :cond_2
    const-string v10, "ArrayList"

    invoke-virtual {v10, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_5

    move-object v0, p0

    .line 321
    check-cast v0, Ljava/util/ArrayList;

    .line 322
    .local v0, "ar":Ljava/util/ArrayList;, "Ljava/util/ArrayList<*>;"
    const/4 v2, 0x0

    .local v2, "i":I
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v5

    .local v5, "n":I
    :goto_1
    if-ge v2, v5, :cond_4

    .line 323
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    .line 324
    .local v6, "obVal":Ljava/lang/Object;
    if-eqz v6, :cond_3

    .line 325
    invoke-static {v6}, Lcom/samsung/commonimsinterface/imscommon/IMSUtility;->clearObject(Ljava/lang/Object;)V

    .line 322
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 328
    .end local v6    # "obVal":Ljava/lang/Object;
    :cond_4
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    goto :goto_0

    .line 329
    .end local v0    # "ar":Ljava/util/ArrayList;, "Ljava/util/ArrayList<*>;"
    .end local v2    # "i":I
    .end local v5    # "n":I
    :cond_5
    const-string v10, "HashMap"

    invoke-virtual {v10, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    move-object v4, p0

    .line 330
    check-cast v4, Ljava/util/HashMap;

    .line 331
    .local v4, "map":Ljava/util/HashMap;, "Ljava/util/HashMap<**>;"
    invoke-virtual {v4}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v10

    invoke-interface {v10}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_6
    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_7

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    .line 332
    .local v7, "objKey":Ljava/lang/Object;
    invoke-virtual {v4, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    .line 333
    .restart local v6    # "obVal":Ljava/lang/Object;
    if-eqz v6, :cond_6

    .line 334
    invoke-static {v6}, Lcom/samsung/commonimsinterface/imscommon/IMSUtility;->clearObject(Ljava/lang/Object;)V

    goto :goto_2

    .line 337
    .end local v6    # "obVal":Ljava/lang/Object;
    .end local v7    # "objKey":Ljava/lang/Object;
    :cond_7
    invoke-virtual {v4}, Ljava/util/HashMap;->clear()V

    goto :goto_0
.end method

.method public static clearSparseArray(Landroid/util/SparseArray;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 304
    .local p0, "spArray":Landroid/util/SparseArray;, "Landroid/util/SparseArray<*>;"
    const/4 v0, 0x0

    .local v0, "i":I
    invoke-virtual {p0}, Landroid/util/SparseArray;->size()I

    move-result v1

    .local v1, "n":I
    :goto_0
    if-ge v0, v1, :cond_1

    .line 305
    invoke-virtual {p0, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v2

    .line 306
    .local v2, "spVal":Ljava/lang/Object;
    if-nez v2, :cond_0

    .line 304
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 307
    :cond_0
    invoke-static {v2}, Lcom/samsung/commonimsinterface/imscommon/IMSUtility;->clearObject(Ljava/lang/Object;)V

    goto :goto_1

    .line 309
    .end local v2    # "spVal":Ljava/lang/Object;
    :cond_1
    invoke-virtual {p0}, Landroid/util/SparseArray;->clear()V

    .line 310
    return-void
.end method

.method public static convertHashtableToBundle(Ljava/util/Hashtable;)Landroid/os/Bundle;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Hashtable",
            "<**>;)",
            "Landroid/os/Bundle;"
        }
    .end annotation

    .prologue
    .line 26
    .local p0, "map":Ljava/util/Hashtable;, "Ljava/util/Hashtable<**>;"
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 28
    .local v0, "bundle":Landroid/os/Bundle;
    if-eqz p0, :cond_0

    .line 29
    invoke-virtual {p0}, Ljava/util/Hashtable;->keys()Ljava/util/Enumeration;

    move-result-object v1

    .line 31
    .local v1, "e":Ljava/util/Enumeration;, "Ljava/util/Enumeration<*>;"
    :goto_0
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 32
    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    move-object v2, v4

    check-cast v2, Ljava/lang/String;

    .line 33
    .local v2, "key":Ljava/lang/String;
    invoke-virtual {p0, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    .line 35
    .local v3, "value":Ljava/lang/String;
    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 39
    .end local v1    # "e":Ljava/util/Enumeration;, "Ljava/util/Enumeration<*>;"
    .end local v2    # "key":Ljava/lang/String;
    .end local v3    # "value":Ljava/lang/String;
    :cond_0
    return-object v0
.end method

.method public static correctRightShift(BI)B
    .locals 1
    .param p0, "source"    # B
    .param p1, "size"    # I

    .prologue
    .line 78
    packed-switch p1, :pswitch_data_0

    .line 89
    :goto_0
    return p0

    .line 79
    :pswitch_0
    and-int/lit8 v0, p0, 0x7f

    int-to-byte p0, v0

    goto :goto_0

    .line 80
    :pswitch_1
    and-int/lit8 v0, p0, 0x3f

    int-to-byte p0, v0

    goto :goto_0

    .line 81
    :pswitch_2
    and-int/lit8 v0, p0, 0x1f

    int-to-byte p0, v0

    goto :goto_0

    .line 82
    :pswitch_3
    and-int/lit8 v0, p0, 0xf

    int-to-byte p0, v0

    goto :goto_0

    .line 83
    :pswitch_4
    and-int/lit8 v0, p0, 0x7

    int-to-byte p0, v0

    goto :goto_0

    .line 84
    :pswitch_5
    and-int/lit8 v0, p0, 0x3

    int-to-byte p0, v0

    goto :goto_0

    .line 85
    :pswitch_6
    and-int/lit8 v0, p0, 0x1

    int-to-byte p0, v0

    goto :goto_0

    .line 78
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public static getBitHex(B)Ljava/lang/String;
    .locals 5
    .param p0, "raw"    # B

    .prologue
    const/16 v4, 0x8

    .line 61
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 62
    .local v2, "result":Ljava/lang/StringBuilder;
    const/16 v0, 0x80

    .line 64
    .local v0, "compare":I
    const/4 v1, 0x0

    .local v1, "index":I
    :goto_0
    if-ge v1, v4, :cond_1

    .line 65
    and-int v3, p0, v0

    if-eqz v3, :cond_0

    .line 66
    const/16 v3, 0x31

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 71
    :goto_1
    shr-int/lit8 v0, v0, 0x1

    .line 64
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 68
    :cond_0
    const/16 v3, 0x30

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 74
    :cond_1
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method public static getCurrentTime()Ljava/lang/String;
    .locals 12

    .prologue
    .line 231
    const/4 v6, 0x0

    .line 232
    .local v6, "month":Ljava/lang/String;
    const/4 v2, 0x0

    .line 233
    .local v2, "day":Ljava/lang/String;
    const/4 v3, 0x0

    .line 234
    .local v3, "hour":Ljava/lang/String;
    const/4 v5, 0x0

    .line 235
    .local v5, "minute":Ljava/lang/String;
    const/4 v7, 0x0

    .line 236
    .local v7, "second":Ljava/lang/String;
    const/4 v4, 0x0

    .line 237
    .local v4, "milliSecond":Ljava/lang/String;
    const/4 v1, 0x0

    .line 238
    .local v1, "currentTime":Ljava/lang/String;
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 240
    .local v0, "calendar":Ljava/util/Calendar;
    new-instance v8, Ljava/text/DecimalFormat;

    const-string v9, "00"

    invoke-direct {v8, v9}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    const/4 v9, 0x2

    invoke-virtual {v0, v9}, Ljava/util/Calendar;->get(I)I

    move-result v9

    add-int/lit8 v9, v9, 0x1

    int-to-long v10, v9

    invoke-virtual {v8, v10, v11}, Ljava/text/DecimalFormat;->format(J)Ljava/lang/String;

    move-result-object v6

    .line 241
    new-instance v8, Ljava/text/DecimalFormat;

    const-string v9, "00"

    invoke-direct {v8, v9}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    const/4 v9, 0x5

    invoke-virtual {v0, v9}, Ljava/util/Calendar;->get(I)I

    move-result v9

    int-to-long v10, v9

    invoke-virtual {v8, v10, v11}, Ljava/text/DecimalFormat;->format(J)Ljava/lang/String;

    move-result-object v2

    .line 242
    new-instance v8, Ljava/text/DecimalFormat;

    const-string v9, "00"

    invoke-direct {v8, v9}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    const/16 v9, 0xb

    invoke-virtual {v0, v9}, Ljava/util/Calendar;->get(I)I

    move-result v9

    int-to-long v10, v9

    invoke-virtual {v8, v10, v11}, Ljava/text/DecimalFormat;->format(J)Ljava/lang/String;

    move-result-object v3

    .line 243
    new-instance v8, Ljava/text/DecimalFormat;

    const-string v9, "00"

    invoke-direct {v8, v9}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    const/16 v9, 0xc

    invoke-virtual {v0, v9}, Ljava/util/Calendar;->get(I)I

    move-result v9

    int-to-long v10, v9

    invoke-virtual {v8, v10, v11}, Ljava/text/DecimalFormat;->format(J)Ljava/lang/String;

    move-result-object v5

    .line 244
    new-instance v8, Ljava/text/DecimalFormat;

    const-string v9, "00"

    invoke-direct {v8, v9}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    const/16 v9, 0xd

    invoke-virtual {v0, v9}, Ljava/util/Calendar;->get(I)I

    move-result v9

    int-to-long v10, v9

    invoke-virtual {v8, v10, v11}, Ljava/text/DecimalFormat;->format(J)Ljava/lang/String;

    move-result-object v7

    .line 245
    new-instance v8, Ljava/text/DecimalFormat;

    const-string v9, "000"

    invoke-direct {v8, v9}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    const/16 v9, 0xe

    invoke-virtual {v0, v9}, Ljava/util/Calendar;->get(I)I

    move-result v9

    int-to-long v10, v9

    invoke-virtual {v8, v10, v11}, Ljava/text/DecimalFormat;->format(J)Ljava/lang/String;

    move-result-object v4

    .line 247
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "-"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ":"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ":"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "."

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 249
    return-object v1
.end method

.method public static getCurrentTimeShort()Ljava/lang/String;
    .locals 10

    .prologue
    .line 253
    const/4 v2, 0x0

    .line 254
    .local v2, "hour":Ljava/lang/String;
    const/4 v4, 0x0

    .line 255
    .local v4, "minute":Ljava/lang/String;
    const/4 v5, 0x0

    .line 256
    .local v5, "second":Ljava/lang/String;
    const/4 v3, 0x0

    .line 257
    .local v3, "milliSecond":Ljava/lang/String;
    const/4 v1, 0x0

    .line 258
    .local v1, "currentTime":Ljava/lang/String;
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 260
    .local v0, "calendar":Ljava/util/Calendar;
    new-instance v6, Ljava/text/DecimalFormat;

    const-string v7, "00"

    invoke-direct {v6, v7}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    const/16 v7, 0xb

    invoke-virtual {v0, v7}, Ljava/util/Calendar;->get(I)I

    move-result v7

    int-to-long v8, v7

    invoke-virtual {v6, v8, v9}, Ljava/text/DecimalFormat;->format(J)Ljava/lang/String;

    move-result-object v2

    .line 261
    new-instance v6, Ljava/text/DecimalFormat;

    const-string v7, "00"

    invoke-direct {v6, v7}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    const/16 v7, 0xc

    invoke-virtual {v0, v7}, Ljava/util/Calendar;->get(I)I

    move-result v7

    int-to-long v8, v7

    invoke-virtual {v6, v8, v9}, Ljava/text/DecimalFormat;->format(J)Ljava/lang/String;

    move-result-object v4

    .line 262
    new-instance v6, Ljava/text/DecimalFormat;

    const-string v7, "00"

    invoke-direct {v6, v7}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    const/16 v7, 0xd

    invoke-virtual {v0, v7}, Ljava/util/Calendar;->get(I)I

    move-result v7

    int-to-long v8, v7

    invoke-virtual {v6, v8, v9}, Ljava/text/DecimalFormat;->format(J)Ljava/lang/String;

    move-result-object v5

    .line 263
    new-instance v6, Ljava/text/DecimalFormat;

    const-string v7, "000"

    invoke-direct {v6, v7}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    const/16 v7, 0xe

    invoke-virtual {v0, v7}, Ljava/util/Calendar;->get(I)I

    move-result v7

    int-to-long v8, v7

    invoke-virtual {v6, v8, v9}, Ljava/text/DecimalFormat;->format(J)Ljava/lang/String;

    move-result-object v3

    .line 265
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ":"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ":"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "."

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 267
    return-object v1
.end method

.method public static getHex([B)Ljava/lang/String;
    .locals 6
    .param p0, "data"    # [B

    .prologue
    .line 43
    const-string v2, "0123456789ABCDEF"

    .line 45
    .local v2, "reference":Ljava/lang/String;
    if-nez p0, :cond_0

    .line 46
    const/4 v4, 0x0

    .line 57
    :goto_0
    return-object v4

    .line 49
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    array-length v4, p0

    mul-int/lit8 v4, v4, 0x2

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 52
    .local v0, "hex":Ljava/lang/StringBuilder;
    const/4 v1, 0x0

    .local v1, "index":I
    :goto_1
    array-length v4, p0

    if-ge v1, v4, :cond_1

    .line 53
    aget-byte v3, p0, v1

    .line 54
    .local v3, "token":B
    and-int/lit16 v4, v3, 0xf0

    shr-int/lit8 v4, v4, 0x4

    invoke-virtual {v2, v4}, Ljava/lang/String;->charAt(I)C

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v4

    and-int/lit8 v5, v3, 0xf

    invoke-virtual {v2, v5}, Ljava/lang/String;->charAt(I)C

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 52
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 57
    .end local v3    # "token":B
    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_0
.end method

.method public static intArrayToString([I)Ljava/lang/String;
    .locals 5
    .param p0, "arr"    # [I

    .prologue
    .line 282
    if-nez p0, :cond_1

    .line 283
    const-string v2, ""

    .line 289
    :cond_0
    return-object v2

    .line 285
    :cond_1
    const-string v2, ""

    .line 286
    .local v2, "retVal":Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, "i":I
    array-length v1, p0

    .local v1, "n":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 287
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "{"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget v4, p0, v0

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "} "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 286
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static objArrayToString([Ljava/lang/Object;)Ljava/lang/String;
    .locals 5
    .param p0, "arr"    # [Ljava/lang/Object;

    .prologue
    .line 293
    if-nez p0, :cond_1

    .line 294
    const-string v2, ""

    .line 300
    :cond_0
    return-object v2

    .line 296
    :cond_1
    const-string v2, ""

    .line 297
    .local v2, "retVal":Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, "i":I
    array-length v1, p0

    .local v1, "n":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 298
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "{"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget-object v4, p0, v0

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "} "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 297
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static packToGsm7bit([BI)[B
    .locals 12
    .param p0, "data"    # [B
    .param p1, "inLength"    # I

    .prologue
    const/4 v11, 0x0

    .line 93
    const/4 v0, 0x0

    .line 94
    .local v0, "index":I
    const/4 v2, 0x0

    .line 95
    .local v2, "pos":I
    const/4 v4, 0x0

    .line 96
    .local v4, "shift":I
    const/4 v1, 0x0

    .line 97
    .local v1, "outLength":I
    move-object v5, p0

    .line 98
    .local v5, "source":[B
    const/4 v6, 0x0

    .line 99
    .local v6, "target":[B
    const/4 v3, 0x0

    .line 100
    .local v3, "result":[B
    const/4 v7, 0x0

    .line 102
    .local v7, "temp":B
    sget-object v8, Lcom/samsung/commonimsinterface/imscommon/IMSUtility;->LOG_TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "packToGsm7bit Begin : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-static {v5}, Lcom/samsung/commonimsinterface/imscommon/IMSUtility;->getHex([B)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    const/4 v0, 0x0

    :goto_0
    if-ge v0, p1, :cond_2

    .line 106
    aget-byte v8, v5, v0

    const/16 v9, 0x40

    if-ne v8, v9, :cond_1

    .line 107
    aput-byte v11, v5, v0

    .line 105
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 108
    :cond_1
    aget-byte v8, v5, v0

    const/16 v9, 0x24

    if-ne v8, v9, :cond_0

    .line 109
    const/4 v8, 0x2

    aput-byte v8, v5, v0

    goto :goto_1

    .line 113
    :cond_2
    rem-int/lit8 v8, p1, 0x8

    if-nez v8, :cond_3

    .line 114
    mul-int/lit8 v8, p1, 0x7

    div-int/lit8 v1, v8, 0x8

    .line 119
    :goto_2
    new-array v6, v1, [B

    .line 121
    const/4 v2, 0x0

    const/4 v0, 0x0

    :goto_3
    if-ge v0, p1, :cond_6

    .line 122
    const/16 v8, 0xb6

    if-lt v2, v8, :cond_4

    .line 158
    .end local v6    # "target":[B
    :goto_4
    return-object v6

    .line 116
    .restart local v6    # "target":[B
    :cond_3
    mul-int/lit8 v8, p1, 0x7

    div-int/lit8 v8, v8, 0x8

    add-int/lit8 v1, v8, 0x1

    goto :goto_2

    .line 127
    :cond_4
    aget-byte v8, v5, v0

    shr-int/2addr v8, v4

    int-to-byte v8, v8

    invoke-static {v8, v4}, Lcom/samsung/commonimsinterface/imscommon/IMSUtility;->correctRightShift(BI)B

    move-result v7

    .line 129
    aput-byte v7, v6, v2

    .line 131
    add-int/lit8 v8, v0, 0x1

    if-ge v8, p1, :cond_5

    .line 133
    aget-byte v8, v6, v2

    add-int/lit8 v9, v0, 0x1

    aget-byte v9, v5, v9

    rsub-int/lit8 v10, v4, 0x7

    shl-int/2addr v9, v10

    int-to-byte v9, v9

    or-int/2addr v8, v9

    int-to-byte v8, v8

    aput-byte v8, v6, v2

    .line 135
    add-int/lit8 v4, v4, 0x1

    .line 137
    const/4 v8, 0x7

    if-ne v4, v8, :cond_5

    .line 138
    const/4 v4, 0x0

    .line 139
    add-int/lit8 v0, v0, 0x1

    .line 121
    :cond_5
    add-int/lit8 v2, v2, 0x1

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 144
    :cond_6
    rem-int/lit8 v8, v1, 0x7

    if-nez v8, :cond_7

    .line 145
    add-int/lit8 v8, v1, -0x1

    aget-byte v8, v6, v8

    shr-int/lit8 v8, v8, 0x1

    int-to-byte v8, v8

    const/4 v9, 0x1

    invoke-static {v8, v9}, Lcom/samsung/commonimsinterface/imscommon/IMSUtility;->correctRightShift(BI)B

    move-result v7

    .line 147
    if-nez v7, :cond_7

    .line 148
    add-int/lit8 v8, v1, -0x1

    aget-byte v9, v6, v8

    or-int/lit8 v9, v9, 0x1a

    int-to-byte v9, v9

    aput-byte v9, v6, v8

    .line 152
    :cond_7
    new-array v3, v1, [B

    .line 154
    invoke-static {v6, v11, v3, v11, v1}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 156
    sget-object v8, Lcom/samsung/commonimsinterface/imscommon/IMSUtility;->LOG_TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "packToGsm7bit End : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-static {v3}, Lcom/samsung/commonimsinterface/imscommon/IMSUtility;->getHex([B)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move-object v6, v3

    .line 158
    goto :goto_4
.end method

.method public static putArrayListToMap(Ljava/lang/String;Ljava/lang/Integer;Ljava/util/HashMap;)V
    .locals 2
    .param p0, "key"    # Ljava/lang/String;
    .param p1, "value"    # Ljava/lang/Integer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 364
    .local p2, "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/util/ArrayList<Ljava/lang/Integer;>;>;"
    const/4 v0, 0x0

    .line 365
    .local v0, "listPerKey":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    invoke-virtual {p2, p0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 366
    invoke-virtual {p2, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "listPerKey":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    check-cast v0, Ljava/util/ArrayList;

    .line 371
    .restart local v0    # "listPerKey":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    :goto_0
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 372
    return-void

    .line 368
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    .end local v0    # "listPerKey":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 369
    .restart local v0    # "listPerKey":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    invoke-virtual {p2, p0, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public static putArrayListToMap(Ljava/lang/String;Ljava/lang/String;Ljava/util/HashMap;)V
    .locals 2
    .param p0, "key"    # Ljava/lang/String;
    .param p1, "value"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 353
    .local p2, "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/util/ArrayList<Ljava/lang/String;>;>;"
    const/4 v0, 0x0

    .line 354
    .local v0, "listPerKey":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {p2, p0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 355
    invoke-virtual {p2, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "listPerKey":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    check-cast v0, Ljava/util/ArrayList;

    .line 360
    .restart local v0    # "listPerKey":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :goto_0
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 361
    return-void

    .line 357
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    .end local v0    # "listPerKey":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 358
    .restart local v0    # "listPerKey":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {p2, p0, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public static putSparseArrayToMap(Ljava/lang/String;ILjava/lang/String;Ljava/util/HashMap;)V
    .locals 2
    .param p0, "mapKey"    # Ljava/lang/String;
    .param p1, "arrKey"    # I
    .param p2, "value"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "Ljava/lang/String;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/String;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 342
    .local p3, "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Landroid/util/SparseArray<Ljava/lang/String;>;>;"
    const/4 v0, 0x0

    .line 343
    .local v0, "listPerKey":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/lang/String;>;"
    invoke-virtual {p3, p0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 344
    invoke-virtual {p3, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "listPerKey":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/lang/String;>;"
    check-cast v0, Landroid/util/SparseArray;

    .line 349
    .restart local v0    # "listPerKey":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/lang/String;>;"
    :goto_0
    invoke-virtual {v0, p1, p2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 350
    return-void

    .line 346
    :cond_0
    new-instance v0, Landroid/util/SparseArray;

    .end local v0    # "listPerKey":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/lang/String;>;"
    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    .line 347
    .restart local v0    # "listPerKey":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/lang/String;>;"
    invoke-virtual {p3, p0, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public static sparseArrayToString(Landroid/util/SparseArray;)Ljava/lang/String;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<*>;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 271
    .local p0, "sparseArray":Landroid/util/SparseArray;, "Landroid/util/SparseArray<*>;"
    if-nez p0, :cond_1

    .line 272
    const-string v2, ""

    .line 278
    :cond_0
    return-object v2

    .line 274
    :cond_1
    const-string v2, ""

    .line 275
    .local v2, "retVal":Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, "i":I
    invoke-virtual {p0}, Landroid/util/SparseArray;->size()I

    move-result v1

    .local v1, "n":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 276
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "{"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0, v0}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "} "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 275
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static unPackToGsm7bit([BI)[B
    .locals 14
    .param p0, "data"    # [B
    .param p1, "inLength"    # I

    .prologue
    const/16 v13, 0x20

    const/4 v12, 0x0

    const/16 v11, 0xd

    .line 162
    const/4 v0, 0x0

    .line 163
    .local v0, "index":I
    const/4 v2, 0x0

    .line 164
    .local v2, "pos":I
    const/4 v4, 0x0

    .line 165
    .local v4, "shift":I
    mul-int/lit8 v8, p1, 0x8

    div-int/lit8 v1, v8, 0x7

    .line 166
    .local v1, "outLength":I
    move-object v5, p0

    .line 167
    .local v5, "source":[B
    new-array v6, v1, [B

    .line 168
    .local v6, "target":[B
    const/4 v3, 0x0

    .line 169
    .local v3, "result":[B
    const/4 v7, 0x0

    .line 171
    .local v7, "temp":B
    sget-object v8, Lcom/samsung/commonimsinterface/imscommon/IMSUtility;->LOG_TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "unPackToGsm7bit Begin : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-static {v5}, Lcom/samsung/commonimsinterface/imscommon/IMSUtility;->getHex([B)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 173
    const/4 v0, 0x0

    :goto_0
    if-ge v2, p1, :cond_1

    .line 174
    aget-byte v8, v5, v2

    shl-int/2addr v8, v4

    and-int/lit8 v8, v8, 0x7f

    int-to-byte v8, v8

    aput-byte v8, v6, v0

    .line 176
    if-eqz v2, :cond_0

    .line 180
    add-int/lit8 v8, v2, -0x1

    aget-byte v8, v5, v8

    rsub-int/lit8 v9, v4, 0x8

    shr-int/2addr v8, v9

    int-to-byte v8, v8

    rsub-int/lit8 v9, v4, 0x8

    invoke-static {v8, v9}, Lcom/samsung/commonimsinterface/imscommon/IMSUtility;->correctRightShift(BI)B

    move-result v7

    .line 182
    aget-byte v8, v6, v0

    or-int/2addr v8, v7

    int-to-byte v8, v8

    aput-byte v8, v6, v0

    .line 185
    aget-byte v8, v6, v0

    if-ne v8, v11, :cond_0

    add-int/lit8 v8, v0, 0x1

    if-ge v8, v1, :cond_0

    .line 186
    aput-byte v13, v6, v0

    .line 191
    :cond_0
    add-int/lit8 v4, v4, 0x1

    .line 193
    const/4 v8, 0x7

    if-ne v4, v8, :cond_3

    .line 194
    const/4 v4, 0x0

    .line 197
    add-int/lit8 v0, v0, 0x1

    .line 199
    aget-byte v8, v5, v2

    shr-int/lit8 v8, v8, 0x1

    int-to-byte v8, v8

    const/4 v9, 0x1

    invoke-static {v8, v9}, Lcom/samsung/commonimsinterface/imscommon/IMSUtility;->correctRightShift(BI)B

    move-result v7

    .line 201
    aput-byte v7, v6, v0

    .line 207
    aget-byte v8, v6, v0

    if-nez v8, :cond_2

    add-int/lit8 v8, v0, 0x1

    if-ne v8, v1, :cond_2

    .line 221
    :cond_1
    :goto_1
    new-array v3, v1, [B

    .line 223
    invoke-static {v6, v12, v3, v12, v1}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 225
    sget-object v8, Lcom/samsung/commonimsinterface/imscommon/IMSUtility;->LOG_TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "unPackToGsm7bit End : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-static {v3}, Lcom/samsung/commonimsinterface/imscommon/IMSUtility;->getHex([B)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 227
    return-object v3

    .line 210
    :cond_2
    aget-byte v8, v6, v0

    if-ne v8, v11, :cond_4

    add-int/lit8 v8, v0, 0x1

    if-ge v8, v1, :cond_4

    .line 211
    aput-byte v13, v6, v0

    .line 173
    :cond_3
    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 213
    :cond_4
    aget-byte v8, v6, v0

    if-ne v8, v11, :cond_3

    add-int/lit8 v8, v0, 0x1

    if-ne v8, v1, :cond_3

    .line 214
    add-int/lit8 v1, v1, -0x1

    .line 215
    sget-object v8, Lcom/samsung/commonimsinterface/imscommon/IMSUtility;->LOG_TAG:Ljava/lang/String;

    const-string v9, "outLength has been cut by 1"

    invoke-static {v8, v9}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method
