.class public Lcom/samsung/commonimsinterface/imscommon/IIMSConstants$GENERAL$FIELD;
.super Ljava/lang/Object;
.source "IIMSConstants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/commonimsinterface/imscommon/IIMSConstants$GENERAL;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "FIELD"
.end annotation


# static fields
.field public static final AMR_AUDIO_BITRATE:I = 0x6

.field public static final AMR_AUDIO_BITRATE_WB:I = 0x7

.field public static final AMR_BANDWITH_EFFICIENT:I = 0x43

.field public static final AMR_OCTET_ALIGNED:I = 0x42

.field public static final AMR_WB:I = 0x37

.field public static final AMR_WB_BANDWITH_EFFICIENT:I = 0x41

.field public static final AMR_WB_OCTET_ALIGNED:I = 0x40

.field public static final AUDIO_RTP_PORT_END:I = 0x3d

.field public static final AUDIO_RTP_PORT_START:I = 0x3c

.field public static final AVAIL_CACHE_EXP:I = 0x20

.field public static final CAP_CACHE_EXP:I = 0x1a

.field public static final CAP_DISCOVERY:I = 0x36

.field public static final CAP_POLL_INTERVAL:I = 0x1b

.field public static final CONF_FACTORY_URI:I = 0x5c

.field public static final CONF_FACTORY_URI_SHOW:I = 0x69

.field public static final DCN_NUMBER:I = 0x2b

.field public static final DM_APP_ID:I = 0x4a

.field public static final DM_CON_REF:I = 0x4c

.field public static final DM_POLLING_PERIOD:I = 0x5a

.field public static final DM_USER_DISP_NAME:I = 0x4b

.field public static final DOMAIN_PUI:I = 0x33

.field public static final DTMF_NB:I = 0x47

.field public static final DTMF_WB:I = 0x46

.field public static final EAB_SETTING:I = 0x1f

.field public static final EAB_SETTING_BY_USER:I = 0x5f

.field public static final EMERGENCY_CONTROL_PREF:I = 0x63

.field public static final FQDN_FOR_PCSCF:I = 0x22

.field public static final GZIP_FLAG:I = 0x26

.field public static final H246_QVGA:I = 0x45

.field public static final H246_VGA:I = 0x44

.field public static final H264_L_VGA:I = 0x6c

.field public static final HD_VOICE:I = 0x3a

.field public static final ICCID:I = 0x5b

.field public static final ICSI:I = 0x4e

.field public static final ICSI_RSC_ALLOC_MODE:I = 0x4f

.field public static final IMS_TEST_MODE:I = 0x29

.field public static final IMS_VOICE_TERMINATION:I = 0x53

.field public static final LBO_PCSCF_ADDRESS:I = 0x4

.field public static final LBO_PCSCF_ADDRESS_TYPE:I = 0x5

.field public static final LVC_BETA_SETTING:I = 0x1e

.field public static final LVC_ENABLED:I = 0x5e

.field public static final LVC_ENABLED_BY_USER:I = 0x61

.field private static final MAX:I = 0x6e

.field public static final MIN_SE:I = 0x2a

.field public static final PCSCF_ADDRESS:I = 0x1

.field public static final PCSCF_DOMAIN:I = 0x0

.field public static final PDP_CONTEXT_PREF:I = 0x4d

.field public static final PHONE_CONTEXT_PARAM:I = 0x56

.field public static final PHONE_CONTEXT_PUID:I = 0x57

.field public static final PHONE_CONTEXT_URI:I = 0x35

.field public static final PIP:I = 0x6b

.field public static final POLL_LIST_SUB_EXP:I = 0x23

.field public static final PREF_CSCF_PORT:I = 0x21

.field public static final PRIVATE_USER_ID:I = 0x2

.field public static final PUBLIC_USER_ID:I = 0x3

.field public static final PUBLISH_ERR_RETRY_TIMER:I = 0x2e

.field public static final PUBLISH_TIMER:I = 0x24

.field public static final PUBLISH_TIMER_EXTEND:I = 0x25

.field public static final REG_RETRY_BASE_TIME:I = 0x54

.field public static final REG_RETRY_MAX_TIME:I = 0x55

.field public static final RINGBACK_TIMER:I = 0x31

.field public static final RINGING_TIMER:I = 0x30

.field public static final RSC_ALLOC_MODE:I = 0x50

.field public static final RTP_RTCP_TIMER:I = 0x32

.field public static final SHOW_REG_INFO_TO_SETTING_APP:I = 0x6d

.field public static final SILENT_REDIAL_ENABLE:I = 0x2c

.field public static final SIP_SESSION_TIMER:I = 0x8

.field public static final SIP_T1_TIMER:I = 0xc

.field public static final SIP_T2_TIMER:I = 0xd

.field public static final SIP_T4_TIMER:I = 0xe

.field public static final SIP_TA_TIMER:I = 0xf

.field public static final SIP_TB_TIMER:I = 0x10

.field public static final SIP_TC_TIMER:I = 0x11

.field public static final SIP_TD_TIMER:I = 0x12

.field public static final SIP_TE_TIMER:I = 0x13

.field public static final SIP_TF_TIMER:I = 0x14

.field public static final SIP_TG_TIMER:I = 0x15

.field public static final SIP_TH_TIMER:I = 0x16

.field public static final SIP_TI_TIMER:I = 0x17

.field public static final SIP_TJ_TIMER:I = 0x18

.field public static final SIP_TK_TIMER:I = 0x19

.field public static final SMS_DOMAIN_UI_SHOW:I = 0x67

.field public static final SMS_FORMAT:I = 0x9

.field public static final SMS_OVER_IMS:I = 0xa

.field public static final SMS_PSI:I = 0x49

.field public static final SMS_WRITE_UICC:I = 0xb

.field public static final SPEAKER_DEFAULT_VIDEO:I = 0x2f

.field public static final SRC_AMR:I = 0x38

.field public static final SRC_AMR_WB:I = 0x39

.field public static final SRC_THROTTLE_PUBLISH:I = 0x1c

.field public static final SS_CONTROL_PREF:I = 0x59

.field public static final SS_DOMAIN_SETTING:I = 0x58

.field public static final SUBSCRIBE_MAX_ENTRY:I = 0x1d

.field public static final TIMER_VZW:I = 0x27

.field public static final TVOLTE_HYS_TIMER:I = 0x6a

.field public static final T_DELAY:I = 0x28

.field public static final T_LTE_911_FAIL:I = 0x2d

.field public static final UDP_KEEP_ALIVE:I = 0x3b

.field public static final URI_MEDIA_RSC_SERV_3WAY_CALL:I = 0x34

.field public static final USSD_CONTROL_PREF:I = 0x62

.field public static final UT_APN_NAME:I = 0x65

.field public static final UT_APN_SETTING_UI_SHOW:I = 0x68

.field public static final UT_PDN:I = 0x64

.field public static final VIDEO_RTP_PORT_END:I = 0x3f

.field public static final VIDEO_RTP_PORT_START:I = 0x3e

.field public static final VOICE_DOMAIN_PREF_EUTRAN:I = 0x51

.field public static final VOICE_DOMAIN_PREF_UTRAN:I = 0x52

.field public static final VOLTE_DOMAIN_UI_SHOW:I = 0x66

.field public static final VOLTE_ENABLED:I = 0x5d

.field public static final VOLTE_ENABLED_BY_USER:I = 0x60

.field public static final VOLTE_PREF_SERVICE_STATUS:I = 0x48


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getMax()I
    .locals 1

    .prologue
    .line 170
    const/16 v0, 0x6e

    return v0
.end method

.method public static values()[I
    .locals 3

    .prologue
    const/16 v2, 0x6e

    .line 174
    new-array v1, v2, [I

    .line 175
    .local v1, "values":[I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v2, :cond_0

    .line 176
    aput v0, v1, v0

    .line 175
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 179
    :cond_0
    return-object v1
.end method
