.class public Lcom/samsung/commonimsinterface/imscommon/IIMSConstants$BEARER_TYPE;
.super Ljava/lang/Object;
.source "IIMSConstants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/commonimsinterface/imscommon/IIMSConstants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "BEARER_TYPE"
.end annotation


# static fields
.field public static final MOBILE:I = 0x1

.field public static final NONE:I = 0x0

.field public static final WIFI:I = 0x2


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 729
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static mapToToken(Ljava/lang/String;)I
    .locals 2
    .param p0, "token"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x1

    .line 735
    const-string v1, "INTERNET_PDN"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 742
    :cond_0
    :goto_0
    return v0

    .line 737
    :cond_1
    const-string v1, "IMS_PDN"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 739
    const-string v0, "WIFI"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 740
    const/4 v0, 0x2

    goto :goto_0

    .line 742
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static toString(I)Ljava/lang/String;
    .locals 1
    .param p0, "bearerType"    # I

    .prologue
    .line 746
    packed-switch p0, :pswitch_data_0

    .line 750
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 747
    :pswitch_0
    const-string v0, "MOBILE"

    goto :goto_0

    .line 748
    :pswitch_1
    const-string v0, "WIFI"

    goto :goto_0

    .line 749
    :pswitch_2
    const-string v0, "MOBILE,WIFI"

    goto :goto_0

    .line 746
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
