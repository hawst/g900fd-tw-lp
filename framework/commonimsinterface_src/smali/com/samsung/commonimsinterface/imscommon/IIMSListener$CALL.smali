.class public Lcom/samsung/commonimsinterface/imscommon/IIMSListener$CALL;
.super Ljava/lang/Object;
.source "IIMSListener.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/commonimsinterface/imscommon/IIMSListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "CALL"
.end annotation


# static fields
.field public static final BASE:I = 0x7d0

.field public static final CALLING:I = 0x7d3

.field public static final CALL_BUSY:I = 0x7d4

.field public static final CALL_ENDED:I = 0x7da

.field public static final CALL_ESTABLISHED:I = 0x7d5

.field public static final CALL_FORWARDED:I = 0x7d9

.field public static final CALL_HELD:I = 0x7d7

.field public static final CALL_RESUMED:I = 0x7d8

.field public static final CALL_SWITCHED:I = 0x7d6

.field public static final CHANGE_REQUEST:I = 0x7db

.field public static final CODEC_BIT_RATE_CHANGE:I = 0x7e6

.field public static final CODEC_DESCRIPTION_CHANGED:I = 0x7e7

.field public static final CONFERENCE_CALL_ESTABLISHED:I = 0x7e1

.field public static final CONFERENCE_CALL_PARTICIPANT_ADDED:I = 0x7e2

.field public static final CONFERENCE_CALL_PARTICIPANT_REMOVED:I = 0x7e3

.field public static final DURING_VIDEOCALL:I = 0x7ec

.field public static final EARLY_MEDIA_START:I = 0x7de

.field public static final EPDG_UNAVAILABLE_REASON:I = 0x7e9

.field public static final EPDG_W2L_HANDOVER_PENDING:I = 0x7eb

.field public static final ERROR:I = 0x7e0

.field public static final MAXCONFERENCECALLUSERS:I = 0x7ea

.field public static final NOTIFY_RECEIVED:I = 0x7dc

.field public static final RINGING:I = 0x7d1

.field public static final RINGING_BACK:I = 0x7d2

.field public static final STOP_ALERT_TONE:I = 0x7df

.field public static final TRYING:I = 0x7e8

.field public static final TTY_TEXT_REQUEST:I = 0x7dd

.field public static final USSD_INDICATION:I = 0x7e5

.field public static final USSD_RESPONSE:I = 0x7e4


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 295
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
