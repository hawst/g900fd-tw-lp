.class public Lcom/samsung/commonimsinterface/imscommon/IMSProfileParams;
.super Ljava/lang/Object;
.source "IMSProfileParams.java"

# interfaces
.implements Landroid/os/Parcelable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/samsung/commonimsinterface/imscommon/IMSProfileParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mAuthUserName:Ljava/lang/String;

.field private mAutoRegistration:Z

.field private mAvailable:I

.field private transient mCallingUid:I

.field private transient mCapabilities:Landroid/os/Bundle;

.field private mDomain:Ljava/lang/String;

.field private mPassword:Ljava/lang/String;

.field private mPort:I

.field private mProfileName:Ljava/lang/String;

.field private mProtocol:Ljava/lang/String;

.field private mProxyAddress:Ljava/lang/String;

.field private mSendKeepAlive:Z

.field private mUriString:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 93
    new-instance v0, Lcom/samsung/commonimsinterface/imscommon/IMSProfileParams$1;

    invoke-direct {v0}, Lcom/samsung/commonimsinterface/imscommon/IMSProfileParams$1;-><init>()V

    sput-object v0, Lcom/samsung/commonimsinterface/imscommon/IMSProfileParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 3
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput-boolean v2, p0, Lcom/samsung/commonimsinterface/imscommon/IMSProfileParams;->mSendKeepAlive:Z

    .line 16
    iput-boolean v1, p0, Lcom/samsung/commonimsinterface/imscommon/IMSProfileParams;->mAutoRegistration:Z

    .line 17
    iput v2, p0, Lcom/samsung/commonimsinterface/imscommon/IMSProfileParams;->mCallingUid:I

    .line 50
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSProfileParams;->mProxyAddress:Ljava/lang/String;

    .line 51
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSProfileParams;->mPassword:Ljava/lang/String;

    .line 52
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSProfileParams;->mDomain:Ljava/lang/String;

    .line 53
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSProfileParams;->mProtocol:Ljava/lang/String;

    .line 54
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSProfileParams;->mProfileName:Ljava/lang/String;

    .line 55
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSProfileParams;->mAuthUserName:Ljava/lang/String;

    .line 56
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSProfileParams;->mPort:I

    .line 57
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSProfileParams;->mSendKeepAlive:Z

    .line 58
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_1

    :goto_1
    iput-boolean v1, p0, Lcom/samsung/commonimsinterface/imscommon/IMSProfileParams;->mAutoRegistration:Z

    .line 59
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSProfileParams;->mAvailable:I

    .line 60
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSProfileParams;->mUriString:Ljava/lang/String;

    .line 61
    invoke-virtual {p1}, Landroid/os/Parcel;->readBundle()Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSProfileParams;->mCapabilities:Landroid/os/Bundle;

    .line 62
    return-void

    :cond_0
    move v0, v2

    .line 57
    goto :goto_0

    :cond_1
    move v1, v2

    .line 58
    goto :goto_1
.end method

.method public constructor <init>(Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;)V
    .locals 2
    .param p1, "profile"    # Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;

    .prologue
    const/4 v1, 0x0

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput-boolean v1, p0, Lcom/samsung/commonimsinterface/imscommon/IMSProfileParams;->mSendKeepAlive:Z

    .line 16
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSProfileParams;->mAutoRegistration:Z

    .line 17
    iput v1, p0, Lcom/samsung/commonimsinterface/imscommon/IMSProfileParams;->mCallingUid:I

    .line 35
    invoke-virtual {p1}, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->getProxyAddress()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSProfileParams;->mProxyAddress:Ljava/lang/String;

    .line 36
    invoke-virtual {p1}, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->getPassword()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSProfileParams;->mPassword:Ljava/lang/String;

    .line 37
    invoke-virtual {p1}, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->getSipDomain()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSProfileParams;->mDomain:Ljava/lang/String;

    .line 38
    invoke-virtual {p1}, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->getProtocol()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSProfileParams;->mProtocol:Ljava/lang/String;

    .line 39
    invoke-virtual {p1}, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->getProfileName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSProfileParams;->mProfileName:Ljava/lang/String;

    .line 40
    invoke-virtual {p1}, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->getAuthUserName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSProfileParams;->mAuthUserName:Ljava/lang/String;

    .line 41
    invoke-virtual {p1}, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->getPort()I

    move-result v0

    iput v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSProfileParams;->mPort:I

    .line 42
    invoke-virtual {p1}, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->getSendKeepAlive()Z

    move-result v0

    iput-boolean v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSProfileParams;->mSendKeepAlive:Z

    .line 43
    invoke-virtual {p1}, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->getAutoRegistration()Z

    move-result v0

    iput-boolean v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSProfileParams;->mAutoRegistration:Z

    .line 44
    invoke-virtual {p1}, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->getCallingUid()I

    move-result v0

    iput v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSProfileParams;->mCallingUid:I

    .line 45
    invoke-virtual {p1}, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->getCapabilities()Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSProfileParams;->mCapabilities:Landroid/os/Bundle;

    .line 46
    invoke-virtual {p1}, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->getAvailability()I

    move-result v0

    iput v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSProfileParams;->mAvailable:I

    .line 47
    invoke-virtual {p1}, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->getUriString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSProfileParams;->mUriString:Ljava/lang/String;

    .line 48
    return-void
.end method


# virtual methods
.method public clone()Lcom/samsung/commonimsinterface/imscommon/IMSProfileParams;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 66
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/commonimsinterface/imscommon/IMSProfileParams;

    .line 68
    .local v0, "clone":Lcom/samsung/commonimsinterface/imscommon/IMSProfileParams;
    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 7
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/IMSProfileParams;->clone()Lcom/samsung/commonimsinterface/imscommon/IMSProfileParams;

    move-result-object v0

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 73
    const/4 v0, 0x0

    return v0
.end method

.method public getAuthUserName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 161
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSProfileParams;->mAuthUserName:Ljava/lang/String;

    return-object v0
.end method

.method public getAutoRegistration()Z
    .locals 1

    .prologue
    .line 233
    iget-boolean v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSProfileParams;->mAutoRegistration:Z

    return v0
.end method

.method public getAvailability()I
    .locals 1

    .prologue
    .line 249
    iget v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSProfileParams;->mAvailable:I

    return v0
.end method

.method public getCallingUid()I
    .locals 1

    .prologue
    .line 241
    iget v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSProfileParams;->mCallingUid:I

    return v0
.end method

.method public getCapabilities()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 245
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSProfileParams;->mCapabilities:Landroid/os/Bundle;

    return-object v0
.end method

.method public getDisplayName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSProfileParams;->mProfileName:Ljava/lang/String;

    return-object v0
.end method

.method public getMDN()Ljava/lang/String;
    .locals 1

    .prologue
    .line 253
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSProfileParams;->mProfileName:Ljava/lang/String;

    return-object v0
.end method

.method public getPassword()Ljava/lang/String;
    .locals 1

    .prologue
    .line 170
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSProfileParams;->mPassword:Ljava/lang/String;

    return-object v0
.end method

.method public getPort()I
    .locals 1

    .prologue
    .line 188
    iget v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSProfileParams;->mPort:I

    return v0
.end method

.method public getProfileName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 215
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSProfileParams;->mProfileName:Ljava/lang/String;

    return-object v0
.end method

.method public getProtocol()Ljava/lang/String;
    .locals 1

    .prologue
    .line 197
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSProfileParams;->mProtocol:Ljava/lang/String;

    return-object v0
.end method

.method public getProxyAddress()Ljava/lang/String;
    .locals 1

    .prologue
    .line 206
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSProfileParams;->mProxyAddress:Ljava/lang/String;

    return-object v0
.end method

.method public getSendKeepAlive()Z
    .locals 1

    .prologue
    .line 224
    iget-boolean v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSProfileParams;->mSendKeepAlive:Z

    return v0
.end method

.method public getSipDomain()Ljava/lang/String;
    .locals 1

    .prologue
    .line 179
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSProfileParams;->mDomain:Ljava/lang/String;

    return-object v0
.end method

.method public getUriString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSProfileParams;->mUriString:Ljava/lang/String;

    return-object v0
.end method

.method public getUserName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSProfileParams;->mProfileName:Ljava/lang/String;

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flag"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 78
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSProfileParams;->mProxyAddress:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 79
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSProfileParams;->mPassword:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 80
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSProfileParams;->mDomain:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 81
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSProfileParams;->mProtocol:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 82
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSProfileParams;->mProfileName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 83
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSProfileParams;->mAuthUserName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 84
    iget v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSProfileParams;->mPort:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 85
    iget-boolean v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSProfileParams;->mSendKeepAlive:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 86
    iget-boolean v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSProfileParams;->mAutoRegistration:Z

    if-eqz v0, :cond_1

    :goto_1
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 87
    iget v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSProfileParams;->mAvailable:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 88
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSProfileParams;->mUriString:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 89
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSProfileParams;->mCapabilities:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    .line 90
    return-void

    :cond_0
    move v0, v2

    .line 85
    goto :goto_0

    :cond_1
    move v1, v2

    .line 86
    goto :goto_1
.end method
