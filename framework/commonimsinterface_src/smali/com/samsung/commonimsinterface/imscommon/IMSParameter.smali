.class public Lcom/samsung/commonimsinterface/imscommon/IMSParameter;
.super Ljava/lang/Object;
.source "IMSParameter.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/commonimsinterface/imscommon/IMSParameter$PRESENCE;,
        Lcom/samsung/commonimsinterface/imscommon/IMSParameter$SSCONFIG;,
        Lcom/samsung/commonimsinterface/imscommon/IMSParameter$SMS;,
        Lcom/samsung/commonimsinterface/imscommon/IMSParameter$MEDIA;,
        Lcom/samsung/commonimsinterface/imscommon/IMSParameter$CALL;,
        Lcom/samsung/commonimsinterface/imscommon/IMSParameter$GENERAL;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/samsung/commonimsinterface/imscommon/IMSParameter;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mAPCSInfo:Lcom/samsung/commonimsinterface/imscommon/IMSAPCSInfo;

.field private mPrimitiveMap:Landroid/os/Bundle;

.field private mProfileParams:[Lcom/samsung/commonimsinterface/imscommon/IMSProfileParams;

.field private mRegistrationInfo:Lcom/samsung/commonimsinterface/imscommon/IMSRegistrationInfo;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 496
    new-instance v0, Lcom/samsung/commonimsinterface/imscommon/IMSParameter$1;

    invoke-direct {v0}, Lcom/samsung/commonimsinterface/imscommon/IMSParameter$1;-><init>()V

    sput-object v0, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 130
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;->mPrimitiveMap:Landroid/os/Bundle;

    .line 15
    iput-object v1, p0, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;->mAPCSInfo:Lcom/samsung/commonimsinterface/imscommon/IMSAPCSInfo;

    .line 16
    iput-object v1, p0, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;->mRegistrationInfo:Lcom/samsung/commonimsinterface/imscommon/IMSRegistrationInfo;

    .line 17
    iput-object v1, p0, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;->mProfileParams:[Lcom/samsung/commonimsinterface/imscommon/IMSProfileParams;

    .line 132
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 4
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    const/4 v3, 0x0

    .line 135
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    iput-object v2, p0, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;->mPrimitiveMap:Landroid/os/Bundle;

    .line 15
    iput-object v3, p0, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;->mAPCSInfo:Lcom/samsung/commonimsinterface/imscommon/IMSAPCSInfo;

    .line 16
    iput-object v3, p0, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;->mRegistrationInfo:Lcom/samsung/commonimsinterface/imscommon/IMSRegistrationInfo;

    .line 17
    iput-object v3, p0, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;->mProfileParams:[Lcom/samsung/commonimsinterface/imscommon/IMSProfileParams;

    .line 136
    invoke-virtual {p1}, Landroid/os/Parcel;->readBundle()Landroid/os/Bundle;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;->mPrimitiveMap:Landroid/os/Bundle;

    .line 138
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_0

    .line 139
    const-class v2, Lcom/samsung/commonimsinterface/imscommon/IMSAPCSInfo;

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/samsung/commonimsinterface/imscommon/IMSAPCSInfo;

    iput-object v2, p0, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;->mAPCSInfo:Lcom/samsung/commonimsinterface/imscommon/IMSAPCSInfo;

    .line 142
    :cond_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_1

    .line 143
    const-class v2, Lcom/samsung/commonimsinterface/imscommon/IMSRegistrationInfo;

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/samsung/commonimsinterface/imscommon/IMSRegistrationInfo;

    iput-object v2, p0, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;->mRegistrationInfo:Lcom/samsung/commonimsinterface/imscommon/IMSRegistrationInfo;

    .line 146
    :cond_1
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 147
    .local v1, "size":I
    if-lez v1, :cond_2

    .line 148
    new-array v2, v1, [Lcom/samsung/commonimsinterface/imscommon/IMSProfileParams;

    iput-object v2, p0, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;->mProfileParams:[Lcom/samsung/commonimsinterface/imscommon/IMSProfileParams;

    .line 150
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_2

    .line 151
    iget-object v3, p0, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;->mProfileParams:[Lcom/samsung/commonimsinterface/imscommon/IMSProfileParams;

    const-class v2, Lcom/samsung/commonimsinterface/imscommon/IMSProfileParams;

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/samsung/commonimsinterface/imscommon/IMSProfileParams;

    aput-object v2, v3, v0

    .line 150
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 154
    .end local v0    # "i":I
    :cond_2
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 463
    const/4 v0, 0x0

    return v0
.end method

.method public getBoolean(Ljava/lang/String;)Z
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 301
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public getBoolean(Ljava/lang/String;Z)Z
    .locals 3
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "defaultValue"    # Z

    .prologue
    .line 306
    iget-object v2, p0, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;->mPrimitiveMap:Landroid/os/Bundle;

    invoke-virtual {v2, p1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 308
    .local v1, "o":Ljava/lang/Object;
    if-nez v1, :cond_0

    .line 314
    .end local v1    # "o":Ljava/lang/Object;
    .end local p2    # "defaultValue":Z
    :goto_0
    return p2

    .line 312
    .restart local v1    # "o":Ljava/lang/Object;
    .restart local p2    # "defaultValue":Z
    :cond_0
    :try_start_0
    check-cast v1, Ljava/lang/Boolean;

    .end local v1    # "o":Ljava/lang/Object;
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    move-result p2

    goto :goto_0

    .line 313
    :catch_0
    move-exception v0

    .line 314
    .local v0, "e":Ljava/lang/ClassCastException;
    goto :goto_0
.end method

.method public getBundle(Ljava/lang/String;)Landroid/os/Bundle;
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 457
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;->mPrimitiveMap:Landroid/os/Bundle;

    invoke-virtual {v0, p1}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method public getByteArray(Ljava/lang/String;)[B
    .locals 4
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 263
    iget-object v3, p0, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;->mPrimitiveMap:Landroid/os/Bundle;

    invoke-virtual {v3, p1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 265
    .local v1, "o":Ljava/lang/Object;
    if-nez v1, :cond_0

    move-object v1, v2

    .line 271
    .end local v1    # "o":Ljava/lang/Object;
    :goto_0
    return-object v1

    .line 269
    .restart local v1    # "o":Ljava/lang/Object;
    :cond_0
    :try_start_0
    check-cast v1, [B

    .end local v1    # "o":Ljava/lang/Object;
    check-cast v1, [B
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 270
    :catch_0
    move-exception v0

    .local v0, "e":Ljava/lang/ClassCastException;
    move-object v1, v2

    .line 271
    goto :goto_0
.end method

.method public getInt(Ljava/lang/String;)I
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 163
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public getInt(Ljava/lang/String;I)I
    .locals 3
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "defaultValue"    # I

    .prologue
    .line 168
    iget-object v2, p0, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;->mPrimitiveMap:Landroid/os/Bundle;

    invoke-virtual {v2, p1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 170
    .local v1, "o":Ljava/lang/Object;
    if-nez v1, :cond_0

    .line 176
    .end local v1    # "o":Ljava/lang/Object;
    .end local p2    # "defaultValue":I
    :goto_0
    return p2

    .line 174
    .restart local v1    # "o":Ljava/lang/Object;
    .restart local p2    # "defaultValue":I
    :cond_0
    :try_start_0
    check-cast v1, Ljava/lang/Integer;

    .end local v1    # "o":Ljava/lang/Object;
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    move-result p2

    goto :goto_0

    .line 175
    :catch_0
    move-exception v0

    .line 176
    .local v0, "e":Ljava/lang/ClassCastException;
    goto :goto_0
.end method

.method public getIntArray(Ljava/lang/String;)[I
    .locals 4
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 282
    iget-object v3, p0, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;->mPrimitiveMap:Landroid/os/Bundle;

    invoke-virtual {v3, p1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 284
    .local v1, "o":Ljava/lang/Object;
    if-nez v1, :cond_0

    move-object v1, v2

    .line 290
    .end local v1    # "o":Ljava/lang/Object;
    :goto_0
    return-object v1

    .line 288
    .restart local v1    # "o":Ljava/lang/Object;
    :cond_0
    :try_start_0
    check-cast v1, [I

    .end local v1    # "o":Ljava/lang/Object;
    check-cast v1, [I
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 289
    :catch_0
    move-exception v0

    .local v0, "e":Ljava/lang/ClassCastException;
    move-object v1, v2

    .line 290
    goto :goto_0
.end method

.method public getIntegerArrayList(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 2
    .param p1, "key"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 427
    invoke-virtual {p0, p1}, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;->getParcelable(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 428
    .local v0, "bundle":Landroid/os/Bundle;
    if-nez v0, :cond_0

    .line 429
    const/4 v1, 0x0

    .line 431
    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0, p1}, Landroid/os/Bundle;->getIntegerArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    goto :goto_0
.end method

.method public getLong(Ljava/lang/String;)J
    .locals 2
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 187
    const-wide/16 v0, 0x0

    invoke-virtual {p0, p1, v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public getLong(Ljava/lang/String;J)J
    .locals 4
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "defaultValue"    # J

    .prologue
    .line 192
    iget-object v2, p0, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;->mPrimitiveMap:Landroid/os/Bundle;

    invoke-virtual {v2, p1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 194
    .local v1, "o":Ljava/lang/Object;
    if-nez v1, :cond_0

    .line 200
    .end local v1    # "o":Ljava/lang/Object;
    .end local p2    # "defaultValue":J
    :goto_0
    return-wide p2

    .line 198
    .restart local v1    # "o":Ljava/lang/Object;
    .restart local p2    # "defaultValue":J
    :cond_0
    :try_start_0
    check-cast v1, Ljava/lang/Long;

    .end local v1    # "o":Ljava/lang/Object;
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide p2

    goto :goto_0

    .line 199
    :catch_0
    move-exception v0

    .line 200
    .local v0, "e":Ljava/lang/ClassCastException;
    goto :goto_0
.end method

.method public getParcelable(Ljava/lang/String;)Ljava/lang/Object;
    .locals 2
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 339
    const-string v1, "apcsinfo"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 340
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;->mAPCSInfo:Lcom/samsung/commonimsinterface/imscommon/IMSAPCSInfo;

    .line 351
    :cond_0
    :goto_0
    return-object v0

    .line 341
    :cond_1
    const-string v1, "registrationinfo"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 342
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;->mRegistrationInfo:Lcom/samsung/commonimsinterface/imscommon/IMSRegistrationInfo;

    goto :goto_0

    .line 345
    :cond_2
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;->mPrimitiveMap:Landroid/os/Bundle;

    invoke-virtual {v1, p1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 347
    .local v0, "o":Ljava/lang/Object;
    if-nez v0, :cond_0

    .line 348
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getParcelableArray(Ljava/lang/String;)[Ljava/lang/Object;
    .locals 4
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 375
    const-string v3, "profileparams"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 376
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;->mProfileParams:[Lcom/samsung/commonimsinterface/imscommon/IMSProfileParams;

    .line 386
    :goto_0
    return-object v1

    .line 378
    :cond_0
    iget-object v3, p0, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;->mPrimitiveMap:Landroid/os/Bundle;

    invoke-virtual {v3, p1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 380
    .local v1, "o":Ljava/lang/Object;
    if-nez v1, :cond_1

    move-object v1, v2

    .line 381
    goto :goto_0

    .line 384
    :cond_1
    :try_start_0
    check-cast v1, [Ljava/lang/Object;

    .end local v1    # "o":Ljava/lang/Object;
    check-cast v1, [Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 385
    :catch_0
    move-exception v0

    .local v0, "e":Ljava/lang/ClassCastException;
    move-object v1, v2

    .line 386
    goto :goto_0
.end method

.method public getSparseStringArray(Ljava/lang/String;)Landroid/util/SparseArray;
    .locals 8
    .param p1, "key"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 401
    invoke-virtual {p0, p1}, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;->getParcelable(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 402
    .local v0, "bundle":Landroid/os/Bundle;
    if-nez v0, :cond_1

    move-object v4, v5

    .line 415
    :cond_0
    :goto_0
    return-object v4

    .line 406
    :cond_1
    new-instance v4, Landroid/util/SparseArray;

    invoke-direct {v4}, Landroid/util/SparseArray;-><init>()V

    .line 407
    .local v4, "value":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/lang/String;>;"
    invoke-virtual {v0}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 409
    .local v1, "bundleKey":Ljava/lang/String;
    :try_start_0
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 410
    :catch_0
    move-exception v2

    .local v2, "e":Ljava/lang/NumberFormatException;
    move-object v4, v5

    .line 411
    goto :goto_0
.end method

.method public getString(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 211
    iget-object v3, p0, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;->mPrimitiveMap:Landroid/os/Bundle;

    invoke-virtual {v3, p1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 213
    .local v1, "o":Ljava/lang/Object;
    if-nez v1, :cond_0

    move-object v1, v2

    .line 219
    .end local v1    # "o":Ljava/lang/Object;
    :goto_0
    return-object v1

    .line 217
    .restart local v1    # "o":Ljava/lang/Object;
    :cond_0
    :try_start_0
    check-cast v1, Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 218
    :catch_0
    move-exception v0

    .local v0, "e":Ljava/lang/ClassCastException;
    move-object v1, v2

    .line 219
    goto :goto_0
.end method

.method public getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "defaultValue"    # Ljava/lang/String;

    .prologue
    .line 225
    iget-object v2, p0, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;->mPrimitiveMap:Landroid/os/Bundle;

    invoke-virtual {v2, p1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 227
    .local v1, "o":Ljava/lang/Object;
    if-nez v1, :cond_0

    .line 233
    .end local v1    # "o":Ljava/lang/Object;
    .end local p2    # "defaultValue":Ljava/lang/String;
    :goto_0
    return-object p2

    .line 231
    .restart local v1    # "o":Ljava/lang/Object;
    .restart local p2    # "defaultValue":Ljava/lang/String;
    :cond_0
    :try_start_0
    check-cast v1, Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .end local v1    # "o":Ljava/lang/Object;
    move-object p2, v1

    goto :goto_0

    .line 232
    .restart local v1    # "o":Ljava/lang/Object;
    :catch_0
    move-exception v0

    .line 233
    .local v0, "e":Ljava/lang/ClassCastException;
    goto :goto_0
.end method

.method public getStringArray(Ljava/lang/String;)[Ljava/lang/String;
    .locals 4
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 244
    iget-object v3, p0, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;->mPrimitiveMap:Landroid/os/Bundle;

    invoke-virtual {v3, p1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 246
    .local v1, "o":Ljava/lang/Object;
    if-nez v1, :cond_0

    move-object v1, v2

    .line 252
    .end local v1    # "o":Ljava/lang/Object;
    :goto_0
    return-object v1

    .line 250
    .restart local v1    # "o":Ljava/lang/Object;
    :cond_0
    :try_start_0
    check-cast v1, [Ljava/lang/String;

    .end local v1    # "o":Ljava/lang/Object;
    check-cast v1, [Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 251
    :catch_0
    move-exception v0

    .local v0, "e":Ljava/lang/ClassCastException;
    move-object v1, v2

    .line 252
    goto :goto_0
.end method

.method public getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 2
    .param p1, "key"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 443
    invoke-virtual {p0, p1}, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;->getParcelable(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 444
    .local v0, "bundle":Landroid/os/Bundle;
    if-nez v0, :cond_0

    .line 445
    const/4 v1, 0x0

    .line 447
    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0, p1}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    goto :goto_0
.end method

.method public putBoolean(Ljava/lang/String;Z)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Z

    .prologue
    .line 296
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;->mPrimitiveMap:Landroid/os/Bundle;

    invoke-virtual {v0, p1, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 297
    return-void
.end method

.method public putBundle(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "inBundle"    # Landroid/os/Bundle;

    .prologue
    .line 452
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;->mPrimitiveMap:Landroid/os/Bundle;

    invoke-virtual {v0, p1, p2}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 453
    return-void
.end method

.method public putByteArray(Ljava/lang/String;[B)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # [B

    .prologue
    .line 258
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;->mPrimitiveMap:Landroid/os/Bundle;

    invoke-virtual {v0, p1, p2}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 259
    return-void
.end method

.method public putInt(Ljava/lang/String;I)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # I

    .prologue
    .line 158
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;->mPrimitiveMap:Landroid/os/Bundle;

    invoke-virtual {v0, p1, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 159
    return-void
.end method

.method public putIntArray(Ljava/lang/String;[I)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # [I

    .prologue
    .line 277
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;->mPrimitiveMap:Landroid/os/Bundle;

    invoke-virtual {v0, p1, p2}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    .line 278
    return-void
.end method

.method public putIntegerArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 420
    .local p2, "value":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 421
    .local v0, "bundle":Landroid/os/Bundle;
    invoke-virtual {v0, p1, p2}, Landroid/os/Bundle;->putIntegerArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 422
    invoke-virtual {p0, p1, v0}, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 423
    return-void
.end method

.method public putLong(Ljava/lang/String;J)V
    .locals 2
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # J

    .prologue
    .line 182
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;->mPrimitiveMap:Landroid/os/Bundle;

    invoke-virtual {v0, p1, p2, p3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 183
    return-void
.end method

.method public putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V
    .locals 2
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Landroid/os/Parcelable;

    .prologue
    .line 320
    const-string v1, "apcsinfo"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    instance-of v1, p2, Lcom/samsung/commonimsinterface/imscommon/IMSAPCSInfo;

    if-eqz v1, :cond_0

    .line 322
    :try_start_0
    check-cast p2, Lcom/samsung/commonimsinterface/imscommon/IMSAPCSInfo;

    .end local p2    # "value":Landroid/os/Parcelable;
    invoke-virtual {p2}, Lcom/samsung/commonimsinterface/imscommon/IMSAPCSInfo;->clone()Lcom/samsung/commonimsinterface/imscommon/IMSAPCSInfo;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;->mAPCSInfo:Lcom/samsung/commonimsinterface/imscommon/IMSAPCSInfo;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 335
    :goto_0
    return-void

    .line 323
    :catch_0
    move-exception v0

    .line 324
    .local v0, "e":Ljava/lang/CloneNotSupportedException;
    invoke-virtual {v0}, Ljava/lang/CloneNotSupportedException;->printStackTrace()V

    goto :goto_0

    .line 326
    .end local v0    # "e":Ljava/lang/CloneNotSupportedException;
    .restart local p2    # "value":Landroid/os/Parcelable;
    :cond_0
    const-string v1, "registrationinfo"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    instance-of v1, p2, Lcom/samsung/commonimsinterface/imscommon/IMSRegistrationInfo;

    if-eqz v1, :cond_1

    .line 328
    :try_start_1
    check-cast p2, Lcom/samsung/commonimsinterface/imscommon/IMSRegistrationInfo;

    .end local p2    # "value":Landroid/os/Parcelable;
    invoke-virtual {p2}, Lcom/samsung/commonimsinterface/imscommon/IMSRegistrationInfo;->clone()Lcom/samsung/commonimsinterface/imscommon/IMSRegistrationInfo;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;->mRegistrationInfo:Lcom/samsung/commonimsinterface/imscommon/IMSRegistrationInfo;
    :try_end_1
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 329
    :catch_1
    move-exception v0

    .line 330
    .restart local v0    # "e":Ljava/lang/CloneNotSupportedException;
    invoke-virtual {v0}, Ljava/lang/CloneNotSupportedException;->printStackTrace()V

    goto :goto_0

    .line 333
    .end local v0    # "e":Ljava/lang/CloneNotSupportedException;
    .restart local p2    # "value":Landroid/os/Parcelable;
    :cond_1
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;->mPrimitiveMap:Landroid/os/Bundle;

    invoke-virtual {v1, p1, p2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    goto :goto_0
.end method

.method public putParcelableArray(Ljava/lang/String;[Landroid/os/Parcelable;)V
    .locals 4
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # [Landroid/os/Parcelable;

    .prologue
    .line 359
    const-string v2, "profileparams"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    instance-of v2, p2, [Lcom/samsung/commonimsinterface/imscommon/IMSProfileParams;

    if-eqz v2, :cond_1

    .line 361
    :try_start_0
    array-length v2, p2

    new-array v2, v2, [Lcom/samsung/commonimsinterface/imscommon/IMSProfileParams;

    iput-object v2, p0, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;->mProfileParams:[Lcom/samsung/commonimsinterface/imscommon/IMSProfileParams;

    .line 362
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v2, p2

    if-ge v1, v2, :cond_0

    .line 363
    iget-object v3, p0, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;->mProfileParams:[Lcom/samsung/commonimsinterface/imscommon/IMSProfileParams;

    aget-object v2, p2, v1

    check-cast v2, Lcom/samsung/commonimsinterface/imscommon/IMSProfileParams;

    invoke-virtual {v2}, Lcom/samsung/commonimsinterface/imscommon/IMSProfileParams;->clone()Lcom/samsung/commonimsinterface/imscommon/IMSProfileParams;

    move-result-object v2

    aput-object v2, v3, v1
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 362
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 365
    .end local v1    # "i":I
    :catch_0
    move-exception v0

    .line 366
    .local v0, "e":Ljava/lang/CloneNotSupportedException;
    invoke-virtual {v0}, Ljava/lang/CloneNotSupportedException;->printStackTrace()V

    .line 371
    .end local v0    # "e":Ljava/lang/CloneNotSupportedException;
    :cond_0
    :goto_1
    return-void

    .line 369
    :cond_1
    iget-object v2, p0, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;->mPrimitiveMap:Landroid/os/Bundle;

    invoke-virtual {v2, p1, p2}, Landroid/os/Bundle;->putParcelableArray(Ljava/lang/String;[Landroid/os/Parcelable;)V

    goto :goto_1
.end method

.method public putSparseStringArray(Ljava/lang/String;Landroid/util/SparseArray;)V
    .locals 5
    .param p1, "key"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 392
    .local p2, "value":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/lang/String;>;"
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 393
    .local v0, "bundle":Landroid/os/Bundle;
    const/4 v1, 0x0

    .local v1, "i":I
    invoke-virtual {p2}, Landroid/util/SparseArray;->size()I

    move-result v2

    .local v2, "n":I
    :goto_0
    if-ge v1, v2, :cond_0

    .line 394
    invoke-virtual {p2, v1}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v4, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 393
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 396
    :cond_0
    invoke-virtual {p0, p1, v0}, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 397
    return-void
.end method

.method public putString(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 206
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;->mPrimitiveMap:Landroid/os/Bundle;

    invoke-virtual {v0, p1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 207
    return-void
.end method

.method public putStringArray(Ljava/lang/String;[Ljava/lang/String;)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # [Ljava/lang/String;

    .prologue
    .line 239
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;->mPrimitiveMap:Landroid/os/Bundle;

    invoke-virtual {v0, p1, p2}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 240
    return-void
.end method

.method public putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 436
    .local p2, "value":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 437
    .local v0, "bundle":Landroid/os/Bundle;
    invoke-virtual {v0, p1, p2}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 438
    invoke-virtual {p0, p1, v0}, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 439
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 7
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 469
    iget-object v4, p0, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;->mPrimitiveMap:Landroid/os/Bundle;

    invoke-virtual {p1, v4}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    .line 471
    iget-object v4, p0, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;->mAPCSInfo:Lcom/samsung/commonimsinterface/imscommon/IMSAPCSInfo;

    if-nez v4, :cond_1

    .line 472
    invoke-virtual {p1, v5}, Landroid/os/Parcel;->writeInt(I)V

    .line 478
    :goto_0
    iget-object v4, p0, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;->mRegistrationInfo:Lcom/samsung/commonimsinterface/imscommon/IMSRegistrationInfo;

    if-nez v4, :cond_2

    .line 479
    invoke-virtual {p1, v5}, Landroid/os/Parcel;->writeInt(I)V

    .line 485
    :goto_1
    iget-object v4, p0, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;->mProfileParams:[Lcom/samsung/commonimsinterface/imscommon/IMSProfileParams;

    if-nez v4, :cond_3

    .line 486
    invoke-virtual {p1, v5}, Landroid/os/Parcel;->writeInt(I)V

    .line 493
    :cond_0
    return-void

    .line 474
    :cond_1
    invoke-virtual {p1, v6}, Landroid/os/Parcel;->writeInt(I)V

    .line 475
    iget-object v4, p0, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;->mAPCSInfo:Lcom/samsung/commonimsinterface/imscommon/IMSAPCSInfo;

    invoke-virtual {p1, v4, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    goto :goto_0

    .line 481
    :cond_2
    invoke-virtual {p1, v6}, Landroid/os/Parcel;->writeInt(I)V

    .line 482
    iget-object v4, p0, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;->mRegistrationInfo:Lcom/samsung/commonimsinterface/imscommon/IMSRegistrationInfo;

    invoke-virtual {p1, v4, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    goto :goto_1

    .line 488
    :cond_3
    iget-object v4, p0, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;->mProfileParams:[Lcom/samsung/commonimsinterface/imscommon/IMSProfileParams;

    array-length v4, v4

    invoke-virtual {p1, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 489
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;->mProfileParams:[Lcom/samsung/commonimsinterface/imscommon/IMSProfileParams;

    .local v0, "arr$":[Lcom/samsung/commonimsinterface/imscommon/IMSProfileParams;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_2
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 490
    .local v3, "profile":Lcom/samsung/commonimsinterface/imscommon/IMSProfileParams;
    invoke-virtual {p1, v3, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 489
    add-int/lit8 v1, v1, 0x1

    goto :goto_2
.end method
