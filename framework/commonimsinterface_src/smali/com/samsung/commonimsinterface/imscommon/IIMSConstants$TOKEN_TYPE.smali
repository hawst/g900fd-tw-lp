.class public Lcom/samsung/commonimsinterface/imscommon/IIMSConstants$TOKEN_TYPE;
.super Ljava/lang/Object;
.source "IIMSConstants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/commonimsinterface/imscommon/IIMSConstants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "TOKEN_TYPE"
.end annotation


# static fields
.field public static final CALL:I = 0x0

.field public static final GENERAL:I = 0x7

.field public static final MAX:I = 0x8

.field public static final MEDIA:I = 0x4

.field public static final PRESENCE_EAB:I = 0x1

.field public static final PRESENCE_TMO:I = 0x3

.field public static final PRESENCE_UCE:I = 0x2

.field public static final SMS:I = 0x5

.field public static final SSCONFIG:I = 0x6


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 756
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static toString(I)Ljava/lang/String;
    .locals 1
    .param p0, "token"    # I

    .prologue
    .line 768
    packed-switch p0, :pswitch_data_0

    .line 777
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 769
    :pswitch_0
    const-string v0, "TOKEN_SERVICE_GENERAL"

    goto :goto_0

    .line 770
    :pswitch_1
    const-string v0, "TOKEN_SERVICE_CALL"

    goto :goto_0

    .line 771
    :pswitch_2
    const-string v0, "TOKEN_SERVICE_PRESENCE_EAB"

    goto :goto_0

    .line 772
    :pswitch_3
    const-string v0, "TOKEN_SERVICE_PRESENCE_UCE"

    goto :goto_0

    .line 773
    :pswitch_4
    const-string v0, "TOKEN_SERVICE_PRESENCE_TMO"

    goto :goto_0

    .line 774
    :pswitch_5
    const-string v0, "TOKEN_SERVICE_MEDIA"

    goto :goto_0

    .line 775
    :pswitch_6
    const-string v0, "TOKEN_SERVICE_SMS"

    goto :goto_0

    .line 776
    :pswitch_7
    const-string v0, "TOKEN_SERVICE_SSCONFIG"

    goto :goto_0

    .line 768
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_0
    .end packed-switch
.end method
