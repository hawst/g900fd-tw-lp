.class public final enum Lcom/samsung/commonimsinterface/imscommon/CameraState;
.super Ljava/lang/Enum;
.source "CameraState.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/commonimsinterface/imscommon/CameraState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/commonimsinterface/imscommon/CameraState;

.field public static final enum CAMERA_CLOSED:Lcom/samsung/commonimsinterface/imscommon/CameraState;

.field public static final enum PREVIEW_STARTED:Lcom/samsung/commonimsinterface/imscommon/CameraState;

.field public static final enum PREVIEW_STOPPED:Lcom/samsung/commonimsinterface/imscommon/CameraState;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 5
    new-instance v0, Lcom/samsung/commonimsinterface/imscommon/CameraState;

    const-string v1, "CAMERA_CLOSED"

    invoke-direct {v0, v1, v2}, Lcom/samsung/commonimsinterface/imscommon/CameraState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/commonimsinterface/imscommon/CameraState;->CAMERA_CLOSED:Lcom/samsung/commonimsinterface/imscommon/CameraState;

    .line 6
    new-instance v0, Lcom/samsung/commonimsinterface/imscommon/CameraState;

    const-string v1, "PREVIEW_STOPPED"

    invoke-direct {v0, v1, v3}, Lcom/samsung/commonimsinterface/imscommon/CameraState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/commonimsinterface/imscommon/CameraState;->PREVIEW_STOPPED:Lcom/samsung/commonimsinterface/imscommon/CameraState;

    .line 7
    new-instance v0, Lcom/samsung/commonimsinterface/imscommon/CameraState;

    const-string v1, "PREVIEW_STARTED"

    invoke-direct {v0, v1, v4}, Lcom/samsung/commonimsinterface/imscommon/CameraState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/commonimsinterface/imscommon/CameraState;->PREVIEW_STARTED:Lcom/samsung/commonimsinterface/imscommon/CameraState;

    .line 4
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/samsung/commonimsinterface/imscommon/CameraState;

    sget-object v1, Lcom/samsung/commonimsinterface/imscommon/CameraState;->CAMERA_CLOSED:Lcom/samsung/commonimsinterface/imscommon/CameraState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/samsung/commonimsinterface/imscommon/CameraState;->PREVIEW_STOPPED:Lcom/samsung/commonimsinterface/imscommon/CameraState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/commonimsinterface/imscommon/CameraState;->PREVIEW_STARTED:Lcom/samsung/commonimsinterface/imscommon/CameraState;

    aput-object v1, v0, v4

    sput-object v0, Lcom/samsung/commonimsinterface/imscommon/CameraState;->$VALUES:[Lcom/samsung/commonimsinterface/imscommon/CameraState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 4
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/commonimsinterface/imscommon/CameraState;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 4
    const-class v0, Lcom/samsung/commonimsinterface/imscommon/CameraState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/commonimsinterface/imscommon/CameraState;

    return-object v0
.end method

.method public static values()[Lcom/samsung/commonimsinterface/imscommon/CameraState;
    .locals 1

    .prologue
    .line 4
    sget-object v0, Lcom/samsung/commonimsinterface/imscommon/CameraState;->$VALUES:[Lcom/samsung/commonimsinterface/imscommon/CameraState;

    invoke-virtual {v0}, [Lcom/samsung/commonimsinterface/imscommon/CameraState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/commonimsinterface/imscommon/CameraState;

    return-object v0
.end method
