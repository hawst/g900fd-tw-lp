.class public Lcom/samsung/commonimsinterface/imscommon/internal/IMSCommonData$SETTING$STORAGE_TYPE;
.super Ljava/lang/Object;
.source "IMSCommonData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/commonimsinterface/imscommon/internal/IMSCommonData$SETTING;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "STORAGE_TYPE"
.end annotation


# static fields
.field public static final STORAGE_DATABASE:I = 0x0

.field public static final STORAGE_FILE_ONLY_SINGLE_VALUE:I = 0x5

.field public static final STORAGE_FILE_TABLEINDEX_VALUE:I = 0x6

.field public static final STORAGE_ISIM:I = 0x3

.field public static final STORAGE_SHARED_PREF:I = 0x1

.field public static final STORAGE_SYSTEM_PROP:I = 0x4

.field public static final STORAGE_TYPE_NAME:[Ljava/lang/String;

.field public static final STORAGE_USIM:I = 0x2


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 82
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "STORAGE_DATABASE"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "STORAGE_SHARED_PREF"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "STORAGE_USIM"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "STORAGE_ISIM"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "STORAGE_SYSTEM_PROP"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "STORAGE_FILE_ONLY_SINGLE_VALUE"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "STORAGE_FILE_TABLEINDEX_VALUE"

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSCommonData$SETTING$STORAGE_TYPE;->STORAGE_TYPE_NAME:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
