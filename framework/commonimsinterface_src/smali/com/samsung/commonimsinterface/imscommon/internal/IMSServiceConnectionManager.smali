.class public Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;
.super Ljava/lang/Object;
.source "IMSServiceConnectionManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager$IMSServiceConnection;
    }
.end annotation


# static fields
.field private static final IMS_SERVICE_KEY_BIND:Ljava/lang/String; = "BIND"

.field private static final IMS_SERVICE_KEY_INSTANCE:Ljava/lang/String; = "INSTANCE"

.field private static final IMS_SERVICE_KEY_INTERFACE:Ljava/lang/String; = "INTERFACE"

.field private static final IMS_SERVICE_KEY_LISTENER:Ljava/lang/String; = "LISTENER"

.field private static final IMS_SERVICE_KEY_NAME:Ljava/lang/String; = "NAME"

.field private static final IMS_SERVICE_KEY_PACKAGE:Ljava/lang/String; = "PACKAGE"

.field private static final LOG_TAG:Ljava/lang/String;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mIMSServiceConnection:Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager$IMSServiceConnection;

.field private mIMSServiceInfo:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const-class v0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object v1, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;->mContext:Landroid/content/Context;

    .line 30
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;->mIMSServiceInfo:Ljava/util/HashMap;

    .line 31
    iput-object v1, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;->mIMSServiceConnection:Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager$IMSServiceConnection;

    .line 40
    iput-object p1, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;->mContext:Landroid/content/Context;

    .line 41
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 16
    sget-object v0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;->LOG_TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;->unbindService()V

    return-void
.end method

.method static synthetic access$200(Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;->bindService()V

    return-void
.end method

.method private bindService()V
    .locals 6

    .prologue
    .line 88
    iget-object v3, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;->mIMSServiceInfo:Ljava/util/HashMap;

    const-string v4, "PACKAGE"

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 89
    .local v2, "servicePkgName":Ljava/lang/String;
    iget-object v3, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;->mIMSServiceInfo:Ljava/util/HashMap;

    const-string v4, "NAME"

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 91
    .local v0, "serviceClsName":Ljava/lang/String;
    sget-object v3, Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;->LOG_TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "bindService to "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    new-instance v3, Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager$IMSServiceConnection;

    iget-object v4, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;->mIMSServiceInfo:Ljava/util/HashMap;

    invoke-direct {v3, p0, v4}, Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager$IMSServiceConnection;-><init>(Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;Ljava/util/HashMap;)V

    iput-object v3, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;->mIMSServiceConnection:Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager$IMSServiceConnection;

    .line 95
    sget-object v3, Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;->LOG_TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "bindService : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;->mIMSServiceConnection:Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager$IMSServiceConnection;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 98
    .local v1, "serviceIntent":Landroid/content/Intent;
    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 100
    iget-object v3, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;->mIMSServiceConnection:Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager$IMSServiceConnection;

    const/4 v5, 0x1

    invoke-virtual {v3, v1, v4, v5}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 103
    return-void
.end method

.method private startService()V
    .locals 4

    .prologue
    .line 68
    sget-object v1, Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;->LOG_TAG:Ljava/lang/String;

    const-string v2, "startService"

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;->mIMSServiceInfo:Ljava/util/HashMap;

    const-string v2, "INSTANCE"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 74
    const/4 v0, 0x0

    .line 75
    .local v0, "listener":Lcom/samsung/commonimsinterface/imscommon/IIMSServiceConnectionListener;
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;->mIMSServiceInfo:Ljava/util/HashMap;

    const-string v2, "LISTENER"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "listener":Lcom/samsung/commonimsinterface/imscommon/IIMSServiceConnectionListener;
    check-cast v0, Lcom/samsung/commonimsinterface/imscommon/IIMSServiceConnectionListener;

    .line 76
    .restart local v0    # "listener":Lcom/samsung/commonimsinterface/imscommon/IIMSServiceConnectionListener;
    invoke-interface {v0}, Lcom/samsung/commonimsinterface/imscommon/IIMSServiceConnectionListener;->onConnected()V

    .line 77
    return-void
.end method

.method private unbindService()V
    .locals 3

    .prologue
    .line 80
    sget-object v0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;->LOG_TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "unbindService : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;->mIMSServiceConnection:Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager$IMSServiceConnection;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;->mIMSServiceConnection:Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager$IMSServiceConnection;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 84
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;->mIMSServiceConnection:Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager$IMSServiceConnection;

    .line 85
    return-void
.end method


# virtual methods
.method public getServiceInstance()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 106
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;->mIMSServiceInfo:Ljava/util/HashMap;

    const-string v1, "INSTANCE"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public startIMSService(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/samsung/commonimsinterface/imscommon/IIMSServiceConnectionListener;)V
    .locals 3
    .param p1, "serviceClsName"    # Ljava/lang/String;
    .param p2, "servicePkgName"    # Ljava/lang/String;
    .param p3, "serviceInterface"    # Ljava/lang/String;
    .param p4, "serviceBind"    # Z
    .param p5, "listener"    # Lcom/samsung/commonimsinterface/imscommon/IIMSServiceConnectionListener;

    .prologue
    .line 48
    sget-object v0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;->LOG_TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "startIMSService : [NAME] "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "PACKAGE"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "INTERFACE"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "BIND"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "LISTENER"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 54
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;->mIMSServiceInfo:Ljava/util/HashMap;

    const-string v1, "NAME"

    invoke-virtual {v0, v1, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 55
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;->mIMSServiceInfo:Ljava/util/HashMap;

    const-string v1, "PACKAGE"

    invoke-virtual {v0, v1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 56
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;->mIMSServiceInfo:Ljava/util/HashMap;

    const-string v1, "INTERFACE"

    invoke-virtual {v0, v1, p3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 57
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;->mIMSServiceInfo:Ljava/util/HashMap;

    const-string v1, "BIND"

    invoke-static {p4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 58
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;->mIMSServiceInfo:Ljava/util/HashMap;

    const-string v1, "LISTENER"

    invoke-virtual {v0, v1, p5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 60
    const/4 v0, 0x1

    if-ne p4, v0, :cond_0

    .line 61
    invoke-direct {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;->bindService()V

    .line 65
    :goto_0
    return-void

    .line 63
    :cond_0
    invoke-direct {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;->startService()V

    goto :goto_0
.end method
