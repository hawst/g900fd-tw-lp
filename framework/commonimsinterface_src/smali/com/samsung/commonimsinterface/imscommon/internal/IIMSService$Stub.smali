.class public abstract Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;
.super Landroid/os/Binder;
.source "IIMSService.java"

# interfaces
.implements Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

.field static final TRANSACTION_abortIMSSettingTransaction:I = 0x76

.field static final TRANSACTION_acceptChangeRequest:I = 0x14

.field static final TRANSACTION_acceptChangeRequestWithCallType:I = 0x15

.field static final TRANSACTION_addParticipantToNWayConferenceCall:I = 0x31

.field static final TRANSACTION_addUserForConferenceCall:I = 0x24

.field static final TRANSACTION_answerCall:I = 0x9

.field static final TRANSACTION_answerCallAudioOnly:I = 0xb

.field static final TRANSACTION_answerCallWithCallType:I = 0xa

.field static final TRANSACTION_captureSurfaceImage:I = 0x97

.field static final TRANSACTION_changeCall:I = 0x13

.field static final TRANSACTION_changeEPDGAudioPath:I = 0x43

.field static final TRANSACTION_closeCamera:I = 0x89

.field static final TRANSACTION_contactSvcCallFunction:I = 0xa6

.field static final TRANSACTION_deRegisterForAPCSStateChange:I = 0x4c

.field static final TRANSACTION_deRegisterForCallStateChange:I = 0x6

.field static final TRANSACTION_deRegisterForConnectivityStateChange:I = 0x46

.field static final TRANSACTION_deRegisterForMediaStateChange:I = 0x7a

.field static final TRANSACTION_deRegisterForPresenceStateChange:I = 0xa0

.field static final TRANSACTION_deRegisterForRegisterStateChange:I = 0x48

.field static final TRANSACTION_deRegisterForSMSStateChange:I = 0xa9

.field static final TRANSACTION_deRegisterForSSConfigStateChange:I = 0xaf

.field static final TRANSACTION_deRegisterForServiceStateChange:I = 0x3

.field static final TRANSACTION_deRegisterForSettingStateListener:I = 0x4a

.field static final TRANSACTION_deinitSurface:I = 0x98

.field static final TRANSACTION_disableWiFiCalling:I = 0x61

.field static final TRANSACTION_downgradeCall:I = 0x12

.field static final TRANSACTION_emergencyDeregister:I = 0x2f

.field static final TRANSACTION_emergencyRegister:I = 0x2e

.field static final TRANSACTION_enableWiFiCalling:I = 0x60

.field static final TRANSACTION_endCall:I = 0xc

.field static final TRANSACTION_endCallWithReason:I = 0xd

.field static final TRANSACTION_endIMSSettingTransaction:I = 0x75

.field static final TRANSACTION_forceRestart:I = 0x5f

.field static final TRANSACTION_getAvailableBearer:I = 0x4

.field static final TRANSACTION_getBackCameraId:I = 0x8a

.field static final TRANSACTION_getCallBarring:I = 0xb5

.field static final TRANSACTION_getCallForwarding:I = 0xb3

.field static final TRANSACTION_getCallType:I = 0x3c

.field static final TRANSACTION_getCallWaiting:I = 0xb1

.field static final TRANSACTION_getCameraDirection:I = 0x8d

.field static final TRANSACTION_getCameraState:I = 0x8c

.field static final TRANSACTION_getCurrentLatchedNetwork:I = 0x29

.field static final TRANSACTION_getErrorCode:I = 0x59

.field static final TRANSACTION_getFeatureMask:I = 0x58

.field static final TRANSACTION_getFrontCameraId:I = 0x8b

.field static final TRANSACTION_getIMSSettingFileEnumByUri:I = 0x73

.field static final TRANSACTION_getIMSSettingValues:I = 0x6f

.field static final TRANSACTION_getIMSSettingValuesWithUri:I = 0x70

.field static final TRANSACTION_getMaxVolume:I = 0x23

.field static final TRANSACTION_getMaxZoom:I = 0x9c

.field static final TRANSACTION_getNegotiatedFPS:I = 0x81

.field static final TRANSACTION_getNegotiatedHeight:I = 0x7f

.field static final TRANSACTION_getNegotiatedWidth:I = 0x80

.field static final TRANSACTION_getNumberOfCameras:I = 0x84

.field static final TRANSACTION_getParticipantListSizeForNWayConferenceCall:I = 0x33

.field static final TRANSACTION_getParticipantPeerURIFromNWayConferenceCall:I = 0x36

.field static final TRANSACTION_getParticipantSessionIdListForNWayConferenceCall:I = 0x35

.field static final TRANSACTION_getParticipantURIListForNWayConferenceCall:I = 0x34

.field static final TRANSACTION_getRemoteCapabilitiesAndAvailability:I = 0xa3

.field static final TRANSACTION_getRemoteCapabilitiesAndAvailabilityPoll:I = 0xa4

.field static final TRANSACTION_getServiceStatus:I = 0x5a

.field static final TRANSACTION_getSessionID:I = 0x3f

.field static final TRANSACTION_getState:I = 0x3d

.field static final TRANSACTION_getTtyMode:I = 0x1d

.field static final TRANSACTION_getZoom:I = 0x9e

.field static final TRANSACTION_handleActionsOverEpdg:I = 0x64

.field static final TRANSACTION_hashCode:I = 0x3e

.field static final TRANSACTION_holdCall:I = 0xf

.field static final TRANSACTION_holdVideo:I = 0x99

.field static final TRANSACTION_indicateSrvccHoStarted:I = 0x26

.field static final TRANSACTION_isCallBarred:I = 0x44

.field static final TRANSACTION_isDeregisteredToMobile:I = 0x50

.field static final TRANSACTION_isDeregisteredToWiFi:I = 0x54

.field static final TRANSACTION_isDeregisteringToMobile:I = 0x51

.field static final TRANSACTION_isDeregisteringToWiFi:I = 0x55

.field static final TRANSACTION_isDisablingMobileData:I = 0x57

.field static final TRANSACTION_isEABMenuShow:I = 0x6d

.field static final TRANSACTION_isEnablingMobileData:I = 0x56

.field static final TRANSACTION_isIMSEnabledOnWifi:I = 0x2a

.field static final TRANSACTION_isIMSServiceReady:I = 0x1

.field static final TRANSACTION_isImsForbidden:I = 0x28

.field static final TRANSACTION_isInCall:I = 0x39

.field static final TRANSACTION_isInEPDGCall:I = 0x40

.field static final TRANSACTION_isIncomingCallIntent:I = 0x38

.field static final TRANSACTION_isLTEVideoCallEnabled:I = 0x6a

.field static final TRANSACTION_isLTEVideoCallMenuShow:I = 0x6c

.field static final TRANSACTION_isLimitedMode:I = 0x77

.field static final TRANSACTION_isMediaReadyToReceivePreview:I = 0x82

.field static final TRANSACTION_isMuted:I = 0x1f

.field static final TRANSACTION_isOnEHRPD:I = 0x2c

.field static final TRANSACTION_isOnHold:I = 0x3a

.field static final TRANSACTION_isOnLTE:I = 0x2b

.field static final TRANSACTION_isProvisioned:I = 0xa7

.field static final TRANSACTION_isRegistered:I = 0x4d

.field static final TRANSACTION_isRegisteredToMobile:I = 0x4e

.field static final TRANSACTION_isRegisteredToWiFi:I = 0x52

.field static final TRANSACTION_isRegisteringToMobile:I = 0x4f

.field static final TRANSACTION_isRegisteringToWiFi:I = 0x53

.field static final TRANSACTION_isVoLTEFeatureEnabled:I = 0x67

.field static final TRANSACTION_isVoLTEMenuShow:I = 0x6b

.field static final TRANSACTION_makeCall:I = 0x7

.field static final TRANSACTION_manualDeregister:I = 0x5c

.field static final TRANSACTION_manualDeregisterForCall:I = 0x1e

.field static final TRANSACTION_manualRegister:I = 0x5b

.field static final TRANSACTION_mediaDeInit:I = 0x7c

.field static final TRANSACTION_mediaInit:I = 0x7b

.field static final TRANSACTION_mmSS_Svc_Api:I = 0xb6

.field static final TRANSACTION_openCamera:I = 0x85

.field static final TRANSACTION_openCameraEx:I = 0x86

.field static final TRANSACTION_publishCapabilitiesAndAvailability:I = 0xa1

.field static final TRANSACTION_registerForAPCSStateChange:I = 0x4b

.field static final TRANSACTION_registerForCallStateChange:I = 0x5

.field static final TRANSACTION_registerForConnectivityStateChange:I = 0x45

.field static final TRANSACTION_registerForMediaStateChange:I = 0x79

.field static final TRANSACTION_registerForPresenceStateChange:I = 0x9f

.field static final TRANSACTION_registerForRegisterStateChange:I = 0x47

.field static final TRANSACTION_registerForSMSStateChange:I = 0xa8

.field static final TRANSACTION_registerForSSConfigStateChange:I = 0xae

.field static final TRANSACTION_registerForServiceStateChange:I = 0x2

.field static final TRANSACTION_registerForSettingStateListener:I = 0x49

.field static final TRANSACTION_registerServiceCapabilityAvailabilityTemplate:I = 0xa5

.field static final TRANSACTION_rejectCall:I = 0xe

.field static final TRANSACTION_rejectChangeRequest:I = 0x16

.field static final TRANSACTION_removeParticipantFromNWayConferenceCall:I = 0x32

.field static final TRANSACTION_resetCameraID:I = 0x93

.field static final TRANSACTION_resumeCall:I = 0x10

.field static final TRANSACTION_resumeVideo:I = 0x9a

.field static final TRANSACTION_send180Ringing:I = 0x42

.field static final TRANSACTION_sendDeliverReport:I = 0xac

.field static final TRANSACTION_sendDeregister:I = 0x5e

.field static final TRANSACTION_sendDtmf:I = 0x17

.field static final TRANSACTION_sendInitialRegister:I = 0x63

.field static final TRANSACTION_sendLiveVideo:I = 0x96

.field static final TRANSACTION_sendRPSMMA:I = 0xad

.field static final TRANSACTION_sendReInvite:I = 0x25

.field static final TRANSACTION_sendRegister:I = 0x5d

.field static final TRANSACTION_sendSMSOverIMS:I = 0xaa

.field static final TRANSACTION_sendSMSResponse:I = 0xab

.field static final TRANSACTION_sendStillImage:I = 0x94

.field static final TRANSACTION_sendTtyData:I = 0x1b

.field static final TRANSACTION_sendUSSD:I = 0x8

.field static final TRANSACTION_setAutoResponse:I = 0x27

.field static final TRANSACTION_setCallBarring:I = 0xb4

.field static final TRANSACTION_setCallForwarding:I = 0xb2

.field static final TRANSACTION_setCallWaiting:I = 0xb0

.field static final TRANSACTION_setCameraDisplayOrientation:I = 0x8e

.field static final TRANSACTION_setCameraDisplayOrientationEx:I = 0x8f

.field static final TRANSACTION_setCameraEffect:I = 0x95

.field static final TRANSACTION_setDisplay:I = 0x7d

.field static final TRANSACTION_setEmergencyPdnInfo:I = 0x2d

.field static final TRANSACTION_setFarEndSurface:I = 0x7e

.field static final TRANSACTION_setIsMediaReadyToReceivePreview:I = 0x83

.field static final TRANSACTION_setLTEVideoCallDisable:I = 0x69

.field static final TRANSACTION_setLTEVideoCallEnable:I = 0x68

.field static final TRANSACTION_setRegistrationFeatureTags:I = 0x62

.field static final TRANSACTION_setSpeakerMode:I = 0x21

.field static final TRANSACTION_setThirdPartyMode:I = 0x6e

.field static final TRANSACTION_setTtyMode:I = 0x1c

.field static final TRANSACTION_setVoLTEFeatureDisable:I = 0x66

.field static final TRANSACTION_setVoLTEFeatureEnable:I = 0x65

.field static final TRANSACTION_setVolume:I = 0x22

.field static final TRANSACTION_setZoom:I = 0x9d

.field static final TRANSACTION_startAudio:I = 0x3b

.field static final TRANSACTION_startCameraPreview:I = 0x87

.field static final TRANSACTION_startDtmf:I = 0x18

.field static final TRANSACTION_startIMSSettingTransaction:I = 0x74

.field static final TRANSACTION_startNWayConferenceCall:I = 0x30

.field static final TRANSACTION_startRender:I = 0x91

.field static final TRANSACTION_startT3402TimerValue:I = 0x78

.field static final TRANSACTION_startVideo:I = 0x9b

.field static final TRANSACTION_stopCameraPreview:I = 0x88

.field static final TRANSACTION_stopDtmf:I = 0x19

.field static final TRANSACTION_swapVideoSurface:I = 0x92

.field static final TRANSACTION_switchCamera:I = 0x90

.field static final TRANSACTION_takeCall:I = 0x37

.field static final TRANSACTION_toggleMute:I = 0x20

.field static final TRANSACTION_unpublishCapabilitiesAndAvailability:I = 0xa2

.field static final TRANSACTION_updateIMSSettingValues:I = 0x71

.field static final TRANSACTION_updateIMSSettingValuesWithUri:I = 0x72

.field static final TRANSACTION_updateSSACInfo:I = 0x41

.field static final TRANSACTION_upgradeCall:I = 0x11

.field static final TRANSACTION_voiceRecord:I = 0x1a


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 15
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p0, p0, v0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 16
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;
    .locals 2
    .param p0, "obj"    # Landroid/os/IBinder;

    .prologue
    .line 23
    if-nez p0, :cond_0

    .line 24
    const/4 v0, 0x0

    .line 30
    :goto_0
    return-object v0

    .line 26
    :cond_0
    const-string v1, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 27
    .local v0, "iin":Landroid/os/IInterface;
    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    if-eqz v1, :cond_1

    .line 28
    check-cast v0, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    goto :goto_0

    .line 30
    :cond_1
    new-instance v0, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub$Proxy;

    .end local v0    # "iin":Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 34
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 10
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 38
    sparse-switch p1, :sswitch_data_0

    .line 1830
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v0

    :goto_0
    return v0

    .line 42
    :sswitch_0
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 43
    const/4 v0, 0x1

    goto :goto_0

    .line 47
    :sswitch_1
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 48
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->isIMSServiceReady()Z

    move-result v8

    .line 49
    .local v8, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 50
    if-eqz v8, :cond_0

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 51
    const/4 v0, 0x1

    goto :goto_0

    .line 50
    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    .line 55
    .end local v8    # "_result":Z
    :sswitch_2
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 57
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;

    move-result-object v1

    .line 58
    .local v1, "_arg0":Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->registerForServiceStateChange(Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;)V

    .line 59
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 60
    const/4 v0, 0x1

    goto :goto_0

    .line 64
    .end local v1    # "_arg0":Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;
    :sswitch_3
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 66
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;

    move-result-object v1

    .line 67
    .restart local v1    # "_arg0":Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->deRegisterForServiceStateChange(Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;)V

    .line 68
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 69
    const/4 v0, 0x1

    goto :goto_0

    .line 73
    .end local v1    # "_arg0":Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;
    :sswitch_4
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 75
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 76
    .local v1, "_arg0":I
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->getAvailableBearer(I)I

    move-result v8

    .line 77
    .local v8, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 78
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    .line 79
    const/4 v0, 0x1

    goto :goto_0

    .line 83
    .end local v1    # "_arg0":I
    .end local v8    # "_result":I
    :sswitch_5
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 85
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;

    move-result-object v1

    .line 86
    .local v1, "_arg0":Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->registerForCallStateChange(Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;)V

    .line 87
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 88
    const/4 v0, 0x1

    goto :goto_0

    .line 92
    .end local v1    # "_arg0":Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;
    :sswitch_6
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 94
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;

    move-result-object v1

    .line 95
    .restart local v1    # "_arg0":Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->deRegisterForCallStateChange(Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;)V

    .line 96
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 97
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 101
    .end local v1    # "_arg0":Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;
    :sswitch_7
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 103
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 105
    .local v1, "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 107
    .local v2, "_arg1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 109
    .local v3, "_arg2":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    .line 111
    .local v4, "_arg3":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v5

    .line 113
    .local v5, "_arg4":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v6

    .line 115
    .local v6, "_arg5":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v7

    .local v7, "_arg6":Ljava/lang/String;
    move-object v0, p0

    .line 116
    invoke-virtual/range {v0 .. v7}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->makeCall(Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v8

    .line 117
    .restart local v8    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 118
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    .line 119
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 123
    .end local v1    # "_arg0":Ljava/lang/String;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v3    # "_arg2":I
    .end local v4    # "_arg3":I
    .end local v5    # "_arg4":Ljava/lang/String;
    .end local v6    # "_arg5":Ljava/lang/String;
    .end local v7    # "_arg6":Ljava/lang/String;
    .end local v8    # "_result":I
    :sswitch_8
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 125
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 127
    .local v1, "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 129
    .local v2, "_arg1":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 131
    .restart local v3    # "_arg2":I
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v4

    .line 133
    .local v4, "_arg3":[B
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v5

    .local v5, "_arg4":I
    move-object v0, p0

    .line 134
    invoke-virtual/range {v0 .. v5}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->sendUSSD(III[BI)I

    move-result v8

    .line 135
    .restart local v8    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 136
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    .line 137
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 141
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":I
    .end local v3    # "_arg2":I
    .end local v4    # "_arg3":[B
    .end local v5    # "_arg4":I
    .end local v8    # "_result":I
    :sswitch_9
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 143
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 144
    .restart local v1    # "_arg0":I
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->answerCall(I)V

    .line 145
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 149
    .end local v1    # "_arg0":I
    :sswitch_a
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 151
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 153
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 154
    .restart local v2    # "_arg1":I
    invoke-virtual {p0, v1, v2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->answerCallWithCallType(II)V

    .line 155
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 159
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":I
    :sswitch_b
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 161
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 162
    .restart local v1    # "_arg0":I
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->answerCallAudioOnly(I)V

    .line 163
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 167
    .end local v1    # "_arg0":I
    :sswitch_c
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 169
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 170
    .restart local v1    # "_arg0":I
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->endCall(I)V

    .line 171
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 175
    .end local v1    # "_arg0":I
    :sswitch_d
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 177
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 179
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 180
    .restart local v2    # "_arg1":I
    invoke-virtual {p0, v1, v2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->endCallWithReason(II)V

    .line 181
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 185
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":I
    :sswitch_e
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 187
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 189
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 190
    .restart local v2    # "_arg1":I
    invoke-virtual {p0, v1, v2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->rejectCall(II)V

    .line 191
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 195
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":I
    :sswitch_f
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 197
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 198
    .restart local v1    # "_arg0":I
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->holdCall(I)V

    .line 199
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 203
    .end local v1    # "_arg0":I
    :sswitch_10
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 205
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 206
    .restart local v1    # "_arg0":I
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->resumeCall(I)V

    .line 207
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 211
    .end local v1    # "_arg0":I
    :sswitch_11
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 213
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 215
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 216
    .restart local v2    # "_arg1":I
    invoke-virtual {p0, v1, v2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->upgradeCall(II)V

    .line 217
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 221
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":I
    :sswitch_12
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 223
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 225
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 226
    .restart local v2    # "_arg1":I
    invoke-virtual {p0, v1, v2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->downgradeCall(II)V

    .line 227
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 231
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":I
    :sswitch_13
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 233
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 235
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 236
    .restart local v2    # "_arg1":I
    invoke-virtual {p0, v1, v2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->changeCall(II)V

    .line 237
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 241
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":I
    :sswitch_14
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 243
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 244
    .restart local v1    # "_arg0":I
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->acceptChangeRequest(I)V

    .line 245
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 249
    .end local v1    # "_arg0":I
    :sswitch_15
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 251
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 253
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 254
    .restart local v2    # "_arg1":I
    invoke-virtual {p0, v1, v2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->acceptChangeRequestWithCallType(II)V

    .line 255
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 259
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":I
    :sswitch_16
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 261
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 262
    .restart local v1    # "_arg0":I
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->rejectChangeRequest(I)V

    .line 263
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 267
    .end local v1    # "_arg0":I
    :sswitch_17
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 269
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 271
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 272
    .restart local v2    # "_arg1":I
    invoke-virtual {p0, v1, v2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->sendDtmf(II)V

    .line 273
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 274
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 278
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":I
    :sswitch_18
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 280
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 282
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 283
    .restart local v2    # "_arg1":I
    invoke-virtual {p0, v1, v2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->startDtmf(II)V

    .line 284
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 285
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 289
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":I
    :sswitch_19
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 291
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 292
    .restart local v1    # "_arg0":I
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->stopDtmf(I)V

    .line 293
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 294
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 298
    .end local v1    # "_arg0":I
    :sswitch_1a
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 300
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 302
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 304
    .restart local v2    # "_arg1":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 305
    .local v3, "_arg2":Ljava/lang/String;
    invoke-virtual {p0, v1, v2, v3}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->voiceRecord(IILjava/lang/String;)V

    .line 306
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 310
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":I
    .end local v3    # "_arg2":Ljava/lang/String;
    :sswitch_1b
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 312
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 314
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v2

    .line 315
    .local v2, "_arg1":[B
    invoke-virtual {p0, v1, v2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->sendTtyData(I[B)V

    .line 316
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 320
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":[B
    :sswitch_1c
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 322
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 323
    .restart local v1    # "_arg0":I
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->setTtyMode(I)V

    .line 324
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 328
    .end local v1    # "_arg0":I
    :sswitch_1d
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 329
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->getTtyMode()I

    move-result v8

    .line 330
    .restart local v8    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 331
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    .line 332
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 336
    .end local v8    # "_result":I
    :sswitch_1e
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 337
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->manualDeregisterForCall()V

    .line 338
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 342
    :sswitch_1f
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 344
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 345
    .restart local v1    # "_arg0":I
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->isMuted(I)Z

    move-result v8

    .line 346
    .local v8, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 347
    if-eqz v8, :cond_1

    const/4 v0, 0x1

    :goto_2
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 348
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 347
    :cond_1
    const/4 v0, 0x0

    goto :goto_2

    .line 352
    .end local v1    # "_arg0":I
    .end local v8    # "_result":Z
    :sswitch_20
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 354
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 355
    .restart local v1    # "_arg0":I
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->toggleMute(I)V

    .line 356
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 360
    .end local v1    # "_arg0":I
    :sswitch_21
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 362
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 364
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_2

    const/4 v2, 0x1

    .line 365
    .local v2, "_arg1":Z
    :goto_3
    invoke-virtual {p0, v1, v2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->setSpeakerMode(IZ)V

    .line 366
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 364
    .end local v2    # "_arg1":Z
    :cond_2
    const/4 v2, 0x0

    goto :goto_3

    .line 370
    .end local v1    # "_arg0":I
    :sswitch_22
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 372
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 374
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 375
    .local v2, "_arg1":I
    invoke-virtual {p0, v1, v2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->setVolume(II)V

    .line 376
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 380
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":I
    :sswitch_23
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 382
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 383
    .restart local v1    # "_arg0":I
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->getMaxVolume(I)I

    move-result v8

    .line 384
    .local v8, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 385
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    .line 386
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 390
    .end local v1    # "_arg0":I
    .end local v8    # "_result":I
    :sswitch_24
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 392
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 394
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 396
    .local v2, "_arg1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 397
    .local v3, "_arg2":I
    invoke-virtual {p0, v1, v2, v3}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->addUserForConferenceCall(ILjava/lang/String;I)I

    move-result v8

    .line 398
    .restart local v8    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 399
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    .line 400
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 404
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v3    # "_arg2":I
    .end local v8    # "_result":I
    :sswitch_25
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 406
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 407
    .restart local v1    # "_arg0":I
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->sendReInvite(I)V

    .line 408
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 412
    .end local v1    # "_arg0":I
    :sswitch_26
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 414
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 415
    .restart local v1    # "_arg0":I
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->indicateSrvccHoStarted(I)V

    .line 416
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 420
    .end local v1    # "_arg0":I
    :sswitch_27
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 422
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 424
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 425
    .local v2, "_arg1":I
    invoke-virtual {p0, v1, v2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->setAutoResponse(II)V

    .line 426
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 430
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":I
    :sswitch_28
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 431
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->isImsForbidden()Z

    move-result v8

    .line 432
    .local v8, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 433
    if-eqz v8, :cond_3

    const/4 v0, 0x1

    :goto_4
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 434
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 433
    :cond_3
    const/4 v0, 0x0

    goto :goto_4

    .line 438
    .end local v8    # "_result":Z
    :sswitch_29
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 439
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->getCurrentLatchedNetwork()Ljava/lang/String;

    move-result-object v8

    .line 440
    .local v8, "_result":Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 441
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 442
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 446
    .end local v8    # "_result":Ljava/lang/String;
    :sswitch_2a
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 447
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->isIMSEnabledOnWifi()Z

    move-result v8

    .line 448
    .local v8, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 449
    if-eqz v8, :cond_4

    const/4 v0, 0x1

    :goto_5
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 450
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 449
    :cond_4
    const/4 v0, 0x0

    goto :goto_5

    .line 454
    .end local v8    # "_result":Z
    :sswitch_2b
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 455
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->isOnLTE()Z

    move-result v8

    .line 456
    .restart local v8    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 457
    if-eqz v8, :cond_5

    const/4 v0, 0x1

    :goto_6
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 458
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 457
    :cond_5
    const/4 v0, 0x0

    goto :goto_6

    .line 462
    .end local v8    # "_result":Z
    :sswitch_2c
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 463
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->isOnEHRPD()Z

    move-result v8

    .line 464
    .restart local v8    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 465
    if-eqz v8, :cond_6

    const/4 v0, 0x1

    :goto_7
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 466
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 465
    :cond_6
    const/4 v0, 0x0

    goto :goto_7

    .line 470
    .end local v8    # "_result":Z
    :sswitch_2d
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 472
    invoke-virtual {p2}, Landroid/os/Parcel;->createStringArray()[Ljava/lang/String;

    move-result-object v1

    .line 474
    .local v1, "_arg0":[Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->createStringArray()[Ljava/lang/String;

    move-result-object v2

    .line 476
    .local v2, "_arg1":[Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 477
    .local v3, "_arg2":Ljava/lang/String;
    invoke-virtual {p0, v1, v2, v3}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->setEmergencyPdnInfo([Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 478
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 482
    .end local v1    # "_arg0":[Ljava/lang/String;
    .end local v2    # "_arg1":[Ljava/lang/String;
    .end local v3    # "_arg2":Ljava/lang/String;
    :sswitch_2e
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 483
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->emergencyRegister()V

    .line 484
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 488
    :sswitch_2f
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 489
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->emergencyDeregister()V

    .line 490
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 494
    :sswitch_30
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 496
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 498
    .local v1, "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 500
    .local v2, "_arg1":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 501
    .local v3, "_arg2":I
    invoke-virtual {p0, v1, v2, v3}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->startNWayConferenceCall(III)I

    move-result v8

    .line 502
    .local v8, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 503
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    .line 504
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 508
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":I
    .end local v3    # "_arg2":I
    .end local v8    # "_result":I
    :sswitch_31
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 510
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 512
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 513
    .restart local v2    # "_arg1":I
    invoke-virtual {p0, v1, v2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->addParticipantToNWayConferenceCall(II)V

    .line 514
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 518
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":I
    :sswitch_32
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 520
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 522
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 523
    .restart local v2    # "_arg1":I
    invoke-virtual {p0, v1, v2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->removeParticipantFromNWayConferenceCall(II)V

    .line 524
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 528
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":I
    :sswitch_33
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 530
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 531
    .restart local v1    # "_arg0":I
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->getParticipantListSizeForNWayConferenceCall(I)I

    move-result v8

    .line 532
    .restart local v8    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 533
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    .line 534
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 538
    .end local v1    # "_arg0":I
    .end local v8    # "_result":I
    :sswitch_34
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 540
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 541
    .restart local v1    # "_arg0":I
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->getParticipantURIListForNWayConferenceCall(I)[Ljava/lang/String;

    move-result-object v8

    .line 542
    .local v8, "_result":[Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 543
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    .line 544
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 548
    .end local v1    # "_arg0":I
    .end local v8    # "_result":[Ljava/lang/String;
    :sswitch_35
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 550
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 551
    .restart local v1    # "_arg0":I
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->getParticipantSessionIdListForNWayConferenceCall(I)[Ljava/lang/String;

    move-result-object v8

    .line 552
    .restart local v8    # "_result":[Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 553
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    .line 554
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 558
    .end local v1    # "_arg0":I
    .end local v8    # "_result":[Ljava/lang/String;
    :sswitch_36
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 560
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 562
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 563
    .restart local v2    # "_arg1":I
    invoke-virtual {p0, v1, v2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->getParticipantPeerURIFromNWayConferenceCall(II)Ljava/lang/String;

    move-result-object v8

    .line 564
    .local v8, "_result":Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 565
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 566
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 570
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":I
    .end local v8    # "_result":Ljava/lang/String;
    :sswitch_37
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 572
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_7

    .line 573
    sget-object v0, Landroid/content/Intent;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Intent;

    .line 578
    .local v1, "_arg0":Landroid/content/Intent;
    :goto_8
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->takeCall(Landroid/content/Intent;)V

    .line 579
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 580
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 576
    .end local v1    # "_arg0":Landroid/content/Intent;
    :cond_7
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/content/Intent;
    goto :goto_8

    .line 584
    .end local v1    # "_arg0":Landroid/content/Intent;
    :sswitch_38
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 586
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_8

    .line 587
    sget-object v0, Landroid/content/Intent;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Intent;

    .line 592
    .restart local v1    # "_arg0":Landroid/content/Intent;
    :goto_9
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->isIncomingCallIntent(Landroid/content/Intent;)Z

    move-result v8

    .line 593
    .local v8, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 594
    if-eqz v8, :cond_9

    const/4 v0, 0x1

    :goto_a
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 595
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 590
    .end local v1    # "_arg0":Landroid/content/Intent;
    .end local v8    # "_result":Z
    :cond_8
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/content/Intent;
    goto :goto_9

    .line 594
    .restart local v8    # "_result":Z
    :cond_9
    const/4 v0, 0x0

    goto :goto_a

    .line 599
    .end local v1    # "_arg0":Landroid/content/Intent;
    .end local v8    # "_result":Z
    :sswitch_39
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 601
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 602
    .local v1, "_arg0":I
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->isInCall(I)Z

    move-result v8

    .line 603
    .restart local v8    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 604
    if-eqz v8, :cond_a

    const/4 v0, 0x1

    :goto_b
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 605
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 604
    :cond_a
    const/4 v0, 0x0

    goto :goto_b

    .line 609
    .end local v1    # "_arg0":I
    .end local v8    # "_result":Z
    :sswitch_3a
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 611
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 612
    .restart local v1    # "_arg0":I
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->isOnHold(I)Z

    move-result v8

    .line 613
    .restart local v8    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 614
    if-eqz v8, :cond_b

    const/4 v0, 0x1

    :goto_c
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 615
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 614
    :cond_b
    const/4 v0, 0x0

    goto :goto_c

    .line 619
    .end local v1    # "_arg0":I
    .end local v8    # "_result":Z
    :sswitch_3b
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 621
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 622
    .restart local v1    # "_arg0":I
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->startAudio(I)V

    .line 623
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 627
    .end local v1    # "_arg0":I
    :sswitch_3c
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 629
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 630
    .restart local v1    # "_arg0":I
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->getCallType(I)I

    move-result v8

    .line 631
    .local v8, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 632
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    .line 633
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 637
    .end local v1    # "_arg0":I
    .end local v8    # "_result":I
    :sswitch_3d
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 639
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 640
    .restart local v1    # "_arg0":I
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->getState(I)I

    move-result v8

    .line 641
    .restart local v8    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 642
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    .line 643
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 647
    .end local v1    # "_arg0":I
    .end local v8    # "_result":I
    :sswitch_3e
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 649
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 650
    .restart local v1    # "_arg0":I
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->hashCode(I)I

    move-result v8

    .line 651
    .restart local v8    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 652
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    .line 653
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 657
    .end local v1    # "_arg0":I
    .end local v8    # "_result":I
    :sswitch_3f
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 659
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 660
    .restart local v1    # "_arg0":I
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->getSessionID(I)I

    move-result v8

    .line 661
    .restart local v8    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 662
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    .line 663
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 667
    .end local v1    # "_arg0":I
    .end local v8    # "_result":I
    :sswitch_40
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 668
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->isInEPDGCall()Z

    move-result v8

    .line 669
    .local v8, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 670
    if-eqz v8, :cond_c

    const/4 v0, 0x1

    :goto_d
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 671
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 670
    :cond_c
    const/4 v0, 0x0

    goto :goto_d

    .line 675
    .end local v8    # "_result":Z
    :sswitch_41
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 677
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_d

    const/4 v1, 0x1

    .line 679
    .local v1, "_arg0":Z
    :goto_e
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 681
    .restart local v2    # "_arg1":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 683
    .restart local v3    # "_arg2":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    .line 685
    .local v4, "_arg3":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v5

    .restart local v5    # "_arg4":I
    move-object v0, p0

    .line 686
    invoke-virtual/range {v0 .. v5}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->updateSSACInfo(ZIIII)V

    .line 687
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 688
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 677
    .end local v1    # "_arg0":Z
    .end local v2    # "_arg1":I
    .end local v3    # "_arg2":I
    .end local v4    # "_arg3":I
    .end local v5    # "_arg4":I
    :cond_d
    const/4 v1, 0x0

    goto :goto_e

    .line 692
    :sswitch_42
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 693
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->send180Ringing()V

    .line 694
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 695
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 699
    :sswitch_43
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 701
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 702
    .local v1, "_arg0":I
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->changeEPDGAudioPath(I)V

    .line 703
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 704
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 708
    .end local v1    # "_arg0":I
    :sswitch_44
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 710
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 711
    .restart local v1    # "_arg0":I
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->isCallBarred(I)Z

    move-result v8

    .line 712
    .restart local v8    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 713
    if-eqz v8, :cond_e

    const/4 v0, 0x1

    :goto_f
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 714
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 713
    :cond_e
    const/4 v0, 0x0

    goto :goto_f

    .line 718
    .end local v1    # "_arg0":I
    .end local v8    # "_result":Z
    :sswitch_45
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 720
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;

    move-result-object v1

    .line 721
    .local v1, "_arg0":Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->registerForConnectivityStateChange(Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;)V

    .line 722
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 723
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 727
    .end local v1    # "_arg0":Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;
    :sswitch_46
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 729
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;

    move-result-object v1

    .line 730
    .restart local v1    # "_arg0":Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->deRegisterForConnectivityStateChange(Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;)V

    .line 731
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 732
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 736
    .end local v1    # "_arg0":Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;
    :sswitch_47
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 738
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;

    move-result-object v1

    .line 739
    .restart local v1    # "_arg0":Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->registerForRegisterStateChange(Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;)V

    .line 740
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 741
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 745
    .end local v1    # "_arg0":Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;
    :sswitch_48
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 747
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;

    move-result-object v1

    .line 748
    .restart local v1    # "_arg0":Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->deRegisterForRegisterStateChange(Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;)V

    .line 749
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 750
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 754
    .end local v1    # "_arg0":Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;
    :sswitch_49
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 756
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;

    move-result-object v1

    .line 757
    .restart local v1    # "_arg0":Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->registerForSettingStateListener(Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;)V

    .line 758
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 759
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 763
    .end local v1    # "_arg0":Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;
    :sswitch_4a
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 765
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;

    move-result-object v1

    .line 766
    .restart local v1    # "_arg0":Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->deRegisterForSettingStateListener(Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;)V

    .line 767
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 768
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 772
    .end local v1    # "_arg0":Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;
    :sswitch_4b
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 774
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;

    move-result-object v1

    .line 775
    .restart local v1    # "_arg0":Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->registerForAPCSStateChange(Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;)V

    .line 776
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 777
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 781
    .end local v1    # "_arg0":Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;
    :sswitch_4c
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 783
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;

    move-result-object v1

    .line 784
    .restart local v1    # "_arg0":Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->deRegisterForAPCSStateChange(Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;)V

    .line 785
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 786
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 790
    .end local v1    # "_arg0":Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;
    :sswitch_4d
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 791
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->isRegistered()Z

    move-result v8

    .line 792
    .restart local v8    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 793
    if-eqz v8, :cond_f

    const/4 v0, 0x1

    :goto_10
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 794
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 793
    :cond_f
    const/4 v0, 0x0

    goto :goto_10

    .line 798
    .end local v8    # "_result":Z
    :sswitch_4e
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 799
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->isRegisteredToMobile()Z

    move-result v8

    .line 800
    .restart local v8    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 801
    if-eqz v8, :cond_10

    const/4 v0, 0x1

    :goto_11
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 802
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 801
    :cond_10
    const/4 v0, 0x0

    goto :goto_11

    .line 806
    .end local v8    # "_result":Z
    :sswitch_4f
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 807
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->isRegisteringToMobile()Z

    move-result v8

    .line 808
    .restart local v8    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 809
    if-eqz v8, :cond_11

    const/4 v0, 0x1

    :goto_12
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 810
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 809
    :cond_11
    const/4 v0, 0x0

    goto :goto_12

    .line 814
    .end local v8    # "_result":Z
    :sswitch_50
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 815
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->isDeregisteredToMobile()Z

    move-result v8

    .line 816
    .restart local v8    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 817
    if-eqz v8, :cond_12

    const/4 v0, 0x1

    :goto_13
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 818
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 817
    :cond_12
    const/4 v0, 0x0

    goto :goto_13

    .line 822
    .end local v8    # "_result":Z
    :sswitch_51
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 823
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->isDeregisteringToMobile()Z

    move-result v8

    .line 824
    .restart local v8    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 825
    if-eqz v8, :cond_13

    const/4 v0, 0x1

    :goto_14
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 826
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 825
    :cond_13
    const/4 v0, 0x0

    goto :goto_14

    .line 830
    .end local v8    # "_result":Z
    :sswitch_52
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 831
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->isRegisteredToWiFi()Z

    move-result v8

    .line 832
    .restart local v8    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 833
    if-eqz v8, :cond_14

    const/4 v0, 0x1

    :goto_15
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 834
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 833
    :cond_14
    const/4 v0, 0x0

    goto :goto_15

    .line 838
    .end local v8    # "_result":Z
    :sswitch_53
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 839
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->isRegisteringToWiFi()Z

    move-result v8

    .line 840
    .restart local v8    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 841
    if-eqz v8, :cond_15

    const/4 v0, 0x1

    :goto_16
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 842
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 841
    :cond_15
    const/4 v0, 0x0

    goto :goto_16

    .line 846
    .end local v8    # "_result":Z
    :sswitch_54
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 847
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->isDeregisteredToWiFi()Z

    move-result v8

    .line 848
    .restart local v8    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 849
    if-eqz v8, :cond_16

    const/4 v0, 0x1

    :goto_17
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 850
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 849
    :cond_16
    const/4 v0, 0x0

    goto :goto_17

    .line 854
    .end local v8    # "_result":Z
    :sswitch_55
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 855
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->isDeregisteringToWiFi()Z

    move-result v8

    .line 856
    .restart local v8    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 857
    if-eqz v8, :cond_17

    const/4 v0, 0x1

    :goto_18
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 858
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 857
    :cond_17
    const/4 v0, 0x0

    goto :goto_18

    .line 862
    .end local v8    # "_result":Z
    :sswitch_56
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 863
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->isEnablingMobileData()Z

    move-result v8

    .line 864
    .restart local v8    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 865
    if-eqz v8, :cond_18

    const/4 v0, 0x1

    :goto_19
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 866
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 865
    :cond_18
    const/4 v0, 0x0

    goto :goto_19

    .line 870
    .end local v8    # "_result":Z
    :sswitch_57
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 871
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->isDisablingMobileData()Z

    move-result v8

    .line 872
    .restart local v8    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 873
    if-eqz v8, :cond_19

    const/4 v0, 0x1

    :goto_1a
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 874
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 873
    :cond_19
    const/4 v0, 0x0

    goto :goto_1a

    .line 878
    .end local v8    # "_result":Z
    :sswitch_58
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 879
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->getFeatureMask()I

    move-result v8

    .line 880
    .local v8, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 881
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    .line 882
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 886
    .end local v8    # "_result":I
    :sswitch_59
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 888
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 889
    .local v1, "_arg0":I
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->getErrorCode(I)I

    move-result v8

    .line 890
    .restart local v8    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 891
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    .line 892
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 896
    .end local v1    # "_arg0":I
    .end local v8    # "_result":I
    :sswitch_5a
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 897
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->getServiceStatus()Z

    move-result v8

    .line 898
    .local v8, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 899
    if-eqz v8, :cond_1a

    const/4 v0, 0x1

    :goto_1b
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 900
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 899
    :cond_1a
    const/4 v0, 0x0

    goto :goto_1b

    .line 904
    .end local v8    # "_result":Z
    :sswitch_5b
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 905
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->manualRegister()V

    .line 906
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 907
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 911
    :sswitch_5c
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 912
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->manualDeregister()V

    .line 913
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 914
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 918
    :sswitch_5d
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 920
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 921
    .restart local v1    # "_arg0":I
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->sendRegister(I)Z

    move-result v8

    .line 922
    .restart local v8    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 923
    if-eqz v8, :cond_1b

    const/4 v0, 0x1

    :goto_1c
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 924
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 923
    :cond_1b
    const/4 v0, 0x0

    goto :goto_1c

    .line 928
    .end local v1    # "_arg0":I
    .end local v8    # "_result":Z
    :sswitch_5e
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 930
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 931
    .restart local v1    # "_arg0":I
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->sendDeregister(I)Z

    move-result v8

    .line 932
    .restart local v8    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 933
    if-eqz v8, :cond_1c

    const/4 v0, 0x1

    :goto_1d
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 934
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 933
    :cond_1c
    const/4 v0, 0x0

    goto :goto_1d

    .line 938
    .end local v1    # "_arg0":I
    .end local v8    # "_result":Z
    :sswitch_5f
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 939
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->forceRestart()Z

    move-result v8

    .line 940
    .restart local v8    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 941
    if-eqz v8, :cond_1d

    const/4 v0, 0x1

    :goto_1e
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 942
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 941
    :cond_1d
    const/4 v0, 0x0

    goto :goto_1e

    .line 946
    .end local v8    # "_result":Z
    :sswitch_60
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 947
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->enableWiFiCalling()V

    .line 948
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 949
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 953
    :sswitch_61
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 954
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->disableWiFiCalling()V

    .line 955
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 956
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 960
    :sswitch_62
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 962
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 964
    .local v1, "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->createStringArray()[Ljava/lang/String;

    move-result-object v2

    .line 965
    .local v2, "_arg1":[Ljava/lang/String;
    invoke-virtual {p0, v1, v2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->setRegistrationFeatureTags(Ljava/lang/String;[Ljava/lang/String;)V

    .line 966
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 967
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 971
    .end local v1    # "_arg0":Ljava/lang/String;
    .end local v2    # "_arg1":[Ljava/lang/String;
    :sswitch_63
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 973
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 974
    .local v1, "_arg0":I
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->sendInitialRegister(I)V

    .line 975
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 976
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 980
    .end local v1    # "_arg0":I
    :sswitch_64
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 981
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->handleActionsOverEpdg()V

    .line 982
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 983
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 987
    :sswitch_65
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 988
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->setVoLTEFeatureEnable()V

    .line 989
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 990
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 994
    :sswitch_66
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 995
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->setVoLTEFeatureDisable()V

    .line 996
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 997
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 1001
    :sswitch_67
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1002
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->isVoLTEFeatureEnabled()Z

    move-result v8

    .line 1003
    .restart local v8    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1004
    if-eqz v8, :cond_1e

    const/4 v0, 0x1

    :goto_1f
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1005
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 1004
    :cond_1e
    const/4 v0, 0x0

    goto :goto_1f

    .line 1009
    .end local v8    # "_result":Z
    :sswitch_68
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1010
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->setLTEVideoCallEnable()V

    .line 1011
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1012
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 1016
    :sswitch_69
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1017
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->setLTEVideoCallDisable()V

    .line 1018
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1019
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 1023
    :sswitch_6a
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1024
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->isLTEVideoCallEnabled()Z

    move-result v8

    .line 1025
    .restart local v8    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1026
    if-eqz v8, :cond_1f

    const/4 v0, 0x1

    :goto_20
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1027
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 1026
    :cond_1f
    const/4 v0, 0x0

    goto :goto_20

    .line 1031
    .end local v8    # "_result":Z
    :sswitch_6b
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1032
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->isVoLTEMenuShow()Z

    move-result v8

    .line 1033
    .restart local v8    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1034
    if-eqz v8, :cond_20

    const/4 v0, 0x1

    :goto_21
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1035
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 1034
    :cond_20
    const/4 v0, 0x0

    goto :goto_21

    .line 1039
    .end local v8    # "_result":Z
    :sswitch_6c
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1040
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->isLTEVideoCallMenuShow()Z

    move-result v8

    .line 1041
    .restart local v8    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1042
    if-eqz v8, :cond_21

    const/4 v0, 0x1

    :goto_22
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1043
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 1042
    :cond_21
    const/4 v0, 0x0

    goto :goto_22

    .line 1047
    .end local v8    # "_result":Z
    :sswitch_6d
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1048
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->isEABMenuShow()Z

    move-result v8

    .line 1049
    .restart local v8    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1050
    if-eqz v8, :cond_22

    const/4 v0, 0x1

    :goto_23
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1051
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 1050
    :cond_22
    const/4 v0, 0x0

    goto :goto_23

    .line 1055
    .end local v8    # "_result":Z
    :sswitch_6e
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1057
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_23

    const/4 v1, 0x1

    .line 1058
    .local v1, "_arg0":Z
    :goto_24
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->setThirdPartyMode(Z)V

    .line 1059
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1060
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 1057
    .end local v1    # "_arg0":Z
    :cond_23
    const/4 v1, 0x0

    goto :goto_24

    .line 1064
    :sswitch_6f
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1066
    invoke-virtual {p2}, Landroid/os/Parcel;->createIntArray()[I

    move-result-object v1

    .line 1068
    .local v1, "_arg0":[I
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v9

    .line 1069
    .local v9, "cl":Ljava/lang/ClassLoader;
    invoke-virtual {p2, v9}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v2

    .line 1071
    .local v2, "_arg1":Ljava/util/List;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 1072
    .restart local v3    # "_arg2":I
    invoke-virtual {p0, v1, v2, v3}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->getIMSSettingValues([ILjava/util/List;I)Lcom/samsung/commonimsinterface/imscommon/IMSParameter;

    move-result-object v8

    .line 1073
    .local v8, "_result":Lcom/samsung/commonimsinterface/imscommon/IMSParameter;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1074
    if-eqz v8, :cond_24

    .line 1075
    const/4 v0, 0x1

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1076
    const/4 v0, 0x1

    invoke-virtual {v8, p3, v0}, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;->writeToParcel(Landroid/os/Parcel;I)V

    .line 1081
    :goto_25
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 1079
    :cond_24
    const/4 v0, 0x0

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_25

    .line 1085
    .end local v1    # "_arg0":[I
    .end local v2    # "_arg1":Ljava/util/List;
    .end local v3    # "_arg2":I
    .end local v8    # "_result":Lcom/samsung/commonimsinterface/imscommon/IMSParameter;
    .end local v9    # "cl":Ljava/lang/ClassLoader;
    :sswitch_70
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1087
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v9

    .line 1088
    .restart local v9    # "cl":Ljava/lang/ClassLoader;
    invoke-virtual {p2, v9}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v1

    .line 1090
    .local v1, "_arg0":Ljava/util/List;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 1091
    .local v2, "_arg1":I
    invoke-virtual {p0, v1, v2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->getIMSSettingValuesWithUri(Ljava/util/List;I)Lcom/samsung/commonimsinterface/imscommon/IMSParameter;

    move-result-object v8

    .line 1092
    .restart local v8    # "_result":Lcom/samsung/commonimsinterface/imscommon/IMSParameter;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1093
    if-eqz v8, :cond_25

    .line 1094
    const/4 v0, 0x1

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1095
    const/4 v0, 0x1

    invoke-virtual {v8, p3, v0}, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;->writeToParcel(Landroid/os/Parcel;I)V

    .line 1100
    :goto_26
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 1098
    :cond_25
    const/4 v0, 0x0

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_26

    .line 1104
    .end local v1    # "_arg0":Ljava/util/List;
    .end local v2    # "_arg1":I
    .end local v8    # "_result":Lcom/samsung/commonimsinterface/imscommon/IMSParameter;
    .end local v9    # "cl":Ljava/lang/ClassLoader;
    :sswitch_71
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1106
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_26

    .line 1107
    sget-object v0, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/Bundle;

    .line 1113
    .local v1, "_arg0":Landroid/os/Bundle;
    :goto_27
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_27

    .line 1114
    sget-object v0, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/Bundle;

    .line 1120
    .local v2, "_arg1":Landroid/os/Bundle;
    :goto_28
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 1121
    .restart local v3    # "_arg2":I
    invoke-virtual {p0, v1, v2, v3}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->updateIMSSettingValues(Landroid/os/Bundle;Landroid/os/Bundle;I)Z

    move-result v8

    .line 1122
    .local v8, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1123
    if-eqz v8, :cond_28

    const/4 v0, 0x1

    :goto_29
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1124
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 1110
    .end local v1    # "_arg0":Landroid/os/Bundle;
    .end local v2    # "_arg1":Landroid/os/Bundle;
    .end local v3    # "_arg2":I
    .end local v8    # "_result":Z
    :cond_26
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/os/Bundle;
    goto :goto_27

    .line 1117
    :cond_27
    const/4 v2, 0x0

    .restart local v2    # "_arg1":Landroid/os/Bundle;
    goto :goto_28

    .line 1123
    .restart local v3    # "_arg2":I
    .restart local v8    # "_result":Z
    :cond_28
    const/4 v0, 0x0

    goto :goto_29

    .line 1128
    .end local v1    # "_arg0":Landroid/os/Bundle;
    .end local v2    # "_arg1":Landroid/os/Bundle;
    .end local v3    # "_arg2":I
    .end local v8    # "_result":Z
    :sswitch_72
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1130
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 1132
    .local v1, "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 1134
    .local v2, "_arg1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 1135
    .restart local v3    # "_arg2":I
    invoke-virtual {p0, v1, v2, v3}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->updateIMSSettingValuesWithUri(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v8

    .line 1136
    .restart local v8    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1137
    if-eqz v8, :cond_29

    const/4 v0, 0x1

    :goto_2a
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1138
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 1137
    :cond_29
    const/4 v0, 0x0

    goto :goto_2a

    .line 1142
    .end local v1    # "_arg0":Ljava/lang/String;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v3    # "_arg2":I
    .end local v8    # "_result":Z
    :sswitch_73
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1144
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v9

    .line 1145
    .restart local v9    # "cl":Ljava/lang/ClassLoader;
    invoke-virtual {p2, v9}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v1

    .line 1146
    .local v1, "_arg0":Ljava/util/List;
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->getIMSSettingFileEnumByUri(Ljava/util/List;)Lcom/samsung/commonimsinterface/imscommon/IMSParameter;

    move-result-object v8

    .line 1147
    .local v8, "_result":Lcom/samsung/commonimsinterface/imscommon/IMSParameter;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1148
    if-eqz v8, :cond_2a

    .line 1149
    const/4 v0, 0x1

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1150
    const/4 v0, 0x1

    invoke-virtual {v8, p3, v0}, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;->writeToParcel(Landroid/os/Parcel;I)V

    .line 1155
    :goto_2b
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 1153
    :cond_2a
    const/4 v0, 0x0

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_2b

    .line 1159
    .end local v1    # "_arg0":Ljava/util/List;
    .end local v8    # "_result":Lcom/samsung/commonimsinterface/imscommon/IMSParameter;
    .end local v9    # "cl":Ljava/lang/ClassLoader;
    :sswitch_74
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1160
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->startIMSSettingTransaction()I

    move-result v8

    .line 1161
    .local v8, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1162
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    .line 1163
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 1167
    .end local v8    # "_result":I
    :sswitch_75
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1169
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 1170
    .local v1, "_arg0":I
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->endIMSSettingTransaction(I)V

    .line 1171
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1172
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 1176
    .end local v1    # "_arg0":I
    :sswitch_76
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1178
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 1179
    .restart local v1    # "_arg0":I
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->abortIMSSettingTransaction(I)V

    .line 1180
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1181
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 1185
    .end local v1    # "_arg0":I
    :sswitch_77
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1187
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 1188
    .restart local v1    # "_arg0":I
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->isLimitedMode(I)Z

    move-result v8

    .line 1189
    .local v8, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1190
    if-eqz v8, :cond_2b

    const/4 v0, 0x1

    :goto_2c
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1191
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 1190
    :cond_2b
    const/4 v0, 0x0

    goto :goto_2c

    .line 1195
    .end local v1    # "_arg0":I
    .end local v8    # "_result":Z
    :sswitch_78
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1197
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 1198
    .restart local v1    # "_arg0":I
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->startT3402TimerValue(I)V

    .line 1199
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1200
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 1204
    .end local v1    # "_arg0":I
    :sswitch_79
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1206
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;

    move-result-object v1

    .line 1207
    .local v1, "_arg0":Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->registerForMediaStateChange(Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;)V

    .line 1208
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1209
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 1213
    .end local v1    # "_arg0":Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;
    :sswitch_7a
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1215
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;

    move-result-object v1

    .line 1216
    .restart local v1    # "_arg0":Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->deRegisterForMediaStateChange(Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;)V

    .line 1217
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1218
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 1222
    .end local v1    # "_arg0":Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;
    :sswitch_7b
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1223
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->mediaInit()V

    .line 1224
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1225
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 1229
    :sswitch_7c
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1230
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->mediaDeInit()V

    .line 1231
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1232
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 1236
    :sswitch_7d
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1238
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_2c

    .line 1239
    sget-object v0, Landroid/view/Surface;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/Surface;

    .line 1245
    .local v1, "_arg0":Landroid/view/Surface;
    :goto_2d
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 1247
    .local v2, "_arg1":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 1248
    .restart local v3    # "_arg2":I
    invoke-virtual {p0, v1, v2, v3}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->setDisplay(Landroid/view/Surface;II)V

    .line 1249
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1250
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 1242
    .end local v1    # "_arg0":Landroid/view/Surface;
    .end local v2    # "_arg1":I
    .end local v3    # "_arg2":I
    :cond_2c
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/view/Surface;
    goto :goto_2d

    .line 1254
    .end local v1    # "_arg0":Landroid/view/Surface;
    :sswitch_7e
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1256
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_2d

    .line 1257
    sget-object v0, Landroid/view/Surface;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/Surface;

    .line 1263
    .restart local v1    # "_arg0":Landroid/view/Surface;
    :goto_2e
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 1265
    .restart local v2    # "_arg1":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 1266
    .restart local v3    # "_arg2":I
    invoke-virtual {p0, v1, v2, v3}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->setFarEndSurface(Landroid/view/Surface;II)V

    .line 1267
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1268
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 1260
    .end local v1    # "_arg0":Landroid/view/Surface;
    .end local v2    # "_arg1":I
    .end local v3    # "_arg2":I
    :cond_2d
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/view/Surface;
    goto :goto_2e

    .line 1272
    .end local v1    # "_arg0":Landroid/view/Surface;
    :sswitch_7f
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1273
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->getNegotiatedHeight()I

    move-result v8

    .line 1274
    .local v8, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1275
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    .line 1276
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 1280
    .end local v8    # "_result":I
    :sswitch_80
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1281
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->getNegotiatedWidth()I

    move-result v8

    .line 1282
    .restart local v8    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1283
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    .line 1284
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 1288
    .end local v8    # "_result":I
    :sswitch_81
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1289
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->getNegotiatedFPS()I

    move-result v8

    .line 1290
    .restart local v8    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1291
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    .line 1292
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 1296
    .end local v8    # "_result":I
    :sswitch_82
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1297
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->isMediaReadyToReceivePreview()Z

    move-result v8

    .line 1298
    .local v8, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1299
    if-eqz v8, :cond_2e

    const/4 v0, 0x1

    :goto_2f
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1300
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 1299
    :cond_2e
    const/4 v0, 0x0

    goto :goto_2f

    .line 1304
    .end local v8    # "_result":Z
    :sswitch_83
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1306
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_2f

    const/4 v1, 0x1

    .line 1307
    .local v1, "_arg0":Z
    :goto_30
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->setIsMediaReadyToReceivePreview(Z)V

    .line 1308
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1309
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 1306
    .end local v1    # "_arg0":Z
    :cond_2f
    const/4 v1, 0x0

    goto :goto_30

    .line 1313
    :sswitch_84
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1314
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->getNumberOfCameras()I

    move-result v8

    .line 1315
    .local v8, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1316
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    .line 1317
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 1321
    .end local v8    # "_result":I
    :sswitch_85
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1323
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 1324
    .local v1, "_arg0":I
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->openCamera(I)Z

    move-result v8

    .line 1325
    .local v8, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1326
    if-eqz v8, :cond_30

    const/4 v0, 0x1

    :goto_31
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1327
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 1326
    :cond_30
    const/4 v0, 0x0

    goto :goto_31

    .line 1331
    .end local v1    # "_arg0":I
    .end local v8    # "_result":Z
    :sswitch_86
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1333
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 1335
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 1336
    .restart local v2    # "_arg1":I
    invoke-virtual {p0, v1, v2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->openCameraEx(II)Z

    move-result v8

    .line 1337
    .restart local v8    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1338
    if-eqz v8, :cond_31

    const/4 v0, 0x1

    :goto_32
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1339
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 1338
    :cond_31
    const/4 v0, 0x0

    goto :goto_32

    .line 1343
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":I
    .end local v8    # "_result":Z
    :sswitch_87
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1345
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_32

    .line 1346
    sget-object v0, Landroid/view/Surface;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/Surface;

    .line 1351
    .local v1, "_arg0":Landroid/view/Surface;
    :goto_33
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->startCameraPreview(Landroid/view/Surface;)V

    .line 1352
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1353
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 1349
    .end local v1    # "_arg0":Landroid/view/Surface;
    :cond_32
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/view/Surface;
    goto :goto_33

    .line 1357
    .end local v1    # "_arg0":Landroid/view/Surface;
    :sswitch_88
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1358
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->stopCameraPreview()V

    .line 1359
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1360
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 1364
    :sswitch_89
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1365
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->closeCamera()V

    .line 1366
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1367
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 1371
    :sswitch_8a
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1372
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->getBackCameraId()I

    move-result v8

    .line 1373
    .local v8, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1374
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    .line 1375
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 1379
    .end local v8    # "_result":I
    :sswitch_8b
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1380
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->getFrontCameraId()I

    move-result v8

    .line 1381
    .restart local v8    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1382
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    .line 1383
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 1387
    .end local v8    # "_result":I
    :sswitch_8c
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1388
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->getCameraState()I

    move-result v8

    .line 1389
    .restart local v8    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1390
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    .line 1391
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 1395
    .end local v8    # "_result":I
    :sswitch_8d
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1396
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->getCameraDirection()I

    move-result v8

    .line 1397
    .restart local v8    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1398
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    .line 1399
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 1403
    .end local v8    # "_result":I
    :sswitch_8e
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1404
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->setCameraDisplayOrientation()V

    .line 1405
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1406
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 1410
    :sswitch_8f
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1412
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 1413
    .local v1, "_arg0":I
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->setCameraDisplayOrientationEx(I)V

    .line 1414
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1415
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 1419
    .end local v1    # "_arg0":I
    :sswitch_90
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1420
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->switchCamera()V

    .line 1421
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1422
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 1426
    :sswitch_91
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1428
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_33

    const/4 v1, 0x1

    .line 1429
    .local v1, "_arg0":Z
    :goto_34
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->startRender(Z)V

    .line 1430
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 1428
    .end local v1    # "_arg0":Z
    :cond_33
    const/4 v1, 0x0

    goto :goto_34

    .line 1434
    :sswitch_92
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1435
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->swapVideoSurface()V

    .line 1436
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1437
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 1441
    :sswitch_93
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1442
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->resetCameraID()V

    .line 1443
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1444
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 1448
    :sswitch_94
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1450
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 1452
    .local v1, "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 1454
    .restart local v2    # "_arg1":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 1456
    .local v3, "_arg2":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    .line 1457
    .restart local v4    # "_arg3":I
    invoke-virtual {p0, v1, v2, v3, v4}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->sendStillImage(Ljava/lang/String;ILjava/lang/String;I)V

    .line 1458
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1459
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 1463
    .end local v1    # "_arg0":Ljava/lang/String;
    .end local v2    # "_arg1":I
    .end local v3    # "_arg2":Ljava/lang/String;
    .end local v4    # "_arg3":I
    :sswitch_95
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1465
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 1466
    .local v1, "_arg0":I
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->setCameraEffect(I)V

    .line 1467
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1468
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 1472
    .end local v1    # "_arg0":I
    :sswitch_96
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1473
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->sendLiveVideo()V

    .line 1474
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1475
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 1479
    :sswitch_97
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1481
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_34

    const/4 v1, 0x1

    .line 1483
    .local v1, "_arg0":Z
    :goto_35
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 1484
    .restart local v2    # "_arg1":I
    invoke-virtual {p0, v1, v2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->captureSurfaceImage(ZI)V

    .line 1485
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1486
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 1481
    .end local v1    # "_arg0":Z
    .end local v2    # "_arg1":I
    :cond_34
    const/4 v1, 0x0

    goto :goto_35

    .line 1490
    :sswitch_98
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1492
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_35

    const/4 v1, 0x1

    .line 1493
    .restart local v1    # "_arg0":Z
    :goto_36
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->deinitSurface(Z)V

    .line 1494
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1495
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 1492
    .end local v1    # "_arg0":Z
    :cond_35
    const/4 v1, 0x0

    goto :goto_36

    .line 1499
    :sswitch_99
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1500
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->holdVideo()V

    .line 1501
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1502
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 1506
    :sswitch_9a
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1507
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->resumeVideo()V

    .line 1508
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1509
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 1513
    :sswitch_9b
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1515
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 1516
    .local v1, "_arg0":I
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->startVideo(I)V

    .line 1517
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 1521
    .end local v1    # "_arg0":I
    :sswitch_9c
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1522
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->getMaxZoom()I

    move-result v8

    .line 1523
    .restart local v8    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1524
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    .line 1525
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 1529
    .end local v8    # "_result":I
    :sswitch_9d
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1531
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 1532
    .restart local v1    # "_arg0":I
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->setZoom(I)V

    .line 1533
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1534
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 1538
    .end local v1    # "_arg0":I
    :sswitch_9e
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1539
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->getZoom()I

    move-result v8

    .line 1540
    .restart local v8    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1541
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    .line 1542
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 1546
    .end local v8    # "_result":I
    :sswitch_9f
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1548
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;

    move-result-object v1

    .line 1549
    .local v1, "_arg0":Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->registerForPresenceStateChange(Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;)V

    .line 1550
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1551
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 1555
    .end local v1    # "_arg0":Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;
    :sswitch_a0
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1557
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;

    move-result-object v1

    .line 1558
    .restart local v1    # "_arg0":Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->deRegisterForPresenceStateChange(Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;)V

    .line 1559
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1560
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 1564
    .end local v1    # "_arg0":Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;
    :sswitch_a1
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1566
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 1568
    .local v1, "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_36

    .line 1569
    sget-object v0, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/Bundle;

    .line 1574
    .local v2, "_arg1":Landroid/os/Bundle;
    :goto_37
    invoke-virtual {p0, v1, v2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->publishCapabilitiesAndAvailability(Ljava/lang/String;Landroid/os/Bundle;)Z

    move-result v8

    .line 1575
    .local v8, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1576
    if-eqz v8, :cond_37

    const/4 v0, 0x1

    :goto_38
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1577
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 1572
    .end local v2    # "_arg1":Landroid/os/Bundle;
    .end local v8    # "_result":Z
    :cond_36
    const/4 v2, 0x0

    .restart local v2    # "_arg1":Landroid/os/Bundle;
    goto :goto_37

    .line 1576
    .restart local v8    # "_result":Z
    :cond_37
    const/4 v0, 0x0

    goto :goto_38

    .line 1581
    .end local v1    # "_arg0":Ljava/lang/String;
    .end local v2    # "_arg1":Landroid/os/Bundle;
    .end local v8    # "_result":Z
    :sswitch_a2
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1582
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->unpublishCapabilitiesAndAvailability()Z

    move-result v8

    .line 1583
    .restart local v8    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1584
    if-eqz v8, :cond_38

    const/4 v0, 0x1

    :goto_39
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1585
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 1584
    :cond_38
    const/4 v0, 0x0

    goto :goto_39

    .line 1589
    .end local v8    # "_result":Z
    :sswitch_a3
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1591
    invoke-virtual {p2}, Landroid/os/Parcel;->createStringArray()[Ljava/lang/String;

    move-result-object v1

    .line 1593
    .local v1, "_arg0":[Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 1594
    .local v2, "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v1, v2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->getRemoteCapabilitiesAndAvailability([Ljava/lang/String;Ljava/lang/String;)Z

    move-result v8

    .line 1595
    .restart local v8    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1596
    if-eqz v8, :cond_39

    const/4 v0, 0x1

    :goto_3a
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1597
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 1596
    :cond_39
    const/4 v0, 0x0

    goto :goto_3a

    .line 1601
    .end local v1    # "_arg0":[Ljava/lang/String;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v8    # "_result":Z
    :sswitch_a4
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1603
    invoke-virtual {p2}, Landroid/os/Parcel;->createStringArray()[Ljava/lang/String;

    move-result-object v1

    .line 1605
    .restart local v1    # "_arg0":[Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 1607
    .restart local v2    # "_arg1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 1609
    .restart local v3    # "_arg2":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_3a

    const/4 v4, 0x1

    .line 1610
    .local v4, "_arg3":Z
    :goto_3b
    invoke-virtual {p0, v1, v2, v3, v4}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->getRemoteCapabilitiesAndAvailabilityPoll([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v8

    .line 1611
    .restart local v8    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1612
    if-eqz v8, :cond_3b

    const/4 v0, 0x1

    :goto_3c
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1613
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 1609
    .end local v4    # "_arg3":Z
    .end local v8    # "_result":Z
    :cond_3a
    const/4 v4, 0x0

    goto :goto_3b

    .line 1612
    .restart local v4    # "_arg3":Z
    .restart local v8    # "_result":Z
    :cond_3b
    const/4 v0, 0x0

    goto :goto_3c

    .line 1617
    .end local v1    # "_arg0":[Ljava/lang/String;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v3    # "_arg2":Ljava/lang/String;
    .end local v4    # "_arg3":Z
    .end local v8    # "_result":Z
    :sswitch_a5
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1619
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 1621
    .local v1, "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 1622
    .restart local v2    # "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v1, v2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->registerServiceCapabilityAvailabilityTemplate(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v8

    .line 1623
    .restart local v8    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1624
    if-eqz v8, :cond_3c

    const/4 v0, 0x1

    :goto_3d
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1625
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 1624
    :cond_3c
    const/4 v0, 0x0

    goto :goto_3d

    .line 1629
    .end local v1    # "_arg0":Ljava/lang/String;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v8    # "_result":Z
    :sswitch_a6
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1631
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 1633
    .local v1, "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 1635
    .local v2, "_arg1":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 1637
    .local v3, "_arg2":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 1638
    .local v4, "_arg3":Ljava/lang/String;
    invoke-virtual {p0, v1, v2, v3, v4}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->contactSvcCallFunction(IIILjava/lang/String;)V

    .line 1639
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 1643
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":I
    .end local v3    # "_arg2":I
    .end local v4    # "_arg3":Ljava/lang/String;
    :sswitch_a7
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1644
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->isProvisioned()Z

    move-result v8

    .line 1645
    .restart local v8    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1646
    if-eqz v8, :cond_3d

    const/4 v0, 0x1

    :goto_3e
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1647
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 1646
    :cond_3d
    const/4 v0, 0x0

    goto :goto_3e

    .line 1651
    .end local v8    # "_result":Z
    :sswitch_a8
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1653
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;

    move-result-object v1

    .line 1654
    .local v1, "_arg0":Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->registerForSMSStateChange(Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;)V

    .line 1655
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1656
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 1660
    .end local v1    # "_arg0":Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;
    :sswitch_a9
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1662
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;

    move-result-object v1

    .line 1663
    .restart local v1    # "_arg0":Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->deRegisterForSMSStateChange(Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;)V

    .line 1664
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1665
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 1669
    .end local v1    # "_arg0":Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;
    :sswitch_aa
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1671
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v1

    .line 1673
    .local v1, "_arg0":[B
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 1675
    .local v2, "_arg1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 1677
    .local v3, "_arg2":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    .line 1678
    .local v4, "_arg3":I
    invoke-virtual {p0, v1, v2, v3, v4}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->sendSMSOverIMS([BLjava/lang/String;Ljava/lang/String;I)V

    .line 1679
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 1683
    .end local v1    # "_arg0":[B
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v3    # "_arg2":Ljava/lang/String;
    .end local v4    # "_arg3":I
    :sswitch_ab
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1685
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_3e

    const/4 v1, 0x1

    .line 1687
    .local v1, "_arg0":Z
    :goto_3f
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 1688
    .local v2, "_arg1":I
    invoke-virtual {p0, v1, v2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->sendSMSResponse(ZI)V

    .line 1689
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 1685
    .end local v1    # "_arg0":Z
    .end local v2    # "_arg1":I
    :cond_3e
    const/4 v1, 0x0

    goto :goto_3f

    .line 1693
    :sswitch_ac
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1695
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v1

    .line 1696
    .local v1, "_arg0":[B
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->sendDeliverReport([B)V

    .line 1697
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 1701
    .end local v1    # "_arg0":[B
    :sswitch_ad
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1703
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 1704
    .local v1, "_arg0":Ljava/lang/String;
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->sendRPSMMA(Ljava/lang/String;)V

    .line 1705
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 1709
    .end local v1    # "_arg0":Ljava/lang/String;
    :sswitch_ae
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1711
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;

    move-result-object v1

    .line 1712
    .local v1, "_arg0":Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->registerForSSConfigStateChange(Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;)V

    .line 1713
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1714
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 1718
    .end local v1    # "_arg0":Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;
    :sswitch_af
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1720
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;

    move-result-object v1

    .line 1721
    .restart local v1    # "_arg0":Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->deRegisterForSSConfigStateChange(Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;)V

    .line 1722
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1723
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 1727
    .end local v1    # "_arg0":Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;
    :sswitch_b0
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1729
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 1731
    .local v1, "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 1732
    .restart local v2    # "_arg1":I
    invoke-virtual {p0, v1, v2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->setCallWaiting(II)I

    move-result v8

    .line 1733
    .local v8, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1734
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    .line 1735
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 1739
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":I
    .end local v8    # "_result":I
    :sswitch_b1
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1741
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 1742
    .restart local v1    # "_arg0":I
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->getCallWaiting(I)I

    move-result v8

    .line 1743
    .restart local v8    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1744
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    .line 1745
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 1749
    .end local v1    # "_arg0":I
    .end local v8    # "_result":I
    :sswitch_b2
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1751
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 1753
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 1755
    .restart local v2    # "_arg1":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 1757
    .local v3, "_arg2":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    .line 1759
    .restart local v4    # "_arg3":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v5

    .line 1761
    .restart local v5    # "_arg4":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v6

    .line 1763
    .local v6, "_arg5":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v7

    .restart local v7    # "_arg6":Ljava/lang/String;
    move-object v0, p0

    .line 1764
    invoke-virtual/range {v0 .. v7}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->setCallForwarding(IIIIIILjava/lang/String;)I

    move-result v8

    .line 1765
    .restart local v8    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1766
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    .line 1767
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 1771
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":I
    .end local v3    # "_arg2":I
    .end local v4    # "_arg3":I
    .end local v5    # "_arg4":I
    .end local v6    # "_arg5":I
    .end local v7    # "_arg6":Ljava/lang/String;
    .end local v8    # "_result":I
    :sswitch_b3
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1773
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 1775
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 1776
    .restart local v2    # "_arg1":I
    invoke-virtual {p0, v1, v2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->getCallForwarding(II)I

    move-result v8

    .line 1777
    .restart local v8    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1778
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    .line 1779
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 1783
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":I
    .end local v8    # "_result":I
    :sswitch_b4
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1785
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 1787
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 1789
    .restart local v2    # "_arg1":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 1791
    .restart local v3    # "_arg2":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 1792
    .local v4, "_arg3":Ljava/lang/String;
    invoke-virtual {p0, v1, v2, v3, v4}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->setCallBarring(IIILjava/lang/String;)I

    move-result v8

    .line 1793
    .restart local v8    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1794
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    .line 1795
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 1799
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":I
    .end local v3    # "_arg2":I
    .end local v4    # "_arg3":Ljava/lang/String;
    .end local v8    # "_result":I
    :sswitch_b5
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1801
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 1803
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 1804
    .restart local v2    # "_arg1":I
    invoke-virtual {p0, v1, v2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->getCallBarring(II)I

    move-result v8

    .line 1805
    .restart local v8    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1806
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    .line 1807
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 1811
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":I
    .end local v8    # "_result":I
    :sswitch_b6
    const-string v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1813
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 1815
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 1817
    .restart local v2    # "_arg1":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 1819
    .restart local v3    # "_arg2":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 1821
    .restart local v4    # "_arg3":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v5

    .line 1823
    .local v5, "_arg4":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v6

    .restart local v6    # "_arg5":I
    move-object v0, p0

    .line 1824
    invoke-virtual/range {v0 .. v6}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->mmSS_Svc_Api(IIILjava/lang/String;Ljava/lang/String;I)I

    move-result v8

    .line 1825
    .restart local v8    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1826
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    .line 1827
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 38
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x7 -> :sswitch_7
        0x8 -> :sswitch_8
        0x9 -> :sswitch_9
        0xa -> :sswitch_a
        0xb -> :sswitch_b
        0xc -> :sswitch_c
        0xd -> :sswitch_d
        0xe -> :sswitch_e
        0xf -> :sswitch_f
        0x10 -> :sswitch_10
        0x11 -> :sswitch_11
        0x12 -> :sswitch_12
        0x13 -> :sswitch_13
        0x14 -> :sswitch_14
        0x15 -> :sswitch_15
        0x16 -> :sswitch_16
        0x17 -> :sswitch_17
        0x18 -> :sswitch_18
        0x19 -> :sswitch_19
        0x1a -> :sswitch_1a
        0x1b -> :sswitch_1b
        0x1c -> :sswitch_1c
        0x1d -> :sswitch_1d
        0x1e -> :sswitch_1e
        0x1f -> :sswitch_1f
        0x20 -> :sswitch_20
        0x21 -> :sswitch_21
        0x22 -> :sswitch_22
        0x23 -> :sswitch_23
        0x24 -> :sswitch_24
        0x25 -> :sswitch_25
        0x26 -> :sswitch_26
        0x27 -> :sswitch_27
        0x28 -> :sswitch_28
        0x29 -> :sswitch_29
        0x2a -> :sswitch_2a
        0x2b -> :sswitch_2b
        0x2c -> :sswitch_2c
        0x2d -> :sswitch_2d
        0x2e -> :sswitch_2e
        0x2f -> :sswitch_2f
        0x30 -> :sswitch_30
        0x31 -> :sswitch_31
        0x32 -> :sswitch_32
        0x33 -> :sswitch_33
        0x34 -> :sswitch_34
        0x35 -> :sswitch_35
        0x36 -> :sswitch_36
        0x37 -> :sswitch_37
        0x38 -> :sswitch_38
        0x39 -> :sswitch_39
        0x3a -> :sswitch_3a
        0x3b -> :sswitch_3b
        0x3c -> :sswitch_3c
        0x3d -> :sswitch_3d
        0x3e -> :sswitch_3e
        0x3f -> :sswitch_3f
        0x40 -> :sswitch_40
        0x41 -> :sswitch_41
        0x42 -> :sswitch_42
        0x43 -> :sswitch_43
        0x44 -> :sswitch_44
        0x45 -> :sswitch_45
        0x46 -> :sswitch_46
        0x47 -> :sswitch_47
        0x48 -> :sswitch_48
        0x49 -> :sswitch_49
        0x4a -> :sswitch_4a
        0x4b -> :sswitch_4b
        0x4c -> :sswitch_4c
        0x4d -> :sswitch_4d
        0x4e -> :sswitch_4e
        0x4f -> :sswitch_4f
        0x50 -> :sswitch_50
        0x51 -> :sswitch_51
        0x52 -> :sswitch_52
        0x53 -> :sswitch_53
        0x54 -> :sswitch_54
        0x55 -> :sswitch_55
        0x56 -> :sswitch_56
        0x57 -> :sswitch_57
        0x58 -> :sswitch_58
        0x59 -> :sswitch_59
        0x5a -> :sswitch_5a
        0x5b -> :sswitch_5b
        0x5c -> :sswitch_5c
        0x5d -> :sswitch_5d
        0x5e -> :sswitch_5e
        0x5f -> :sswitch_5f
        0x60 -> :sswitch_60
        0x61 -> :sswitch_61
        0x62 -> :sswitch_62
        0x63 -> :sswitch_63
        0x64 -> :sswitch_64
        0x65 -> :sswitch_65
        0x66 -> :sswitch_66
        0x67 -> :sswitch_67
        0x68 -> :sswitch_68
        0x69 -> :sswitch_69
        0x6a -> :sswitch_6a
        0x6b -> :sswitch_6b
        0x6c -> :sswitch_6c
        0x6d -> :sswitch_6d
        0x6e -> :sswitch_6e
        0x6f -> :sswitch_6f
        0x70 -> :sswitch_70
        0x71 -> :sswitch_71
        0x72 -> :sswitch_72
        0x73 -> :sswitch_73
        0x74 -> :sswitch_74
        0x75 -> :sswitch_75
        0x76 -> :sswitch_76
        0x77 -> :sswitch_77
        0x78 -> :sswitch_78
        0x79 -> :sswitch_79
        0x7a -> :sswitch_7a
        0x7b -> :sswitch_7b
        0x7c -> :sswitch_7c
        0x7d -> :sswitch_7d
        0x7e -> :sswitch_7e
        0x7f -> :sswitch_7f
        0x80 -> :sswitch_80
        0x81 -> :sswitch_81
        0x82 -> :sswitch_82
        0x83 -> :sswitch_83
        0x84 -> :sswitch_84
        0x85 -> :sswitch_85
        0x86 -> :sswitch_86
        0x87 -> :sswitch_87
        0x88 -> :sswitch_88
        0x89 -> :sswitch_89
        0x8a -> :sswitch_8a
        0x8b -> :sswitch_8b
        0x8c -> :sswitch_8c
        0x8d -> :sswitch_8d
        0x8e -> :sswitch_8e
        0x8f -> :sswitch_8f
        0x90 -> :sswitch_90
        0x91 -> :sswitch_91
        0x92 -> :sswitch_92
        0x93 -> :sswitch_93
        0x94 -> :sswitch_94
        0x95 -> :sswitch_95
        0x96 -> :sswitch_96
        0x97 -> :sswitch_97
        0x98 -> :sswitch_98
        0x99 -> :sswitch_99
        0x9a -> :sswitch_9a
        0x9b -> :sswitch_9b
        0x9c -> :sswitch_9c
        0x9d -> :sswitch_9d
        0x9e -> :sswitch_9e
        0x9f -> :sswitch_9f
        0xa0 -> :sswitch_a0
        0xa1 -> :sswitch_a1
        0xa2 -> :sswitch_a2
        0xa3 -> :sswitch_a3
        0xa4 -> :sswitch_a4
        0xa5 -> :sswitch_a5
        0xa6 -> :sswitch_a6
        0xa7 -> :sswitch_a7
        0xa8 -> :sswitch_a8
        0xa9 -> :sswitch_a9
        0xaa -> :sswitch_aa
        0xab -> :sswitch_ab
        0xac -> :sswitch_ac
        0xad -> :sswitch_ad
        0xae -> :sswitch_ae
        0xaf -> :sswitch_af
        0xb0 -> :sswitch_b0
        0xb1 -> :sswitch_b1
        0xb2 -> :sswitch_b2
        0xb3 -> :sswitch_b3
        0xb4 -> :sswitch_b4
        0xb5 -> :sswitch_b5
        0xb6 -> :sswitch_b6
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
