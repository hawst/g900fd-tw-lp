.class public Lcom/samsung/commonimsinterface/imscommon/internal/IMSCommonData;
.super Ljava/lang/Object;
.source "IMSCommonData.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/commonimsinterface/imscommon/internal/IMSCommonData$SETTING;,
        Lcom/samsung/commonimsinterface/imscommon/internal/IMSCommonData$REJECT_REASON;,
        Lcom/samsung/commonimsinterface/imscommon/internal/IMSCommonData$END_REASON;,
        Lcom/samsung/commonimsinterface/imscommon/internal/IMSCommonData$NOTIFY_CALL_END_MODE;
    }
.end annotation


# static fields
.field public static final CONNECTIVITY_TYPE_MAX:I = 0x3

.field public static final CONNECTIVITY_TYPE_MOBILE:I = 0x0

.field public static final CONNECTIVITY_TYPE_MOBILE_IMS:I = 0x1

.field public static final CONNECTIVITY_TYPE_WIFI:I = 0x2

.field public static final IMS_SYSTEM_SERVICE_NAME:Ljava/lang/String; = "ims"

.field public static final NATIVE_CALL_PACKAGE_NAME:Ljava/lang/String; = "com.sec.ims"

.field public static final NETWORK_TYPE_MAX:I = 0x2

.field public static final NETWORK_TYPE_MOBILE:I = 0x0

.field public static final NETWORK_TYPE_WIFI:I = 0x1

.field public static final NUMBER_OF_REQUIRED_DEDICATED_BEARERS:I = 0x2

.field public static final OS_VERSION_CODE_KITKAT:I = 0x13

.field public static final TTY_FULL:I = 0x3

.field public static final TTY_HEAR:I = 0x2

.field public static final TTY_OFF:I = 0x0

.field public static final TTY_TALK:I = 0x1

.field public static final TTY_UNDEFINED:I = 0xff


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 4
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    return-void
.end method
