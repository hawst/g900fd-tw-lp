.class public Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;
.super Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener$Stub;
.source "IMSRemoteListenerStub.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub$InnerHandler;
    }
.end annotation


# static fields
.field private static final LOG_TAG:Ljava/lang/String;


# instance fields
.field private mContext:Landroid/content/Context;

.field protected mHandler:Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub$InnerHandler;

.field private mIntArrayList:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/samsung/commonimsinterface/imscommon/IIMSListener;",
            "[I>;"
        }
    .end annotation
.end field

.field private mListenerList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/commonimsinterface/imscommon/IIMSListener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    const-class v0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener$Stub;-><init>()V

    .line 27
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;->mListenerList:Ljava/util/List;

    .line 28
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;->mIntArrayList:Ljava/util/HashMap;

    .line 29
    new-instance v0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub$InnerHandler;

    invoke-direct {v0, p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub$InnerHandler;-><init>(Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;)V

    iput-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;->mHandler:Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub$InnerHandler;

    .line 30
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;->mContext:Landroid/content/Context;

    .line 39
    iput-object p1, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;->mContext:Landroid/content/Context;

    .line 40
    return-void
.end method

.method private blockCallEventToNativeApp(I)Z
    .locals 2
    .param p1, "action"    # I

    .prologue
    .line 159
    invoke-direct {p0, p1}, Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;->isCallAction(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;->ignoreNotifyMTCallEvent2NativeCallApp()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.sec.ims"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 162
    const/4 v0, 0x1

    .line 164
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isCallAction(I)Z
    .locals 1
    .param p1, "action"    # I

    .prologue
    .line 173
    const/16 v0, 0x7d0

    if-le p1, v0, :cond_0

    const/16 v0, 0xbb8

    if-ge p1, v0, :cond_0

    .line 174
    const/4 v0, 0x1

    .line 176
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public addListener(Lcom/samsung/commonimsinterface/imscommon/IIMSListener;)Z
    .locals 1
    .param p1, "listener"    # Lcom/samsung/commonimsinterface/imscommon/IIMSListener;

    .prologue
    .line 68
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;->mListenerList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 69
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;->mListenerList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 71
    const/4 v0, 0x1

    .line 74
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public addListener(Lcom/samsung/commonimsinterface/imscommon/IIMSListener;[I)Z
    .locals 2
    .param p1, "listener"    # Lcom/samsung/commonimsinterface/imscommon/IIMSListener;
    .param p2, "requiredFields"    # [I

    .prologue
    const/4 v0, 0x1

    .line 78
    invoke-virtual {p0, p1}, Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;->addListener(Lcom/samsung/commonimsinterface/imscommon/IIMSListener;)Z

    move-result v1

    if-ne v1, v0, :cond_0

    .line 79
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;->mIntArrayList:Ljava/util/HashMap;

    invoke-virtual {v1, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 83
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected handleMessage(Landroid/os/Message;)V
    .locals 0
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 65
    return-void
.end method

.method public ignoreNotifyMTCallEvent2NativeCallApp()Z
    .locals 2

    .prologue
    .line 169
    const-string v0, "net.sip.vzthirdpartyapi"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public onReceive(IILcom/samsung/commonimsinterface/imscommon/IMSParameter;)V
    .locals 19
    .param p1, "token"    # I
    .param p2, "action"    # I
    .param p3, "parameter"    # Lcom/samsung/commonimsinterface/imscommon/IMSParameter;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 102
    sget-object v2, Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;->LOG_TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onReceive : token ["

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, p1

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "] action ["

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, p2

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "] parameter ["

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "]"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 104
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;->mListenerList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_0
    :goto_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/commonimsinterface/imscommon/IIMSListener;

    .line 105
    .local v4, "listener":Lcom/samsung/commonimsinterface/imscommon/IIMSListener;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;->mIntArrayList:Ljava/util/HashMap;

    invoke-virtual {v2, v4}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 106
    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-direct {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;->blockCallEventToNativeApp(I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 107
    sget-object v2, Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;->LOG_TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Ignore notifying to native call app : "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " , action : "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, p2

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 109
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;->mHandler:Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub$InnerHandler;

    move-object/from16 v18, v0

    new-instance v2, Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub$1;

    move-object/from16 v3, p0

    move/from16 v5, p1

    move/from16 v6, p2

    move-object/from16 v7, p3

    invoke-direct/range {v2 .. v7}, Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub$1;-><init>(Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;Lcom/samsung/commonimsinterface/imscommon/IIMSListener;IILcom/samsung/commonimsinterface/imscommon/IMSParameter;)V

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub$InnerHandler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 117
    :cond_2
    const/16 v2, 0x3f1

    move/from16 v0, p2

    if-ne v0, v2, :cond_0

    .line 118
    const-string v2, "settingsvalue"

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;->getSparseStringArray(Ljava/lang/String;)Landroid/util/SparseArray;

    move-result-object v17

    .line 120
    .local v17, "values":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/lang/String;>;"
    if-nez v17, :cond_4

    .line 121
    sget-object v2, Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;->LOG_TAG:Ljava/lang/String;

    const-string v3, "getSparseStringArray() value is null"

    invoke-static {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 156
    .end local v4    # "listener":Lcom/samsung/commonimsinterface/imscommon/IIMSListener;
    .end local v17    # "values":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/lang/String;>;"
    :cond_3
    return-void

    .line 125
    .restart local v4    # "listener":Lcom/samsung/commonimsinterface/imscommon/IIMSListener;
    .restart local v17    # "values":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/lang/String;>;"
    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;->mIntArrayList:Ljava/util/HashMap;

    invoke-virtual {v2, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, [I

    .line 126
    .local v9, "fields":[I
    if-nez v9, :cond_5

    .line 127
    sget-object v2, Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;->LOG_TAG:Ljava/lang/String;

    const-string v3, "onReceive() fields is null"

    invoke-static {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 130
    :cond_5
    new-instance v15, Landroid/util/SparseArray;

    invoke-direct {v15}, Landroid/util/SparseArray;-><init>()V

    .line 132
    .local v15, "result":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/lang/String;>;"
    const/4 v12, 0x0

    .local v12, "index":I
    invoke-virtual/range {v17 .. v17}, Landroid/util/SparseArray;->size()I

    move-result v16

    .local v16, "size":I
    :goto_1
    move/from16 v0, v16

    if-ge v12, v0, :cond_8

    .line 133
    move-object v8, v9

    .local v8, "arr$":[I
    array-length v14, v8

    .local v14, "len$":I
    const/4 v11, 0x0

    .local v11, "i$":I
    :goto_2
    if-ge v11, v14, :cond_6

    aget v13, v8, v11

    .line 134
    .local v13, "keyValue":I
    move-object/from16 v0, v17

    invoke-virtual {v0, v12}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v2

    if-ne v13, v2, :cond_7

    .line 135
    move-object/from16 v0, v17

    invoke-virtual {v0, v12}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v2

    move-object/from16 v0, v17

    invoke-virtual {v0, v12}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v15, v2, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 132
    .end local v13    # "keyValue":I
    :cond_6
    add-int/lit8 v12, v12, 0x1

    goto :goto_1

    .line 133
    .restart local v13    # "keyValue":I
    :cond_7
    add-int/lit8 v11, v11, 0x1

    goto :goto_2

    .line 141
    .end local v8    # "arr$":[I
    .end local v11    # "i$":I
    .end local v13    # "keyValue":I
    .end local v14    # "len$":I
    :cond_8
    invoke-virtual {v15}, Landroid/util/SparseArray;->size()I

    move-result v2

    if-lez v2, :cond_0

    .line 142
    new-instance v7, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;

    invoke-direct {v7}, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;-><init>()V

    .line 144
    .local v7, "newParamter":Lcom/samsung/commonimsinterface/imscommon/IMSParameter;
    const-string v2, "settingsvalue"

    invoke-virtual {v7, v2, v15}, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;->putSparseStringArray(Ljava/lang/String;Landroid/util/SparseArray;)V

    .line 146
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;->mHandler:Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub$InnerHandler;

    move-object/from16 v18, v0

    new-instance v2, Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub$2;

    move-object/from16 v3, p0

    move/from16 v5, p1

    move/from16 v6, p2

    invoke-direct/range {v2 .. v7}, Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub$2;-><init>(Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;Lcom/samsung/commonimsinterface/imscommon/IIMSListener;IILcom/samsung/commonimsinterface/imscommon/IMSParameter;)V

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub$InnerHandler;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_0
.end method

.method public removeListener(Lcom/samsung/commonimsinterface/imscommon/IIMSListener;)Z
    .locals 2
    .param p1, "listener"    # Lcom/samsung/commonimsinterface/imscommon/IIMSListener;

    .prologue
    const/4 v0, 0x1

    .line 87
    if-eqz p1, :cond_1

    iget-object v1, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;->mListenerList:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-ne v1, v0, :cond_1

    .line 88
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;->mListenerList:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 90
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;->mIntArrayList:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-ne v1, v0, :cond_0

    .line 91
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;->mIntArrayList:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 97
    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
