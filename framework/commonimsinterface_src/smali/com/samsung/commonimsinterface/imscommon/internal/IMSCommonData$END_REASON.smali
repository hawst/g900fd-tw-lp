.class public Lcom/samsung/commonimsinterface/imscommon/internal/IMSCommonData$END_REASON;
.super Ljava/lang/Object;
.source "IMSCommonData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/commonimsinterface/imscommon/internal/IMSCommonData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "END_REASON"
.end annotation


# static fields
.field public static final BEARER_LOST:I = 0xb

.field public static final CALL_BUSY:I = 0x10

.field public static final IMS_DEREGISTERED:I = 0xe

.field public static final LOST_EPDN_CONNECTION:I = 0xf

.field public static final LOST_LTE_WIFI_CONNECTION:I = 0xc

.field public static final LOW_BATTERY:I = 0x6

.field public static final NORMAL:I = 0x5

.field public static final NWAY_CONFERENCE:I = 0x7

.field public static final NW_HANDOVER:I = 0x4

.field public static final OUT_OF_BATTERY:I = 0xa

.field public static final PS_BARRING:I = 0xd

.field public static final SRVCC_HANDOVER:I = 0x8

.field public static final VOPS_DISABLED:I = 0x9


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
