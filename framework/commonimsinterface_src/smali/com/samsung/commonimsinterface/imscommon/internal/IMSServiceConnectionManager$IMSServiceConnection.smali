.class Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager$IMSServiceConnection;
.super Ljava/lang/Object;
.source "IMSServiceConnectionManager.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "IMSServiceConnection"
.end annotation


# instance fields
.field private mServiceInfo:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;


# direct methods
.method public constructor <init>(Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;Ljava/util/HashMap;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 112
    .local p2, "serviceInfo":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    iput-object p1, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager$IMSServiceConnection;->this$0:Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 110
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager$IMSServiceConnection;->mServiceInfo:Ljava/util/HashMap;

    .line 113
    iput-object p2, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager$IMSServiceConnection;->mServiceInfo:Ljava/util/HashMap;

    .line 114
    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 9
    .param p1, "className"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 118
    const/4 v2, 0x0

    .line 119
    .local v2, "serviceInterface":Ljava/lang/Object;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager$IMSServiceConnection;->mServiceInfo:Ljava/util/HashMap;

    const-string v6, "INTERFACE"

    invoke-virtual {v4, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "$Stub"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 121
    .local v3, "stubName":Ljava/lang/String;
    # getter for: Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;->access$000()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "onServiceConnected :"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    # getter for: Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;->access$000()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Get Remote Service-Interface to "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    :try_start_0
    invoke-static {v3}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v4

    const-string v5, "asInterface"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Class;

    const/4 v7, 0x0

    const-class v8, Landroid/os/IBinder;

    aput-object v8, v6, v7

    invoke-virtual {v4, v5, v6}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object p2, v6, v7

    invoke-virtual {v4, v5, v6}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_3

    move-result-object v2

    .line 140
    .end local v2    # "serviceInterface":Ljava/lang/Object;
    :goto_0
    if-nez v2, :cond_0

    .line 151
    :goto_1
    return-void

    .line 126
    .restart local v2    # "serviceInterface":Ljava/lang/Object;
    :catch_0
    move-exception v0

    .line 127
    .local v0, "e":Ljava/lang/ClassNotFoundException;
    # getter for: Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;->access$000()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Dynamic Method Loading Failed with Exception e - "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 128
    invoke-virtual {v0}, Ljava/lang/ClassNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 129
    .end local v0    # "e":Ljava/lang/ClassNotFoundException;
    :catch_1
    move-exception v0

    .line 130
    .local v0, "e":Ljava/lang/NoSuchMethodException;
    # getter for: Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;->access$000()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Dynamic Method Loading Failed with Exception e - "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    invoke-virtual {v0}, Ljava/lang/NoSuchMethodException;->printStackTrace()V

    goto :goto_0

    .line 132
    .end local v0    # "e":Ljava/lang/NoSuchMethodException;
    :catch_2
    move-exception v0

    .line 133
    .local v0, "e":Ljava/lang/IllegalAccessException;
    # getter for: Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;->access$000()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Dynamic Method Loading Failed with Exception e - "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 134
    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_0

    .line 135
    .end local v0    # "e":Ljava/lang/IllegalAccessException;
    :catch_3
    move-exception v0

    .line 136
    .local v0, "e":Ljava/lang/reflect/InvocationTargetException;
    # getter for: Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;->access$000()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Dynamic Method Loading Failed with Exception e - "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 137
    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V

    goto :goto_0

    .line 144
    .end local v0    # "e":Ljava/lang/reflect/InvocationTargetException;
    .end local v2    # "serviceInterface":Ljava/lang/Object;
    :cond_0
    # getter for: Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;->access$000()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Attached to Service-Interface : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    iget-object v4, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager$IMSServiceConnection;->mServiceInfo:Ljava/util/HashMap;

    const-string v5, "INSTANCE"

    invoke-virtual {v4, v5, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 148
    const/4 v1, 0x0

    .line 149
    .local v1, "listener":Lcom/samsung/commonimsinterface/imscommon/IIMSServiceConnectionListener;
    iget-object v4, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager$IMSServiceConnection;->mServiceInfo:Ljava/util/HashMap;

    const-string v5, "LISTENER"

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "listener":Lcom/samsung/commonimsinterface/imscommon/IIMSServiceConnectionListener;
    check-cast v1, Lcom/samsung/commonimsinterface/imscommon/IIMSServiceConnectionListener;

    .line 150
    .restart local v1    # "listener":Lcom/samsung/commonimsinterface/imscommon/IIMSServiceConnectionListener;
    invoke-interface {v1}, Lcom/samsung/commonimsinterface/imscommon/IIMSServiceConnectionListener;->onConnected()V

    goto/16 :goto_1
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 4
    .param p1, "className"    # Landroid/content/ComponentName;

    .prologue
    .line 155
    # getter for: Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;->access$000()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onServiceDisconnected :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 156
    # getter for: Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;->access$000()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "IMS Service Connection Got Closed : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/content/ComponentName;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 158
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager$IMSServiceConnection;->this$0:Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;

    # invokes: Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;->unbindService()V
    invoke-static {v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;->access$100(Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;)V

    .line 160
    const/4 v0, 0x0

    .line 161
    .local v0, "listener":Lcom/samsung/commonimsinterface/imscommon/IIMSServiceConnectionListener;
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager$IMSServiceConnection;->mServiceInfo:Ljava/util/HashMap;

    const-string v2, "LISTENER"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "listener":Lcom/samsung/commonimsinterface/imscommon/IIMSServiceConnectionListener;
    check-cast v0, Lcom/samsung/commonimsinterface/imscommon/IIMSServiceConnectionListener;

    .line 162
    .restart local v0    # "listener":Lcom/samsung/commonimsinterface/imscommon/IIMSServiceConnectionListener;
    invoke-interface {v0}, Lcom/samsung/commonimsinterface/imscommon/IIMSServiceConnectionListener;->onDisconnected()V

    .line 164
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager$IMSServiceConnection;->this$0:Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;

    # invokes: Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;->bindService()V
    invoke-static {v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;->access$200(Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;)V

    .line 165
    return-void
.end method
