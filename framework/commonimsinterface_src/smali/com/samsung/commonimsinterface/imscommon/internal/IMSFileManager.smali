.class public Lcom/samsung/commonimsinterface/imscommon/internal/IMSFileManager;
.super Ljava/lang/Object;
.source "IMSFileManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/commonimsinterface/imscommon/internal/IMSFileManager$FilenameFilterImpl;
    }
.end annotation


# instance fields
.field private final LOG_TAG:Ljava/lang/String;

.field private MAX_SINGLE_LEN:I

.field private MAX_VALUE_LEN:I

.field private mFile:Ljava/io/File;

.field private mMakeFileNotExist:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 1
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "fileName"    # Ljava/lang/String;
    .param p3, "makeFileNotExist"    # Z

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    const-class v0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSFileManager;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSFileManager;->LOG_TAG:Ljava/lang/String;

    .line 23
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSFileManager;->mFile:Ljava/io/File;

    .line 24
    const/16 v0, 0x100

    iput v0, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSFileManager;->MAX_SINGLE_LEN:I

    .line 25
    const/16 v0, 0x800

    iput v0, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSFileManager;->MAX_VALUE_LEN:I

    .line 26
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSFileManager;->mMakeFileNotExist:Z

    .line 48
    iput-boolean p3, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSFileManager;->mMakeFileNotExist:Z

    .line 49
    invoke-direct {p0, p1, p2}, Lcom/samsung/commonimsinterface/imscommon/internal/IMSFileManager;->initFile(Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Z)V
    .locals 4
    .param p1, "filePathName"    # Ljava/lang/String;
    .param p2, "makeFileNotExist"    # Z

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    const-class v3, Lcom/samsung/commonimsinterface/imscommon/internal/IMSFileManager;

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSFileManager;->LOG_TAG:Ljava/lang/String;

    .line 23
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSFileManager;->mFile:Ljava/io/File;

    .line 24
    const/16 v3, 0x100

    iput v3, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSFileManager;->MAX_SINGLE_LEN:I

    .line 25
    const/16 v3, 0x800

    iput v3, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSFileManager;->MAX_VALUE_LEN:I

    .line 26
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSFileManager;->mMakeFileNotExist:Z

    .line 40
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 41
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v2

    .line 42
    .local v2, "filePath":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    .line 43
    .local v1, "fileName":Ljava/lang/String;
    iput-boolean p2, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSFileManager;->mMakeFileNotExist:Z

    .line 44
    invoke-direct {p0, v2, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IMSFileManager;->initFile(Ljava/lang/String;Ljava/lang/String;)V

    .line 45
    return-void
.end method

.method public static exists(Ljava/lang/String;)Z
    .locals 2
    .param p0, "filePathName"    # Ljava/lang/String;

    .prologue
    .line 53
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 54
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    return v1
.end method

.method public static getFileStartsWith(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 7
    .param p0, "filePath"    # Ljava/lang/String;
    .param p1, "fileName"    # Ljava/lang/String;
    .param p2, "ext"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 58
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 59
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v6

    if-nez v6, :cond_1

    .line 60
    const/4 v5, 0x0

    .line 71
    :cond_0
    return-object v5

    .line 62
    :cond_1
    const/4 v5, 0x0

    .line 63
    .local v5, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v2, Lcom/samsung/commonimsinterface/imscommon/internal/IMSFileManager$FilenameFilterImpl;

    invoke-direct {v2, p1, p2}, Lcom/samsung/commonimsinterface/imscommon/internal/IMSFileManager$FilenameFilterImpl;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    .local v2, "filter":Lcom/samsung/commonimsinterface/imscommon/internal/IMSFileManager$FilenameFilterImpl;
    invoke-virtual {v0, v2}, Ljava/io/File;->listFiles(Ljava/io/FilenameFilter;)[Ljava/io/File;

    move-result-object v1

    .line 65
    .local v1, "fileList":[Ljava/io/File;
    if-eqz v1, :cond_0

    array-length v6, v1

    if-lez v6, :cond_0

    .line 66
    new-instance v5, Ljava/util/ArrayList;

    .end local v5    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 67
    .restart local v5    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v3, 0x0

    .local v3, "idx":I
    array-length v4, v1

    .local v4, "len":I
    :goto_0
    if-ge v3, v4, :cond_0

    .line 68
    aget-object v6, v1, v3

    invoke-virtual {v6}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v6

    invoke-virtual {v6}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 67
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method private initFile(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "fileName"    # Ljava/lang/String;

    .prologue
    .line 75
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 76
    .local v1, "path":Ljava/io/File;
    const-string v2, "/"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "/"

    invoke-virtual {p1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 77
    const/4 v2, 0x0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {p1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    .line 78
    iget-object v2, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSFileManager;->LOG_TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "initFile() filePath ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    :cond_0
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_2

    .line 82
    iget-boolean v2, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSFileManager;->mMakeFileNotExist:Z

    if-nez v2, :cond_1

    .line 105
    :goto_0
    return-void

    .line 85
    :cond_1
    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    .line 87
    iget-object v2, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSFileManager;->LOG_TAG:Ljava/lang/String;

    const-string v3, "Path Created!"

    invoke-static {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    :goto_1
    new-instance v2, Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v2, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSFileManager;->mFile:Ljava/io/File;

    .line 94
    iget-object v2, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSFileManager;->mFile:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_3

    iget-boolean v2, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSFileManager;->mMakeFileNotExist:Z

    if-eqz v2, :cond_3

    .line 96
    :try_start_0
    iget-object v2, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSFileManager;->mFile:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->createNewFile()Z

    .line 98
    iget-object v2, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSFileManager;->LOG_TAG:Ljava/lang/String;

    const-string v3, "File Created!"

    invoke-static {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 99
    :catch_0
    move-exception v0

    .line 100
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 89
    .end local v0    # "e":Ljava/io/IOException;
    :cond_2
    iget-object v2, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSFileManager;->LOG_TAG:Ljava/lang/String;

    const-string v3, "Path Already Exists!"

    invoke-static {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 103
    :cond_3
    iget-object v2, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSFileManager;->LOG_TAG:Ljava/lang/String;

    const-string v3, "File Already Exists!"

    invoke-static {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private write(Landroid/util/SparseArray;)Z
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 178
    .local p1, "list":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/lang/String;>;"
    const/4 v9, 0x0

    .line 179
    .local v9, "os":Ljava/io/BufferedOutputStream;
    const-string v12, ""

    .line 180
    .local v12, "strData":Ljava/lang/String;
    const/4 v6, -0x1

    .line 181
    .local v6, "key":I
    const/4 v7, 0x0

    .line 182
    .local v7, "len":I
    const/4 v13, 0x3

    new-array v1, v13, [B

    .line 183
    .local v1, "buf":[B
    const/4 v2, 0x0

    .line 184
    .local v2, "bufStr":[B
    const/4 v11, 0x1

    .line 186
    .local v11, "retVal":Z
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSFileManager;->mFile:Ljava/io/File;

    if-nez v13, :cond_0

    .line 187
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSFileManager;->LOG_TAG:Ljava/lang/String;

    const-string v14, "write - file not exist"

    invoke-static {v13, v14}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 188
    const/4 v13, 0x0

    .line 225
    :goto_0
    return v13

    .line 191
    :cond_0
    :try_start_0
    new-instance v10, Ljava/io/BufferedOutputStream;

    new-instance v13, Ljava/io/FileOutputStream;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSFileManager;->mFile:Ljava/io/File;

    invoke-direct {v13, v14}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v10, v13}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_7
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 192
    .end local v9    # "os":Ljava/io/BufferedOutputStream;
    .local v10, "os":Ljava/io/BufferedOutputStream;
    const/4 v5, 0x0

    .local v5, "i":I
    :try_start_1
    invoke-virtual/range {p1 .. p1}, Landroid/util/SparseArray;->size()I

    move-result v8

    .local v8, "n":I
    :goto_1
    if-ge v5, v8, :cond_4

    .line 193
    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v6

    .line 194
    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v13

    move-object v0, v13

    check-cast v0, Ljava/lang/String;

    move-object v12, v0

    .line 195
    const/4 v13, -0x1

    if-eq v13, v6, :cond_1

    if-nez v12, :cond_2

    .line 192
    :cond_1
    :goto_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 198
    :cond_2
    invoke-virtual {v12}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    .line 199
    array-length v13, v2

    and-int/lit16 v7, v13, 0xff

    .line 200
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSFileManager;->LOG_TAG:Ljava/lang/String;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, " writeSettingValueToFile() key["

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "], len["

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "], data ["

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "]"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 201
    const/4 v13, 0x0

    and-int/lit16 v14, v6, 0xff

    int-to-byte v14, v14

    aput-byte v14, v1, v13

    .line 202
    const/4 v13, 0x1

    const v14, 0xff00

    and-int/2addr v14, v6

    shr-int/lit8 v14, v14, 0x8

    int-to-byte v14, v14

    aput-byte v14, v1, v13

    .line 203
    const/4 v13, 0x2

    int-to-byte v14, v7

    aput-byte v14, v1, v13

    .line 204
    const/4 v13, 0x0

    const/4 v14, 0x3

    invoke-virtual {v10, v1, v13, v14}, Ljava/io/BufferedOutputStream;->write([BII)V

    .line 205
    const/4 v13, 0x0

    invoke-virtual {v10, v2, v13, v7}, Ljava/io/BufferedOutputStream;->write([BII)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_2

    .line 209
    .end local v8    # "n":I
    :catch_0
    move-exception v3

    move-object v9, v10

    .line 210
    .end local v5    # "i":I
    .end local v10    # "os":Ljava/io/BufferedOutputStream;
    .local v3, "e":Ljava/io/FileNotFoundException;
    .restart local v9    # "os":Ljava/io/BufferedOutputStream;
    :goto_3
    const/4 v11, 0x0

    .line 211
    :try_start_2
    invoke-virtual {v3}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 217
    if-eqz v9, :cond_3

    .line 218
    :try_start_3
    invoke-virtual {v9}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    .end local v3    # "e":Ljava/io/FileNotFoundException;
    :cond_3
    :goto_4
    move v13, v11

    .line 225
    goto/16 :goto_0

    .line 207
    .end local v9    # "os":Ljava/io/BufferedOutputStream;
    .restart local v5    # "i":I
    .restart local v8    # "n":I
    .restart local v10    # "os":Ljava/io/BufferedOutputStream;
    :cond_4
    :try_start_4
    invoke-virtual {v10}, Ljava/io/BufferedOutputStream;->flush()V

    .line 208
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSFileManager;->LOG_TAG:Ljava/lang/String;

    const-string v14, "write(SparseArray<String> list) success"

    invoke-static {v13, v14}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->v(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_6
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 217
    if-eqz v10, :cond_5

    .line 218
    :try_start_5
    invoke-virtual {v10}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1

    :cond_5
    move-object v9, v10

    .line 223
    .end local v10    # "os":Ljava/io/BufferedOutputStream;
    .restart local v9    # "os":Ljava/io/BufferedOutputStream;
    goto :goto_4

    .line 220
    .end local v9    # "os":Ljava/io/BufferedOutputStream;
    .restart local v10    # "os":Ljava/io/BufferedOutputStream;
    :catch_1
    move-exception v4

    .line 221
    .local v4, "ex":Ljava/io/IOException;
    const/4 v11, 0x0

    .line 222
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    move-object v9, v10

    .line 224
    .end local v10    # "os":Ljava/io/BufferedOutputStream;
    .restart local v9    # "os":Ljava/io/BufferedOutputStream;
    goto :goto_4

    .line 220
    .end local v4    # "ex":Ljava/io/IOException;
    .end local v5    # "i":I
    .end local v8    # "n":I
    .restart local v3    # "e":Ljava/io/FileNotFoundException;
    :catch_2
    move-exception v4

    .line 221
    .restart local v4    # "ex":Ljava/io/IOException;
    const/4 v11, 0x0

    .line 222
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 212
    .end local v3    # "e":Ljava/io/FileNotFoundException;
    .end local v4    # "ex":Ljava/io/IOException;
    :catch_3
    move-exception v3

    .line 213
    .local v3, "e":Ljava/io/IOException;
    :goto_5
    const/4 v11, 0x0

    .line 214
    :try_start_6
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 217
    if-eqz v9, :cond_3

    .line 218
    :try_start_7
    invoke-virtual {v9}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4

    goto :goto_4

    .line 220
    :catch_4
    move-exception v4

    .line 221
    .restart local v4    # "ex":Ljava/io/IOException;
    const/4 v11, 0x0

    .line 222
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 216
    .end local v3    # "e":Ljava/io/IOException;
    .end local v4    # "ex":Ljava/io/IOException;
    :catchall_0
    move-exception v13

    .line 217
    :goto_6
    if-eqz v9, :cond_6

    .line 218
    :try_start_8
    invoke-virtual {v9}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_5

    .line 223
    :cond_6
    :goto_7
    throw v13

    .line 220
    :catch_5
    move-exception v4

    .line 221
    .restart local v4    # "ex":Ljava/io/IOException;
    const/4 v11, 0x0

    .line 222
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_7

    .line 216
    .end local v4    # "ex":Ljava/io/IOException;
    .end local v9    # "os":Ljava/io/BufferedOutputStream;
    .restart local v5    # "i":I
    .restart local v10    # "os":Ljava/io/BufferedOutputStream;
    :catchall_1
    move-exception v13

    move-object v9, v10

    .end local v10    # "os":Ljava/io/BufferedOutputStream;
    .restart local v9    # "os":Ljava/io/BufferedOutputStream;
    goto :goto_6

    .line 212
    .end local v9    # "os":Ljava/io/BufferedOutputStream;
    .restart local v10    # "os":Ljava/io/BufferedOutputStream;
    :catch_6
    move-exception v3

    move-object v9, v10

    .end local v10    # "os":Ljava/io/BufferedOutputStream;
    .restart local v9    # "os":Ljava/io/BufferedOutputStream;
    goto :goto_5

    .line 209
    .end local v5    # "i":I
    :catch_7
    move-exception v3

    goto :goto_3
.end method


# virtual methods
.method public appendNewLine(Ljava/lang/String;)Z
    .locals 1
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x1

    .line 115
    invoke-virtual {p0, p1, v0, v0}, Lcom/samsung/commonimsinterface/imscommon/internal/IMSFileManager;->write(Ljava/lang/String;ZZ)Z

    move-result v0

    return v0
.end method

.method public deleteFile()Z
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSFileManager;->mFile:Ljava/io/File;

    if-eqz v0, :cond_0

    .line 109
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSFileManager;->mFile:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v0

    .line 111
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public readSingleValue()Ljava/lang/String;
    .locals 11

    .prologue
    const/4 v8, 0x0

    .line 229
    const/4 v3, 0x0

    .line 230
    .local v3, "in":Ljava/io/BufferedInputStream;
    const/4 v0, 0x0

    .line 231
    .local v0, "buf":[B
    const-string v6, ""

    .line 232
    .local v6, "retVal":Ljava/lang/String;
    const/4 v5, 0x0

    .line 233
    .local v5, "len":I
    iget-object v9, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSFileManager;->mFile:Ljava/io/File;

    if-nez v9, :cond_0

    .line 234
    iget-object v9, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSFileManager;->LOG_TAG:Ljava/lang/String;

    const-string v10, "readSingleValue - file not exist"

    invoke-static {v9, v10}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    move-object v7, v8

    .line 261
    :goto_0
    return-object v7

    .line 238
    :cond_0
    :try_start_0
    new-instance v4, Ljava/io/BufferedInputStream;

    new-instance v9, Ljava/io/FileInputStream;

    iget-object v10, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSFileManager;->mFile:Ljava/io/File;

    invoke-direct {v9, v10}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v4, v9}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 239
    .end local v3    # "in":Ljava/io/BufferedInputStream;
    .local v4, "in":Ljava/io/BufferedInputStream;
    :try_start_1
    iget v9, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSFileManager;->MAX_SINGLE_LEN:I

    new-array v0, v9, [B

    .line 240
    const/4 v9, 0x0

    invoke-static {v0, v9}, Ljava/util/Arrays;->fill([BB)V

    .line 241
    invoke-virtual {v4, v0}, Ljava/io/BufferedInputStream;->read([B)I

    move-result v5

    .line 242
    invoke-virtual {v4}, Ljava/io/BufferedInputStream;->close()V

    .line 243
    if-gtz v5, :cond_1

    .line 244
    iget-object v9, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSFileManager;->LOG_TAG:Ljava/lang/String;

    const-string v10, "Can\'t read file"

    invoke-static {v9, v10}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move-object v3, v4

    .end local v4    # "in":Ljava/io/BufferedInputStream;
    .restart local v3    # "in":Ljava/io/BufferedInputStream;
    move-object v7, v8

    .line 245
    goto :goto_0

    .line 247
    .end local v3    # "in":Ljava/io/BufferedInputStream;
    .restart local v4    # "in":Ljava/io/BufferedInputStream;
    :cond_1
    new-instance v7, Ljava/lang/String;

    const/4 v9, 0x0

    invoke-direct {v7, v0, v9, v5}, Ljava/lang/String;-><init>([BII)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3

    .line 248
    .end local v6    # "retVal":Ljava/lang/String;
    .local v7, "retVal":Ljava/lang/String;
    const/4 v0, 0x0

    move-object v6, v7

    .end local v7    # "retVal":Ljava/lang/String;
    .restart local v6    # "retVal":Ljava/lang/String;
    move-object v3, v4

    .line 261
    .end local v4    # "in":Ljava/io/BufferedInputStream;
    .restart local v3    # "in":Ljava/io/BufferedInputStream;
    goto :goto_0

    .line 249
    :catch_0
    move-exception v1

    .line 250
    .local v1, "e":Ljava/io/FileNotFoundException;
    :goto_1
    iget-object v9, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSFileManager;->LOG_TAG:Ljava/lang/String;

    const-string v10, "file does not exist"

    invoke-static {v9, v10}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move-object v7, v8

    .line 251
    goto :goto_0

    .line 252
    .end local v1    # "e":Ljava/io/FileNotFoundException;
    :catch_1
    move-exception v1

    .line 253
    .local v1, "e":Ljava/io/IOException;
    :goto_2
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .line 255
    :try_start_2
    invoke-virtual {v3}, Ljava/io/BufferedInputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    :goto_3
    move-object v7, v8

    .line 259
    goto :goto_0

    .line 256
    :catch_2
    move-exception v2

    .line 257
    .local v2, "e1":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 252
    .end local v1    # "e":Ljava/io/IOException;
    .end local v2    # "e1":Ljava/io/IOException;
    .end local v3    # "in":Ljava/io/BufferedInputStream;
    .restart local v4    # "in":Ljava/io/BufferedInputStream;
    :catch_3
    move-exception v1

    move-object v3, v4

    .end local v4    # "in":Ljava/io/BufferedInputStream;
    .restart local v3    # "in":Ljava/io/BufferedInputStream;
    goto :goto_2

    .line 249
    .end local v3    # "in":Ljava/io/BufferedInputStream;
    .restart local v4    # "in":Ljava/io/BufferedInputStream;
    :catch_4
    move-exception v1

    move-object v3, v4

    .end local v4    # "in":Ljava/io/BufferedInputStream;
    .restart local v3    # "in":Ljava/io/BufferedInputStream;
    goto :goto_1
.end method

.method public readValuesWithSparseArray()Landroid/util/SparseArray;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 265
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    .line 266
    .local v0, "list":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/lang/String;>;"
    invoke-virtual {p0, v0}, Lcom/samsung/commonimsinterface/imscommon/internal/IMSFileManager;->readValuesWithSparseArray(Landroid/util/SparseArray;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 267
    const/4 v0, 0x0

    .line 269
    .end local v0    # "list":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/lang/String;>;"
    :cond_0
    return-object v0
.end method

.method public readValuesWithSparseArray(Landroid/util/SparseArray;)Z
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 273
    .local p1, "list":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/lang/String;>;"
    const/4 v3, 0x0

    .line 275
    .local v3, "in":Ljava/io/BufferedInputStream;
    const/4 v7, 0x0

    .line 276
    .local v7, "strData":Ljava/lang/String;
    iget-object v9, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSFileManager;->mFile:Ljava/io/File;

    if-nez v9, :cond_0

    .line 277
    iget-object v9, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSFileManager;->LOG_TAG:Ljava/lang/String;

    const-string v10, "readValuesWithSparseArray - file not exist"

    invoke-static {v9, v10}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 278
    const/4 v9, 0x0

    .line 314
    :goto_0
    return v9

    .line 281
    :cond_0
    :try_start_0
    new-instance v4, Ljava/io/BufferedInputStream;

    new-instance v9, Ljava/io/FileInputStream;

    iget-object v10, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSFileManager;->mFile:Ljava/io/File;

    invoke-direct {v9, v10}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v4, v9}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 282
    .end local v3    # "in":Ljava/io/BufferedInputStream;
    .local v4, "in":Ljava/io/BufferedInputStream;
    :try_start_1
    iget v9, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSFileManager;->MAX_VALUE_LEN:I

    new-array v0, v9, [B

    .line 283
    .local v0, "buf":[B
    const/4 v9, 0x0

    invoke-static {v0, v9}, Ljava/util/Arrays;->fill([BB)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_6
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3

    move-object v8, v7

    .line 284
    .end local v7    # "strData":Ljava/lang/String;
    .local v8, "strData":Ljava/lang/String;
    :goto_1
    const/4 v9, 0x0

    const/4 v10, 0x2

    :try_start_2
    invoke-virtual {v4, v0, v9, v10}, Ljava/io/BufferedInputStream;->read([BII)I

    move-result v9

    const/4 v10, -0x1

    if-eq v9, v10, :cond_1

    .line 286
    const/4 v9, 0x0

    aget-byte v9, v0, v9

    and-int/lit16 v9, v9, 0xff

    const/4 v10, 0x1

    aget-byte v10, v0, v10

    shl-int/lit8 v10, v10, 0x8

    const v11, 0xff00

    and-int/2addr v10, v11

    or-int v5, v9, v10

    .line 287
    .local v5, "key":I
    invoke-virtual {v4}, Ljava/io/BufferedInputStream;->read()I

    move-result v6

    .line 288
    .local v6, "len":I
    const/4 v9, -0x1

    if-ne v9, v6, :cond_2

    .line 289
    iget-object v9, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSFileManager;->LOG_TAG:Ljava/lang/String;

    const-string v10, "wrong file format. fail to get len"

    invoke-static {v9, v10}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 301
    .end local v5    # "key":I
    .end local v6    # "len":I
    :cond_1
    :goto_2
    invoke-virtual {v4}, Ljava/io/BufferedInputStream;->close()V

    .line 314
    const/4 v9, 0x1

    move-object v7, v8

    .end local v8    # "strData":Ljava/lang/String;
    .restart local v7    # "strData":Ljava/lang/String;
    move-object v3, v4

    .end local v4    # "in":Ljava/io/BufferedInputStream;
    .restart local v3    # "in":Ljava/io/BufferedInputStream;
    goto :goto_0

    .line 292
    .end local v3    # "in":Ljava/io/BufferedInputStream;
    .end local v7    # "strData":Ljava/lang/String;
    .restart local v4    # "in":Ljava/io/BufferedInputStream;
    .restart local v5    # "key":I
    .restart local v6    # "len":I
    .restart local v8    # "strData":Ljava/lang/String;
    :cond_2
    const/4 v9, 0x0

    invoke-static {v0, v9}, Ljava/util/Arrays;->fill([BB)V

    .line 293
    const/4 v9, 0x0

    invoke-virtual {v4, v0, v9, v6}, Ljava/io/BufferedInputStream;->read([BII)I

    move-result v9

    const/4 v10, -0x1

    if-ne v9, v10, :cond_3

    .line 294
    iget-object v9, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSFileManager;->LOG_TAG:Ljava/lang/String;

    const-string v10, "wrong file format. data is not matched with len"

    invoke-static {v9, v10}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_4

    goto :goto_2

    .line 302
    .end local v5    # "key":I
    .end local v6    # "len":I
    :catch_0
    move-exception v1

    move-object v7, v8

    .end local v8    # "strData":Ljava/lang/String;
    .restart local v7    # "strData":Ljava/lang/String;
    move-object v3, v4

    .line 303
    .end local v0    # "buf":[B
    .end local v4    # "in":Ljava/io/BufferedInputStream;
    .local v1, "e":Ljava/io/FileNotFoundException;
    .restart local v3    # "in":Ljava/io/BufferedInputStream;
    :goto_3
    iget-object v9, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSFileManager;->LOG_TAG:Ljava/lang/String;

    const-string v10, "file does not exist"

    invoke-static {v9, v10}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 304
    const/4 v9, 0x0

    goto :goto_0

    .line 297
    .end local v1    # "e":Ljava/io/FileNotFoundException;
    .end local v3    # "in":Ljava/io/BufferedInputStream;
    .end local v7    # "strData":Ljava/lang/String;
    .restart local v0    # "buf":[B
    .restart local v4    # "in":Ljava/io/BufferedInputStream;
    .restart local v5    # "key":I
    .restart local v6    # "len":I
    .restart local v8    # "strData":Ljava/lang/String;
    :cond_3
    :try_start_3
    new-instance v7, Ljava/lang/String;

    const/4 v9, 0x0

    invoke-direct {v7, v0, v9, v6}, Ljava/lang/String;-><init>([BII)V
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_4

    .line 298
    .end local v8    # "strData":Ljava/lang/String;
    .restart local v7    # "strData":Ljava/lang/String;
    const/4 v9, 0x0

    :try_start_4
    invoke-static {v0, v9}, Ljava/util/Arrays;->fill([BB)V

    .line 299
    invoke-virtual {p1, v5, v7}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
    :try_end_4
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_4} :catch_6
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    move-object v8, v7

    .end local v7    # "strData":Ljava/lang/String;
    .restart local v8    # "strData":Ljava/lang/String;
    goto :goto_1

    .line 305
    .end local v0    # "buf":[B
    .end local v4    # "in":Ljava/io/BufferedInputStream;
    .end local v5    # "key":I
    .end local v6    # "len":I
    .end local v8    # "strData":Ljava/lang/String;
    .restart local v3    # "in":Ljava/io/BufferedInputStream;
    .restart local v7    # "strData":Ljava/lang/String;
    :catch_1
    move-exception v1

    .line 306
    .local v1, "e":Ljava/io/IOException;
    :goto_4
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .line 308
    :try_start_5
    invoke-virtual {v3}, Ljava/io/BufferedInputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    .line 312
    :goto_5
    const/4 v9, 0x0

    goto :goto_0

    .line 309
    :catch_2
    move-exception v2

    .line 310
    .local v2, "e1":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5

    .line 305
    .end local v1    # "e":Ljava/io/IOException;
    .end local v2    # "e1":Ljava/io/IOException;
    .end local v3    # "in":Ljava/io/BufferedInputStream;
    .restart local v4    # "in":Ljava/io/BufferedInputStream;
    :catch_3
    move-exception v1

    move-object v3, v4

    .end local v4    # "in":Ljava/io/BufferedInputStream;
    .restart local v3    # "in":Ljava/io/BufferedInputStream;
    goto :goto_4

    .end local v3    # "in":Ljava/io/BufferedInputStream;
    .end local v7    # "strData":Ljava/lang/String;
    .restart local v0    # "buf":[B
    .restart local v4    # "in":Ljava/io/BufferedInputStream;
    .restart local v8    # "strData":Ljava/lang/String;
    :catch_4
    move-exception v1

    move-object v7, v8

    .end local v8    # "strData":Ljava/lang/String;
    .restart local v7    # "strData":Ljava/lang/String;
    move-object v3, v4

    .end local v4    # "in":Ljava/io/BufferedInputStream;
    .restart local v3    # "in":Ljava/io/BufferedInputStream;
    goto :goto_4

    .line 302
    .end local v0    # "buf":[B
    :catch_5
    move-exception v1

    goto :goto_3

    .end local v3    # "in":Ljava/io/BufferedInputStream;
    .restart local v4    # "in":Ljava/io/BufferedInputStream;
    :catch_6
    move-exception v1

    move-object v3, v4

    .end local v4    # "in":Ljava/io/BufferedInputStream;
    .restart local v3    # "in":Ljava/io/BufferedInputStream;
    goto :goto_3
.end method

.method public write(Landroid/util/SparseArray;Z)Z
    .locals 7
    .param p2, "isEdit"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/String;",
            ">;Z)Z"
        }
    .end annotation

    .prologue
    .line 157
    .local p1, "list":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/lang/String;>;"
    const/4 v0, 0x0

    .line 158
    .local v0, "arrList":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/lang/String;>;"
    if-eqz p2, :cond_4

    .line 159
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IMSFileManager;->readValuesWithSparseArray()Landroid/util/SparseArray;

    move-result-object v4

    .line 160
    .local v4, "readSp":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/lang/String;>;"
    if-nez v4, :cond_0

    .line 161
    move-object v0, p1

    .line 174
    .end local v4    # "readSp":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/lang/String;>;"
    :goto_0
    invoke-direct {p0, v0}, Lcom/samsung/commonimsinterface/imscommon/internal/IMSFileManager;->write(Landroid/util/SparseArray;)Z

    move-result v6

    return v6

    .line 163
    .restart local v4    # "readSp":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/lang/String;>;"
    :cond_0
    const/4 v1, 0x0

    .local v1, "i":I
    invoke-virtual {p1}, Landroid/util/SparseArray;->size()I

    move-result v3

    .local v3, "n":I
    :goto_1
    if-ge v1, v3, :cond_3

    .line 164
    invoke-virtual {p1, v1}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v2

    .line 165
    .local v2, "key":I
    invoke-virtual {p1, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 166
    .local v5, "value":Ljava/lang/String;
    if-ltz v2, :cond_1

    if-nez v5, :cond_2

    .line 163
    :cond_1
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 167
    :cond_2
    invoke-virtual {v4, v2, v5}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto :goto_2

    .line 169
    .end local v2    # "key":I
    .end local v5    # "value":Ljava/lang/String;
    :cond_3
    move-object v0, v4

    goto :goto_0

    .line 172
    .end local v1    # "i":I
    .end local v3    # "n":I
    .end local v4    # "readSp":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/lang/String;>;"
    :cond_4
    move-object v0, p1

    goto :goto_0
.end method

.method public write(Ljava/lang/String;Z)Z
    .locals 1
    .param p1, "value"    # Ljava/lang/String;
    .param p2, "append"    # Z

    .prologue
    .line 119
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/samsung/commonimsinterface/imscommon/internal/IMSFileManager;->write(Ljava/lang/String;ZZ)Z

    move-result v0

    return v0
.end method

.method public write(Ljava/lang/String;ZZ)Z
    .locals 7
    .param p1, "value"    # Ljava/lang/String;
    .param p2, "append"    # Z
    .param p3, "addNewLine"    # Z

    .prologue
    .line 123
    const/4 v0, 0x0

    .line 124
    .local v0, "bufferedWriter":Ljava/io/BufferedWriter;
    const/4 v4, 0x1

    .line 125
    .local v4, "retVal":Z
    iget-object v5, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSFileManager;->mFile:Ljava/io/File;

    if-nez v5, :cond_0

    .line 126
    iget-object v5, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSFileManager;->LOG_TAG:Ljava/lang/String;

    const-string v6, "write - file not exist"

    invoke-static {v5, v6}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    const/4 v5, 0x0

    .line 153
    :goto_0
    return v5

    .line 130
    :cond_0
    :try_start_0
    new-instance v1, Ljava/io/BufferedWriter;

    new-instance v5, Ljava/io/FileWriter;

    iget-object v6, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSFileManager;->mFile:Ljava/io/File;

    invoke-direct {v5, v6, p2}, Ljava/io/FileWriter;-><init>(Ljava/io/File;Z)V

    invoke-direct {v1, v5}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 132
    .end local v0    # "bufferedWriter":Ljava/io/BufferedWriter;
    .local v1, "bufferedWriter":Ljava/io/BufferedWriter;
    :try_start_1
    invoke-virtual {v1, p1}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    .line 133
    if-eqz p3, :cond_1

    .line 134
    invoke-virtual {v1}, Ljava/io/BufferedWriter;->newLine()V

    .line 136
    :cond_1
    invoke-virtual {v1}, Ljava/io/BufferedWriter;->flush()V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 145
    if-eqz v1, :cond_2

    .line 146
    :try_start_2
    invoke-virtual {v1}, Ljava/io/BufferedWriter;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :cond_2
    move-object v0, v1

    .end local v1    # "bufferedWriter":Ljava/io/BufferedWriter;
    .restart local v0    # "bufferedWriter":Ljava/io/BufferedWriter;
    :cond_3
    :goto_1
    move v5, v4

    .line 153
    goto :goto_0

    .line 148
    .end local v0    # "bufferedWriter":Ljava/io/BufferedWriter;
    .restart local v1    # "bufferedWriter":Ljava/io/BufferedWriter;
    :catch_0
    move-exception v3

    .line 149
    .local v3, "ex":Ljava/io/IOException;
    const/4 v4, 0x0

    .line 150
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    move-object v0, v1

    .line 152
    .end local v1    # "bufferedWriter":Ljava/io/BufferedWriter;
    .restart local v0    # "bufferedWriter":Ljava/io/BufferedWriter;
    goto :goto_1

    .line 137
    .end local v3    # "ex":Ljava/io/IOException;
    :catch_1
    move-exception v2

    .line 138
    .local v2, "e":Ljava/io/FileNotFoundException;
    :goto_2
    const/4 v4, 0x0

    .line 139
    :try_start_3
    invoke-virtual {v2}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 145
    if-eqz v0, :cond_3

    .line 146
    :try_start_4
    invoke-virtual {v0}, Ljava/io/BufferedWriter;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_1

    .line 148
    :catch_2
    move-exception v3

    .line 149
    .restart local v3    # "ex":Ljava/io/IOException;
    const/4 v4, 0x0

    .line 150
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 140
    .end local v2    # "e":Ljava/io/FileNotFoundException;
    .end local v3    # "ex":Ljava/io/IOException;
    :catch_3
    move-exception v2

    .line 141
    .local v2, "e":Ljava/io/IOException;
    :goto_3
    const/4 v4, 0x0

    .line 142
    :try_start_5
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 145
    if-eqz v0, :cond_3

    .line 146
    :try_start_6
    invoke-virtual {v0}, Ljava/io/BufferedWriter;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    goto :goto_1

    .line 148
    :catch_4
    move-exception v3

    .line 149
    .restart local v3    # "ex":Ljava/io/IOException;
    const/4 v4, 0x0

    .line 150
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 144
    .end local v2    # "e":Ljava/io/IOException;
    .end local v3    # "ex":Ljava/io/IOException;
    :catchall_0
    move-exception v5

    .line 145
    :goto_4
    if-eqz v0, :cond_4

    .line 146
    :try_start_7
    invoke-virtual {v0}, Ljava/io/BufferedWriter;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_5

    .line 151
    :cond_4
    :goto_5
    throw v5

    .line 148
    :catch_5
    move-exception v3

    .line 149
    .restart local v3    # "ex":Ljava/io/IOException;
    const/4 v4, 0x0

    .line 150
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5

    .line 144
    .end local v0    # "bufferedWriter":Ljava/io/BufferedWriter;
    .end local v3    # "ex":Ljava/io/IOException;
    .restart local v1    # "bufferedWriter":Ljava/io/BufferedWriter;
    :catchall_1
    move-exception v5

    move-object v0, v1

    .end local v1    # "bufferedWriter":Ljava/io/BufferedWriter;
    .restart local v0    # "bufferedWriter":Ljava/io/BufferedWriter;
    goto :goto_4

    .line 140
    .end local v0    # "bufferedWriter":Ljava/io/BufferedWriter;
    .restart local v1    # "bufferedWriter":Ljava/io/BufferedWriter;
    :catch_6
    move-exception v2

    move-object v0, v1

    .end local v1    # "bufferedWriter":Ljava/io/BufferedWriter;
    .restart local v0    # "bufferedWriter":Ljava/io/BufferedWriter;
    goto :goto_3

    .line 137
    .end local v0    # "bufferedWriter":Ljava/io/BufferedWriter;
    .restart local v1    # "bufferedWriter":Ljava/io/BufferedWriter;
    :catch_7
    move-exception v2

    move-object v0, v1

    .end local v1    # "bufferedWriter":Ljava/io/BufferedWriter;
    .restart local v0    # "bufferedWriter":Ljava/io/BufferedWriter;
    goto :goto_2
.end method
