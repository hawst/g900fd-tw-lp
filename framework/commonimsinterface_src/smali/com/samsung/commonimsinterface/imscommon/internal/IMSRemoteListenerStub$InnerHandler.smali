.class public Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub$InnerHandler;
.super Landroid/os/Handler;
.source "IMSRemoteListenerStub.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "InnerHandler"
.end annotation


# instance fields
.field private mManager:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;)V
    .locals 1
    .param p1, "manager"    # Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;

    .prologue
    .line 45
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 43
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub$InnerHandler;->mManager:Ljava/lang/ref/WeakReference;

    .line 46
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub$InnerHandler;->mManager:Ljava/lang/ref/WeakReference;

    .line 47
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1, "message"    # Landroid/os/Message;

    .prologue
    .line 51
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 53
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub$InnerHandler;->mManager:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;

    .line 55
    .local v0, "instance":Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;
    if-nez v0, :cond_0

    .line 60
    :goto_0
    return-void

    .line 59
    :cond_0
    invoke-virtual {v0, p1}, Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;->handleMessage(Landroid/os/Message;)V

    goto :goto_0
.end method
