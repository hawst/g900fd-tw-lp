.class Lcom/samsung/commonimsinterface/imscommon/internal/IMSFileManager$FilenameFilterImpl;
.super Ljava/lang/Object;
.source "IMSFileManager.java"

# interfaces
.implements Ljava/io/FilenameFilter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/commonimsinterface/imscommon/internal/IMSFileManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "FilenameFilterImpl"
.end annotation


# instance fields
.field mExt:Ljava/lang/String;

.field mStartFileName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "startFileName"    # Ljava/lang/String;
    .param p2, "ext"    # Ljava/lang/String;

    .prologue
    .line 320
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 321
    iput-object p1, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSFileManager$FilenameFilterImpl;->mStartFileName:Ljava/lang/String;

    .line 322
    iput-object p2, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSFileManager$FilenameFilterImpl;->mExt:Ljava/lang/String;

    .line 323
    return-void
.end method


# virtual methods
.method public accept(Ljava/io/File;Ljava/lang/String;)Z
    .locals 1
    .param p1, "dir"    # Ljava/io/File;
    .param p2, "fileName"    # Ljava/lang/String;

    .prologue
    .line 326
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSFileManager$FilenameFilterImpl;->mStartFileName:Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSFileManager$FilenameFilterImpl;->mExt:Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 327
    const/4 v0, 0x1

    .line 329
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
