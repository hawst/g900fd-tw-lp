.class public Lcom/samsung/commonimsinterface/imscommon/internal/IMSCommonData$REJECT_REASON;
.super Ljava/lang/Object;
.source "IMSCommonData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/commonimsinterface/imscommon/internal/IMSCommonData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "REJECT_REASON"
.end annotation


# static fields
.field public static final CALL_BUSY:I = 0xb

.field public static final LOW_BATTERY:I = 0x6

.field public static final NW_HANDOVER:I = 0x4

.field public static final ONLY_WLAN_CONNECTED:I = 0xa

.field public static final SRVCC_HANDOVER:I = 0x8

.field public static final TEMP_NOT_ACCEPTABLE:I = 0x9

.field public static final USER_BUSY:I = 0x2

.field public static final USER_BUSY_CS_CALL:I = 0x7

.field public static final USER_CALL_BLOCK:I = 0xc

.field public static final USER_DECLINE:I = 0x3


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
