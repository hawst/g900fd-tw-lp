.class public Lcom/samsung/commonimsinterface/imscommon/IIMSConstants$eCameraEffect;
.super Ljava/lang/Object;
.source "IIMSConstants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/commonimsinterface/imscommon/IIMSConstants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "eCameraEffect"
.end annotation


# static fields
.field public static final CAMERA_EFFECT_ANTIQUE:I = 0x9

.field public static final CAMERA_EFFECT_AQUA:I = 0x8

.field public static final CAMERA_EFFECT_BLACKBOARD:I = 0x7

.field public static final CAMERA_EFFECT_BLUR:I = 0x10

.field public static final CAMERA_EFFECT_COLD:I = 0xe

.field public static final CAMERA_EFFECT_MAX:I = 0x11

.field public static final CAMERA_EFFECT_MONO:I = 0x1

.field public static final CAMERA_EFFECT_NEGATIVE:I = 0x2

.field public static final CAMERA_EFFECT_NONE:I = 0x0

.field public static final CAMERA_EFFECT_POINT_BLUE:I = 0xa

.field public static final CAMERA_EFFECT_POINT_RED:I = 0xb

.field public static final CAMERA_EFFECT_POINT_YELLOW:I = 0xc

.field public static final CAMERA_EFFECT_POSTERIZE:I = 0x5

.field public static final CAMERA_EFFECT_SEPIA:I = 0x4

.field public static final CAMERA_EFFECT_SOLARIZE:I = 0x3

.field public static final CAMERA_EFFECT_WARM:I = 0xd

.field public static final CAMERA_EFFECT_WASHED:I = 0xf

.field public static final CAMERA_EFFECT_WHITEBOARD:I = 0x6


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 336
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
