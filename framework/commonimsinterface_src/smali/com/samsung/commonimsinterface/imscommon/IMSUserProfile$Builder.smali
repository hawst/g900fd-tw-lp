.class public Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile$Builder;
.super Ljava/lang/Object;
.source "IMSUserProfile.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private mDisplayName:Ljava/lang/String;

.field private mProfile:Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;

.field private mProxyAddress:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;)V
    .locals 4
    .param p1, "profile"    # Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;

    .prologue
    .line 104
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 96
    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;-><init>(Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile$1;)V

    iput-object v1, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile$Builder;->mProfile:Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;

    .line 105
    if-nez p1, :cond_0

    .line 106
    new-instance v1, Ljava/lang/NullPointerException;

    invoke-direct {v1}, Ljava/lang/NullPointerException;-><init>()V

    throw v1

    .line 108
    :cond_0
    :try_start_0
    # invokes: Ljava/lang/Object;->clone()Ljava/lang/Object;
    invoke-static {p1}, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->access$200(Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;

    iput-object v1, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile$Builder;->mProfile:Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 113
    invoke-virtual {p1}, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->getDisplayName()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile$Builder;->mDisplayName:Ljava/lang/String;

    .line 114
    invoke-virtual {p1}, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->getProxyAddress()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile$Builder;->mProxyAddress:Ljava/lang/String;

    .line 115
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile$Builder;->mProfile:Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;

    invoke-virtual {p1}, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->getPort()I

    move-result v2

    # setter for: Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mPort:I
    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->access$302(Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;I)I

    .line 117
    # getter for: Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->access$400()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "DisplayName:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile$Builder;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ProxyAddress:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile$Builder;->mProxyAddress:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 118
    return-void

    .line 109
    :catch_0
    move-exception v0

    .line 110
    .local v0, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "should not occur"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 2
    .param p1, "uriString"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    .prologue
    .line 126
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 96
    new-instance v0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;-><init>(Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile$1;)V

    iput-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile$Builder;->mProfile:Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;

    .line 127
    if-nez p1, :cond_0

    .line 128
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "uriString cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 130
    :cond_0
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile$Builder;->mProfile:Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;

    # setter for: Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mProfileName:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->access$502(Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;Ljava/lang/String;)Ljava/lang/String;

    .line 132
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "username"    # Ljava/lang/String;
    .param p2, "serverDomain"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    .prologue
    .line 143
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 96
    new-instance v0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;-><init>(Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile$1;)V

    iput-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile$Builder;->mProfile:Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;

    .line 144
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 145
    :cond_0
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "username and serverDomain cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 148
    :cond_1
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile$Builder;->mProfile:Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;

    # setter for: Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mDomain:Ljava/lang/String;
    invoke-static {v0, p2}, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->access$602(Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;Ljava/lang/String;)Ljava/lang/String;

    .line 149
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile$Builder;->mProfile:Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;

    # setter for: Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mProfileName:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->access$502(Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;Ljava/lang/String;)Ljava/lang/String;

    .line 150
    return-void
.end method

.method private fix(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "uriString"    # Ljava/lang/String;

    .prologue
    .line 154
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "sip:"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .end local p1    # "uriString":Ljava/lang/String;
    :goto_0
    return-object p1

    .restart local p1    # "uriString":Ljava/lang/String;
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "sip:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0
.end method


# virtual methods
.method public build()Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;
    .locals 1

    .prologue
    .line 300
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile$Builder;->mProfile:Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;

    return-object v0
.end method

.method public setAuthUserName(Ljava/lang/String;)Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile$Builder;
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 165
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile$Builder;->mProfile:Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;

    # setter for: Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mAuthUserName:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->access$702(Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;Ljava/lang/String;)Ljava/lang/String;

    .line 166
    return-object p0
.end method

.method public setAutoRegistration(Z)Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile$Builder;
    .locals 1
    .param p1, "flag"    # Z

    .prologue
    .line 268
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile$Builder;->mProfile:Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;

    # setter for: Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mAutoRegistration:Z
    invoke-static {v0, p1}, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->access$1102(Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;Z)Z

    .line 269
    return-object p0
.end method

.method public setDisplayName(Ljava/lang/String;)Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile$Builder;
    .locals 0
    .param p1, "displayName"    # Ljava/lang/String;

    .prologue
    .line 244
    iput-object p1, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile$Builder;->mDisplayName:Ljava/lang/String;

    .line 245
    return-object p0
.end method

.method public setOutboundProxy(Ljava/lang/String;)Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile$Builder;
    .locals 0
    .param p1, "outboundProxy"    # Ljava/lang/String;

    .prologue
    .line 233
    iput-object p1, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile$Builder;->mProxyAddress:Ljava/lang/String;

    .line 234
    return-object p0
.end method

.method public setPassword(Ljava/lang/String;)Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile$Builder;
    .locals 1
    .param p1, "password"    # Ljava/lang/String;

    .prologue
    .line 187
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile$Builder;->mProfile:Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;

    # setter for: Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mPassword:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->access$802(Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;Ljava/lang/String;)Ljava/lang/String;

    .line 188
    return-object p0
.end method

.method public setPort(I)Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile$Builder;
    .locals 3
    .param p1, "port"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 199
    const v0, 0xffff

    if-gt p1, v0, :cond_0

    const/16 v0, 0x3e8

    if-ge p1, v0, :cond_1

    .line 200
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "incorrect port arugment: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 202
    :cond_1
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile$Builder;->mProfile:Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;

    # setter for: Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mPort:I
    invoke-static {v0, p1}, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->access$302(Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;I)I

    .line 203
    return-object p0
.end method

.method public setProfileName(Ljava/lang/String;)Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile$Builder;
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 176
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile$Builder;->mProfile:Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;

    # setter for: Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mProfileName:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->access$502(Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;Ljava/lang/String;)Ljava/lang/String;

    .line 177
    return-object p0
.end method

.method public setProtocol(Ljava/lang/String;)Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile$Builder;
    .locals 3
    .param p1, "protocol"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 215
    if-nez p1, :cond_0

    .line 216
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "protocol cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 218
    :cond_0
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p1, v0}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p1

    .line 219
    const-string v0, "UDP"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "TCP"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 220
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "unsupported protocol: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 222
    :cond_1
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile$Builder;->mProfile:Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;

    # setter for: Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mProtocol:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->access$902(Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;Ljava/lang/String;)Ljava/lang/String;

    .line 223
    return-object p0
.end method

.method public setSendKeepAlive(Z)Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile$Builder;
    .locals 1
    .param p1, "flag"    # Z

    .prologue
    .line 256
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile$Builder;->mProfile:Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;

    # setter for: Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mSendKeepAlive:Z
    invoke-static {v0, p1}, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->access$1002(Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;Z)Z

    .line 257
    return-object p0
.end method
