.class Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;
.super Ljava/lang/Object;
.source "MultiSocketInputStream.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/http/impl/client/MultiSocketInputStream;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "RangeRequest"
.end annotation


# instance fields
.field private bHandover:Z

.field public bIOExceptionDuringContinueChunk:Z

.field private chunkStartTime:J

.field private connInfID:I

.field private currentBlockNumber:I

.field private dbuf:Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;

.field director:Lorg/apache/http/impl/client/DefaultRequestDirector;

.field private mbReconnect:Z

.field rrCustomHttpClient:Lorg/apache/http/impl/client/CustomHttpClient;

.field public rrExceptionCount:I

.field public rrStatus:I

.field private sockID:I

.field private t0:J

.field private t1:J

.field private t2:J

.field final synthetic this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

.field private final threadName:Ljava/lang/String;

.field private timer:[J

.field public totalElapsedTime:J

.field private totalReadSize:J


# direct methods
.method public constructor <init>(Lorg/apache/http/impl/client/MultiSocketInputStream;I)V
    .locals 5
    .param p2, "socketId"    # I

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x0

    const-wide/16 v2, 0x0

    .line 2745
    iput-object p1, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2667
    iput-boolean v1, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->bHandover:Z

    .line 2677
    iput-wide v2, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->chunkStartTime:J

    .line 2682
    iput-object v4, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->dbuf:Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;

    .line 2717
    iput-object v4, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->rrCustomHttpClient:Lorg/apache/http/impl/client/CustomHttpClient;

    .line 2722
    iput v1, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->rrExceptionCount:I

    .line 2727
    iput v1, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->rrStatus:I

    .line 2732
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->currentBlockNumber:I

    .line 2737
    iput-boolean v1, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->bIOExceptionDuringContinueChunk:Z

    .line 2746
    iput p2, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->sockID:I

    .line 2747
    iput p2, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->connInfID:I

    .line 2748
    iget v0, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->sockID:I

    if-nez v0, :cond_0

    .line 2749
    const-string v0, "WIFI_Socket_Thread"

    iput-object v0, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->threadName:Ljava/lang/String;

    .line 2753
    :goto_0
    iput-wide v2, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->totalReadSize:J

    .line 2754
    iput-wide v2, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->totalElapsedTime:J

    .line 2755
    iput-wide v2, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->t0:J

    .line 2756
    iput-wide v2, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->t1:J

    .line 2757
    iput-wide v2, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->t2:J

    .line 2765
    new-instance v0, Lorg/apache/http/impl/client/CustomHttpClient;

    iget-object v1, p1, Lorg/apache/http/impl/client/MultiSocketInputStream;->mSchemeRegistry:Lorg/apache/http/conn/scheme/SchemeRegistry;

    invoke-direct {v0, v1}, Lorg/apache/http/impl/client/CustomHttpClient;-><init>(Lorg/apache/http/conn/scheme/SchemeRegistry;)V

    iput-object v0, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->rrCustomHttpClient:Lorg/apache/http/impl/client/CustomHttpClient;

    .line 2766
    iput-object v4, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->director:Lorg/apache/http/impl/client/DefaultRequestDirector;

    .line 2767
    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mIsConnectionClosedAfterEveryRequest:Z
    invoke-static {p1}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$3000(Lorg/apache/http/impl/client/MultiSocketInputStream;)Z

    move-result v0

    iput-boolean v0, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->mbReconnect:Z

    .line 2768
    return-void

    .line 2751
    :cond_0
    const-string v0, "Mobile_Socket_Thread"

    iput-object v0, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->threadName:Ljava/lang/String;

    goto :goto_0
.end method

.method private reconnect(I)Z
    .locals 12
    .param p1, "sockId"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/http/HttpException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 3769
    sget-boolean v0, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v0, :cond_0

    .line 3770
    const-string v0, "try to connect again"

    invoke-static {v0}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 3772
    :cond_0
    iput-boolean v7, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->bHandover:Z

    .line 3773
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iput-wide v4, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->t1:J

    .line 3776
    iget-object v0, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->director:Lorg/apache/http/impl/client/DefaultRequestDirector;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->director:Lorg/apache/http/impl/client/DefaultRequestDirector;

    iget-object v0, v0, Lorg/apache/http/impl/client/DefaultRequestDirector;->managedConn:Lorg/apache/http/conn/ManagedClientConnection;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->director:Lorg/apache/http/impl/client/DefaultRequestDirector;

    iget-object v0, v0, Lorg/apache/http/impl/client/DefaultRequestDirector;->managedConn:Lorg/apache/http/conn/ManagedClientConnection;

    invoke-interface {v0}, Lorg/apache/http/conn/ManagedClientConnection;->isOpen()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->mbReconnect:Z

    if-eqz v0, :cond_3

    .line 3778
    :cond_1
    sget-boolean v0, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v0, :cond_2

    .line 3779
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Closing the connection for socket id: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->director:Lorg/apache/http/impl/client/DefaultRequestDirector;

    iget-object v3, v3, Lorg/apache/http/impl/client/DefaultRequestDirector;->managedConn:Lorg/apache/http/conn/ManagedClientConnection;

    invoke-virtual {v3}, Ljava/lang/Object;->hashCode()I

    move-result v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 3783
    :cond_2
    invoke-virtual {p0}, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->closeHTTP()V

    .line 3784
    iget-object v0, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->rrCustomHttpClient:Lorg/apache/http/impl/client/CustomHttpClient;

    iget-object v3, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mHttpRequest:[Lorg/apache/http/HttpRequest;
    invoke-static {v3}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$2400(Lorg/apache/http/impl/client/MultiSocketInputStream;)[Lorg/apache/http/HttpRequest;

    move-result-object v3

    aget-object v3, v3, p1

    invoke-virtual {v0, v3}, Lorg/apache/http/impl/client/CustomHttpClient;->createClientRequestDirector(Lorg/apache/http/HttpRequest;)Lorg/apache/http/impl/client/DefaultRequestDirector;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->director:Lorg/apache/http/impl/client/DefaultRequestDirector;

    .line 3787
    :cond_3
    iget-object v0, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->director:Lorg/apache/http/impl/client/DefaultRequestDirector;

    if-nez v0, :cond_4

    .line 3788
    iget-object v0, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->rrCustomHttpClient:Lorg/apache/http/impl/client/CustomHttpClient;

    iget-object v3, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mHttpRequest:[Lorg/apache/http/HttpRequest;
    invoke-static {v3}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$2400(Lorg/apache/http/impl/client/MultiSocketInputStream;)[Lorg/apache/http/HttpRequest;

    move-result-object v3

    aget-object v3, v3, p1

    invoke-virtual {v0, v3}, Lorg/apache/http/impl/client/CustomHttpClient;->createClientRequestDirector(Lorg/apache/http/HttpRequest;)Lorg/apache/http/impl/client/DefaultRequestDirector;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->director:Lorg/apache/http/impl/client/DefaultRequestDirector;

    .line 3791
    :cond_4
    :try_start_0
    iget-object v0, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->director:Lorg/apache/http/impl/client/DefaultRequestDirector;

    iget-object v0, v0, Lorg/apache/http/impl/client/DefaultRequestDirector;->managedConn:Lorg/apache/http/conn/ManagedClientConnection;

    if-nez v0, :cond_5

    .line 3793
    iget-object v0, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mHttpRequest:[Lorg/apache/http/HttpRequest;
    invoke-static {v0}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$2400(Lorg/apache/http/impl/client/MultiSocketInputStream;)[Lorg/apache/http/HttpRequest;

    move-result-object v0

    aget-object v2, v0, p1

    .line 3794
    .local v2, "request":Lorg/apache/http/HttpRequest;
    iget-object v0, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mHttpHost:[Lorg/apache/http/HttpHost;
    invoke-static {v0}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$200(Lorg/apache/http/impl/client/MultiSocketInputStream;)[Lorg/apache/http/HttpHost;

    move-result-object v0

    aget-object v1, v0, p1

    .line 3796
    .local v1, "target":Lorg/apache/http/HttpHost;
    iget-object v0, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->director:Lorg/apache/http/impl/client/DefaultRequestDirector;

    iget-object v3, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mHttpContext:Lorg/apache/http/protocol/HttpContext;
    invoke-static {v3}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$100(Lorg/apache/http/impl/client/MultiSocketInputStream;)Lorg/apache/http/protocol/HttpContext;

    move-result-object v3

    iget-object v4, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->rrCustomHttpClient:Lorg/apache/http/impl/client/CustomHttpClient;

    iget-object v4, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    iget v4, v4, Lorg/apache/http/impl/client/MultiSocketInputStream;->mDestIPVer:I

    invoke-static {p1, v4}, Lorg/apache/http/impl/client/CustomHttpClient;->getLocalAddrEx(II)Ljava/net/InetAddress;

    move-result-object v4

    iget-object v5, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mRemoteAddress:[Ljava/net/InetAddress;
    invoke-static {v5}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$2900(Lorg/apache/http/impl/client/MultiSocketInputStream;)[Ljava/net/InetAddress;

    move-result-object v5

    aget-object v5, v5, p1

    invoke-virtual/range {v0 .. v5}, Lorg/apache/http/impl/client/DefaultRequestDirector;->reconnect(Lorg/apache/http/HttpHost;Lorg/apache/http/HttpRequest;Lorg/apache/http/protocol/HttpContext;Ljava/net/InetAddress;Ljava/net/InetAddress;)V

    .line 3799
    sget-boolean v0, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v0, :cond_5

    .line 3800
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Creating socket for the "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "socket id: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->director:Lorg/apache/http/impl/client/DefaultRequestDirector;

    iget-object v3, v3, Lorg/apache/http/impl/client/DefaultRequestDirector;->managedConn:Lorg/apache/http/conn/ManagedClientConnection;

    invoke-virtual {v3}, Ljava/lang/Object;->hashCode()I

    move-result v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 3806
    .end local v1    # "target":Lorg/apache/http/HttpHost;
    .end local v2    # "request":Lorg/apache/http/HttpRequest;
    :cond_5
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iget-wide v10, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->t1:J

    sub-long v8, v4, v10

    .line 3807
    .local v8, "tmp":J
    iget-object v0, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockManager:Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;
    invoke-static {v0}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$800(Lorg/apache/http/impl/client/MultiSocketInputStream;)Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;

    move-result-object v0

    iget-object v0, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->reconnTime:[J

    iget v3, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->sockID:I

    aput-wide v8, v0, v3

    .line 3813
    iget-object v0, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->director:Lorg/apache/http/impl/client/DefaultRequestDirector;

    iget-object v0, v0, Lorg/apache/http/impl/client/DefaultRequestDirector;->managedConn:Lorg/apache/http/conn/ManagedClientConnection;

    invoke-interface {v0}, Lorg/apache/http/conn/ManagedClientConnection;->isOpen()Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 3815
    .end local v8    # "tmp":J
    :goto_0
    return v0

    .line 3814
    :catch_0
    move-exception v6

    .local v6, "ex":Ljava/lang/Throwable;
    move v0, v7

    .line 3815
    goto :goto_0
.end method

.method private submitData()V
    .locals 10

    .prologue
    .line 3706
    const/4 v4, 0x2

    new-array v2, v4, [J

    .line 3707
    .local v2, "lens":[J
    const/4 v4, 0x2

    new-array v3, v4, [J

    .line 3708
    .local v3, "times":[J
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    const/4 v4, 0x2

    if-ge v1, v4, :cond_0

    .line 3709
    iget-object v4, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mRequestHandlers:[Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;
    invoke-static {v4}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$4000(Lorg/apache/http/impl/client/MultiSocketInputStream;)[Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;

    move-result-object v4

    aget-object v4, v4, v1

    iget-wide v4, v4, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->totalReadSize:J

    aput-wide v4, v2, v1

    .line 3710
    iget-object v4, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mRequestHandlers:[Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;
    invoke-static {v4}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$4000(Lorg/apache/http/impl/client/MultiSocketInputStream;)[Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;

    move-result-object v4

    aget-object v4, v4, v1

    iget-wide v4, v4, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->totalElapsedTime:J

    aput-wide v4, v3, v1

    .line 3708
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 3712
    :cond_0
    const/4 v4, 0x0

    aget-wide v4, v3, v4

    const-wide/16 v6, 0x7d0

    cmp-long v4, v4, v6

    if-lez v4, :cond_3

    const/4 v4, 0x1

    aget-wide v4, v3, v4

    const-wide/16 v6, 0x7d0

    cmp-long v4, v4, v6

    if-lez v4, :cond_3

    const/4 v4, 0x0

    aget-wide v4, v2, v4

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-lez v4, :cond_3

    const/4 v4, 0x1

    aget-wide v4, v2, v4

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-lez v4, :cond_3

    .line 3713
    sget-boolean v4, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v4, :cond_1

    .line 3714
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "submit speed for rr case - socket[0]: len="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/4 v5, 0x0

    aget-wide v6, v2, v5

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", time="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/4 v5, 0x0

    aget-wide v6, v3, v5

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", speed="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/4 v5, 0x0

    aget-wide v6, v2, v5

    const-wide/16 v8, 0x8

    mul-long/2addr v6, v8

    const-wide/16 v8, 0x3e8

    mul-long/2addr v6, v8

    const-wide/16 v8, 0x400

    div-long/2addr v6, v8

    const/4 v5, 0x0

    aget-wide v8, v3, v5

    div-long/2addr v6, v8

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "Kbps"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 3715
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "submit speed for rr case - socket[1]: len="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/4 v5, 0x1

    aget-wide v6, v2, v5

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", time="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/4 v5, 0x1

    aget-wide v6, v3, v5

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", speed="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/4 v5, 0x1

    aget-wide v6, v2, v5

    const-wide/16 v8, 0x8

    mul-long/2addr v6, v8

    const-wide/16 v8, 0x3e8

    mul-long/2addr v6, v8

    const-wide/16 v8, 0x400

    div-long/2addr v6, v8

    const/4 v5, 0x1

    aget-wide v8, v3, v5

    div-long/2addr v6, v8

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "Kbps"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 3717
    :cond_1
    invoke-static {v2, v3}, Lorg/apache/http/impl/client/CustomHttpClient;->submitMultiSocketData([J[J)V

    .line 3725
    :cond_2
    :goto_1
    const/4 v4, 0x4

    new-array v0, v4, [J

    .line 3726
    .local v0, "data":[J
    const/4 v1, 0x0

    :goto_2
    const/4 v4, 0x2

    if-ge v1, v4, :cond_4

    .line 3727
    mul-int/lit8 v4, v1, 0x2

    aget-wide v6, v2, v1

    aput-wide v6, v0, v4

    .line 3728
    mul-int/lit8 v4, v1, 0x2

    add-int/lit8 v4, v4, 0x1

    aget-wide v6, v3, v1

    aput-wide v6, v0, v4

    .line 3726
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 3720
    .end local v0    # "data":[J
    :cond_3
    sget-boolean v4, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v4, :cond_2

    .line 3721
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "not to submit speed for rr case - socket[0]: len="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/4 v5, 0x0

    aget-wide v6, v2, v5

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", time="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/4 v5, 0x0

    aget-wide v6, v3, v5

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 3722
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "not to submit speed for rr case - socket[1]: len="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/4 v5, 0x1

    aget-wide v6, v2, v5

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", time="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/4 v5, 0x1

    aget-wide v6, v3, v5

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    goto :goto_1

    .line 3730
    .restart local v0    # "data":[J
    :cond_4
    iget-object v4, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mMainThreadID:J
    invoke-static {v4}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$000(Lorg/apache/http/impl/client/MultiSocketInputStream;)J

    move-result-wide v4

    invoke-static {v4, v5, v0}, Lorg/apache/http/impl/client/CustomHttpClient;->reportSBUsage(J[J)V

    .line 3731
    const/4 v4, 0x1

    sput-boolean v4, Lorg/apache/http/impl/client/DefaultRequestDirector;->bIsDataSubmited:Z

    .line 3732
    return-void
.end method


# virtual methods
.method public closeHTTP()V
    .locals 2

    .prologue
    .line 3747
    :try_start_0
    iget-object v1, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->director:Lorg/apache/http/impl/client/DefaultRequestDirector;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->director:Lorg/apache/http/impl/client/DefaultRequestDirector;

    iget-object v1, v1, Lorg/apache/http/impl/client/DefaultRequestDirector;->managedConn:Lorg/apache/http/conn/ManagedClientConnection;

    if-eqz v1, :cond_0

    .line 3748
    iget-object v1, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->director:Lorg/apache/http/impl/client/DefaultRequestDirector;

    invoke-virtual {v1}, Lorg/apache/http/impl/client/DefaultRequestDirector;->releaseConnection()V

    .line 3750
    :cond_0
    sget-boolean v1, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v1, :cond_1

    .line 3751
    const-string v1, "try to close current HTTP session"

    invoke-static {v1}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 3759
    :cond_1
    :goto_0
    return-void

    .line 3753
    :catch_0
    move-exception v0

    .line 3754
    .local v0, "ex":Ljava/lang/Throwable;
    sget-boolean v1, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v1, :cond_1

    .line 3755
    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 3756
    invoke-static {v0}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public handOver()V
    .locals 1

    .prologue
    .line 3738
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->bHandover:Z

    .line 3739
    invoke-virtual {p0}, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->closeHTTP()V

    .line 3740
    return-void
.end method

.method public run()V
    .locals 86

    .prologue
    .line 2772
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v9, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->threadName:Ljava/lang/String;

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    # operator++ for: Lorg/apache/http/impl/client/MultiSocketInputStream;->sThreadID:I
    invoke-static {}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1808()I

    move-result v9

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v9, "_"

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Thread;->getId()J

    move-result-wide v10

    invoke-virtual {v5, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    .line 2775
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    iget-object v5, v4, Lorg/apache/http/impl/client/MultiSocketInputStream;->mDefaultRequestDirectorInstance:Lorg/apache/http/impl/client/DefaultRequestDirector;

    monitor-enter v5

    .line 2776
    :try_start_0
    sget v4, Lorg/apache/http/impl/client/DefaultRequestDirector;->sProcessUsingMultiSocket:I

    add-int/lit8 v4, v4, 0x1

    sput v4, Lorg/apache/http/impl/client/DefaultRequestDirector;->sProcessUsingMultiSocket:I

    .line 2777
    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2779
    const/4 v14, 0x1

    .line 2781
    .local v14, "bFirstBlockRsp":Z
    sget v53, Lorg/apache/http/impl/client/MultiSocketInputStream;->INIT_BUFFERLEN:I

    .line 2782
    .local v53, "iRealBlockSize":I
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    move-object/from16 v0, p0

    iput-wide v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->t0:J

    .line 2783
    const-wide/16 v84, 0x0

    .line 2789
    .local v84, "waitTime":J
    :goto_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    iget-boolean v4, v4, Lorg/apache/http/impl/client/MultiSocketInputStream;->mFinished:Z

    if-nez v4, :cond_c

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mRangeRequestSuccess:I
    invoke-static {v4}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$2300(Lorg/apache/http/impl/client/MultiSocketInputStream;)I

    move-result v4

    const/4 v5, 0x1

    if-eq v4, v5, :cond_c

    move-object/from16 v0, p0

    iget v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->rrStatus:I

    const/4 v5, -0x1

    if-eq v4, v5, :cond_c

    .line 2790
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mInBuffer:Ljava/util/HashMap;
    invoke-static {v4}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1500(Lorg/apache/http/impl/client/MultiSocketInputStream;)Ljava/util/HashMap;

    move-result-object v5

    monitor-enter v5

    .line 2792
    :try_start_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v72

    .line 2795
    .local v72, "startChunkTime":J
    :cond_0
    :goto_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    iget-boolean v4, v4, Lorg/apache/http/impl/client/MultiSocketInputStream;->mFinished:Z

    if-nez v4, :cond_1

    .line 2798
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    iget-object v4, v4, Lorg/apache/http/impl/client/MultiSocketInputStream;->mMainRequest:Lorg/apache/http/HttpRequest;

    check-cast v4, Lorg/apache/http/client/methods/HttpUriRequest;

    invoke-interface {v4}, Lorg/apache/http/client/methods/HttpUriRequest;->isAborted()Z

    move-result v4

    if-eqz v4, :cond_f

    .line 2799
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    const/4 v9, 0x1

    iput-boolean v9, v4, Lorg/apache/http/impl/client/MultiSocketInputStream;->mFinished:Z

    .line 2904
    :cond_1
    :goto_2
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2905
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    move-object/from16 v0, p0

    iget v5, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->sockID:I

    # invokes: Lorg/apache/http/impl/client/MultiSocketInputStream;->getOtherSocketID(I)I
    invoke-static {v4, v5}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$5700(Lorg/apache/http/impl/client/MultiSocketInputStream;I)I

    move-result v61

    .line 2908
    .local v61, "otherSockID":I
    :cond_2
    :goto_3
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    iget-boolean v4, v4, Lorg/apache/http/impl/client/MultiSocketInputStream;->mFinished:Z

    if-nez v4, :cond_5

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mRequestHandlers:[Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;
    invoke-static {v4}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$4000(Lorg/apache/http/impl/client/MultiSocketInputStream;)[Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;

    move-result-object v4

    aget-object v4, v4, v61

    iget v4, v4, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->rrStatus:I

    const/4 v5, -0x1

    if-eq v4, v5, :cond_5

    .line 2912
    const/16 v32, 0x0

    .line 2913
    .local v32, "chunkNotRead":I
    const/16 v22, 0x0

    .line 2917
    .local v22, "blockedState":I
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mReadBlockNumber:Ljava/lang/Integer;
    invoke-static {v5}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$5600(Lorg/apache/http/impl/client/MultiSocketInputStream;)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    move-result v52

    .line 2918
    .local v52, "i":I
    :goto_4
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockStatus:[B
    invoke-static {v4}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1600(Lorg/apache/http/impl/client/MultiSocketInputStream;)[B

    move-result-object v4

    array-length v4, v4

    move/from16 v0, v52

    if-ge v0, v4, :cond_4

    .line 2919
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockStatus:[B
    invoke-static {v4}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1600(Lorg/apache/http/impl/client/MultiSocketInputStream;)[B

    move-result-object v4

    aget-byte v4, v4, v52

    if-ltz v4, :cond_1e

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockStatus:[B
    invoke-static {v4}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1600(Lorg/apache/http/impl/client/MultiSocketInputStream;)[B

    move-result-object v4

    aget-byte v4, v4, v52

    const/4 v5, 0x3

    if-ge v4, v5, :cond_1e

    .line 2920
    add-int/lit8 v32, v32, 0x1

    .line 2925
    :cond_3
    const/4 v4, 0x1

    move/from16 v0, v32

    if-le v0, v4, :cond_1f

    .line 2931
    :cond_4
    :goto_5
    const/4 v4, 0x1

    move/from16 v0, v32

    if-gt v0, v4, :cond_5

    if-lez v22, :cond_20

    .line 2954
    .end local v22    # "blockedState":I
    .end local v32    # "chunkNotRead":I
    .end local v52    # "i":I
    :cond_5
    sget-boolean v4, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v4, :cond_6

    .line 2955
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "start to check socket connectivity ReCon="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->mbReconnect:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 2959
    :cond_6
    :goto_6
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    iget-boolean v4, v4, Lorg/apache/http/impl/client/MultiSocketInputStream;->mFinished:Z

    if-nez v4, :cond_a

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->director:Lorg/apache/http/impl/client/DefaultRequestDirector;

    if-eqz v4, :cond_7

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->director:Lorg/apache/http/impl/client/DefaultRequestDirector;

    iget-object v4, v4, Lorg/apache/http/impl/client/DefaultRequestDirector;->managedConn:Lorg/apache/http/conn/ManagedClientConnection;

    if-eqz v4, :cond_7

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->director:Lorg/apache/http/impl/client/DefaultRequestDirector;

    iget-object v4, v4, Lorg/apache/http/impl/client/DefaultRequestDirector;->managedConn:Lorg/apache/http/conn/ManagedClientConnection;

    invoke-interface {v4}, Lorg/apache/http/conn/ManagedClientConnection;->isOpen()Z

    move-result v4

    if-eqz v4, :cond_7

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->mbReconnect:Z

    if-eqz v4, :cond_a

    :cond_7
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    iget-object v4, v4, Lorg/apache/http/impl/client/MultiSocketInputStream;->mMainRequest:Lorg/apache/http/HttpRequest;

    check-cast v4, Lorg/apache/http/client/methods/HttpUriRequest;

    invoke-interface {v4}, Lorg/apache/http/client/methods/HttpUriRequest;->isAborted()Z

    move-result v4

    if-nez v4, :cond_a

    .line 2963
    :try_start_2
    move-object/from16 v0, p0

    iget v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->connInfID:I

    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    iget v5, v5, Lorg/apache/http/impl/client/MultiSocketInputStream;->mDestIPVer:I

    invoke-static {v4, v5}, Lorg/apache/http/impl/client/CustomHttpClient;->getLocalAddrEx(II)Ljava/net/InetAddress;

    move-result-object v59

    .line 2967
    .local v59, "localAddress":Ljava/net/InetAddress;
    if-nez v59, :cond_9

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mRequestHandlers:[Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;
    invoke-static {v4}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$4000(Lorg/apache/http/impl/client/MultiSocketInputStream;)[Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;

    move-result-object v4

    aget-object v4, v4, v61

    iget v4, v4, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->rrStatus:I

    const/4 v5, -0x1

    if-ne v4, v5, :cond_9

    .line 2968
    sget-boolean v4, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v4, :cond_8

    .line 2969
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "The interface for socket id "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget v5, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->sockID:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " is null and "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "other thread is RR_STOPPED"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 2972
    :cond_8
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    move-object/from16 v0, p0

    iget v5, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->connInfID:I

    # invokes: Lorg/apache/http/impl/client/MultiSocketInputStream;->getOtherSocketID(I)I
    invoke-static {v4, v5}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$5700(Lorg/apache/http/impl/client/MultiSocketInputStream;I)I

    move-result v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    iget v5, v5, Lorg/apache/http/impl/client/MultiSocketInputStream;->mDestIPVer:I

    invoke-static {v4, v5}, Lorg/apache/http/impl/client/CustomHttpClient;->getLocalAddrEx(II)Ljava/net/InetAddress;

    move-result-object v59

    .line 2973
    if-eqz v59, :cond_9

    .line 2974
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mRequestHandlers:[Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;
    invoke-static {v4}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$4000(Lorg/apache/http/impl/client/MultiSocketInputStream;)[Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;

    move-result-object v4

    move-object/from16 v0, p0

    iget v5, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->sockID:I

    aget-object v4, v4, v5

    const/4 v5, -0x1

    iput v5, v4, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->rrStatus:I

    .line 2975
    const/4 v4, 0x1

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->bIOExceptionDuringContinueChunk:Z

    .line 2977
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    move-object/from16 v0, p0

    iget v5, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->connInfID:I

    # invokes: Lorg/apache/http/impl/client/MultiSocketInputStream;->getOtherSocketID(I)I
    invoke-static {v4, v5}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$5700(Lorg/apache/http/impl/client/MultiSocketInputStream;I)I

    move-result v4

    move-object/from16 v0, p0

    iput v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->connInfID:I

    .line 2978
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mRequestHandlers:[Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;
    invoke-static {v4}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$4000(Lorg/apache/http/impl/client/MultiSocketInputStream;)[Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;

    move-result-object v4

    move-object/from16 v0, p0

    iget v5, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->sockID:I

    aget-object v4, v4, v5

    const/4 v5, 0x2

    iput v5, v4, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->rrStatus:I

    .line 2979
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mRequestHandlers:[Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;
    invoke-static {v4}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$4000(Lorg/apache/http/impl/client/MultiSocketInputStream;)[Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;

    move-result-object v4

    move-object/from16 v0, p0

    iget v5, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->sockID:I

    aget-object v4, v4, v5

    const/4 v5, 0x0

    iput v5, v4, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->rrExceptionCount:I

    .line 2980
    sget-boolean v4, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v4, :cond_9

    .line 2981
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Other socket interface ip is not null and so changing the socket id to "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget v5, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->connInfID:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " for thread is :"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget v5, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->sockID:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 2990
    :cond_9
    move-object/from16 v0, p0

    iget v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->connInfID:I

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->reconnect(I)Z

    move-result v4

    if-eqz v4, :cond_22

    .line 2991
    sget-boolean v4, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v4, :cond_a

    .line 2992
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "download thread bind new socket "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mHttpHost:[Lorg/apache/http/HttpHost;
    invoke-static {v5}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$200(Lorg/apache/http/impl/client/MultiSocketInputStream;)[Lorg/apache/http/HttpHost;

    move-result-object v5

    move-object/from16 v0, p0

    iget v9, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->connInfID:I

    aget-object v5, v5, v9

    invoke-virtual {v5}, Lorg/apache/http/HttpHost;->getHostName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ":"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mHttpHost:[Lorg/apache/http/HttpHost;
    invoke-static {v5}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$200(Lorg/apache/http/impl/client/MultiSocketInputStream;)[Lorg/apache/http/HttpHost;

    move-result-object v5

    move-object/from16 v0, p0

    iget v9, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->connInfID:I

    aget-object v5, v5, v9

    invoke-virtual {v5}, Lorg/apache/http/HttpHost;->getPort()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_3

    .line 3019
    .end local v59    # "localAddress":Ljava/net/InetAddress;
    :cond_a
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    iget-boolean v4, v4, Lorg/apache/http/impl/client/MultiSocketInputStream;->mFinished:Z

    if-eqz v4, :cond_25

    .line 3020
    sget-boolean v4, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v4, :cond_b

    .line 3021
    const-string v4, "Full download complete -> mFinished = true, break"

    invoke-static {v4}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 3024
    :cond_b
    invoke-virtual/range {p0 .. p0}, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->closeHTTP()V

    .line 3661
    .end local v61    # "otherSockID":I
    .end local v72    # "startChunkTime":J
    :cond_c
    :goto_7
    sget-boolean v4, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v4, :cond_d

    .line 3662
    const-string v4, "download thread end"

    invoke-static {v4}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 3664
    :cond_d
    const/16 v16, 0x0

    .line 3665
    .local v16, "bSubmitHere":Z
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockManager:Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;
    invoke-static {v4}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$800(Lorg/apache/http/impl/client/MultiSocketInputStream;)Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;

    move-result-object v5

    monitor-enter v5

    .line 3666
    :try_start_3
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->finishedThreadNum:I
    invoke-static {v4}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$3900(Lorg/apache/http/impl/client/MultiSocketInputStream;)I

    move-result v4

    if-nez v4, :cond_7d

    const/16 v16, 0x1

    .line 3667
    :goto_8
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # operator++ for: Lorg/apache/http/impl/client/MultiSocketInputStream;->finishedThreadNum:I
    invoke-static {v4}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$3908(Lorg/apache/http/impl/client/MultiSocketInputStream;)I

    .line 3668
    sget-boolean v4, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v4, :cond_e

    .line 3669
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "total finished thread num = "

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v9, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->finishedThreadNum:I
    invoke-static {v9}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$3900(Lorg/apache/http/impl/client/MultiSocketInputStream;)I

    move-result v9

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 3672
    :cond_e
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockManager:Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;
    invoke-static {v4}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$800(Lorg/apache/http/impl/client/MultiSocketInputStream;)Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->notifyAll()V

    .line 3673
    :goto_9
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->finishedThreadNum:I
    invoke-static {v4}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$3900(Lorg/apache/http/impl/client/MultiSocketInputStream;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_13

    move-result v4

    const/4 v9, 0x2

    if-ge v4, v9, :cond_7e

    .line 3675
    :try_start_4
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockManager:Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;
    invoke-static {v4}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$800(Lorg/apache/http/impl/client/MultiSocketInputStream;)Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;

    move-result-object v4

    sget v9, Lorg/apache/http/impl/client/MultiSocketInputStream;->IDLE_THREAD_WAIT_INTERVAL:I

    int-to-long v10, v9

    invoke-virtual {v4, v10, v11}, Ljava/lang/Object;->wait(J)V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_13

    goto :goto_9

    .line 3676
    :catch_0
    move-exception v4

    goto :goto_9

    .line 2777
    .end local v14    # "bFirstBlockRsp":Z
    .end local v16    # "bSubmitHere":Z
    .end local v53    # "iRealBlockSize":I
    .end local v84    # "waitTime":J
    :catchall_0
    move-exception v4

    :try_start_5
    monitor-exit v5
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    throw v4

    .line 2802
    .restart local v14    # "bFirstBlockRsp":Z
    .restart local v53    # "iRealBlockSize":I
    .restart local v72    # "startChunkTime":J
    .restart local v84    # "waitTime":J
    :cond_f
    :try_start_6
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockStatus:[B
    invoke-static {v4}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1600(Lorg/apache/http/impl/client/MultiSocketInputStream;)[B

    move-result-object v4

    if-nez v4, :cond_10

    .line 2803
    sget-boolean v4, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v4, :cond_1

    .line 2804
    const-string v4, "Block status is not initialized"

    invoke-static {v4}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 2904
    .end local v72    # "startChunkTime":J
    :catchall_1
    move-exception v4

    monitor-exit v5
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    throw v4

    .line 2811
    .restart local v72    # "startChunkTime":J
    :cond_10
    :try_start_7
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mInBuffer:Ljava/util/HashMap;
    invoke-static {v4}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1500(Lorg/apache/http/impl/client/MultiSocketInputStream;)Ljava/util/HashMap;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/HashMap;->size()I

    move-result v4

    move-object/from16 v0, p0

    iget-object v9, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mMaxBlockNumber:I
    invoke-static {v9}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$3700(Lorg/apache/http/impl/client/MultiSocketInputStream;)I

    move-result v9

    if-gt v4, v9, :cond_16

    .line 2812
    const/16 v60, 0x0

    .line 2819
    .local v60, "number":I
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-object v9, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mReadBlockNumber:Ljava/lang/Integer;
    invoke-static {v9}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$5600(Lorg/apache/http/impl/client/MultiSocketInputStream;)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v9

    add-int/lit8 v9, v9, -0x1

    invoke-static {v4, v9}, Ljava/lang/Math;->max(II)I

    move-result v52

    .restart local v52    # "i":I
    :goto_a
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockStatus:[B
    invoke-static {v4}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1600(Lorg/apache/http/impl/client/MultiSocketInputStream;)[B

    move-result-object v4

    array-length v4, v4

    move/from16 v0, v52

    if-ge v0, v4, :cond_13

    .line 2820
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockStatus:[B
    invoke-static {v4}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1600(Lorg/apache/http/impl/client/MultiSocketInputStream;)[B

    move-result-object v4

    aget-byte v4, v4, v52

    const/4 v9, 0x3

    if-ne v4, v9, :cond_12

    .line 2821
    add-int/lit8 v60, v60, 0x1

    .line 2819
    :cond_11
    add-int/lit8 v52, v52, 0x1

    goto :goto_a

    .line 2822
    :cond_12
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockStatus:[B
    invoke-static {v4}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1600(Lorg/apache/http/impl/client/MultiSocketInputStream;)[B

    move-result-object v4

    aget-byte v4, v4, v52

    const/4 v9, 0x2

    if-gt v4, v9, :cond_11

    .line 2832
    :cond_13
    const/4 v4, 0x2

    move/from16 v0, v60

    if-ge v0, v4, :cond_14

    .line 2833
    sget-boolean v4, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v4, :cond_1

    .line 2834
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "BlockStatus: full read block "

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v60

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v9, " < "

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/4 v9, 0x2

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 2839
    :cond_14
    sget-boolean v4, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v4, :cond_15

    .line 2840
    const-string v4, "BlockStatus: there are continue number of portions fulled 2"

    invoke-static {v4}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 2843
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Is request aborted = "

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    iget-object v4, v4, Lorg/apache/http/impl/client/MultiSocketInputStream;->mMainRequest:Lorg/apache/http/HttpRequest;

    check-cast v4, Lorg/apache/http/client/methods/HttpUriRequest;

    invoke-interface {v4}, Lorg/apache/http/client/methods/HttpUriRequest;->isAborted()Z

    move-result v4

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 2846
    :cond_15
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    iget-object v4, v4, Lorg/apache/http/impl/client/MultiSocketInputStream;->mMainRequest:Lorg/apache/http/HttpRequest;

    check-cast v4, Lorg/apache/http/client/methods/HttpUriRequest;

    invoke-interface {v4}, Lorg/apache/http/client/methods/HttpUriRequest;->isAborted()Z

    move-result v4

    if-eqz v4, :cond_1b

    .line 2847
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    const/4 v9, 0x1

    iput-boolean v9, v4, Lorg/apache/http/impl/client/MultiSocketInputStream;->mFinished:Z

    goto/16 :goto_2

    .line 2852
    .end local v52    # "i":I
    .end local v60    # "number":I
    :cond_16
    sget-boolean v4, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v4, :cond_17

    .line 2853
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "BlockStatus: block size "

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v9, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mInBuffer:Ljava/util/HashMap;
    invoke-static {v9}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1500(Lorg/apache/http/impl/client/MultiSocketInputStream;)Ljava/util/HashMap;

    move-result-object v9

    invoke-virtual {v9}, Ljava/util/HashMap;->size()I

    move-result v9

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v9, " > Max Size "

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v9, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mMaxBlockNumber:I
    invoke-static {v9}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$3700(Lorg/apache/http/impl/client/MultiSocketInputStream;)I

    move-result v9

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 2857
    :cond_17
    const/4 v15, 0x0

    .line 2858
    .local v15, "bOtherBlocked":Z
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-object v9, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mReadBlockNumber:Ljava/lang/Integer;
    invoke-static {v9}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$5600(Lorg/apache/http/impl/client/MultiSocketInputStream;)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v9

    add-int/lit8 v9, v9, -0x1

    invoke-static {v4, v9}, Ljava/lang/Math;->max(II)I

    move-result v52

    .line 2859
    .restart local v52    # "i":I
    :goto_b
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-object v9, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mReadBlockNumber:Ljava/lang/Integer;
    invoke-static {v9}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$5600(Lorg/apache/http/impl/client/MultiSocketInputStream;)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v9

    invoke-static {v4, v9}, Ljava/lang/Math;->max(II)I

    move-result v4

    move/from16 v0, v52

    if-gt v0, v4, :cond_19

    .line 2860
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockStatus:[B
    invoke-static {v4}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1600(Lorg/apache/http/impl/client/MultiSocketInputStream;)[B

    move-result-object v4

    aget-byte v4, v4, v52

    const/4 v9, -0x1

    if-ne v4, v9, :cond_1a

    .line 2861
    sget-boolean v4, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v4, :cond_18

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "BlockStatus: a blocked chunk: "

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v52

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 2862
    :cond_18
    const/4 v15, 0x1

    .line 2866
    :cond_19
    if-nez v15, :cond_1

    .line 2868
    sget-boolean v4, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v4, :cond_1b

    .line 2869
    const/16 v52, 0x0

    :goto_c
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockStatus:[B
    invoke-static {v4}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1600(Lorg/apache/http/impl/client/MultiSocketInputStream;)[B

    move-result-object v4

    array-length v4, v4

    move/from16 v0, v52

    if-ge v0, v4, :cond_1b

    .line 2870
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "mBlockStatus["

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v52

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v9, "] "

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v9, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockStatus:[B
    invoke-static {v9}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1600(Lorg/apache/http/impl/client/MultiSocketInputStream;)[B

    move-result-object v9

    aget-byte v9, v9, v52

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 2869
    add-int/lit8 v52, v52, 0x1

    goto :goto_c

    .line 2859
    :cond_1a
    add-int/lit8 v52, v52, 0x1

    goto/16 :goto_b

    .line 2882
    .end local v15    # "bOtherBlocked":Z
    :cond_1b
    :try_start_8
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mInBuffer:Ljava/util/HashMap;
    invoke-static {v4}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1500(Lorg/apache/http/impl/client/MultiSocketInputStream;)Ljava/util/HashMap;

    move-result-object v4

    sget v9, Lorg/apache/http/impl/client/MultiSocketInputStream;->WAIT_TIME_TILL_READ_FROM_COMMON_BUFFER:I

    int-to-long v10, v9

    invoke-virtual {v4, v10, v11}, Ljava/lang/Object;->wait(J)V
    :try_end_8
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 2895
    :cond_1c
    :goto_d
    :try_start_9
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    sub-long v10, v10, v72

    sget v4, Lorg/apache/http/impl/client/MultiSocketInputStream;->WAIT_FOR_INTERFACE_TIME_OUT:I

    int-to-long v12, v4

    cmp-long v4, v10, v12

    if-lez v4, :cond_0

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBytesRemaining:J
    invoke-static {v4}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$700(Lorg/apache/http/impl/client/MultiSocketInputStream;)J

    move-result-wide v10

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBytesForMultiSocket:J
    invoke-static {v4}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1100(Lorg/apache/http/impl/client/MultiSocketInputStream;)J

    move-result-wide v12

    cmp-long v4, v10, v12

    if-gtz v4, :cond_0

    .line 2897
    sget-boolean v4, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v4, :cond_1d

    .line 2898
    const-string v4, "RangeRequest wait time out"

    invoke-static {v4}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 2900
    :cond_1d
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    const/4 v9, 0x1

    iput-boolean v9, v4, Lorg/apache/http/impl/client/MultiSocketInputStream;->mFinished:Z

    goto/16 :goto_1

    .line 2883
    :catch_1
    move-exception v45

    .line 2884
    .local v45, "e":Ljava/lang/Throwable;
    sget-boolean v4, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v4, :cond_1c

    .line 2885
    invoke-static/range {v45 .. v45}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/Throwable;)V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    goto :goto_d

    .line 2921
    .end local v45    # "e":Ljava/lang/Throwable;
    .restart local v22    # "blockedState":I
    .restart local v32    # "chunkNotRead":I
    .restart local v61    # "otherSockID":I
    :cond_1e
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockStatus:[B
    invoke-static {v4}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1600(Lorg/apache/http/impl/client/MultiSocketInputStream;)[B

    move-result-object v4

    aget-byte v4, v4, v52

    const/4 v5, -0x1

    if-ne v4, v5, :cond_3

    .line 2922
    add-int/lit8 v22, v22, 0x1

    .line 2923
    goto/16 :goto_5

    .line 2918
    :cond_1f
    add-int/lit8 v52, v52, 0x1

    goto/16 :goto_4

    .line 2933
    :cond_20
    const/4 v4, 0x1

    move/from16 v0, v32

    if-ne v0, v4, :cond_21

    .line 2936
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockManager:Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;
    invoke-static {v4}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$800(Lorg/apache/http/impl/client/MultiSocketInputStream;)Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;

    move-result-object v4

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->socketSpeed:[J
    invoke-static {v4}, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->access$5800(Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;)[J

    move-result-object v4

    move-object/from16 v0, p0

    iget v5, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->sockID:I

    aget-wide v4, v4, v5

    move-object/from16 v0, p0

    iget-object v9, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockManager:Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;
    invoke-static {v9}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$800(Lorg/apache/http/impl/client/MultiSocketInputStream;)Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;

    move-result-object v9

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->socketSpeed:[J
    invoke-static {v9}, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->access$5800(Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;)[J

    move-result-object v9

    aget-wide v10, v9, v61

    const-wide/16 v12, 0x2

    mul-long/2addr v10, v12

    cmp-long v4, v4, v10

    if-gtz v4, :cond_5

    .line 2944
    :cond_21
    :try_start_a
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mInBuffer:Ljava/util/HashMap;
    invoke-static {v4}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1500(Lorg/apache/http/impl/client/MultiSocketInputStream;)Ljava/util/HashMap;

    move-result-object v5

    monitor-enter v5
    :try_end_a
    .catch Ljava/lang/Throwable; {:try_start_a .. :try_end_a} :catch_2

    .line 2945
    :try_start_b
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mInBuffer:Ljava/util/HashMap;
    invoke-static {v4}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1500(Lorg/apache/http/impl/client/MultiSocketInputStream;)Ljava/util/HashMap;

    move-result-object v4

    sget v9, Lorg/apache/http/impl/client/MultiSocketInputStream;->WAIT_TIME_TILL_READ_FROM_COMMON_BUFFER:I

    int-to-long v10, v9

    invoke-virtual {v4, v10, v11}, Ljava/lang/Object;->wait(J)V

    .line 2946
    monitor-exit v5

    goto/16 :goto_3

    :catchall_2
    move-exception v4

    monitor-exit v5
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_2

    :try_start_c
    throw v4
    :try_end_c
    .catch Ljava/lang/Throwable; {:try_start_c .. :try_end_c} :catch_2

    .line 2947
    :catch_2
    move-exception v45

    .line 2948
    .restart local v45    # "e":Ljava/lang/Throwable;
    sget-boolean v4, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v4, :cond_2

    .line 2949
    invoke-static/range {v45 .. v45}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/Throwable;)V

    goto/16 :goto_3

    .line 2999
    .end local v22    # "blockedState":I
    .end local v32    # "chunkNotRead":I
    .end local v45    # "e":Ljava/lang/Throwable;
    .end local v52    # "i":I
    .restart local v59    # "localAddress":Ljava/net/InetAddress;
    :cond_22
    :try_start_d
    sget-boolean v4, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v4, :cond_23

    .line 3000
    const-string v4, "Reconnect returns false."

    invoke-static {v4}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 3001
    :cond_23
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v5

    monitor-enter v5
    :try_end_d
    .catch Ljava/lang/Throwable; {:try_start_d .. :try_end_d} :catch_3

    .line 3002
    :try_start_e
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v4

    sget v9, Lorg/apache/http/impl/client/MultiSocketInputStream;->IDLE_THREAD_WAIT_INTERVAL:I

    int-to-long v10, v9

    invoke-virtual {v4, v10, v11}, Ljava/lang/Object;->wait(J)V

    .line 3003
    monitor-exit v5

    goto/16 :goto_6

    :catchall_3
    move-exception v4

    monitor-exit v5
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_3

    :try_start_f
    throw v4
    :try_end_f
    .catch Ljava/lang/Throwable; {:try_start_f .. :try_end_f} :catch_3

    .line 3005
    .end local v59    # "localAddress":Ljava/net/InetAddress;
    :catch_3
    move-exception v45

    .line 3006
    .restart local v45    # "e":Ljava/lang/Throwable;
    sget-boolean v4, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v4, :cond_24

    .line 3007
    const-string v4, "HttpException is thrown while reconnecting"

    invoke-static {v4}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 3010
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "is RequestAborted = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    iget-object v4, v4, Lorg/apache/http/impl/client/MultiSocketInputStream;->mMainRequest:Lorg/apache/http/HttpRequest;

    check-cast v4, Lorg/apache/http/client/methods/HttpUriRequest;

    invoke-interface {v4}, Lorg/apache/http/client/methods/HttpUriRequest;->isAborted()Z

    move-result v4

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 3012
    invoke-static/range {v45 .. v45}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/Throwable;)V

    .line 3014
    :cond_24
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    iget-object v4, v4, Lorg/apache/http/impl/client/MultiSocketInputStream;->mMainRequest:Lorg/apache/http/HttpRequest;

    check-cast v4, Lorg/apache/http/client/methods/HttpUriRequest;

    invoke-interface {v4}, Lorg/apache/http/client/methods/HttpUriRequest;->isAborted()Z

    move-result v4

    if-eqz v4, :cond_6

    .line 3015
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    const/4 v5, 0x1

    iput-boolean v5, v4, Lorg/apache/http/impl/client/MultiSocketInputStream;->mFinished:Z

    goto/16 :goto_6

    .line 3028
    .end local v45    # "e":Ljava/lang/Throwable;
    :cond_25
    sget-boolean v4, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v4, :cond_26

    .line 3029
    const-string v4, "start to get another range request "

    invoke-static {v4}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 3034
    :cond_26
    const-wide/16 v28, 0x0

    .line 3035
    .local v28, "bytesToRead":J
    const-wide/16 v82, 0x0

    .line 3037
    .local v82, "totallen":J
    const/16 v17, 0x0

    .line 3039
    .local v17, "blockInfo":[J
    :try_start_10
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockManager:Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;
    invoke-static {v4}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$800(Lorg/apache/http/impl/client/MultiSocketInputStream;)Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;

    move-result-object v5

    monitor-enter v5
    :try_end_10
    .catch Ljava/lang/Throwable; {:try_start_10 .. :try_end_10} :catch_5

    .line 3040
    :try_start_11
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockManager:Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;
    invoke-static {v4}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$800(Lorg/apache/http/impl/client/MultiSocketInputStream;)Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;

    move-result-object v4

    move-object/from16 v0, p0

    iget v9, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->sockID:I

    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->dbuf:Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;

    invoke-virtual {v4, v9, v10}, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->getNextHTTPBlock(ILorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;)[J

    move-result-object v17

    .line 3041
    monitor-exit v5
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_5

    .line 3051
    :cond_27
    :goto_e
    if-eqz v17, :cond_28

    move-object/from16 v0, v17

    array-length v4, v0

    const/4 v5, 0x4

    if-ge v4, v5, :cond_2d

    .line 3053
    :cond_28
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v78

    .line 3055
    .local v78, "tmpWaitTime":J
    :cond_29
    :goto_f
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    iget-boolean v4, v4, Lorg/apache/http/impl/client/MultiSocketInputStream;->mFinished:Z

    if-nez v4, :cond_2b

    move-object/from16 v0, p0

    iget v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->rrStatus:I

    const/4 v5, -0x1

    if-eq v4, v5, :cond_2b

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mRangeRequestSuccess:I
    invoke-static {v4}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$2300(Lorg/apache/http/impl/client/MultiSocketInputStream;)I

    move-result v4

    const/4 v5, 0x1

    if-eq v4, v5, :cond_2b

    if-eqz v17, :cond_2a

    move-object/from16 v0, v17

    array-length v4, v0

    const/4 v5, 0x4

    if-ge v4, v5, :cond_2b

    .line 3059
    :cond_2a
    :try_start_12
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v5

    monitor-enter v5
    :try_end_12
    .catch Ljava/lang/Throwable; {:try_start_12 .. :try_end_12} :catch_4

    .line 3060
    :try_start_13
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v4

    sget v9, Lorg/apache/http/impl/client/MultiSocketInputStream;->IDLE_THREAD_WAIT_INTERVAL:I

    int-to-long v10, v9

    invoke-virtual {v4, v10, v11}, Ljava/lang/Object;->wait(J)V

    .line 3061
    monitor-exit v5
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_6

    .line 3063
    :try_start_14
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockManager:Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;
    invoke-static {v4}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$800(Lorg/apache/http/impl/client/MultiSocketInputStream;)Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;

    move-result-object v5

    monitor-enter v5
    :try_end_14
    .catch Ljava/lang/Throwable; {:try_start_14 .. :try_end_14} :catch_4

    .line 3064
    :try_start_15
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockManager:Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;
    invoke-static {v4}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$800(Lorg/apache/http/impl/client/MultiSocketInputStream;)Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;

    move-result-object v4

    move-object/from16 v0, p0

    iget v9, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->sockID:I

    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->dbuf:Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;

    invoke-virtual {v4, v9, v10}, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->getNextHTTPBlock(ILorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;)[J

    move-result-object v17

    .line 3065
    monitor-exit v5

    goto :goto_f

    :catchall_4
    move-exception v4

    monitor-exit v5
    :try_end_15
    .catchall {:try_start_15 .. :try_end_15} :catchall_4

    :try_start_16
    throw v4
    :try_end_16
    .catch Ljava/lang/Throwable; {:try_start_16 .. :try_end_16} :catch_4

    .line 3066
    :catch_4
    move-exception v45

    .line 3067
    .restart local v45    # "e":Ljava/lang/Throwable;
    sget-boolean v4, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v4, :cond_29

    .line 3068
    invoke-static/range {v45 .. v45}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/Throwable;)V

    goto :goto_f

    .line 3041
    .end local v45    # "e":Ljava/lang/Throwable;
    .end local v78    # "tmpWaitTime":J
    :catchall_5
    move-exception v4

    :try_start_17
    monitor-exit v5
    :try_end_17
    .catchall {:try_start_17 .. :try_end_17} :catchall_5

    :try_start_18
    throw v4
    :try_end_18
    .catch Ljava/lang/Throwable; {:try_start_18 .. :try_end_18} :catch_5

    .line 3042
    :catch_5
    move-exception v45

    .line 3043
    .restart local v45    # "e":Ljava/lang/Throwable;
    sget-boolean v4, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v4, :cond_27

    .line 3044
    invoke-static/range {v45 .. v45}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/Throwable;)V

    goto :goto_e

    .line 3061
    .end local v45    # "e":Ljava/lang/Throwable;
    .restart local v78    # "tmpWaitTime":J
    :catchall_6
    move-exception v4

    :try_start_19
    monitor-exit v5
    :try_end_19
    .catchall {:try_start_19 .. :try_end_19} :catchall_6

    :try_start_1a
    throw v4
    :try_end_1a
    .catch Ljava/lang/Throwable; {:try_start_1a .. :try_end_1a} :catch_4

    .line 3073
    :cond_2b
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    add-long v4, v4, v84

    sub-long v84, v4, v78

    .line 3074
    if-eqz v17, :cond_2c

    move-object/from16 v0, v17

    array-length v4, v0

    const/4 v5, 0x4

    if-lt v4, v5, :cond_2c

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    iget-boolean v4, v4, Lorg/apache/http/impl/client/MultiSocketInputStream;->mFinished:Z

    if-eqz v4, :cond_2f

    .line 3076
    :cond_2c
    sget-boolean v4, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v4, :cond_c

    .line 3077
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getNextHTTPBlock() results 0, finish reading totalLength("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mContentLength:J
    invoke-static {v5}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$400(Lorg/apache/http/impl/client/MultiSocketInputStream;)J

    move-result-wide v10

    invoke-virtual {v4, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "), break"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    goto/16 :goto_7

    .line 3083
    .end local v78    # "tmpWaitTime":J
    :cond_2d
    move-object/from16 v0, p0

    iget v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->rrStatus:I

    const/4 v5, -0x1

    if-ne v4, v5, :cond_2e

    .line 3084
    sget-boolean v4, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v4, :cond_2f

    .line 3085
    const-string v4, "this thread is stopped, and the other thread is downloading continue chunk"

    invoke-static {v4}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    goto/16 :goto_7

    .line 3089
    :cond_2e
    sget-boolean v4, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v4, :cond_2f

    .line 3090
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getNextHTTPBlock() results id "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/4 v5, 0x3

    aget-wide v10, v17, v5

    invoke-virtual {v4, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ": "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/4 v5, 0x0

    aget-wide v10, v17, v5

    invoke-virtual {v4, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "-"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/4 v5, 0x1

    aget-wide v10, v17, v5

    invoke-virtual {v4, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 3095
    :cond_2f
    const/4 v4, 0x2

    aget-wide v28, v17, v4

    .line 3096
    const/4 v4, 0x3

    aget-wide v4, v17, v4

    long-to-int v4, v4

    move-object/from16 v0, p0

    iput v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->currentBlockNumber:I

    .line 3099
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockManager:Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;
    invoke-static {v4}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$800(Lorg/apache/http/impl/client/MultiSocketInputStream;)Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;

    move-result-object v4

    iget-object v4, v4, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->toBeReadLen:[J

    move-object/from16 v0, p0

    iget v5, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->sockID:I

    aput-wide v28, v4, v5

    .line 3103
    const/16 v25, 0x0

    .line 3104
    .local v25, "buf_offset":I
    const/16 v26, 0x0

    .line 3106
    .local v26, "buf_ret":I
    const/16 v31, 0x0

    .line 3108
    .local v31, "chunkLengths":[I
    const/16 v30, 0x0

    .line 3110
    .local v30, "chunkIndex":I
    const/16 v80, 0x0

    .line 3112
    .local v80, "totalContinuousChunk":I
    :try_start_1b
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mInBuffer:Ljava/util/HashMap;
    invoke-static {v4}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1500(Lorg/apache/http/impl/client/MultiSocketInputStream;)Ljava/util/HashMap;

    move-result-object v5

    monitor-enter v5
    :try_end_1b
    .catch Lorg/apache/http/HttpException; {:try_start_1b .. :try_end_1b} :catch_6
    .catch Ljava/lang/InterruptedException; {:try_start_1b .. :try_end_1b} :catch_7
    .catch Ljava/lang/Throwable; {:try_start_1b .. :try_end_1b} :catch_8

    .line 3113
    :try_start_1c
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mInBuffer:Ljava/util/HashMap;
    invoke-static {v4}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1500(Lorg/apache/http/impl/client/MultiSocketInputStream;)Ljava/util/HashMap;

    move-result-object v4

    new-instance v9, Ljava/lang/Integer;

    move-object/from16 v0, p0

    iget v10, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->currentBlockNumber:I

    invoke-direct {v9, v10}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v4, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;

    move-object/from16 v0, p0

    iput-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->dbuf:Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;

    .line 3114
    monitor-exit v5
    :try_end_1c
    .catchall {:try_start_1c .. :try_end_1c} :catchall_7

    .line 3118
    :try_start_1d
    move-object/from16 v0, p0

    iget v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->currentBlockNumber:I

    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockStatus:[B
    invoke-static {v5}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1600(Lorg/apache/http/impl/client/MultiSocketInputStream;)[B

    move-result-object v5

    array-length v5, v5

    add-int/lit8 v5, v5, -0x1

    if-eq v4, v5, :cond_36

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockSize:I
    invoke-static {v4}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$900(Lorg/apache/http/impl/client/MultiSocketInputStream;)I

    move-result v4

    int-to-long v4, v4

    cmp-long v4, v28, v4

    if-lez v4, :cond_36

    .line 3119
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->dbuf:Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;

    invoke-virtual {v4}, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->getTotalChunks()I

    move-result v80

    .line 3120
    move/from16 v0, v80

    new-array v0, v0, [I

    move-object/from16 v31, v0

    .line 3122
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->dbuf:Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;

    invoke-virtual {v4}, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->getToBeReadLength()J

    move-result-wide v4

    long-to-int v4, v4

    aput v4, v31, v30

    .line 3124
    sget-boolean v4, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v4, :cond_30

    .line 3125
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "chunkLengths[0]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/4 v5, 0x0

    aget v5, v31, v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 3128
    :cond_30
    const/16 v55, 0x1

    .local v55, "j":I
    :goto_10
    move-object/from16 v0, v31

    array-length v4, v0

    move/from16 v0, v55

    if-ge v0, v4, :cond_36

    .line 3130
    move-object/from16 v0, p0

    iget v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->currentBlockNumber:I

    add-int v4, v4, v55

    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockStatus:[B
    invoke-static {v5}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1600(Lorg/apache/http/impl/client/MultiSocketInputStream;)[B

    move-result-object v5

    array-length v5, v5

    add-int/lit8 v5, v5, -0x1

    if-ne v4, v5, :cond_34

    .line 3133
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBytesForMultiSocket:J
    invoke-static {v4}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1100(Lorg/apache/http/impl/client/MultiSocketInputStream;)J

    move-result-wide v4

    move-object/from16 v0, p0

    iget v9, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->currentBlockNumber:I

    add-int v9, v9, v55

    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockSize:I
    invoke-static {v10}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$900(Lorg/apache/http/impl/client/MultiSocketInputStream;)I

    move-result v10

    mul-int/2addr v9, v10

    int-to-long v10, v9

    sub-long/2addr v4, v10

    long-to-int v4, v4

    aput v4, v31, v55

    .line 3139
    :goto_11
    sget-boolean v4, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v4, :cond_31

    .line 3140
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "chunkLengths["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v55

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget v5, v31, v55

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V
    :try_end_1d
    .catch Lorg/apache/http/HttpException; {:try_start_1d .. :try_end_1d} :catch_6
    .catch Ljava/lang/InterruptedException; {:try_start_1d .. :try_end_1d} :catch_7
    .catch Ljava/lang/Throwable; {:try_start_1d .. :try_end_1d} :catch_8

    .line 3128
    :cond_31
    add-int/lit8 v55, v55, 0x1

    goto :goto_10

    .line 3114
    .end local v55    # "j":I
    :catchall_7
    move-exception v4

    :try_start_1e
    monitor-exit v5
    :try_end_1e
    .catchall {:try_start_1e .. :try_end_1e} :catchall_7

    :try_start_1f
    throw v4
    :try_end_1f
    .catch Lorg/apache/http/HttpException; {:try_start_1f .. :try_end_1f} :catch_6
    .catch Ljava/lang/InterruptedException; {:try_start_1f .. :try_end_1f} :catch_7
    .catch Ljava/lang/Throwable; {:try_start_1f .. :try_end_1f} :catch_8

    .line 3553
    :catch_6
    move-exception v45

    .line 3554
    .local v45, "e":Lorg/apache/http/HttpException;
    sget-boolean v4, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v4, :cond_32

    .line 3555
    const-string v4, "HttpException is thrown"

    invoke-static {v4}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 3556
    invoke-static/range {v45 .. v45}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/Throwable;)V

    .line 3560
    :cond_32
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    iget-object v4, v4, Lorg/apache/http/impl/client/MultiSocketInputStream;->mMainRequest:Lorg/apache/http/HttpRequest;

    check-cast v4, Lorg/apache/http/client/methods/HttpUriRequest;

    invoke-interface {v4}, Lorg/apache/http/client/methods/HttpUriRequest;->isAborted()Z

    move-result v4

    if-eqz v4, :cond_33

    .line 3561
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    const/4 v5, 0x1

    iput-boolean v5, v4, Lorg/apache/http/impl/client/MultiSocketInputStream;->mFinished:Z

    .line 3654
    .end local v45    # "e":Lorg/apache/http/HttpException;
    :cond_33
    :goto_12
    move-object/from16 v0, p0

    iget-wide v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->totalReadSize:J

    add-long v4, v4, v82

    move-object/from16 v0, p0

    iput-wide v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->totalReadSize:J

    .line 3656
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    move-object/from16 v0, p0

    iget-wide v10, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->t0:J

    sub-long/2addr v4, v10

    sub-long v4, v4, v84

    move-object/from16 v0, p0

    iput-wide v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->totalElapsedTime:J

    goto/16 :goto_0

    .line 3137
    .restart local v55    # "j":I
    :cond_34
    :try_start_20
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockSize:I
    invoke-static {v4}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$900(Lorg/apache/http/impl/client/MultiSocketInputStream;)I

    move-result v4

    aput v4, v31, v55
    :try_end_20
    .catch Lorg/apache/http/HttpException; {:try_start_20 .. :try_end_20} :catch_6
    .catch Ljava/lang/InterruptedException; {:try_start_20 .. :try_end_20} :catch_7
    .catch Ljava/lang/Throwable; {:try_start_20 .. :try_end_20} :catch_8

    goto :goto_11

    .line 3563
    .end local v55    # "j":I
    :catch_7
    move-exception v51

    .line 3564
    .local v51, "ex":Ljava/lang/InterruptedException;
    sget-boolean v4, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v4, :cond_35

    .line 3565
    const-string v4, "in InterruptedException, handle handover"

    invoke-static {v4}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 3569
    :cond_35
    invoke-virtual/range {p0 .. p0}, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->closeHTTP()V

    goto :goto_12

    .line 3146
    .end local v51    # "ex":Ljava/lang/InterruptedException;
    :cond_36
    :try_start_21
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    move-object/from16 v0, p0

    iput-wide v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->t1:J

    .line 3148
    const/16 v56, 0x0

    .line 3157
    .local v56, "len":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->director:Lorg/apache/http/impl/client/DefaultRequestDirector;

    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mHttpRequest:[Lorg/apache/http/HttpRequest;
    invoke-static {v5}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$2400(Lorg/apache/http/impl/client/MultiSocketInputStream;)[Lorg/apache/http/HttpRequest;

    move-result-object v5

    move-object/from16 v0, p0

    iget v9, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->connInfID:I

    aget-object v5, v5, v9

    invoke-interface {v5}, Lorg/apache/http/HttpRequest;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v5

    const-string v9, "http.route.default-proxy"

    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->rrCustomHttpClient:Lorg/apache/http/impl/client/CustomHttpClient;

    move-object/from16 v0, p0

    iget v11, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->connInfID:I

    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mHttpHost:[Lorg/apache/http/HttpHost;
    invoke-static {v12}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$200(Lorg/apache/http/impl/client/MultiSocketInputStream;)[Lorg/apache/http/HttpHost;

    move-result-object v12

    move-object/from16 v0, p0

    iget v13, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->connInfID:I

    aget-object v12, v12, v13

    invoke-virtual {v10, v11, v12}, Lorg/apache/http/impl/client/CustomHttpClient;->getProxy(ILorg/apache/http/HttpHost;)Lorg/apache/http/HttpHost;

    move-result-object v10

    invoke-virtual {v4, v5, v9, v10}, Lorg/apache/http/impl/client/DefaultRequestDirector;->setParameter(Lorg/apache/http/params/HttpParams;Ljava/lang/String;Ljava/lang/Object;)V

    .line 3161
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->director:Lorg/apache/http/impl/client/DefaultRequestDirector;

    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mHttpHost:[Lorg/apache/http/HttpHost;
    invoke-static {v5}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$200(Lorg/apache/http/impl/client/MultiSocketInputStream;)[Lorg/apache/http/HttpHost;

    move-result-object v5

    move-object/from16 v0, p0

    iget v9, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->connInfID:I

    aget-object v5, v5, v9

    move-object/from16 v0, p0

    iget-object v9, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mHttpRequest:[Lorg/apache/http/HttpRequest;
    invoke-static {v9}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$2400(Lorg/apache/http/impl/client/MultiSocketInputStream;)[Lorg/apache/http/HttpRequest;

    move-result-object v9

    move-object/from16 v0, p0

    iget v10, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->connInfID:I

    aget-object v6, v9, v10

    move-object/from16 v0, p0

    iget-object v9, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mHttpContext:Lorg/apache/http/protocol/HttpContext;
    invoke-static {v9}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$100(Lorg/apache/http/impl/client/MultiSocketInputStream;)Lorg/apache/http/protocol/HttpContext;

    move-result-object v7

    const/4 v9, 0x0

    aget-wide v8, v17, v9

    const/4 v10, 0x1

    aget-wide v10, v17, v10

    move-object/from16 v0, p0

    iget v12, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->connInfID:I

    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    iget v13, v13, Lorg/apache/http/impl/client/MultiSocketInputStream;->mDestIPVer:I

    invoke-static {v12, v13}, Lorg/apache/http/impl/client/CustomHttpClient;->getLocalAddrEx(II)Ljava/net/InetAddress;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mRemoteAddress:[Ljava/net/InetAddress;
    invoke-static {v13}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$2900(Lorg/apache/http/impl/client/MultiSocketInputStream;)[Ljava/net/InetAddress;

    move-result-object v13

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->connInfID:I

    move/from16 v81, v0

    aget-object v13, v13, v81

    invoke-virtual/range {v4 .. v13}, Lorg/apache/http/impl/client/DefaultRequestDirector;->execute(Lorg/apache/http/HttpHost;Lorg/apache/http/HttpRequest;Lorg/apache/http/protocol/HttpContext;JJLjava/net/InetAddress;Ljava/net/InetAddress;)Lorg/apache/http/HttpResponse;

    move-result-object v67

    .line 3166
    .local v67, "response":Lorg/apache/http/HttpResponse;
    sget-boolean v4, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v4, :cond_37

    .line 3167
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " Response time difference from main for range request id: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/4 v5, 0x3

    aget-wide v10, v17, v5

    invoke-virtual {v4, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ": "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/4 v5, 0x0

    aget-wide v10, v17, v5

    invoke-virtual {v4, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "-"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/4 v5, 0x1

    aget-wide v10, v17, v5

    invoke-virtual {v4, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " time:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mResponeTimeFromMainSocket:J
    invoke-static {v5}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$5900(Lorg/apache/http/impl/client/MultiSocketInputStream;)J

    move-result-wide v12

    sub-long/2addr v10, v12

    invoke-virtual {v4, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 3177
    :cond_37
    const-string v4, "Connection"

    move-object/from16 v0, v67

    invoke-interface {v0, v4}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v33

    .line 3178
    .local v33, "connectionCloseHeader":Lorg/apache/http/Header;
    if-nez v33, :cond_41

    const/4 v4, 0x0

    :goto_13
    move-object/from16 v0, p0

    iput-boolean v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->mbReconnect:Z

    .line 3181
    sget-boolean v4, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v4, :cond_38

    .line 3182
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "RangeRequest: Reconnect = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->mbReconnect:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 3185
    :cond_38
    invoke-interface/range {v67 .. v67}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v4

    invoke-interface {v4}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v68

    .line 3186
    .local v68, "rspCode":I
    invoke-interface/range {v67 .. v67}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v50

    .line 3187
    .local v50, "entity":Lorg/apache/http/HttpEntity;
    invoke-interface/range {v50 .. v50}, Lorg/apache/http/HttpEntity;->getContentLength()J

    move-result-wide v34

    .line 3188
    .local v34, "contentLength":J
    invoke-interface/range {v50 .. v50}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v54

    .line 3190
    .local v54, "in":Ljava/io/InputStream;
    const/16 v4, 0xce

    move/from16 v0, v68

    if-eq v0, v4, :cond_39

    const/16 v4, 0xc8

    move/from16 v0, v68

    if-ne v0, v4, :cond_3a

    :cond_39
    cmp-long v4, v34, v28

    if-eqz v4, :cond_43

    .line 3192
    :cond_3a
    sget-boolean v4, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v4, :cond_3b

    .line 3193
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "response code is not 206 or 200 : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v68

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " or length is not expected: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-wide/from16 v0, v34

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "but length expected is :"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/4 v5, 0x1

    aget-wide v10, v17, v5

    const/4 v5, 0x0

    aget-wide v12, v17, v5

    sub-long/2addr v10, v12

    invoke-virtual {v4, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 3203
    :cond_3b
    if-eqz v14, :cond_42

    .line 3204
    sget-boolean v4, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v4, :cond_3c

    .line 3205
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "move to RR_FAILED from "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mRangeRequestSuccess:I
    invoke-static {v5}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$2300(Lorg/apache/http/impl/client/MultiSocketInputStream;)I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 3207
    :cond_3c
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockManager:Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;
    invoke-static {v4}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$800(Lorg/apache/http/impl/client/MultiSocketInputStream;)Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;

    move-result-object v5

    monitor-enter v5
    :try_end_21
    .catch Lorg/apache/http/HttpException; {:try_start_21 .. :try_end_21} :catch_6
    .catch Ljava/lang/InterruptedException; {:try_start_21 .. :try_end_21} :catch_7
    .catch Ljava/lang/Throwable; {:try_start_21 .. :try_end_21} :catch_8

    .line 3208
    :try_start_22
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mRangeRequestSuccess:I
    invoke-static {v4}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$2300(Lorg/apache/http/impl/client/MultiSocketInputStream;)I

    move-result v4

    if-eqz v4, :cond_3d

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mRangeRequestSuccess:I
    invoke-static {v4}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$2300(Lorg/apache/http/impl/client/MultiSocketInputStream;)I

    move-result v4

    const/4 v9, 0x2

    if-ne v4, v9, :cond_3e

    .line 3210
    :cond_3d
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    const/4 v9, 0x1

    # setter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mRangeRequestSuccess:I
    invoke-static {v4, v9}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$2302(Lorg/apache/http/impl/client/MultiSocketInputStream;I)I

    .line 3211
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockManager:Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;
    invoke-static {v4}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$800(Lorg/apache/http/impl/client/MultiSocketInputStream;)Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->notifyAll()V

    .line 3213
    :cond_3e
    monitor-exit v5
    :try_end_22
    .catchall {:try_start_22 .. :try_end_22} :catchall_9

    .line 3214
    :try_start_23
    invoke-virtual/range {p0 .. p0}, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->closeHTTP()V
    :try_end_23
    .catch Lorg/apache/http/HttpException; {:try_start_23 .. :try_end_23} :catch_6
    .catch Ljava/lang/InterruptedException; {:try_start_23 .. :try_end_23} :catch_7
    .catch Ljava/lang/Throwable; {:try_start_23 .. :try_end_23} :catch_8

    goto/16 :goto_7

    .line 3570
    .end local v33    # "connectionCloseHeader":Lorg/apache/http/Header;
    .end local v34    # "contentLength":J
    .end local v50    # "entity":Lorg/apache/http/HttpEntity;
    .end local v54    # "in":Ljava/io/InputStream;
    .end local v56    # "len":I
    .end local v67    # "response":Lorg/apache/http/HttpResponse;
    .end local v68    # "rspCode":I
    :catch_8
    move-exception v45

    .line 3571
    .local v45, "e":Ljava/lang/Throwable;
    sget-boolean v4, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v4, :cond_3f

    .line 3572
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "reading thread blocked by some Exception: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {v45 .. v45}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 3574
    invoke-static/range {v45 .. v45}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/Throwable;)V

    .line 3578
    :cond_3f
    move-object/from16 v0, p0

    iget v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->rrExceptionCount:I

    add-int/lit8 v4, v4, 0x1

    move-object/from16 v0, p0

    iput v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->rrExceptionCount:I

    .line 3582
    if-eqz v14, :cond_74

    .line 3583
    sget-boolean v4, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v4, :cond_40

    .line 3584
    const-string v4, "move to RR_EXCEPTION"

    invoke-static {v4}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 3586
    :cond_40
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockManager:Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;
    invoke-static {v4}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$800(Lorg/apache/http/impl/client/MultiSocketInputStream;)Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;

    move-result-object v5

    monitor-enter v5

    .line 3587
    :try_start_24
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mRangeRequestSuccess:I
    invoke-static {v4}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$2300(Lorg/apache/http/impl/client/MultiSocketInputStream;)I

    move-result v4

    const/4 v9, 0x2

    if-ne v4, v9, :cond_71

    move-object/from16 v0, p0

    iget v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->rrStatus:I

    const/4 v9, -0x1

    if-ne v4, v9, :cond_71

    .line 3588
    if-lez v80, :cond_70

    .line 3589
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockManager:Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;
    invoke-static {v4}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$800(Lorg/apache/http/impl/client/MultiSocketInputStream;)Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;

    move-result-object v4

    move-object/from16 v0, p0

    iget v9, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->currentBlockNumber:I

    add-int v9, v9, v30

    move-object/from16 v0, p0

    iget v10, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->sockID:I

    invoke-virtual {v4, v9, v10}, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->block(II)V

    .line 3593
    :goto_14
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    const/4 v9, 0x3

    # setter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mRangeRequestSuccess:I
    invoke-static {v4, v9}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$2302(Lorg/apache/http/impl/client/MultiSocketInputStream;I)I

    .line 3594
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockManager:Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;
    invoke-static {v4}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$800(Lorg/apache/http/impl/client/MultiSocketInputStream;)Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->notifyAll()V

    .line 3595
    invoke-virtual/range {p0 .. p0}, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->closeHTTP()V

    .line 3596
    monitor-exit v5

    goto/16 :goto_7

    .line 3603
    :catchall_8
    move-exception v4

    monitor-exit v5
    :try_end_24
    .catchall {:try_start_24 .. :try_end_24} :catchall_8

    throw v4

    .line 3178
    .end local v45    # "e":Ljava/lang/Throwable;
    .restart local v33    # "connectionCloseHeader":Lorg/apache/http/Header;
    .restart local v56    # "len":I
    .restart local v67    # "response":Lorg/apache/http/HttpResponse;
    :cond_41
    :try_start_25
    invoke-interface/range {v33 .. v33}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v4

    const-string v5, "Close"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z
    :try_end_25
    .catch Lorg/apache/http/HttpException; {:try_start_25 .. :try_end_25} :catch_6
    .catch Ljava/lang/InterruptedException; {:try_start_25 .. :try_end_25} :catch_7
    .catch Ljava/lang/Throwable; {:try_start_25 .. :try_end_25} :catch_8

    move-result v4

    goto/16 :goto_13

    .line 3213
    .restart local v34    # "contentLength":J
    .restart local v50    # "entity":Lorg/apache/http/HttpEntity;
    .restart local v54    # "in":Ljava/io/InputStream;
    .restart local v68    # "rspCode":I
    :catchall_9
    move-exception v4

    :try_start_26
    monitor-exit v5
    :try_end_26
    .catchall {:try_start_26 .. :try_end_26} :catchall_9

    :try_start_27
    throw v4

    .line 3217
    :cond_42
    new-instance v4, Ljava/lang/Exception;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "response code is not 206 or 200 : "

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v68

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v4

    .line 3222
    :cond_43
    sget-boolean v4, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v4, :cond_44

    .line 3223
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "response code is 206 or 200 : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v68

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " and length is expected: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-wide/from16 v0, v34

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 3228
    :cond_44
    if-eqz v14, :cond_49

    .line 3229
    const/4 v14, 0x0

    .line 3230
    sget-boolean v4, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v4, :cond_45

    .line 3231
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "move to RR_SUCCESS from "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mRangeRequestSuccess:I
    invoke-static {v5}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$2300(Lorg/apache/http/impl/client/MultiSocketInputStream;)I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 3236
    :cond_45
    const-string v4, "Content-Range"

    move-object/from16 v0, v67

    invoke-interface {v0, v4}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v66

    .line 3238
    .local v66, "rangeRequestTotalContentLength":Lorg/apache/http/Header;
    invoke-interface/range {v66 .. v66}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v27

    .line 3240
    .local v27, "byteRange":Ljava/lang/String;
    const-string v4, "/"

    move-object/from16 v0, v27

    invoke-virtual {v0, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    invoke-virtual/range {v27 .. v27}, Ljava/lang/String;->length()I

    move-result v5

    move-object/from16 v0, v27

    invoke-virtual {v0, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v36

    .line 3245
    .local v36, "contentLengthRangeRequest":J
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockManager:Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;
    invoke-static {v4}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$800(Lorg/apache/http/impl/client/MultiSocketInputStream;)Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;

    move-result-object v5

    monitor-enter v5
    :try_end_27
    .catch Lorg/apache/http/HttpException; {:try_start_27 .. :try_end_27} :catch_6
    .catch Ljava/lang/InterruptedException; {:try_start_27 .. :try_end_27} :catch_7
    .catch Ljava/lang/Throwable; {:try_start_27 .. :try_end_27} :catch_8

    .line 3248
    const-wide/16 v10, 0x0

    cmp-long v4, v36, v10

    if-eqz v4, :cond_46

    :try_start_28
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mContentLengthRangeRequest:J
    invoke-static {v4}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$2800(Lorg/apache/http/impl/client/MultiSocketInputStream;)J

    move-result-wide v10

    cmp-long v4, v36, v10

    if-eqz v4, :cond_46

    .line 3250
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    const/4 v9, 0x1

    # setter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mRangeRequestSuccess:I
    invoke-static {v4, v9}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$2302(Lorg/apache/http/impl/client/MultiSocketInputStream;I)I

    .line 3251
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockManager:Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;
    invoke-static {v4}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$800(Lorg/apache/http/impl/client/MultiSocketInputStream;)Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->notifyAll()V

    .line 3253
    :cond_46
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mRangeRequestSuccess:I
    invoke-static {v4}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$2300(Lorg/apache/http/impl/client/MultiSocketInputStream;)I

    move-result v4

    const/4 v9, 0x1

    if-ne v4, v9, :cond_47

    .line 3254
    invoke-virtual/range {p0 .. p0}, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->closeHTTP()V

    .line 3255
    monitor-exit v5

    goto/16 :goto_7

    .line 3262
    :catchall_a
    move-exception v4

    monitor-exit v5
    :try_end_28
    .catchall {:try_start_28 .. :try_end_28} :catchall_a

    :try_start_29
    throw v4
    :try_end_29
    .catch Lorg/apache/http/HttpException; {:try_start_29 .. :try_end_29} :catch_6
    .catch Ljava/lang/InterruptedException; {:try_start_29 .. :try_end_29} :catch_7
    .catch Ljava/lang/Throwable; {:try_start_29 .. :try_end_29} :catch_8

    .line 3256
    :cond_47
    :try_start_2a
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mRangeRequestSuccess:I
    invoke-static {v4}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$2300(Lorg/apache/http/impl/client/MultiSocketInputStream;)I

    move-result v4

    if-nez v4, :cond_4f

    .line 3257
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    const/4 v9, 0x2

    # setter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mRangeRequestSuccess:I
    invoke-static {v4, v9}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$2302(Lorg/apache/http/impl/client/MultiSocketInputStream;I)I

    .line 3262
    :cond_48
    :goto_15
    monitor-exit v5
    :try_end_2a
    .catchall {:try_start_2a .. :try_end_2a} :catchall_a

    .line 3266
    .end local v27    # "byteRange":Ljava/lang/String;
    .end local v36    # "contentLengthRangeRequest":J
    .end local v66    # "rangeRequestTotalContentLength":Lorg/apache/http/Header;
    :cond_49
    :try_start_2b
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    move-object/from16 v0, p0

    iput-wide v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->t2:J

    .line 3267
    move-object/from16 v0, p0

    iget-wide v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->t2:J

    move-object/from16 v0, p0

    iput-wide v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->chunkStartTime:J

    .line 3268
    move-object/from16 v0, p0

    iget-wide v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->t2:J

    move-object/from16 v0, p0

    iget-wide v10, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->t1:J

    sub-long v74, v4, v10

    .line 3269
    .local v74, "tmpH":J
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockManager:Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;
    invoke-static {v4}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$800(Lorg/apache/http/impl/client/MultiSocketInputStream;)Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;

    move-result-object v4

    iget-object v4, v4, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->headerTime:[J

    move-object/from16 v0, p0

    iget v5, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->sockID:I

    aput-wide v74, v4, v5

    .line 3278
    sget-boolean v4, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v4, :cond_4a

    .line 3279
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "start to read body for block["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget v5, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->currentBlockNumber:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "], bytes to read: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-wide/from16 v0, v28

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 3284
    :cond_4a
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mHttpRequest:[Lorg/apache/http/HttpRequest;
    invoke-static {v4}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$2400(Lorg/apache/http/impl/client/MultiSocketInputStream;)[Lorg/apache/http/HttpRequest;

    move-result-object v4

    move-object/from16 v0, p0

    iget v5, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->connInfID:I

    aget-object v4, v4, v5

    invoke-interface {v4}, Lorg/apache/http/HttpRequest;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v4

    const-string v5, "http.socket.timeout"

    sget v9, Lorg/apache/http/impl/client/MultiSocketInputStream;->WAIT_FOR_INTERFACE_TIME_OUT:I

    invoke-interface {v4, v5, v9}, Lorg/apache/http/params/HttpParams;->getIntParameter(Ljava/lang/String;I)I

    move-result v4

    int-to-long v0, v4

    move-wide/from16 v70, v0

    .line 3287
    .local v70, "socketTimeout":J
    const-wide/16 v4, 0x2

    div-long v4, v70, v4

    sget v9, Lorg/apache/http/impl/client/MultiSocketInputStream;->TIMEOUT_WRITE_COMMON_BUFFER:I

    int-to-long v10, v9

    invoke-static {v4, v5, v10, v11}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v70

    .line 3291
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->dbuf:Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;

    monitor-enter v5
    :try_end_2b
    .catch Lorg/apache/http/HttpException; {:try_start_2b .. :try_end_2b} :catch_6
    .catch Ljava/lang/InterruptedException; {:try_start_2b .. :try_end_2b} :catch_7
    .catch Ljava/lang/Throwable; {:try_start_2b .. :try_end_2b} :catch_8

    .line 3292
    :try_start_2c
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->dbuf:Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;

    invoke-virtual {v4}, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->startReceiveData()V

    .line 3293
    monitor-exit v5
    :try_end_2c
    .catchall {:try_start_2c .. :try_end_2c} :catchall_c

    .line 3294
    const/16 v57, 0x0

    .line 3299
    .local v57, "listId":I
    :try_start_2d
    sget v53, Lorg/apache/http/impl/client/MultiSocketInputStream;->INIT_BUFFERLEN:I

    .line 3301
    const/16 v44, 0x0

    .local v44, "dataRead":I
    move/from16 v58, v57

    .line 3302
    .end local v57    # "listId":I
    .local v58, "listId":I
    :goto_16
    cmp-long v4, v82, v28

    if-gez v4, :cond_58

    .line 3304
    const/16 v23, 0x0

    .line 3307
    .local v23, "bsize":I
    if-nez v31, :cond_50

    .line 3308
    move/from16 v0, v53

    int-to-long v4, v0

    sub-long v10, v28, v82

    invoke-static {v4, v5, v10, v11}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    long-to-int v0, v4

    move/from16 v23, v0

    .line 3313
    :goto_17
    sget-boolean v4, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v4, :cond_4b

    .line 3314
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "RangeRequest alloc memory for new block size: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v23

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 3317
    :cond_4b
    move/from16 v0, v23

    new-array v0, v0, [B

    move-object/from16 v24, v0

    .line 3318
    .local v24, "buf":[B
    const/16 v69, 0x0

    .line 3319
    .local v69, "tempbuf":[B
    sget v4, Lorg/apache/http/impl/client/MultiSocketInputStream;->MAX_BUFFERLEN:I

    shl-int/lit8 v5, v53, 0x1

    move-object/from16 v0, p0

    iget-object v9, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockSize:I
    invoke-static {v9}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$900(Lorg/apache/http/impl/client/MultiSocketInputStream;)I

    move-result v9

    invoke-static {v5, v9}, Ljava/lang/Math;->min(II)I

    move-result v5

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I
    :try_end_2d
    .catch Lorg/apache/http/HttpException; {:try_start_2d .. :try_end_2d} :catch_6
    .catch Ljava/lang/InterruptedException; {:try_start_2d .. :try_end_2d} :catch_7
    .catch Ljava/lang/Throwable; {:try_start_2d .. :try_end_2d} :catch_8

    move-result v53

    .line 3321
    const/16 v25, 0x0

    .line 3322
    const/16 v26, 0x0

    .line 3325
    :try_start_2e
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    iget-object v5, v4, Lorg/apache/http/impl/client/MultiSocketInputStream;->lockAppReading:Ljava/lang/Object;

    monitor-enter v5
    :try_end_2e
    .catch Ljava/lang/Throwable; {:try_start_2e .. :try_end_2e} :catch_a
    .catch Lorg/apache/http/HttpException; {:try_start_2e .. :try_end_2e} :catch_6
    .catch Ljava/lang/InterruptedException; {:try_start_2e .. :try_end_2e} :catch_7

    .line 3326
    :cond_4c
    :goto_18
    :try_start_2f
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    iget-boolean v4, v4, Lorg/apache/http/impl/client/MultiSocketInputStream;->bAppReading:Z

    if-nez v4, :cond_51

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    iget-boolean v4, v4, Lorg/apache/http/impl/client/MultiSocketInputStream;->mFinished:Z

    if-nez v4, :cond_51

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    iget-object v4, v4, Lorg/apache/http/impl/client/MultiSocketInputStream;->mMainRequest:Lorg/apache/http/HttpRequest;

    check-cast v4, Lorg/apache/http/client/methods/HttpUriRequest;

    invoke-interface {v4}, Lorg/apache/http/client/methods/HttpUriRequest;->isAborted()Z

    move-result v4

    if-nez v4, :cond_51

    .line 3327
    sget-boolean v4, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v4, :cond_4d

    const-string v4, "APP not reading, waiting"

    invoke-static {v4}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V
    :try_end_2f
    .catchall {:try_start_2f .. :try_end_2f} :catchall_b

    .line 3329
    :cond_4d
    :try_start_30
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    iget-object v4, v4, Lorg/apache/http/impl/client/MultiSocketInputStream;->lockAppReading:Ljava/lang/Object;

    const-wide/16 v10, 0x3e8

    invoke-virtual {v4, v10, v11}, Ljava/lang/Object;->wait(J)V
    :try_end_30
    .catch Ljava/lang/Throwable; {:try_start_30 .. :try_end_30} :catch_9
    .catchall {:try_start_30 .. :try_end_30} :catchall_b

    goto :goto_18

    .line 3331
    :catch_9
    move-exception v45

    .line 3332
    .restart local v45    # "e":Ljava/lang/Throwable;
    :try_start_31
    sget-boolean v4, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v4, :cond_4c

    invoke-static/range {v45 .. v45}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/Throwable;)V

    goto :goto_18

    .line 3335
    .end local v45    # "e":Ljava/lang/Throwable;
    :catchall_b
    move-exception v4

    monitor-exit v5
    :try_end_31
    .catchall {:try_start_31 .. :try_end_31} :catchall_b

    :try_start_32
    throw v4
    :try_end_32
    .catch Ljava/lang/Throwable; {:try_start_32 .. :try_end_32} :catch_a
    .catch Lorg/apache/http/HttpException; {:try_start_32 .. :try_end_32} :catch_6
    .catch Ljava/lang/InterruptedException; {:try_start_32 .. :try_end_32} :catch_7

    .line 3389
    :catch_a
    move-exception v51

    .line 3390
    .local v51, "ex":Ljava/lang/Throwable;
    :try_start_33
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->bHandover:Z

    if-eqz v4, :cond_5d

    .line 3391
    sget-boolean v4, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v4, :cond_4e

    .line 3392
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "This block shall be read by another socket, this socket is slow: block["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget v5, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->currentBlockNumber:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "], socket["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget v5, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->sockID:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 3399
    :cond_4e
    move/from16 v0, v25

    int-to-long v4, v0

    sub-long v82, v82, v4

    .line 3400
    new-instance v4, Ljava/lang/InterruptedException;

    invoke-direct {v4}, Ljava/lang/InterruptedException;-><init>()V

    throw v4
    :try_end_33
    .catch Lorg/apache/http/HttpException; {:try_start_33 .. :try_end_33} :catch_6
    .catch Ljava/lang/InterruptedException; {:try_start_33 .. :try_end_33} :catch_7
    .catch Ljava/lang/Throwable; {:try_start_33 .. :try_end_33} :catch_8

    .line 3258
    .end local v23    # "bsize":I
    .end local v24    # "buf":[B
    .end local v44    # "dataRead":I
    .end local v51    # "ex":Ljava/lang/Throwable;
    .end local v58    # "listId":I
    .end local v69    # "tempbuf":[B
    .end local v70    # "socketTimeout":J
    .end local v74    # "tmpH":J
    .restart local v27    # "byteRange":Ljava/lang/String;
    .restart local v36    # "contentLengthRangeRequest":J
    .restart local v66    # "rangeRequestTotalContentLength":Lorg/apache/http/Header;
    :cond_4f
    :try_start_34
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mRangeRequestSuccess:I
    invoke-static {v4}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$2300(Lorg/apache/http/impl/client/MultiSocketInputStream;)I

    move-result v4

    const/4 v9, 0x2

    if-ne v4, v9, :cond_48

    .line 3259
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    const/4 v9, 0x3

    # setter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mRangeRequestSuccess:I
    invoke-static {v4, v9}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$2302(Lorg/apache/http/impl/client/MultiSocketInputStream;I)I

    .line 3260
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockManager:Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;
    invoke-static {v4}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$800(Lorg/apache/http/impl/client/MultiSocketInputStream;)Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->notifyAll()V
    :try_end_34
    .catchall {:try_start_34 .. :try_end_34} :catchall_a

    goto/16 :goto_15

    .line 3293
    .end local v27    # "byteRange":Ljava/lang/String;
    .end local v36    # "contentLengthRangeRequest":J
    .end local v66    # "rangeRequestTotalContentLength":Lorg/apache/http/Header;
    .restart local v70    # "socketTimeout":J
    .restart local v74    # "tmpH":J
    :catchall_c
    move-exception v4

    :try_start_35
    monitor-exit v5
    :try_end_35
    .catchall {:try_start_35 .. :try_end_35} :catchall_c

    :try_start_36
    throw v4

    .line 3310
    .restart local v23    # "bsize":I
    .restart local v44    # "dataRead":I
    .restart local v58    # "listId":I
    :cond_50
    aget v4, v31, v30

    sub-int v4, v4, v44

    move/from16 v0, v53

    invoke-static {v0, v4}, Ljava/lang/Math;->min(II)I
    :try_end_36
    .catch Lorg/apache/http/HttpException; {:try_start_36 .. :try_end_36} :catch_6
    .catch Ljava/lang/InterruptedException; {:try_start_36 .. :try_end_36} :catch_7
    .catch Ljava/lang/Throwable; {:try_start_36 .. :try_end_36} :catch_8

    move-result v23

    goto/16 :goto_17

    .line 3335
    .restart local v24    # "buf":[B
    .restart local v69    # "tempbuf":[B
    :cond_51
    :try_start_37
    monitor-exit v5
    :try_end_37
    .catchall {:try_start_37 .. :try_end_37} :catchall_b

    .line 3337
    :try_start_38
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v64

    .line 3338
    .local v64, "prevTime":J
    move/from16 v0, v25

    int-to-long v0, v0

    move-wide/from16 v62, v0

    .line 3339
    .local v62, "prevOffset":J
    move-wide/from16 v20, v64

    .line 3342
    .local v20, "blockStrtTime":J
    :cond_52
    move-object/from16 v0, v24

    array-length v4, v0

    sub-int v4, v4, v25

    move-object/from16 v0, v54

    move-object/from16 v1, v24

    move/from16 v2, v25

    invoke-virtual {v0, v1, v2, v4}, Ljava/io/InputStream;->read([BII)I

    move-result v56

    if-lez v56, :cond_57

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    iget-object v4, v4, Lorg/apache/http/impl/client/MultiSocketInputStream;->mMainRequest:Lorg/apache/http/HttpRequest;

    check-cast v4, Lorg/apache/http/client/methods/HttpUriRequest;

    invoke-interface {v4}, Lorg/apache/http/client/methods/HttpUriRequest;->isAborted()Z

    move-result v4

    if-nez v4, :cond_57

    .line 3343
    move/from16 v0, v56

    int-to-long v4, v0

    add-long v82, v82, v4

    .line 3344
    add-int v25, v25, v56

    .line 3345
    add-int v44, v44, v56

    .line 3347
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockManager:Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;
    invoke-static {v4}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$800(Lorg/apache/http/impl/client/MultiSocketInputStream;)Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;

    move-result-object v4

    iget-object v4, v4, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->toBeReadLen:[J

    move-object/from16 v0, p0

    iget v5, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->sockID:I

    aget-wide v10, v4, v5

    move/from16 v0, v56

    int-to-long v12, v0

    sub-long/2addr v10, v12

    aput-wide v10, v4, v5

    .line 3348
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v42

    .line 3349
    .local v42, "curTime":J
    move/from16 v0, v25

    int-to-long v0, v0

    move-wide/from16 v40, v0

    .line 3350
    .local v40, "curOffset":J
    sub-long v48, v42, v64

    .line 3351
    .local v48, "diffTime":J
    sub-long v46, v40, v62

    .line 3352
    .local v46, "diffOffset":J
    move-wide/from16 v18, v42

    .line 3353
    .local v18, "blockEndTime":J
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockManager:Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;
    invoke-static {v4}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$800(Lorg/apache/http/impl/client/MultiSocketInputStream;)Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;

    move-result-object v4

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->socketSpeed:[J
    invoke-static {v4}, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->access$5800(Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;)[J

    move-result-object v4

    move-object/from16 v0, p0

    iget v5, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->sockID:I

    aget-wide v76, v4, v5

    .line 3354
    .local v76, "tmpSpeed":J
    sget v4, Lorg/apache/http/impl/client/MultiSocketInputStream;->MIN_DATA_READ_SPEED_CALC:I

    int-to-long v4, v4

    cmp-long v4, v82, v4

    if-lez v4, :cond_56

    sget v4, Lorg/apache/http/impl/client/MultiSocketInputStream;->MINSIZEFORSPEED:I

    int-to-long v4, v4

    cmp-long v4, v46, v4

    if-ltz v4, :cond_53

    sget-wide v4, Lorg/apache/http/impl/client/MultiSocketInputStream;->MINTIMEFORSPEED:J

    cmp-long v4, v48, v4

    if-gez v4, :cond_54

    :cond_53
    sget-wide v4, Lorg/apache/http/impl/client/MultiSocketInputStream;->MAXTIMEFORSPEED:J

    cmp-long v4, v48, v4

    if-ltz v4, :cond_56

    .line 3356
    :cond_54
    const-wide/16 v4, 0x0

    cmp-long v4, v76, v4

    if-nez v4, :cond_5a

    .line 3357
    sget-boolean v4, Lorg/apache/http/impl/client/MultiSocketInputStream;->SPEED_CALC:Z

    if-eqz v4, :cond_59

    .line 3358
    const-wide/16 v4, 0x8

    mul-long v4, v4, v82

    move-object/from16 v0, p0

    iget-wide v10, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->chunkStartTime:J

    sub-long v10, v42, v10

    div-long v76, v4, v10

    .line 3368
    :cond_55
    :goto_19
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockManager:Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;
    invoke-static {v4}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$800(Lorg/apache/http/impl/client/MultiSocketInputStream;)Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;

    move-result-object v4

    move-object/from16 v0, p0

    iget v5, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->sockID:I

    move-wide/from16 v0, v76

    invoke-virtual {v4, v5, v0, v1}, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->setSpeed(IJ)V

    .line 3369
    move-wide/from16 v64, v42

    .line 3370
    move-wide/from16 v62, v40

    .line 3371
    sget-boolean v4, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v4, :cond_56

    .line 3372
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "RangeRequest set speed: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-wide/from16 v0, v76

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 3376
    :cond_56
    sub-long v4, v18, v20

    cmp-long v4, v4, v70

    if-ltz v4, :cond_5c

    .line 3377
    move/from16 v0, v25

    new-array v0, v0, [B

    move-object/from16 v69, v0

    .line 3378
    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object/from16 v0, v24

    move-object/from16 v1, v69

    move/from16 v2, v25

    invoke-static {v0, v4, v1, v5, v2}, Ljava/lang/System;->arraycopy([BI[BII)V
    :try_end_38
    .catch Ljava/lang/Throwable; {:try_start_38 .. :try_end_38} :catch_a
    .catch Lorg/apache/http/HttpException; {:try_start_38 .. :try_end_38} :catch_6
    .catch Ljava/lang/InterruptedException; {:try_start_38 .. :try_end_38} :catch_7

    .line 3405
    .end local v18    # "blockEndTime":J
    .end local v40    # "curOffset":J
    .end local v42    # "curTime":J
    .end local v46    # "diffOffset":J
    .end local v48    # "diffTime":J
    .end local v76    # "tmpSpeed":J
    :cond_57
    :goto_1a
    :try_start_39
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    iget-object v4, v4, Lorg/apache/http/impl/client/MultiSocketInputStream;->mMainRequest:Lorg/apache/http/HttpRequest;

    check-cast v4, Lorg/apache/http/client/methods/HttpUriRequest;

    invoke-interface {v4}, Lorg/apache/http/client/methods/HttpUriRequest;->isAborted()Z

    move-result v4

    if-eqz v4, :cond_5e

    .line 3406
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    const/4 v5, 0x1

    iput-boolean v5, v4, Lorg/apache/http/impl/client/MultiSocketInputStream;->mFinished:Z

    .line 3550
    .end local v20    # "blockStrtTime":J
    .end local v23    # "bsize":I
    .end local v24    # "buf":[B
    .end local v62    # "prevOffset":J
    .end local v64    # "prevTime":J
    .end local v69    # "tempbuf":[B
    :cond_58
    sget-boolean v4, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v4, :cond_33

    .line 3551
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "finish to read body, bytes read: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-wide/from16 v0, v82

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V
    :try_end_39
    .catch Lorg/apache/http/HttpException; {:try_start_39 .. :try_end_39} :catch_6
    .catch Ljava/lang/InterruptedException; {:try_start_39 .. :try_end_39} :catch_7
    .catch Ljava/lang/Throwable; {:try_start_39 .. :try_end_39} :catch_8

    goto/16 :goto_12

    .line 3361
    .restart local v18    # "blockEndTime":J
    .restart local v20    # "blockStrtTime":J
    .restart local v23    # "bsize":I
    .restart local v24    # "buf":[B
    .restart local v40    # "curOffset":J
    .restart local v42    # "curTime":J
    .restart local v46    # "diffOffset":J
    .restart local v48    # "diffTime":J
    .restart local v62    # "prevOffset":J
    .restart local v64    # "prevTime":J
    .restart local v69    # "tempbuf":[B
    .restart local v76    # "tmpSpeed":J
    :cond_59
    const-wide/16 v4, 0x8

    mul-long v4, v4, v46

    :try_start_3a
    div-long v76, v4, v48

    goto/16 :goto_19

    .line 3363
    :cond_5a
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockSize:I
    invoke-static {v4}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$900(Lorg/apache/http/impl/client/MultiSocketInputStream;)I

    move-result v4

    div-int/lit8 v4, v4, 0x4

    sget v5, Lorg/apache/http/impl/client/MultiSocketInputStream;->MAX_DOWNLOADED_DATA_FOR_SPEED_CALC:I

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v4

    int-to-long v4, v4

    cmp-long v4, v82, v4

    if-gtz v4, :cond_5b

    move-object/from16 v0, p0

    iget-wide v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->chunkStartTime:J

    sub-long v4, v42, v4

    sget-wide v10, Lorg/apache/http/impl/client/MultiSocketInputStream;->MAXMAXTIMEFORSPEED:J

    cmp-long v4, v4, v10

    if-ltz v4, :cond_55

    .line 3365
    :cond_5b
    const-wide/16 v4, 0x8

    mul-long v4, v4, v82

    move-object/from16 v0, p0

    iget-wide v10, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->chunkStartTime:J

    sub-long v10, v42, v10

    div-long v76, v4, v10

    goto/16 :goto_19

    .line 3383
    :cond_5c
    move-object/from16 v0, v24

    array-length v4, v0
    :try_end_3a
    .catch Ljava/lang/Throwable; {:try_start_3a .. :try_end_3a} :catch_a
    .catch Lorg/apache/http/HttpException; {:try_start_3a .. :try_end_3a} :catch_6
    .catch Ljava/lang/InterruptedException; {:try_start_3a .. :try_end_3a} :catch_7

    move/from16 v0, v25

    if-ne v4, v0, :cond_52

    goto :goto_1a

    .line 3402
    .end local v18    # "blockEndTime":J
    .end local v20    # "blockStrtTime":J
    .end local v40    # "curOffset":J
    .end local v42    # "curTime":J
    .end local v46    # "diffOffset":J
    .end local v48    # "diffTime":J
    .end local v62    # "prevOffset":J
    .end local v64    # "prevTime":J
    .end local v76    # "tmpSpeed":J
    .restart local v51    # "ex":Ljava/lang/Throwable;
    :cond_5d
    :try_start_3b
    throw v51

    .line 3409
    .end local v51    # "ex":Ljava/lang/Throwable;
    .restart local v20    # "blockStrtTime":J
    .restart local v62    # "prevOffset":J
    .restart local v64    # "prevTime":J
    :cond_5e
    sget-boolean v4, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v4, :cond_5f

    .line 3410
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "RangeRequest get a buffer block, offset: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v25

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", buf.length: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v24

    array-length v5, v0

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", buf_offset:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v25

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 3418
    :cond_5f
    move-object/from16 v0, v24

    array-length v4, v0

    move/from16 v0, v25

    if-eq v0, v4, :cond_60

    if-eqz v69, :cond_6b

    .line 3422
    :cond_60
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->dbuf:Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;

    monitor-enter v5
    :try_end_3b
    .catch Lorg/apache/http/HttpException; {:try_start_3b .. :try_end_3b} :catch_6
    .catch Ljava/lang/InterruptedException; {:try_start_3b .. :try_end_3b} :catch_7
    .catch Ljava/lang/Throwable; {:try_start_3b .. :try_end_3b} :catch_8

    .line 3423
    if-eqz v69, :cond_62

    .line 3424
    :try_start_3c
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->dbuf:Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;

    move-object/from16 v0, p0

    iget v9, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->sockID:I

    move-object/from16 v0, v69

    array-length v10, v0

    move-object/from16 v0, v69

    invoke-virtual {v4, v9, v0, v10}, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->push(I[BI)J

    move-result-wide v10

    long-to-int v0, v10

    move/from16 v26, v0

    .line 3428
    :goto_1b
    if-gez v26, :cond_63

    .line 3429
    sget-boolean v4, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v4, :cond_61

    .line 3430
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "This block shall be read by another socket, this socket is slow: block["

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget v9, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->currentBlockNumber:I

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v9, "], socket["

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget v9, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->sockID:I

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v9, "]"

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 3437
    :cond_61
    move/from16 v0, v25

    int-to-long v10, v0

    sub-long v82, v82, v10

    .line 3438
    new-instance v4, Ljava/lang/InterruptedException;

    invoke-direct {v4}, Ljava/lang/InterruptedException;-><init>()V

    throw v4

    .line 3444
    :catchall_d
    move-exception v4

    monitor-exit v5
    :try_end_3c
    .catchall {:try_start_3c .. :try_end_3c} :catchall_d

    :try_start_3d
    throw v4
    :try_end_3d
    .catch Lorg/apache/http/HttpException; {:try_start_3d .. :try_end_3d} :catch_6
    .catch Ljava/lang/InterruptedException; {:try_start_3d .. :try_end_3d} :catch_7
    .catch Ljava/lang/Throwable; {:try_start_3d .. :try_end_3d} :catch_8

    .line 3426
    :cond_62
    :try_start_3e
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->dbuf:Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;

    move-object/from16 v0, p0

    iget v9, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->sockID:I

    move-object/from16 v0, v24

    array-length v10, v0

    move-object/from16 v0, v24

    invoke-virtual {v4, v9, v0, v10}, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->push(I[BI)J
    :try_end_3e
    .catchall {:try_start_3e .. :try_end_3e} :catchall_d

    move-result-wide v10

    long-to-int v0, v10

    move/from16 v26, v0

    goto :goto_1b

    .line 3441
    :cond_63
    :try_start_3f
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->dbuf:Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;

    invoke-virtual {v4}, Ljava/lang/Object;->notifyAll()V
    :try_end_3f
    .catch Ljava/lang/Throwable; {:try_start_3f .. :try_end_3f} :catch_c
    .catchall {:try_start_3f .. :try_end_3f} :catchall_d

    .line 3444
    :goto_1c
    :try_start_40
    monitor-exit v5
    :try_end_40
    .catchall {:try_start_40 .. :try_end_40} :catchall_d

    .line 3447
    :try_start_41
    sget-boolean v4, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v4, :cond_82

    .line 3448
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "RangeRequest response buffer["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget v5, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->currentBlockNumber:I

    add-int v5, v5, v30

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    add-int/lit8 v57, v58, 0x1

    .end local v58    # "listId":I
    .restart local v57    # "listId":I
    move/from16 v0, v58

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "] inserted: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    if-nez v69, :cond_69

    move-object/from16 v0, v24

    array-length v4, v0

    :goto_1d
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 3455
    :goto_1e
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mReadBlockNumber:Ljava/lang/Integer;
    invoke-static {v4}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$5600(Lorg/apache/http/impl/client/MultiSocketInputStream;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v39

    .line 3458
    .local v39, "currentReadingBlock":I
    if-eqz v31, :cond_6a

    .line 3460
    aget v4, v31, v30

    move/from16 v0, v44

    if-ne v0, v4, :cond_64

    add-int/lit8 v4, v80, -0x1

    move/from16 v0, v30

    if-ge v0, v4, :cond_64

    .line 3461
    add-int/lit8 v30, v30, 0x1

    .line 3462
    move-object/from16 v0, p0

    iget v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->currentBlockNumber:I

    add-int v8, v4, v30

    .line 3463
    .local v8, "blockNum":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockSize:I
    invoke-static {v4}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$900(Lorg/apache/http/impl/client/MultiSocketInputStream;)I

    move-result v4

    mul-int/2addr v4, v8

    int-to-long v4, v4

    move-object/from16 v0, p0

    iget-object v9, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mOffset:J
    invoke-static {v9}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1200(Lorg/apache/http/impl/client/MultiSocketInputStream;)J

    move-result-wide v10

    add-long v6, v4, v10

    .line 3465
    .local v6, "startIndex":J
    const/16 v44, 0x0

    .line 3466
    const/16 v57, 0x0

    .line 3470
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mInBuffer:Ljava/util/HashMap;
    invoke-static {v4}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1500(Lorg/apache/http/impl/client/MultiSocketInputStream;)Ljava/util/HashMap;

    move-result-object v13

    monitor-enter v13
    :try_end_41
    .catch Lorg/apache/http/HttpException; {:try_start_41 .. :try_end_41} :catch_6
    .catch Ljava/lang/InterruptedException; {:try_start_41 .. :try_end_41} :catch_7
    .catch Ljava/lang/Throwable; {:try_start_41 .. :try_end_41} :catch_8

    .line 3471
    :try_start_42
    new-instance v4, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;

    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    aget v9, v31, v30

    new-instance v10, Ljava/util/LinkedList;

    invoke-direct {v10}, Ljava/util/LinkedList;-><init>()V

    move-object/from16 v0, p0

    iget v11, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->sockID:I

    const/4 v12, 0x1

    invoke-direct/range {v4 .. v12}, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;-><init>(Lorg/apache/http/impl/client/MultiSocketInputStream;JIILjava/util/LinkedList;II)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->dbuf:Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;

    .line 3474
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mInBuffer:Ljava/util/HashMap;
    invoke-static {v4}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1500(Lorg/apache/http/impl/client/MultiSocketInputStream;)Ljava/util/HashMap;

    move-result-object v4

    new-instance v5, Ljava/lang/Integer;

    invoke-direct {v5, v8}, Ljava/lang/Integer;-><init>(I)V

    move-object/from16 v0, p0

    iget-object v9, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->dbuf:Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;

    invoke-virtual {v4, v5, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3475
    monitor-exit v13
    :try_end_42
    .catchall {:try_start_42 .. :try_end_42} :catchall_e

    .line 3476
    :try_start_43
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->dbuf:Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;

    monitor-enter v5
    :try_end_43
    .catch Lorg/apache/http/HttpException; {:try_start_43 .. :try_end_43} :catch_6
    .catch Ljava/lang/InterruptedException; {:try_start_43 .. :try_end_43} :catch_7
    .catch Ljava/lang/Throwable; {:try_start_43 .. :try_end_43} :catch_8

    .line 3477
    :try_start_44
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->dbuf:Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;

    invoke-virtual {v4}, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->startReceiveData()V

    .line 3478
    monitor-exit v5
    :try_end_44
    .catchall {:try_start_44 .. :try_end_44} :catchall_f

    .line 3481
    .end local v6    # "startIndex":J
    .end local v8    # "blockNum":I
    :cond_64
    :try_start_45
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v72

    .line 3487
    const/16 v38, 0x0

    .line 3489
    .local v38, "continiousChunkNotRead":Z
    if-lez v39, :cond_65

    .line 3490
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockStatus:[B
    invoke-static {v4}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1600(Lorg/apache/http/impl/client/MultiSocketInputStream;)[B

    move-result-object v4

    aget-byte v4, v4, v39

    const/4 v5, 0x3

    if-ne v4, v5, :cond_65

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockStatus:[B
    invoke-static {v4}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1600(Lorg/apache/http/impl/client/MultiSocketInputStream;)[B

    move-result-object v4

    add-int/lit8 v5, v39, 0x1

    aget-byte v4, v4, v5

    const/4 v5, 0x3

    if-ne v4, v5, :cond_65

    .line 3492
    const/16 v38, 0x1

    .line 3495
    :cond_65
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mInBuffer:Ljava/util/HashMap;
    invoke-static {v4}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1500(Lorg/apache/http/impl/client/MultiSocketInputStream;)Ljava/util/HashMap;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/HashMap;->size()I

    move-result v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mMaxBlockNumber:I
    invoke-static {v5}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$3700(Lorg/apache/http/impl/client/MultiSocketInputStream;)I

    move-result v5

    if-ge v4, v5, :cond_66

    if-eqz v38, :cond_6a

    .line 3496
    :cond_66
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mInBuffer:Ljava/util/HashMap;
    invoke-static {v4}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1500(Lorg/apache/http/impl/client/MultiSocketInputStream;)Ljava/util/HashMap;

    move-result-object v5

    monitor-enter v5
    :try_end_45
    .catch Lorg/apache/http/HttpException; {:try_start_45 .. :try_end_45} :catch_6
    .catch Ljava/lang/InterruptedException; {:try_start_45 .. :try_end_45} :catch_7
    .catch Ljava/lang/Throwable; {:try_start_45 .. :try_end_45} :catch_8

    .line 3498
    :try_start_46
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mInBuffer:Ljava/util/HashMap;
    invoke-static {v4}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1500(Lorg/apache/http/impl/client/MultiSocketInputStream;)Ljava/util/HashMap;

    move-result-object v4

    sget v9, Lorg/apache/http/impl/client/MultiSocketInputStream;->WAIT_TIME_TILL_READ_FROM_COMMON_BUFFER:I

    int-to-long v10, v9

    invoke-virtual {v4, v10, v11}, Ljava/lang/Object;->wait(J)V
    :try_end_46
    .catch Ljava/lang/Throwable; {:try_start_46 .. :try_end_46} :catch_b
    .catchall {:try_start_46 .. :try_end_46} :catchall_10

    .line 3503
    :cond_67
    :goto_1f
    :try_start_47
    monitor-exit v5
    :try_end_47
    .catchall {:try_start_47 .. :try_end_47} :catchall_10

    .line 3508
    :try_start_48
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long v4, v4, v72

    sget v9, Lorg/apache/http/impl/client/MultiSocketInputStream;->WAIT_FOR_INTERFACE_TIME_OUT:I

    int-to-long v10, v9

    cmp-long v4, v4, v10

    if-lez v4, :cond_65

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBytesRemaining:J
    invoke-static {v4}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$700(Lorg/apache/http/impl/client/MultiSocketInputStream;)J

    move-result-wide v4

    move-object/from16 v0, p0

    iget-object v9, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBytesForMultiSocket:J
    invoke-static {v9}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1100(Lorg/apache/http/impl/client/MultiSocketInputStream;)J

    move-result-wide v10

    cmp-long v4, v4, v10

    if-gtz v4, :cond_65

    .line 3510
    sget-boolean v4, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v4, :cond_68

    .line 3511
    const-string v4, "RangeRequest wait time out"

    invoke-static {v4}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 3513
    :cond_68
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    const/4 v5, 0x1

    iput-boolean v5, v4, Lorg/apache/http/impl/client/MultiSocketInputStream;->mFinished:Z

    .line 3514
    new-instance v4, Ljava/io/IOException;

    const-string v5, "Data is not read by the application"

    invoke-direct {v4, v5}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 3448
    .end local v38    # "continiousChunkNotRead":Z
    .end local v39    # "currentReadingBlock":I
    :cond_69
    move-object/from16 v0, v69

    array-length v4, v0
    :try_end_48
    .catch Lorg/apache/http/HttpException; {:try_start_48 .. :try_end_48} :catch_6
    .catch Ljava/lang/InterruptedException; {:try_start_48 .. :try_end_48} :catch_7
    .catch Ljava/lang/Throwable; {:try_start_48 .. :try_end_48} :catch_8

    goto/16 :goto_1d

    .line 3475
    .restart local v6    # "startIndex":J
    .restart local v8    # "blockNum":I
    .restart local v39    # "currentReadingBlock":I
    :catchall_e
    move-exception v4

    :try_start_49
    monitor-exit v13
    :try_end_49
    .catchall {:try_start_49 .. :try_end_49} :catchall_e

    :try_start_4a
    throw v4
    :try_end_4a
    .catch Lorg/apache/http/HttpException; {:try_start_4a .. :try_end_4a} :catch_6
    .catch Ljava/lang/InterruptedException; {:try_start_4a .. :try_end_4a} :catch_7
    .catch Ljava/lang/Throwable; {:try_start_4a .. :try_end_4a} :catch_8

    .line 3478
    :catchall_f
    move-exception v4

    :try_start_4b
    monitor-exit v5
    :try_end_4b
    .catchall {:try_start_4b .. :try_end_4b} :catchall_f

    :try_start_4c
    throw v4
    :try_end_4c
    .catch Lorg/apache/http/HttpException; {:try_start_4c .. :try_end_4c} :catch_6
    .catch Ljava/lang/InterruptedException; {:try_start_4c .. :try_end_4c} :catch_7
    .catch Ljava/lang/Throwable; {:try_start_4c .. :try_end_4c} :catch_8

    .line 3499
    .end local v6    # "startIndex":J
    .end local v8    # "blockNum":I
    .restart local v38    # "continiousChunkNotRead":Z
    :catch_b
    move-exception v45

    .line 3500
    .restart local v45    # "e":Ljava/lang/Throwable;
    :try_start_4d
    sget-boolean v4, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v4, :cond_67

    .line 3501
    invoke-static/range {v45 .. v45}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/Throwable;)V

    goto :goto_1f

    .line 3503
    .end local v45    # "e":Ljava/lang/Throwable;
    :catchall_10
    move-exception v4

    monitor-exit v5
    :try_end_4d
    .catchall {:try_start_4d .. :try_end_4d} :catchall_10

    :try_start_4e
    throw v4

    .line 3520
    .end local v38    # "continiousChunkNotRead":Z
    :cond_6a
    if-lez v39, :cond_6f

    .line 3521
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockStatus:[B
    invoke-static {v4}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1600(Lorg/apache/http/impl/client/MultiSocketInputStream;)[B

    move-result-object v4

    add-int/lit8 v5, v39, -0x1

    aget-byte v4, v4, v5

    const/4 v5, -0x1

    if-ne v4, v5, :cond_6f

    .line 3522
    new-instance v4, Ljava/io/IOException;

    const-string v5, "Forcefull exception to handover to the blocked Chunk which is read by application, so that application read should not stop"

    invoke-direct {v4, v5}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 3527
    .end local v39    # "currentReadingBlock":I
    .end local v57    # "listId":I
    .restart local v58    # "listId":I
    :cond_6b
    const/4 v4, -0x1

    move/from16 v0, v56

    if-ne v0, v4, :cond_6d

    cmp-long v4, v82, v28

    if-gez v4, :cond_6d

    .line 3528
    sget-boolean v4, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v4, :cond_6c

    .line 3529
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "RangeRequest read body Exception: totallen="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-wide/from16 v0, v82

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", bytesToRead= "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-wide/from16 v0, v28

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 3532
    :cond_6c
    new-instance v4, Ljava/io/IOException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "RangeRequest read body Exception: totallen="

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-wide/from16 v0, v82

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v9, ", bytesToRead= "

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-wide/from16 v0, v28

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 3535
    :cond_6d
    sget-boolean v4, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v4, :cond_6e

    .line 3536
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "RangeRequest read body Exception2: totallen="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-wide/from16 v0, v82

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", bytesToRead= "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-wide/from16 v0, v28

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 3539
    :cond_6e
    new-instance v4, Ljava/io/IOException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "RangeRequest read body Exception2: totallen="

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-wide/from16 v0, v82

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v9, ", bytesToRead= "

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-wide/from16 v0, v28

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_4e
    .catch Lorg/apache/http/HttpException; {:try_start_4e .. :try_end_4e} :catch_6
    .catch Ljava/lang/InterruptedException; {:try_start_4e .. :try_end_4e} :catch_7
    .catch Ljava/lang/Throwable; {:try_start_4e .. :try_end_4e} :catch_8

    .end local v58    # "listId":I
    .restart local v39    # "currentReadingBlock":I
    .restart local v57    # "listId":I
    :cond_6f
    move/from16 v58, v57

    .line 3542
    .end local v57    # "listId":I
    .restart local v58    # "listId":I
    goto/16 :goto_16

    .line 3591
    .end local v20    # "blockStrtTime":J
    .end local v23    # "bsize":I
    .end local v24    # "buf":[B
    .end local v33    # "connectionCloseHeader":Lorg/apache/http/Header;
    .end local v34    # "contentLength":J
    .end local v39    # "currentReadingBlock":I
    .end local v44    # "dataRead":I
    .end local v50    # "entity":Lorg/apache/http/HttpEntity;
    .end local v54    # "in":Ljava/io/InputStream;
    .end local v56    # "len":I
    .end local v58    # "listId":I
    .end local v62    # "prevOffset":J
    .end local v64    # "prevTime":J
    .end local v67    # "response":Lorg/apache/http/HttpResponse;
    .end local v68    # "rspCode":I
    .end local v69    # "tempbuf":[B
    .end local v70    # "socketTimeout":J
    .end local v74    # "tmpH":J
    .restart local v45    # "e":Ljava/lang/Throwable;
    :cond_70
    :try_start_4f
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockManager:Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;
    invoke-static {v4}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$800(Lorg/apache/http/impl/client/MultiSocketInputStream;)Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;

    move-result-object v4

    move-object/from16 v0, p0

    iget v9, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->currentBlockNumber:I

    move-object/from16 v0, p0

    iget v10, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->sockID:I

    invoke-virtual {v4, v9, v10}, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->block(II)V

    goto/16 :goto_14

    .line 3597
    :cond_71
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mRangeRequestSuccess:I
    invoke-static {v4}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$2300(Lorg/apache/http/impl/client/MultiSocketInputStream;)I

    move-result v4

    if-eqz v4, :cond_72

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mRangeRequestSuccess:I
    invoke-static {v4}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$2300(Lorg/apache/http/impl/client/MultiSocketInputStream;)I

    move-result v4

    const/4 v9, 0x2

    if-ne v4, v9, :cond_73

    .line 3598
    :cond_72
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    const/4 v9, 0x1

    # setter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mRangeRequestSuccess:I
    invoke-static {v4, v9}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$2302(Lorg/apache/http/impl/client/MultiSocketInputStream;I)I

    .line 3599
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockManager:Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;
    invoke-static {v4}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$800(Lorg/apache/http/impl/client/MultiSocketInputStream;)Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->notifyAll()V

    .line 3600
    invoke-virtual/range {p0 .. p0}, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->closeHTTP()V

    .line 3601
    monitor-exit v5

    goto/16 :goto_7

    .line 3603
    :cond_73
    monitor-exit v5
    :try_end_4f
    .catchall {:try_start_4f .. :try_end_4f} :catchall_8

    .line 3607
    :cond_74
    move-object/from16 v0, v45

    instance-of v4, v0, Ljava/lang/OutOfMemoryError;

    if-eqz v4, :cond_76

    .line 3608
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mInBuffer:Ljava/util/HashMap;
    invoke-static {v4}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1500(Lorg/apache/http/impl/client/MultiSocketInputStream;)Ljava/util/HashMap;

    move-result-object v5

    monitor-enter v5

    .line 3609
    :try_start_50
    invoke-virtual/range {p0 .. p0}, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->closeHTTP()V

    .line 3610
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mRequestHandlers:[Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;
    invoke-static {v4}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$4000(Lorg/apache/http/impl/client/MultiSocketInputStream;)[Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;

    move-result-object v4

    move-object/from16 v0, p0

    iget v9, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->sockID:I

    add-int/lit8 v9, v9, 0x1

    rem-int/lit8 v9, v9, 0x2

    aget-object v4, v4, v9

    invoke-virtual {v4}, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->closeHTTP()V

    .line 3611
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mReadBlockNumber:Ljava/lang/Integer;
    invoke-static {v4}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$5600(Lorg/apache/http/impl/client/MultiSocketInputStream;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v52

    .restart local v52    # "i":I
    :goto_20
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockStatus:[B
    invoke-static {v4}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1600(Lorg/apache/http/impl/client/MultiSocketInputStream;)[B

    move-result-object v4

    array-length v4, v4

    move/from16 v0, v52

    if-ge v0, v4, :cond_75

    .line 3612
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mInBuffer:Ljava/util/HashMap;
    invoke-static {v4}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1500(Lorg/apache/http/impl/client/MultiSocketInputStream;)Ljava/util/HashMap;

    move-result-object v4

    new-instance v9, Ljava/lang/Integer;

    move/from16 v0, v52

    invoke-direct {v9, v0}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v4, v9}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 3611
    add-int/lit8 v52, v52, 0x1

    goto :goto_20

    .line 3614
    :cond_75
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 3615
    monitor-exit v5

    goto/16 :goto_12

    .end local v52    # "i":I
    :catchall_11
    move-exception v4

    monitor-exit v5
    :try_end_50
    .catchall {:try_start_50 .. :try_end_50} :catchall_11

    throw v4

    .line 3619
    :cond_76
    sget-boolean v4, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v4, :cond_77

    .line 3620
    const-string v4, "IOException is thrown"

    invoke-static {v4}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 3621
    const-string v4, "in IOException, handle handover"

    invoke-static {v4}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 3623
    :cond_77
    if-gtz v26, :cond_78

    .line 3624
    move/from16 v0, v25

    int-to-long v4, v0

    sub-long v82, v82, v4

    .line 3625
    :cond_78
    cmp-long v4, v82, v28

    if-gez v4, :cond_7c

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    iget-boolean v4, v4, Lorg/apache/http/impl/client/MultiSocketInputStream;->mFinished:Z

    if-nez v4, :cond_7c

    .line 3626
    sget-boolean v4, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v4, :cond_79

    .line 3627
    const-string v4, "Moving to Block state"

    invoke-static {v4}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 3629
    :cond_79
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockManager:Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;
    invoke-static {v4}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$800(Lorg/apache/http/impl/client/MultiSocketInputStream;)Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;

    move-result-object v5

    monitor-enter v5

    .line 3633
    if-lez v80, :cond_7a

    .line 3634
    :try_start_51
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockManager:Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;
    invoke-static {v4}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$800(Lorg/apache/http/impl/client/MultiSocketInputStream;)Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;

    move-result-object v4

    move-object/from16 v0, p0

    iget v9, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->currentBlockNumber:I

    add-int v9, v9, v30

    move-object/from16 v0, p0

    iget v10, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->sockID:I

    invoke-virtual {v4, v9, v10}, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->block(II)V

    .line 3638
    :goto_21
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockManager:Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;
    invoke-static {v4}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$800(Lorg/apache/http/impl/client/MultiSocketInputStream;)Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->notifyAll()V

    .line 3639
    monitor-exit v5
    :try_end_51
    .catchall {:try_start_51 .. :try_end_51} :catchall_12

    .line 3642
    if-lez v80, :cond_7b

    .line 3643
    add-int/lit8 v52, v30, 0x1

    .restart local v52    # "i":I
    :goto_22
    move/from16 v0, v52

    move/from16 v1, v80

    if-ge v0, v1, :cond_7b

    .line 3644
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockStatus:[B
    invoke-static {v4}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1600(Lorg/apache/http/impl/client/MultiSocketInputStream;)[B

    move-result-object v4

    move-object/from16 v0, p0

    iget v5, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->currentBlockNumber:I

    add-int v5, v5, v52

    const/4 v9, 0x0

    aput-byte v9, v4, v5

    .line 3643
    add-int/lit8 v52, v52, 0x1

    goto :goto_22

    .line 3636
    .end local v52    # "i":I
    :cond_7a
    :try_start_52
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockManager:Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;
    invoke-static {v4}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$800(Lorg/apache/http/impl/client/MultiSocketInputStream;)Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;

    move-result-object v4

    move-object/from16 v0, p0

    iget v9, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->currentBlockNumber:I

    move-object/from16 v0, p0

    iget v10, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->sockID:I

    invoke-virtual {v4, v9, v10}, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->block(II)V

    goto :goto_21

    .line 3639
    :catchall_12
    move-exception v4

    monitor-exit v5
    :try_end_52
    .catchall {:try_start_52 .. :try_end_52} :catchall_12

    throw v4

    .line 3647
    :cond_7b
    invoke-virtual/range {p0 .. p0}, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->closeHTTP()V

    .line 3649
    :cond_7c
    sget-boolean v4, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v4, :cond_33

    .line 3650
    const-string v4, "Checked if Block state is moved to BLOCKED"

    invoke-static {v4}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    goto/16 :goto_12

    .line 3666
    .end local v17    # "blockInfo":[J
    .end local v25    # "buf_offset":I
    .end local v26    # "buf_ret":I
    .end local v28    # "bytesToRead":J
    .end local v30    # "chunkIndex":I
    .end local v31    # "chunkLengths":[I
    .end local v45    # "e":Ljava/lang/Throwable;
    .end local v61    # "otherSockID":I
    .end local v72    # "startChunkTime":J
    .end local v80    # "totalContinuousChunk":I
    .end local v82    # "totallen":J
    .restart local v16    # "bSubmitHere":Z
    :cond_7d
    const/16 v16, 0x0

    goto/16 :goto_8

    .line 3679
    :cond_7e
    :try_start_53
    monitor-exit v5
    :try_end_53
    .catchall {:try_start_53 .. :try_end_53} :catchall_13

    .line 3680
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mRequestHandlers:[Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;
    invoke-static {v4}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$4000(Lorg/apache/http/impl/client/MultiSocketInputStream;)[Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;

    move-result-object v4

    const/4 v5, 0x0

    aget-object v4, v4, v5

    iget-boolean v4, v4, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->bIOExceptionDuringContinueChunk:Z

    if-nez v4, :cond_80

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mRequestHandlers:[Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;
    invoke-static {v4}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$4000(Lorg/apache/http/impl/client/MultiSocketInputStream;)[Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;

    move-result-object v4

    const/4 v5, 0x1

    aget-object v4, v4, v5

    iget-boolean v4, v4, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->bIOExceptionDuringContinueChunk:Z

    if-nez v4, :cond_80

    if-eqz v16, :cond_80

    .line 3682
    sget-boolean v4, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v4, :cond_7f

    .line 3683
    const-string v4, "submit statistics data to ConnectivityService"

    invoke-static {v4}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 3685
    :cond_7f
    invoke-direct/range {p0 .. p0}, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->submitData()V

    .line 3688
    :cond_80
    invoke-virtual/range {p0 .. p0}, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->closeHTTP()V

    .line 3689
    sget-boolean v4, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v4, :cond_81

    .line 3690
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Total data downloaded : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-wide v10, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->totalElapsedTime:J

    invoke-virtual {v4, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " Total data downloaded "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-wide v10, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->totalReadSize:J

    invoke-virtual {v4, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 3693
    const-string v4, "download thread exit"

    invoke-static {v4}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 3696
    :cond_81
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    iget-object v5, v4, Lorg/apache/http/impl/client/MultiSocketInputStream;->mDefaultRequestDirectorInstance:Lorg/apache/http/impl/client/DefaultRequestDirector;

    monitor-enter v5

    .line 3697
    :try_start_54
    sget v4, Lorg/apache/http/impl/client/DefaultRequestDirector;->sProcessUsingMultiSocket:I

    add-int/lit8 v4, v4, -0x1

    sput v4, Lorg/apache/http/impl/client/DefaultRequestDirector;->sProcessUsingMultiSocket:I

    .line 3698
    monitor-exit v5
    :try_end_54
    .catchall {:try_start_54 .. :try_end_54} :catchall_14

    .line 3699
    return-void

    .line 3679
    :catchall_13
    move-exception v4

    :try_start_55
    monitor-exit v5
    :try_end_55
    .catchall {:try_start_55 .. :try_end_55} :catchall_13

    throw v4

    .line 3698
    :catchall_14
    move-exception v4

    :try_start_56
    monitor-exit v5
    :try_end_56
    .catchall {:try_start_56 .. :try_end_56} :catchall_14

    throw v4

    .line 3442
    .end local v16    # "bSubmitHere":Z
    .restart local v17    # "blockInfo":[J
    .restart local v20    # "blockStrtTime":J
    .restart local v23    # "bsize":I
    .restart local v24    # "buf":[B
    .restart local v25    # "buf_offset":I
    .restart local v26    # "buf_ret":I
    .restart local v28    # "bytesToRead":J
    .restart local v30    # "chunkIndex":I
    .restart local v31    # "chunkLengths":[I
    .restart local v33    # "connectionCloseHeader":Lorg/apache/http/Header;
    .restart local v34    # "contentLength":J
    .restart local v44    # "dataRead":I
    .restart local v50    # "entity":Lorg/apache/http/HttpEntity;
    .restart local v54    # "in":Ljava/io/InputStream;
    .restart local v56    # "len":I
    .restart local v58    # "listId":I
    .restart local v61    # "otherSockID":I
    .restart local v62    # "prevOffset":J
    .restart local v64    # "prevTime":J
    .restart local v67    # "response":Lorg/apache/http/HttpResponse;
    .restart local v68    # "rspCode":I
    .restart local v69    # "tempbuf":[B
    .restart local v70    # "socketTimeout":J
    .restart local v72    # "startChunkTime":J
    .restart local v74    # "tmpH":J
    .restart local v80    # "totalContinuousChunk":I
    .restart local v82    # "totallen":J
    :catch_c
    move-exception v4

    goto/16 :goto_1c

    :cond_82
    move/from16 v57, v58

    .end local v58    # "listId":I
    .restart local v57    # "listId":I
    goto/16 :goto_1e
.end method
