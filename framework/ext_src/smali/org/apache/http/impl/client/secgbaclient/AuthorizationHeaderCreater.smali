.class public Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeaderCreater;
.super Ljava/lang/Object;
.source "AuthorizationHeaderCreater.java"


# static fields
.field private static final Log:Lorg/apache/http/impl/client/secgbaclient/util/GbaLogger;

.field private static final TAG:Ljava/lang/String; = "AuthorizationHeaderCreater"


# instance fields
.field private algorithm:Ljava/lang/String;

.field private cnonce:Ljava/lang/String;

.field private entityBody:[B

.field private gbaServiceKey:Ljava/lang/String;

.field private httpMethod:Ljava/lang/String;

.field private nonce:Ljava/lang/String;

.field private nonceCount:Ljava/lang/String;

.field private opaque:Ljava/lang/String;

.field private qop:Ljava/lang/String;

.field private realm:Ljava/lang/String;

.field private uri:Ljava/lang/String;

.field private userName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 27
    new-instance v0, Lorg/apache/http/impl/client/secgbaclient/util/GbaLogger;

    const-string v1, "AuthorizationHeaderCreater"

    invoke-direct {v0, v1}, Lorg/apache/http/impl/client/secgbaclient/util/GbaLogger;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeaderCreater;->Log:Lorg/apache/http/impl/client/secgbaclient/util/GbaLogger;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object v0, p0, Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeaderCreater;->algorithm:Ljava/lang/String;

    .line 30
    iput-object v0, p0, Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeaderCreater;->realm:Ljava/lang/String;

    .line 31
    iput-object v0, p0, Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeaderCreater;->nonce:Ljava/lang/String;

    .line 32
    iput-object v0, p0, Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeaderCreater;->qop:Ljava/lang/String;

    .line 33
    iput-object v0, p0, Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeaderCreater;->opaque:Ljava/lang/String;

    .line 34
    iput-object v0, p0, Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeaderCreater;->cnonce:Ljava/lang/String;

    .line 35
    iput-object v0, p0, Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeaderCreater;->nonceCount:Ljava/lang/String;

    .line 36
    iput-object v0, p0, Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeaderCreater;->gbaServiceKey:Ljava/lang/String;

    .line 37
    iput-object v0, p0, Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeaderCreater;->uri:Ljava/lang/String;

    .line 38
    iput-object v0, p0, Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeaderCreater;->httpMethod:Ljava/lang/String;

    .line 39
    iput-object v0, p0, Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeaderCreater;->userName:Ljava/lang/String;

    .line 40
    iput-object v0, p0, Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeaderCreater;->entityBody:[B

    .line 48
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[BLjava/lang/String;)V
    .locals 1
    .param p1, "userName"    # Ljava/lang/String;
    .param p2, "algo"    # Ljava/lang/String;
    .param p3, "realm"    # Ljava/lang/String;
    .param p4, "nonce"    # Ljava/lang/String;
    .param p5, "qop"    # Ljava/lang/String;
    .param p6, "opaque"    # Ljava/lang/String;
    .param p7, "uri"    # Ljava/lang/String;
    .param p8, "gbaKey"    # Ljava/lang/String;
    .param p9, "entity"    # [B
    .param p10, "method"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object v0, p0, Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeaderCreater;->algorithm:Ljava/lang/String;

    .line 30
    iput-object v0, p0, Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeaderCreater;->realm:Ljava/lang/String;

    .line 31
    iput-object v0, p0, Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeaderCreater;->nonce:Ljava/lang/String;

    .line 32
    iput-object v0, p0, Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeaderCreater;->qop:Ljava/lang/String;

    .line 33
    iput-object v0, p0, Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeaderCreater;->opaque:Ljava/lang/String;

    .line 34
    iput-object v0, p0, Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeaderCreater;->cnonce:Ljava/lang/String;

    .line 35
    iput-object v0, p0, Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeaderCreater;->nonceCount:Ljava/lang/String;

    .line 36
    iput-object v0, p0, Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeaderCreater;->gbaServiceKey:Ljava/lang/String;

    .line 37
    iput-object v0, p0, Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeaderCreater;->uri:Ljava/lang/String;

    .line 38
    iput-object v0, p0, Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeaderCreater;->httpMethod:Ljava/lang/String;

    .line 39
    iput-object v0, p0, Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeaderCreater;->userName:Ljava/lang/String;

    .line 40
    iput-object v0, p0, Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeaderCreater;->entityBody:[B

    .line 56
    iput-object p1, p0, Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeaderCreater;->userName:Ljava/lang/String;

    .line 57
    iput-object p2, p0, Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeaderCreater;->algorithm:Ljava/lang/String;

    .line 58
    iput-object p3, p0, Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeaderCreater;->realm:Ljava/lang/String;

    .line 59
    iput-object p4, p0, Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeaderCreater;->nonce:Ljava/lang/String;

    .line 60
    iput-object p5, p0, Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeaderCreater;->qop:Ljava/lang/String;

    .line 61
    iput-object p6, p0, Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeaderCreater;->opaque:Ljava/lang/String;

    .line 62
    iput-object p7, p0, Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeaderCreater;->uri:Ljava/lang/String;

    .line 63
    iput-object p8, p0, Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeaderCreater;->gbaServiceKey:Ljava/lang/String;

    .line 64
    iput-object p9, p0, Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeaderCreater;->entityBody:[B

    .line 65
    iput-object p10, p0, Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeaderCreater;->httpMethod:Ljava/lang/String;

    .line 67
    return-void
.end method

.method private setAlgorithm(Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeader;)V
    .locals 4
    .param p1, "header"    # Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeader;

    .prologue
    .line 116
    const/4 v0, 0x0

    .line 117
    .local v0, "algo":Ljava/lang/String;
    iget-object v0, p0, Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeaderCreater;->algorithm:Ljava/lang/String;

    .line 118
    if-eqz v0, :cond_0

    .line 120
    invoke-virtual {p1, v0}, Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeader;->setAlgorithm(Ljava/lang/String;)V

    .line 126
    :goto_0
    return-void

    .line 124
    :cond_0
    sget-object v1, Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeaderCreater;->Log:Lorg/apache/http/impl/client/secgbaclient/util/GbaLogger;

    const-string v2, "AuthorizationHeaderCreater"

    const-string v3, "Algo is null"

    invoke-virtual {v1, v2, v3}, Lorg/apache/http/impl/client/secgbaclient/util/GbaLogger;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private setCnonce(Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeader;)V
    .locals 3
    .param p1, "header"    # Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeader;

    .prologue
    .line 186
    invoke-static {}, Lorg/apache/http/impl/client/secgbaclient/Cnonce;->getCnonce()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeaderCreater;->cnonce:Ljava/lang/String;

    .line 188
    iget-object v0, p0, Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeaderCreater;->cnonce:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 190
    iget-object v0, p0, Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeaderCreater;->cnonce:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeader;->setCnonce(Ljava/lang/String;)V

    .line 197
    :goto_0
    return-void

    .line 194
    :cond_0
    sget-object v0, Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeaderCreater;->Log:Lorg/apache/http/impl/client/secgbaclient/util/GbaLogger;

    const-string v1, "AuthorizationHeaderCreater"

    const-string v2, "cNonce is null"

    invoke-virtual {v0, v1, v2}, Lorg/apache/http/impl/client/secgbaclient/util/GbaLogger;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private setDigestResponse(Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeader;Ljava/lang/String;)V
    .locals 5
    .param p1, "header"    # Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeader;
    .param p2, "digestRes"    # Ljava/lang/String;

    .prologue
    .line 237
    const/4 v0, 0x0

    .line 238
    .local v0, "response":Ljava/lang/String;
    move-object v0, p2

    .line 240
    if-eqz v0, :cond_0

    .line 242
    sget-object v1, Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeaderCreater;->Log:Lorg/apache/http/impl/client/secgbaclient/util/GbaLogger;

    const-string v2, "AuthorizationHeaderCreater"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "GBA Service digest Response is: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lorg/apache/http/impl/client/secgbaclient/util/GbaLogger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 243
    invoke-virtual {p1, v0}, Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeader;->setResponse(Ljava/lang/String;)V

    .line 249
    :goto_0
    return-void

    .line 247
    :cond_0
    sget-object v1, Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeaderCreater;->Log:Lorg/apache/http/impl/client/secgbaclient/util/GbaLogger;

    const-string v2, "AuthorizationHeaderCreater"

    const-string v3, "Digest Response is null"

    invoke-virtual {v1, v2, v3}, Lorg/apache/http/impl/client/secgbaclient/util/GbaLogger;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private setNonce(Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeader;)V
    .locals 4
    .param p1, "header"    # Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeader;

    .prologue
    .line 144
    const/4 v0, 0x0

    .line 145
    .local v0, "nonce":Ljava/lang/String;
    iget-object v0, p0, Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeaderCreater;->nonce:Ljava/lang/String;

    .line 146
    if-eqz v0, :cond_0

    .line 148
    invoke-virtual {p1, v0}, Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeader;->setNonce(Ljava/lang/String;)V

    .line 154
    :goto_0
    return-void

    .line 152
    :cond_0
    sget-object v1, Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeaderCreater;->Log:Lorg/apache/http/impl/client/secgbaclient/util/GbaLogger;

    const-string v2, "AuthorizationHeaderCreater"

    const-string v3, "Nonce is null"

    invoke-virtual {v1, v2, v3}, Lorg/apache/http/impl/client/secgbaclient/util/GbaLogger;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private setNonceCount(Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeader;)V
    .locals 1
    .param p1, "header"    # Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeader;

    .prologue
    .line 201
    const-string v0, "00000001"

    iput-object v0, p0, Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeaderCreater;->nonceCount:Ljava/lang/String;

    .line 203
    iget-object v0, p0, Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeaderCreater;->nonceCount:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeader;->setNonceCount(Ljava/lang/String;)V

    .line 204
    return-void
.end method

.method private setOpaque(Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeader;)V
    .locals 4
    .param p1, "header"    # Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeader;

    .prologue
    .line 172
    const/4 v0, 0x0

    .line 173
    .local v0, "opaque":Ljava/lang/String;
    iget-object v0, p0, Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeaderCreater;->opaque:Ljava/lang/String;

    .line 174
    if-eqz v0, :cond_0

    .line 176
    invoke-virtual {p1, v0}, Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeader;->setOpaque(Ljava/lang/String;)V

    .line 182
    :goto_0
    return-void

    .line 180
    :cond_0
    sget-object v1, Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeaderCreater;->Log:Lorg/apache/http/impl/client/secgbaclient/util/GbaLogger;

    const-string v2, "AuthorizationHeaderCreater"

    const-string v3, "Opaque is null"

    invoke-virtual {v1, v2, v3}, Lorg/apache/http/impl/client/secgbaclient/util/GbaLogger;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private setQop(Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeader;)V
    .locals 4
    .param p1, "header"    # Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeader;

    .prologue
    .line 158
    const/4 v0, 0x0

    .line 159
    .local v0, "qop":Ljava/lang/String;
    iget-object v0, p0, Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeaderCreater;->qop:Ljava/lang/String;

    .line 160
    if-eqz v0, :cond_0

    .line 162
    invoke-virtual {p1, v0}, Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeader;->setQop(Ljava/lang/String;)V

    .line 168
    :goto_0
    return-void

    .line 166
    :cond_0
    sget-object v1, Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeaderCreater;->Log:Lorg/apache/http/impl/client/secgbaclient/util/GbaLogger;

    const-string v2, "AuthorizationHeaderCreater"

    const-string v3, "QOP is null"

    invoke-virtual {v1, v2, v3}, Lorg/apache/http/impl/client/secgbaclient/util/GbaLogger;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private setRealm(Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeader;)V
    .locals 4
    .param p1, "header"    # Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeader;

    .prologue
    .line 130
    const/4 v0, 0x0

    .line 131
    .local v0, "realm":Ljava/lang/String;
    iget-object v0, p0, Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeaderCreater;->realm:Ljava/lang/String;

    .line 132
    if-eqz v0, :cond_0

    .line 134
    invoke-virtual {p1, v0}, Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeader;->setRealm(Ljava/lang/String;)V

    .line 140
    :goto_0
    return-void

    .line 138
    :cond_0
    sget-object v1, Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeaderCreater;->Log:Lorg/apache/http/impl/client/secgbaclient/util/GbaLogger;

    const-string v2, "AuthorizationHeaderCreater"

    const-string v3, "realm is null"

    invoke-virtual {v1, v2, v3}, Lorg/apache/http/impl/client/secgbaclient/util/GbaLogger;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private setResponse(Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeader;)V
    .locals 5
    .param p1, "header"    # Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeader;

    .prologue
    .line 222
    const/4 v0, 0x0

    .line 223
    .local v0, "gbaKey":Ljava/lang/String;
    iget-object v0, p0, Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeaderCreater;->gbaServiceKey:Ljava/lang/String;

    .line 224
    if-eqz v0, :cond_0

    .line 226
    sget-object v1, Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeaderCreater;->Log:Lorg/apache/http/impl/client/secgbaclient/util/GbaLogger;

    const-string v2, "AuthorizationHeaderCreater"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "GBA Service Response is: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lorg/apache/http/impl/client/secgbaclient/util/GbaLogger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 227
    invoke-virtual {p1, v0}, Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeader;->setResponse(Ljava/lang/String;)V

    .line 233
    :goto_0
    return-void

    .line 231
    :cond_0
    sget-object v1, Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeaderCreater;->Log:Lorg/apache/http/impl/client/secgbaclient/util/GbaLogger;

    const-string v2, "AuthorizationHeaderCreater"

    const-string v3, "GBA Service Response is null"

    invoke-virtual {v1, v2, v3}, Lorg/apache/http/impl/client/secgbaclient/util/GbaLogger;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private setUri(Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeader;)V
    .locals 4
    .param p1, "header"    # Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeader;

    .prologue
    .line 208
    const/4 v0, 0x0

    .line 209
    .local v0, "uri":Ljava/lang/String;
    iget-object v0, p0, Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeaderCreater;->uri:Ljava/lang/String;

    .line 210
    if-eqz v0, :cond_0

    .line 212
    invoke-virtual {p1, v0}, Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeader;->setUri(Ljava/lang/String;)V

    .line 218
    :goto_0
    return-void

    .line 216
    :cond_0
    sget-object v1, Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeaderCreater;->Log:Lorg/apache/http/impl/client/secgbaclient/util/GbaLogger;

    const-string v2, "AuthorizationHeaderCreater"

    const-string v3, "URI is null"

    invoke-virtual {v1, v2, v3}, Lorg/apache/http/impl/client/secgbaclient/util/GbaLogger;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private setUserName(Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeader;)V
    .locals 3
    .param p1, "header"    # Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeader;

    .prologue
    .line 102
    iget-object v0, p0, Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeaderCreater;->userName:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 103
    invoke-static {}, Lorg/apache/http/impl/client/secgbaclient/GbaRequest;->getImpi()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeaderCreater;->userName:Ljava/lang/String;

    .line 104
    :cond_0
    iget-object v0, p0, Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeaderCreater;->userName:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 106
    iget-object v0, p0, Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeaderCreater;->userName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeader;->setUserName(Ljava/lang/String;)V

    .line 112
    :goto_0
    return-void

    .line 110
    :cond_1
    sget-object v0, Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeaderCreater;->Log:Lorg/apache/http/impl/client/secgbaclient/util/GbaLogger;

    const-string v1, "AuthorizationHeaderCreater"

    const-string v2, "userName is null"

    invoke-virtual {v0, v1, v2}, Lorg/apache/http/impl/client/secgbaclient/util/GbaLogger;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public createAuthorizationHeader()Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeader;
    .locals 7

    .prologue
    .line 71
    new-instance v0, Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeader;

    invoke-direct {v0}, Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeader;-><init>()V

    .line 72
    .local v0, "authorizationHeader":Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeader;
    const/4 v2, 0x0

    .line 73
    .local v2, "digestResponse":Ljava/lang/String;
    const/4 v1, 0x0

    .line 75
    .local v1, "digestCalculator":Lorg/apache/http/impl/client/secgbaclient/httpdigest/DigestCalculator;
    invoke-direct {p0, v0}, Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeaderCreater;->setUserName(Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeader;)V

    .line 76
    invoke-direct {p0, v0}, Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeaderCreater;->setAlgorithm(Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeader;)V

    .line 77
    invoke-direct {p0, v0}, Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeaderCreater;->setCnonce(Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeader;)V

    .line 78
    invoke-direct {p0, v0}, Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeaderCreater;->setNonce(Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeader;)V

    .line 79
    invoke-direct {p0, v0}, Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeaderCreater;->setNonceCount(Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeader;)V

    .line 80
    invoke-direct {p0, v0}, Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeaderCreater;->setOpaque(Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeader;)V

    .line 81
    invoke-direct {p0, v0}, Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeaderCreater;->setQop(Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeader;)V

    .line 82
    invoke-direct {p0, v0}, Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeaderCreater;->setRealm(Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeader;)V

    .line 83
    invoke-direct {p0, v0}, Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeaderCreater;->setResponse(Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeader;)V

    .line 84
    invoke-direct {p0, v0}, Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeaderCreater;->setUri(Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeader;)V

    .line 87
    new-instance v1, Lorg/apache/http/impl/client/secgbaclient/httpdigest/DigestCalculator;

    .end local v1    # "digestCalculator":Lorg/apache/http/impl/client/secgbaclient/httpdigest/DigestCalculator;
    iget-object v3, p0, Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeaderCreater;->httpMethod:Ljava/lang/String;

    invoke-virtual {v0}, Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeader;->getUri()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeaderCreater;->entityBody:[B

    invoke-direct {v1, v0, v3, v4, v5}, Lorg/apache/http/impl/client/secgbaclient/httpdigest/DigestCalculator;-><init>(Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeader;Ljava/lang/String;Ljava/lang/String;[B)V

    .line 92
    .restart local v1    # "digestCalculator":Lorg/apache/http/impl/client/secgbaclient/httpdigest/DigestCalculator;
    invoke-virtual {v1}, Lorg/apache/http/impl/client/secgbaclient/httpdigest/DigestCalculator;->calculateDigest()Ljava/lang/String;

    move-result-object v2

    .line 93
    sget-object v3, Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeaderCreater;->Log:Lorg/apache/http/impl/client/secgbaclient/util/GbaLogger;

    const-string v4, "AuthorizationHeaderCreater"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Digest Response is "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lorg/apache/http/impl/client/secgbaclient/util/GbaLogger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    invoke-direct {p0, v0, v2}, Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeaderCreater;->setDigestResponse(Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeader;Ljava/lang/String;)V

    .line 96
    return-object v0
.end method

.method public getAlgorithm()Ljava/lang/String;
    .locals 1

    .prologue
    .line 256
    iget-object v0, p0, Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeaderCreater;->algorithm:Ljava/lang/String;

    return-object v0
.end method

.method public getCnonce()Ljava/lang/String;
    .locals 1

    .prologue
    .line 326
    iget-object v0, p0, Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeaderCreater;->cnonce:Ljava/lang/String;

    return-object v0
.end method

.method public getEntityBody()[B
    .locals 1

    .prologue
    .line 410
    iget-object v0, p0, Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeaderCreater;->entityBody:[B

    return-object v0
.end method

.method public getGbaServiceKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 354
    iget-object v0, p0, Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeaderCreater;->gbaServiceKey:Ljava/lang/String;

    return-object v0
.end method

.method public getHttpMethod()Ljava/lang/String;
    .locals 1

    .prologue
    .line 382
    iget-object v0, p0, Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeaderCreater;->httpMethod:Ljava/lang/String;

    return-object v0
.end method

.method public getNonce()Ljava/lang/String;
    .locals 1

    .prologue
    .line 284
    iget-object v0, p0, Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeaderCreater;->nonce:Ljava/lang/String;

    return-object v0
.end method

.method public getNonceCount()Ljava/lang/String;
    .locals 1

    .prologue
    .line 340
    iget-object v0, p0, Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeaderCreater;->nonceCount:Ljava/lang/String;

    return-object v0
.end method

.method public getOpaque()Ljava/lang/String;
    .locals 1

    .prologue
    .line 312
    iget-object v0, p0, Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeaderCreater;->opaque:Ljava/lang/String;

    return-object v0
.end method

.method public getQop()Ljava/lang/String;
    .locals 1

    .prologue
    .line 298
    iget-object v0, p0, Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeaderCreater;->qop:Ljava/lang/String;

    return-object v0
.end method

.method public getRealm()Ljava/lang/String;
    .locals 1

    .prologue
    .line 270
    iget-object v0, p0, Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeaderCreater;->realm:Ljava/lang/String;

    return-object v0
.end method

.method public getUri()Ljava/lang/String;
    .locals 1

    .prologue
    .line 368
    iget-object v0, p0, Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeaderCreater;->uri:Ljava/lang/String;

    return-object v0
.end method

.method public getUserName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 396
    iget-object v0, p0, Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeaderCreater;->userName:Ljava/lang/String;

    return-object v0
.end method

.method public setAlgorithm(Ljava/lang/String;)V
    .locals 0
    .param p1, "algorithm"    # Ljava/lang/String;

    .prologue
    .line 263
    iput-object p1, p0, Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeaderCreater;->algorithm:Ljava/lang/String;

    .line 264
    return-void
.end method

.method public setCnonce(Ljava/lang/String;)V
    .locals 0
    .param p1, "cnonce"    # Ljava/lang/String;

    .prologue
    .line 333
    iput-object p1, p0, Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeaderCreater;->cnonce:Ljava/lang/String;

    .line 334
    return-void
.end method

.method public setEntityBody([B)V
    .locals 0
    .param p1, "entityBody"    # [B

    .prologue
    .line 417
    iput-object p1, p0, Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeaderCreater;->entityBody:[B

    .line 418
    return-void
.end method

.method public setGbaServiceKey(Ljava/lang/String;)V
    .locals 0
    .param p1, "gbaServiceKey"    # Ljava/lang/String;

    .prologue
    .line 361
    iput-object p1, p0, Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeaderCreater;->gbaServiceKey:Ljava/lang/String;

    .line 362
    return-void
.end method

.method public setHttpMethod(Ljava/lang/String;)V
    .locals 0
    .param p1, "httpMethod"    # Ljava/lang/String;

    .prologue
    .line 389
    iput-object p1, p0, Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeaderCreater;->httpMethod:Ljava/lang/String;

    .line 390
    return-void
.end method

.method public setNonce(Ljava/lang/String;)V
    .locals 0
    .param p1, "nonce"    # Ljava/lang/String;

    .prologue
    .line 291
    iput-object p1, p0, Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeaderCreater;->nonce:Ljava/lang/String;

    .line 292
    return-void
.end method

.method public setNonceCount(Ljava/lang/String;)V
    .locals 0
    .param p1, "nonceCount"    # Ljava/lang/String;

    .prologue
    .line 347
    iput-object p1, p0, Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeaderCreater;->nonceCount:Ljava/lang/String;

    .line 348
    return-void
.end method

.method public setOpaque(Ljava/lang/String;)V
    .locals 0
    .param p1, "opaque"    # Ljava/lang/String;

    .prologue
    .line 319
    iput-object p1, p0, Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeaderCreater;->opaque:Ljava/lang/String;

    .line 320
    return-void
.end method

.method public setQop(Ljava/lang/String;)V
    .locals 0
    .param p1, "qop"    # Ljava/lang/String;

    .prologue
    .line 305
    iput-object p1, p0, Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeaderCreater;->qop:Ljava/lang/String;

    .line 306
    return-void
.end method

.method public setRealm(Ljava/lang/String;)V
    .locals 0
    .param p1, "realm"    # Ljava/lang/String;

    .prologue
    .line 277
    iput-object p1, p0, Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeaderCreater;->realm:Ljava/lang/String;

    .line 278
    return-void
.end method

.method public setUri(Ljava/lang/String;)V
    .locals 0
    .param p1, "uri"    # Ljava/lang/String;

    .prologue
    .line 375
    iput-object p1, p0, Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeaderCreater;->uri:Ljava/lang/String;

    .line 376
    return-void
.end method

.method public setUserName(Ljava/lang/String;)V
    .locals 0
    .param p1, "userName"    # Ljava/lang/String;

    .prologue
    .line 403
    iput-object p1, p0, Lorg/apache/http/impl/client/secgbaclient/AuthorizationHeaderCreater;->userName:Ljava/lang/String;

    .line 404
    return-void
.end method
