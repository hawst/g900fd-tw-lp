.class public final Lorg/apache/http/impl/client/TwoChunkInputStream;
.super Ljava/io/InputStream;
.source "TwoChunkInputStream.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;,
        Lorg/apache/http/impl/client/TwoChunkInputStream$SecondChunkThread;
    }
.end annotation


# static fields
.field private static final CACHE_READING_SPEED:I = 0x1e

.field private static final MAXDEPTH:I = 0xa

.field private static final MAX_TIMEFOR_REUSE:J = 0x1388L

.field private static final MIN_CACHED_DATA_TO_CONSIDER:J = 0xa00000L

.field private static final MIN_DIV_SIZE:I = 0x100000


# instance fields
.field private final MAX_TIMEFORALL_BY0:J

.field private final MAX_TIMEFORALL_BY1:J

.field private final MAX_TIMEFORTAIL_BY0:J

.field private bFailedInSecChunk:Z

.field private bSecThreadExisted:Z

.field private bSecThreadStarted:Z

.field private bSwitchOnFirstRead:Z

.field private childIS:Lorg/apache/http/impl/client/TwoChunkInputStream;

.field private closed:Z

.field private mBytesForTail:J

.field private mContext:Lorg/apache/http/protocol/HttpContext;

.field private mDBuf:Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;

.field private mDataDownloaded:[J

.field private final mDepth:I

.field private final mEnd:J

.field private final mFullConSize:J

.field private mHost:[Lorg/apache/http/HttpHost;

.field private mKeepSecondChunk:I

.field private final mLength:J

.field private mManConn:Lorg/apache/http/conn/ManagedClientConnection;

.field private mPrevSpeed:[J

.field private mRemainBytes:J

.field private mRequest:[Lorg/apache/http/HttpRequest;

.field private mSecThread:Lorg/apache/http/impl/client/TwoChunkInputStream$SecondChunkThread;

.field private mSocket0:I

.field private mSocket1:I

.field private final mStart:J

.field private mStartReadTime:[J

.field private mTimeForDownload:[J

.field private final mTimeOut:I

.field private mainInput:Ljava/io/InputStream;

.field private final parentIS:Lorg/apache/http/impl/client/MultiSocketInputStream;


# direct methods
.method constructor <init>(Ljava/io/InputStream;Lorg/apache/http/conn/ManagedClientConnection;IIJJ[Lorg/apache/http/HttpRequest;[Lorg/apache/http/HttpHost;Lorg/apache/http/protocol/HttpContext;JJILorg/apache/http/impl/client/MultiSocketInputStream;JIZJ)V
    .locals 34
    .param p1, "in"    # Ljava/io/InputStream;
    .param p2, "manConn"    # Lorg/apache/http/conn/ManagedClientConnection;
    .param p3, "sockID0"    # I
    .param p4, "sockID1"    # I
    .param p5, "start"    # J
    .param p7, "end"    # J
    .param p9, "oriReq"    # [Lorg/apache/http/HttpRequest;
    .param p10, "host"    # [Lorg/apache/http/HttpHost;
    .param p11, "context"    # Lorg/apache/http/protocol/HttpContext;
    .param p12, "speed0"    # J
    .param p14, "speed1"    # J
    .param p16, "depth"    # I
    .param p17, "multiInput"    # Lorg/apache/http/impl/client/MultiSocketInputStream;
    .param p18, "fullSize"    # J
    .param p20, "timeout"    # I
    .param p21, "bswitch"    # Z
    .param p22, "cachedData"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 159
    invoke-direct/range {p0 .. p0}, Ljava/io/InputStream;-><init>()V

    .line 126
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->bFailedInSecChunk:Z

    .line 139
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput v4, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mKeepSecondChunk:I

    .line 160
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/apache/http/impl/client/TwoChunkInputStream;->mainInput:Ljava/io/InputStream;

    .line 161
    sget-boolean v4, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v4, :cond_0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "new input stream in twochunk creator sec mainInput = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mainInput:Ljava/io/InputStream;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lorg/apache/http/impl/client/TwoChunkInputStream;->log(Ljava/lang/String;)V

    .line 162
    :cond_0
    move-wide/from16 v0, p5

    move-object/from16 v2, p0

    iput-wide v0, v2, Lorg/apache/http/impl/client/TwoChunkInputStream;->mStart:J

    .line 163
    move-wide/from16 v0, p7

    move-object/from16 v2, p0

    iput-wide v0, v2, Lorg/apache/http/impl/client/TwoChunkInputStream;->mEnd:J

    .line 164
    sub-long v4, p7, p5

    const-wide/16 v6, 0x1

    add-long/2addr v4, v6

    move-object/from16 v0, p0

    iput-wide v4, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mLength:J

    .line 165
    move-object/from16 v0, p0

    iget-wide v4, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mLength:J

    move-object/from16 v0, p0

    iput-wide v4, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mRemainBytes:J

    .line 166
    move/from16 v0, p3

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/http/impl/client/TwoChunkInputStream;->mSocket0:I

    .line 167
    move/from16 v0, p4

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/http/impl/client/TwoChunkInputStream;->mSocket1:I

    .line 168
    move/from16 v0, p16

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/http/impl/client/TwoChunkInputStream;->mDepth:I

    .line 169
    move-object/from16 v0, p17

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/apache/http/impl/client/TwoChunkInputStream;->parentIS:Lorg/apache/http/impl/client/MultiSocketInputStream;

    .line 170
    move-wide/from16 v0, p18

    move-object/from16 v2, p0

    iput-wide v0, v2, Lorg/apache/http/impl/client/TwoChunkInputStream;->mFullConSize:J

    .line 171
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->childIS:Lorg/apache/http/impl/client/TwoChunkInputStream;

    .line 172
    move/from16 v0, p20

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/http/impl/client/TwoChunkInputStream;->mTimeOut:I

    .line 173
    move-object/from16 v0, p2

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/apache/http/impl/client/TwoChunkInputStream;->mManConn:Lorg/apache/http/conn/ManagedClientConnection;

    .line 174
    move-object/from16 v0, p11

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/apache/http/impl/client/TwoChunkInputStream;->mContext:Lorg/apache/http/protocol/HttpContext;

    .line 176
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->parentIS:Lorg/apache/http/impl/client/MultiSocketInputStream;

    iget-boolean v4, v4, Lorg/apache/http/impl/client/MultiSocketInputStream;->bISHTTPS:Z

    if-eqz v4, :cond_1

    .line 177
    const-wide/16 v4, 0xfa0

    move-object/from16 v0, p0

    iput-wide v4, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->MAX_TIMEFORTAIL_BY0:J

    .line 178
    const-wide/16 v4, 0x2710

    move-object/from16 v0, p0

    iput-wide v4, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->MAX_TIMEFORALL_BY0:J

    .line 179
    const-wide/16 v4, 0x1f40

    move-object/from16 v0, p0

    iput-wide v4, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->MAX_TIMEFORALL_BY1:J

    .line 187
    :goto_0
    const/4 v4, 0x2

    new-array v4, v4, [J

    move-object/from16 v0, p0

    iput-object v4, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mDataDownloaded:[J

    .line 188
    const/4 v4, 0x2

    new-array v4, v4, [J

    move-object/from16 v0, p0

    iput-object v4, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mTimeForDownload:[J

    .line 189
    const/4 v4, 0x2

    new-array v4, v4, [J

    move-object/from16 v0, p0

    iput-object v4, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mStartReadTime:[J

    .line 190
    const/4 v4, 0x2

    new-array v4, v4, [J

    move-object/from16 v0, p0

    iput-object v4, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mPrevSpeed:[J

    .line 191
    const/4 v4, 0x2

    new-array v4, v4, [Lorg/apache/http/HttpHost;

    move-object/from16 v0, p0

    iput-object v4, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mHost:[Lorg/apache/http/HttpHost;

    .line 192
    const/4 v4, 0x2

    new-array v4, v4, [Lorg/apache/http/HttpRequest;

    move-object/from16 v0, p0

    iput-object v4, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mRequest:[Lorg/apache/http/HttpRequest;

    .line 193
    const/16 v19, 0x0

    .local v19, "i":I
    :goto_1
    const/4 v4, 0x2

    move/from16 v0, v19

    if-ge v0, v4, :cond_2

    .line 194
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mDataDownloaded:[J

    const-wide/16 v6, 0x0

    aput-wide v6, v4, v19

    .line 195
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mTimeForDownload:[J

    const-wide/16 v6, 0x0

    aput-wide v6, v4, v19

    .line 196
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mStartReadTime:[J

    const-wide/16 v6, 0x0

    aput-wide v6, v4, v19

    .line 197
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mHost:[Lorg/apache/http/HttpHost;

    aget-object v5, p10, v19

    aput-object v5, v4, v19

    .line 198
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mRequest:[Lorg/apache/http/HttpRequest;

    aget-object v5, p9, v19

    aput-object v5, v4, v19

    .line 193
    add-int/lit8 v19, v19, 0x1

    goto :goto_1

    .line 182
    .end local v19    # "i":I
    :cond_1
    const-wide/16 v4, 0x7d0

    move-object/from16 v0, p0

    iput-wide v4, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->MAX_TIMEFORTAIL_BY0:J

    .line 183
    const-wide/16 v4, 0x1388

    move-object/from16 v0, p0

    iput-wide v4, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->MAX_TIMEFORALL_BY0:J

    .line 184
    const-wide/16 v4, 0xfa0

    move-object/from16 v0, p0

    iput-wide v4, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->MAX_TIMEFORALL_BY1:J

    goto :goto_0

    .line 200
    .restart local v19    # "i":I
    :cond_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mPrevSpeed:[J

    aput-wide p12, v4, p3

    .line 201
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mPrevSpeed:[J

    aput-wide p14, v4, p4

    .line 202
    move/from16 v0, p21

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lorg/apache/http/impl/client/TwoChunkInputStream;->bSwitchOnFirstRead:Z

    .line 203
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->closed:Z

    .line 205
    const-wide/high16 v24, 0x4059000000000000L    # 100.0

    .line 206
    .local v24, "ratio":D
    const-wide/16 v4, 0x0

    cmp-long v4, p14, v4

    if-eqz v4, :cond_3

    .line 207
    const-wide/high16 v6, 0x4059000000000000L    # 100.0

    const-wide v10, 0x3f847ae147ae147bL    # 0.01

    move-wide/from16 v0, p12

    long-to-double v14, v0

    move-wide/from16 v0, p14

    long-to-double v0, v0

    move-wide/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->parentIS:Lorg/apache/http/impl/client/MultiSocketInputStream;

    iget-boolean v4, v4, Lorg/apache/http/impl/client/MultiSocketInputStream;->bISHTTPS:Z

    if-eqz v4, :cond_a

    const/4 v4, 0x2

    :goto_2
    int-to-double v4, v4

    div-double v4, v16, v4

    div-double v4, v14, v4

    invoke-static {v10, v11, v4, v5}, Ljava/lang/Math;->max(DD)D

    move-result-wide v4

    invoke-static {v6, v7, v4, v5}, Ljava/lang/Math;->min(DD)D

    move-result-wide v24

    .line 209
    :cond_3
    move-wide/from16 v20, p22

    .line 210
    .local v20, "cd":J
    const-wide/32 v4, 0xa00000

    cmp-long v4, v20, v4

    if-gtz v4, :cond_4

    const-wide/16 v20, 0x0

    .line 211
    :cond_4
    const-wide/16 v4, 0x400

    div-long v4, v20, v4

    const-wide/16 v6, 0x400

    div-long/2addr v4, v6

    const-wide/16 v6, 0x3e8

    mul-long/2addr v4, v6

    long-to-double v4, v4

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->parentIS:Lorg/apache/http/impl/client/MultiSocketInputStream;

    iget-boolean v6, v6, Lorg/apache/http/impl/client/MultiSocketInputStream;->bISHTTPS:Z

    move-object/from16 v0, p0

    move-wide/from16 v1, p18

    invoke-direct {v0, v1, v2, v6}, Lorg/apache/http/impl/client/TwoChunkInputStream;->getCacheReadSpeed(JZ)D

    move-result-wide v6

    div-double/2addr v4, v6

    double-to-long v0, v4

    move-wide/from16 v30, v0

    .line 212
    .local v30, "timeForCachedData":J
    move-object/from16 v0, p0

    iget-wide v4, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mLength:J

    const-wide/16 v6, 0x8

    div-long v6, p12, v6

    mul-long v6, v6, v30

    add-long/2addr v4, v6

    long-to-double v4, v4

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    add-double v6, v6, v24

    sget-wide v10, Lorg/apache/http/impl/client/MultiSocketInputStream;->B_Ratio_BUF_LTE:D

    div-double v10, v24, v10

    sub-double/2addr v6, v10

    div-double/2addr v4, v6

    double-to-long v8, v4

    .line 213
    .local v8, "lenTail":J
    move-object/from16 v0, p0

    iget-wide v4, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mLength:J

    cmp-long v4, v8, v4

    if-lez v4, :cond_5

    move-object/from16 v0, p0

    iget-wide v8, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mLength:J

    .line 215
    :cond_5
    const-wide/16 v4, 0x0

    cmp-long v4, p12, v4

    if-nez v4, :cond_b

    const-wide v32, 0x7fffffffffffffffL

    .line 216
    .local v32, "timeForTailBy0":J
    :goto_3
    const-wide/16 v4, 0x0

    cmp-long v4, p12, v4

    if-nez v4, :cond_c

    const-wide v26, 0x7fffffffffffffffL

    .line 217
    .local v26, "timeForAllBy0":J
    :goto_4
    const-wide/16 v4, 0x0

    cmp-long v4, p14, v4

    if-nez v4, :cond_d

    const-wide v28, 0x7fffffffffffffffL

    .line 218
    .local v28, "timeForAllBy1":J
    :goto_5
    sget-boolean v4, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v4, :cond_6

    .line 219
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "start twochunkInputStream["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, p3

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, p4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "] : range="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-wide/from16 v0, p5

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "-"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-wide/from16 v0, p7

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", speed="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-wide/from16 v0, p12

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ":"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-wide/from16 v0, p14

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", depth="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, p16

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", tail="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", timeForTailBy0="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-wide/from16 v0, v32

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", timeForAllBy0="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-wide/from16 v0, v26

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", timeForAllBy1="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-wide/from16 v0, v28

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", cachedData="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-wide/from16 v0, p22

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", timeForCachedData="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-wide/from16 v0, v30

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", SocketTimeoutException Count: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->parentIS:Lorg/apache/http/impl/client/MultiSocketInputStream;

    iget-object v5, v5, Lorg/apache/http/impl/client/MultiSocketInputStream;->disconnCount:[I

    aget v5, v5, p3

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->parentIS:Lorg/apache/http/impl/client/MultiSocketInputStream;

    iget-object v5, v5, Lorg/apache/http/impl/client/MultiSocketInputStream;->disconnCount:[I

    aget v5, v5, p4

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lorg/apache/http/impl/client/TwoChunkInputStream;->log(Ljava/lang/String;)V

    .line 226
    :cond_6
    move-object/from16 v0, p0

    iget-wide v4, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mLength:J

    const-wide/32 v6, 0x100000

    cmp-long v4, v4, v6

    if-gtz v4, :cond_e

    .line 227
    const-wide/16 v4, 0x0

    move-object/from16 v0, p0

    iput-wide v4, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mBytesForTail:J

    .line 228
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mSecThread:Lorg/apache/http/impl/client/TwoChunkInputStream$SecondChunkThread;

    .line 229
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mDBuf:Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;

    .line 230
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->bSecThreadStarted:Z

    .line 231
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->bSecThreadExisted:Z

    .line 232
    const/4 v4, 0x1

    move-object/from16 v0, p0

    iput v4, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mKeepSecondChunk:I

    .line 233
    sget-boolean v4, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v4, :cond_7

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "download by one interface for length is small, bSwitchOnFirstRead = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->bSwitchOnFirstRead:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lorg/apache/http/impl/client/TwoChunkInputStream;->log(Ljava/lang/String;)V

    .line 291
    :cond_7
    :goto_6
    if-eqz p21, :cond_8

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput v4, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mKeepSecondChunk:I

    .line 292
    :cond_8
    sget-boolean v4, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v4, :cond_9

    .line 293
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "calculated mBytesForTail is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-wide v6, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mBytesForTail:J

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", chunk0: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-wide/from16 v0, p5

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "-"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-wide v6, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mBytesForTail:J

    sub-long v6, p7, v6

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", chunk1: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-wide v6, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mBytesForTail:J

    sub-long v6, p7, v6

    const-wide/16 v10, 0x1

    add-long/2addr v6, v10

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "-"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-wide/from16 v0, p7

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", mKeepSecondChunk is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget v5, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mKeepSecondChunk:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lorg/apache/http/impl/client/TwoChunkInputStream;->log(Ljava/lang/String;)V

    .line 298
    :cond_9
    return-void

    .line 207
    .end local v8    # "lenTail":J
    .end local v20    # "cd":J
    .end local v26    # "timeForAllBy0":J
    .end local v28    # "timeForAllBy1":J
    .end local v30    # "timeForCachedData":J
    .end local v32    # "timeForTailBy0":J
    :cond_a
    const/4 v4, 0x1

    goto/16 :goto_2

    .line 215
    .restart local v8    # "lenTail":J
    .restart local v20    # "cd":J
    .restart local v30    # "timeForCachedData":J
    :cond_b
    const-wide/16 v4, 0x8

    mul-long/2addr v4, v8

    div-long v32, v4, p12

    goto/16 :goto_3

    .line 216
    .restart local v32    # "timeForTailBy0":J
    :cond_c
    move-object/from16 v0, p0

    iget-wide v4, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mLength:J

    const-wide/16 v6, 0x8

    mul-long/2addr v4, v6

    div-long v26, v4, p12

    goto/16 :goto_4

    .line 217
    .restart local v26    # "timeForAllBy0":J
    :cond_d
    move-object/from16 v0, p0

    iget-wide v4, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mLength:J

    const-wide/16 v6, 0x8

    mul-long/2addr v4, v6

    div-long v4, v4, p14

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->parentIS:Lorg/apache/http/impl/client/MultiSocketInputStream;

    iget-object v6, v6, Lorg/apache/http/impl/client/MultiSocketInputStream;->connectionTime:[J

    aget-wide v6, v6, p4

    add-long v28, v4, v6

    goto/16 :goto_5

    .line 235
    .restart local v28    # "timeForAllBy1":J
    :cond_e
    move-object/from16 v0, p0

    iget-wide v4, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->MAX_TIMEFORALL_BY0:J

    cmp-long v4, v26, v4

    if-ltz v4, :cond_f

    move-object/from16 v0, p0

    iget-wide v4, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->MAX_TIMEFORALL_BY1:J

    cmp-long v4, v28, v4

    if-gez v4, :cond_11

    .line 236
    :cond_f
    const-wide/16 v4, 0x0

    move-object/from16 v0, p0

    iput-wide v4, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mBytesForTail:J

    .line 237
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mSecThread:Lorg/apache/http/impl/client/TwoChunkInputStream$SecondChunkThread;

    .line 238
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mDBuf:Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;

    .line 239
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->bSecThreadStarted:Z

    .line 240
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->bSecThreadExisted:Z

    .line 241
    const/4 v4, 0x1

    move-object/from16 v0, p0

    iput v4, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mKeepSecondChunk:I

    .line 242
    sub-long v4, v26, v28

    move-object/from16 v0, p0

    iget-wide v6, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->MAX_TIMEFORALL_BY0:J

    move-object/from16 v0, p0

    iget-wide v10, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->MAX_TIMEFORALL_BY1:J

    sub-long/2addr v6, v10

    const-wide/16 v10, 0x2

    mul-long/2addr v6, v10

    cmp-long v4, v4, v6

    if-lez v4, :cond_10

    .line 243
    const/4 v4, 0x1

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->bSwitchOnFirstRead:Z

    .line 244
    const/4 v4, -0x1

    move-object/from16 v0, p0

    iput v4, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mKeepSecondChunk:I

    .line 246
    :cond_10
    sget-boolean v4, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v4, :cond_7

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "download by one interface time is small, bSwitchOnFirstRead = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->bSwitchOnFirstRead:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lorg/apache/http/impl/client/TwoChunkInputStream;->log(Ljava/lang/String;)V

    goto/16 :goto_6

    .line 248
    :cond_11
    const-wide/16 v4, 0x2

    cmp-long v4, p14, v4

    if-gtz v4, :cond_13

    .line 249
    const-wide/16 v22, 0x0

    .line 250
    .end local v8    # "lenTail":J
    .local v22, "lenTail":J
    sget-boolean v4, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v4, :cond_12

    const-string v4, "speed1 is 0, set tail be 0"

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lorg/apache/http/impl/client/TwoChunkInputStream;->log(Ljava/lang/String;)V

    .line 251
    :cond_12
    move-wide/from16 v0, v22

    move-object/from16 v2, p0

    iput-wide v0, v2, Lorg/apache/http/impl/client/TwoChunkInputStream;->mBytesForTail:J

    .line 252
    new-instance v5, Lorg/apache/http/impl/client/TwoChunkInputStream$SecondChunkThread;

    const-wide/16 v6, 0x1

    sub-long v8, p7, v6

    const/4 v12, 0x0

    move-object/from16 v6, p0

    move/from16 v7, p4

    move-wide/from16 v10, p7

    invoke-direct/range {v5 .. v12}, Lorg/apache/http/impl/client/TwoChunkInputStream$SecondChunkThread;-><init>(Lorg/apache/http/impl/client/TwoChunkInputStream;IJJLorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;)V

    move-object/from16 v0, p0

    iput-object v5, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mSecThread:Lorg/apache/http/impl/client/TwoChunkInputStream$SecondChunkThread;

    .line 253
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput v4, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mKeepSecondChunk:I

    move-wide/from16 v8, v22

    .end local v22    # "lenTail":J
    .restart local v8    # "lenTail":J
    goto/16 :goto_6

    .line 255
    :cond_13
    const-wide/16 v4, 0x2

    cmp-long v4, p12, v4

    if-gtz v4, :cond_15

    .line 256
    const-wide/16 v22, 0x0

    .line 257
    .end local v8    # "lenTail":J
    .restart local v22    # "lenTail":J
    sget-boolean v4, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v4, :cond_14

    const-string v4, "speed0 is 0, switch chunk and set tail be 0"

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lorg/apache/http/impl/client/TwoChunkInputStream;->log(Ljava/lang/String;)V

    .line 258
    :cond_14
    const/4 v4, 0x1

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->bSwitchOnFirstRead:Z

    .line 259
    move-wide/from16 v0, v22

    move-object/from16 v2, p0

    iput-wide v0, v2, Lorg/apache/http/impl/client/TwoChunkInputStream;->mBytesForTail:J

    .line 260
    new-instance v5, Lorg/apache/http/impl/client/TwoChunkInputStream$SecondChunkThread;

    const-wide/16 v6, 0x1

    sub-long v8, p7, v6

    const/4 v12, 0x0

    move-object/from16 v6, p0

    move/from16 v7, p3

    move-wide/from16 v10, p7

    invoke-direct/range {v5 .. v12}, Lorg/apache/http/impl/client/TwoChunkInputStream$SecondChunkThread;-><init>(Lorg/apache/http/impl/client/TwoChunkInputStream;IJJLorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;)V

    move-object/from16 v0, p0

    iput-object v5, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mSecThread:Lorg/apache/http/impl/client/TwoChunkInputStream$SecondChunkThread;

    .line 261
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput v4, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mKeepSecondChunk:I

    move-wide/from16 v8, v22

    .end local v22    # "lenTail":J
    .restart local v8    # "lenTail":J
    goto/16 :goto_6

    .line 263
    :cond_15
    move-object/from16 v0, p0

    iget-wide v4, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->MAX_TIMEFORTAIL_BY0:J

    cmp-long v4, v32, v4

    if-ltz v4, :cond_1c

    move-object/from16 v0, p0

    iget v4, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mDepth:I

    const/16 v5, 0xa

    if-gt v4, v5, :cond_1c

    .line 264
    move/from16 v13, p4

    .line 265
    .local v13, "secChunkSock":I
    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    cmpg-double v4, v24, v4

    if-gez v4, :cond_16

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->parentIS:Lorg/apache/http/impl/client/MultiSocketInputStream;

    iget-object v4, v4, Lorg/apache/http/impl/client/MultiSocketInputStream;->disconnCount:[I

    aget v4, v4, p3

    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->parentIS:Lorg/apache/http/impl/client/MultiSocketInputStream;

    iget-object v5, v5, Lorg/apache/http/impl/client/MultiSocketInputStream;->disconnCount:[I

    aget v5, v5, p4

    if-gt v4, v5, :cond_18

    :cond_16
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->parentIS:Lorg/apache/http/impl/client/MultiSocketInputStream;

    iget-object v4, v4, Lorg/apache/http/impl/client/MultiSocketInputStream;->disconnCount:[I

    aget v4, v4, p3

    const/4 v5, 0x2

    if-lt v4, v5, :cond_17

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->parentIS:Lorg/apache/http/impl/client/MultiSocketInputStream;

    iget-object v4, v4, Lorg/apache/http/impl/client/MultiSocketInputStream;->disconnCount:[I

    aget v4, v4, p4

    if-eqz v4, :cond_18

    :cond_17
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->parentIS:Lorg/apache/http/impl/client/MultiSocketInputStream;

    iget-object v4, v4, Lorg/apache/http/impl/client/MultiSocketInputStream;->disconnCount:[I

    aget v4, v4, p3

    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->parentIS:Lorg/apache/http/impl/client/MultiSocketInputStream;

    iget-object v5, v5, Lorg/apache/http/impl/client/MultiSocketInputStream;->disconnCount:[I

    aget v5, v5, p4

    mul-int/lit8 v5, v5, 0x2

    if-le v4, v5, :cond_1a

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->parentIS:Lorg/apache/http/impl/client/MultiSocketInputStream;

    iget-object v4, v4, Lorg/apache/http/impl/client/MultiSocketInputStream;->disconnCount:[I

    aget v4, v4, p4

    if-lez v4, :cond_1a

    .line 268
    :cond_18
    const-wide/high16 v4, 0x4024000000000000L    # 10.0

    const-wide v6, 0x3fb999999999999aL    # 0.1

    move-wide/from16 v0, p14

    long-to-double v10, v0

    move-wide/from16 v0, p12

    long-to-double v14, v0

    div-double/2addr v10, v14

    invoke-static {v6, v7, v10, v11}, Ljava/lang/Math;->max(DD)D

    move-result-wide v6

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->min(DD)D

    move-result-wide v24

    .line 270
    move-object/from16 v0, p0

    iget-wide v4, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mLength:J

    long-to-double v4, v4

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    add-double v6, v6, v24

    sget-wide v10, Lorg/apache/http/impl/client/MultiSocketInputStream;->B_Ratio_BUF_LTE:D

    div-double v10, v24, v10

    sub-double/2addr v6, v10

    div-double/2addr v4, v6

    double-to-long v8, v4

    .line 271
    sget-boolean v4, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v4, :cond_19

    const-string v4, "ratio too small, switch chunk"

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lorg/apache/http/impl/client/TwoChunkInputStream;->log(Ljava/lang/String;)V

    .line 272
    :cond_19
    move/from16 v13, p3

    .line 273
    const/4 v4, 0x1

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->bSwitchOnFirstRead:Z

    .line 275
    :cond_1a
    move-object/from16 v0, p0

    iput-wide v8, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mBytesForTail:J

    .line 276
    new-instance v4, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;

    sub-long v6, p7, v8

    const-wide/16 v10, 0x1

    add-long/2addr v6, v10

    move-object/from16 v5, p0

    invoke-direct/range {v4 .. v9}, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;-><init>(Lorg/apache/http/impl/client/TwoChunkInputStream;JJ)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mDBuf:Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;

    .line 277
    new-instance v11, Lorg/apache/http/impl/client/TwoChunkInputStream$SecondChunkThread;

    sub-long v4, p7, v8

    const-wide/16 v6, 0x1

    add-long v14, v4, v6

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mDBuf:Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;

    move-object/from16 v18, v0

    move-object/from16 v12, p0

    move-wide/from16 v16, p7

    invoke-direct/range {v11 .. v18}, Lorg/apache/http/impl/client/TwoChunkInputStream$SecondChunkThread;-><init>(Lorg/apache/http/impl/client/TwoChunkInputStream;IJJLorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;)V

    move-object/from16 v0, p0

    iput-object v11, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mSecThread:Lorg/apache/http/impl/client/TwoChunkInputStream$SecondChunkThread;

    .line 278
    sget-boolean v4, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v4, :cond_1b

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "divide chunk with a tail length "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", bSwitchOnFirstRead = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->bSwitchOnFirstRead:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lorg/apache/http/impl/client/TwoChunkInputStream;->log(Ljava/lang/String;)V

    .line 279
    :cond_1b
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput v4, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mKeepSecondChunk:I

    goto/16 :goto_6

    .line 282
    .end local v13    # "secChunkSock":I
    :cond_1c
    sget-boolean v4, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v4, :cond_1d

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "tail chunk is small, not to start another chunk for length "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lorg/apache/http/impl/client/TwoChunkInputStream;->log(Ljava/lang/String;)V

    .line 283
    :cond_1d
    const-wide/16 v8, 0x0

    .line 284
    move-object/from16 v0, p0

    iput-wide v8, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mBytesForTail:J

    .line 285
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mSecThread:Lorg/apache/http/impl/client/TwoChunkInputStream$SecondChunkThread;

    .line 286
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mDBuf:Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;

    .line 287
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->bSecThreadStarted:Z

    .line 288
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->bSecThreadExisted:Z

    .line 289
    const/4 v4, 0x1

    move-object/from16 v0, p0

    iput v4, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mKeepSecondChunk:I

    goto/16 :goto_6
.end method

.method static synthetic access$000(Lorg/apache/http/impl/client/TwoChunkInputStream;)Lorg/apache/http/impl/client/MultiSocketInputStream;
    .locals 1
    .param p0, "x0"    # Lorg/apache/http/impl/client/TwoChunkInputStream;

    .prologue
    .line 26
    iget-object v0, p0, Lorg/apache/http/impl/client/TwoChunkInputStream;->parentIS:Lorg/apache/http/impl/client/MultiSocketInputStream;

    return-object v0
.end method

.method static synthetic access$100(Lorg/apache/http/impl/client/TwoChunkInputStream;)[Lorg/apache/http/HttpRequest;
    .locals 1
    .param p0, "x0"    # Lorg/apache/http/impl/client/TwoChunkInputStream;

    .prologue
    .line 26
    iget-object v0, p0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mRequest:[Lorg/apache/http/HttpRequest;

    return-object v0
.end method

.method static synthetic access$1000(Lorg/apache/http/impl/client/TwoChunkInputStream;)Lorg/apache/http/protocol/HttpContext;
    .locals 1
    .param p0, "x0"    # Lorg/apache/http/impl/client/TwoChunkInputStream;

    .prologue
    .line 26
    iget-object v0, p0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mContext:Lorg/apache/http/protocol/HttpContext;

    return-object v0
.end method

.method static synthetic access$1100(Lorg/apache/http/impl/client/TwoChunkInputStream;)I
    .locals 1
    .param p0, "x0"    # Lorg/apache/http/impl/client/TwoChunkInputStream;

    .prologue
    .line 26
    iget v0, p0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mDepth:I

    return v0
.end method

.method static synthetic access$1200(Lorg/apache/http/impl/client/TwoChunkInputStream;)J
    .locals 2
    .param p0, "x0"    # Lorg/apache/http/impl/client/TwoChunkInputStream;

    .prologue
    .line 26
    iget-wide v0, p0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mFullConSize:J

    return-wide v0
.end method

.method static synthetic access$1300(Lorg/apache/http/impl/client/TwoChunkInputStream;)I
    .locals 1
    .param p0, "x0"    # Lorg/apache/http/impl/client/TwoChunkInputStream;

    .prologue
    .line 26
    iget v0, p0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mTimeOut:I

    return v0
.end method

.method static synthetic access$1402(Lorg/apache/http/impl/client/TwoChunkInputStream;Z)Z
    .locals 0
    .param p0, "x0"    # Lorg/apache/http/impl/client/TwoChunkInputStream;
    .param p1, "x1"    # Z

    .prologue
    .line 26
    iput-boolean p1, p0, Lorg/apache/http/impl/client/TwoChunkInputStream;->bFailedInSecChunk:Z

    return p1
.end method

.method static synthetic access$1500(Lorg/apache/http/impl/client/TwoChunkInputStream;)[J
    .locals 1
    .param p0, "x0"    # Lorg/apache/http/impl/client/TwoChunkInputStream;

    .prologue
    .line 26
    iget-object v0, p0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mStartReadTime:[J

    return-object v0
.end method

.method static synthetic access$1600(Lorg/apache/http/impl/client/TwoChunkInputStream;)I
    .locals 1
    .param p0, "x0"    # Lorg/apache/http/impl/client/TwoChunkInputStream;

    .prologue
    .line 26
    iget v0, p0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mSocket0:I

    return v0
.end method

.method static synthetic access$1700(Lorg/apache/http/impl/client/TwoChunkInputStream;)Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;
    .locals 1
    .param p0, "x0"    # Lorg/apache/http/impl/client/TwoChunkInputStream;

    .prologue
    .line 26
    iget-object v0, p0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mDBuf:Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;

    return-object v0
.end method

.method static synthetic access$1702(Lorg/apache/http/impl/client/TwoChunkInputStream;Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;)Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;
    .locals 0
    .param p0, "x0"    # Lorg/apache/http/impl/client/TwoChunkInputStream;
    .param p1, "x1"    # Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;

    .prologue
    .line 26
    iput-object p1, p0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mDBuf:Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;

    return-object p1
.end method

.method static synthetic access$1800(Lorg/apache/http/impl/client/TwoChunkInputStream;)[J
    .locals 1
    .param p0, "x0"    # Lorg/apache/http/impl/client/TwoChunkInputStream;

    .prologue
    .line 26
    iget-object v0, p0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mTimeForDownload:[J

    return-object v0
.end method

.method static synthetic access$1902(Lorg/apache/http/impl/client/TwoChunkInputStream;Z)Z
    .locals 0
    .param p0, "x0"    # Lorg/apache/http/impl/client/TwoChunkInputStream;
    .param p1, "x1"    # Z

    .prologue
    .line 26
    iput-boolean p1, p0, Lorg/apache/http/impl/client/TwoChunkInputStream;->bSecThreadExisted:Z

    return p1
.end method

.method static synthetic access$200(Lorg/apache/http/impl/client/TwoChunkInputStream;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lorg/apache/http/impl/client/TwoChunkInputStream;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lorg/apache/http/impl/client/TwoChunkInputStream;->log(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$2000(Lorg/apache/http/impl/client/TwoChunkInputStream;Z)V
    .locals 0
    .param p0, "x0"    # Lorg/apache/http/impl/client/TwoChunkInputStream;
    .param p1, "x1"    # Z

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lorg/apache/http/impl/client/TwoChunkInputStream;->createTwoChunkInTwoChunk(Z)V

    return-void
.end method

.method static synthetic access$300(Lorg/apache/http/impl/client/TwoChunkInputStream;)J
    .locals 2
    .param p0, "x0"    # Lorg/apache/http/impl/client/TwoChunkInputStream;

    .prologue
    .line 26
    iget-wide v0, p0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mRemainBytes:J

    return-wide v0
.end method

.method static synthetic access$400(Lorg/apache/http/impl/client/TwoChunkInputStream;)J
    .locals 2
    .param p0, "x0"    # Lorg/apache/http/impl/client/TwoChunkInputStream;

    .prologue
    .line 26
    iget-wide v0, p0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mBytesForTail:J

    return-wide v0
.end method

.method static synthetic access$402(Lorg/apache/http/impl/client/TwoChunkInputStream;J)J
    .locals 1
    .param p0, "x0"    # Lorg/apache/http/impl/client/TwoChunkInputStream;
    .param p1, "x1"    # J

    .prologue
    .line 26
    iput-wide p1, p0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mBytesForTail:J

    return-wide p1
.end method

.method static synthetic access$500(Lorg/apache/http/impl/client/TwoChunkInputStream;)Lorg/apache/http/impl/client/TwoChunkInputStream;
    .locals 1
    .param p0, "x0"    # Lorg/apache/http/impl/client/TwoChunkInputStream;

    .prologue
    .line 26
    iget-object v0, p0, Lorg/apache/http/impl/client/TwoChunkInputStream;->childIS:Lorg/apache/http/impl/client/TwoChunkInputStream;

    return-object v0
.end method

.method static synthetic access$502(Lorg/apache/http/impl/client/TwoChunkInputStream;Lorg/apache/http/impl/client/TwoChunkInputStream;)Lorg/apache/http/impl/client/TwoChunkInputStream;
    .locals 0
    .param p0, "x0"    # Lorg/apache/http/impl/client/TwoChunkInputStream;
    .param p1, "x1"    # Lorg/apache/http/impl/client/TwoChunkInputStream;

    .prologue
    .line 26
    iput-object p1, p0, Lorg/apache/http/impl/client/TwoChunkInputStream;->childIS:Lorg/apache/http/impl/client/TwoChunkInputStream;

    return-object p1
.end method

.method static synthetic access$600(Lorg/apache/http/impl/client/TwoChunkInputStream;)J
    .locals 2
    .param p0, "x0"    # Lorg/apache/http/impl/client/TwoChunkInputStream;

    .prologue
    .line 26
    iget-wide v0, p0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mLength:J

    return-wide v0
.end method

.method static synthetic access$700(Lorg/apache/http/impl/client/TwoChunkInputStream;)J
    .locals 2
    .param p0, "x0"    # Lorg/apache/http/impl/client/TwoChunkInputStream;

    .prologue
    .line 26
    iget-wide v0, p0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mStart:J

    return-wide v0
.end method

.method static synthetic access$800(Lorg/apache/http/impl/client/TwoChunkInputStream;)Lorg/apache/http/conn/ManagedClientConnection;
    .locals 1
    .param p0, "x0"    # Lorg/apache/http/impl/client/TwoChunkInputStream;

    .prologue
    .line 26
    iget-object v0, p0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mManConn:Lorg/apache/http/conn/ManagedClientConnection;

    return-object v0
.end method

.method static synthetic access$900(Lorg/apache/http/impl/client/TwoChunkInputStream;)[Lorg/apache/http/HttpHost;
    .locals 1
    .param p0, "x0"    # Lorg/apache/http/impl/client/TwoChunkInputStream;

    .prologue
    .line 26
    iget-object v0, p0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mHost:[Lorg/apache/http/HttpHost;

    return-object v0
.end method

.method private createTwoChunkInTwoChunk(Z)V
    .locals 26
    .param p1, "bReversed"    # Z

    .prologue
    .line 561
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->parentIS:Lorg/apache/http/impl/client/MultiSocketInputStream;

    iget-boolean v2, v2, Lorg/apache/http/impl/client/MultiSocketInputStream;->mFinished:Z

    if-eqz v2, :cond_1

    .line 562
    sget-boolean v2, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v2, :cond_0

    const-string v2, "createTwoChunkInTwoChunk: session is finished"

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lorg/apache/http/impl/client/TwoChunkInputStream;->log(Ljava/lang/String;)V

    .line 594
    :cond_0
    :goto_0
    return-void

    .line 565
    :cond_1
    monitor-enter p0

    .line 566
    :try_start_0
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->bSecThreadStarted:Z

    if-eqz v2, :cond_4

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->bSecThreadExisted:Z

    if-nez v2, :cond_4

    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->childIS:Lorg/apache/http/impl/client/TwoChunkInputStream;

    if-nez v2, :cond_4

    .line 567
    sget-boolean v2, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v2, :cond_2

    .line 568
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Chunk1 is finished while mRemainBytes="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-wide v4, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mRemainBytes:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " totallen="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-wide v4, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mLength:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " start offset="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-wide v4, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mStart:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lorg/apache/http/impl/client/TwoChunkInputStream;->log(Ljava/lang/String;)V

    .line 570
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->parentIS:Lorg/apache/http/impl/client/MultiSocketInputStream;

    move-object/from16 v0, p0

    iget v3, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mSocket0:I

    invoke-virtual {v2, v3}, Lorg/apache/http/impl/client/MultiSocketInputStream;->getSpeed(I)J

    move-result-wide v14

    .line 571
    .local v14, "speed0":J
    move-object/from16 v0, p0

    iget v2, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mSocket0:I

    add-int/lit8 v2, v2, 0x1

    rem-int/lit8 v2, v2, 0x2

    move-object/from16 v0, p0

    iput v2, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mSocket1:I

    .line 572
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->parentIS:Lorg/apache/http/impl/client/MultiSocketInputStream;

    move-object/from16 v0, p0

    iget v3, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mSocket1:I

    invoke-virtual {v2, v3}, Lorg/apache/http/impl/client/MultiSocketInputStream;->getSpeed(I)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v16

    .line 574
    .local v16, "speed1":J
    :try_start_1
    move-object/from16 v0, p0

    iget-wide v2, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mRemainBytes:J

    move-object/from16 v0, p0

    iget-wide v4, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mBytesForTail:J

    const-wide/32 v6, 0x100000

    add-long/2addr v4, v6

    cmp-long v2, v2, v4

    if-lez v2, :cond_5

    .line 575
    new-instance v2, Lorg/apache/http/impl/client/TwoChunkInputStream;

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mainInput:Ljava/io/InputStream;

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mManConn:Lorg/apache/http/conn/ManagedClientConnection;

    move-object/from16 v0, p0

    iget v5, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mSocket0:I

    move-object/from16 v0, p0

    iget v6, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mSocket1:I

    move-object/from16 v0, p0

    iget-wide v8, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mStart:J

    move-object/from16 v0, p0

    iget-wide v10, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mLength:J

    add-long/2addr v8, v10

    move-object/from16 v0, p0

    iget-wide v10, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mRemainBytes:J

    sub-long v7, v8, v10

    move-object/from16 v0, p0

    iget-wide v10, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mStart:J

    move-object/from16 v0, p0

    iget-wide v12, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mLength:J

    add-long/2addr v10, v12

    move-object/from16 v0, p0

    iget-wide v12, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mBytesForTail:J

    sub-long/2addr v10, v12

    const-wide/16 v12, 0x1

    sub-long v9, v10, v12

    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mRequest:[Lorg/apache/http/HttpRequest;

    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mHost:[Lorg/apache/http/HttpHost;

    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mContext:Lorg/apache/http/protocol/HttpContext;

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mDepth:I

    move/from16 v18, v0

    add-int/lit8 v18, v18, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->parentIS:Lorg/apache/http/impl/client/MultiSocketInputStream;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mFullConSize:J

    move-wide/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mTimeOut:I

    move/from16 v22, v0

    const-wide/16 v24, 0x0

    move/from16 v23, p1

    invoke-direct/range {v2 .. v25}, Lorg/apache/http/impl/client/TwoChunkInputStream;-><init>(Ljava/io/InputStream;Lorg/apache/http/conn/ManagedClientConnection;IIJJ[Lorg/apache/http/HttpRequest;[Lorg/apache/http/HttpHost;Lorg/apache/http/protocol/HttpContext;JJILorg/apache/http/impl/client/MultiSocketInputStream;JIZJ)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->childIS:Lorg/apache/http/impl/client/TwoChunkInputStream;

    .line 579
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->childIS:Lorg/apache/http/impl/client/TwoChunkInputStream;

    move-object/from16 v0, p0

    iput-object v2, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mainInput:Ljava/io/InputStream;

    .line 580
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->parentIS:Lorg/apache/http/impl/client/MultiSocketInputStream;

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->childIS:Lorg/apache/http/impl/client/TwoChunkInputStream;

    invoke-virtual {v3}, Lorg/apache/http/impl/client/TwoChunkInputStream;->isSingleThreadRun()Z

    move-result v3

    invoke-virtual {v2, v3}, Lorg/apache/http/impl/client/MultiSocketInputStream;->startSingleThreadRunning(Z)V

    .line 581
    sget-boolean v2, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v2, :cond_3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "new input stream in create twochunk sec mainInput = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mainInput:Ljava/io/InputStream;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lorg/apache/http/impl/client/TwoChunkInputStream;->log(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 590
    :cond_3
    :goto_1
    :try_start_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->childIS:Lorg/apache/http/impl/client/TwoChunkInputStream;

    if-eqz v2, :cond_4

    .line 591
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->childIS:Lorg/apache/http/impl/client/TwoChunkInputStream;

    invoke-virtual {v2}, Lorg/apache/http/impl/client/TwoChunkInputStream;->startRun()V

    .line 593
    .end local v14    # "speed0":J
    .end local v16    # "speed1":J
    :cond_4
    monitor-exit p0

    goto/16 :goto_0

    :catchall_0
    move-exception v2

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v2

    .line 584
    .restart local v14    # "speed0":J
    .restart local v16    # "speed1":J
    :cond_5
    :try_start_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->parentIS:Lorg/apache/http/impl/client/MultiSocketInputStream;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lorg/apache/http/impl/client/MultiSocketInputStream;->startSingleThreadRunning(Z)V

    .line 585
    sget-boolean v2, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v2, :cond_3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "not to create sec thread since mRemainBytes is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-wide v4, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mRemainBytes:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " and mBytesForTail is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-wide v4, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mBytesForTail:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lorg/apache/http/impl/client/TwoChunkInputStream;->log(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 589
    :catch_0
    move-exception v2

    goto :goto_1
.end method

.method private getCacheReadSpeed(JZ)D
    .locals 2
    .param p1, "fileSize"    # J
    .param p3, "bIsHTTPS"    # Z

    .prologue
    .line 1615
    sget-wide v0, Lorg/apache/http/impl/client/MultiSocketInputStream;->BUF_Read_Speed:D

    return-wide v0
.end method

.method private log(Ljava/lang/String;)V
    .locals 2
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 333
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "(Depth_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mDepth:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 334
    return-void
.end method

.method private readForChunk0([BII)I
    .locals 30
    .param p1, "buffer"    # [B
    .param p2, "offset"    # I
    .param p3, "count"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 607
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->parentIS:Lorg/apache/http/impl/client/MultiSocketInputStream;

    iget-object v5, v5, Lorg/apache/http/impl/client/MultiSocketInputStream;->mMainRequest:Lorg/apache/http/HttpRequest;

    check-cast v5, Lorg/apache/http/client/methods/HttpUriRequest;

    invoke-interface {v5}, Lorg/apache/http/client/methods/HttpUriRequest;->isAborted()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 608
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->parentIS:Lorg/apache/http/impl/client/MultiSocketInputStream;

    const/4 v6, 0x1

    iput-boolean v6, v5, Lorg/apache/http/impl/client/MultiSocketInputStream;->mFinished:Z

    .line 610
    :cond_0
    const/16 v18, 0x0

    .line 612
    .local v18, "isMyThrown":Z
    :try_start_0
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->bSwitchOnFirstRead:Z

    if-eqz v5, :cond_8

    .line 613
    sget-boolean v5, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v5, :cond_1

    .line 614
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Need to switch socket for the first read from "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget v6, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mSocket0:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " to "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget v6, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mSocket1:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lorg/apache/http/impl/client/TwoChunkInputStream;->log(Ljava/lang/String;)V

    .line 616
    :cond_1
    const/4 v5, 0x0

    move-object/from16 v0, p0

    iput-boolean v5, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->bSwitchOnFirstRead:Z

    .line 617
    const/16 v18, 0x1

    .line 618
    new-instance v5, Ljava/io/IOException;

    const-string v6, "Please switch socket"

    invoke-direct {v5, v6}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v5
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 632
    :catch_0
    move-exception v14

    .line 633
    .local v14, "e":Ljava/io/IOException;
    if-nez v18, :cond_2

    .line 634
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->parentIS:Lorg/apache/http/impl/client/MultiSocketInputStream;

    iget-object v5, v5, Lorg/apache/http/impl/client/MultiSocketInputStream;->disconnCount:[I

    move-object/from16 v0, p0

    iget v6, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mSocket0:I

    aget v7, v5, v6

    add-int/lit8 v7, v7, 0x1

    aput v7, v5, v6

    .line 635
    :cond_2
    sget-boolean v5, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v5, :cond_3

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "SocketTimeoutException Count: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->parentIS:Lorg/apache/http/impl/client/MultiSocketInputStream;

    iget-object v6, v6, Lorg/apache/http/impl/client/MultiSocketInputStream;->disconnCount:[I

    const/4 v7, 0x0

    aget v6, v6, v7

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->parentIS:Lorg/apache/http/impl/client/MultiSocketInputStream;

    iget-object v6, v6, Lorg/apache/http/impl/client/MultiSocketInputStream;->disconnCount:[I

    const/4 v7, 0x1

    aget v6, v6, v7

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lorg/apache/http/impl/client/TwoChunkInputStream;->log(Ljava/lang/String;)V

    .line 636
    :cond_3
    sget-boolean v5, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v5, :cond_4

    .line 637
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "IOException in TwoChunk reading while mRemainBytes="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-wide v6, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mRemainBytes:J

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " totallen="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-wide v6, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mLength:J

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " start offset="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-wide v6, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mStart:J

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lorg/apache/http/impl/client/TwoChunkInputStream;->log(Ljava/lang/String;)V

    .line 638
    invoke-static {v14}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/Throwable;)V

    .line 640
    :cond_4
    monitor-enter p0

    .line 641
    :try_start_1
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->childIS:Lorg/apache/http/impl/client/TwoChunkInputStream;

    if-eqz v5, :cond_b

    .line 642
    sget-boolean v5, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v5, :cond_5

    .line 643
    const-string v5, "childIS is created"

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lorg/apache/http/impl/client/TwoChunkInputStream;->log(Ljava/lang/String;)V

    .line 645
    :cond_5
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mainInput:Ljava/io/InputStream;

    move-object/from16 v0, p1

    move/from16 v1, p2

    move/from16 v2, p3

    invoke-virtual {v5, v0, v1, v2}, Ljava/io/InputStream;->read([BII)I

    move-result v20

    .line 646
    .local v20, "read":I
    sget-boolean v5, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v5, :cond_6

    .line 647
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "childIS is created and read "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v20

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lorg/apache/http/impl/client/TwoChunkInputStream;->log(Ljava/lang/String;)V

    .line 649
    :cond_6
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 740
    .end local v14    # "e":Ljava/io/IOException;
    .end local v20    # "read":I
    :cond_7
    :goto_0
    return v20

    .line 620
    :cond_8
    :try_start_2
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mStartReadTime:[J

    move-object/from16 v0, p0

    iget v6, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mSocket0:I

    aget-wide v6, v5, v6

    const-wide/16 v8, 0x0

    cmp-long v5, v6, v8

    if-nez v5, :cond_9

    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mStartReadTime:[J

    move-object/from16 v0, p0

    iget v6, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mSocket0:I

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    aput-wide v8, v5, v6

    .line 621
    :cond_9
    const/4 v5, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lorg/apache/http/impl/client/TwoChunkInputStream;->createTwoChunkInTwoChunk(Z)V

    .line 622
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mainInput:Ljava/io/InputStream;

    move-object/from16 v0, p1

    move/from16 v1, p2

    move/from16 v2, p3

    invoke-virtual {v5, v0, v1, v2}, Ljava/io/InputStream;->read([BII)I

    move-result v20

    .line 623
    .restart local v20    # "read":I
    if-gez v20, :cond_a

    .line 624
    new-instance v5, Ljava/io/IOException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "read return exception value "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, v20

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 626
    :cond_a
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->childIS:Lorg/apache/http/impl/client/TwoChunkInputStream;

    if-nez v5, :cond_7

    .line 627
    move-object/from16 v0, p0

    iget v5, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mSocket0:I

    move/from16 v0, v20

    int-to-long v6, v0

    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v6, v7}, Lorg/apache/http/impl/client/TwoChunkInputStream;->incByte(IJ)V

    .line 628
    move-object/from16 v0, p0

    iget v5, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mSocket0:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lorg/apache/http/impl/client/TwoChunkInputStream;->setTime(I)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 651
    .end local v20    # "read":I
    .restart local v14    # "e":Ljava/io/IOException;
    :cond_b
    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 652
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v24

    .line 653
    .local v24, "startWaitTime":J
    const/4 v13, 0x0

    .line 655
    .local v13, "bForceThrow":Z
    move-object/from16 v0, p0

    iget v5, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mSocket0:I

    add-int/lit8 v5, v5, 0x1

    rem-int/lit8 v27, v5, 0x2

    .line 656
    .local v27, "trySocket":I
    const/16 v26, 0x0

    .line 657
    .local v26, "triedNum":I
    :cond_c
    :goto_1
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->parentIS:Lorg/apache/http/impl/client/MultiSocketInputStream;

    iget-boolean v5, v5, Lorg/apache/http/impl/client/MultiSocketInputStream;->mFinished:Z

    if-nez v5, :cond_d

    .line 658
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->parentIS:Lorg/apache/http/impl/client/MultiSocketInputStream;

    iget-object v5, v5, Lorg/apache/http/impl/client/MultiSocketInputStream;->mMainRequest:Lorg/apache/http/HttpRequest;

    check-cast v5, Lorg/apache/http/client/methods/HttpUriRequest;

    invoke-interface {v5}, Lorg/apache/http/client/methods/HttpUriRequest;->isAborted()Z

    move-result v5

    if-eqz v5, :cond_e

    .line 659
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->parentIS:Lorg/apache/http/impl/client/MultiSocketInputStream;

    const/4 v6, 0x1

    iput-boolean v6, v5, Lorg/apache/http/impl/client/MultiSocketInputStream;->mFinished:Z

    .line 771
    :cond_d
    throw v14

    .line 651
    .end local v13    # "bForceThrow":Z
    .end local v24    # "startWaitTime":J
    .end local v26    # "triedNum":I
    .end local v27    # "trySocket":I
    :catchall_0
    move-exception v5

    :try_start_4
    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v5

    .line 662
    .restart local v13    # "bForceThrow":Z
    .restart local v24    # "startWaitTime":J
    .restart local v26    # "triedNum":I
    .restart local v27    # "trySocket":I
    :cond_e
    const/4 v5, 0x3

    move/from16 v0, v26

    if-le v0, v5, :cond_12

    .line 663
    sget-boolean v5, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v5, :cond_f

    const-string v5, "tried twice1, check NB status"

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lorg/apache/http/impl/client/TwoChunkInputStream;->log(Ljava/lang/String;)V

    .line 664
    :cond_f
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->parentIS:Lorg/apache/http/impl/client/MultiSocketInputStream;

    invoke-virtual {v5}, Lorg/apache/http/impl/client/MultiSocketInputStream;->isMultiRATworking()Z

    move-result v5

    if-nez v5, :cond_11

    .line 665
    sget-boolean v5, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v5, :cond_10

    const-string v5, "tried twice1, and NB Status is false"

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lorg/apache/http/impl/client/TwoChunkInputStream;->log(Ljava/lang/String;)V

    .line 666
    :cond_10
    throw v14

    .line 669
    :cond_11
    const/16 v26, 0x0

    .line 673
    :cond_12
    :try_start_5
    monitor-enter p0
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Lorg/apache/http/HttpException; {:try_start_5 .. :try_end_5} :catch_2

    .line 674
    :try_start_6
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->childIS:Lorg/apache/http/impl/client/TwoChunkInputStream;

    if-nez v5, :cond_1c

    .line 676
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mManConn:Lorg/apache/http/conn/ManagedClientConnection;

    invoke-static {v5}, Lorg/apache/http/impl/client/MultiSocketInputStream;->closeConnQuiet(Lorg/apache/http/conn/ManagedClientConnection;)V

    .line 677
    sget-boolean v5, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v5, :cond_13

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "try to close main input stream  in exception in depth "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget v6, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mDepth:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", input is "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mainInput:Ljava/io/InputStream;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lorg/apache/http/impl/client/TwoChunkInputStream;->log(Ljava/lang/String;)V

    .line 679
    :cond_13
    new-instance v21, Lorg/apache/http/impl/client/CustomHttpClient;

    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->parentIS:Lorg/apache/http/impl/client/MultiSocketInputStream;

    iget-object v5, v5, Lorg/apache/http/impl/client/MultiSocketInputStream;->mSchemeRegistry:Lorg/apache/http/conn/scheme/SchemeRegistry;

    move-object/from16 v0, v21

    invoke-direct {v0, v5}, Lorg/apache/http/impl/client/CustomHttpClient;-><init>(Lorg/apache/http/conn/scheme/SchemeRegistry;)V

    .line 680
    .local v21, "readCustomHttpClient":Lorg/apache/http/impl/client/CustomHttpClient;
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->parentIS:Lorg/apache/http/impl/client/MultiSocketInputStream;

    iget v5, v5, Lorg/apache/http/impl/client/MultiSocketInputStream;->mDestIPVer:I

    move/from16 v0, v27

    invoke-static {v0, v5}, Lorg/apache/http/impl/client/CustomHttpClient;->getLocalAddrEx(II)Ljava/net/InetAddress;

    move-result-object v12

    .line 681
    .local v12, "localAddr":Ljava/net/InetAddress;
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mRequest:[Lorg/apache/http/HttpRequest;

    aget-object v5, v5, v27

    move-object/from16 v0, v21

    invoke-virtual {v0, v5}, Lorg/apache/http/impl/client/CustomHttpClient;->createClientRequestDirector(Lorg/apache/http/HttpRequest;)Lorg/apache/http/impl/client/DefaultRequestDirector;

    move-result-object v4

    .line 690
    .local v4, "director":Lorg/apache/http/impl/client/DefaultRequestDirector;
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mRequest:[Lorg/apache/http/HttpRequest;

    aget-object v5, v5, v27

    invoke-interface {v5}, Lorg/apache/http/HttpRequest;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v5

    const-string v6, "http.route.default-proxy"

    move-object/from16 v0, p0

    iget-object v7, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mHost:[Lorg/apache/http/HttpHost;

    aget-object v7, v7, v27

    move-object/from16 v0, v21

    move/from16 v1, v27

    invoke-virtual {v0, v1, v7}, Lorg/apache/http/impl/client/CustomHttpClient;->getProxy(ILorg/apache/http/HttpHost;)Lorg/apache/http/HttpHost;

    move-result-object v7

    invoke-virtual {v4, v5, v6, v7}, Lorg/apache/http/impl/client/DefaultRequestDirector;->setParameter(Lorg/apache/http/params/HttpParams;Ljava/lang/String;Ljava/lang/Object;)V

    .line 694
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mHost:[Lorg/apache/http/HttpHost;

    aget-object v5, v5, v27

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mRequest:[Lorg/apache/http/HttpRequest;

    aget-object v6, v6, v27

    move-object/from16 v0, p0

    iget-object v7, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mContext:Lorg/apache/http/protocol/HttpContext;

    move-object/from16 v0, p0

    iget-wide v8, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mStart:J

    move-object/from16 v0, p0

    iget-wide v10, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mLength:J

    add-long/2addr v8, v10

    move-object/from16 v0, p0

    iget-wide v10, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mRemainBytes:J

    sub-long/2addr v8, v10

    move-object/from16 v0, p0

    iget-wide v10, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mEnd:J

    invoke-virtual/range {v4 .. v12}, Lorg/apache/http/impl/client/DefaultRequestDirector;->execute(Lorg/apache/http/HttpHost;Lorg/apache/http/HttpRequest;Lorg/apache/http/protocol/HttpContext;JJLjava/net/InetAddress;)Lorg/apache/http/HttpResponse;

    move-result-object v23

    .line 697
    .local v23, "response":Lorg/apache/http/HttpResponse;
    invoke-interface/range {v23 .. v23}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v5

    invoke-interface {v5}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v5

    const/16 v6, 0x193

    if-ne v5, v6, :cond_15

    .line 698
    const/4 v13, 0x1

    .line 699
    new-instance v5, Ljava/io/IOException;

    const-string v6, "Cannot connect to server"

    invoke-direct {v5, v6}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 730
    .end local v4    # "director":Lorg/apache/http/impl/client/DefaultRequestDirector;
    .end local v12    # "localAddr":Ljava/net/InetAddress;
    .end local v21    # "readCustomHttpClient":Lorg/apache/http/impl/client/CustomHttpClient;
    .end local v23    # "response":Lorg/apache/http/HttpResponse;
    :catchall_1
    move-exception v5

    monitor-exit p0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    :try_start_7
    throw v5
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_1
    .catch Lorg/apache/http/HttpException; {:try_start_7 .. :try_end_7} :catch_2

    .line 742
    :catch_1
    move-exception v17

    .line 743
    .local v17, "ex":Ljava/io/IOException;
    sget-boolean v5, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v5, :cond_14

    .line 744
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Exception in TwoChunk mainInput Handover "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v17

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lorg/apache/http/impl/client/TwoChunkInputStream;->log(Ljava/lang/String;)V

    .line 745
    invoke-static/range {v17 .. v17}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/Throwable;)V

    .line 748
    :cond_14
    if-eqz v13, :cond_1f

    throw v17

    .line 701
    .end local v17    # "ex":Ljava/io/IOException;
    .restart local v4    # "director":Lorg/apache/http/impl/client/DefaultRequestDirector;
    .restart local v12    # "localAddr":Ljava/net/InetAddress;
    .restart local v21    # "readCustomHttpClient":Lorg/apache/http/impl/client/CustomHttpClient;
    .restart local v23    # "response":Lorg/apache/http/HttpResponse;
    :cond_15
    :try_start_8
    invoke-interface/range {v23 .. v23}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v16

    .line 703
    .local v16, "entity":Lorg/apache/http/HttpEntity;
    iget-object v5, v4, Lorg/apache/http/impl/client/DefaultRequestDirector;->managedConn:Lorg/apache/http/conn/ManagedClientConnection;

    move-object/from16 v0, p0

    iput-object v5, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mManConn:Lorg/apache/http/conn/ManagedClientConnection;

    .line 704
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mSocket0:I

    move/from16 v19, v0

    .line 705
    .local v19, "preMainSocketID":I
    iget-object v5, v4, Lorg/apache/http/impl/client/DefaultRequestDirector;->managedConn:Lorg/apache/http/conn/ManagedClientConnection;

    invoke-interface {v5}, Lorg/apache/http/conn/ManagedClientConnection;->getLocalAddress()Ljava/net/InetAddress;

    move-result-object v5

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-object v7, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->parentIS:Lorg/apache/http/impl/client/MultiSocketInputStream;

    iget v7, v7, Lorg/apache/http/impl/client/MultiSocketInputStream;->mDestIPVer:I

    invoke-static {v6, v7}, Lorg/apache/http/impl/client/CustomHttpClient;->getLocalAddrEx(II)Ljava/net/InetAddress;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/net/InetAddress;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1b

    .line 706
    const/4 v5, 0x0

    move-object/from16 v0, p0

    iput v5, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mSocket0:I

    .line 707
    const/4 v5, 0x1

    move-object/from16 v0, p0

    iput v5, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mSocket1:I

    .line 713
    :goto_2
    sget-boolean v5, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v5, :cond_16

    .line 714
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "This is TwoChunk MainThead, actually switch to "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget v6, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mSocket0:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " from "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v19

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lorg/apache/http/impl/client/TwoChunkInputStream;->log(Ljava/lang/String;)V

    .line 716
    :cond_16
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->bSecThreadStarted:Z

    if-eqz v5, :cond_18

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->bSecThreadExisted:Z

    if-eqz v5, :cond_18

    move-object/from16 v0, p0

    iget v5, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mSocket0:I

    move/from16 v0, v19

    if-eq v5, v0, :cond_18

    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mSecThread:Lorg/apache/http/impl/client/TwoChunkInputStream$SecondChunkThread;

    if-eqz v5, :cond_18

    .line 717
    sget-boolean v5, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v5, :cond_17

    .line 718
    const-string v5, "Need to switch TwoChunk SecondThread Socket ID"

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lorg/apache/http/impl/client/TwoChunkInputStream;->log(Ljava/lang/String;)V

    .line 720
    :cond_17
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mSecThread:Lorg/apache/http/impl/client/TwoChunkInputStream$SecondChunkThread;

    move/from16 v0, v19

    invoke-virtual {v5, v0}, Lorg/apache/http/impl/client/TwoChunkInputStream$SecondChunkThread;->switchSocket(I)V

    .line 723
    :cond_18
    invoke-interface/range {v16 .. v16}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v5

    move-object/from16 v0, p0

    iput-object v5, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mainInput:Ljava/io/InputStream;

    .line 730
    .end local v4    # "director":Lorg/apache/http/impl/client/DefaultRequestDirector;
    .end local v12    # "localAddr":Ljava/net/InetAddress;
    .end local v16    # "entity":Lorg/apache/http/HttpEntity;
    .end local v19    # "preMainSocketID":I
    .end local v21    # "readCustomHttpClient":Lorg/apache/http/impl/client/CustomHttpClient;
    .end local v23    # "response":Lorg/apache/http/HttpResponse;
    :cond_19
    :goto_3
    monitor-exit p0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 731
    :try_start_9
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mStartReadTime:[J

    move-object/from16 v0, p0

    iget v6, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mSocket0:I

    aget-wide v6, v5, v6

    const-wide/16 v8, 0x0

    cmp-long v5, v6, v8

    if-nez v5, :cond_1a

    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mStartReadTime:[J

    move-object/from16 v0, p0

    iget v6, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mSocket0:I

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    aput-wide v8, v5, v6

    .line 732
    :cond_1a
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mainInput:Ljava/io/InputStream;

    move-object/from16 v0, p1

    move/from16 v1, p2

    move/from16 v2, p3

    invoke-virtual {v5, v0, v1, v2}, Ljava/io/InputStream;->read([BII)I

    move-result v22

    .line 733
    .local v22, "readLen":I
    if-gez v22, :cond_1d

    .line 734
    new-instance v5, Ljava/io/IOException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "read return exception value "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, v22

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v5
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_1
    .catch Lorg/apache/http/HttpException; {:try_start_9 .. :try_end_9} :catch_2

    .line 767
    .end local v22    # "readLen":I
    :catch_2
    move-exception v15

    .line 768
    .local v15, "e1":Lorg/apache/http/HttpException;
    sget-boolean v5, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v5, :cond_c

    invoke-static {v15}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/Throwable;)V

    goto/16 :goto_1

    .line 710
    .end local v15    # "e1":Lorg/apache/http/HttpException;
    .restart local v4    # "director":Lorg/apache/http/impl/client/DefaultRequestDirector;
    .restart local v12    # "localAddr":Ljava/net/InetAddress;
    .restart local v16    # "entity":Lorg/apache/http/HttpEntity;
    .restart local v19    # "preMainSocketID":I
    .restart local v21    # "readCustomHttpClient":Lorg/apache/http/impl/client/CustomHttpClient;
    .restart local v23    # "response":Lorg/apache/http/HttpResponse;
    :cond_1b
    const/4 v5, 0x1

    :try_start_a
    move-object/from16 v0, p0

    iput v5, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mSocket0:I

    .line 711
    const/4 v5, 0x0

    move-object/from16 v0, p0

    iput v5, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mSocket1:I

    goto/16 :goto_2

    .line 726
    .end local v4    # "director":Lorg/apache/http/impl/client/DefaultRequestDirector;
    .end local v12    # "localAddr":Ljava/net/InetAddress;
    .end local v16    # "entity":Lorg/apache/http/HttpEntity;
    .end local v19    # "preMainSocketID":I
    .end local v21    # "readCustomHttpClient":Lorg/apache/http/impl/client/CustomHttpClient;
    .end local v23    # "response":Lorg/apache/http/HttpResponse;
    :cond_1c
    sget-boolean v5, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v5, :cond_19

    .line 727
    const-string v5, "childIS is created before this "

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lorg/apache/http/impl/client/TwoChunkInputStream;->log(Ljava/lang/String;)V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    goto :goto_3

    .line 736
    .restart local v22    # "readLen":I
    :cond_1d
    :try_start_b
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->childIS:Lorg/apache/http/impl/client/TwoChunkInputStream;

    if-nez v5, :cond_1e

    .line 737
    move-object/from16 v0, p0

    iget v5, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mSocket0:I

    move/from16 v0, v22

    int-to-long v6, v0

    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v6, v7}, Lorg/apache/http/impl/client/TwoChunkInputStream;->incByte(IJ)V

    .line 738
    move-object/from16 v0, p0

    iget v5, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mSocket0:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lorg/apache/http/impl/client/TwoChunkInputStream;->setTime(I)V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_1
    .catch Lorg/apache/http/HttpException; {:try_start_b .. :try_end_b} :catch_2

    :cond_1e
    move/from16 v20, v22

    .line 740
    goto/16 :goto_0

    .line 749
    .end local v22    # "readLen":I
    .restart local v17    # "ex":Ljava/io/IOException;
    :cond_1f
    add-int/lit8 v26, v26, 0x1

    .line 751
    add-int/lit8 v5, v27, 0x1

    rem-int/lit8 v27, v5, 0x2

    .line 752
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sub-long v28, v6, v24

    .line 753
    .local v28, "waitedTime":J
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mTimeForDownload:[J

    move-object/from16 v0, p0

    iget v6, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mSocket0:I

    aget-wide v6, v5, v6

    const-wide/16 v8, 0x0

    cmp-long v5, v6, v8

    if-lez v5, :cond_20

    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->childIS:Lorg/apache/http/impl/client/TwoChunkInputStream;

    if-nez v5, :cond_20

    .line 754
    move-object/from16 v0, p0

    iget v5, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mSocket0:I

    move-object/from16 v0, p0

    move-wide/from16 v1, v28

    invoke-virtual {v0, v5, v1, v2}, Lorg/apache/http/impl/client/TwoChunkInputStream;->decTime(IJ)V

    .line 755
    :cond_20
    move-object/from16 v0, p0

    iget v5, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mTimeOut:I

    if-lez v5, :cond_22

    move-object/from16 v0, p0

    iget v5, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mTimeOut:I

    int-to-long v6, v5

    cmp-long v5, v28, v6

    if-lez v5, :cond_22

    .line 756
    sget-boolean v5, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v5, :cond_21

    .line 757
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "TwoChunk waited time "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-wide/from16 v0, v28

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " time out "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget v6, v0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mTimeOut:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lorg/apache/http/impl/client/TwoChunkInputStream;->log(Ljava/lang/String;)V

    .line 759
    :cond_21
    throw v17

    .line 761
    :cond_22
    monitor-enter p0

    .line 763
    :try_start_c
    sget v5, Lorg/apache/http/impl/client/MultiSocketInputStream;->HANDOVER_WAIT_INTERVAL:I

    int-to-long v6, v5

    move-object/from16 v0, p0

    invoke-virtual {v0, v6, v7}, Ljava/lang/Object;->wait(J)V
    :try_end_c
    .catch Ljava/lang/Throwable; {:try_start_c .. :try_end_c} :catch_3
    .catchall {:try_start_c .. :try_end_c} :catchall_2

    .line 766
    :goto_4
    :try_start_d
    monitor-exit p0

    goto/16 :goto_1

    :catchall_2
    move-exception v5

    monitor-exit p0
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_2

    throw v5

    .line 765
    :catch_3
    move-exception v5

    goto :goto_4
.end method


# virtual methods
.method public available()I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 448
    iget-wide v0, p0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mRemainBytes:J

    iget-wide v2, p0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mBytesForTail:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 449
    iget-object v0, p0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mainInput:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->available()I

    move-result v0

    .line 453
    :goto_0
    return v0

    .line 452
    :cond_0
    iget-object v1, p0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mDBuf:Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;

    monitor-enter v1

    .line 453
    :try_start_0
    iget-object v0, p0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mDBuf:Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;

    invoke-virtual {v0}, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->getRestLength()J

    move-result-wide v2

    long-to-int v0, v2

    monitor-exit v1

    goto :goto_0

    .line 454
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method protected bTwoInfDownloading()Z
    .locals 1

    .prologue
    .line 320
    iget-object v0, p0, Lorg/apache/http/impl/client/TwoChunkInputStream;->childIS:Lorg/apache/http/impl/client/TwoChunkInputStream;

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lorg/apache/http/impl/client/TwoChunkInputStream;->bSecThreadExisted:Z

    .line 321
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lorg/apache/http/impl/client/TwoChunkInputStream;->childIS:Lorg/apache/http/impl/client/TwoChunkInputStream;

    invoke-virtual {v0}, Lorg/apache/http/impl/client/TwoChunkInputStream;->bTwoInfDownloading()Z

    move-result v0

    goto :goto_0
.end method

.method public close()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 471
    sget-boolean v0, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v0, :cond_0

    .line 472
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "TwoChunkInputStream:close "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mDepth:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/apache/http/impl/client/TwoChunkInputStream;->log(Ljava/lang/String;)V

    .line 474
    :cond_0
    iget-boolean v0, p0, Lorg/apache/http/impl/client/TwoChunkInputStream;->closed:Z

    if-eqz v0, :cond_2

    .line 492
    :cond_1
    :goto_0
    return-void

    .line 477
    :cond_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/http/impl/client/TwoChunkInputStream;->closed:Z

    .line 479
    :try_start_0
    iget-object v0, p0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mDBuf:Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;

    if-eqz v0, :cond_3

    .line 480
    iget-object v1, p0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mDBuf:Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;

    monitor-enter v1
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 481
    :try_start_1
    iget-object v0, p0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mDBuf:Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;

    invoke-virtual {v0}, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->clearBuffer()V

    .line 482
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 484
    :cond_3
    :try_start_2
    iget-object v0, p0, Lorg/apache/http/impl/client/TwoChunkInputStream;->childIS:Lorg/apache/http/impl/client/TwoChunkInputStream;

    if-eqz v0, :cond_4

    .line 485
    iget-object v0, p0, Lorg/apache/http/impl/client/TwoChunkInputStream;->childIS:Lorg/apache/http/impl/client/TwoChunkInputStream;

    invoke-virtual {v0}, Lorg/apache/http/impl/client/TwoChunkInputStream;->close()V

    .line 487
    :cond_4
    iget-object v0, p0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mSecThread:Lorg/apache/http/impl/client/TwoChunkInputStream$SecondChunkThread;

    if-eqz v0, :cond_1

    .line 488
    iget-object v0, p0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mSecThread:Lorg/apache/http/impl/client/TwoChunkInputStream$SecondChunkThread;

    invoke-virtual {v0}, Lorg/apache/http/impl/client/TwoChunkInputStream$SecondChunkThread;->closeSocketAndStreams()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 491
    :catch_0
    move-exception v0

    goto :goto_0

    .line 482
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v0
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_0
.end method

.method protected decTime(IJ)V
    .locals 0
    .param p1, "sid"    # I
    .param p2, "time"    # J

    .prologue
    .line 540
    return-void
.end method

.method protected getDownloadLen(I)J
    .locals 4
    .param p1, "sid"    # I

    .prologue
    .line 500
    iget-object v0, p0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mDataDownloaded:[J

    aget-wide v2, v0, p1

    iget-object v0, p0, Lorg/apache/http/impl/client/TwoChunkInputStream;->childIS:Lorg/apache/http/impl/client/TwoChunkInputStream;

    if-nez v0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    add-long/2addr v0, v2

    return-wide v0

    :cond_0
    iget-object v0, p0, Lorg/apache/http/impl/client/TwoChunkInputStream;->childIS:Lorg/apache/http/impl/client/TwoChunkInputStream;

    invoke-virtual {v0, p1}, Lorg/apache/http/impl/client/TwoChunkInputStream;->getDownloadLen(I)J

    move-result-wide v0

    goto :goto_0
.end method

.method protected getDownloadTime(I)J
    .locals 4
    .param p1, "sid"    # I

    .prologue
    .line 509
    iget-object v0, p0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mTimeForDownload:[J

    aget-wide v2, v0, p1

    iget-object v0, p0, Lorg/apache/http/impl/client/TwoChunkInputStream;->childIS:Lorg/apache/http/impl/client/TwoChunkInputStream;

    if-nez v0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    add-long/2addr v0, v2

    return-wide v0

    :cond_0
    iget-object v0, p0, Lorg/apache/http/impl/client/TwoChunkInputStream;->childIS:Lorg/apache/http/impl/client/TwoChunkInputStream;

    invoke-virtual {v0, p1}, Lorg/apache/http/impl/client/TwoChunkInputStream;->getDownloadTime(I)J

    move-result-wide v0

    goto :goto_0
.end method

.method public getNeedToExitSecThread()I
    .locals 1

    .prologue
    .line 329
    iget v0, p0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mKeepSecondChunk:I

    return v0
.end method

.method protected incByte(IJ)V
    .locals 4
    .param p1, "sid"    # I
    .param p2, "bytes"    # J

    .prologue
    .line 548
    iget-object v0, p0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mDataDownloaded:[J

    aget-wide v2, v0, p1

    add-long/2addr v2, p2

    aput-wide v2, v0, p1

    .line 549
    sget-boolean v0, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v0, :cond_1

    .line 551
    const-wide/32 v0, 0x100000

    cmp-long v0, p2, v0

    if-gtz v0, :cond_0

    const-wide/16 v0, 0x0

    cmp-long v0, p2, v0

    if-gtz v0, :cond_1

    .line 552
    :cond_0
    new-instance v0, Ljava/lang/Throwable;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "speed calc >> set data for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " from "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mDataDownloaded:[J

    aget-wide v2, v2, p1

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " with a inc "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/Throwable;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/Throwable;)V

    .line 554
    :cond_1
    return-void
.end method

.method protected isClosed()Z
    .locals 1

    .prologue
    .line 463
    iget-boolean v0, p0, Lorg/apache/http/impl/client/TwoChunkInputStream;->closed:Z

    return v0
.end method

.method public isSingleThreadRun()Z
    .locals 1

    .prologue
    .line 301
    iget-object v0, p0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mSecThread:Lorg/apache/http/impl/client/TwoChunkInputStream$SecondChunkThread;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public read()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1605
    const/4 v1, 0x1

    new-array v0, v1, [B

    .line 1606
    .local v0, "buf":[B
    invoke-virtual {p0, v0}, Lorg/apache/http/impl/client/TwoChunkInputStream;->read([B)I

    move-result v1

    return v1
.end method

.method public read([B)I
    .locals 2
    .param p1, "buffer"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1611
    const/4 v0, 0x0

    array-length v1, p1

    invoke-virtual {p0, p1, v0, v1}, Lorg/apache/http/impl/client/TwoChunkInputStream;->read([BII)I

    move-result v0

    return v0
.end method

.method public read([BII)I
    .locals 12
    .param p1, "buffer"    # [B
    .param p2, "offset"    # I
    .param p3, "count"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    const-wide/16 v10, 0x0

    .line 343
    const/4 v0, 0x0

    .line 345
    .local v0, "readLen":I
    if-nez p3, :cond_1

    .line 346
    sget-boolean v5, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v5, :cond_0

    .line 347
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "read for 0 : buffer.length="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    array-length v6, p1

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", offset="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", count="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v5}, Lorg/apache/http/impl/client/TwoChunkInputStream;->log(Ljava/lang/String;)V

    .line 443
    :cond_0
    :goto_0
    return v4

    .line 350
    :cond_1
    iget-wide v6, p0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mRemainBytes:J

    cmp-long v5, v6, v10

    if-nez v5, :cond_2

    .line 351
    sget-boolean v5, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v5, :cond_0

    .line 352
    const-string v5, "finish to read size, no byte remained, return 0"

    invoke-direct {p0, v5}, Lorg/apache/http/impl/client/TwoChunkInputStream;->log(Ljava/lang/String;)V

    goto :goto_0

    .line 356
    :cond_2
    iget-wide v6, p0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mRemainBytes:J

    cmp-long v5, v6, v10

    if-gez v5, :cond_3

    .line 357
    sget-boolean v5, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v5, :cond_0

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "finish to read size, remained bytes is negative: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-wide v6, p0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mRemainBytes:J

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v5}, Lorg/apache/http/impl/client/TwoChunkInputStream;->log(Ljava/lang/String;)V

    goto :goto_0

    .line 361
    :cond_3
    move v1, p3

    .line 362
    .local v1, "toReadCount":I
    int-to-long v4, v1

    iget-wide v6, p0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mRemainBytes:J

    cmp-long v4, v4, v6

    if-lez v4, :cond_5

    .line 363
    sget-boolean v4, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v4, :cond_4

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "toReadCount "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " > mReamainBytes "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-wide v6, p0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mRemainBytes:J

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lorg/apache/http/impl/client/TwoChunkInputStream;->log(Ljava/lang/String;)V

    .line 364
    :cond_4
    iget-wide v4, p0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mRemainBytes:J

    long-to-int v1, v4

    .line 367
    :cond_5
    iget-wide v4, p0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mBytesForTail:J

    cmp-long v4, v4, v10

    if-gtz v4, :cond_9

    .line 368
    invoke-direct {p0, p1, p2, v1}, Lorg/apache/http/impl/client/TwoChunkInputStream;->readForChunk0([BII)I

    move-result v0

    .line 424
    :cond_6
    :goto_1
    const/4 v4, -0x1

    if-ne v0, v4, :cond_17

    .line 425
    sget-boolean v4, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v4, :cond_7

    .line 426
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "unexpected end of stream, but return 0, mRemainBytes="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-wide v6, p0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mRemainBytes:J

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", mBytesForTail="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-wide v6, p0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mBytesForTail:J

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lorg/apache/http/impl/client/TwoChunkInputStream;->log(Ljava/lang/String;)V

    .line 439
    :cond_7
    :goto_2
    if-nez v0, :cond_18

    iget-boolean v4, p0, Lorg/apache/http/impl/client/TwoChunkInputStream;->bFailedInSecChunk:Z

    if-eqz v4, :cond_18

    .line 440
    sget-boolean v4, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v4, :cond_8

    const-string v4, "exception in reading secChunk"

    invoke-direct {p0, v4}, Lorg/apache/http/impl/client/TwoChunkInputStream;->log(Ljava/lang/String;)V

    .line 441
    :cond_8
    new-instance v4, Ljava/io/IOException;

    const-string v5, "exception in reading secChunk"

    invoke-direct {v4, v5}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 371
    :cond_9
    iget-wide v4, p0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mRemainBytes:J

    iget-wide v6, p0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mBytesForTail:J

    cmp-long v4, v4, v6

    if-lez v4, :cond_a

    .line 372
    int-to-long v4, v1

    iget-wide v6, p0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mRemainBytes:J

    iget-wide v8, p0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mBytesForTail:J

    sub-long/2addr v6, v8

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    long-to-int v4, v4

    invoke-direct {p0, p1, p2, v4}, Lorg/apache/http/impl/client/TwoChunkInputStream;->readForChunk0([BII)I

    move-result v0

    goto :goto_1

    .line 375
    :cond_a
    iget-wide v4, p0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mRemainBytes:J

    iget-wide v6, p0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mBytesForTail:J

    cmp-long v4, v4, v6

    if-nez v4, :cond_c

    .line 376
    iget-boolean v4, p0, Lorg/apache/http/impl/client/TwoChunkInputStream;->bFailedInSecChunk:Z

    if-eqz v4, :cond_f

    .line 377
    sget-boolean v4, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v4, :cond_b

    const-string v4, "bFailedInSecChunk is true, not to use second chunk, and never re-divide"

    invoke-direct {p0, v4}, Lorg/apache/http/impl/client/TwoChunkInputStream;->log(Ljava/lang/String;)V

    .line 378
    :cond_b
    iput-wide v10, p0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mBytesForTail:J

    .line 379
    invoke-direct {p0, p1, p2, v1}, Lorg/apache/http/impl/client/TwoChunkInputStream;->readForChunk0([BII)I

    move-result v0

    .line 390
    :cond_c
    :goto_3
    iget-wide v4, p0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mBytesForTail:J

    cmp-long v4, v4, v10

    if-lez v4, :cond_6

    .line 391
    iget-object v5, p0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mDBuf:Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;

    monitor-enter v5

    .line 392
    :try_start_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 393
    .local v2, "startTime":J
    :goto_4
    iget-object v4, p0, Lorg/apache/http/impl/client/TwoChunkInputStream;->parentIS:Lorg/apache/http/impl/client/MultiSocketInputStream;

    iget-boolean v4, v4, Lorg/apache/http/impl/client/MultiSocketInputStream;->mFinished:Z

    if-nez v4, :cond_16

    .line 394
    iget-object v4, p0, Lorg/apache/http/impl/client/TwoChunkInputStream;->parentIS:Lorg/apache/http/impl/client/MultiSocketInputStream;

    iget-object v4, v4, Lorg/apache/http/impl/client/MultiSocketInputStream;->mMainRequest:Lorg/apache/http/HttpRequest;

    check-cast v4, Lorg/apache/http/client/methods/HttpUriRequest;

    invoke-interface {v4}, Lorg/apache/http/client/methods/HttpUriRequest;->isAborted()Z

    move-result v4

    if-eqz v4, :cond_d

    .line 395
    iget-object v4, p0, Lorg/apache/http/impl/client/TwoChunkInputStream;->parentIS:Lorg/apache/http/impl/client/MultiSocketInputStream;

    const/4 v6, 0x1

    iput-boolean v6, v4, Lorg/apache/http/impl/client/MultiSocketInputStream;->mFinished:Z

    .line 397
    :cond_d
    iget-object v4, p0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mDBuf:Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;

    invoke-virtual {v4, p1, p2, v1}, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->read([BII)I

    move-result v0

    .line 398
    if-nez v0, :cond_16

    .line 399
    iget-boolean v4, p0, Lorg/apache/http/impl/client/TwoChunkInputStream;->bFailedInSecChunk:Z

    if-eqz v4, :cond_13

    .line 400
    sget-boolean v4, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v4, :cond_e

    const-string v4, "exception in reading secChunk"

    invoke-direct {p0, v4}, Lorg/apache/http/impl/client/TwoChunkInputStream;->log(Ljava/lang/String;)V

    .line 401
    :cond_e
    new-instance v4, Ljava/io/IOException;

    const-string v6, "exception in reading secChunk"

    invoke-direct {v4, v6}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 420
    .end local v2    # "startTime":J
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4

    .line 382
    :cond_f
    sget-boolean v4, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v4, :cond_10

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "finish read chunk0 mRemainBytes == mBytesForTail = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-wide v6, p0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mRemainBytes:J

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lorg/apache/http/impl/client/TwoChunkInputStream;->log(Ljava/lang/String;)V

    .line 383
    :cond_10
    iget-boolean v4, p0, Lorg/apache/http/impl/client/TwoChunkInputStream;->bSecThreadStarted:Z

    if-eqz v4, :cond_11

    iget-boolean v4, p0, Lorg/apache/http/impl/client/TwoChunkInputStream;->bSecThreadExisted:Z

    if-eqz v4, :cond_11

    iget-object v4, p0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mSecThread:Lorg/apache/http/impl/client/TwoChunkInputStream$SecondChunkThread;

    if-eqz v4, :cond_11

    iget-object v4, p0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mSecThread:Lorg/apache/http/impl/client/TwoChunkInputStream$SecondChunkThread;

    iget v5, p0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mSocket0:I

    invoke-virtual {v4, v5}, Lorg/apache/http/impl/client/TwoChunkInputStream$SecondChunkThread;->startTryBoth(I)V

    .line 384
    :cond_11
    sget-boolean v4, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v4, :cond_12

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "try to close main input stream in depth "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mDepth:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", input is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mainInput:Ljava/io/InputStream;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lorg/apache/http/impl/client/TwoChunkInputStream;->log(Ljava/lang/String;)V

    .line 385
    :cond_12
    iget-object v4, p0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mManConn:Lorg/apache/http/conn/ManagedClientConnection;

    invoke-static {v4}, Lorg/apache/http/impl/client/MultiSocketInputStream;->closeConnQuiet(Lorg/apache/http/conn/ManagedClientConnection;)V

    .line 386
    const/4 v4, 0x0

    iput-object v4, p0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mManConn:Lorg/apache/http/conn/ManagedClientConnection;

    .line 387
    sget-boolean v4, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v4, :cond_c

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "end to close main stream in depth "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mDepth:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lorg/apache/http/impl/client/TwoChunkInputStream;->log(Ljava/lang/String;)V

    goto/16 :goto_3

    .line 403
    .restart local v2    # "startTime":J
    :cond_13
    :try_start_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sub-long/2addr v6, v2

    sget v4, Lorg/apache/http/impl/client/MultiSocketInputStream;->WAIT_FOR_RANGEREQUEST_TIME_OUT:I

    add-int/lit16 v4, v4, 0x3e8

    int-to-long v8, v4

    cmp-long v4, v6, v8

    if-ltz v4, :cond_15

    .line 404
    sget-boolean v4, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v4, :cond_14

    .line 405
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "wait for long time without read data, sec thread = "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v6, p0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mSecThread:Lorg/apache/http/impl/client/TwoChunkInputStream$SecondChunkThread;

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lorg/apache/http/impl/client/TwoChunkInputStream;->log(Ljava/lang/String;)V

    .line 407
    :cond_14
    iget-object v4, p0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mSecThread:Lorg/apache/http/impl/client/TwoChunkInputStream$SecondChunkThread;

    if-eqz v4, :cond_15

    .line 408
    iget-object v4, p0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mSecThread:Lorg/apache/http/impl/client/TwoChunkInputStream$SecondChunkThread;

    const/4 v6, 0x1

    invoke-virtual {v4, v6}, Lorg/apache/http/impl/client/TwoChunkInputStream$SecondChunkThread;->createTwoChunkInSec(Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 412
    :cond_15
    :try_start_2
    iget-object v4, p0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mDBuf:Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;

    sget v6, Lorg/apache/http/impl/client/MultiSocketInputStream;->HANDOVER_WAIT_INTERVAL:I

    int-to-long v6, v6

    invoke-virtual {v4, v6, v7}, Ljava/lang/Object;->wait(J)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_4

    .line 414
    :catch_0
    move-exception v4

    goto/16 :goto_4

    .line 420
    :cond_16
    :try_start_3
    monitor-exit v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_1

    .line 429
    .end local v2    # "startTime":J
    :cond_17
    iget-wide v4, p0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mRemainBytes:J

    int-to-long v6, v0

    sub-long/2addr v4, v6

    iput-wide v4, p0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mRemainBytes:J

    .line 431
    iget-wide v4, p0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mRemainBytes:J

    cmp-long v4, v4, v10

    if-gtz v4, :cond_7

    .line 432
    sget-boolean v4, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v4, :cond_7

    .line 433
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "no byte remained, start to end input"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-wide v6, p0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mRemainBytes:J

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lorg/apache/http/impl/client/TwoChunkInputStream;->log(Ljava/lang/String;)V

    goto/16 :goto_2

    :cond_18
    move v4, v0

    .line 443
    goto/16 :goto_0
.end method

.method protected setTime(I)V
    .locals 6
    .param p1, "sid"    # I

    .prologue
    .line 517
    iget-object v0, p0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mTimeForDownload:[J

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-object v1, p0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mStartReadTime:[J

    aget-wide v4, v1, p1

    sub-long/2addr v2, v4

    aput-wide v2, v0, p1

    .line 525
    return-void
.end method

.method public startRun()V
    .locals 4

    .prologue
    .line 305
    monitor-enter p0

    .line 306
    :try_start_0
    iget-object v1, p0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mSecThread:Lorg/apache/http/impl/client/TwoChunkInputStream$SecondChunkThread;

    if-eqz v1, :cond_2

    .line 307
    sget-boolean v1, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v1, :cond_0

    const-string v1, "start run sec chunk"

    invoke-direct {p0, v1}, Lorg/apache/http/impl/client/TwoChunkInputStream;->log(Ljava/lang/String;)V

    .line 308
    :cond_0
    new-instance v0, Ljava/lang/Thread;

    iget-object v1, p0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mSecThread:Lorg/apache/http/impl/client/TwoChunkInputStream$SecondChunkThread;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ":SecChunk_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lorg/apache/http/impl/client/TwoChunkInputStream;->mDepth:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    .line 309
    .local v0, "t":Ljava/lang/Thread;
    const/4 v1, 0x1

    iput-boolean v1, p0, Lorg/apache/http/impl/client/TwoChunkInputStream;->bSecThreadExisted:Z

    .line 310
    const/4 v1, 0x1

    iput-boolean v1, p0, Lorg/apache/http/impl/client/TwoChunkInputStream;->bSecThreadStarted:Z

    .line 311
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 316
    .end local v0    # "t":Ljava/lang/Thread;
    :cond_1
    :goto_0
    monitor-exit p0

    .line 317
    return-void

    .line 314
    :cond_2
    sget-boolean v1, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v1, :cond_1

    const-string v1, "sec chunk is not created"

    invoke-direct {p0, v1}, Lorg/apache/http/impl/client/TwoChunkInputStream;->log(Ljava/lang/String;)V

    goto :goto_0

    .line 316
    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method
