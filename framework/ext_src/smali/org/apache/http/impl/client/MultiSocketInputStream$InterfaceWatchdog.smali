.class Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;
.super Ljava/lang/Object;
.source "MultiSocketInputStream.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/http/impl/client/MultiSocketInputStream;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "InterfaceWatchdog"
.end annotation


# instance fields
.field bFileTested:Z

.field final synthetic this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;


# direct methods
.method constructor <init>(Lorg/apache/http/impl/client/MultiSocketInputStream;)V
    .locals 1

    .prologue
    .line 1110
    iput-object p1, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1112
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->bFileTested:Z

    return-void
.end method

.method private testFileCaching()Z
    .locals 12

    .prologue
    const/4 v7, 0x1

    .line 1118
    iget-boolean v8, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->bFileTested:Z

    if-eqz v8, :cond_0

    .line 1176
    :goto_0
    return v7

    .line 1119
    :cond_0
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, ".sbBuf_"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Thread;->getId()J

    move-result-wide v10

    invoke-virtual {v8, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "_"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    invoke-virtual {v8, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1120
    .local v0, "bufFileName":Ljava/lang/String;
    sget-boolean v8, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v8, :cond_1

    .line 1121
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "try to save buffer to file "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 1123
    :cond_1
    const/4 v6, 0x0

    .line 1124
    .local v6, "fileBuf":Ljava/io/File;
    const/4 v1, 0x0

    .line 1125
    .local v1, "dis":Ljava/io/DataInputStream;
    const/4 v3, 0x0

    .line 1127
    .local v3, "dos":Ljava/io/DataOutputStream;
    :try_start_0
    iget-object v8, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    invoke-virtual {v8}, Lorg/apache/http/impl/client/MultiSocketInputStream;->createBufferDir()V

    .line 1128
    const/4 v8, 0x0

    iget-object v9, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    iget-object v9, v9, Lorg/apache/http/impl/client/MultiSocketInputStream;->parentDir:Ljava/io/File;

    invoke-static {v0, v8, v9}, Ljava/io/File;->createTempFile(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;

    move-result-object v6

    .line 1129
    sget-boolean v8, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v8, :cond_2

    .line 1130
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "resource check: test buffered file generated "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v6}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 1132
    :cond_2
    new-instance v2, Ljava/io/DataInputStream;

    new-instance v8, Ljava/io/FileInputStream;

    invoke-direct {v8, v6}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v2, v8}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 1133
    .end local v1    # "dis":Ljava/io/DataInputStream;
    .local v2, "dis":Ljava/io/DataInputStream;
    :try_start_1
    new-instance v4, Ljava/io/DataOutputStream;

    new-instance v8, Ljava/io/FileOutputStream;

    invoke-direct {v8, v6}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v4, v8}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_4

    .line 1134
    .end local v3    # "dos":Ljava/io/DataOutputStream;
    .local v4, "dos":Ljava/io/DataOutputStream;
    const/4 v8, 0x1

    :try_start_2
    invoke-virtual {v4, v8}, Ljava/io/DataOutputStream;->write(I)V

    .line 1135
    if-eqz v2, :cond_a

    .line 1136
    invoke-virtual {v2}, Ljava/io/DataInputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_5

    .line 1137
    const/4 v1, 0x0

    .line 1139
    .end local v2    # "dis":Ljava/io/DataInputStream;
    .restart local v1    # "dis":Ljava/io/DataInputStream;
    :goto_1
    if-eqz v4, :cond_9

    .line 1140
    :try_start_3
    invoke-virtual {v4}, Ljava/io/DataOutputStream;->flush()V

    .line 1141
    invoke-virtual {v4}, Ljava/io/DataOutputStream;->close()V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_6

    .line 1142
    const/4 v3, 0x0

    .line 1144
    .end local v4    # "dos":Ljava/io/DataOutputStream;
    .restart local v3    # "dos":Ljava/io/DataOutputStream;
    :goto_2
    if-eqz v6, :cond_4

    .line 1145
    :try_start_4
    invoke-virtual {v6}, Ljava/io/File;->delete()Z

    .line 1146
    sget-boolean v8, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v8, :cond_3

    .line 1147
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "resource check: test buffered file removed "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v6}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 1149
    :cond_3
    const/4 v6, 0x0

    .line 1151
    :cond_4
    const/4 v8, 0x1

    iput-boolean v8, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->bFileTested:Z
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_0

    goto/16 :goto_0

    .line 1154
    :catch_0
    move-exception v5

    .line 1155
    .local v5, "e":Ljava/lang/Throwable;
    :goto_3
    sget-boolean v7, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v7, :cond_5

    invoke-static {v5}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/Throwable;)V

    .line 1158
    :cond_5
    if-eqz v1, :cond_6

    .line 1159
    :try_start_5
    invoke-virtual {v1}, Ljava/io/DataInputStream;->close()V
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_1

    .line 1160
    const/4 v1, 0x0

    .line 1165
    :cond_6
    :goto_4
    if-eqz v3, :cond_7

    .line 1166
    :try_start_6
    invoke-virtual {v3}, Ljava/io/DataOutputStream;->close()V
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_2

    .line 1167
    const/4 v3, 0x0

    .line 1172
    :cond_7
    :goto_5
    if-eqz v6, :cond_8

    .line 1173
    :try_start_7
    invoke-virtual {v6}, Ljava/io/File;->delete()Z
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_3

    .line 1176
    :cond_8
    :goto_6
    const/4 v7, 0x0

    goto/16 :goto_0

    .line 1163
    :catch_1
    move-exception v7

    goto :goto_4

    .line 1170
    :catch_2
    move-exception v7

    goto :goto_5

    .line 1175
    :catch_3
    move-exception v7

    goto :goto_6

    .line 1154
    .end local v1    # "dis":Ljava/io/DataInputStream;
    .end local v5    # "e":Ljava/lang/Throwable;
    .restart local v2    # "dis":Ljava/io/DataInputStream;
    :catch_4
    move-exception v5

    move-object v1, v2

    .end local v2    # "dis":Ljava/io/DataInputStream;
    .restart local v1    # "dis":Ljava/io/DataInputStream;
    goto :goto_3

    .end local v1    # "dis":Ljava/io/DataInputStream;
    .end local v3    # "dos":Ljava/io/DataOutputStream;
    .restart local v2    # "dis":Ljava/io/DataInputStream;
    .restart local v4    # "dos":Ljava/io/DataOutputStream;
    :catch_5
    move-exception v5

    move-object v3, v4

    .end local v4    # "dos":Ljava/io/DataOutputStream;
    .restart local v3    # "dos":Ljava/io/DataOutputStream;
    move-object v1, v2

    .end local v2    # "dis":Ljava/io/DataInputStream;
    .restart local v1    # "dis":Ljava/io/DataInputStream;
    goto :goto_3

    .end local v3    # "dos":Ljava/io/DataOutputStream;
    .restart local v4    # "dos":Ljava/io/DataOutputStream;
    :catch_6
    move-exception v5

    move-object v3, v4

    .end local v4    # "dos":Ljava/io/DataOutputStream;
    .restart local v3    # "dos":Ljava/io/DataOutputStream;
    goto :goto_3

    .end local v3    # "dos":Ljava/io/DataOutputStream;
    .restart local v4    # "dos":Ljava/io/DataOutputStream;
    :cond_9
    move-object v3, v4

    .end local v4    # "dos":Ljava/io/DataOutputStream;
    .restart local v3    # "dos":Ljava/io/DataOutputStream;
    goto :goto_2

    .end local v1    # "dis":Ljava/io/DataInputStream;
    .end local v3    # "dos":Ljava/io/DataOutputStream;
    .restart local v2    # "dis":Ljava/io/DataInputStream;
    .restart local v4    # "dos":Ljava/io/DataOutputStream;
    :cond_a
    move-object v1, v2

    .end local v2    # "dis":Ljava/io/DataInputStream;
    .restart local v1    # "dis":Ljava/io/DataInputStream;
    goto :goto_1
.end method


# virtual methods
.method public run()V
    .locals 99

    .prologue
    .line 1185
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Thread;->getId()J

    move-result-wide v8

    # setter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mMainThreadID:J
    invoke-static {v6, v8, v9}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$002(Lorg/apache/http/impl/client/MultiSocketInputStream;J)J

    .line 1186
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mHttpContext:Lorg/apache/http/protocol/HttpContext;
    invoke-static {v6}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$100(Lorg/apache/http/impl/client/MultiSocketInputStream;)Lorg/apache/http/protocol/HttpContext;

    move-result-object v6

    const-string v7, "SMARTBONDING_MAIN_THREAD_ID"

    new-instance v8, Ljava/lang/Long;

    move-object/from16 v0, p0

    iget-object v9, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mMainThreadID:J
    invoke-static {v9}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$000(Lorg/apache/http/impl/client/MultiSocketInputStream;)J

    move-result-wide v10

    invoke-direct {v8, v10, v11}, Ljava/lang/Long;-><init>(J)V

    invoke-interface {v6, v7, v8}, Lorg/apache/http/protocol/HttpContext;->setAttribute(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1188
    sget-boolean v6, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v6, :cond_0

    .line 1189
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "resource check: thread checking: start interface watchdog "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Thread;->getId()J

    move-result-wide v8

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 1190
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "setup main threadID to be "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mMainThreadID:J
    invoke-static {v7}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$000(Lorg/apache/http/impl/client/MultiSocketInputStream;)J

    move-result-wide v8

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 1191
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Main interface target: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mHttpHost:[Lorg/apache/http/HttpHost;
    invoke-static {v7}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$200(Lorg/apache/http/impl/client/MultiSocketInputStream;)[Lorg/apache/http/HttpHost;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mMainSocketInterface:I
    invoke-static {v8}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$300(Lorg/apache/http/impl/client/MultiSocketInputStream;)I

    move-result v8

    aget-object v7, v7, v8

    invoke-virtual {v7}, Lorg/apache/http/HttpHost;->getHostName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 1192
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Secondary interface target: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mHttpHost:[Lorg/apache/http/HttpHost;
    invoke-static {v7}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$200(Lorg/apache/http/impl/client/MultiSocketInputStream;)[Lorg/apache/http/HttpHost;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mMainSocketInterface:I
    invoke-static {v8}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$300(Lorg/apache/http/impl/client/MultiSocketInputStream;)I

    move-result v8

    add-int/lit8 v8, v8, 0x1

    rem-int/lit8 v8, v8, 0x2

    aget-object v7, v7, v8

    invoke-virtual {v7}, Lorg/apache/http/HttpHost;->getHostName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 1195
    :cond_0
    new-instance v5, Lorg/apache/http/impl/client/CustomHttpClient;

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    iget-object v6, v6, Lorg/apache/http/impl/client/MultiSocketInputStream;->mSchemeRegistry:Lorg/apache/http/conn/scheme/SchemeRegistry;

    invoke-direct {v5, v6}, Lorg/apache/http/impl/client/CustomHttpClient;-><init>(Lorg/apache/http/conn/scheme/SchemeRegistry;)V

    .line 1197
    .local v5, "customHttpClientInfWdog":Lorg/apache/http/impl/client/CustomHttpClient;
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    iget-object v6, v6, Lorg/apache/http/impl/client/MultiSocketInputStream;->mMainRequest:Lorg/apache/http/HttpRequest;

    invoke-interface {v6}, Lorg/apache/http/HttpRequest;->getRequestLine()Lorg/apache/http/RequestLine;

    move-result-object v6

    invoke-interface {v6}, Lorg/apache/http/RequestLine;->getUri()Ljava/lang/String;

    move-result-object v12

    .line 1198
    .local v12, "url":Ljava/lang/String;
    const-string v98, "/"

    .line 1199
    .local v98, "uri":Ljava/lang/String;
    const/16 v6, 0x8

    move-object/from16 v0, v98

    invoke-virtual {v12, v0, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v57

    .line 1200
    .local v57, "index":I
    const/4 v6, -0x1

    move/from16 v0, v57

    if-le v0, v6, :cond_1

    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v6

    move/from16 v0, v57

    if-ge v0, v6, :cond_1

    .line 1201
    move/from16 v0, v57

    invoke-virtual {v12, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v98

    .line 1204
    :cond_1
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mMainThreadID:J
    invoke-static {v6}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$000(Lorg/apache/http/impl/client/MultiSocketInputStream;)J

    move-result-wide v6

    move-object/from16 v0, p0

    iget-object v8, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mContentLength:J
    invoke-static {v8}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$400(Lorg/apache/http/impl/client/MultiSocketInputStream;)J

    move-result-wide v8

    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    iget-wide v10, v10, Lorg/apache/http/impl/client/MultiSocketInputStream;->mOriOffset:J

    invoke-virtual/range {v5 .. v12}, Lorg/apache/http/impl/client/CustomHttpClient;->startSBDirectUsage(JJJLjava/lang/String;)Z

    move-result v13

    .line 1205
    .local v13, "bShallUseSB":Z
    sget-boolean v6, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v6, :cond_2

    .line 1206
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "startSBUsage return "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v13}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " for thread "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mMainThreadID:J
    invoke-static {v7}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$000(Lorg/apache/http/impl/client/MultiSocketInputStream;)J

    move-result-wide v8

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", size "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mContentLength:J
    invoke-static {v7}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$400(Lorg/apache/http/impl/client/MultiSocketInputStream;)J

    move-result-wide v8

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 1209
    :cond_2
    if-eqz v13, :cond_5

    .line 1210
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->sbUsedLocker:Ljava/lang/Object;
    invoke-static {v6}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$500(Lorg/apache/http/impl/client/MultiSocketInputStream;)Ljava/lang/Object;

    move-result-object v7

    monitor-enter v7

    .line 1211
    :try_start_0
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    const/4 v8, 0x1

    # setter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->bSBUsed:Z
    invoke-static {v6, v8}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$602(Lorg/apache/http/impl/client/MultiSocketInputStream;Z)Z

    .line 1213
    :cond_3
    :goto_0
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    iget-boolean v6, v6, Lorg/apache/http/impl/client/MultiSocketInputStream;->mFinished:Z

    if-nez v6, :cond_4

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    iget-object v6, v6, Lorg/apache/http/impl/client/MultiSocketInputStream;->mMainRequest:Lorg/apache/http/HttpRequest;

    check-cast v6, Lorg/apache/http/client/methods/HttpUriRequest;

    invoke-interface {v6}, Lorg/apache/http/client/methods/HttpUriRequest;->isAborted()Z

    move-result v6

    if-nez v6, :cond_4

    .line 1214
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mMainThreadID:J
    invoke-static {v6}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$000(Lorg/apache/http/impl/client/MultiSocketInputStream;)J

    move-result-wide v8

    invoke-virtual {v5, v8, v9}, Lorg/apache/http/impl/client/CustomHttpClient;->getSBUsageStatus(J)I

    move-result v4

    .line 1215
    .local v4, "bSBStatus":I
    const/4 v6, 0x2

    if-ne v4, v6, :cond_c

    .line 1216
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    const/4 v8, 0x0

    # setter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->bSBUsed:Z
    invoke-static {v6, v8}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$602(Lorg/apache/http/impl/client/MultiSocketInputStream;Z)Z

    .line 1217
    sget-boolean v6, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v6, :cond_4

    .line 1218
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "getSBUsuageStatus return NO bSBStatus for thread "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v8, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mMainThreadID:J
    invoke-static {v8}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$000(Lorg/apache/http/impl/client/MultiSocketInputStream;)J

    move-result-wide v8

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 1239
    .end local v4    # "bSBStatus":I
    :cond_4
    :goto_1
    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1243
    :cond_5
    const/16 v59, 0x1

    .line 1248
    .local v59, "isRangeRequestSupported":Z
    :goto_2
    if-eqz v13, :cond_8

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->bSBUsed:Z
    invoke-static {v6}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$600(Lorg/apache/http/impl/client/MultiSocketInputStream;)Z

    move-result v6

    if-eqz v6, :cond_8

    if-eqz v59, :cond_8

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    iget-boolean v6, v6, Lorg/apache/http/impl/client/MultiSocketInputStream;->mFinished:Z

    if-nez v6, :cond_8

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBytesRemaining:J
    invoke-static {v6}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$700(Lorg/apache/http/impl/client/MultiSocketInputStream;)J

    move-result-wide v6

    sget v8, Lorg/apache/http/impl/client/MultiSocketInputStream;->MIN_SIZE_FOR_MULTISOCKET_CON_CLOSE:I

    int-to-long v8, v8

    cmp-long v6, v6, v8

    if-lez v6, :cond_8

    .line 1249
    const-wide v66, 0x7fffffffffffffffL

    .line 1250
    .local v66, "maxBufDataSize":J
    invoke-direct/range {p0 .. p0}, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->testFileCaching()Z

    move-result v6

    if-nez v6, :cond_e

    .line 1251
    sget-boolean v6, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v6, :cond_6

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "testFileCaching return false, stop D/B while remain data size  is "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBytesRemaining:J
    invoke-static {v7}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$700(Lorg/apache/http/impl/client/MultiSocketInputStream;)J

    move-result-wide v8

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 1252
    :cond_6
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->sbUsedLocker:Ljava/lang/Object;
    invoke-static {v6}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$500(Lorg/apache/http/impl/client/MultiSocketInputStream;)Ljava/lang/Object;

    move-result-object v7

    monitor-enter v7

    .line 1253
    :try_start_1
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->bSBUsed:Z
    invoke-static {v6}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$600(Lorg/apache/http/impl/client/MultiSocketInputStream;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 1254
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    iget-object v6, v6, Lorg/apache/http/impl/client/MultiSocketInputStream;->customHttpClient:Lorg/apache/http/impl/client/CustomHttpClient;

    move-object/from16 v0, p0

    iget-object v8, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mMainThreadID:J
    invoke-static {v8}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$000(Lorg/apache/http/impl/client/MultiSocketInputStream;)J

    move-result-wide v8

    const/4 v10, 0x2

    invoke-virtual {v6, v8, v9, v10}, Lorg/apache/http/impl/client/CustomHttpClient;->stopSBUsageWithReason(JI)I

    .line 1255
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    const/4 v8, 0x0

    # setter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->bSBUsed:Z
    invoke-static {v6, v8}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$602(Lorg/apache/http/impl/client/MultiSocketInputStream;Z)Z

    .line 1257
    :cond_7
    monitor-exit v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1622
    .end local v66    # "maxBufDataSize":J
    :cond_8
    :goto_3
    sget-boolean v6, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v6, :cond_9

    .line 1623
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "finish interface watchdog with bMultisocket="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mIsMultiSocketStarted:Z
    invoke-static {v7}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$3800(Lorg/apache/http/impl/client/MultiSocketInputStream;)Z

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 1626
    :cond_9
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->TEST_GET_TRAFFIC:Z
    invoke-static {v6}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$4100(Lorg/apache/http/impl/client/MultiSocketInputStream;)Z

    move-result v6

    if-nez v6, :cond_a

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->bSBUsed:Z
    invoke-static {v6}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$600(Lorg/apache/http/impl/client/MultiSocketInputStream;)Z

    move-result v6

    if-eqz v6, :cond_51

    .line 1627
    :cond_a
    const/4 v6, 0x2

    new-array v0, v6, [J

    move-object/from16 v79, v0

    fill-array-data v79, :array_0

    .line 1628
    .local v79, "prevTime":[J
    const/4 v6, 0x2

    new-array v0, v6, [J

    move-object/from16 v75, v0

    fill-array-data v75, :array_1

    .line 1629
    .local v75, "prevBytes":[J
    const/4 v6, 0x2

    new-array v0, v6, [J

    move-object/from16 v78, v0

    fill-array-data v78, :array_2

    .line 1630
    .local v78, "prevSp":[J
    const/16 v69, 0x5

    .line 1631
    .local v69, "maxCountToSend":I
    const-wide/16 v36, 0x7d0

    .line 1632
    .local v36, "byteThresholdAs0":J
    const/4 v6, 0x2

    new-array v0, v6, [I

    move-object/from16 v74, v0

    fill-array-data v74, :array_3

    .line 1634
    .local v74, "noDataCount":[I
    :cond_b
    :goto_4
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    iget-object v6, v6, Lorg/apache/http/impl/client/MultiSocketInputStream;->mMainRequest:Lorg/apache/http/HttpRequest;

    check-cast v6, Lorg/apache/http/client/methods/HttpUriRequest;

    invoke-interface {v6}, Lorg/apache/http/client/methods/HttpUriRequest;->isAborted()Z

    move-result v6

    if-nez v6, :cond_51

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    iget-boolean v6, v6, Lorg/apache/http/impl/client/MultiSocketInputStream;->mFinished:Z

    if-nez v6, :cond_51

    .line 1635
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->TEST_GET_TRAFFIC:Z
    invoke-static {v6}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$4100(Lorg/apache/http/impl/client/MultiSocketInputStream;)Z

    move-result v6

    if-eqz v6, :cond_45

    .line 1636
    const/4 v6, 0x2

    new-array v0, v6, [J

    move-object/from16 v63, v0

    .line 1637
    .local v63, "lens":[J
    const/4 v6, 0x2

    new-array v0, v6, [J

    move-object/from16 v96, v0

    .line 1638
    .local v96, "times":[J
    const/16 v54, 0x0

    .local v54, "i":I
    :goto_5
    const/4 v6, 0x2

    move/from16 v0, v54

    if-ge v0, v6, :cond_43

    .line 1639
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mDataDownloaded:[J
    invoke-static {v6}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$4200(Lorg/apache/http/impl/client/MultiSocketInputStream;)[J

    move-result-object v6

    aget-wide v8, v6, v54

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->childIS:Lorg/apache/http/impl/client/TwoChunkInputStream;
    invoke-static {v6}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$4300(Lorg/apache/http/impl/client/MultiSocketInputStream;)Lorg/apache/http/impl/client/TwoChunkInputStream;

    move-result-object v6

    if-nez v6, :cond_41

    const-wide/16 v6, 0x0

    :goto_6
    add-long/2addr v6, v8

    aput-wide v6, v63, v54

    .line 1640
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mTimeForDownload:[J
    invoke-static {v6}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$4400(Lorg/apache/http/impl/client/MultiSocketInputStream;)[J

    move-result-object v6

    aget-wide v8, v6, v54

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->childIS:Lorg/apache/http/impl/client/TwoChunkInputStream;
    invoke-static {v6}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$4300(Lorg/apache/http/impl/client/MultiSocketInputStream;)Lorg/apache/http/impl/client/TwoChunkInputStream;

    move-result-object v6

    if-nez v6, :cond_42

    const-wide/16 v6, 0x0

    :goto_7
    add-long/2addr v6, v8

    aput-wide v6, v96, v54

    .line 1638
    add-int/lit8 v54, v54, 0x1

    goto :goto_5

    .line 1222
    .end local v36    # "byteThresholdAs0":J
    .end local v54    # "i":I
    .end local v59    # "isRangeRequestSupported":Z
    .end local v63    # "lens":[J
    .end local v69    # "maxCountToSend":I
    .end local v74    # "noDataCount":[I
    .end local v75    # "prevBytes":[J
    .end local v78    # "prevSp":[J
    .end local v79    # "prevTime":[J
    .end local v96    # "times":[J
    .restart local v4    # "bSBStatus":I
    :cond_c
    const/4 v6, 0x1

    if-ne v4, v6, :cond_d

    .line 1223
    :try_start_2
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    const/4 v8, 0x1

    # setter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->bSBUsed:Z
    invoke-static {v6, v8}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$602(Lorg/apache/http/impl/client/MultiSocketInputStream;Z)Z

    .line 1224
    sget-boolean v6, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v6, :cond_4

    .line 1225
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "getSBUsuageStatus return YES bSBStatus for thread "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v8, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mMainThreadID:J
    invoke-static {v8}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$000(Lorg/apache/http/impl/client/MultiSocketInputStream;)J

    move-result-wide v8

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1239
    .end local v4    # "bSBStatus":I
    :catchall_0
    move-exception v6

    monitor-exit v7
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v6

    .line 1231
    .restart local v4    # "bSBStatus":I
    :cond_d
    :try_start_3
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->sbUsedLocker:Ljava/lang/Object;
    invoke-static {v6}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$500(Lorg/apache/http/impl/client/MultiSocketInputStream;)Ljava/lang/Object;

    move-result-object v6

    const-wide/16 v8, 0x64

    invoke-virtual {v6, v8, v9}, Ljava/lang/Object;->wait(J)V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_0

    .line 1233
    :catch_0
    move-exception v48

    .line 1234
    .local v48, "e":Ljava/lang/Throwable;
    :try_start_4
    sget-boolean v6, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v6, :cond_3

    invoke-static/range {v48 .. v48}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 1257
    .end local v4    # "bSBStatus":I
    .end local v48    # "e":Ljava/lang/Throwable;
    .restart local v59    # "isRangeRequestSupported":Z
    .restart local v66    # "maxBufDataSize":J
    :catchall_1
    move-exception v6

    :try_start_5
    monitor-exit v7
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    throw v6

    .line 1261
    :cond_e
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockManager:Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;
    invoke-static {v6}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$800(Lorg/apache/http/impl/client/MultiSocketInputStream;)Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;

    move-result-object v8

    monitor-enter v8

    .line 1264
    :try_start_6
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    iget-object v6, v6, Lorg/apache/http/impl/client/MultiSocketInputStream;->mMainRequest:Lorg/apache/http/HttpRequest;

    check-cast v6, Lorg/apache/http/client/methods/HttpUriRequest;

    invoke-interface {v6}, Lorg/apache/http/client/methods/HttpUriRequest;->isAborted()Z

    move-result v6

    if-eqz v6, :cond_f

    .line 1265
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    const/4 v7, 0x1

    iput-boolean v7, v6, Lorg/apache/http/impl/client/MultiSocketInputStream;->mFinished:Z

    .line 1266
    monitor-exit v8

    goto/16 :goto_3

    .line 1619
    :catchall_2
    move-exception v6

    monitor-exit v8
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    throw v6

    .line 1269
    :cond_f
    :try_start_7
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    move-object/from16 v0, p0

    iget-object v9, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    iget-wide v10, v9, Lorg/apache/http/impl/client/MultiSocketInputStream;->mLastReadTime:J

    sub-long v64, v6, v10

    .line 1270
    .local v64, "mainReadTimeDiff":J
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    iget-wide v6, v6, Lorg/apache/http/impl/client/MultiSocketInputStream;->mLastReadTime:J

    const-wide/16 v10, 0x0

    cmp-long v6, v6, v10

    if-lez v6, :cond_10

    sget v6, Lorg/apache/http/impl/client/MultiSocketInputStream;->WAIT_FOR_INTERFACE_TIME_OUT:I

    int-to-long v6, v6

    cmp-long v6, v64, v6

    if-lez v6, :cond_10

    .line 1273
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    const/4 v7, 0x1

    iput-boolean v7, v6, Lorg/apache/http/impl/client/MultiSocketInputStream;->mFinished:Z

    .line 1274
    monitor-exit v8

    goto/16 :goto_3

    .line 1279
    :cond_10
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    iget v6, v6, Lorg/apache/http/impl/client/MultiSocketInputStream;->mDestIPVer:I

    invoke-static {v6}, Lorg/apache/http/impl/client/CustomHttpClient;->bBothInfAvail(I)Z

    move-result v6

    if-eqz v6, :cond_3f

    .line 1280
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockSize:I
    invoke-static {v6}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$900(Lorg/apache/http/impl/client/MultiSocketInputStream;)I

    move-result v6

    mul-int/lit8 v6, v6, 0x3

    div-int/lit8 v6, v6, 0x2

    int-to-long v0, v6

    move-wide/from16 v72, v0

    .line 1281
    .local v72, "maxTailLen":J
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBytesRemaining:J
    invoke-static {v6}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$700(Lorg/apache/http/impl/client/MultiSocketInputStream;)J

    move-result-wide v6

    const-wide/16 v10, 0x2

    div-long v86, v6, v10

    .line 1282
    .local v86, "sizeForExtremeCase":J
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    iget-object v6, v6, Lorg/apache/http/impl/client/MultiSocketInputStream;->customHttpClient:Lorg/apache/http/impl/client/CustomHttpClient;

    const/4 v7, -0x1

    invoke-virtual {v6, v7}, Lorg/apache/http/impl/client/CustomHttpClient;->getSmartBondingData(I)[J

    move-result-object v52

    .line 1283
    .local v52, "highThroughPutInterface":[J
    const-wide/16 v80, 0x64

    .line 1284
    .local v80, "ra":J
    const-wide/16 v60, 0x0

    .line 1285
    .local v60, "inf":J
    if-eqz v52, :cond_12

    move-object/from16 v0, v52

    array-length v6, v0

    const/4 v7, 0x2

    if-lt v6, v7, :cond_12

    .line 1286
    const/4 v6, 0x1

    aget-wide v80, v52, v6

    .line 1287
    const/4 v6, 0x0

    aget-wide v60, v52, v6

    .line 1288
    const-wide/16 v6, 0x0

    cmp-long v6, v60, v6

    if-ltz v6, :cond_11

    const-wide/16 v6, 0x1

    cmp-long v6, v60, v6

    if-lez v6, :cond_12

    .line 1289
    :cond_11
    const-wide/16 v60, 0x0

    .line 1291
    :cond_12
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    move-wide/from16 v0, v80

    long-to-double v10, v0

    const-wide/high16 v14, 0x4059000000000000L    # 100.0

    div-double/2addr v10, v14

    # setter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mSpeedRatio:D
    invoke-static {v6, v10, v11}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1002(Lorg/apache/http/impl/client/MultiSocketInputStream;D)D

    .line 1292
    sget-boolean v6, Lorg/apache/http/impl/client/MultiSocketInputStream;->ENABLE_EXTREME_CONDITION_THREAD:Z

    if-eqz v6, :cond_13

    .line 1293
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    const-wide v10, 0x3fb999999999999aL    # 0.1

    const-wide/high16 v14, 0x4024000000000000L    # 10.0

    move-object/from16 v0, p0

    iget-object v7, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mSpeedRatio:D
    invoke-static {v7}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1000(Lorg/apache/http/impl/client/MultiSocketInputStream;)D

    move-result-wide v18

    move-wide/from16 v0, v18

    invoke-static {v14, v15, v0, v1}, Ljava/lang/Math;->min(DD)D

    move-result-wide v14

    invoke-static {v10, v11, v14, v15}, Ljava/lang/Math;->max(DD)D

    move-result-wide v10

    # setter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mSpeedRatio:D
    invoke-static {v6, v10, v11}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1002(Lorg/apache/http/impl/client/MultiSocketInputStream;D)D

    .line 1294
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mMainSocketInterface:I
    invoke-static {v6}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$300(Lorg/apache/http/impl/client/MultiSocketInputStream;)I

    move-result v6

    int-to-long v6, v6

    cmp-long v6, v60, v6

    if-nez v6, :cond_16

    .line 1295
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mContentLength:J
    invoke-static {v6}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$400(Lorg/apache/http/impl/client/MultiSocketInputStream;)J

    move-result-wide v6

    long-to-double v6, v6

    move-object/from16 v0, p0

    iget-object v9, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mSpeedRatio:D
    invoke-static {v9}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1000(Lorg/apache/http/impl/client/MultiSocketInputStream;)D

    move-result-wide v10

    const-wide/high16 v14, 0x3ff0000000000000L    # 1.0

    add-double/2addr v10, v14

    move-object/from16 v0, p0

    iget-object v9, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mSpeedRatio:D
    invoke-static {v9}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1000(Lorg/apache/http/impl/client/MultiSocketInputStream;)D

    move-result-wide v14

    sget-wide v18, Lorg/apache/http/impl/client/MultiSocketInputStream;->B_Ratio_BUF_LTE:D

    div-double v14, v14, v18

    sub-double/2addr v10, v14

    div-double/2addr v6, v10

    double-to-long v0, v6

    move-wide/from16 v86, v0

    .line 1299
    :goto_8
    sget v6, Lorg/apache/http/impl/client/MultiSocketInputStream;->MIN_SIZE_FOR_MULTISOCKET:I

    int-to-long v6, v6

    move-object/from16 v0, p0

    iget-object v9, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBytesRemaining:J
    invoke-static {v9}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$700(Lorg/apache/http/impl/client/MultiSocketInputStream;)J

    move-result-wide v10

    sget v9, Lorg/apache/http/impl/client/MultiSocketInputStream;->MIN_SIZE_FOR_MULTISOCKET:I

    int-to-long v14, v9

    sub-long/2addr v10, v14

    move-wide/from16 v0, v86

    invoke-static {v10, v11, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v10

    invoke-static {v6, v7, v10, v11}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v86

    .line 1301
    :cond_13
    sget-boolean v6, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v6, :cond_14

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "before check exetThread, ratio: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mSpeedRatio:D
    invoke-static {v7}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1000(Lorg/apache/http/impl/client/MultiSocketInputStream;)D

    move-result-wide v10

    invoke-virtual {v6, v10, v11}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", taillen: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBytesForMultiSocket:J
    invoke-static {v7}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1100(Lorg/apache/http/impl/client/MultiSocketInputStream;)J

    move-result-wide v10

    invoke-virtual {v6, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", maxTailLen: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-wide/from16 v0, v72

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", total: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mContentLength:J
    invoke-static {v7}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$400(Lorg/apache/http/impl/client/MultiSocketInputStream;)J

    move-result-wide v10

    invoke-virtual {v6, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", oriOffset: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mOffset:J
    invoke-static {v7}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1200(Lorg/apache/http/impl/client/MultiSocketInputStream;)J

    move-result-wide v10

    invoke-virtual {v6, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", B_Ratio_BUF_LTE: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-wide v10, Lorg/apache/http/impl/client/MultiSocketInputStream;->B_Ratio_BUF_LTE:D

    invoke-virtual {v6, v10, v11}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 1304
    :cond_14
    sget v6, Lorg/apache/http/impl/client/MultiSocketInputStream;->MIN_SIZE_FOR_MULTISOCKET:I

    int-to-long v6, v6

    cmp-long v6, v86, v6

    if-gez v6, :cond_17

    .line 1305
    sget-boolean v6, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v6, :cond_15

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "tail length is too small, exit watchdog "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-wide/from16 v0, v86

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 1306
    :cond_15
    monitor-exit v8

    goto/16 :goto_3

    .line 1297
    :cond_16
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mContentLength:J
    invoke-static {v6}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$400(Lorg/apache/http/impl/client/MultiSocketInputStream;)J

    move-result-wide v6

    long-to-double v6, v6

    move-object/from16 v0, p0

    iget-object v9, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mSpeedRatio:D
    invoke-static {v9}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1000(Lorg/apache/http/impl/client/MultiSocketInputStream;)D

    move-result-wide v10

    mul-double/2addr v6, v10

    move-object/from16 v0, p0

    iget-object v9, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mSpeedRatio:D
    invoke-static {v9}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1000(Lorg/apache/http/impl/client/MultiSocketInputStream;)D

    move-result-wide v10

    const-wide/high16 v14, 0x3ff0000000000000L    # 1.0

    add-double/2addr v10, v14

    const-wide/high16 v14, 0x3ff0000000000000L    # 1.0

    sget-wide v18, Lorg/apache/http/impl/client/MultiSocketInputStream;->B_Ratio_BUF_LTE:D

    div-double v14, v14, v18

    sub-double/2addr v10, v14

    div-double/2addr v6, v10

    double-to-long v0, v6

    move-wide/from16 v86, v0

    goto/16 :goto_8

    .line 1308
    :cond_17
    sget-boolean v6, Lorg/apache/http/impl/client/MultiSocketInputStream;->ENABLE_EXTREME_CONDITION_THREAD:Z

    if-eqz v6, :cond_1c

    cmp-long v6, v86, v66

    if-gez v6, :cond_1c

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mAlgoType:I
    invoke-static {v6}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1300(Lorg/apache/http/impl/client/MultiSocketInputStream;)I

    move-result v6

    if-nez v6, :cond_1c

    .line 1311
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    move-wide/from16 v0, v66

    move-wide/from16 v2, v86

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v10

    # setter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBytesForMultiSocket:J
    invoke-static {v6, v10, v11}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1102(Lorg/apache/http/impl/client/MultiSocketInputStream;J)J

    .line 1313
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mOffset:J
    invoke-static {v6}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1200(Lorg/apache/http/impl/client/MultiSocketInputStream;)J

    move-result-wide v6

    move-object/from16 v0, p0

    iget-object v9, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mContentLength:J
    invoke-static {v9}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$400(Lorg/apache/http/impl/client/MultiSocketInputStream;)J

    move-result-wide v10

    add-long/2addr v6, v10

    move-object/from16 v0, p0

    iget-object v9, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBytesForMultiSocket:J
    invoke-static {v9}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1100(Lorg/apache/http/impl/client/MultiSocketInputStream;)J

    move-result-wide v10

    sub-long v16, v6, v10

    .line 1314
    .local v16, "start":J
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mOffset:J
    invoke-static {v6}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1200(Lorg/apache/http/impl/client/MultiSocketInputStream;)J

    move-result-wide v6

    move-object/from16 v0, p0

    iget-object v9, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mContentLength:J
    invoke-static {v9}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$400(Lorg/apache/http/impl/client/MultiSocketInputStream;)J

    move-result-wide v10

    add-long/2addr v6, v10

    const-wide/16 v10, 0x1

    sub-long v24, v6, v10

    .line 1315
    .local v24, "end":J
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mMainSocketInterface:I
    invoke-static {v6}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$300(Lorg/apache/http/impl/client/MultiSocketInputStream;)I

    move-result v6

    add-int/lit8 v6, v6, 0x1

    rem-int/lit8 v21, v6, 0x2

    .line 1317
    .local v21, "otherInterface":I
    sget-boolean v6, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v6, :cond_18

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "start exetThread, interface: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, v21

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " ratio: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mSpeedRatio:D
    invoke-static {v7}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1000(Lorg/apache/http/impl/client/MultiSocketInputStream;)D

    move-result-wide v10

    invoke-virtual {v6, v10, v11}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", taillen: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBytesForMultiSocket:J
    invoke-static {v7}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1100(Lorg/apache/http/impl/client/MultiSocketInputStream;)J

    move-result-wide v10

    invoke-virtual {v6, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-wide/from16 v0, v16

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "-"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-wide/from16 v0, v24

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", total: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mContentLength:J
    invoke-static {v7}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$400(Lorg/apache/http/impl/client/MultiSocketInputStream;)J

    move-result-wide v10

    invoke-virtual {v6, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", oriOffset: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mOffset:J
    invoke-static {v7}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1200(Lorg/apache/http/impl/client/MultiSocketInputStream;)J

    move-result-wide v10

    invoke-virtual {v6, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 1320
    :cond_18
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    new-instance v14, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;

    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    const/16 v18, 0x0

    sub-long v10, v24, v16

    const-wide/16 v22, 0x1

    add-long v10, v10, v22

    long-to-int v0, v10

    move/from16 v19, v0

    new-instance v20, Ljava/util/LinkedList;

    invoke-direct/range {v20 .. v20}, Ljava/util/LinkedList;-><init>()V

    const/16 v22, 0x1

    invoke-direct/range {v14 .. v22}, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;-><init>(Lorg/apache/http/impl/client/MultiSocketInputStream;JIILjava/util/LinkedList;II)V

    # setter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mExtDBuf:Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;
    invoke-static {v6, v14}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1402(Lorg/apache/http/impl/client/MultiSocketInputStream;Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;)Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;

    .line 1321
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mInBuffer:Ljava/util/HashMap;
    invoke-static {v6}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1500(Lorg/apache/http/impl/client/MultiSocketInputStream;)Ljava/util/HashMap;

    move-result-object v6

    new-instance v7, Ljava/lang/Integer;

    const/4 v9, 0x0

    invoke-direct {v7, v9}, Ljava/lang/Integer;-><init>(I)V

    move-object/from16 v0, p0

    iget-object v9, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mExtDBuf:Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;
    invoke-static {v9}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1400(Lorg/apache/http/impl/client/MultiSocketInputStream;)Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;

    move-result-object v9

    invoke-virtual {v6, v7, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1322
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    const/4 v7, 0x1

    new-array v7, v7, [B

    # setter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockStatus:[B
    invoke-static {v6, v7}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1602(Lorg/apache/http/impl/client/MultiSocketInputStream;[B)[B

    .line 1323
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockStatus:[B
    invoke-static {v6}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1600(Lorg/apache/http/impl/client/MultiSocketInputStream;)[B

    move-result-object v6

    const/4 v7, 0x0

    const/4 v9, 0x1

    aput-byte v9, v6, v7

    .line 1324
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    new-instance v19, Lorg/apache/http/impl/client/MultiSocketInputStream$ExtremeConditionThread;

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v7, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mExtDBuf:Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;
    invoke-static {v7}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1400(Lorg/apache/http/impl/client/MultiSocketInputStream;)Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;

    move-result-object v26

    const/16 v27, 0x0

    move-wide/from16 v22, v16

    invoke-direct/range {v19 .. v27}, Lorg/apache/http/impl/client/MultiSocketInputStream$ExtremeConditionThread;-><init>(Lorg/apache/http/impl/client/MultiSocketInputStream;IJJLorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;Z)V

    move-object/from16 v0, v19

    # setter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mExtThread:Lorg/apache/http/impl/client/MultiSocketInputStream$ExtremeConditionThread;
    invoke-static {v6, v0}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1702(Lorg/apache/http/impl/client/MultiSocketInputStream;Lorg/apache/http/impl/client/MultiSocketInputStream$ExtremeConditionThread;)Lorg/apache/http/impl/client/MultiSocketInputStream$ExtremeConditionThread;

    .line 1325
    new-instance v85, Ljava/lang/Thread;

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mExtThread:Lorg/apache/http/impl/client/MultiSocketInputStream$ExtremeConditionThread;
    invoke-static {v6}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1700(Lorg/apache/http/impl/client/MultiSocketInputStream;)Lorg/apache/http/impl/client/MultiSocketInputStream$ExtremeConditionThread;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v9, ":ExtremeCondition_"

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    # operator++ for: Lorg/apache/http/impl/client/MultiSocketInputStream;->sThreadID:I
    invoke-static {}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1808()I

    move-result v9

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, v85

    invoke-direct {v0, v6, v7}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    .line 1328
    .local v85, "t":Ljava/lang/Thread;
    sget-boolean v6, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v6, :cond_19

    .line 1329
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "resource check: finish reading startup data "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    iget-wide v10, v7, Lorg/apache/http/impl/client/MultiSocketInputStream;->mOriOffset:J

    invoke-virtual {v6, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "-"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    iget-wide v10, v7, Lorg/apache/http/impl/client/MultiSocketInputStream;->mOriOffset:J

    move-object/from16 v0, p0

    iget-object v7, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mContentLength:J
    invoke-static {v7}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$400(Lorg/apache/http/impl/client/MultiSocketInputStream;)J

    move-result-wide v14

    add-long/2addr v10, v14

    move-object/from16 v0, p0

    iget-object v7, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBytesRemaining:J
    invoke-static {v7}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$700(Lorg/apache/http/impl/client/MultiSocketInputStream;)J

    move-result-wide v14

    sub-long/2addr v10, v14

    const-wide/16 v14, 0x1

    sub-long/2addr v10, v14

    invoke-virtual {v6, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " with length "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mContentLength:J
    invoke-static {v7}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$400(Lorg/apache/http/impl/client/MultiSocketInputStream;)J

    move-result-wide v10

    move-object/from16 v0, p0

    iget-object v7, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBytesRemaining:J
    invoke-static {v7}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$700(Lorg/apache/http/impl/client/MultiSocketInputStream;)J

    move-result-wide v14

    sub-long/2addr v10, v14

    invoke-virtual {v6, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", now ExtremeThread created: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " Main socket read "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    iget-wide v10, v7, Lorg/apache/http/impl/client/MultiSocketInputStream;->mOriOffset:J

    move-object/from16 v0, p0

    iget-object v7, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mContentLength:J
    invoke-static {v7}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$400(Lorg/apache/http/impl/client/MultiSocketInputStream;)J

    move-result-wide v14

    add-long/2addr v10, v14

    move-object/from16 v0, p0

    iget-object v7, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBytesRemaining:J
    invoke-static {v7}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$700(Lorg/apache/http/impl/client/MultiSocketInputStream;)J

    move-result-wide v14

    sub-long/2addr v10, v14

    invoke-virtual {v6, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "-"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-wide/16 v10, 0x1

    sub-long v10, v16, v10

    invoke-virtual {v6, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", ExtremeThread read from "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-wide/from16 v0, v16

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "-"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-wide/from16 v0, v24

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 1333
    :cond_19
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    const/4 v7, 0x1

    # setter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->bExtremThreadStarted:Z
    invoke-static {v6, v7}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1902(Lorg/apache/http/impl/client/MultiSocketInputStream;Z)Z

    .line 1334
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    const/4 v7, 0x1

    # setter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->bExtremThreadExisted:Z
    invoke-static {v6, v7}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$2002(Lorg/apache/http/impl/client/MultiSocketInputStream;Z)Z

    .line 1338
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mMainManagedConn:Lorg/apache/http/conn/ManagedClientConnection;
    invoke-static {v6}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$2100(Lorg/apache/http/impl/client/MultiSocketInputStream;)Lorg/apache/http/conn/ManagedClientConnection;

    move-result-object v6

    if-eqz v6, :cond_1a

    .line 1339
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mMainManagedConn:Lorg/apache/http/conn/ManagedClientConnection;
    invoke-static {v6}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$2100(Lorg/apache/http/impl/client/MultiSocketInputStream;)Lorg/apache/http/conn/ManagedClientConnection;

    move-result-object v6

    const/16 v7, 0x1388

    invoke-interface {v6, v7}, Lorg/apache/http/conn/ManagedClientConnection;->setSocketTimeout(I)V

    .line 1341
    :cond_1a
    invoke-virtual/range {v85 .. v85}, Ljava/lang/Thread;->start()V

    .line 1610
    .end local v16    # "start":J
    .end local v21    # "otherInterface":I
    .end local v24    # "end":J
    .end local v85    # "t":Ljava/lang/Thread;
    :cond_1b
    :goto_9
    monitor-exit v8

    goto/16 :goto_3

    .line 1344
    :cond_1c
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    move-object/from16 v0, p0

    iget-object v7, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBytesRemaining:J
    invoke-static {v7}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$700(Lorg/apache/http/impl/client/MultiSocketInputStream;)J

    move-result-wide v10

    # setter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->bytesA:J
    invoke-static {v6, v10, v11}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$2202(Lorg/apache/http/impl/client/MultiSocketInputStream;J)J

    .line 1345
    const-wide/16 v44, 0x0

    .line 1347
    .local v44, "currentConnectTime":J
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mRangeRequestSuccess:I
    invoke-static {v6}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$2300(Lorg/apache/http/impl/client/MultiSocketInputStream;)I

    move-result v6

    const/4 v7, 0x1

    if-eq v6, v7, :cond_23

    .line 1348
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mHttpRequest:[Lorg/apache/http/HttpRequest;
    invoke-static {v6}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$2400(Lorg/apache/http/impl/client/MultiSocketInputStream;)[Lorg/apache/http/HttpRequest;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mOtherInterface:I
    invoke-static {v7}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$2500(Lorg/apache/http/impl/client/MultiSocketInputStream;)I

    move-result v7

    aget-object v6, v6, v7

    invoke-virtual {v5, v6}, Lorg/apache/http/impl/client/CustomHttpClient;->createClientRequestDirector(Lorg/apache/http/HttpRequest;)Lorg/apache/http/impl/client/DefaultRequestDirector;
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    move-result-object v26

    .line 1355
    .local v26, "defReqDirector":Lorg/apache/http/impl/client/DefaultRequestDirector;
    :try_start_8
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mOtherInterface:I
    invoke-static {v6}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$2500(Lorg/apache/http/impl/client/MultiSocketInputStream;)I

    move-result v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    iget v7, v7, Lorg/apache/http/impl/client/MultiSocketInputStream;->mDestIPVer:I

    invoke-static {v6, v7}, Lorg/apache/http/impl/client/CustomHttpClient;->getLocalAddrEx(II)Ljava/net/InetAddress;

    move-result-object v34

    .line 1356
    .local v34, "localAddr":Ljava/net/InetAddress;
    sget-boolean v6, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v6, :cond_1d

    .line 1357
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Got local interface ip address : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    if-eqz v34, :cond_25

    invoke-virtual/range {v34 .. v34}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v6

    :goto_a
    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 1375
    :cond_1d
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mHttpRequest:[Lorg/apache/http/HttpRequest;
    invoke-static {v6}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$2400(Lorg/apache/http/impl/client/MultiSocketInputStream;)[Lorg/apache/http/HttpRequest;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mOtherInterface:I
    invoke-static {v7}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$2500(Lorg/apache/http/impl/client/MultiSocketInputStream;)I

    move-result v7

    aget-object v6, v6, v7

    invoke-interface {v6}, Lorg/apache/http/HttpRequest;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v6

    const-string v7, "http.route.default-proxy"

    move-object/from16 v0, p0

    iget-object v9, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mOtherInterface:I
    invoke-static {v9}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$2500(Lorg/apache/http/impl/client/MultiSocketInputStream;)I

    move-result v9

    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mHttpHost:[Lorg/apache/http/HttpHost;
    invoke-static {v10}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$200(Lorg/apache/http/impl/client/MultiSocketInputStream;)[Lorg/apache/http/HttpHost;

    move-result-object v10

    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mOtherInterface:I
    invoke-static {v11}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$2500(Lorg/apache/http/impl/client/MultiSocketInputStream;)I

    move-result v11

    aget-object v10, v10, v11

    invoke-virtual {v5, v9, v10}, Lorg/apache/http/impl/client/CustomHttpClient;->getProxy(ILorg/apache/http/HttpHost;)Lorg/apache/http/HttpHost;

    move-result-object v9

    move-object/from16 v0, v26

    invoke-virtual {v0, v6, v7, v9}, Lorg/apache/http/impl/client/DefaultRequestDirector;->setParameter(Lorg/apache/http/params/HttpParams;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1379
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mHttpHost:[Lorg/apache/http/HttpHost;
    invoke-static {v6}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$200(Lorg/apache/http/impl/client/MultiSocketInputStream;)[Lorg/apache/http/HttpHost;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mOtherInterface:I
    invoke-static {v7}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$2500(Lorg/apache/http/impl/client/MultiSocketInputStream;)I

    move-result v7

    aget-object v27, v6, v7

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mHttpRequest:[Lorg/apache/http/HttpRequest;
    invoke-static {v6}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$2400(Lorg/apache/http/impl/client/MultiSocketInputStream;)[Lorg/apache/http/HttpRequest;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mOtherInterface:I
    invoke-static {v7}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$2500(Lorg/apache/http/impl/client/MultiSocketInputStream;)I

    move-result v7

    aget-object v28, v6, v7

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mHttpContext:Lorg/apache/http/protocol/HttpContext;
    invoke-static {v6}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$100(Lorg/apache/http/impl/client/MultiSocketInputStream;)Lorg/apache/http/protocol/HttpContext;

    move-result-object v29

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    iget-wide v0, v6, Lorg/apache/http/impl/client/MultiSocketInputStream;->mOriOffset:J

    move-wide/from16 v30, v0

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    iget-wide v6, v6, Lorg/apache/http/impl/client/MultiSocketInputStream;->mOriOffset:J

    const-wide/16 v10, 0x1

    add-long v32, v6, v10

    invoke-virtual/range {v26 .. v34}, Lorg/apache/http/impl/client/DefaultRequestDirector;->execute(Lorg/apache/http/HttpHost;Lorg/apache/http/HttpRequest;Lorg/apache/http/protocol/HttpContext;JJLjava/net/InetAddress;)Lorg/apache/http/HttpResponse;

    move-result-object v83

    .line 1382
    .local v83, "response":Lorg/apache/http/HttpResponse;
    const-wide/16 v42, 0x0

    .line 1383
    .local v42, "contentLengthRangeRequest":J
    invoke-interface/range {v83 .. v83}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v53

    .line 1385
    .local v53, "httpEntity":Lorg/apache/http/HttpEntity;
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    move-object/from16 v0, p0

    iget-object v7, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBytesRemaining:J
    invoke-static {v7}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$700(Lorg/apache/http/impl/client/MultiSocketInputStream;)J

    move-result-wide v10

    # setter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->bytesB:J
    invoke-static {v6, v10, v11}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$2602(Lorg/apache/http/impl/client/MultiSocketInputStream;J)J

    .line 1386
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    move-object/from16 v0, p0

    iget-object v7, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->bytesA:J
    invoke-static {v7}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$2200(Lorg/apache/http/impl/client/MultiSocketInputStream;)J

    move-result-wide v10

    move-object/from16 v0, p0

    iget-object v7, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->bytesB:J
    invoke-static {v7}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$2600(Lorg/apache/http/impl/client/MultiSocketInputStream;)J

    move-result-wide v14

    sub-long/2addr v10, v14

    # setter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->bytesC:J
    invoke-static {v6, v10, v11}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$2702(Lorg/apache/http/impl/client/MultiSocketInputStream;J)J

    .line 1388
    const-string v6, "Content-Range"

    move-object/from16 v0, v83

    invoke-interface {v0, v6}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v82

    .line 1389
    .local v82, "rangeRequestTotalContentLength":Lorg/apache/http/Header;
    if-eqz v82, :cond_27

    .line 1390
    invoke-interface/range {v82 .. v82}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v35

    .line 1391
    .local v35, "byteRange":Ljava/lang/String;
    const-string v6, "/"

    move-object/from16 v0, v35

    invoke-virtual {v0, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v58

    .line 1393
    .local v58, "indexOfSlash":I
    const/4 v6, -0x1

    move/from16 v0, v58

    if-eq v0, v6, :cond_26

    .line 1395
    add-int/lit8 v6, v58, 0x1

    invoke-virtual/range {v35 .. v35}, Ljava/lang/String;->length()I

    move-result v7

    move-object/from16 v0, v35

    invoke-virtual {v0, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v42

    .line 1411
    .end local v35    # "byteRange":Ljava/lang/String;
    .end local v58    # "indexOfSlash":I
    :goto_b
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mContentLengthRangeRequest:J
    invoke-static {v6}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$2800(Lorg/apache/http/impl/client/MultiSocketInputStream;)J

    move-result-wide v6

    cmp-long v6, v42, v6

    if-eqz v6, :cond_28

    .line 1412
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mRemoteAddress:[Ljava/net/InetAddress;
    invoke-static {v6}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$2900(Lorg/apache/http/impl/client/MultiSocketInputStream;)[Ljava/net/InetAddress;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mOtherInterface:I
    invoke-static {v7}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$2500(Lorg/apache/http/impl/client/MultiSocketInputStream;)I

    move-result v7

    move-object/from16 v0, p0

    iget-object v9, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mRemoteAddress:[Ljava/net/InetAddress;
    invoke-static {v9}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$2900(Lorg/apache/http/impl/client/MultiSocketInputStream;)[Ljava/net/InetAddress;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mMainSocketInterface:I
    invoke-static {v10}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$300(Lorg/apache/http/impl/client/MultiSocketInputStream;)I

    move-result v10

    aget-object v9, v9, v10

    aput-object v9, v6, v7

    .line 1413
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mHttpRequest:[Lorg/apache/http/HttpRequest;
    invoke-static {v6}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$2400(Lorg/apache/http/impl/client/MultiSocketInputStream;)[Lorg/apache/http/HttpRequest;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mOtherInterface:I
    invoke-static {v7}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$2500(Lorg/apache/http/impl/client/MultiSocketInputStream;)I

    move-result v7

    move-object/from16 v0, p0

    iget-object v9, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mHttpRequest:[Lorg/apache/http/HttpRequest;
    invoke-static {v9}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$2400(Lorg/apache/http/impl/client/MultiSocketInputStream;)[Lorg/apache/http/HttpRequest;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mMainSocketInterface:I
    invoke-static {v10}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$300(Lorg/apache/http/impl/client/MultiSocketInputStream;)I

    move-result v10

    aget-object v9, v9, v10

    aput-object v9, v6, v7

    .line 1414
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mHttpHost:[Lorg/apache/http/HttpHost;
    invoke-static {v6}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$200(Lorg/apache/http/impl/client/MultiSocketInputStream;)[Lorg/apache/http/HttpHost;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mOtherInterface:I
    invoke-static {v7}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$2500(Lorg/apache/http/impl/client/MultiSocketInputStream;)I

    move-result v7

    move-object/from16 v0, p0

    iget-object v9, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mHttpHost:[Lorg/apache/http/HttpHost;
    invoke-static {v9}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$200(Lorg/apache/http/impl/client/MultiSocketInputStream;)[Lorg/apache/http/HttpHost;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mMainSocketInterface:I
    invoke-static {v10}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$300(Lorg/apache/http/impl/client/MultiSocketInputStream;)I

    move-result v10

    aget-object v9, v9, v10

    aput-object v9, v6, v7

    .line 1420
    :goto_c
    sget-boolean v6, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v6, :cond_1e

    .line 1421
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "The HttpTarget after sample request is :"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mHttpHost:[Lorg/apache/http/HttpHost;
    invoke-static {v7}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$200(Lorg/apache/http/impl/client/MultiSocketInputStream;)[Lorg/apache/http/HttpHost;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v9, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mOtherInterface:I
    invoke-static {v9}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$2500(Lorg/apache/http/impl/client/MultiSocketInputStream;)I

    move-result v9

    aget-object v7, v7, v9

    invoke-virtual {v7}, Lorg/apache/http/HttpHost;->getHostName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ":"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mHttpHost:[Lorg/apache/http/HttpHost;
    invoke-static {v7}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$200(Lorg/apache/http/impl/client/MultiSocketInputStream;)[Lorg/apache/http/HttpHost;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v9, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mOtherInterface:I
    invoke-static {v9}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$2500(Lorg/apache/http/impl/client/MultiSocketInputStream;)I

    move-result v9

    aget-object v7, v7, v9

    invoke-virtual {v7}, Lorg/apache/http/HttpHost;->getPort()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 1425
    :cond_1e
    invoke-interface/range {v83 .. v83}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v6

    invoke-interface {v6}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v84

    .line 1427
    .local v84, "rspCode":I
    invoke-interface/range {v83 .. v83}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v50

    .line 1429
    .local v50, "entity":Lorg/apache/http/HttpEntity;
    const/16 v6, 0xce

    move/from16 v0, v84

    if-eq v0, v6, :cond_1f

    const/16 v6, 0xc8

    move/from16 v0, v84

    if-ne v0, v6, :cond_20

    :cond_1f
    invoke-interface/range {v50 .. v50}, Lorg/apache/http/HttpEntity;->getContentLength()J

    move-result-wide v6

    const-wide/16 v10, 0x2

    cmp-long v6, v6, v10

    if-eqz v6, :cond_2a

    .line 1431
    :cond_20
    sget-boolean v6, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v6, :cond_21

    .line 1432
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Server Response not proper, got response "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, v84

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 1435
    :cond_21
    const/16 v59, 0x0

    .line 1436
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    const/4 v7, 0x1

    # setter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mRangeRequestSuccess:I
    invoke-static {v6, v7}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$2302(Lorg/apache/http/impl/client/MultiSocketInputStream;I)I

    .line 1437
    sget-boolean v6, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v6, :cond_22

    .line 1438
    const-string v6, "Interface Watchdog thread will exit and Range Request thread will not start"

    invoke-static {v6}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 1455
    :cond_22
    :goto_d
    invoke-virtual/range {v26 .. v26}, Lorg/apache/http/impl/client/DefaultRequestDirector;->getConnectionTime()J
    :try_end_8
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_3

    move-result-wide v44

    .line 1465
    :try_start_9
    move-object/from16 v0, v26

    iget-object v6, v0, Lorg/apache/http/impl/client/DefaultRequestDirector;->managedConn:Lorg/apache/http/conn/ManagedClientConnection;

    if-eqz v6, :cond_23

    move-object/from16 v0, v26

    iget-object v6, v0, Lorg/apache/http/impl/client/DefaultRequestDirector;->managedConn:Lorg/apache/http/conn/ManagedClientConnection;

    invoke-interface {v6}, Lorg/apache/http/conn/ManagedClientConnection;->isOpen()Z

    move-result v6

    if-eqz v6, :cond_23

    .line 1466
    invoke-virtual/range {v26 .. v26}, Lorg/apache/http/impl/client/DefaultRequestDirector;->releaseConnection()V

    .line 1467
    const/16 v26, 0x0

    .line 1471
    .end local v26    # "defReqDirector":Lorg/apache/http/impl/client/DefaultRequestDirector;
    .end local v34    # "localAddr":Ljava/net/InetAddress;
    .end local v42    # "contentLengthRangeRequest":J
    .end local v50    # "entity":Lorg/apache/http/HttpEntity;
    .end local v53    # "httpEntity":Lorg/apache/http/HttpEntity;
    .end local v82    # "rangeRequestTotalContentLength":Lorg/apache/http/Header;
    .end local v83    # "response":Lorg/apache/http/HttpResponse;
    .end local v84    # "rspCode":I
    :cond_23
    :goto_e
    if-nez v59, :cond_2d

    .line 1472
    sget-boolean v6, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v6, :cond_24

    .line 1473
    const-string v6, "range request not supported or exception while connection, exit watchdog"

    invoke-static {v6}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 1477
    :cond_24
    monitor-exit v8
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    goto/16 :goto_3

    .line 1357
    .restart local v26    # "defReqDirector":Lorg/apache/http/impl/client/DefaultRequestDirector;
    .restart local v34    # "localAddr":Ljava/net/InetAddress;
    :cond_25
    :try_start_a
    const-string v6, "null"

    goto/16 :goto_a

    .line 1402
    .restart local v35    # "byteRange":Ljava/lang/String;
    .restart local v42    # "contentLengthRangeRequest":J
    .restart local v53    # "httpEntity":Lorg/apache/http/HttpEntity;
    .restart local v58    # "indexOfSlash":I
    .restart local v82    # "rangeRequestTotalContentLength":Lorg/apache/http/Header;
    .restart local v83    # "response":Lorg/apache/http/HttpResponse;
    :cond_26
    invoke-interface/range {v53 .. v53}, Lorg/apache/http/HttpEntity;->getContentLength()J

    move-result-wide v6

    move-object/from16 v0, p0

    iget-object v9, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    iget-wide v10, v9, Lorg/apache/http/impl/client/MultiSocketInputStream;->mOriOffset:J

    add-long v42, v6, v10

    goto/16 :goto_b

    .line 1407
    .end local v35    # "byteRange":Ljava/lang/String;
    .end local v58    # "indexOfSlash":I
    :cond_27
    invoke-interface/range {v53 .. v53}, Lorg/apache/http/HttpEntity;->getContentLength()J

    move-result-wide v42

    goto/16 :goto_b

    .line 1416
    :cond_28
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mRemoteAddress:[Ljava/net/InetAddress;
    invoke-static {v6}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$2900(Lorg/apache/http/impl/client/MultiSocketInputStream;)[Ljava/net/InetAddress;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mOtherInterface:I
    invoke-static {v7}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$2500(Lorg/apache/http/impl/client/MultiSocketInputStream;)I

    move-result v7

    move-object/from16 v0, v26

    iget-object v9, v0, Lorg/apache/http/impl/client/DefaultRequestDirector;->managedConn:Lorg/apache/http/conn/ManagedClientConnection;

    invoke-interface {v9}, Lorg/apache/http/conn/ManagedClientConnection;->getRemoteAddress()Ljava/net/InetAddress;

    move-result-object v9

    aput-object v9, v6, v7

    .line 1417
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mHttpRequest:[Lorg/apache/http/HttpRequest;
    invoke-static {v6}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$2400(Lorg/apache/http/impl/client/MultiSocketInputStream;)[Lorg/apache/http/HttpRequest;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mOtherInterface:I
    invoke-static {v7}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$2500(Lorg/apache/http/impl/client/MultiSocketInputStream;)I

    move-result v7

    invoke-virtual/range {v26 .. v26}, Lorg/apache/http/impl/client/DefaultRequestDirector;->getHttpRequest()Lorg/apache/http/HttpRequest;

    move-result-object v9

    aput-object v9, v6, v7

    .line 1418
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mHttpHost:[Lorg/apache/http/HttpHost;
    invoke-static {v6}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$200(Lorg/apache/http/impl/client/MultiSocketInputStream;)[Lorg/apache/http/HttpHost;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mOtherInterface:I
    invoke-static {v7}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$2500(Lorg/apache/http/impl/client/MultiSocketInputStream;)I

    move-result v7

    invoke-virtual/range {v26 .. v26}, Lorg/apache/http/impl/client/DefaultRequestDirector;->getHttpHost()Lorg/apache/http/HttpHost;

    move-result-object v9

    aput-object v9, v6, v7
    :try_end_a
    .catch Ljava/lang/Throwable; {:try_start_a .. :try_end_a} :catch_1
    .catchall {:try_start_a .. :try_end_a} :catchall_3

    goto/16 :goto_c

    .line 1456
    .end local v34    # "localAddr":Ljava/net/InetAddress;
    .end local v42    # "contentLengthRangeRequest":J
    .end local v53    # "httpEntity":Lorg/apache/http/HttpEntity;
    .end local v82    # "rangeRequestTotalContentLength":Lorg/apache/http/Header;
    .end local v83    # "response":Lorg/apache/http/HttpResponse;
    :catch_1
    move-exception v49

    .line 1457
    .local v49, "e1":Ljava/lang/Throwable;
    :try_start_b
    sget-boolean v6, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v6, :cond_29

    .line 1458
    invoke-static/range {v49 .. v49}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/Throwable;)V

    .line 1461
    :cond_29
    const/16 v59, 0x0

    .line 1462
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    const/4 v7, 0x1

    # setter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mRangeRequestSuccess:I
    invoke-static {v6, v7}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$2302(Lorg/apache/http/impl/client/MultiSocketInputStream;I)I
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_3

    .line 1465
    :try_start_c
    move-object/from16 v0, v26

    iget-object v6, v0, Lorg/apache/http/impl/client/DefaultRequestDirector;->managedConn:Lorg/apache/http/conn/ManagedClientConnection;

    if-eqz v6, :cond_23

    move-object/from16 v0, v26

    iget-object v6, v0, Lorg/apache/http/impl/client/DefaultRequestDirector;->managedConn:Lorg/apache/http/conn/ManagedClientConnection;

    invoke-interface {v6}, Lorg/apache/http/conn/ManagedClientConnection;->isOpen()Z

    move-result v6

    if-eqz v6, :cond_23

    .line 1466
    invoke-virtual/range {v26 .. v26}, Lorg/apache/http/impl/client/DefaultRequestDirector;->releaseConnection()V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_2

    .line 1467
    const/16 v26, 0x0

    goto/16 :goto_e

    .line 1442
    .end local v49    # "e1":Ljava/lang/Throwable;
    .restart local v34    # "localAddr":Ljava/net/InetAddress;
    .restart local v42    # "contentLengthRangeRequest":J
    .restart local v50    # "entity":Lorg/apache/http/HttpEntity;
    .restart local v53    # "httpEntity":Lorg/apache/http/HttpEntity;
    .restart local v82    # "rangeRequestTotalContentLength":Lorg/apache/http/Header;
    .restart local v83    # "response":Lorg/apache/http/HttpResponse;
    .restart local v84    # "rspCode":I
    :cond_2a
    const/16 v59, 0x1

    .line 1444
    :try_start_d
    const-string v6, "Connection"

    move-object/from16 v0, v83

    invoke-interface {v0, v6}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v40

    .line 1446
    .local v40, "connectionCloseHeader":Lorg/apache/http/Header;
    move-object/from16 v0, p0

    iget-object v7, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    if-nez v40, :cond_2c

    const/4 v6, 0x0

    :goto_f
    # setter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mIsConnectionClosedAfterEveryRequest:Z
    invoke-static {v7, v6}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$3002(Lorg/apache/http/impl/client/MultiSocketInputStream;Z)Z

    .line 1449
    sget-boolean v6, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v6, :cond_22

    .line 1450
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "InterfaceWatchdog: Reconnect = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mIsConnectionClosedAfterEveryRequest:Z
    invoke-static {v7}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$3000(Lorg/apache/http/impl/client/MultiSocketInputStream;)Z

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V
    :try_end_d
    .catch Ljava/lang/Throwable; {:try_start_d .. :try_end_d} :catch_1
    .catchall {:try_start_d .. :try_end_d} :catchall_3

    goto/16 :goto_d

    .line 1465
    .end local v34    # "localAddr":Ljava/net/InetAddress;
    .end local v40    # "connectionCloseHeader":Lorg/apache/http/Header;
    .end local v42    # "contentLengthRangeRequest":J
    .end local v50    # "entity":Lorg/apache/http/HttpEntity;
    .end local v53    # "httpEntity":Lorg/apache/http/HttpEntity;
    .end local v82    # "rangeRequestTotalContentLength":Lorg/apache/http/Header;
    .end local v83    # "response":Lorg/apache/http/HttpResponse;
    .end local v84    # "rspCode":I
    :catchall_3
    move-exception v6

    :try_start_e
    move-object/from16 v0, v26

    iget-object v7, v0, Lorg/apache/http/impl/client/DefaultRequestDirector;->managedConn:Lorg/apache/http/conn/ManagedClientConnection;

    if-eqz v7, :cond_2b

    move-object/from16 v0, v26

    iget-object v7, v0, Lorg/apache/http/impl/client/DefaultRequestDirector;->managedConn:Lorg/apache/http/conn/ManagedClientConnection;

    invoke-interface {v7}, Lorg/apache/http/conn/ManagedClientConnection;->isOpen()Z

    move-result v7

    if-eqz v7, :cond_2b

    .line 1466
    invoke-virtual/range {v26 .. v26}, Lorg/apache/http/impl/client/DefaultRequestDirector;->releaseConnection()V

    .line 1467
    const/16 v26, 0x0

    :cond_2b
    throw v6
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_2

    .line 1446
    .restart local v34    # "localAddr":Ljava/net/InetAddress;
    .restart local v40    # "connectionCloseHeader":Lorg/apache/http/Header;
    .restart local v42    # "contentLengthRangeRequest":J
    .restart local v50    # "entity":Lorg/apache/http/HttpEntity;
    .restart local v53    # "httpEntity":Lorg/apache/http/HttpEntity;
    .restart local v82    # "rangeRequestTotalContentLength":Lorg/apache/http/Header;
    .restart local v83    # "response":Lorg/apache/http/HttpResponse;
    .restart local v84    # "rspCode":I
    :cond_2c
    :try_start_f
    invoke-interface/range {v40 .. v40}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v6

    const-string v9, "Close"

    invoke-virtual {v6, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z
    :try_end_f
    .catch Ljava/lang/Throwable; {:try_start_f .. :try_end_f} :catch_1
    .catchall {:try_start_f .. :try_end_f} :catchall_3

    move-result v6

    goto :goto_f

    .line 1481
    .end local v26    # "defReqDirector":Lorg/apache/http/impl/client/DefaultRequestDirector;
    .end local v34    # "localAddr":Ljava/net/InetAddress;
    .end local v40    # "connectionCloseHeader":Lorg/apache/http/Header;
    .end local v42    # "contentLengthRangeRequest":J
    .end local v50    # "entity":Lorg/apache/http/HttpEntity;
    .end local v53    # "httpEntity":Lorg/apache/http/HttpEntity;
    .end local v82    # "rangeRequestTotalContentLength":Lorg/apache/http/Header;
    .end local v83    # "response":Lorg/apache/http/HttpResponse;
    .end local v84    # "rspCode":I
    :cond_2d
    :try_start_10
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mMainSocketConnectTime:J
    invoke-static {v6}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$3100(Lorg/apache/http/impl/client/MultiSocketInputStream;)J

    move-result-wide v6

    sget v9, Lorg/apache/http/impl/client/MultiSocketInputStream;->EXTREME_TIME_RATIO:I

    int-to-long v10, v9

    mul-long/2addr v6, v10

    cmp-long v6, v6, v44

    if-gez v6, :cond_2f

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mMainSocketConnectTime:J
    invoke-static {v6}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$3100(Lorg/apache/http/impl/client/MultiSocketInputStream;)J

    move-result-wide v6

    sub-long v6, v44, v6

    sget v9, Lorg/apache/http/impl/client/MultiSocketInputStream;->MIN_DIFF_TIME_FOR_HANDOVER:I

    int-to-long v10, v9

    cmp-long v6, v6, v10

    if-lez v6, :cond_2f

    .line 1483
    sget-boolean v6, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v6, :cond_2e

    .line 1484
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Exiting the interface : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mOtherInterface:I
    invoke-static {v7}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$2500(Lorg/apache/http/impl/client/MultiSocketInputStream;)I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " because interface connect time is slow. mMainSocketConnectTime="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mStartReadTime:[J
    invoke-static {v7}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$3200(Lorg/apache/http/impl/client/MultiSocketInputStream;)[J

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v9, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mMainSocketInterface:I
    invoke-static {v9}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$300(Lorg/apache/http/impl/client/MultiSocketInputStream;)I

    move-result v9

    aget-wide v10, v7, v9

    invoke-virtual {v6, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " currentConnectTime="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-wide/from16 v0, v44

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 1488
    :cond_2e
    monitor-exit v8

    goto/16 :goto_3

    .line 1489
    :cond_2f
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mMainSocketConnectTime:J
    invoke-static {v6}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$3100(Lorg/apache/http/impl/client/MultiSocketInputStream;)J

    move-result-wide v6

    sget v9, Lorg/apache/http/impl/client/MultiSocketInputStream;->EXTREME_TIME_RATIO:I

    int-to-long v10, v9

    mul-long v10, v10, v44

    cmp-long v6, v6, v10

    if-lez v6, :cond_31

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mMainSocketConnectTime:J
    invoke-static {v6}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$3100(Lorg/apache/http/impl/client/MultiSocketInputStream;)J

    move-result-wide v6

    sub-long v6, v6, v44

    sget v9, Lorg/apache/http/impl/client/MultiSocketInputStream;->MIN_DIFF_TIME_FOR_HANDOVER:I

    int-to-long v10, v9

    cmp-long v6, v6, v10

    if-lez v6, :cond_31

    .line 1493
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mMainManagedConn:Lorg/apache/http/conn/ManagedClientConnection;
    invoke-static {v6}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$2100(Lorg/apache/http/impl/client/MultiSocketInputStream;)Lorg/apache/http/conn/ManagedClientConnection;

    move-result-object v6

    invoke-static {v6}, Lorg/apache/http/impl/client/MultiSocketInputStream;->closeConnQuiet(Lorg/apache/http/conn/ManagedClientConnection;)V

    .line 1494
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    const/4 v7, 0x0

    # setter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mMainManagedConn:Lorg/apache/http/conn/ManagedClientConnection;
    invoke-static {v6, v7}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$2102(Lorg/apache/http/impl/client/MultiSocketInputStream;Lorg/apache/http/conn/ManagedClientConnection;)Lorg/apache/http/conn/ManagedClientConnection;

    .line 1495
    sget-boolean v6, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v6, :cond_30

    .line 1496
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Exiting the interface : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mOtherInterface:I
    invoke-static {v7}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$2500(Lorg/apache/http/impl/client/MultiSocketInputStream;)I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " because interface connect time is slow. mMainSocketConnectTime="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mStartReadTime:[J
    invoke-static {v7}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$3200(Lorg/apache/http/impl/client/MultiSocketInputStream;)[J

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v9, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mMainSocketInterface:I
    invoke-static {v9}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$300(Lorg/apache/http/impl/client/MultiSocketInputStream;)I

    move-result v9

    aget-wide v10, v7, v9

    invoke-virtual {v6, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " currentConnectTime="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-wide/from16 v0, v44

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 1500
    :cond_30
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    move-object/from16 v0, p0

    iget-object v7, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mOtherInterface:I
    invoke-static {v7}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$2500(Lorg/apache/http/impl/client/MultiSocketInputStream;)I

    move-result v7

    # setter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mMainSocketAfterHandover:I
    invoke-static {v6, v7}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$3302(Lorg/apache/http/impl/client/MultiSocketInputStream;I)I

    .line 1501
    monitor-exit v8

    goto/16 :goto_3

    .line 1504
    :cond_31
    sget-boolean v6, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v6, :cond_32

    .line 1505
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "both interface connected while bytesRemaining="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBytesRemaining:J
    invoke-static {v7}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$700(Lorg/apache/http/impl/client/MultiSocketInputStream;)J

    move-result-wide v10

    invoke-virtual {v6, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 1510
    :cond_32
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mContentLength:J
    invoke-static {v6}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$400(Lorg/apache/http/impl/client/MultiSocketInputStream;)J

    move-result-wide v6

    move-object/from16 v0, p0

    iget-object v9, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBytesRemaining:J
    invoke-static {v9}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$700(Lorg/apache/http/impl/client/MultiSocketInputStream;)J

    move-result-wide v10

    sub-long/2addr v6, v10

    sget v9, Lorg/apache/http/impl/client/MultiSocketInputStream;->EXPECTSIZE:I

    int-to-long v10, v9

    move-object/from16 v0, p0

    iget-object v9, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->bytesC:J
    invoke-static {v9}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$2700(Lorg/apache/http/impl/client/MultiSocketInputStream;)J

    move-result-wide v14

    invoke-static {v10, v11, v14, v15}, Ljava/lang/Math;->max(JJ)J
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_2

    move-result-wide v10

    add-long v38, v6, v10

    .line 1512
    .local v38, "bytesExpected":J
    :try_start_11
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mMainSocketInputStream:Ljava/io/InputStream;
    invoke-static {v6}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$3400(Lorg/apache/http/impl/client/MultiSocketInputStream;)Ljava/io/InputStream;

    move-result-object v6

    invoke-virtual {v6}, Ljava/io/InputStream;->available()I

    move-result v6

    int-to-long v6, v6

    sget v9, Lorg/apache/http/impl/client/MultiSocketInputStream;->EXPECTSIZE:I

    int-to-long v10, v9

    move-object/from16 v0, p0

    iget-object v9, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->bytesC:J
    invoke-static {v9}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$2700(Lorg/apache/http/impl/client/MultiSocketInputStream;)J

    move-result-wide v14

    invoke-static {v10, v11, v14, v15}, Ljava/lang/Math;->max(JJ)J
    :try_end_11
    .catch Ljava/lang/Throwable; {:try_start_11 .. :try_end_11} :catch_2
    .catchall {:try_start_11 .. :try_end_11} :catchall_2

    move-result-wide v10

    add-long v38, v6, v10

    .line 1520
    :cond_33
    :goto_10
    :try_start_12
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    move-object/from16 v0, p0

    iget-object v7, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBytesRemaining:J
    invoke-static {v7}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$700(Lorg/apache/http/impl/client/MultiSocketInputStream;)J

    move-result-wide v10

    sub-long v10, v10, v38

    # setter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBytesForMultiSocket:J
    invoke-static {v6, v10, v11}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1102(Lorg/apache/http/impl/client/MultiSocketInputStream;J)J

    .line 1525
    sget v97, Lorg/apache/http/impl/client/MultiSocketInputStream;->MIN_MULTIRAT_BLOCK_SIZE:I

    .line 1526
    .local v97, "tmpBlockSize":I
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mIsConnectionClosedAfterEveryRequest:Z
    invoke-static {v6}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$3000(Lorg/apache/http/impl/client/MultiSocketInputStream;)Z

    move-result v6

    if-eqz v6, :cond_37

    .line 1527
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBytesForMultiSocket:J
    invoke-static {v6}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1100(Lorg/apache/http/impl/client/MultiSocketInputStream;)J

    move-result-wide v6

    sget v9, Lorg/apache/http/impl/client/MultiSocketInputStream;->MULTIRAT_BLOCK_DIV_CON_CLOSED:I

    int-to-long v10, v9

    div-long/2addr v6, v10

    long-to-int v0, v6

    move/from16 v97, v0

    .line 1528
    sget v6, Lorg/apache/http/impl/client/MultiSocketInputStream;->MAX_MULTIRAT_BLOCK_SIZE_CON_CLOSED:I

    move/from16 v0, v97

    invoke-static {v0, v6}, Ljava/lang/Math;->min(II)I

    move-result v97

    .line 1530
    sget v6, Lorg/apache/http/impl/client/MultiSocketInputStream;->MIN_SIZE_FOR_MULTISOCKET_CON_CLOSE:I

    move/from16 v0, v97

    invoke-static {v0, v6}, Ljava/lang/Math;->max(II)I

    move-result v97

    .line 1532
    const/4 v6, 0x4

    # setter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->sMaxBlock:I
    invoke-static {v6}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$3502(I)I

    .line 1539
    :goto_11
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    move/from16 v0, v97

    # setter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mRestBlockSize:I
    invoke-static {v6, v0}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$3602(Lorg/apache/http/impl/client/MultiSocketInputStream;I)I

    .line 1540
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    move/from16 v0, v97

    # setter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockSize:I
    invoke-static {v6, v0}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$902(Lorg/apache/http/impl/client/MultiSocketInputStream;I)I

    .line 1541
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBytesForMultiSocket:J
    invoke-static {v6}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1100(Lorg/apache/http/impl/client/MultiSocketInputStream;)J

    move-result-wide v6

    move-object/from16 v0, p0

    iget-object v9, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockSize:I
    invoke-static {v9}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$900(Lorg/apache/http/impl/client/MultiSocketInputStream;)I

    move-result v9

    mul-int/lit8 v9, v9, 0x3

    div-int/lit8 v9, v9, 0x2

    int-to-long v10, v9

    cmp-long v6, v6, v10

    if-lez v6, :cond_3e

    .line 1543
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Runtime;->maxMemory()J

    move-result-wide v70

    .line 1544
    .local v70, "maxMem":J
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    const-wide/16 v10, 0x2

    div-long v10, v70, v10

    move-object/from16 v0, p0

    iget-object v7, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockSize:I
    invoke-static {v7}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$900(Lorg/apache/http/impl/client/MultiSocketInputStream;)I

    move-result v7

    int-to-long v14, v7

    div-long/2addr v10, v14

    long-to-int v7, v10

    const/4 v9, 0x1

    invoke-static {v7, v9}, Ljava/lang/Math;->max(II)I

    move-result v7

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->sMaxBlock:I
    invoke-static {}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$3500()I

    move-result v9

    invoke-static {v7, v9}, Ljava/lang/Math;->min(II)I

    move-result v7

    # setter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mMaxBlockNumber:I
    invoke-static {v6, v7}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$3702(Lorg/apache/http/impl/client/MultiSocketInputStream;I)I

    .line 1546
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    move-object/from16 v0, p0

    iget-object v7, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mOffset:J
    invoke-static {v7}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1200(Lorg/apache/http/impl/client/MultiSocketInputStream;)J

    move-result-wide v10

    move-object/from16 v0, p0

    iget-object v7, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mContentLength:J
    invoke-static {v7}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$400(Lorg/apache/http/impl/client/MultiSocketInputStream;)J

    move-result-wide v14

    add-long/2addr v10, v14

    move-object/from16 v0, p0

    iget-object v7, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBytesForMultiSocket:J
    invoke-static {v7}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1100(Lorg/apache/http/impl/client/MultiSocketInputStream;)J

    move-result-wide v14

    sub-long/2addr v10, v14

    # setter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mOffset:J
    invoke-static {v6, v10, v11}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1202(Lorg/apache/http/impl/client/MultiSocketInputStream;J)J

    .line 1548
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBytesForMultiSocket:J
    invoke-static {v6}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1100(Lorg/apache/http/impl/client/MultiSocketInputStream;)J

    move-result-wide v6

    move-object/from16 v0, p0

    iget-object v9, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockSize:I
    invoke-static {v9}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$900(Lorg/apache/http/impl/client/MultiSocketInputStream;)I

    move-result v9

    int-to-long v10, v9

    rem-long/2addr v6, v10

    const-wide/16 v10, 0x0

    cmp-long v6, v6, v10

    if-nez v6, :cond_38

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBytesForMultiSocket:J
    invoke-static {v6}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1100(Lorg/apache/http/impl/client/MultiSocketInputStream;)J

    move-result-wide v6

    move-object/from16 v0, p0

    iget-object v9, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockSize:I
    invoke-static {v9}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$900(Lorg/apache/http/impl/client/MultiSocketInputStream;)I

    move-result v9

    int-to-long v10, v9

    div-long/2addr v6, v10

    :goto_12
    long-to-int v0, v6

    move/from16 v68, v0

    .line 1551
    .local v68, "maxChunks":I
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBytesForMultiSocket:J
    invoke-static {v6}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1100(Lorg/apache/http/impl/client/MultiSocketInputStream;)J

    move-result-wide v6

    move-object/from16 v0, p0

    iget-object v9, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockSize:I
    invoke-static {v9}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$900(Lorg/apache/http/impl/client/MultiSocketInputStream;)I

    move-result v9

    int-to-long v10, v9

    rem-long/2addr v6, v10

    long-to-int v0, v6

    move/from16 v62, v0

    .line 1553
    .local v62, "lastChunkSize":I
    sget v6, Lorg/apache/http/impl/client/MultiSocketInputStream;->MAX_LAST_CHUNK_SIZE_FOR_MERGE:I

    move-object/from16 v0, p0

    iget-object v7, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockSize:I
    invoke-static {v7}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$900(Lorg/apache/http/impl/client/MultiSocketInputStream;)I

    move-result v7

    div-int/lit8 v7, v7, 0x3

    invoke-static {v6, v7}, Ljava/lang/Math;->min(II)I

    move-result v6

    move/from16 v0, v62

    if-gt v0, v6, :cond_35

    if-lez v62, :cond_35

    .line 1554
    sget-boolean v6, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v6, :cond_34

    .line 1555
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "The no of chunks initially "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, v68

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " new no of chunks "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    add-int/lit8 v7, v68, -0x1

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 1557
    :cond_34
    add-int/lit8 v68, v68, -0x1

    .line 1560
    :cond_35
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    move/from16 v0, v68

    new-array v7, v0, [B

    # setter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockStatus:[B
    invoke-static {v6, v7}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1602(Lorg/apache/http/impl/client/MultiSocketInputStream;[B)[B

    .line 1562
    sget-boolean v6, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v6, :cond_36

    .line 1563
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Total chunks = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockStatus:[B
    invoke-static {v7}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1600(Lorg/apache/http/impl/client/MultiSocketInputStream;)[B

    move-result-object v7

    array-length v7, v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 1567
    :cond_36
    const/16 v54, 0x0

    .restart local v54    # "i":I
    :goto_13
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockStatus:[B
    invoke-static {v6}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1600(Lorg/apache/http/impl/client/MultiSocketInputStream;)[B

    move-result-object v6

    array-length v6, v6

    move/from16 v0, v54

    if-ge v0, v6, :cond_39

    .line 1568
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockStatus:[B
    invoke-static {v6}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1600(Lorg/apache/http/impl/client/MultiSocketInputStream;)[B

    move-result-object v6

    const/4 v7, 0x0

    aput-byte v7, v6, v54

    .line 1567
    add-int/lit8 v54, v54, 0x1

    goto :goto_13

    .line 1514
    .end local v54    # "i":I
    .end local v62    # "lastChunkSize":I
    .end local v68    # "maxChunks":I
    .end local v70    # "maxMem":J
    .end local v97    # "tmpBlockSize":I
    :catch_2
    move-exception v51

    .line 1515
    .local v51, "ex":Ljava/lang/Throwable;
    sget-boolean v6, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v6, :cond_33

    .line 1516
    invoke-static/range {v51 .. v51}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/Throwable;)V

    goto/16 :goto_10

    .line 1534
    .end local v51    # "ex":Ljava/lang/Throwable;
    .restart local v97    # "tmpBlockSize":I
    :cond_37
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBytesForMultiSocket:J
    invoke-static {v6}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1100(Lorg/apache/http/impl/client/MultiSocketInputStream;)J

    move-result-wide v6

    sget v9, Lorg/apache/http/impl/client/MultiSocketInputStream;->MULTIRAT_BLOCK_DIV:I

    int-to-long v10, v9

    div-long/2addr v6, v10

    long-to-int v0, v6

    move/from16 v97, v0

    .line 1535
    sget v6, Lorg/apache/http/impl/client/MultiSocketInputStream;->MAX_MULTIRAT_BLOCK_SIZE:I

    move/from16 v0, v97

    invoke-static {v0, v6}, Ljava/lang/Math;->min(II)I

    move-result v97

    .line 1536
    sget v6, Lorg/apache/http/impl/client/MultiSocketInputStream;->MIN_MULTIRAT_BLOCK_SIZE:I

    move/from16 v0, v97

    invoke-static {v0, v6}, Ljava/lang/Math;->max(II)I

    move-result v97

    .line 1537
    const/16 v6, 0x8

    # setter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->sMaxBlock:I
    invoke-static {v6}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$3502(I)I

    goto/16 :goto_11

    .line 1548
    .restart local v70    # "maxMem":J
    :cond_38
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBytesForMultiSocket:J
    invoke-static {v6}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1100(Lorg/apache/http/impl/client/MultiSocketInputStream;)J

    move-result-wide v6

    move-object/from16 v0, p0

    iget-object v9, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockSize:I
    invoke-static {v9}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$900(Lorg/apache/http/impl/client/MultiSocketInputStream;)I

    move-result v9

    int-to-long v10, v9

    div-long/2addr v6, v10

    const-wide/16 v10, 0x1

    add-long/2addr v6, v10

    goto/16 :goto_12

    .line 1570
    .restart local v54    # "i":I
    .restart local v62    # "lastChunkSize":I
    .restart local v68    # "maxChunks":I
    :cond_39
    sget-boolean v6, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v6, :cond_3a

    .line 1571
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "block status is initialized with size: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockStatus:[B
    invoke-static {v7}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1600(Lorg/apache/http/impl/client/MultiSocketInputStream;)[B

    move-result-object v7

    array-length v7, v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 1576
    :cond_3a
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    const/4 v7, 0x1

    # setter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mIsMultiSocketStarted:Z
    invoke-static {v6, v7}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$3802(Lorg/apache/http/impl/client/MultiSocketInputStream;Z)Z

    .line 1577
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    const/4 v7, 0x0

    # setter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->finishedThreadNum:I
    invoke-static {v6, v7}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$3902(Lorg/apache/http/impl/client/MultiSocketInputStream;I)I

    .line 1578
    sget-boolean v6, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v6, :cond_3b

    .line 1579
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "start socket thread for interfaces "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBytesRemaining:J
    invoke-static {v7}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$700(Lorg/apache/http/impl/client/MultiSocketInputStream;)J

    move-result-wide v10

    invoke-virtual {v6, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ":"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBytesForMultiSocket:J
    invoke-static {v7}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1100(Lorg/apache/http/impl/client/MultiSocketInputStream;)J

    move-result-wide v10

    invoke-virtual {v6, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ":"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mOffset:J
    invoke-static {v7}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1200(Lorg/apache/http/impl/client/MultiSocketInputStream;)J

    move-result-wide v10

    invoke-virtual {v6, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 1588
    :cond_3b
    const/4 v6, 0x0

    aget-wide v6, v52, v6

    const-wide/16 v10, -0x1

    cmp-long v6, v6, v10

    if-eqz v6, :cond_3c

    const/4 v6, 0x0

    aget-wide v6, v52, v6

    :goto_14
    long-to-int v6, v6

    sput v6, Lorg/apache/http/impl/client/MultiSocketInputStream;->DEFAULT_INTERFACE_FOR_INITIAL_CHUNK:I

    .line 1592
    const/4 v6, 0x2

    new-array v0, v6, [Ljava/lang/Thread;

    move-object/from16 v85, v0

    .line 1594
    .local v85, "t":[Ljava/lang/Thread;
    const/16 v54, 0x0

    :goto_15
    const/4 v6, 0x2

    move/from16 v0, v54

    if-ge v0, v6, :cond_3d

    .line 1595
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mRequestHandlers:[Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;
    invoke-static {v6}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$4000(Lorg/apache/http/impl/client/MultiSocketInputStream;)[Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;

    move-result-object v6

    new-instance v7, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;

    move-object/from16 v0, p0

    iget-object v9, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    move/from16 v0, v54

    invoke-direct {v7, v9, v0}, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;-><init>(Lorg/apache/http/impl/client/MultiSocketInputStream;I)V

    aput-object v7, v6, v54

    .line 1596
    new-instance v6, Ljava/lang/Thread;

    move-object/from16 v0, p0

    iget-object v7, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mRequestHandlers:[Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;
    invoke-static {v7}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$4000(Lorg/apache/http/impl/client/MultiSocketInputStream;)[Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;

    move-result-object v7

    aget-object v7, v7, v54

    invoke-direct {v6, v7}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    aput-object v6, v85, v54

    .line 1594
    add-int/lit8 v54, v54, 0x1

    goto :goto_15

    .line 1588
    .end local v85    # "t":[Ljava/lang/Thread;
    :cond_3c
    const-wide/16 v6, 0x0

    goto :goto_14

    .line 1599
    .restart local v85    # "t":[Ljava/lang/Thread;
    :cond_3d
    const/16 v54, 0x0

    :goto_16
    const/4 v6, 0x2

    move/from16 v0, v54

    if-ge v0, v6, :cond_1b

    .line 1600
    aget-object v6, v85, v54

    invoke-virtual {v6}, Ljava/lang/Thread;->start()V

    .line 1599
    add-int/lit8 v54, v54, 0x1

    goto :goto_16

    .line 1603
    .end local v54    # "i":I
    .end local v62    # "lastChunkSize":I
    .end local v68    # "maxChunks":I
    .end local v70    # "maxMem":J
    .end local v85    # "t":[Ljava/lang/Thread;
    :cond_3e
    sget-boolean v6, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v6, :cond_1b

    .line 1604
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "rest size too small to start Multi socket: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBytesForMultiSocket:J
    invoke-static {v7}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1100(Lorg/apache/http/impl/client/MultiSocketInputStream;)J

    move-result-wide v10

    invoke-virtual {v6, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_2

    goto/16 :goto_9

    .line 1614
    .end local v38    # "bytesExpected":J
    .end local v44    # "currentConnectTime":J
    .end local v52    # "highThroughPutInterface":[J
    .end local v60    # "inf":J
    .end local v72    # "maxTailLen":J
    .end local v80    # "ra":J
    .end local v86    # "sizeForExtremeCase":J
    .end local v97    # "tmpBlockSize":I
    :cond_3f
    :try_start_13
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockManager:Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;
    invoke-static {v6}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$800(Lorg/apache/http/impl/client/MultiSocketInputStream;)Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;

    move-result-object v6

    const-wide/16 v10, 0xc8

    invoke-virtual {v6, v10, v11}, Ljava/lang/Object;->wait(J)V
    :try_end_13
    .catch Ljava/lang/Throwable; {:try_start_13 .. :try_end_13} :catch_3
    .catchall {:try_start_13 .. :try_end_13} :catchall_2

    .line 1619
    :cond_40
    :goto_17
    :try_start_14
    monitor-exit v8

    goto/16 :goto_2

    .line 1615
    :catch_3
    move-exception v48

    .line 1616
    .restart local v48    # "e":Ljava/lang/Throwable;
    sget-boolean v6, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v6, :cond_40

    .line 1617
    invoke-static/range {v48 .. v48}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/Throwable;)V
    :try_end_14
    .catchall {:try_start_14 .. :try_end_14} :catchall_2

    goto :goto_17

    .line 1639
    .end local v48    # "e":Ljava/lang/Throwable;
    .end local v64    # "mainReadTimeDiff":J
    .end local v66    # "maxBufDataSize":J
    .restart local v36    # "byteThresholdAs0":J
    .restart local v54    # "i":I
    .restart local v63    # "lens":[J
    .restart local v69    # "maxCountToSend":I
    .restart local v74    # "noDataCount":[I
    .restart local v75    # "prevBytes":[J
    .restart local v78    # "prevSp":[J
    .restart local v79    # "prevTime":[J
    .restart local v96    # "times":[J
    :cond_41
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->childIS:Lorg/apache/http/impl/client/TwoChunkInputStream;
    invoke-static {v6}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$4300(Lorg/apache/http/impl/client/MultiSocketInputStream;)Lorg/apache/http/impl/client/TwoChunkInputStream;

    move-result-object v6

    move/from16 v0, v54

    invoke-virtual {v6, v0}, Lorg/apache/http/impl/client/TwoChunkInputStream;->getDownloadLen(I)J

    move-result-wide v6

    goto/16 :goto_6

    .line 1640
    :cond_42
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->childIS:Lorg/apache/http/impl/client/TwoChunkInputStream;
    invoke-static {v6}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$4300(Lorg/apache/http/impl/client/MultiSocketInputStream;)Lorg/apache/http/impl/client/TwoChunkInputStream;

    move-result-object v6

    move/from16 v0, v54

    invoke-virtual {v6, v0}, Lorg/apache/http/impl/client/TwoChunkInputStream;->getDownloadTime(I)J

    move-result-wide v6

    goto/16 :goto_7

    .line 1642
    :cond_43
    const/4 v6, 0x0

    aget-wide v6, v79, v6

    const-wide/16 v8, 0x0

    cmp-long v6, v6, v8

    if-nez v6, :cond_52

    .line 1643
    const/16 v54, 0x0

    :goto_18
    const/4 v6, 0x2

    move/from16 v0, v54

    if-ge v0, v6, :cond_44

    .line 1644
    aget-wide v6, v96, v54

    aput-wide v6, v79, v54

    .line 1645
    aget-wide v6, v63, v54

    aput-wide v6, v75, v54

    .line 1643
    add-int/lit8 v54, v54, 0x1

    goto :goto_18

    .line 1647
    :cond_44
    sget-boolean v6, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v6, :cond_45

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "[speed calc detail]init speed data: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const/4 v7, 0x0

    aget-wide v8, v75, v7

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const/4 v7, 0x0

    aget-wide v8, v79, v7

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ":"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const/4 v7, 0x1

    aget-wide v8, v75, v7

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const/4 v7, 0x1

    aget-wide v8, v79, v7

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 1698
    .end local v54    # "i":I
    .end local v63    # "lens":[J
    .end local v96    # "times":[J
    :cond_45
    :goto_19
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->bSBUsed:Z
    invoke-static {v6}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$600(Lorg/apache/http/impl/client/MultiSocketInputStream;)Z

    move-result v6

    if-eqz v6, :cond_4d

    .line 1699
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->sbUsedLocker:Ljava/lang/Object;
    invoke-static {v6}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$500(Lorg/apache/http/impl/client/MultiSocketInputStream;)Ljava/lang/Object;

    move-result-object v7

    monitor-enter v7

    .line 1700
    :try_start_15
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->bSBUsed:Z
    invoke-static {v6}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$600(Lorg/apache/http/impl/client/MultiSocketInputStream;)Z

    move-result v6

    if-eqz v6, :cond_46

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mRangeRequestSuccess:I
    invoke-static {v6}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$2300(Lorg/apache/http/impl/client/MultiSocketInputStream;)I

    move-result v6

    const/4 v8, 0x1

    if-ne v6, v8, :cond_46

    .line 1701
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    iget-object v6, v6, Lorg/apache/http/impl/client/MultiSocketInputStream;->customHttpClient:Lorg/apache/http/impl/client/CustomHttpClient;

    move-object/from16 v0, p0

    iget-object v8, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mMainThreadID:J
    invoke-static {v8}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$000(Lorg/apache/http/impl/client/MultiSocketInputStream;)J

    move-result-wide v8

    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->failReasonInExtreme:I
    invoke-static {v10}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$4800(Lorg/apache/http/impl/client/MultiSocketInputStream;)I

    move-result v10

    invoke-virtual {v6, v8, v9, v10}, Lorg/apache/http/impl/client/CustomHttpClient;->stopSBUsageWithReason(JI)I

    .line 1702
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    const/4 v8, 0x0

    # setter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->bSBUsed:Z
    invoke-static {v6, v8}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$602(Lorg/apache/http/impl/client/MultiSocketInputStream;Z)Z

    .line 1704
    :cond_46
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->bStopSecDecisionMade:Z
    invoke-static {v6}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$4700(Lorg/apache/http/impl/client/MultiSocketInputStream;)Z

    move-result v6

    if-nez v6, :cond_4c

    .line 1705
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->bExtremThreadStarted:Z
    invoke-static {v6}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1900(Lorg/apache/http/impl/client/MultiSocketInputStream;)Z

    move-result v6

    if-eqz v6, :cond_4c

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->bExtremThreadExisted:Z
    invoke-static {v6}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$2000(Lorg/apache/http/impl/client/MultiSocketInputStream;)Z

    move-result v6

    if-eqz v6, :cond_4c

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mExtThread:Lorg/apache/http/impl/client/MultiSocketInputStream$ExtremeConditionThread;
    invoke-static {v6}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1700(Lorg/apache/http/impl/client/MultiSocketInputStream;)Lorg/apache/http/impl/client/MultiSocketInputStream$ExtremeConditionThread;

    move-result-object v6

    if-eqz v6, :cond_4c

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->childIS:Lorg/apache/http/impl/client/TwoChunkInputStream;
    invoke-static {v6}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$4300(Lorg/apache/http/impl/client/MultiSocketInputStream;)Lorg/apache/http/impl/client/TwoChunkInputStream;

    move-result-object v6

    if-nez v6, :cond_4c

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mIsReadFromBuffer:Z
    invoke-static {v6}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$4900(Lorg/apache/http/impl/client/MultiSocketInputStream;)Z

    move-result v6

    if-nez v6, :cond_4c

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mRangeRequestSuccess:I
    invoke-static {v6}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$2300(Lorg/apache/http/impl/client/MultiSocketInputStream;)I

    move-result v6

    const/4 v8, 0x3

    if-ne v6, v8, :cond_4c

    .line 1707
    sget-boolean v6, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v6, :cond_47

    .line 1708
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "extremThread running, main socket reading from socket, remain bytes = "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v8, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBytesRemaining:J
    invoke-static {v8}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$700(Lorg/apache/http/impl/client/MultiSocketInputStream;)J

    move-result-wide v8

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 1710
    :cond_47
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mMainSocketInterface:I
    invoke-static {v6}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$300(Lorg/apache/http/impl/client/MultiSocketInputStream;)I

    move-result v55

    .line 1711
    .local v55, "id0":I
    add-int/lit8 v6, v55, 0x1

    rem-int/lit8 v56, v6, 0x2

    .line 1712
    .local v56, "id1":I
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mStartReadTime:[J
    invoke-static {v6}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$3200(Lorg/apache/http/impl/client/MultiSocketInputStream;)[J

    move-result-object v6

    aget-wide v10, v6, v55

    sub-long v92, v8, v10

    .line 1713
    .local v92, "time0":J
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mStartReadTime:[J
    invoke-static {v6}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$3200(Lorg/apache/http/impl/client/MultiSocketInputStream;)[J

    move-result-object v6

    aget-wide v10, v6, v56

    sub-long v94, v8, v10

    .line 1714
    .local v94, "time1":J
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->MIN_TIME_TO_CHECK_STOP_SLOW:I
    invoke-static {v6}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$5000(Lorg/apache/http/impl/client/MultiSocketInputStream;)I

    move-result v6

    int-to-long v8, v6

    cmp-long v6, v92, v8

    if-ltz v6, :cond_4c

    .line 1715
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    move/from16 v0, v55

    invoke-virtual {v6, v0}, Lorg/apache/http/impl/client/MultiSocketInputStream;->getSpeed(I)J

    move-result-wide v88

    .line 1716
    .local v88, "sp0":J
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    move/from16 v0, v56

    invoke-virtual {v6, v0}, Lorg/apache/http/impl/client/MultiSocketInputStream;->getSpeed(I)J

    move-result-wide v90

    .line 1717
    .local v90, "sp1":J
    sget-boolean v6, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v6, :cond_48

    .line 1718
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "current speed is "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-wide/from16 v0, v88

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, ":"

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-wide/from16 v0, v90

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, " threshold ratio is "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->MIN_RATIO_FOR_ONLY_ONE_INF_0V1:I
    invoke-static {}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$5100()I

    move-result v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, " and "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->MIN_RATIO_FOR_ONLY_ONE_INF_1V0:I
    invoke-static {}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$5200()I

    move-result v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 1722
    :cond_48
    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->MIN_RATIO_FOR_ONLY_ONE_INF_0V1:I
    invoke-static {}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$5100()I

    move-result v6

    if-ltz v6, :cond_5c

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->MIN_TIME_TO_CHECK_STOP_SLOW:I
    invoke-static {v6}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$5000(Lorg/apache/http/impl/client/MultiSocketInputStream;)I

    move-result v6

    int-to-long v8, v6

    cmp-long v6, v94, v8

    if-ltz v6, :cond_49

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->MIN_RATIO_FOR_ONLY_ONE_INF_0V1:I
    invoke-static {}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$5100()I

    move-result v6

    int-to-long v8, v6

    mul-long v8, v8, v90

    cmp-long v6, v88, v8

    if-lez v6, :cond_49

    const-wide/16 v8, 0x0

    cmp-long v6, v88, v8

    if-lez v6, :cond_49

    const-wide/16 v8, 0x0

    cmp-long v6, v90, v8

    if-gtz v6, :cond_4a

    :cond_49
    const-wide/16 v8, 0xa

    cmp-long v6, v90, v8

    if-gtz v6, :cond_5c

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->MIN_TIME_TO_CHECK_STOP_SLOW:I
    invoke-static {v6}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$5000(Lorg/apache/http/impl/client/MultiSocketInputStream;)I

    move-result v6

    div-int/lit8 v6, v6, 0x2

    int-to-long v8, v6

    cmp-long v6, v94, v8

    if-ltz v6, :cond_5c

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mDataDownloaded:[J
    invoke-static {v6}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$4200(Lorg/apache/http/impl/client/MultiSocketInputStream;)[J

    move-result-object v6

    aget-wide v8, v6, v56

    const-wide/16 v10, 0x0

    cmp-long v6, v8, v10

    if-lez v6, :cond_5c

    .line 1726
    :cond_4a
    sget-boolean v6, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v6, :cond_4b

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "now stop the interface "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, v56

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, " and swtich sec interface to "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, v55

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 1727
    :cond_4b
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    const/4 v8, 0x1

    # setter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->bStopSecDecisionMade:Z
    invoke-static {v6, v8}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$4702(Lorg/apache/http/impl/client/MultiSocketInputStream;Z)Z

    .line 1733
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    const/4 v8, 0x0

    # setter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mIsReadFromBuffer:Z
    invoke-static {v6, v8}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$4902(Lorg/apache/http/impl/client/MultiSocketInputStream;Z)Z

    .line 1734
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    const/4 v8, 0x0

    # setter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mIsMultiSocketStarted:Z
    invoke-static {v6, v8}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$3802(Lorg/apache/http/impl/client/MultiSocketInputStream;Z)Z

    .line 1735
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    const/4 v8, 0x0

    # setter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->bExtremThreadStarted:Z
    invoke-static {v6, v8}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1902(Lorg/apache/http/impl/client/MultiSocketInputStream;Z)Z

    .line 1736
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mExtThread:Lorg/apache/http/impl/client/MultiSocketInputStream$ExtremeConditionThread;
    invoke-static {v6}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1700(Lorg/apache/http/impl/client/MultiSocketInputStream;)Lorg/apache/http/impl/client/MultiSocketInputStream$ExtremeConditionThread;

    move-result-object v6

    const/4 v8, 0x1

    # setter for: Lorg/apache/http/impl/client/MultiSocketInputStream$ExtremeConditionThread;->bDeprecated:Z
    invoke-static {v6, v8}, Lorg/apache/http/impl/client/MultiSocketInputStream$ExtremeConditionThread;->access$5302(Lorg/apache/http/impl/client/MultiSocketInputStream$ExtremeConditionThread;Z)Z

    .line 1737
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mExtThread:Lorg/apache/http/impl/client/MultiSocketInputStream$ExtremeConditionThread;
    invoke-static {v6}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1700(Lorg/apache/http/impl/client/MultiSocketInputStream;)Lorg/apache/http/impl/client/MultiSocketInputStream$ExtremeConditionThread;

    move-result-object v6

    invoke-virtual {v6}, Lorg/apache/http/impl/client/MultiSocketInputStream$ExtremeConditionThread;->closeConnection()V

    .line 1739
    if-nez v55, :cond_5b

    .line 1740
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    iget-object v6, v6, Lorg/apache/http/impl/client/MultiSocketInputStream;->customHttpClient:Lorg/apache/http/impl/client/CustomHttpClient;

    move-object/from16 v0, p0

    iget-object v8, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mMainThreadID:J
    invoke-static {v8}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$000(Lorg/apache/http/impl/client/MultiSocketInputStream;)J

    move-result-wide v8

    const/4 v10, -0x1

    invoke-virtual {v6, v8, v9, v10}, Lorg/apache/http/impl/client/CustomHttpClient;->stopSBUsageWithReason(JI)I

    .line 1763
    .end local v55    # "id0":I
    .end local v56    # "id1":I
    .end local v88    # "sp0":J
    .end local v90    # "sp1":J
    .end local v92    # "time0":J
    .end local v94    # "time1":J
    :cond_4c
    :goto_1a
    monitor-exit v7
    :try_end_15
    .catchall {:try_start_15 .. :try_end_15} :catchall_5

    .line 1765
    :cond_4d
    const-wide/16 v76, 0x0

    .line 1766
    .local v76, "nonReadTime":J
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    iget-wide v6, v6, Lorg/apache/http/impl/client/MultiSocketInputStream;->mLastReadTime:J

    const-wide/16 v8, 0x0

    cmp-long v6, v6, v8

    if-lez v6, :cond_4e

    .line 1767
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    move-object/from16 v0, p0

    iget-object v8, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    iget-wide v8, v8, Lorg/apache/http/impl/client/MultiSocketInputStream;->mLastReadTime:J

    sub-long v76, v6, v8

    .line 1768
    :cond_4e
    sget-boolean v6, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v6, :cond_4f

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "nonReadTime till now = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-wide/from16 v0, v76

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " bAppIsReadingNow="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->bAppIsReadingNow:Z
    invoke-static {v7}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$5500(Lorg/apache/http/impl/client/MultiSocketInputStream;)Z

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 1769
    :cond_4f
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    iget-object v6, v6, Lorg/apache/http/impl/client/MultiSocketInputStream;->mMainRequest:Lorg/apache/http/HttpRequest;

    check-cast v6, Lorg/apache/http/client/methods/HttpUriRequest;

    invoke-interface {v6}, Lorg/apache/http/client/methods/HttpUriRequest;->isAborted()Z

    move-result v6

    if-nez v6, :cond_50

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    iget-boolean v6, v6, Lorg/apache/http/impl/client/MultiSocketInputStream;->mFinished:Z

    if-nez v6, :cond_50

    sget v6, Lorg/apache/http/impl/client/MultiSocketInputStream;->WAIT_FOR_INTERFACE_TIME_OUT:I

    int-to-long v6, v6

    cmp-long v6, v76, v6

    if-lez v6, :cond_5f

    .line 1772
    :cond_50
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    const/4 v7, 0x1

    iput-boolean v7, v6, Lorg/apache/http/impl/client/MultiSocketInputStream;->mFinished:Z

    .line 1773
    sget-boolean v6, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v6, :cond_51

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "close input stream since app is not reading data for "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-wide/from16 v0, v76

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 1791
    .end local v36    # "byteThresholdAs0":J
    .end local v69    # "maxCountToSend":I
    .end local v74    # "noDataCount":[I
    .end local v75    # "prevBytes":[J
    .end local v76    # "nonReadTime":J
    .end local v78    # "prevSp":[J
    .end local v79    # "prevTime":[J
    :cond_51
    :goto_1b
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    iget-object v6, v6, Lorg/apache/http/impl/client/MultiSocketInputStream;->mMainRequest:Lorg/apache/http/HttpRequest;

    check-cast v6, Lorg/apache/http/client/methods/HttpUriRequest;

    invoke-interface {v6}, Lorg/apache/http/client/methods/HttpUriRequest;->isAborted()Z

    move-result v6

    if-nez v6, :cond_62

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    iget-boolean v6, v6, Lorg/apache/http/impl/client/MultiSocketInputStream;->mFinished:Z

    if-nez v6, :cond_62

    .line 1793
    :try_start_16
    monitor-enter v5
    :try_end_16
    .catch Ljava/lang/Throwable; {:try_start_16 .. :try_end_16} :catch_4

    .line 1794
    const-wide/16 v6, 0x3e8

    :try_start_17
    invoke-virtual {v5, v6, v7}, Ljava/lang/Object;->wait(J)V

    .line 1795
    monitor-exit v5

    goto :goto_1b

    :catchall_4
    move-exception v6

    monitor-exit v5
    :try_end_17
    .catchall {:try_start_17 .. :try_end_17} :catchall_4

    :try_start_18
    throw v6
    :try_end_18
    .catch Ljava/lang/Throwable; {:try_start_18 .. :try_end_18} :catch_4

    .line 1796
    :catch_4
    move-exception v48

    .line 1797
    .restart local v48    # "e":Ljava/lang/Throwable;
    sget-boolean v6, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v6, :cond_51

    invoke-static/range {v48 .. v48}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/Throwable;)V

    goto :goto_1b

    .line 1650
    .end local v48    # "e":Ljava/lang/Throwable;
    .restart local v36    # "byteThresholdAs0":J
    .restart local v54    # "i":I
    .restart local v63    # "lens":[J
    .restart local v69    # "maxCountToSend":I
    .restart local v74    # "noDataCount":[I
    .restart local v75    # "prevBytes":[J
    .restart local v78    # "prevSp":[J
    .restart local v79    # "prevTime":[J
    .restart local v96    # "times":[J
    :cond_52
    const/4 v6, 0x2

    new-array v0, v6, [J

    move-object/from16 v46, v0

    .line 1651
    .local v46, "diffLen":[J
    const/4 v6, 0x2

    new-array v0, v6, [J

    move-object/from16 v47, v0

    .line 1652
    .local v47, "diffTime":[J
    const/4 v6, 0x2

    new-array v0, v6, [J

    move-object/from16 v41, v0

    .line 1653
    .local v41, "curSp":[J
    const/16 v54, 0x0

    :goto_1c
    const/4 v6, 0x2

    move/from16 v0, v54

    if-ge v0, v6, :cond_54

    .line 1654
    aget-wide v6, v63, v54

    aget-wide v8, v75, v54

    sub-long/2addr v6, v8

    aput-wide v6, v46, v54

    .line 1655
    aget-wide v6, v96, v54

    aget-wide v8, v79, v54

    sub-long/2addr v6, v8

    aput-wide v6, v47, v54

    .line 1656
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    aget-wide v8, v46, v54

    aget-wide v10, v47, v54

    invoke-virtual {v6, v8, v9, v10, v11}, Lorg/apache/http/impl/client/MultiSocketInputStream;->getSpeed(JJ)J

    move-result-wide v6

    aput-wide v6, v41, v54

    .line 1657
    aget-wide v6, v96, v54

    aput-wide v6, v79, v54

    .line 1658
    aget-wide v6, v63, v54

    aput-wide v6, v75, v54

    .line 1659
    aget-wide v6, v46, v54

    const-wide/16 v8, 0x7d0

    cmp-long v6, v6, v8

    if-gtz v6, :cond_53

    .line 1660
    aget v6, v74, v54

    add-int/lit8 v6, v6, 0x1

    aput v6, v74, v54

    .line 1653
    :goto_1d
    add-int/lit8 v54, v54, 0x1

    goto :goto_1c

    .line 1662
    :cond_53
    const/4 v6, 0x0

    aput v6, v74, v54

    goto :goto_1d

    .line 1664
    :cond_54
    sget-boolean v6, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v6, :cond_55

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "[speed calc detail] reportSBUsage current speed:\t"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const/4 v7, 0x0

    aget-wide v8, v41, v7

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\t:\t"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const/4 v7, 0x1

    aget-wide v8, v41, v7

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\t("

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const/4 v7, 0x0

    aget-wide v8, v46, v7

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const/4 v7, 0x0

    aget-wide v8, v47, v7

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const/4 v7, 0x1

    aget-wide v8, v46, v7

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const/4 v7, 0x1

    aget-wide v8, v47, v7

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\t noDataCount:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const/4 v7, 0x0

    aget v7, v74, v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ","

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const/4 v7, 0x1

    aget v7, v74, v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 1667
    :cond_55
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->SEND_NO_TRAFFIC_WARN:Z
    invoke-static {v6}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$4500(Lorg/apache/http/impl/client/MultiSocketInputStream;)Z

    move-result v6

    if-eqz v6, :cond_57

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->bSBUsed:Z
    invoke-static {v6}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$600(Lorg/apache/http/impl/client/MultiSocketInputStream;)Z

    move-result v6

    if-eqz v6, :cond_57

    const/4 v6, 0x0

    aget v6, v74, v6

    const/4 v7, 0x5

    if-lt v6, v7, :cond_57

    .line 1668
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # invokes: Lorg/apache/http/impl/client/MultiSocketInputStream;->bTwoInfDownloading()Z
    invoke-static {v6}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$4600(Lorg/apache/http/impl/client/MultiSocketInputStream;)Z

    move-result v6

    if-eqz v6, :cond_59

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->bStopSecDecisionMade:Z
    invoke-static {v6}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$4700(Lorg/apache/http/impl/client/MultiSocketInputStream;)Z

    move-result v6

    if-nez v6, :cond_59

    .line 1669
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    iget-object v6, v6, Lorg/apache/http/impl/client/MultiSocketInputStream;->customHttpClient:Lorg/apache/http/impl/client/CustomHttpClient;

    move-object/from16 v0, p0

    iget-object v7, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mMainThreadID:J
    invoke-static {v7}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$000(Lorg/apache/http/impl/client/MultiSocketInputStream;)J

    move-result-wide v8

    const/4 v7, -0x3

    invoke-virtual {v6, v8, v9, v7}, Lorg/apache/http/impl/client/CustomHttpClient;->stopSBUsageWithReason(JI)I

    .line 1674
    :cond_56
    :goto_1e
    const/4 v6, 0x0

    const/4 v7, 0x0

    aput v7, v74, v6

    .line 1676
    :cond_57
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->SEND_NO_TRAFFIC_WARN:Z
    invoke-static {v6}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$4500(Lorg/apache/http/impl/client/MultiSocketInputStream;)Z

    move-result v6

    if-eqz v6, :cond_45

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->bSBUsed:Z
    invoke-static {v6}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$600(Lorg/apache/http/impl/client/MultiSocketInputStream;)Z

    move-result v6

    if-eqz v6, :cond_45

    const/4 v6, 0x1

    aget v6, v74, v6

    const/4 v7, 0x5

    if-lt v6, v7, :cond_45

    .line 1677
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # invokes: Lorg/apache/http/impl/client/MultiSocketInputStream;->bTwoInfDownloading()Z
    invoke-static {v6}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$4600(Lorg/apache/http/impl/client/MultiSocketInputStream;)Z

    move-result v6

    if-eqz v6, :cond_5a

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->bStopSecDecisionMade:Z
    invoke-static {v6}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$4700(Lorg/apache/http/impl/client/MultiSocketInputStream;)Z

    move-result v6

    if-nez v6, :cond_5a

    .line 1678
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    iget-object v6, v6, Lorg/apache/http/impl/client/MultiSocketInputStream;->customHttpClient:Lorg/apache/http/impl/client/CustomHttpClient;

    move-object/from16 v0, p0

    iget-object v7, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mMainThreadID:J
    invoke-static {v7}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$000(Lorg/apache/http/impl/client/MultiSocketInputStream;)J

    move-result-wide v8

    const/4 v7, -0x4

    invoke-virtual {v6, v8, v9, v7}, Lorg/apache/http/impl/client/CustomHttpClient;->stopSBUsageWithReason(JI)I

    .line 1683
    :cond_58
    :goto_1f
    const/4 v6, 0x1

    const/4 v7, 0x0

    aput v7, v74, v6

    goto/16 :goto_19

    .line 1672
    :cond_59
    sget-boolean v6, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v6, :cond_56

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "noDatacount[0] goes to "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const/4 v7, 0x0

    aget v7, v74, v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", but only 1 inf is using now"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    goto :goto_1e

    .line 1681
    :cond_5a
    sget-boolean v6, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v6, :cond_58

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "noDataCount[1] goes to "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const/4 v7, 0x1

    aget v7, v74, v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", but only 1 inf is using now"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    goto :goto_1f

    .line 1742
    .end local v41    # "curSp":[J
    .end local v46    # "diffLen":[J
    .end local v47    # "diffTime":[J
    .end local v54    # "i":I
    .end local v63    # "lens":[J
    .end local v96    # "times":[J
    .restart local v55    # "id0":I
    .restart local v56    # "id1":I
    .restart local v88    # "sp0":J
    .restart local v90    # "sp1":J
    .restart local v92    # "time0":J
    .restart local v94    # "time1":J
    :cond_5b
    :try_start_19
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    iget-object v6, v6, Lorg/apache/http/impl/client/MultiSocketInputStream;->customHttpClient:Lorg/apache/http/impl/client/CustomHttpClient;

    move-object/from16 v0, p0

    iget-object v8, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mMainThreadID:J
    invoke-static {v8}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$000(Lorg/apache/http/impl/client/MultiSocketInputStream;)J

    move-result-wide v8

    const/4 v10, -0x2

    invoke-virtual {v6, v8, v9, v10}, Lorg/apache/http/impl/client/CustomHttpClient;->stopSBUsageWithReason(JI)I

    goto/16 :goto_1a

    .line 1763
    .end local v55    # "id0":I
    .end local v56    # "id1":I
    .end local v88    # "sp0":J
    .end local v90    # "sp1":J
    .end local v92    # "time0":J
    .end local v94    # "time1":J
    :catchall_5
    move-exception v6

    monitor-exit v7
    :try_end_19
    .catchall {:try_start_19 .. :try_end_19} :catchall_5

    throw v6

    .line 1745
    .restart local v55    # "id0":I
    .restart local v56    # "id1":I
    .restart local v88    # "sp0":J
    .restart local v90    # "sp1":J
    .restart local v92    # "time0":J
    .restart local v94    # "time1":J
    :cond_5c
    :try_start_1a
    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->MIN_RATIO_FOR_ONLY_ONE_INF_1V0:I
    invoke-static {}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$5200()I

    move-result v6

    if-ltz v6, :cond_4c

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->MIN_RATIO_FOR_ONLY_ONE_INF_1V0:I
    invoke-static {}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$5200()I

    move-result v6

    int-to-long v8, v6

    mul-long v8, v8, v88

    cmp-long v6, v90, v8

    if-lez v6, :cond_4c

    const-wide/16 v8, 0x0

    cmp-long v6, v88, v8

    if-lez v6, :cond_4c

    const-wide/16 v8, 0x0

    cmp-long v6, v90, v8

    if-lez v6, :cond_4c

    .line 1746
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mExtThread:Lorg/apache/http/impl/client/MultiSocketInputStream$ExtremeConditionThread;
    invoke-static {v6}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1700(Lorg/apache/http/impl/client/MultiSocketInputStream;)Lorg/apache/http/impl/client/MultiSocketInputStream$ExtremeConditionThread;

    move-result-object v6

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream$ExtremeConditionThread;->bIsSocketWorkingFine:Z
    invoke-static {v6}, Lorg/apache/http/impl/client/MultiSocketInputStream$ExtremeConditionThread;->access$5400(Lorg/apache/http/impl/client/MultiSocketInputStream$ExtremeConditionThread;)Z

    move-result v6

    if-eqz v6, :cond_5e

    .line 1747
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    const/4 v8, 0x1

    # setter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->bStopSecDecisionMade:Z
    invoke-static {v6, v8}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$4702(Lorg/apache/http/impl/client/MultiSocketInputStream;Z)Z

    .line 1748
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    move/from16 v0, v56

    # setter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mMainSocketAfterHandover:I
    invoke-static {v6, v0}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$3302(Lorg/apache/http/impl/client/MultiSocketInputStream;I)I

    .line 1749
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mMainManagedConn:Lorg/apache/http/conn/ManagedClientConnection;
    invoke-static {v6}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$2100(Lorg/apache/http/impl/client/MultiSocketInputStream;)Lorg/apache/http/conn/ManagedClientConnection;

    move-result-object v6

    invoke-static {v6}, Lorg/apache/http/impl/client/MultiSocketInputStream;->closeConnQuiet(Lorg/apache/http/conn/ManagedClientConnection;)V

    .line 1750
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    const/4 v8, 0x0

    # setter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mMainManagedConn:Lorg/apache/http/conn/ManagedClientConnection;
    invoke-static {v6, v8}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$2102(Lorg/apache/http/impl/client/MultiSocketInputStream;Lorg/apache/http/conn/ManagedClientConnection;)Lorg/apache/http/conn/ManagedClientConnection;

    .line 1751
    if-nez v56, :cond_5d

    .line 1752
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    iget-object v6, v6, Lorg/apache/http/impl/client/MultiSocketInputStream;->customHttpClient:Lorg/apache/http/impl/client/CustomHttpClient;

    move-object/from16 v0, p0

    iget-object v8, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mMainThreadID:J
    invoke-static {v8}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$000(Lorg/apache/http/impl/client/MultiSocketInputStream;)J

    move-result-wide v8

    const/4 v10, -0x1

    invoke-virtual {v6, v8, v9, v10}, Lorg/apache/http/impl/client/CustomHttpClient;->stopSBUsageWithReason(JI)I

    .line 1755
    :goto_20
    sget-boolean v6, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v6, :cond_4c

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "now stop the interface "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, v55

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, " and switch main interface to "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, v56

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    goto/16 :goto_1a

    .line 1754
    :cond_5d
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    iget-object v6, v6, Lorg/apache/http/impl/client/MultiSocketInputStream;->customHttpClient:Lorg/apache/http/impl/client/CustomHttpClient;

    move-object/from16 v0, p0

    iget-object v8, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mMainThreadID:J
    invoke-static {v8}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$000(Lorg/apache/http/impl/client/MultiSocketInputStream;)J

    move-result-wide v8

    const/4 v10, -0x2

    invoke-virtual {v6, v8, v9, v10}, Lorg/apache/http/impl/client/CustomHttpClient;->stopSBUsageWithReason(JI)I

    goto :goto_20

    .line 1757
    :cond_5e
    sget-boolean v6, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v6, :cond_4c

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "do not stop the interface [2nd>1st] since "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, v56

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, " is not working"

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V
    :try_end_1a
    .catchall {:try_start_1a .. :try_end_1a} :catchall_5

    goto/16 :goto_1a

    .line 1776
    .end local v55    # "id0":I
    .end local v56    # "id1":I
    .end local v88    # "sp0":J
    .end local v90    # "sp1":J
    .end local v92    # "time0":J
    .end local v94    # "time1":J
    .restart local v76    # "nonReadTime":J
    :cond_5f
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->bAppIsReadingNow:Z
    invoke-static {v6}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$5500(Lorg/apache/http/impl/client/MultiSocketInputStream;)Z

    move-result v6

    if-nez v6, :cond_61

    sget v6, Lorg/apache/http/impl/client/MultiSocketInputStream;->WAIT_FOR_RANGEREQUEST_TIME_OUT:I

    add-int/lit16 v6, v6, 0x3e8

    int-to-long v6, v6

    cmp-long v6, v76, v6

    if-lez v6, :cond_61

    .line 1777
    sget-boolean v6, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v6, :cond_60

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "stop reading data for second chunks for APP nonReadTime is "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-wide/from16 v0, v76

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 1778
    :cond_60
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    const/4 v7, 0x0

    iput-boolean v7, v6, Lorg/apache/http/impl/client/MultiSocketInputStream;->bAppReading:Z

    .line 1781
    :cond_61
    :try_start_1b
    monitor-enter v5
    :try_end_1b
    .catch Ljava/lang/Throwable; {:try_start_1b .. :try_end_1b} :catch_5

    .line 1782
    const-wide/16 v6, 0x3e8

    :try_start_1c
    invoke-virtual {v5, v6, v7}, Ljava/lang/Object;->wait(J)V

    .line 1783
    monitor-exit v5

    goto/16 :goto_4

    :catchall_6
    move-exception v6

    monitor-exit v5
    :try_end_1c
    .catchall {:try_start_1c .. :try_end_1c} :catchall_6

    :try_start_1d
    throw v6
    :try_end_1d
    .catch Ljava/lang/Throwable; {:try_start_1d .. :try_end_1d} :catch_5

    .line 1784
    :catch_5
    move-exception v48

    .line 1785
    .restart local v48    # "e":Ljava/lang/Throwable;
    sget-boolean v6, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v6, :cond_b

    invoke-static/range {v48 .. v48}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/Throwable;)V

    goto/16 :goto_4

    .line 1802
    .end local v36    # "byteThresholdAs0":J
    .end local v48    # "e":Ljava/lang/Throwable;
    .end local v69    # "maxCountToSend":I
    .end local v74    # "noDataCount":[I
    .end local v75    # "prevBytes":[J
    .end local v76    # "nonReadTime":J
    .end local v78    # "prevSp":[J
    .end local v79    # "prevTime":[J
    :cond_62
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    const/4 v7, 0x1

    iput-boolean v7, v6, Lorg/apache/http/impl/client/MultiSocketInputStream;->mFinished:Z

    .line 1805
    :try_start_1e
    sget-boolean v6, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v6, :cond_63

    const-string v6, "Close is called from Interface Watchdog"

    invoke-static {v6}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 1806
    :cond_63
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$InterfaceWatchdog;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    invoke-virtual {v6}, Lorg/apache/http/impl/client/MultiSocketInputStream;->close()V
    :try_end_1e
    .catch Ljava/lang/Throwable; {:try_start_1e .. :try_end_1e} :catch_6

    .line 1811
    :cond_64
    :goto_21
    invoke-static {}, Lorg/apache/http/impl/client/CustomHttpClient;->flushLogs()V

    .line 1812
    return-void

    .line 1807
    :catch_6
    move-exception v51

    .line 1808
    .restart local v51    # "ex":Ljava/lang/Throwable;
    sget-boolean v6, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v6, :cond_64

    invoke-static/range {v51 .. v51}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/Throwable;)V

    goto :goto_21

    .line 1627
    nop

    :array_0
    .array-data 8
        0x0
        0x0
    .end array-data

    .line 1628
    :array_1
    .array-data 8
        0x0
        0x0
    .end array-data

    .line 1629
    :array_2
    .array-data 8
        0x0
        0x0
    .end array-data

    .line 1632
    :array_3
    .array-data 4
        0x0
        0x0
    .end array-data
.end method
