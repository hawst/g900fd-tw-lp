.class Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;
.super Ljava/lang/Object;
.source "TwoChunkInputStream.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/http/impl/client/TwoChunkInputStream;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "SimpleDataBuffer"
.end annotation


# instance fields
.field private bSwitchedToChild:Z

.field private final bufOffset:J

.field private childChunkEnd:J

.field private childChunkInput:Ljava/io/InputStream;

.field private childChunkStart:J

.field private dis:Ljava/io/DataInputStream;

.field private dos:Ljava/io/DataOutputStream;

.field private fileBuf:Ljava/io/File;

.field private fullRead:Z

.field private hasReadLen:J

.field private final mBufferLength:J

.field private offset:J

.field private readOffset:J

.field private restLen:J

.field final synthetic this$0:Lorg/apache/http/impl/client/TwoChunkInputStream;


# direct methods
.method public constructor <init>(Lorg/apache/http/impl/client/TwoChunkInputStream;JJ)V
    .locals 8
    .param p2, "start"    # J
    .param p4, "len"    # J

    .prologue
    const/4 v6, 0x0

    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    .line 1284
    iput-object p1, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->this$0:Lorg/apache/http/impl/client/TwoChunkInputStream;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1259
    iput-object v2, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->dis:Ljava/io/DataInputStream;

    .line 1263
    iput-object v2, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->dos:Ljava/io/DataOutputStream;

    .line 1267
    iput-object v2, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->fileBuf:Ljava/io/File;

    .line 1269
    iput-object v2, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->childChunkInput:Ljava/io/InputStream;

    .line 1270
    iput-wide v4, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->childChunkStart:J

    .line 1271
    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->childChunkEnd:J

    .line 1274
    iput-boolean v6, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->bSwitchedToChild:Z

    .line 1285
    iput-wide p4, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->mBufferLength:J

    .line 1286
    iput-boolean v6, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->fullRead:Z

    .line 1287
    iput-wide v4, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->hasReadLen:J

    .line 1288
    iput-wide v4, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->restLen:J

    .line 1289
    iput-wide p2, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->offset:J

    .line 1290
    iput-wide p2, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->readOffset:J

    .line 1291
    iput-wide p2, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->bufOffset:J

    .line 1292
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/4 v2, 0x3

    if-ge v0, v2, :cond_0

    .line 1293
    invoke-direct {p0}, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->createTempBufFile()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1307
    :cond_0
    sget-boolean v2, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v2, :cond_1

    .line 1308
    new-instance v2, Ljava/lang/Throwable;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "new SimpleDBuffer added from "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " with length "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p4, p5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/Throwable;-><init>(Ljava/lang/String;)V

    invoke-static {v2}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/Throwable;)V

    .line 1310
    :cond_1
    return-void

    .line 1297
    :cond_2
    invoke-direct {p0}, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->closeAndDelFile()V

    .line 1298
    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    .line 1299
    .local v1, "obj":Ljava/lang/Object;
    monitor-enter v1

    .line 1301
    const-wide/16 v2, 0x1f4

    :try_start_0
    invoke-virtual {v1, v2, v3}, Ljava/lang/Object;->wait(J)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1304
    :goto_1
    :try_start_1
    monitor-exit v1

    .line 1292
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1304
    :catchall_0
    move-exception v2

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2

    .line 1303
    :catch_0
    move-exception v2

    goto :goto_1
.end method

.method private closeAndDelFile()V
    .locals 5

    .prologue
    .line 1345
    iget-object v1, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->this$0:Lorg/apache/http/impl/client/TwoChunkInputStream;

    # getter for: Lorg/apache/http/impl/client/TwoChunkInputStream;->parentIS:Lorg/apache/http/impl/client/MultiSocketInputStream;
    invoke-static {v1}, Lorg/apache/http/impl/client/TwoChunkInputStream;->access$000(Lorg/apache/http/impl/client/TwoChunkInputStream;)Lorg/apache/http/impl/client/MultiSocketInputStream;

    move-result-object v2

    monitor-enter v2

    .line 1347
    :try_start_0
    sget-boolean v1, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v1, :cond_0

    .line 1348
    iget-object v1, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->this$0:Lorg/apache/http/impl/client/TwoChunkInputStream;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "buffered file removed "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->fileBuf:Ljava/io/File;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    # invokes: Lorg/apache/http/impl/client/TwoChunkInputStream;->log(Ljava/lang/String;)V
    invoke-static {v1, v3}, Lorg/apache/http/impl/client/TwoChunkInputStream;->access$200(Lorg/apache/http/impl/client/TwoChunkInputStream;Ljava/lang/String;)V

    .line 1350
    :cond_0
    iget-object v1, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->fileBuf:Ljava/io/File;

    if-eqz v1, :cond_4

    .line 1351
    iget-object v1, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->dis:Ljava/io/DataInputStream;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->dis:Ljava/io/DataInputStream;

    invoke-virtual {v1}, Ljava/io/DataInputStream;->close()V

    .line 1352
    :cond_1
    iget-object v1, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->dos:Ljava/io/DataOutputStream;

    if-eqz v1, :cond_2

    .line 1353
    iget-object v1, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->dos:Ljava/io/DataOutputStream;

    invoke-virtual {v1}, Ljava/io/DataOutputStream;->flush()V

    .line 1354
    iget-object v1, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->dos:Ljava/io/DataOutputStream;

    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V

    .line 1356
    :cond_2
    iget-object v1, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->fileBuf:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 1357
    sget-boolean v1, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v1, :cond_3

    iget-object v1, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->this$0:Lorg/apache/http/impl/client/TwoChunkInputStream;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "resource check: buffered file removed "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->fileBuf:Ljava/io/File;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    # invokes: Lorg/apache/http/impl/client/TwoChunkInputStream;->log(Ljava/lang/String;)V
    invoke-static {v1, v3}, Lorg/apache/http/impl/client/TwoChunkInputStream;->access$200(Lorg/apache/http/impl/client/TwoChunkInputStream;Ljava/lang/String;)V

    .line 1358
    :cond_3
    const/4 v1, 0x0

    iput-object v1, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->fileBuf:Ljava/io/File;

    .line 1360
    :cond_4
    iget-object v1, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->this$0:Lorg/apache/http/impl/client/TwoChunkInputStream;

    # getter for: Lorg/apache/http/impl/client/TwoChunkInputStream;->parentIS:Lorg/apache/http/impl/client/MultiSocketInputStream;
    invoke-static {v1}, Lorg/apache/http/impl/client/TwoChunkInputStream;->access$000(Lorg/apache/http/impl/client/TwoChunkInputStream;)Lorg/apache/http/impl/client/MultiSocketInputStream;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/http/impl/client/MultiSocketInputStream;->clearBufferDir()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1367
    :cond_5
    :goto_0
    :try_start_1
    monitor-exit v2

    .line 1368
    return-void

    .line 1362
    :catch_0
    move-exception v0

    .line 1363
    .local v0, "ex":Ljava/lang/Throwable;
    sget-boolean v1, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v1, :cond_5

    .line 1364
    invoke-static {v0}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1367
    .end local v0    # "ex":Ljava/lang/Throwable;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method private createTempBufFile()Z
    .locals 6

    .prologue
    .line 1316
    iget-object v2, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->this$0:Lorg/apache/http/impl/client/TwoChunkInputStream;

    # getter for: Lorg/apache/http/impl/client/TwoChunkInputStream;->parentIS:Lorg/apache/http/impl/client/MultiSocketInputStream;
    invoke-static {v2}, Lorg/apache/http/impl/client/TwoChunkInputStream;->access$000(Lorg/apache/http/impl/client/TwoChunkInputStream;)Lorg/apache/http/impl/client/MultiSocketInputStream;

    move-result-object v3

    monitor-enter v3

    .line 1317
    :try_start_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ".sbBuf_"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Thread;->getId()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "_"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1318
    .local v0, "bufFileName":Ljava/lang/String;
    sget-boolean v2, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v2, :cond_0

    .line 1319
    iget-object v2, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->this$0:Lorg/apache/http/impl/client/TwoChunkInputStream;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "try to save buffer to file "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    # invokes: Lorg/apache/http/impl/client/TwoChunkInputStream;->log(Ljava/lang/String;)V
    invoke-static {v2, v4}, Lorg/apache/http/impl/client/TwoChunkInputStream;->access$200(Lorg/apache/http/impl/client/TwoChunkInputStream;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1322
    :cond_0
    :try_start_1
    iget-object v2, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->this$0:Lorg/apache/http/impl/client/TwoChunkInputStream;

    # getter for: Lorg/apache/http/impl/client/TwoChunkInputStream;->parentIS:Lorg/apache/http/impl/client/MultiSocketInputStream;
    invoke-static {v2}, Lorg/apache/http/impl/client/TwoChunkInputStream;->access$000(Lorg/apache/http/impl/client/TwoChunkInputStream;)Lorg/apache/http/impl/client/MultiSocketInputStream;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/http/impl/client/MultiSocketInputStream;->createBufferDir()V

    .line 1323
    const/4 v2, 0x0

    iget-object v4, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->this$0:Lorg/apache/http/impl/client/TwoChunkInputStream;

    # getter for: Lorg/apache/http/impl/client/TwoChunkInputStream;->parentIS:Lorg/apache/http/impl/client/MultiSocketInputStream;
    invoke-static {v4}, Lorg/apache/http/impl/client/TwoChunkInputStream;->access$000(Lorg/apache/http/impl/client/TwoChunkInputStream;)Lorg/apache/http/impl/client/MultiSocketInputStream;

    move-result-object v4

    iget-object v4, v4, Lorg/apache/http/impl/client/MultiSocketInputStream;->parentDir:Ljava/io/File;

    invoke-static {v0, v2, v4}, Ljava/io/File;->createTempFile(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;

    move-result-object v2

    iput-object v2, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->fileBuf:Ljava/io/File;

    .line 1324
    sget-boolean v2, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v2, :cond_1

    .line 1325
    iget-object v2, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->this$0:Lorg/apache/http/impl/client/TwoChunkInputStream;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "resource check: buffered file generated "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->fileBuf:Ljava/io/File;

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    # invokes: Lorg/apache/http/impl/client/TwoChunkInputStream;->log(Ljava/lang/String;)V
    invoke-static {v2, v4}, Lorg/apache/http/impl/client/TwoChunkInputStream;->access$200(Lorg/apache/http/impl/client/TwoChunkInputStream;Ljava/lang/String;)V

    .line 1327
    :cond_1
    new-instance v2, Ljava/io/DataInputStream;

    new-instance v4, Ljava/io/FileInputStream;

    iget-object v5, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->fileBuf:Ljava/io/File;

    invoke-direct {v4, v5}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v2, v4}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    iput-object v2, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->dis:Ljava/io/DataInputStream;

    .line 1328
    new-instance v2, Ljava/io/DataOutputStream;

    new-instance v4, Ljava/io/FileOutputStream;

    iget-object v5, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->fileBuf:Ljava/io/File;

    invoke-direct {v4, v5}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v2, v4}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    iput-object v2, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->dos:Ljava/io/DataOutputStream;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1329
    const/4 v2, 0x1

    :try_start_2
    monitor-exit v3

    .line 1337
    :goto_0
    return v2

    .line 1331
    :catch_0
    move-exception v1

    .line 1332
    .local v1, "e":Ljava/lang/Throwable;
    sget-boolean v2, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v2, :cond_2

    .line 1333
    invoke-static {v1}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/Throwable;)V

    .line 1335
    :cond_2
    invoke-direct {p0}, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->closeAndDelFile()V

    .line 1336
    const/4 v2, 0x0

    iput-object v2, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->fileBuf:Ljava/io/File;

    .line 1337
    const/4 v2, 0x0

    monitor-exit v3

    goto :goto_0

    .line 1339
    .end local v0    # "bufFileName":Ljava/lang/String;
    .end local v1    # "e":Ljava/lang/Throwable;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v2
.end method


# virtual methods
.method public clearBuffer()V
    .locals 0

    .prologue
    .line 1566
    invoke-direct {p0}, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->closeAndDelFile()V

    .line 1567
    return-void
.end method

.method public getHasReadLen()J
    .locals 2

    .prologue
    .line 1381
    iget-wide v0, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->hasReadLen:J

    return-wide v0
.end method

.method public getLength()J
    .locals 2

    .prologue
    .line 1374
    iget-wide v0, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->mBufferLength:J

    return-wide v0
.end method

.method public getOffset()J
    .locals 2

    .prologue
    .line 1599
    iget-wide v0, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->offset:J

    return-wide v0
.end method

.method public getRestLength()J
    .locals 2

    .prologue
    .line 1583
    iget-wide v0, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->restLen:J

    return-wide v0
.end method

.method public getToBeReadLength()J
    .locals 4

    .prologue
    .line 1591
    iget-wide v0, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->mBufferLength:J

    iget-wide v2, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->hasReadLen:J

    sub-long/2addr v0, v2

    return-wide v0
.end method

.method public isEmpty()Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 1554
    monitor-enter p0

    .line 1555
    :try_start_0
    iget-object v1, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->childChunkInput:Ljava/io/InputStream;

    if-nez v1, :cond_0

    iget-object v1, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->fileBuf:Ljava/io/File;

    if-nez v1, :cond_0

    .line 1556
    monitor-exit p0

    .line 1557
    :goto_0
    return v0

    :cond_0
    iget-wide v2, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->restLen:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-gtz v1, :cond_1

    :goto_1
    monitor-exit p0

    goto :goto_0

    .line 1558
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 1557
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public isFullRead()Z
    .locals 1

    .prologue
    .line 1574
    iget-boolean v0, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->fullRead:Z

    return v0
.end method

.method public push(I[BI)J
    .locals 10
    .param p1, "sid"    # I
    .param p2, "b"    # [B
    .param p3, "length"    # I

    .prologue
    const-wide/16 v8, -0x1

    .line 1478
    :try_start_0
    iget-object v4, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->dos:Ljava/io/DataOutputStream;

    const/4 v5, 0x0

    invoke-virtual {v4, p2, v5, p3}, Ljava/io/DataOutputStream;->write([BII)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 1537
    :goto_0
    iget-wide v4, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->hasReadLen:J

    int-to-long v6, p3

    add-long/2addr v4, v6

    iput-wide v4, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->hasReadLen:J

    .line 1538
    iget-wide v4, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->restLen:J

    int-to-long v6, p3

    add-long/2addr v4, v6

    iput-wide v4, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->restLen:J

    .line 1539
    iget-wide v4, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->offset:J

    int-to-long v6, p3

    add-long/2addr v4, v6

    iput-wide v4, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->offset:J

    .line 1540
    iget-wide v4, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->hasReadLen:J

    iget-wide v6, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->mBufferLength:J

    cmp-long v4, v4, v6

    if-ltz v4, :cond_1

    .line 1541
    sget-boolean v4, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v4, :cond_0

    .line 1542
    iget-object v4, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->this$0:Lorg/apache/http/impl/client/TwoChunkInputStream;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Buffer full read "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-wide v6, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->mBufferLength:J

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    # invokes: Lorg/apache/http/impl/client/TwoChunkInputStream;->log(Ljava/lang/String;)V
    invoke-static {v4, v5}, Lorg/apache/http/impl/client/TwoChunkInputStream;->access$200(Lorg/apache/http/impl/client/TwoChunkInputStream;Ljava/lang/String;)V

    .line 1544
    :cond_0
    const/4 v4, 0x1

    iput-boolean v4, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->fullRead:Z

    .line 1546
    :cond_1
    int-to-long v4, p3

    :goto_1
    return-wide v4

    .line 1480
    :catch_0
    move-exception v1

    .line 1481
    .local v1, "e":Ljava/lang/Throwable;
    iget-object v4, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->this$0:Lorg/apache/http/impl/client/TwoChunkInputStream;

    # getter for: Lorg/apache/http/impl/client/TwoChunkInputStream;->parentIS:Lorg/apache/http/impl/client/MultiSocketInputStream;
    invoke-static {v4}, Lorg/apache/http/impl/client/TwoChunkInputStream;->access$000(Lorg/apache/http/impl/client/TwoChunkInputStream;)Lorg/apache/http/impl/client/MultiSocketInputStream;

    move-result-object v4

    iget-boolean v4, v4, Lorg/apache/http/impl/client/MultiSocketInputStream;->mFinished:Z

    if-eqz v4, :cond_3

    .line 1482
    sget-boolean v4, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v4, :cond_2

    iget-object v4, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->this$0:Lorg/apache/http/impl/client/TwoChunkInputStream;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "resource check: buffered file is already removed since download cancelled "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->fileBuf:Ljava/io/File;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    # invokes: Lorg/apache/http/impl/client/TwoChunkInputStream;->log(Ljava/lang/String;)V
    invoke-static {v4, v5}, Lorg/apache/http/impl/client/TwoChunkInputStream;->access$200(Lorg/apache/http/impl/client/TwoChunkInputStream;Ljava/lang/String;)V

    .line 1483
    :cond_2
    int-to-long v4, p3

    goto :goto_1

    .line 1485
    :cond_3
    sget-boolean v4, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v4, :cond_4

    .line 1486
    invoke-static {v1}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/Throwable;)V

    .line 1488
    :cond_4
    const/4 v0, 0x1

    .line 1489
    .local v0, "bCreated":Z
    iget-object v4, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->fileBuf:Ljava/io/File;

    if-eqz v4, :cond_5

    iget-object v4, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->fileBuf:Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v4

    if-nez v4, :cond_8

    .line 1490
    :cond_5
    sget-boolean v4, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v4, :cond_6

    .line 1491
    iget-object v4, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->this$0:Lorg/apache/http/impl/client/TwoChunkInputStream;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "buffered file not found in push "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->fileBuf:Ljava/io/File;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    # invokes: Lorg/apache/http/impl/client/TwoChunkInputStream;->log(Ljava/lang/String;)V
    invoke-static {v4, v5}, Lorg/apache/http/impl/client/TwoChunkInputStream;->access$200(Lorg/apache/http/impl/client/TwoChunkInputStream;Ljava/lang/String;)V

    .line 1493
    :cond_6
    const/4 v0, 0x0

    .line 1494
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_2
    const/4 v4, 0x3

    if-ge v3, v4, :cond_7

    .line 1495
    invoke-direct {p0}, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->closeAndDelFile()V

    .line 1496
    invoke-direct {p0}, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->createTempBufFile()Z

    move-result v4

    if-eqz v4, :cond_c

    .line 1497
    const/4 v0, 0x1

    .line 1509
    :cond_7
    if-nez v0, :cond_8

    .line 1510
    sget-boolean v4, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v4, :cond_8

    .line 1511
    iget-object v4, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->this$0:Lorg/apache/http/impl/client/TwoChunkInputStream;

    const-string v5, "failed to create temp buffered file for 3 times "

    # invokes: Lorg/apache/http/impl/client/TwoChunkInputStream;->log(Ljava/lang/String;)V
    invoke-static {v4, v5}, Lorg/apache/http/impl/client/TwoChunkInputStream;->access$200(Lorg/apache/http/impl/client/TwoChunkInputStream;Ljava/lang/String;)V

    .line 1515
    .end local v3    # "i":I
    :cond_8
    if-eqz v0, :cond_f

    .line 1516
    sget-boolean v4, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v4, :cond_9

    iget-object v4, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->this$0:Lorg/apache/http/impl/client/TwoChunkInputStream;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "buffer file created again in push exception "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->fileBuf:Ljava/io/File;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    # invokes: Lorg/apache/http/impl/client/TwoChunkInputStream;->log(Ljava/lang/String;)V
    invoke-static {v4, v5}, Lorg/apache/http/impl/client/TwoChunkInputStream;->access$200(Lorg/apache/http/impl/client/TwoChunkInputStream;Ljava/lang/String;)V

    .line 1517
    :cond_9
    invoke-virtual {p0}, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_d

    .line 1518
    sget-boolean v4, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v4, :cond_a

    iget-object v4, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->this$0:Lorg/apache/http/impl/client/TwoChunkInputStream;

    const-string v5, "buffer is empty now, can keep on write"

    # invokes: Lorg/apache/http/impl/client/TwoChunkInputStream;->log(Ljava/lang/String;)V
    invoke-static {v4, v5}, Lorg/apache/http/impl/client/TwoChunkInputStream;->access$200(Lorg/apache/http/impl/client/TwoChunkInputStream;Ljava/lang/String;)V

    .line 1520
    :cond_a
    :try_start_1
    iget-object v4, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->dos:Ljava/io/DataOutputStream;

    const/4 v5, 0x0

    invoke-virtual {v4, p2, v5, p3}, Ljava/io/DataOutputStream;->write([BII)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_0

    .line 1522
    :catch_1
    move-exception v2

    .line 1523
    .local v2, "ex":Ljava/lang/Throwable;
    sget-boolean v4, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v4, :cond_b

    invoke-static {v2}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/Throwable;)V

    .line 1524
    :cond_b
    iget-wide v4, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->readOffset:J

    mul-long/2addr v4, v8

    goto/16 :goto_1

    .line 1501
    .end local v2    # "ex":Ljava/lang/Throwable;
    .restart local v3    # "i":I
    :cond_c
    monitor-enter p0

    .line 1503
    const-wide/16 v4, 0x1f4

    :try_start_2
    invoke-virtual {p0, v4, v5}, Ljava/lang/Object;->wait(J)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1506
    :goto_3
    :try_start_3
    monitor-exit p0

    .line 1494
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 1506
    :catchall_0
    move-exception v4

    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v4

    .line 1528
    .end local v3    # "i":I
    :cond_d
    sget-boolean v4, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v4, :cond_e

    iget-object v4, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->this$0:Lorg/apache/http/impl/client/TwoChunkInputStream;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "buffer file created again in push exception, but some bufferred data is missing "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-wide v6, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->restLen:J

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    # invokes: Lorg/apache/http/impl/client/TwoChunkInputStream;->log(Ljava/lang/String;)V
    invoke-static {v4, v5}, Lorg/apache/http/impl/client/TwoChunkInputStream;->access$200(Lorg/apache/http/impl/client/TwoChunkInputStream;Ljava/lang/String;)V

    .line 1529
    :cond_e
    iget-wide v4, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->readOffset:J

    mul-long/2addr v4, v8

    goto/16 :goto_1

    .line 1533
    :cond_f
    sget-boolean v4, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v4, :cond_10

    iget-object v4, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->this$0:Lorg/apache/http/impl/client/TwoChunkInputStream;

    const-string v5, "buffer cannot be created again"

    # invokes: Lorg/apache/http/impl/client/TwoChunkInputStream;->log(Ljava/lang/String;)V
    invoke-static {v4, v5}, Lorg/apache/http/impl/client/TwoChunkInputStream;->access$200(Lorg/apache/http/impl/client/TwoChunkInputStream;Ljava/lang/String;)V

    .line 1534
    :cond_10
    iget-wide v4, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->readOffset:J

    mul-long/2addr v4, v8

    goto/16 :goto_1

    .line 1505
    .restart local v3    # "i":I
    :catch_2
    move-exception v4

    goto :goto_3
.end method

.method public push(Ljava/io/InputStream;JJ)V
    .locals 6
    .param p1, "cin"    # Ljava/io/InputStream;
    .param p2, "s"    # J
    .param p4, "e"    # J

    .prologue
    .line 1391
    monitor-enter p0

    .line 1392
    :try_start_0
    iput-object p1, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->childChunkInput:Ljava/io/InputStream;

    .line 1393
    iput-wide p2, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->childChunkStart:J

    .line 1394
    iput-wide p4, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->childChunkEnd:J

    .line 1396
    sub-long v2, p4, p2

    const-wide/16 v4, 0x1

    add-long v0, v2, v4

    .line 1397
    .local v0, "len":J
    iget-wide v2, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->hasReadLen:J

    add-long/2addr v2, v0

    iput-wide v2, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->hasReadLen:J

    .line 1398
    iget-wide v2, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->restLen:J

    add-long/2addr v2, v0

    iput-wide v2, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->restLen:J

    .line 1399
    iget-wide v2, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->offset:J

    add-long/2addr v2, v0

    iput-wide v2, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->offset:J

    .line 1400
    iget-wide v2, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->hasReadLen:J

    iget-wide v4, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->mBufferLength:J

    cmp-long v2, v2, v4

    if-ltz v2, :cond_1

    .line 1401
    sget-boolean v2, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->this$0:Lorg/apache/http/impl/client/TwoChunkInputStream;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Buffer full read : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v4, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->mBufferLength:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    # invokes: Lorg/apache/http/impl/client/TwoChunkInputStream;->log(Ljava/lang/String;)V
    invoke-static {v2, v3}, Lorg/apache/http/impl/client/TwoChunkInputStream;->access$200(Lorg/apache/http/impl/client/TwoChunkInputStream;Ljava/lang/String;)V

    .line 1402
    :cond_0
    const/4 v2, 0x1

    iput-boolean v2, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->fullRead:Z

    .line 1404
    :cond_1
    sget-boolean v2, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v2, :cond_2

    iget-object v2, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->this$0:Lorg/apache/http/impl/client/TwoChunkInputStream;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "push inputstream to data buffer from "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "-"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p4, p5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    # invokes: Lorg/apache/http/impl/client/TwoChunkInputStream;->log(Ljava/lang/String;)V
    invoke-static {v2, v3}, Lorg/apache/http/impl/client/TwoChunkInputStream;->access$200(Lorg/apache/http/impl/client/TwoChunkInputStream;Ljava/lang/String;)V

    .line 1405
    :cond_2
    monitor-exit p0

    .line 1406
    return-void

    .line 1405
    .end local v0    # "len":J
    :catchall_0
    move-exception v2

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public read([BII)I
    .locals 10
    .param p1, "buffer"    # [B
    .param p2, "offset"    # I
    .param p3, "length"    # I

    .prologue
    const-wide/16 v8, 0x1

    .line 1418
    const/4 v1, 0x0

    .line 1420
    .local v1, "readLen":I
    :try_start_0
    invoke-virtual {p0}, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x0

    .line 1469
    :goto_0
    return v2

    .line 1421
    :cond_0
    iget-object v2, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->childChunkInput:Ljava/io/InputStream;

    if-nez v2, :cond_3

    .line 1422
    iget-object v2, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->dis:Ljava/io/DataInputStream;

    invoke-virtual {v2, p1, p2, p3}, Ljava/io/DataInputStream;->read([BII)I
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1457
    :goto_1
    if-lez v1, :cond_2

    .line 1458
    iget-wide v2, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->readOffset:J

    int-to-long v4, v1

    add-long/2addr v2, v4

    iput-wide v2, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->readOffset:J

    .line 1459
    iget-wide v2, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->restLen:J

    int-to-long v4, v1

    sub-long/2addr v2, v4

    iput-wide v2, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->restLen:J

    .line 1460
    iget-wide v2, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->restLen:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-nez v2, :cond_2

    iget-wide v2, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->hasReadLen:J

    iget-wide v4, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->mBufferLength:J

    cmp-long v2, v2, v4

    if-ltz v2, :cond_2

    .line 1461
    sget-boolean v2, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v2, :cond_1

    iget-object v2, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->this$0:Lorg/apache/http/impl/client/TwoChunkInputStream;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "resource check: finish reading chunk "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v4, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->bufOffset:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "-"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v4, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->bufOffset:J

    iget-wide v6, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->mBufferLength:J

    add-long/2addr v4, v6

    sub-long/2addr v4, v8

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " with length "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v4, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->mBufferLength:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", in which "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v4, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->bufOffset:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "-"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v4, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->childChunkStart:J

    sub-long/2addr v4, v8

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " is from cache file "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->fileBuf:Ljava/io/File;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", and "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v4, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->childChunkStart:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "-"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v4, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->childChunkEnd:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " is from child input "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->childChunkInput:Ljava/io/InputStream;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    # invokes: Lorg/apache/http/impl/client/TwoChunkInputStream;->log(Ljava/lang/String;)V
    invoke-static {v2, v3}, Lorg/apache/http/impl/client/TwoChunkInputStream;->access$200(Lorg/apache/http/impl/client/TwoChunkInputStream;Ljava/lang/String;)V

    .line 1466
    :cond_1
    invoke-direct {p0}, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->closeAndDelFile()V

    :cond_2
    move v2, v1

    .line 1469
    goto/16 :goto_0

    .line 1425
    :cond_3
    :try_start_1
    iget-wide v2, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->readOffset:J

    iget-wide v4, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->childChunkStart:J

    cmp-long v2, v2, v4

    if-gez v2, :cond_4

    .line 1426
    iget-object v2, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->dis:Ljava/io/DataInputStream;

    iget-wide v4, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->childChunkStart:J

    iget-wide v6, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->readOffset:J

    sub-long/2addr v4, v6

    long-to-int v3, v4

    invoke-static {p3, v3}, Ljava/lang/Math;->min(II)I

    move-result v3

    invoke-virtual {v2, p1, p2, v3}, Ljava/io/DataInputStream;->read([BII)I

    move-result v1

    goto/16 :goto_1

    .line 1429
    :cond_4
    iget-boolean v2, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->bSwitchedToChild:Z

    if-nez v2, :cond_6

    .line 1430
    sget-boolean v2, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v2, :cond_5

    iget-object v2, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->this$0:Lorg/apache/http/impl/client/TwoChunkInputStream;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "resource check: finish reading sec chunk "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v4, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->bufOffset:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "-"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v4, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->childChunkStart:J

    sub-long/2addr v4, v8

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " with length "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v4, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->childChunkStart:J

    iget-wide v6, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->bufOffset:J

    sub-long/2addr v4, v6

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " from cache file "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->fileBuf:Ljava/io/File;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", now bytesRemaining data "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v4, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->childChunkStart:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "-"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v4, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->childChunkEnd:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " will be read from child input "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->childChunkInput:Ljava/io/InputStream;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    # invokes: Lorg/apache/http/impl/client/TwoChunkInputStream;->log(Ljava/lang/String;)V
    invoke-static {v2, v3}, Lorg/apache/http/impl/client/TwoChunkInputStream;->access$200(Lorg/apache/http/impl/client/TwoChunkInputStream;Ljava/lang/String;)V

    .line 1434
    :cond_5
    invoke-direct {p0}, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->closeAndDelFile()V

    .line 1435
    const/4 v2, 0x1

    iput-boolean v2, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->bSwitchedToChild:Z

    .line 1437
    :cond_6
    sget-boolean v2, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v2, :cond_7

    .line 1438
    iget-wide v2, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->readOffset:J

    iget-wide v4, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->childChunkStart:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_7

    iget-object v2, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->childChunkInput:Ljava/io/InputStream;

    if-eqz v2, :cond_7

    .line 1439
    iget-object v2, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->this$0:Lorg/apache/http/impl/client/TwoChunkInputStream;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "now start to read from childChunkInput "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->childChunkInput:Ljava/io/InputStream;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    # invokes: Lorg/apache/http/impl/client/TwoChunkInputStream;->log(Ljava/lang/String;)V
    invoke-static {v2, v3}, Lorg/apache/http/impl/client/TwoChunkInputStream;->access$200(Lorg/apache/http/impl/client/TwoChunkInputStream;Ljava/lang/String;)V

    .line 1442
    :cond_7
    iget-object v2, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->childChunkInput:Ljava/io/InputStream;

    instance-of v2, v2, Lorg/apache/http/impl/client/TwoChunkInputStream;

    if-eqz v2, :cond_9

    iget-object v2, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->childChunkInput:Ljava/io/InputStream;

    check-cast v2, Lorg/apache/http/impl/client/TwoChunkInputStream;

    invoke-virtual {v2}, Lorg/apache/http/impl/client/TwoChunkInputStream;->isClosed()Z

    move-result v2

    if-eqz v2, :cond_9

    .line 1443
    sget-boolean v2, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v2, :cond_8

    iget-object v2, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->this$0:Lorg/apache/http/impl/client/TwoChunkInputStream;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "childChunkInput is closed : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->childChunkInput:Ljava/io/InputStream;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    # invokes: Lorg/apache/http/impl/client/TwoChunkInputStream;->log(Ljava/lang/String;)V
    invoke-static {v2, v3}, Lorg/apache/http/impl/client/TwoChunkInputStream;->access$200(Lorg/apache/http/impl/client/TwoChunkInputStream;Ljava/lang/String;)V

    .line 1444
    :cond_8
    const/4 v2, -0x1

    goto/16 :goto_0

    .line 1446
    :cond_9
    iget-object v2, p0, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->childChunkInput:Ljava/io/InputStream;

    invoke-virtual {v2, p1, p2, p3}, Ljava/io/InputStream;->read([BII)I
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    move-result v1

    goto/16 :goto_1

    .line 1450
    :catch_0
    move-exception v0

    .line 1451
    .local v0, "e":Ljava/lang/Throwable;
    sget-boolean v2, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v2, :cond_a

    .line 1452
    invoke-static {v0}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/Throwable;)V

    .line 1454
    :cond_a
    invoke-direct {p0}, Lorg/apache/http/impl/client/TwoChunkInputStream$SimpleDataBuffer;->closeAndDelFile()V

    .line 1455
    const/4 v1, -0x1

    goto/16 :goto_1
.end method
