.class final Lorg/apache/http/impl/client/CustomHttpClient$2;
.super Ljava/lang/Object;
.source "CustomHttpClient.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/http/impl/client/CustomHttpClient;->getAddrsByHost(JLjava/lang/String;ILjava/lang/String;)[Ljava/net/InetAddress;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$host:Ljava/lang/String;

.field final synthetic val$retAddrs:Ljava/util/LinkedList;

.field final synthetic val$threadID:J

.field final synthetic val$timeOut:J


# direct methods
.method constructor <init>(JLjava/lang/String;JLjava/util/LinkedList;)V
    .locals 0

    .prologue
    .line 756
    iput-wide p1, p0, Lorg/apache/http/impl/client/CustomHttpClient$2;->val$threadID:J

    iput-object p3, p0, Lorg/apache/http/impl/client/CustomHttpClient$2;->val$host:Ljava/lang/String;

    iput-wide p4, p0, Lorg/apache/http/impl/client/CustomHttpClient$2;->val$timeOut:J

    iput-object p6, p0, Lorg/apache/http/impl/client/CustomHttpClient$2;->val$retAddrs:Ljava/util/LinkedList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 12

    .prologue
    .line 758
    sget-boolean v7, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v7, :cond_0

    const-string v7, "SBServiceAPI: start run1: get from Service: "

    invoke-static {v7}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 759
    :cond_0
    const/4 v6, 0x0

    .line 761
    .local v6, "responseGetAllByName":Ljava/lang/reflect/Method;
    :try_start_0
    # invokes: Lorg/apache/http/impl/client/CustomHttpClient;->getService()V
    invoke-static {}, Lorg/apache/http/impl/client/CustomHttpClient;->access$000()V

    .line 762
    sget-object v7, Lorg/apache/http/impl/client/CustomHttpClient;->sIConnectivityManagerClass:Ljava/lang/Class;

    const-string v8, "requestGetAllByName"

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Class;

    const/4 v10, 0x0

    sget-object v11, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    aput-object v11, v9, v10

    const/4 v10, 0x1

    const-class v11, Ljava/lang/String;

    aput-object v11, v9, v10

    invoke-virtual {v7, v8, v9}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v5

    .line 763
    .local v5, "requestGetAllByName":Ljava/lang/reflect/Method;
    sget-boolean v7, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v7, :cond_1

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "SBServiceAPI: requestGetAllByName "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-wide v8, p0, Lorg/apache/http/impl/client/CustomHttpClient$2;->val$threadID:J

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ","

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lorg/apache/http/impl/client/CustomHttpClient$2;->val$host:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 764
    :cond_1
    sget-object v7, Lorg/apache/http/impl/client/CustomHttpClient;->sIConnectivityManager:Ljava/lang/Object;

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    iget-wide v10, p0, Lorg/apache/http/impl/client/CustomHttpClient$2;->val$threadID:J

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x1

    iget-object v10, p0, Lorg/apache/http/impl/client/CustomHttpClient$2;->val$host:Ljava/lang/String;

    aput-object v10, v8, v9

    invoke-virtual {v5, v7, v8}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 766
    sget-object v7, Lorg/apache/http/impl/client/CustomHttpClient;->sIConnectivityManagerClass:Ljava/lang/Class;

    const-string v8, "responseGetAllByName"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Class;

    const/4 v10, 0x0

    sget-object v11, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    aput-object v11, v9, v10

    invoke-virtual {v7, v8, v9}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 772
    if-nez v6, :cond_4

    .line 812
    .end local v5    # "requestGetAllByName":Ljava/lang/reflect/Method;
    :cond_2
    :goto_0
    return-void

    .line 768
    :catch_0
    move-exception v2

    .line 769
    .local v2, "e":Ljava/lang/Throwable;
    sget-boolean v7, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v7, :cond_2

    invoke-static {v2}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 810
    .end local v2    # "e":Ljava/lang/Throwable;
    .local v4, "ipAddrs":[Ljava/lang/String;
    .restart local v5    # "requestGetAllByName":Ljava/lang/reflect/Method;
    :cond_3
    :try_start_1
    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 773
    .end local v4    # "ipAddrs":[Ljava/lang/String;
    :cond_4
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    iget-wide v10, p0, Lorg/apache/http/impl/client/CustomHttpClient$2;->val$timeOut:J

    cmp-long v7, v8, v10

    if-gez v7, :cond_2

    .line 774
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v8

    monitor-enter v8

    .line 776
    :try_start_2
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v7

    const-wide/16 v10, 0x14

    invoke-virtual {v7, v10, v11}, Ljava/lang/Object;->wait(J)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 781
    :cond_5
    :goto_1
    :try_start_3
    monitor-exit v8
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 783
    const/4 v4, 0x0

    .line 785
    .restart local v4    # "ipAddrs":[Ljava/lang/String;
    :try_start_4
    sget-object v7, Lorg/apache/http/impl/client/CustomHttpClient;->sIConnectivityManager:Ljava/lang/Object;

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    iget-wide v10, p0, Lorg/apache/http/impl/client/CustomHttpClient$2;->val$threadID:J

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-virtual {v6, v7, v8}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, [Ljava/lang/String;

    move-object v0, v7

    check-cast v0, [Ljava/lang/String;

    move-object v4, v0
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_2

    .line 790
    :cond_6
    :goto_2
    iget-object v8, p0, Lorg/apache/http/impl/client/CustomHttpClient$2;->val$retAddrs:Ljava/util/LinkedList;

    monitor-enter v8

    .line 791
    :try_start_5
    iget-object v7, p0, Lorg/apache/http/impl/client/CustomHttpClient$2;->val$retAddrs:Ljava/util/LinkedList;

    invoke-virtual {v7}, Ljava/util/LinkedList;->size()I

    move-result v7

    if-lez v7, :cond_7

    .line 792
    monitor-exit v8

    goto :goto_0

    .line 810
    :catchall_0
    move-exception v7

    monitor-exit v8
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    throw v7

    .line 778
    .end local v4    # "ipAddrs":[Ljava/lang/String;
    :catch_1
    move-exception v2

    .line 779
    .restart local v2    # "e":Ljava/lang/Throwable;
    :try_start_6
    sget-boolean v7, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v7, :cond_5

    invoke-static {v2}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/Throwable;)V

    goto :goto_1

    .line 781
    .end local v2    # "e":Ljava/lang/Throwable;
    :catchall_1
    move-exception v7

    monitor-exit v8
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    throw v7

    .line 787
    .restart local v4    # "ipAddrs":[Ljava/lang/String;
    :catch_2
    move-exception v2

    .line 788
    .restart local v2    # "e":Ljava/lang/Throwable;
    sget-boolean v7, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v7, :cond_6

    invoke-static {v2}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/Throwable;)V

    goto :goto_2

    .line 794
    .end local v2    # "e":Ljava/lang/Throwable;
    :cond_7
    if-eqz v4, :cond_3

    :try_start_7
    array-length v7, v4

    if-lez v7, :cond_3

    const/4 v7, 0x0

    aget-object v7, v4, v7

    if-eqz v7, :cond_3

    .line 795
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_3
    array-length v7, v4

    if-ge v3, v7, :cond_9

    .line 796
    aget-object v7, v4, v3
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    if-eqz v7, :cond_9

    .line 798
    :try_start_8
    iget-object v7, p0, Lorg/apache/http/impl/client/CustomHttpClient$2;->val$retAddrs:Ljava/util/LinkedList;

    aget-object v9, v4, v3

    invoke-static {v9}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v9

    invoke-virtual {v7, v9}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 799
    sget-boolean v7, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v7, :cond_8

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "SBServiceAPI: responseGetAllByName "

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    aget-object v9, v4, v3

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V
    :try_end_8
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_8} :catch_3
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 795
    :cond_8
    :goto_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 801
    :catch_3
    move-exception v2

    .line 802
    .restart local v2    # "e":Ljava/lang/Throwable;
    :try_start_9
    sget-boolean v7, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v7, :cond_8

    invoke-static {v2}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/Throwable;)V

    goto :goto_4

    .line 807
    .end local v2    # "e":Ljava/lang/Throwable;
    :cond_9
    iget-object v7, p0, Lorg/apache/http/impl/client/CustomHttpClient$2;->val$retAddrs:Ljava/util/LinkedList;

    invoke-virtual {v7}, Ljava/lang/Object;->notifyAll()V

    .line 808
    monitor-exit v8
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto/16 :goto_0
.end method
