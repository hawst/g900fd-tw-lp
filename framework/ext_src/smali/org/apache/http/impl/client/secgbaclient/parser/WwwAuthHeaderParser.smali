.class public Lorg/apache/http/impl/client/secgbaclient/parser/WwwAuthHeaderParser;
.super Lorg/apache/http/impl/client/secgbaclient/parser/HttpHeaderParser;
.source "WwwAuthHeaderParser.java"


# static fields
.field private static final Log:Lorg/apache/http/impl/client/secgbaclient/util/GbaLogger;

.field private static final TAG:Ljava/lang/String; = "WwwAuthHeaderParser"


# instance fields
.field private paramSplitHeader:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 30
    new-instance v0, Lorg/apache/http/impl/client/secgbaclient/util/GbaLogger;

    const-string v1, "WwwAuthHeaderParser"

    invoke-direct {v0, v1}, Lorg/apache/http/impl/client/secgbaclient/util/GbaLogger;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/http/impl/client/secgbaclient/parser/WwwAuthHeaderParser;->Log:Lorg/apache/http/impl/client/secgbaclient/util/GbaLogger;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0}, Lorg/apache/http/impl/client/secgbaclient/parser/HttpHeaderParser;-><init>()V

    .line 31
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/http/impl/client/secgbaclient/parser/WwwAuthHeaderParser;->paramSplitHeader:Ljava/lang/String;

    .line 39
    return-void
.end method

.method private parse(Lorg/apache/http/impl/client/secgbaclient/WwwAuthenticateHeader;Ljava/lang/String;)V
    .locals 3
    .param p1, "parsedAuthHeader"    # Lorg/apache/http/impl/client/secgbaclient/WwwAuthenticateHeader;
    .param p2, "headerVal"    # Ljava/lang/String;

    .prologue
    .line 91
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    .line 93
    invoke-direct {p0, p1, p2}, Lorg/apache/http/impl/client/secgbaclient/parser/WwwAuthHeaderParser;->setScheme(Lorg/apache/http/impl/client/secgbaclient/WwwAuthenticateHeader;Ljava/lang/String;)V

    .line 94
    invoke-direct {p0, p1, p2}, Lorg/apache/http/impl/client/secgbaclient/parser/WwwAuthHeaderParser;->setRealm(Lorg/apache/http/impl/client/secgbaclient/WwwAuthenticateHeader;Ljava/lang/String;)V

    .line 95
    invoke-direct {p0, p1, p2}, Lorg/apache/http/impl/client/secgbaclient/parser/WwwAuthHeaderParser;->setNonce(Lorg/apache/http/impl/client/secgbaclient/WwwAuthenticateHeader;Ljava/lang/String;)V

    .line 96
    invoke-direct {p0, p1, p2}, Lorg/apache/http/impl/client/secgbaclient/parser/WwwAuthHeaderParser;->setAlgorithm(Lorg/apache/http/impl/client/secgbaclient/WwwAuthenticateHeader;Ljava/lang/String;)V

    .line 97
    invoke-direct {p0, p1, p2}, Lorg/apache/http/impl/client/secgbaclient/parser/WwwAuthHeaderParser;->setQop(Lorg/apache/http/impl/client/secgbaclient/WwwAuthenticateHeader;Ljava/lang/String;)V

    .line 98
    invoke-direct {p0, p1, p2}, Lorg/apache/http/impl/client/secgbaclient/parser/WwwAuthHeaderParser;->setStale(Lorg/apache/http/impl/client/secgbaclient/WwwAuthenticateHeader;Ljava/lang/String;)V

    .line 99
    invoke-direct {p0, p1, p2}, Lorg/apache/http/impl/client/secgbaclient/parser/WwwAuthHeaderParser;->setOpaque(Lorg/apache/http/impl/client/secgbaclient/WwwAuthenticateHeader;Ljava/lang/String;)V

    .line 101
    invoke-virtual {p1}, Lorg/apache/http/impl/client/secgbaclient/WwwAuthenticateHeader;->toString()Ljava/lang/String;

    .line 108
    :goto_0
    return-void

    .line 105
    :cond_0
    sget-object v0, Lorg/apache/http/impl/client/secgbaclient/parser/WwwAuthHeaderParser;->Log:Lorg/apache/http/impl/client/secgbaclient/util/GbaLogger;

    const-string v1, "WwwAuthHeaderParser"

    const-string v2, "Null Arguments"

    invoke-virtual {v0, v1, v2}, Lorg/apache/http/impl/client/secgbaclient/util/GbaLogger;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private setAlgorithm(Lorg/apache/http/impl/client/secgbaclient/WwwAuthenticateHeader;Ljava/lang/String;)V
    .locals 2
    .param p1, "parsedAuthHeader"    # Lorg/apache/http/impl/client/secgbaclient/WwwAuthenticateHeader;
    .param p2, "headerVal"    # Ljava/lang/String;

    .prologue
    .line 163
    const/4 v0, 0x0

    .line 166
    .local v0, "algorithmVal":Ljava/lang/String;
    const-string v1, "algorithm[\\s]*="

    invoke-virtual {p0, v1, p2}, Lorg/apache/http/impl/client/secgbaclient/parser/WwwAuthHeaderParser;->getSplitHeader(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/http/impl/client/secgbaclient/parser/WwwAuthHeaderParser;->paramSplitHeader:Ljava/lang/String;

    .line 168
    iget-object v1, p0, Lorg/apache/http/impl/client/secgbaclient/parser/WwwAuthHeaderParser;->paramSplitHeader:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 170
    iget-object v1, p0, Lorg/apache/http/impl/client/secgbaclient/parser/WwwAuthHeaderParser;->paramSplitHeader:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lorg/apache/http/impl/client/secgbaclient/parser/WwwAuthHeaderParser;->getParamValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 174
    :cond_0
    invoke-virtual {p1, v0}, Lorg/apache/http/impl/client/secgbaclient/WwwAuthenticateHeader;->setAlgorithm(Ljava/lang/String;)V

    .line 175
    return-void
.end method

.method private setNonce(Lorg/apache/http/impl/client/secgbaclient/WwwAuthenticateHeader;Ljava/lang/String;)V
    .locals 2
    .param p1, "parsedAuthHeader"    # Lorg/apache/http/impl/client/secgbaclient/WwwAuthenticateHeader;
    .param p2, "headerVal"    # Ljava/lang/String;

    .prologue
    .line 148
    const/4 v0, 0x0

    .line 151
    .local v0, "nonceVal":Ljava/lang/String;
    const-string v1, "nonce[\\s]*="

    invoke-virtual {p0, v1, p2}, Lorg/apache/http/impl/client/secgbaclient/parser/WwwAuthHeaderParser;->getSplitHeader(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/http/impl/client/secgbaclient/parser/WwwAuthHeaderParser;->paramSplitHeader:Ljava/lang/String;

    .line 153
    iget-object v1, p0, Lorg/apache/http/impl/client/secgbaclient/parser/WwwAuthHeaderParser;->paramSplitHeader:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 155
    iget-object v1, p0, Lorg/apache/http/impl/client/secgbaclient/parser/WwwAuthHeaderParser;->paramSplitHeader:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lorg/apache/http/impl/client/secgbaclient/parser/WwwAuthHeaderParser;->getParamValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 158
    :cond_0
    invoke-virtual {p1, v0}, Lorg/apache/http/impl/client/secgbaclient/WwwAuthenticateHeader;->setNonce(Ljava/lang/String;)V

    .line 159
    return-void
.end method

.method private setOpaque(Lorg/apache/http/impl/client/secgbaclient/WwwAuthenticateHeader;Ljava/lang/String;)V
    .locals 2
    .param p1, "parsedAuthHeader"    # Lorg/apache/http/impl/client/secgbaclient/WwwAuthenticateHeader;
    .param p2, "headerVal"    # Ljava/lang/String;

    .prologue
    .line 195
    const/4 v0, 0x0

    .line 198
    .local v0, "opaqueVal":Ljava/lang/String;
    const-string v1, "opaque[\\s]*="

    invoke-virtual {p0, v1, p2}, Lorg/apache/http/impl/client/secgbaclient/parser/WwwAuthHeaderParser;->getSplitHeader(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/http/impl/client/secgbaclient/parser/WwwAuthHeaderParser;->paramSplitHeader:Ljava/lang/String;

    .line 200
    iget-object v1, p0, Lorg/apache/http/impl/client/secgbaclient/parser/WwwAuthHeaderParser;->paramSplitHeader:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 202
    iget-object v1, p0, Lorg/apache/http/impl/client/secgbaclient/parser/WwwAuthHeaderParser;->paramSplitHeader:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lorg/apache/http/impl/client/secgbaclient/parser/WwwAuthHeaderParser;->getParamValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 206
    :cond_0
    invoke-virtual {p1, v0}, Lorg/apache/http/impl/client/secgbaclient/WwwAuthenticateHeader;->setOpaque(Ljava/lang/String;)V

    .line 207
    return-void
.end method

.method private setQop(Lorg/apache/http/impl/client/secgbaclient/WwwAuthenticateHeader;Ljava/lang/String;)V
    .locals 2
    .param p1, "parsedAuthHeader"    # Lorg/apache/http/impl/client/secgbaclient/WwwAuthenticateHeader;
    .param p2, "headerVal"    # Ljava/lang/String;

    .prologue
    .line 179
    const/4 v0, 0x0

    .line 182
    .local v0, "qopVal":Ljava/lang/String;
    const-string v1, "qop[\\s]*="

    invoke-virtual {p0, v1, p2}, Lorg/apache/http/impl/client/secgbaclient/parser/WwwAuthHeaderParser;->getSplitHeader(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/http/impl/client/secgbaclient/parser/WwwAuthHeaderParser;->paramSplitHeader:Ljava/lang/String;

    .line 184
    iget-object v1, p0, Lorg/apache/http/impl/client/secgbaclient/parser/WwwAuthHeaderParser;->paramSplitHeader:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 186
    iget-object v1, p0, Lorg/apache/http/impl/client/secgbaclient/parser/WwwAuthHeaderParser;->paramSplitHeader:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lorg/apache/http/impl/client/secgbaclient/parser/WwwAuthHeaderParser;->getParamValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 190
    :cond_0
    invoke-virtual {p1, v0}, Lorg/apache/http/impl/client/secgbaclient/WwwAuthenticateHeader;->setQop(Ljava/lang/String;)V

    .line 191
    return-void
.end method

.method private setRealm(Lorg/apache/http/impl/client/secgbaclient/WwwAuthenticateHeader;Ljava/lang/String;)V
    .locals 2
    .param p1, "parsedAuthHeader"    # Lorg/apache/http/impl/client/secgbaclient/WwwAuthenticateHeader;
    .param p2, "headerVal"    # Ljava/lang/String;

    .prologue
    .line 132
    const/4 v0, 0x0

    .line 135
    .local v0, "realmVal":Ljava/lang/String;
    const-string v1, "realm[\\s]*="

    invoke-virtual {p0, v1, p2}, Lorg/apache/http/impl/client/secgbaclient/parser/WwwAuthHeaderParser;->getSplitHeader(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/http/impl/client/secgbaclient/parser/WwwAuthHeaderParser;->paramSplitHeader:Ljava/lang/String;

    .line 137
    iget-object v1, p0, Lorg/apache/http/impl/client/secgbaclient/parser/WwwAuthHeaderParser;->paramSplitHeader:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 139
    iget-object v1, p0, Lorg/apache/http/impl/client/secgbaclient/parser/WwwAuthHeaderParser;->paramSplitHeader:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lorg/apache/http/impl/client/secgbaclient/parser/WwwAuthHeaderParser;->getParamValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 142
    :cond_0
    invoke-virtual {p1, v0}, Lorg/apache/http/impl/client/secgbaclient/WwwAuthenticateHeader;->setRealm(Ljava/lang/String;)V

    .line 144
    return-void
.end method

.method private setScheme(Lorg/apache/http/impl/client/secgbaclient/WwwAuthenticateHeader;Ljava/lang/String;)V
    .locals 1
    .param p1, "parsedAuthHeader"    # Lorg/apache/http/impl/client/secgbaclient/WwwAuthenticateHeader;
    .param p2, "headerVal"    # Ljava/lang/String;

    .prologue
    .line 113
    const-string v0, "Digest"

    invoke-virtual {p2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 116
    const-string v0, "Digest"

    invoke-virtual {p1, v0}, Lorg/apache/http/impl/client/secgbaclient/WwwAuthenticateHeader;->setScheme(Ljava/lang/String;)V

    .line 128
    :goto_0
    return-void

    .line 118
    :cond_0
    const-string v0, "Basic"

    invoke-virtual {p2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 121
    const-string v0, "Basic"

    invoke-virtual {p1, v0}, Lorg/apache/http/impl/client/secgbaclient/WwwAuthenticateHeader;->setScheme(Ljava/lang/String;)V

    goto :goto_0

    .line 126
    :cond_1
    const-string v0, "Unknown"

    invoke-virtual {p1, v0}, Lorg/apache/http/impl/client/secgbaclient/WwwAuthenticateHeader;->setScheme(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private setStale(Lorg/apache/http/impl/client/secgbaclient/WwwAuthenticateHeader;Ljava/lang/String;)V
    .locals 2
    .param p1, "parsedAuthHeader"    # Lorg/apache/http/impl/client/secgbaclient/WwwAuthenticateHeader;
    .param p2, "headerVal"    # Ljava/lang/String;

    .prologue
    .line 211
    const/4 v0, 0x0

    .line 214
    .local v0, "staleVal":Ljava/lang/String;
    const-string v1, "stale[\\s]*="

    invoke-virtual {p0, v1, p2}, Lorg/apache/http/impl/client/secgbaclient/parser/WwwAuthHeaderParser;->getSplitHeader(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/http/impl/client/secgbaclient/parser/WwwAuthHeaderParser;->paramSplitHeader:Ljava/lang/String;

    .line 216
    iget-object v1, p0, Lorg/apache/http/impl/client/secgbaclient/parser/WwwAuthHeaderParser;->paramSplitHeader:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 218
    iget-object v1, p0, Lorg/apache/http/impl/client/secgbaclient/parser/WwwAuthHeaderParser;->paramSplitHeader:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lorg/apache/http/impl/client/secgbaclient/parser/WwwAuthHeaderParser;->getParamValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 222
    :cond_0
    invoke-static {v0}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v1

    invoke-virtual {p1, v1}, Lorg/apache/http/impl/client/secgbaclient/WwwAuthenticateHeader;->setStale(Z)V

    .line 223
    return-void
.end method


# virtual methods
.method public WwwAuthHeaderParse(Lorg/apache/http/HttpResponse;)Lorg/apache/http/impl/client/secgbaclient/WwwAuthenticateHeader;
    .locals 6
    .param p1, "response"    # Lorg/apache/http/HttpResponse;

    .prologue
    .line 42
    const/4 v0, 0x0

    .line 43
    .local v0, "headerVal":Ljava/lang/String;
    const/4 v1, 0x0

    .line 44
    .local v1, "parseAuthHeader":Lorg/apache/http/impl/client/secgbaclient/WwwAuthenticateHeader;
    sget-object v3, Lorg/apache/http/impl/client/secgbaclient/parser/WwwAuthHeaderParser;->Log:Lorg/apache/http/impl/client/secgbaclient/util/GbaLogger;

    const-string v4, "WwwAuthHeaderParser"

    const-string v5, "Parse the Header WwwAuthorizationHeader"

    invoke-virtual {v3, v4, v5}, Lorg/apache/http/impl/client/secgbaclient/util/GbaLogger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 45
    if-nez p1, :cond_0

    .line 47
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "Http response is Null"

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 50
    :cond_0
    const-string v3, "WWW-Authenticate"

    invoke-virtual {p0, p1, v3}, Lorg/apache/http/impl/client/secgbaclient/parser/WwwAuthHeaderParser;->getHeaderValue(Lorg/apache/http/HttpResponse;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 51
    if-nez v0, :cond_1

    .line 53
    sget-object v3, Lorg/apache/http/impl/client/secgbaclient/parser/WwwAuthHeaderParser;->Log:Lorg/apache/http/impl/client/secgbaclient/util/GbaLogger;

    const-string v4, "WwwAuthHeaderParser"

    const-string v5, "AuthHeader value is Null"

    invoke-virtual {v3, v4, v5}, Lorg/apache/http/impl/client/secgbaclient/util/GbaLogger;->e(Ljava/lang/String;Ljava/lang/String;)V

    move-object v2, v1

    .line 61
    .end local v1    # "parseAuthHeader":Lorg/apache/http/impl/client/secgbaclient/WwwAuthenticateHeader;
    .local v2, "parseAuthHeader":Lorg/apache/http/impl/client/secgbaclient/WwwAuthenticateHeader;
    :goto_0
    return-object v2

    .line 57
    .end local v2    # "parseAuthHeader":Lorg/apache/http/impl/client/secgbaclient/WwwAuthenticateHeader;
    .restart local v1    # "parseAuthHeader":Lorg/apache/http/impl/client/secgbaclient/WwwAuthenticateHeader;
    :cond_1
    new-instance v1, Lorg/apache/http/impl/client/secgbaclient/WwwAuthenticateHeader;

    .end local v1    # "parseAuthHeader":Lorg/apache/http/impl/client/secgbaclient/WwwAuthenticateHeader;
    invoke-direct {v1}, Lorg/apache/http/impl/client/secgbaclient/WwwAuthenticateHeader;-><init>()V

    .line 58
    .restart local v1    # "parseAuthHeader":Lorg/apache/http/impl/client/secgbaclient/WwwAuthenticateHeader;
    invoke-direct {p0, v1, v0}, Lorg/apache/http/impl/client/secgbaclient/parser/WwwAuthHeaderParser;->parse(Lorg/apache/http/impl/client/secgbaclient/WwwAuthenticateHeader;Ljava/lang/String;)V

    .line 59
    sget-object v3, Lorg/apache/http/impl/client/secgbaclient/parser/WwwAuthHeaderParser;->Log:Lorg/apache/http/impl/client/secgbaclient/util/GbaLogger;

    const-string v4, "WwwAuthHeaderParser"

    invoke-virtual {v1}, Lorg/apache/http/impl/client/secgbaclient/WwwAuthenticateHeader;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lorg/apache/http/impl/client/secgbaclient/util/GbaLogger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    sget-object v3, Lorg/apache/http/impl/client/secgbaclient/parser/WwwAuthHeaderParser;->Log:Lorg/apache/http/impl/client/secgbaclient/util/GbaLogger;

    const-string v4, "WwwAuthHeaderParser"

    const-string v5, "Header parsing Completed "

    invoke-virtual {v3, v4, v5}, Lorg/apache/http/impl/client/secgbaclient/util/GbaLogger;->d(Ljava/lang/String;Ljava/lang/String;)V

    move-object v2, v1

    .line 61
    .end local v1    # "parseAuthHeader":Lorg/apache/http/impl/client/secgbaclient/WwwAuthenticateHeader;
    .restart local v2    # "parseAuthHeader":Lorg/apache/http/impl/client/secgbaclient/WwwAuthenticateHeader;
    goto :goto_0
.end method

.method public proxyAuthHeaderParse(Lorg/apache/http/HttpResponse;)Lorg/apache/http/impl/client/secgbaclient/WwwAuthenticateHeader;
    .locals 6
    .param p1, "response"    # Lorg/apache/http/HttpResponse;

    .prologue
    .line 67
    const/4 v0, 0x0

    .line 68
    .local v0, "headerVal":Ljava/lang/String;
    const/4 v1, 0x0

    .line 69
    .local v1, "parseAuthHeader":Lorg/apache/http/impl/client/secgbaclient/WwwAuthenticateHeader;
    sget-object v3, Lorg/apache/http/impl/client/secgbaclient/parser/WwwAuthHeaderParser;->Log:Lorg/apache/http/impl/client/secgbaclient/util/GbaLogger;

    const-string v4, "WwwAuthHeaderParser"

    const-string v5, "Parse the Header WwwAuthorizationHeader"

    invoke-virtual {v3, v4, v5}, Lorg/apache/http/impl/client/secgbaclient/util/GbaLogger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    if-nez p1, :cond_0

    .line 72
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "Http response is Null"

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 75
    :cond_0
    const-string v3, "Proxy-Authenticate"

    invoke-virtual {p0, p1, v3}, Lorg/apache/http/impl/client/secgbaclient/parser/WwwAuthHeaderParser;->getHeaderValue(Lorg/apache/http/HttpResponse;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 77
    if-nez v0, :cond_1

    .line 79
    sget-object v3, Lorg/apache/http/impl/client/secgbaclient/parser/WwwAuthHeaderParser;->Log:Lorg/apache/http/impl/client/secgbaclient/util/GbaLogger;

    const-string v4, "WwwAuthHeaderParser"

    const-string v5, "AuthHeader value is Null"

    invoke-virtual {v3, v4, v5}, Lorg/apache/http/impl/client/secgbaclient/util/GbaLogger;->e(Ljava/lang/String;Ljava/lang/String;)V

    move-object v2, v1

    .line 87
    .end local v1    # "parseAuthHeader":Lorg/apache/http/impl/client/secgbaclient/WwwAuthenticateHeader;
    .local v2, "parseAuthHeader":Lorg/apache/http/impl/client/secgbaclient/WwwAuthenticateHeader;
    :goto_0
    return-object v2

    .line 83
    .end local v2    # "parseAuthHeader":Lorg/apache/http/impl/client/secgbaclient/WwwAuthenticateHeader;
    .restart local v1    # "parseAuthHeader":Lorg/apache/http/impl/client/secgbaclient/WwwAuthenticateHeader;
    :cond_1
    new-instance v1, Lorg/apache/http/impl/client/secgbaclient/WwwAuthenticateHeader;

    .end local v1    # "parseAuthHeader":Lorg/apache/http/impl/client/secgbaclient/WwwAuthenticateHeader;
    invoke-direct {v1}, Lorg/apache/http/impl/client/secgbaclient/WwwAuthenticateHeader;-><init>()V

    .line 84
    .restart local v1    # "parseAuthHeader":Lorg/apache/http/impl/client/secgbaclient/WwwAuthenticateHeader;
    invoke-direct {p0, v1, v0}, Lorg/apache/http/impl/client/secgbaclient/parser/WwwAuthHeaderParser;->parse(Lorg/apache/http/impl/client/secgbaclient/WwwAuthenticateHeader;Ljava/lang/String;)V

    .line 85
    sget-object v3, Lorg/apache/http/impl/client/secgbaclient/parser/WwwAuthHeaderParser;->Log:Lorg/apache/http/impl/client/secgbaclient/util/GbaLogger;

    const-string v4, "WwwAuthHeaderParser"

    invoke-virtual {v1}, Lorg/apache/http/impl/client/secgbaclient/WwwAuthenticateHeader;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lorg/apache/http/impl/client/secgbaclient/util/GbaLogger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 86
    sget-object v3, Lorg/apache/http/impl/client/secgbaclient/parser/WwwAuthHeaderParser;->Log:Lorg/apache/http/impl/client/secgbaclient/util/GbaLogger;

    const-string v4, "WwwAuthHeaderParser"

    const-string v5, "Header parsing Completed "

    invoke-virtual {v3, v4, v5}, Lorg/apache/http/impl/client/secgbaclient/util/GbaLogger;->d(Ljava/lang/String;Ljava/lang/String;)V

    move-object v2, v1

    .line 87
    .end local v1    # "parseAuthHeader":Lorg/apache/http/impl/client/secgbaclient/WwwAuthenticateHeader;
    .restart local v2    # "parseAuthHeader":Lorg/apache/http/impl/client/secgbaclient/WwwAuthenticateHeader;
    goto :goto_0
.end method
