.class Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;
.super Ljava/lang/Object;
.source "MultiSocketInputStream.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/http/impl/client/MultiSocketInputStream;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "DataBuffer"
.end annotation


# instance fields
.field private bSavedInFile:Z

.field private bSwitchedToChild:Z

.field private final bufOffset:J

.field private bufStartReadTime:J

.field private childChunkEnd:J

.field private childChunkInput:Ljava/io/InputStream;

.field private childChunkStart:J

.field private dis:Ljava/io/DataInputStream;

.field private dos:Ljava/io/DataOutputStream;

.field private fileBuf:Ljava/io/File;

.field private firstBlockOffset:I

.field private fullRead:Z

.field private hasReadLen:I

.field private hasReadLenForSpeed:I

.field private lastTime:J

.field private mBuffer:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<[B>;"
        }
    .end annotation
.end field

.field private final mBufferLength:I

.field private final mID:I

.field private offset:J

.field private readOffset:J

.field private restLen:I

.field private sockID:I

.field private startTime:J

.field final synthetic this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

.field private totalContinuousChunk:I


# direct methods
.method public constructor <init>(Lorg/apache/http/impl/client/MultiSocketInputStream;JIILjava/util/LinkedList;II)V
    .locals 6
    .param p2, "start"    # J
    .param p4, "id"    # I
    .param p5, "len"    # I
    .param p7, "sID"    # I
    .param p8, "totalBlocks"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JII",
            "Ljava/util/LinkedList",
            "<[B>;II)V"
        }
    .end annotation

    .prologue
    .line 3936
    .local p6, "buffer":Ljava/util/LinkedList;, "Ljava/util/LinkedList<[B>;"
    iput-object p1, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3878
    const/4 v3, 0x0

    iput-object v3, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->dis:Ljava/io/DataInputStream;

    .line 3882
    const/4 v3, 0x0

    iput-object v3, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->dos:Ljava/io/DataOutputStream;

    .line 3886
    const/4 v3, 0x0

    iput-object v3, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->fileBuf:Ljava/io/File;

    .line 3891
    const/4 v3, 0x0

    iput v3, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->firstBlockOffset:I

    .line 3895
    const/4 v3, 0x0

    iput-object v3, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->childChunkInput:Ljava/io/InputStream;

    .line 3899
    const-wide/16 v4, 0x0

    iput-wide v4, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->childChunkStart:J

    .line 3903
    const-wide/16 v4, -0x1

    iput-wide v4, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->childChunkEnd:J

    .line 3915
    const/4 v3, 0x0

    iput-boolean v3, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->bSwitchedToChild:Z

    .line 3916
    const-wide/16 v4, 0x0

    iput-wide v4, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->bufStartReadTime:J

    .line 3937
    iput p4, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->mID:I

    .line 3938
    iput p7, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->sockID:I

    .line 3939
    iput p5, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->mBufferLength:I

    .line 3940
    iput-object p6, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->mBuffer:Ljava/util/LinkedList;

    .line 3941
    const/4 v3, 0x0

    iput-boolean v3, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->fullRead:Z

    .line 3942
    const/4 v3, 0x0

    iput v3, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->hasReadLen:I

    .line 3943
    const/4 v3, 0x0

    iput v3, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->restLen:I

    .line 3944
    const-wide/16 v4, 0x0

    iput-wide v4, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->startTime:J

    .line 3945
    const-wide/16 v4, 0x0

    iput-wide v4, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->lastTime:J

    .line 3946
    iput-wide p2, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->offset:J

    .line 3947
    iput-wide p2, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->readOffset:J

    .line 3948
    iput-wide p2, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->bufOffset:J

    .line 3949
    iput p8, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->totalContinuousChunk:I

    .line 3950
    const/4 v3, 0x0

    iput-boolean v3, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->bSavedInFile:Z

    .line 3952
    const/4 v0, 0x0

    .line 3953
    .local v0, "bCreated":Z
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    const/4 v3, 0x3

    if-ge v1, v3, :cond_0

    .line 3954
    invoke-direct {p0}, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->createTempBufFile()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 3955
    const/4 v0, 0x1

    .line 3968
    :cond_0
    iput-boolean v0, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->bSavedInFile:Z

    .line 3970
    return-void

    .line 3959
    :cond_1
    new-instance v2, Ljava/lang/Object;

    invoke-direct {v2}, Ljava/lang/Object;-><init>()V

    .line 3960
    .local v2, "obj":Ljava/lang/Object;
    monitor-enter v2

    .line 3962
    const-wide/16 v4, 0x1f4

    :try_start_0
    invoke-virtual {v2, v4, v5}, Ljava/lang/Object;->wait(J)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3965
    :goto_1
    :try_start_1
    monitor-exit v2

    .line 3953
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 3965
    :catchall_0
    move-exception v3

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v3

    .line 3964
    :catch_0
    move-exception v3

    goto :goto_1
.end method

.method private closeAndDelFile()V
    .locals 4

    .prologue
    .line 4007
    iget-object v2, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    monitor-enter v2

    .line 4009
    :try_start_0
    sget-boolean v1, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v1, :cond_0

    .line 4010
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "close and delete tmp buffer file "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->fileBuf:Ljava/io/File;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 4012
    :cond_0
    iget-object v1, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->fileBuf:Ljava/io/File;

    if-eqz v1, :cond_4

    .line 4013
    iget-object v1, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->dis:Ljava/io/DataInputStream;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->dis:Ljava/io/DataInputStream;

    invoke-virtual {v1}, Ljava/io/DataInputStream;->close()V

    .line 4014
    :cond_1
    iget-object v1, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->dos:Ljava/io/DataOutputStream;

    if-eqz v1, :cond_2

    .line 4015
    iget-object v1, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->dos:Ljava/io/DataOutputStream;

    invoke-virtual {v1}, Ljava/io/DataOutputStream;->flush()V

    .line 4016
    iget-object v1, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->dos:Ljava/io/DataOutputStream;

    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V

    .line 4018
    :cond_2
    iget-object v1, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->fileBuf:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 4019
    sget-boolean v1, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v1, :cond_3

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "resource check: buffered file removed (main) "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->fileBuf:Ljava/io/File;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 4020
    :cond_3
    const/4 v1, 0x0

    iput-object v1, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->fileBuf:Ljava/io/File;

    .line 4022
    :cond_4
    iget-object v1, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    invoke-virtual {v1}, Lorg/apache/http/impl/client/MultiSocketInputStream;->clearBufferDir()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4029
    :cond_5
    :goto_0
    :try_start_1
    monitor-exit v2

    .line 4030
    return-void

    .line 4024
    :catch_0
    move-exception v0

    .line 4025
    .local v0, "ex":Ljava/lang/Throwable;
    sget-boolean v1, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v1, :cond_5

    .line 4026
    invoke-static {v0}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 4029
    .end local v0    # "ex":Ljava/lang/Throwable;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method private createTempBufFile()Z
    .locals 6

    .prologue
    .line 3977
    iget-object v3, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    monitor-enter v3

    .line 3978
    :try_start_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ".sbBuf_"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Thread;->getId()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "_"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 3979
    .local v0, "bufFileName":Ljava/lang/String;
    sget-boolean v2, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v2, :cond_0

    .line 3980
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "try to save buffer to file "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3983
    :cond_0
    :try_start_1
    iget-object v2, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    invoke-virtual {v2}, Lorg/apache/http/impl/client/MultiSocketInputStream;->createBufferDir()V

    .line 3984
    const/4 v2, 0x0

    iget-object v4, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    iget-object v4, v4, Lorg/apache/http/impl/client/MultiSocketInputStream;->parentDir:Ljava/io/File;

    invoke-static {v0, v2, v4}, Ljava/io/File;->createTempFile(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;

    move-result-object v2

    iput-object v2, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->fileBuf:Ljava/io/File;

    .line 3985
    sget-boolean v2, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v2, :cond_1

    .line 3986
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "resource check: buffered file generated "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v4, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->fileBuf:Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 3988
    :cond_1
    new-instance v2, Ljava/io/DataInputStream;

    new-instance v4, Ljava/io/FileInputStream;

    iget-object v5, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->fileBuf:Ljava/io/File;

    invoke-direct {v4, v5}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v2, v4}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    iput-object v2, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->dis:Ljava/io/DataInputStream;

    .line 3989
    new-instance v2, Ljava/io/DataOutputStream;

    new-instance v4, Ljava/io/FileOutputStream;

    iget-object v5, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->fileBuf:Ljava/io/File;

    invoke-direct {v4, v5}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v2, v4}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    iput-object v2, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->dos:Ljava/io/DataOutputStream;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 3990
    const/4 v2, 0x1

    :try_start_2
    monitor-exit v3

    .line 3998
    :goto_0
    return v2

    .line 3992
    :catch_0
    move-exception v1

    .line 3993
    .local v1, "e":Ljava/lang/Throwable;
    sget-boolean v2, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v2, :cond_2

    .line 3994
    invoke-static {v1}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/Throwable;)V

    .line 3996
    :cond_2
    invoke-direct {p0}, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->closeAndDelFile()V

    .line 3997
    const/4 v0, 0x0

    .line 3998
    const/4 v2, 0x0

    monitor-exit v3

    goto :goto_0

    .line 4000
    .end local v0    # "bufFileName":Ljava/lang/String;
    .end local v1    # "e":Ljava/lang/Throwable;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v2
.end method


# virtual methods
.method public changeSockID(I)V
    .locals 0
    .param p1, "sid"    # I

    .prologue
    .line 4330
    iput p1, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->sockID:I

    .line 4331
    return-void
.end method

.method public clearBuffer()V
    .locals 1

    .prologue
    .line 4360
    iget-boolean v0, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->bSavedInFile:Z

    if-eqz v0, :cond_0

    .line 4361
    invoke-direct {p0}, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->closeAndDelFile()V

    .line 4366
    :goto_0
    return-void

    .line 4364
    :cond_0
    iget-object v0, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->mBuffer:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    goto :goto_0
.end method

.method public getHasReadLen()I
    .locals 1

    .prologue
    .line 4072
    iget v0, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->hasReadLen:I

    return v0
.end method

.method public getID()I
    .locals 1

    .prologue
    .line 4055
    iget v0, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->mID:I

    return v0
.end method

.method public getLength()I
    .locals 1

    .prologue
    .line 4046
    iget v0, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->mBufferLength:I

    return v0
.end method

.method public getOffset()J
    .locals 2

    .prologue
    .line 4411
    iget-wide v0, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->offset:J

    return-wide v0
.end method

.method public getRestLength()I
    .locals 1

    .prologue
    .line 4394
    iget v0, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->restLen:I

    return v0
.end method

.method public getSockID()I
    .locals 1

    .prologue
    .line 4064
    iget v0, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->sockID:I

    return v0
.end method

.method public getToBeReadLength()J
    .locals 2

    .prologue
    .line 4403
    iget v0, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->mBufferLength:I

    iget v1, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->hasReadLen:I

    sub-int/2addr v0, v1

    int-to-long v0, v0

    return-wide v0
.end method

.method public getTotalChunks()I
    .locals 1

    .prologue
    .line 4420
    iget v0, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->totalContinuousChunk:I

    return v0
.end method

.method public isBufferInFile()Z
    .locals 1

    .prologue
    .line 4037
    iget-boolean v0, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->bSavedInFile:Z

    return v0
.end method

.method public isEmpty()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 4339
    monitor-enter p0

    .line 4340
    :try_start_0
    iget-boolean v1, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->bSavedInFile:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->childChunkInput:Ljava/io/InputStream;

    if-nez v1, :cond_0

    iget-object v1, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->fileBuf:Ljava/io/File;

    if-nez v1, :cond_0

    .line 4341
    monitor-exit p0

    .line 4342
    :goto_0
    return v0

    :cond_0
    iget v1, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->restLen:I

    if-gtz v1, :cond_1

    :goto_1
    monitor-exit p0

    goto :goto_0

    .line 4343
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 4342
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public isFullRead()Z
    .locals 1

    .prologue
    .line 4352
    iget v0, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->restLen:I

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public push(I[BI)J
    .locals 8
    .param p1, "sid"    # I
    .param p2, "b"    # [B
    .param p3, "length"    # I

    .prologue
    const-wide/16 v4, -0x1

    const/4 v7, 0x0

    .line 4211
    iget v6, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->sockID:I

    if-eq v6, p1, :cond_1

    .line 4259
    :cond_0
    :goto_0
    return-wide v4

    .line 4215
    :cond_1
    iget-boolean v6, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->bSavedInFile:Z

    if-eqz v6, :cond_5

    .line 4217
    :try_start_0
    iget-object v6, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->dos:Ljava/io/DataOutputStream;

    const/4 v7, 0x0

    invoke-virtual {v6, p2, v7, p3}, Ljava/io/DataOutputStream;->write([BII)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 4239
    :goto_1
    iget v4, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->hasReadLen:I

    add-int/2addr v4, p3

    iput v4, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->hasReadLen:I

    .line 4240
    iget v4, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->restLen:I

    add-int/2addr v4, p3

    iput v4, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->restLen:I

    .line 4241
    iget-wide v4, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->offset:J

    int-to-long v6, p3

    add-long/2addr v4, v6

    iput-wide v4, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->offset:J

    .line 4242
    iget v4, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->hasReadLenForSpeed:I

    add-int/2addr v4, p3

    iput v4, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->hasReadLenForSpeed:I

    .line 4243
    iget v4, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->hasReadLen:I

    iget v5, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->mBufferLength:I

    if-lt v4, v5, :cond_2

    .line 4244
    const/4 v4, 0x1

    iput-boolean v4, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->fullRead:Z

    .line 4245
    iget-object v4, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockStatus:[B
    invoke-static {v4}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1600(Lorg/apache/http/impl/client/MultiSocketInputStream;)[B

    move-result-object v4

    iget v5, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->mID:I

    const/4 v6, 0x3

    aput-byte v6, v4, v5

    .line 4247
    :cond_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iput-wide v4, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->lastTime:J

    .line 4248
    iget-wide v4, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->lastTime:J

    iget-wide v6, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->startTime:J

    sub-long v2, v4, v6

    .line 4259
    .local v2, "t":J
    int-to-long v4, p3

    goto :goto_0

    .line 4219
    .end local v2    # "t":J
    :catch_0
    move-exception v0

    .line 4220
    .local v0, "ex":Ljava/lang/Throwable;
    iget-object v6, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    iget-boolean v6, v6, Lorg/apache/http/impl/client/MultiSocketInputStream;->mFinished:Z

    if-eqz v6, :cond_4

    .line 4221
    sget-boolean v4, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v4, :cond_3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "resource check: buffered file is already removed since download cancelled "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->fileBuf:Ljava/io/File;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 4222
    :cond_3
    int-to-long v4, p3

    goto :goto_0

    .line 4225
    :cond_4
    sget-boolean v6, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v6, :cond_0

    invoke-static {v0}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 4231
    .end local v0    # "ex":Ljava/lang/Throwable;
    :cond_5
    array-length v4, p2

    if-ne v4, p3, :cond_6

    .line 4232
    iget-object v4, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->mBuffer:Ljava/util/LinkedList;

    invoke-virtual {v4, p2}, Ljava/util/LinkedList;->addLast(Ljava/lang/Object;)V

    goto :goto_1

    .line 4234
    :cond_6
    new-array v1, p3, [B

    .line 4235
    .local v1, "tmp":[B
    invoke-static {p2, v7, v1, v7, p3}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 4236
    iget-object v4, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->mBuffer:Ljava/util/LinkedList;

    invoke-virtual {v4, v1}, Ljava/util/LinkedList;->addLast(Ljava/lang/Object;)V

    goto :goto_1
.end method

.method public push(Ljava/io/InputStream;JJ)V
    .locals 6
    .param p1, "cin"    # Ljava/io/InputStream;
    .param p2, "s"    # J
    .param p4, "e"    # J

    .prologue
    .line 4269
    monitor-enter p0

    .line 4270
    :try_start_0
    iput-object p1, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->childChunkInput:Ljava/io/InputStream;

    .line 4271
    iput-wide p2, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->childChunkStart:J

    .line 4272
    iput-wide p4, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->childChunkEnd:J

    .line 4274
    sub-long v2, p4, p2

    const-wide/16 v4, 0x1

    add-long v0, v2, v4

    .line 4275
    .local v0, "len":J
    iget v2, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->hasReadLen:I

    int-to-long v2, v2

    add-long/2addr v2, v0

    long-to-int v2, v2

    iput v2, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->hasReadLen:I

    .line 4276
    iget v2, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->restLen:I

    int-to-long v2, v2

    add-long/2addr v2, v0

    long-to-int v2, v2

    iput v2, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->restLen:I

    .line 4277
    iget-wide v2, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->offset:J

    add-long/2addr v2, v0

    iput-wide v2, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->offset:J

    .line 4278
    iget v2, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->hasReadLenForSpeed:I

    int-to-long v2, v2

    add-long/2addr v2, v0

    long-to-int v2, v2

    iput v2, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->hasReadLenForSpeed:I

    .line 4279
    iget v2, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->hasReadLen:I

    iget v3, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->mBufferLength:I

    if-lt v2, v3, :cond_1

    .line 4280
    sget-boolean v2, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v2, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Buffer full read "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->mID:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->mBufferLength:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 4281
    :cond_0
    const/4 v2, 0x1

    iput-boolean v2, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->fullRead:Z

    .line 4282
    iget-object v2, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockStatus:[B
    invoke-static {v2}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1600(Lorg/apache/http/impl/client/MultiSocketInputStream;)[B

    move-result-object v2

    iget v3, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->mID:I

    const/4 v4, 0x3

    aput-byte v4, v2, v3

    .line 4284
    :cond_1
    sget-boolean v2, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v2, :cond_2

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "push inputstream to data buffer from "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "-"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4, p5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 4285
    :cond_2
    monitor-exit p0

    .line 4286
    return-void

    .line 4285
    .end local v0    # "len":J
    :catchall_0
    move-exception v2

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public read([BII)I
    .locals 20
    .param p1, "buffer"    # [B
    .param p2, "offset"    # I
    .param p3, "length"    # I

    .prologue
    .line 4086
    const/4 v11, 0x0

    .line 4087
    .local v11, "readLen":I
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->bSavedInFile:Z

    if-eqz v14, :cond_e

    .line 4089
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->isEmpty()Z

    move-result v14

    if-eqz v14, :cond_0

    const/4 v14, 0x0

    .line 4186
    :goto_0
    return v14

    .line 4090
    :cond_0
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->childChunkInput:Ljava/io/InputStream;

    if-nez v14, :cond_7

    .line 4091
    move-object/from16 v0, p0

    iget-wide v14, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->bufStartReadTime:J

    const-wide/16 v16, 0x0

    cmp-long v14, v14, v16

    if-nez v14, :cond_1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v14

    move-object/from16 v0, p0

    iput-wide v14, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->bufStartReadTime:J

    .line 4092
    :cond_1
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->dis:Ljava/io/DataInputStream;

    move-object/from16 v0, p1

    move/from16 v1, p2

    move/from16 v2, p3

    invoke-virtual {v14, v0, v1, v2}, Ljava/io/DataInputStream;->read([BII)I
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result v11

    .line 4159
    :goto_1
    if-lez v11, :cond_6

    .line 4160
    move-object/from16 v0, p0

    iget-wide v14, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->readOffset:J

    int-to-long v0, v11

    move-wide/from16 v16, v0

    add-long v14, v14, v16

    move-object/from16 v0, p0

    iput-wide v14, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->readOffset:J

    .line 4161
    move-object/from16 v0, p0

    iget v14, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->restLen:I

    sub-int/2addr v14, v11

    move-object/from16 v0, p0

    iput v14, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->restLen:I

    .line 4162
    move-object/from16 v0, p0

    iget v14, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->restLen:I

    if-nez v14, :cond_3

    move-object/from16 v0, p0

    iget v14, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->hasReadLen:I

    move-object/from16 v0, p0

    iget v15, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->mBufferLength:I

    if-lt v14, v15, :cond_3

    .line 4163
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockStatus:[B
    invoke-static {v14}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1600(Lorg/apache/http/impl/client/MultiSocketInputStream;)[B

    move-result-object v14

    move-object/from16 v0, p0

    iget v15, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->mID:I

    const/16 v16, 0x4

    aput-byte v16, v14, v15

    .line 4164
    sget-boolean v14, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v14, :cond_2

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "resource check: finish reading chunk "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->bufOffset:J

    move-wide/from16 v16, v0

    move-wide/from16 v0, v16

    invoke-virtual {v14, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "-"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->bufOffset:J

    move-wide/from16 v16, v0

    move-object/from16 v0, p0

    iget v15, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->mBufferLength:I

    int-to-long v0, v15

    move-wide/from16 v18, v0

    add-long v16, v16, v18

    const-wide/16 v18, 0x1

    sub-long v16, v16, v18

    move-wide/from16 v0, v16

    invoke-virtual {v14, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " with length "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget v15, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->mBufferLength:I

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ", in which "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->bufOffset:J

    move-wide/from16 v16, v0

    move-wide/from16 v0, v16

    invoke-virtual {v14, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "-"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->childChunkStart:J

    move-wide/from16 v16, v0

    const-wide/16 v18, 0x1

    sub-long v16, v16, v18

    move-wide/from16 v0, v16

    invoke-virtual {v14, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " is from cache file "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->fileBuf:Ljava/io/File;

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ", and "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->childChunkStart:J

    move-wide/from16 v16, v0

    move-wide/from16 v0, v16

    invoke-virtual {v14, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "-"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->childChunkEnd:J

    move-wide/from16 v16, v0

    move-wide/from16 v0, v16

    invoke-virtual {v14, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " is from child input "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->childChunkInput:Ljava/io/InputStream;

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 4169
    :cond_2
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->bSavedInFile:Z

    if-eqz v14, :cond_3

    .line 4170
    invoke-direct/range {p0 .. p0}, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->closeAndDelFile()V

    .line 4173
    :cond_3
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->bufReadSpeed:J
    invoke-static {v14}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$6000(Lorg/apache/http/impl/client/MultiSocketInputStream;)J

    move-result-wide v14

    const-wide/16 v16, 0x0

    cmp-long v14, v14, v16

    if-nez v14, :cond_6

    move-object/from16 v0, p0

    iget-boolean v14, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->bSavedInFile:Z

    if-eqz v14, :cond_6

    move-object/from16 v0, p0

    iget v14, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->restLen:I

    if-nez v14, :cond_4

    move-object/from16 v0, p0

    iget v14, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->hasReadLen:I

    move-object/from16 v0, p0

    iget v15, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->mBufferLength:I

    if-ge v14, v15, :cond_5

    :cond_4
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mExtThread:Lorg/apache/http/impl/client/MultiSocketInputStream$ExtremeConditionThread;
    invoke-static {v14}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1700(Lorg/apache/http/impl/client/MultiSocketInputStream;)Lorg/apache/http/impl/client/MultiSocketInputStream$ExtremeConditionThread;

    move-result-object v14

    if-eqz v14, :cond_6

    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mExtThread:Lorg/apache/http/impl/client/MultiSocketInputStream$ExtremeConditionThread;
    invoke-static {v14}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1700(Lorg/apache/http/impl/client/MultiSocketInputStream;)Lorg/apache/http/impl/client/MultiSocketInputStream$ExtremeConditionThread;

    move-result-object v14

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream$ExtremeConditionThread;->mSecThreadCreationResult:I
    invoke-static {v14}, Lorg/apache/http/impl/client/MultiSocketInputStream$ExtremeConditionThread;->access$6100(Lorg/apache/http/impl/client/MultiSocketInputStream$ExtremeConditionThread;)I

    move-result v14

    const/16 v15, -0x64

    if-eq v14, v15, :cond_6

    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mExtThread:Lorg/apache/http/impl/client/MultiSocketInputStream$ExtremeConditionThread;
    invoke-static {v14}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1700(Lorg/apache/http/impl/client/MultiSocketInputStream;)Lorg/apache/http/impl/client/MultiSocketInputStream$ExtremeConditionThread;

    move-result-object v14

    iget-wide v14, v14, Lorg/apache/http/impl/client/MultiSocketInputStream$ExtremeConditionThread;->mNonBufOffset:J

    const-wide/16 v16, 0x0

    cmp-long v14, v14, v16

    if-lez v14, :cond_6

    move-object/from16 v0, p0

    iget-wide v14, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->readOffset:J

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    move-object/from16 v16, v0

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mExtThread:Lorg/apache/http/impl/client/MultiSocketInputStream$ExtremeConditionThread;
    invoke-static/range {v16 .. v16}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1700(Lorg/apache/http/impl/client/MultiSocketInputStream;)Lorg/apache/http/impl/client/MultiSocketInputStream$ExtremeConditionThread;

    move-result-object v16

    move-object/from16 v0, v16

    iget-wide v0, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$ExtremeConditionThread;->mNonBufOffset:J

    move-wide/from16 v16, v0

    cmp-long v14, v14, v16

    if-ltz v14, :cond_6

    .line 4175
    :cond_5
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v14

    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->bufStartReadTime:J

    move-wide/from16 v16, v0

    sub-long v12, v14, v16

    .line 4176
    .local v12, "time":J
    move-object/from16 v0, p0

    iget-wide v14, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->readOffset:J

    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->bufOffset:J

    move-wide/from16 v16, v0

    sub-long v8, v14, v16

    .line 4177
    .local v8, "len":J
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    invoke-virtual {v15, v8, v9, v12, v13}, Lorg/apache/http/impl/client/MultiSocketInputStream;->getSpeed(JJ)J

    move-result-wide v16

    move-wide/from16 v0, v16

    # setter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->bufReadSpeed:J
    invoke-static {v14, v0, v1}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$6002(Lorg/apache/http/impl/client/MultiSocketInputStream;J)J

    .line 4178
    const-wide/16 v14, 0x0

    cmp-long v14, v12, v14

    if-lez v14, :cond_13

    const-wide/16 v14, 0x0

    cmp-long v14, v8, v14

    if-lez v14, :cond_13

    .line 4179
    sget-boolean v14, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v14, :cond_6

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "calculate buf read speed with len="

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " and time="

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " speed="

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->bufReadSpeed:J
    invoke-static {v15}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$6000(Lorg/apache/http/impl/client/MultiSocketInputStream;)J

    move-result-wide v16

    move-wide/from16 v0, v16

    invoke-virtual {v14, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .end local v8    # "len":J
    .end local v12    # "time":J
    :cond_6
    :goto_2
    move v14, v11

    .line 4186
    goto/16 :goto_0

    .line 4095
    :cond_7
    :try_start_1
    move-object/from16 v0, p0

    iget-wide v14, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->readOffset:J

    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->childChunkStart:J

    move-wide/from16 v16, v0

    cmp-long v14, v14, v16

    if-gez v14, :cond_8

    .line 4096
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->dis:Ljava/io/DataInputStream;

    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->childChunkStart:J

    move-wide/from16 v16, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->readOffset:J

    move-wide/from16 v18, v0

    sub-long v16, v16, v18

    move-wide/from16 v0, v16

    long-to-int v15, v0

    move/from16 v0, p3

    invoke-static {v0, v15}, Ljava/lang/Math;->min(II)I

    move-result v15

    move-object/from16 v0, p1

    move/from16 v1, p2

    invoke-virtual {v14, v0, v1, v15}, Ljava/io/DataInputStream;->read([BII)I

    move-result v11

    goto/16 :goto_1

    .line 4099
    :cond_8
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->bSwitchedToChild:Z

    if-nez v14, :cond_a

    .line 4100
    sget-boolean v14, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v14, :cond_9

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "resource check: finish reading chunk "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->bufOffset:J

    move-wide/from16 v16, v0

    move-wide/from16 v0, v16

    invoke-virtual {v14, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "-"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->childChunkStart:J

    move-wide/from16 v16, v0

    const-wide/16 v18, 0x1

    sub-long v16, v16, v18

    move-wide/from16 v0, v16

    invoke-virtual {v14, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " with length "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->childChunkStart:J

    move-wide/from16 v16, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->bufOffset:J

    move-wide/from16 v18, v0

    sub-long v16, v16, v18

    move-wide/from16 v0, v16

    invoke-virtual {v14, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " from cache file "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->fileBuf:Ljava/io/File;

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ", now bytesRemaining data "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->childChunkStart:J

    move-wide/from16 v16, v0

    move-wide/from16 v0, v16

    invoke-virtual {v14, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "-"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->childChunkEnd:J

    move-wide/from16 v16, v0

    move-wide/from16 v0, v16

    invoke-virtual {v14, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " will be read from child input "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->childChunkInput:Ljava/io/InputStream;

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 4104
    :cond_9
    invoke-direct/range {p0 .. p0}, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->closeAndDelFile()V

    .line 4105
    const/4 v14, 0x1

    move-object/from16 v0, p0

    iput-boolean v14, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->bSwitchedToChild:Z

    .line 4107
    :cond_a
    sget-boolean v14, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v14, :cond_b

    .line 4108
    move-object/from16 v0, p0

    iget-wide v14, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->readOffset:J

    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->childChunkStart:J

    move-wide/from16 v16, v0

    cmp-long v14, v14, v16

    if-nez v14, :cond_b

    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->childChunkInput:Ljava/io/InputStream;

    if-eqz v14, :cond_b

    .line 4109
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "now start to read from childChunkInput "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->childChunkInput:Ljava/io/InputStream;

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 4112
    :cond_b
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->childChunkInput:Ljava/io/InputStream;

    instance-of v14, v14, Lorg/apache/http/impl/client/TwoChunkInputStream;

    if-eqz v14, :cond_d

    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->childChunkInput:Ljava/io/InputStream;

    check-cast v14, Lorg/apache/http/impl/client/TwoChunkInputStream;

    invoke-virtual {v14}, Lorg/apache/http/impl/client/TwoChunkInputStream;->isClosed()Z

    move-result v14

    if-eqz v14, :cond_d

    .line 4113
    sget-boolean v14, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v14, :cond_c

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "childChunkInput is closed : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->childChunkInput:Ljava/io/InputStream;

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 4114
    :cond_c
    const/4 v14, -0x1

    goto/16 :goto_0

    .line 4116
    :cond_d
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->childChunkInput:Ljava/io/InputStream;

    move-object/from16 v0, p1

    move/from16 v1, p2

    move/from16 v2, p3

    invoke-virtual {v14, v0, v1, v2}, Ljava/io/InputStream;->read([BII)I
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    move-result v11

    goto/16 :goto_1

    .line 4120
    :catch_0
    move-exception v6

    .line 4121
    .local v6, "e":Ljava/lang/Throwable;
    invoke-static {v6}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/Throwable;)V

    .line 4122
    invoke-direct/range {p0 .. p0}, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->closeAndDelFile()V

    .line 4123
    const/4 v11, -0x1

    .line 4124
    goto/16 :goto_1

    .line 4127
    .end local v6    # "e":Ljava/lang/Throwable;
    :cond_e
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->mBuffer:Ljava/util/LinkedList;

    monitor-enter v15

    .line 4128
    move/from16 v4, p2

    .line 4129
    .local v4, "appOffset":I
    :cond_f
    :try_start_2
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->mBuffer:Ljava/util/LinkedList;

    invoke-virtual {v14}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v14

    if-nez v14, :cond_10

    .line 4130
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->mBuffer:Ljava/util/LinkedList;

    invoke-virtual {v14}, Ljava/util/LinkedList;->getFirst()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [B

    .line 4131
    .local v5, "b":[B
    if-nez v5, :cond_11

    .line 4132
    const/4 v14, 0x0

    move-object/from16 v0, p0

    iput v14, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->firstBlockOffset:I

    .line 4156
    .end local v5    # "b":[B
    :cond_10
    :goto_3
    monitor-exit v15

    goto/16 :goto_1

    :catchall_0
    move-exception v14

    monitor-exit v15
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v14

    .line 4135
    .restart local v5    # "b":[B
    :cond_11
    :try_start_3
    array-length v14, v5

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->firstBlockOffset:I

    move/from16 v16, v0

    sub-int v10, v14, v16

    .line 4137
    .local v10, "notReadLen":I
    add-int v14, v11, v10

    move/from16 v0, p3

    if-gt v14, v0, :cond_12

    .line 4138
    move-object/from16 v0, p0

    iget v14, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->firstBlockOffset:I

    move-object/from16 v0, p1

    invoke-static {v5, v14, v0, v4, v10}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 4139
    add-int/2addr v11, v10

    .line 4140
    add-int/2addr v4, v10

    .line 4141
    const/4 v14, 0x0

    move-object/from16 v0, p0

    iput v14, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->firstBlockOffset:I

    .line 4142
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->mBuffer:Ljava/util/LinkedList;

    invoke-virtual {v14}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    .line 4143
    move/from16 v0, p3

    if-ne v11, v0, :cond_f

    goto :goto_3

    .line 4148
    :cond_12
    sub-int v7, p3, v11

    .line 4149
    .local v7, "l":I
    move-object/from16 v0, p0

    iget v14, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->firstBlockOffset:I

    move-object/from16 v0, p1

    invoke-static {v5, v14, v0, v4, v7}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 4150
    add-int/2addr v11, v7

    .line 4151
    add-int/2addr v4, v7

    .line 4152
    move-object/from16 v0, p0

    iget v14, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->firstBlockOffset:I

    add-int/2addr v14, v7

    move-object/from16 v0, p0

    iput v14, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->firstBlockOffset:I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_3

    .line 4182
    .end local v4    # "appOffset":I
    .end local v5    # "b":[B
    .end local v7    # "l":I
    .end local v10    # "notReadLen":I
    .restart local v8    # "len":J
    .restart local v12    # "time":J
    :cond_13
    sget-boolean v14, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v14, :cond_6

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "cannot calculate buf read speed with len="

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " and time="

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    goto/16 :goto_2
.end method

.method public removeTail(I)V
    .locals 6
    .param p1, "count"    # I

    .prologue
    .line 4374
    iget-boolean v2, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->bSavedInFile:Z

    if-eqz v2, :cond_1

    .line 4386
    :cond_0
    return-void

    .line 4378
    :cond_1
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, p1, :cond_0

    .line 4379
    iget-object v2, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->mBuffer:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->removeLast()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    .line 4380
    .local v0, "b":[B
    iget v2, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->hasReadLen:I

    array-length v3, v0

    sub-int/2addr v2, v3

    iput v2, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->hasReadLen:I

    .line 4381
    iget v2, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->restLen:I

    array-length v3, v0

    sub-int/2addr v2, v3

    iput v2, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->restLen:I

    .line 4382
    iget-wide v2, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->offset:J

    array-length v4, v0

    int-to-long v4, v4

    sub-long/2addr v2, v4

    iput-wide v2, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->offset:J

    .line 4383
    iget v2, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->hasReadLenForSpeed:I

    array-length v3, v0

    sub-int/2addr v2, v3

    iput v2, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->hasReadLenForSpeed:I

    .line 4378
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public setTotalChunks(I)V
    .locals 0
    .param p1, "totalChunks"    # I

    .prologue
    .line 4429
    iput p1, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->totalContinuousChunk:I

    .line 4430
    return-void
.end method

.method public startReceiveData()V
    .locals 3

    .prologue
    .line 4193
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->startTime:J

    .line 4194
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->hasReadLenForSpeed:I

    .line 4195
    iget-object v0, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockStatus:[B
    invoke-static {v0}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1600(Lorg/apache/http/impl/client/MultiSocketInputStream;)[B

    move-result-object v0

    iget v1, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->mID:I

    const/4 v2, 0x2

    aput-byte v2, v0, v1

    .line 4196
    return-void
.end method

.method public switchSocket(IZ)[J
    .locals 6
    .param p1, "newSockID"    # I
    .param p2, "bForce"    # Z

    .prologue
    .line 4297
    monitor-enter p0

    .line 4299
    :try_start_0
    invoke-virtual {p0}, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->getToBeReadLength()J

    move-result-wide v2

    sget v1, Lorg/apache/http/impl/client/MultiSocketInputStream;->MIN_BLOCKSIZE_TO_HANDOVER:I

    int-to-long v4, v1

    cmp-long v1, v2, v4

    if-gtz v1, :cond_1

    if-nez p2, :cond_1

    .line 4300
    sget-boolean v1, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v1, :cond_0

    .line 4301
    const-string v1, "Socket do not need to switch"

    invoke-static {v1}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 4303
    :cond_0
    const/4 v1, 0x0

    new-array v0, v1, [J

    monitor-exit p0

    .line 4317
    :goto_0
    return-object v0

    .line 4305
    :cond_1
    iget-object v1, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mRequestHandlers:[Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;
    invoke-static {v1}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$4000(Lorg/apache/http/impl/client/MultiSocketInputStream;)[Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mRequestHandlers:[Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;
    invoke-static {v1}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$4000(Lorg/apache/http/impl/client/MultiSocketInputStream;)[Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;

    move-result-object v1

    array-length v1, v1

    iget v2, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->sockID:I

    if-le v1, v2, :cond_2

    iget-object v1, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mRequestHandlers:[Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;
    invoke-static {v1}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$4000(Lorg/apache/http/impl/client/MultiSocketInputStream;)[Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;

    move-result-object v1

    iget v2, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->sockID:I

    aget-object v1, v1, v2

    if-eqz v1, :cond_2

    .line 4308
    iget-object v1, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mRequestHandlers:[Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;
    invoke-static {v1}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$4000(Lorg/apache/http/impl/client/MultiSocketInputStream;)[Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;

    move-result-object v1

    iget v2, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->sockID:I

    aget-object v1, v1, v2

    invoke-virtual {v1}, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->handOver()V

    .line 4311
    :cond_2
    const/4 v1, 0x4

    new-array v0, v1, [J

    .line 4312
    .local v0, "range":[J
    const/4 v1, 0x0

    iget-wide v2, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->offset:J

    aput-wide v2, v0, v1

    .line 4313
    const/4 v1, 0x1

    iget-wide v2, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->offset:J

    invoke-virtual {p0}, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->getToBeReadLength()J

    move-result-wide v4

    add-long/2addr v2, v4

    const-wide/16 v4, 0x1

    sub-long/2addr v2, v4

    aput-wide v2, v0, v1

    .line 4314
    const/4 v1, 0x2

    invoke-virtual {p0}, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->getToBeReadLength()J

    move-result-wide v2

    aput-wide v2, v0, v1

    .line 4315
    const/4 v1, 0x3

    iget v2, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->mID:I

    int-to-long v2, v2

    aput-wide v2, v0, v1

    .line 4316
    iput p1, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->sockID:I

    .line 4317
    monitor-exit p0

    goto :goto_0

    .line 4319
    .end local v0    # "range":[J
    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method
