.class Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;
.super Ljava/lang/Object;
.source "MultiSocketInputStream.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/http/impl/client/MultiSocketInputStream;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "BlockManager"
.end annotation


# static fields
.field public static final BLOCKINFOSIZE:I = 0x4


# instance fields
.field protected headerTime:[J

.field private httpRequestID:[I

.field private minNotReadBlockID:I

.field protected reconnTime:[J

.field private socketSpeed:[J

.field final synthetic this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

.field protected toBeReadLen:[J


# direct methods
.method public constructor <init>(Lorg/apache/http/impl/client/MultiSocketInputStream;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v3, 0x2

    .line 4473
    iput-object p1, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4474
    new-array v1, v3, [I

    iput-object v1, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->httpRequestID:[I

    .line 4475
    new-array v1, v3, [J

    iput-object v1, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->socketSpeed:[J

    .line 4476
    new-array v1, v3, [J

    iput-object v1, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->reconnTime:[J

    .line 4477
    new-array v1, v3, [J

    iput-object v1, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->headerTime:[J

    .line 4478
    new-array v1, v3, [J

    iput-object v1, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->toBeReadLen:[J

    .line 4479
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v3, :cond_0

    .line 4480
    iget-object v1, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->httpRequestID:[I

    const/4 v2, -0x1

    aput v2, v1, v0

    .line 4481
    iget-object v1, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->socketSpeed:[J

    aput-wide v4, v1, v0

    .line 4482
    iget-object v1, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->reconnTime:[J

    aput-wide v4, v1, v0

    .line 4483
    iget-object v1, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->headerTime:[J

    aput-wide v4, v1, v0

    .line 4484
    iget-object v1, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->toBeReadLen:[J

    aput-wide v4, v1, v0

    .line 4479
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 4486
    :cond_0
    const/4 v1, 0x0

    iput v1, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->minNotReadBlockID:I

    .line 4487
    return-void
.end method

.method static synthetic access$5800(Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;)[J
    .locals 1
    .param p0, "x0"    # Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;

    .prologue
    .line 4440
    iget-object v0, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->socketSpeed:[J

    return-object v0
.end method

.method private getBlockForWithID(II)[J
    .locals 4
    .param p1, "sockID"    # I
    .param p2, "blockid"    # I

    .prologue
    const/4 v3, 0x0

    .line 4845
    iget-object v1, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockStatus:[B
    invoke-static {v1}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1600(Lorg/apache/http/impl/client/MultiSocketInputStream;)[B

    move-result-object v1

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    invoke-static {p2, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 4847
    .local v0, "tmpID":I
    :cond_0
    if-ltz v0, :cond_1

    iget-object v1, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockStatus:[B
    invoke-static {v1}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1600(Lorg/apache/http/impl/client/MultiSocketInputStream;)[B

    move-result-object v1

    array-length v1, v1

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockStatus:[B
    invoke-static {v1}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1600(Lorg/apache/http/impl/client/MultiSocketInputStream;)[B

    move-result-object v1

    aget-byte v1, v1, v0

    if-lez v1, :cond_1

    .line 4848
    add-int/lit8 v0, v0, 0x1

    .line 4850
    iget-object v1, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockStatus:[B
    invoke-static {v1}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1600(Lorg/apache/http/impl/client/MultiSocketInputStream;)[B

    move-result-object v1

    array-length v1, v1

    if-ne v0, v1, :cond_0

    .line 4851
    sget-boolean v1, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v1, :cond_1

    .line 4852
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "id reach the maximum "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockStatus:[B
    invoke-static {v2}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1600(Lorg/apache/http/impl/client/MultiSocketInputStream;)[B

    move-result-object v2

    array-length v2, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 4858
    :cond_1
    iget-object v1, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockStatus:[B
    invoke-static {v1}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1600(Lorg/apache/http/impl/client/MultiSocketInputStream;)[B

    move-result-object v1

    array-length v1, v1

    if-ne v0, v1, :cond_4

    .line 4859
    iget-object v1, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockStatus:[B
    invoke-static {v1}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1600(Lorg/apache/http/impl/client/MultiSocketInputStream;)[B

    move-result-object v1

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    invoke-static {p2, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 4860
    :cond_2
    if-ltz v0, :cond_4

    iget-object v1, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockStatus:[B
    invoke-static {v1}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1600(Lorg/apache/http/impl/client/MultiSocketInputStream;)[B

    move-result-object v1

    array-length v1, v1

    if-ge v0, v1, :cond_4

    iget-object v1, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockStatus:[B
    invoke-static {v1}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1600(Lorg/apache/http/impl/client/MultiSocketInputStream;)[B

    move-result-object v1

    aget-byte v1, v1, v0

    if-lez v1, :cond_4

    .line 4861
    add-int/lit8 v0, v0, -0x1

    .line 4862
    iget v1, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->minNotReadBlockID:I

    if-ne v0, v1, :cond_2

    .line 4863
    sget-boolean v1, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v1, :cond_3

    .line 4864
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "id reach the minimum "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->minNotReadBlockID:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 4866
    :cond_3
    new-array v1, v3, [J

    .line 4877
    :goto_0
    return-object v1

    .line 4870
    :cond_4
    if-ltz v0, :cond_5

    iget-object v1, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockStatus:[B
    invoke-static {v1}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1600(Lorg/apache/http/impl/client/MultiSocketInputStream;)[B

    move-result-object v1

    array-length v1, v1

    if-lt v0, v1, :cond_7

    .line 4871
    :cond_5
    sget-boolean v1, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v1, :cond_6

    .line 4872
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "get an illegal tmpID "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 4874
    :cond_6
    new-array v1, v3, [J

    goto :goto_0

    .line 4876
    :cond_7
    iget-object v1, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->httpRequestID:[I

    aput v0, v1, p1

    .line 4877
    invoke-direct {p0, v0, p1}, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->getNewBuf(II)[J

    move-result-object v1

    goto :goto_0
.end method

.method private getContinuousChunk(I)[J
    .locals 18
    .param p1, "sockID"    # I

    .prologue
    .line 4962
    const/4 v10, 0x0

    .line 4963
    .local v10, "notReadContinuousBlock":I
    const-wide/16 v4, -0x1

    .line 4964
    .local v4, "startOffset":J
    const-wide/16 v12, -0x1

    .line 4965
    .local v12, "endOffset":J
    const/4 v6, 0x0

    .line 4966
    .local v6, "blockId":I
    sget-boolean v3, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v3, :cond_0

    .line 4967
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Inside getContinuousChunk(), mReadBlockNumber="

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v8, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mReadBlockNumber:Ljava/lang/Integer;
    invoke-static {v8}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$5600(Lorg/apache/http/impl/client/MultiSocketInputStream;)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 4969
    :cond_0
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v8, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mReadBlockNumber:Ljava/lang/Integer;
    invoke-static {v8}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$5600(Lorg/apache/http/impl/client/MultiSocketInputStream;)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    invoke-static {v3, v8}, Ljava/lang/Math;->max(II)I

    move-result v14

    .local v14, "i":I
    :goto_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockStatus:[B
    invoke-static {v3}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1600(Lorg/apache/http/impl/client/MultiSocketInputStream;)[B

    move-result-object v3

    array-length v3, v3

    if-ge v14, v3, :cond_d

    .line 4971
    sget-boolean v3, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v3, :cond_1

    .line 4972
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "mBlockStatus["

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v8, "] = "

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v8, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockStatus:[B
    invoke-static {v8}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1600(Lorg/apache/http/impl/client/MultiSocketInputStream;)[B

    move-result-object v8

    aget-byte v8, v8, v14

    move-object/from16 v0, p0

    invoke-direct {v0, v8}, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->statusToStr(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 4974
    :cond_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockStatus:[B
    invoke-static {v3}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1600(Lorg/apache/http/impl/client/MultiSocketInputStream;)[B

    move-result-object v3

    aget-byte v3, v3, v14

    packed-switch v3, :pswitch_data_0

    .line 4969
    :cond_2
    :goto_1
    :pswitch_0
    add-int/lit8 v14, v14, 0x1

    goto :goto_0

    .line 4977
    :pswitch_1
    sget-boolean v3, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v3, :cond_3

    .line 4978
    const-string v3, "Inside NOT_READ BLOCK"

    invoke-static {v3}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 4982
    :cond_3
    add-int/lit8 v10, v10, 0x1

    .line 4984
    const-wide/16 v8, -0x1

    cmp-long v3, v4, v8

    if-nez v3, :cond_4

    .line 4985
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockSize:I
    invoke-static {v3}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$900(Lorg/apache/http/impl/client/MultiSocketInputStream;)I

    move-result v3

    mul-int/2addr v3, v14

    int-to-long v8, v3

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mOffset:J
    invoke-static {v3}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1200(Lorg/apache/http/impl/client/MultiSocketInputStream;)J

    move-result-wide v16

    add-long v4, v8, v16

    .line 4986
    move v6, v14

    .line 4990
    :cond_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockStatus:[B
    invoke-static {v3}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1600(Lorg/apache/http/impl/client/MultiSocketInputStream;)[B

    move-result-object v3

    array-length v3, v3

    add-int/lit8 v3, v3, -0x1

    if-ne v14, v3, :cond_5

    .line 4991
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mContentLength:J
    invoke-static {v3}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$400(Lorg/apache/http/impl/client/MultiSocketInputStream;)J

    move-result-wide v8

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    iget-wide v0, v3, Lorg/apache/http/impl/client/MultiSocketInputStream;->mOriOffset:J

    move-wide/from16 v16, v0

    add-long v8, v8, v16

    const-wide/16 v16, 0x1

    sub-long v12, v8, v16

    goto :goto_1

    .line 4993
    :cond_5
    add-int v3, v6, v10

    move-object/from16 v0, p0

    iget-object v8, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockSize:I
    invoke-static {v8}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$900(Lorg/apache/http/impl/client/MultiSocketInputStream;)I

    move-result v8

    mul-int/2addr v3, v8

    int-to-long v8, v3

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mOffset:J
    invoke-static {v3}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1200(Lorg/apache/http/impl/client/MultiSocketInputStream;)J

    move-result-wide v16

    add-long v8, v8, v16

    const-wide/16 v16, 0x1

    sub-long v12, v8, v16

    .line 4996
    goto :goto_1

    .line 4998
    :pswitch_2
    sget-boolean v3, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v3, :cond_6

    .line 4999
    const-string v3, "Inside BLOCKED BLOCK"

    invoke-static {v3}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 5001
    :cond_6
    const/4 v3, 0x1

    if-lt v10, v3, :cond_7

    .line 5002
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockStatus:[B
    invoke-static {v3}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1600(Lorg/apache/http/impl/client/MultiSocketInputStream;)[B

    move-result-object v3

    array-length v14, v3

    goto :goto_1

    .line 5003
    :cond_7
    if-nez v10, :cond_2

    .line 5004
    sget-boolean v3, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v3, :cond_8

    .line 5005
    const-string v3, "Inside BLOCKED BLOCK: Getting the start index."

    invoke-static {v3}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 5007
    :cond_8
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mInBuffer:Ljava/util/HashMap;
    invoke-static {v3}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1500(Lorg/apache/http/impl/client/MultiSocketInputStream;)Ljava/util/HashMap;

    move-result-object v3

    new-instance v8, Ljava/lang/Integer;

    invoke-direct {v8, v14}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v3, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;

    .line 5008
    .local v2, "buf":Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;
    invoke-virtual {v2}, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->getOffset()J

    move-result-wide v4

    .line 5009
    move v6, v14

    .line 5010
    add-int/lit8 v10, v10, 0x1

    .line 5011
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockStatus:[B
    invoke-static {v3}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1600(Lorg/apache/http/impl/client/MultiSocketInputStream;)[B

    move-result-object v3

    array-length v3, v3

    add-int/lit8 v3, v3, -0x1

    if-ne v14, v3, :cond_9

    .line 5012
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mContentLength:J
    invoke-static {v3}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$400(Lorg/apache/http/impl/client/MultiSocketInputStream;)J

    move-result-wide v8

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    iget-wide v0, v3, Lorg/apache/http/impl/client/MultiSocketInputStream;->mOriOffset:J

    move-wide/from16 v16, v0

    add-long v8, v8, v16

    const-wide/16 v16, 0x1

    sub-long v12, v8, v16

    goto/16 :goto_1

    .line 5014
    :cond_9
    add-int v3, v6, v10

    move-object/from16 v0, p0

    iget-object v8, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockSize:I
    invoke-static {v8}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$900(Lorg/apache/http/impl/client/MultiSocketInputStream;)I

    move-result v8

    mul-int/2addr v3, v8

    int-to-long v8, v3

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mOffset:J
    invoke-static {v3}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1200(Lorg/apache/http/impl/client/MultiSocketInputStream;)J

    move-result-wide v16

    add-long v8, v8, v16

    const-wide/16 v16, 0x1

    sub-long v12, v8, v16

    goto/16 :goto_1

    .line 5021
    .end local v2    # "buf":Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;
    :pswitch_3
    sget-boolean v3, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v3, :cond_a

    .line 5022
    const-string v3, "Inside FULLREAD BLOCK"

    invoke-static {v3}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 5024
    :cond_a
    const/4 v3, 0x1

    if-lt v10, v3, :cond_2

    .line 5025
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockStatus:[B
    invoke-static {v3}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1600(Lorg/apache/http/impl/client/MultiSocketInputStream;)[B

    move-result-object v3

    array-length v14, v3

    .line 5026
    sget-boolean v3, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v3, :cond_b

    .line 5027
    const-string v3, "Inside FULLREAD BLOCK: exiting this loop as notReadContinuousBlock >= 1"

    invoke-static {v3}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 5029
    :cond_b
    add-int v3, v6, v10

    move-object/from16 v0, p0

    iget-object v8, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockSize:I
    invoke-static {v8}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$900(Lorg/apache/http/impl/client/MultiSocketInputStream;)I

    move-result v8

    mul-int/2addr v3, v8

    int-to-long v8, v3

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mOffset:J
    invoke-static {v3}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1200(Lorg/apache/http/impl/client/MultiSocketInputStream;)J

    move-result-wide v16

    add-long v8, v8, v16

    const-wide/16 v16, 0x1

    sub-long v12, v8, v16

    goto/16 :goto_1

    .line 5035
    :pswitch_4
    const/4 v3, 0x1

    if-lt v10, v3, :cond_2

    .line 5036
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockStatus:[B
    invoke-static {v3}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1600(Lorg/apache/http/impl/client/MultiSocketInputStream;)[B

    move-result-object v3

    array-length v14, v3

    .line 5037
    sget-boolean v3, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v3, :cond_c

    .line 5038
    const-string v3, "Inside READING Block: exiting this loop as notReadContinuousBlock >= 1"

    invoke-static {v3}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 5040
    :cond_c
    add-int v3, v6, v10

    move-object/from16 v0, p0

    iget-object v8, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockSize:I
    invoke-static {v8}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$900(Lorg/apache/http/impl/client/MultiSocketInputStream;)I

    move-result v8

    mul-int/2addr v3, v8

    int-to-long v8, v3

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mOffset:J
    invoke-static {v3}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1200(Lorg/apache/http/impl/client/MultiSocketInputStream;)J

    move-result-wide v16

    add-long v8, v8, v16

    const-wide/16 v16, 0x1

    sub-long v12, v8, v16

    goto/16 :goto_1

    .line 5050
    :cond_d
    const/4 v3, 0x1

    if-lt v10, v3, :cond_14

    .line 5052
    const/4 v3, 0x4

    new-array v11, v3, [J

    .line 5053
    .local v11, "block":[J
    const/4 v3, 0x0

    aput-wide v4, v11, v3

    .line 5054
    const/4 v3, 0x1

    aput-wide v12, v11, v3

    .line 5055
    const/4 v3, 0x2

    sub-long v8, v12, v4

    const-wide/16 v16, 0x1

    add-long v8, v8, v16

    aput-wide v8, v11, v3

    .line 5056
    const/4 v3, 0x3

    int-to-long v8, v6

    aput-wide v8, v11, v3

    .line 5059
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockStatus:[B
    invoke-static {v3}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1600(Lorg/apache/http/impl/client/MultiSocketInputStream;)[B

    move-result-object v3

    aget-byte v3, v3, v6

    const/4 v8, -0x1

    if-eq v3, v8, :cond_13

    .line 5060
    const/4 v7, 0x0

    .line 5061
    .local v7, "endIndex":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockStatus:[B
    invoke-static {v3}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1600(Lorg/apache/http/impl/client/MultiSocketInputStream;)[B

    move-result-object v3

    array-length v3, v3

    add-int/lit8 v3, v3, -0x1

    if-ne v6, v3, :cond_e

    .line 5062
    const/4 v3, 0x2

    aget-wide v8, v11, v3

    long-to-int v7, v8

    .line 5067
    :goto_2
    new-instance v2, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    new-instance v8, Ljava/util/LinkedList;

    invoke-direct {v8}, Ljava/util/LinkedList;-><init>()V

    move/from16 v9, p1

    invoke-direct/range {v2 .. v10}, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;-><init>(Lorg/apache/http/impl/client/MultiSocketInputStream;JIILjava/util/LinkedList;II)V

    .line 5069
    .restart local v2    # "buf":Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mInBuffer:Ljava/util/HashMap;
    invoke-static {v3}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1500(Lorg/apache/http/impl/client/MultiSocketInputStream;)Ljava/util/HashMap;

    move-result-object v3

    new-instance v8, Ljava/lang/Integer;

    invoke-direct {v8, v6}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v3, v8, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 5071
    monitor-enter v2

    .line 5072
    const/4 v15, 0x1

    .local v15, "j":I
    :goto_3
    if-ge v15, v10, :cond_10

    .line 5073
    :try_start_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockStatus:[B
    invoke-static {v3}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1600(Lorg/apache/http/impl/client/MultiSocketInputStream;)[B

    move-result-object v3

    add-int v8, v6, v15

    const/4 v9, 0x1

    aput-byte v9, v3, v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 5072
    add-int/lit8 v15, v15, 0x1

    goto :goto_3

    .line 5064
    .end local v2    # "buf":Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;
    .end local v15    # "j":I
    :cond_e
    const/4 v3, 0x2

    aget-wide v8, v11, v3

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockSize:I
    invoke-static {v3}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$900(Lorg/apache/http/impl/client/MultiSocketInputStream;)I

    move-result v3

    int-to-long v0, v3

    move-wide/from16 v16, v0

    cmp-long v3, v8, v16

    if-lez v3, :cond_f

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockSize:I
    invoke-static {v3}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$900(Lorg/apache/http/impl/client/MultiSocketInputStream;)I

    move-result v3

    int-to-long v8, v3

    :goto_4
    long-to-int v7, v8

    goto :goto_2

    :cond_f
    const/4 v3, 0x2

    aget-wide v8, v11, v3

    goto :goto_4

    .line 5075
    .restart local v2    # "buf":Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;
    .restart local v15    # "j":I
    :cond_10
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 5083
    .end local v2    # "buf":Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;
    .end local v7    # "endIndex":I
    .end local v15    # "j":I
    :cond_11
    :goto_5
    sget-boolean v3, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v3, :cond_12

    .line 5084
    const-string v3, ""

    invoke-static {v3}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 5088
    .end local v11    # "block":[J
    :cond_12
    :goto_6
    return-object v11

    .line 5075
    .restart local v2    # "buf":Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;
    .restart local v7    # "endIndex":I
    .restart local v11    # "block":[J
    .restart local v15    # "j":I
    :catchall_0
    move-exception v3

    :try_start_2
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v3

    .line 5076
    .end local v2    # "buf":Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;
    .end local v7    # "endIndex":I
    .end local v15    # "j":I
    :cond_13
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockStatus:[B
    invoke-static {v3}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1600(Lorg/apache/http/impl/client/MultiSocketInputStream;)[B

    move-result-object v3

    aget-byte v3, v3, v6

    const/4 v8, -0x1

    if-ne v3, v8, :cond_11

    .line 5077
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mInBuffer:Ljava/util/HashMap;
    invoke-static {v3}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1500(Lorg/apache/http/impl/client/MultiSocketInputStream;)Ljava/util/HashMap;

    move-result-object v3

    new-instance v8, Ljava/lang/Integer;

    invoke-direct {v8, v6}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v3, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;

    .line 5078
    .restart local v2    # "buf":Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;
    monitor-enter v2

    .line 5079
    :try_start_3
    invoke-virtual {v2, v10}, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->setTotalChunks(I)V

    .line 5080
    move/from16 v0, p1

    invoke-virtual {v2, v0}, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->changeSockID(I)V

    .line 5081
    monitor-exit v2

    goto :goto_5

    :catchall_1
    move-exception v3

    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v3

    .line 5088
    .end local v2    # "buf":Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;
    .end local v11    # "block":[J
    :cond_14
    const/4 v3, 0x0

    new-array v11, v3, [J

    goto :goto_6

    .line 4974
    nop

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_4
        :pswitch_3
    .end packed-switch
.end method

.method private getMinNotReadBlock(I)[J
    .locals 4
    .param p1, "sockID"    # I

    .prologue
    const/4 v3, 0x0

    .line 4811
    iget v1, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->minNotReadBlockID:I

    iget-object v2, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockStatus:[B
    invoke-static {v2}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1600(Lorg/apache/http/impl/client/MultiSocketInputStream;)[B

    move-result-object v2

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 4813
    .local v0, "tmpID":I
    :cond_0
    if-ltz v0, :cond_2

    iget-object v1, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockStatus:[B
    invoke-static {v1}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1600(Lorg/apache/http/impl/client/MultiSocketInputStream;)[B

    move-result-object v1

    array-length v1, v1

    if-ge v0, v1, :cond_2

    iget-object v1, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockStatus:[B
    invoke-static {v1}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1600(Lorg/apache/http/impl/client/MultiSocketInputStream;)[B

    move-result-object v1

    aget-byte v1, v1, v0

    if-lez v1, :cond_2

    .line 4814
    add-int/lit8 v0, v0, 0x1

    .line 4816
    iget-object v1, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockStatus:[B
    invoke-static {v1}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1600(Lorg/apache/http/impl/client/MultiSocketInputStream;)[B

    move-result-object v1

    array-length v1, v1

    if-ne v0, v1, :cond_0

    .line 4817
    sget-boolean v1, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v1, :cond_1

    .line 4818
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "id reach the maximum "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockStatus:[B
    invoke-static {v2}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1600(Lorg/apache/http/impl/client/MultiSocketInputStream;)[B

    move-result-object v2

    array-length v2, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 4821
    :cond_1
    new-array v1, v3, [J

    .line 4832
    :goto_0
    return-object v1

    .line 4824
    :cond_2
    if-ltz v0, :cond_3

    iget-object v1, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockStatus:[B
    invoke-static {v1}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1600(Lorg/apache/http/impl/client/MultiSocketInputStream;)[B

    move-result-object v1

    array-length v1, v1

    if-lt v0, v1, :cond_5

    .line 4825
    :cond_3
    sget-boolean v1, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v1, :cond_4

    .line 4826
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "get an illegal tmpID "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 4828
    :cond_4
    new-array v1, v3, [J

    goto :goto_0

    .line 4830
    :cond_5
    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->minNotReadBlockID:I

    .line 4831
    iget-object v1, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->httpRequestID:[I

    aput v0, v1, p1

    .line 4832
    invoke-direct {p0, v0, p1}, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->getNewBuf(II)[J

    move-result-object v1

    goto :goto_0
.end method

.method private getMinNotReadBlockID()I
    .locals 3

    .prologue
    .line 4790
    iget v1, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->minNotReadBlockID:I

    iget-object v2, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockStatus:[B
    invoke-static {v2}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1600(Lorg/apache/http/impl/client/MultiSocketInputStream;)[B

    move-result-object v2

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 4791
    .local v0, "tmpID":I
    :cond_0
    iget-object v1, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockStatus:[B
    invoke-static {v1}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1600(Lorg/apache/http/impl/client/MultiSocketInputStream;)[B

    move-result-object v1

    aget-byte v1, v1, v0

    if-lez v1, :cond_2

    .line 4792
    add-int/lit8 v0, v0, 0x1

    .line 4793
    iget-object v1, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockStatus:[B
    invoke-static {v1}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1600(Lorg/apache/http/impl/client/MultiSocketInputStream;)[B

    move-result-object v1

    array-length v1, v1

    if-ne v0, v1, :cond_0

    .line 4794
    sget-boolean v1, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v1, :cond_1

    .line 4795
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "in getMinNotReadBlockID, id reach the maximum "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockStatus:[B
    invoke-static {v2}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1600(Lorg/apache/http/impl/client/MultiSocketInputStream;)[B

    move-result-object v2

    array-length v2, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 4799
    :cond_1
    const/4 v1, -0x1

    .line 4802
    :goto_0
    return v1

    :cond_2
    move v1, v0

    goto :goto_0
.end method

.method private getNewBuf(II)[J
    .locals 13
    .param p1, "blockID"    # I
    .param p2, "sockID"    # I

    .prologue
    .line 4891
    const/4 v1, 0x4

    new-array v10, v1, [J

    .line 4892
    .local v10, "range":[J
    iget-object v1, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mInBuffer:Ljava/util/HashMap;
    invoke-static {v1}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1500(Lorg/apache/http/impl/client/MultiSocketInputStream;)Ljava/util/HashMap;

    move-result-object v12

    monitor-enter v12

    .line 4894
    :try_start_0
    iget-object v1, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockStatus:[B
    invoke-static {v1}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1600(Lorg/apache/http/impl/client/MultiSocketInputStream;)[B

    move-result-object v1

    aget-byte v1, v1, p1

    const/4 v2, -0x1

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mInBuffer:Ljava/util/HashMap;
    invoke-static {v1}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1500(Lorg/apache/http/impl/client/MultiSocketInputStream;)Ljava/util/HashMap;

    move-result-object v1

    new-instance v2, Ljava/lang/Integer;

    invoke-direct {v2, p1}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 4896
    sget-boolean v1, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v1, :cond_0

    .line 4897
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "block "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "is blocked and now it is occupied again"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 4900
    :cond_0
    iget-object v1, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mInBuffer:Ljava/util/HashMap;
    invoke-static {v1}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1500(Lorg/apache/http/impl/client/MultiSocketInputStream;)Ljava/util/HashMap;

    move-result-object v1

    new-instance v2, Ljava/lang/Integer;

    invoke-direct {v2, p1}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;

    .line 4901
    .local v9, "buf":Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;
    const/4 v1, 0x0

    invoke-virtual {v9}, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->getOffset()J

    move-result-wide v2

    aput-wide v2, v10, v1

    .line 4902
    const/4 v1, 0x1

    invoke-virtual {v9}, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->getOffset()J

    move-result-wide v2

    invoke-virtual {v9}, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->getToBeReadLength()J

    move-result-wide v4

    add-long/2addr v2, v4

    const-wide/16 v4, 0x1

    sub-long/2addr v2, v4

    aput-wide v2, v10, v1

    .line 4903
    const/4 v1, 0x2

    invoke-virtual {v9}, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->getToBeReadLength()J

    move-result-wide v2

    aput-wide v2, v10, v1

    .line 4904
    const/4 v1, 0x3

    int-to-long v2, p1

    aput-wide v2, v10, v1

    .line 4905
    invoke-virtual {v9, p2}, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->changeSockID(I)V

    .line 4940
    .end local v9    # "buf":Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;
    :goto_0
    iget-object v1, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockStatus:[B
    invoke-static {v1}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1600(Lorg/apache/http/impl/client/MultiSocketInputStream;)[B

    move-result-object v1

    const/4 v2, 0x1

    aput-byte v2, v1, p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4942
    :try_start_1
    iget-object v1, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mInBuffer:Ljava/util/HashMap;
    invoke-static {v1}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1500(Lorg/apache/http/impl/client/MultiSocketInputStream;)Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->notifyAll()V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 4945
    :goto_1
    :try_start_2
    monitor-exit v12

    .line 4946
    .end local v10    # "range":[J
    :goto_2
    return-object v10

    .line 4909
    .restart local v10    # "range":[J
    :cond_1
    iget-object v1, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockSize:I
    invoke-static {v1}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$900(Lorg/apache/http/impl/client/MultiSocketInputStream;)I

    move-result v1

    mul-int/2addr v1, p1

    int-to-long v2, v1

    iget-object v1, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mOffset:J
    invoke-static {v1}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1200(Lorg/apache/http/impl/client/MultiSocketInputStream;)J

    move-result-wide v4

    add-long/2addr v2, v4

    long-to-int v11, v2

    .line 4910
    .local v11, "start":I
    const/4 v1, 0x0

    int-to-long v2, v11

    aput-wide v2, v10, v1

    .line 4912
    const/4 v1, 0x0

    aget-wide v2, v10, v1

    iget-object v1, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    iget-wide v4, v1, Lorg/apache/http/impl/client/MultiSocketInputStream;->mOriOffset:J

    iget-object v1, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mContentLength:J
    invoke-static {v1}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$400(Lorg/apache/http/impl/client/MultiSocketInputStream;)J

    move-result-wide v6

    add-long/2addr v4, v6

    cmp-long v1, v2, v4

    if-ltz v1, :cond_3

    .line 4913
    sget-boolean v1, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v1, :cond_2

    .line 4914
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "start("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v2, 0x0

    aget-wide v2, v10, v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") >= totalLength("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    iget-wide v2, v2, Lorg/apache/http/impl/client/MultiSocketInputStream;->mOriOffset:J

    iget-object v4, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mContentLength:J
    invoke-static {v4}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$400(Lorg/apache/http/impl/client/MultiSocketInputStream;)J

    move-result-wide v4

    add-long/2addr v2, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "), break"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 4919
    :cond_2
    const/4 v1, 0x0

    new-array v10, v1, [J

    .end local v10    # "range":[J
    monitor-exit v12

    goto :goto_2

    .line 4945
    .end local v11    # "start":I
    :catchall_0
    move-exception v1

    monitor-exit v12
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    .line 4923
    .restart local v10    # "range":[J
    .restart local v11    # "start":I
    :cond_3
    :try_start_3
    iget-object v1, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockStatus:[B
    invoke-static {v1}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1600(Lorg/apache/http/impl/client/MultiSocketInputStream;)[B

    move-result-object v1

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ne p1, v1, :cond_5

    .line 4924
    const/4 v1, 0x1

    iget-object v2, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    iget-wide v2, v2, Lorg/apache/http/impl/client/MultiSocketInputStream;->mOriOffset:J

    iget-object v4, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mContentLength:J
    invoke-static {v4}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$400(Lorg/apache/http/impl/client/MultiSocketInputStream;)J

    move-result-wide v4

    add-long/2addr v2, v4

    const-wide/16 v4, 0x1

    sub-long/2addr v2, v4

    long-to-int v2, v2

    int-to-long v2, v2

    aput-wide v2, v10, v1

    .line 4925
    sget-boolean v1, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v1, :cond_4

    .line 4926
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Tail chunk byte range "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v2, 0x0

    aget-wide v2, v10, v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v2, 0x1

    aget-wide v2, v10, v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 4932
    :cond_4
    :goto_3
    const/4 v1, 0x2

    const/4 v2, 0x1

    aget-wide v2, v10, v2

    const/4 v4, 0x0

    aget-wide v4, v10, v4

    sub-long/2addr v2, v4

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    aput-wide v2, v10, v1

    .line 4933
    const/4 v1, 0x3

    int-to-long v2, p1

    aput-wide v2, v10, v1

    .line 4935
    new-instance v0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;

    iget-object v1, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    const/4 v2, 0x0

    aget-wide v2, v10, v2

    const/4 v4, 0x2

    aget-wide v4, v10, v4

    long-to-int v5, v4

    new-instance v6, Ljava/util/LinkedList;

    invoke-direct {v6}, Ljava/util/LinkedList;-><init>()V

    const/4 v8, 0x1

    move v4, p1

    move v7, p2

    invoke-direct/range {v0 .. v8}, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;-><init>(Lorg/apache/http/impl/client/MultiSocketInputStream;JIILjava/util/LinkedList;II)V

    .line 4937
    .local v0, "dbuf":Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;
    iget-object v1, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mInBuffer:Ljava/util/HashMap;
    invoke-static {v1}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1500(Lorg/apache/http/impl/client/MultiSocketInputStream;)Ljava/util/HashMap;

    move-result-object v1

    new-instance v2, Ljava/lang/Integer;

    invoke-direct {v2, p1}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 4929
    .end local v0    # "dbuf":Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;
    :cond_5
    const/4 v1, 0x1

    iget-object v2, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockSize:I
    invoke-static {v2}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$900(Lorg/apache/http/impl/client/MultiSocketInputStream;)I

    move-result v2

    add-int/2addr v2, v11

    add-int/lit8 v2, v2, -0x1

    int-to-long v2, v2

    iget-object v4, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    iget-wide v4, v4, Lorg/apache/http/impl/client/MultiSocketInputStream;->mOriOffset:J

    iget-object v6, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mContentLength:J
    invoke-static {v6}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$400(Lorg/apache/http/impl/client/MultiSocketInputStream;)J

    move-result-wide v6

    add-long/2addr v4, v6

    const-wide/16 v6, 0x1

    sub-long/2addr v4, v6

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v2

    long-to-int v2, v2

    int-to-long v2, v2

    aput-wide v2, v10, v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_3

    .line 4943
    .end local v11    # "start":I
    :catch_0
    move-exception v1

    goto/16 :goto_1
.end method

.method private statusToStr(I)Ljava/lang/String;
    .locals 1
    .param p1, "status"    # I

    .prologue
    .line 5128
    packed-switch p1, :pswitch_data_0

    .line 5142
    const-string v0, "UNKNOWN"

    :goto_0
    return-object v0

    .line 5130
    :pswitch_0
    const-string v0, "BLOCKED"

    goto :goto_0

    .line 5132
    :pswitch_1
    const-string v0, "NOT_READ"

    goto :goto_0

    .line 5134
    :pswitch_2
    const-string v0, "OCCUPIED"

    goto :goto_0

    .line 5136
    :pswitch_3
    const-string v0, "READING"

    goto :goto_0

    .line 5138
    :pswitch_4
    const-string v0, "FULLREAD"

    goto :goto_0

    .line 5140
    :pswitch_5
    const-string v0, "CLEARED"

    goto :goto_0

    .line 5128
    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method


# virtual methods
.method public block(II)V
    .locals 2
    .param p1, "blockID"    # I
    .param p2, "socketID"    # I

    .prologue
    const/4 v1, -0x1

    .line 4527
    iget-object v0, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockStatus:[B
    invoke-static {v0}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1600(Lorg/apache/http/impl/client/MultiSocketInputStream;)[B

    move-result-object v0

    aput-byte v1, v0, p1

    .line 4528
    iget v0, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->minNotReadBlockID:I

    invoke-static {v0, p1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->minNotReadBlockID:I

    .line 4529
    iget-object v0, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->httpRequestID:[I

    aput v1, v0, p2

    .line 4530
    return-void
.end method

.method public blockStatusToStr()Ljava/lang/String;
    .locals 6

    .prologue
    .line 5097
    new-instance v3, Ljava/lang/StringBuffer;

    const-string v4, "BlockStatus: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 5098
    .local v3, "str":Ljava/lang/StringBuffer;
    iget-object v4, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockStatus:[B
    invoke-static {v4}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1600(Lorg/apache/http/impl/client/MultiSocketInputStream;)[B

    move-result-object v4

    array-length v4, v4

    add-int/lit8 v2, v4, -0x1

    .line 5099
    .local v2, "min":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v4, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockStatus:[B
    invoke-static {v4}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1600(Lorg/apache/http/impl/client/MultiSocketInputStream;)[B

    move-result-object v4

    array-length v4, v4

    if-ge v0, v4, :cond_0

    .line 5100
    iget-object v4, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockStatus:[B
    invoke-static {v4}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1600(Lorg/apache/http/impl/client/MultiSocketInputStream;)[B

    move-result-object v4

    aget-byte v4, v4, v0

    const/4 v5, 0x4

    if-ge v4, v5, :cond_2

    .line 5101
    add-int/lit8 v4, v0, -0x2

    const/4 v5, 0x0

    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 5105
    :cond_0
    add-int/lit8 v4, v2, 0x2

    iget-object v5, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockStatus:[B
    invoke-static {v5}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1600(Lorg/apache/http/impl/client/MultiSocketInputStream;)[B

    move-result-object v5

    array-length v5, v5

    add-int/lit8 v5, v5, -0x1

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 5106
    .local v1, "max":I
    iget-object v4, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockStatus:[B
    invoke-static {v4}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1600(Lorg/apache/http/impl/client/MultiSocketInputStream;)[B

    move-result-object v4

    array-length v4, v4

    add-int/lit8 v0, v4, -0x1

    :goto_1
    if-le v0, v2, :cond_1

    .line 5107
    iget-object v4, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockStatus:[B
    invoke-static {v4}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1600(Lorg/apache/http/impl/client/MultiSocketInputStream;)[B

    move-result-object v4

    aget-byte v4, v4, v0

    if-eqz v4, :cond_3

    .line 5108
    add-int/lit8 v4, v0, 0x1

    iget-object v5, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockStatus:[B
    invoke-static {v5}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1600(Lorg/apache/http/impl/client/MultiSocketInputStream;)[B

    move-result-object v5

    array-length v5, v5

    add-int/lit8 v5, v5, -0x1

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 5113
    :cond_1
    move v0, v2

    :goto_2
    if-gt v0, v1, :cond_4

    .line 5114
    invoke-virtual {v3, v0}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string v5, "-"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    iget-object v5, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockStatus:[B
    invoke-static {v5}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1600(Lorg/apache/http/impl/client/MultiSocketInputStream;)[B

    move-result-object v5

    aget-byte v5, v5, v0

    invoke-direct {p0, v5}, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->statusToStr(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 5113
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 5099
    .end local v1    # "max":I
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 5106
    .restart local v1    # "max":I
    :cond_3
    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    .line 5117
    :cond_4
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4
.end method

.method public getMaxFetchingBlock()I
    .locals 3

    .prologue
    .line 4520
    iget-object v0, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->httpRequestID:[I

    const/4 v1, 0x0

    aget v0, v0, v1

    iget-object v1, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->httpRequestID:[I

    const/4 v2, 0x1

    aget v1, v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method

.method public getNextHTTPBlock(ILorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;)[J
    .locals 32
    .param p1, "sockID"    # I
    .param p2, "thisBuf"    # Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;

    .prologue
    .line 4542
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    move/from16 v1, p1

    # invokes: Lorg/apache/http/impl/client/MultiSocketInputStream;->getOtherSocketID(I)I
    invoke-static {v0, v1}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$5700(Lorg/apache/http/impl/client/MultiSocketInputStream;I)I

    move-result v15

    .line 4543
    .local v15, "otherSockID":I
    sget-boolean v27, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v27, :cond_0

    .line 4544
    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    const-string v28, "getNextHTTPBlock("

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, v27

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v27

    const-string v28, ","

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {p0 .. p0}, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->toString()Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    invoke-static/range {v27 .. v27}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 4546
    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    const-string v28, "Current Socket status:"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    move-object/from16 v28, v0

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mRequestHandlers:[Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;
    invoke-static/range {v28 .. v28}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$4000(Lorg/apache/http/impl/client/MultiSocketInputStream;)[Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;

    move-result-object v28

    aget-object v28, v28, p1

    move-object/from16 v0, v28

    iget v0, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->rrStatus:I

    move/from16 v28, v0

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v27

    const-string v28, ", Other Socket status:"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    move-object/from16 v28, v0

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mRequestHandlers:[Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;
    invoke-static/range {v28 .. v28}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$4000(Lorg/apache/http/impl/client/MultiSocketInputStream;)[Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;

    move-result-object v28

    aget-object v28, v28, v15

    move-object/from16 v0, v28

    iget v0, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->rrStatus:I

    move/from16 v28, v0

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    invoke-static/range {v27 .. v27}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 4551
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    move-object/from16 v27, v0

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mRequestHandlers:[Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;
    invoke-static/range {v27 .. v27}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$4000(Lorg/apache/http/impl/client/MultiSocketInputStream;)[Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;

    move-result-object v27

    aget-object v27, v27, p1

    move-object/from16 v0, v27

    iget v0, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->rrStatus:I

    move/from16 v27, v0

    const/16 v28, -0x1

    move/from16 v0, v27

    move/from16 v1, v28

    if-ne v0, v1, :cond_1

    .line 4553
    const/16 v27, 0x1

    move/from16 v0, v27

    new-array v0, v0, [J

    move-object/from16 v22, v0

    const/16 v27, 0x0

    const-wide/16 v28, -0x1

    aput-wide v28, v22, v27

    .line 4778
    :goto_0
    return-object v22

    .line 4555
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    move-object/from16 v27, v0

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mRequestHandlers:[Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;
    invoke-static/range {v27 .. v27}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$4000(Lorg/apache/http/impl/client/MultiSocketInputStream;)[Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;

    move-result-object v27

    aget-object v27, v27, v15

    move-object/from16 v0, v27

    iget v0, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->rrStatus:I

    move/from16 v27, v0

    const/16 v28, -0x1

    move/from16 v0, v27

    move/from16 v1, v28

    if-ne v0, v1, :cond_2

    .line 4558
    invoke-direct/range {p0 .. p1}, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->getContinuousChunk(I)[J

    move-result-object v22

    goto :goto_0

    .line 4561
    :cond_2
    if-nez p2, :cond_4

    .line 4562
    sget v27, Lorg/apache/http/impl/client/MultiSocketInputStream;->DEFAULT_INTERFACE_FOR_INITIAL_CHUNK:I

    move/from16 v0, p1

    move/from16 v1, v27

    if-ne v0, v1, :cond_3

    const/16 v23, 0x0

    .line 4563
    .local v23, "targetChunk":I
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    move-object/from16 v27, v0

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockStatus:[B
    invoke-static/range {v27 .. v27}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1600(Lorg/apache/http/impl/client/MultiSocketInputStream;)[B

    move-result-object v27

    move-object/from16 v0, v27

    array-length v0, v0

    move/from16 v27, v0

    move/from16 v0, v23

    move/from16 v1, v27

    if-ge v0, v1, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    move-object/from16 v27, v0

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockStatus:[B
    invoke-static/range {v27 .. v27}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1600(Lorg/apache/http/impl/client/MultiSocketInputStream;)[B

    move-result-object v27

    aget-byte v27, v27, v23

    if-nez v27, :cond_4

    .line 4565
    move-object/from16 v0, p0

    move/from16 v1, p1

    move/from16 v2, v23

    invoke-direct {v0, v1, v2}, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->getBlockForWithID(II)[J

    move-result-object v22

    goto :goto_0

    .line 4562
    .end local v23    # "targetChunk":I
    :cond_3
    const/16 v23, 0x1

    goto :goto_1

    .line 4573
    :cond_4
    const-wide/16 v6, 0x0

    .line 4574
    .local v6, "fRatio":D
    const/16 v20, 0x1

    .line 4577
    .local v20, "ratio":I
    if-eqz p2, :cond_5

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->httpRequestID:[I

    move-object/from16 v27, v0

    aget v27, v27, v15

    if-gez v27, :cond_7

    .line 4578
    :cond_5
    sget-boolean v27, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v27, :cond_6

    .line 4579
    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    const-string v28, "first time getNextHTTPBlock for socket "

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, v27

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v27

    const-string v28, " or the other socket is not active"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    invoke-static/range {v27 .. v27}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 4582
    :cond_6
    invoke-direct/range {p0 .. p1}, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->getMinNotReadBlock(I)[J

    move-result-object v22

    goto/16 :goto_0

    .line 4584
    :cond_7
    const-wide/16 v24, 0x0

    .line 4585
    .local v24, "thisSpeed":D
    const-wide/16 v18, 0x0

    .line 4586
    .local v18, "otherSpeed":D
    const-wide/16 v12, 0x0

    .line 4587
    .local v12, "lTS":J
    const-wide/16 v10, 0x0

    .line 4590
    .local v10, "lOS":J
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->socketSpeed:[J

    move-object/from16 v27, v0

    aget-wide v12, v27, p1

    .line 4591
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->socketSpeed:[J

    move-object/from16 v27, v0

    aget-wide v10, v27, v15

    .line 4593
    const-wide/16 v28, 0x0

    cmp-long v27, v12, v28

    if-nez v27, :cond_8

    .line 4594
    invoke-direct/range {p0 .. p1}, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->getMinNotReadBlock(I)[J
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v22

    goto/16 :goto_0

    .line 4596
    :cond_8
    long-to-double v0, v12

    move-wide/from16 v24, v0

    .line 4597
    long-to-double v0, v10

    move-wide/from16 v18, v0

    .line 4605
    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->MIN_RATIO_FOR_ONLY_ONE_INF_0V1:I
    invoke-static {}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$5100()I

    move-result v27

    if-ltz v27, :cond_c

    sget v27, Lorg/apache/http/impl/client/MultiSocketInputStream;->SPEED_RATIO_MAKE_STOPPED:I

    move/from16 v0, v27

    int-to-double v0, v0

    move-wide/from16 v28, v0

    mul-double v28, v28, v18

    cmpl-double v27, v24, v28

    if-gtz v27, :cond_9

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    move-object/from16 v27, v0

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mRequestHandlers:[Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;
    invoke-static/range {v27 .. v27}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$4000(Lorg/apache/http/impl/client/MultiSocketInputStream;)[Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;

    move-result-object v27

    aget-object v27, v27, v15

    move-object/from16 v0, v27

    iget v0, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->rrExceptionCount:I

    move/from16 v27, v0

    sget v28, Lorg/apache/http/impl/client/MultiSocketInputStream;->MAX_EXCEPTION_COUNT:I

    move/from16 v0, v27

    move/from16 v1, v28

    if-le v0, v1, :cond_c

    .line 4607
    :cond_9
    sget-boolean v27, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v27, :cond_a

    .line 4608
    const-string v27, "The speed of other socket is slow so closing and setting the thread status as RR_STOPPED"

    invoke-static/range {v27 .. v27}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 4612
    :cond_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    move-object/from16 v27, v0

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mRequestHandlers:[Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;
    invoke-static/range {v27 .. v27}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$4000(Lorg/apache/http/impl/client/MultiSocketInputStream;)[Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;

    move-result-object v27

    aget-object v27, v27, v15

    invoke-virtual/range {v27 .. v27}, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->closeHTTP()V

    .line 4613
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    move-object/from16 v27, v0

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mRequestHandlers:[Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;
    invoke-static/range {v27 .. v27}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$4000(Lorg/apache/http/impl/client/MultiSocketInputStream;)[Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;

    move-result-object v27

    aget-object v27, v27, v15

    const/16 v28, -0x1

    move/from16 v0, v28

    move-object/from16 v1, v27

    iput v0, v1, Lorg/apache/http/impl/client/MultiSocketInputStream$RangeRequest;->rrStatus:I

    .line 4614
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    move-object/from16 v27, v0

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockManager:Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;
    invoke-static/range {v27 .. v27}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$800(Lorg/apache/http/impl/client/MultiSocketInputStream;)Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;

    move-result-object v28

    monitor-enter v28

    .line 4616
    :try_start_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    move-object/from16 v27, v0

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockManager:Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;
    invoke-static/range {v27 .. v27}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$800(Lorg/apache/http/impl/client/MultiSocketInputStream;)Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;

    move-result-object v27

    const-wide/16 v30, 0x3e8

    move-object/from16 v0, v27

    move-wide/from16 v1, v30

    invoke-virtual {v0, v1, v2}, Ljava/lang/Object;->wait(J)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 4619
    :goto_2
    :try_start_2
    monitor-exit v28
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 4620
    invoke-direct/range {p0 .. p1}, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->getContinuousChunk(I)[J

    move-result-object v22

    goto/16 :goto_0

    .line 4598
    :catch_0
    move-exception v5

    .line 4599
    .local v5, "e":Ljava/lang/Throwable;
    sget-boolean v27, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v27, :cond_b

    .line 4600
    invoke-static {v5}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/Throwable;)V

    .line 4601
    :cond_b
    invoke-direct/range {p0 .. p1}, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->getMinNotReadBlock(I)[J

    move-result-object v22

    goto/16 :goto_0

    .line 4619
    .end local v5    # "e":Ljava/lang/Throwable;
    :catchall_0
    move-exception v27

    :try_start_3
    monitor-exit v28
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v27

    .line 4623
    :cond_c
    invoke-direct/range {p0 .. p0}, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->getMinNotReadBlockID()I

    move-result v26

    .line 4624
    .local v26, "toBeDoID":I
    if-gez v26, :cond_e

    .line 4625
    sget-boolean v27, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v27, :cond_d

    .line 4626
    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    const-string v28, "all block has been read or is reading "

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, v27

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    invoke-static/range {v27 .. v27}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 4630
    :cond_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->httpRequestID:[I

    move-object/from16 v27, v0

    aget v27, v27, v15

    if-ltz v27, :cond_11

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->httpRequestID:[I

    move-object/from16 v27, v0

    aget v27, v27, v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    move-object/from16 v28, v0

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockStatus:[B
    invoke-static/range {v28 .. v28}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1600(Lorg/apache/http/impl/client/MultiSocketInputStream;)[B

    move-result-object v28

    move-object/from16 v0, v28

    array-length v0, v0

    move/from16 v28, v0

    move/from16 v0, v27

    move/from16 v1, v28

    if-ge v0, v1, :cond_11

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    move-object/from16 v27, v0

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockStatus:[B
    invoke-static/range {v27 .. v27}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1600(Lorg/apache/http/impl/client/MultiSocketInputStream;)[B

    move-result-object v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->httpRequestID:[I

    move-object/from16 v28, v0

    aget v28, v28, v15

    aget-byte v27, v27, v28

    const/16 v28, 0x2

    move/from16 v0, v27

    move/from16 v1, v28

    if-gt v0, v1, :cond_11

    cmpl-double v27, v24, v18

    if-lez v27, :cond_11

    invoke-virtual/range {p2 .. p2}, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->getSockID()I

    move-result v27

    move/from16 v0, v27

    move/from16 v1, p1

    if-ne v0, v1, :cond_11

    .line 4635
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    move-object/from16 v27, v0

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mBlockStatus:[B
    invoke-static/range {v27 .. v27}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1600(Lorg/apache/http/impl/client/MultiSocketInputStream;)[B

    move-result-object v27

    move-object/from16 v0, v27

    array-length v0, v0

    move/from16 v27, v0

    add-int/lit8 v26, v27, -0x1

    .line 4640
    :cond_e
    sget-boolean v27, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v27, :cond_f

    .line 4641
    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    const-string v28, "thisSpeed="

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, v27

    move-wide/from16 v1, v24

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v27

    const-string v28, ", otherSpeed="

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, v27

    move-wide/from16 v1, v18

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v27

    const-string v28, ", toBeDoID="

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, v27

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v27

    const-string v28, ", httpRequestID[otherSockID]="

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->httpRequestID:[I

    move-object/from16 v28, v0

    aget v28, v28, v15

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    invoke-static/range {v27 .. v27}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 4646
    :cond_f
    cmpl-double v27, v24, v18

    if-ltz v27, :cond_1f

    invoke-virtual/range {p2 .. p2}, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->getSockID()I

    move-result v27

    move/from16 v0, v27

    move/from16 v1, p1

    if-ne v0, v1, :cond_1f

    .line 4649
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->httpRequestID:[I

    move-object/from16 v27, v0

    aget v27, v27, v15

    move/from16 v0, v26

    move/from16 v1, v27

    if-ge v0, v1, :cond_12

    .line 4650
    sget-boolean v27, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v27, :cond_10

    .line 4651
    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    const-string v28, "get a smaller block in getNextHTTPBlock for socket "

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, v27

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v27

    const-string v28, ", toBeDoID="

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, v27

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v27

    const-string v28, ", httpRequestID[otherSockID]="

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->httpRequestID:[I

    move-object/from16 v28, v0

    aget v28, v28, v15

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    invoke-static/range {v27 .. v27}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 4659
    :cond_10
    invoke-direct/range {p0 .. p1}, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->getMinNotReadBlock(I)[J

    move-result-object v22

    goto/16 :goto_0

    .line 4637
    :cond_11
    const/16 v27, 0x0

    move/from16 v0, v27

    new-array v0, v0, [J

    move-object/from16 v22, v0

    goto/16 :goto_0

    .line 4663
    :cond_12
    const/4 v14, 0x0

    .line 4664
    .local v14, "otherBuf":Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    move-object/from16 v27, v0

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mInBuffer:Ljava/util/HashMap;
    invoke-static/range {v27 .. v27}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1500(Lorg/apache/http/impl/client/MultiSocketInputStream;)Ljava/util/HashMap;

    move-result-object v28

    monitor-enter v28

    .line 4665
    :try_start_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    move-object/from16 v27, v0

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mInBuffer:Ljava/util/HashMap;
    invoke-static/range {v27 .. v27}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1500(Lorg/apache/http/impl/client/MultiSocketInputStream;)Ljava/util/HashMap;

    move-result-object v27

    new-instance v29, Ljava/lang/Integer;

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->httpRequestID:[I

    move-object/from16 v30, v0

    aget v30, v30, v15

    invoke-direct/range {v29 .. v30}, Ljava/lang/Integer;-><init>(I)V

    move-object/from16 v0, v27

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v27

    move-object/from16 v0, v27

    check-cast v0, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;

    move-object v14, v0

    .line 4666
    monitor-exit v28
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 4668
    if-nez v14, :cond_14

    .line 4669
    sget-boolean v27, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v27, :cond_13

    .line 4670
    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    const-string v28, "otherBuf is null in getNextHTTPBlock for socket "

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, v27

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    invoke-static/range {v27 .. v27}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 4674
    :cond_13
    invoke-direct/range {p0 .. p1}, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->getMinNotReadBlock(I)[J

    move-result-object v22

    goto/16 :goto_0

    .line 4666
    :catchall_1
    move-exception v27

    :try_start_5
    monitor-exit v28
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    throw v27

    .line 4677
    :cond_14
    const-wide v28, 0x3f50624dd2f1a9fcL    # 0.001

    cmpg-double v27, v18, v28

    if-gez v27, :cond_18

    .line 4680
    const-wide v6, 0x408f400000000000L    # 1000.0

    .line 4685
    :goto_3
    const-wide/16 v16, 0x0

    .line 4686
    .local v16, "otherRest":J
    const/4 v4, 0x0

    .line 4688
    .local v4, "bSwitch":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->toBeReadLen:[J

    move-object/from16 v27, v0

    aget-wide v16, v27, v15

    .line 4689
    const-wide/16 v28, 0x0

    cmp-long v27, v10, v28

    if-nez v27, :cond_1a

    .line 4690
    const-wide/16 v28, 0x0

    cmp-long v27, v16, v28

    if-lez v27, :cond_19

    const/4 v4, 0x1

    .line 4704
    :goto_4
    sget-boolean v27, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v27, :cond_15

    .line 4705
    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    const-string v28, "prepare for switch socket "

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, v27

    invoke-virtual {v0, v6, v7}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v27

    const-string v28, ", "

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, v27

    move-wide/from16 v1, v16

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v27

    const-string v28, ", "

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    const-string v28, ", "

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->headerTime:[J

    move-object/from16 v28, v0

    aget-wide v28, v28, p1

    invoke-virtual/range {v27 .. v29}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    invoke-static/range {v27 .. v27}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 4708
    :cond_15
    if-eqz v4, :cond_1d

    .line 4709
    sget-boolean v27, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v27, :cond_16

    .line 4710
    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    const-string v28, "ratio or rest len is big, switch socket "

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, v27

    invoke-virtual {v0, v6, v7}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v27

    const-string v28, " , "

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, v27

    move-wide/from16 v1, v16

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    invoke-static/range {v27 .. v27}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 4714
    :cond_16
    const/16 v27, 0x0

    move/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v14, v0, v1}, Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;->switchSocket(IZ)[J

    move-result-object v21

    .line 4715
    .local v21, "ret":[J
    if-eqz v21, :cond_17

    move-object/from16 v0, v21

    array-length v0, v0

    move/from16 v27, v0

    if-nez v27, :cond_1c

    .line 4716
    :cond_17
    invoke-direct/range {p0 .. p1}, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->getMinNotReadBlock(I)[J

    move-result-object v22

    goto/16 :goto_0

    .line 4682
    .end local v4    # "bSwitch":Z
    .end local v16    # "otherRest":J
    .end local v21    # "ret":[J
    :cond_18
    div-double v6, v24, v18

    goto/16 :goto_3

    .line 4690
    .restart local v4    # "bSwitch":Z
    .restart local v16    # "otherRest":J
    :cond_19
    const/4 v4, 0x0

    goto/16 :goto_4

    .line 4692
    :cond_1a
    const-wide/high16 v28, 0x4000000000000000L    # 2.0

    cmpl-double v27, v6, v28

    if-lez v27, :cond_1b

    const-wide/16 v28, 0x8

    mul-long v28, v28, v16

    div-long v28, v28, v12

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->headerTime:[J

    move-object/from16 v27, v0

    aget-wide v30, v27, p1

    add-long v28, v28, v30

    const-wide/16 v30, 0x8

    mul-long v30, v30, v16

    div-long v30, v30, v10

    cmp-long v27, v28, v30

    if-gez v27, :cond_1b

    const/4 v4, 0x1

    :goto_5
    goto/16 :goto_4

    :cond_1b
    const/4 v4, 0x0

    goto :goto_5

    .line 4718
    .restart local v21    # "ret":[J
    :cond_1c
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->httpRequestID:[I

    move-object/from16 v27, v0

    const/16 v28, 0x3

    aget-wide v28, v21, v28

    move-wide/from16 v0, v28

    long-to-int v0, v0

    move/from16 v28, v0

    aput v28, v27, p1

    move-object/from16 v22, v21

    .line 4719
    goto/16 :goto_0

    .line 4722
    .end local v21    # "ret":[J
    :cond_1d
    sget-boolean v27, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v27, :cond_1e

    .line 4723
    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    const-string v28, "ratio or rest len is not too big, get next block "

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, v27

    invoke-virtual {v0, v6, v7}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v27

    const-string v28, " , "

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, v27

    move-wide/from16 v1, v16

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    invoke-static/range {v27 .. v27}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 4727
    :cond_1e
    invoke-direct/range {p0 .. p1}, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->getMinNotReadBlock(I)[J

    move-result-object v22

    goto/16 :goto_0

    .line 4733
    .end local v4    # "bSwitch":Z
    .end local v14    # "otherBuf":Lorg/apache/http/impl/client/MultiSocketInputStream$DataBuffer;
    .end local v16    # "otherRest":J
    :cond_1f
    div-double v6, v18, v24

    .line 4734
    sget-boolean v27, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v27, :cond_20

    .line 4735
    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    const-string v28, "this socket slower than other speed, fRatio="

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, v27

    invoke-virtual {v0, v6, v7}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v27

    const-string v28, ", other socket is downloading "

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->httpRequestID:[I

    move-object/from16 v28, v0

    aget v28, v28, v15

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v27

    const-string v28, ", toBeDoID is "

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, v27

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    invoke-static/range {v27 .. v27}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 4743
    :cond_20
    const-wide/high16 v28, 0x3ff8000000000000L    # 1.5

    cmpg-double v27, v6, v28

    if-gez v27, :cond_22

    .line 4744
    sget-boolean v27, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v27, :cond_21

    .line 4745
    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    const-string v28, "this socket is not too slow, download toBeDoID, ratio="

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, v27

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    invoke-static/range {v27 .. v27}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 4749
    :cond_21
    invoke-direct/range {p0 .. p1}, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->getMinNotReadBlock(I)[J

    move-result-object v22

    goto/16 :goto_0

    .line 4750
    :cond_22
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->httpRequestID:[I

    move-object/from16 v27, v0

    aget v27, v27, v15

    move/from16 v0, v26

    move/from16 v1, v27

    if-ge v0, v1, :cond_24

    .line 4751
    sget-boolean v27, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v27, :cond_23

    .line 4752
    const-string v27, "this socket is slow, but there is blank portion, still download smallest portion"

    invoke-static/range {v27 .. v27}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 4754
    :cond_23
    invoke-direct/range {p0 .. p1}, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->getMinNotReadBlock(I)[J

    move-result-object v22

    goto/16 :goto_0

    .line 4760
    :cond_24
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    move-object/from16 v27, v0

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mInBuffer:Ljava/util/HashMap;
    invoke-static/range {v27 .. v27}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1500(Lorg/apache/http/impl/client/MultiSocketInputStream;)Ljava/util/HashMap;

    move-result-object v28

    monitor-enter v28

    .line 4761
    :try_start_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    move-object/from16 v27, v0

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mInBuffer:Ljava/util/HashMap;
    invoke-static/range {v27 .. v27}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$1500(Lorg/apache/http/impl/client/MultiSocketInputStream;)Ljava/util/HashMap;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/util/HashMap;->size()I

    move-result v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->this$0:Lorg/apache/http/impl/client/MultiSocketInputStream;

    move-object/from16 v29, v0

    # getter for: Lorg/apache/http/impl/client/MultiSocketInputStream;->mMaxBlockNumber:I
    invoke-static/range {v29 .. v29}, Lorg/apache/http/impl/client/MultiSocketInputStream;->access$3700(Lorg/apache/http/impl/client/MultiSocketInputStream;)I

    move-result v29

    move/from16 v0, v27

    move/from16 v1, v29

    if-lt v0, v1, :cond_25

    .line 4762
    invoke-direct/range {p0 .. p1}, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->getMinNotReadBlock(I)[J

    move-result-object v22

    monitor-exit v28

    goto/16 :goto_0

    .line 4764
    :catchall_2
    move-exception v27

    monitor-exit v28
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    throw v27

    :cond_25
    :try_start_7
    monitor-exit v28
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 4767
    invoke-static {v6, v7}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v28

    move-wide/from16 v0, v28

    double-to-int v0, v0

    move/from16 v27, v0

    sget v28, Lorg/apache/http/impl/client/MultiSocketInputStream;->MAX_JUMP_STEP:I

    invoke-static/range {v27 .. v28}, Ljava/lang/Math;->min(II)I

    move-result v8

    .line 4769
    .local v8, "iR":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->httpRequestID:[I

    move-object/from16 v27, v0

    aget v27, v27, v15

    add-int v9, v8, v27

    .line 4770
    .local v9, "nextid":I
    sget-boolean v27, Lorg/apache/http/impl/client/CustomHttpClient;->APACHE_HTTP_DBG:Z

    if-eqz v27, :cond_26

    .line 4771
    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    const-string v28, "this socket is slow, download with step, ratio="

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, v27

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v27

    const-string v28, ", otherRequestID="

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->httpRequestID:[I

    move-object/from16 v28, v0

    aget v28, v28, v15

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v27

    const-string v28, ", nextid="

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, v27

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    invoke-static/range {v27 .. v27}, Lorg/apache/http/impl/client/CustomHttpClient;->log(Ljava/lang/String;)V

    .line 4778
    :cond_26
    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-direct {v0, v1, v9}, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->getBlockForWithID(II)[J

    move-result-object v22

    goto/16 :goto_0

    .line 4617
    .end local v8    # "iR":I
    .end local v9    # "nextid":I
    .end local v26    # "toBeDoID":I
    :catch_1
    move-exception v27

    goto/16 :goto_2
.end method

.method public setSpeed(IJ)V
    .locals 2
    .param p1, "sid"    # I
    .param p2, "speed"    # J

    .prologue
    .line 4511
    iget-object v0, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->socketSpeed:[J

    aput-wide p2, v0, p1

    .line 4512
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 4493
    new-instance v1, Ljava/lang/StringBuffer;

    const-string v2, "BlockManager:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 4494
    .local v1, "sb":Ljava/lang/StringBuffer;
    const-string v2, "httpRequestID{"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 4495
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/4 v2, 0x2

    if-ge v0, v2, :cond_0

    .line 4496
    iget-object v2, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->httpRequestID:[I

    aget v2, v2, v0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 4495
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 4498
    :cond_0
    const-string v2, "}; "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 4499
    const-string v2, "minNotReadBlockID{"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    iget v3, p0, Lorg/apache/http/impl/client/MultiSocketInputStream$BlockManager;->minNotReadBlockID:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v3, "}"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 4501
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method
