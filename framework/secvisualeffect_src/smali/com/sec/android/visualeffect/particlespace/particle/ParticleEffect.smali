.class public Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;
.super Landroid/view/View;
.source "ParticleEffect.java"


# instance fields
.field private TAG:Ljava/lang/String;

.field private dotMaxLimit:I

.field private dotUnlockSpeed:I

.field private drawingBottom:I

.field private drawingDelayTime:I

.field private drawingLeft:I

.field private drawingMargin:I

.field private drawingRight:I

.field private drawingTop:I

.field private hsvOrigin:[F

.field private hsvTemp:[F

.field private initCreatedDotAmount:I

.field private isDrawing:Z

.field private isPaused:Z

.field private lastAddedColor:I

.field private lastAddedX:F

.field private lastAddedY:F

.field mHandler:Landroid/os/Handler;

.field private nextParticleIndex:I

.field private particleAliveList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/visualeffect/lock/particle/Particle;",
            ">;"
        }
    .end annotation
.end field

.field private particleTotalList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/visualeffect/lock/particle/Particle;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 12
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v11, 0x3

    const/4 v10, 0x1

    const/4 v9, 0x0

    const/4 v8, 0x0

    .line 45
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 23
    const-string v7, "VisualEffectParticleEffect"

    iput-object v7, p0, Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;->TAG:Ljava/lang/String;

    .line 24
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    iput-object v7, p0, Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;->particleTotalList:Ljava/util/ArrayList;

    .line 25
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    iput-object v7, p0, Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;->particleAliveList:Ljava/util/ArrayList;

    .line 27
    const/4 v7, 0x2

    iput v7, p0, Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;->drawingDelayTime:I

    .line 28
    const/16 v7, 0xfa

    iput v7, p0, Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;->initCreatedDotAmount:I

    .line 29
    const/16 v7, 0x96

    iput v7, p0, Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;->dotMaxLimit:I

    .line 30
    const/4 v7, 0x5

    iput v7, p0, Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;->dotUnlockSpeed:I

    .line 31
    iput v9, p0, Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;->lastAddedX:F

    .line 32
    iput v9, p0, Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;->lastAddedY:F

    .line 33
    iput v8, p0, Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;->lastAddedColor:I

    .line 36
    iput v8, p0, Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;->drawingLeft:I

    .line 37
    iput v8, p0, Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;->drawingTop:I

    .line 38
    iput v10, p0, Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;->drawingRight:I

    .line 39
    iput v10, p0, Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;->drawingBottom:I

    .line 40
    const/16 v7, 0xb

    iput v7, p0, Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;->drawingMargin:I

    .line 41
    const/4 v7, -0x1

    iput v7, p0, Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;->nextParticleIndex:I

    .line 42
    iput-boolean v8, p0, Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;->isPaused:Z

    .line 143
    new-instance v7, Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect$1;

    invoke-direct {v7, p0}, Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect$1;-><init>(Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;)V

    iput-object v7, p0, Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;->mHandler:Landroid/os/Handler;

    .line 46
    new-array v7, v11, [F

    iput-object v7, p0, Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;->hsvOrigin:[F

    .line 47
    new-array v7, v11, [F

    iput-object v7, p0, Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;->hsvTemp:[F

    .line 49
    invoke-virtual {p0}, Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 50
    .local v0, "dm":Landroid/util/DisplayMetrics;
    iget v5, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 51
    .local v5, "screenWidth":I
    iget v4, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 52
    .local v4, "screenHeight":I
    if-ge v5, v4, :cond_0

    move v6, v5

    .line 54
    .local v6, "smallestWidth":I
    :goto_0
    int-to-float v7, v6

    const/high16 v8, 0x44870000    # 1080.0f

    div-float v3, v7, v8

    .line 55
    .local v3, "ratio":F
    iget-object v7, p0, Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "ParticleEffect : Constructor, "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " x "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 56
    iget-object v7, p0, Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "ParticleEffect : ratio = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 58
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    iget v7, p0, Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;->initCreatedDotAmount:I

    if-ge v2, v7, :cond_1

    .line 59
    new-instance v1, Lcom/samsung/android/visualeffect/lock/particle/Particle;

    invoke-direct {v1, v3}, Lcom/samsung/android/visualeffect/lock/particle/Particle;-><init>(F)V

    .line 60
    .local v1, "dot":Lcom/samsung/android/visualeffect/lock/particle/Particle;
    iget-object v7, p0, Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;->particleTotalList:Ljava/util/ArrayList;

    invoke-virtual {v7, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 58
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .end local v1    # "dot":Lcom/samsung/android/visualeffect/lock/particle/Particle;
    .end local v2    # "i":I
    .end local v3    # "ratio":F
    .end local v6    # "smallestWidth":I
    :cond_0
    move v6, v4

    .line 52
    goto :goto_0

    .line 62
    .restart local v2    # "i":I
    .restart local v3    # "ratio":F
    .restart local v6    # "smallestWidth":I
    :cond_1
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;->isAvailableRect()Z

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;

    .prologue
    .line 21
    iget v0, p0, Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;->drawingLeft:I

    return v0
.end method

.method static synthetic access$200(Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;

    .prologue
    .line 21
    iget v0, p0, Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;->drawingMargin:I

    return v0
.end method

.method static synthetic access$300(Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;

    .prologue
    .line 21
    iget v0, p0, Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;->drawingTop:I

    return v0
.end method

.method static synthetic access$400(Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;

    .prologue
    .line 21
    iget v0, p0, Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;->drawingRight:I

    return v0
.end method

.method static synthetic access$500(Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;

    .prologue
    .line 21
    iget v0, p0, Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;->drawingBottom:I

    return v0
.end method

.method static synthetic access$600(Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;

    .prologue
    .line 21
    iget-boolean v0, p0, Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;->isDrawing:Z

    return v0
.end method

.method static synthetic access$700(Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;

    .prologue
    .line 21
    iget-boolean v0, p0, Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;->isPaused:Z

    return v0
.end method

.method static synthetic access$800(Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;

    .prologue
    .line 21
    iget v0, p0, Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;->drawingDelayTime:I

    return v0
.end method

.method private getNextDot()Lcom/samsung/android/visualeffect/lock/particle/Particle;
    .locals 2

    .prologue
    .line 99
    iget v0, p0, Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;->nextParticleIndex:I

    iget v1, p0, Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;->initCreatedDotAmount:I

    add-int/lit8 v1, v1, -0x1

    if-lt v0, v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput v0, p0, Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;->nextParticleIndex:I

    .line 100
    iget-object v0, p0, Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;->particleTotalList:Ljava/util/ArrayList;

    iget v1, p0, Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;->nextParticleIndex:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/visualeffect/lock/particle/Particle;

    return-object v0

    .line 99
    :cond_0
    iget v0, p0, Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;->nextParticleIndex:I

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private isAvailableRect()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 159
    iget v1, p0, Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;->drawingLeft:I

    iget v2, p0, Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;->drawingRight:I

    if-lt v1, v2, :cond_1

    .line 172
    :cond_0
    :goto_0
    return v0

    .line 161
    :cond_1
    iget v1, p0, Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;->drawingTop:I

    iget v2, p0, Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;->drawingBottom:I

    if-ge v1, v2, :cond_0

    .line 163
    iget v1, p0, Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;->drawingLeft:I

    invoke-virtual {p0}, Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;->getWidth()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 165
    iget v1, p0, Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;->drawingRight:I

    if-lez v1, :cond_0

    .line 167
    iget v1, p0, Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;->drawingTop:I

    invoke-virtual {p0}, Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;->getHeight()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 169
    iget v1, p0, Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;->drawingBottom:I

    if-lez v1, :cond_0

    .line 172
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private startDrawing()V
    .locals 4

    .prologue
    .line 89
    iget-boolean v0, p0, Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;->isDrawing:Z

    if-eqz v0, :cond_0

    .line 92
    :goto_0
    return-void

    .line 90
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;->isDrawing:Z

    .line 91
    iget-object v0, p0, Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    iget v2, p0, Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;->drawingDelayTime:I

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0
.end method

.method private stopDrawing()V
    .locals 1

    .prologue
    .line 95
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;->isDrawing:Z

    .line 96
    return-void
.end method


# virtual methods
.method public addDots(IFFI)V
    .locals 16
    .param p1, "amount"    # I
    .param p2, "x"    # F
    .param p3, "y"    # F
    .param p4, "color"    # I

    .prologue
    .line 66
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;->particleAliveList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    add-int v5, v5, p1

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;->dotMaxLimit:I

    if-le v5, v6, :cond_0

    .line 86
    :goto_0
    return-void

    .line 68
    :cond_0
    move/from16 v0, p2

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;->lastAddedX:F

    .line 69
    move/from16 v0, p3

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;->lastAddedY:F

    .line 70
    move/from16 v0, p4

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;->lastAddedColor:I

    .line 72
    invoke-static/range {p4 .. p4}, Landroid/graphics/Color;->red(I)I

    move-result v5

    invoke-static/range {p4 .. p4}, Landroid/graphics/Color;->green(I)I

    move-result v6

    invoke-static/range {p4 .. p4}, Landroid/graphics/Color;->blue(I)I

    move-result v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;->hsvOrigin:[F

    invoke-static {v5, v6, v7, v8}, Landroid/graphics/Color;->RGBToHSV(III[F)V

    .line 74
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    move/from16 v0, p1

    if-ge v3, v0, :cond_1

    .line 75
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;->hsvTemp:[F

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;->hsvOrigin:[F

    const/4 v8, 0x0

    aget v7, v7, v8

    aput v7, v5, v6

    .line 76
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;->hsvTemp:[F

    const/4 v6, 0x1

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;->hsvOrigin:[F

    const/4 v8, 0x1

    aget v7, v7, v8

    aput v7, v5, v6

    .line 77
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;->hsvTemp:[F

    const/4 v6, 0x2

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;->hsvOrigin:[F

    const/4 v8, 0x2

    aget v7, v7, v8

    aput v7, v5, v6

    .line 78
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;->hsvTemp:[F

    const/4 v6, 0x1

    aget v7, v5, v6

    float-to-double v8, v7

    const-wide/high16 v10, 0x3ff0000000000000L    # 1.0

    const-wide v12, 0x3fe6666666666666L    # 0.7

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v14

    mul-double/2addr v12, v14

    sub-double/2addr v10, v12

    mul-double/2addr v8, v10

    double-to-float v7, v8

    aput v7, v5, v6

    .line 79
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;->hsvTemp:[F

    const/4 v6, 0x2

    aget v7, v5, v6

    float-to-double v8, v7

    const/high16 v7, 0x3f800000    # 1.0f

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;->hsvTemp:[F

    const/4 v11, 0x2

    aget v10, v10, v11

    sub-float/2addr v7, v10

    float-to-double v10, v7

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v12

    mul-double/2addr v10, v12

    add-double/2addr v8, v10

    double-to-float v7, v8

    aput v7, v5, v6

    .line 80
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;->hsvTemp:[F

    invoke-static {v5}, Landroid/graphics/Color;->HSVToColor([F)I

    move-result v4

    .line 81
    .local v4, "resultColor":I
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;->getNextDot()Lcom/samsung/android/visualeffect/lock/particle/Particle;

    move-result-object v2

    .line 82
    .local v2, "dot":Lcom/samsung/android/visualeffect/lock/particle/Particle;
    move/from16 v0, p2

    move/from16 v1, p3

    invoke-virtual {v2, v0, v1, v4}, Lcom/samsung/android/visualeffect/lock/particle/Particle;->initialize(FFI)V

    .line 83
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;->particleAliveList:Ljava/util/ArrayList;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 74
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 85
    .end local v2    # "dot":Lcom/samsung/android/visualeffect/lock/particle/Particle;
    .end local v4    # "resultColor":I
    :cond_1
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;->startDrawing()V

    goto/16 :goto_0
.end method

.method public clearEffect()V
    .locals 2

    .prologue
    .line 177
    invoke-direct {p0}, Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;->stopDrawing()V

    .line 178
    invoke-virtual {p0}, Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;->invalidate()V

    .line 179
    iget-object v1, p0, Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;->particleAliveList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v0, v1, -0x1

    .local v0, "i":I
    :goto_0
    if-ltz v0, :cond_0

    .line 180
    iget-object v1, p0, Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;->particleAliveList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 179
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 182
    :cond_0
    return-void
.end method

.method public destroy()V
    .locals 0

    .prologue
    .line 185
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 7
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 114
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 116
    iget-object v6, p0, Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;->particleAliveList:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 117
    invoke-direct {p0}, Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;->stopDrawing()V

    .line 141
    :cond_0
    return-void

    .line 119
    :cond_1
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v6, p0, Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;->particleAliveList:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-ge v2, v6, :cond_0

    .line 120
    iget-object v6, p0, Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;->particleAliveList:Ljava/util/ArrayList;

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/visualeffect/lock/particle/Particle;

    .line 122
    .local v1, "dot":Lcom/samsung/android/visualeffect/lock/particle/Particle;
    if-eqz v1, :cond_6

    invoke-virtual {v1}, Lcom/samsung/android/visualeffect/lock/particle/Particle;->isAlive()Z

    move-result v6

    if-eqz v6, :cond_6

    .line 123
    invoke-virtual {v1}, Lcom/samsung/android/visualeffect/lock/particle/Particle;->move()V

    .line 124
    invoke-virtual {v1, p1}, Lcom/samsung/android/visualeffect/lock/particle/Particle;->draw(Landroid/graphics/Canvas;)V

    .line 126
    invoke-virtual {v1}, Lcom/samsung/android/visualeffect/lock/particle/Particle;->getLeft()I

    move-result v3

    .line 127
    .local v3, "left":I
    invoke-virtual {v1}, Lcom/samsung/android/visualeffect/lock/particle/Particle;->getRight()I

    move-result v4

    .line 128
    .local v4, "right":I
    invoke-virtual {v1}, Lcom/samsung/android/visualeffect/lock/particle/Particle;->getTop()I

    move-result v5

    .line 129
    .local v5, "top":I
    invoke-virtual {v1}, Lcom/samsung/android/visualeffect/lock/particle/Particle;->getBottom()I

    move-result v0

    .line 131
    .local v0, "bottom":I
    if-nez v2, :cond_2

    .end local v3    # "left":I
    :goto_1
    iput v3, p0, Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;->drawingLeft:I

    .line 132
    if-nez v2, :cond_3

    .end local v5    # "top":I
    :goto_2
    iput v5, p0, Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;->drawingTop:I

    .line 133
    if-nez v2, :cond_4

    .end local v4    # "right":I
    :goto_3
    iput v4, p0, Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;->drawingRight:I

    .line 134
    if-nez v2, :cond_5

    .end local v0    # "bottom":I
    :goto_4
    iput v0, p0, Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;->drawingBottom:I

    .line 119
    :goto_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 131
    .restart local v0    # "bottom":I
    .restart local v3    # "left":I
    .restart local v4    # "right":I
    .restart local v5    # "top":I
    :cond_2
    iget v6, p0, Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;->drawingLeft:I

    invoke-static {v6, v3}, Ljava/lang/Math;->min(II)I

    move-result v3

    goto :goto_1

    .line 132
    .end local v3    # "left":I
    :cond_3
    iget v6, p0, Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;->drawingTop:I

    invoke-static {v6, v5}, Ljava/lang/Math;->min(II)I

    move-result v5

    goto :goto_2

    .line 133
    .end local v5    # "top":I
    :cond_4
    iget v6, p0, Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;->drawingRight:I

    invoke-static {v6, v4}, Ljava/lang/Math;->max(II)I

    move-result v4

    goto :goto_3

    .line 134
    .end local v4    # "right":I
    :cond_5
    iget v6, p0, Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;->drawingBottom:I

    invoke-static {v6, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto :goto_4

    .line 136
    .end local v0    # "bottom":I
    :cond_6
    iget-object v6, p0, Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;->particleAliveList:Ljava/util/ArrayList;

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 137
    add-int/lit8 v2, v2, -0x1

    goto :goto_5
.end method

.method public pauseEffect()V
    .locals 2

    .prologue
    .line 189
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;->isPaused:Z

    .line 190
    iget-object v0, p0, Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;->TAG:Ljava/lang/String;

    const-string v1, "ParticleEffect : pauseEffect"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 191
    return-void
.end method

.method public resumeEffect()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 195
    iget-object v0, p0, Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;->TAG:Ljava/lang/String;

    const-string v1, "ParticleEffect : resumeEffect"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 196
    iget-boolean v0, p0, Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;->isPaused:Z

    if-eqz v0, :cond_0

    .line 197
    iput-boolean v4, p0, Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;->isPaused:Z

    .line 198
    iget-boolean v0, p0, Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;->isDrawing:Z

    if-eqz v0, :cond_1

    .line 199
    iget-object v0, p0, Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;->mHandler:Landroid/os/Handler;

    iget v1, p0, Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;->drawingDelayTime:I

    int-to-long v2, v1

    invoke-virtual {v0, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 204
    :cond_0
    :goto_0
    return-void

    .line 201
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;->clearEffect()V

    goto :goto_0
.end method

.method public unlockDots()V
    .locals 6

    .prologue
    .line 104
    iget v3, p0, Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;->dotMaxLimit:I

    iget-object v4, p0, Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;->particleAliveList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    sub-int v2, v3, v4

    .line 105
    .local v2, "totalAdded":I
    iget v3, p0, Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;->lastAddedX:F

    iget v4, p0, Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;->lastAddedY:F

    iget v5, p0, Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;->lastAddedColor:I

    invoke-virtual {p0, v2, v3, v4, v5}, Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;->addDots(IFFI)V

    .line 107
    iget-object v3, p0, Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;->particleAliveList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/visualeffect/lock/particle/Particle;

    .line 108
    .local v1, "particle":Lcom/samsung/android/visualeffect/lock/particle/Particle;
    iget v3, p0, Lcom/sec/android/visualeffect/particlespace/particle/ParticleEffect;->dotUnlockSpeed:I

    int-to-float v3, v3

    invoke-virtual {v1, v3}, Lcom/samsung/android/visualeffect/lock/particle/Particle;->unlock(F)V

    goto :goto_0

    .line 110
    .end local v1    # "particle":Lcom/samsung/android/visualeffect/lock/particle/Particle;
    :cond_0
    return-void
.end method
