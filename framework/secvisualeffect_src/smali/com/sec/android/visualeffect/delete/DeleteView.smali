.class public Lcom/sec/android/visualeffect/delete/DeleteView;
.super Landroid/view/TextureView;
.source "DeleteView.java"

# interfaces
.implements Landroid/view/TextureView$SurfaceTextureListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/visualeffect/delete/DeleteView$CrumplingEventListener;
    }
.end annotation


# static fields
.field private static TAG:Ljava/lang/String;

.field static mListener:Lcom/sec/android/visualeffect/delete/DeleteView$CrumplingEventListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/visualeffect/delete/DeleteView;->mListener:Lcom/sec/android/visualeffect/delete/DeleteView$CrumplingEventListener;

    .line 15
    const-string v0, "DeleteView"

    sput-object v0, Lcom/sec/android/visualeffect/delete/DeleteView;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 19
    invoke-direct {p0, p1}, Landroid/view/TextureView;-><init>(Landroid/content/Context;)V

    .line 20
    invoke-direct {p0}, Lcom/sec/android/visualeffect/delete/DeleteView;->init()V

    .line 21
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 24
    invoke-direct {p0, p1, p2}, Landroid/view/TextureView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 26
    invoke-direct {p0}, Lcom/sec/android/visualeffect/delete/DeleteView;->init()V

    .line 27
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 30
    invoke-direct {p0, p1, p2, p3}, Landroid/view/TextureView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 32
    invoke-direct {p0}, Lcom/sec/android/visualeffect/delete/DeleteView;->init()V

    .line 33
    return-void
.end method

.method public static callbackFromNative(I)V
    .locals 2
    .param p0, "v"    # I

    .prologue
    .line 145
    sget-object v0, Lcom/sec/android/visualeffect/delete/DeleteView;->mListener:Lcom/sec/android/visualeffect/delete/DeleteView$CrumplingEventListener;

    if-nez v0, :cond_0

    .line 147
    sget-object v0, Lcom/sec/android/visualeffect/delete/DeleteView;->TAG:Ljava/lang/String;

    const-string v1, "CrumplingEventListener not registered"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 151
    :cond_0
    return-void
.end method

.method private init()V
    .locals 2

    .prologue
    .line 36
    sget-object v0, Lcom/sec/android/visualeffect/delete/DeleteView;->TAG:Ljava/lang/String;

    const-string v1, "version 2014.1.08"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 46
    invoke-virtual {p0, p0}, Lcom/sec/android/visualeffect/delete/DeleteView;->setSurfaceTextureListener(Landroid/view/TextureView$SurfaceTextureListener;)V

    .line 53
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/visualeffect/delete/DeleteView;->setOpaque(Z)V

    .line 54
    return-void
.end method


# virtual methods
.method public initModel(IIFFLandroid/graphics/Bitmap;IIFFZI)V
    .locals 1
    .param p1, "startPosX"    # I
    .param p2, "startPosY"    # I
    .param p3, "pageWidth"    # F
    .param p4, "pageHeight"    # F
    .param p5, "front"    # Landroid/graphics/Bitmap;
    .param p6, "docX"    # I
    .param p7, "docY"    # I
    .param p8, "docWidth"    # F
    .param p9, "docHeight"    # F
    .param p10, "isGesture"    # Z
    .param p11, "mode"    # I

    .prologue
    .line 134
    sget-object v0, Lcom/sec/android/visualeffect/delete/DeleteView;->mListener:Lcom/sec/android/visualeffect/delete/DeleteView$CrumplingEventListener;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/android/visualeffect/delete/DeleteView;->mListener:Lcom/sec/android/visualeffect/delete/DeleteView$CrumplingEventListener;

    invoke-interface {v0}, Lcom/sec/android/visualeffect/delete/DeleteView$CrumplingEventListener;->onViewReady()V

    .line 135
    :cond_0
    return-void
.end method

.method public initView(FFFLandroid/graphics/Bitmap;)V
    .locals 2
    .param p1, "bgColorR"    # F
    .param p2, "bgColorG"    # F
    .param p3, "bgColorB"    # F
    .param p4, "shadow"    # Landroid/graphics/Bitmap;

    .prologue
    .line 123
    if-eqz p4, :cond_0

    invoke-virtual {p4}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 125
    :cond_0
    sget-object v0, Lcom/sec/android/visualeffect/delete/DeleteView;->TAG:Ljava/lang/String;

    const-string v1, "shadow recycled"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 129
    :cond_1
    return-void
.end method

.method public initView(Landroid/graphics/Bitmap;)V
    .locals 2
    .param p1, "shadow"    # Landroid/graphics/Bitmap;

    .prologue
    const v1, 0x3eb4b4b5

    .line 117
    const v0, 0x3eb2b2b3

    invoke-virtual {p0, v1, v0, v1, p1}, Lcom/sec/android/visualeffect/delete/DeleteView;->initView(FFFLandroid/graphics/Bitmap;)V

    .line 118
    return-void
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 214
    return-void
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 208
    return-void
.end method

.method public onSurfaceTextureAvailable(Landroid/graphics/SurfaceTexture;II)V
    .locals 3
    .param p1, "surface"    # Landroid/graphics/SurfaceTexture;
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    .line 60
    sget-object v0, Lcom/sec/android/visualeffect/delete/DeleteView;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onSurfaceTextureAvailable "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 62
    sget-object v0, Lcom/sec/android/visualeffect/delete/DeleteView;->mListener:Lcom/sec/android/visualeffect/delete/DeleteView$CrumplingEventListener;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/android/visualeffect/delete/DeleteView;->mListener:Lcom/sec/android/visualeffect/delete/DeleteView$CrumplingEventListener;

    invoke-interface {v0}, Lcom/sec/android/visualeffect/delete/DeleteView$CrumplingEventListener;->onViewCreated()V

    .line 80
    :cond_0
    return-void
.end method

.method public onSurfaceTextureDestroyed(Landroid/graphics/SurfaceTexture;)Z
    .locals 3
    .param p1, "surface"    # Landroid/graphics/SurfaceTexture;

    .prologue
    .line 90
    sget-object v0, Lcom/sec/android/visualeffect/delete/DeleteView;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onSurfaceTextureDestroyed start "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 92
    const/4 v0, 0x1

    return v0
.end method

.method public onSurfaceTextureSizeChanged(Landroid/graphics/SurfaceTexture;II)V
    .locals 3
    .param p1, "surface"    # Landroid/graphics/SurfaceTexture;
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    .line 84
    sget-object v0, Lcom/sec/android/visualeffect/delete/DeleteView;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onSurfaceTextureSizeChanged "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 86
    return-void
.end method

.method public onSurfaceTextureUpdated(Landroid/graphics/SurfaceTexture;)V
    .locals 0
    .param p1, "surface"    # Landroid/graphics/SurfaceTexture;

    .prologue
    .line 97
    return-void
.end method

.method public setCrumplingEventListener(Lcom/sec/android/visualeffect/delete/DeleteView$CrumplingEventListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/visualeffect/delete/DeleteView$CrumplingEventListener;

    .prologue
    .line 193
    sput-object p1, Lcom/sec/android/visualeffect/delete/DeleteView;->mListener:Lcom/sec/android/visualeffect/delete/DeleteView$CrumplingEventListener;

    .line 194
    return-void
.end method

.method public setStartState()V
    .locals 0

    .prologue
    .line 218
    invoke-virtual {p0}, Lcom/sec/android/visualeffect/delete/DeleteView;->onResume()V

    .line 219
    return-void
.end method

.method public setStopState()V
    .locals 0

    .prologue
    .line 223
    invoke-virtual {p0}, Lcom/sec/android/visualeffect/delete/DeleteView;->onPause()V

    .line 224
    return-void
.end method

.method public startCrumplingAnimation(Z)V
    .locals 1
    .param p1, "isGesture"    # Z

    .prologue
    .line 139
    sget-object v0, Lcom/sec/android/visualeffect/delete/DeleteView;->mListener:Lcom/sec/android/visualeffect/delete/DeleteView$CrumplingEventListener;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/android/visualeffect/delete/DeleteView;->mListener:Lcom/sec/android/visualeffect/delete/DeleteView$CrumplingEventListener;

    invoke-interface {v0}, Lcom/sec/android/visualeffect/delete/DeleteView$CrumplingEventListener;->onAnimationEnd()V

    .line 140
    :cond_0
    return-void
.end method
