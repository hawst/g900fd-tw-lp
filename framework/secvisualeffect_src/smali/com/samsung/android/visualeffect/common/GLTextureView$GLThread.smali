.class Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;
.super Ljava/lang/Thread;
.source "GLTextureView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/visualeffect/common/GLTextureView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "GLThread"
.end annotation


# instance fields
.field protected currFPSTime:J

.field protected lastUpdateTime:J

.field private mEglHelper:Lcom/samsung/android/visualeffect/common/GLTextureView$EglHelper;

.field private mEventQueue:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private mExited:Z

.field private mFinishedCreatingEglSurface:Z

.field private mGLTextureViewWeakRef:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/samsung/android/visualeffect/common/GLTextureView;",
            ">;"
        }
    .end annotation
.end field

.field private mHasSurface:Z

.field private mHaveEglContext:Z

.field private mHaveEglSurface:Z

.field private mHeight:I

.field private mPaused:Z

.field private mRenderComplete:Z

.field private mRenderMode:I

.field private mRequestPaused:Z

.field private mRequestRender:Z

.field private mShouldExit:Z

.field private mShouldReleaseEglContext:Z

.field private mSizeChanged:Z

.field private mSurfaceIsBad:Z

.field private mWaitingForSurface:Z

.field private mWidth:I

.field protected prevFPSTime:J


# direct methods
.method constructor <init>(Ljava/lang/ref/WeakReference;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/samsung/android/visualeffect/common/GLTextureView;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "glSurfaceViewWeakRef":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lcom/samsung/android/visualeffect/common/GLTextureView;>;"
    const/4 v4, 0x0

    const-wide/16 v2, 0x0

    const/4 v1, 0x1

    .line 1078
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 1073
    iput-wide v2, p0, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->prevFPSTime:J

    .line 1074
    iput-wide v2, p0, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->currFPSTime:J

    .line 1075
    iput-wide v2, p0, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->lastUpdateTime:J

    .line 1673
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->mEventQueue:Ljava/util/ArrayList;

    .line 1674
    iput-boolean v1, p0, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->mSizeChanged:Z

    .line 1079
    iput v4, p0, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->mWidth:I

    .line 1080
    iput v4, p0, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->mHeight:I

    .line 1081
    iput-boolean v1, p0, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->mRequestRender:Z

    .line 1082
    iput v1, p0, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->mRenderMode:I

    .line 1083
    iput-object p1, p0, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->mGLTextureViewWeakRef:Ljava/lang/ref/WeakReference;

    .line 1084
    return-void
.end method

.method static synthetic access$902(Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;
    .param p1, "x1"    # Z

    .prologue
    .line 1071
    iput-boolean p1, p0, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->mExited:Z

    return p1
.end method

.method private guardedRun()V
    .locals 28
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 1126
    new-instance v24, Lcom/samsung/android/visualeffect/common/GLTextureView$EglHelper;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->mGLTextureViewWeakRef:Ljava/lang/ref/WeakReference;

    move-object/from16 v25, v0

    invoke-direct/range {v24 .. v25}, Lcom/samsung/android/visualeffect/common/GLTextureView$EglHelper;-><init>(Ljava/lang/ref/WeakReference;)V

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->mEglHelper:Lcom/samsung/android/visualeffect/common/GLTextureView$EglHelper;

    .line 1127
    const/16 v24, 0x0

    move/from16 v0, v24

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->mHaveEglContext:Z

    .line 1128
    const/16 v24, 0x0

    move/from16 v0, v24

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->mHaveEglSurface:Z

    .line 1130
    const/4 v12, 0x0

    .line 1131
    .local v12, "gl":Ljavax/microedition/khronos/opengles/GL10;
    const/4 v5, 0x0

    .line 1132
    .local v5, "createEglContext":Z
    const/4 v6, 0x0

    .line 1133
    .local v6, "createEglSurface":Z
    const/4 v7, 0x0

    .line 1134
    .local v7, "createGlInterface":Z
    const/4 v15, 0x0

    .line 1135
    .local v15, "lostEglContext":Z
    const/16 v18, 0x0

    .line 1136
    .local v18, "sizeChanged":Z
    const/16 v23, 0x0

    .line 1137
    .local v23, "wantRenderNotification":Z
    const/4 v10, 0x0

    .line 1138
    .local v10, "doRenderNotification":Z
    const/4 v4, 0x0

    .line 1139
    .local v4, "askedToReleaseEglContext":Z
    const/16 v22, 0x0

    .line 1140
    .local v22, "w":I
    const/4 v13, 0x0

    .line 1141
    .local v13, "h":I
    const/4 v11, 0x0

    .line 1144
    .local v11, "event":Ljava/lang/Runnable;
    :cond_0
    :goto_0
    :try_start_0
    # getter for: Lcom/samsung/android/visualeffect/common/GLTextureView;->sGLThreadManager:Lcom/samsung/android/visualeffect/common/GLTextureView$GLThreadManager;
    invoke-static {}, Lcom/samsung/android/visualeffect/common/GLTextureView;->access$600()Lcom/samsung/android/visualeffect/common/GLTextureView$GLThreadManager;

    move-result-object v25

    monitor-enter v25
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 1146
    :goto_1
    :try_start_1
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->mShouldExit:Z

    move/from16 v24, v0

    if-eqz v24, :cond_1

    .line 1147
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->mGLTextureViewWeakRef:Ljava/lang/ref/WeakReference;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Lcom/samsung/android/visualeffect/common/GLTextureView;

    # getter for: Lcom/samsung/android/visualeffect/common/GLTextureView;->mRenderer:Lcom/samsung/android/visualeffect/common/GLTextureView$Renderer;
    invoke-static/range {v24 .. v24}, Lcom/samsung/android/visualeffect/common/GLTextureView;->access$700(Lcom/samsung/android/visualeffect/common/GLTextureView;)Lcom/samsung/android/visualeffect/common/GLTextureView$Renderer;

    move-result-object v24

    invoke-interface/range {v24 .. v24}, Lcom/samsung/android/visualeffect/common/GLTextureView$Renderer;->onDestroy()V

    .line 1149
    monitor-exit v25
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1472
    # getter for: Lcom/samsung/android/visualeffect/common/GLTextureView;->sGLThreadManager:Lcom/samsung/android/visualeffect/common/GLTextureView$GLThreadManager;
    invoke-static {}, Lcom/samsung/android/visualeffect/common/GLTextureView;->access$600()Lcom/samsung/android/visualeffect/common/GLTextureView$GLThreadManager;

    move-result-object v25

    monitor-enter v25

    .line 1473
    :try_start_2
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->stopEglSurfaceLocked()V

    .line 1474
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->stopEglContextLocked()V

    .line 1475
    monitor-exit v25

    return-void

    :catchall_0
    move-exception v24

    monitor-exit v25
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v24

    .line 1152
    :cond_1
    :try_start_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->mEventQueue:Ljava/util/ArrayList;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v24

    if-nez v24, :cond_2

    .line 1153
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->mEventQueue:Ljava/util/ArrayList;

    move-object/from16 v24, v0

    const/16 v26, 0x0

    move-object/from16 v0, v24

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v24

    move-object/from16 v0, v24

    check-cast v0, Ljava/lang/Runnable;

    move-object v11, v0

    .line 1343
    :goto_2
    monitor-exit v25
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 1345
    if-eqz v11, :cond_14

    .line 1346
    :try_start_4
    invoke-interface {v11}, Ljava/lang/Runnable;->run()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 1347
    const/4 v11, 0x0

    .line 1348
    goto :goto_0

    .line 1158
    :cond_2
    const/16 v16, 0x0

    .line 1159
    .local v16, "pausing":Z
    :try_start_5
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->mPaused:Z

    move/from16 v24, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->mRequestPaused:Z

    move/from16 v26, v0

    move/from16 v0, v24

    move/from16 v1, v26

    if-eq v0, v1, :cond_3

    .line 1160
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->mRequestPaused:Z

    move/from16 v16, v0

    .line 1161
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->mRequestPaused:Z

    move/from16 v24, v0

    move/from16 v0, v24

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->mPaused:Z

    .line 1162
    # getter for: Lcom/samsung/android/visualeffect/common/GLTextureView;->sGLThreadManager:Lcom/samsung/android/visualeffect/common/GLTextureView$GLThreadManager;
    invoke-static {}, Lcom/samsung/android/visualeffect/common/GLTextureView;->access$600()Lcom/samsung/android/visualeffect/common/GLTextureView$GLThreadManager;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/Object;->notifyAll()V

    .line 1170
    :cond_3
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->mShouldReleaseEglContext:Z

    move/from16 v24, v0

    if-eqz v24, :cond_4

    .line 1176
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->stopEglSurfaceLocked()V

    .line 1177
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->stopEglContextLocked()V

    .line 1178
    const/16 v24, 0x0

    move/from16 v0, v24

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->mShouldReleaseEglContext:Z

    .line 1179
    const/4 v4, 0x1

    .line 1183
    :cond_4
    if-eqz v15, :cond_5

    .line 1184
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->stopEglSurfaceLocked()V

    .line 1185
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->stopEglContextLocked()V

    .line 1186
    const/4 v15, 0x0

    .line 1190
    :cond_5
    if-eqz v16, :cond_6

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->mHaveEglSurface:Z

    move/from16 v24, v0

    if-eqz v24, :cond_6

    .line 1196
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->stopEglSurfaceLocked()V

    .line 1200
    :cond_6
    if-eqz v16, :cond_8

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->mHaveEglContext:Z

    move/from16 v24, v0

    if-eqz v24, :cond_8

    .line 1201
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->mGLTextureViewWeakRef:Ljava/lang/ref/WeakReference;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Lcom/samsung/android/visualeffect/common/GLTextureView;

    .line 1203
    .local v21, "view":Lcom/samsung/android/visualeffect/common/GLTextureView;
    if-nez v21, :cond_11

    const/16 v17, 0x0

    .line 1205
    .local v17, "preserveEglContextOnPause":Z
    :goto_3
    if-eqz v17, :cond_7

    # getter for: Lcom/samsung/android/visualeffect/common/GLTextureView;->sGLThreadManager:Lcom/samsung/android/visualeffect/common/GLTextureView$GLThreadManager;
    invoke-static {}, Lcom/samsung/android/visualeffect/common/GLTextureView;->access$600()Lcom/samsung/android/visualeffect/common/GLTextureView$GLThreadManager;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThreadManager;->shouldReleaseEGLContextWhenPausing()Z

    move-result v24

    if-eqz v24, :cond_8

    .line 1208
    :cond_7
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->stopEglContextLocked()V

    .line 1218
    .end local v17    # "preserveEglContextOnPause":Z
    .end local v21    # "view":Lcom/samsung/android/visualeffect/common/GLTextureView;
    :cond_8
    if-eqz v16, :cond_9

    .line 1219
    # getter for: Lcom/samsung/android/visualeffect/common/GLTextureView;->sGLThreadManager:Lcom/samsung/android/visualeffect/common/GLTextureView$GLThreadManager;
    invoke-static {}, Lcom/samsung/android/visualeffect/common/GLTextureView;->access$600()Lcom/samsung/android/visualeffect/common/GLTextureView$GLThreadManager;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThreadManager;->shouldTerminateEGLWhenPausing()Z

    move-result v24

    if-eqz v24, :cond_9

    .line 1221
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->mEglHelper:Lcom/samsung/android/visualeffect/common/GLTextureView$EglHelper;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lcom/samsung/android/visualeffect/common/GLTextureView$EglHelper;->finish()V

    .line 1231
    :cond_9
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->mHasSurface:Z

    move/from16 v24, v0

    if-nez v24, :cond_b

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->mWaitingForSurface:Z

    move/from16 v24, v0

    if-nez v24, :cond_b

    .line 1237
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->mHaveEglSurface:Z

    move/from16 v24, v0

    if-eqz v24, :cond_a

    .line 1238
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->stopEglSurfaceLocked()V

    .line 1240
    :cond_a
    const/16 v24, 0x1

    move/from16 v0, v24

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->mWaitingForSurface:Z

    .line 1241
    const/16 v24, 0x0

    move/from16 v0, v24

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->mSurfaceIsBad:Z

    .line 1242
    # getter for: Lcom/samsung/android/visualeffect/common/GLTextureView;->sGLThreadManager:Lcom/samsung/android/visualeffect/common/GLTextureView$GLThreadManager;
    invoke-static {}, Lcom/samsung/android/visualeffect/common/GLTextureView;->access$600()Lcom/samsung/android/visualeffect/common/GLTextureView$GLThreadManager;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/Object;->notifyAll()V

    .line 1246
    :cond_b
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->mHasSurface:Z

    move/from16 v24, v0

    if-eqz v24, :cond_c

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->mWaitingForSurface:Z

    move/from16 v24, v0

    if-eqz v24, :cond_c

    .line 1252
    const/16 v24, 0x0

    move/from16 v0, v24

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->mWaitingForSurface:Z

    .line 1253
    # getter for: Lcom/samsung/android/visualeffect/common/GLTextureView;->sGLThreadManager:Lcom/samsung/android/visualeffect/common/GLTextureView$GLThreadManager;
    invoke-static {}, Lcom/samsung/android/visualeffect/common/GLTextureView;->access$600()Lcom/samsung/android/visualeffect/common/GLTextureView$GLThreadManager;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/Object;->notifyAll()V

    .line 1256
    :cond_c
    if-eqz v10, :cond_d

    .line 1262
    const/16 v23, 0x0

    .line 1263
    const/4 v10, 0x0

    .line 1264
    const/16 v24, 0x1

    move/from16 v0, v24

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->mRenderComplete:Z

    .line 1265
    # getter for: Lcom/samsung/android/visualeffect/common/GLTextureView;->sGLThreadManager:Lcom/samsung/android/visualeffect/common/GLTextureView$GLThreadManager;
    invoke-static {}, Lcom/samsung/android/visualeffect/common/GLTextureView;->access$600()Lcom/samsung/android/visualeffect/common/GLTextureView$GLThreadManager;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/Object;->notifyAll()V

    .line 1269
    :cond_d
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->readyToDraw()Z

    move-result v24

    if-eqz v24, :cond_13

    .line 1273
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->mHaveEglContext:Z

    move/from16 v24, v0

    if-nez v24, :cond_e

    .line 1274
    if-eqz v4, :cond_12

    .line 1275
    const/4 v4, 0x0

    .line 1292
    :cond_e
    :goto_4
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->mHaveEglContext:Z

    move/from16 v24, v0

    if-eqz v24, :cond_f

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->mHaveEglSurface:Z

    move/from16 v24, v0

    if-nez v24, :cond_f

    .line 1293
    const/16 v24, 0x1

    move/from16 v0, v24

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->mHaveEglSurface:Z

    .line 1294
    const/4 v6, 0x1

    .line 1295
    const/4 v7, 0x1

    .line 1296
    const/16 v18, 0x1

    .line 1299
    :cond_f
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->mHaveEglSurface:Z

    move/from16 v24, v0

    if-eqz v24, :cond_13

    .line 1300
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->mSizeChanged:Z

    move/from16 v24, v0

    if-eqz v24, :cond_10

    .line 1301
    const/16 v18, 0x1

    .line 1302
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->mWidth:I

    move/from16 v22, v0

    .line 1303
    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->mHeight:I

    .line 1304
    const/16 v23, 0x1

    .line 1312
    const/4 v6, 0x1

    .line 1314
    const/16 v24, 0x0

    move/from16 v0, v24

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->mSizeChanged:Z

    .line 1316
    :cond_10
    const/16 v24, 0x0

    move/from16 v0, v24

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->mRequestRender:Z

    .line 1317
    # getter for: Lcom/samsung/android/visualeffect/common/GLTextureView;->sGLThreadManager:Lcom/samsung/android/visualeffect/common/GLTextureView$GLThreadManager;
    invoke-static {}, Lcom/samsung/android/visualeffect/common/GLTextureView;->access$600()Lcom/samsung/android/visualeffect/common/GLTextureView$GLThreadManager;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/Object;->notifyAll()V

    goto/16 :goto_2

    .line 1343
    .end local v16    # "pausing":Z
    :catchall_1
    move-exception v24

    monitor-exit v25
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :try_start_6
    throw v24
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .line 1472
    :catchall_2
    move-exception v24

    # getter for: Lcom/samsung/android/visualeffect/common/GLTextureView;->sGLThreadManager:Lcom/samsung/android/visualeffect/common/GLTextureView$GLThreadManager;
    invoke-static {}, Lcom/samsung/android/visualeffect/common/GLTextureView;->access$600()Lcom/samsung/android/visualeffect/common/GLTextureView$GLThreadManager;

    move-result-object v25

    monitor-enter v25

    .line 1473
    :try_start_7
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->stopEglSurfaceLocked()V

    .line 1474
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->stopEglContextLocked()V

    .line 1475
    monitor-exit v25
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_6

    throw v24

    .line 1203
    .restart local v16    # "pausing":Z
    .restart local v21    # "view":Lcom/samsung/android/visualeffect/common/GLTextureView;
    :cond_11
    :try_start_8
    # getter for: Lcom/samsung/android/visualeffect/common/GLTextureView;->mPreserveEGLContextOnPause:Z
    invoke-static/range {v21 .. v21}, Lcom/samsung/android/visualeffect/common/GLTextureView;->access$800(Lcom/samsung/android/visualeffect/common/GLTextureView;)Z

    move-result v17

    goto/16 :goto_3

    .line 1276
    .end local v21    # "view":Lcom/samsung/android/visualeffect/common/GLTextureView;
    :cond_12
    # getter for: Lcom/samsung/android/visualeffect/common/GLTextureView;->sGLThreadManager:Lcom/samsung/android/visualeffect/common/GLTextureView$GLThreadManager;
    invoke-static {}, Lcom/samsung/android/visualeffect/common/GLTextureView;->access$600()Lcom/samsung/android/visualeffect/common/GLTextureView$GLThreadManager;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThreadManager;->tryAcquireEglContextLocked(Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;)Z
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    move-result v24

    if-eqz v24, :cond_e

    .line 1279
    :try_start_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->mEglHelper:Lcom/samsung/android/visualeffect/common/GLTextureView$EglHelper;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lcom/samsung/android/visualeffect/common/GLTextureView$EglHelper;->start()V
    :try_end_9
    .catch Ljava/lang/RuntimeException; {:try_start_9 .. :try_end_9} :catch_0
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    .line 1285
    const/16 v24, 0x1

    :try_start_a
    move/from16 v0, v24

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->mHaveEglContext:Z

    .line 1286
    const/4 v5, 0x1

    .line 1288
    # getter for: Lcom/samsung/android/visualeffect/common/GLTextureView;->sGLThreadManager:Lcom/samsung/android/visualeffect/common/GLTextureView$GLThreadManager;
    invoke-static {}, Lcom/samsung/android/visualeffect/common/GLTextureView;->access$600()Lcom/samsung/android/visualeffect/common/GLTextureView$GLThreadManager;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/Object;->notifyAll()V

    goto/16 :goto_4

    .line 1280
    :catch_0
    move-exception v20

    .line 1281
    .local v20, "t":Ljava/lang/RuntimeException;
    # getter for: Lcom/samsung/android/visualeffect/common/GLTextureView;->sGLThreadManager:Lcom/samsung/android/visualeffect/common/GLTextureView$GLThreadManager;
    invoke-static {}, Lcom/samsung/android/visualeffect/common/GLTextureView;->access$600()Lcom/samsung/android/visualeffect/common/GLTextureView$GLThreadManager;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThreadManager;->releaseEglContextLocked(Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;)V

    .line 1283
    throw v20

    .line 1341
    .end local v20    # "t":Ljava/lang/RuntimeException;
    :cond_13
    # getter for: Lcom/samsung/android/visualeffect/common/GLTextureView;->sGLThreadManager:Lcom/samsung/android/visualeffect/common/GLTextureView$GLThreadManager;
    invoke-static {}, Lcom/samsung/android/visualeffect/common/GLTextureView;->access$600()Lcom/samsung/android/visualeffect/common/GLTextureView$GLThreadManager;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/Object;->wait()V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    goto/16 :goto_1

    .line 1351
    .end local v16    # "pausing":Z
    :cond_14
    if-eqz v6, :cond_15

    .line 1355
    :try_start_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->mEglHelper:Lcom/samsung/android/visualeffect/common/GLTextureView$EglHelper;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lcom/samsung/android/visualeffect/common/GLTextureView$EglHelper;->createSurface()Z

    move-result v24

    if-eqz v24, :cond_1e

    .line 1356
    # getter for: Lcom/samsung/android/visualeffect/common/GLTextureView;->sGLThreadManager:Lcom/samsung/android/visualeffect/common/GLTextureView$GLThreadManager;
    invoke-static {}, Lcom/samsung/android/visualeffect/common/GLTextureView;->access$600()Lcom/samsung/android/visualeffect/common/GLTextureView$GLThreadManager;

    move-result-object v25

    monitor-enter v25
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_2

    .line 1357
    const/16 v24, 0x1

    :try_start_c
    move/from16 v0, v24

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->mFinishedCreatingEglSurface:Z

    .line 1358
    # getter for: Lcom/samsung/android/visualeffect/common/GLTextureView;->sGLThreadManager:Lcom/samsung/android/visualeffect/common/GLTextureView$GLThreadManager;
    invoke-static {}, Lcom/samsung/android/visualeffect/common/GLTextureView;->access$600()Lcom/samsung/android/visualeffect/common/GLTextureView$GLThreadManager;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/Object;->notifyAll()V

    .line 1359
    monitor-exit v25
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_3

    .line 1368
    const/4 v6, 0x0

    .line 1371
    :cond_15
    if-eqz v7, :cond_16

    .line 1372
    :try_start_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->mEglHelper:Lcom/samsung/android/visualeffect/common/GLTextureView$EglHelper;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lcom/samsung/android/visualeffect/common/GLTextureView$EglHelper;->createGL()Ljavax/microedition/khronos/opengles/GL;

    move-result-object v24

    move-object/from16 v0, v24

    check-cast v0, Ljavax/microedition/khronos/opengles/GL10;

    move-object v12, v0

    .line 1374
    # getter for: Lcom/samsung/android/visualeffect/common/GLTextureView;->sGLThreadManager:Lcom/samsung/android/visualeffect/common/GLTextureView$GLThreadManager;
    invoke-static {}, Lcom/samsung/android/visualeffect/common/GLTextureView;->access$600()Lcom/samsung/android/visualeffect/common/GLTextureView$GLThreadManager;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v0, v12}, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThreadManager;->checkGLDriver(Ljavax/microedition/khronos/opengles/GL10;)V

    .line 1375
    const/4 v7, 0x0

    .line 1378
    :cond_16
    if-eqz v5, :cond_18

    .line 1382
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->mGLTextureViewWeakRef:Ljava/lang/ref/WeakReference;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Lcom/samsung/android/visualeffect/common/GLTextureView;

    .line 1383
    .restart local v21    # "view":Lcom/samsung/android/visualeffect/common/GLTextureView;
    if-eqz v21, :cond_17

    .line 1384
    # getter for: Lcom/samsung/android/visualeffect/common/GLTextureView;->mRenderer:Lcom/samsung/android/visualeffect/common/GLTextureView$Renderer;
    invoke-static/range {v21 .. v21}, Lcom/samsung/android/visualeffect/common/GLTextureView;->access$700(Lcom/samsung/android/visualeffect/common/GLTextureView;)Lcom/samsung/android/visualeffect/common/GLTextureView$Renderer;

    move-result-object v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->mEglHelper:Lcom/samsung/android/visualeffect/common/GLTextureView$EglHelper;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/samsung/android/visualeffect/common/GLTextureView$EglHelper;->mEglConfig:Ljavax/microedition/khronos/egl/EGLConfig;

    move-object/from16 v25, v0

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    invoke-interface {v0, v12, v1}, Lcom/samsung/android/visualeffect/common/GLTextureView$Renderer;->onSurfaceCreated(Ljavax/microedition/khronos/opengles/GL10;Ljavax/microedition/khronos/egl/EGLConfig;)V

    .line 1387
    :cond_17
    const/4 v5, 0x0

    .line 1390
    .end local v21    # "view":Lcom/samsung/android/visualeffect/common/GLTextureView;
    :cond_18
    if-eqz v18, :cond_1a

    .line 1395
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->mGLTextureViewWeakRef:Ljava/lang/ref/WeakReference;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Lcom/samsung/android/visualeffect/common/GLTextureView;

    .line 1396
    .restart local v21    # "view":Lcom/samsung/android/visualeffect/common/GLTextureView;
    if-eqz v21, :cond_19

    .line 1397
    # getter for: Lcom/samsung/android/visualeffect/common/GLTextureView;->mRenderer:Lcom/samsung/android/visualeffect/common/GLTextureView$Renderer;
    invoke-static/range {v21 .. v21}, Lcom/samsung/android/visualeffect/common/GLTextureView;->access$700(Lcom/samsung/android/visualeffect/common/GLTextureView;)Lcom/samsung/android/visualeffect/common/GLTextureView$Renderer;

    move-result-object v24

    move-object/from16 v0, v24

    move/from16 v1, v22

    invoke-interface {v0, v12, v1, v13}, Lcom/samsung/android/visualeffect/common/GLTextureView$Renderer;->onSurfaceChanged(Ljavax/microedition/khronos/opengles/GL10;II)V

    .line 1399
    :cond_19
    const/16 v18, 0x0

    .line 1402
    .end local v21    # "view":Lcom/samsung/android/visualeffect/common/GLTextureView;
    :cond_1a
    const/4 v14, 0x1

    .line 1408
    .local v14, "isSwaped":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->mGLTextureViewWeakRef:Ljava/lang/ref/WeakReference;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Lcom/samsung/android/visualeffect/common/GLTextureView;

    .line 1409
    .restart local v21    # "view":Lcom/samsung/android/visualeffect/common/GLTextureView;
    if-eqz v21, :cond_1c

    .line 1411
    invoke-virtual/range {v21 .. v21}, Lcom/samsung/android/visualeffect/common/GLTextureView;->getSurfaceTexture()Landroid/graphics/SurfaceTexture;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Landroid/graphics/SurfaceTexture;->getTimestamp()J

    move-result-wide v24

    move-wide/from16 v0, v24

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->currFPSTime:J

    .line 1412
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->currFPSTime:J

    move-wide/from16 v24, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->prevFPSTime:J

    move-wide/from16 v26, v0

    sub-long v8, v24, v26

    .line 1414
    .local v8, "diffFPSTime":J
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->currFPSTime:J

    move-wide/from16 v24, v0

    const-wide/16 v26, 0x0

    cmp-long v24, v24, v26

    if-eqz v24, :cond_1b

    const-wide/16 v24, 0x0

    cmp-long v24, v8, v24

    if-eqz v24, :cond_1f

    .line 1415
    :cond_1b
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v24

    move-wide/from16 v0, v24

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->lastUpdateTime:J

    .line 1416
    const/4 v14, 0x1

    .line 1417
    # getter for: Lcom/samsung/android/visualeffect/common/GLTextureView;->mRenderer:Lcom/samsung/android/visualeffect/common/GLTextureView$Renderer;
    invoke-static/range {v21 .. v21}, Lcom/samsung/android/visualeffect/common/GLTextureView;->access$700(Lcom/samsung/android/visualeffect/common/GLTextureView;)Lcom/samsung/android/visualeffect/common/GLTextureView$Renderer;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-interface {v0, v12}, Lcom/samsung/android/visualeffect/common/GLTextureView$Renderer;->onDrawFrame(Ljavax/microedition/khronos/opengles/GL10;)V

    .line 1418
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->currFPSTime:J

    move-wide/from16 v24, v0

    move-wide/from16 v0, v24

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->prevFPSTime:J

    .line 1431
    .end local v8    # "diffFPSTime":J
    :cond_1c
    :goto_5
    if-eqz v14, :cond_1d

    .line 1432
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->mEglHelper:Lcom/samsung/android/visualeffect/common/GLTextureView$EglHelper;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lcom/samsung/android/visualeffect/common/GLTextureView$EglHelper;->swap()I

    move-result v19

    .line 1433
    .local v19, "swapError":I
    sparse-switch v19, :sswitch_data_0

    .line 1452
    const-string v24, "GLThread"

    const-string v25, "eglSwapBuffers"

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    move/from16 v2, v19

    invoke-static {v0, v1, v2}, Lcom/samsung/android/visualeffect/common/GLTextureView$EglHelper;->logEglErrorAsWarning(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1455
    # getter for: Lcom/samsung/android/visualeffect/common/GLTextureView;->sGLThreadManager:Lcom/samsung/android/visualeffect/common/GLTextureView$GLThreadManager;
    invoke-static {}, Lcom/samsung/android/visualeffect/common/GLTextureView;->access$600()Lcom/samsung/android/visualeffect/common/GLTextureView$GLThreadManager;

    move-result-object v25

    monitor-enter v25
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_2

    .line 1456
    const/16 v24, 0x1

    :try_start_e
    move/from16 v0, v24

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->mSurfaceIsBad:Z

    .line 1457
    # getter for: Lcom/samsung/android/visualeffect/common/GLTextureView;->sGLThreadManager:Lcom/samsung/android/visualeffect/common/GLTextureView$GLThreadManager;
    invoke-static {}, Lcom/samsung/android/visualeffect/common/GLTextureView;->access$600()Lcom/samsung/android/visualeffect/common/GLTextureView$GLThreadManager;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/Object;->notifyAll()V

    .line 1458
    monitor-exit v25
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_5

    .line 1463
    .end local v19    # "swapError":I
    :cond_1d
    :goto_6
    :sswitch_0
    if-eqz v23, :cond_0

    .line 1464
    const/4 v10, 0x1

    goto/16 :goto_0

    .line 1359
    .end local v14    # "isSwaped":Z
    .end local v21    # "view":Lcom/samsung/android/visualeffect/common/GLTextureView;
    :catchall_3
    move-exception v24

    :try_start_f
    monitor-exit v25
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_3

    :try_start_10
    throw v24

    .line 1361
    :cond_1e
    # getter for: Lcom/samsung/android/visualeffect/common/GLTextureView;->sGLThreadManager:Lcom/samsung/android/visualeffect/common/GLTextureView$GLThreadManager;
    invoke-static {}, Lcom/samsung/android/visualeffect/common/GLTextureView;->access$600()Lcom/samsung/android/visualeffect/common/GLTextureView$GLThreadManager;

    move-result-object v25

    monitor-enter v25
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_2

    .line 1362
    const/16 v24, 0x1

    :try_start_11
    move/from16 v0, v24

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->mFinishedCreatingEglSurface:Z

    .line 1363
    const/16 v24, 0x1

    move/from16 v0, v24

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->mSurfaceIsBad:Z

    .line 1364
    # getter for: Lcom/samsung/android/visualeffect/common/GLTextureView;->sGLThreadManager:Lcom/samsung/android/visualeffect/common/GLTextureView$GLThreadManager;
    invoke-static {}, Lcom/samsung/android/visualeffect/common/GLTextureView;->access$600()Lcom/samsung/android/visualeffect/common/GLTextureView$GLThreadManager;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/Object;->notifyAll()V

    .line 1365
    monitor-exit v25

    goto/16 :goto_0

    :catchall_4
    move-exception v24

    monitor-exit v25
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_4

    :try_start_12
    throw v24

    .line 1420
    .restart local v8    # "diffFPSTime":J
    .restart local v14    # "isSwaped":Z
    .restart local v21    # "view":Lcom/samsung/android/visualeffect/common/GLTextureView;
    :cond_1f
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v24

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->lastUpdateTime:J

    move-wide/from16 v26, v0

    sub-long v24, v24, v26

    const-wide/16 v26, 0x28

    cmp-long v24, v24, v26

    if-lez v24, :cond_20

    .line 1421
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v24

    move-wide/from16 v0, v24

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->lastUpdateTime:J

    .line 1422
    const/4 v14, 0x1

    .line 1423
    # getter for: Lcom/samsung/android/visualeffect/common/GLTextureView;->mRenderer:Lcom/samsung/android/visualeffect/common/GLTextureView$Renderer;
    invoke-static/range {v21 .. v21}, Lcom/samsung/android/visualeffect/common/GLTextureView;->access$700(Lcom/samsung/android/visualeffect/common/GLTextureView;)Lcom/samsung/android/visualeffect/common/GLTextureView$Renderer;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-interface {v0, v12}, Lcom/samsung/android/visualeffect/common/GLTextureView$Renderer;->onDrawFrame(Ljavax/microedition/khronos/opengles/GL10;)V

    .line 1424
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->currFPSTime:J

    move-wide/from16 v24, v0

    move-wide/from16 v0, v24

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->prevFPSTime:J
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_2

    goto/16 :goto_5

    .line 1427
    :cond_20
    const/4 v14, 0x0

    goto/16 :goto_5

    .line 1441
    .end local v8    # "diffFPSTime":J
    .restart local v19    # "swapError":I
    :sswitch_1
    const/4 v15, 0x1

    .line 1442
    goto :goto_6

    .line 1458
    :catchall_5
    move-exception v24

    :try_start_13
    monitor-exit v25
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_5

    :try_start_14
    throw v24
    :try_end_14
    .catchall {:try_start_14 .. :try_end_14} :catchall_2

    .line 1475
    .end local v14    # "isSwaped":Z
    .end local v19    # "swapError":I
    .end local v21    # "view":Lcom/samsung/android/visualeffect/common/GLTextureView;
    :catchall_6
    move-exception v24

    :try_start_15
    monitor-exit v25
    :try_end_15
    .catchall {:try_start_15 .. :try_end_15} :catchall_6

    throw v24

    .line 1433
    nop

    :sswitch_data_0
    .sparse-switch
        0x3000 -> :sswitch_0
        0x300e -> :sswitch_1
    .end sparse-switch
.end method

.method private readyToDraw()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1484
    iget-boolean v1, p0, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->mPaused:Z

    if-nez v1, :cond_1

    iget-boolean v1, p0, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->mHasSurface:Z

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->mSurfaceIsBad:Z

    if-nez v1, :cond_1

    iget v1, p0, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->mWidth:I

    if-lez v1, :cond_1

    iget v1, p0, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->mHeight:I

    if-lez v1, :cond_1

    iget-boolean v1, p0, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->mRequestRender:Z

    if-nez v1, :cond_0

    iget v1, p0, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->mRenderMode:I

    if-ne v1, v0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private stopEglContextLocked()V
    .locals 1

    .prologue
    .line 1118
    iget-boolean v0, p0, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->mHaveEglContext:Z

    if-eqz v0, :cond_0

    .line 1119
    iget-object v0, p0, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->mEglHelper:Lcom/samsung/android/visualeffect/common/GLTextureView$EglHelper;

    invoke-virtual {v0}, Lcom/samsung/android/visualeffect/common/GLTextureView$EglHelper;->finish()V

    .line 1120
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->mHaveEglContext:Z

    .line 1121
    # getter for: Lcom/samsung/android/visualeffect/common/GLTextureView;->sGLThreadManager:Lcom/samsung/android/visualeffect/common/GLTextureView$GLThreadManager;
    invoke-static {}, Lcom/samsung/android/visualeffect/common/GLTextureView;->access$600()Lcom/samsung/android/visualeffect/common/GLTextureView$GLThreadManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThreadManager;->releaseEglContextLocked(Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;)V

    .line 1123
    :cond_0
    return-void
.end method

.method private stopEglSurfaceLocked()V
    .locals 1

    .prologue
    .line 1107
    iget-boolean v0, p0, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->mHaveEglSurface:Z

    if-eqz v0, :cond_0

    .line 1108
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->mHaveEglSurface:Z

    .line 1109
    iget-object v0, p0, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->mEglHelper:Lcom/samsung/android/visualeffect/common/GLTextureView$EglHelper;

    invoke-virtual {v0}, Lcom/samsung/android/visualeffect/common/GLTextureView$EglHelper;->destroySurface()V

    .line 1111
    :cond_0
    return-void
.end method


# virtual methods
.method public ableToDraw()Z
    .locals 1

    .prologue
    .line 1480
    iget-boolean v0, p0, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->mHaveEglContext:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->mHaveEglSurface:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->readyToDraw()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getRenderMode()I
    .locals 2

    .prologue
    .line 1503
    # getter for: Lcom/samsung/android/visualeffect/common/GLTextureView;->sGLThreadManager:Lcom/samsung/android/visualeffect/common/GLTextureView$GLThreadManager;
    invoke-static {}, Lcom/samsung/android/visualeffect/common/GLTextureView;->access$600()Lcom/samsung/android/visualeffect/common/GLTextureView$GLThreadManager;

    move-result-object v1

    monitor-enter v1

    .line 1504
    :try_start_0
    iget v0, p0, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->mRenderMode:I

    monitor-exit v1

    return v0

    .line 1505
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public onPause()V
    .locals 3

    .prologue
    .line 1552
    # getter for: Lcom/samsung/android/visualeffect/common/GLTextureView;->sGLThreadManager:Lcom/samsung/android/visualeffect/common/GLTextureView$GLThreadManager;
    invoke-static {}, Lcom/samsung/android/visualeffect/common/GLTextureView;->access$600()Lcom/samsung/android/visualeffect/common/GLTextureView$GLThreadManager;

    move-result-object v2

    monitor-enter v2

    .line 1556
    const/4 v1, 0x1

    :try_start_0
    iput-boolean v1, p0, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->mRequestPaused:Z

    .line 1557
    # getter for: Lcom/samsung/android/visualeffect/common/GLTextureView;->sGLThreadManager:Lcom/samsung/android/visualeffect/common/GLTextureView$GLThreadManager;
    invoke-static {}, Lcom/samsung/android/visualeffect/common/GLTextureView;->access$600()Lcom/samsung/android/visualeffect/common/GLTextureView$GLThreadManager;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->notifyAll()V

    .line 1558
    :goto_0
    iget-boolean v1, p0, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->mExited:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->mPaused:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_0

    .line 1563
    :try_start_1
    # getter for: Lcom/samsung/android/visualeffect/common/GLTextureView;->sGLThreadManager:Lcom/samsung/android/visualeffect/common/GLTextureView$GLThreadManager;
    invoke-static {}, Lcom/samsung/android/visualeffect/common/GLTextureView;->access$600()Lcom/samsung/android/visualeffect/common/GLTextureView$GLThreadManager;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1564
    :catch_0
    move-exception v0

    .line 1565
    .local v0, "ex":Ljava/lang/InterruptedException;
    :try_start_2
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0

    .line 1568
    .end local v0    # "ex":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    :cond_0
    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1569
    return-void
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 1572
    # getter for: Lcom/samsung/android/visualeffect/common/GLTextureView;->sGLThreadManager:Lcom/samsung/android/visualeffect/common/GLTextureView$GLThreadManager;
    invoke-static {}, Lcom/samsung/android/visualeffect/common/GLTextureView;->access$600()Lcom/samsung/android/visualeffect/common/GLTextureView$GLThreadManager;

    move-result-object v2

    monitor-enter v2

    .line 1576
    const/4 v1, 0x0

    :try_start_0
    iput-boolean v1, p0, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->mRequestPaused:Z

    .line 1577
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->mRequestRender:Z

    .line 1578
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->mRenderComplete:Z

    .line 1579
    # getter for: Lcom/samsung/android/visualeffect/common/GLTextureView;->sGLThreadManager:Lcom/samsung/android/visualeffect/common/GLTextureView$GLThreadManager;
    invoke-static {}, Lcom/samsung/android/visualeffect/common/GLTextureView;->access$600()Lcom/samsung/android/visualeffect/common/GLTextureView$GLThreadManager;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->notifyAll()V

    .line 1580
    :goto_0
    iget-boolean v1, p0, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->mExited:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->mPaused:Z

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->mRenderComplete:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_0

    .line 1585
    :try_start_1
    # getter for: Lcom/samsung/android/visualeffect/common/GLTextureView;->sGLThreadManager:Lcom/samsung/android/visualeffect/common/GLTextureView$GLThreadManager;
    invoke-static {}, Lcom/samsung/android/visualeffect/common/GLTextureView;->access$600()Lcom/samsung/android/visualeffect/common/GLTextureView$GLThreadManager;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1586
    :catch_0
    move-exception v0

    .line 1587
    .local v0, "ex":Ljava/lang/InterruptedException;
    :try_start_2
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0

    .line 1590
    .end local v0    # "ex":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    :cond_0
    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1591
    return-void
.end method

.method public onWindowResize(II)V
    .locals 3
    .param p1, "w"    # I
    .param p2, "h"    # I

    .prologue
    .line 1594
    # getter for: Lcom/samsung/android/visualeffect/common/GLTextureView;->sGLThreadManager:Lcom/samsung/android/visualeffect/common/GLTextureView$GLThreadManager;
    invoke-static {}, Lcom/samsung/android/visualeffect/common/GLTextureView;->access$600()Lcom/samsung/android/visualeffect/common/GLTextureView$GLThreadManager;

    move-result-object v2

    monitor-enter v2

    .line 1595
    :try_start_0
    iput p1, p0, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->mWidth:I

    .line 1596
    iput p2, p0, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->mHeight:I

    .line 1597
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->mSizeChanged:Z

    .line 1598
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->mRequestRender:Z

    .line 1599
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->mRenderComplete:Z

    .line 1600
    # getter for: Lcom/samsung/android/visualeffect/common/GLTextureView;->sGLThreadManager:Lcom/samsung/android/visualeffect/common/GLTextureView$GLThreadManager;
    invoke-static {}, Lcom/samsung/android/visualeffect/common/GLTextureView;->access$600()Lcom/samsung/android/visualeffect/common/GLTextureView$GLThreadManager;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->notifyAll()V

    .line 1603
    :goto_0
    iget-boolean v1, p0, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->mExited:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->mPaused:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->mRenderComplete:Z

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->ableToDraw()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-eqz v1, :cond_0

    .line 1610
    :try_start_1
    # getter for: Lcom/samsung/android/visualeffect/common/GLTextureView;->sGLThreadManager:Lcom/samsung/android/visualeffect/common/GLTextureView$GLThreadManager;
    invoke-static {}, Lcom/samsung/android/visualeffect/common/GLTextureView;->access$600()Lcom/samsung/android/visualeffect/common/GLTextureView$GLThreadManager;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1611
    :catch_0
    move-exception v0

    .line 1612
    .local v0, "ex":Ljava/lang/InterruptedException;
    :try_start_2
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0

    .line 1615
    .end local v0    # "ex":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    :cond_0
    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1616
    return-void
.end method

.method public queueEvent(Ljava/lang/Runnable;)V
    .locals 2
    .param p1, "r"    # Ljava/lang/Runnable;

    .prologue
    .line 1646
    if-nez p1, :cond_0

    .line 1647
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "r must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1649
    :cond_0
    # getter for: Lcom/samsung/android/visualeffect/common/GLTextureView;->sGLThreadManager:Lcom/samsung/android/visualeffect/common/GLTextureView$GLThreadManager;
    invoke-static {}, Lcom/samsung/android/visualeffect/common/GLTextureView;->access$600()Lcom/samsung/android/visualeffect/common/GLTextureView$GLThreadManager;

    move-result-object v1

    monitor-enter v1

    .line 1650
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->mEventQueue:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1651
    # getter for: Lcom/samsung/android/visualeffect/common/GLTextureView;->sGLThreadManager:Lcom/samsung/android/visualeffect/common/GLTextureView$GLThreadManager;
    invoke-static {}, Lcom/samsung/android/visualeffect/common/GLTextureView;->access$600()Lcom/samsung/android/visualeffect/common/GLTextureView$GLThreadManager;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 1652
    monitor-exit v1

    .line 1653
    return-void

    .line 1652
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public requestExitAndWait()V
    .locals 3

    .prologue
    .line 1621
    # getter for: Lcom/samsung/android/visualeffect/common/GLTextureView;->sGLThreadManager:Lcom/samsung/android/visualeffect/common/GLTextureView$GLThreadManager;
    invoke-static {}, Lcom/samsung/android/visualeffect/common/GLTextureView;->access$600()Lcom/samsung/android/visualeffect/common/GLTextureView$GLThreadManager;

    move-result-object v2

    monitor-enter v2

    .line 1622
    const/4 v1, 0x1

    :try_start_0
    iput-boolean v1, p0, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->mShouldExit:Z

    .line 1623
    # getter for: Lcom/samsung/android/visualeffect/common/GLTextureView;->sGLThreadManager:Lcom/samsung/android/visualeffect/common/GLTextureView$GLThreadManager;
    invoke-static {}, Lcom/samsung/android/visualeffect/common/GLTextureView;->access$600()Lcom/samsung/android/visualeffect/common/GLTextureView$GLThreadManager;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->notifyAll()V

    .line 1624
    :goto_0
    iget-boolean v1, p0, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->mExited:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_0

    .line 1626
    :try_start_1
    # getter for: Lcom/samsung/android/visualeffect/common/GLTextureView;->sGLThreadManager:Lcom/samsung/android/visualeffect/common/GLTextureView$GLThreadManager;
    invoke-static {}, Lcom/samsung/android/visualeffect/common/GLTextureView;->access$600()Lcom/samsung/android/visualeffect/common/GLTextureView$GLThreadManager;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1627
    :catch_0
    move-exception v0

    .line 1628
    .local v0, "ex":Ljava/lang/InterruptedException;
    :try_start_2
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0

    .line 1631
    .end local v0    # "ex":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    :cond_0
    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1632
    return-void
.end method

.method public requestReleaseEglContextLocked()V
    .locals 1

    .prologue
    .line 1635
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->mShouldReleaseEglContext:Z

    .line 1636
    # getter for: Lcom/samsung/android/visualeffect/common/GLTextureView;->sGLThreadManager:Lcom/samsung/android/visualeffect/common/GLTextureView$GLThreadManager;
    invoke-static {}, Lcom/samsung/android/visualeffect/common/GLTextureView;->access$600()Lcom/samsung/android/visualeffect/common/GLTextureView$GLThreadManager;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 1637
    return-void
.end method

.method public requestRender()V
    .locals 2

    .prologue
    .line 1509
    # getter for: Lcom/samsung/android/visualeffect/common/GLTextureView;->sGLThreadManager:Lcom/samsung/android/visualeffect/common/GLTextureView$GLThreadManager;
    invoke-static {}, Lcom/samsung/android/visualeffect/common/GLTextureView;->access$600()Lcom/samsung/android/visualeffect/common/GLTextureView$GLThreadManager;

    move-result-object v1

    monitor-enter v1

    .line 1510
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->mRequestRender:Z

    .line 1511
    # getter for: Lcom/samsung/android/visualeffect/common/GLTextureView;->sGLThreadManager:Lcom/samsung/android/visualeffect/common/GLTextureView$GLThreadManager;
    invoke-static {}, Lcom/samsung/android/visualeffect/common/GLTextureView;->access$600()Lcom/samsung/android/visualeffect/common/GLTextureView$GLThreadManager;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 1512
    monitor-exit v1

    .line 1513
    return-void

    .line 1512
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public run()V
    .locals 4

    .prologue
    .line 1088
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "GLThread "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->getId()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->setName(Ljava/lang/String;)V

    .line 1094
    :try_start_0
    invoke-direct {p0}, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->guardedRun()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1098
    # getter for: Lcom/samsung/android/visualeffect/common/GLTextureView;->sGLThreadManager:Lcom/samsung/android/visualeffect/common/GLTextureView$GLThreadManager;
    invoke-static {}, Lcom/samsung/android/visualeffect/common/GLTextureView;->access$600()Lcom/samsung/android/visualeffect/common/GLTextureView$GLThreadManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThreadManager;->threadExiting(Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;)V

    .line 1100
    :goto_0
    return-void

    .line 1095
    :catch_0
    move-exception v0

    .line 1098
    # getter for: Lcom/samsung/android/visualeffect/common/GLTextureView;->sGLThreadManager:Lcom/samsung/android/visualeffect/common/GLTextureView$GLThreadManager;
    invoke-static {}, Lcom/samsung/android/visualeffect/common/GLTextureView;->access$600()Lcom/samsung/android/visualeffect/common/GLTextureView$GLThreadManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThreadManager;->threadExiting(Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    # getter for: Lcom/samsung/android/visualeffect/common/GLTextureView;->sGLThreadManager:Lcom/samsung/android/visualeffect/common/GLTextureView$GLThreadManager;
    invoke-static {}, Lcom/samsung/android/visualeffect/common/GLTextureView;->access$600()Lcom/samsung/android/visualeffect/common/GLTextureView$GLThreadManager;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThreadManager;->threadExiting(Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;)V

    throw v0
.end method

.method public setRenderMode(I)V
    .locals 2
    .param p1, "renderMode"    # I

    .prologue
    .line 1493
    if-ltz p1, :cond_0

    const/4 v0, 0x1

    if-le p1, v0, :cond_1

    .line 1494
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "renderMode"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1496
    :cond_1
    # getter for: Lcom/samsung/android/visualeffect/common/GLTextureView;->sGLThreadManager:Lcom/samsung/android/visualeffect/common/GLTextureView$GLThreadManager;
    invoke-static {}, Lcom/samsung/android/visualeffect/common/GLTextureView;->access$600()Lcom/samsung/android/visualeffect/common/GLTextureView$GLThreadManager;

    move-result-object v1

    monitor-enter v1

    .line 1497
    :try_start_0
    iput p1, p0, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->mRenderMode:I

    .line 1498
    # getter for: Lcom/samsung/android/visualeffect/common/GLTextureView;->sGLThreadManager:Lcom/samsung/android/visualeffect/common/GLTextureView$GLThreadManager;
    invoke-static {}, Lcom/samsung/android/visualeffect/common/GLTextureView;->access$600()Lcom/samsung/android/visualeffect/common/GLTextureView$GLThreadManager;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 1499
    monitor-exit v1

    .line 1500
    return-void

    .line 1499
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public surfaceCreated()V
    .locals 3

    .prologue
    .line 1516
    # getter for: Lcom/samsung/android/visualeffect/common/GLTextureView;->sGLThreadManager:Lcom/samsung/android/visualeffect/common/GLTextureView$GLThreadManager;
    invoke-static {}, Lcom/samsung/android/visualeffect/common/GLTextureView;->access$600()Lcom/samsung/android/visualeffect/common/GLTextureView$GLThreadManager;

    move-result-object v2

    monitor-enter v2

    .line 1520
    const/4 v1, 0x1

    :try_start_0
    iput-boolean v1, p0, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->mHasSurface:Z

    .line 1521
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->mFinishedCreatingEglSurface:Z

    .line 1522
    # getter for: Lcom/samsung/android/visualeffect/common/GLTextureView;->sGLThreadManager:Lcom/samsung/android/visualeffect/common/GLTextureView$GLThreadManager;
    invoke-static {}, Lcom/samsung/android/visualeffect/common/GLTextureView;->access$600()Lcom/samsung/android/visualeffect/common/GLTextureView$GLThreadManager;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->notifyAll()V

    .line 1524
    :goto_0
    iget-boolean v1, p0, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->mWaitingForSurface:Z

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->mFinishedCreatingEglSurface:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->mExited:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_0

    .line 1526
    :try_start_1
    # getter for: Lcom/samsung/android/visualeffect/common/GLTextureView;->sGLThreadManager:Lcom/samsung/android/visualeffect/common/GLTextureView$GLThreadManager;
    invoke-static {}, Lcom/samsung/android/visualeffect/common/GLTextureView;->access$600()Lcom/samsung/android/visualeffect/common/GLTextureView$GLThreadManager;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1527
    :catch_0
    move-exception v0

    .line 1528
    .local v0, "e":Ljava/lang/InterruptedException;
    :try_start_2
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0

    .line 1531
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    :cond_0
    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1532
    return-void
.end method

.method public surfaceDestroyed()V
    .locals 3

    .prologue
    .line 1535
    # getter for: Lcom/samsung/android/visualeffect/common/GLTextureView;->sGLThreadManager:Lcom/samsung/android/visualeffect/common/GLTextureView$GLThreadManager;
    invoke-static {}, Lcom/samsung/android/visualeffect/common/GLTextureView;->access$600()Lcom/samsung/android/visualeffect/common/GLTextureView$GLThreadManager;

    move-result-object v2

    monitor-enter v2

    .line 1539
    const/4 v1, 0x0

    :try_start_0
    iput-boolean v1, p0, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->mHasSurface:Z

    .line 1540
    # getter for: Lcom/samsung/android/visualeffect/common/GLTextureView;->sGLThreadManager:Lcom/samsung/android/visualeffect/common/GLTextureView$GLThreadManager;
    invoke-static {}, Lcom/samsung/android/visualeffect/common/GLTextureView;->access$600()Lcom/samsung/android/visualeffect/common/GLTextureView$GLThreadManager;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->notifyAll()V

    .line 1541
    :goto_0
    iget-boolean v1, p0, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->mWaitingForSurface:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/samsung/android/visualeffect/common/GLTextureView$GLThread;->mExited:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_0

    .line 1543
    :try_start_1
    # getter for: Lcom/samsung/android/visualeffect/common/GLTextureView;->sGLThreadManager:Lcom/samsung/android/visualeffect/common/GLTextureView$GLThreadManager;
    invoke-static {}, Lcom/samsung/android/visualeffect/common/GLTextureView;->access$600()Lcom/samsung/android/visualeffect/common/GLTextureView$GLThreadManager;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1544
    :catch_0
    move-exception v0

    .line 1545
    .local v0, "e":Ljava/lang/InterruptedException;
    :try_start_2
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0

    .line 1548
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    :cond_0
    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1549
    return-void
.end method
