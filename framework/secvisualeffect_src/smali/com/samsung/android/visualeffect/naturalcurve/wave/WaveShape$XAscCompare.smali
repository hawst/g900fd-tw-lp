.class Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape$XAscCompare;
.super Ljava/lang/Object;
.source "WaveShape.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "XAscCompare"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/samsung/android/visualeffect/naturalcurve/wave/WavePoint;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;


# direct methods
.method constructor <init>(Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;)V
    .locals 0

    .prologue
    .line 258
    iput-object p1, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape$XAscCompare;->this$0:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/samsung/android/visualeffect/naturalcurve/wave/WavePoint;Lcom/samsung/android/visualeffect/naturalcurve/wave/WavePoint;)I
    .locals 4
    .param p1, "lhs"    # Lcom/samsung/android/visualeffect/naturalcurve/wave/WavePoint;
    .param p2, "rhs"    # Lcom/samsung/android/visualeffect/naturalcurve/wave/WavePoint;

    .prologue
    const/4 v3, 0x0

    .line 261
    invoke-virtual {p1}, Lcom/samsung/android/visualeffect/naturalcurve/wave/WavePoint;->getX()F

    move-result v1

    invoke-virtual {p2}, Lcom/samsung/android/visualeffect/naturalcurve/wave/WavePoint;->getX()F

    move-result v2

    sub-float v0, v1, v2

    .line 263
    .local v0, "gap":F
    cmpg-float v1, v0, v3

    if-gez v1, :cond_0

    .line 264
    const/4 v1, 0x1

    .line 268
    :goto_0
    return v1

    .line 265
    :cond_0
    cmpl-float v1, v0, v3

    if-lez v1, :cond_1

    .line 266
    const/4 v1, -0x1

    goto :goto_0

    .line 268
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 258
    check-cast p1, Lcom/samsung/android/visualeffect/naturalcurve/wave/WavePoint;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/samsung/android/visualeffect/naturalcurve/wave/WavePoint;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape$XAscCompare;->compare(Lcom/samsung/android/visualeffect/naturalcurve/wave/WavePoint;Lcom/samsung/android/visualeffect/naturalcurve/wave/WavePoint;)I

    move-result v0

    return v0
.end method
