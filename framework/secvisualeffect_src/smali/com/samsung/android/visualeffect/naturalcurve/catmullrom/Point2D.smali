.class public Lcom/samsung/android/visualeffect/naturalcurve/catmullrom/Point2D;
.super Ljava/lang/Object;
.source "Point2D.java"


# instance fields
.field private x:F

.field private y:F


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 12
    invoke-direct {p0, v0, v0}, Lcom/samsung/android/visualeffect/naturalcurve/catmullrom/Point2D;-><init>(FF)V

    .line 13
    return-void
.end method

.method public constructor <init>(FF)V
    .locals 0
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput p1, p0, Lcom/samsung/android/visualeffect/naturalcurve/catmullrom/Point2D;->x:F

    .line 17
    iput p2, p0, Lcom/samsung/android/visualeffect/naturalcurve/catmullrom/Point2D;->y:F

    .line 18
    return-void
.end method


# virtual methods
.method public getX()F
    .locals 1

    .prologue
    .line 24
    iget v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/catmullrom/Point2D;->x:F

    return v0
.end method

.method public getY()F
    .locals 1

    .prologue
    .line 38
    iget v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/catmullrom/Point2D;->y:F

    return v0
.end method

.method public setX(F)V
    .locals 0
    .param p1, "x"    # F

    .prologue
    .line 31
    iput p1, p0, Lcom/samsung/android/visualeffect/naturalcurve/catmullrom/Point2D;->x:F

    .line 32
    return-void
.end method

.method public setY(F)V
    .locals 0
    .param p1, "y"    # F

    .prologue
    .line 45
    iput p1, p0, Lcom/samsung/android/visualeffect/naturalcurve/catmullrom/Point2D;->y:F

    .line 46
    return-void
.end method
