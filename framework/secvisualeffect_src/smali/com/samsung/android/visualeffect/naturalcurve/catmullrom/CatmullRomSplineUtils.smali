.class public Lcom/samsung/android/visualeffect/naturalcurve/catmullrom/CatmullRomSplineUtils;
.super Ljava/lang/Object;
.source "CatmullRomSplineUtils.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 8
    const-class v0, Lcom/samsung/android/visualeffect/naturalcurve/catmullrom/CatmullRomSplineUtils;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/samsung/android/visualeffect/naturalcurve/catmullrom/CatmullRomSplineUtils;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static subdividePoints([Lcom/samsung/android/visualeffect/naturalcurve/catmullrom/Point2D;I)[Lcom/samsung/android/visualeffect/naturalcurve/catmullrom/Point2D;
    .locals 11
    .param p0, "points"    # [Lcom/samsung/android/visualeffect/naturalcurve/catmullrom/Point2D;
    .param p1, "subdivisions"    # I

    .prologue
    .line 18
    sget-boolean v9, Lcom/samsung/android/visualeffect/naturalcurve/catmullrom/CatmullRomSplineUtils;->$assertionsDisabled:Z

    if-nez v9, :cond_0

    if-nez p0, :cond_0

    new-instance v9, Ljava/lang/AssertionError;

    invoke-direct {v9}, Ljava/lang/AssertionError;-><init>()V

    throw v9

    .line 19
    :cond_0
    sget-boolean v9, Lcom/samsung/android/visualeffect/naturalcurve/catmullrom/CatmullRomSplineUtils;->$assertionsDisabled:Z

    if-nez v9, :cond_1

    array-length v9, p0

    const/4 v10, 0x3

    if-ge v9, v10, :cond_1

    new-instance v9, Ljava/lang/AssertionError;

    invoke-direct {v9}, Ljava/lang/AssertionError;-><init>()V

    throw v9

    .line 21
    :cond_1
    array-length v9, p0

    add-int/lit8 v9, v9, -0x1

    mul-int/2addr v9, p1

    add-int/lit8 v9, v9, 0x1

    new-array v8, v9, [Lcom/samsung/android/visualeffect/naturalcurve/catmullrom/Point2D;

    .line 23
    .local v8, "subdividedPoints":[Lcom/samsung/android/visualeffect/naturalcurve/catmullrom/Point2D;
    const/high16 v9, 0x3f800000    # 1.0f

    int-to-float v10, p1

    div-float v2, v9, v10

    .line 25
    .local v2, "increments":F
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v9, p0

    add-int/lit8 v9, v9, -0x1

    if-ge v1, v9, :cond_5

    .line 26
    if-nez v1, :cond_2

    aget-object v4, p0, v1

    .line 27
    .local v4, "p0":Lcom/samsung/android/visualeffect/naturalcurve/catmullrom/Point2D;
    :goto_1
    aget-object v5, p0, v1

    .line 28
    .local v5, "p1":Lcom/samsung/android/visualeffect/naturalcurve/catmullrom/Point2D;
    add-int/lit8 v9, v1, 0x1

    aget-object v6, p0, v9

    .line 29
    .local v6, "p2":Lcom/samsung/android/visualeffect/naturalcurve/catmullrom/Point2D;
    add-int/lit8 v9, v1, 0x2

    array-length v10, p0

    if-ne v9, v10, :cond_3

    add-int/lit8 v9, v1, 0x1

    aget-object v7, p0, v9

    .line 31
    .local v7, "p3":Lcom/samsung/android/visualeffect/naturalcurve/catmullrom/Point2D;
    :goto_2
    new-instance v0, Lcom/samsung/android/visualeffect/naturalcurve/catmullrom/CatmullRomSpline2D;

    invoke-direct {v0, v4, v5, v6, v7}, Lcom/samsung/android/visualeffect/naturalcurve/catmullrom/CatmullRomSpline2D;-><init>(Lcom/samsung/android/visualeffect/naturalcurve/catmullrom/Point2D;Lcom/samsung/android/visualeffect/naturalcurve/catmullrom/Point2D;Lcom/samsung/android/visualeffect/naturalcurve/catmullrom/Point2D;Lcom/samsung/android/visualeffect/naturalcurve/catmullrom/Point2D;)V

    .line 33
    .local v0, "crs":Lcom/samsung/android/visualeffect/naturalcurve/catmullrom/CatmullRomSpline2D;
    const/4 v3, 0x0

    .local v3, "j":I
    :goto_3
    if-gt v3, p1, :cond_4

    .line 34
    mul-int v9, v1, p1

    add-int/2addr v9, v3

    int-to-float v10, v3

    mul-float/2addr v10, v2

    invoke-virtual {v0, v10}, Lcom/samsung/android/visualeffect/naturalcurve/catmullrom/CatmullRomSpline2D;->q(F)Lcom/samsung/android/visualeffect/naturalcurve/catmullrom/Point2D;

    move-result-object v10

    aput-object v10, v8, v9

    .line 33
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 26
    .end local v0    # "crs":Lcom/samsung/android/visualeffect/naturalcurve/catmullrom/CatmullRomSpline2D;
    .end local v3    # "j":I
    .end local v4    # "p0":Lcom/samsung/android/visualeffect/naturalcurve/catmullrom/Point2D;
    .end local v5    # "p1":Lcom/samsung/android/visualeffect/naturalcurve/catmullrom/Point2D;
    .end local v6    # "p2":Lcom/samsung/android/visualeffect/naturalcurve/catmullrom/Point2D;
    .end local v7    # "p3":Lcom/samsung/android/visualeffect/naturalcurve/catmullrom/Point2D;
    :cond_2
    add-int/lit8 v9, v1, -0x1

    aget-object v4, p0, v9

    goto :goto_1

    .line 29
    .restart local v4    # "p0":Lcom/samsung/android/visualeffect/naturalcurve/catmullrom/Point2D;
    .restart local v5    # "p1":Lcom/samsung/android/visualeffect/naturalcurve/catmullrom/Point2D;
    .restart local v6    # "p2":Lcom/samsung/android/visualeffect/naturalcurve/catmullrom/Point2D;
    :cond_3
    add-int/lit8 v9, v1, 0x2

    aget-object v7, p0, v9

    goto :goto_2

    .line 25
    .restart local v0    # "crs":Lcom/samsung/android/visualeffect/naturalcurve/catmullrom/CatmullRomSpline2D;
    .restart local v3    # "j":I
    .restart local v7    # "p3":Lcom/samsung/android/visualeffect/naturalcurve/catmullrom/Point2D;
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 37
    .end local v0    # "crs":Lcom/samsung/android/visualeffect/naturalcurve/catmullrom/CatmullRomSpline2D;
    .end local v3    # "j":I
    .end local v4    # "p0":Lcom/samsung/android/visualeffect/naturalcurve/catmullrom/Point2D;
    .end local v5    # "p1":Lcom/samsung/android/visualeffect/naturalcurve/catmullrom/Point2D;
    .end local v6    # "p2":Lcom/samsung/android/visualeffect/naturalcurve/catmullrom/Point2D;
    .end local v7    # "p3":Lcom/samsung/android/visualeffect/naturalcurve/catmullrom/Point2D;
    :cond_5
    return-object v8
.end method
