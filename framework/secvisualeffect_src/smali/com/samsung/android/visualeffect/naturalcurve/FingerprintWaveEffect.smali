.class public Lcom/samsung/android/visualeffect/naturalcurve/FingerprintWaveEffect;
.super Landroid/widget/FrameLayout;
.source "FingerprintWaveEffect.java"


# instance fields
.field private final TAG:Ljava/lang/String;

.field private effect:Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;

.field private effectHeight:I

.field private effectWidth:I


# direct methods
.method public constructor <init>(Landroid/content/Context;III)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "width"    # I
    .param p3, "height"    # I
    .param p4, "imageResId"    # I

    .prologue
    const/4 v4, 0x0

    .line 21
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 13
    const-string v0, "FingerprintWaveEffect"

    iput-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/FingerprintWaveEffect;->TAG:Ljava/lang/String;

    .line 22
    const-string v0, "FingerprintWaveEffect"

    const-string v1, "FingerprintWaveEffect Constructor"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 24
    iput p2, p0, Lcom/samsung/android/visualeffect/naturalcurve/FingerprintWaveEffect;->effectWidth:I

    .line 25
    iput p3, p0, Lcom/samsung/android/visualeffect/naturalcurve/FingerprintWaveEffect;->effectHeight:I

    .line 27
    new-instance v0, Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;

    invoke-virtual {p0}, Lcom/samsung/android/visualeffect/naturalcurve/FingerprintWaveEffect;->getContext()Landroid/content/Context;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/visualeffect/naturalcurve/FingerprintWaveEffect;->effectWidth:I

    iget v3, p0, Lcom/samsung/android/visualeffect/naturalcurve/FingerprintWaveEffect;->effectHeight:I

    move v5, v4

    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;-><init>(Landroid/content/Context;IIII)V

    iput-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/FingerprintWaveEffect;->effect:Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;

    .line 28
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/FingerprintWaveEffect;->effect:Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;

    iget v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/FingerprintWaveEffect;->effectWidth:I

    iget v2, p0, Lcom/samsung/android/visualeffect/naturalcurve/FingerprintWaveEffect;->effectHeight:I

    invoke-virtual {p0, v0, v1, v2}, Lcom/samsung/android/visualeffect/naturalcurve/FingerprintWaveEffect;->addView(Landroid/view/View;II)V

    .line 30
    new-instance v6, Landroid/widget/ImageView;

    invoke-direct {v6, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 31
    .local v6, "img":Landroid/widget/ImageView;
    invoke-virtual {v6, p4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 32
    invoke-virtual {p0, v6}, Lcom/samsung/android/visualeffect/naturalcurve/FingerprintWaveEffect;->addView(Landroid/view/View;)V

    .line 33
    return-void
.end method


# virtual methods
.method public setColor(II)V
    .locals 2
    .param p1, "colorUpside"    # I
    .param p2, "colorDownside"    # I

    .prologue
    .line 41
    const-string v0, "FingerprintWaveEffect"

    const-string v1, "BatteryEffect : setColor"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 42
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/FingerprintWaveEffect;->effect:Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/FingerprintWaveEffect;->effect:Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;->setColor(II)V

    .line 43
    :cond_0
    return-void
.end method

.method public setPercent(I)V
    .locals 3
    .param p1, "percent"    # I

    .prologue
    .line 36
    const-string v0, "FingerprintWaveEffect"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "FingerprintWaveEffect : setPercent = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 37
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/FingerprintWaveEffect;->effect:Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/FingerprintWaveEffect;->effect:Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;->setBatteryPercent(I)V

    .line 38
    :cond_0
    return-void
.end method
