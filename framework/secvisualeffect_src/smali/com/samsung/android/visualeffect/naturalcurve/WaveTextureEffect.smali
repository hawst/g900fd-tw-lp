.class public Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;
.super Landroid/view/TextureView;
.source "WaveTextureEffect.java"

# interfaces
.implements Landroid/view/TextureView$SurfaceTextureListener;
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect$1;,
        Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect$VerticalAlign;,
        Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect$OpaqueType;
    }
.end annotation


# instance fields
.field private final ANIMATION_STOP_VALUE:F

.field private final TAG:Ljava/lang/String;

.field private final WAVE_OPACITY_0:F

.field private final WAVE_OPACITY_1:F

.field private animationValue0:F

.field private animationValue1:F

.field private bottomColor:I

.field private centerColor:I

.field private commonColor:I

.field private currentOpaqueType:Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect$OpaqueType;

.field private currentPercent:F

.field private currentWaveHeightRatio0:F

.field private currentWaveHeightRatio1:F

.field private easingValue:F

.field private effectHeight:I

.field private effectWidth:I

.field private elasticDist0:F

.field private elasticDist1:F

.field private elasticValue:F

.field private isForTension:Z

.field private isStartEffectCalledBeforeSurfaceCreated:Z

.field private isSurfaceCreated:Z

.field private isThreadRunning:Z

.field private isYMoving:Z

.field private opacity:F

.field private prevTimeStamp:J

.field private surface:Landroid/graphics/SurfaceTexture;

.field private targetPercent:F

.field private targetWaveHeightRatio:F

.field private theOtherSide:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape$SideType;

.field private thisSide:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape$SideType;

.field private thread:Ljava/lang/Thread;

.field private topColor:I

.field private verticalAlign:Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect$VerticalAlign;

.field private wave0:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;

.field private wave1:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;

.field private waveHeight0:F

.field private waveHeight1:F

.field private waveOpacity0:F

.field private waveOpacity1:F

.field private waveRatio0:F

.field private waveRatio1:F

.field private waveSpeed0:F

.field private waveSpeed1:F


# direct methods
.method public constructor <init>(Landroid/content/Context;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect$VerticalAlign;FF)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "width"    # I
    .param p3, "height"    # I
    .param p4, "topColor"    # Ljava/lang/String;
    .param p5, "centerColor"    # Ljava/lang/String;
    .param p6, "bottomColor"    # Ljava/lang/String;
    .param p7, "align"    # Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect$VerticalAlign;
    .param p8, "waveWidthRatio"    # F
    .param p9, "opacity"    # F

    .prologue
    const/high16 v5, 0x3f000000    # 0.5f

    const/high16 v4, 0x3f800000    # 1.0f

    const v3, 0x3f333333    # 0.7f

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 74
    invoke-direct {p0, p1}, Landroid/view/TextureView;-><init>(Landroid/content/Context;)V

    .line 19
    const-string v0, "VisualEffectWaveTextureEffect"

    iput-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->TAG:Ljava/lang/String;

    .line 21
    iput v3, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->WAVE_OPACITY_0:F

    .line 22
    iput v5, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->WAVE_OPACITY_1:F

    .line 23
    const v0, 0x3727c5ac    # 1.0E-5f

    iput v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->ANIMATION_STOP_VALUE:F

    .line 27
    const v0, -0x9f7a69

    iput v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->topColor:I

    .line 28
    const v0, -0x1c232a

    iput v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->bottomColor:I

    .line 29
    const v0, -0x502446

    iput v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->centerColor:I

    .line 30
    iput v4, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->opacity:F

    .line 31
    iput v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->elasticDist0:F

    .line 32
    iput v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->elasticDist1:F

    .line 33
    const/high16 v0, 0x40a00000    # 5.0f

    iput v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->easingValue:F

    .line 34
    const v0, 0x3f19999a    # 0.6f

    iput v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->waveRatio0:F

    .line 35
    iput v3, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->waveRatio1:F

    .line 36
    const v0, 0x3f666666    # 0.9f

    iput v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->elasticValue:F

    .line 37
    const/high16 v0, 0x41600000    # 14.0f

    iput v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->waveSpeed0:F

    .line 38
    const/high16 v0, 0x41b00000    # 22.0f

    iput v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->waveSpeed1:F

    .line 39
    const/high16 v0, 0x42a00000    # 80.0f

    iput v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->waveHeight0:F

    .line 40
    const/high16 v0, 0x42b40000    # 90.0f

    iput v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->waveHeight1:F

    .line 41
    iput v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->targetPercent:F

    .line 42
    iput v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->currentPercent:F

    .line 43
    iput v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->animationValue0:F

    .line 44
    iput v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->animationValue1:F

    .line 45
    iput v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->targetWaveHeightRatio:F

    .line 46
    iput v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->currentWaveHeightRatio0:F

    .line 47
    iput v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->currentWaveHeightRatio1:F

    .line 48
    iput v3, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->waveOpacity0:F

    .line 49
    iput v5, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->waveOpacity1:F

    .line 50
    iput-boolean v2, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->isYMoving:Z

    .line 51
    iput-boolean v2, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->isForTension:Z

    .line 52
    iput-boolean v2, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->isThreadRunning:Z

    .line 53
    iput-boolean v2, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->isSurfaceCreated:Z

    .line 54
    iput-boolean v2, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->isStartEffectCalledBeforeSurfaceCreated:Z

    .line 55
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->prevTimeStamp:J

    .line 62
    sget-object v0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect$OpaqueType;->BOTH:Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect$OpaqueType;

    iput-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->currentOpaqueType:Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect$OpaqueType;

    .line 75
    const-string v0, "VisualEffectWaveTextureEffect"

    const-string v1, "WaveTextureEffect constructor"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 77
    iput p2, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->effectWidth:I

    .line 78
    iput p3, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->effectHeight:I

    .line 79
    iput p9, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->opacity:F

    .line 80
    cmpl-float v0, p9, v4

    if-eqz v0, :cond_0

    .line 81
    mul-float v0, p9, v3

    const v1, 0x3f59999a    # 0.85f

    div-float/2addr v0, v1

    iput v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->waveOpacity0:F

    .line 82
    iget v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->waveOpacity0:F

    sub-float v0, p9, v0

    iget v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->waveOpacity0:F

    sub-float v1, v4, v1

    div-float/2addr v0, v1

    iput v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->waveOpacity1:F

    .line 84
    :cond_0
    iput-object p7, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->verticalAlign:Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect$VerticalAlign;

    .line 85
    iget v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->waveRatio0:F

    mul-float/2addr v0, p8

    iput v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->waveRatio0:F

    .line 86
    iget v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->waveRatio1:F

    mul-float/2addr v0, p8

    iput v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->waveRatio1:F

    .line 88
    if-eqz p4, :cond_2

    .line 89
    invoke-static {p4}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->topColor:I

    .line 95
    :goto_0
    if-eqz p5, :cond_1

    .line 96
    invoke-static {p5}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->centerColor:I

    .line 98
    :cond_1
    if-eqz p6, :cond_3

    .line 99
    invoke-static {p6}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->bottomColor:I

    .line 106
    :goto_1
    const-string v0, "VisualEffectWaveTextureEffect"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setInitValues : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->effectWidth:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " x "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->effectHeight:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 107
    const-string v0, "VisualEffectWaveTextureEffect"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setInitValues - top : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->topColor:I

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", center : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->centerColor:I

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", bottom : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->bottomColor:I

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 110
    const-string v0, "VisualEffectWaveTextureEffect"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setInitValues - top : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", center : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", bottom : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 113
    const-string v0, "VisualEffectWaveTextureEffect"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setInitValues - currentOpaqueType : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->currentOpaqueType:Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect$OpaqueType;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 114
    const-string v0, "VisualEffectWaveTextureEffect"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setInitValues - verticalAlign : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->verticalAlign:Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect$VerticalAlign;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 115
    const-string v0, "VisualEffectWaveTextureEffect"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setInitValues - opacity : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p9}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 116
    const-string v0, "VisualEffectWaveTextureEffect"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setInitValues - waveWidthRatio : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p8}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 118
    invoke-direct {p0}, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->initEffect()V

    .line 119
    return-void

    .line 91
    :cond_2
    iput v2, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->topColor:I

    .line 92
    sget-object v0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect$OpaqueType;->BOTTOM:Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect$OpaqueType;

    iput-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->currentOpaqueType:Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect$OpaqueType;

    .line 93
    invoke-virtual {p0, v2}, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->setOpaque(Z)V

    goto/16 :goto_0

    .line 101
    :cond_3
    iput v2, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->bottomColor:I

    .line 102
    sget-object v0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect$OpaqueType;->TOP:Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect$OpaqueType;

    iput-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->currentOpaqueType:Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect$OpaqueType;

    .line 103
    invoke-virtual {p0, v2}, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->setOpaque(Z)V

    goto/16 :goto_1
.end method

.method private doDraw(Landroid/graphics/Canvas;)V
    .locals 8
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/high16 v7, 0x40000000    # 2.0f

    const/high16 v6, 0x41200000    # 10.0f

    const v5, 0x3727c5ac    # 1.0E-5f

    const/4 v4, 0x0

    .line 193
    if-nez p1, :cond_0

    .line 257
    :goto_0
    return-void

    .line 195
    :cond_0
    iget v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->effectWidth:I

    iget v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->effectHeight:I

    invoke-virtual {p1, v4, v4, v0, v1}, Landroid/graphics/Canvas;->clipRect(IIII)Z

    .line 197
    iget-boolean v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->isYMoving:Z

    if-eqz v0, :cond_1

    .line 198
    iget v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->currentPercent:F

    iget v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->targetPercent:F

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    const v1, 0x3e99999a    # 0.3f

    cmpg-float v0, v0, v1

    if-gez v0, :cond_5

    .line 199
    iget v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->targetPercent:F

    iput v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->currentPercent:F

    .line 200
    iput-boolean v4, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->isYMoving:Z

    .line 204
    :goto_1
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->wave0:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;

    iget v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->currentPercent:F

    invoke-virtual {v0, v1}, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->setPercent(F)V

    .line 205
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->wave1:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;

    iget v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->currentPercent:F

    invoke-virtual {v0, v1}, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->setPercent(F)V

    .line 208
    :cond_1
    iget-boolean v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->isForTension:Z

    if-eqz v0, :cond_3

    .line 209
    iget v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->animationValue0:F

    iget v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->animationValue0:F

    const/high16 v2, 0x41a00000    # 20.0f

    div-float/2addr v1, v2

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->animationValue0:F

    .line 210
    iget v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->animationValue1:F

    iget v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->animationValue1:F

    div-float/2addr v1, v6

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->animationValue1:F

    .line 211
    iget v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->animationValue0:F

    cmpg-float v0, v0, v5

    if-gez v0, :cond_2

    iget v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->animationValue1:F

    cmpg-float v0, v0, v5

    if-gez v0, :cond_2

    .line 212
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->animationValue1:F

    iput v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->animationValue0:F

    .line 213
    iput-boolean v4, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->isForTension:Z

    .line 214
    const-string v0, "VisualEffectWaveTextureEffect"

    const-string v1, "stopTention"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 216
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->wave0:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;

    iget v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->waveSpeed0:F

    iget v2, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->animationValue0:F

    const/high16 v3, 0x41700000    # 15.0f

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->setSpeed(F)V

    .line 217
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->wave1:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;

    iget v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->waveSpeed1:F

    iget v2, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->animationValue1:F

    mul-float/2addr v2, v6

    add-float/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->setSpeed(F)V

    .line 220
    :cond_3
    iget v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->targetWaveHeightRatio:F

    iget v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->animationValue0:F

    div-float/2addr v1, v7

    add-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->currentWaveHeightRatio0:F

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->easingValue:F

    div-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->elasticDist0:F

    iget v2, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->elasticValue:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    iput v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->elasticDist0:F

    .line 221
    iget v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->currentWaveHeightRatio0:F

    iget v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->elasticDist0:F

    add-float/2addr v0, v1

    iput v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->currentWaveHeightRatio0:F

    .line 222
    iget v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->targetWaveHeightRatio:F

    iget v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->animationValue1:F

    div-float/2addr v1, v7

    add-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->currentWaveHeightRatio1:F

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->easingValue:F

    div-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->elasticDist1:F

    iget v2, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->elasticValue:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    iput v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->elasticDist1:F

    .line 223
    iget v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->currentWaveHeightRatio1:F

    iget v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->elasticDist1:F

    add-float/2addr v0, v1

    iput v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->currentWaveHeightRatio1:F

    .line 225
    iget-boolean v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->isThreadRunning:Z

    if-eqz v0, :cond_4

    iget v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->currentWaveHeightRatio0:F

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    cmpg-float v0, v0, v5

    if-gtz v0, :cond_4

    iget v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->currentWaveHeightRatio1:F

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    cmpg-float v0, v0, v5

    if-gtz v0, :cond_4

    .line 227
    invoke-direct {p0}, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->stopThread()V

    .line 230
    :cond_4
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->wave0:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;

    iget v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->currentWaveHeightRatio0:F

    invoke-virtual {v0, v1}, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->setWaveHeightRatio(F)V

    .line 231
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->wave1:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;

    iget v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->currentWaveHeightRatio1:F

    invoke-virtual {v0, v1}, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->setWaveHeightRatio(F)V

    .line 233
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->wave0:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;

    invoke-virtual {v0, p1}, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->calculatePoints(Landroid/graphics/Canvas;)V

    .line 234
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->wave1:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;

    invoke-virtual {v0, p1}, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->calculatePoints(Landroid/graphics/Canvas;)V

    .line 236
    sget-object v0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect$1;->$SwitchMap$com$samsung$android$visualeffect$naturalcurve$WaveTextureEffect$OpaqueType:[I

    iget-object v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->currentOpaqueType:Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect$OpaqueType;

    invoke-virtual {v1}, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect$OpaqueType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto/16 :goto_0

    .line 238
    :pswitch_0
    iget v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->bottomColor:I

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 239
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->wave0:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;

    sget-object v1, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape$SideType;->Upside:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape$SideType;

    invoke-virtual {v0, p1, v1}, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->drawWave(Landroid/graphics/Canvas;Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape$SideType;)V

    .line 240
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->wave1:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;

    sget-object v1, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape$SideType;->Upside:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape$SideType;

    invoke-virtual {v0, p1, v1}, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->drawWave(Landroid/graphics/Canvas;Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape$SideType;)V

    .line 241
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->wave0:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;

    sget-object v1, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape$SideType;->Downside:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape$SideType;

    invoke-virtual {v0, p1, v1}, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->drawWave(Landroid/graphics/Canvas;Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape$SideType;)V

    goto/16 :goto_0

    .line 202
    :cond_5
    iget v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->currentPercent:F

    iget v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->targetPercent:F

    iget v2, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->currentPercent:F

    sub-float/2addr v1, v2

    div-float/2addr v1, v6

    add-float/2addr v0, v1

    iput v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->currentPercent:F

    goto/16 :goto_1

    .line 246
    :pswitch_1
    sget-object v0, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {p1, v4, v0}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    .line 247
    iget v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->opacity:F

    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v0, v0, v1

    if-nez v0, :cond_6

    .line 248
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->wave0:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;

    iget-object v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->thisSide:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape$SideType;

    invoke-virtual {v0, p1, v1}, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->drawWave(Landroid/graphics/Canvas;Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape$SideType;)V

    .line 249
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->wave1:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;

    iget-object v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->theOtherSide:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape$SideType;

    invoke-virtual {v0, p1, v1}, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->drawWave(Landroid/graphics/Canvas;Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape$SideType;)V

    .line 250
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->wave1:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;

    iget-object v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->thisSide:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape$SideType;

    invoke-virtual {v0, p1, v1}, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->drawWave(Landroid/graphics/Canvas;Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape$SideType;)V

    goto/16 :goto_0

    .line 252
    :cond_6
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->wave0:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;

    iget-object v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->thisSide:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape$SideType;

    invoke-virtual {v0, p1, v1}, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->drawWave(Landroid/graphics/Canvas;Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape$SideType;)V

    .line 253
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->wave1:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;

    iget-object v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->thisSide:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape$SideType;

    invoke-virtual {v0, p1, v1}, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->drawWave(Landroid/graphics/Canvas;Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape$SideType;)V

    goto/16 :goto_0

    .line 236
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private getColorWithOpacity(IF)I
    .locals 5
    .param p1, "color"    # I
    .param p2, "opacity"    # F

    .prologue
    .line 350
    const v2, 0xffffff

    and-int v0, p1, v2

    .line 351
    .local v0, "resultColor":I
    const/high16 v2, 0x437f0000    # 255.0f

    mul-float/2addr v2, p2

    float-to-int v1, v2

    .line 352
    .local v1, "tempColor":I
    shl-int/lit8 v1, v1, 0x18

    .line 353
    or-int/2addr v0, v1

    .line 354
    const-string v2, "VisualEffectWaveTextureEffect"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getColorWithOpacity : color = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", opacity = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", resultColor = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 356
    return v0
.end method

.method private getDefaultPercent()F
    .locals 2

    .prologue
    .line 344
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->verticalAlign:Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect$VerticalAlign;

    sget-object v1, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect$VerticalAlign;->TOP:Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect$VerticalAlign;

    if-ne v0, v1, :cond_0

    const/high16 v0, 0x42c80000    # 100.0f

    .line 346
    :goto_0
    return v0

    .line 345
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->verticalAlign:Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect$VerticalAlign;

    sget-object v1, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect$VerticalAlign;->BOTTOM:Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect$VerticalAlign;

    if-ne v0, v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 346
    :cond_1
    const/high16 v0, 0x42480000    # 50.0f

    goto :goto_0
.end method

.method private initEffect()V
    .locals 9

    .prologue
    const/high16 v3, 0x44b40000    # 1440.0f

    const/high16 v2, 0x40000000    # 2.0f

    const/4 v8, 0x0

    const/4 v5, 0x1

    const/4 v7, 0x0

    .line 122
    invoke-virtual {p0, p0}, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->setSurfaceTextureListener(Landroid/view/TextureView$SurfaceTextureListener;)V

    .line 124
    iget v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->waveSpeed0:F

    iget v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->effectWidth:I

    int-to-float v1, v1

    div-float/2addr v1, v3

    mul-float/2addr v0, v1

    iput v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->waveSpeed0:F

    .line 125
    iget v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->waveSpeed1:F

    iget v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->effectWidth:I

    int-to-float v1, v1

    div-float/2addr v1, v3

    mul-float/2addr v0, v1

    iput v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->waveSpeed1:F

    .line 126
    iget v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->effectHeight:I

    int-to-float v0, v0

    div-float/2addr v0, v2

    iput v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->waveHeight0:F

    .line 127
    iget v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->effectHeight:I

    int-to-float v0, v0

    div-float/2addr v0, v2

    const v1, 0x3f666666    # 0.9f

    mul-float/2addr v0, v1

    iput v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->waveHeight1:F

    .line 129
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->currentOpaqueType:Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect$OpaqueType;

    sget-object v1, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect$OpaqueType;->TOP:Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect$OpaqueType;

    if-ne v0, v1, :cond_1

    .line 130
    sget-object v0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape$SideType;->Upside:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape$SideType;

    iput-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->thisSide:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape$SideType;

    .line 131
    sget-object v0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape$SideType;->Downside:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape$SideType;

    iput-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->theOtherSide:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape$SideType;

    .line 132
    iget v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->topColor:I

    iput v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->commonColor:I

    .line 139
    :cond_0
    :goto_0
    sget-object v0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect$1;->$SwitchMap$com$samsung$android$visualeffect$naturalcurve$WaveTextureEffect$OpaqueType:[I

    iget-object v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->currentOpaqueType:Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect$OpaqueType;

    invoke-virtual {v1}, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect$OpaqueType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 165
    :goto_1
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->wave0:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;

    iget v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->waveSpeed0:F

    invoke-virtual {v0, v1}, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->setSpeed(F)V

    .line 166
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->wave0:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;

    iget v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->waveHeight0:F

    invoke-virtual {v0, v1}, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->setWaveHeight(F)V

    .line 167
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->wave0:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;

    invoke-virtual {v0, v8}, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->setWaveHeightRatio(F)V

    .line 168
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->wave1:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;

    iget v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->waveSpeed1:F

    invoke-virtual {v0, v1}, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->setSpeed(F)V

    .line 169
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->wave1:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;

    iget v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->waveHeight1:F

    invoke-virtual {v0, v1}, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->setWaveHeight(F)V

    .line 170
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->wave1:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;

    invoke-virtual {v0, v8}, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->setWaveHeightRatio(F)V

    .line 172
    invoke-direct {p0}, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->getDefaultPercent()F

    move-result v6

    .line 173
    .local v6, "percent":F
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->wave0:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;

    invoke-virtual {v0, v6}, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->setPercent(F)V

    .line 174
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->wave1:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;

    invoke-virtual {v0, v6}, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->setPercent(F)V

    .line 175
    iput v6, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->currentPercent:F

    .line 176
    return-void

    .line 133
    .end local v6    # "percent":F
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->currentOpaqueType:Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect$OpaqueType;

    sget-object v1, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect$OpaqueType;->BOTTOM:Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect$OpaqueType;

    if-ne v0, v1, :cond_0

    .line 134
    sget-object v0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape$SideType;->Downside:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape$SideType;

    iput-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->thisSide:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape$SideType;

    .line 135
    sget-object v0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape$SideType;->Upside:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape$SideType;

    iput-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->theOtherSide:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape$SideType;

    .line 136
    iget v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->bottomColor:I

    iput v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->commonColor:I

    goto :goto_0

    .line 141
    :pswitch_0
    new-instance v0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;

    iget v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->effectWidth:I

    iget v2, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->effectHeight:I

    sget-object v3, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape$SideType;->Bothside:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape$SideType;

    iget v4, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->waveRatio0:F

    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;-><init>(IILcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape$SideType;FZ)V

    iput-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->wave0:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;

    .line 142
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->wave0:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;

    iget v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->centerColor:I

    iget v2, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->bottomColor:I

    invoke-virtual {v0, v1, v2, v5}, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->setColor(IIZ)V

    .line 143
    new-instance v0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;

    iget v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->effectWidth:I

    iget v2, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->effectHeight:I

    sget-object v3, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape$SideType;->Upside:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape$SideType;

    iget v4, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->waveRatio1:F

    move v5, v7

    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;-><init>(IILcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape$SideType;FZ)V

    iput-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->wave1:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;

    .line 144
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->wave1:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;

    iget v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->topColor:I

    invoke-virtual {v0, v1, v7}, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->setColor(IZ)V

    goto :goto_1

    .line 149
    :pswitch_1
    iget v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->opacity:F

    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v0, v0, v1

    if-nez v0, :cond_2

    .line 150
    new-instance v0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;

    iget v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->effectWidth:I

    iget v2, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->effectHeight:I

    iget-object v3, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->thisSide:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape$SideType;

    iget v4, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->waveRatio0:F

    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;-><init>(IILcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape$SideType;FZ)V

    iput-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->wave0:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;

    .line 151
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->wave0:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;

    iget v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->commonColor:I

    invoke-virtual {v0, v1, v7}, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->setColor(IZ)V

    .line 152
    new-instance v0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;

    iget v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->effectWidth:I

    iget v2, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->effectHeight:I

    sget-object v3, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape$SideType;->Bothside:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape$SideType;

    iget v4, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->waveRatio1:F

    move v5, v7

    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;-><init>(IILcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape$SideType;FZ)V

    iput-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->wave1:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;

    .line 153
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->wave1:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;

    iget v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->commonColor:I

    const v2, 0x3f333333    # 0.7f

    invoke-direct {p0, v1, v2}, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->getColorWithOpacity(IF)I

    move-result v1

    iget v2, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->commonColor:I

    const/high16 v3, 0x3f000000    # 0.5f

    invoke-direct {p0, v2, v3}, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->getColorWithOpacity(IF)I

    move-result v2

    invoke-virtual {v0, v1, v2, v7}, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->setColor(IIZ)V

    .line 155
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->wave1:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;

    sget-object v1, Landroid/graphics/PorterDuff$Mode;->DST_OUT:Landroid/graphics/PorterDuff$Mode;

    iget-object v2, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->theOtherSide:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape$SideType;

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->setPaintMode(Landroid/graphics/PorterDuff$Mode;Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape$SideType;)V

    goto/16 :goto_1

    .line 157
    :cond_2
    new-instance v0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;

    iget v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->effectWidth:I

    iget v2, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->effectHeight:I

    iget-object v3, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->thisSide:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape$SideType;

    iget v4, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->waveRatio0:F

    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;-><init>(IILcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape$SideType;FZ)V

    iput-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->wave0:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;

    .line 158
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->wave0:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;

    iget v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->commonColor:I

    iget v2, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->waveOpacity0:F

    invoke-direct {p0, v1, v2}, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->getColorWithOpacity(IF)I

    move-result v1

    invoke-virtual {v0, v1, v7}, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->setColor(IZ)V

    .line 159
    new-instance v0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;

    iget v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->effectWidth:I

    iget v2, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->effectHeight:I

    iget-object v3, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->thisSide:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape$SideType;

    iget v4, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->waveRatio1:F

    move v5, v7

    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;-><init>(IILcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape$SideType;FZ)V

    iput-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->wave1:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;

    .line 160
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->wave1:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;

    iget v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->commonColor:I

    iget v2, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->waveOpacity1:F

    invoke-direct {p0, v1, v2}, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->getColorWithOpacity(IF)I

    move-result v1

    invoke-virtual {v0, v1, v7}, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->setColor(IZ)V

    goto/16 :goto_1

    .line 139
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private startThread()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 324
    const-string v0, "VisualEffectWaveTextureEffect"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "startThread : isRunning = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->isThreadRunning:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 325
    iget-boolean v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->isThreadRunning:Z

    if-eqz v0, :cond_0

    .line 331
    :goto_0
    return-void

    .line 327
    :cond_0
    iput-boolean v3, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->isThreadRunning:Z

    .line 328
    new-instance v0, Ljava/lang/Thread;

    invoke-direct {v0, p0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->thread:Ljava/lang/Thread;

    .line 329
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->thread:Ljava/lang/Thread;

    invoke-virtual {v0, v3}, Ljava/lang/Thread;->setDaemon(Z)V

    .line 330
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->thread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_0
.end method

.method private stopThread()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 334
    const-string v0, "VisualEffectWaveTextureEffect"

    const-string v1, "stopThread"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 335
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->isThreadRunning:Z

    .line 336
    iput v2, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->elasticDist0:F

    .line 337
    iput v2, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->elasticDist1:F

    .line 338
    iput v2, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->targetWaveHeightRatio:F

    .line 339
    iput v2, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->currentWaveHeightRatio0:F

    .line 340
    iput v2, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->currentWaveHeightRatio1:F

    .line 341
    return-void
.end method

.method private synchronizedDraw()V
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    .line 382
    :try_start_0
    iget-object v6, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->surface:Landroid/graphics/SurfaceTexture;

    invoke-virtual {v6}, Landroid/graphics/SurfaceTexture;->getTimestamp()J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 386
    .local v2, "current":J
    :goto_0
    iget-wide v6, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->prevTimeStamp:J

    sub-long v4, v2, v6

    .line 388
    .local v4, "differ":J
    iput-wide v2, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->prevTimeStamp:J

    .line 390
    cmp-long v6, v2, v8

    if-eqz v6, :cond_0

    cmp-long v6, v4, v8

    if-eqz v6, :cond_1

    .line 391
    :cond_0
    const/4 v0, 0x0

    .line 392
    .local v0, "c":Landroid/graphics/Canvas;
    invoke-virtual {p0}, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->lockCanvas()Landroid/graphics/Canvas;

    move-result-object v0

    .line 393
    invoke-direct {p0, v0}, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->doDraw(Landroid/graphics/Canvas;)V

    .line 394
    if-eqz v0, :cond_1

    invoke-virtual {p0, v0}, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->unlockCanvasAndPost(Landroid/graphics/Canvas;)V

    .line 396
    .end local v0    # "c":Landroid/graphics/Canvas;
    :cond_1
    return-void

    .line 383
    .end local v2    # "current":J
    .end local v4    # "differ":J
    :catch_0
    move-exception v1

    .line 384
    .local v1, "e":Ljava/lang/Exception;
    const-wide/16 v2, 0x0

    .restart local v2    # "current":J
    goto :goto_0
.end method


# virtual methods
.method public addTension(F)V
    .locals 2
    .param p1, "intensity"    # F

    .prologue
    .line 312
    iget v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->targetWaveHeightRatio:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    .line 321
    :goto_0
    return-void

    .line 313
    :cond_0
    const-string v0, "VisualEffectWaveTextureEffect"

    const-string v1, "addTension"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 314
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->isForTension:Z

    .line 316
    iput p1, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->animationValue0:F

    .line 317
    iput p1, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->animationValue1:F

    .line 319
    const/high16 v0, 0x40a00000    # 5.0f

    iput v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->easingValue:F

    .line 320
    const v0, 0x3f4ccccd    # 0.8f

    iput v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->elasticValue:F

    goto :goto_0
.end method

.method public clearEffect()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 297
    const-string v0, "VisualEffectWaveTextureEffect"

    const-string v1, "clearEffect"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 298
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->isYMoving:Z

    .line 299
    iput v2, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->targetWaveHeightRatio:F

    .line 300
    iput v2, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->currentWaveHeightRatio0:F

    .line 301
    iput v2, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->currentWaveHeightRatio1:F

    .line 302
    iput v2, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->elasticDist0:F

    .line 303
    iput v2, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->elasticDist1:F

    .line 304
    iput v2, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->animationValue0:F

    .line 305
    iput v2, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->animationValue1:F

    .line 306
    invoke-direct {p0}, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->getDefaultPercent()F

    move-result v0

    iput v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->currentPercent:F

    .line 307
    invoke-direct {p0}, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->getDefaultPercent()F

    move-result v0

    iput v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->targetPercent:F

    .line 308
    invoke-direct {p0}, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->synchronizedDraw()V

    .line 309
    return-void
.end method

.method public onSurfaceTextureAvailable(Landroid/graphics/SurfaceTexture;II)V
    .locals 2
    .param p1, "surface"    # Landroid/graphics/SurfaceTexture;
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    .line 180
    const-string v0, "VisualEffectWaveTextureEffect"

    const-string v1, "onSurfaceTexture Available"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 181
    iput-object p1, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->surface:Landroid/graphics/SurfaceTexture;

    .line 182
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->isSurfaceCreated:Z

    .line 184
    invoke-direct {p0}, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->synchronizedDraw()V

    .line 186
    iget-boolean v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->isStartEffectCalledBeforeSurfaceCreated:Z

    if-eqz v0, :cond_0

    .line 187
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->isStartEffectCalledBeforeSurfaceCreated:Z

    .line 188
    invoke-virtual {p0}, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->startEffect()V

    .line 190
    :cond_0
    return-void
.end method

.method public onSurfaceTextureDestroyed(Landroid/graphics/SurfaceTexture;)Z
    .locals 5
    .param p1, "surface"    # Landroid/graphics/SurfaceTexture;

    .prologue
    const/4 v4, 0x0

    .line 400
    const-string v2, "VisualEffectWaveTextureEffect"

    const-string v3, "onSurfaceTexture Destroyed"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 401
    iput-boolean v4, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->isSurfaceCreated:Z

    .line 402
    invoke-direct {p0}, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->stopThread()V

    .line 404
    const/4 v1, 0x1

    .line 405
    .local v1, "retry":Z
    :goto_0
    if-eqz v1, :cond_1

    .line 407
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->thread:Ljava/lang/Thread;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->thread:Ljava/lang/Thread;

    invoke-virtual {v2}, Ljava/lang/Thread;->join()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 408
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 409
    :catch_0
    move-exception v0

    .line 410
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->getStackTrace()[Ljava/lang/StackTraceElement;

    goto :goto_0

    .line 413
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :cond_1
    return v4
.end method

.method public onSurfaceTextureSizeChanged(Landroid/graphics/SurfaceTexture;II)V
    .locals 2
    .param p1, "surface"    # Landroid/graphics/SurfaceTexture;
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    .line 368
    const-string v0, "VisualEffectWaveTextureEffect"

    const-string v1, "onSurfaceTexture SizeChanged"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 369
    return-void
.end method

.method public onSurfaceTextureUpdated(Landroid/graphics/SurfaceTexture;)V
    .locals 0
    .param p1, "surface"    # Landroid/graphics/SurfaceTexture;

    .prologue
    .line 418
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 3
    .param p1, "hasWindowFocus"    # Z

    .prologue
    .line 361
    invoke-super {p0, p1}, Landroid/view/TextureView;->onWindowFocusChanged(Z)V

    .line 362
    const-string v0, "VisualEffectWaveTextureEffect"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onWindowFocusChanged : hasWindowFocus = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 363
    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->stopEffect()V

    .line 364
    :cond_0
    return-void
.end method

.method public run()V
    .locals 2

    .prologue
    .line 373
    const-string v0, "VisualEffectWaveTextureEffect"

    const-string v1, "thread run"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 374
    :goto_0
    iget-boolean v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->isThreadRunning:Z

    if-eqz v0, :cond_0

    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v0

    if-nez v0, :cond_0

    .line 375
    invoke-direct {p0}, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->synchronizedDraw()V

    goto :goto_0

    .line 377
    :cond_0
    return-void
.end method

.method public setWaveMaxHeightRatio(F)V
    .locals 3
    .param p1, "ratio"    # F

    .prologue
    .line 260
    const-string v0, "VisualEffectWaveTextureEffect"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setWaveMaxHeightRatio : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 261
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->wave0:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;

    iget v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->waveHeight0:F

    mul-float/2addr v1, p1

    invoke-virtual {v0, v1}, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->setWaveHeight(F)V

    .line 262
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->wave1:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;

    iget v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->waveHeight1:F

    mul-float/2addr v1, p1

    invoke-virtual {v0, v1}, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->setWaveHeight(F)V

    .line 263
    return-void
.end method

.method public setWaveSpeedRatio(F)V
    .locals 3
    .param p1, "ratio"    # F

    .prologue
    .line 266
    const-string v0, "VisualEffectWaveTextureEffect"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setWaveSpeedRatio : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 267
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->wave0:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;

    iget v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->waveSpeed0:F

    mul-float/2addr v1, p1

    invoke-virtual {v0, v1}, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->setSpeed(F)V

    .line 268
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->wave1:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;

    iget v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->waveSpeed1:F

    mul-float/2addr v1, p1

    invoke-virtual {v0, v1}, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->setSpeed(F)V

    .line 269
    return-void
.end method

.method public startEffect()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 272
    iget-boolean v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->isSurfaceCreated:Z

    if-nez v0, :cond_0

    .line 273
    const-string v0, "VisualEffectWaveTextureEffect"

    const-string v1, "startEffect : delayed until surface created"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 274
    iput-boolean v2, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->isStartEffectCalledBeforeSurfaceCreated:Z

    .line 285
    :goto_0
    return-void

    .line 277
    :cond_0
    const-string v0, "VisualEffectWaveTextureEffect"

    const-string v1, "startEffect"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 278
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->targetWaveHeightRatio:F

    .line 279
    const/high16 v0, 0x41a00000    # 20.0f

    iput v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->easingValue:F

    .line 280
    const v0, 0x3e99999a    # 0.3f

    iput v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->elasticValue:F

    .line 281
    iput-boolean v2, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->isYMoving:Z

    .line 282
    const/high16 v0, 0x42480000    # 50.0f

    iput v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->targetPercent:F

    .line 284
    invoke-direct {p0}, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->startThread()V

    goto :goto_0
.end method

.method public stopEffect()V
    .locals 2

    .prologue
    .line 288
    const-string v0, "VisualEffectWaveTextureEffect"

    const-string v1, "stopEffect"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 289
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->targetWaveHeightRatio:F

    .line 290
    const/high16 v0, 0x41a00000    # 20.0f

    iput v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->easingValue:F

    .line 291
    const v0, 0x3f5eb852    # 0.87f

    iput v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->elasticValue:F

    .line 292
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->isYMoving:Z

    .line 293
    invoke-direct {p0}, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->getDefaultPercent()F

    move-result v0

    iput v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveTextureEffect;->targetPercent:F

    .line 294
    return-void
.end method
