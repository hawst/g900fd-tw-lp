.class public Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;
.super Landroid/view/SurfaceView;
.source "WaveSurfaceEffect.java"

# interfaces
.implements Landroid/view/SurfaceHolder$Callback;
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect$VerticalAlign;
    }
.end annotation


# instance fields
.field private final ANIMATION_STOP_VALUE:F

.field private final TAG:Ljava/lang/String;

.field private animationValue0:F

.field private animationValue1:F

.field private bottomColor:I

.field private centerColor:I

.field private currentPercent:F

.field private currentWaveHeightRatio0:F

.field private currentWaveHeightRatio1:F

.field private easingValue:F

.field private effectHeight:I

.field private effectWidth:I

.field private elasticDist0:F

.field private elasticDist1:F

.field private elasticValue:F

.field private isForTension:Z

.field private isStartEffectCalledBeforeSurfaceCreated:Z

.field private isSurfaceCreated:Z

.field private isThreadRunning:Z

.field private isYMoving:Z

.field private surfaceHolder:Landroid/view/SurfaceHolder;

.field private targetPercent:F

.field private targetWaveHeightRatio:F

.field private thread:Ljava/lang/Thread;

.field private topColor:I

.field private verticalAlign:Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect$VerticalAlign;

.field private wave0:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;

.field private wave1:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;

.field private waveHeight0:F

.field private waveHeight1:F

.field private waveRatio0:F

.field private waveRatio1:F

.field private waveSpeed0:F

.field private waveSpeed1:F


# direct methods
.method public constructor <init>(Landroid/content/Context;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect$VerticalAlign;F)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "width"    # I
    .param p3, "height"    # I
    .param p4, "topColor"    # Ljava/lang/String;
    .param p5, "centerColor"    # Ljava/lang/String;
    .param p6, "bottomColor"    # Ljava/lang/String;
    .param p7, "align"    # Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect$VerticalAlign;
    .param p8, "waveWidthRatio"    # F

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 61
    invoke-direct {p0, p1}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;)V

    .line 18
    const-string v0, "VisualEffectWaveSurfaceEffect"

    iput-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->TAG:Ljava/lang/String;

    .line 20
    const v0, 0x3727c5ac    # 1.0E-5f

    iput v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->ANIMATION_STOP_VALUE:F

    .line 24
    const v0, -0x9f7a69

    iput v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->topColor:I

    .line 25
    const v0, -0x1c232a

    iput v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->bottomColor:I

    .line 26
    const v0, -0x502446

    iput v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->centerColor:I

    .line 27
    iput v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->elasticDist0:F

    .line 28
    iput v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->elasticDist1:F

    .line 29
    const/high16 v0, 0x40a00000    # 5.0f

    iput v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->easingValue:F

    .line 30
    const v0, 0x3f19999a    # 0.6f

    iput v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->waveRatio0:F

    .line 31
    const v0, 0x3f333333    # 0.7f

    iput v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->waveRatio1:F

    .line 32
    const v0, 0x3f666666    # 0.9f

    iput v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->elasticValue:F

    .line 33
    const/high16 v0, 0x41600000    # 14.0f

    iput v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->waveSpeed0:F

    .line 34
    const/high16 v0, 0x41b00000    # 22.0f

    iput v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->waveSpeed1:F

    .line 35
    const/high16 v0, 0x42a00000    # 80.0f

    iput v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->waveHeight0:F

    .line 36
    const/high16 v0, 0x42b40000    # 90.0f

    iput v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->waveHeight1:F

    .line 37
    iput v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->targetPercent:F

    .line 38
    iput v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->currentPercent:F

    .line 39
    iput v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->animationValue0:F

    .line 40
    iput v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->animationValue1:F

    .line 41
    iput v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->targetWaveHeightRatio:F

    .line 42
    iput v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->currentWaveHeightRatio0:F

    .line 43
    iput v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->currentWaveHeightRatio1:F

    .line 44
    iput-boolean v2, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->isYMoving:Z

    .line 45
    iput-boolean v2, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->isForTension:Z

    .line 46
    iput-boolean v2, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->isThreadRunning:Z

    .line 47
    iput-boolean v2, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->isSurfaceCreated:Z

    .line 48
    iput-boolean v2, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->isStartEffectCalledBeforeSurfaceCreated:Z

    .line 62
    const-string v0, "VisualEffectWaveSurfaceEffect"

    const-string v1, "WaveSurfaceEffect constructor"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 64
    iput p2, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->effectWidth:I

    .line 65
    iput p3, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->effectHeight:I

    .line 66
    invoke-static {p4}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->topColor:I

    .line 67
    invoke-static {p5}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->centerColor:I

    .line 68
    invoke-static {p6}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->bottomColor:I

    .line 69
    iput-object p7, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->verticalAlign:Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect$VerticalAlign;

    .line 70
    iget v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->waveRatio0:F

    mul-float/2addr v0, p8

    iput v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->waveRatio0:F

    .line 71
    iget v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->waveRatio1:F

    mul-float/2addr v0, p8

    iput v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->waveRatio1:F

    .line 73
    const-string v0, "VisualEffectWaveSurfaceEffect"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setInitValues : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->effectWidth:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " x "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->effectHeight:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 74
    const-string v0, "VisualEffectWaveSurfaceEffect"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setInitValues - top : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->topColor:I

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", center : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->centerColor:I

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", bottom : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->bottomColor:I

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 77
    const-string v0, "VisualEffectWaveSurfaceEffect"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setInitValues - verticalAlign : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->verticalAlign:Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect$VerticalAlign;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 78
    const-string v0, "VisualEffectWaveSurfaceEffect"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setInitValues - waveWidthRatio : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p8}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 80
    invoke-direct {p0}, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->initEffect()V

    .line 81
    return-void
.end method

.method private doDraw(Landroid/graphics/Canvas;)V
    .locals 7
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/high16 v6, 0x40000000    # 2.0f

    const/high16 v5, 0x41200000    # 10.0f

    const/4 v3, 0x0

    const v4, 0x3727c5ac    # 1.0E-5f

    .line 125
    if-nez p1, :cond_0

    .line 173
    :goto_0
    return-void

    .line 127
    :cond_0
    iget v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->effectWidth:I

    iget v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->effectHeight:I

    invoke-virtual {p1, v3, v3, v0, v1}, Landroid/graphics/Canvas;->clipRect(IIII)Z

    .line 129
    iget-boolean v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->isYMoving:Z

    if-eqz v0, :cond_1

    .line 130
    iget v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->currentPercent:F

    iget v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->targetPercent:F

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    const v1, 0x3e99999a    # 0.3f

    cmpg-float v0, v0, v1

    if-gez v0, :cond_5

    .line 131
    iget v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->targetPercent:F

    iput v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->currentPercent:F

    .line 132
    iput-boolean v3, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->isYMoving:Z

    .line 136
    :goto_1
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->wave0:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;

    iget v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->currentPercent:F

    invoke-virtual {v0, v1}, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->setPercent(F)V

    .line 137
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->wave1:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;

    iget v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->currentPercent:F

    invoke-virtual {v0, v1}, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->setPercent(F)V

    .line 140
    :cond_1
    iget-boolean v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->isForTension:Z

    if-eqz v0, :cond_3

    .line 141
    iget v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->animationValue0:F

    iget v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->animationValue0:F

    const/high16 v2, 0x41a00000    # 20.0f

    div-float/2addr v1, v2

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->animationValue0:F

    .line 142
    iget v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->animationValue1:F

    iget v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->animationValue1:F

    div-float/2addr v1, v5

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->animationValue1:F

    .line 143
    iget v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->animationValue0:F

    cmpg-float v0, v0, v4

    if-gez v0, :cond_2

    iget v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->animationValue1:F

    cmpg-float v0, v0, v4

    if-gez v0, :cond_2

    .line 144
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->animationValue1:F

    iput v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->animationValue0:F

    .line 145
    iput-boolean v3, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->isForTension:Z

    .line 146
    const-string v0, "VisualEffectWaveSurfaceEffect"

    const-string v1, "stopTention"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 148
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->wave0:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;

    iget v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->waveSpeed0:F

    iget v2, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->animationValue0:F

    const/high16 v3, 0x41700000    # 15.0f

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->setSpeed(F)V

    .line 149
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->wave1:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;

    iget v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->waveSpeed1:F

    iget v2, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->animationValue1:F

    mul-float/2addr v2, v5

    add-float/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->setSpeed(F)V

    .line 152
    :cond_3
    iget v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->targetWaveHeightRatio:F

    iget v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->animationValue0:F

    div-float/2addr v1, v6

    add-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->currentWaveHeightRatio0:F

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->easingValue:F

    div-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->elasticDist0:F

    iget v2, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->elasticValue:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    iput v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->elasticDist0:F

    .line 153
    iget v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->currentWaveHeightRatio0:F

    iget v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->elasticDist0:F

    add-float/2addr v0, v1

    iput v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->currentWaveHeightRatio0:F

    .line 154
    iget v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->targetWaveHeightRatio:F

    iget v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->animationValue1:F

    div-float/2addr v1, v6

    add-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->currentWaveHeightRatio1:F

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->easingValue:F

    div-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->elasticDist1:F

    iget v2, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->elasticValue:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    iput v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->elasticDist1:F

    .line 155
    iget v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->currentWaveHeightRatio1:F

    iget v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->elasticDist1:F

    add-float/2addr v0, v1

    iput v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->currentWaveHeightRatio1:F

    .line 157
    iget-boolean v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->isThreadRunning:Z

    if-eqz v0, :cond_4

    iget-boolean v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->isYMoving:Z

    if-nez v0, :cond_4

    iget v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->currentWaveHeightRatio0:F

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    cmpg-float v0, v0, v4

    if-gtz v0, :cond_4

    iget v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->currentWaveHeightRatio1:F

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    cmpg-float v0, v0, v4

    if-gtz v0, :cond_4

    .line 159
    invoke-direct {p0}, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->stopThread()V

    .line 162
    :cond_4
    iget v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->bottomColor:I

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 164
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->wave0:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;

    iget v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->currentWaveHeightRatio0:F

    invoke-virtual {v0, v1}, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->setWaveHeightRatio(F)V

    .line 165
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->wave1:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;

    iget v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->currentWaveHeightRatio1:F

    invoke-virtual {v0, v1}, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->setWaveHeightRatio(F)V

    .line 167
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->wave0:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;

    invoke-virtual {v0, p1}, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->calculatePoints(Landroid/graphics/Canvas;)V

    .line 168
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->wave1:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;

    invoke-virtual {v0, p1}, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->calculatePoints(Landroid/graphics/Canvas;)V

    .line 170
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->wave0:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;

    sget-object v1, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape$SideType;->Upside:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape$SideType;

    invoke-virtual {v0, p1, v1}, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->drawWave(Landroid/graphics/Canvas;Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape$SideType;)V

    .line 171
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->wave1:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;

    sget-object v1, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape$SideType;->Upside:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape$SideType;

    invoke-virtual {v0, p1, v1}, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->drawWave(Landroid/graphics/Canvas;Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape$SideType;)V

    .line 172
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->wave0:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;

    sget-object v1, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape$SideType;->Downside:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape$SideType;

    invoke-virtual {v0, p1, v1}, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->drawWave(Landroid/graphics/Canvas;Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape$SideType;)V

    goto/16 :goto_0

    .line 134
    :cond_5
    iget v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->currentPercent:F

    iget v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->targetPercent:F

    iget v2, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->currentPercent:F

    sub-float/2addr v1, v2

    div-float/2addr v1, v5

    add-float/2addr v0, v1

    iput v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->currentPercent:F

    goto/16 :goto_1
.end method

.method private getDefaultPercent()F
    .locals 2

    .prologue
    .line 260
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->verticalAlign:Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect$VerticalAlign;

    sget-object v1, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect$VerticalAlign;->TOP:Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect$VerticalAlign;

    if-ne v0, v1, :cond_0

    const/high16 v0, 0x42c80000    # 100.0f

    .line 262
    :goto_0
    return v0

    .line 261
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->verticalAlign:Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect$VerticalAlign;

    sget-object v1, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect$VerticalAlign;->BOTTOM:Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect$VerticalAlign;

    if-ne v0, v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 262
    :cond_1
    const/high16 v0, 0x42480000    # 50.0f

    goto :goto_0
.end method

.method private initEffect()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/high16 v3, 0x44b40000    # 1440.0f

    const/high16 v2, 0x40000000    # 2.0f

    const/4 v7, 0x0

    const/4 v5, 0x1

    .line 84
    invoke-virtual {p0}, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->surfaceHolder:Landroid/view/SurfaceHolder;

    .line 85
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->surfaceHolder:Landroid/view/SurfaceHolder;

    invoke-interface {v0, v5}, Landroid/view/SurfaceHolder;->setFormat(I)V

    .line 86
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->surfaceHolder:Landroid/view/SurfaceHolder;

    invoke-interface {v0, p0}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    .line 88
    iget v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->waveSpeed0:F

    iget v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->effectWidth:I

    int-to-float v1, v1

    div-float/2addr v1, v3

    mul-float/2addr v0, v1

    iput v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->waveSpeed0:F

    .line 89
    iget v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->waveSpeed1:F

    iget v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->effectWidth:I

    int-to-float v1, v1

    div-float/2addr v1, v3

    mul-float/2addr v0, v1

    iput v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->waveSpeed1:F

    .line 90
    iget v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->effectHeight:I

    int-to-float v0, v0

    div-float/2addr v0, v2

    iput v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->waveHeight0:F

    .line 91
    iget v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->effectHeight:I

    int-to-float v0, v0

    div-float/2addr v0, v2

    const v1, 0x3f666666    # 0.9f

    mul-float/2addr v0, v1

    iput v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->waveHeight1:F

    .line 93
    new-instance v0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;

    iget v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->effectWidth:I

    iget v2, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->effectHeight:I

    sget-object v3, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape$SideType;->Bothside:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape$SideType;

    iget v4, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->waveRatio0:F

    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;-><init>(IILcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape$SideType;FZ)V

    iput-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->wave0:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;

    .line 94
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->wave0:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;

    iget v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->centerColor:I

    iget v2, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->bottomColor:I

    invoke-virtual {v0, v1, v2, v5}, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->setColor(IIZ)V

    .line 95
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->wave0:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;

    iget v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->waveSpeed0:F

    invoke-virtual {v0, v1}, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->setSpeed(F)V

    .line 96
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->wave0:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;

    iget v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->waveHeight0:F

    invoke-virtual {v0, v1}, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->setWaveHeight(F)V

    .line 97
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->wave0:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;

    invoke-virtual {v0, v7}, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->setWaveHeightRatio(F)V

    .line 99
    new-instance v0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;

    iget v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->effectWidth:I

    iget v2, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->effectHeight:I

    sget-object v3, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape$SideType;->Upside:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape$SideType;

    iget v4, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->waveRatio1:F

    move v5, v8

    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;-><init>(IILcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape$SideType;FZ)V

    iput-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->wave1:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;

    .line 100
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->wave1:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;

    iget v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->topColor:I

    invoke-virtual {v0, v1, v8}, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->setColor(IZ)V

    .line 101
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->wave1:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;

    iget v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->waveSpeed1:F

    invoke-virtual {v0, v1}, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->setSpeed(F)V

    .line 102
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->wave1:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;

    iget v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->waveHeight1:F

    invoke-virtual {v0, v1}, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->setWaveHeight(F)V

    .line 103
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->wave1:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;

    invoke-virtual {v0, v7}, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->setWaveHeightRatio(F)V

    .line 105
    invoke-direct {p0}, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->getDefaultPercent()F

    move-result v6

    .line 106
    .local v6, "percent":F
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->wave0:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;

    invoke-virtual {v0, v6}, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->setPercent(F)V

    .line 107
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->wave1:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;

    invoke-virtual {v0, v6}, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->setPercent(F)V

    .line 108
    iput v6, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->currentPercent:F

    .line 109
    return-void
.end method

.method private startThread()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 240
    const-string v0, "VisualEffectWaveSurfaceEffect"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "startThread : isRunning = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->isThreadRunning:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 241
    iget-boolean v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->isThreadRunning:Z

    if-eqz v0, :cond_0

    .line 247
    :goto_0
    return-void

    .line 243
    :cond_0
    iput-boolean v3, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->isThreadRunning:Z

    .line 244
    new-instance v0, Ljava/lang/Thread;

    invoke-direct {v0, p0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->thread:Ljava/lang/Thread;

    .line 245
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->thread:Ljava/lang/Thread;

    invoke-virtual {v0, v3}, Ljava/lang/Thread;->setDaemon(Z)V

    .line 246
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->thread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_0
.end method

.method private stopThread()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 250
    const-string v0, "VisualEffectWaveSurfaceEffect"

    const-string v1, "stopThread"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 251
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->isThreadRunning:Z

    .line 252
    iput v2, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->elasticDist0:F

    .line 253
    iput v2, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->elasticDist1:F

    .line 254
    iput v2, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->targetWaveHeightRatio:F

    .line 255
    iput v2, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->currentWaveHeightRatio0:F

    .line 256
    iput v2, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->currentWaveHeightRatio1:F

    .line 257
    return-void
.end method

.method private synchronizedDraw()V
    .locals 3

    .prologue
    .line 286
    const/4 v0, 0x0

    .line 287
    .local v0, "c":Landroid/graphics/Canvas;
    iget-object v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->surfaceHolder:Landroid/view/SurfaceHolder;

    invoke-interface {v1}, Landroid/view/SurfaceHolder;->lockCanvas()Landroid/graphics/Canvas;

    move-result-object v0

    .line 288
    iget-object v2, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->surfaceHolder:Landroid/view/SurfaceHolder;

    monitor-enter v2

    .line 289
    :try_start_0
    invoke-direct {p0, v0}, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->doDraw(Landroid/graphics/Canvas;)V

    .line 290
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 291
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->surfaceHolder:Landroid/view/SurfaceHolder;

    invoke-interface {v1, v0}, Landroid/view/SurfaceHolder;->unlockCanvasAndPost(Landroid/graphics/Canvas;)V

    .line 292
    :cond_0
    return-void

    .line 290
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method


# virtual methods
.method public addTension(F)V
    .locals 2
    .param p1, "intensity"    # F

    .prologue
    .line 228
    iget v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->targetWaveHeightRatio:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    .line 237
    :goto_0
    return-void

    .line 229
    :cond_0
    const-string v0, "VisualEffectWaveSurfaceEffect"

    const-string v1, "addTension"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 230
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->isForTension:Z

    .line 232
    iput p1, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->animationValue0:F

    .line 233
    iput p1, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->animationValue1:F

    .line 235
    const/high16 v0, 0x40a00000    # 5.0f

    iput v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->easingValue:F

    .line 236
    const v0, 0x3f4ccccd    # 0.8f

    iput v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->elasticValue:F

    goto :goto_0
.end method

.method public clearEffect()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 213
    const-string v0, "VisualEffectWaveSurfaceEffect"

    const-string v1, "clearEffect"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 214
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->isYMoving:Z

    .line 215
    iput v2, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->targetWaveHeightRatio:F

    .line 216
    iput v2, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->currentWaveHeightRatio0:F

    .line 217
    iput v2, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->currentWaveHeightRatio1:F

    .line 218
    iput v2, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->elasticDist0:F

    .line 219
    iput v2, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->elasticDist1:F

    .line 220
    iput v2, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->animationValue0:F

    .line 221
    iput v2, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->animationValue1:F

    .line 222
    invoke-direct {p0}, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->getDefaultPercent()F

    move-result v0

    iput v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->currentPercent:F

    .line 223
    invoke-direct {p0}, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->getDefaultPercent()F

    move-result v0

    iput v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->targetPercent:F

    .line 224
    invoke-direct {p0}, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->synchronizedDraw()V

    .line 225
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 3
    .param p1, "hasWindowFocus"    # Z

    .prologue
    .line 267
    invoke-super {p0, p1}, Landroid/view/SurfaceView;->onWindowFocusChanged(Z)V

    .line 268
    const-string v0, "VisualEffectWaveSurfaceEffect"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onWindowFocusChanged : hasWindowFocus = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 269
    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->stopEffect()V

    .line 270
    :cond_0
    return-void
.end method

.method public run()V
    .locals 2

    .prologue
    .line 279
    const-string v0, "VisualEffectWaveSurfaceEffect"

    const-string v1, "thread run"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 280
    :goto_0
    iget-boolean v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->isThreadRunning:Z

    if-eqz v0, :cond_0

    .line 281
    invoke-direct {p0}, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->synchronizedDraw()V

    goto :goto_0

    .line 283
    :cond_0
    return-void
.end method

.method public setWaveMaxHeightRatio(F)V
    .locals 3
    .param p1, "ratio"    # F

    .prologue
    .line 176
    const-string v0, "VisualEffectWaveSurfaceEffect"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setWaveMaxHeightRatio : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 177
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->wave0:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;

    iget v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->waveHeight0:F

    mul-float/2addr v1, p1

    invoke-virtual {v0, v1}, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->setWaveHeight(F)V

    .line 178
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->wave1:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;

    iget v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->waveHeight1:F

    mul-float/2addr v1, p1

    invoke-virtual {v0, v1}, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->setWaveHeight(F)V

    .line 179
    return-void
.end method

.method public setWaveSpeedRatio(F)V
    .locals 3
    .param p1, "ratio"    # F

    .prologue
    .line 182
    const-string v0, "VisualEffectWaveSurfaceEffect"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setWaveSpeedRatio : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 183
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->wave0:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;

    iget v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->waveSpeed0:F

    mul-float/2addr v1, p1

    invoke-virtual {v0, v1}, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->setSpeed(F)V

    .line 184
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->wave1:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;

    iget v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->waveSpeed1:F

    mul-float/2addr v1, p1

    invoke-virtual {v0, v1}, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->setSpeed(F)V

    .line 185
    return-void
.end method

.method public startEffect()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 188
    iget-boolean v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->isSurfaceCreated:Z

    if-nez v0, :cond_0

    .line 189
    const-string v0, "VisualEffectWaveSurfaceEffect"

    const-string v1, "startEffect : delayed until surface created"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 190
    iput-boolean v2, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->isStartEffectCalledBeforeSurfaceCreated:Z

    .line 201
    :goto_0
    return-void

    .line 193
    :cond_0
    const-string v0, "VisualEffectWaveSurfaceEffect"

    const-string v1, "startEffect"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 194
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->targetWaveHeightRatio:F

    .line 195
    const/high16 v0, 0x41a00000    # 20.0f

    iput v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->easingValue:F

    .line 196
    const v0, 0x3e99999a    # 0.3f

    iput v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->elasticValue:F

    .line 197
    iput-boolean v2, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->isYMoving:Z

    .line 198
    const/high16 v0, 0x42480000    # 50.0f

    iput v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->targetPercent:F

    .line 200
    invoke-direct {p0}, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->startThread()V

    goto :goto_0
.end method

.method public stopEffect()V
    .locals 2

    .prologue
    .line 204
    const-string v0, "VisualEffectWaveSurfaceEffect"

    const-string v1, "stopEffect"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 205
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->targetWaveHeightRatio:F

    .line 206
    const/high16 v0, 0x41a00000    # 20.0f

    iput v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->easingValue:F

    .line 207
    const v0, 0x3f5eb852    # 0.87f

    iput v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->elasticValue:F

    .line 208
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->isYMoving:Z

    .line 209
    invoke-direct {p0}, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->getDefaultPercent()F

    move-result v0

    iput v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->targetPercent:F

    .line 210
    return-void
.end method

.method public surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 2
    .param p1, "holder"    # Landroid/view/SurfaceHolder;
    .param p2, "format"    # I
    .param p3, "width"    # I
    .param p4, "height"    # I

    .prologue
    .line 274
    const-string v0, "VisualEffectWaveSurfaceEffect"

    const-string v1, "surfaceChanged"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 275
    return-void
.end method

.method public surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 2
    .param p1, "holder"    # Landroid/view/SurfaceHolder;

    .prologue
    .line 113
    const-string v0, "VisualEffectWaveSurfaceEffect"

    const-string v1, "surfaceCreated"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 114
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->isSurfaceCreated:Z

    .line 116
    invoke-direct {p0}, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->synchronizedDraw()V

    .line 118
    iget-boolean v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->isStartEffectCalledBeforeSurfaceCreated:Z

    if-eqz v0, :cond_0

    .line 119
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->isStartEffectCalledBeforeSurfaceCreated:Z

    .line 120
    invoke-virtual {p0}, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->startEffect()V

    .line 122
    :cond_0
    return-void
.end method

.method public surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 4
    .param p1, "holder"    # Landroid/view/SurfaceHolder;

    .prologue
    .line 296
    const-string v2, "VisualEffectWaveSurfaceEffect"

    const-string v3, "surfaceDestroyed"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 297
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->isSurfaceCreated:Z

    .line 298
    invoke-direct {p0}, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->stopThread()V

    .line 300
    const/4 v1, 0x1

    .line 301
    .local v1, "retry":Z
    :goto_0
    if-eqz v1, :cond_1

    .line 303
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->thread:Ljava/lang/Thread;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/android/visualeffect/naturalcurve/WaveSurfaceEffect;->thread:Ljava/lang/Thread;

    invoke-virtual {v2}, Ljava/lang/Thread;->join()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 304
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 305
    :catch_0
    move-exception v0

    .line 306
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->getStackTrace()[Ljava/lang/StackTraceElement;

    goto :goto_0

    .line 309
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :cond_1
    return-void
.end method
