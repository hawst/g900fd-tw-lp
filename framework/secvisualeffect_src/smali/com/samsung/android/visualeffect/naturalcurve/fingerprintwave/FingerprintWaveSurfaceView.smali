.class public Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;
.super Landroid/view/SurfaceView;
.source "FingerprintWaveSurfaceView.java"

# interfaces
.implements Landroid/view/SurfaceHolder$Callback;
.implements Ljava/lang/Runnable;


# instance fields
.field private final ANIMATION_STOP_VALUE:F

.field private final TAG:Ljava/lang/String;

.field private animationValue:F

.field private batteryHeight:I

.field private batteryWidth:I

.field private batteryX:I

.field private batteryY:I

.field private bgColor:I

.field private colorDownside:I

.field private colorUpside:I

.field private currentPercent:F

.field private currentWaveHeightRatio:F

.field private easingValue:F

.field private elasticDist:F

.field private elasticValue:F

.field private isThreadRunning:Z

.field private isYMoving:Z

.field private surfaceHolder:Landroid/view/SurfaceHolder;

.field private targetPercent:F

.field private targetWaveHeightRatio:F

.field private thread:Ljava/lang/Thread;

.field private wave:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;

.field private waveHeight:F

.field private waveSpeed:F


# direct methods
.method public constructor <init>(Landroid/content/Context;IIII)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "batteryWidth"    # I
    .param p3, "batteryHeight"    # I
    .param p4, "batteryX"    # I
    .param p5, "batteryY"    # I

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 44
    invoke-direct {p0, p1}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;)V

    .line 17
    const-string v0, "VisualEffectBatteryEffect"

    iput-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;->TAG:Ljava/lang/String;

    .line 19
    const v0, 0x3727c5ac    # 1.0E-5f

    iput v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;->ANIMATION_STOP_VALUE:F

    .line 21
    const v0, -0x50506

    iput v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;->bgColor:I

    .line 22
    const v0, -0x9f7a69

    iput v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;->colorUpside:I

    .line 23
    const v0, -0x1c232a

    iput v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;->colorDownside:I

    .line 26
    iput v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;->elasticDist:F

    .line 27
    const/high16 v0, 0x40a00000    # 5.0f

    iput v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;->easingValue:F

    .line 28
    const v0, 0x3f666666    # 0.9f

    iput v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;->elasticValue:F

    .line 29
    const/high16 v0, 0x41880000    # 17.0f

    iput v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;->waveSpeed:F

    .line 30
    const/high16 v0, 0x41f00000    # 30.0f

    iput v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;->waveHeight:F

    .line 31
    iput v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;->targetPercent:F

    .line 32
    iput v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;->currentPercent:F

    .line 33
    iput v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;->animationValue:F

    .line 34
    iput v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;->targetWaveHeightRatio:F

    .line 35
    iput v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;->currentWaveHeightRatio:F

    .line 36
    iput-boolean v2, p0, Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;->isYMoving:Z

    .line 37
    iput-boolean v2, p0, Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;->isThreadRunning:Z

    .line 45
    const-string v0, "VisualEffectBatteryEffect"

    const-string v1, "BatteryEffectSurfaceView constructor"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 47
    iput p2, p0, Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;->batteryWidth:I

    .line 48
    iput p3, p0, Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;->batteryHeight:I

    .line 49
    iput p4, p0, Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;->batteryX:I

    .line 50
    iput p5, p0, Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;->batteryY:I

    .line 51
    iget v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;->waveSpeed:F

    int-to-float v1, p2

    mul-float/2addr v0, v1

    const/high16 v1, 0x43fa0000    # 500.0f

    div-float/2addr v0, v1

    iput v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;->waveSpeed:F

    .line 52
    iget v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;->waveHeight:F

    int-to-float v1, p3

    mul-float/2addr v0, v1

    const/high16 v1, 0x443f0000    # 764.0f

    div-float/2addr v0, v1

    iput v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;->waveHeight:F

    .line 54
    invoke-virtual {p0}, Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;->surfaceHolder:Landroid/view/SurfaceHolder;

    .line 55
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;->surfaceHolder:Landroid/view/SurfaceHolder;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/view/SurfaceHolder;->setFormat(I)V

    .line 56
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;->surfaceHolder:Landroid/view/SurfaceHolder;

    invoke-interface {v0, p0}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    .line 57
    return-void
.end method

.method private doDraw(Landroid/graphics/Canvas;)V
    .locals 7
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 78
    if-nez p1, :cond_0

    .line 110
    :goto_0
    return-void

    .line 80
    :cond_0
    iget-boolean v4, p0, Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;->isYMoving:Z

    if-eqz v4, :cond_1

    .line 81
    iget v4, p0, Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;->currentPercent:F

    iget v5, p0, Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;->targetPercent:F

    sub-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    const v5, 0x3e99999a    # 0.3f

    cmpg-float v4, v4, v5

    if-gez v4, :cond_3

    .line 82
    iget v4, p0, Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;->targetPercent:F

    iput v4, p0, Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;->currentPercent:F

    .line 83
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;->isYMoving:Z

    .line 84
    invoke-direct {p0}, Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;->stopEffect()V

    .line 88
    :goto_1
    iget-object v4, p0, Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;->wave:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;

    iget v5, p0, Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;->currentPercent:F

    invoke-virtual {v4, v5}, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->setPercent(F)V

    .line 91
    :cond_1
    iget v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;->batteryX:I

    .line 92
    .local v1, "left":I
    iget v3, p0, Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;->batteryY:I

    .line 93
    .local v3, "top":I
    iget v4, p0, Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;->batteryWidth:I

    add-int v2, v1, v4

    .line 94
    .local v2, "right":I
    iget v4, p0, Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;->batteryHeight:I

    add-int v0, v3, v4

    .line 96
    .local v0, "bottom":I
    invoke-virtual {p1, v1, v3, v2, v0}, Landroid/graphics/Canvas;->clipRect(IIII)Z

    .line 98
    iget v4, p0, Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;->targetWaveHeightRatio:F

    iget v5, p0, Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;->animationValue:F

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v5, v6

    add-float/2addr v4, v5

    iget v5, p0, Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;->currentWaveHeightRatio:F

    sub-float/2addr v4, v5

    iget v5, p0, Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;->easingValue:F

    div-float/2addr v4, v5

    iget v5, p0, Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;->elasticDist:F

    iget v6, p0, Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;->elasticValue:F

    mul-float/2addr v5, v6

    add-float/2addr v4, v5

    iput v4, p0, Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;->elasticDist:F

    .line 99
    iget v4, p0, Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;->currentWaveHeightRatio:F

    iget v5, p0, Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;->elasticDist:F

    add-float/2addr v4, v5

    iput v4, p0, Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;->currentWaveHeightRatio:F

    .line 101
    iget-boolean v4, p0, Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;->isThreadRunning:Z

    if-eqz v4, :cond_2

    iget v4, p0, Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;->currentWaveHeightRatio:F

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    const v5, 0x3727c5ac    # 1.0E-5f

    cmpg-float v4, v4, v5

    if-gtz v4, :cond_2

    .line 102
    invoke-direct {p0}, Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;->stopThread()V

    .line 105
    :cond_2
    iget v4, p0, Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;->colorDownside:I

    invoke-virtual {p1, v4}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 107
    iget-object v4, p0, Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;->wave:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;

    iget v5, p0, Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;->currentWaveHeightRatio:F

    invoke-virtual {v4, v5}, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->setWaveHeightRatio(F)V

    .line 108
    iget-object v4, p0, Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;->wave:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;

    invoke-virtual {v4, p1}, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->calculatePoints(Landroid/graphics/Canvas;)V

    .line 109
    iget-object v4, p0, Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;->wave:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;

    sget-object v5, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape$SideType;->Upside:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape$SideType;

    invoke-virtual {v4, p1, v5}, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->drawWave(Landroid/graphics/Canvas;Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape$SideType;)V

    goto :goto_0

    .line 86
    .end local v0    # "bottom":I
    .end local v1    # "left":I
    .end local v2    # "right":I
    .end local v3    # "top":I
    :cond_3
    iget v4, p0, Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;->currentPercent:F

    iget v5, p0, Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;->targetPercent:F

    iget v6, p0, Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;->currentPercent:F

    sub-float/2addr v5, v6

    const/high16 v6, 0x41a00000    # 20.0f

    div-float/2addr v5, v6

    add-float/2addr v4, v5

    iput v4, p0, Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;->currentPercent:F

    goto :goto_1
.end method

.method private startEffect()V
    .locals 2

    .prologue
    .line 129
    const-string v0, "VisualEffectBatteryEffect"

    const-string v1, "startEffect"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 130
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;->targetWaveHeightRatio:F

    .line 131
    const/high16 v0, 0x41a00000    # 20.0f

    iput v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;->easingValue:F

    .line 132
    const v0, 0x3e99999a    # 0.3f

    iput v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;->elasticValue:F

    .line 133
    invoke-direct {p0}, Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;->startThread()V

    .line 134
    return-void
.end method

.method private startThread()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 144
    const-string v0, "VisualEffectBatteryEffect"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "startThread : isRunning = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;->isThreadRunning:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 145
    iget-boolean v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;->isThreadRunning:Z

    if-eqz v0, :cond_0

    .line 151
    :goto_0
    return-void

    .line 147
    :cond_0
    iput-boolean v3, p0, Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;->isThreadRunning:Z

    .line 148
    new-instance v0, Ljava/lang/Thread;

    invoke-direct {v0, p0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;->thread:Ljava/lang/Thread;

    .line 149
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;->thread:Ljava/lang/Thread;

    invoke-virtual {v0, v3}, Ljava/lang/Thread;->setDaemon(Z)V

    .line 150
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;->thread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_0
.end method

.method private stopEffect()V
    .locals 2

    .prologue
    .line 137
    const-string v0, "VisualEffectBatteryEffect"

    const-string v1, "stopEffect"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 138
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;->targetWaveHeightRatio:F

    .line 139
    const/high16 v0, 0x42200000    # 40.0f

    iput v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;->easingValue:F

    .line 140
    const v0, 0x3f5eb852    # 0.87f

    iput v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;->elasticValue:F

    .line 141
    return-void
.end method

.method private stopThread()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 154
    const-string v0, "VisualEffectBatteryEffect"

    const-string v1, "stopThread"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 155
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;->isThreadRunning:Z

    .line 156
    iput v2, p0, Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;->elasticDist:F

    .line 157
    iput v2, p0, Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;->targetWaveHeightRatio:F

    .line 158
    iput v2, p0, Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;->currentWaveHeightRatio:F

    .line 159
    return-void
.end method

.method private synchronizedDraw()V
    .locals 3

    .prologue
    .line 198
    const/4 v0, 0x0

    .line 199
    .local v0, "c":Landroid/graphics/Canvas;
    iget-object v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;->surfaceHolder:Landroid/view/SurfaceHolder;

    invoke-interface {v1}, Landroid/view/SurfaceHolder;->lockCanvas()Landroid/graphics/Canvas;

    move-result-object v0

    .line 200
    iget-object v2, p0, Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;->surfaceHolder:Landroid/view/SurfaceHolder;

    monitor-enter v2

    .line 201
    :try_start_0
    invoke-direct {p0, v0}, Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;->doDraw(Landroid/graphics/Canvas;)V

    .line 202
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 203
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;->surfaceHolder:Landroid/view/SurfaceHolder;

    invoke-interface {v1, v0}, Landroid/view/SurfaceHolder;->unlockCanvasAndPost(Landroid/graphics/Canvas;)V

    .line 204
    :cond_0
    return-void

    .line 202
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method


# virtual methods
.method public onWindowFocusChanged(Z)V
    .locals 3
    .param p1, "hasWindowFocus"    # Z

    .prologue
    .line 179
    invoke-super {p0, p1}, Landroid/view/SurfaceView;->onWindowFocusChanged(Z)V

    .line 180
    const-string v0, "VisualEffectBatteryEffect"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onWindowFocusChanged : hasWindowFocus = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 181
    if-nez p1, :cond_0

    invoke-direct {p0}, Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;->stopEffect()V

    .line 182
    :cond_0
    return-void
.end method

.method public run()V
    .locals 2

    .prologue
    .line 191
    const-string v0, "VisualEffectBatteryEffect"

    const-string v1, "thread run"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 192
    :goto_0
    iget-boolean v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;->isThreadRunning:Z

    if-eqz v0, :cond_0

    .line 193
    invoke-direct {p0}, Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;->synchronizedDraw()V

    goto :goto_0

    .line 195
    :cond_0
    return-void
.end method

.method public setBatteryPercent(I)V
    .locals 3
    .param p1, "percent"    # I

    .prologue
    .line 113
    const-string v0, "VisualEffectBatteryEffect"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setBatteryPercent : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 114
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;->isYMoving:Z

    .line 115
    int-to-float v0, p1

    iput v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;->targetPercent:F

    .line 116
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;->wave:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;->startEffect()V

    .line 117
    :cond_0
    return-void
.end method

.method public setColor(II)V
    .locals 3
    .param p1, "colorUpside"    # I
    .param p2, "colorDownside"    # I

    .prologue
    .line 120
    const-string v0, "VisualEffectBatteryEffect"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setColor - Up : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", Down : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 122
    iput p1, p0, Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;->colorUpside:I

    .line 123
    iput p2, p0, Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;->colorDownside:I

    .line 124
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;->wave:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;->wave:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->setColor(IZ)V

    .line 125
    :cond_0
    return-void
.end method

.method public surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 2
    .param p1, "holder"    # Landroid/view/SurfaceHolder;
    .param p2, "format"    # I
    .param p3, "width"    # I
    .param p4, "height"    # I

    .prologue
    .line 186
    const-string v0, "VisualEffectBatteryEffect"

    const-string v1, "surfaceChanged"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 187
    return-void
.end method

.method public surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 7
    .param p1, "holder"    # Landroid/view/SurfaceHolder;

    .prologue
    const/4 v6, 0x0

    .line 61
    const-string v0, "VisualEffectBatteryEffect"

    const-string v1, "surfaceCreated"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 63
    iput v6, p0, Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;->currentPercent:F

    .line 65
    new-instance v0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;

    iget v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;->batteryWidth:I

    iget v2, p0, Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;->batteryHeight:I

    sget-object v3, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape$SideType;->Upside:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape$SideType;

    const v4, 0x3f4ccccd    # 0.8f

    const/4 v5, 0x1

    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;-><init>(IILcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape$SideType;FZ)V

    iput-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;->wave:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;

    .line 66
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;->wave:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;

    iget v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;->colorUpside:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->setColor(IZ)V

    .line 67
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;->wave:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;

    iget v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;->waveSpeed:F

    invoke-virtual {v0, v1}, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->setSpeed(F)V

    .line 68
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;->wave:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;

    iget v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;->waveHeight:F

    invoke-virtual {v0, v1}, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->setWaveHeight(F)V

    .line 69
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;->wave:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;

    invoke-virtual {v0, v6}, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->setWaveHeightRatio(F)V

    .line 70
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;->wave:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;

    invoke-virtual {v0, v6}, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->setPercent(F)V

    .line 72
    invoke-direct {p0}, Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;->synchronizedDraw()V

    .line 74
    iget v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;->targetPercent:F

    cmpl-float v0, v0, v6

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;->targetPercent:F

    float-to-int v0, v0

    invoke-virtual {p0, v0}, Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;->setBatteryPercent(I)V

    .line 75
    :cond_0
    return-void
.end method

.method public surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 4
    .param p1, "holder"    # Landroid/view/SurfaceHolder;

    .prologue
    .line 163
    const-string v2, "VisualEffectBatteryEffect"

    const-string v3, "surfaceDestroyed"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 164
    invoke-direct {p0}, Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;->stopThread()V

    .line 166
    const/4 v1, 0x1

    .line 167
    .local v1, "retry":Z
    :goto_0
    if-eqz v1, :cond_1

    .line 169
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;->thread:Ljava/lang/Thread;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/android/visualeffect/naturalcurve/fingerprintwave/FingerprintWaveSurfaceView;->thread:Ljava/lang/Thread;

    invoke-virtual {v2}, Ljava/lang/Thread;->join()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 170
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 171
    :catch_0
    move-exception v0

    .line 172
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->getStackTrace()[Ljava/lang/StackTraceElement;

    goto :goto_0

    .line 175
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :cond_1
    return-void
.end method
