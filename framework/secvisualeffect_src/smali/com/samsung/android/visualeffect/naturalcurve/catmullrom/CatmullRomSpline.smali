.class public Lcom/samsung/android/visualeffect/naturalcurve/catmullrom/CatmullRomSpline;
.super Ljava/lang/Object;
.source "CatmullRomSpline.java"


# instance fields
.field private p0:F

.field private p1:F

.field private p2:F

.field private p3:F


# direct methods
.method public constructor <init>(FFFF)V
    .locals 0
    .param p1, "p0"    # F
    .param p2, "p1"    # F
    .param p3, "p2"    # F
    .param p4, "p3"    # F

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    iput p1, p0, Lcom/samsung/android/visualeffect/naturalcurve/catmullrom/CatmullRomSpline;->p0:F

    .line 13
    iput p2, p0, Lcom/samsung/android/visualeffect/naturalcurve/catmullrom/CatmullRomSpline;->p1:F

    .line 14
    iput p3, p0, Lcom/samsung/android/visualeffect/naturalcurve/catmullrom/CatmullRomSpline;->p2:F

    .line 15
    iput p4, p0, Lcom/samsung/android/visualeffect/naturalcurve/catmullrom/CatmullRomSpline;->p3:F

    .line 16
    return-void
.end method


# virtual methods
.method public getP0()F
    .locals 1

    .prologue
    .line 29
    iget v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/catmullrom/CatmullRomSpline;->p0:F

    return v0
.end method

.method public getP1()F
    .locals 1

    .prologue
    .line 43
    iget v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/catmullrom/CatmullRomSpline;->p1:F

    return v0
.end method

.method public getP2()F
    .locals 1

    .prologue
    .line 57
    iget v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/catmullrom/CatmullRomSpline;->p2:F

    return v0
.end method

.method public getP3()F
    .locals 1

    .prologue
    .line 71
    iget v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/catmullrom/CatmullRomSpline;->p3:F

    return v0
.end method

.method public q(F)F
    .locals 6
    .param p1, "t"    # F

    .prologue
    const/high16 v5, 0x40400000    # 3.0f

    const/high16 v4, 0x40000000    # 2.0f

    .line 19
    const/high16 v0, 0x3f000000    # 0.5f

    iget v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/catmullrom/CatmullRomSpline;->p1:F

    mul-float/2addr v1, v4

    iget v2, p0, Lcom/samsung/android/visualeffect/naturalcurve/catmullrom/CatmullRomSpline;->p2:F

    iget v3, p0, Lcom/samsung/android/visualeffect/naturalcurve/catmullrom/CatmullRomSpline;->p0:F

    sub-float/2addr v2, v3

    mul-float/2addr v2, p1

    add-float/2addr v1, v2

    iget v2, p0, Lcom/samsung/android/visualeffect/naturalcurve/catmullrom/CatmullRomSpline;->p0:F

    mul-float/2addr v2, v4

    const/high16 v3, 0x40a00000    # 5.0f

    iget v4, p0, Lcom/samsung/android/visualeffect/naturalcurve/catmullrom/CatmullRomSpline;->p1:F

    mul-float/2addr v3, v4

    sub-float/2addr v2, v3

    const/high16 v3, 0x40800000    # 4.0f

    iget v4, p0, Lcom/samsung/android/visualeffect/naturalcurve/catmullrom/CatmullRomSpline;->p2:F

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    iget v3, p0, Lcom/samsung/android/visualeffect/naturalcurve/catmullrom/CatmullRomSpline;->p3:F

    sub-float/2addr v2, v3

    mul-float/2addr v2, p1

    mul-float/2addr v2, p1

    add-float/2addr v1, v2

    iget v2, p0, Lcom/samsung/android/visualeffect/naturalcurve/catmullrom/CatmullRomSpline;->p1:F

    mul-float/2addr v2, v5

    iget v3, p0, Lcom/samsung/android/visualeffect/naturalcurve/catmullrom/CatmullRomSpline;->p0:F

    sub-float/2addr v2, v3

    iget v3, p0, Lcom/samsung/android/visualeffect/naturalcurve/catmullrom/CatmullRomSpline;->p2:F

    mul-float/2addr v3, v5

    sub-float/2addr v2, v3

    iget v3, p0, Lcom/samsung/android/visualeffect/naturalcurve/catmullrom/CatmullRomSpline;->p3:F

    add-float/2addr v2, v3

    mul-float/2addr v2, p1

    mul-float/2addr v2, p1

    mul-float/2addr v2, p1

    add-float/2addr v1, v2

    mul-float/2addr v0, v1

    return v0
.end method

.method public setP0(F)V
    .locals 0
    .param p1, "p0"    # F

    .prologue
    .line 36
    iput p1, p0, Lcom/samsung/android/visualeffect/naturalcurve/catmullrom/CatmullRomSpline;->p0:F

    .line 37
    return-void
.end method

.method public setP1(F)V
    .locals 0
    .param p1, "p1"    # F

    .prologue
    .line 50
    iput p1, p0, Lcom/samsung/android/visualeffect/naturalcurve/catmullrom/CatmullRomSpline;->p1:F

    .line 51
    return-void
.end method

.method public setP2(F)V
    .locals 0
    .param p1, "p2"    # F

    .prologue
    .line 64
    iput p1, p0, Lcom/samsung/android/visualeffect/naturalcurve/catmullrom/CatmullRomSpline;->p2:F

    .line 65
    return-void
.end method

.method public setP3(F)V
    .locals 0
    .param p1, "p3"    # F

    .prologue
    .line 78
    iput p1, p0, Lcom/samsung/android/visualeffect/naturalcurve/catmullrom/CatmullRomSpline;->p3:F

    .line 79
    return-void
.end method
