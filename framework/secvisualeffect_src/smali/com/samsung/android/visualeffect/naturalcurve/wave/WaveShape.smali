.class public Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;
.super Ljava/lang/Object;
.source "WaveShape.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape$1;,
        Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape$XAscCompare;,
        Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape$XDescCompare;,
        Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape$SideType;
    }
.end annotation


# instance fields
.field private final MARGIN:I

.field private centerY:F

.field private circlePaint:Landroid/graphics/Paint;

.field private colorDown:I

.field private colorUp:I

.field private isNewWaveUpside:Z

.field private isRTL:Z

.field private left:I

.field private pathDown:Landroid/graphics/Path;

.field private pathUp:Landroid/graphics/Path;

.field private pointArray:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/visualeffect/naturalcurve/wave/WavePoint;",
            ">;"
        }
    .end annotation
.end field

.field private pointTotal:I

.field private rectPaint:Landroid/graphics/Paint;

.field private right:I

.field private shapePaintDown:Landroid/graphics/Paint;

.field private shapePaintUp:Landroid/graphics/Paint;

.field private sideType:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape$SideType;

.field private speed:F

.field private subDivision:I

.field private waveHeight:F

.field private waveHeightRatio:F

.field private waveShapeHeight:I

.field private waveShapeWidth:I

.field private waveWidth:F


# direct methods
.method public constructor <init>(IILcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape$SideType;FZ)V
    .locals 5
    .param p1, "waveShapeWidth"    # I
    .param p2, "waveShapeHeight"    # I
    .param p3, "sideType"    # Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape$SideType;
    .param p4, "waveRatio"    # F
    .param p5, "isRightToLeft"    # Z

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const/high16 v2, -0x1000000

    const/high16 v1, 0x3f800000    # 1.0f

    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput v3, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->MARGIN:I

    .line 25
    const/16 v0, 0x14

    iput v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->subDivision:I

    .line 26
    iput v2, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->colorUp:I

    .line 27
    iput v2, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->colorDown:I

    .line 31
    const/high16 v0, 0x40000000    # 2.0f

    iput v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->speed:F

    .line 32
    const/high16 v0, 0x41f00000    # 30.0f

    iput v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->waveHeight:F

    .line 33
    iput v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->waveHeightRatio:F

    .line 34
    iput-boolean v4, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->isRTL:Z

    .line 35
    iput-boolean v4, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->isNewWaveUpside:Z

    .line 49
    iput p1, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->waveShapeWidth:I

    .line 50
    iput p2, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->waveShapeHeight:I

    .line 51
    iput-object p3, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->sideType:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape$SideType;

    .line 52
    iput-boolean p5, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->isRTL:Z

    .line 53
    add-int/lit8 v0, p1, 0x0

    int-to-float v0, v0

    mul-float/2addr v0, p4

    iput v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->waveWidth:F

    .line 54
    div-int/lit8 v0, p2, 0x2

    int-to-float v0, v0

    iput v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->centerY:F

    .line 55
    div-float v0, v1, p4

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    double-to-int v0, v0

    add-int/lit8 v0, v0, 0x5

    iput v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->pointTotal:I

    .line 56
    iput v3, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->left:I

    .line 57
    add-int/lit8 v0, p1, 0x0

    iput v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->right:I

    .line 59
    sget-object v0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape$SideType;->Downside:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape$SideType;

    if-eq p3, v0, :cond_0

    .line 60
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->pathUp:Landroid/graphics/Path;

    .line 61
    :cond_0
    sget-object v0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape$SideType;->Upside:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape$SideType;

    if-eq p3, v0, :cond_1

    .line 62
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->pathDown:Landroid/graphics/Path;

    .line 64
    :cond_1
    invoke-direct {p0}, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->setPoints()V

    .line 65
    invoke-direct {p0}, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->setPaint()V

    .line 66
    return-void
.end method

.method private getIsNewWaveUpside()Z
    .locals 1

    .prologue
    .line 240
    iget-boolean v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->isNewWaveUpside:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->isNewWaveUpside:Z

    .line 241
    iget-boolean v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->isNewWaveUpside:Z

    return v0

    .line 240
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getOpacityColor(I)I
    .locals 2
    .param p1, "color"    # I

    .prologue
    .line 235
    const v1, 0xffffff

    and-int v0, p1, v1

    .line 236
    .local v0, "colorWithoutAlpha":I
    const/high16 v1, -0x78000000

    or-int/2addr v1, v0

    return v1
.end method

.method private setPaint()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 79
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->circlePaint:Landroid/graphics/Paint;

    .line 80
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->circlePaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 81
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->circlePaint:Landroid/graphics/Paint;

    const/high16 v1, -0x10000

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 82
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->circlePaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 84
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->rectPaint:Landroid/graphics/Paint;

    .line 85
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->rectPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 86
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->rectPaint:Landroid/graphics/Paint;

    const/high16 v1, 0x41a00000    # 20.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 87
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->rectPaint:Landroid/graphics/Paint;

    const/high16 v1, 0x66ff0000

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 88
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->rectPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 90
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->shapePaintUp:Landroid/graphics/Paint;

    .line 91
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->shapePaintUp:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 92
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->shapePaintUp:Landroid/graphics/Paint;

    iget v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->colorUp:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 93
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->shapePaintUp:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 95
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->shapePaintDown:Landroid/graphics/Paint;

    .line 96
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->shapePaintDown:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 97
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->shapePaintDown:Landroid/graphics/Paint;

    iget v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->colorDown:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 98
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->shapePaintDown:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 99
    return-void
.end method

.method private setPoints()V
    .locals 6

    .prologue
    .line 69
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->pointArray:Ljava/util/ArrayList;

    .line 71
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v3, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->pointTotal:I

    if-ge v0, v3, :cond_1

    .line 72
    iget-boolean v3, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->isRTL:Z

    if-eqz v3, :cond_0

    iget v3, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->left:I

    int-to-float v3, v3

    iget v4, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->waveWidth:F

    add-int/lit8 v5, v0, -0x1

    int-to-float v5, v5

    mul-float/2addr v4, v5

    add-float v2, v3, v4

    .line 73
    .local v2, "x":F
    :goto_1
    new-instance v1, Lcom/samsung/android/visualeffect/naturalcurve/wave/WavePoint;

    iget v3, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->centerY:F

    invoke-direct {p0}, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->getIsNewWaveUpside()Z

    move-result v4

    invoke-direct {v1, v2, v3, v4}, Lcom/samsung/android/visualeffect/naturalcurve/wave/WavePoint;-><init>(FFZ)V

    .line 74
    .local v1, "point":Lcom/samsung/android/visualeffect/naturalcurve/wave/WavePoint;
    iget-object v3, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->pointArray:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 71
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 72
    .end local v1    # "point":Lcom/samsung/android/visualeffect/naturalcurve/wave/WavePoint;
    .end local v2    # "x":F
    :cond_0
    iget v3, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->right:I

    int-to-float v3, v3

    iget v4, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->waveWidth:F

    add-int/lit8 v5, v0, -0x1

    int-to-float v5, v5

    mul-float/2addr v4, v5

    sub-float v2, v3, v4

    goto :goto_1

    .line 76
    :cond_1
    return-void
.end method


# virtual methods
.method public calculatePoints(Landroid/graphics/Canvas;)V
    .locals 13
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v12, 0x2

    const/4 v2, 0x0

    const/4 v5, 0x0

    .line 160
    iget v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->pointTotal:I

    new-array v6, v0, [Lcom/samsung/android/visualeffect/naturalcurve/catmullrom/Point2D;

    .line 162
    .local v6, "catmullromPointArray":[Lcom/samsung/android/visualeffect/naturalcurve/catmullrom/Point2D;
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_0
    iget v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->pointTotal:I

    if-ge v7, v0, :cond_3

    .line 163
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->pointArray:Ljava/util/ArrayList;

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/samsung/android/visualeffect/naturalcurve/wave/WavePoint;

    .line 164
    .local v10, "point":Lcom/samsung/android/visualeffect/naturalcurve/wave/WavePoint;
    iget v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->centerY:F

    iget v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->waveHeight:F

    iget v3, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->waveHeightRatio:F

    mul-float/2addr v3, v0

    invoke-virtual {v10}, Lcom/samsung/android/visualeffect/naturalcurve/wave/WavePoint;->getIsUpside()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, -0x1

    :goto_1
    int-to-float v0, v0

    mul-float/2addr v0, v3

    add-float/2addr v0, v1

    invoke-virtual {v10, v0}, Lcom/samsung/android/visualeffect/naturalcurve/wave/WavePoint;->setY(F)V

    .line 165
    iget v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->left:I

    if-lez v0, :cond_0

    .line 166
    invoke-virtual {v10}, Lcom/samsung/android/visualeffect/naturalcurve/wave/WavePoint;->getX()F

    move-result v0

    invoke-virtual {v10}, Lcom/samsung/android/visualeffect/naturalcurve/wave/WavePoint;->getY()F

    move-result v1

    const/high16 v3, 0x41700000    # 15.0f

    iget-object v4, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->circlePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v3, v4}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 167
    :cond_0
    aput-object v10, v6, v7

    .line 169
    iget-boolean v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->isRTL:Z

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->speed:F

    neg-float v9, v0

    .line 170
    .local v9, "offset":F
    :goto_2
    invoke-virtual {v10}, Lcom/samsung/android/visualeffect/naturalcurve/wave/WavePoint;->getX()F

    move-result v0

    add-float/2addr v0, v9

    invoke-virtual {v10, v0}, Lcom/samsung/android/visualeffect/naturalcurve/wave/WavePoint;->setX(F)V

    .line 162
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 164
    .end local v9    # "offset":F
    :cond_1
    const/4 v0, 0x1

    goto :goto_1

    .line 169
    :cond_2
    iget v9, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->speed:F

    goto :goto_2

    .line 173
    .end local v10    # "point":Lcom/samsung/android/visualeffect/naturalcurve/wave/WavePoint;
    :cond_3
    iget v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->subDivision:I

    invoke-static {v6, v0}, Lcom/samsung/android/visualeffect/naturalcurve/catmullrom/CatmullRomSplineUtils;->subdividePoints([Lcom/samsung/android/visualeffect/naturalcurve/catmullrom/Point2D;I)[Lcom/samsung/android/visualeffect/naturalcurve/catmullrom/Point2D;

    move-result-object v11

    .line 177
    .local v11, "subdividedPoints":[Lcom/samsung/android/visualeffect/naturalcurve/catmullrom/Point2D;
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->sideType:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape$SideType;

    sget-object v1, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape$SideType;->Downside:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape$SideType;

    if-eq v0, v1, :cond_4

    .line 178
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->pathUp:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 179
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->pathUp:Landroid/graphics/Path;

    aget-object v1, v11, v5

    invoke-virtual {v1}, Lcom/samsung/android/visualeffect/naturalcurve/catmullrom/Point2D;->getX()F

    move-result v1

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 181
    :cond_4
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->sideType:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape$SideType;

    sget-object v1, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape$SideType;->Upside:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape$SideType;

    if-eq v0, v1, :cond_5

    .line 182
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->pathDown:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 183
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->pathDown:Landroid/graphics/Path;

    aget-object v1, v11, v5

    invoke-virtual {v1}, Lcom/samsung/android/visualeffect/naturalcurve/catmullrom/Point2D;->getX()F

    move-result v1

    iget v3, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->waveShapeHeight:I

    int-to-float v3, v3

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Path;->moveTo(FF)V

    .line 187
    :cond_5
    const/4 v8, 0x1

    .local v8, "j":I
    :goto_3
    array-length v0, v11

    if-ge v8, v0, :cond_8

    .line 188
    aget-object v10, v11, v8

    .line 189
    .local v10, "point":Lcom/samsung/android/visualeffect/naturalcurve/catmullrom/Point2D;
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->sideType:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape$SideType;

    sget-object v1, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape$SideType;->Downside:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape$SideType;

    if-eq v0, v1, :cond_6

    .line 190
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->pathUp:Landroid/graphics/Path;

    invoke-virtual {v10}, Lcom/samsung/android/visualeffect/naturalcurve/catmullrom/Point2D;->getX()F

    move-result v1

    invoke-virtual {v10}, Lcom/samsung/android/visualeffect/naturalcurve/catmullrom/Point2D;->getY()F

    move-result v3

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 191
    :cond_6
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->sideType:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape$SideType;

    sget-object v1, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape$SideType;->Upside:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape$SideType;

    if-eq v0, v1, :cond_7

    .line 192
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->pathDown:Landroid/graphics/Path;

    invoke-virtual {v10}, Lcom/samsung/android/visualeffect/naturalcurve/catmullrom/Point2D;->getX()F

    move-result v1

    invoke-virtual {v10}, Lcom/samsung/android/visualeffect/naturalcurve/catmullrom/Point2D;->getY()F

    move-result v3

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 187
    :cond_7
    add-int/lit8 v8, v8, 0x1

    goto :goto_3

    .line 196
    .end local v10    # "point":Lcom/samsung/android/visualeffect/naturalcurve/catmullrom/Point2D;
    :cond_8
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->sideType:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape$SideType;

    sget-object v1, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape$SideType;->Downside:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape$SideType;

    if-eq v0, v1, :cond_9

    .line 197
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->pathUp:Landroid/graphics/Path;

    array-length v1, v11

    add-int/lit8 v1, v1, -0x1

    aget-object v1, v11, v1

    invoke-virtual {v1}, Lcom/samsung/android/visualeffect/naturalcurve/catmullrom/Point2D;->getX()F

    move-result v1

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 198
    :cond_9
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->sideType:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape$SideType;

    sget-object v1, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape$SideType;->Upside:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape$SideType;

    if-eq v0, v1, :cond_a

    .line 199
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->pathDown:Landroid/graphics/Path;

    array-length v1, v11

    add-int/lit8 v1, v1, -0x1

    aget-object v1, v11, v1

    invoke-virtual {v1}, Lcom/samsung/android/visualeffect/naturalcurve/catmullrom/Point2D;->getX()F

    move-result v1

    iget v3, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->waveShapeHeight:I

    int-to-float v3, v3

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 202
    :cond_a
    iget-boolean v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->isRTL:Z

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->pointArray:Ljava/util/ArrayList;

    invoke-virtual {v0, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WavePoint;

    invoke-virtual {v0}, Lcom/samsung/android/visualeffect/naturalcurve/wave/WavePoint;->getX()F

    move-result v0

    iget v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->left:I

    int-to-float v1, v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_b

    .line 203
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->pointArray:Ljava/util/ArrayList;

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WavePoint;

    iget-object v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->pointArray:Ljava/util/ArrayList;

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/visualeffect/naturalcurve/wave/WavePoint;

    invoke-virtual {v1}, Lcom/samsung/android/visualeffect/naturalcurve/wave/WavePoint;->getX()F

    move-result v1

    iget v3, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->pointTotal:I

    int-to-float v3, v3

    iget v4, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->waveWidth:F

    mul-float/2addr v3, v4

    add-float/2addr v1, v3

    invoke-virtual {v0, v1}, Lcom/samsung/android/visualeffect/naturalcurve/wave/WavePoint;->setX(F)V

    .line 204
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->pointArray:Ljava/util/ArrayList;

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WavePoint;

    invoke-direct {p0}, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->getIsNewWaveUpside()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/visualeffect/naturalcurve/wave/WavePoint;->setIsUpside(Z)V

    .line 205
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->pointArray:Ljava/util/ArrayList;

    new-instance v1, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape$XDescCompare;

    invoke-direct {v1, p0}, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape$XDescCompare;-><init>(Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;)V

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 207
    :cond_b
    iget-boolean v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->isRTL:Z

    if-nez v0, :cond_c

    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->pointArray:Ljava/util/ArrayList;

    invoke-virtual {v0, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WavePoint;

    invoke-virtual {v0}, Lcom/samsung/android/visualeffect/naturalcurve/wave/WavePoint;->getX()F

    move-result v0

    iget v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->right:I

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_c

    .line 208
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->pointArray:Ljava/util/ArrayList;

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WavePoint;

    iget-object v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->pointArray:Ljava/util/ArrayList;

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/visualeffect/naturalcurve/wave/WavePoint;

    invoke-virtual {v1}, Lcom/samsung/android/visualeffect/naturalcurve/wave/WavePoint;->getX()F

    move-result v1

    iget v3, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->pointTotal:I

    int-to-float v3, v3

    iget v4, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->waveWidth:F

    mul-float/2addr v3, v4

    sub-float/2addr v1, v3

    invoke-virtual {v0, v1}, Lcom/samsung/android/visualeffect/naturalcurve/wave/WavePoint;->setX(F)V

    .line 209
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->pointArray:Ljava/util/ArrayList;

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WavePoint;

    invoke-direct {p0}, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->getIsNewWaveUpside()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/visualeffect/naturalcurve/wave/WavePoint;->setIsUpside(Z)V

    .line 210
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->pointArray:Ljava/util/ArrayList;

    new-instance v1, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape$XAscCompare;

    invoke-direct {v1, p0}, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape$XAscCompare;-><init>(Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;)V

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 213
    :cond_c
    iget v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->left:I

    if-lez v0, :cond_d

    .line 214
    iget v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->left:I

    int-to-float v1, v0

    iget v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->right:I

    int-to-float v3, v0

    iget v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->waveShapeHeight:I

    int-to-float v4, v0

    iget-object v5, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->rectPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 215
    :cond_d
    return-void
.end method

.method public drawWave(Landroid/graphics/Canvas;Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape$SideType;)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "type"    # Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape$SideType;

    .prologue
    .line 218
    sget-object v0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape$1;->$SwitchMap$com$samsung$android$visualeffect$naturalcurve$wave$WaveShape$SideType:[I

    invoke-virtual {p2}, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape$SideType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 232
    :goto_0
    return-void

    .line 220
    :pswitch_0
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->pathUp:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->shapePaintUp:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    goto :goto_0

    .line 224
    :pswitch_1
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->pathDown:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->shapePaintDown:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    goto :goto_0

    .line 228
    :pswitch_2
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->pathUp:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->shapePaintUp:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 229
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->pathDown:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->shapePaintDown:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    goto :goto_0

    .line 218
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public setColor(IIZ)V
    .locals 2
    .param p1, "colorUp"    # I
    .param p2, "colorDown"    # I
    .param p3, "hasOpacity"    # Z

    .prologue
    .line 122
    iput p1, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->colorUp:I

    .line 123
    if-eqz p3, :cond_0

    invoke-direct {p0, p2}, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->getOpacityColor(I)I

    move-result p2

    .end local p2    # "colorDown":I
    :cond_0
    iput p2, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->colorDown:I

    .line 124
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->shapePaintUp:Landroid/graphics/Paint;

    iget v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->colorUp:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 125
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->shapePaintDown:Landroid/graphics/Paint;

    iget v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->colorDown:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 126
    return-void
.end method

.method public setColor(IZ)V
    .locals 2
    .param p1, "color"    # I
    .param p2, "hasOpacity"    # Z

    .prologue
    .line 112
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->sideType:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape$SideType;

    sget-object v1, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape$SideType;->Downside:Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape$SideType;

    if-ne v0, v1, :cond_1

    .line 113
    if-eqz p2, :cond_0

    invoke-direct {p0, p1}, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->getOpacityColor(I)I

    move-result p1

    .end local p1    # "color":I
    :cond_0
    iput p1, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->colorDown:I

    .line 114
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->shapePaintDown:Landroid/graphics/Paint;

    iget v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->colorDown:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 119
    :goto_0
    return-void

    .line 116
    .restart local p1    # "color":I
    :cond_1
    if-eqz p2, :cond_2

    invoke-direct {p0, p1}, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->getOpacityColor(I)I

    move-result p1

    .end local p1    # "color":I
    :cond_2
    iput p1, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->colorUp:I

    .line 117
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->shapePaintUp:Landroid/graphics/Paint;

    iget v1, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->colorUp:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    goto :goto_0
.end method

.method public setPaintMode(Landroid/graphics/PorterDuff$Mode;Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape$SideType;)V
    .locals 2
    .param p1, "mode"    # Landroid/graphics/PorterDuff$Mode;
    .param p2, "side"    # Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape$SideType;

    .prologue
    .line 129
    sget-object v0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape$1;->$SwitchMap$com$samsung$android$visualeffect$naturalcurve$wave$WaveShape$SideType:[I

    invoke-virtual {p2}, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape$SideType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 141
    :goto_0
    return-void

    .line 131
    :pswitch_0
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->shapePaintUp:Landroid/graphics/Paint;

    new-instance v1, Landroid/graphics/PorterDuffXfermode;

    invoke-direct {v1, p1}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    goto :goto_0

    .line 134
    :pswitch_1
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->shapePaintDown:Landroid/graphics/Paint;

    new-instance v1, Landroid/graphics/PorterDuffXfermode;

    invoke-direct {v1, p1}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    goto :goto_0

    .line 137
    :pswitch_2
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->shapePaintUp:Landroid/graphics/Paint;

    new-instance v1, Landroid/graphics/PorterDuffXfermode;

    invoke-direct {v1, p1}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 138
    iget-object v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->shapePaintDown:Landroid/graphics/Paint;

    new-instance v1, Landroid/graphics/PorterDuffXfermode;

    invoke-direct {v1, p1}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    goto :goto_0

    .line 129
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public setPercent(F)V
    .locals 3
    .param p1, "percent"    # F

    .prologue
    const/high16 v2, 0x42c80000    # 100.0f

    .line 102
    iget v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->waveShapeHeight:I

    int-to-float v0, v0

    sub-float v1, v2, p1

    mul-float/2addr v0, v1

    div-float/2addr v0, v2

    iput v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->centerY:F

    .line 103
    return-void
.end method

.method public setSpeed(F)V
    .locals 0
    .param p1, "speed"    # F

    .prologue
    .line 144
    iput p1, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->speed:F

    .line 145
    return-void
.end method

.method public setSubDivision(I)V
    .locals 0
    .param p1, "subDivision"    # I

    .prologue
    .line 152
    iput p1, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->subDivision:I

    .line 153
    return-void
.end method

.method public setWaveHeight(F)V
    .locals 0
    .param p1, "height"    # F

    .prologue
    .line 148
    iput p1, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->waveHeight:F

    .line 149
    return-void
.end method

.method public setWaveHeightRatio(F)V
    .locals 0
    .param p1, "ratio"    # F

    .prologue
    .line 156
    iput p1, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->waveHeightRatio:F

    .line 157
    return-void
.end method

.method public setWaveRatio(F)V
    .locals 2
    .param p1, "waveRatio"    # F

    .prologue
    .line 106
    iget v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->waveShapeWidth:I

    add-int/lit8 v0, v0, 0x0

    int-to-float v0, v0

    mul-float/2addr v0, p1

    iput v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->waveWidth:F

    .line 107
    const/high16 v0, 0x3f800000    # 1.0f

    div-float/2addr v0, p1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    double-to-int v0, v0

    add-int/lit8 v0, v0, 0x5

    iput v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->pointTotal:I

    .line 108
    invoke-direct {p0}, Lcom/samsung/android/visualeffect/naturalcurve/wave/WaveShape;->setPoints()V

    .line 109
    return-void
.end method
