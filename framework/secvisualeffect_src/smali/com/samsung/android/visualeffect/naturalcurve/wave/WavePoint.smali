.class public Lcom/samsung/android/visualeffect/naturalcurve/wave/WavePoint;
.super Lcom/samsung/android/visualeffect/naturalcurve/catmullrom/Point2D;
.source "WavePoint.java"


# instance fields
.field private isUpside:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Lcom/samsung/android/visualeffect/naturalcurve/catmullrom/Point2D;-><init>()V

    return-void
.end method

.method public constructor <init>(FFZ)V
    .locals 0
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "isUpside"    # Z

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/samsung/android/visualeffect/naturalcurve/catmullrom/Point2D;-><init>()V

    .line 13
    invoke-virtual {p0, p1}, Lcom/samsung/android/visualeffect/naturalcurve/wave/WavePoint;->setX(F)V

    .line 14
    invoke-virtual {p0, p2}, Lcom/samsung/android/visualeffect/naturalcurve/wave/WavePoint;->setY(F)V

    .line 15
    iput-boolean p3, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WavePoint;->isUpside:Z

    .line 16
    return-void
.end method


# virtual methods
.method public getIsUpside()Z
    .locals 1

    .prologue
    .line 23
    iget-boolean v0, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WavePoint;->isUpside:Z

    return v0
.end method

.method public setIsUpside(Z)V
    .locals 0
    .param p1, "isUpside"    # Z

    .prologue
    .line 19
    iput-boolean p1, p0, Lcom/samsung/android/visualeffect/naturalcurve/wave/WavePoint;->isUpside:Z

    .line 20
    return-void
.end method
