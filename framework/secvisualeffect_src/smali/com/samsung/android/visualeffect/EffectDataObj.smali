.class public Lcom/samsung/android/visualeffect/EffectDataObj;
.super Ljava/lang/Object;
.source "EffectDataObj.java"


# instance fields
.field public circleData:Lcom/samsung/android/visualeffect/lock/data/CircleData;

.field public indigoDiffuseData:Lcom/samsung/android/visualeffect/lock/data/IndigoDiffuseData;

.field public lensFlareData:Lcom/samsung/android/visualeffect/lock/data/LensFlareData;

.field public poppingColorData:Lcom/samsung/android/visualeffect/lock/data/PoppingColorData;

.field public rippleInkData:Lcom/samsung/android/visualeffect/lock/data/RippleInkData;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public setEffect(I)V
    .locals 1
    .param p1, "effect"    # I

    .prologue
    .line 20
    packed-switch p1, :pswitch_data_0

    .line 41
    :goto_0
    :pswitch_0
    return-void

    .line 23
    :pswitch_1
    new-instance v0, Lcom/samsung/android/visualeffect/lock/data/PoppingColorData;

    invoke-direct {v0}, Lcom/samsung/android/visualeffect/lock/data/PoppingColorData;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/visualeffect/EffectDataObj;->poppingColorData:Lcom/samsung/android/visualeffect/lock/data/PoppingColorData;

    goto :goto_0

    .line 26
    :pswitch_2
    new-instance v0, Lcom/samsung/android/visualeffect/lock/data/CircleData;

    invoke-direct {v0}, Lcom/samsung/android/visualeffect/lock/data/CircleData;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/visualeffect/EffectDataObj;->circleData:Lcom/samsung/android/visualeffect/lock/data/CircleData;

    goto :goto_0

    .line 29
    :pswitch_3
    new-instance v0, Lcom/samsung/android/visualeffect/lock/data/PoppingColorData;

    invoke-direct {v0}, Lcom/samsung/android/visualeffect/lock/data/PoppingColorData;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/visualeffect/EffectDataObj;->poppingColorData:Lcom/samsung/android/visualeffect/lock/data/PoppingColorData;

    goto :goto_0

    .line 32
    :pswitch_4
    new-instance v0, Lcom/samsung/android/visualeffect/lock/data/RippleInkData;

    invoke-direct {v0}, Lcom/samsung/android/visualeffect/lock/data/RippleInkData;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/visualeffect/EffectDataObj;->rippleInkData:Lcom/samsung/android/visualeffect/lock/data/RippleInkData;

    goto :goto_0

    .line 35
    :pswitch_5
    new-instance v0, Lcom/samsung/android/visualeffect/lock/data/IndigoDiffuseData;

    invoke-direct {v0}, Lcom/samsung/android/visualeffect/lock/data/IndigoDiffuseData;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/visualeffect/EffectDataObj;->indigoDiffuseData:Lcom/samsung/android/visualeffect/lock/data/IndigoDiffuseData;

    goto :goto_0

    .line 38
    :pswitch_6
    new-instance v0, Lcom/samsung/android/visualeffect/lock/data/LensFlareData;

    invoke-direct {v0}, Lcom/samsung/android/visualeffect/lock/data/LensFlareData;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/visualeffect/EffectDataObj;->lensFlareData:Lcom/samsung/android/visualeffect/lock/data/LensFlareData;

    goto :goto_0

    .line 20
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_6
    .end packed-switch
.end method
