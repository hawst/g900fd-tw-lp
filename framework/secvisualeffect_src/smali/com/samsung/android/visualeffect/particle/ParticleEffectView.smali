.class public Lcom/samsung/android/visualeffect/particle/ParticleEffectView;
.super Landroid/view/View;
.source "ParticleEffectView.java"


# instance fields
.field private TAG:Ljava/lang/String;

.field private dotMaxLimit:I

.field private dotUnlockSpeed:I

.field private drawingBottom:I

.field private drawingDelayTime:I

.field private drawingLeft:I

.field private drawingMargin:I

.field private drawingRight:I

.field private drawingTop:I

.field private hsvOrigin:[F

.field private hsvTemp:[F

.field private initCreatedDotAmount:I

.field private isDrawing:Z

.field private isPaused:Z

.field private lastAddedColor:I

.field private lastAddedX:F

.field private lastAddedY:F

.field mHandler:Landroid/os/Handler;

.field private nextParticleIndex:I

.field private particleAliveList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/visualeffect/lock/particle/Particle;",
            ">;"
        }
    .end annotation
.end field

.field private particleTotalList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/visualeffect/lock/particle/Particle;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 12
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v11, 0x3

    const/4 v10, 0x1

    const/4 v9, 0x0

    const/4 v8, 0x0

    .line 45
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 18
    const-string v7, "VisualEffectParticleEffect"

    iput-object v7, p0, Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->TAG:Ljava/lang/String;

    .line 19
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    iput-object v7, p0, Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->particleTotalList:Ljava/util/ArrayList;

    .line 20
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    iput-object v7, p0, Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->particleAliveList:Ljava/util/ArrayList;

    .line 22
    const/4 v7, 0x2

    iput v7, p0, Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->drawingDelayTime:I

    .line 23
    const/16 v7, 0xfa

    iput v7, p0, Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->initCreatedDotAmount:I

    .line 24
    const/16 v7, 0x96

    iput v7, p0, Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->dotMaxLimit:I

    .line 25
    const/4 v7, 0x5

    iput v7, p0, Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->dotUnlockSpeed:I

    .line 26
    iput v9, p0, Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->lastAddedX:F

    .line 27
    iput v9, p0, Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->lastAddedY:F

    .line 28
    iput v8, p0, Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->lastAddedColor:I

    .line 31
    iput v8, p0, Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->drawingLeft:I

    .line 32
    iput v8, p0, Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->drawingTop:I

    .line 33
    iput v10, p0, Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->drawingRight:I

    .line 34
    iput v10, p0, Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->drawingBottom:I

    .line 35
    const/16 v7, 0xb

    iput v7, p0, Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->drawingMargin:I

    .line 36
    const/4 v7, -0x1

    iput v7, p0, Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->nextParticleIndex:I

    .line 37
    iput-boolean v8, p0, Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->isPaused:Z

    .line 153
    new-instance v7, Lcom/samsung/android/visualeffect/particle/ParticleEffectView$1;

    invoke-direct {v7, p0}, Lcom/samsung/android/visualeffect/particle/ParticleEffectView$1;-><init>(Lcom/samsung/android/visualeffect/particle/ParticleEffectView;)V

    iput-object v7, p0, Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->mHandler:Landroid/os/Handler;

    .line 46
    new-array v7, v11, [F

    iput-object v7, p0, Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->hsvOrigin:[F

    .line 47
    new-array v7, v11, [F

    iput-object v7, p0, Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->hsvTemp:[F

    .line 49
    invoke-virtual {p0}, Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 50
    .local v0, "dm":Landroid/util/DisplayMetrics;
    iget v5, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 51
    .local v5, "screenWidth":I
    iget v4, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 52
    .local v4, "screenHeight":I
    if-ge v5, v4, :cond_0

    move v6, v5

    .line 54
    .local v6, "smallestWidth":I
    :goto_0
    int-to-float v7, v6

    const/high16 v8, 0x44870000    # 1080.0f

    div-float v3, v7, v8

    .line 55
    .local v3, "ratio":F
    iget-object v7, p0, Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "ParticleEffect : Constructor, "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " x "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 56
    iget-object v7, p0, Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "ParticleEffect : ratio = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 58
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    iget v7, p0, Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->initCreatedDotAmount:I

    if-ge v2, v7, :cond_1

    .line 59
    new-instance v1, Lcom/samsung/android/visualeffect/lock/particle/Particle;

    invoke-direct {v1, v3}, Lcom/samsung/android/visualeffect/lock/particle/Particle;-><init>(F)V

    .line 60
    .local v1, "dot":Lcom/samsung/android/visualeffect/lock/particle/Particle;
    iget-object v7, p0, Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->particleTotalList:Ljava/util/ArrayList;

    invoke-virtual {v7, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 58
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .end local v1    # "dot":Lcom/samsung/android/visualeffect/lock/particle/Particle;
    .end local v2    # "i":I
    .end local v3    # "ratio":F
    .end local v6    # "smallestWidth":I
    :cond_0
    move v6, v4

    .line 52
    goto :goto_0

    .line 62
    .restart local v2    # "i":I
    .restart local v3    # "ratio":F
    .restart local v6    # "smallestWidth":I
    :cond_1
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/visualeffect/particle/ParticleEffectView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/visualeffect/particle/ParticleEffectView;

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->isAvailableRect()Z

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/samsung/android/visualeffect/particle/ParticleEffectView;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/visualeffect/particle/ParticleEffectView;

    .prologue
    .line 16
    iget v0, p0, Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->drawingLeft:I

    return v0
.end method

.method static synthetic access$200(Lcom/samsung/android/visualeffect/particle/ParticleEffectView;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/visualeffect/particle/ParticleEffectView;

    .prologue
    .line 16
    iget v0, p0, Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->drawingMargin:I

    return v0
.end method

.method static synthetic access$300(Lcom/samsung/android/visualeffect/particle/ParticleEffectView;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/visualeffect/particle/ParticleEffectView;

    .prologue
    .line 16
    iget v0, p0, Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->drawingTop:I

    return v0
.end method

.method static synthetic access$400(Lcom/samsung/android/visualeffect/particle/ParticleEffectView;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/visualeffect/particle/ParticleEffectView;

    .prologue
    .line 16
    iget v0, p0, Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->drawingRight:I

    return v0
.end method

.method static synthetic access$500(Lcom/samsung/android/visualeffect/particle/ParticleEffectView;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/visualeffect/particle/ParticleEffectView;

    .prologue
    .line 16
    iget v0, p0, Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->drawingBottom:I

    return v0
.end method

.method static synthetic access$600(Lcom/samsung/android/visualeffect/particle/ParticleEffectView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/visualeffect/particle/ParticleEffectView;

    .prologue
    .line 16
    iget-boolean v0, p0, Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->isDrawing:Z

    return v0
.end method

.method static synthetic access$700(Lcom/samsung/android/visualeffect/particle/ParticleEffectView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/visualeffect/particle/ParticleEffectView;

    .prologue
    .line 16
    iget-boolean v0, p0, Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->isPaused:Z

    return v0
.end method

.method static synthetic access$800(Lcom/samsung/android/visualeffect/particle/ParticleEffectView;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/visualeffect/particle/ParticleEffectView;

    .prologue
    .line 16
    iget v0, p0, Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->drawingDelayTime:I

    return v0
.end method

.method private getNextDot()Lcom/samsung/android/visualeffect/lock/particle/Particle;
    .locals 2

    .prologue
    .line 107
    iget v0, p0, Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->nextParticleIndex:I

    iget v1, p0, Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->initCreatedDotAmount:I

    add-int/lit8 v1, v1, -0x1

    if-lt v0, v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput v0, p0, Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->nextParticleIndex:I

    .line 108
    iget-object v0, p0, Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->particleTotalList:Ljava/util/ArrayList;

    iget v1, p0, Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->nextParticleIndex:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/visualeffect/lock/particle/Particle;

    return-object v0

    .line 107
    :cond_0
    iget v0, p0, Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->nextParticleIndex:I

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private isAvailableRect()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 169
    iget v1, p0, Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->drawingLeft:I

    iget v2, p0, Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->drawingRight:I

    if-lt v1, v2, :cond_1

    .line 182
    :cond_0
    :goto_0
    return v0

    .line 171
    :cond_1
    iget v1, p0, Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->drawingTop:I

    iget v2, p0, Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->drawingBottom:I

    if-ge v1, v2, :cond_0

    .line 173
    iget v1, p0, Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->drawingLeft:I

    invoke-virtual {p0}, Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->getWidth()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 175
    iget v1, p0, Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->drawingRight:I

    if-lez v1, :cond_0

    .line 177
    iget v1, p0, Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->drawingTop:I

    invoke-virtual {p0}, Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->getHeight()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 179
    iget v1, p0, Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->drawingBottom:I

    if-lez v1, :cond_0

    .line 182
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private startDrawing()V
    .locals 4

    .prologue
    .line 97
    iget-boolean v0, p0, Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->isDrawing:Z

    if-eqz v0, :cond_0

    .line 100
    :goto_0
    return-void

    .line 98
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->isDrawing:Z

    .line 99
    iget-object v0, p0, Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    iget v2, p0, Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->drawingDelayTime:I

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0
.end method

.method private stopDrawing()V
    .locals 1

    .prologue
    .line 103
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->isDrawing:Z

    .line 104
    return-void
.end method


# virtual methods
.method public addDots(IFFI)V
    .locals 16
    .param p1, "amount"    # I
    .param p2, "x"    # F
    .param p3, "y"    # F
    .param p4, "color"    # I

    .prologue
    .line 74
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->particleAliveList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    add-int v5, v5, p1

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->dotMaxLimit:I

    if-le v5, v6, :cond_0

    .line 94
    :goto_0
    return-void

    .line 76
    :cond_0
    move/from16 v0, p2

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->lastAddedX:F

    .line 77
    move/from16 v0, p3

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->lastAddedY:F

    .line 78
    move/from16 v0, p4

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->lastAddedColor:I

    .line 80
    invoke-static/range {p4 .. p4}, Landroid/graphics/Color;->red(I)I

    move-result v5

    invoke-static/range {p4 .. p4}, Landroid/graphics/Color;->green(I)I

    move-result v6

    invoke-static/range {p4 .. p4}, Landroid/graphics/Color;->blue(I)I

    move-result v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->hsvOrigin:[F

    invoke-static {v5, v6, v7, v8}, Landroid/graphics/Color;->RGBToHSV(III[F)V

    .line 82
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    move/from16 v0, p1

    if-ge v3, v0, :cond_1

    .line 83
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->hsvTemp:[F

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->hsvOrigin:[F

    const/4 v8, 0x0

    aget v7, v7, v8

    aput v7, v5, v6

    .line 84
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->hsvTemp:[F

    const/4 v6, 0x1

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->hsvOrigin:[F

    const/4 v8, 0x1

    aget v7, v7, v8

    aput v7, v5, v6

    .line 85
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->hsvTemp:[F

    const/4 v6, 0x2

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->hsvOrigin:[F

    const/4 v8, 0x2

    aget v7, v7, v8

    aput v7, v5, v6

    .line 86
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->hsvTemp:[F

    const/4 v6, 0x1

    aget v7, v5, v6

    float-to-double v8, v7

    const-wide/high16 v10, 0x3ff0000000000000L    # 1.0

    const-wide v12, 0x3fe6666666666666L    # 0.7

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v14

    mul-double/2addr v12, v14

    sub-double/2addr v10, v12

    mul-double/2addr v8, v10

    double-to-float v7, v8

    aput v7, v5, v6

    .line 87
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->hsvTemp:[F

    const/4 v6, 0x2

    aget v7, v5, v6

    float-to-double v8, v7

    const/high16 v7, 0x3f800000    # 1.0f

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->hsvTemp:[F

    const/4 v11, 0x2

    aget v10, v10, v11

    sub-float/2addr v7, v10

    float-to-double v10, v7

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v12

    mul-double/2addr v10, v12

    add-double/2addr v8, v10

    double-to-float v7, v8

    aput v7, v5, v6

    .line 88
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->hsvTemp:[F

    invoke-static {v5}, Landroid/graphics/Color;->HSVToColor([F)I

    move-result v4

    .line 89
    .local v4, "resultColor":I
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->getNextDot()Lcom/samsung/android/visualeffect/lock/particle/Particle;

    move-result-object v2

    .line 90
    .local v2, "dot":Lcom/samsung/android/visualeffect/lock/particle/Particle;
    move/from16 v0, p2

    move/from16 v1, p3

    invoke-virtual {v2, v0, v1, v4}, Lcom/samsung/android/visualeffect/lock/particle/Particle;->initialize(FFI)V

    .line 91
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->particleAliveList:Ljava/util/ArrayList;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 82
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 93
    .end local v2    # "dot":Lcom/samsung/android/visualeffect/lock/particle/Particle;
    .end local v4    # "resultColor":I
    :cond_1
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->startDrawing()V

    goto/16 :goto_0
.end method

.method public clearAllDots()V
    .locals 0

    .prologue
    .line 190
    invoke-virtual {p0}, Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->clearEffect()V

    .line 191
    return-void
.end method

.method public clearEffect()V
    .locals 2

    .prologue
    .line 195
    invoke-direct {p0}, Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->stopDrawing()V

    .line 196
    invoke-virtual {p0}, Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->invalidate()V

    .line 197
    iget-object v1, p0, Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->particleAliveList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v0, v1, -0x1

    .local v0, "i":I
    :goto_0
    if-ltz v0, :cond_0

    .line 198
    iget-object v1, p0, Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->particleAliveList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 197
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 200
    :cond_0
    return-void
.end method

.method public destroy()V
    .locals 0

    .prologue
    .line 204
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 7
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 124
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 126
    iget-object v6, p0, Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->particleAliveList:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 127
    invoke-direct {p0}, Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->stopDrawing()V

    .line 151
    :cond_0
    return-void

    .line 129
    :cond_1
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v6, p0, Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->particleAliveList:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-ge v2, v6, :cond_0

    .line 130
    iget-object v6, p0, Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->particleAliveList:Ljava/util/ArrayList;

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/visualeffect/lock/particle/Particle;

    .line 132
    .local v1, "dot":Lcom/samsung/android/visualeffect/lock/particle/Particle;
    if-eqz v1, :cond_6

    invoke-virtual {v1}, Lcom/samsung/android/visualeffect/lock/particle/Particle;->isAlive()Z

    move-result v6

    if-eqz v6, :cond_6

    .line 133
    invoke-virtual {v1}, Lcom/samsung/android/visualeffect/lock/particle/Particle;->move()V

    .line 134
    invoke-virtual {v1, p1}, Lcom/samsung/android/visualeffect/lock/particle/Particle;->draw(Landroid/graphics/Canvas;)V

    .line 136
    invoke-virtual {v1}, Lcom/samsung/android/visualeffect/lock/particle/Particle;->getLeft()I

    move-result v3

    .line 137
    .local v3, "left":I
    invoke-virtual {v1}, Lcom/samsung/android/visualeffect/lock/particle/Particle;->getRight()I

    move-result v4

    .line 138
    .local v4, "right":I
    invoke-virtual {v1}, Lcom/samsung/android/visualeffect/lock/particle/Particle;->getTop()I

    move-result v5

    .line 139
    .local v5, "top":I
    invoke-virtual {v1}, Lcom/samsung/android/visualeffect/lock/particle/Particle;->getBottom()I

    move-result v0

    .line 141
    .local v0, "bottom":I
    if-nez v2, :cond_2

    .end local v3    # "left":I
    :goto_1
    iput v3, p0, Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->drawingLeft:I

    .line 142
    if-nez v2, :cond_3

    .end local v5    # "top":I
    :goto_2
    iput v5, p0, Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->drawingTop:I

    .line 143
    if-nez v2, :cond_4

    .end local v4    # "right":I
    :goto_3
    iput v4, p0, Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->drawingRight:I

    .line 144
    if-nez v2, :cond_5

    .end local v0    # "bottom":I
    :goto_4
    iput v0, p0, Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->drawingBottom:I

    .line 129
    :goto_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 141
    .restart local v0    # "bottom":I
    .restart local v3    # "left":I
    .restart local v4    # "right":I
    .restart local v5    # "top":I
    :cond_2
    iget v6, p0, Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->drawingLeft:I

    invoke-static {v6, v3}, Ljava/lang/Math;->min(II)I

    move-result v3

    goto :goto_1

    .line 142
    .end local v3    # "left":I
    :cond_3
    iget v6, p0, Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->drawingTop:I

    invoke-static {v6, v5}, Ljava/lang/Math;->min(II)I

    move-result v5

    goto :goto_2

    .line 143
    .end local v5    # "top":I
    :cond_4
    iget v6, p0, Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->drawingRight:I

    invoke-static {v6, v4}, Ljava/lang/Math;->max(II)I

    move-result v4

    goto :goto_3

    .line 144
    .end local v4    # "right":I
    :cond_5
    iget v6, p0, Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->drawingBottom:I

    invoke-static {v6, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto :goto_4

    .line 146
    .end local v0    # "bottom":I
    :cond_6
    iget-object v6, p0, Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->particleAliveList:Ljava/util/ArrayList;

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 147
    add-int/lit8 v2, v2, -0x1

    goto :goto_5
.end method

.method public pauseEffect()V
    .locals 2

    .prologue
    .line 208
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->isPaused:Z

    .line 209
    iget-object v0, p0, Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->TAG:Ljava/lang/String;

    const-string v1, "ParticleEffect : pauseEffect"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 210
    return-void
.end method

.method public resumeEffect()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 214
    iget-object v0, p0, Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->TAG:Ljava/lang/String;

    const-string v1, "ParticleEffect : resumeEffect"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 215
    iget-boolean v0, p0, Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->isPaused:Z

    if-eqz v0, :cond_0

    .line 216
    iput-boolean v4, p0, Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->isPaused:Z

    .line 217
    iget-boolean v0, p0, Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->isDrawing:Z

    if-eqz v0, :cond_1

    .line 218
    iget-object v0, p0, Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->mHandler:Landroid/os/Handler;

    iget v1, p0, Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->drawingDelayTime:I

    int-to-long v2, v1

    invoke-virtual {v0, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 223
    :cond_0
    :goto_0
    return-void

    .line 220
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->clearEffect()V

    goto :goto_0
.end method

.method public unlockDots()V
    .locals 6

    .prologue
    .line 113
    iget v3, p0, Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->dotMaxLimit:I

    iget-object v4, p0, Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->particleAliveList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    sub-int v2, v3, v4

    .line 114
    .local v2, "totalAdded":I
    iget v3, p0, Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->lastAddedX:F

    iget v4, p0, Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->lastAddedY:F

    iget v5, p0, Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->lastAddedColor:I

    invoke-virtual {p0, v2, v3, v4, v5}, Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->addDots(IFFI)V

    .line 116
    iget-object v3, p0, Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->particleAliveList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/visualeffect/lock/particle/Particle;

    .line 117
    .local v1, "particle":Lcom/samsung/android/visualeffect/lock/particle/Particle;
    iget v3, p0, Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->dotUnlockSpeed:I

    int-to-float v3, v3

    invoke-virtual {v1, v3}, Lcom/samsung/android/visualeffect/lock/particle/Particle;->unlock(F)V

    goto :goto_0

    .line 119
    .end local v1    # "particle":Lcom/samsung/android/visualeffect/lock/particle/Particle;
    :cond_0
    return-void
.end method
