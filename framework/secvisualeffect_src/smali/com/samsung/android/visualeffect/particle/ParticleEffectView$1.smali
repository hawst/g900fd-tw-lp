.class Lcom/samsung/android/visualeffect/particle/ParticleEffectView$1;
.super Landroid/os/Handler;
.source "ParticleEffectView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/visualeffect/particle/ParticleEffectView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/visualeffect/particle/ParticleEffectView;


# direct methods
.method constructor <init>(Lcom/samsung/android/visualeffect/particle/ParticleEffectView;)V
    .locals 0

    .prologue
    .line 153
    iput-object p1, p0, Lcom/samsung/android/visualeffect/particle/ParticleEffectView$1;->this$0:Lcom/samsung/android/visualeffect/particle/ParticleEffectView;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 9
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v5, 0x1

    const/4 v8, 0x0

    .line 155
    iget-object v4, p0, Lcom/samsung/android/visualeffect/particle/ParticleEffectView$1;->this$0:Lcom/samsung/android/visualeffect/particle/ParticleEffectView;

    # invokes: Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->isAvailableRect()Z
    invoke-static {v4}, Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->access$000(Lcom/samsung/android/visualeffect/particle/ParticleEffectView;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 156
    iget-object v4, p0, Lcom/samsung/android/visualeffect/particle/ParticleEffectView$1;->this$0:Lcom/samsung/android/visualeffect/particle/ParticleEffectView;

    # getter for: Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->drawingLeft:I
    invoke-static {v4}, Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->access$100(Lcom/samsung/android/visualeffect/particle/ParticleEffectView;)I

    move-result v4

    iget-object v5, p0, Lcom/samsung/android/visualeffect/particle/ParticleEffectView$1;->this$0:Lcom/samsung/android/visualeffect/particle/ParticleEffectView;

    # getter for: Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->drawingMargin:I
    invoke-static {v5}, Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->access$200(Lcom/samsung/android/visualeffect/particle/ParticleEffectView;)I

    move-result v5

    sub-int v1, v4, v5

    .line 157
    .local v1, "tL":I
    iget-object v4, p0, Lcom/samsung/android/visualeffect/particle/ParticleEffectView$1;->this$0:Lcom/samsung/android/visualeffect/particle/ParticleEffectView;

    # getter for: Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->drawingTop:I
    invoke-static {v4}, Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->access$300(Lcom/samsung/android/visualeffect/particle/ParticleEffectView;)I

    move-result v4

    iget-object v5, p0, Lcom/samsung/android/visualeffect/particle/ParticleEffectView$1;->this$0:Lcom/samsung/android/visualeffect/particle/ParticleEffectView;

    # getter for: Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->drawingMargin:I
    invoke-static {v5}, Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->access$200(Lcom/samsung/android/visualeffect/particle/ParticleEffectView;)I

    move-result v5

    sub-int v3, v4, v5

    .line 158
    .local v3, "tT":I
    iget-object v4, p0, Lcom/samsung/android/visualeffect/particle/ParticleEffectView$1;->this$0:Lcom/samsung/android/visualeffect/particle/ParticleEffectView;

    # getter for: Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->drawingRight:I
    invoke-static {v4}, Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->access$400(Lcom/samsung/android/visualeffect/particle/ParticleEffectView;)I

    move-result v4

    iget-object v5, p0, Lcom/samsung/android/visualeffect/particle/ParticleEffectView$1;->this$0:Lcom/samsung/android/visualeffect/particle/ParticleEffectView;

    # getter for: Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->drawingMargin:I
    invoke-static {v5}, Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->access$200(Lcom/samsung/android/visualeffect/particle/ParticleEffectView;)I

    move-result v5

    add-int v2, v4, v5

    .line 159
    .local v2, "tR":I
    iget-object v4, p0, Lcom/samsung/android/visualeffect/particle/ParticleEffectView$1;->this$0:Lcom/samsung/android/visualeffect/particle/ParticleEffectView;

    # getter for: Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->drawingBottom:I
    invoke-static {v4}, Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->access$500(Lcom/samsung/android/visualeffect/particle/ParticleEffectView;)I

    move-result v4

    iget-object v5, p0, Lcom/samsung/android/visualeffect/particle/ParticleEffectView$1;->this$0:Lcom/samsung/android/visualeffect/particle/ParticleEffectView;

    # getter for: Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->drawingMargin:I
    invoke-static {v5}, Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->access$200(Lcom/samsung/android/visualeffect/particle/ParticleEffectView;)I

    move-result v5

    add-int v0, v4, v5

    .line 160
    .local v0, "tB":I
    iget-object v4, p0, Lcom/samsung/android/visualeffect/particle/ParticleEffectView$1;->this$0:Lcom/samsung/android/visualeffect/particle/ParticleEffectView;

    invoke-virtual {v4, v1, v3, v2, v0}, Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->invalidate(IIII)V

    .line 164
    .end local v0    # "tB":I
    .end local v1    # "tL":I
    .end local v2    # "tR":I
    .end local v3    # "tT":I
    :goto_0
    iget-object v4, p0, Lcom/samsung/android/visualeffect/particle/ParticleEffectView$1;->this$0:Lcom/samsung/android/visualeffect/particle/ParticleEffectView;

    # getter for: Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->isDrawing:Z
    invoke-static {v4}, Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->access$600(Lcom/samsung/android/visualeffect/particle/ParticleEffectView;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/samsung/android/visualeffect/particle/ParticleEffectView$1;->this$0:Lcom/samsung/android/visualeffect/particle/ParticleEffectView;

    # getter for: Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->isPaused:Z
    invoke-static {v4}, Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->access$700(Lcom/samsung/android/visualeffect/particle/ParticleEffectView;)Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/samsung/android/visualeffect/particle/ParticleEffectView$1;->this$0:Lcom/samsung/android/visualeffect/particle/ParticleEffectView;

    iget-object v4, v4, Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->mHandler:Landroid/os/Handler;

    iget-object v5, p0, Lcom/samsung/android/visualeffect/particle/ParticleEffectView$1;->this$0:Lcom/samsung/android/visualeffect/particle/ParticleEffectView;

    # getter for: Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->drawingDelayTime:I
    invoke-static {v5}, Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->access$800(Lcom/samsung/android/visualeffect/particle/ParticleEffectView;)I

    move-result v5

    int-to-long v6, v5

    invoke-virtual {v4, v8, v6, v7}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 165
    :cond_0
    return-void

    .line 162
    :cond_1
    iget-object v4, p0, Lcom/samsung/android/visualeffect/particle/ParticleEffectView$1;->this$0:Lcom/samsung/android/visualeffect/particle/ParticleEffectView;

    invoke-virtual {v4, v8, v8, v5, v5}, Lcom/samsung/android/visualeffect/particle/ParticleEffectView;->invalidate(IIII)V

    goto :goto_0
.end method
