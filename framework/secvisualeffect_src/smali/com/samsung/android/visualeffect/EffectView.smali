.class public Lcom/samsung/android/visualeffect/EffectView;
.super Landroid/widget/FrameLayout;
.source "EffectView.java"

# interfaces
.implements Lcom/samsung/android/visualeffect/IEffectView;


# instance fields
.field private final TAG:Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field private mEffectType:I

.field private mView:Lcom/samsung/android/visualeffect/IEffectView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 26
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 18
    const-string v0, "EffectView"

    iput-object v0, p0, Lcom/samsung/android/visualeffect/EffectView;->TAG:Ljava/lang/String;

    .line 27
    iput-object p1, p0, Lcom/samsung/android/visualeffect/EffectView;->mContext:Landroid/content/Context;

    .line 28
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 37
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 18
    const-string v0, "EffectView"

    iput-object v0, p0, Lcom/samsung/android/visualeffect/EffectView;->TAG:Ljava/lang/String;

    .line 38
    iput-object p1, p0, Lcom/samsung/android/visualeffect/EffectView;->mContext:Landroid/content/Context;

    .line 39
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    .line 49
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 18
    const-string v0, "EffectView"

    iput-object v0, p0, Lcom/samsung/android/visualeffect/EffectView;->TAG:Ljava/lang/String;

    .line 50
    iput-object p1, p0, Lcom/samsung/android/visualeffect/EffectView;->mContext:Landroid/content/Context;

    .line 51
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I
    .param p4, "defStyleRes"    # I

    .prologue
    .line 63
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    .line 18
    const-string v0, "EffectView"

    iput-object v0, p0, Lcom/samsung/android/visualeffect/EffectView;->TAG:Ljava/lang/String;

    .line 64
    iput-object p1, p0, Lcom/samsung/android/visualeffect/EffectView;->mContext:Landroid/content/Context;

    .line 65
    return-void
.end method


# virtual methods
.method public clearScreen()V
    .locals 2

    .prologue
    .line 184
    iget-object v0, p0, Lcom/samsung/android/visualeffect/EffectView;->mView:Lcom/samsung/android/visualeffect/IEffectView;

    if-nez v0, :cond_0

    .line 185
    const-string v0, "EffectView"

    const-string v1, "clearScreen : mView is null"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 190
    :goto_0
    return-void

    .line 189
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/visualeffect/EffectView;->mView:Lcom/samsung/android/visualeffect/IEffectView;

    invoke-interface {v0}, Lcom/samsung/android/visualeffect/IEffectView;->clearScreen()V

    goto :goto_0
.end method

.method public getEffect()I
    .locals 3

    .prologue
    .line 106
    iget-object v0, p0, Lcom/samsung/android/visualeffect/EffectView;->mView:Lcom/samsung/android/visualeffect/IEffectView;

    if-nez v0, :cond_0

    .line 107
    const-string v0, "EffectView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getEffect : Current mView is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 108
    const/4 v0, -0x1

    .line 111
    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/samsung/android/visualeffect/EffectView;->mEffectType:I

    goto :goto_0
.end method

.method public handleCustomEvent(ILjava/util/HashMap;)V
    .locals 2
    .param p1, "cmd"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/HashMap",
            "<**>;)V"
        }
    .end annotation

    .prologue
    .line 169
    .local p2, "params":Ljava/util/HashMap;, "Ljava/util/HashMap<**>;"
    iget-object v0, p0, Lcom/samsung/android/visualeffect/EffectView;->mView:Lcom/samsung/android/visualeffect/IEffectView;

    if-nez v0, :cond_0

    .line 170
    const-string v0, "EffectView"

    const-string v1, "handleCustomEvent : mView is null"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 174
    :goto_0
    return-void

    .line 173
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/visualeffect/EffectView;->mView:Lcom/samsung/android/visualeffect/IEffectView;

    invoke-interface {v0, p1, p2}, Lcom/samsung/android/visualeffect/IEffectView;->handleCustomEvent(ILjava/util/HashMap;)V

    goto :goto_0
.end method

.method public handleTouchEvent(Landroid/view/MotionEvent;Landroid/view/View;)V
    .locals 2
    .param p1, "event"    # Landroid/view/MotionEvent;
    .param p2, "view"    # Landroid/view/View;

    .prologue
    .line 138
    iget-object v0, p0, Lcom/samsung/android/visualeffect/EffectView;->mView:Lcom/samsung/android/visualeffect/IEffectView;

    if-nez v0, :cond_0

    .line 139
    const-string v0, "EffectView"

    const-string v1, "handleTouchEvent : mView is null"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 143
    :goto_0
    return-void

    .line 142
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/visualeffect/EffectView;->mView:Lcom/samsung/android/visualeffect/IEffectView;

    invoke-interface {v0, p1, p2}, Lcom/samsung/android/visualeffect/IEffectView;->handleTouchEvent(Landroid/view/MotionEvent;Landroid/view/View;)V

    goto :goto_0
.end method

.method public init(Lcom/samsung/android/visualeffect/EffectDataObj;)V
    .locals 2
    .param p1, "data"    # Lcom/samsung/android/visualeffect/EffectDataObj;

    .prologue
    .line 117
    iget-object v0, p0, Lcom/samsung/android/visualeffect/EffectView;->mView:Lcom/samsung/android/visualeffect/IEffectView;

    if-nez v0, :cond_0

    .line 118
    const-string v0, "EffectView"

    const-string v1, "setInitValues : mView is null"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 122
    :goto_0
    return-void

    .line 121
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/visualeffect/EffectView;->mView:Lcom/samsung/android/visualeffect/IEffectView;

    invoke-interface {v0, p1}, Lcom/samsung/android/visualeffect/IEffectView;->init(Lcom/samsung/android/visualeffect/EffectDataObj;)V

    goto :goto_0
.end method

.method public onCommand(Ljava/lang/String;Ljava/util/HashMap;)V
    .locals 1
    .param p1, "cmd"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/HashMap",
            "<**>;)V"
        }
    .end annotation

    .prologue
    .line 150
    .local p2, "params":Ljava/util/HashMap;, "Ljava/util/HashMap<**>;"
    const-string v0, "clear"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 151
    iget-object v0, p0, Lcom/samsung/android/visualeffect/EffectView;->mView:Lcom/samsung/android/visualeffect/IEffectView;

    invoke-interface {v0}, Lcom/samsung/android/visualeffect/IEffectView;->clearScreen()V

    .line 156
    :goto_0
    return-void

    .line 154
    :cond_0
    const/4 v0, 0x3

    invoke-virtual {p0, v0, p2}, Lcom/samsung/android/visualeffect/EffectView;->handleCustomEvent(ILjava/util/HashMap;)V

    goto :goto_0
.end method

.method public reInit(Lcom/samsung/android/visualeffect/EffectDataObj;)V
    .locals 2
    .param p1, "data"    # Lcom/samsung/android/visualeffect/EffectDataObj;

    .prologue
    .line 127
    iget-object v0, p0, Lcom/samsung/android/visualeffect/EffectView;->mView:Lcom/samsung/android/visualeffect/IEffectView;

    if-nez v0, :cond_0

    .line 128
    const-string v0, "EffectView"

    const-string v1, "reInitAndValues : mView is null"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 132
    :goto_0
    return-void

    .line 131
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/visualeffect/EffectView;->mView:Lcom/samsung/android/visualeffect/IEffectView;

    invoke-interface {v0, p1}, Lcom/samsung/android/visualeffect/IEffectView;->reInit(Lcom/samsung/android/visualeffect/EffectDataObj;)V

    goto :goto_0
.end method

.method public removeEffect()V
    .locals 1

    .prologue
    .line 217
    invoke-virtual {p0}, Lcom/samsung/android/visualeffect/EffectView;->removeAllViews()V

    .line 218
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/visualeffect/EffectView;->mView:Lcom/samsung/android/visualeffect/IEffectView;

    .line 219
    return-void
.end method

.method public removeListener()V
    .locals 2

    .prologue
    .line 205
    iget-object v0, p0, Lcom/samsung/android/visualeffect/EffectView;->mView:Lcom/samsung/android/visualeffect/IEffectView;

    if-nez v0, :cond_0

    .line 206
    const-string v0, "EffectView"

    const-string v1, "removeListener : mView is null"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 210
    :goto_0
    return-void

    .line 209
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/visualeffect/EffectView;->mView:Lcom/samsung/android/visualeffect/IEffectView;

    invoke-interface {v0}, Lcom/samsung/android/visualeffect/IEffectView;->removeListener()V

    goto :goto_0
.end method

.method public setEffect(I)V
    .locals 3
    .param p1, "effect"    # I

    .prologue
    .line 88
    iget-object v0, p0, Lcom/samsung/android/visualeffect/EffectView;->mView:Lcom/samsung/android/visualeffect/IEffectView;

    if-eqz v0, :cond_0

    .line 89
    const-string v0, "EffectView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setEffect : Current mView is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/visualeffect/EffectView;->mView:Lcom/samsung/android/visualeffect/IEffectView;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 90
    invoke-virtual {p0}, Lcom/samsung/android/visualeffect/EffectView;->removeAllViews()V

    .line 93
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/visualeffect/EffectView;->mContext:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/samsung/android/visualeffect/InnerViewManager;->getInstance(Landroid/content/Context;I)Lcom/samsung/android/visualeffect/IEffectView;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/visualeffect/EffectView;->mView:Lcom/samsung/android/visualeffect/IEffectView;

    .line 94
    iget-object v0, p0, Lcom/samsung/android/visualeffect/EffectView;->mView:Lcom/samsung/android/visualeffect/IEffectView;

    check-cast v0, Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/samsung/android/visualeffect/EffectView;->addView(Landroid/view/View;)V

    .line 95
    iput p1, p0, Lcom/samsung/android/visualeffect/EffectView;->mEffectType:I

    .line 96
    return-void
.end method

.method public setEffect(Ljava/lang/String;)V
    .locals 4
    .param p1, "effect"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x4

    .line 71
    iget-object v0, p0, Lcom/samsung/android/visualeffect/EffectView;->mView:Lcom/samsung/android/visualeffect/IEffectView;

    if-eqz v0, :cond_0

    .line 72
    const-string v0, "EffectView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setEffect : Current mView is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/visualeffect/EffectView;->mView:Lcom/samsung/android/visualeffect/IEffectView;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 73
    invoke-virtual {p0}, Lcom/samsung/android/visualeffect/EffectView;->removeAllViews()V

    .line 76
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/visualeffect/EffectView;->mContext:Landroid/content/Context;

    invoke-static {v0, v3}, Lcom/samsung/android/visualeffect/InnerViewManager;->getInstance(Landroid/content/Context;I)Lcom/samsung/android/visualeffect/IEffectView;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/visualeffect/EffectView;->mView:Lcom/samsung/android/visualeffect/IEffectView;

    .line 77
    iget-object v0, p0, Lcom/samsung/android/visualeffect/EffectView;->mView:Lcom/samsung/android/visualeffect/IEffectView;

    check-cast v0, Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/samsung/android/visualeffect/EffectView;->addView(Landroid/view/View;)V

    .line 78
    iput v3, p0, Lcom/samsung/android/visualeffect/EffectView;->mEffectType:I

    .line 79
    return-void
.end method

.method public setListener(Lcom/samsung/android/visualeffect/IEffectListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/samsung/android/visualeffect/IEffectListener;

    .prologue
    .line 195
    iget-object v0, p0, Lcom/samsung/android/visualeffect/EffectView;->mView:Lcom/samsung/android/visualeffect/IEffectView;

    if-nez v0, :cond_0

    .line 196
    const-string v0, "EffectView"

    const-string v1, "setListener : mView is null"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 200
    :goto_0
    return-void

    .line 199
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/visualeffect/EffectView;->mView:Lcom/samsung/android/visualeffect/IEffectView;

    invoke-interface {v0, p1}, Lcom/samsung/android/visualeffect/IEffectView;->setListener(Lcom/samsung/android/visualeffect/IEffectListener;)V

    goto :goto_0
.end method
