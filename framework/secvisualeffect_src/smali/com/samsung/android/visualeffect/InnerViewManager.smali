.class public Lcom/samsung/android/visualeffect/InnerViewManager;
.super Ljava/lang/Object;
.source "InnerViewManager.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getInstance(Landroid/content/Context;I)Lcom/samsung/android/visualeffect/IEffectView;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "argv"    # I

    .prologue
    .line 23
    if-nez p1, :cond_0

    .line 24
    new-instance v0, Lcom/samsung/android/visualeffect/lock/abstracttile/AbstractTileEffect;

    invoke-direct {v0, p0}, Lcom/samsung/android/visualeffect/lock/abstracttile/AbstractTileEffect;-><init>(Landroid/content/Context;)V

    .line 60
    :goto_0
    return-object v0

    .line 26
    :cond_0
    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    .line 27
    new-instance v0, Lcom/samsung/android/visualeffect/lock/geometricmosaic/GeometricMosaicEffect;

    invoke-direct {v0, p0}, Lcom/samsung/android/visualeffect/lock/geometricmosaic/GeometricMosaicEffect;-><init>(Landroid/content/Context;)V

    goto :goto_0

    .line 29
    :cond_1
    const/4 v0, 0x2

    if-ne p1, v0, :cond_2

    .line 30
    new-instance v0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;

    invoke-direct {v0, p0}, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;-><init>(Landroid/content/Context;)V

    goto :goto_0

    .line 32
    :cond_2
    const/4 v0, 0x3

    if-ne p1, v0, :cond_3

    .line 33
    new-instance v0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;

    invoke-direct {v0, p0}, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;-><init>(Landroid/content/Context;)V

    goto :goto_0

    .line 35
    :cond_3
    const/4 v0, 0x4

    if-ne p1, v0, :cond_4

    .line 36
    new-instance v0, Lcom/samsung/android/visualeffect/lock/particle/ParticleEffect;

    invoke-direct {v0, p0}, Lcom/samsung/android/visualeffect/lock/particle/ParticleEffect;-><init>(Landroid/content/Context;)V

    goto :goto_0

    .line 38
    :cond_4
    const/4 v0, 0x5

    if-ne p1, v0, :cond_5

    .line 39
    new-instance v0, Lcom/samsung/android/visualeffect/lock/watercolor/WaterColorEffect;

    invoke-direct {v0, p0}, Lcom/samsung/android/visualeffect/lock/watercolor/WaterColorEffect;-><init>(Landroid/content/Context;)V

    goto :goto_0

    .line 41
    :cond_5
    const/4 v0, 0x6

    if-ne p1, v0, :cond_6

    .line 42
    new-instance v0, Lcom/samsung/android/visualeffect/lock/brilliantcut/BrilliantCutEffect;

    invoke-direct {v0, p0}, Lcom/samsung/android/visualeffect/lock/brilliantcut/BrilliantCutEffect;-><init>(Landroid/content/Context;)V

    goto :goto_0

    .line 44
    :cond_6
    const/4 v0, 0x7

    if-ne p1, v0, :cond_7

    .line 45
    new-instance v0, Lcom/samsung/android/visualeffect/lock/brilliantring/BrilliantRingEffect;

    invoke-direct {v0, p0}, Lcom/samsung/android/visualeffect/lock/brilliantring/BrilliantRingEffect;-><init>(Landroid/content/Context;)V

    goto :goto_0

    .line 47
    :cond_7
    const/16 v0, 0x8

    if-ne p1, v0, :cond_8

    .line 48
    new-instance v0, Lcom/samsung/android/visualeffect/lock/rippleink/RippleInkEffect;

    invoke-direct {v0, p0}, Lcom/samsung/android/visualeffect/lock/rippleink/RippleInkEffect;-><init>(Landroid/content/Context;)V

    goto :goto_0

    .line 50
    :cond_8
    const/16 v0, 0x9

    if-ne p1, v0, :cond_9

    .line 51
    new-instance v0, Lcom/samsung/android/visualeffect/lock/indigodiffusion/IndigoDiffusionEffect;

    invoke-direct {v0, p0}, Lcom/samsung/android/visualeffect/lock/indigodiffusion/IndigoDiffusionEffect;-><init>(Landroid/content/Context;)V

    goto :goto_0

    .line 53
    :cond_9
    const/16 v0, 0xa

    if-ne p1, v0, :cond_a

    .line 54
    new-instance v0, Lcom/samsung/android/visualeffect/lock/blind/BlindEffect;

    invoke-direct {v0, p0}, Lcom/samsung/android/visualeffect/lock/blind/BlindEffect;-><init>(Landroid/content/Context;)V

    goto :goto_0

    .line 56
    :cond_a
    const/16 v0, 0xb

    if-ne p1, v0, :cond_b

    .line 57
    new-instance v0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;

    invoke-direct {v0, p0}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;-><init>(Landroid/content/Context;)V

    goto :goto_0

    .line 60
    :cond_b
    const/4 v0, 0x0

    goto :goto_0
.end method
