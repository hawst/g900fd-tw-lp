.class public Lcom/samsung/android/visualeffect/EffectType;
.super Ljava/lang/Object;
.source "EffectType.java"


# static fields
.field public static final ABSTRACT_TILES:I = 0x0

.field public static final BLIND:I = 0xa

.field public static final BRILLIANT_CUT:I = 0x6

.field public static final BRILLIANT_RING:I = 0x7

.field public static final CIRCLE:I = 0x2

.field public static final GEOMETRIC_MOSAIC:I = 0x1

.field public static final INDIGO_DIFFUSE:I = 0x9

.field public static final LENSFLARE:I = 0xb

.field public static final PARTICLE:Ljava/lang/String; = "particle"

.field public static final PARTICLE_MUSIC:I = 0x4

.field public static final POPPING_COLOUR:I = 0x3

.field public static final RIPPLE_INK:I = 0x8

.field public static final WATERCOLOR:I = 0x5


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    return-void
.end method
