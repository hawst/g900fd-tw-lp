.class public Lcom/samsung/android/visualeffect/feature/SecVEFeature_EFFECT;
.super Ljava/lang/Object;
.source "SecVEFeature_EFFECT.java"


# static fields
.field public static final SEC_VE_FEATURE_EFFECT_POPPINGCOLOR_CPU_MIN_CLOCK_RESTRICT:Z = false

.field public static final SEC_VE_FEATURE_EFFECT_POPPINGCOLOR_CPU_MIN_CLOCK_RESTRICT_NUM:Ljava/lang/String; = "0"

.field public static final SEC_VE_FEATURE_EFFECT_POPPINGCOLOR_GPU_FREQ_RESTRICT:Z = false

.field public static final SEC_VE_FEATURE_EFFECT_POPPINGCOLOR_GPU_FREQ_RESTRICT_NUM:Ljava/lang/String; = "240000000"

.field public static final SEC_VE_FEATURE_EFFECT_RIPPLE_CPU_CLOCK_RESTRICT:Z = false

.field public static final SEC_VE_FEATURE_EFFECT_RIPPLE_CPU_CLOCK_RESTRICT_NUM:Ljava/lang/String; = "1574400"

.field public static final SEC_VE_FEATURE_EFFECT_RIPPLE_GPU_FREQ_RESTRICT:Z = false

.field public static final SEC_VE_FEATURE_EFFECT_RIPPLE_GPU_FREQ_RESTRICT_NUM:Ljava/lang/String; = "389000000"

.field public static final SEC_VE_FEATURE_EFFECT_TARGET_SHIP:Z = true

.field public static final SEC_VE_FEATURE_EFFECT_WATERCOLOR_CPU_CLOCK_RESTRICT:Z = false

.field public static final SEC_VE_FEATURE_EFFECT_WATERCOLOR_CPU_CLOCK_RESTRICT_NUM:Ljava/lang/String; = "1574400"

.field public static final SEC_VE_FEATURE_EFFECT_WATERCOLOR_GPU_FREQ_RESTRICT:Z = false

.field public static final SEC_VE_FEATURE_EFFECT_WATERCOLOR_GPU_FREQ_RESTRICT_NUM:Ljava/lang/String; = "389000000"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
