.class public Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;
.super Landroid/widget/FrameLayout;
.source "ParticleSpaceEffect.java"

# interfaces
.implements Lcom/samsung/android/visualeffect/IEffectView;


# instance fields
.field private final CREATED_DOTS_AMOUNT_AFFORDANCE:I

.field private final CREATED_DOTS_AMOUNT_DOWN:I

.field private final CREATED_DOTS_AMOUNT_MOVE:I

.field private final DBG:Z

.field private final SCREEN_ON_ANIMATION_DURATION:I

.field private final SCREEN_ON_BACKGROUND_SCALE:F

.field private final SCREEN_ON_WIDGET_SCALE:F

.field private final TAG:Ljava/lang/String;

.field private affordanceColor:I

.field private affordanceRunnable:Ljava/lang/Runnable;

.field private affordanceX:F

.field private affordanceY:F

.field private centerX:F

.field private centerY:F

.field private currentX:F

.field private currentY:F

.field private isLayerSet:Z

.field private isUnlocked:Z

.field private mBgBitmap:Landroid/graphics/Bitmap;

.field private mContext:Landroid/content/Context;

.field private particleEffect:Lcom/samsung/android/visualeffect/lock/particle/ParticleEffect;

.field private screenOnAnimation:Landroid/animation/ValueAnimator;

.field private screenOnAnimationValue:F

.field private stageHeight:F

.field private stageRatio:F

.field private stageWidth:F

.field private wallpaperWidget:Landroid/widget/FrameLayout;

.field private widgetLayout:Landroid/widget/FrameLayout;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 62
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 28
    const-string v0, "VisualEffectParticleEffect"

    iput-object v0, p0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->TAG:Ljava/lang/String;

    .line 30
    const/4 v0, 0x3

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->CREATED_DOTS_AMOUNT_MOVE:I

    .line 31
    const/16 v0, 0xf

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->CREATED_DOTS_AMOUNT_DOWN:I

    .line 32
    const/16 v0, 0x32

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->CREATED_DOTS_AMOUNT_AFFORDANCE:I

    .line 33
    const/16 v0, 0x2bc

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->SCREEN_ON_ANIMATION_DURATION:I

    .line 34
    const v0, 0x3f99999a    # 1.2f

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->SCREEN_ON_WIDGET_SCALE:F

    .line 35
    const v0, 0x3f866666    # 1.05f

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->SCREEN_ON_BACKGROUND_SCALE:F

    .line 36
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->DBG:Z

    .line 46
    iput-boolean v1, p0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->isLayerSet:Z

    .line 51
    iput-boolean v1, p0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->isUnlocked:Z

    .line 63
    const-string v0, "VisualEffectParticleEffect"

    const-string v1, "Constructor"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 64
    iput-object p1, p0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->mContext:Landroid/content/Context;

    .line 65
    invoke-direct {p0}, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->particleSpaceInit()V

    .line 66
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v1, 0x0

    .line 76
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 28
    const-string v0, "VisualEffectParticleEffect"

    iput-object v0, p0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->TAG:Ljava/lang/String;

    .line 30
    const/4 v0, 0x3

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->CREATED_DOTS_AMOUNT_MOVE:I

    .line 31
    const/16 v0, 0xf

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->CREATED_DOTS_AMOUNT_DOWN:I

    .line 32
    const/16 v0, 0x32

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->CREATED_DOTS_AMOUNT_AFFORDANCE:I

    .line 33
    const/16 v0, 0x2bc

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->SCREEN_ON_ANIMATION_DURATION:I

    .line 34
    const v0, 0x3f99999a    # 1.2f

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->SCREEN_ON_WIDGET_SCALE:F

    .line 35
    const v0, 0x3f866666    # 1.05f

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->SCREEN_ON_BACKGROUND_SCALE:F

    .line 36
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->DBG:Z

    .line 46
    iput-boolean v1, p0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->isLayerSet:Z

    .line 51
    iput-boolean v1, p0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->isUnlocked:Z

    .line 77
    const-string v0, "VisualEffectParticleEffect"

    const-string v1, "Constructor"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 78
    iput-object p1, p0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->mContext:Landroid/content/Context;

    .line 79
    invoke-direct {p0}, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->particleSpaceInit()V

    .line 80
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v1, 0x0

    .line 69
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 28
    const-string v0, "VisualEffectParticleEffect"

    iput-object v0, p0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->TAG:Ljava/lang/String;

    .line 30
    const/4 v0, 0x3

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->CREATED_DOTS_AMOUNT_MOVE:I

    .line 31
    const/16 v0, 0xf

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->CREATED_DOTS_AMOUNT_DOWN:I

    .line 32
    const/16 v0, 0x32

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->CREATED_DOTS_AMOUNT_AFFORDANCE:I

    .line 33
    const/16 v0, 0x2bc

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->SCREEN_ON_ANIMATION_DURATION:I

    .line 34
    const v0, 0x3f99999a    # 1.2f

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->SCREEN_ON_WIDGET_SCALE:F

    .line 35
    const v0, 0x3f866666    # 1.05f

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->SCREEN_ON_BACKGROUND_SCALE:F

    .line 36
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->DBG:Z

    .line 46
    iput-boolean v1, p0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->isLayerSet:Z

    .line 51
    iput-boolean v1, p0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->isUnlocked:Z

    .line 70
    const-string v0, "VisualEffectParticleEffect"

    const-string v1, "Constructor"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 71
    iput-object p1, p0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->mContext:Landroid/content/Context;

    .line 72
    invoke-direct {p0}, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->particleSpaceInit()V

    .line 73
    return-void
.end method

.method static synthetic access$002(Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;F)F
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;
    .param p1, "x1"    # F

    .prologue
    .line 26
    iput p1, p0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->screenOnAnimationValue:F

    return p1
.end method

.method static synthetic access$100(Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->animateScreenOn()V

    return-void
.end method

.method static synthetic access$200(Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->unlockFinished()V

    return-void
.end method

.method static synthetic access$300(Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;)F
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;

    .prologue
    .line 26
    iget v0, p0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->affordanceX:F

    return v0
.end method

.method static synthetic access$400(Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;)F
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;

    .prologue
    .line 26
    iget v0, p0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->affordanceY:F

    return v0
.end method

.method static synthetic access$500(Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;

    .prologue
    .line 26
    iget v0, p0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->affordanceColor:I

    return v0
.end method

.method static synthetic access$600(Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;)Lcom/samsung/android/visualeffect/lock/particle/ParticleEffect;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->particleEffect:Lcom/samsung/android/visualeffect/lock/particle/ParticleEffect;

    return-object v0
.end method

.method static synthetic access$702(Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;Ljava/lang/Runnable;)Ljava/lang/Runnable;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;
    .param p1, "x1"    # Ljava/lang/Runnable;

    .prologue
    .line 26
    iput-object p1, p0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->affordanceRunnable:Ljava/lang/Runnable;

    return-object p1
.end method

.method private animateScreenOn()V
    .locals 6

    .prologue
    const/high16 v5, 0x3f800000    # 1.0f

    .line 135
    const v3, 0x3e4cccd0    # 0.20000005f

    iget v4, p0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->screenOnAnimationValue:F

    mul-float/2addr v3, v4

    add-float v2, v3, v5

    .line 136
    .local v2, "widgetScale":F
    iget-object v3, p0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->widgetLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v3, v2}, Landroid/widget/FrameLayout;->setScaleX(F)V

    .line 137
    iget-object v3, p0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->widgetLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v3, v2}, Landroid/widget/FrameLayout;->setScaleY(F)V

    .line 139
    const v3, 0x3d4cccc0    # 0.049999952f

    iget v4, p0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->screenOnAnimationValue:F

    mul-float/2addr v3, v4

    add-float v1, v3, v5

    .line 140
    .local v1, "wallpaperScale":F
    iget-object v3, p0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->wallpaperWidget:Landroid/widget/FrameLayout;

    invoke-virtual {v3, v1}, Landroid/widget/FrameLayout;->setScaleX(F)V

    .line 141
    iget-object v3, p0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->wallpaperWidget:Landroid/widget/FrameLayout;

    invoke-virtual {v3, v1}, Landroid/widget/FrameLayout;->setScaleY(F)V

    .line 143
    iget v3, p0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->screenOnAnimationValue:F

    sub-float v0, v5, v3

    .line 144
    .local v0, "bgAlpha":F
    iget-object v3, p0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->widgetLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v3, v0}, Landroid/widget/FrameLayout;->setAlpha(F)V

    .line 145
    return-void
.end method

.method private clearEffect()V
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/high16 v4, 0x3f800000    # 1.0f

    .line 148
    const-string v0, "VisualEffectParticleEffect"

    const-string v1, "clearEffect"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 149
    iget v0, p0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->centerX:F

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->currentX:F

    .line 150
    iget v0, p0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->centerY:F

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->currentY:F

    .line 151
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->widgetLayout:Landroid/widget/FrameLayout;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->widgetLayout:Landroid/widget/FrameLayout;

    move-object v0, p0

    move v3, v2

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v6}, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->setViewProperties(Landroid/view/View;FFFFF)V

    .line 152
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->wallpaperWidget:Landroid/widget/FrameLayout;

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->wallpaperWidget:Landroid/widget/FrameLayout;

    move-object v0, p0

    move v3, v2

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v6}, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->setViewProperties(Landroid/view/View;FFFFF)V

    .line 154
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->particleEffect:Lcom/samsung/android/visualeffect/lock/particle/ParticleEffect;

    invoke-virtual {v0}, Lcom/samsung/android/visualeffect/lock/particle/ParticleEffect;->clearEffect()V

    .line 155
    return-void
.end method

.method private getColor(FF)I
    .locals 17
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 166
    const v4, 0xffffff

    .line 167
    .local v4, "color":I
    const/4 v14, 0x0

    cmpg-float v14, p1, v14

    if-lez v14, :cond_0

    move-object/from16 v0, p0

    iget v14, v0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->stageWidth:F

    cmpl-float v14, p1, v14

    if-lez v14, :cond_1

    :cond_0
    move v5, v4

    .line 209
    .end local v4    # "color":I
    .local v5, "color":I
    :goto_0
    return v5

    .line 168
    .end local v5    # "color":I
    .restart local v4    # "color":I
    :cond_1
    const/4 v14, 0x0

    cmpg-float v14, p2, v14

    if-lez v14, :cond_2

    move-object/from16 v0, p0

    iget v14, v0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->stageHeight:F

    cmpl-float v14, p2, v14

    if-lez v14, :cond_3

    :cond_2
    move v5, v4

    .end local v4    # "color":I
    .restart local v5    # "color":I
    goto :goto_0

    .line 170
    .end local v5    # "color":I
    .restart local v4    # "color":I
    :cond_3
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->mBgBitmap:Landroid/graphics/Bitmap;

    if-nez v14, :cond_4

    .line 171
    const-string v14, "VisualEffectParticleEffect"

    const-string v15, "getColor : mBgBitmap = null"

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_1
    move v5, v4

    .line 209
    .end local v4    # "color":I
    .restart local v5    # "color":I
    goto :goto_0

    .line 173
    .end local v5    # "color":I
    .restart local v4    # "color":I
    :cond_4
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->mBgBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v14}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    .line 174
    .local v3, "bitmapWidth":I
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->mBgBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v14}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    .line 175
    .local v1, "bitmapHeight":I
    int-to-float v14, v3

    int-to-float v15, v1

    div-float v2, v14, v15

    .line 177
    .local v2, "bitmapRatio":F
    const/4 v9, 0x0

    .line 178
    .local v9, "offsetX":I
    const/4 v10, 0x0

    .line 180
    .local v10, "offsetY":I
    move-object/from16 v0, p0

    iget v14, v0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->stageRatio:F

    cmpl-float v14, v2, v14

    if-lez v14, :cond_9

    .line 182
    int-to-float v14, v1

    move-object/from16 v0, p0

    iget v15, v0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->stageHeight:F

    div-float v11, v14, v15

    .line 183
    .local v11, "ratio":F
    move-object/from16 v0, p0

    iget v14, v0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->stageWidth:F

    mul-float v13, v14, v11

    .line 184
    .local v13, "resizedStageWidth":F
    int-to-float v12, v1

    .line 185
    .local v12, "resizedStageHeight":F
    int-to-float v14, v3

    sub-float/2addr v14, v13

    const/high16 v15, 0x40000000    # 2.0f

    div-float/2addr v14, v15

    float-to-int v9, v14

    .line 193
    :goto_2
    mul-float v14, p1, v11

    float-to-int v14, v14

    add-int v7, v9, v14

    .line 194
    .local v7, "finalX":I
    mul-float v14, p2, v11

    float-to-int v14, v14

    add-int v8, v10, v14

    .line 195
    .local v8, "finalY":I
    if-gez v7, :cond_5

    const/4 v7, 0x0

    .line 196
    :cond_5
    if-lt v7, v3, :cond_6

    add-int/lit8 v7, v3, -0x1

    .line 197
    :cond_6
    if-gez v8, :cond_7

    const/4 v8, 0x0

    .line 198
    :cond_7
    if-lt v8, v1, :cond_8

    add-int/lit8 v8, v1, -0x1

    .line 201
    :cond_8
    :try_start_0
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->mBgBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v14, v7, v8}, Landroid/graphics/Bitmap;->getPixel(II)I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    goto :goto_1

    .line 188
    .end local v7    # "finalX":I
    .end local v8    # "finalY":I
    .end local v11    # "ratio":F
    .end local v12    # "resizedStageHeight":F
    .end local v13    # "resizedStageWidth":F
    :cond_9
    int-to-float v14, v3

    move-object/from16 v0, p0

    iget v15, v0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->stageWidth:F

    div-float v11, v14, v15

    .line 189
    .restart local v11    # "ratio":F
    int-to-float v13, v3

    .line 190
    .restart local v13    # "resizedStageWidth":F
    move-object/from16 v0, p0

    iget v14, v0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->stageHeight:F

    mul-float v12, v14, v11

    .line 191
    .restart local v12    # "resizedStageHeight":F
    int-to-float v14, v1

    sub-float/2addr v14, v12

    const/high16 v15, 0x40000000    # 2.0f

    div-float/2addr v14, v15

    float-to-int v10, v14

    goto :goto_2

    .line 202
    .restart local v7    # "finalX":I
    .restart local v8    # "finalY":I
    :catch_0
    move-exception v6

    .line 203
    .local v6, "e":Ljava/lang/IllegalArgumentException;
    const-string v14, "VisualEffectParticleEffect"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "getColor : IllegalArgumentException = "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v6}, Ljava/lang/IllegalArgumentException;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 204
    const-string v14, "VisualEffectParticleEffect"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "getColor : bitmap = "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->mBgBitmap:Landroid/graphics/Bitmap;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " x "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->mBgBitmap:Landroid/graphics/Bitmap;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 205
    const-string v14, "VisualEffectParticleEffect"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "getColor : stageWidth = "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->stageWidth:F

    move/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ", stageHeight =  "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->stageHeight:F

    move/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 206
    const-string v14, "VisualEffectParticleEffect"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "getColor : x = "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move/from16 v0, p1

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ", y =  "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move/from16 v0, p2

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1
.end method

.method private particleSpaceInit()V
    .locals 2

    .prologue
    .line 93
    const-string v0, "VisualEffectParticleEffect"

    const-string v1, "particleSpaceInit"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 94
    invoke-direct {p0}, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->resetOrientation()V

    .line 96
    new-instance v0, Lcom/samsung/android/visualeffect/lock/particle/ParticleEffect;

    iget-object v1, p0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/samsung/android/visualeffect/lock/particle/ParticleEffect;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->particleEffect:Lcom/samsung/android/visualeffect/lock/particle/ParticleEffect;

    .line 97
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->particleEffect:Lcom/samsung/android/visualeffect/lock/particle/ParticleEffect;

    invoke-virtual {p0, v0}, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->addView(Landroid/view/View;)V

    .line 99
    invoke-direct {p0}, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->setAnimator()V

    .line 100
    return-void
.end method

.method private resetOrientation()V
    .locals 5

    .prologue
    const/high16 v4, 0x40000000    # 2.0f

    .line 103
    const-string v1, "VisualEffectParticleEffect"

    const-string v2, "resetOrientation"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 104
    invoke-virtual {p0}, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 105
    .local v0, "dm":Landroid/util/DisplayMetrics;
    iget v1, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v1, v1

    iput v1, p0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->stageWidth:F

    .line 106
    iget v1, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-float v1, v1

    iput v1, p0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->stageHeight:F

    .line 107
    const-string v1, "VisualEffectParticleEffect"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "stage : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->stageWidth:F

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " x "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->stageHeight:F

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 108
    iget v1, p0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->stageWidth:F

    div-float/2addr v1, v4

    iput v1, p0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->centerX:F

    .line 109
    iget v1, p0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->stageHeight:F

    div-float/2addr v1, v4

    iput v1, p0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->centerY:F

    .line 110
    iget v1, p0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->stageWidth:F

    iget v2, p0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->stageHeight:F

    div-float/2addr v1, v2

    iput v1, p0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->stageRatio:F

    .line 111
    iget-object v1, p0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->particleEffect:Lcom/samsung/android/visualeffect/lock/particle/ParticleEffect;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->particleEffect:Lcom/samsung/android/visualeffect/lock/particle/ParticleEffect;

    invoke-virtual {v1}, Lcom/samsung/android/visualeffect/lock/particle/ParticleEffect;->clearEffect()V

    .line 112
    :cond_0
    return-void
.end method

.method private screenOnAnimationStart()V
    .locals 2

    .prologue
    .line 83
    const-string v0, "VisualEffectParticleEffect"

    const-string v1, "screenOnAnimationStart"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 84
    iget-boolean v0, p0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->isLayerSet:Z

    if-eqz v0, :cond_0

    .line 85
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->screenOnAnimation:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 90
    :goto_0
    return-void

    .line 87
    :cond_0
    const-string v0, "VisualEffectParticleEffect"

    const-string v1, "isLayerSet = false"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private setAnimator()V
    .locals 4

    .prologue
    .line 122
    const/4 v0, 0x2

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->screenOnAnimation:Landroid/animation/ValueAnimator;

    .line 123
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->screenOnAnimation:Landroid/animation/ValueAnimator;

    new-instance v1, Landroid/view/animation/interpolator/CubicEaseOut;

    invoke-direct {v1}, Landroid/view/animation/interpolator/CubicEaseOut;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 124
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->screenOnAnimation:Landroid/animation/ValueAnimator;

    const-wide/16 v2, 0x2bc

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 125
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->screenOnAnimation:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect$1;

    invoke-direct {v1, p0}, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect$1;-><init>(Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 132
    return-void

    .line 122
    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data
.end method

.method private setBGBitmap(Landroid/graphics/Bitmap;)V
    .locals 3
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 261
    const-string v0, "VisualEffectParticleEffect"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setWallpaperBitmap : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 262
    const-string v0, "VisualEffectParticleEffect"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setWallpaperBitmap : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " x "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 263
    iput-object p1, p0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->mBgBitmap:Landroid/graphics/Bitmap;

    .line 264
    return-void
.end method

.method private setViewProperties(Landroid/view/View;FFFFF)V
    .locals 0
    .param p1, "obj"    # Landroid/view/View;
    .param p2, "x"    # F
    .param p3, "y"    # F
    .param p4, "scaleX"    # F
    .param p5, "scaleY"    # F
    .param p6, "alpha"    # F

    .prologue
    .line 158
    invoke-virtual {p1, p2}, Landroid/view/View;->setX(F)V

    .line 159
    invoke-virtual {p1, p3}, Landroid/view/View;->setY(F)V

    .line 160
    invoke-virtual {p1, p4}, Landroid/view/View;->setScaleX(F)V

    .line 161
    invoke-virtual {p1, p5}, Landroid/view/View;->setScaleY(F)V

    .line 162
    invoke-virtual {p1, p6}, Landroid/view/View;->setAlpha(F)V

    .line 163
    return-void
.end method

.method private showAffordanceEffect(JLandroid/graphics/Rect;)V
    .locals 3
    .param p1, "startDelay"    # J
    .param p3, "rect"    # Landroid/graphics/Rect;

    .prologue
    .line 267
    const-string v0, "VisualEffectParticleEffect"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "showUnlockAffordance : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p3, Landroid/graphics/Rect;->left:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p3, Landroid/graphics/Rect;->right:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p3, Landroid/graphics/Rect;->top:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p3, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", startDelay : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 269
    iget v0, p3, Landroid/graphics/Rect;->left:I

    iget v1, p3, Landroid/graphics/Rect;->right:I

    iget v2, p3, Landroid/graphics/Rect;->left:I

    sub-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    int-to-float v0, v0

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->affordanceX:F

    .line 270
    iget v0, p3, Landroid/graphics/Rect;->top:I

    iget v1, p3, Landroid/graphics/Rect;->bottom:I

    iget v2, p3, Landroid/graphics/Rect;->top:I

    sub-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    int-to-float v0, v0

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->affordanceY:F

    .line 271
    iget v0, p0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->affordanceX:F

    iget v1, p0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->affordanceY:F

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->getColor(FF)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->affordanceColor:I

    .line 272
    new-instance v0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect$3;

    invoke-direct {v0, p0}, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect$3;-><init>(Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;)V

    iput-object v0, p0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->affordanceRunnable:Ljava/lang/Runnable;

    .line 279
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->affordanceRunnable:Ljava/lang/Runnable;

    invoke-virtual {p0, v0, p1, p2}, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 280
    return-void
.end method

.method private unlock()V
    .locals 6

    .prologue
    const v5, 0x3f99999a    # 1.2f

    const v4, 0x3f866666    # 1.05f

    .line 213
    const-string v2, "VisualEffectParticleEffect"

    const-string v3, "unlock"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 214
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->isUnlocked:Z

    .line 215
    iget-object v2, p0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->particleEffect:Lcom/samsung/android/visualeffect/lock/particle/ParticleEffect;

    invoke-virtual {v2}, Lcom/samsung/android/visualeffect/lock/particle/ParticleEffect;->unlockDots()V

    .line 216
    const-wide/16 v0, 0x15e

    .line 218
    .local v0, "unlockDuration":J
    iget-object v2, p0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->wallpaperWidget:Landroid/widget/FrameLayout;

    invoke-virtual {v2}, Landroid/widget/FrameLayout;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/view/ViewPropertyAnimator;->scaleX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/view/ViewPropertyAnimator;->scaleY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    new-instance v3, Landroid/view/animation/interpolator/CubicEaseOut;

    invoke-direct {v3}, Landroid/view/animation/interpolator/CubicEaseOut;-><init>()V

    invoke-virtual {v2, v3}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    new-instance v3, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect$2;

    invoke-direct {v3, p0}, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect$2;-><init>(Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;)V

    invoke-virtual {v2, v3}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 239
    iget-object v2, p0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->widgetLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v2}, Landroid/widget/FrameLayout;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/view/ViewPropertyAnimator;->scaleX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/view/ViewPropertyAnimator;->scaleY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    new-instance v3, Landroid/view/animation/interpolator/CubicEaseOut;

    invoke-direct {v3}, Landroid/view/animation/interpolator/CubicEaseOut;-><init>()V

    invoke-virtual {v2, v3}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    .line 243
    return-void
.end method

.method private unlockFinished()V
    .locals 2

    .prologue
    .line 246
    const-string v0, "VisualEffectParticleEffect"

    const-string v1, "unlockFinished"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 247
    invoke-direct {p0}, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->clearEffect()V

    .line 248
    return-void
.end method


# virtual methods
.method public clearScreen()V
    .locals 0

    .prologue
    .line 356
    invoke-direct {p0}, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->clearEffect()V

    .line 357
    return-void
.end method

.method public handleCustomEvent(ILjava/util/HashMap;)V
    .locals 4
    .param p1, "cmd"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/HashMap",
            "<**>;)V"
        }
    .end annotation

    .prologue
    .line 299
    .local p2, "params":Ljava/util/HashMap;, "Ljava/util/HashMap<**>;"
    if-nez p1, :cond_1

    .line 300
    const-string v0, "BGBitmap"

    invoke-virtual {p2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-direct {p0, v0}, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->setBGBitmap(Landroid/graphics/Bitmap;)V

    .line 311
    :cond_0
    :goto_0
    return-void

    .line 302
    :cond_1
    const/4 v0, 0x1

    if-ne p1, v0, :cond_2

    .line 303
    const-string v0, "StartDelay"

    invoke-virtual {p2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-string v0, "Rect"

    invoke-virtual {p2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Rect;

    invoke-direct {p0, v2, v3, v0}, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->showAffordanceEffect(JLandroid/graphics/Rect;)V

    goto :goto_0

    .line 305
    :cond_2
    const/4 v0, 0x2

    if-ne p1, v0, :cond_3

    .line 306
    invoke-direct {p0}, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->unlock()V

    goto :goto_0

    .line 308
    :cond_3
    const/4 v0, 0x3

    if-ne p1, v0, :cond_0

    .line 309
    invoke-direct {p0}, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->screenOnAnimationStart()V

    goto :goto_0
.end method

.method public handleTouchEvent(Landroid/view/MotionEvent;Landroid/view/View;)V
    .locals 5
    .param p1, "event"    # Landroid/view/MotionEvent;
    .param p2, "view"    # Landroid/view/View;

    .prologue
    const/16 v4, 0xf

    .line 324
    iget-boolean v1, p0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->isLayerSet:Z

    if-nez v1, :cond_1

    .line 325
    const-string v1, "VisualEffectParticleEffect"

    const-string v2, "handleTouchEvent : isLayerSet is false"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 351
    :cond_0
    :goto_0
    return-void

    .line 328
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v1

    iput v1, p0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->currentX:F

    .line 329
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v1

    iput v1, p0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->currentY:F

    .line 330
    iget v1, p0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->currentX:F

    iget v2, p0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->currentY:F

    invoke-direct {p0, v1, v2}, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->getColor(FF)I

    move-result v0

    .line 332
    .local v0, "color":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v1

    if-nez v1, :cond_2

    .line 333
    const-string v1, "VisualEffectParticleEffect"

    const-string v2, "handleTouchEvent : ACTION_DOWN"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 334
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->isUnlocked:Z

    .line 336
    iget-object v1, p0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->particleEffect:Lcom/samsung/android/visualeffect/lock/particle/ParticleEffect;

    iget v2, p0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->currentX:F

    iget v3, p0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->currentY:F

    invoke-virtual {v1, v4, v2, v3, v0}, Lcom/samsung/android/visualeffect/lock/particle/ParticleEffect;->addDots(IFFI)V

    goto :goto_0

    .line 338
    :cond_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v1

    if-nez v1, :cond_3

    .line 340
    iget-boolean v1, p0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->isUnlocked:Z

    if-nez v1, :cond_0

    .line 341
    iget-object v1, p0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->particleEffect:Lcom/samsung/android/visualeffect/lock/particle/ParticleEffect;

    iget v2, p0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->currentX:F

    iget v3, p0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->currentY:F

    invoke-virtual {v1, v4, v2, v3, v0}, Lcom/samsung/android/visualeffect/lock/particle/ParticleEffect;->addDots(IFFI)V

    goto :goto_0

    .line 344
    :cond_3
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v1

    const/4 v2, 0x3

    if-ne v1, v2, :cond_0

    .line 346
    :cond_4
    const-string v1, "VisualEffectParticleEffect"

    const-string v2, "handleTouchEvent : ACTION_UP || ACTION_CANCEL"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 348
    iget v1, p0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->centerX:F

    iput v1, p0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->currentX:F

    .line 349
    iget v1, p0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->centerY:F

    iput v1, p0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->currentY:F

    goto :goto_0
.end method

.method public init(Lcom/samsung/android/visualeffect/EffectDataObj;)V
    .locals 3
    .param p1, "data"    # Lcom/samsung/android/visualeffect/EffectDataObj;

    .prologue
    .line 284
    iget-object v0, p1, Lcom/samsung/android/visualeffect/EffectDataObj;->poppingColorData:Lcom/samsung/android/visualeffect/lock/data/PoppingColorData;

    iget-object v0, v0, Lcom/samsung/android/visualeffect/lock/data/PoppingColorData;->widgetLayout:Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->widgetLayout:Landroid/widget/FrameLayout;

    .line 285
    iget-object v0, p1, Lcom/samsung/android/visualeffect/EffectDataObj;->poppingColorData:Lcom/samsung/android/visualeffect/lock/data/PoppingColorData;

    iget-object v0, v0, Lcom/samsung/android/visualeffect/lock/data/PoppingColorData;->wallpaperWidget:Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->wallpaperWidget:Landroid/widget/FrameLayout;

    .line 287
    const-string v0, "VisualEffectParticleEffect"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setInitValues : widgetLayout = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->widgetLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 289
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->isLayerSet:Z

    .line 290
    invoke-direct {p0}, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->clearEffect()V

    .line 291
    return-void
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 116
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 117
    const-string v0, "VisualEffectParticleEffect"

    const-string v1, "onConfigurationChanged"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 118
    invoke-direct {p0}, Lcom/samsung/android/visualeffect/lock/particle/ParticleSpaceEffect;->resetOrientation()V

    .line 119
    return-void
.end method

.method public reInit(Lcom/samsung/android/visualeffect/EffectDataObj;)V
    .locals 0
    .param p1, "data"    # Lcom/samsung/android/visualeffect/EffectDataObj;

    .prologue
    .line 295
    return-void
.end method

.method public removeListener()V
    .locals 0

    .prologue
    .line 319
    return-void
.end method

.method public setListener(Lcom/samsung/android/visualeffect/IEffectListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/android/visualeffect/IEffectListener;

    .prologue
    .line 315
    return-void
.end method
