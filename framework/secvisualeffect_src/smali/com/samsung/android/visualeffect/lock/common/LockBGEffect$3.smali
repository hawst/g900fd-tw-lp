.class Lcom/samsung/android/visualeffect/lock/common/LockBGEffect$3;
.super Ljava/lang/Object;
.source "LockBGEffect.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/visualeffect/lock/common/LockBGEffect;->handleCustomEvent(ILjava/util/HashMap;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/visualeffect/lock/common/LockBGEffect;

.field final synthetic val$cmdVal:I

.field final synthetic val$hashParam:Ljava/util/HashMap;


# direct methods
.method constructor <init>(Lcom/samsung/android/visualeffect/lock/common/LockBGEffect;ILjava/util/HashMap;)V
    .locals 0

    .prologue
    .line 128
    iput-object p1, p0, Lcom/samsung/android/visualeffect/lock/common/LockBGEffect$3;->this$0:Lcom/samsung/android/visualeffect/lock/common/LockBGEffect;

    iput p2, p0, Lcom/samsung/android/visualeffect/lock/common/LockBGEffect$3;->val$cmdVal:I

    iput-object p3, p0, Lcom/samsung/android/visualeffect/lock/common/LockBGEffect$3;->val$hashParam:Ljava/util/HashMap;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 131
    const-string v0, "TEST"

    const-string v1, "run!!!"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 132
    iget v0, p0, Lcom/samsung/android/visualeffect/lock/common/LockBGEffect$3;->val$cmdVal:I

    if-nez v0, :cond_1

    .line 133
    iget-object v1, p0, Lcom/samsung/android/visualeffect/lock/common/LockBGEffect$3;->this$0:Lcom/samsung/android/visualeffect/lock/common/LockBGEffect;

    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/LockBGEffect$3;->val$hashParam:Ljava/util/HashMap;

    const-string v2, "Bitmap"

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-virtual {v1, v0}, Lcom/samsung/android/visualeffect/lock/common/LockBGEffect;->setBGBitmap(Landroid/graphics/Bitmap;)V

    .line 144
    :cond_0
    :goto_0
    return-void

    .line 135
    :cond_1
    iget v0, p0, Lcom/samsung/android/visualeffect/lock/common/LockBGEffect$3;->val$cmdVal:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 136
    iget-object v1, p0, Lcom/samsung/android/visualeffect/lock/common/LockBGEffect$3;->this$0:Lcom/samsung/android/visualeffect/lock/common/LockBGEffect;

    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/LockBGEffect$3;->val$hashParam:Ljava/util/HashMap;

    const-string v2, "StartDelay"

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/LockBGEffect$3;->val$hashParam:Ljava/util/HashMap;

    const-string v4, "Rect"

    invoke-virtual {v0, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Rect;

    invoke-virtual {v1, v2, v3, v0}, Lcom/samsung/android/visualeffect/lock/common/LockBGEffect;->showAffordanceEffect(JLandroid/graphics/Rect;)V

    goto :goto_0

    .line 138
    :cond_2
    iget v0, p0, Lcom/samsung/android/visualeffect/lock/common/LockBGEffect$3;->val$cmdVal:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    .line 139
    const-string v0, "unlock"

    const-string v1, "lockBGEffect unlock"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 140
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/LockBGEffect$3;->this$0:Lcom/samsung/android/visualeffect/lock/common/LockBGEffect;

    invoke-virtual {v0}, Lcom/samsung/android/visualeffect/lock/common/LockBGEffect;->showUnlockEffect()V

    goto :goto_0

    .line 141
    :cond_3
    iget v0, p0, Lcom/samsung/android/visualeffect/lock/common/LockBGEffect$3;->val$cmdVal:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 142
    iget-object v2, p0, Lcom/samsung/android/visualeffect/lock/common/LockBGEffect$3;->this$0:Lcom/samsung/android/visualeffect/lock/common/LockBGEffect;

    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/LockBGEffect$3;->val$hashParam:Ljava/util/HashMap;

    const-string v1, "Nums"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [I

    check-cast v0, [I

    iget-object v1, p0, Lcom/samsung/android/visualeffect/lock/common/LockBGEffect$3;->val$hashParam:Ljava/util/HashMap;

    const-string v3, "Values"

    invoke-virtual {v1, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [F

    check-cast v1, [F

    invoke-virtual {v2, v0, v1}, Lcom/samsung/android/visualeffect/lock/common/LockBGEffect;->setParameters([I[F)V

    goto :goto_0
.end method
