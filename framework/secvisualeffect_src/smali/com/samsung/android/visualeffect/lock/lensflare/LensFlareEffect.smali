.class public Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;
.super Landroid/widget/FrameLayout;
.source "LensFlareEffect.java"

# interfaces
.implements Lcom/samsung/android/visualeffect/IEffectView;


# static fields
.field private static final sound_tap_path:Ljava/lang/String; = "/system/media/audio/ui/lens_flare_tap.ogg"

.field private static final sound_unlock_path:Ljava/lang/String; = "/system/media/audio/ui/lens_flare_unlock.ogg"


# instance fields
.field private final AFFORDANCE_OFF_DURATION:I

.field private final AFFORDANCE_ON_DURATION:I

.field private final DBG:Z

.field private final FADEOUT_ANIMATION_DURATION:I

.field private FINGER_HOVER_Y_OFFSET:I

.field private final FOG_MAX_ALPHA:F

.field private final FOG_ON_DURATION:I

.field private final HEXAGON_CIRCLE_TOTAL:I

.field private HEXAGON_TAP_TOTAL:I

.field private HEXAGON_TOTAL:I

.field private final HOVER_DURATION:I

.field private final HOVER_LIGHT_IN_DURATION:I

.field private final HOVER_LIGHT_OUT_DURATION:I

.field private MAX_ALPHA_DISTANCE:I

.field private PEN_HOVER_Y_OFFSET:I

.field private final SHOW_ANIMATION_DURATION:I

.field private final TAG:Ljava/lang/String;

.field private final TAP_ANIMATION_DURATION:I

.field private TAP_AREA_RADIUS:I

.field private final UNLOCK_ANIMATION_DURATION:I

.field private final UNLOCK_SOUND_PLAY_TIME:J

.field private final X_OFFSET:I

.field private Y_OFFSET:I

.field private affordanceAnimationValue:F

.field private affordanceOffAnimator:Landroid/animation/ValueAnimator;

.field private affordanceOnAnimator:Landroid/animation/ValueAnimator;

.field private affordancePoint:Landroid/graphics/Point;

.field private affordanceRunnable:Ljava/lang/Runnable;

.field private createdDelaytime:J

.field private currentX:F

.field private currentY:F

.field private defaultConfig:Landroid/graphics/Bitmap$Config;

.field private defaultInSampleSize:F

.field private distance:D

.field private distancePerMaxAlpha:F

.field private fadeOutAnimator:Landroid/animation/ValueAnimator;

.field private fadeoutAnimationValue:F

.field private fogAlpha:F

.field private fogAnimationValue:F

.field private fogOnAnimator:Landroid/animation/ValueAnimator;

.field private globalAlpha:F

.field private hexagon:[Lcom/samsung/android/visualeffect/common/ImageViewBlended;

.field private hexagonDistance:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private hexagonRes:[I

.field private hexagonRotation:[I

.field private hexagonScale:[F

.field private hexagon_blue_id:I

.field private hexagon_green_id:I

.field private hexagon_orange_id:I

.field private hoverAnimator:Landroid/animation/ValueAnimator;

.field private hoverLight1:Lcom/samsung/android/visualeffect/common/ImageViewBlended;

.field private hoverLightAnimationValue:F

.field private hoverLightInAnimator:Landroid/animation/ValueAnimator;

.field private hoverLightOutAnimator:Landroid/animation/ValueAnimator;

.field private hoverX:F

.field private hoverY:F

.field private hoverlight_id:I

.field private isBeforeInit:Z

.field private isPlayAffordance:Z

.field private isSoundEnable:Z

.field private isSystemSoundChecked:Z

.field private isTouched:Z

.field private lightFog:Lcom/samsung/android/visualeffect/common/ImageViewBlended;

.field private lightObj:Landroid/widget/FrameLayout;

.field private lightTap:Landroid/widget/FrameLayout;

.field private light_id:I

.field private longLight:Lcom/samsung/android/visualeffect/common/ImageViewBlended;

.field private long_light_id:I

.field private mContext:Landroid/content/Context;

.field private mFirstCreatedRunnable:Ljava/lang/Runnable;

.field private objAlpha:F

.field private objAnimationValue:F

.field private objAnimator:Landroid/animation/ValueAnimator;

.field private particle:Lcom/samsung/android/visualeffect/common/ImageViewBlended;

.field private particle_id:I

.field private rainbow:Lcom/samsung/android/visualeffect/common/ImageViewBlended;

.field private rainbow_id:I

.field private randomRotation:F

.field private releaseSoundRunnable:Ljava/lang/Runnable;

.field private ring:Lcom/samsung/android/visualeffect/common/ImageViewBlended;

.field private ring_id:I

.field private screenHeight:I

.field private screenWidth:I

.field private showStartX:F

.field private showStartY:F

.field private sound_tap:I

.field private sound_unlock:I

.field private soundpool:Landroid/media/SoundPool;

.field private tapAnimationValue:F

.field private tapAnimator:Landroid/animation/ValueAnimator;

.field private tapHexRandomPoint:[Landroid/graphics/Point;

.field private tapHexagon:[Lcom/samsung/android/visualeffect/common/ImageViewBlended;

.field private tapHexagonScale:[F

.field private tap_sound_id:I

.field private unlockAnimationValue:F

.field private unlockAnimator:Landroid/animation/ValueAnimator;

.field private unlock_sound_id:I

.field private vignetting:Landroid/widget/ImageView;

.field private vignettingAlpha:F

.field private vignetting_id:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/16 v5, 0x1f4

    const/16 v4, -0x50

    const/4 v3, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 147
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 91
    const-string v0, "LensFlare"

    iput-object v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->TAG:Ljava/lang/String;

    .line 93
    iput v2, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->HEXAGON_CIRCLE_TOTAL:I

    .line 94
    const/4 v0, 0x5

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->HEXAGON_TAP_TOTAL:I

    .line 95
    const v0, 0x3f19999a    # 0.6f

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->FOG_MAX_ALPHA:F

    .line 96
    const/16 v0, 0x5dc

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->MAX_ALPHA_DISTANCE:I

    .line 97
    const/16 v0, 0x1770

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->SHOW_ANIMATION_DURATION:I

    .line 98
    const/16 v0, 0xfa0

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->TAP_ANIMATION_DURATION:I

    .line 99
    const/16 v0, 0x4b0

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->UNLOCK_ANIMATION_DURATION:I

    .line 100
    const v0, 0x186a0

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->HOVER_DURATION:I

    .line 101
    iput v5, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->HOVER_LIGHT_IN_DURATION:I

    .line 102
    const/16 v0, 0x12c

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->HOVER_LIGHT_OUT_DURATION:I

    .line 103
    const/16 v0, 0xc8

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->AFFORDANCE_ON_DURATION:I

    .line 104
    const/16 v0, 0x44c

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->AFFORDANCE_OFF_DURATION:I

    .line 105
    const/16 v0, 0x258

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->TAP_AREA_RADIUS:I

    .line 106
    const/16 v0, 0x64

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->FOG_ON_DURATION:I

    .line 107
    iput v5, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->FADEOUT_ANIMATION_DURATION:I

    .line 108
    iput v2, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->X_OFFSET:I

    .line 109
    iput v4, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->Y_OFFSET:I

    .line 110
    iput v4, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->FINGER_HOVER_Y_OFFSET:I

    .line 111
    iput v2, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->PEN_HOVER_Y_OFFSET:I

    .line 113
    iput-object v3, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->soundpool:Landroid/media/SoundPool;

    .line 119
    iput-boolean v1, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->isSoundEnable:Z

    .line 120
    iput-boolean v1, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->DBG:Z

    .line 121
    iput-boolean v1, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->isBeforeInit:Z

    .line 122
    iput-object v3, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->mFirstCreatedRunnable:Ljava/lang/Runnable;

    .line 123
    const-wide/16 v0, 0x64

    iput-wide v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->createdDelaytime:J

    .line 124
    const-wide/16 v0, 0x7d0

    iput-wide v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->UNLOCK_SOUND_PLAY_TIME:J

    .line 125
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->affordancePoint:Landroid/graphics/Point;

    .line 128
    iput-boolean v2, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->isPlayAffordance:Z

    .line 129
    iput-boolean v2, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->isTouched:Z

    .line 130
    sget-object v0, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    iput-object v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->defaultConfig:Landroid/graphics/Bitmap$Config;

    .line 131
    const/high16 v0, 0x40000000    # 2.0f

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->defaultInSampleSize:F

    .line 132
    const v0, 0x3f4ccccd    # 0.8f

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->globalAlpha:F

    .line 148
    iput-object p1, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->mContext:Landroid/content/Context;

    .line 149
    iput-object v3, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->mFirstCreatedRunnable:Ljava/lang/Runnable;

    .line 150
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/16 v5, 0x1f4

    const/16 v4, -0x50

    const/4 v3, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 159
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 91
    const-string v0, "LensFlare"

    iput-object v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->TAG:Ljava/lang/String;

    .line 93
    iput v2, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->HEXAGON_CIRCLE_TOTAL:I

    .line 94
    const/4 v0, 0x5

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->HEXAGON_TAP_TOTAL:I

    .line 95
    const v0, 0x3f19999a    # 0.6f

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->FOG_MAX_ALPHA:F

    .line 96
    const/16 v0, 0x5dc

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->MAX_ALPHA_DISTANCE:I

    .line 97
    const/16 v0, 0x1770

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->SHOW_ANIMATION_DURATION:I

    .line 98
    const/16 v0, 0xfa0

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->TAP_ANIMATION_DURATION:I

    .line 99
    const/16 v0, 0x4b0

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->UNLOCK_ANIMATION_DURATION:I

    .line 100
    const v0, 0x186a0

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->HOVER_DURATION:I

    .line 101
    iput v5, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->HOVER_LIGHT_IN_DURATION:I

    .line 102
    const/16 v0, 0x12c

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->HOVER_LIGHT_OUT_DURATION:I

    .line 103
    const/16 v0, 0xc8

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->AFFORDANCE_ON_DURATION:I

    .line 104
    const/16 v0, 0x44c

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->AFFORDANCE_OFF_DURATION:I

    .line 105
    const/16 v0, 0x258

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->TAP_AREA_RADIUS:I

    .line 106
    const/16 v0, 0x64

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->FOG_ON_DURATION:I

    .line 107
    iput v5, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->FADEOUT_ANIMATION_DURATION:I

    .line 108
    iput v2, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->X_OFFSET:I

    .line 109
    iput v4, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->Y_OFFSET:I

    .line 110
    iput v4, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->FINGER_HOVER_Y_OFFSET:I

    .line 111
    iput v2, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->PEN_HOVER_Y_OFFSET:I

    .line 113
    iput-object v3, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->soundpool:Landroid/media/SoundPool;

    .line 119
    iput-boolean v1, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->isSoundEnable:Z

    .line 120
    iput-boolean v1, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->DBG:Z

    .line 121
    iput-boolean v1, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->isBeforeInit:Z

    .line 122
    iput-object v3, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->mFirstCreatedRunnable:Ljava/lang/Runnable;

    .line 123
    const-wide/16 v0, 0x64

    iput-wide v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->createdDelaytime:J

    .line 124
    const-wide/16 v0, 0x7d0

    iput-wide v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->UNLOCK_SOUND_PLAY_TIME:J

    .line 125
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->affordancePoint:Landroid/graphics/Point;

    .line 128
    iput-boolean v2, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->isPlayAffordance:Z

    .line 129
    iput-boolean v2, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->isTouched:Z

    .line 130
    sget-object v0, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    iput-object v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->defaultConfig:Landroid/graphics/Bitmap$Config;

    .line 131
    const/high16 v0, 0x40000000    # 2.0f

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->defaultInSampleSize:F

    .line 132
    const v0, 0x3f4ccccd    # 0.8f

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->globalAlpha:F

    .line 160
    iput-object p1, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->mContext:Landroid/content/Context;

    .line 161
    iput-object v3, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->mFirstCreatedRunnable:Ljava/lang/Runnable;

    .line 162
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/16 v5, 0x1f4

    const/16 v4, -0x50

    const/4 v3, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 153
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 91
    const-string v0, "LensFlare"

    iput-object v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->TAG:Ljava/lang/String;

    .line 93
    iput v2, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->HEXAGON_CIRCLE_TOTAL:I

    .line 94
    const/4 v0, 0x5

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->HEXAGON_TAP_TOTAL:I

    .line 95
    const v0, 0x3f19999a    # 0.6f

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->FOG_MAX_ALPHA:F

    .line 96
    const/16 v0, 0x5dc

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->MAX_ALPHA_DISTANCE:I

    .line 97
    const/16 v0, 0x1770

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->SHOW_ANIMATION_DURATION:I

    .line 98
    const/16 v0, 0xfa0

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->TAP_ANIMATION_DURATION:I

    .line 99
    const/16 v0, 0x4b0

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->UNLOCK_ANIMATION_DURATION:I

    .line 100
    const v0, 0x186a0

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->HOVER_DURATION:I

    .line 101
    iput v5, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->HOVER_LIGHT_IN_DURATION:I

    .line 102
    const/16 v0, 0x12c

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->HOVER_LIGHT_OUT_DURATION:I

    .line 103
    const/16 v0, 0xc8

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->AFFORDANCE_ON_DURATION:I

    .line 104
    const/16 v0, 0x44c

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->AFFORDANCE_OFF_DURATION:I

    .line 105
    const/16 v0, 0x258

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->TAP_AREA_RADIUS:I

    .line 106
    const/16 v0, 0x64

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->FOG_ON_DURATION:I

    .line 107
    iput v5, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->FADEOUT_ANIMATION_DURATION:I

    .line 108
    iput v2, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->X_OFFSET:I

    .line 109
    iput v4, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->Y_OFFSET:I

    .line 110
    iput v4, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->FINGER_HOVER_Y_OFFSET:I

    .line 111
    iput v2, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->PEN_HOVER_Y_OFFSET:I

    .line 113
    iput-object v3, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->soundpool:Landroid/media/SoundPool;

    .line 119
    iput-boolean v1, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->isSoundEnable:Z

    .line 120
    iput-boolean v1, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->DBG:Z

    .line 121
    iput-boolean v1, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->isBeforeInit:Z

    .line 122
    iput-object v3, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->mFirstCreatedRunnable:Ljava/lang/Runnable;

    .line 123
    const-wide/16 v0, 0x64

    iput-wide v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->createdDelaytime:J

    .line 124
    const-wide/16 v0, 0x7d0

    iput-wide v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->UNLOCK_SOUND_PLAY_TIME:J

    .line 125
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->affordancePoint:Landroid/graphics/Point;

    .line 128
    iput-boolean v2, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->isPlayAffordance:Z

    .line 129
    iput-boolean v2, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->isTouched:Z

    .line 130
    sget-object v0, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    iput-object v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->defaultConfig:Landroid/graphics/Bitmap$Config;

    .line 131
    const/high16 v0, 0x40000000    # 2.0f

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->defaultInSampleSize:F

    .line 132
    const v0, 0x3f4ccccd    # 0.8f

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->globalAlpha:F

    .line 154
    iput-object p1, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->mContext:Landroid/content/Context;

    .line 155
    iput-object v3, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->mFirstCreatedRunnable:Ljava/lang/Runnable;

    .line 156
    return-void
.end method

.method static synthetic access$002(Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;F)F
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;
    .param p1, "x1"    # F

    .prologue
    .line 41
    iput p1, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->objAnimationValue:F

    return p1
.end method

.method static synthetic access$100(Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->animatedDragPos()V

    return-void
.end method

.method static synthetic access$1000(Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->animatedHover()V

    return-void
.end method

.method static synthetic access$1102(Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;F)F
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;
    .param p1, "x1"    # F

    .prologue
    .line 41
    iput p1, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->hoverLightAnimationValue:F

    return p1
.end method

.method static synthetic access$1200(Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->animatedHoverLight()V

    return-void
.end method

.method static synthetic access$1300(Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;)F
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;

    .prologue
    .line 41
    iget v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->affordanceAnimationValue:F

    return v0
.end method

.method static synthetic access$1302(Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;F)F
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;
    .param p1, "x1"    # F

    .prologue
    .line 41
    iput p1, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->affordanceAnimationValue:F

    return p1
.end method

.method static synthetic access$1400(Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->animatedAffordance()V

    return-void
.end method

.method static synthetic access$1500(Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;)Landroid/animation/ValueAnimator;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->affordanceOnAnimator:Landroid/animation/ValueAnimator;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;Landroid/animation/Animator;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;
    .param p1, "x1"    # Landroid/animation/Animator;

    .prologue
    .line 41
    invoke-direct {p0, p1}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->cancelAnimator(Landroid/animation/Animator;)V

    return-void
.end method

.method static synthetic access$1700(Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;)Landroid/animation/ValueAnimator;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->affordanceOffAnimator:Landroid/animation/ValueAnimator;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->playUnlockAffordance()V

    return-void
.end method

.method static synthetic access$1902(Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;
    .param p1, "x1"    # Z

    .prologue
    .line 41
    iput-boolean p1, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->isPlayAffordance:Z

    return p1
.end method

.method static synthetic access$2002(Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;Ljava/lang/Runnable;)Ljava/lang/Runnable;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;
    .param p1, "x1"    # Ljava/lang/Runnable;

    .prologue
    .line 41
    iput-object p1, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->affordanceRunnable:Ljava/lang/Runnable;

    return-object p1
.end method

.method static synthetic access$202(Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;F)F
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;
    .param p1, "x1"    # F

    .prologue
    .line 41
    iput p1, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->fogAnimationValue:F

    return p1
.end method

.method static synthetic access$2100(Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->lensFlareinit()V

    return-void
.end method

.method static synthetic access$2200(Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;)Landroid/media/SoundPool;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->soundpool:Landroid/media/SoundPool;

    return-object v0
.end method

.method static synthetic access$2202(Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;Landroid/media/SoundPool;)Landroid/media/SoundPool;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;
    .param p1, "x1"    # Landroid/media/SoundPool;

    .prologue
    .line 41
    iput-object p1, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->soundpool:Landroid/media/SoundPool;

    return-object p1
.end method

.method static synthetic access$2302(Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;Ljava/lang/Runnable;)Ljava/lang/Runnable;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;
    .param p1, "x1"    # Ljava/lang/Runnable;

    .prologue
    .line 41
    iput-object p1, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->releaseSoundRunnable:Ljava/lang/Runnable;

    return-object p1
.end method

.method static synthetic access$300(Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->animatedDragAlpha()V

    return-void
.end method

.method static synthetic access$402(Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;F)F
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;
    .param p1, "x1"    # F

    .prologue
    .line 41
    iput p1, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->tapAnimationValue:F

    return p1
.end method

.method static synthetic access$500(Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->animatedTap()V

    return-void
.end method

.method static synthetic access$602(Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;F)F
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;
    .param p1, "x1"    # F

    .prologue
    .line 41
    iput p1, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->fadeoutAnimationValue:F

    return p1
.end method

.method static synthetic access$700(Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->animatedFadeOut()V

    return-void
.end method

.method static synthetic access$802(Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;F)F
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;
    .param p1, "x1"    # F

    .prologue
    .line 41
    iput p1, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->unlockAnimationValue:F

    return p1
.end method

.method static synthetic access$900(Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->animatedUnlock()V

    return-void
.end method

.method private animatedAffordance()V
    .locals 2

    .prologue
    .line 769
    iget v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->affordanceAnimationValue:F

    invoke-direct {p0, v0}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->getCorrectAlpha(F)F

    move-result v0

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->fogAlpha:F

    .line 770
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->lightFog:Lcom/samsung/android/visualeffect/common/ImageViewBlended;

    iget v1, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->fogAlpha:F

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->setAlphaAndVisibility(Landroid/view/View;F)V

    .line 771
    return-void
.end method

.method private animatedDragAlpha()V
    .locals 5

    .prologue
    .line 733
    iget v2, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->fogAnimationValue:F

    const/high16 v3, 0x3f800000    # 1.0f

    iget v4, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->distancePerMaxAlpha:F

    sub-float/2addr v3, v4

    mul-float/2addr v2, v3

    invoke-direct {p0, v2}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->getCorrectAlpha(F)F

    move-result v2

    iput v2, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->fogAlpha:F

    .line 734
    iget v2, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->fogAlpha:F

    iget v3, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->globalAlpha:F

    mul-float/2addr v2, v3

    iput v2, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->fogAlpha:F

    .line 735
    iget v2, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->distancePerMaxAlpha:F

    const/high16 v3, 0x40400000    # 3.0f

    mul-float/2addr v2, v3

    invoke-direct {p0, v2}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->getCorrectAlpha(F)F

    move-result v2

    iput v2, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->objAlpha:F

    .line 736
    iget v2, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->distancePerMaxAlpha:F

    const v3, 0x3fa66666    # 1.3f

    mul-float/2addr v2, v3

    invoke-direct {p0, v2}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->getCorrectAlpha(F)F

    move-result v2

    iput v2, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->vignettingAlpha:F

    .line 738
    iget-object v2, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->lightFog:Lcom/samsung/android/visualeffect/common/ImageViewBlended;

    iget v3, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->fogAlpha:F

    invoke-direct {p0, v2, v3}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->setAlphaAndVisibility(Landroid/view/View;F)V

    .line 739
    iget-object v2, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->vignetting:Landroid/widget/ImageView;

    iget v3, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->vignettingAlpha:F

    invoke-direct {p0, v2, v3}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->setAlphaAndVisibility(Landroid/view/View;F)V

    .line 741
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget v2, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->HEXAGON_TOTAL:I

    if-ge v1, v2, :cond_0

    .line 742
    iget-object v2, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->hexagon:[Lcom/samsung/android/visualeffect/common/ImageViewBlended;

    aget-object v0, v2, v1

    .line 743
    .local v0, "hex":Lcom/samsung/android/visualeffect/common/ImageViewBlended;
    iget v2, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->objAlpha:F

    invoke-direct {p0, v0, v2}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->setAlphaAndVisibility(Landroid/view/View;F)V

    .line 741
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 745
    .end local v0    # "hex":Lcom/samsung/android/visualeffect/common/ImageViewBlended;
    :cond_0
    return-void
.end method

.method private animatedDragPos()V
    .locals 12

    .prologue
    const/high16 v6, 0x3f800000    # 1.0f

    .line 717
    iget v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->distancePerMaxAlpha:F

    mul-float/2addr v0, v6

    add-float v11, v6, v0

    .line 718
    .local v11, "scale":F
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->lightFog:Lcom/samsung/android/visualeffect/common/ImageViewBlended;

    iget v2, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->defaultInSampleSize:F

    mul-float/2addr v2, v11

    invoke-virtual {v0, v2}, Lcom/samsung/android/visualeffect/common/ImageViewBlended;->setScaleX(F)V

    .line 719
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->lightFog:Lcom/samsung/android/visualeffect/common/ImageViewBlended;

    iget v2, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->defaultInSampleSize:F

    mul-float/2addr v2, v11

    invoke-virtual {v0, v2}, Lcom/samsung/android/visualeffect/common/ImageViewBlended;->setScaleY(F)V

    .line 720
    iget v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->objAnimationValue:F

    neg-float v0, v0

    const/high16 v2, 0x41f00000    # 30.0f

    mul-float/2addr v0, v2

    iget v2, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->distancePerMaxAlpha:F

    const/high16 v3, 0x43200000    # 160.0f

    mul-float/2addr v2, v3

    sub-float v10, v0, v2

    .line 721
    .local v10, "rotation":F
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->lightFog:Lcom/samsung/android/visualeffect/common/ImageViewBlended;

    invoke-virtual {v0, v10}, Lcom/samsung/android/visualeffect/common/ImageViewBlended;->setRotation(F)V

    .line 723
    iget-object v1, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->lightFog:Lcom/samsung/android/visualeffect/common/ImageViewBlended;

    iget v2, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->showStartX:F

    iget v3, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->showStartY:F

    iget v4, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->currentX:F

    iget v5, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->currentY:F

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->setCenterPos(Landroid/view/View;FFFFF)V

    .line 725
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_0
    iget v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->HEXAGON_TOTAL:I

    if-ge v9, v0, :cond_0

    .line 726
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->hexagon:[Lcom/samsung/android/visualeffect/common/ImageViewBlended;

    aget-object v1, v0, v9

    .line 727
    .local v1, "hex":Landroid/widget/ImageView;
    iget v2, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->showStartX:F

    iget v3, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->showStartY:F

    iget v4, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->currentX:F

    iget v5, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->currentY:F

    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->hexagonDistance:Ljava/util/ArrayList;

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v6

    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->hexagonScale:[F

    aget v7, v0, v9

    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->hexagonRotation:[I

    aget v8, v0, v9

    move-object v0, p0

    invoke-direct/range {v0 .. v8}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->setCenterPos(Landroid/view/View;FFFFFFI)V

    .line 725
    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    .line 730
    .end local v1    # "hex":Landroid/widget/ImageView;
    :cond_0
    return-void
.end method

.method private animatedFadeOut()V
    .locals 5

    .prologue
    .line 748
    iget-object v2, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->lightFog:Lcom/samsung/android/visualeffect/common/ImageViewBlended;

    iget v3, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->fogAlpha:F

    iget v4, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->fadeoutAnimationValue:F

    mul-float/2addr v3, v4

    invoke-direct {p0, v2, v3}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->setAlphaAndVisibility(Landroid/view/View;F)V

    .line 749
    iget-object v2, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->vignetting:Landroid/widget/ImageView;

    iget v3, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->vignettingAlpha:F

    iget v4, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->fadeoutAnimationValue:F

    mul-float/2addr v3, v4

    invoke-direct {p0, v2, v3}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->setAlphaAndVisibility(Landroid/view/View;F)V

    .line 751
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget v2, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->HEXAGON_TOTAL:I

    if-ge v1, v2, :cond_0

    .line 752
    iget-object v2, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->hexagon:[Lcom/samsung/android/visualeffect/common/ImageViewBlended;

    aget-object v0, v2, v1

    .line 753
    .local v0, "hex":Lcom/samsung/android/visualeffect/common/ImageViewBlended;
    iget v2, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->objAlpha:F

    iget v3, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->fadeoutAnimationValue:F

    mul-float/2addr v2, v3

    invoke-direct {p0, v0, v2}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->setAlphaAndVisibility(Landroid/view/View;F)V

    .line 751
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 755
    .end local v0    # "hex":Lcom/samsung/android/visualeffect/common/ImageViewBlended;
    :cond_0
    return-void
.end method

.method private animatedHover()V
    .locals 6

    .prologue
    const/high16 v5, 0x40400000    # 3.0f

    const/high16 v4, 0x40000000    # 2.0f

    .line 659
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->hoverLight1:Lcom/samsung/android/visualeffect/common/ImageViewBlended;

    iget-object v1, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->hoverLight1:Lcom/samsung/android/visualeffect/common/ImageViewBlended;

    invoke-virtual {v1}, Lcom/samsung/android/visualeffect/common/ImageViewBlended;->getX()F

    move-result v1

    iget v2, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->hoverX:F

    iget-object v3, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->hoverLight1:Lcom/samsung/android/visualeffect/common/ImageViewBlended;

    invoke-virtual {v3}, Lcom/samsung/android/visualeffect/common/ImageViewBlended;->getWidth()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v3, v4

    sub-float/2addr v2, v3

    iget-object v3, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->hoverLight1:Lcom/samsung/android/visualeffect/common/ImageViewBlended;

    invoke-virtual {v3}, Lcom/samsung/android/visualeffect/common/ImageViewBlended;->getX()F

    move-result v3

    sub-float/2addr v2, v3

    div-float/2addr v2, v5

    add-float/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/samsung/android/visualeffect/common/ImageViewBlended;->setX(F)V

    .line 660
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->hoverLight1:Lcom/samsung/android/visualeffect/common/ImageViewBlended;

    iget-object v1, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->hoverLight1:Lcom/samsung/android/visualeffect/common/ImageViewBlended;

    invoke-virtual {v1}, Lcom/samsung/android/visualeffect/common/ImageViewBlended;->getY()F

    move-result v1

    iget v2, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->hoverY:F

    iget-object v3, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->hoverLight1:Lcom/samsung/android/visualeffect/common/ImageViewBlended;

    invoke-virtual {v3}, Lcom/samsung/android/visualeffect/common/ImageViewBlended;->getHeight()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v3, v4

    sub-float/2addr v2, v3

    iget-object v3, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->hoverLight1:Lcom/samsung/android/visualeffect/common/ImageViewBlended;

    invoke-virtual {v3}, Lcom/samsung/android/visualeffect/common/ImageViewBlended;->getY()F

    move-result v3

    sub-float/2addr v2, v3

    div-float/2addr v2, v5

    add-float/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/samsung/android/visualeffect/common/ImageViewBlended;->setY(F)V

    .line 661
    return-void
.end method

.method private animatedHoverLight()V
    .locals 3

    .prologue
    .line 664
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->hoverLight1:Lcom/samsung/android/visualeffect/common/ImageViewBlended;

    iget v1, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->hoverLightAnimationValue:F

    iget v2, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->defaultInSampleSize:F

    mul-float/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/samsung/android/visualeffect/common/ImageViewBlended;->setScaleX(F)V

    .line 665
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->hoverLight1:Lcom/samsung/android/visualeffect/common/ImageViewBlended;

    iget v1, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->hoverLightAnimationValue:F

    iget v2, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->defaultInSampleSize:F

    mul-float/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/samsung/android/visualeffect/common/ImageViewBlended;->setScaleY(F)V

    .line 666
    return-void
.end method

.method private animatedTap()V
    .locals 19

    .prologue
    .line 670
    move-object/from16 v0, p0

    iget v15, v0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->tapAnimationValue:F

    const/high16 v16, 0x3f000000    # 0.5f

    cmpg-float v15, v15, v16

    if-gez v15, :cond_0

    const/high16 v1, 0x3f800000    # 1.0f

    .line 671
    .local v1, "alpha":F
    :goto_0
    move-object/from16 v0, p0

    iget v15, v0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->globalAlpha:F

    mul-float/2addr v1, v15

    .line 673
    const v15, 0x3e4ccccd    # 0.2f

    const v16, 0x3f4ccccd    # 0.8f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->tapAnimationValue:F

    move/from16 v17, v0

    mul-float v16, v16, v17

    add-float v2, v15, v16

    .line 674
    .local v2, "distanceScale":F
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_1
    move-object/from16 v0, p0

    iget v15, v0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->HEXAGON_TAP_TOTAL:I

    if-ge v4, v15, :cond_1

    .line 675
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->tapHexagon:[Lcom/samsung/android/visualeffect/common/ImageViewBlended;

    aget-object v3, v15, v4

    .line 676
    .local v3, "hex":Landroid/widget/ImageView;
    move-object/from16 v0, p0

    invoke-direct {v0, v3, v1}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->setAlphaAndVisibility(Landroid/view/View;F)V

    .line 678
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->tapHexagonScale:[F

    aget v15, v15, v4

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->tapAnimationValue:F

    move/from16 v16, v0

    const v17, 0x3f4ccccd    # 0.8f

    mul-float v16, v16, v17

    const v17, 0x3f333333    # 0.7f

    add-float v16, v16, v17

    mul-float v10, v15, v16

    .line 679
    .local v10, "scale":F
    move-object/from16 v0, p0

    iget v15, v0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->defaultInSampleSize:F

    mul-float/2addr v15, v10

    invoke-virtual {v3, v15}, Landroid/widget/ImageView;->setScaleX(F)V

    .line 680
    move-object/from16 v0, p0

    iget v15, v0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->defaultInSampleSize:F

    mul-float/2addr v15, v10

    invoke-virtual {v3, v15}, Landroid/widget/ImageView;->setScaleY(F)V

    .line 682
    move-object/from16 v0, p0

    iget v15, v0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->showStartX:F

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->tapHexRandomPoint:[Landroid/graphics/Point;

    move-object/from16 v16, v0

    aget-object v16, v16, v4

    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v16, v0

    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v16, v0

    mul-float v16, v16, v2

    add-float v13, v15, v16

    .line 683
    .local v13, "tx":F
    move-object/from16 v0, p0

    iget v15, v0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->showStartY:F

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->tapHexRandomPoint:[Landroid/graphics/Point;

    move-object/from16 v16, v0

    aget-object v16, v16, v4

    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v16, v0

    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v16, v0

    mul-float v16, v16, v2

    add-float v14, v15, v16

    .line 685
    .local v14, "ty":F
    invoke-virtual {v3}, Landroid/widget/ImageView;->getWidth()I

    move-result v15

    int-to-float v15, v15

    const/high16 v16, 0x40000000    # 2.0f

    div-float v15, v15, v16

    sub-float/2addr v13, v15

    .line 686
    invoke-virtual {v3}, Landroid/widget/ImageView;->getHeight()I

    move-result v15

    int-to-float v15, v15

    const/high16 v16, 0x40000000    # 2.0f

    div-float v15, v15, v16

    sub-float/2addr v14, v15

    .line 688
    invoke-virtual {v3, v13}, Landroid/widget/ImageView;->setX(F)V

    .line 689
    invoke-virtual {v3, v14}, Landroid/widget/ImageView;->setY(F)V

    .line 674
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_1

    .line 670
    .end local v1    # "alpha":F
    .end local v2    # "distanceScale":F
    .end local v3    # "hex":Landroid/widget/ImageView;
    .end local v4    # "i":I
    .end local v10    # "scale":F
    .end local v13    # "tx":F
    .end local v14    # "ty":F
    :cond_0
    const/high16 v15, 0x3f800000    # 1.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->tapAnimationValue:F

    move/from16 v16, v0

    const/high16 v17, 0x3f000000    # 0.5f

    sub-float v16, v16, v17

    const/high16 v17, 0x40000000    # 2.0f

    mul-float v16, v16, v17

    sub-float v1, v15, v16

    goto/16 :goto_0

    .line 693
    .restart local v1    # "alpha":F
    .restart local v2    # "distanceScale":F
    .restart local v4    # "i":I
    :cond_1
    move-object/from16 v0, p0

    iget v15, v0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->tapAnimationValue:F

    const v16, 0x3fe66666    # 1.8f

    mul-float v11, v15, v16

    .line 694
    .local v11, "tapAniamationParticleValue":F
    const/high16 v15, 0x3f000000    # 0.5f

    cmpg-float v15, v11, v15

    if-gez v15, :cond_2

    const/high16 v15, 0x3f800000    # 1.0f

    :goto_2
    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->getCorrectAlpha(F)F

    move-result v7

    .line 695
    .local v7, "prticleAalpha":F
    move-object/from16 v0, p0

    iget v15, v0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->tapAnimationValue:F

    const v16, 0x3f99999a    # 1.2f

    mul-float v6, v15, v16

    .line 696
    .local v6, "particleScale":F
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->particle:Lcom/samsung/android/visualeffect/common/ImageViewBlended;

    move-object/from16 v0, p0

    invoke-direct {v0, v15, v7}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->setAlphaAndVisibility(Landroid/view/View;F)V

    .line 697
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->particle:Lcom/samsung/android/visualeffect/common/ImageViewBlended;

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->defaultInSampleSize:F

    move/from16 v16, v0

    mul-float v16, v16, v6

    invoke-virtual/range {v15 .. v16}, Lcom/samsung/android/visualeffect/common/ImageViewBlended;->setScaleX(F)V

    .line 698
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->particle:Lcom/samsung/android/visualeffect/common/ImageViewBlended;

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->defaultInSampleSize:F

    move/from16 v16, v0

    mul-float v16, v16, v6

    invoke-virtual/range {v15 .. v16}, Lcom/samsung/android/visualeffect/common/ImageViewBlended;->setScaleY(F)V

    .line 701
    move-object/from16 v0, p0

    iget v15, v0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->tapAnimationValue:F

    const v16, 0x3fb33333    # 1.4f

    mul-float v12, v15, v16

    .line 702
    .local v12, "tapAniamationRingValue":F
    const/high16 v15, 0x3f000000    # 0.5f

    cmpg-float v15, v12, v15

    if-gez v15, :cond_3

    const/high16 v15, 0x3f800000    # 1.0f

    :goto_3
    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->getCorrectAlpha(F)F

    move-result v8

    .line 703
    .local v8, "ringAlpha":F
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->ring:Lcom/samsung/android/visualeffect/common/ImageViewBlended;

    move-object/from16 v0, p0

    invoke-direct {v0, v15, v8}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->setAlphaAndVisibility(Landroid/view/View;F)V

    .line 704
    const/high16 v15, 0x3f000000    # 0.5f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->tapAnimationValue:F

    move/from16 v16, v0

    add-float v9, v15, v16

    .line 705
    .local v9, "ringScale":F
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->ring:Lcom/samsung/android/visualeffect/common/ImageViewBlended;

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->defaultInSampleSize:F

    move/from16 v16, v0

    mul-float v16, v16, v9

    invoke-virtual/range {v15 .. v16}, Lcom/samsung/android/visualeffect/common/ImageViewBlended;->setScaleX(F)V

    .line 706
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->ring:Lcom/samsung/android/visualeffect/common/ImageViewBlended;

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->defaultInSampleSize:F

    move/from16 v16, v0

    mul-float v16, v16, v9

    invoke-virtual/range {v15 .. v16}, Lcom/samsung/android/visualeffect/common/ImageViewBlended;->setScaleY(F)V

    .line 709
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->longLight:Lcom/samsung/android/visualeffect/common/ImageViewBlended;

    move-object/from16 v0, p0

    invoke-direct {v0, v15, v8}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->setAlphaAndVisibility(Landroid/view/View;F)V

    .line 710
    const/high16 v15, 0x3fc00000    # 1.5f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->tapAnimationValue:F

    move/from16 v16, v0

    const/high16 v17, 0x40000000    # 2.0f

    mul-float v16, v16, v17

    add-float v5, v15, v16

    .line 711
    .local v5, "longScale":F
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->longLight:Lcom/samsung/android/visualeffect/common/ImageViewBlended;

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->defaultInSampleSize:F

    move/from16 v16, v0

    mul-float v16, v16, v5

    invoke-virtual/range {v15 .. v16}, Lcom/samsung/android/visualeffect/common/ImageViewBlended;->setScaleX(F)V

    .line 712
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->longLight:Lcom/samsung/android/visualeffect/common/ImageViewBlended;

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->defaultInSampleSize:F

    move/from16 v16, v0

    mul-float v16, v16, v5

    invoke-virtual/range {v15 .. v16}, Lcom/samsung/android/visualeffect/common/ImageViewBlended;->setScaleY(F)V

    .line 713
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->longLight:Lcom/samsung/android/visualeffect/common/ImageViewBlended;

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->randomRotation:F

    move/from16 v16, v0

    const/high16 v17, 0x41f00000    # 30.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->tapAnimationValue:F

    move/from16 v18, v0

    mul-float v17, v17, v18

    add-float v16, v16, v17

    invoke-virtual/range {v15 .. v16}, Lcom/samsung/android/visualeffect/common/ImageViewBlended;->setRotation(F)V

    .line 714
    return-void

    .line 694
    .end local v5    # "longScale":F
    .end local v6    # "particleScale":F
    .end local v7    # "prticleAalpha":F
    .end local v8    # "ringAlpha":F
    .end local v9    # "ringScale":F
    .end local v12    # "tapAniamationRingValue":F
    :cond_2
    const/high16 v15, 0x3f800000    # 1.0f

    const/high16 v16, 0x3f000000    # 0.5f

    sub-float v16, v11, v16

    const/high16 v17, 0x40000000    # 2.0f

    mul-float v16, v16, v17

    sub-float v15, v15, v16

    goto/16 :goto_2

    .line 702
    .restart local v6    # "particleScale":F
    .restart local v7    # "prticleAalpha":F
    .restart local v12    # "tapAniamationRingValue":F
    :cond_3
    const/high16 v15, 0x3f800000    # 1.0f

    const/high16 v16, 0x3f000000    # 0.5f

    sub-float v16, v12, v16

    const/high16 v17, 0x40000000    # 2.0f

    mul-float v16, v16, v17

    sub-float v15, v15, v16

    goto/16 :goto_3
.end method

.method private animatedUnlock()V
    .locals 9

    .prologue
    const/high16 v4, 0x40000000    # 2.0f

    const/high16 v3, 0x3f800000    # 1.0f

    const/high16 v2, 0x3f000000    # 0.5f

    .line 759
    iget v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->unlockAnimationValue:F

    const v1, 0x3fa66666    # 1.3f

    mul-float/2addr v0, v1

    add-float v8, v3, v0

    .line 760
    .local v8, "rainbowScale":F
    iget v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->unlockAnimationValue:F

    cmpg-float v0, v0, v2

    if-gez v0, :cond_0

    iget v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->unlockAnimationValue:F

    mul-float v7, v0, v4

    .line 762
    .local v7, "rainbowAlpha":F
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->rainbow:Lcom/samsung/android/visualeffect/common/ImageViewBlended;

    invoke-direct {p0, v0, v7}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->setAlphaAndVisibility(Landroid/view/View;F)V

    .line 763
    iget-object v1, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->rainbow:Lcom/samsung/android/visualeffect/common/ImageViewBlended;

    iget v2, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->showStartX:F

    iget v3, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->showStartY:F

    iget v4, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->currentX:F

    iget v5, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->currentY:F

    const v6, 0x3ecccccd    # 0.4f

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->setCenterPos(Landroid/view/View;FFFFF)V

    .line 764
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->rainbow:Lcom/samsung/android/visualeffect/common/ImageViewBlended;

    iget v1, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->defaultInSampleSize:F

    mul-float/2addr v1, v8

    invoke-virtual {v0, v1}, Lcom/samsung/android/visualeffect/common/ImageViewBlended;->setScaleX(F)V

    .line 765
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->rainbow:Lcom/samsung/android/visualeffect/common/ImageViewBlended;

    iget v1, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->defaultInSampleSize:F

    mul-float/2addr v1, v8

    invoke-virtual {v0, v1}, Lcom/samsung/android/visualeffect/common/ImageViewBlended;->setScaleY(F)V

    .line 766
    return-void

    .line 760
    .end local v7    # "rainbowAlpha":F
    :cond_0
    iget v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->unlockAnimationValue:F

    sub-float/2addr v0, v2

    mul-float/2addr v0, v4

    sub-float v7, v3, v0

    goto :goto_0
.end method

.method private calculateDistance(FF)V
    .locals 8
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    .line 783
    iget v2, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->showStartX:F

    sub-float v0, p1, v2

    .line 784
    .local v0, "diffX":F
    iget v2, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->showStartY:F

    sub-float v1, p2, v2

    .line 785
    .local v1, "diffY":F
    float-to-double v2, v0

    invoke-static {v2, v3, v6, v7}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    float-to-double v4, v1

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v4

    add-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    iput-wide v2, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->distance:D

    .line 786
    iget-wide v2, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->distance:D

    double-to-float v2, v2

    iget v3, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->MAX_ALPHA_DISTANCE:I

    int-to-float v3, v3

    div-float/2addr v2, v3

    iput v2, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->distancePerMaxAlpha:F

    .line 787
    return-void
.end method

.method private cancelAnimator(Landroid/animation/Animator;)V
    .locals 1
    .param p1, "animator"    # Landroid/animation/Animator;

    .prologue
    .line 871
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/animation/Animator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 872
    invoke-virtual {p1}, Landroid/animation/Animator;->cancel()V

    .line 873
    :cond_0
    return-void
.end method

.method private cleanUp()V
    .locals 4

    .prologue
    .line 951
    const-string v0, "LensFlare"

    const-string v1, "cleanUp"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 952
    iget-boolean v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->isBeforeInit:Z

    if-eqz v0, :cond_0

    .line 969
    :goto_0
    return-void

    .line 953
    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->cleanUpAllViews()V

    .line 954
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->isPlayAffordance:Z

    .line 956
    invoke-direct {p0}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->stopReleaseSound()V

    .line 957
    new-instance v0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect$13;

    invoke-direct {v0, p0}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect$13;-><init>(Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;)V

    iput-object v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->releaseSoundRunnable:Ljava/lang/Runnable;

    .line 968
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->releaseSoundRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x7d0

    invoke-virtual {p0, v0, v2, v3}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method private cleanUpAllViews()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 844
    iget-object v2, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->lightFog:Lcom/samsung/android/visualeffect/common/ImageViewBlended;

    invoke-direct {p0, v2, v3}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->setAlphaAndVisibility(Landroid/view/View;F)V

    .line 845
    iget-object v2, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->ring:Lcom/samsung/android/visualeffect/common/ImageViewBlended;

    invoke-direct {p0, v2, v3}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->setAlphaAndVisibility(Landroid/view/View;F)V

    .line 846
    iget-object v2, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->longLight:Lcom/samsung/android/visualeffect/common/ImageViewBlended;

    invoke-direct {p0, v2, v3}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->setAlphaAndVisibility(Landroid/view/View;F)V

    .line 847
    iget-object v2, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->rainbow:Lcom/samsung/android/visualeffect/common/ImageViewBlended;

    invoke-direct {p0, v2, v3}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->setAlphaAndVisibility(Landroid/view/View;F)V

    .line 848
    iget-object v2, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->particle:Lcom/samsung/android/visualeffect/common/ImageViewBlended;

    invoke-direct {p0, v2, v3}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->setAlphaAndVisibility(Landroid/view/View;F)V

    .line 849
    iget-object v2, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->vignetting:Landroid/widget/ImageView;

    invoke-direct {p0, v2, v3}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->setAlphaAndVisibility(Landroid/view/View;F)V

    .line 851
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget v2, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->HEXAGON_TOTAL:I

    if-ge v1, v2, :cond_0

    .line 852
    iget-object v2, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->hexagon:[Lcom/samsung/android/visualeffect/common/ImageViewBlended;

    aget-object v0, v2, v1

    .line 853
    .local v0, "hex":Lcom/samsung/android/visualeffect/common/ImageViewBlended;
    invoke-direct {p0, v0, v3}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->setAlphaAndVisibility(Landroid/view/View;F)V

    .line 851
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 855
    .end local v0    # "hex":Lcom/samsung/android/visualeffect/common/ImageViewBlended;
    :cond_0
    const/4 v1, 0x0

    :goto_1
    iget v2, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->HEXAGON_TAP_TOTAL:I

    if-ge v1, v2, :cond_1

    .line 856
    iget-object v2, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->tapHexagon:[Lcom/samsung/android/visualeffect/common/ImageViewBlended;

    aget-object v0, v2, v1

    .line 857
    .restart local v0    # "hex":Lcom/samsung/android/visualeffect/common/ImageViewBlended;
    invoke-direct {p0, v0, v3}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->setAlphaAndVisibility(Landroid/view/View;F)V

    .line 855
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 860
    .end local v0    # "hex":Lcom/samsung/android/visualeffect/common/ImageViewBlended;
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->tapAnimator:Landroid/animation/ValueAnimator;

    invoke-direct {p0, v2}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->cancelAnimator(Landroid/animation/Animator;)V

    .line 861
    iget-object v2, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->objAnimator:Landroid/animation/ValueAnimator;

    invoke-direct {p0, v2}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->cancelAnimator(Landroid/animation/Animator;)V

    .line 862
    iget-object v2, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->fadeOutAnimator:Landroid/animation/ValueAnimator;

    invoke-direct {p0, v2}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->cancelAnimator(Landroid/animation/Animator;)V

    .line 863
    iget-object v2, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->fogOnAnimator:Landroid/animation/ValueAnimator;

    invoke-direct {p0, v2}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->cancelAnimator(Landroid/animation/Animator;)V

    .line 864
    iget-object v2, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->unlockAnimator:Landroid/animation/ValueAnimator;

    invoke-direct {p0, v2}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->cancelAnimator(Landroid/animation/Animator;)V

    .line 865
    iget-object v2, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->hoverAnimator:Landroid/animation/ValueAnimator;

    invoke-direct {p0, v2}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->cancelAnimator(Landroid/animation/Animator;)V

    .line 866
    iget-object v2, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->affordanceOnAnimator:Landroid/animation/ValueAnimator;

    invoke-direct {p0, v2}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->cancelAnimator(Landroid/animation/Animator;)V

    .line 867
    iget-object v2, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->affordanceOffAnimator:Landroid/animation/ValueAnimator;

    invoke-direct {p0, v2}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->cancelAnimator(Landroid/animation/Animator;)V

    .line 868
    return-void
.end method

.method private getCorrectAlpha(F)F
    .locals 3
    .param p1, "alpha"    # F

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v0, 0x0

    .line 774
    cmpg-float v2, p1, v0

    if-gtz v2, :cond_1

    move p1, v0

    .line 779
    .end local p1    # "alpha":F
    :cond_0
    :goto_0
    return p1

    .line 776
    .restart local p1    # "alpha":F
    :cond_1
    cmpl-float v0, p1, v1

    if-ltz v0, :cond_0

    move p1, v1

    .line 777
    goto :goto_0
.end method

.method private getUnlockDelay()J
    .locals 2

    .prologue
    .line 993
    const-wide/16 v0, 0x12c

    return-wide v0
.end method

.method private handleHoverEvent(Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v3, 0x0

    .line 997
    iget-boolean v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->isBeforeInit:Z

    if-eqz v0, :cond_0

    .line 999
    const-string v0, "LensFlare"

    const-string v1, "Return handleTouchEvent!!! Becuase isBeforeInit is true"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1024
    :goto_0
    return v3

    .line 1003
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 1021
    :pswitch_1
    invoke-direct {p0}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->hoverExit()V

    goto :goto_0

    .line 1005
    :pswitch_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getSource()I

    move-result v0

    and-int/lit16 v0, v0, 0x4002

    const/16 v1, 0x4002

    if-eq v0, v1, :cond_1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getSource()I

    move-result v0

    and-int/lit16 v0, v0, 0x2002

    const/16 v1, 0x2002

    if-ne v0, v1, :cond_2

    .line 1007
    :cond_1
    iget v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->PEN_HOVER_Y_OFFSET:I

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->Y_OFFSET:I

    .line 1011
    :goto_1
    const-string v0, "LensFlare"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "InputDevice.SOURCE_STYLUS = 16386, Y_OFFSET = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->Y_OFFSET:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1012
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->hoverEnter(FF)V

    goto :goto_0

    .line 1009
    :cond_2
    iget v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->FINGER_HOVER_Y_OFFSET:I

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->Y_OFFSET:I

    goto :goto_1

    .line 1016
    :pswitch_3
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->hoverMove(FF)V

    goto :goto_0

    .line 1003
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method private hide()V
    .locals 1

    .prologue
    .line 619
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->isTouched:Z

    .line 620
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->fogOnAnimator:Landroid/animation/ValueAnimator;

    invoke-direct {p0, v0}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->cancelAnimator(Landroid/animation/Animator;)V

    .line 621
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->fadeOutAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 622
    return-void
.end method

.method private hoverEnter(FF)V
    .locals 1
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 637
    const/4 v0, 0x0

    add-float/2addr v0, p1

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->hoverX:F

    .line 638
    iget v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->Y_OFFSET:I

    int-to-float v0, v0

    add-float/2addr v0, p2

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->hoverY:F

    .line 640
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->hoverAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 641
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->hoverLightInAnimator:Landroid/animation/ValueAnimator;

    invoke-direct {p0, v0}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->cancelAnimator(Landroid/animation/Animator;)V

    .line 642
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->hoverLightOutAnimator:Landroid/animation/ValueAnimator;

    invoke-direct {p0, v0}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->cancelAnimator(Landroid/animation/Animator;)V

    .line 643
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->hoverLightInAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 644
    return-void
.end method

.method private hoverExit()V
    .locals 1

    .prologue
    .line 652
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->hoverAnimator:Landroid/animation/ValueAnimator;

    invoke-direct {p0, v0}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->cancelAnimator(Landroid/animation/Animator;)V

    .line 653
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->hoverLightInAnimator:Landroid/animation/ValueAnimator;

    invoke-direct {p0, v0}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->cancelAnimator(Landroid/animation/Animator;)V

    .line 654
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->hoverLightOutAnimator:Landroid/animation/ValueAnimator;

    invoke-direct {p0, v0}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->cancelAnimator(Landroid/animation/Animator;)V

    .line 655
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->hoverLightOutAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 656
    return-void
.end method

.method private hoverMove(FF)V
    .locals 1
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 647
    const/4 v0, 0x0

    add-float/2addr v0, p1

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->hoverX:F

    .line 648
    iget v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->Y_OFFSET:I

    int-to-float v0, v0

    add-float/2addr v0, p2

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->hoverY:F

    .line 649
    return-void
.end method

.method private lensFlareinit()V
    .locals 7

    .prologue
    .line 185
    invoke-virtual {p0}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->getChildCount()I

    move-result v4

    if-nez v4, :cond_1

    .line 187
    const-string v4, "LensFlare"

    const-string v5, "this.getChildCount() == 0"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 188
    invoke-virtual {p0}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 189
    .local v0, "dm":Landroid/util/DisplayMetrics;
    iget v4, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iput v4, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->screenWidth:I

    .line 190
    iget v4, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    iput v4, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->screenHeight:I

    .line 192
    iget v4, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->screenWidth:I

    iget v5, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->screenHeight:I

    if-ge v4, v5, :cond_2

    iget v3, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->screenWidth:I

    .line 194
    .local v3, "smallestWidth":I
    :goto_0
    const-string v4, "LensFlare"

    const-string v5, "lensFlareinit ============================"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 195
    const-string v4, "LensFlare"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "screenWidth : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->screenWidth:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 196
    const-string v4, "LensFlare"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "screenHeight : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->screenHeight:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 197
    const-string v4, "LensFlare"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "smallestWidth : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 200
    const-string v1, "0.8"

    .line 201
    .local v1, "opacity":Ljava/lang/String;
    const-string v4, "LensFlare"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "GLOBALCONFIG_LOCKSCREEN_LIGHT_OPACITY : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 202
    invoke-static {v1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v4

    iput v4, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->globalAlpha:F

    .line 204
    const/16 v4, 0x438

    if-eq v3, v4, :cond_0

    .line 205
    int-to-float v4, v3

    const/high16 v5, 0x44870000    # 1080.0f

    div-float v2, v4, v5

    .line 206
    .local v2, "ratio":F
    const-string v4, "LensFlare"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "ratio : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 207
    iget v4, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->Y_OFFSET:I

    int-to-float v4, v4

    mul-float/2addr v4, v2

    float-to-int v4, v4

    iput v4, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->Y_OFFSET:I

    .line 208
    iget v4, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->MAX_ALPHA_DISTANCE:I

    int-to-float v4, v4

    mul-float/2addr v4, v2

    float-to-int v4, v4

    iput v4, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->MAX_ALPHA_DISTANCE:I

    .line 209
    iget v4, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->TAP_AREA_RADIUS:I

    int-to-float v4, v4

    mul-float/2addr v4, v2

    float-to-int v4, v4

    iput v4, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->TAP_AREA_RADIUS:I

    .line 212
    .end local v2    # "ratio":F
    :cond_0
    const-string v4, "LensFlare"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Y_OFFSET : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->Y_OFFSET:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 213
    const-string v4, "LensFlare"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "MAX_ALPHA_DISTANCE : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->MAX_ALPHA_DISTANCE:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 214
    const-string v4, "LensFlare"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "TAP_AREA_RADIUS : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->TAP_AREA_RADIUS:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 216
    invoke-direct {p0}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->setLayout()V

    .line 217
    invoke-direct {p0}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->setHover()V

    .line 218
    invoke-direct {p0}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->setHexagon()V

    .line 219
    invoke-direct {p0}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->setTapHexagon()V

    .line 220
    invoke-direct {p0}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->setAnimator()V

    .line 222
    .end local v0    # "dm":Landroid/util/DisplayMetrics;
    .end local v1    # "opacity":Ljava/lang/String;
    .end local v3    # "smallestWidth":I
    :cond_1
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->isBeforeInit:Z

    .line 223
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->mFirstCreatedRunnable:Ljava/lang/Runnable;

    .line 224
    return-void

    .line 192
    .restart local v0    # "dm":Landroid/util/DisplayMetrics;
    :cond_2
    iget v3, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->screenHeight:I

    goto/16 :goto_0
.end method

.method private move(FF)V
    .locals 2
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 604
    iget-boolean v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->isTouched:Z

    if-nez v0, :cond_1

    .line 605
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->showLight(FF)V

    .line 616
    :cond_0
    :goto_0
    return-void

    .line 607
    :cond_1
    const/4 v0, 0x0

    add-float/2addr v0, p1

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->currentX:F

    .line 608
    iget v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->Y_OFFSET:I

    int-to-float v0, v0

    add-float/2addr v0, p2

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->currentY:F

    .line 609
    iget v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->currentX:F

    iget v1, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->currentY:F

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->calculateDistance(FF)V

    .line 611
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->fogOnAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-nez v0, :cond_2

    .line 612
    invoke-direct {p0}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->animatedDragAlpha()V

    .line 613
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->objAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-nez v0, :cond_0

    .line 614
    invoke-direct {p0}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->animatedDragPos()V

    goto :goto_0
.end method

.method private playLockSound()V
    .locals 2

    .prologue
    .line 923
    const-string v0, "LensFlare"

    const-string v1, "playSound"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 924
    iget v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->sound_unlock:I

    invoke-direct {p0, v0}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->playSound(I)V

    .line 925
    return-void
.end method

.method private playSound(I)V
    .locals 7
    .param p1, "soundId"    # I

    .prologue
    const/4 v4, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    .line 565
    iget-boolean v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->isSoundEnable:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->isSystemSoundChecked:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->soundpool:Landroid/media/SoundPool;

    if-eqz v0, :cond_0

    .line 566
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->soundpool:Landroid/media/SoundPool;

    move v1, p1

    move v3, v2

    move v5, v4

    move v6, v2

    invoke-virtual/range {v0 .. v6}, Landroid/media/SoundPool;->play(IFFIIF)I

    .line 568
    :cond_0
    return-void
.end method

.method private playUnlockAffordance()V
    .locals 7

    .prologue
    const/high16 v6, 0x3f800000    # 1.0f

    .line 896
    iget-boolean v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->isBeforeInit:Z

    if-eqz v0, :cond_0

    .line 912
    :goto_0
    return-void

    .line 898
    :cond_0
    const-string v0, "LensFlare"

    const-string v1, "playUnlockAffordance"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 899
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->affordancePoint:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->x:I

    int-to-float v0, v0

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->showStartX:F

    .line 900
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->affordancePoint:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->y:I

    int-to-float v0, v0

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->showStartY:F

    .line 902
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->setHexagonRandomTarget(Z)V

    .line 904
    iget-object v1, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->ring:Lcom/samsung/android/visualeffect/common/ImageViewBlended;

    iget v2, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->showStartX:F

    iget v3, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->showStartY:F

    iget v4, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->showStartX:F

    iget v5, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->showStartY:F

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->setCenterPos(Landroid/view/View;FFFFF)V

    .line 905
    iget-object v1, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->longLight:Lcom/samsung/android/visualeffect/common/ImageViewBlended;

    iget v2, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->showStartX:F

    iget v3, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->showStartY:F

    iget v4, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->showStartX:F

    iget v5, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->showStartY:F

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->setCenterPos(Landroid/view/View;FFFFF)V

    .line 906
    iget-object v1, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->particle:Lcom/samsung/android/visualeffect/common/ImageViewBlended;

    iget v2, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->showStartX:F

    iget v3, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->showStartY:F

    iget v4, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->showStartX:F

    iget v5, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->showStartY:F

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->setCenterPos(Landroid/view/View;FFFFF)V

    .line 907
    iget-object v1, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->lightFog:Lcom/samsung/android/visualeffect/common/ImageViewBlended;

    iget v2, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->showStartX:F

    iget v3, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->showStartY:F

    iget v4, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->showStartX:F

    iget v5, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->showStartY:F

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->setCenterPos(Landroid/view/View;FFFFF)V

    .line 909
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->tapAnimator:Landroid/animation/ValueAnimator;

    invoke-direct {p0, v0}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->cancelAnimator(Landroid/animation/Animator;)V

    .line 910
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->tapAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 911
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->affordanceOnAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    goto :goto_0
.end method

.method private reset()V
    .locals 2

    .prologue
    .line 979
    const-string v0, "LensFlare"

    const-string v1, "reset"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 980
    iget-boolean v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->isBeforeInit:Z

    if-eqz v0, :cond_0

    .line 985
    :goto_0
    return-void

    .line 982
    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->cleanUpAllViews()V

    .line 983
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->isPlayAffordance:Z

    .line 984
    invoke-direct {p0}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->stopUnlockAffordance()V

    goto :goto_0
.end method

.method private screenTurnedOn()V
    .locals 2

    .prologue
    .line 988
    const-string v0, "LensFlare"

    const-string v1, "screenTurnedOn"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 989
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->isPlayAffordance:Z

    .line 990
    return-void
.end method

.method private setAlphaAndVisibility(Landroid/view/View;F)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .param p2, "alpha"    # F

    .prologue
    const/16 v0, 0x8

    .line 832
    const/4 v1, 0x0

    cmpl-float v1, p2, v1

    if-nez v1, :cond_2

    .line 833
    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-eq v1, v0, :cond_1

    .line 834
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x4

    :cond_0
    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 841
    :cond_1
    :goto_0
    return-void

    .line 837
    :cond_2
    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_3

    .line 838
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 839
    :cond_3
    invoke-virtual {p1, p2}, Landroid/view/View;->setAlpha(F)V

    goto :goto_0
.end method

.method private setAnimator()V
    .locals 8

    .prologue
    const-wide/16 v6, 0x1f4

    const/4 v4, 0x2

    .line 452
    new-array v0, v4, [F

    fill-array-data v0, :array_0

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->objAnimator:Landroid/animation/ValueAnimator;

    .line 453
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->objAnimator:Landroid/animation/ValueAnimator;

    new-instance v1, Landroid/view/animation/interpolator/QuintEaseOut;

    invoke-direct {v1}, Landroid/view/animation/interpolator/QuintEaseOut;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 454
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->objAnimator:Landroid/animation/ValueAnimator;

    const-wide/16 v2, 0x1770

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 455
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->objAnimator:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect$1;

    invoke-direct {v1, p0}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect$1;-><init>(Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 463
    new-array v0, v4, [F

    fill-array-data v0, :array_1

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->fogOnAnimator:Landroid/animation/ValueAnimator;

    .line 464
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->fogOnAnimator:Landroid/animation/ValueAnimator;

    new-instance v1, Landroid/view/animation/interpolator/QuintEaseOut;

    invoke-direct {v1}, Landroid/view/animation/interpolator/QuintEaseOut;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 465
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->fogOnAnimator:Landroid/animation/ValueAnimator;

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 466
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->fogOnAnimator:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect$2;

    invoke-direct {v1, p0}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect$2;-><init>(Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 474
    new-array v0, v4, [F

    fill-array-data v0, :array_2

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->tapAnimator:Landroid/animation/ValueAnimator;

    .line 475
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->tapAnimator:Landroid/animation/ValueAnimator;

    new-instance v1, Landroid/view/animation/interpolator/QuintEaseOut;

    invoke-direct {v1}, Landroid/view/animation/interpolator/QuintEaseOut;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 476
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->tapAnimator:Landroid/animation/ValueAnimator;

    const-wide/16 v2, 0xfa0

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 477
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->tapAnimator:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect$3;

    invoke-direct {v1, p0}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect$3;-><init>(Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 485
    new-array v0, v4, [F

    fill-array-data v0, :array_3

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->fadeOutAnimator:Landroid/animation/ValueAnimator;

    .line 486
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->fadeOutAnimator:Landroid/animation/ValueAnimator;

    new-instance v1, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v1}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 487
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->fadeOutAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0, v6, v7}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 488
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->fadeOutAnimator:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect$4;

    invoke-direct {v1, p0}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect$4;-><init>(Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 496
    new-array v0, v4, [F

    fill-array-data v0, :array_4

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->unlockAnimator:Landroid/animation/ValueAnimator;

    .line 497
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->unlockAnimator:Landroid/animation/ValueAnimator;

    new-instance v1, Landroid/view/animation/interpolator/QuintEaseOut;

    invoke-direct {v1}, Landroid/view/animation/interpolator/QuintEaseOut;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 498
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->unlockAnimator:Landroid/animation/ValueAnimator;

    const-wide/16 v2, 0x4b0

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 499
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->unlockAnimator:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect$5;

    invoke-direct {v1, p0}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect$5;-><init>(Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 507
    new-array v0, v4, [F

    fill-array-data v0, :array_5

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->hoverAnimator:Landroid/animation/ValueAnimator;

    .line 508
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->hoverAnimator:Landroid/animation/ValueAnimator;

    new-instance v1, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v1}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 509
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->hoverAnimator:Landroid/animation/ValueAnimator;

    const/16 v1, 0x2710

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 510
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->hoverAnimator:Landroid/animation/ValueAnimator;

    const-wide/32 v2, 0x186a0

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 511
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->hoverAnimator:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect$6;

    invoke-direct {v1, p0}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect$6;-><init>(Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 518
    new-array v0, v4, [F

    fill-array-data v0, :array_6

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->hoverLightInAnimator:Landroid/animation/ValueAnimator;

    .line 519
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->hoverLightInAnimator:Landroid/animation/ValueAnimator;

    new-instance v1, Landroid/view/animation/interpolator/BackEaseOut;

    const/high16 v2, 0x41000000    # 8.0f

    invoke-direct {v1, v2}, Landroid/view/animation/interpolator/BackEaseOut;-><init>(F)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 520
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->hoverLightInAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0, v6, v7}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 521
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->hoverLightInAnimator:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect$7;

    invoke-direct {v1, p0}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect$7;-><init>(Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 528
    new-array v0, v4, [F

    fill-array-data v0, :array_7

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->hoverLightOutAnimator:Landroid/animation/ValueAnimator;

    .line 529
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->hoverLightOutAnimator:Landroid/animation/ValueAnimator;

    new-instance v1, Landroid/view/animation/interpolator/QuintEaseOut;

    invoke-direct {v1}, Landroid/view/animation/interpolator/QuintEaseOut;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 530
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->hoverLightOutAnimator:Landroid/animation/ValueAnimator;

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 531
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->hoverLightOutAnimator:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect$8;

    invoke-direct {v1, p0}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect$8;-><init>(Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 539
    new-array v0, v4, [F

    fill-array-data v0, :array_8

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->affordanceOnAnimator:Landroid/animation/ValueAnimator;

    .line 540
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->affordanceOnAnimator:Landroid/animation/ValueAnimator;

    new-instance v1, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v1}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 541
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->affordanceOnAnimator:Landroid/animation/ValueAnimator;

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 542
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->affordanceOnAnimator:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect$9;

    invoke-direct {v1, p0}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect$9;-><init>(Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 553
    new-array v0, v4, [F

    fill-array-data v0, :array_9

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->affordanceOffAnimator:Landroid/animation/ValueAnimator;

    .line 554
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->affordanceOffAnimator:Landroid/animation/ValueAnimator;

    new-instance v1, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v1}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 555
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->affordanceOffAnimator:Landroid/animation/ValueAnimator;

    const-wide/16 v2, 0x44c

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 556
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->affordanceOffAnimator:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect$10;

    invoke-direct {v1, p0}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect$10;-><init>(Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 562
    return-void

    .line 452
    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data

    .line 463
    :array_1
    .array-data 4
        0x0
        0x3f19999a    # 0.6f
    .end array-data

    .line 474
    :array_2
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data

    .line 485
    :array_3
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data

    .line 496
    :array_4
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data

    .line 507
    :array_5
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data

    .line 518
    :array_6
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data

    .line 528
    :array_7
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data

    .line 539
    :array_8
    .array-data 4
        0x0
        0x3f19999a    # 0.6f
    .end array-data

    .line 553
    :array_9
    .array-data 4
        0x3f19999a    # 0.6f
        0x0
    .end array-data
.end method

.method private setCenterPos(Landroid/view/View;FFFFF)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "fromX"    # F
    .param p3, "fromY"    # F
    .param p4, "x"    # F
    .param p5, "y"    # F
    .param p6, "distanceScale"    # F

    .prologue
    const/high16 v3, 0x40000000    # 2.0f

    .line 791
    sub-float v2, p4, p2

    mul-float/2addr v2, p6

    add-float v0, v2, p2

    .line 792
    .local v0, "tx":F
    sub-float v2, p5, p3

    mul-float/2addr v2, p6

    add-float v1, v2, p3

    .line 794
    .local v1, "ty":F
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v3

    sub-float/2addr v0, v2

    .line 795
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v3

    sub-float/2addr v1, v2

    .line 797
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v2

    if-eqz v2, :cond_0

    .line 798
    invoke-virtual {p1, v0}, Landroid/view/View;->setX(F)V

    .line 799
    invoke-virtual {p1, v1}, Landroid/view/View;->setY(F)V

    .line 801
    :cond_0
    return-void
.end method

.method private setCenterPos(Landroid/view/View;FFFFFFI)V
    .locals 20
    .param p1, "view"    # Landroid/view/View;
    .param p2, "fromX"    # F
    .param p3, "fromY"    # F
    .param p4, "x"    # F
    .param p5, "y"    # F
    .param p6, "distanceScale"    # F
    .param p7, "scale"    # F
    .param p8, "rotation"    # I

    .prologue
    .line 805
    const/high16 v13, 0x3f000000    # 0.5f

    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->distance:D

    double-to-float v14, v14

    const/high16 v15, 0x44340000    # 720.0f

    div-float/2addr v14, v15

    const/high16 v15, 0x3f000000    # 0.5f

    mul-float/2addr v14, v15

    add-float v9, v13, v14

    .line 806
    .local v9, "scaleByDistance":F
    const/high16 v13, 0x3f000000    # 0.5f

    move-object/from16 v0, p0

    iget v14, v0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->objAnimationValue:F

    const/high16 v15, 0x3f000000    # 0.5f

    mul-float/2addr v14, v15

    add-float v8, v13, v14

    .line 807
    .local v8, "scaleByAnimationValue":F
    mul-float v13, p7, v9

    mul-float v10, v13, v8

    .line 808
    .local v10, "tscale":F
    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->defaultInSampleSize:F

    mul-float/2addr v13, v10

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Landroid/view/View;->setScaleX(F)V

    .line 809
    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->defaultInSampleSize:F

    mul-float/2addr v13, v10

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Landroid/view/View;->setScaleY(F)V

    .line 811
    const/high16 v13, 0x3f000000    # 0.5f

    move-object/from16 v0, p0

    iget v14, v0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->objAnimationValue:F

    const/high16 v15, 0x3f000000    # 0.5f

    mul-float/2addr v14, v15

    add-float v3, v13, v14

    .line 812
    .local v3, "distanceScaleByAnimation":F
    sub-float v13, p4, p2

    mul-float v13, v13, p6

    mul-float/2addr v13, v3

    add-float v11, v13, p2

    .line 813
    .local v11, "tx":F
    sub-float v13, p5, p3

    mul-float v13, v13, p6

    mul-float/2addr v13, v3

    add-float v12, v13, p3

    .line 815
    .local v12, "ty":F
    if-eqz p8, :cond_0

    .line 816
    const/high16 v13, 0x43960000    # 300.0f

    mul-float v2, p7, v13

    .line 817
    .local v2, "dist":F
    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->distance:D

    double-to-float v13, v14

    const/high16 v14, 0x447a0000    # 1000.0f

    div-float/2addr v13, v14

    mul-float v13, v13, p7

    mul-float v7, v13, p7

    .line 818
    .local v7, "rotationByDistance":F
    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->objAnimationValue:F

    const/high16 v14, 0x3f800000    # 1.0f

    mul-float v6, v13, v14

    .line 819
    .local v6, "rotationByAnimation":F
    move/from16 v0, p8

    int-to-double v14, v0

    const-wide v16, 0x400921fb54442d18L    # Math.PI

    mul-double v14, v14, v16

    const-wide v16, 0x4066800000000000L    # 180.0

    div-double v14, v14, v16

    float-to-double v0, v7

    move-wide/from16 v16, v0

    add-double v14, v14, v16

    float-to-double v0, v6

    move-wide/from16 v16, v0

    add-double v4, v14, v16

    .line 820
    .local v4, "rad":D
    float-to-double v14, v2

    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v16

    mul-double v14, v14, v16

    float-to-double v0, v2

    move-wide/from16 v16, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->sin(D)D

    move-result-wide v18

    mul-double v16, v16, v18

    add-double v14, v14, v16

    double-to-float v13, v14

    add-float v11, v13, p4

    .line 821
    float-to-double v14, v2

    invoke-static {v4, v5}, Ljava/lang/Math;->sin(D)D

    move-result-wide v16

    move-wide/from16 v0, v16

    neg-double v0, v0

    move-wide/from16 v16, v0

    mul-double v14, v14, v16

    float-to-double v0, v2

    move-wide/from16 v16, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v18

    mul-double v16, v16, v18

    add-double v14, v14, v16

    double-to-float v13, v14

    add-float v12, v13, p5

    .line 824
    .end local v2    # "dist":F
    .end local v4    # "rad":D
    .end local v6    # "rotationByAnimation":F
    .end local v7    # "rotationByDistance":F
    :cond_0
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getWidth()I

    move-result v13

    int-to-float v13, v13

    const/high16 v14, 0x40000000    # 2.0f

    div-float/2addr v13, v14

    sub-float/2addr v11, v13

    .line 825
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getHeight()I

    move-result v13

    int-to-float v13, v13

    const/high16 v14, 0x40000000    # 2.0f

    div-float/2addr v13, v14

    sub-float/2addr v12, v13

    .line 827
    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Landroid/view/View;->setX(F)V

    .line 828
    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Landroid/view/View;->setY(F)V

    .line 829
    return-void
.end method

.method private setHexagon()V
    .locals 9

    .prologue
    const/4 v8, -0x2

    .line 302
    const/4 v3, 0x6

    new-array v3, v3, [I

    const/4 v4, 0x0

    iget v5, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->hexagon_blue_id:I

    aput v5, v3, v4

    const/4 v4, 0x1

    iget v5, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->hexagon_orange_id:I

    aput v5, v3, v4

    const/4 v4, 0x2

    iget v5, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->hexagon_blue_id:I

    aput v5, v3, v4

    const/4 v4, 0x3

    iget v5, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->hexagon_orange_id:I

    aput v5, v3, v4

    const/4 v4, 0x4

    iget v5, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->hexagon_green_id:I

    aput v5, v3, v4

    const/4 v4, 0x5

    iget v5, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->hexagon_green_id:I

    aput v5, v3, v4

    iput-object v3, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->hexagonRes:[I

    .line 309
    iget-object v3, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->hexagonRes:[I

    array-length v3, v3

    iput v3, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->HEXAGON_TOTAL:I

    .line 311
    iget v3, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->HEXAGON_TOTAL:I

    new-array v3, v3, [Lcom/samsung/android/visualeffect/common/ImageViewBlended;

    iput-object v3, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->hexagon:[Lcom/samsung/android/visualeffect/common/ImageViewBlended;

    .line 312
    iget v3, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->HEXAGON_TOTAL:I

    new-array v3, v3, [I

    iput-object v3, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->hexagonRotation:[I

    .line 313
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->hexagonDistance:Ljava/util/ArrayList;

    .line 314
    iget v3, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->HEXAGON_TOTAL:I

    new-array v3, v3, [F

    iput-object v3, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->hexagonScale:[F

    .line 316
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget v3, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->HEXAGON_TOTAL:I

    if-ge v1, v3, :cond_0

    .line 317
    new-instance v0, Lcom/samsung/android/visualeffect/common/ImageViewBlended;

    iget-object v3, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->mContext:Landroid/content/Context;

    invoke-direct {v0, v3}, Lcom/samsung/android/visualeffect/common/ImageViewBlended;-><init>(Landroid/content/Context;)V

    .line 318
    .local v0, "hex":Lcom/samsung/android/visualeffect/common/ImageViewBlended;
    iget-object v3, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->hexagonRes:[I

    aget v3, v3, v1

    const/4 v4, 0x0

    invoke-direct {p0, v0, v3, v4}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->setImageWithOption(Landroid/widget/ImageView;ILandroid/graphics/Bitmap$Config;)V

    .line 319
    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v4

    const-wide/high16 v6, 0x4034000000000000L    # 20.0

    mul-double/2addr v4, v6

    double-to-float v2, v4

    .line 320
    .local v2, "rotation":F
    const/4 v3, 0x0

    invoke-direct {p0, v0, v3}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->setAlphaAndVisibility(Landroid/view/View;F)V

    .line 321
    invoke-virtual {v0, v2}, Lcom/samsung/android/visualeffect/common/ImageViewBlended;->setRotation(F)V

    .line 322
    iget-object v3, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->lightObj:Landroid/widget/FrameLayout;

    invoke-virtual {v3, v0, v8, v8}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;II)V

    .line 323
    iget-object v3, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->hexagon:[Lcom/samsung/android/visualeffect/common/ImageViewBlended;

    aput-object v0, v3, v1

    .line 316
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 325
    .end local v0    # "hex":Lcom/samsung/android/visualeffect/common/ImageViewBlended;
    .end local v2    # "rotation":F
    :cond_0
    return-void
.end method

.method private setHexagonRandomTarget(Z)V
    .locals 18
    .param p1, "isForUnlockAffordance"    # Z

    .prologue
    .line 354
    move-object/from16 v0, p0

    iget v12, v0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->HEXAGON_TAP_TOTAL:I

    new-array v12, v12, [Landroid/graphics/Point;

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->tapHexRandomPoint:[Landroid/graphics/Point;

    .line 355
    move-object/from16 v0, p0

    iget v12, v0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->HEXAGON_TAP_TOTAL:I

    new-array v12, v12, [F

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->tapHexagonScale:[F

    .line 356
    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v12

    const-wide v14, 0x4076800000000000L    # 360.0

    mul-double/2addr v12, v14

    double-to-int v12, v12

    int-to-float v12, v12

    move-object/from16 v0, p0

    iput v12, v0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->randomRotation:F

    .line 359
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    move-object/from16 v0, p0

    iget v12, v0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->HEXAGON_TAP_TOTAL:I

    if-ge v5, v12, :cond_1

    .line 360
    if-eqz p1, :cond_0

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v12

    const-wide v14, 0x4076800000000000L    # 360.0

    mul-double/2addr v12, v14

    double-to-int v12, v12

    int-to-float v12, v12

    move-object/from16 v0, p0

    iput v12, v0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->randomRotation:F

    .line 361
    :cond_0
    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v12

    move-object/from16 v0, p0

    iget v14, v0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->TAP_AREA_RADIUS:I

    int-to-double v14, v14

    mul-double/2addr v12, v14

    double-to-int v8, v12

    .line 362
    .local v8, "randomDistance":I
    move-object/from16 v0, p0

    iget v12, v0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->randomRotation:F

    float-to-double v12, v12

    invoke-static {v12, v13}, Ljava/lang/Math;->cos(D)D

    move-result-wide v12

    int-to-double v14, v8

    mul-double/2addr v12, v14

    double-to-int v10, v12

    .line 363
    .local v10, "tx":I
    move-object/from16 v0, p0

    iget v12, v0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->randomRotation:F

    float-to-double v12, v12

    invoke-static {v12, v13}, Ljava/lang/Math;->sin(D)D

    move-result-wide v12

    int-to-double v14, v8

    mul-double/2addr v12, v14

    double-to-int v11, v12

    .line 365
    .local v11, "ty":I
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->tapHexRandomPoint:[Landroid/graphics/Point;

    new-instance v13, Landroid/graphics/Point;

    invoke-direct {v13, v10, v11}, Landroid/graphics/Point;-><init>(II)V

    aput-object v13, v12, v5

    .line 366
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->tapHexagonScale:[F

    const v13, 0x3e4ccccd    # 0.2f

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v14

    const-wide v16, 0x3fe99999a0000000L    # 0.800000011920929

    mul-double v14, v14, v16

    double-to-float v14, v14

    add-float/2addr v13, v14

    aput v13, v12, v5

    .line 367
    const/high16 v12, 0x3f000000    # 0.5f

    const/high16 v13, 0x3f000000    # 0.5f

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v14

    double-to-float v14, v14

    mul-float/2addr v13, v14

    add-float v2, v12, v13

    .line 368
    .local v2, "alpha":F
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->tapHexagon:[Lcom/samsung/android/visualeffect/common/ImageViewBlended;

    aget-object v12, v12, v5

    move-object/from16 v0, p0

    invoke-direct {v0, v12, v2}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->setAlphaAndVisibility(Landroid/view/View;F)V

    .line 359
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 372
    .end local v2    # "alpha":F
    .end local v8    # "randomDistance":I
    .end local v10    # "tx":I
    .end local v11    # "ty":I
    :cond_1
    if-nez p1, :cond_5

    .line 374
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->hexagonDistance:Ljava/util/ArrayList;

    invoke-virtual {v12}, Ljava/util/ArrayList;->clear()V

    .line 375
    const v9, 0x3e4ccccd    # 0.2f

    .line 376
    .local v9, "startDistance":F
    const v3, 0x3e75c28f    # 0.24f

    .line 377
    .local v3, "distanceGap":F
    const/4 v6, 0x0

    .local v6, "j":I
    :goto_1
    move-object/from16 v0, p0

    iget v12, v0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->HEXAGON_TOTAL:I

    if-ge v6, v12, :cond_2

    .line 379
    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v12

    double-to-float v12, v12

    const/high16 v13, 0x3f000000    # 0.5f

    sub-float/2addr v12, v13

    const v13, 0x3ecccccd    # 0.4f

    mul-float v7, v12, v13

    .line 380
    .local v7, "random":F
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->hexagonDistance:Ljava/util/ArrayList;

    int-to-float v13, v6

    mul-float/2addr v13, v3

    add-float/2addr v13, v9

    add-float/2addr v13, v7

    invoke-static {v13}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 377
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 382
    .end local v7    # "random":F
    :cond_2
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->hexagonDistance:Ljava/util/ArrayList;

    invoke-static {v12}, Ljava/util/Collections;->shuffle(Ljava/util/List;)V

    .line 384
    const/4 v5, 0x0

    :goto_2
    move-object/from16 v0, p0

    iget v12, v0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->HEXAGON_TOTAL:I

    if-ge v5, v12, :cond_4

    .line 386
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->hexagon:[Lcom/samsung/android/visualeffect/common/ImageViewBlended;

    aget-object v4, v12, v5

    .line 387
    .local v4, "hex":Lcom/samsung/android/visualeffect/common/ImageViewBlended;
    move-object/from16 v0, p0

    iget v12, v0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->HEXAGON_TOTAL:I

    add-int/lit8 v12, v12, 0x0

    if-ge v5, v12, :cond_3

    .line 389
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->hexagonRotation:[I

    const/4 v13, 0x0

    aput v13, v12, v5

    .line 390
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->hexagonScale:[F

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->hexagonDistance:Ljava/util/ArrayList;

    invoke-virtual {v12, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/Float;

    invoke-virtual {v12}, Ljava/lang/Float;->floatValue()F

    move-result v12

    const v14, 0x3dcccccd    # 0.1f

    add-float/2addr v12, v14

    aput v12, v13, v5

    .line 397
    :goto_3
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->hexagonScale:[F

    aget v12, v12, v5

    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->defaultInSampleSize:F

    mul-float/2addr v12, v13

    invoke-virtual {v4, v12}, Lcom/samsung/android/visualeffect/common/ImageViewBlended;->setScaleX(F)V

    .line 398
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->hexagonScale:[F

    aget v12, v12, v5

    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->defaultInSampleSize:F

    mul-float/2addr v12, v13

    invoke-virtual {v4, v12}, Lcom/samsung/android/visualeffect/common/ImageViewBlended;->setScaleY(F)V

    .line 384
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 394
    :cond_3
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->hexagonRotation:[I

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v14

    const-wide v16, 0x4076800000000000L    # 360.0

    mul-double v14, v14, v16

    double-to-int v13, v14

    aput v13, v12, v5

    .line 395
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->hexagonScale:[F

    const v13, 0x3f19999a    # 0.6f

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v14

    const-wide/high16 v16, 0x3ff0000000000000L    # 1.0

    mul-double v14, v14, v16

    double-to-float v14, v14

    add-float/2addr v13, v14

    aput v13, v12, v5

    goto :goto_3

    .line 400
    .end local v4    # "hex":Lcom/samsung/android/visualeffect/common/ImageViewBlended;
    :cond_4
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->particle:Lcom/samsung/android/visualeffect/common/ImageViewBlended;

    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->randomRotation:F

    invoke-virtual {v12, v13}, Lcom/samsung/android/visualeffect/common/ImageViewBlended;->setRotation(F)V

    .line 402
    .end local v3    # "distanceGap":F
    .end local v6    # "j":I
    .end local v9    # "startDistance":F
    :cond_5
    return-void
.end method

.method private setHover()V
    .locals 5

    .prologue
    const/4 v4, -0x2

    const/4 v3, 0x0

    .line 294
    new-instance v0, Lcom/samsung/android/visualeffect/common/ImageViewBlended;

    iget-object v1, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/samsung/android/visualeffect/common/ImageViewBlended;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->hoverLight1:Lcom/samsung/android/visualeffect/common/ImageViewBlended;

    .line 295
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->hoverLight1:Lcom/samsung/android/visualeffect/common/ImageViewBlended;

    iget v1, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->hoverlight_id:I

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->setImageWithOption(Landroid/widget/ImageView;ILandroid/graphics/Bitmap$Config;)V

    .line 296
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->hoverLight1:Lcom/samsung/android/visualeffect/common/ImageViewBlended;

    iget v1, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->defaultInSampleSize:F

    mul-float/2addr v1, v3

    invoke-virtual {v0, v1}, Lcom/samsung/android/visualeffect/common/ImageViewBlended;->setScaleX(F)V

    .line 297
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->hoverLight1:Lcom/samsung/android/visualeffect/common/ImageViewBlended;

    iget v1, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->defaultInSampleSize:F

    mul-float/2addr v1, v3

    invoke-virtual {v0, v1}, Lcom/samsung/android/visualeffect/common/ImageViewBlended;->setScaleY(F)V

    .line 298
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->hoverLight1:Lcom/samsung/android/visualeffect/common/ImageViewBlended;

    invoke-virtual {p0, v0, v4, v4}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->addView(Landroid/view/View;II)V

    .line 299
    return-void
.end method

.method private setImageResourceId(IIIIIIIIII)V
    .locals 0
    .param p1, "hexagon_blue"    # I
    .param p2, "hexagon_green"    # I
    .param p3, "hexagon_orange"    # I
    .param p4, "hoverlight"    # I
    .param p5, "light"    # I
    .param p6, "long_light"    # I
    .param p7, "particle"    # I
    .param p8, "rainbow"    # I
    .param p9, "ring"    # I
    .param p10, "vignetting"    # I

    .prologue
    .line 166
    iput p1, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->hexagon_blue_id:I

    .line 167
    iput p2, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->hexagon_green_id:I

    .line 168
    iput p3, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->hexagon_orange_id:I

    .line 169
    iput p4, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->hoverlight_id:I

    .line 170
    iput p5, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->light_id:I

    .line 171
    iput p6, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->long_light_id:I

    .line 172
    iput p7, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->particle_id:I

    .line 173
    iput p8, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->rainbow_id:I

    .line 174
    iput p9, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->ring_id:I

    .line 175
    iput p10, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->vignetting_id:I

    .line 176
    return-void
.end method

.method private setImageWithOption(Landroid/widget/ImageView;ILandroid/graphics/Bitmap$Config;)V
    .locals 2
    .param p1, "imageView"    # Landroid/widget/ImageView;
    .param p2, "resId"    # I
    .param p3, "config"    # Landroid/graphics/Bitmap$Config;

    .prologue
    .line 277
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 278
    .local v0, "options":Landroid/graphics/BitmapFactory$Options;
    if-nez p3, :cond_0

    .line 279
    sget-object v1, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    iput-object v1, v0, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 288
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v1, p2, v0}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 289
    iget v1, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->defaultInSampleSize:F

    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setScaleX(F)V

    .line 290
    iget v1, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->defaultInSampleSize:F

    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setScaleY(F)V

    .line 291
    return-void

    .line 281
    :cond_0
    iput-object p3, v0, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    goto :goto_0
.end method

.method private setLayout()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    const/4 v4, -0x2

    .line 229
    new-instance v1, Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->vignetting:Landroid/widget/ImageView;

    .line 230
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 231
    .local v0, "options":Landroid/graphics/BitmapFactory$Options;
    sget-object v1, Landroid/graphics/Bitmap$Config;->ALPHA_8:Landroid/graphics/Bitmap$Config;

    iput-object v1, v0, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 232
    iget-object v1, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->vignetting:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget v3, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->vignetting_id:I

    invoke-static {v2, v3, v0}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 233
    iget-object v1, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->vignetting:Landroid/widget/ImageView;

    sget-object v2, Landroid/widget/ImageView$ScaleType;->FIT_XY:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 234
    iget-object v1, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->vignetting:Landroid/widget/ImageView;

    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 235
    iget-object v1, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->vignetting:Landroid/widget/ImageView;

    invoke-virtual {p0, v1}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->addView(Landroid/view/View;)V

    .line 238
    new-instance v1, Lcom/samsung/android/visualeffect/common/ImageViewBlended;

    iget-object v2, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/samsung/android/visualeffect/common/ImageViewBlended;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->lightFog:Lcom/samsung/android/visualeffect/common/ImageViewBlended;

    .line 239
    iget-object v1, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->lightFog:Lcom/samsung/android/visualeffect/common/ImageViewBlended;

    iget v2, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->light_id:I

    invoke-direct {p0, v1, v2, v6}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->setImageWithOption(Landroid/widget/ImageView;ILandroid/graphics/Bitmap$Config;)V

    .line 240
    iget-object v1, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->lightFog:Lcom/samsung/android/visualeffect/common/ImageViewBlended;

    invoke-virtual {p0, v1, v4, v4}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->addView(Landroid/view/View;II)V

    .line 241
    iget-object v1, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->lightFog:Lcom/samsung/android/visualeffect/common/ImageViewBlended;

    invoke-direct {p0, v1, v5}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->setAlphaAndVisibility(Landroid/view/View;F)V

    .line 244
    new-instance v1, Landroid/widget/FrameLayout;

    iget-object v2, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->lightObj:Landroid/widget/FrameLayout;

    .line 245
    iget-object v1, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->lightObj:Landroid/widget/FrameLayout;

    invoke-virtual {p0, v1}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->addView(Landroid/view/View;)V

    .line 248
    new-instance v1, Landroid/widget/FrameLayout;

    iget-object v2, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->lightTap:Landroid/widget/FrameLayout;

    .line 249
    iget-object v1, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->lightTap:Landroid/widget/FrameLayout;

    invoke-virtual {p0, v1}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->addView(Landroid/view/View;)V

    .line 252
    new-instance v1, Lcom/samsung/android/visualeffect/common/ImageViewBlended;

    iget-object v2, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/samsung/android/visualeffect/common/ImageViewBlended;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->ring:Lcom/samsung/android/visualeffect/common/ImageViewBlended;

    .line 253
    iget-object v1, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->ring:Lcom/samsung/android/visualeffect/common/ImageViewBlended;

    iget v2, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->ring_id:I

    invoke-direct {p0, v1, v2, v6}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->setImageWithOption(Landroid/widget/ImageView;ILandroid/graphics/Bitmap$Config;)V

    .line 254
    iget-object v1, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->ring:Lcom/samsung/android/visualeffect/common/ImageViewBlended;

    invoke-virtual {p0, v1, v4, v4}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->addView(Landroid/view/View;II)V

    .line 255
    iget-object v1, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->ring:Lcom/samsung/android/visualeffect/common/ImageViewBlended;

    invoke-direct {p0, v1, v5}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->setAlphaAndVisibility(Landroid/view/View;F)V

    .line 258
    new-instance v1, Lcom/samsung/android/visualeffect/common/ImageViewBlended;

    iget-object v2, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/samsung/android/visualeffect/common/ImageViewBlended;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->longLight:Lcom/samsung/android/visualeffect/common/ImageViewBlended;

    .line 259
    iget-object v1, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->longLight:Lcom/samsung/android/visualeffect/common/ImageViewBlended;

    iget v2, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->long_light_id:I

    invoke-direct {p0, v1, v2, v6}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->setImageWithOption(Landroid/widget/ImageView;ILandroid/graphics/Bitmap$Config;)V

    .line 260
    iget-object v1, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->longLight:Lcom/samsung/android/visualeffect/common/ImageViewBlended;

    invoke-virtual {p0, v1, v4, v4}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->addView(Landroid/view/View;II)V

    .line 261
    iget-object v1, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->longLight:Lcom/samsung/android/visualeffect/common/ImageViewBlended;

    invoke-direct {p0, v1, v5}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->setAlphaAndVisibility(Landroid/view/View;F)V

    .line 264
    new-instance v1, Lcom/samsung/android/visualeffect/common/ImageViewBlended;

    iget-object v2, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/samsung/android/visualeffect/common/ImageViewBlended;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->particle:Lcom/samsung/android/visualeffect/common/ImageViewBlended;

    .line 265
    iget-object v1, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->particle:Lcom/samsung/android/visualeffect/common/ImageViewBlended;

    iget v2, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->particle_id:I

    invoke-direct {p0, v1, v2, v6}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->setImageWithOption(Landroid/widget/ImageView;ILandroid/graphics/Bitmap$Config;)V

    .line 266
    iget-object v1, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->particle:Lcom/samsung/android/visualeffect/common/ImageViewBlended;

    invoke-virtual {p0, v1, v4, v4}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->addView(Landroid/view/View;II)V

    .line 267
    iget-object v1, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->particle:Lcom/samsung/android/visualeffect/common/ImageViewBlended;

    invoke-direct {p0, v1, v5}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->setAlphaAndVisibility(Landroid/view/View;F)V

    .line 270
    new-instance v1, Lcom/samsung/android/visualeffect/common/ImageViewBlended;

    iget-object v2, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/samsung/android/visualeffect/common/ImageViewBlended;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->rainbow:Lcom/samsung/android/visualeffect/common/ImageViewBlended;

    .line 271
    iget-object v1, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->rainbow:Lcom/samsung/android/visualeffect/common/ImageViewBlended;

    iget v2, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->rainbow_id:I

    invoke-direct {p0, v1, v2, v6}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->setImageWithOption(Landroid/widget/ImageView;ILandroid/graphics/Bitmap$Config;)V

    .line 272
    iget-object v1, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->rainbow:Lcom/samsung/android/visualeffect/common/ImageViewBlended;

    invoke-virtual {p0, v1, v4, v4}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->addView(Landroid/view/View;II)V

    .line 273
    iget-object v1, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->rainbow:Lcom/samsung/android/visualeffect/common/ImageViewBlended;

    invoke-direct {p0, v1, v5}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->setAlphaAndVisibility(Landroid/view/View;F)V

    .line 274
    return-void
.end method

.method private setSound()V
    .locals 9

    .prologue
    const/4 v6, 0x1

    .line 405
    invoke-direct {p0}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->stopReleaseSound()V

    .line 408
    iget-object v5, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    .line 410
    .local v3, "cr":Landroid/content/ContentResolver;
    const/4 v4, 0x0

    .line 421
    .local v4, "result":I
    if-ne v4, v6, :cond_3

    move v5, v6

    :goto_0
    iput-boolean v5, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->isSystemSoundChecked:Z

    .line 423
    iget-object v5, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->soundpool:Landroid/media/SoundPool;

    if-nez v5, :cond_2

    .line 425
    new-instance v5, Landroid/media/AudioAttributes$Builder;

    invoke-direct {v5}, Landroid/media/AudioAttributes$Builder;-><init>()V

    const/16 v7, 0xd

    invoke-virtual {v5, v7}, Landroid/media/AudioAttributes$Builder;->setUsage(I)Landroid/media/AudioAttributes$Builder;

    move-result-object v5

    const/4 v7, 0x4

    invoke-virtual {v5, v7}, Landroid/media/AudioAttributes$Builder;->setContentType(I)Landroid/media/AudioAttributes$Builder;

    move-result-object v5

    invoke-virtual {v5}, Landroid/media/AudioAttributes$Builder;->build()Landroid/media/AudioAttributes;

    move-result-object v0

    .line 429
    .local v0, "attr":Landroid/media/AudioAttributes;
    new-instance v5, Landroid/media/SoundPool$Builder;

    invoke-direct {v5}, Landroid/media/SoundPool$Builder;-><init>()V

    const/16 v7, 0xa

    invoke-virtual {v5, v7}, Landroid/media/SoundPool$Builder;->setMaxStreams(I)Landroid/media/SoundPool$Builder;

    move-result-object v5

    invoke-virtual {v5, v0}, Landroid/media/SoundPool$Builder;->setAudioAttributes(Landroid/media/AudioAttributes;)Landroid/media/SoundPool$Builder;

    move-result-object v5

    invoke-virtual {v5}, Landroid/media/SoundPool$Builder;->build()Landroid/media/SoundPool;

    move-result-object v5

    iput-object v5, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->soundpool:Landroid/media/SoundPool;

    .line 431
    new-instance v1, Ljava/io/File;

    const-string v5, "/system/media/audio/ui/lens_flare_tap.ogg"

    invoke-direct {v1, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 432
    .local v1, "checkTapFile":Ljava/io/File;
    new-instance v2, Ljava/io/File;

    const-string v5, "/system/media/audio/ui/lens_flare_unlock.ogg"

    invoke-direct {v2, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 434
    .local v2, "checkUnlockFile":Ljava/io/File;
    iget-object v5, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->soundpool:Landroid/media/SoundPool;

    const-string v7, "/system/media/audio/ui/lens_flare_tap.ogg"

    invoke-virtual {v5, v7, v6}, Landroid/media/SoundPool;->load(Ljava/lang/String;I)I

    move-result v5

    iput v5, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->sound_tap:I

    .line 435
    iget-object v5, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->soundpool:Landroid/media/SoundPool;

    const-string v7, "/system/media/audio/ui/lens_flare_unlock.ogg"

    invoke-virtual {v5, v7, v6}, Landroid/media/SoundPool;->load(Ljava/lang/String;I)I

    move-result v5

    iput v5, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->sound_unlock:I

    .line 436
    const-string v5, "LensFlare"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Sound ID get from path sound_tap = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->sound_tap:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", sound_unlock = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->sound_unlock:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 438
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v5

    if-nez v5, :cond_0

    .line 439
    iget-object v5, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->soundpool:Landroid/media/SoundPool;

    iget-object v7, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->mContext:Landroid/content/Context;

    iget v8, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->tap_sound_id:I

    invoke-virtual {v5, v7, v8, v6}, Landroid/media/SoundPool;->load(Landroid/content/Context;II)I

    move-result v5

    iput v5, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->sound_tap:I

    .line 442
    :cond_0
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v5

    if-nez v5, :cond_1

    .line 443
    iget-object v5, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->soundpool:Landroid/media/SoundPool;

    iget-object v7, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->mContext:Landroid/content/Context;

    iget v8, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->unlock_sound_id:I

    invoke-virtual {v5, v7, v8, v6}, Landroid/media/SoundPool;->load(Landroid/content/Context;II)I

    move-result v5

    iput v5, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->sound_unlock:I

    .line 446
    :cond_1
    const-string v5, "LensFlare"

    const-string v6, "LensFlare sound : load"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 448
    .end local v0    # "attr":Landroid/media/AudioAttributes;
    .end local v1    # "checkTapFile":Ljava/io/File;
    .end local v2    # "checkUnlockFile":Ljava/io/File;
    :cond_2
    return-void

    .line 421
    :cond_3
    const/4 v5, 0x0

    goto/16 :goto_0
.end method

.method private setSoundResourceId(II)V
    .locals 0
    .param p1, "tapSound"    # I
    .param p2, "unlockSound"    # I

    .prologue
    .line 179
    iput p1, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->tap_sound_id:I

    .line 180
    iput p2, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->unlock_sound_id:I

    .line 181
    return-void
.end method

.method private setTapHexagon()V
    .locals 11

    .prologue
    const/4 v10, -0x2

    .line 328
    iget v5, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->HEXAGON_TAP_TOTAL:I

    new-array v5, v5, [Lcom/samsung/android/visualeffect/common/ImageViewBlended;

    iput-object v5, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->tapHexagon:[Lcom/samsung/android/visualeffect/common/ImageViewBlended;

    .line 329
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget v5, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->HEXAGON_TAP_TOTAL:I

    if-ge v1, v5, :cond_0

    .line 330
    rem-int/lit8 v2, v1, 0x3

    .line 331
    .local v2, "index":I
    const/4 v4, 0x0

    .line 332
    .local v4, "resource":I
    packed-switch v2, :pswitch_data_0

    .line 343
    :goto_1
    new-instance v0, Lcom/samsung/android/visualeffect/common/ImageViewBlended;

    iget-object v5, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->mContext:Landroid/content/Context;

    invoke-direct {v0, v5}, Lcom/samsung/android/visualeffect/common/ImageViewBlended;-><init>(Landroid/content/Context;)V

    .line 344
    .local v0, "hex":Lcom/samsung/android/visualeffect/common/ImageViewBlended;
    const/4 v5, 0x0

    invoke-direct {p0, v0, v4, v5}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->setImageWithOption(Landroid/widget/ImageView;ILandroid/graphics/Bitmap$Config;)V

    .line 345
    const/4 v5, 0x0

    invoke-direct {p0, v0, v5}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->setAlphaAndVisibility(Landroid/view/View;F)V

    .line 346
    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v6

    const-wide v8, 0x4076800000000000L    # 360.0

    mul-double/2addr v6, v8

    double-to-int v3, v6

    .line 347
    .local v3, "randomRotation":I
    int-to-float v5, v3

    invoke-virtual {v0, v5}, Lcom/samsung/android/visualeffect/common/ImageViewBlended;->setRotation(F)V

    .line 348
    iget-object v5, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->lightTap:Landroid/widget/FrameLayout;

    invoke-virtual {v5, v0, v10, v10}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;II)V

    .line 349
    iget-object v5, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->tapHexagon:[Lcom/samsung/android/visualeffect/common/ImageViewBlended;

    aput-object v0, v5, v1

    .line 329
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 334
    .end local v0    # "hex":Lcom/samsung/android/visualeffect/common/ImageViewBlended;
    .end local v3    # "randomRotation":I
    :pswitch_0
    iget v4, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->hexagon_blue_id:I

    .line 335
    goto :goto_1

    .line 337
    :pswitch_1
    iget v4, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->hexagon_orange_id:I

    .line 338
    goto :goto_1

    .line 340
    :pswitch_2
    iget v4, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->hexagon_green_id:I

    goto :goto_1

    .line 351
    .end local v2    # "index":I
    .end local v4    # "resource":I
    :cond_0
    return-void

    .line 332
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private show()V
    .locals 4

    .prologue
    .line 928
    const-string v0, "LensFlare"

    const-string v1, "show"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 930
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->isPlayAffordance:Z

    .line 931
    iget-boolean v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->isBeforeInit:Z

    if-eqz v0, :cond_0

    .line 933
    const-string v0, "LensFlare"

    const-string v1, "isBeforeInit is true"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 934
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->mFirstCreatedRunnable:Ljava/lang/Runnable;

    if-nez v0, :cond_0

    .line 936
    new-instance v0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect$12;

    invoke-direct {v0, p0}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect$12;-><init>(Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;)V

    iput-object v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->mFirstCreatedRunnable:Ljava/lang/Runnable;

    .line 943
    const-string v0, "LensFlare"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "this.postDelayed, createdDelaytime = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->createdDelaytime:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 944
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->mFirstCreatedRunnable:Ljava/lang/Runnable;

    iget-wide v2, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->createdDelaytime:J

    invoke-virtual {p0, v0, v2, v3}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 947
    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->setSound()V

    .line 948
    return-void
.end method

.method private showLight(FF)V
    .locals 7
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    const/4 v2, 0x0

    const/high16 v6, 0x3f800000    # 1.0f

    .line 571
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->isTouched:Z

    .line 572
    invoke-direct {p0}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->stopUnlockAffordance()V

    .line 574
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->distance:D

    .line 575
    iput v2, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->distancePerMaxAlpha:F

    .line 576
    add-float v0, p1, v2

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->showStartX:F

    .line 577
    iget v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->Y_OFFSET:I

    int-to-float v0, v0

    add-float/2addr v0, p2

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->showStartY:F

    .line 578
    iget v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->showStartX:F

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->currentX:F

    .line 579
    iget v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->showStartY:F

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->currentY:F

    .line 580
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->setHexagonRandomTarget(Z)V

    .line 582
    invoke-direct {p0}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->animatedDragPos()V

    .line 584
    iget-object v1, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->lightFog:Lcom/samsung/android/visualeffect/common/ImageViewBlended;

    iget v2, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->showStartX:F

    iget v3, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->showStartY:F

    iget v4, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->showStartX:F

    iget v5, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->showStartY:F

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->setCenterPos(Landroid/view/View;FFFFF)V

    .line 585
    iget-object v1, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->ring:Lcom/samsung/android/visualeffect/common/ImageViewBlended;

    iget v2, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->showStartX:F

    iget v3, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->showStartY:F

    iget v4, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->showStartX:F

    iget v5, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->showStartY:F

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->setCenterPos(Landroid/view/View;FFFFF)V

    .line 586
    iget-object v1, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->longLight:Lcom/samsung/android/visualeffect/common/ImageViewBlended;

    iget v2, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->showStartX:F

    iget v3, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->showStartY:F

    iget v4, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->showStartX:F

    iget v5, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->showStartY:F

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->setCenterPos(Landroid/view/View;FFFFF)V

    .line 587
    iget-object v1, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->particle:Lcom/samsung/android/visualeffect/common/ImageViewBlended;

    iget v2, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->showStartX:F

    iget v3, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->showStartY:F

    iget v4, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->showStartX:F

    iget v5, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->showStartY:F

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->setCenterPos(Landroid/view/View;FFFFF)V

    .line 589
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->fadeOutAnimator:Landroid/animation/ValueAnimator;

    invoke-direct {p0, v0}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->cancelAnimator(Landroid/animation/Animator;)V

    .line 590
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->objAnimator:Landroid/animation/ValueAnimator;

    invoke-direct {p0, v0}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->cancelAnimator(Landroid/animation/Animator;)V

    .line 591
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->fogOnAnimator:Landroid/animation/ValueAnimator;

    invoke-direct {p0, v0}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->cancelAnimator(Landroid/animation/Animator;)V

    .line 592
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->tapAnimator:Landroid/animation/ValueAnimator;

    invoke-direct {p0, v0}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->cancelAnimator(Landroid/animation/Animator;)V

    .line 593
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->affordanceOnAnimator:Landroid/animation/ValueAnimator;

    invoke-direct {p0, v0}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->cancelAnimator(Landroid/animation/Animator;)V

    .line 594
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->affordanceOffAnimator:Landroid/animation/ValueAnimator;

    invoke-direct {p0, v0}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->cancelAnimator(Landroid/animation/Animator;)V

    .line 596
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->objAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 597
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->fogOnAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 598
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->tapAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 600
    iget v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->sound_tap:I

    invoke-direct {p0, v0}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->playSound(I)V

    .line 601
    return-void
.end method

.method private showUnlockAffordance(JLandroid/graphics/Rect;)V
    .locals 5
    .param p1, "startDelay"    # J
    .param p3, "rect"    # Landroid/graphics/Rect;

    .prologue
    .line 876
    const-string v0, "LensFlare"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "showUnlockAffordance : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p3, Landroid/graphics/Rect;->left:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p3, Landroid/graphics/Rect;->right:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p3, Landroid/graphics/Rect;->top:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p3, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", startDelay : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 878
    iget-boolean v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->isPlayAffordance:Z

    if-eqz v0, :cond_0

    .line 879
    invoke-direct {p0}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->stopUnlockAffordance()V

    .line 880
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->affordancePoint:Landroid/graphics/Point;

    iget v1, p3, Landroid/graphics/Rect;->left:I

    iget v2, p3, Landroid/graphics/Rect;->right:I

    iget v3, p3, Landroid/graphics/Rect;->left:I

    sub-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Point;->x:I

    .line 881
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->affordancePoint:Landroid/graphics/Point;

    iget v1, p3, Landroid/graphics/Rect;->top:I

    iget v2, p3, Landroid/graphics/Rect;->bottom:I

    iget v3, p3, Landroid/graphics/Rect;->top:I

    sub-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Point;->y:I

    .line 883
    new-instance v0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect$11;

    invoke-direct {v0, p0}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect$11;-><init>(Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;)V

    iput-object v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->affordanceRunnable:Ljava/lang/Runnable;

    .line 891
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->affordanceRunnable:Ljava/lang/Runnable;

    invoke-virtual {p0, v0, p1, p2}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 893
    :cond_0
    return-void
.end method

.method private stopReleaseSound()V
    .locals 1

    .prologue
    .line 972
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->releaseSoundRunnable:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 973
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->releaseSoundRunnable:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 974
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->releaseSoundRunnable:Ljava/lang/Runnable;

    .line 976
    :cond_0
    return-void
.end method

.method private stopUnlockAffordance()V
    .locals 2

    .prologue
    .line 915
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->affordanceRunnable:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 916
    const-string v0, "LensFlare"

    const-string v1, "remove delayed UnlockAffordance callback"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 917
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->affordanceRunnable:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 918
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->affordanceRunnable:Ljava/lang/Runnable;

    .line 920
    :cond_0
    return-void
.end method

.method private unlock()V
    .locals 6

    .prologue
    .line 625
    iget-boolean v1, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->isBeforeInit:Z

    if-eqz v1, :cond_0

    .line 626
    const-string v1, "LensFlare"

    const-string v2, "unlock before init"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 627
    invoke-direct {p0}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->lensFlareinit()V

    .line 634
    :goto_0
    return-void

    .line 629
    :cond_0
    iget v1, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->sound_unlock:I

    invoke-direct {p0, v1}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->playSound(I)V

    .line 630
    iget v1, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->currentY:F

    iget v2, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->showStartY:F

    sub-float/2addr v1, v2

    float-to-double v2, v1

    iget v1, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->currentX:F

    iget v4, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->showStartX:F

    sub-float/2addr v1, v4

    float-to-double v4, v1

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v2

    const-wide v4, 0x4066800000000000L    # 180.0

    mul-double/2addr v2, v4

    const-wide v4, 0x400921fb54442d18L    # Math.PI

    div-double/2addr v2, v4

    double-to-float v1, v2

    const/high16 v2, 0x42200000    # 40.0f

    sub-float v0, v1, v2

    .line 631
    .local v0, "degree":F
    iget-object v1, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->rainbow:Lcom/samsung/android/visualeffect/common/ImageViewBlended;

    invoke-virtual {v1, v0}, Lcom/samsung/android/visualeffect/common/ImageViewBlended;->setRotation(F)V

    .line 632
    iget-object v1, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->unlockAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->start()V

    goto :goto_0
.end method


# virtual methods
.method public clearScreen()V
    .locals 0

    .prologue
    .line 1114
    invoke-direct {p0}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->cleanUp()V

    .line 1115
    return-void
.end method

.method public handleCustomEvent(ILjava/util/HashMap;)V
    .locals 4
    .param p1, "cmd"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/HashMap",
            "<**>;)V"
        }
    .end annotation

    .prologue
    .line 1053
    .local p2, "params":Ljava/util/HashMap;, "Ljava/util/HashMap<**>;"
    const/4 v0, 0x2

    if-ne p1, v0, :cond_1

    .line 1054
    invoke-direct {p0}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->unlock()V

    .line 1074
    :cond_0
    :goto_0
    return-void

    .line 1056
    :cond_1
    const/4 v0, 0x1

    if-ne p1, v0, :cond_2

    .line 1057
    const-string v0, "startDelay"

    invoke-virtual {p2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-string v0, "rect"

    invoke-virtual {p2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Rect;

    invoke-direct {p0, v2, v3, v0}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->showUnlockAffordance(JLandroid/graphics/Rect;)V

    goto :goto_0

    .line 1059
    :cond_2
    const/4 v0, 0x3

    if-ne p1, v0, :cond_0

    .line 1060
    const-string v0, "manualInit"

    invoke-virtual {p2, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1061
    invoke-direct {p0}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->lensFlareinit()V

    goto :goto_0

    .line 1062
    :cond_3
    const-string v0, "show"

    invoke-virtual {p2, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1063
    invoke-direct {p0}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->show()V

    goto :goto_0

    .line 1064
    :cond_4
    const-string v0, "reset"

    invoke-virtual {p2, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1065
    invoke-direct {p0}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->reset()V

    goto :goto_0

    .line 1066
    :cond_5
    const-string v0, "screenTurnedOn"

    invoke-virtual {p2, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1067
    invoke-direct {p0}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->screenTurnedOn()V

    goto :goto_0

    .line 1068
    :cond_6
    const-string v0, "showUnlockAffordance"

    invoke-virtual {p2, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1069
    const-string v0, "startDelay"

    invoke-virtual {p2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-string v0, "rect"

    invoke-virtual {p2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Rect;

    invoke-direct {p0, v2, v3, v0}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->showUnlockAffordance(JLandroid/graphics/Rect;)V

    goto :goto_0

    .line 1070
    :cond_7
    const-string v0, "playLockSound"

    invoke-virtual {p2, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1071
    invoke-direct {p0}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->playLockSound()V

    goto :goto_0
.end method

.method public handleTouchEvent(Landroid/view/MotionEvent;Landroid/view/View;)V
    .locals 3
    .param p1, "event"    # Landroid/view/MotionEvent;
    .param p2, "view"    # Landroid/view/View;

    .prologue
    .line 1087
    iget-boolean v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->isBeforeInit:Z

    if-eqz v0, :cond_1

    .line 1089
    const-string v0, "LensFlare"

    const-string v1, "Return handleTouchEvent!!! Becuase isBeforeInit is true"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1109
    :cond_0
    :goto_0
    return-void

    .line 1093
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    if-nez v0, :cond_4

    .line 1094
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getSource()I

    move-result v0

    and-int/lit16 v0, v0, 0x4002

    const/16 v1, 0x4002

    if-eq v0, v1, :cond_2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getSource()I

    move-result v0

    and-int/lit16 v0, v0, 0x2002

    const/16 v1, 0x2002

    if-ne v0, v1, :cond_3

    .line 1096
    :cond_2
    iget v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->PEN_HOVER_Y_OFFSET:I

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->Y_OFFSET:I

    .line 1100
    :goto_1
    const-string v0, "LensFlare"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "InputDevice.SOURCE_STYLUS = 16386, Y_OFFSET = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->Y_OFFSET:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1101
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->showLight(FF)V

    goto :goto_0

    .line 1098
    :cond_3
    iget v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->FINGER_HOVER_Y_OFFSET:I

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->Y_OFFSET:I

    goto :goto_1

    .line 1102
    :cond_4
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_5

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v0

    if-nez v0, :cond_5

    .line 1104
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->move(FF)V

    goto :goto_0

    .line 1105
    :cond_5
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_6

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 1107
    :cond_6
    invoke-direct {p0}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->hide()V

    goto :goto_0
.end method

.method public init(Lcom/samsung/android/visualeffect/EffectDataObj;)V
    .locals 11
    .param p1, "data"    # Lcom/samsung/android/visualeffect/EffectDataObj;

    .prologue
    .line 1029
    iget-object v0, p1, Lcom/samsung/android/visualeffect/EffectDataObj;->lensFlareData:Lcom/samsung/android/visualeffect/lock/data/LensFlareData;

    iget v1, v0, Lcom/samsung/android/visualeffect/lock/data/LensFlareData;->hexagon_blue:I

    iget-object v0, p1, Lcom/samsung/android/visualeffect/EffectDataObj;->lensFlareData:Lcom/samsung/android/visualeffect/lock/data/LensFlareData;

    iget v2, v0, Lcom/samsung/android/visualeffect/lock/data/LensFlareData;->hexagon_green:I

    iget-object v0, p1, Lcom/samsung/android/visualeffect/EffectDataObj;->lensFlareData:Lcom/samsung/android/visualeffect/lock/data/LensFlareData;

    iget v3, v0, Lcom/samsung/android/visualeffect/lock/data/LensFlareData;->hexagon_orange:I

    iget-object v0, p1, Lcom/samsung/android/visualeffect/EffectDataObj;->lensFlareData:Lcom/samsung/android/visualeffect/lock/data/LensFlareData;

    iget v4, v0, Lcom/samsung/android/visualeffect/lock/data/LensFlareData;->hoverlight:I

    iget-object v0, p1, Lcom/samsung/android/visualeffect/EffectDataObj;->lensFlareData:Lcom/samsung/android/visualeffect/lock/data/LensFlareData;

    iget v5, v0, Lcom/samsung/android/visualeffect/lock/data/LensFlareData;->light:I

    iget-object v0, p1, Lcom/samsung/android/visualeffect/EffectDataObj;->lensFlareData:Lcom/samsung/android/visualeffect/lock/data/LensFlareData;

    iget v6, v0, Lcom/samsung/android/visualeffect/lock/data/LensFlareData;->long_light:I

    iget-object v0, p1, Lcom/samsung/android/visualeffect/EffectDataObj;->lensFlareData:Lcom/samsung/android/visualeffect/lock/data/LensFlareData;

    iget v7, v0, Lcom/samsung/android/visualeffect/lock/data/LensFlareData;->particle:I

    iget-object v0, p1, Lcom/samsung/android/visualeffect/EffectDataObj;->lensFlareData:Lcom/samsung/android/visualeffect/lock/data/LensFlareData;

    iget v8, v0, Lcom/samsung/android/visualeffect/lock/data/LensFlareData;->rainbow:I

    iget-object v0, p1, Lcom/samsung/android/visualeffect/EffectDataObj;->lensFlareData:Lcom/samsung/android/visualeffect/lock/data/LensFlareData;

    iget v9, v0, Lcom/samsung/android/visualeffect/lock/data/LensFlareData;->ring:I

    iget-object v0, p1, Lcom/samsung/android/visualeffect/EffectDataObj;->lensFlareData:Lcom/samsung/android/visualeffect/lock/data/LensFlareData;

    iget v10, v0, Lcom/samsung/android/visualeffect/lock/data/LensFlareData;->vignetting:I

    move-object v0, p0

    invoke-direct/range {v0 .. v10}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->setImageResourceId(IIIIIIIIII)V

    .line 1041
    iget-object v0, p1, Lcom/samsung/android/visualeffect/EffectDataObj;->lensFlareData:Lcom/samsung/android/visualeffect/lock/data/LensFlareData;

    iget v0, v0, Lcom/samsung/android/visualeffect/lock/data/LensFlareData;->tapSound:I

    iget-object v1, p1, Lcom/samsung/android/visualeffect/EffectDataObj;->lensFlareData:Lcom/samsung/android/visualeffect/lock/data/LensFlareData;

    iget v1, v1, Lcom/samsung/android/visualeffect/lock/data/LensFlareData;->unlockSound:I

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/visualeffect/lock/lensflare/LensFlareEffect;->setSoundResourceId(II)V

    .line 1045
    return-void
.end method

.method public reInit(Lcom/samsung/android/visualeffect/EffectDataObj;)V
    .locals 0
    .param p1, "data"    # Lcom/samsung/android/visualeffect/EffectDataObj;

    .prologue
    .line 1049
    return-void
.end method

.method public removeListener()V
    .locals 0

    .prologue
    .line 1082
    return-void
.end method

.method public setListener(Lcom/samsung/android/visualeffect/IEffectListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/android/visualeffect/IEffectListener;

    .prologue
    .line 1078
    return-void
.end method
