.class public Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;
.super Ljava/lang/Object;
.source "GLTextureViewRendererForRippleType.java"

# interfaces
.implements Lcom/samsung/android/visualeffect/common/GLTextureView$Renderer;


# static fields
.field protected static final PORTRAIT_MODE:I = 0x0

.field protected static final TABLET_MODE:I = 0x1


# instance fields
.field protected final BMP_ENVIRONMENTIMG:I

.field protected final BMP_LANDSCAPEIMG:I

.field protected final BMP_PORTRAITIMG:I

.field protected final DBG:Z

.field protected final INK_DISABLE:I

.field protected MESH_SIZE_HEIGHT:I

.field protected MESH_SIZE_WIDTH:I

.field protected NUM_DETAILS_HEIGHT:I

.field protected NUM_DETAILS_WIDTH:I

.field protected final REDUCTION_RATE_NORMAL:F

.field protected final RIPPLE_LIGHT:I

.field protected final RIPPLE_LIGHT_WITH_INK:I

.field protected SURFACE_DETAILS_HEIGHT:I

.field protected SURFACE_DETAILS_WIDTH:I

.field protected TAG:Ljava/lang/String;

.field protected TOUCH_EXPONENT:F

.field protected TOUCH_FRESENL:F

.field protected TOUCH_SPECULAR:F

.field protected final UPDATE_TYPE_USER_SWITCHING:I

.field protected VCOUNT:I

.field protected XRatioAdjustLandscape:F

.field protected XRatioAdjustPortrait:F

.field protected XRatioForLandscape:F

.field protected XRatioForPortrait:F

.field protected YRatioForLandscape:F

.field protected YRatioForPortrait:F

.field protected alphaRatio1:F

.field protected alphaRatio2:F

.field protected bitmapEnvironmentBG:Landroid/graphics/Bitmap;

.field protected bitmapLandscapeBG:Landroid/graphics/Bitmap;

.field protected bitmapPortraitBG:Landroid/graphics/Bitmap;

.field protected defaultX:F

.field protected defaultY:F

.field protected diffPressTime:J

.field protected downX:F

.field protected downY:F

.field protected drawCount:I

.field protected effectType:I

.field protected framecounter:I

.field protected glX:F

.field protected glY:F

.field protected gpuHeights:[F

.field protected heights:[F

.field protected heightsSub1:[F

.field protected heightsSub2:[F

.field protected indices:[S

.field protected inkColorFromSetting:[[F

.field protected inkColors:[F

.field protected intensityForLandscape:F

.field protected intensityForPortrait:F

.field protected intensityForRipple:F

.field protected isIndigoDiffusion:Z

.field protected isMakedASpenToucdUp:Z

.field protected isOrientationChangCount:I

.field protected isOrientationChanged:Z

.field protected isSPenSupport:Z

.field protected isSurfaceChanged:Z

.field protected isTouched:Z

.field protected mBgChangeCheckQueue:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field protected mBitmapRatio:F

.field protected mContext:Landroid/content/Context;

.field protected mDefaultRunnable:Ljava/lang/Runnable;

.field protected mEffectChangeCheckQueue:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field protected mExponentRatio:F

.field protected mFresnelRatio:F

.field protected mInkEffectColor:I

.field protected mLandscape:Z

.field mListener:Lcom/samsung/android/visualeffect/IEffectListener;

.field protected mParent:Lcom/samsung/android/visualeffect/common/GLTextureView;

.field protected mReductionRate:F

.field protected mScreenHeight:I

.field protected mScreenWidth:I

.field protected mSpecularRatio:F

.field protected max:I

.field protected minLength:I

.field protected mouseX:F

.field protected mouseY:F

.field protected preEffectType:I

.field protected prevPressTime:J

.field protected proj:[F

.field protected reflectionRatio:F

.field protected refractiveIndex:F

.field protected rippleDistance:I

.field protected rippleDragThreshold:D

.field protected spanXForLandscape:I

.field protected spanXForPortrait:I

.field protected spanYForLandscape:I

.field protected spanYForPortrait:I

.field protected spenUspLevel:I

.field protected textures0:[I

.field protected textures1:[I

.field protected timeStart:J

.field protected translateXForLandscape:F

.field protected translateXForPortrait:F

.field protected translateYForLandscape:F

.field protected translateYForPortrait:F

.field protected translateZForLandscape:F

.field protected translateZForPortrait:F

.field protected unitCellHeight:F

.field protected unitCellWidth:F

.field protected velocity:[F

.field protected velocitySub1:[F

.field protected velocitySub2:[F

.field protected vertices:[F

.field protected view:[F

.field protected windowHeight:I

.field protected windowWidth:I

.field protected world:[F

.field protected wvp:[F


# direct methods
.method public constructor <init>()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x3

    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 70
    iput-boolean v6, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->DBG:Z

    .line 71
    const-string v0, "RippleTypes"

    iput-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->TAG:Ljava/lang/String;

    .line 73
    const/16 v0, 0x68

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->NUM_DETAILS_WIDTH:I

    .line 74
    const/16 v0, 0x68

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->NUM_DETAILS_HEIGHT:I

    .line 76
    const/16 v0, 0x32

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->MESH_SIZE_WIDTH:I

    .line 77
    const/16 v0, 0x32

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->MESH_SIZE_HEIGHT:I

    .line 79
    iput v3, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->SURFACE_DETAILS_WIDTH:I

    .line 80
    iput v3, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->SURFACE_DETAILS_HEIGHT:I

    .line 82
    iput v3, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->max:I

    .line 83
    iput v3, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->VCOUNT:I

    .line 85
    iput v2, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->unitCellWidth:F

    .line 86
    iput v2, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->unitCellHeight:F

    .line 88
    iput-object v5, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->heights:[F

    .line 89
    iput-object v5, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->velocity:[F

    .line 91
    iput-object v5, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->heightsSub1:[F

    .line 92
    new-array v0, v3, [F

    iput-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->velocitySub1:[F

    .line 94
    new-array v0, v3, [F

    iput-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->heightsSub2:[F

    .line 95
    new-array v0, v3, [F

    iput-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->velocitySub2:[F

    .line 98
    new-array v0, v3, [F

    iput-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->vertices:[F

    .line 99
    new-array v0, v3, [F

    iput-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->gpuHeights:[F

    .line 100
    new-array v0, v3, [S

    iput-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->indices:[S

    .line 102
    new-array v0, v3, [I

    iput-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->textures0:[I

    .line 103
    new-array v0, v3, [I

    iput-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->textures1:[I

    .line 106
    const/16 v0, 0x10

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->view:[F

    .line 107
    const/16 v0, 0x10

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->proj:[F

    .line 108
    const/16 v0, 0x10

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->world:[F

    .line 109
    const/16 v0, 0x10

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->wvp:[F

    .line 111
    iput v3, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->mScreenWidth:I

    .line 112
    iput v3, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->mScreenHeight:I

    .line 114
    iput v2, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->mouseX:F

    .line 115
    iput v2, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->mouseY:F

    .line 118
    const v0, 0x3f70a3d7    # 0.94f

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->REDUCTION_RATE_NORMAL:F

    .line 121
    iput v2, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->mBitmapRatio:F

    .line 124
    const v0, 0x3dcccccd    # 0.1f

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->TOUCH_FRESENL:F

    .line 125
    const/high16 v0, 0x3f000000    # 0.5f

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->TOUCH_SPECULAR:F

    .line 126
    const/high16 v0, 0x41a00000    # 20.0f

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->TOUCH_EXPONENT:F

    .line 128
    iget v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->TOUCH_FRESENL:F

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->mFresnelRatio:F

    .line 129
    iget v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->TOUCH_SPECULAR:F

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->mSpecularRatio:F

    .line 130
    iget v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->TOUCH_EXPONENT:F

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->mExponentRatio:F

    .line 133
    iput-boolean v3, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->mLandscape:Z

    .line 136
    const v0, 0x3f70a3d7    # 0.94f

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->mReductionRate:F

    .line 139
    const v0, 0x3f6e147b    # 0.93f

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->refractiveIndex:F

    .line 140
    const v0, 0x3e051eb8    # 0.13f

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->reflectionRatio:F

    .line 141
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->alphaRatio1:F

    .line 142
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->alphaRatio2:F

    .line 146
    iput-object v5, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->bitmapPortraitBG:Landroid/graphics/Bitmap;

    .line 147
    iput-object v5, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->bitmapLandscapeBG:Landroid/graphics/Bitmap;

    .line 148
    iput-object v5, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->bitmapEnvironmentBG:Landroid/graphics/Bitmap;

    .line 150
    iput v2, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->glY:F

    .line 151
    iput v2, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->glX:F

    .line 158
    const v0, 0x3eb33333    # 0.35f

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->intensityForLandscape:F

    .line 159
    const/high16 v0, 0x3f000000    # 0.5f

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->intensityForPortrait:F

    .line 163
    iput v2, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->translateXForLandscape:F

    .line 164
    iput v2, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->translateXForPortrait:F

    .line 167
    iput v2, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->translateYForLandscape:F

    .line 168
    iput v2, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->translateYForPortrait:F

    .line 171
    const v0, -0x3dd3cccd    # -43.05f

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->translateZForPortrait:F

    .line 172
    const v0, -0x3e41999a    # -23.8f

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->translateZForLandscape:F

    .line 175
    iput v4, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->spanXForLandscape:I

    .line 176
    const/16 v0, 0x15

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->spanYForLandscape:I

    .line 177
    const/16 v0, 0x15

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->spanXForPortrait:I

    .line 178
    iput v4, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->spanYForPortrait:I

    .line 181
    const/high16 v0, 0x42340000    # 45.0f

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->XRatioForLandscape:F

    .line 182
    const/high16 v0, 0x41c80000    # 25.0f

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->YRatioForLandscape:F

    .line 183
    const/high16 v0, 0x41f00000    # 30.0f

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->XRatioForPortrait:F

    .line 184
    const/high16 v0, 0x42380000    # 46.0f

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->YRatioForPortrait:F

    .line 185
    iput v2, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->XRatioAdjustPortrait:F

    .line 186
    iput v2, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->XRatioAdjustLandscape:F

    .line 187
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->intensityForRipple:F

    .line 192
    iput v3, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->windowWidth:I

    .line 193
    iput v3, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->windowHeight:I

    .line 194
    iput v3, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->minLength:I

    .line 197
    iput v2, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->downX:F

    .line 198
    iput v2, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->downY:F

    .line 199
    const-wide v0, 0x4062c00000000000L    # 150.0

    iput-wide v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->rippleDragThreshold:D

    .line 200
    iput v3, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->rippleDistance:I

    .line 203
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->diffPressTime:J

    .line 204
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->prevPressTime:J

    .line 205
    iput v3, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->drawCount:I

    .line 206
    iput-object v5, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->mListener:Lcom/samsung/android/visualeffect/IEffectListener;

    .line 212
    iput-boolean v6, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->isMakedASpenToucdUp:Z

    .line 214
    iput v2, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->defaultX:F

    .line 215
    iput v2, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->defaultY:F

    .line 220
    iput-boolean v3, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->isTouched:Z

    .line 221
    iput v3, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->INK_DISABLE:I

    .line 222
    iput v3, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->mInkEffectColor:I

    .line 223
    new-array v0, v4, [F

    iput-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->inkColors:[F

    .line 224
    iput-object v5, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->mContext:Landroid/content/Context;

    .line 225
    iput v3, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->spenUspLevel:I

    .line 226
    iput-boolean v3, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->isSPenSupport:Z

    .line 229
    const/16 v0, 0x9

    new-array v0, v0, [[F

    new-array v1, v4, [F

    fill-array-data v1, :array_0

    aput-object v1, v0, v3

    new-array v1, v4, [F

    fill-array-data v1, :array_1

    aput-object v1, v0, v6

    const/4 v1, 0x2

    new-array v2, v4, [F

    fill-array-data v2, :array_2

    aput-object v2, v0, v1

    new-array v1, v4, [F

    fill-array-data v1, :array_3

    aput-object v1, v0, v4

    const/4 v1, 0x4

    new-array v2, v4, [F

    fill-array-data v2, :array_4

    aput-object v2, v0, v1

    const/4 v1, 0x5

    new-array v2, v4, [F

    fill-array-data v2, :array_5

    aput-object v2, v0, v1

    const/4 v1, 0x6

    new-array v2, v4, [F

    fill-array-data v2, :array_6

    aput-object v2, v0, v1

    const/4 v1, 0x7

    new-array v2, v4, [F

    fill-array-data v2, :array_7

    aput-object v2, v0, v1

    const/16 v1, 0x8

    new-array v2, v4, [F

    fill-array-data v2, :array_8

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->inkColorFromSetting:[[F

    .line 244
    iput-object v5, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->mBgChangeCheckQueue:Ljava/util/Queue;

    .line 245
    iput-object v5, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->mEffectChangeCheckQueue:Ljava/util/Queue;

    .line 248
    iput-boolean v3, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->isOrientationChanged:Z

    .line 249
    iput v3, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->isOrientationChangCount:I

    .line 250
    iput-boolean v3, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->isSurfaceChanged:Z

    .line 255
    iput v3, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->RIPPLE_LIGHT:I

    .line 256
    iput v6, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->RIPPLE_LIGHT_WITH_INK:I

    .line 257
    iput v3, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->effectType:I

    .line 258
    iput v3, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->preEffectType:I

    .line 263
    const/4 v0, 0x2

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->UPDATE_TYPE_USER_SWITCHING:I

    .line 264
    iput v3, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->BMP_ENVIRONMENTIMG:I

    .line 265
    iput v6, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->BMP_PORTRAITIMG:I

    .line 266
    const/4 v0, 0x2

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->BMP_LANDSCAPEIMG:I

    .line 280
    iput-boolean v3, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->isIndigoDiffusion:Z

    return-void

    .line 229
    :array_0
    .array-data 4
        0x0
        0x3ecccccd    # 0.4f
        0x3f800000    # 1.0f
    .end array-data

    :array_1
    .array-data 4
        0x3f43c3b5
        0x3ef0f0e9
        0x3f0c8c82
    .end array-data

    :array_2
    .array-data 4
        0x3f3ebebe
        0x3edcdcca
        0x3df0f0e9    # 0.117647f
    .end array-data

    :array_3
    .array-data 4
        0x3e8c8c72
        0x3f028273
        0x3df0f0e9    # 0.117647f
    .end array-data

    :array_4
    .array-data 4
        0x3d209fe8
        0x3eaaaa9f
        0x3f7afaf8
    .end array-data

    :array_5
    .array-data 4
        0x0
        0x3df0f0e9    # 0.117647f
        0x3eb4b4af
    .end array-data

    :array_6
    .array-data 4
        0x3eb4b4af
        0x3e70f0e9    # 0.235294f
        0x3f34b4af
    .end array-data

    :array_7
    .array-data 4
        0x3e5cdcca
        0x3dc8c8ac
        0x3d209fe8
    .end array-data

    :array_8
    .array-data 4
        0x3ea0a090
        0x3f20a090
        0x3f34b4af
    .end array-data
.end method


# virtual methods
.method public declared-synchronized changeBackground(Landroid/graphics/Bitmap;)V
    .locals 2
    .param p1, "bmp"    # Landroid/graphics/Bitmap;

    .prologue
    .line 1001
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->TAG:Ljava/lang/String;

    const-string v1, "changeBackground()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1002
    if-nez p1, :cond_0

    .line 1004
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->TAG:Ljava/lang/String;

    const-string v1, "bmp == null"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1017
    :goto_0
    monitor-exit p0

    return-void

    .line 1008
    :cond_0
    :try_start_1
    invoke-virtual {p0, p1}, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->setBackground(Landroid/graphics/Bitmap;)V

    .line 1009
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->mBgChangeCheckQueue:Ljava/util/Queue;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    .line 1011
    iget-boolean v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->isIndigoDiffusion:Z

    if-nez v0, :cond_1

    .line 1013
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->setRippleVersion(Z)V

    .line 1016
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->mParent:Lcom/samsung/android/visualeffect/common/GLTextureView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/android/visualeffect/common/GLTextureView;->setRenderMode(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1001
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized checkBackground()V
    .locals 2

    .prologue
    .line 1032
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->mBgChangeCheckQueue:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1034
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->TAG:Ljava/lang/String;

    const-string v1, "Change opengl BG Texture"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1035
    invoke-virtual {p0}, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->loadBGTexture()V

    .line 1036
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->mBgChangeCheckQueue:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1038
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->mBgChangeCheckQueue:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->remove()Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1032
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1041
    :cond_0
    monitor-exit p0

    return-void
.end method

.method public declared-synchronized checkEffectChange()V
    .locals 2

    .prologue
    .line 1020
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->mEffectChangeCheckQueue:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1022
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->TAG:Ljava/lang/String;

    const-string v1, "Change Effect Type and loadShaderSetting()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1023
    invoke-virtual {p0}, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->loadShaderSetting()V

    .line 1024
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->mEffectChangeCheckQueue:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1026
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->mEffectChangeCheckQueue:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->remove()Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1020
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1029
    :cond_0
    monitor-exit p0

    return-void
.end method

.method public checkOrientationChanged()V
    .locals 3

    .prologue
    .line 492
    iget-boolean v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->isOrientationChanged:Z

    if-eqz v0, :cond_1

    .line 494
    iget-boolean v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->isSurfaceChanged:Z

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->isOrientationChangCount:I

    const/16 v1, 0x14

    if-le v0, v1, :cond_2

    .line 496
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "= onConfigurationChanged = onDrawFrame isSurfaceChanged == true && isOrientationChanged == true, isOrientationChangCount = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->isOrientationChangCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 497
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->isOrientationChanged:Z

    .line 504
    :cond_1
    :goto_0
    return-void

    .line 501
    :cond_2
    iget v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->isOrientationChangCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->isOrientationChangCount:I

    goto :goto_0
.end method

.method public declared-synchronized clearAllEffect()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 829
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->clearRipple()V

    .line 831
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->isTouched:Z

    .line 832
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->isOrientationChanged:Z

    .line 834
    iget v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->effectType:I

    if-ne v0, v1, :cond_0

    .line 836
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->TAG:Ljava/lang/String;

    const-string v1, "clearInkValue"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 837
    invoke-virtual {p0}, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->native_clear()V

    .line 840
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->mParent:Lcom/samsung/android/visualeffect/common/GLTextureView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/android/visualeffect/common/GLTextureView;->setRenderMode(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 841
    monitor-exit p0

    return-void

    .line 829
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public clearRipple()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 808
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->TAG:Ljava/lang/String;

    const-string v1, "clearRipple"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 810
    iget v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->NUM_DETAILS_WIDTH:I

    if-nez v0, :cond_1

    .line 825
    :cond_0
    :goto_0
    return-void

    .line 813
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->velocity:[F

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->heights:[F

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->velocity:[F

    array-length v0, v0

    iget v1, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->max:I

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->heights:[F

    array-length v0, v0

    iget v1, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->max:I

    if-lt v0, v1, :cond_0

    .line 818
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->heights:[F

    invoke-static {v0, v2}, Ljava/util/Arrays;->fill([FF)V

    .line 819
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->velocity:[F

    invoke-static {v0, v2}, Ljava/util/Arrays;->fill([FF)V

    .line 820
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->heightsSub1:[F

    invoke-static {v0, v2}, Ljava/util/Arrays;->fill([FF)V

    .line 821
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->velocitySub1:[F

    invoke-static {v0, v2}, Ljava/util/Arrays;->fill([FF)V

    .line 822
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->heightsSub2:[F

    invoke-static {v0, v2}, Ljava/util/Arrays;->fill([FF)V

    .line 823
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->velocitySub2:[F

    invoke-static {v0, v2}, Ljava/util/Arrays;->fill([FF)V

    .line 824
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->gpuHeights:[F

    invoke-static {v0, v2}, Ljava/util/Arrays;->fill([FF)V

    goto :goto_0
.end method

.method public getCenterCropBitmap(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;
    .locals 10
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    const/4 v9, 0x0

    .line 975
    iget-object v7, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->TAG:Ljava/lang/String;

    const-string v8, "getCenterCropBitmap()"

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 978
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    .line 979
    .local v2, "bitmapWidth":I
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    .line 980
    .local v0, "bitmapHeight":I
    int-to-float v7, p2

    int-to-float v8, p3

    div-float v4, v7, v8

    .line 981
    .local v4, "ratio":F
    int-to-float v7, v2

    int-to-float v8, v0

    div-float v1, v7, v8

    .line 983
    .local v1, "bitmapRatio":F
    cmpl-float v7, v1, v4

    if-lez v7, :cond_0

    .line 986
    iget-object v7, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->TAG:Ljava/lang/String;

    const-string v8, "bmp is horizontally"

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 987
    int-to-float v7, v0

    mul-float/2addr v7, v4

    float-to-int v6, v7

    .line 988
    .local v6, "targetWidth":I
    sub-int v7, v2, v6

    div-int/lit8 v7, v7, 0x2

    invoke-static {p1, v7, v9, v6, v0}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 997
    .end local v6    # "targetWidth":I
    .local v3, "finalBitmap":Landroid/graphics/Bitmap;
    :goto_0
    return-object v3

    .line 992
    .end local v3    # "finalBitmap":Landroid/graphics/Bitmap;
    :cond_0
    iget-object v7, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->TAG:Ljava/lang/String;

    const-string v8, "bmp is vertically"

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 993
    int-to-float v7, v2

    div-float/2addr v7, v4

    float-to-int v5, v7

    .line 994
    .local v5, "targetHeight":I
    sub-int v7, v0, v5

    div-int/lit8 v7, v7, 0x2

    invoke-static {p1, v9, v7, v2, v5}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;

    move-result-object v3

    .restart local v3    # "finalBitmap":Landroid/graphics/Bitmap;
    goto :goto_0
.end method

.method public handleTouchEvent(IIFFF)V
    .locals 14
    .param p1, "action"    # I
    .param p2, "sourceType"    # I
    .param p3, "x"    # F
    .param p4, "y"    # F
    .param p5, "pressure"    # F

    .prologue
    .line 521
    iget v7, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->drawCount:I

    if-nez v7, :cond_1

    .line 523
    iget-object v7, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->TAG:Ljava/lang/String;

    const-string v8, "drawCount == 0 Touch Return"

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 617
    :cond_0
    :goto_0
    return-void

    .line 527
    :cond_1
    move/from16 v0, p3

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->mouseX:F

    .line 528
    move/from16 v0, p4

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->mouseY:F

    .line 530
    iget-boolean v7, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->mLandscape:Z

    if-eqz v7, :cond_6

    .line 531
    iget v7, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->mouseX:F

    iget v8, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->mScreenWidth:I

    int-to-float v8, v8

    const/high16 v9, 0x40000000    # 2.0f

    div-float/2addr v8, v9

    sub-float/2addr v7, v8

    iget v8, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->XRatioAdjustLandscape:F

    sub-float/2addr v7, v8

    iput v7, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->glX:F

    .line 532
    iget v7, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->glX:F

    iget v8, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->XRatioForLandscape:F

    iget v9, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->mScreenWidth:I

    int-to-float v9, v9

    div-float/2addr v8, v9

    mul-float/2addr v7, v8

    iput v7, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->glX:F

    .line 533
    iget v7, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->mScreenHeight:I

    int-to-float v7, v7

    iget v8, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->mouseY:F

    sub-float/2addr v7, v8

    iget v8, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->mScreenHeight:I

    int-to-float v8, v8

    const/high16 v9, 0x40000000    # 2.0f

    div-float/2addr v8, v9

    sub-float/2addr v7, v8

    iput v7, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->glY:F

    .line 534
    iget v7, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->glY:F

    neg-float v7, v7

    iget v8, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->YRatioForLandscape:F

    iget v9, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->mScreenHeight:I

    int-to-float v9, v9

    div-float/2addr v8, v9

    mul-float/2addr v7, v8

    iput v7, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->glY:F

    .line 542
    :goto_1
    iget v7, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->effectType:I

    const/4 v8, 0x1

    if-ne v7, v8, :cond_5

    .line 544
    move/from16 v5, p5

    .line 546
    .local v5, "mPressure":F
    iget-boolean v7, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->isIndigoDiffusion:Z

    if-nez v7, :cond_2

    float-to-double v8, v5

    const-wide/high16 v10, 0x3ff0000000000000L    # 1.0

    cmpl-double v7, v8, v10

    if-ltz v7, :cond_3

    .line 548
    :cond_2
    const/high16 v5, 0x3f800000    # 1.0f

    .line 551
    :cond_3
    const/4 v7, 0x3

    if-eq p1, v7, :cond_4

    const/4 v7, 0x1

    if-ne p1, v7, :cond_7

    .line 553
    :cond_4
    iget v7, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->mouseX:F

    float-to-int v7, v7

    iget v8, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->mouseY:F

    float-to-int v8, v8

    const/4 v9, 0x1

    invoke-virtual {p0, v7, v8, v9, v5}, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->native_onTouch(IIIF)V

    .line 554
    const/4 v7, 0x1

    iput-boolean v7, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->isMakedASpenToucdUp:Z

    .line 563
    .end local v5    # "mPressure":F
    :cond_5
    :goto_2
    if-nez p1, :cond_9

    .line 566
    const/4 v7, 0x1

    iput-boolean v7, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->isTouched:Z

    .line 567
    const/4 v7, 0x0

    iput-boolean v7, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->isOrientationChanged:Z

    .line 568
    iget v7, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->mouseX:F

    iput v7, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->downX:F

    .line 569
    iget v7, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->mouseY:F

    iput v7, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->downY:F

    .line 570
    const/4 v7, 0x0

    iput v7, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->rippleDistance:I

    .line 571
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v8

    iput-wide v8, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->prevPressTime:J

    .line 572
    const-wide/16 v8, 0x0

    iput-wide v8, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->diffPressTime:J

    .line 574
    iget v7, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->glY:F

    iget v8, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->glX:F

    iget v9, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->intensityForRipple:F

    const/high16 v10, 0x40800000    # 4.0f

    mul-float/2addr v9, v10

    invoke-virtual {p0, v7, v8, v9}, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->ripple(FFF)V

    .line 576
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    .line 577
    .local v6, "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v7, "sound"

    const-string v8, "down"

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 578
    iget-object v7, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->mListener:Lcom/samsung/android/visualeffect/IEffectListener;

    const/4 v8, 0x1

    invoke-interface {v7, v8, v6}, Lcom/samsung/android/visualeffect/IEffectListener;->onReceive(ILjava/util/HashMap;)V

    goto/16 :goto_0

    .line 536
    .end local v6    # "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_6
    iget v7, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->mouseX:F

    iget v8, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->mScreenWidth:I

    int-to-float v8, v8

    const/high16 v9, 0x40000000    # 2.0f

    div-float/2addr v8, v9

    sub-float/2addr v7, v8

    iget v8, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->XRatioAdjustPortrait:F

    sub-float/2addr v7, v8

    iput v7, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->glX:F

    .line 537
    iget v7, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->glX:F

    iget v8, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->XRatioForPortrait:F

    iget v9, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->mScreenWidth:I

    int-to-float v9, v9

    div-float/2addr v8, v9

    mul-float/2addr v7, v8

    iput v7, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->glX:F

    .line 538
    iget v7, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->mScreenHeight:I

    int-to-float v7, v7

    iget v8, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->mouseY:F

    sub-float/2addr v7, v8

    iget v8, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->mScreenHeight:I

    int-to-float v8, v8

    const/high16 v9, 0x40000000    # 2.0f

    div-float/2addr v8, v9

    sub-float/2addr v7, v8

    iput v7, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->glY:F

    .line 539
    iget v7, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->glY:F

    neg-float v7, v7

    iget v8, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->YRatioForPortrait:F

    iget v9, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->mScreenHeight:I

    int-to-float v9, v9

    div-float/2addr v8, v9

    mul-float/2addr v7, v8

    iput v7, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->glY:F

    goto/16 :goto_1

    .line 556
    .restart local v5    # "mPressure":F
    :cond_7
    iget-boolean v7, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->isIndigoDiffusion:Z

    if-nez v7, :cond_8

    move/from16 v0, p2

    and-int/lit16 v7, v0, 0x4002

    const/16 v8, 0x4002

    if-ne v7, v8, :cond_5

    .line 558
    :cond_8
    iget v7, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->mouseX:F

    float-to-int v7, v7

    iget v8, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->mouseY:F

    float-to-int v8, v8

    invoke-virtual {p0, v7, v8, p1, v5}, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->native_onTouch(IIIF)V

    .line 559
    const/4 v7, 0x0

    iput-boolean v7, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->isMakedASpenToucdUp:Z

    goto/16 :goto_2

    .line 580
    .end local v5    # "mPressure":F
    :cond_9
    const/4 v7, 0x2

    if-ne p1, v7, :cond_a

    .line 582
    const/4 v7, 0x1

    iput-boolean v7, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->isTouched:Z

    .line 583
    iget v7, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->mouseX:F

    iget v8, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->downX:F

    sub-float v3, v7, v8

    .line 584
    .local v3, "dx":F
    iget v7, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->mouseY:F

    iget v8, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->downY:F

    sub-float v4, v7, v8

    .line 585
    .local v4, "dy":F
    float-to-double v8, v3

    const-wide/high16 v10, 0x4000000000000000L    # 2.0

    invoke-static {v8, v9, v10, v11}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v8

    float-to-double v10, v4

    const-wide/high16 v12, 0x4000000000000000L    # 2.0

    invoke-static {v10, v11, v12, v13}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v10

    add-double/2addr v8, v10

    invoke-static {v8, v9}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v8

    double-to-int v2, v8

    .line 586
    .local v2, "distForwWave":I
    iget v7, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->rippleDistance:I

    add-int/2addr v7, v2

    iput v7, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->rippleDistance:I

    .line 587
    iget v7, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->mouseX:F

    iput v7, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->downX:F

    .line 588
    iget v7, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->mouseY:F

    iput v7, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->downY:F

    .line 590
    iget v7, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->rippleDistance:I

    int-to-double v8, v7

    iget-wide v10, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->rippleDragThreshold:D

    cmpl-double v7, v8, v10

    if-lez v7, :cond_0

    .line 591
    const/4 v7, 0x0

    iput v7, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->rippleDistance:I

    .line 593
    iget v7, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->glY:F

    iget v8, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->glX:F

    iget v9, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->intensityForRipple:F

    const/high16 v10, 0x40400000    # 3.0f

    mul-float/2addr v9, v10

    invoke-virtual {p0, v7, v8, v9}, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->ripple(FFF)V

    .line 595
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    .line 596
    .restart local v6    # "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v7, "sound"

    const-string v8, "drag"

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 597
    iget-object v7, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->mListener:Lcom/samsung/android/visualeffect/IEffectListener;

    const/4 v8, 0x1

    invoke-interface {v7, v8, v6}, Lcom/samsung/android/visualeffect/IEffectListener;->onReceive(ILjava/util/HashMap;)V

    goto/16 :goto_0

    .line 600
    .end local v2    # "distForwWave":I
    .end local v3    # "dx":F
    .end local v4    # "dy":F
    .end local v6    # "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_a
    const/4 v7, 0x1

    if-ne p1, v7, :cond_b

    .line 602
    const/4 v7, 0x0

    iput-boolean v7, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->isTouched:Z

    .line 604
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v8

    iget-wide v10, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->prevPressTime:J

    sub-long/2addr v8, v10

    iput-wide v8, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->diffPressTime:J

    .line 606
    iget-wide v8, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->diffPressTime:J

    const-wide/16 v10, 0x258

    cmp-long v7, v8, v10

    if-lez v7, :cond_0

    .line 608
    iget v7, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->glY:F

    iget v8, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->glX:F

    iget v9, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->intensityForRipple:F

    const/high16 v10, 0x40800000    # 4.0f

    mul-float/2addr v9, v10

    invoke-virtual {p0, v7, v8, v9}, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->ripple(FFF)V

    .line 610
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    .line 611
    .restart local v6    # "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v7, "sound"

    const-string v8, "down"

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 612
    iget-object v7, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->mListener:Lcom/samsung/android/visualeffect/IEffectListener;

    const/4 v8, 0x1

    invoke-interface {v7, v8, v6}, Lcom/samsung/android/visualeffect/IEffectListener;->onReceive(ILjava/util/HashMap;)V

    goto/16 :goto_0

    .line 614
    .end local v6    # "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_b
    const/4 v7, 0x3

    if-ne p1, v7, :cond_0

    .line 615
    const/4 v7, 0x0

    iput-boolean v7, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->isTouched:Z

    goto/16 :goto_0
.end method

.method public initWaters()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 638
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->TAG:Ljava/lang/String;

    const-string v1, "initWaters"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 640
    iget v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->VCOUNT:I

    mul-int/lit8 v0, v0, 0x3

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->vertices:[F

    .line 641
    iget v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->SURFACE_DETAILS_WIDTH:I

    add-int/lit8 v0, v0, -0x1

    iget v1, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->SURFACE_DETAILS_HEIGHT:I

    add-int/lit8 v1, v1, -0x1

    mul-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x6

    new-array v0, v0, [S

    iput-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->indices:[S

    .line 643
    iget v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->max:I

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->heights:[F

    .line 644
    iget v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->max:I

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->velocity:[F

    .line 645
    iget v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->max:I

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->heightsSub1:[F

    .line 646
    iget v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->max:I

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->velocitySub1:[F

    .line 647
    iget v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->max:I

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->heightsSub2:[F

    .line 648
    iget v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->max:I

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->velocitySub2:[F

    .line 649
    iget v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->VCOUNT:I

    mul-int/lit8 v0, v0, 0x3

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->gpuHeights:[F

    .line 651
    invoke-virtual {p0}, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->native_initWaters()V

    .line 654
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->heights:[F

    invoke-static {v0, v2}, Ljava/util/Arrays;->fill([FF)V

    .line 655
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->velocity:[F

    invoke-static {v0, v2}, Ljava/util/Arrays;->fill([FF)V

    .line 656
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->heightsSub1:[F

    invoke-static {v0, v2}, Ljava/util/Arrays;->fill([FF)V

    .line 657
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->velocitySub1:[F

    invoke-static {v0, v2}, Ljava/util/Arrays;->fill([FF)V

    .line 658
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->heightsSub2:[F

    invoke-static {v0, v2}, Ljava/util/Arrays;->fill([FF)V

    .line 659
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->velocitySub2:[F

    invoke-static {v0, v2}, Ljava/util/Arrays;->fill([FF)V

    .line 660
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->gpuHeights:[F

    invoke-static {v0, v2}, Ljava/util/Arrays;->fill([FF)V

    .line 661
    return-void
.end method

.method public declared-synchronized loadBGTexture()V
    .locals 2

    .prologue
    .line 927
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->TAG:Ljava/lang/String;

    const-string v1, "loadBGTexture"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 929
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->bitmapPortraitBG:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 931
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->TAG:Ljava/lang/String;

    const-string v1, "transferBitmap bitmapPortraitBG"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 932
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->bitmapPortraitBG:Landroid/graphics/Bitmap;

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->native_LoadTextures(Landroid/graphics/Bitmap;I)V

    .line 933
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->bitmapPortraitBG:Landroid/graphics/Bitmap;

    .line 940
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->bitmapLandscapeBG:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    .line 942
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->TAG:Ljava/lang/String;

    const-string v1, "transferBitmap bitmapLandscapeBG"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 943
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->bitmapLandscapeBG:Landroid/graphics/Bitmap;

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->native_LoadTextures(Landroid/graphics/Bitmap;I)V

    .line 944
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->bitmapLandscapeBG:Landroid/graphics/Bitmap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 950
    :goto_1
    monitor-exit p0

    return-void

    .line 937
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->TAG:Ljava/lang/String;

    const-string v1, "bitmapPortraitBG is null"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 927
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 948
    :cond_1
    :try_start_2
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->TAG:Ljava/lang/String;

    const-string v1, "bitmapLandscapeBG is null"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method public declared-synchronized loadEnvironmentTexture()V
    .locals 2

    .prologue
    .line 954
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->TAG:Ljava/lang/String;

    const-string v1, "loadEnvironmentTexture"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 956
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->bitmapEnvironmentBG:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 958
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->TAG:Ljava/lang/String;

    const-string v1, "transferWaterBitmap"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 959
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->bitmapEnvironmentBG:Landroid/graphics/Bitmap;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->native_LoadTextures(Landroid/graphics/Bitmap;I)V

    .line 960
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->bitmapEnvironmentBG:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 961
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->bitmapEnvironmentBG:Landroid/graphics/Bitmap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 967
    :goto_0
    monitor-exit p0

    return-void

    .line 965
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->TAG:Ljava/lang/String;

    const-string v1, "bitmapEnvironmentBG is null"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 954
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public loadShaderSetting()V
    .locals 4

    .prologue
    .line 433
    const/4 v0, 0x0

    .line 434
    .local v0, "isInk":Z
    iget v1, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->effectType:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 435
    const/4 v0, 0x1

    .line 438
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isInk = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 439
    invoke-virtual {p0, v0}, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->native_loadShaderSetting(Z)V

    .line 440
    return-void
.end method

.method public declared-synchronized move()V
    .locals 15

    .prologue
    const/4 v12, 0x1

    .line 672
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->heights:[F

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->velocity:[F

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->heightsSub1:[F

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->velocitySub1:[F

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->heightsSub2:[F

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->velocitySub2:[F

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->gpuHeights:[F

    if-nez v0, :cond_2

    .line 681
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->TAG:Ljava/lang/String;

    const-string v5, "initWaters don\'t called"

    invoke-static {v0, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 746
    :cond_1
    monitor-exit p0

    return-void

    .line 686
    :cond_2
    const/4 v2, 0x1

    .line 687
    .local v2, "xSpan":I
    const/4 v1, 0x1

    .line 689
    .local v1, "ySpan":I
    :try_start_1
    iget-boolean v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->mLandscape:Z

    if-eqz v0, :cond_5

    .line 690
    iget v2, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->spanXForLandscape:I

    .line 691
    iget v1, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->spanYForLandscape:I

    .line 692
    iget v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->NUM_DETAILS_WIDTH:I

    sub-int v3, v0, v1

    .line 693
    .local v3, "imax":I
    iget v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->NUM_DETAILS_HEIGHT:I

    sub-int v4, v0, v2

    .line 701
    .local v4, "jmax":I
    :goto_0
    const/4 v10, 0x1

    .line 702
    .local v10, "result1":I
    const/4 v11, 0x1

    .line 704
    .local v11, "result2":I
    const/4 v5, 0x1

    const/high16 v6, 0x3f000000    # 0.5f

    move-object v0, p0

    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->native_move(IIIIZF)I

    move-result v0

    if-eqz v0, :cond_3

    if-eqz v10, :cond_3

    if-eqz v11, :cond_3

    .line 706
    iget v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->drawCount:I

    const/4 v5, 0x2

    if-lt v0, v5, :cond_3

    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->mBgChangeCheckQueue:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->size()I

    move-result v0

    if-nez v0, :cond_3

    .line 708
    iget v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->effectType:I

    if-ne v0, v12, :cond_6

    .line 710
    iget-boolean v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->isTouched:Z

    if-nez v0, :cond_3

    .line 712
    invoke-virtual {p0}, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->setRenderModeDirty()V

    .line 720
    :cond_3
    :goto_1
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_2
    iget v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->SURFACE_DETAILS_HEIGHT:I

    if-ge v7, v0, :cond_1

    .line 721
    const/4 v9, 0x0

    .local v9, "j":I
    :goto_3
    iget v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->SURFACE_DETAILS_WIDTH:I

    if-ge v9, v0, :cond_7

    .line 722
    iget v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->SURFACE_DETAILS_HEIGHT:I

    mul-int/2addr v0, v9

    add-int/2addr v0, v7

    mul-int/lit8 v8, v0, 0x3

    .line 723
    .local v8, "idx":I
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->gpuHeights:[F

    add-int/lit8 v5, v8, 0x0

    iget-object v6, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->heights:[F

    add-int/lit8 v12, v9, 0x2

    iget v13, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->NUM_DETAILS_WIDTH:I

    mul-int/2addr v12, v13

    add-int/2addr v12, v7

    add-int/lit8 v12, v12, 0x2

    aget v6, v6, v12

    aput v6, v0, v5

    .line 725
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->gpuHeights:[F

    add-int/lit8 v5, v8, 0x1

    iget-object v6, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->heights:[F

    add-int/lit8 v12, v9, 0x2

    iget v13, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->NUM_DETAILS_WIDTH:I

    mul-int/2addr v12, v13

    add-int/2addr v12, v7

    add-int/lit8 v12, v12, 0x1

    aget v6, v6, v12

    aput v6, v0, v5

    .line 727
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->gpuHeights:[F

    add-int/lit8 v5, v8, 0x2

    iget-object v6, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->heights:[F

    add-int/lit8 v12, v9, 0x1

    iget v13, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->NUM_DETAILS_WIDTH:I

    mul-int/2addr v12, v13

    add-int/2addr v12, v7

    add-int/lit8 v12, v12, 0x2

    aget v6, v6, v12

    aput v6, v0, v5

    .line 730
    add-int/lit8 v0, v7, -0x7

    if-lez v0, :cond_4

    .line 731
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->gpuHeights:[F

    add-int/lit8 v5, v8, 0x0

    aget v6, v0, v5

    iget-object v12, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->heightsSub1:[F

    add-int/lit8 v13, v9, 0x2

    iget v14, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->NUM_DETAILS_WIDTH:I

    mul-int/2addr v13, v14

    add-int/2addr v13, v7

    add-int/lit8 v13, v13, -0x6

    aget v12, v12, v13

    add-float/2addr v6, v12

    aput v6, v0, v5

    .line 733
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->gpuHeights:[F

    add-int/lit8 v5, v8, 0x1

    aget v6, v0, v5

    iget-object v12, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->heightsSub1:[F

    add-int/lit8 v13, v9, 0x2

    iget v14, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->NUM_DETAILS_WIDTH:I

    mul-int/2addr v13, v14

    add-int/2addr v13, v7

    add-int/lit8 v13, v13, -0x7

    aget v12, v12, v13

    add-float/2addr v6, v12

    aput v6, v0, v5

    .line 735
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->gpuHeights:[F

    add-int/lit8 v5, v8, 0x2

    aget v6, v0, v5

    iget-object v12, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->heightsSub1:[F

    add-int/lit8 v13, v9, 0x1

    iget v14, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->NUM_DETAILS_WIDTH:I

    mul-int/2addr v13, v14

    add-int/2addr v13, v7

    add-int/lit8 v13, v13, -0x6

    aget v12, v12, v13

    add-float/2addr v6, v12

    aput v6, v0, v5

    .line 737
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->gpuHeights:[F

    add-int/lit8 v5, v8, 0x0

    aget v6, v0, v5

    iget-object v12, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->heightsSub2:[F

    add-int/lit8 v13, v9, 0x2

    iget v14, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->NUM_DETAILS_WIDTH:I

    mul-int/2addr v13, v14

    add-int/2addr v13, v7

    add-int/lit8 v13, v13, -0x6

    aget v12, v12, v13

    add-float/2addr v6, v12

    aput v6, v0, v5

    .line 739
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->gpuHeights:[F

    add-int/lit8 v5, v8, 0x1

    aget v6, v0, v5

    iget-object v12, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->heightsSub2:[F

    add-int/lit8 v13, v9, 0x2

    iget v14, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->NUM_DETAILS_WIDTH:I

    mul-int/2addr v13, v14

    add-int/2addr v13, v7

    add-int/lit8 v13, v13, -0x7

    aget v12, v12, v13

    add-float/2addr v6, v12

    aput v6, v0, v5

    .line 741
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->gpuHeights:[F

    add-int/lit8 v5, v8, 0x2

    aget v6, v0, v5

    iget-object v12, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->heightsSub2:[F

    add-int/lit8 v13, v9, 0x1

    iget v14, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->NUM_DETAILS_WIDTH:I

    mul-int/2addr v13, v14

    add-int/2addr v13, v7

    add-int/lit8 v13, v13, -0x6

    aget v12, v12, v13

    add-float/2addr v6, v12

    aput v6, v0, v5

    .line 721
    :cond_4
    add-int/lit8 v9, v9, 0x1

    goto/16 :goto_3

    .line 695
    .end local v3    # "imax":I
    .end local v4    # "jmax":I
    .end local v7    # "i":I
    .end local v8    # "idx":I
    .end local v9    # "j":I
    .end local v10    # "result1":I
    .end local v11    # "result2":I
    :cond_5
    iget v2, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->spanXForPortrait:I

    .line 696
    iget v1, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->spanYForPortrait:I

    .line 697
    iget v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->NUM_DETAILS_WIDTH:I

    sub-int v3, v0, v1

    .line 698
    .restart local v3    # "imax":I
    iget v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->NUM_DETAILS_HEIGHT:I

    sub-int v4, v0, v2

    .restart local v4    # "jmax":I
    goto/16 :goto_0

    .line 715
    .restart local v10    # "result1":I
    .restart local v11    # "result2":I
    :cond_6
    invoke-virtual {p0}, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->setRenderModeDirty()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_1

    .line 672
    .end local v1    # "ySpan":I
    .end local v2    # "xSpan":I
    .end local v3    # "imax":I
    .end local v4    # "jmax":I
    .end local v10    # "result1":I
    .end local v11    # "result2":I
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 720
    .restart local v1    # "ySpan":I
    .restart local v2    # "xSpan":I
    .restart local v3    # "imax":I
    .restart local v4    # "jmax":I
    .restart local v7    # "i":I
    .restart local v9    # "j":I
    .restart local v10    # "result1":I
    .restart local v11    # "result2":I
    :cond_7
    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_2
.end method

.method public native_LoadTextures(Landroid/graphics/Bitmap;I)V
    .locals 0
    .param p1, "bmp"    # Landroid/graphics/Bitmap;
    .param p2, "type"    # I

    .prologue
    .line 971
    return-void
.end method

.method public native_clear()V
    .locals 0

    .prologue
    .line 846
    return-void
.end method

.method public native_initWaters()V
    .locals 0

    .prologue
    .line 665
    return-void
.end method

.method public native_loadShaderSetting(Z)V
    .locals 0
    .param p1, "isInk"    # Z

    .prologue
    .line 444
    return-void
.end method

.method public native_move(IIIIZF)I
    .locals 1
    .param p1, "ySpan"    # I
    .param p2, "xSpan"    # I
    .param p3, "imax"    # I
    .param p4, "jmax"    # I
    .param p5, "control"    # Z
    .param p6, "speed"    # F

    .prologue
    .line 751
    const/4 v0, 0x0

    return v0
.end method

.method public native_onDrawFrame()V
    .locals 0

    .prologue
    .line 508
    return-void
.end method

.method public native_onInitUVHBuffer()V
    .locals 0

    .prologue
    .line 372
    return-void
.end method

.method public native_onTouch(IIIF)V
    .locals 0
    .param p1, "x"    # I
    .param p2, "y"    # I
    .param p3, "action"    # I
    .param p4, "pressure"    # F

    .prologue
    .line 621
    return-void
.end method

.method public native_ripple(FFF)V
    .locals 0
    .param p1, "mx"    # F
    .param p2, "my"    # F
    .param p3, "intensity"    # F

    .prologue
    .line 777
    return-void
.end method

.method public onConfigurationChanged()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 512
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->TAG:Ljava/lang/String;

    const-string v1, "= onConfigurationChanged = Renderer onConfigurationChanged"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 513
    iput-boolean v2, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->isOrientationChanged:Z

    .line 514
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->isOrientationChangCount:I

    .line 515
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->mParent:Lcom/samsung/android/visualeffect/common/GLTextureView;

    invoke-virtual {v0, v2}, Lcom/samsung/android/visualeffect/common/GLTextureView;->setRenderMode(I)V

    .line 516
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 1109
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->TAG:Ljava/lang/String;

    const-string v1, "destroyed"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1111
    return-void
.end method

.method public onDrawFrame(Ljavax/microedition/khronos/opengles/GL10;)V
    .locals 9
    .param p1, "gl"    # Ljavax/microedition/khronos/opengles/GL10;

    .prologue
    const/4 v8, 0x2

    const/4 v7, -0x1

    const/high16 v6, 0x447a0000    # 1000.0f

    .line 451
    iget v4, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->mScreenWidth:I

    iget v5, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->minLength:I

    if-lt v4, v5, :cond_0

    iget v4, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->mScreenHeight:I

    iget v5, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->minLength:I

    if-ge v4, v5, :cond_2

    .line 453
    :cond_0
    iget-object v4, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "onDrawFrame problem width "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->mScreenWidth:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->mScreenHeight:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "  disp "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->windowWidth:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->windowHeight:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 488
    :cond_1
    :goto_0
    return-void

    .line 457
    :cond_2
    iget-boolean v4, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->isIndigoDiffusion:Z

    if-nez v4, :cond_3

    .line 458
    invoke-virtual {p0}, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->checkEffectChange()V

    .line 460
    :cond_3
    invoke-virtual {p0}, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->checkBackground()V

    .line 461
    invoke-virtual {p0}, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->native_onDrawFrame()V

    .line 462
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->isSurfaceChanged:Z

    .line 464
    iget v4, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->drawCount:I

    if-lez v4, :cond_4

    iget-boolean v4, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->isOrientationChanged:Z

    if-nez v4, :cond_4

    .line 466
    invoke-virtual {p0}, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->move()V

    .line 469
    :cond_4
    iget v4, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->drawCount:I

    if-ge v4, v8, :cond_5

    .line 471
    iget v4, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->drawCount:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->drawCount:I

    .line 474
    :cond_5
    iget v4, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->framecounter:I

    if-ne v4, v7, :cond_6

    .line 475
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->timeStart:J

    .line 478
    :cond_6
    iget v4, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->framecounter:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->framecounter:I

    .line 479
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 480
    .local v2, "now":J
    iget-wide v4, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->timeStart:J

    sub-long v4, v2, v4

    long-to-float v1, v4

    .line 481
    .local v1, "spent":F
    cmpl-float v4, v1, v6

    if-ltz v4, :cond_1

    .line 482
    iget v4, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->framecounter:I

    if-le v4, v8, :cond_7

    .line 483
    iget v4, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->framecounter:I

    int-to-float v4, v4

    mul-float/2addr v4, v6

    div-float v0, v4, v1

    .line 484
    .local v0, "fps":F
    iget-object v4, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "fps "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 486
    .end local v0    # "fps":F
    :cond_7
    iput v7, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->framecounter:I

    goto :goto_0
.end method

.method public onSurfaceChanged(Ljavax/microedition/khronos/opengles/GL10;II)V
    .locals 17
    .param p1, "gl"    # Ljavax/microedition/khronos/opengles/GL10;
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    .line 380
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onSurfaceChanged, width = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, p2

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", height = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, p3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 382
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->isSurfaceChanged:Z

    .line 383
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->drawCount:I

    .line 385
    move/from16 v0, p2

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->mScreenWidth:I

    .line 386
    move/from16 v0, p3

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->mScreenHeight:I

    .line 388
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->mScreenWidth:I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->minLength:I

    if-lt v2, v3, :cond_0

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->mScreenHeight:I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->minLength:I

    if-ge v2, v3, :cond_1

    .line 390
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onSurfaceChanged problem width "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->mScreenWidth:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->mScreenHeight:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "  disp "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->windowWidth:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->windowHeight:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 429
    :goto_0
    return-void

    .line 394
    :cond_1
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->mScreenWidth:I

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->mScreenHeight:I

    int-to-float v3, v3

    div-float v13, v2, v3

    .line 396
    .local v13, "ratio":F
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->view:[F

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/high16 v6, 0x3f800000    # 1.0f

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/high16 v11, 0x3f800000    # 1.0f

    const/4 v12, 0x0

    invoke-static/range {v2 .. v12}, Landroid/opengl/Matrix;->setLookAtM([FIFFFFFFFFF)V

    .line 397
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->proj:[F

    const/high16 v4, 0x42340000    # 45.0f

    const v6, 0x3dcccccd    # 0.1f

    const/high16 v7, 0x43fa0000    # 500.0f

    move-object/from16 v2, p0

    move v5, v13

    invoke-virtual/range {v2 .. v7}, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->perspectiveM([FFFFF)V

    .line 399
    const/4 v14, 0x0

    .line 400
    .local v14, "translateX":F
    const/4 v15, 0x0

    .line 401
    .local v15, "translateY":F
    const/16 v16, 0x0

    .line 403
    .local v16, "translateZ":F
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->mScreenWidth:I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->mScreenHeight:I

    if-le v2, v3, :cond_2

    .line 405
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->mLandscape:Z

    .line 406
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->intensityForLandscape:F

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->intensityForRipple:F

    .line 407
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->windowWidth:I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->windowHeight:I

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->windowWidth:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->windowHeight:I

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v2, v3

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->mBitmapRatio:F

    .line 408
    move-object/from16 v0, p0

    iget v14, v0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->translateXForLandscape:F

    .line 409
    move-object/from16 v0, p0

    iget v15, v0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->translateYForLandscape:F

    .line 410
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->translateZForLandscape:F

    move/from16 v16, v0

    .line 422
    :goto_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->world:[F

    const/4 v3, 0x0

    invoke-static {v2, v3}, Landroid/opengl/Matrix;->setIdentityM([FI)V

    .line 423
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->wvp:[F

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->view:[F

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->world:[F

    const/4 v7, 0x0

    invoke-static/range {v2 .. v7}, Landroid/opengl/Matrix;->multiplyMM([FI[FI[FI)V

    .line 424
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->wvp:[F

    const/4 v3, 0x0

    move/from16 v0, v16

    invoke-static {v2, v3, v14, v15, v0}, Landroid/opengl/Matrix;->translateM([FIFFF)V

    .line 425
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->wvp:[F

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->proj:[F

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->wvp:[F

    const/4 v7, 0x0

    invoke-static/range {v2 .. v7}, Landroid/opengl/Matrix;->multiplyMM([FI[FI[FI)V

    .line 427
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->loadShaderSetting()V

    .line 428
    const/4 v2, -0x1

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->framecounter:I

    goto/16 :goto_0

    .line 414
    :cond_2
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->mLandscape:Z

    .line 415
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->intensityForPortrait:F

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->intensityForRipple:F

    .line 416
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->windowWidth:I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->windowHeight:I

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->windowWidth:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->windowHeight:I

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v2, v3

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->mBitmapRatio:F

    .line 417
    move-object/from16 v0, p0

    iget v14, v0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->translateXForPortrait:F

    .line 418
    move-object/from16 v0, p0

    iget v15, v0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->translateYForPortrait:F

    .line 419
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->translateZForPortrait:F

    move/from16 v16, v0

    goto :goto_1
.end method

.method public onSurfaceCreated(Ljavax/microedition/khronos/opengles/GL10;Ljavax/microedition/khronos/egl/EGLConfig;)V
    .locals 2
    .param p1, "gl"    # Ljavax/microedition/khronos/opengles/GL10;
    .param p2, "config"    # Ljavax/microedition/khronos/egl/EGLConfig;

    .prologue
    .line 365
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->TAG:Ljava/lang/String;

    const-string v1, "onSurfaceCreated"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 366
    invoke-virtual {p0}, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->native_onInitUVHBuffer()V

    .line 367
    invoke-virtual {p0}, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->loadEnvironmentTexture()V

    .line 368
    return-void
.end method

.method public perspectiveM([FFFFF)V
    .locals 9
    .param p1, "m"    # [F
    .param p2, "angle"    # F
    .param p3, "aspect"    # F
    .param p4, "near"    # F
    .param p5, "far"    # F

    .prologue
    const/4 v8, 0x0

    .line 787
    const-wide/high16 v2, 0x3fe0000000000000L    # 0.5

    const-wide v4, 0x400921fb54442d18L    # Math.PI

    float-to-double v6, p2

    sub-double/2addr v4, v6

    mul-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->tan(D)D

    move-result-wide v2

    double-to-float v0, v2

    .line 788
    .local v0, "f":F
    sub-float v1, p4, p5

    .line 789
    .local v1, "range":F
    const/4 v2, 0x0

    div-float v3, v0, p3

    aput v3, p1, v2

    .line 790
    const/4 v2, 0x1

    aput v8, p1, v2

    .line 791
    const/4 v2, 0x2

    aput v8, p1, v2

    .line 792
    const/4 v2, 0x3

    aput v8, p1, v2

    .line 793
    const/4 v2, 0x4

    aput v8, p1, v2

    .line 794
    const/4 v2, 0x5

    aput v0, p1, v2

    .line 795
    const/4 v2, 0x6

    aput v8, p1, v2

    .line 796
    const/4 v2, 0x7

    aput v8, p1, v2

    .line 797
    const/16 v2, 0x8

    aput v8, p1, v2

    .line 798
    const/16 v2, 0x9

    aput v8, p1, v2

    .line 799
    const/16 v2, 0xa

    div-float v3, p5, v1

    aput v3, p1, v2

    .line 800
    const/16 v2, 0xb

    const/high16 v3, -0x40800000    # -1.0f

    aput v3, p1, v2

    .line 801
    const/16 v2, 0xc

    aput v8, p1, v2

    .line 802
    const/16 v2, 0xd

    aput v8, p1, v2

    .line 803
    const/16 v2, 0xe

    mul-float v3, p4, p5

    div-float/2addr v3, v1

    aput v3, p1, v2

    .line 804
    const/16 v2, 0xf

    aput v8, p1, v2

    .line 805
    return-void
.end method

.method public declared-synchronized ripple(FFF)V
    .locals 3
    .param p1, "mx"    # F
    .param p2, "my"    # F
    .param p3, "intensity"    # F

    .prologue
    .line 769
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ripple(), mx = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", my = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", intensity = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 770
    invoke-virtual {p0, p1, p2, p3}, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->native_ripple(FFF)V

    .line 772
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->mParent:Lcom/samsung/android/visualeffect/common/GLTextureView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/android/visualeffect/common/GLTextureView;->setRenderMode(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 773
    monitor-exit p0

    return-void

    .line 769
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setBackground(Landroid/graphics/Bitmap;)V
    .locals 3
    .param p1, "bmp"    # Landroid/graphics/Bitmap;

    .prologue
    .line 912
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->TAG:Ljava/lang/String;

    const-string v1, "setBackground"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 914
    if-nez p1, :cond_0

    .line 916
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->TAG:Ljava/lang/String;

    const-string v1, "bmp is null"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 923
    :goto_0
    return-void

    .line 920
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->TAG:Ljava/lang/String;

    const-string v1, "TABLET_MODE, getCenterCropBitmap bitmapPortraitBG, bitmapLandscapeBG"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 921
    iget v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->windowWidth:I

    iget v1, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->windowHeight:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iget v1, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->windowWidth:I

    iget v2, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->windowHeight:I

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    invoke-virtual {p0, p1, v0, v1}, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->getCenterCropBitmap(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->bitmapPortraitBG:Landroid/graphics/Bitmap;

    .line 922
    iget v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->windowWidth:I

    iget v1, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->windowHeight:I

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iget v1, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->windowWidth:I

    iget v2, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->windowHeight:I

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-virtual {p0, p1, v0, v1}, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->getCenterCropBitmap(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->bitmapLandscapeBG:Landroid/graphics/Bitmap;

    goto :goto_0
.end method

.method public setListener(Lcom/samsung/android/visualeffect/IEffectListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/android/visualeffect/IEffectListener;

    .prologue
    .line 624
    iput-object p1, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->mListener:Lcom/samsung/android/visualeffect/IEffectListener;

    .line 625
    return-void
.end method

.method public setRenderModeDirty()V
    .locals 2

    .prologue
    .line 755
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->mParent:Lcom/samsung/android/visualeffect/common/GLTextureView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/visualeffect/common/GLTextureView;->setRenderMode(I)V

    .line 756
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->TAG:Ljava/lang/String;

    const-string v1, "RENDERMODE_WHEN_DIRTY!!!"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 757
    return-void
.end method

.method public declared-synchronized setResourcesBitmap(Landroid/graphics/Bitmap;)V
    .locals 2
    .param p1, "bmp"    # Landroid/graphics/Bitmap;

    .prologue
    .line 906
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->TAG:Ljava/lang/String;

    const-string v1, "setResourcesBitmap"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 907
    iput-object p1, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->bitmapEnvironmentBG:Landroid/graphics/Bitmap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 908
    monitor-exit p0

    return-void

    .line 906
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setRippleConfiguration(II)V
    .locals 9
    .param p1, "w"    # I
    .param p2, "h"    # I

    .prologue
    const/16 v8, 0xe

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/16 v5, 0x500

    const/16 v4, 0x320

    .line 284
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setRippleConfiguration, w = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", h = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 286
    iput p1, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->windowWidth:I

    .line 287
    iput p2, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->windowHeight:I

    .line 289
    iget v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->windowWidth:I

    iget v1, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->windowHeight:I

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    div-int/lit8 v0, v0, 0x5

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->minLength:I

    .line 292
    const v0, 0x3e4ccccd    # 0.2f

    iget v1, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->windowWidth:I

    iget v2, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->windowHeight:I

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v0, v1

    float-to-int v0, v0

    int-to-double v0, v0

    iput-wide v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->rippleDragThreshold:D

    .line 293
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "rippleDragThreshold = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->rippleDragThreshold:D

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 296
    iget v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->windowWidth:I

    const/16 v1, 0xa00

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->windowHeight:I

    const/16 v1, 0x640

    if-eq v0, v1, :cond_1

    :cond_0
    iget v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->windowWidth:I

    const/16 v1, 0x640

    if-ne v0, v1, :cond_2

    iget v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->windowHeight:I

    const/16 v1, 0xa00

    if-ne v0, v1, :cond_2

    .line 350
    :cond_1
    :goto_0
    iget v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->NUM_DETAILS_WIDTH:I

    add-int/lit8 v0, v0, -0x4

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->SURFACE_DETAILS_WIDTH:I

    .line 351
    iget v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->NUM_DETAILS_HEIGHT:I

    add-int/lit8 v0, v0, -0x4

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->SURFACE_DETAILS_HEIGHT:I

    .line 352
    iget v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->SURFACE_DETAILS_WIDTH:I

    iget v1, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->SURFACE_DETAILS_HEIGHT:I

    mul-int/2addr v0, v1

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->VCOUNT:I

    .line 354
    iget v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->NUM_DETAILS_WIDTH:I

    iget v1, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->NUM_DETAILS_HEIGHT:I

    mul-int/2addr v0, v1

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->max:I

    .line 356
    invoke-virtual {p0}, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->initWaters()V

    .line 357
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->setRippleVersion(Z)V

    .line 358
    return-void

    .line 317
    :cond_2
    iget v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->windowWidth:I

    if-ne v0, v5, :cond_3

    iget v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->windowHeight:I

    if-eq v0, v4, :cond_4

    :cond_3
    iget v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->windowWidth:I

    if-ne v0, v4, :cond_5

    iget v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->windowHeight:I

    if-ne v0, v5, :cond_5

    .line 319
    :cond_4
    const v0, -0x3e388937    # -24.933f

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->translateZForLandscape:F

    .line 321
    iput v7, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->spanXForLandscape:I

    .line 322
    const/16 v0, 0x15

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->spanYForLandscape:I

    .line 323
    const/16 v0, 0x13

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->spanXForPortrait:I

    .line 324
    iput v7, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->spanYForPortrait:I

    .line 326
    const/high16 v0, 0x42400000    # 48.0f

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->XRatioForLandscape:F

    .line 327
    const/high16 v0, 0x41d80000    # 27.0f

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->YRatioForLandscape:F

    goto :goto_0

    .line 330
    :cond_5
    iget v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->windowWidth:I

    const/16 v1, 0x2d0

    if-ne v0, v1, :cond_6

    iget v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->windowHeight:I

    if-eq v0, v5, :cond_1

    :cond_6
    iget v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->windowWidth:I

    if-ne v0, v5, :cond_7

    iget v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->windowHeight:I

    const/16 v1, 0x2d0

    if-eq v0, v1, :cond_1

    .line 333
    :cond_7
    iget v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->windowWidth:I

    const/16 v1, 0x21c

    if-ne v0, v1, :cond_8

    iget v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->windowHeight:I

    const/16 v1, 0x3c0

    if-eq v0, v1, :cond_1

    :cond_8
    iget v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->windowWidth:I

    const/16 v1, 0x3c0

    if-ne v0, v1, :cond_9

    iget v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->windowHeight:I

    const/16 v1, 0x21c

    if-eq v0, v1, :cond_1

    .line 336
    :cond_9
    iget v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->windowWidth:I

    const/16 v1, 0x1e0

    if-ne v0, v1, :cond_a

    iget v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->windowHeight:I

    if-eq v0, v4, :cond_b

    :cond_a
    iget v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->windowWidth:I

    if-ne v0, v4, :cond_1

    iget v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->windowHeight:I

    const/16 v1, 0x1e0

    if-ne v0, v1, :cond_1

    .line 338
    :cond_b
    const/16 v0, 0x4a

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->NUM_DETAILS_WIDTH:I

    .line 339
    const/16 v0, 0x4a

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->NUM_DETAILS_HEIGHT:I

    .line 341
    iput v6, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->spanXForLandscape:I

    .line 342
    iput v8, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->spanYForLandscape:I

    .line 343
    iput v8, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->spanXForPortrait:I

    .line 344
    iput v6, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->spanYForPortrait:I

    .line 346
    const/high16 v0, 0x41e00000    # 28.0f

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->XRatioForPortrait:F

    .line 347
    const/high16 v0, 0x42340000    # 45.0f

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->YRatioForPortrait:F

    goto/16 :goto_0
.end method

.method public setRippleVersion(Z)V
    .locals 6
    .param p1, "isCheckEffectChange"    # Z

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 849
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->TAG:Ljava/lang/String;

    const-string v1, "setRippleVersion"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 851
    iget-boolean v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->isIndigoDiffusion:Z

    if-eqz v0, :cond_0

    .line 853
    iput v3, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->effectType:I

    .line 902
    :goto_0
    return-void

    .line 857
    :cond_0
    iget-boolean v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->isSPenSupport:Z

    if-eqz v0, :cond_3

    .line 868
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mInkEffectColor = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->mInkEffectColor:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 870
    iget v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->mInkEffectColor:I

    if-eqz v0, :cond_2

    .line 872
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->TAG:Ljava/lang/String;

    const-string v1, "Def.MODE = ModeType.RIPPLE_LIGHT_WITH_INK"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 873
    iput v3, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->effectType:I

    .line 874
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->inkColors:[F

    iget-object v1, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->inkColorFromSetting:[[F

    iget v2, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->mInkEffectColor:I

    aget-object v1, v1, v2

    aget v1, v1, v4

    aput v1, v0, v4

    .line 875
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->inkColors:[F

    iget-object v1, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->inkColorFromSetting:[[F

    iget v2, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->mInkEffectColor:I

    aget-object v1, v1, v2

    aget v1, v1, v3

    aput v1, v0, v3

    .line 876
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->inkColors:[F

    iget-object v1, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->inkColorFromSetting:[[F

    iget v2, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->mInkEffectColor:I

    aget-object v1, v1, v2

    aget v1, v1, v5

    aput v1, v0, v5

    .line 884
    :goto_1
    if-eqz p1, :cond_1

    .line 886
    iget v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->preEffectType:I

    iget v1, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->effectType:I

    if-eq v0, v1, :cond_1

    .line 888
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "effectType = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->effectType:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", preEffectType = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->preEffectType:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 889
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->mEffectChangeCheckQueue:Ljava/util/Queue;

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    .line 890
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->mParent:Lcom/samsung/android/visualeffect/common/GLTextureView;

    invoke-virtual {v0, v3}, Lcom/samsung/android/visualeffect/common/GLTextureView;->setRenderMode(I)V

    .line 894
    :cond_1
    iget v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->effectType:I

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->preEffectType:I

    goto/16 :goto_0

    .line 880
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->TAG:Ljava/lang/String;

    const-string v1, "ModeType.RIPPLE_LIGHT"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 881
    iput v4, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->effectType:I

    goto :goto_1

    .line 898
    :cond_3
    iput v4, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->mInkEffectColor:I

    .line 899
    iput v4, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->effectType:I

    goto/16 :goto_0
.end method

.method public show()V
    .locals 2

    .prologue
    .line 1045
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->TAG:Ljava/lang/String;

    const-string v1, "show"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1046
    iget-boolean v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->isIndigoDiffusion:Z

    if-nez v0, :cond_0

    .line 1047
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->setRippleVersion(Z)V

    .line 1048
    :cond_0
    return-void
.end method

.method public showUnlockAffordance(JLandroid/graphics/Rect;)V
    .locals 3
    .param p1, "startDelay"    # J
    .param p3, "rect"    # Landroid/graphics/Rect;

    .prologue
    .line 1052
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->TAG:Ljava/lang/String;

    const-string v1, "showUnlockAffordance()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1056
    invoke-virtual {p3}, Landroid/graphics/Rect;->centerX()I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->defaultX:F

    .line 1057
    invoke-virtual {p3}, Landroid/graphics/Rect;->centerY()I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->defaultY:F

    .line 1059
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->mDefaultRunnable:Ljava/lang/Runnable;

    if-nez v0, :cond_0

    .line 1061
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->TAG:Ljava/lang/String;

    const-string v1, "mDefaultRunnable,  new Runnable()!!!"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1063
    new-instance v0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType$1;-><init>(Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;)V

    iput-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->mDefaultRunnable:Ljava/lang/Runnable;

    .line 1100
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->TAG:Ljava/lang/String;

    const-string v1, "mDefaultRunnable, postDelayed()!!!"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1101
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->mParent:Lcom/samsung/android/visualeffect/common/GLTextureView;

    iget-object v1, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRendererForRippleType;->mDefaultRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1, p1, p2}, Lcom/samsung/android/visualeffect/common/GLTextureView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1103
    return-void
.end method
