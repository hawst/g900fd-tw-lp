.class public Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRenderer;
.super Ljava/lang/Object;
.source "GLTextureViewRenderer.java"

# interfaces
.implements Lcom/samsung/android/visualeffect/common/GLTextureView$Renderer;


# instance fields
.field protected TAG:Ljava/lang/String;

.field protected framecounter:I

.field protected isAffordanceOccur:Z

.field protected isRendered:Z

.field protected mAffordancePosX:I

.field protected mAffordancePosY:I

.field protected mBackgroundHeight:I

.field protected mBackgroundPixels:[I

.field protected mBackgroundWidth:I

.field protected mContext:Landroid/content/Context;

.field protected mGlView:Lcom/samsung/android/visualeffect/common/GLTextureView;

.field protected mHeight:I

.field protected mIsNeedToReinit:Z

.field private mLibDir:Ljava/lang/String;

.field protected mLibName:Ljava/lang/String;

.field protected final mNative:Lcom/samsung/android/visualeffect/lock/common/Native;

.field protected mWidth:I

.field protected timeStart:J


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    iput v2, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRenderer;->mWidth:I

    .line 45
    iput v2, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRenderer;->mHeight:I

    .line 46
    iput v2, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRenderer;->mAffordancePosX:I

    .line 47
    iput v2, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRenderer;->mAffordancePosY:I

    .line 48
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRenderer;->timeStart:J

    .line 49
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRenderer;->framecounter:I

    .line 51
    iput-object v3, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRenderer;->mBackgroundPixels:[I

    .line 52
    iput v2, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRenderer;->mBackgroundWidth:I

    .line 53
    iput v2, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRenderer;->mBackgroundHeight:I

    .line 56
    iput-boolean v2, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRenderer;->mIsNeedToReinit:Z

    .line 58
    iput-object v3, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRenderer;->mLibDir:Ljava/lang/String;

    .line 59
    new-instance v0, Lcom/samsung/android/visualeffect/lock/common/Native;

    invoke-direct {v0}, Lcom/samsung/android/visualeffect/lock/common/Native;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRenderer;->mNative:Lcom/samsung/android/visualeffect/lock/common/Native;

    .line 60
    iput-boolean v2, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRenderer;->isRendered:Z

    return-void
.end method


# virtual methods
.method public clearEffect()V
    .locals 2

    .prologue
    .line 215
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRenderer;->TAG:Ljava/lang/String;

    const-string v1, "clearEffect()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 217
    iget-boolean v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRenderer;->isRendered:Z

    if-eqz v0, :cond_0

    .line 219
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRenderer;->mNative:Lcom/samsung/android/visualeffect/lock/common/Native;

    invoke-virtual {v0}, Lcom/samsung/android/visualeffect/lock/common/Native;->clear()V

    .line 220
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRenderer;->mGlView:Lcom/samsung/android/visualeffect/common/GLTextureView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/android/visualeffect/common/GLTextureView;->setRenderMode(I)V

    .line 226
    :goto_0
    return-void

    .line 224
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRenderer;->TAG:Ljava/lang/String;

    const-string v1, "Ignore! because isRendered is false"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public handleTouchEvent(III)V
    .locals 5
    .param p1, "action"    # I
    .param p2, "x"    # I
    .param p3, "y"    # I

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 177
    iget-boolean v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRenderer;->isRendered:Z

    if-eqz v0, :cond_1

    .line 179
    if-eq p1, v4, :cond_0

    const/4 v0, 0x7

    if-eq p1, v0, :cond_0

    .line 180
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRenderer;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Renderer handleTouchEvent action = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 183
    :cond_0
    packed-switch p1, :pswitch_data_0

    .line 209
    :goto_0
    :pswitch_0
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRenderer;->mGlView:Lcom/samsung/android/visualeffect/common/GLTextureView;

    invoke-virtual {v0, v3}, Lcom/samsung/android/visualeffect/common/GLTextureView;->setRenderMode(I)V

    .line 211
    :cond_1
    return-void

    .line 185
    :pswitch_1
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRenderer;->mNative:Lcom/samsung/android/visualeffect/lock/common/Native;

    const/4 v1, 0x0

    invoke-virtual {v0, p2, p3, v1}, Lcom/samsung/android/visualeffect/lock/common/Native;->onTouch(III)V

    goto :goto_0

    .line 188
    :pswitch_2
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRenderer;->mNative:Lcom/samsung/android/visualeffect/lock/common/Native;

    invoke-virtual {v0, p2, p3, v3}, Lcom/samsung/android/visualeffect/lock/common/Native;->onTouch(III)V

    goto :goto_0

    .line 191
    :pswitch_3
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRenderer;->mNative:Lcom/samsung/android/visualeffect/lock/common/Native;

    invoke-virtual {v0, p2, p3, v4}, Lcom/samsung/android/visualeffect/lock/common/Native;->onTouch(III)V

    goto :goto_0

    .line 194
    :pswitch_4
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRenderer;->mNative:Lcom/samsung/android/visualeffect/lock/common/Native;

    const/4 v1, 0x3

    invoke-virtual {v0, p2, p3, v1}, Lcom/samsung/android/visualeffect/lock/common/Native;->onTouch(III)V

    goto :goto_0

    .line 197
    :pswitch_5
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRenderer;->mNative:Lcom/samsung/android/visualeffect/lock/common/Native;

    const/4 v1, 0x4

    invoke-virtual {v0, p2, p3, v1}, Lcom/samsung/android/visualeffect/lock/common/Native;->onTouch(III)V

    goto :goto_0

    .line 200
    :pswitch_6
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRenderer;->mNative:Lcom/samsung/android/visualeffect/lock/common/Native;

    const/4 v1, 0x5

    invoke-virtual {v0, p2, p3, v1}, Lcom/samsung/android/visualeffect/lock/common/Native;->onTouch(III)V

    goto :goto_0

    .line 183
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_6
        :pswitch_0
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public loadSpecialTexture([Ljava/lang/String;)V
    .locals 17
    .param p1, "aTexture"    # [Ljava/lang/String;

    .prologue
    .line 261
    if-nez p1, :cond_1

    .line 307
    :cond_0
    :goto_0
    return-void

    .line 264
    :cond_1
    new-instance v14, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v14}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 265
    .local v14, "options":Landroid/graphics/BitmapFactory$Options;
    const/4 v3, 0x0

    iput-boolean v3, v14, Landroid/graphics/BitmapFactory$Options;->inScaled:Z

    .line 269
    const-string v15, "com.samsung.android.visualeffect.res"

    .line 275
    .local v15, "packageName":Ljava/lang/String;
    const/4 v9, 0x0

    .line 277
    .local v9, "con":Landroid/content/Context;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRenderer;->mContext:Landroid/content/Context;

    const/4 v5, 0x0

    invoke-virtual {v3, v15, v5}, Landroid/content/Context;->createPackageContext(Ljava/lang/String;I)Landroid/content/Context;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v9

    .line 282
    :goto_1
    if-eqz v9, :cond_0

    .line 285
    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v16

    .line 287
    .local v16, "res":Landroid/content/res/Resources;
    const/4 v12, 0x0

    .local v12, "i":I
    :goto_2
    move-object/from16 v0, p1

    array-length v3, v0

    if-ge v12, v3, :cond_2

    .line 289
    :try_start_1
    aget-object v3, p1, v12

    const-string v5, "drawable"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3, v5, v15}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v13

    .line 291
    .local v13, "id":I
    move-object/from16 v0, v16

    invoke-static {v0, v13}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 292
    .local v1, "bitmap":Landroid/graphics/Bitmap;
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    .line 293
    .local v4, "width":I
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v8

    .line 294
    .local v8, "height":I
    mul-int v3, v4, v8

    new-array v2, v3, [I

    .line 295
    .local v2, "pixels":[I
    const/4 v3, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move v7, v4

    invoke-virtual/range {v1 .. v8}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    .line 296
    const-string v3, ""

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Adding texture Width\'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 297
    const-string v3, ""

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Adding texture Height\'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 298
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRenderer;->mNative:Lcom/samsung/android/visualeffect/lock/common/Native;

    aget-object v5, p1, v12

    invoke-virtual {v3, v5, v2, v4, v8}, Lcom/samsung/android/visualeffect/lock/common/Native;->loadTexture(Ljava/lang/String;[III)V

    .line 299
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 287
    .end local v1    # "bitmap":Landroid/graphics/Bitmap;
    .end local v2    # "pixels":[I
    .end local v4    # "width":I
    .end local v8    # "height":I
    .end local v13    # "id":I
    :goto_3
    add-int/lit8 v12, v12, 0x1

    goto :goto_2

    .line 278
    .end local v12    # "i":I
    .end local v16    # "res":Landroid/content/res/Resources;
    :catch_0
    move-exception v11

    .line 280
    .local v11, "e1":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v11}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_1

    .line 302
    .end local v11    # "e1":Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v12    # "i":I
    .restart local v16    # "res":Landroid/content/res/Resources;
    :catch_1
    move-exception v10

    .line 303
    .local v10, "e":Ljava/lang/Exception;
    const-string v3, ""

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "There is no image \'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    aget-object v6, p1, v12

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 306
    .end local v10    # "e":Ljava/lang/Exception;
    :cond_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRenderer;->mGlView:Lcom/samsung/android/visualeffect/common/GLTextureView;

    const/4 v5, 0x1

    invoke-virtual {v3, v5}, Lcom/samsung/android/visualeffect/common/GLTextureView;->setRenderMode(I)V

    goto/16 :goto_0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 229
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRenderer;->TAG:Ljava/lang/String;

    const-string v1, "onDestroy()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 231
    iget-boolean v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRenderer;->isRendered:Z

    if-eqz v0, :cond_0

    .line 233
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRenderer;->mNative:Lcom/samsung/android/visualeffect/lock/common/Native;

    invoke-virtual {v0}, Lcom/samsung/android/visualeffect/lock/common/Native;->destroy()V

    .line 239
    :goto_0
    return-void

    .line 237
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRenderer;->TAG:Ljava/lang/String;

    const-string v1, "Ignore! because isRendered is false"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onDrawFrame(Ljavax/microedition/khronos/opengles/GL10;)V
    .locals 14
    .param p1, "gl"    # Ljavax/microedition/khronos/opengles/GL10;

    .prologue
    const/4 v13, 0x1

    const/high16 v12, 0x447a0000    # 1000.0f

    const/4 v11, 0x0

    const/4 v10, -0x1

    .line 118
    iget-boolean v5, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRenderer;->mIsNeedToReinit:Z

    if-eqz v5, :cond_0

    .line 119
    iget-object v5, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRenderer;->mNative:Lcom/samsung/android/visualeffect/lock/common/Native;

    iget-object v6, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRenderer;->mLibName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Lcom/samsung/android/visualeffect/lock/common/Native;->loadEffect(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRenderer;->loadSpecialTexture([Ljava/lang/String;)V

    .line 120
    iput v10, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRenderer;->framecounter:I

    .line 123
    :cond_0
    iget v5, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRenderer;->framecounter:I

    if-ne v5, v10, :cond_1

    .line 124
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRenderer;->timeStart:J

    .line 127
    :cond_1
    iget-object v5, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRenderer;->mBackgroundPixels:[I

    if-eqz v5, :cond_2

    .line 128
    iget-object v5, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRenderer;->TAG:Ljava/lang/String;

    const-string v6, "mNative.loadTexture"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 129
    iget-object v5, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRenderer;->mNative:Lcom/samsung/android/visualeffect/lock/common/Native;

    const-string v6, "bg"

    iget-object v7, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRenderer;->mBackgroundPixels:[I

    iget v8, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRenderer;->mBackgroundWidth:I

    iget v9, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRenderer;->mBackgroundHeight:I

    invoke-virtual {v5, v6, v7, v8, v9}, Lcom/samsung/android/visualeffect/lock/common/Native;->loadTexture(Ljava/lang/String;[III)V

    .line 131
    const/4 v5, 0x0

    iput-object v5, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRenderer;->mBackgroundPixels:[I

    .line 134
    :cond_2
    iget-boolean v5, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRenderer;->mIsNeedToReinit:Z

    if-eqz v5, :cond_3

    .line 135
    iget-object v5, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRenderer;->mNative:Lcom/samsung/android/visualeffect/lock/common/Native;

    iget v6, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRenderer;->mWidth:I

    iget v7, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRenderer;->mHeight:I

    invoke-virtual {v5, v6, v7, v13}, Lcom/samsung/android/visualeffect/lock/common/Native;->init(IIZ)V

    .line 136
    iput-boolean v11, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRenderer;->mIsNeedToReinit:Z

    .line 139
    :cond_3
    iget-boolean v5, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRenderer;->isAffordanceOccur:Z

    if-eqz v5, :cond_4

    .line 140
    iget-object v5, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRenderer;->mNative:Lcom/samsung/android/visualeffect/lock/common/Native;

    iget v6, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRenderer;->mAffordancePosX:I

    iget v7, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRenderer;->mAffordancePosY:I

    invoke-virtual {v5, v6, v7}, Lcom/samsung/android/visualeffect/lock/common/Native;->showAffordance(II)V

    .line 141
    iput-boolean v11, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRenderer;->isAffordanceOccur:Z

    .line 144
    :cond_4
    const/4 v1, 0x0

    .line 146
    .local v1, "isStopDrawing":Z
    iget-object v5, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRenderer;->mNative:Lcom/samsung/android/visualeffect/lock/common/Native;

    invoke-virtual {v5}, Lcom/samsung/android/visualeffect/lock/common/Native;->draw()Z

    move-result v5

    if-nez v5, :cond_5

    .line 147
    iget-object v5, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRenderer;->TAG:Ljava/lang/String;

    const-string v6, "dirty mode"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 148
    const/4 v1, 0x1

    .line 149
    iget-object v5, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRenderer;->mGlView:Lcom/samsung/android/visualeffect/common/GLTextureView;

    invoke-virtual {v5, v11}, Lcom/samsung/android/visualeffect/common/GLTextureView;->setRenderMode(I)V

    .line 153
    :cond_5
    iget v5, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRenderer;->framecounter:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRenderer;->framecounter:I

    .line 154
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 155
    .local v2, "now":J
    iget-wide v6, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRenderer;->timeStart:J

    sub-long v6, v2, v6

    long-to-float v4, v6

    .line 156
    .local v4, "spent":F
    cmpl-float v5, v4, v12

    if-gez v5, :cond_6

    if-eqz v1, :cond_8

    .line 157
    :cond_6
    iget v5, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRenderer;->framecounter:I

    const/4 v6, 0x2

    if-le v5, v6, :cond_7

    .line 158
    iget v5, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRenderer;->framecounter:I

    int-to-float v5, v5

    mul-float/2addr v5, v12

    div-float v0, v5, v4

    .line 159
    .local v0, "fps":F
    iget-object v5, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRenderer;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "fps "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 161
    .end local v0    # "fps":F
    :cond_7
    iput v10, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRenderer;->framecounter:I

    .line 164
    :cond_8
    iget-boolean v5, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRenderer;->isRendered:Z

    if-nez v5, :cond_9

    .line 166
    iput-boolean v13, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRenderer;->isRendered:Z

    .line 167
    iget-object v5, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRenderer;->TAG:Ljava/lang/String;

    const-string v6, "onDrawFrame, First Rendering!"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 169
    :cond_9
    return-void
.end method

.method public onSurfaceChanged(Ljavax/microedition/khronos/opengles/GL10;II)V
    .locals 3
    .param p1, "gl"    # Ljavax/microedition/khronos/opengles/GL10;
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    const/4 v1, 0x1

    .line 94
    if-lez p2, :cond_0

    if-gtz p3, :cond_1

    .line 95
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRenderer;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onSurfaceChanged problem1 width"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 114
    :goto_0
    return-void

    .line 99
    :cond_1
    iget v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRenderer;->mWidth:I

    if-eq p2, v0, :cond_2

    iget v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRenderer;->mWidth:I

    if-ne p3, v0, :cond_3

    :cond_2
    iget v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRenderer;->mHeight:I

    if-eq p2, v0, :cond_4

    iget v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRenderer;->mHeight:I

    if-eq p3, v0, :cond_4

    .line 100
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRenderer;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onSurfaceChanged problem2 width"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "disp "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRenderer;->mWidth:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRenderer;->mWidth:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 104
    :cond_4
    iget v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRenderer;->mWidth:I

    if-ne v0, p2, :cond_5

    iget v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRenderer;->mHeight:I

    if-eq v0, p3, :cond_6

    .line 105
    :cond_5
    iput-boolean v1, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRenderer;->mIsNeedToReinit:Z

    .line 108
    :cond_6
    iput p2, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRenderer;->mWidth:I

    .line 109
    iput p3, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRenderer;->mHeight:I

    .line 111
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRenderer;->mGlView:Lcom/samsung/android/visualeffect/common/GLTextureView;

    invoke-virtual {v0, v1}, Lcom/samsung/android/visualeffect/common/GLTextureView;->setRenderMode(I)V

    .line 113
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRenderer;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onSurfaceChanged "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method public onSurfaceCreated(Ljavax/microedition/khronos/opengles/GL10;Ljavax/microedition/khronos/egl/EGLConfig;)V
    .locals 6
    .param p1, "gl"    # Ljavax/microedition/khronos/opengles/GL10;
    .param p2, "config"    # Ljavax/microedition/khronos/egl/EGLConfig;

    .prologue
    .line 65
    iget-object v3, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRenderer;->TAG:Ljava/lang/String;

    const-string v4, "onSurfaceCreated"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 67
    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    .line 68
    .local v0, "displayMetrics":Landroid/util/DisplayMetrics;
    iget-object v3, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRenderer;->mContext:Landroid/content/Context;

    const-string v4, "window"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/WindowManager;

    .line 69
    .local v2, "mWindowManager":Landroid/view/WindowManager;
    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/view/Display;->getRealMetrics(Landroid/util/DisplayMetrics;)V

    .line 71
    iget v3, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iput v3, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRenderer;->mWidth:I

    .line 72
    iget v3, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    iput v3, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRenderer;->mHeight:I

    .line 74
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRenderer;->mIsNeedToReinit:Z

    .line 76
    iget-object v3, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRenderer;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    .line 77
    .local v1, "m1":Landroid/content/pm/ApplicationInfo;
    iget-object v3, v1, Landroid/content/pm/ApplicationInfo;->nativeLibraryDir:Ljava/lang/String;

    iput-object v3, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRenderer;->mLibDir:Ljava/lang/String;

    .line 80
    iget-object v3, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRenderer;->mLibDir:Ljava/lang/String;

    const-string v4, "64"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 81
    const-string v3, "/system/lib64"

    iput-object v3, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRenderer;->mLibDir:Ljava/lang/String;

    .line 87
    :goto_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRenderer;->mLibDir:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRenderer;->mLibName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRenderer;->mLibName:Ljava/lang/String;

    .line 88
    iget-object v3, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRenderer;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "mLibName = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRenderer;->mLibName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 89
    return-void

    .line 84
    :cond_0
    const-string v3, "/system/lib"

    iput-object v3, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRenderer;->mLibDir:Ljava/lang/String;

    goto :goto_0
.end method

.method public setBackgroundBitmap([III)V
    .locals 2
    .param p1, "pixels"    # [I
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    .line 252
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRenderer;->TAG:Ljava/lang/String;

    const-string v1, "setBackgroundBitmap"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 253
    iput-object p1, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRenderer;->mBackgroundPixels:[I

    .line 254
    iput p2, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRenderer;->mBackgroundWidth:I

    .line 255
    iput p3, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRenderer;->mBackgroundHeight:I

    .line 257
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRenderer;->mGlView:Lcom/samsung/android/visualeffect/common/GLTextureView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/android/visualeffect/common/GLTextureView;->setRenderMode(I)V

    .line 258
    return-void
.end method

.method public setParameters([I[F)V
    .locals 1
    .param p1, "aNums"    # [I
    .param p2, "aValues"    # [F

    .prologue
    .line 173
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRenderer;->mNative:Lcom/samsung/android/visualeffect/lock/common/Native;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/android/visualeffect/lock/common/Native;->setParameters([I[F)V

    .line 174
    return-void
.end method

.method public showUnlock()V
    .locals 2

    .prologue
    .line 310
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRenderer;->TAG:Ljava/lang/String;

    const-string v1, "showUnlock()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 312
    iget-boolean v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRenderer;->isRendered:Z

    if-eqz v0, :cond_0

    .line 314
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRenderer;->mNative:Lcom/samsung/android/visualeffect/lock/common/Native;

    invoke-virtual {v0}, Lcom/samsung/android/visualeffect/lock/common/Native;->showUnlock()V

    .line 315
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRenderer;->mGlView:Lcom/samsung/android/visualeffect/common/GLTextureView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/android/visualeffect/common/GLTextureView;->setRenderMode(I)V

    .line 321
    :goto_0
    return-void

    .line 319
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRenderer;->TAG:Ljava/lang/String;

    const-string v1, "Ignore! because isRendered is false"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public showUnlockAffordance(II)V
    .locals 3
    .param p1, "posX"    # I
    .param p2, "posY"    # I

    .prologue
    const/4 v2, 0x1

    .line 242
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRenderer;->TAG:Ljava/lang/String;

    const-string v1, "showUnlockAffordance"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 244
    iput p1, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRenderer;->mAffordancePosX:I

    .line 245
    iput p2, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRenderer;->mAffordancePosY:I

    .line 247
    iput-boolean v2, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRenderer;->isAffordanceOccur:Z

    .line 248
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLTextureViewRenderer;->mGlView:Lcom/samsung/android/visualeffect/common/GLTextureView;

    invoke-virtual {v0, v2}, Lcom/samsung/android/visualeffect/common/GLTextureView;->setRenderMode(I)V

    .line 249
    return-void
.end method
