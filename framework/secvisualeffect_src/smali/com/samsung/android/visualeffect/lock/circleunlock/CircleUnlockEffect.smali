.class public Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;
.super Landroid/widget/FrameLayout;
.source "CircleUnlockEffect.java"

# interfaces
.implements Lcom/samsung/android/visualeffect/IEffectView;


# instance fields
.field private final ARROW_ANIMATION_DURATION:I

.field private final DBG:Z

.field private final IN_ANIMATION_DURATION:I

.field private final IN_ANIMATION_DURATION_FOR_AFFORDANCE:I

.field private final OUT_ANIMATION_DURATION:I

.field private final OUT_ANIMATION_DURATION_FOR_AFFORDANCE:I

.field private final SHOWING_DURATION_FOR_AFFORDANCE:I

.field private final TAG:Ljava/lang/String;

.field private affordanceX:F

.field private affordanceY:F

.field private arrow:Landroid/widget/ImageView;

.field private arrowAlphaMax:F

.field private arrowAnimationToggle:Z

.field private arrowAnimator:Landroid/animation/ValueAnimator;

.field private arrowImageId:I

.field private circle:Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockCircle;

.field private circleAnimationMax:F

.field private circleAnimationMin:F

.field private circleGroup:Landroid/widget/FrameLayout;

.field private circleInAnimator:Landroid/animation/ValueAnimator;

.field private circleOutAnimator:Landroid/animation/ValueAnimator;

.field private circleUnlockMaxHeight:I

.field private circleUnlockMaxRadius:I

.field private circleUnlockMaxWidth:I

.field private circleUnlockMinRadius:I

.field private circleUnlockMinWidth:I

.field private currentLockSequenceNumber:I

.field private dragAnimationValue:F

.field private fillAnimationValueMax:F

.field private innerStrokeWidth:I

.field private isForAffordance:Z

.field private isForShortcut:Z

.field private isUnlocked:Z

.field private lockImageView:Landroid/widget/ImageView;

.field private lockSequenceImageId:[I

.field private lockSequenceTotal:I

.field private mContext:Landroid/content/Context;

.field private outerStrokeWidth:I

.field private startX:F

.field private startY:F

.field private strokeAnimationValue:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/16 v3, 0x29a

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 72
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 28
    const-string v0, "VisualEffectCircleUnlockEffect"

    iput-object v0, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->TAG:Ljava/lang/String;

    .line 29
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->DBG:Z

    .line 30
    iput v3, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->IN_ANIMATION_DURATION:I

    .line 31
    const/16 v0, 0x14d

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->OUT_ANIMATION_DURATION:I

    .line 32
    const/16 v0, 0x1f4

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->ARROW_ANIMATION_DURATION:I

    .line 33
    const/16 v0, -0xc8

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->SHOWING_DURATION_FOR_AFFORDANCE:I

    .line 34
    iput v3, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->IN_ANIMATION_DURATION_FOR_AFFORDANCE:I

    .line 35
    const/16 v0, 0x2bc

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->OUT_ANIMATION_DURATION_FOR_AFFORDANCE:I

    .line 46
    iput v1, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->currentLockSequenceNumber:I

    .line 50
    iput v2, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->strokeAnimationValue:F

    .line 51
    iput v2, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->dragAnimationValue:F

    .line 53
    iput v2, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->circleAnimationMin:F

    .line 57
    iput-boolean v1, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->isForShortcut:Z

    .line 58
    iput-boolean v1, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->arrowAnimationToggle:Z

    .line 59
    iput-boolean v1, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->isUnlocked:Z

    .line 60
    iput-boolean v1, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->isForAffordance:Z

    .line 73
    const-string v0, "VisualEffectCircleUnlockEffect"

    const-string v1, "Constructor"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 74
    iput-object p1, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->mContext:Landroid/content/Context;

    .line 75
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;)F
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;

    .prologue
    .line 26
    iget v0, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->strokeAnimationValue:F

    return v0
.end method

.method static synthetic access$002(Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;F)F
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;
    .param p1, "x1"    # F

    .prologue
    .line 26
    iput p1, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->strokeAnimationValue:F

    return p1
.end method

.method static synthetic access$100(Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;)F
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;

    .prologue
    .line 26
    iget v0, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->circleAnimationMin:F

    return v0
.end method

.method static synthetic access$1000(Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->arrow:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;

    .prologue
    .line 26
    iget-boolean v0, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->arrowAnimationToggle:Z

    return v0
.end method

.method static synthetic access$1102(Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;
    .param p1, "x1"    # Z

    .prologue
    .line 26
    iput-boolean p1, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->arrowAnimationToggle:Z

    return p1
.end method

.method static synthetic access$200(Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;)Landroid/widget/FrameLayout;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->circleGroup:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;Landroid/view/View;F)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;
    .param p1, "x1"    # Landroid/view/View;
    .param p2, "x2"    # F

    .prologue
    .line 26
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->setAlphaAndVisibility(Landroid/view/View;F)V

    return-void
.end method

.method static synthetic access$400(Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;)Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockCircle;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->circle:Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockCircle;

    return-object v0
.end method

.method static synthetic access$500(Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;)F
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;

    .prologue
    .line 26
    iget v0, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->circleAnimationMax:F

    return v0
.end method

.method static synthetic access$600(Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;)F
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;

    .prologue
    .line 26
    iget v0, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->dragAnimationValue:F

    return v0
.end method

.method static synthetic access$602(Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;F)F
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;
    .param p1, "x1"    # F

    .prologue
    .line 26
    iput p1, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->dragAnimationValue:F

    return p1
.end method

.method static synthetic access$700(Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;)F
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;

    .prologue
    .line 26
    iget v0, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->fillAnimationValueMax:F

    return v0
.end method

.method static synthetic access$800(Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;F)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;
    .param p1, "x1"    # F

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->setImageInLockImageView(F)V

    return-void
.end method

.method static synthetic access$900(Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;)F
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;

    .prologue
    .line 26
    iget v0, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->arrowAlphaMax:F

    return v0
.end method

.method private cancelAllAnimator()V
    .locals 1

    .prologue
    .line 355
    invoke-direct {p0}, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->resetAnimatorAfterAffordance()V

    .line 356
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->circleInAnimator:Landroid/animation/ValueAnimator;

    invoke-direct {p0, v0}, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->cancelAnimator(Landroid/animation/Animator;)V

    .line 357
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->circleOutAnimator:Landroid/animation/ValueAnimator;

    invoke-direct {p0, v0}, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->cancelAnimator(Landroid/animation/Animator;)V

    .line 358
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->arrowAnimator:Landroid/animation/ValueAnimator;

    invoke-direct {p0, v0}, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->cancelAnimator(Landroid/animation/Animator;)V

    .line 359
    return-void
.end method

.method private cancelAnimator(Landroid/animation/Animator;)V
    .locals 1
    .param p1, "animator"    # Landroid/animation/Animator;

    .prologue
    .line 362
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/animation/Animator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 363
    invoke-virtual {p1}, Landroid/animation/Animator;->cancel()V

    .line 364
    :cond_0
    return-void
.end method

.method private changeModeForCircleUnlock()V
    .locals 2

    .prologue
    .line 221
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->lockImageView:Landroid/widget/ImageView;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->setAlphaAndVisibility(Landroid/view/View;F)V

    .line 222
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->isForShortcut:Z

    .line 223
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->circle:Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockCircle;

    if-eqz v0, :cond_0

    .line 224
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->circle:Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockCircle;

    iget-boolean v1, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->isForShortcut:Z

    invoke-virtual {v0, v1}, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockCircle;->setIsForShortcut(Z)V

    .line 225
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->circle:Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockCircle;

    iget v1, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->circleUnlockMinWidth:I

    invoke-virtual {v0, v1}, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockCircle;->setCircleMinWidth(I)V

    .line 227
    :cond_0
    return-void
.end method

.method private changeModeForShorcutButton(I)V
    .locals 2
    .param p1, "viewWidth"    # I

    .prologue
    .line 212
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->lockImageView:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->setAlphaAndVisibility(Landroid/view/View;F)V

    .line 213
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->isForShortcut:Z

    .line 214
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->circle:Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockCircle;

    if-eqz v0, :cond_0

    .line 215
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->circle:Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockCircle;

    iget-boolean v1, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->isForShortcut:Z

    invoke-virtual {v0, v1}, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockCircle;->setIsForShortcut(Z)V

    .line 216
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->circle:Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockCircle;

    add-int/lit8 v1, p1, -0x4

    invoke-virtual {v0, v1}, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockCircle;->setCircleMinWidth(I)V

    .line 218
    :cond_0
    return-void
.end method

.method private clearEffect()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 348
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->arrowAnimationToggle:Z

    .line 349
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->circleGroup:Landroid/widget/FrameLayout;

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->setAlphaAndVisibility(Landroid/view/View;F)V

    .line 350
    invoke-direct {p0}, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->cancelAllAnimator()V

    .line 351
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->circle:Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockCircle;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->circle:Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockCircle;

    invoke-virtual {v0, v1}, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockCircle;->dragAnimationUpdate(F)V

    .line 352
    :cond_0
    return-void
.end method

.method private getResourceImageSize(IZ)I
    .locals 2
    .param p1, "resource"    # I
    .param p2, "isWidth"    # Z

    .prologue
    .line 367
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 368
    .local v0, "opts":Landroid/graphics/BitmapFactory$Options;
    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 369
    invoke-virtual {p0}, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v1, p1, v0}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 370
    if-eqz p2, :cond_0

    iget v1, v0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    :goto_0
    return v1

    :cond_0
    iget v1, v0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    goto :goto_0
.end method

.method private resetAnimatorAfterAffordance()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 262
    iget-boolean v0, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->isForAffordance:Z

    if-nez v0, :cond_0

    .line 274
    :goto_0
    return-void

    .line 263
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->isForAffordance:Z

    .line 265
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->circleInAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->end()V

    .line 266
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->circleInAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0, v4, v5}, Landroid/animation/ValueAnimator;->setStartDelay(J)V

    .line 267
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->circleInAnimator:Landroid/animation/ValueAnimator;

    const-wide/16 v2, 0x29a

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 268
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->circleInAnimator:Landroid/animation/ValueAnimator;

    new-instance v1, Landroid/view/animation/interpolator/QuintEaseOut;

    invoke-direct {v1}, Landroid/view/animation/interpolator/QuintEaseOut;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 270
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->circleOutAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->end()V

    .line 271
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->circleOutAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0, v4, v5}, Landroid/animation/ValueAnimator;->setStartDelay(J)V

    .line 272
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->circleOutAnimator:Landroid/animation/ValueAnimator;

    const-wide/16 v2, 0x14d

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 273
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->circleOutAnimator:Landroid/animation/ValueAnimator;

    new-instance v1, Landroid/view/animation/interpolator/QuintEaseOut;

    invoke-direct {v1}, Landroid/view/animation/interpolator/QuintEaseOut;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    goto :goto_0
.end method

.method private setAlphaAndVisibility(Landroid/view/View;F)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .param p2, "alpha"    # F

    .prologue
    const/16 v0, 0x8

    .line 374
    const/4 v1, 0x0

    cmpl-float v1, p2, v1

    if-nez v1, :cond_2

    .line 375
    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-eq v1, v0, :cond_1

    .line 376
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x4

    :cond_0
    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 383
    :cond_1
    :goto_0
    return-void

    .line 379
    :cond_2
    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_3

    .line 380
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 381
    :cond_3
    invoke-virtual {p1, p2}, Landroid/view/View;->setAlpha(F)V

    goto :goto_0
.end method

.method private setAnimator()V
    .locals 4

    .prologue
    const/4 v2, 0x2

    .line 142
    new-array v0, v2, [F

    fill-array-data v0, :array_0

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->circleInAnimator:Landroid/animation/ValueAnimator;

    .line 143
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->circleInAnimator:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect$1;

    invoke-direct {v1, p0}, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect$1;-><init>(Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 153
    new-array v0, v2, [F

    fill-array-data v0, :array_1

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->circleOutAnimator:Landroid/animation/ValueAnimator;

    .line 154
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->circleOutAnimator:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect$2;

    invoke-direct {v1, p0}, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect$2;-><init>(Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 167
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->circleOutAnimator:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect$3;

    invoke-direct {v1, p0}, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect$3;-><init>(Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 181
    invoke-direct {p0}, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->resetAnimatorAfterAffordance()V

    .line 184
    new-array v0, v2, [F

    fill-array-data v0, :array_2

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->arrowAnimator:Landroid/animation/ValueAnimator;

    .line 185
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->arrowAnimator:Landroid/animation/ValueAnimator;

    new-instance v1, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v1}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 186
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->arrowAnimator:Landroid/animation/ValueAnimator;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 187
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->arrowAnimator:Landroid/animation/ValueAnimator;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 188
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->arrowAnimator:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect$4;

    invoke-direct {v1, p0}, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect$4;-><init>(Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 197
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->arrowAnimator:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect$5;

    invoke-direct {v1, p0}, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect$5;-><init>(Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 209
    return-void

    .line 142
    nop

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data

    .line 153
    :array_1
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data

    .line 184
    :array_2
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method private setCircleGroupXY(FF)V
    .locals 4
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    const/high16 v3, 0x40000000    # 2.0f

    .line 333
    iget v2, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->circleUnlockMaxWidth:I

    int-to-float v2, v2

    div-float/2addr v2, v3

    sub-float v0, p1, v2

    .line 334
    .local v0, "tx":F
    iget v2, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->circleUnlockMaxHeight:I

    int-to-float v2, v2

    div-float/2addr v2, v3

    sub-float v1, p2, v2

    .line 335
    .local v1, "ty":F
    iget-object v2, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->circleGroup:Landroid/widget/FrameLayout;

    invoke-virtual {v2, v0}, Landroid/widget/FrameLayout;->setX(F)V

    .line 336
    iget-object v2, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->circleGroup:Landroid/widget/FrameLayout;

    invoke-virtual {v2, v1}, Landroid/widget/FrameLayout;->setY(F)V

    .line 337
    return-void
.end method

.method private setImageInLockImageView(F)V
    .locals 3
    .param p1, "value"    # F

    .prologue
    .line 340
    iget v1, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->lockSequenceTotal:I

    add-int/lit8 v1, v1, -0x1

    int-to-float v1, v1

    mul-float/2addr v1, p1

    float-to-int v0, v1

    .line 341
    .local v0, "sequenceNumber":I
    iget v1, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->currentLockSequenceNumber:I

    if-eq v1, v0, :cond_0

    .line 342
    iget-object v1, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->lockImageView:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->lockSequenceImageId:[I

    aget v2, v2, v0

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 343
    iput v0, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->currentLockSequenceNumber:I

    .line 345
    :cond_0
    return-void
.end method

.method private setLayout()V
    .locals 11

    .prologue
    const/4 v10, 0x1

    const/4 v9, -0x2

    const/4 v8, 0x0

    .line 110
    new-instance v0, Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->circleGroup:Landroid/widget/FrameLayout;

    .line 111
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->circleGroup:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v8}, Landroid/widget/FrameLayout;->setLayoutDirection(I)V

    .line 112
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->circleGroup:Landroid/widget/FrameLayout;

    iget v1, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->circleUnlockMaxWidth:I

    iget v2, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->circleUnlockMaxHeight:I

    invoke-virtual {p0, v0, v1, v2}, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->addView(Landroid/view/View;II)V

    .line 113
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->circleGroup:Landroid/widget/FrameLayout;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->setAlphaAndVisibility(Landroid/view/View;F)V

    .line 117
    new-instance v0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockCircle;

    iget-object v1, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->mContext:Landroid/content/Context;

    iget v2, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->circleUnlockMaxWidth:I

    iget v3, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->circleUnlockMinWidth:I

    iget v4, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->outerStrokeWidth:I

    iget v5, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->innerStrokeWidth:I

    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockCircle;-><init>(Landroid/content/Context;IIII)V

    iput-object v0, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->circle:Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockCircle;

    .line 118
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->circleGroup:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->circle:Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockCircle;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 121
    new-instance v0, Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->arrow:Landroid/widget/ImageView;

    .line 122
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->arrow:Landroid/widget/ImageView;

    iget v1, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->arrowImageId:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 123
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->circleGroup:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->arrow:Landroid/widget/ImageView;

    invoke-virtual {v0, v1, v9, v9}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;II)V

    .line 124
    iget v0, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->circleUnlockMaxWidth:I

    iget v1, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->arrowImageId:I

    invoke-direct {p0, v1, v10}, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->getResourceImageSize(IZ)I

    move-result v1

    sub-int/2addr v0, v1

    div-int/lit8 v6, v0, 0x2

    .line 125
    .local v6, "tx":I
    iget v0, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->circleUnlockMaxHeight:I

    iget v1, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->arrowImageId:I

    invoke-direct {p0, v1, v8}, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->getResourceImageSize(IZ)I

    move-result v1

    sub-int/2addr v0, v1

    div-int/lit8 v7, v0, 0x2

    .line 126
    .local v7, "ty":I
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->arrow:Landroid/widget/ImageView;

    int-to-float v1, v6

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setX(F)V

    .line 127
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->arrow:Landroid/widget/ImageView;

    int-to-float v1, v7

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setY(F)V

    .line 130
    new-instance v0, Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->lockImageView:Landroid/widget/ImageView;

    .line 131
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->lockImageView:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->lockSequenceImageId:[I

    aget v1, v1, v8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 132
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->circleGroup:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->lockImageView:Landroid/widget/ImageView;

    invoke-virtual {v0, v1, v9, v9}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;II)V

    .line 133
    iget v0, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->circleUnlockMaxWidth:I

    iget-object v1, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->lockSequenceImageId:[I

    aget v1, v1, v8

    invoke-direct {p0, v1, v10}, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->getResourceImageSize(IZ)I

    move-result v1

    sub-int/2addr v0, v1

    div-int/lit8 v6, v0, 0x2

    .line 134
    iget v0, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->circleUnlockMaxHeight:I

    iget-object v1, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->lockSequenceImageId:[I

    aget v1, v1, v8

    invoke-direct {p0, v1, v8}, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->getResourceImageSize(IZ)I

    move-result v1

    sub-int/2addr v0, v1

    div-int/lit8 v7, v0, 0x2

    .line 135
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->lockImageView:Landroid/widget/ImageView;

    int-to-float v1, v6

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setX(F)V

    .line 136
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->lockImageView:Landroid/widget/ImageView;

    int-to-float v1, v7

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setY(F)V

    .line 137
    return-void
.end method

.method private showAffordanceEffect(JLandroid/graphics/Rect;)V
    .locals 5
    .param p1, "startDelay"    # J
    .param p3, "rect"    # Landroid/graphics/Rect;

    .prologue
    const/high16 v3, 0x40000000    # 2.0f

    .line 386
    const-string v0, "VisualEffectCircleUnlockEffect"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "showUnlockAffordance : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p3, Landroid/graphics/Rect;->left:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p3, Landroid/graphics/Rect;->right:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p3, Landroid/graphics/Rect;->top:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p3, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", startDelay : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 387
    iget v0, p3, Landroid/graphics/Rect;->left:I

    int-to-float v0, v0

    iget v1, p3, Landroid/graphics/Rect;->right:I

    iget v2, p3, Landroid/graphics/Rect;->left:I

    sub-int/2addr v1, v2

    int-to-float v1, v1

    div-float/2addr v1, v3

    add-float/2addr v0, v1

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->affordanceX:F

    .line 388
    iget v0, p3, Landroid/graphics/Rect;->top:I

    int-to-float v0, v0

    iget v1, p3, Landroid/graphics/Rect;->bottom:I

    iget v2, p3, Landroid/graphics/Rect;->top:I

    sub-int/2addr v1, v2

    int-to-float v1, v1

    div-float/2addr v1, v3

    add-float/2addr v0, v1

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->affordanceY:F

    .line 389
    iget v0, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->affordanceX:F

    iget v1, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->affordanceY:F

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->startAnimatorForAffordance(JFF)V

    .line 390
    return-void
.end method

.method private startAnimatorForAffordance(JFF)V
    .locals 9
    .param p1, "startDelay"    # J
    .param p3, "x"    # F
    .param p4, "y"    # F

    .prologue
    const-wide/16 v6, 0x29a

    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v3, 0x0

    .line 234
    iget-boolean v2, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->isForAffordance:Z

    if-eqz v2, :cond_0

    .line 259
    :goto_0
    return-void

    .line 235
    :cond_0
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->isForAffordance:Z

    .line 236
    invoke-direct {p0}, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->changeModeForCircleUnlock()V

    .line 237
    iput v3, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->dragAnimationValue:F

    .line 238
    iput v3, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->strokeAnimationValue:F

    .line 239
    iput v3, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->fillAnimationValueMax:F

    .line 240
    iput v3, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->circleAnimationMin:F

    .line 241
    iput v4, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->circleAnimationMax:F

    .line 242
    iput v4, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->arrowAlphaMax:F

    .line 243
    iget-object v2, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->circle:Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockCircle;

    iget v3, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->dragAnimationValue:F

    invoke-virtual {v2, v3}, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockCircle;->dragAnimationUpdate(F)V

    .line 244
    iget v2, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->dragAnimationValue:F

    invoke-direct {p0, v2}, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->setImageInLockImageView(F)V

    .line 245
    iget-object v2, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->arrow:Landroid/widget/ImageView;

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 247
    invoke-direct {p0, p3, p4}, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->setCircleGroupXY(FF)V

    .line 249
    iget-object v2, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->circleInAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v2, p1, p2}, Landroid/animation/ValueAnimator;->setStartDelay(J)V

    .line 250
    iget-object v2, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->circleInAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v2, v6, v7}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 251
    iget-object v2, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->circleInAnimator:Landroid/animation/ValueAnimator;

    new-instance v3, Landroid/view/animation/interpolator/QuintEaseOut;

    invoke-direct {v3}, Landroid/view/animation/interpolator/QuintEaseOut;-><init>()V

    invoke-virtual {v2, v3}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 252
    iget-object v2, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->circleInAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v2}, Landroid/animation/ValueAnimator;->start()V

    .line 254
    add-long v2, p1, v6

    const-wide/16 v4, -0xc8

    add-long v0, v2, v4

    .line 255
    .local v0, "delay":J
    iget-object v2, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->circleOutAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v2, v0, v1}, Landroid/animation/ValueAnimator;->setStartDelay(J)V

    .line 256
    iget-object v2, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->circleOutAnimator:Landroid/animation/ValueAnimator;

    const-wide/16 v4, 0x2bc

    invoke-virtual {v2, v4, v5}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 257
    iget-object v2, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->circleOutAnimator:Landroid/animation/ValueAnimator;

    new-instance v3, Landroid/view/animation/interpolator/QuintEaseIn;

    invoke-direct {v3}, Landroid/view/animation/interpolator/QuintEaseIn;-><init>()V

    invoke-virtual {v2, v3}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 258
    iget-object v2, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->circleOutAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v2}, Landroid/animation/ValueAnimator;->start()V

    goto :goto_0
.end method

.method private unlock()V
    .locals 1

    .prologue
    .line 230
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->isUnlocked:Z

    .line 231
    return-void
.end method


# virtual methods
.method public clearScreen()V
    .locals 0

    .prologue
    .line 424
    invoke-direct {p0}, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->clearEffect()V

    .line 425
    return-void
.end method

.method public handleCustomEvent(ILjava/util/HashMap;)V
    .locals 4
    .param p1, "cmd"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/HashMap",
            "<**>;)V"
        }
    .end annotation

    .prologue
    .line 398
    .local p2, "params":Ljava/util/HashMap;, "Ljava/util/HashMap<**>;"
    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    .line 399
    const-string v0, "StartDelay"

    invoke-virtual {p2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-string v0, "Rect"

    invoke-virtual {p2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Rect;

    invoke-direct {p0, v2, v3, v0}, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->showAffordanceEffect(JLandroid/graphics/Rect;)V

    .line 411
    :cond_0
    :goto_0
    return-void

    .line 401
    :cond_1
    const/4 v0, 0x2

    if-ne p1, v0, :cond_2

    .line 402
    invoke-direct {p0}, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->unlock()V

    goto :goto_0

    .line 404
    :cond_2
    const/4 v0, 0x3

    if-ne p1, v0, :cond_0

    .line 405
    const-string v0, "changeModeForShorcutButton"

    invoke-virtual {p2, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 406
    const-string v0, "changeModeForShorcutButton"

    invoke-virtual {p2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->changeModeForShorcutButton(I)V

    goto :goto_0

    .line 407
    :cond_3
    const-string v0, "changeModeForCircleUnlock"

    invoke-virtual {p2, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 408
    invoke-direct {p0}, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->changeModeForCircleUnlock()V

    goto :goto_0
.end method

.method public handleTouchEvent(Landroid/view/MotionEvent;Landroid/view/View;)V
    .locals 16
    .param p1, "event"    # Landroid/view/MotionEvent;
    .param p2, "view"    # Landroid/view/View;

    .prologue
    .line 281
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v9

    if-nez v9, :cond_0

    .line 282
    if-eqz p2, :cond_2

    .line 283
    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getWidth()I

    move-result v9

    move-object/from16 v0, p0

    invoke-direct {v0, v9}, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->changeModeForShorcutButton(I)V

    .line 289
    :cond_0
    :goto_0
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v7

    .line 290
    .local v7, "touchX":F
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v8

    .line 292
    .local v8, "touchY":F
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v9

    if-nez v9, :cond_4

    .line 293
    const-string v9, "VisualEffectCircleUnlockEffect"

    const-string v10, "handleTouchEvent : ACTION_DOWN"

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 294
    const/4 v9, 0x0

    move-object/from16 v0, p0

    iput-boolean v9, v0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->isUnlocked:Z

    .line 295
    move-object/from16 v0, p0

    iget-boolean v9, v0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->isForShortcut:Z

    if-eqz v9, :cond_3

    .line 296
    new-instance v5, Landroid/graphics/Rect;

    invoke-direct {v5}, Landroid/graphics/Rect;-><init>()V

    .line 297
    .local v5, "r":Landroid/graphics/Rect;
    move-object/from16 v0, p2

    invoke-virtual {v0, v5}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 298
    iget v9, v5, Landroid/graphics/Rect;->left:I

    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getWidth()I

    move-result v10

    div-int/lit8 v10, v10, 0x2

    add-int/2addr v9, v10

    int-to-float v9, v9

    move-object/from16 v0, p0

    iput v9, v0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->startX:F

    .line 299
    iget v9, v5, Landroid/graphics/Rect;->top:I

    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getHeight()I

    move-result v10

    div-int/lit8 v10, v10, 0x2

    add-int/2addr v9, v10

    int-to-float v9, v9

    move-object/from16 v0, p0

    iput v9, v0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->startY:F

    .line 304
    .end local v5    # "r":Landroid/graphics/Rect;
    :goto_1
    const/4 v9, 0x0

    move-object/from16 v0, p0

    iput-boolean v9, v0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->arrowAnimationToggle:Z

    .line 305
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->cancelAllAnimator()V

    .line 306
    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->startX:F

    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->startY:F

    move-object/from16 v0, p0

    invoke-direct {v0, v9, v10}, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->setCircleGroupXY(FF)V

    .line 307
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->circleInAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v9}, Landroid/animation/ValueAnimator;->start()V

    .line 308
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->arrowAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v9}, Landroid/animation/ValueAnimator;->start()V

    .line 330
    :cond_1
    :goto_2
    return-void

    .line 285
    .end local v7    # "touchX":F
    .end local v8    # "touchY":F
    :cond_2
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->changeModeForCircleUnlock()V

    goto :goto_0

    .line 301
    .restart local v7    # "touchX":F
    .restart local v8    # "touchY":F
    :cond_3
    move-object/from16 v0, p0

    iput v7, v0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->startX:F

    .line 302
    move-object/from16 v0, p0

    iput v8, v0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->startY:F

    goto :goto_1

    .line 310
    :cond_4
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v9

    const/4 v10, 0x2

    if-ne v9, v10, :cond_7

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v9

    if-nez v9, :cond_7

    .line 312
    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->startX:F

    sub-float v2, v7, v9

    .line 313
    .local v2, "diffX":F
    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->startY:F

    sub-float v3, v8, v9

    .line 314
    .local v3, "diffY":F
    float-to-double v10, v2

    const-wide/high16 v12, 0x4000000000000000L    # 2.0

    invoke-static {v10, v11, v12, v13}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v10

    float-to-double v12, v3

    const-wide/high16 v14, 0x4000000000000000L    # 2.0

    invoke-static {v12, v13, v14, v15}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v12

    add-double/2addr v10, v12

    invoke-static {v10, v11}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v10

    double-to-float v4, v10

    .line 315
    .local v4, "distance":F
    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->circleUnlockMinRadius:I

    int-to-float v9, v9

    sub-float v9, v4, v9

    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->circleUnlockMaxRadius:I

    move-object/from16 v0, p0

    iget v11, v0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->circleUnlockMinRadius:I

    sub-int/2addr v10, v11

    int-to-float v10, v10

    div-float v6, v9, v10

    .line 316
    .local v6, "ratio":F
    const/4 v9, 0x0

    cmpg-float v9, v6, v9

    if-gez v9, :cond_5

    const/4 v6, 0x0

    .line 317
    :cond_5
    const/high16 v9, 0x3f800000    # 1.0f

    cmpl-float v9, v6, v9

    if-lez v9, :cond_6

    const/high16 v6, 0x3f800000    # 1.0f

    .line 318
    :cond_6
    move-object/from16 v0, p0

    iput v6, v0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->dragAnimationValue:F

    .line 319
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->circle:Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockCircle;

    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->dragAnimationValue:F

    invoke-virtual {v9, v10}, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockCircle;->dragAnimationUpdate(F)V

    .line 320
    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->dragAnimationValue:F

    move-object/from16 v0, p0

    invoke-direct {v0, v9}, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->setImageInLockImageView(F)V

    goto :goto_2

    .line 322
    .end local v2    # "diffX":F
    .end local v3    # "diffY":F
    .end local v4    # "distance":F
    .end local v6    # "ratio":F
    :cond_7
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v9

    const/4 v10, 0x1

    if-eq v9, v10, :cond_8

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v9

    const/4 v10, 0x3

    if-ne v9, v10, :cond_1

    .line 324
    :cond_8
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->cancelAllAnimator()V

    .line 325
    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->strokeAnimationValue:F

    move-object/from16 v0, p0

    iput v9, v0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->circleAnimationMax:F

    .line 326
    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->dragAnimationValue:F

    move-object/from16 v0, p0

    iput v9, v0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->fillAnimationValueMax:F

    .line 327
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->arrow:Landroid/widget/ImageView;

    invoke-virtual {v9}, Landroid/widget/ImageView;->getAlpha()F

    move-result v9

    move-object/from16 v0, p0

    iput v9, v0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->arrowAlphaMax:F

    .line 328
    move-object/from16 v0, p0

    iget-boolean v9, v0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->isUnlocked:Z

    if-nez v9, :cond_1

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->circleOutAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v9}, Landroid/animation/ValueAnimator;->start()V

    goto/16 :goto_2
.end method

.method public init(Lcom/samsung/android/visualeffect/EffectDataObj;)V
    .locals 3
    .param p1, "data"    # Lcom/samsung/android/visualeffect/EffectDataObj;

    .prologue
    .line 79
    iget-object v0, p1, Lcom/samsung/android/visualeffect/EffectDataObj;->circleData:Lcom/samsung/android/visualeffect/lock/data/CircleData;

    if-nez v0, :cond_0

    .line 81
    const-string v0, "VisualEffectCircleUnlockEffect"

    const-string v1, "circleData is null"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 105
    :goto_0
    return-void

    .line 85
    :cond_0
    iget-object v0, p1, Lcom/samsung/android/visualeffect/EffectDataObj;->circleData:Lcom/samsung/android/visualeffect/lock/data/CircleData;

    iget v0, v0, Lcom/samsung/android/visualeffect/lock/data/CircleData;->arrowId:I

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->arrowImageId:I

    .line 86
    iget-object v0, p1, Lcom/samsung/android/visualeffect/EffectDataObj;->circleData:Lcom/samsung/android/visualeffect/lock/data/CircleData;

    iget v0, v0, Lcom/samsung/android/visualeffect/lock/data/CircleData;->circleUnlockMaxWidth:I

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->circleUnlockMaxWidth:I

    .line 87
    iget-object v0, p1, Lcom/samsung/android/visualeffect/EffectDataObj;->circleData:Lcom/samsung/android/visualeffect/lock/data/CircleData;

    iget v0, v0, Lcom/samsung/android/visualeffect/lock/data/CircleData;->outerStrokeWidth:I

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->outerStrokeWidth:I

    .line 88
    iget-object v0, p1, Lcom/samsung/android/visualeffect/EffectDataObj;->circleData:Lcom/samsung/android/visualeffect/lock/data/CircleData;

    iget v0, v0, Lcom/samsung/android/visualeffect/lock/data/CircleData;->innerStrokeWidth:I

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->innerStrokeWidth:I

    .line 89
    iget v0, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->arrowImageId:I

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->getResourceImageSize(IZ)I

    move-result v0

    iget v1, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->innerStrokeWidth:I

    sub-int/2addr v0, v1

    add-int/lit8 v0, v0, -0x4

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->circleUnlockMinWidth:I

    .line 90
    iget v0, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->circleUnlockMaxWidth:I

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->circleUnlockMaxHeight:I

    .line 91
    iget v0, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->circleUnlockMinWidth:I

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->circleUnlockMinRadius:I

    .line 92
    iget v0, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->circleUnlockMaxWidth:I

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->circleUnlockMaxRadius:I

    .line 93
    iget-object v0, p1, Lcom/samsung/android/visualeffect/EffectDataObj;->circleData:Lcom/samsung/android/visualeffect/lock/data/CircleData;

    iget-object v0, v0, Lcom/samsung/android/visualeffect/lock/data/CircleData;->lockSequenceImageId:[I

    iput-object v0, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->lockSequenceImageId:[I

    .line 94
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->lockSequenceImageId:[I

    array-length v0, v0

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->lockSequenceTotal:I

    .line 96
    const-string v0, "VisualEffectCircleUnlockEffect"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "arrowImageId = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->arrowImageId:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 97
    const-string v0, "VisualEffectCircleUnlockEffect"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "circleUnlockMaxWidth = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->circleUnlockMaxWidth:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 98
    const-string v0, "VisualEffectCircleUnlockEffect"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "circleUnlockMinWidth = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->circleUnlockMinWidth:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 99
    const-string v0, "VisualEffectCircleUnlockEffect"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "outerStrokeWidth = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->outerStrokeWidth:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 100
    const-string v0, "VisualEffectCircleUnlockEffect"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "innerStrokeWidth = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->innerStrokeWidth:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 101
    const-string v0, "VisualEffectCircleUnlockEffect"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "lockSequenceTotal = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->lockSequenceTotal:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 103
    invoke-direct {p0}, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->setLayout()V

    .line 104
    invoke-direct {p0}, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockEffect;->setAnimator()V

    goto/16 :goto_0
.end method

.method public reInit(Lcom/samsung/android/visualeffect/EffectDataObj;)V
    .locals 0
    .param p1, "data"    # Lcom/samsung/android/visualeffect/EffectDataObj;

    .prologue
    .line 394
    return-void
.end method

.method public removeListener()V
    .locals 0

    .prologue
    .line 419
    return-void
.end method

.method public setListener(Lcom/samsung/android/visualeffect/IEffectListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/android/visualeffect/IEffectListener;

    .prologue
    .line 415
    return-void
.end method
