.class public Lcom/samsung/android/visualeffect/lock/watercolor/WaterColorGLSurfaceRenderer;
.super Lcom/samsung/android/visualeffect/lock/common/GLSurfaceViewRenderer;
.source "WaterColorGLSurfaceRenderer.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/opengl/GLSurfaceView;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "view"    # Landroid/opengl/GLSurfaceView;

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/samsung/android/visualeffect/lock/common/GLSurfaceViewRenderer;-><init>()V

    .line 12
    iput-object p1, p0, Lcom/samsung/android/visualeffect/lock/watercolor/WaterColorGLSurfaceRenderer;->mContext:Landroid/content/Context;

    .line 13
    iput-object p2, p0, Lcom/samsung/android/visualeffect/lock/watercolor/WaterColorGLSurfaceRenderer;->mGlView:Landroid/opengl/GLSurfaceView;

    .line 14
    const-string v0, "/system/lib/libsecveWaterColor.so"

    iput-object v0, p0, Lcom/samsung/android/visualeffect/lock/watercolor/WaterColorGLSurfaceRenderer;->mLibName:Ljava/lang/String;

    .line 16
    const-string v0, "WaterColor Renderer"

    iput-object v0, p0, Lcom/samsung/android/visualeffect/lock/watercolor/WaterColorGLSurfaceRenderer;->TAG:Ljava/lang/String;

    .line 17
    return-void
.end method
