.class public Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;
.super Landroid/widget/FrameLayout;
.source "BlindMaskEffect.java"


# instance fields
.field private final TAG:Ljava/lang/String;

.field private final UNLOCK_ANIMATION_DURATION:I

.field private blindWidth:I

.field private height:I

.field private isRight:Z

.field private mContext:Landroid/content/Context;

.field private n:I

.field private pointX:F

.field private unlockAnimationDelay:I

.field private unlockAnimator:[Landroid/animation/ValueAnimator;

.field private unlockBlind:[Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMask;

.field private unlockLastAnimator:Landroid/animation/ValueAnimator;

.field private unlockLastBlind:Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMask;

.field private unlockWholeAnimationDuration:I

.field private width:I


# direct methods
.method public constructor <init>(Landroid/content/Context;IIFZI)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "width"    # I
    .param p3, "height"    # I
    .param p4, "pointX"    # F
    .param p5, "isRight"    # Z
    .param p6, "unlockDelayDuration"    # I

    .prologue
    .line 30
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 12
    const-string v0, "BlindEffect"

    iput-object v0, p0, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;->TAG:Ljava/lang/String;

    .line 17
    const/16 v0, 0xa

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;->n:I

    .line 27
    const/16 v0, 0x190

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;->UNLOCK_ANIMATION_DURATION:I

    .line 31
    const-string v0, "BlindEffect"

    const-string v1, "BlindMaskEffect : Constructor"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 32
    iput p2, p0, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;->width:I

    .line 33
    iput p3, p0, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;->height:I

    .line 34
    iput-object p1, p0, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;->mContext:Landroid/content/Context;

    .line 35
    iput p4, p0, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;->pointX:F

    .line 36
    iput-boolean p5, p0, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;->isRight:Z

    .line 37
    iput p6, p0, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;->unlockWholeAnimationDuration:I

    .line 38
    iget v0, p0, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;->unlockWholeAnimationDuration:I

    add-int/lit16 v0, v0, -0x190

    iget v1, p0, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;->n:I

    div-int/2addr v0, v1

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;->unlockAnimationDelay:I

    .line 39
    invoke-direct {p0}, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;->initUnLock()V

    .line 40
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;)[Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMask;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;

    .prologue
    .line 10
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;->unlockBlind:[Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMask;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;

    .prologue
    .line 10
    iget-boolean v0, p0, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;->isRight:Z

    return v0
.end method

.method static synthetic access$200(Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;)Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMask;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;

    .prologue
    .line 10
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;->unlockLastBlind:Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMask;

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;

    .prologue
    .line 10
    invoke-direct {p0}, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;->cancelAllMaskAnimator()V

    return-void
.end method

.method private cancelAllMaskAnimator()V
    .locals 3

    .prologue
    .line 192
    const-string v1, "BlindEffect"

    const-string v2, "BlindMaskEffect : cancelAllMaskAnimator"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 193
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v1, p0, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;->n:I

    if-ge v0, v1, :cond_1

    .line 194
    iget-object v1, p0, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;->unlockBlind:[Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMask;

    if-eqz v1, :cond_0

    .line 195
    iget-object v1, p0, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;->unlockAnimator:[Landroid/animation/ValueAnimator;

    aget-object v1, v1, v0

    invoke-direct {p0, v1}, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;->cancelAnimator(Landroid/animation/Animator;)V

    .line 193
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 197
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;->unlockLastBlind:Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMask;

    if-eqz v1, :cond_2

    .line 198
    iget-object v1, p0, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;->unlockLastAnimator:Landroid/animation/ValueAnimator;

    invoke-direct {p0, v1}, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;->cancelAnimator(Landroid/animation/Animator;)V

    .line 199
    :cond_2
    return-void
.end method

.method private cancelAnimator(Landroid/animation/Animator;)V
    .locals 1
    .param p1, "animator"    # Landroid/animation/Animator;

    .prologue
    .line 202
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/animation/Animator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 203
    invoke-virtual {p1}, Landroid/animation/Animator;->cancel()V

    .line 204
    :cond_0
    return-void
.end method

.method private fac(I)I
    .locals 2
    .param p1, "n"    # I

    .prologue
    .line 176
    const/4 v1, 0x0

    .line 178
    .local v1, "result":I
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_0
    if-gt v0, p1, :cond_0

    .line 179
    add-int/2addr v1, v0

    .line 178
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 181
    :cond_0
    return v1
.end method

.method private initUnLock()V
    .locals 3

    .prologue
    .line 43
    new-instance v1, Landroid/animation/ValueAnimator;

    invoke-direct {v1}, Landroid/animation/ValueAnimator;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;->unlockLastAnimator:Landroid/animation/ValueAnimator;

    .line 44
    iget v1, p0, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;->n:I

    new-array v1, v1, [Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMask;

    iput-object v1, p0, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;->unlockBlind:[Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMask;

    .line 45
    iget v1, p0, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;->n:I

    new-array v1, v1, [Landroid/animation/ValueAnimator;

    iput-object v1, p0, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;->unlockAnimator:[Landroid/animation/ValueAnimator;

    .line 47
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v1, p0, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;->n:I

    if-ge v0, v1, :cond_0

    .line 48
    iget-object v1, p0, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;->unlockAnimator:[Landroid/animation/ValueAnimator;

    new-instance v2, Landroid/animation/ValueAnimator;

    invoke-direct {v2}, Landroid/animation/ValueAnimator;-><init>()V

    aput-object v2, v1, v0

    .line 47
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 49
    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;->setunlockBlind()V

    .line 50
    invoke-direct {p0}, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;->setAnimation()V

    .line 51
    return-void
.end method

.method private setAnimation()V
    .locals 10

    .prologue
    const-wide/16 v8, 0x190

    const/4 v6, 0x2

    .line 120
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget v2, p0, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;->n:I

    if-ge v1, v2, :cond_1

    .line 122
    iget-boolean v2, p0, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;->isRight:Z

    if-eqz v2, :cond_0

    .line 123
    move v0, v1

    .line 129
    .local v0, "count":I
    :goto_1
    iget-object v2, p0, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;->unlockAnimator:[Landroid/animation/ValueAnimator;

    new-array v3, v6, [F

    fill-array-data v3, :array_0

    invoke-static {v3}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v3

    aput-object v3, v2, v0

    .line 130
    iget-object v2, p0, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;->unlockAnimator:[Landroid/animation/ValueAnimator;

    aget-object v2, v2, v0

    new-instance v3, Landroid/view/animation/interpolator/CubicEaseIn;

    invoke-direct {v3}, Landroid/view/animation/interpolator/CubicEaseIn;-><init>()V

    invoke-virtual {v2, v3}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 131
    iget-object v2, p0, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;->unlockAnimator:[Landroid/animation/ValueAnimator;

    aget-object v2, v2, v0

    invoke-virtual {v2, v8, v9}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 132
    iget-object v2, p0, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;->unlockAnimator:[Landroid/animation/ValueAnimator;

    aget-object v2, v2, v0

    iget v3, p0, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;->unlockAnimationDelay:I

    mul-int/2addr v3, v1

    int-to-long v4, v3

    invoke-virtual {v2, v4, v5}, Landroid/animation/ValueAnimator;->setStartDelay(J)V

    .line 133
    iget-object v2, p0, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;->unlockAnimator:[Landroid/animation/ValueAnimator;

    aget-object v2, v2, v0

    new-instance v3, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect$1;

    invoke-direct {v3, p0, v0}, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect$1;-><init>(Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;I)V

    invoke-virtual {v2, v3}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 120
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 126
    .end local v0    # "count":I
    :cond_0
    iget v2, p0, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;->n:I

    sub-int/2addr v2, v1

    add-int/lit8 v0, v2, -0x1

    .restart local v0    # "count":I
    goto :goto_1

    .line 145
    .end local v0    # "count":I
    :cond_1
    new-array v2, v6, [F

    fill-array-data v2, :array_1

    invoke-static {v2}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;->unlockLastAnimator:Landroid/animation/ValueAnimator;

    .line 146
    iget-object v2, p0, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;->unlockLastAnimator:Landroid/animation/ValueAnimator;

    new-instance v3, Landroid/view/animation/interpolator/CubicEaseIn;

    invoke-direct {v3}, Landroid/view/animation/interpolator/CubicEaseIn;-><init>()V

    invoke-virtual {v2, v3}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 147
    iget-object v2, p0, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;->unlockLastAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v2, v8, v9}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 148
    iget-object v2, p0, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;->unlockLastAnimator:Landroid/animation/ValueAnimator;

    iget v3, p0, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;->unlockWholeAnimationDuration:I

    add-int/lit16 v3, v3, -0x190

    int-to-long v4, v3

    invoke-virtual {v2, v4, v5}, Landroid/animation/ValueAnimator;->setStartDelay(J)V

    .line 149
    iget-object v2, p0, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;->unlockLastAnimator:Landroid/animation/ValueAnimator;

    new-instance v3, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect$2;

    invoke-direct {v3, p0}, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect$2;-><init>(Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;)V

    invoke-virtual {v2, v3}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 158
    iget-object v2, p0, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;->unlockLastAnimator:Landroid/animation/ValueAnimator;

    new-instance v3, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect$3;

    invoke-direct {v3, p0}, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect$3;-><init>(Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;)V

    invoke-virtual {v2, v3}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 173
    return-void

    .line 129
    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data

    .line 145
    :array_1
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method private setLeftUnlockLastBlind()V
    .locals 8

    .prologue
    const/4 v7, -0x2

    .line 89
    const/4 v0, 0x0

    .line 90
    .local v0, "blindLeft":I
    iget v3, p0, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;->pointX:F

    float-to-int v2, v3

    .line 92
    .local v2, "pointXplus":I
    iget v3, p0, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;->width:I

    div-int/lit8 v3, v3, 0xa

    sub-int/2addr v2, v3

    .line 94
    iput v2, p0, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;->blindWidth:I

    .line 96
    new-instance v3, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMask;

    iget-object v4, p0, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;->mContext:Landroid/content/Context;

    iget v5, p0, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;->blindWidth:I

    invoke-direct {v3, v4, v0, v5}, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMask;-><init>(Landroid/content/Context;II)V

    iput-object v3, p0, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;->unlockLastBlind:Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMask;

    .line 97
    iget-object v3, p0, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;->unlockLastBlind:Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMask;

    invoke-virtual {p0, v3, v7, v7}, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;->addView(Landroid/view/View;II)V

    .line 99
    iget-object v3, p0, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;->unlockLastBlind:Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMask;

    iget v4, p0, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;->height:I

    invoke-virtual {v3, v4}, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMask;->setHeight(I)V

    .line 100
    iget v3, p0, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;->blindWidth:I

    add-int/2addr v0, v3

    .line 102
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget v3, p0, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;->n:I

    if-ge v1, v3, :cond_1

    .line 104
    iget v3, p0, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;->n:I

    add-int/lit8 v3, v3, -0x1

    if-ne v1, v3, :cond_0

    .line 105
    iget v3, p0, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;->width:I

    sub-int/2addr v3, v0

    iput v3, p0, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;->blindWidth:I

    .line 110
    :goto_1
    iget-object v3, p0, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;->unlockBlind:[Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMask;

    new-instance v4, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMask;

    iget-object v5, p0, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;->mContext:Landroid/content/Context;

    iget v6, p0, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;->blindWidth:I

    invoke-direct {v4, v5, v0, v6}, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMask;-><init>(Landroid/content/Context;II)V

    aput-object v4, v3, v1

    .line 111
    iget-object v3, p0, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;->unlockBlind:[Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMask;

    aget-object v3, v3, v1

    invoke-virtual {p0, v3, v7, v7}, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;->addView(Landroid/view/View;II)V

    .line 113
    iget-object v3, p0, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;->unlockBlind:[Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMask;

    aget-object v3, v3, v1

    iget v4, p0, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;->height:I

    invoke-virtual {v3, v4}, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMask;->setHeight(I)V

    .line 114
    iget v3, p0, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;->blindWidth:I

    add-int/2addr v0, v3

    .line 102
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 107
    :cond_0
    add-int/lit8 v3, v1, 0x1

    iget v4, p0, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;->width:I

    sub-int/2addr v4, v2

    mul-int/2addr v3, v4

    iget v4, p0, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;->n:I

    invoke-direct {p0, v4}, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;->fac(I)I

    move-result v4

    div-int/2addr v3, v4

    iput v3, p0, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;->blindWidth:I

    goto :goto_1

    .line 116
    :cond_1
    return-void
.end method

.method private setRightUnlockBlind()V
    .locals 8

    .prologue
    const/4 v7, -0x2

    .line 63
    const/4 v0, 0x0

    .line 64
    .local v0, "blindLeft":I
    iget v3, p0, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;->pointX:F

    float-to-int v2, v3

    .line 66
    .local v2, "pointXplus":I
    iget v3, p0, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;->width:I

    div-int/lit8 v3, v3, 0xa

    add-int/2addr v2, v3

    .line 68
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget v3, p0, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;->n:I

    if-ge v1, v3, :cond_0

    .line 70
    iget v3, p0, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;->n:I

    sub-int/2addr v3, v1

    mul-int/2addr v3, v2

    iget v4, p0, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;->n:I

    invoke-direct {p0, v4}, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;->fac(I)I

    move-result v4

    div-int/2addr v3, v4

    iput v3, p0, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;->blindWidth:I

    .line 72
    iget-object v3, p0, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;->unlockBlind:[Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMask;

    new-instance v4, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMask;

    iget-object v5, p0, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;->mContext:Landroid/content/Context;

    iget v6, p0, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;->blindWidth:I

    invoke-direct {v4, v5, v0, v6}, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMask;-><init>(Landroid/content/Context;II)V

    aput-object v4, v3, v1

    .line 73
    iget-object v3, p0, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;->unlockBlind:[Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMask;

    aget-object v3, v3, v1

    invoke-virtual {p0, v3, v7, v7}, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;->addView(Landroid/view/View;II)V

    .line 75
    iget-object v3, p0, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;->unlockBlind:[Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMask;

    aget-object v3, v3, v1

    iget v4, p0, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;->height:I

    invoke-virtual {v3, v4}, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMask;->setHeight(I)V

    .line 76
    iget v3, p0, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;->blindWidth:I

    add-int/2addr v0, v3

    .line 68
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 79
    :cond_0
    iget v3, p0, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;->width:I

    sub-int/2addr v3, v0

    iput v3, p0, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;->blindWidth:I

    .line 81
    new-instance v3, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMask;

    iget-object v4, p0, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;->mContext:Landroid/content/Context;

    iget v5, p0, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;->blindWidth:I

    invoke-direct {v3, v4, v0, v5}, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMask;-><init>(Landroid/content/Context;II)V

    iput-object v3, p0, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;->unlockLastBlind:Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMask;

    .line 82
    iget-object v3, p0, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;->unlockLastBlind:Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMask;

    invoke-virtual {p0, v3, v7, v7}, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;->addView(Landroid/view/View;II)V

    .line 84
    iget-object v3, p0, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;->unlockLastBlind:Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMask;

    iget v4, p0, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;->height:I

    invoke-virtual {v3, v4}, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMask;->setHeight(I)V

    .line 85
    iget v3, p0, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;->blindWidth:I

    add-int/2addr v0, v3

    .line 86
    return-void
.end method

.method private setunlockBlind()V
    .locals 1

    .prologue
    .line 54
    iget-boolean v0, p0, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;->isRight:Z

    if-eqz v0, :cond_0

    .line 55
    invoke-direct {p0}, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;->setRightUnlockBlind()V

    .line 60
    :goto_0
    return-void

    .line 58
    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;->setLeftUnlockLastBlind()V

    goto :goto_0
.end method


# virtual methods
.method public animationStart()V
    .locals 2

    .prologue
    .line 185
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v1, p0, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;->n:I

    if-ge v0, v1, :cond_0

    .line 186
    iget-object v1, p0, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;->unlockAnimator:[Landroid/animation/ValueAnimator;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->start()V

    .line 185
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 188
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;->unlockLastAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->start()V

    .line 189
    return-void
.end method

.method public destroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 207
    const-string v0, "BlindEffect"

    const-string v1, "BlindMaskEffect : destroy"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 208
    invoke-direct {p0}, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;->cancelAllMaskAnimator()V

    .line 209
    invoke-virtual {p0}, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;->removeAllViews()V

    .line 210
    iput-object v2, p0, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;->unlockBlind:[Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMask;

    .line 211
    iput-object v2, p0, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;->unlockLastBlind:Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMask;

    .line 212
    iput-object v2, p0, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;->unlockAnimator:[Landroid/animation/ValueAnimator;

    .line 213
    iput-object v2, p0, Lcom/samsung/android/visualeffect/lock/blind/blindmask/BlindMaskEffect;->unlockLastAnimator:Landroid/animation/ValueAnimator;

    .line 214
    return-void
.end method
