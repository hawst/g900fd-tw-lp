.class public Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockCircle;
.super Landroid/view/View;
.source "CircleUnlockCircle.java"


# instance fields
.field private final DBG:Z

.field private final TAG:Ljava/lang/String;

.field private betweenRadius:I

.field private centerX:I

.field private centerY:I

.field private fillAnimationValue:F

.field private fillStrokePaint:Landroid/graphics/Paint;

.field private innerStrokePaint:Landroid/graphics/Paint;

.field private innerStrokeWidth:I

.field private isForShortcut:Z

.field private maxRadius:I

.field private minRadius:I

.field private outStrokePaint:Landroid/graphics/Paint;

.field private outerStrokeWidth:I

.field private strokeAnimationValue:F


# direct methods
.method public constructor <init>(Landroid/content/Context;IIII)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "circleMaxWidth"    # I
    .param p3, "circleMinWidth"    # I
    .param p4, "outerStrokeWidth"    # I
    .param p5, "innerStrokeWidth"    # I

    .prologue
    const/4 v1, 0x0

    .line 28
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 11
    const-string v0, "VisualEffectCircleUnlockEffect"

    iput-object v0, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockCircle;->TAG:Ljava/lang/String;

    .line 12
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockCircle;->DBG:Z

    .line 23
    iput v1, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockCircle;->strokeAnimationValue:F

    .line 24
    iput v1, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockCircle;->fillAnimationValue:F

    .line 25
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockCircle;->isForShortcut:Z

    .line 29
    div-int/lit8 v0, p2, 0x2

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockCircle;->centerY:I

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockCircle;->centerX:I

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockCircle;->maxRadius:I

    .line 30
    div-int/lit8 v0, p3, 0x2

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockCircle;->minRadius:I

    .line 31
    iget v0, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockCircle;->maxRadius:I

    iget v1, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockCircle;->minRadius:I

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockCircle;->betweenRadius:I

    .line 32
    iput p4, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockCircle;->outerStrokeWidth:I

    .line 33
    iput p5, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockCircle;->innerStrokeWidth:I

    .line 34
    invoke-direct {p0}, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockCircle;->setLayout()V

    .line 35
    return-void
.end method

.method private setLayout()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 39
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockCircle;->outStrokePaint:Landroid/graphics/Paint;

    .line 40
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockCircle;->outStrokePaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 41
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockCircle;->outStrokePaint:Landroid/graphics/Paint;

    const v1, -0x55000001

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 42
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockCircle;->outStrokePaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 43
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockCircle;->outStrokePaint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockCircle;->outerStrokeWidth:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 45
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockCircle;->innerStrokePaint:Landroid/graphics/Paint;

    .line 46
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockCircle;->innerStrokePaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 47
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockCircle;->innerStrokePaint:Landroid/graphics/Paint;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 48
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockCircle;->innerStrokePaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 49
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockCircle;->innerStrokePaint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockCircle;->innerStrokeWidth:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 51
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockCircle;->fillStrokePaint:Landroid/graphics/Paint;

    .line 52
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockCircle;->fillStrokePaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 53
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockCircle;->fillStrokePaint:Landroid/graphics/Paint;

    const v1, 0x55ffffff    # 3.518437E13f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 54
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockCircle;->fillStrokePaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 55
    return-void
.end method


# virtual methods
.method public dragAnimationUpdate(F)V
    .locals 0
    .param p1, "value"    # F

    .prologue
    .line 63
    iput p1, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockCircle;->fillAnimationValue:F

    .line 64
    invoke-virtual {p0}, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockCircle;->invalidate()V

    .line 65
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 8
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/high16 v7, 0x40000000    # 2.0f

    .line 78
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 81
    iget v3, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockCircle;->minRadius:I

    int-to-float v3, v3

    iget v4, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockCircle;->betweenRadius:I

    int-to-float v4, v4

    iget v5, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockCircle;->strokeAnimationValue:F

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    iget v4, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockCircle;->outerStrokeWidth:I

    int-to-float v4, v4

    div-float/2addr v4, v7

    sub-float v1, v3, v4

    .line 82
    .local v1, "radius":F
    iget v3, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockCircle;->centerX:I

    int-to-float v3, v3

    iget v4, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockCircle;->centerY:I

    int-to-float v4, v4

    iget-object v5, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockCircle;->outStrokePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v3, v4, v1, v5}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 85
    iget-boolean v3, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockCircle;->isForShortcut:Z

    if-nez v3, :cond_0

    .line 87
    iget v3, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockCircle;->centerX:I

    int-to-float v3, v3

    iget v4, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockCircle;->centerY:I

    int-to-float v4, v4

    iget v5, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockCircle;->minRadius:I

    int-to-float v5, v5

    iget-object v6, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockCircle;->innerStrokePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v3, v4, v5, v6}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 91
    :cond_0
    iget v3, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockCircle;->fillAnimationValue:F

    const/4 v4, 0x0

    cmpl-float v3, v3, v4

    if-lez v3, :cond_1

    .line 92
    iget v3, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockCircle;->fillAnimationValue:F

    iget v4, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockCircle;->strokeAnimationValue:F

    cmpl-float v3, v3, v4

    if-lez v3, :cond_2

    iget v2, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockCircle;->strokeAnimationValue:F

    .line 93
    .local v2, "tvalue":F
    :goto_0
    iget-object v3, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockCircle;->fillStrokePaint:Landroid/graphics/Paint;

    iget v4, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockCircle;->betweenRadius:I

    int-to-float v4, v4

    mul-float/2addr v4, v2

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 94
    iget v3, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockCircle;->minRadius:I

    int-to-float v3, v3

    iget v4, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockCircle;->betweenRadius:I

    int-to-float v4, v4

    mul-float/2addr v4, v2

    div-float/2addr v4, v7

    add-float v0, v3, v4

    .line 95
    .local v0, "fillRadius":F
    iget v3, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockCircle;->centerX:I

    int-to-float v3, v3

    iget v4, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockCircle;->centerY:I

    int-to-float v4, v4

    iget-object v5, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockCircle;->fillStrokePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v3, v4, v0, v5}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 97
    .end local v0    # "fillRadius":F
    .end local v2    # "tvalue":F
    :cond_1
    return-void

    .line 92
    :cond_2
    iget v2, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockCircle;->fillAnimationValue:F

    goto :goto_0
.end method

.method public setCircleMinWidth(I)V
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 72
    div-int/lit8 v0, p1, 0x2

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockCircle;->minRadius:I

    .line 73
    iget v0, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockCircle;->maxRadius:I

    iget v1, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockCircle;->minRadius:I

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockCircle;->betweenRadius:I

    .line 74
    return-void
.end method

.method public setIsForShortcut(Z)V
    .locals 0
    .param p1, "value"    # Z

    .prologue
    .line 68
    iput-boolean p1, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockCircle;->isForShortcut:Z

    .line 69
    return-void
.end method

.method public strokeAnimationUpdate(F)V
    .locals 0
    .param p1, "value"    # F

    .prologue
    .line 58
    iput p1, p0, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockCircle;->strokeAnimationValue:F

    .line 59
    invoke-virtual {p0}, Lcom/samsung/android/visualeffect/lock/circleunlock/CircleUnlockCircle;->invalidate()V

    .line 60
    return-void
.end method
