.class public Lcom/samsung/android/visualeffect/lock/common/GLSurfaceViewRenderer;
.super Ljava/lang/Object;
.source "GLSurfaceViewRenderer.java"

# interfaces
.implements Landroid/opengl/GLSurfaceView$Renderer;


# instance fields
.field protected TAG:Ljava/lang/String;

.field protected framecounter:I

.field protected isAffordanceOccur:Z

.field protected mAffordancePosX:I

.field protected mAffordancePosY:I

.field protected mBackgroundHeight:I

.field protected mBackgroundPixels:[I

.field protected mBackgroundWidth:I

.field protected mContext:Landroid/content/Context;

.field protected mGlView:Landroid/opengl/GLSurfaceView;

.field protected mHeight:I

.field protected mIsNeedToReinit:Z

.field protected final mLibDir:Ljava/lang/String;

.field protected mLibName:Ljava/lang/String;

.field protected final mNative:Lcom/samsung/android/visualeffect/lock/common/Native;

.field protected mWidth:I

.field protected timeStart:J


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput v1, p0, Lcom/samsung/android/visualeffect/lock/common/GLSurfaceViewRenderer;->mWidth:I

    .line 43
    iput v1, p0, Lcom/samsung/android/visualeffect/lock/common/GLSurfaceViewRenderer;->mHeight:I

    .line 44
    iput v1, p0, Lcom/samsung/android/visualeffect/lock/common/GLSurfaceViewRenderer;->mAffordancePosX:I

    .line 45
    iput v1, p0, Lcom/samsung/android/visualeffect/lock/common/GLSurfaceViewRenderer;->mAffordancePosY:I

    .line 49
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLSurfaceViewRenderer;->mBackgroundPixels:[I

    .line 50
    iput v1, p0, Lcom/samsung/android/visualeffect/lock/common/GLSurfaceViewRenderer;->mBackgroundWidth:I

    .line 51
    iput v1, p0, Lcom/samsung/android/visualeffect/lock/common/GLSurfaceViewRenderer;->mBackgroundHeight:I

    .line 54
    iput-boolean v1, p0, Lcom/samsung/android/visualeffect/lock/common/GLSurfaceViewRenderer;->mIsNeedToReinit:Z

    .line 56
    const-string v0, "/system/lib/"

    iput-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLSurfaceViewRenderer;->mLibDir:Ljava/lang/String;

    .line 58
    new-instance v0, Lcom/samsung/android/visualeffect/lock/common/Native;

    invoke-direct {v0}, Lcom/samsung/android/visualeffect/lock/common/Native;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLSurfaceViewRenderer;->mNative:Lcom/samsung/android/visualeffect/lock/common/Native;

    return-void
.end method


# virtual methods
.method public clearEffect()V
    .locals 2

    .prologue
    .line 175
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLSurfaceViewRenderer;->mNative:Lcom/samsung/android/visualeffect/lock/common/Native;

    invoke-virtual {v0}, Lcom/samsung/android/visualeffect/lock/common/Native;->clear()V

    .line 177
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLSurfaceViewRenderer;->mGlView:Landroid/opengl/GLSurfaceView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/opengl/GLSurfaceView;->setRenderMode(I)V

    .line 178
    return-void
.end method

.method public handleTouchEvent(III)V
    .locals 3
    .param p1, "action"    # I
    .param p2, "x"    # I
    .param p3, "y"    # I

    .prologue
    const/4 v2, 0x1

    .line 144
    packed-switch p1, :pswitch_data_0

    .line 170
    :goto_0
    :pswitch_0
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLSurfaceViewRenderer;->mGlView:Landroid/opengl/GLSurfaceView;

    invoke-virtual {v0, v2}, Landroid/opengl/GLSurfaceView;->setRenderMode(I)V

    .line 171
    return-void

    .line 146
    :pswitch_1
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLSurfaceViewRenderer;->mNative:Lcom/samsung/android/visualeffect/lock/common/Native;

    const/4 v1, 0x0

    invoke-virtual {v0, p2, p3, v1}, Lcom/samsung/android/visualeffect/lock/common/Native;->onTouch(III)V

    goto :goto_0

    .line 149
    :pswitch_2
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLSurfaceViewRenderer;->mNative:Lcom/samsung/android/visualeffect/lock/common/Native;

    invoke-virtual {v0, p2, p3, v2}, Lcom/samsung/android/visualeffect/lock/common/Native;->onTouch(III)V

    goto :goto_0

    .line 152
    :pswitch_3
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLSurfaceViewRenderer;->mNative:Lcom/samsung/android/visualeffect/lock/common/Native;

    const/4 v1, 0x2

    invoke-virtual {v0, p2, p3, v1}, Lcom/samsung/android/visualeffect/lock/common/Native;->onTouch(III)V

    goto :goto_0

    .line 155
    :pswitch_4
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLSurfaceViewRenderer;->mNative:Lcom/samsung/android/visualeffect/lock/common/Native;

    const/4 v1, 0x3

    invoke-virtual {v0, p2, p3, v1}, Lcom/samsung/android/visualeffect/lock/common/Native;->onTouch(III)V

    goto :goto_0

    .line 158
    :pswitch_5
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLSurfaceViewRenderer;->mNative:Lcom/samsung/android/visualeffect/lock/common/Native;

    const/4 v1, 0x4

    invoke-virtual {v0, p2, p3, v1}, Lcom/samsung/android/visualeffect/lock/common/Native;->onTouch(III)V

    goto :goto_0

    .line 161
    :pswitch_6
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLSurfaceViewRenderer;->mNative:Lcom/samsung/android/visualeffect/lock/common/Native;

    const/4 v1, 0x5

    invoke-virtual {v0, p2, p3, v1}, Lcom/samsung/android/visualeffect/lock/common/Native;->onTouch(III)V

    goto :goto_0

    .line 144
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_6
        :pswitch_0
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public loadSpecialTexture([Ljava/lang/String;)V
    .locals 17
    .param p1, "aTexture"    # [Ljava/lang/String;

    .prologue
    .line 207
    if-nez p1, :cond_1

    .line 253
    :cond_0
    :goto_0
    return-void

    .line 210
    :cond_1
    new-instance v14, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v14}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 211
    .local v14, "options":Landroid/graphics/BitmapFactory$Options;
    const/4 v3, 0x0

    iput-boolean v3, v14, Landroid/graphics/BitmapFactory$Options;->inScaled:Z

    .line 215
    const-string v15, "com.samsung.android.visualeffect.res"

    .line 221
    .local v15, "packageName":Ljava/lang/String;
    const/4 v9, 0x0

    .line 223
    .local v9, "con":Landroid/content/Context;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/visualeffect/lock/common/GLSurfaceViewRenderer;->mContext:Landroid/content/Context;

    const/4 v5, 0x0

    invoke-virtual {v3, v15, v5}, Landroid/content/Context;->createPackageContext(Ljava/lang/String;I)Landroid/content/Context;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v9

    .line 228
    :goto_1
    if-eqz v9, :cond_0

    .line 231
    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v16

    .line 233
    .local v16, "res":Landroid/content/res/Resources;
    const/4 v12, 0x0

    .local v12, "i":I
    :goto_2
    move-object/from16 v0, p1

    array-length v3, v0

    if-ge v12, v3, :cond_2

    .line 235
    :try_start_1
    aget-object v3, p1, v12

    const-string v5, "drawable"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3, v5, v15}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v13

    .line 237
    .local v13, "id":I
    move-object/from16 v0, v16

    invoke-static {v0, v13}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 238
    .local v1, "bitmap":Landroid/graphics/Bitmap;
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    .line 239
    .local v4, "width":I
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v8

    .line 240
    .local v8, "height":I
    mul-int v3, v4, v8

    new-array v2, v3, [I

    .line 241
    .local v2, "pixels":[I
    const/4 v3, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move v7, v4

    invoke-virtual/range {v1 .. v8}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    .line 242
    const-string v3, ""

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Adding texture Width\'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 243
    const-string v3, ""

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Adding texture Height\'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 244
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/visualeffect/lock/common/GLSurfaceViewRenderer;->mNative:Lcom/samsung/android/visualeffect/lock/common/Native;

    aget-object v5, p1, v12

    invoke-virtual {v3, v5, v2, v4, v8}, Lcom/samsung/android/visualeffect/lock/common/Native;->loadTexture(Ljava/lang/String;[III)V

    .line 245
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 233
    .end local v1    # "bitmap":Landroid/graphics/Bitmap;
    .end local v2    # "pixels":[I
    .end local v4    # "width":I
    .end local v8    # "height":I
    .end local v13    # "id":I
    :goto_3
    add-int/lit8 v12, v12, 0x1

    goto :goto_2

    .line 224
    .end local v12    # "i":I
    .end local v16    # "res":Landroid/content/res/Resources;
    :catch_0
    move-exception v11

    .line 226
    .local v11, "e1":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v11}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_1

    .line 248
    .end local v11    # "e1":Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v12    # "i":I
    .restart local v16    # "res":Landroid/content/res/Resources;
    :catch_1
    move-exception v10

    .line 249
    .local v10, "e":Ljava/lang/Exception;
    const-string v3, ""

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "There is no image \'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    aget-object v6, p1, v12

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 252
    .end local v10    # "e":Ljava/lang/Exception;
    :cond_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/visualeffect/lock/common/GLSurfaceViewRenderer;->mGlView:Landroid/opengl/GLSurfaceView;

    const/4 v5, 0x1

    invoke-virtual {v3, v5}, Landroid/opengl/GLSurfaceView;->setRenderMode(I)V

    goto/16 :goto_0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 181
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLSurfaceViewRenderer;->mNative:Lcom/samsung/android/visualeffect/lock/common/Native;

    invoke-virtual {v0}, Lcom/samsung/android/visualeffect/lock/common/Native;->destroy()V

    .line 182
    return-void
.end method

.method public onDrawFrame(Ljavax/microedition/khronos/opengles/GL10;)V
    .locals 13
    .param p1, "gl"    # Ljavax/microedition/khronos/opengles/GL10;

    .prologue
    const/high16 v12, 0x447a0000    # 1000.0f

    const/4 v11, -0x1

    const/4 v10, 0x0

    .line 91
    iget-boolean v5, p0, Lcom/samsung/android/visualeffect/lock/common/GLSurfaceViewRenderer;->mIsNeedToReinit:Z

    if-eqz v5, :cond_0

    .line 92
    invoke-static {}, Landroid/opengl/EGL14;->eglGetCurrentDisplay()Landroid/opengl/EGLDisplay;

    move-result-object v5

    invoke-static {v5, v10}, Landroid/opengl/EGL14;->eglSwapInterval(Landroid/opengl/EGLDisplay;I)Z

    .line 93
    iget-object v5, p0, Lcom/samsung/android/visualeffect/lock/common/GLSurfaceViewRenderer;->mNative:Lcom/samsung/android/visualeffect/lock/common/Native;

    iget-object v6, p0, Lcom/samsung/android/visualeffect/lock/common/GLSurfaceViewRenderer;->mLibName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Lcom/samsung/android/visualeffect/lock/common/Native;->loadEffect(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/samsung/android/visualeffect/lock/common/GLSurfaceViewRenderer;->loadSpecialTexture([Ljava/lang/String;)V

    .line 94
    iput v11, p0, Lcom/samsung/android/visualeffect/lock/common/GLSurfaceViewRenderer;->framecounter:I

    .line 97
    :cond_0
    iget v5, p0, Lcom/samsung/android/visualeffect/lock/common/GLSurfaceViewRenderer;->framecounter:I

    if-ne v5, v11, :cond_1

    .line 98
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/samsung/android/visualeffect/lock/common/GLSurfaceViewRenderer;->timeStart:J

    .line 101
    :cond_1
    iget-object v5, p0, Lcom/samsung/android/visualeffect/lock/common/GLSurfaceViewRenderer;->mBackgroundPixels:[I

    if-eqz v5, :cond_2

    .line 102
    iget-object v5, p0, Lcom/samsung/android/visualeffect/lock/common/GLSurfaceViewRenderer;->mNative:Lcom/samsung/android/visualeffect/lock/common/Native;

    const-string v6, "bg"

    iget-object v7, p0, Lcom/samsung/android/visualeffect/lock/common/GLSurfaceViewRenderer;->mBackgroundPixels:[I

    iget v8, p0, Lcom/samsung/android/visualeffect/lock/common/GLSurfaceViewRenderer;->mBackgroundWidth:I

    iget v9, p0, Lcom/samsung/android/visualeffect/lock/common/GLSurfaceViewRenderer;->mBackgroundHeight:I

    invoke-virtual {v5, v6, v7, v8, v9}, Lcom/samsung/android/visualeffect/lock/common/Native;->loadTexture(Ljava/lang/String;[III)V

    .line 104
    const/4 v5, 0x0

    iput-object v5, p0, Lcom/samsung/android/visualeffect/lock/common/GLSurfaceViewRenderer;->mBackgroundPixels:[I

    .line 107
    :cond_2
    iget-boolean v5, p0, Lcom/samsung/android/visualeffect/lock/common/GLSurfaceViewRenderer;->mIsNeedToReinit:Z

    if-eqz v5, :cond_3

    .line 108
    iget-object v5, p0, Lcom/samsung/android/visualeffect/lock/common/GLSurfaceViewRenderer;->mNative:Lcom/samsung/android/visualeffect/lock/common/Native;

    iget v6, p0, Lcom/samsung/android/visualeffect/lock/common/GLSurfaceViewRenderer;->mWidth:I

    iget v7, p0, Lcom/samsung/android/visualeffect/lock/common/GLSurfaceViewRenderer;->mHeight:I

    const/4 v8, 0x1

    invoke-virtual {v5, v6, v7, v8}, Lcom/samsung/android/visualeffect/lock/common/Native;->init(IIZ)V

    .line 109
    iput-boolean v10, p0, Lcom/samsung/android/visualeffect/lock/common/GLSurfaceViewRenderer;->mIsNeedToReinit:Z

    .line 112
    :cond_3
    iget-boolean v5, p0, Lcom/samsung/android/visualeffect/lock/common/GLSurfaceViewRenderer;->isAffordanceOccur:Z

    if-eqz v5, :cond_4

    .line 113
    iget-object v5, p0, Lcom/samsung/android/visualeffect/lock/common/GLSurfaceViewRenderer;->mNative:Lcom/samsung/android/visualeffect/lock/common/Native;

    iget v6, p0, Lcom/samsung/android/visualeffect/lock/common/GLSurfaceViewRenderer;->mAffordancePosX:I

    iget v7, p0, Lcom/samsung/android/visualeffect/lock/common/GLSurfaceViewRenderer;->mAffordancePosY:I

    invoke-virtual {v5, v6, v7}, Lcom/samsung/android/visualeffect/lock/common/Native;->showAffordance(II)V

    .line 114
    iput-boolean v10, p0, Lcom/samsung/android/visualeffect/lock/common/GLSurfaceViewRenderer;->isAffordanceOccur:Z

    .line 117
    :cond_4
    const/4 v1, 0x0

    .line 119
    .local v1, "isStopDrawing":Z
    iget-object v5, p0, Lcom/samsung/android/visualeffect/lock/common/GLSurfaceViewRenderer;->mNative:Lcom/samsung/android/visualeffect/lock/common/Native;

    invoke-virtual {v5}, Lcom/samsung/android/visualeffect/lock/common/Native;->draw()Z

    move-result v5

    if-nez v5, :cond_5

    .line 120
    iget-object v5, p0, Lcom/samsung/android/visualeffect/lock/common/GLSurfaceViewRenderer;->TAG:Ljava/lang/String;

    const-string v6, "dirty mode"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 121
    const/4 v1, 0x1

    .line 122
    iget-object v5, p0, Lcom/samsung/android/visualeffect/lock/common/GLSurfaceViewRenderer;->mGlView:Landroid/opengl/GLSurfaceView;

    invoke-virtual {v5, v10}, Landroid/opengl/GLSurfaceView;->setRenderMode(I)V

    .line 126
    :cond_5
    iget v5, p0, Lcom/samsung/android/visualeffect/lock/common/GLSurfaceViewRenderer;->framecounter:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/samsung/android/visualeffect/lock/common/GLSurfaceViewRenderer;->framecounter:I

    .line 127
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 128
    .local v2, "now":J
    iget-wide v6, p0, Lcom/samsung/android/visualeffect/lock/common/GLSurfaceViewRenderer;->timeStart:J

    sub-long v6, v2, v6

    long-to-float v4, v6

    .line 129
    .local v4, "spent":F
    cmpl-float v5, v4, v12

    if-gez v5, :cond_6

    if-eqz v1, :cond_8

    .line 130
    :cond_6
    iget v5, p0, Lcom/samsung/android/visualeffect/lock/common/GLSurfaceViewRenderer;->framecounter:I

    const/4 v6, 0x2

    if-le v5, v6, :cond_7

    .line 131
    iget v5, p0, Lcom/samsung/android/visualeffect/lock/common/GLSurfaceViewRenderer;->framecounter:I

    int-to-float v5, v5

    mul-float/2addr v5, v12

    div-float v0, v5, v4

    .line 132
    .local v0, "fps":F
    iget-object v5, p0, Lcom/samsung/android/visualeffect/lock/common/GLSurfaceViewRenderer;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "fps "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 134
    .end local v0    # "fps":F
    :cond_7
    iput v11, p0, Lcom/samsung/android/visualeffect/lock/common/GLSurfaceViewRenderer;->framecounter:I

    .line 136
    :cond_8
    return-void
.end method

.method public onSurfaceChanged(Ljavax/microedition/khronos/opengles/GL10;II)V
    .locals 3
    .param p1, "gl"    # Ljavax/microedition/khronos/opengles/GL10;
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    const/4 v1, 0x1

    .line 72
    if-lez p2, :cond_0

    if-gtz p3, :cond_1

    .line 73
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLSurfaceViewRenderer;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onSurfaceChanged problem width"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 87
    :goto_0
    return-void

    .line 77
    :cond_1
    iget v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLSurfaceViewRenderer;->mWidth:I

    if-ne v0, p2, :cond_2

    iget v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLSurfaceViewRenderer;->mHeight:I

    if-eq v0, p3, :cond_3

    .line 78
    :cond_2
    iput-boolean v1, p0, Lcom/samsung/android/visualeffect/lock/common/GLSurfaceViewRenderer;->mIsNeedToReinit:Z

    .line 81
    :cond_3
    iput p2, p0, Lcom/samsung/android/visualeffect/lock/common/GLSurfaceViewRenderer;->mWidth:I

    .line 82
    iput p3, p0, Lcom/samsung/android/visualeffect/lock/common/GLSurfaceViewRenderer;->mHeight:I

    .line 84
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLSurfaceViewRenderer;->mGlView:Landroid/opengl/GLSurfaceView;

    invoke-virtual {v0, v1}, Landroid/opengl/GLSurfaceView;->setRenderMode(I)V

    .line 86
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLSurfaceViewRenderer;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onSurfaceChanged "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onSurfaceCreated(Ljavax/microedition/khronos/opengles/GL10;Ljavax/microedition/khronos/egl/EGLConfig;)V
    .locals 3
    .param p1, "gl"    # Ljavax/microedition/khronos/opengles/GL10;
    .param p2, "config"    # Ljavax/microedition/khronos/egl/EGLConfig;

    .prologue
    const/4 v2, 0x0

    .line 63
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLSurfaceViewRenderer;->TAG:Ljava/lang/String;

    const-string v1, "onSurfaceCreated"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 64
    iput v2, p0, Lcom/samsung/android/visualeffect/lock/common/GLSurfaceViewRenderer;->mWidth:I

    .line 65
    iput v2, p0, Lcom/samsung/android/visualeffect/lock/common/GLSurfaceViewRenderer;->mHeight:I

    .line 66
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLSurfaceViewRenderer;->mIsNeedToReinit:Z

    .line 67
    return-void
.end method

.method public setBackgroundBitmap([III)V
    .locals 3
    .param p1, "pixels"    # [I
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    const/4 v2, 0x1

    .line 196
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLSurfaceViewRenderer;->TAG:Ljava/lang/String;

    const-string v1, "setBackgroundBitmap"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 197
    iput-object p1, p0, Lcom/samsung/android/visualeffect/lock/common/GLSurfaceViewRenderer;->mBackgroundPixels:[I

    .line 198
    iput p2, p0, Lcom/samsung/android/visualeffect/lock/common/GLSurfaceViewRenderer;->mBackgroundWidth:I

    .line 199
    iput p3, p0, Lcom/samsung/android/visualeffect/lock/common/GLSurfaceViewRenderer;->mBackgroundHeight:I

    .line 201
    iput-boolean v2, p0, Lcom/samsung/android/visualeffect/lock/common/GLSurfaceViewRenderer;->mIsNeedToReinit:Z

    .line 203
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLSurfaceViewRenderer;->mGlView:Landroid/opengl/GLSurfaceView;

    invoke-virtual {v0, v2}, Landroid/opengl/GLSurfaceView;->setRenderMode(I)V

    .line 204
    return-void
.end method

.method public setParameters([I[F)V
    .locals 1
    .param p1, "aNums"    # [I
    .param p2, "aValues"    # [F

    .prologue
    .line 140
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLSurfaceViewRenderer;->mNative:Lcom/samsung/android/visualeffect/lock/common/Native;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/android/visualeffect/lock/common/Native;->setParameters([I[F)V

    .line 141
    return-void
.end method

.method public showUnlock()V
    .locals 2

    .prologue
    .line 256
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLSurfaceViewRenderer;->mNative:Lcom/samsung/android/visualeffect/lock/common/Native;

    invoke-virtual {v0}, Lcom/samsung/android/visualeffect/lock/common/Native;->showUnlock()V

    .line 257
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLSurfaceViewRenderer;->mGlView:Landroid/opengl/GLSurfaceView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/opengl/GLSurfaceView;->setRenderMode(I)V

    .line 258
    return-void
.end method

.method public showUnlockAffordance(II)V
    .locals 3
    .param p1, "posX"    # I
    .param p2, "posY"    # I

    .prologue
    const/4 v2, 0x1

    .line 185
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLSurfaceViewRenderer;->TAG:Ljava/lang/String;

    const-string v1, "showUnlockAffordance"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 187
    iput p1, p0, Lcom/samsung/android/visualeffect/lock/common/GLSurfaceViewRenderer;->mAffordancePosX:I

    .line 188
    iput p2, p0, Lcom/samsung/android/visualeffect/lock/common/GLSurfaceViewRenderer;->mAffordancePosY:I

    .line 190
    iput-boolean v2, p0, Lcom/samsung/android/visualeffect/lock/common/GLSurfaceViewRenderer;->isAffordanceOccur:Z

    .line 192
    iget-object v0, p0, Lcom/samsung/android/visualeffect/lock/common/GLSurfaceViewRenderer;->mGlView:Landroid/opengl/GLSurfaceView;

    invoke-virtual {v0, v2}, Landroid/opengl/GLSurfaceView;->setRenderMode(I)V

    .line 193
    return-void
.end method
