.class Lcom/android/sec/org/bouncycastle/operator/bc/BcDigestCalculatorProvider$1;
.super Ljava/lang/Object;
.source "BcDigestCalculatorProvider.java"

# interfaces
.implements Lcom/android/sec/org/bouncycastle/operator/DigestCalculator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/sec/org/bouncycastle/operator/bc/BcDigestCalculatorProvider;->get(Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;)Lcom/android/sec/org/bouncycastle/operator/DigestCalculator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/sec/org/bouncycastle/operator/bc/BcDigestCalculatorProvider;

.field final synthetic val$algorithm:Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;

.field final synthetic val$stream:Lcom/android/sec/org/bouncycastle/operator/bc/BcDigestCalculatorProvider$DigestOutputStream;


# direct methods
.method constructor <init>(Lcom/android/sec/org/bouncycastle/operator/bc/BcDigestCalculatorProvider;Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;Lcom/android/sec/org/bouncycastle/operator/bc/BcDigestCalculatorProvider$DigestOutputStream;)V
    .locals 0

    .prologue
    .line 27
    iput-object p1, p0, Lcom/android/sec/org/bouncycastle/operator/bc/BcDigestCalculatorProvider$1;->this$0:Lcom/android/sec/org/bouncycastle/operator/bc/BcDigestCalculatorProvider;

    iput-object p2, p0, Lcom/android/sec/org/bouncycastle/operator/bc/BcDigestCalculatorProvider$1;->val$algorithm:Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;

    iput-object p3, p0, Lcom/android/sec/org/bouncycastle/operator/bc/BcDigestCalculatorProvider$1;->val$stream:Lcom/android/sec/org/bouncycastle/operator/bc/BcDigestCalculatorProvider$DigestOutputStream;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getAlgorithmIdentifier()Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/operator/bc/BcDigestCalculatorProvider$1;->val$algorithm:Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;

    return-object v0
.end method

.method public getDigest()[B
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/operator/bc/BcDigestCalculatorProvider$1;->val$stream:Lcom/android/sec/org/bouncycastle/operator/bc/BcDigestCalculatorProvider$DigestOutputStream;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/operator/bc/BcDigestCalculatorProvider$DigestOutputStream;->getDigest()[B

    move-result-object v0

    return-object v0
.end method

.method public getOutputStream()Ljava/io/OutputStream;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/operator/bc/BcDigestCalculatorProvider$1;->val$stream:Lcom/android/sec/org/bouncycastle/operator/bc/BcDigestCalculatorProvider$DigestOutputStream;

    return-object v0
.end method
