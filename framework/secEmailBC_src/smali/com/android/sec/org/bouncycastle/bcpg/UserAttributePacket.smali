.class public Lcom/android/sec/org/bouncycastle/bcpg/UserAttributePacket;
.super Lcom/android/sec/org/bouncycastle/bcpg/ContainedPacket;
.source "UserAttributePacket.java"


# instance fields
.field private subpackets:[Lcom/android/sec/org/bouncycastle/bcpg/UserAttributeSubpacket;


# direct methods
.method public constructor <init>(Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;)V
    .locals 6
    .param p1, "in"    # Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/bcpg/ContainedPacket;-><init>()V

    .line 19
    new-instance v1, Lcom/android/sec/org/bouncycastle/bcpg/UserAttributeSubpacketInputStream;

    invoke-direct {v1, p1}, Lcom/android/sec/org/bouncycastle/bcpg/UserAttributeSubpacketInputStream;-><init>(Ljava/io/InputStream;)V

    .line 22
    .local v1, "sIn":Lcom/android/sec/org/bouncycastle/bcpg/UserAttributeSubpacketInputStream;
    new-instance v3, Ljava/util/Vector;

    invoke-direct {v3}, Ljava/util/Vector;-><init>()V

    .line 23
    .local v3, "v":Ljava/util/Vector;
    :goto_0
    invoke-virtual {v1}, Lcom/android/sec/org/bouncycastle/bcpg/UserAttributeSubpacketInputStream;->readPacket()Lcom/android/sec/org/bouncycastle/bcpg/UserAttributeSubpacket;

    move-result-object v2

    .local v2, "sub":Lcom/android/sec/org/bouncycastle/bcpg/UserAttributeSubpacket;
    if-eqz v2, :cond_0

    .line 25
    invoke-virtual {v3, v2}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    goto :goto_0

    .line 28
    :cond_0
    invoke-virtual {v3}, Ljava/util/Vector;->size()I

    move-result v4

    new-array v4, v4, [Lcom/android/sec/org/bouncycastle/bcpg/UserAttributeSubpacket;

    iput-object v4, p0, Lcom/android/sec/org/bouncycastle/bcpg/UserAttributePacket;->subpackets:[Lcom/android/sec/org/bouncycastle/bcpg/UserAttributeSubpacket;

    .line 30
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v4, p0, Lcom/android/sec/org/bouncycastle/bcpg/UserAttributePacket;->subpackets:[Lcom/android/sec/org/bouncycastle/bcpg/UserAttributeSubpacket;

    array-length v4, v4

    if-eq v0, v4, :cond_1

    .line 32
    iget-object v5, p0, Lcom/android/sec/org/bouncycastle/bcpg/UserAttributePacket;->subpackets:[Lcom/android/sec/org/bouncycastle/bcpg/UserAttributeSubpacket;

    invoke-virtual {v3, v0}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/sec/org/bouncycastle/bcpg/UserAttributeSubpacket;

    aput-object v4, v5, v0

    .line 30
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 34
    :cond_1
    return-void
.end method

.method public constructor <init>([Lcom/android/sec/org/bouncycastle/bcpg/UserAttributeSubpacket;)V
    .locals 0
    .param p1, "subpackets"    # [Lcom/android/sec/org/bouncycastle/bcpg/UserAttributeSubpacket;

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/bcpg/ContainedPacket;-><init>()V

    .line 39
    iput-object p1, p0, Lcom/android/sec/org/bouncycastle/bcpg/UserAttributePacket;->subpackets:[Lcom/android/sec/org/bouncycastle/bcpg/UserAttributeSubpacket;

    .line 40
    return-void
.end method


# virtual methods
.method public encode(Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;)V
    .locals 5
    .param p1, "out"    # Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 51
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 53
    .local v0, "bOut":Ljava/io/ByteArrayOutputStream;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/UserAttributePacket;->subpackets:[Lcom/android/sec/org/bouncycastle/bcpg/UserAttributeSubpacket;

    array-length v2, v2

    if-eq v1, v2, :cond_0

    .line 55
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/UserAttributePacket;->subpackets:[Lcom/android/sec/org/bouncycastle/bcpg/UserAttributeSubpacket;

    aget-object v2, v2, v1

    invoke-virtual {v2, v0}, Lcom/android/sec/org/bouncycastle/bcpg/UserAttributeSubpacket;->encode(Ljava/io/OutputStream;)V

    .line 53
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 58
    :cond_0
    const/16 v2, 0x11

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {p1, v2, v3, v4}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->writePacket(I[BZ)V

    .line 59
    return-void
.end method

.method public getSubpackets()[Lcom/android/sec/org/bouncycastle/bcpg/UserAttributeSubpacket;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/UserAttributePacket;->subpackets:[Lcom/android/sec/org/bouncycastle/bcpg/UserAttributeSubpacket;

    return-object v0
.end method
