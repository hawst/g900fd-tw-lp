.class public Lcom/android/sec/org/bouncycastle/bcpg/sig/TrustSignature;
.super Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;
.source "TrustSignature.java"


# direct methods
.method public constructor <init>(ZII)V
    .locals 2
    .param p1, "critical"    # Z
    .param p2, "depth"    # I
    .param p3, "trustAmount"    # I

    .prologue
    .line 36
    const/4 v0, 0x5

    invoke-static {p2, p3}, Lcom/android/sec/org/bouncycastle/bcpg/sig/TrustSignature;->intToByteArray(II)[B

    move-result-object v1

    invoke-direct {p0, v0, p1, v1}, Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;-><init>(IZ[B)V

    .line 37
    return-void
.end method

.method public constructor <init>(Z[B)V
    .locals 1
    .param p1, "critical"    # Z
    .param p2, "data"    # [B

    .prologue
    .line 28
    const/4 v0, 0x5

    invoke-direct {p0, v0, p1, p2}, Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;-><init>(IZ[B)V

    .line 29
    return-void
.end method

.method private static intToByteArray(II)[B
    .locals 3
    .param p0, "v1"    # I
    .param p1, "v2"    # I

    .prologue
    .line 16
    const/4 v1, 0x2

    new-array v0, v1, [B

    .line 18
    .local v0, "data":[B
    const/4 v1, 0x0

    int-to-byte v2, p0

    aput-byte v2, v0, v1

    .line 19
    const/4 v1, 0x1

    int-to-byte v2, p1

    aput-byte v2, v0, v1

    .line 21
    return-object v0
.end method


# virtual methods
.method public getDepth()I
    .locals 2

    .prologue
    .line 41
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/sig/TrustSignature;->data:[B

    const/4 v1, 0x0

    aget-byte v0, v0, v1

    and-int/lit16 v0, v0, 0xff

    return v0
.end method

.method public getTrustAmount()I
    .locals 2

    .prologue
    .line 46
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/sig/TrustSignature;->data:[B

    const/4 v1, 0x1

    aget-byte v0, v0, v1

    and-int/lit16 v0, v0, 0xff

    return v0
.end method
