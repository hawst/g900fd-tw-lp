.class public abstract Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedData;
.super Ljava/lang/Object;
.source "PGPEncryptedData.java"

# interfaces
.implements Lcom/android/sec/org/bouncycastle/bcpg/SymmetricKeyAlgorithmTags;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedData$TruncatedStream;
    }
.end annotation


# instance fields
.field encData:Lcom/android/sec/org/bouncycastle/bcpg/InputStreamPacket;

.field encStream:Ljava/io/InputStream;

.field truncStream:Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedData$TruncatedStream;


# direct methods
.method constructor <init>(Lcom/android/sec/org/bouncycastle/bcpg/InputStreamPacket;)V
    .locals 0
    .param p1, "encData"    # Lcom/android/sec/org/bouncycastle/bcpg/InputStreamPacket;

    .prologue
    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82
    iput-object p1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedData;->encData:Lcom/android/sec/org/bouncycastle/bcpg/InputStreamPacket;

    .line 83
    return-void
.end method


# virtual methods
.method public getInputStream()Ljava/io/InputStream;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedData;->encData:Lcom/android/sec/org/bouncycastle/bcpg/InputStreamPacket;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/bcpg/InputStreamPacket;->getInputStream()Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;

    move-result-object v0

    return-object v0
.end method

.method public isIntegrityProtected()Z
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedData;->encData:Lcom/android/sec/org/bouncycastle/bcpg/InputStreamPacket;

    instance-of v0, v0, Lcom/android/sec/org/bouncycastle/bcpg/SymmetricEncIntegrityPacket;

    return v0
.end method

.method public verify()Z
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 113
    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedData;->isIntegrityProtected()Z

    move-result v6

    if-nez v6, :cond_0

    .line 115
    new-instance v6, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;

    const-string v7, "data not integrity protected."

    invoke-direct {v6, v7}, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 118
    :cond_0
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedData;->encStream:Ljava/io/InputStream;

    check-cast v0, Ljava/security/DigestInputStream;

    .line 123
    .local v0, "dIn":Ljava/security/DigestInputStream;
    :cond_1
    iget-object v6, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedData;->encStream:Ljava/io/InputStream;

    invoke-virtual {v6}, Ljava/io/InputStream;->read()I

    move-result v6

    if-gez v6, :cond_1

    .line 128
    invoke-virtual {v0}, Ljava/security/DigestInputStream;->getMessageDigest()Ljava/security/MessageDigest;

    move-result-object v2

    .line 133
    .local v2, "hash":Ljava/security/MessageDigest;
    iget-object v6, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedData;->truncStream:Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedData$TruncatedStream;

    invoke-virtual {v6}, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedData$TruncatedStream;->getLookAhead()[I

    move-result-object v4

    .line 135
    .local v4, "lookAhead":[I
    const/4 v6, 0x0

    aget v6, v4, v6

    int-to-byte v6, v6

    invoke-virtual {v2, v6}, Ljava/security/MessageDigest;->update(B)V

    .line 136
    const/4 v6, 0x1

    aget v6, v4, v6

    int-to-byte v6, v6

    invoke-virtual {v2, v6}, Ljava/security/MessageDigest;->update(B)V

    .line 138
    invoke-virtual {v2}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v1

    .line 139
    .local v1, "digest":[B
    array-length v6, v1

    new-array v5, v6, [B

    .line 141
    .local v5, "streamDigest":[B
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    array-length v6, v5

    if-eq v3, v6, :cond_2

    .line 143
    add-int/lit8 v6, v3, 0x2

    aget v6, v4, v6

    int-to-byte v6, v6

    aput-byte v6, v5, v3

    .line 141
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 146
    :cond_2
    invoke-static {v1, v5}, Lcom/android/sec/org/bouncycastle/util/Arrays;->constantTimeAreEqual([B[B)Z

    move-result v6

    return v6
.end method
