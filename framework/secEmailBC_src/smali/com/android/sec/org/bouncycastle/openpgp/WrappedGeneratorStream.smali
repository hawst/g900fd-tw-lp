.class Lcom/android/sec/org/bouncycastle/openpgp/WrappedGeneratorStream;
.super Ljava/io/OutputStream;
.source "WrappedGeneratorStream.java"


# instance fields
.field private final _out:Ljava/io/OutputStream;

.field private final _sGen:Lcom/android/sec/org/bouncycastle/openpgp/StreamGenerator;


# direct methods
.method public constructor <init>(Ljava/io/OutputStream;Lcom/android/sec/org/bouncycastle/openpgp/StreamGenerator;)V
    .locals 0
    .param p1, "out"    # Ljava/io/OutputStream;
    .param p2, "sGen"    # Lcom/android/sec/org/bouncycastle/openpgp/StreamGenerator;

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/io/OutputStream;-><init>()V

    .line 14
    iput-object p1, p0, Lcom/android/sec/org/bouncycastle/openpgp/WrappedGeneratorStream;->_out:Ljava/io/OutputStream;

    .line 15
    iput-object p2, p0, Lcom/android/sec/org/bouncycastle/openpgp/WrappedGeneratorStream;->_sGen:Lcom/android/sec/org/bouncycastle/openpgp/StreamGenerator;

    .line 16
    return-void
.end method


# virtual methods
.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 44
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/WrappedGeneratorStream;->_sGen:Lcom/android/sec/org/bouncycastle/openpgp/StreamGenerator;

    invoke-interface {v0}, Lcom/android/sec/org/bouncycastle/openpgp/StreamGenerator;->close()V

    .line 45
    return-void
.end method

.method public flush()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 38
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/WrappedGeneratorStream;->_out:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->flush()V

    .line 39
    return-void
.end method

.method public write(I)V
    .locals 1
    .param p1, "b"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 32
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/WrappedGeneratorStream;->_out:Ljava/io/OutputStream;

    invoke-virtual {v0, p1}, Ljava/io/OutputStream;->write(I)V

    .line 33
    return-void
.end method

.method public write([B)V
    .locals 1
    .param p1, "bytes"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 20
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/WrappedGeneratorStream;->_out:Ljava/io/OutputStream;

    invoke-virtual {v0, p1}, Ljava/io/OutputStream;->write([B)V

    .line 21
    return-void
.end method

.method public write([BII)V
    .locals 1
    .param p1, "bytes"    # [B
    .param p2, "offset"    # I
    .param p3, "length"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 26
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/WrappedGeneratorStream;->_out:Ljava/io/OutputStream;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/OutputStream;->write([BII)V

    .line 27
    return-void
.end method
