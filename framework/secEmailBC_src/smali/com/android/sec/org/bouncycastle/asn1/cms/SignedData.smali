.class public Lcom/android/sec/org/bouncycastle/asn1/cms/SignedData;
.super Lcom/android/sec/org/bouncycastle/asn1/ASN1Object;
.source "SignedData.java"


# static fields
.field private static final VERSION_1:Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;

.field private static final VERSION_3:Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;

.field private static final VERSION_4:Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;

.field private static final VERSION_5:Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;


# instance fields
.field private certificates:Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;

.field private certsBer:Z

.field private contentInfo:Lcom/android/sec/org/bouncycastle/asn1/cms/ContentInfo;

.field private crls:Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;

.field private crlsBer:Z

.field private digestAlgorithms:Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;

.field private signerInfos:Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;

.field private version:Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 24
    new-instance v0, Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;

    const-wide/16 v2, 0x1

    invoke-direct {v0, v2, v3}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;-><init>(J)V

    sput-object v0, Lcom/android/sec/org/bouncycastle/asn1/cms/SignedData;->VERSION_1:Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;

    .line 25
    new-instance v0, Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;

    const-wide/16 v2, 0x3

    invoke-direct {v0, v2, v3}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;-><init>(J)V

    sput-object v0, Lcom/android/sec/org/bouncycastle/asn1/cms/SignedData;->VERSION_3:Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;

    .line 26
    new-instance v0, Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;

    const-wide/16 v2, 0x4

    invoke-direct {v0, v2, v3}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;-><init>(J)V

    sput-object v0, Lcom/android/sec/org/bouncycastle/asn1/cms/SignedData;->VERSION_4:Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;

    .line 27
    new-instance v0, Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;

    const-wide/16 v2, 0x5

    invoke-direct {v0, v2, v3}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;-><init>(J)V

    sput-object v0, Lcom/android/sec/org/bouncycastle/asn1/cms/SignedData;->VERSION_5:Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;

    return-void
.end method

.method private constructor <init>(Lcom/android/sec/org/bouncycastle/asn1/ASN1Sequence;)V
    .locals 6
    .param p1, "seq"    # Lcom/android/sec/org/bouncycastle/asn1/ASN1Sequence;

    .prologue
    const/4 v4, 0x0

    .line 187
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Object;-><init>()V

    .line 188
    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Sequence;->getObjects()Ljava/util/Enumeration;

    move-result-object v0

    .line 190
    .local v0, "e":Ljava/util/Enumeration;
    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v3

    invoke-static {v3}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;->getInstance(Ljava/lang/Object;)Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;

    move-result-object v3

    iput-object v3, p0, Lcom/android/sec/org/bouncycastle/asn1/cms/SignedData;->version:Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;

    .line 191
    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;

    iput-object v3, p0, Lcom/android/sec/org/bouncycastle/asn1/cms/SignedData;->digestAlgorithms:Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;

    .line 192
    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v3

    invoke-static {v3}, Lcom/android/sec/org/bouncycastle/asn1/cms/ContentInfo;->getInstance(Ljava/lang/Object;)Lcom/android/sec/org/bouncycastle/asn1/cms/ContentInfo;

    move-result-object v3

    iput-object v3, p0, Lcom/android/sec/org/bouncycastle/asn1/cms/SignedData;->contentInfo:Lcom/android/sec/org/bouncycastle/asn1/cms/ContentInfo;

    .line 194
    :goto_0
    invoke-interface {v0}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 196
    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/sec/org/bouncycastle/asn1/ASN1Primitive;

    .line 203
    .local v1, "o":Lcom/android/sec/org/bouncycastle/asn1/ASN1Primitive;
    instance-of v3, v1, Lcom/android/sec/org/bouncycastle/asn1/ASN1TaggedObject;

    if-eqz v3, :cond_0

    move-object v2, v1

    .line 205
    check-cast v2, Lcom/android/sec/org/bouncycastle/asn1/ASN1TaggedObject;

    .line 207
    .local v2, "tagged":Lcom/android/sec/org/bouncycastle/asn1/ASN1TaggedObject;
    invoke-virtual {v2}, Lcom/android/sec/org/bouncycastle/asn1/ASN1TaggedObject;->getTagNo()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 218
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "unknown tag value "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Lcom/android/sec/org/bouncycastle/asn1/ASN1TaggedObject;->getTagNo()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 210
    :pswitch_0
    instance-of v3, v2, Lcom/android/sec/org/bouncycastle/asn1/BERTaggedObject;

    iput-boolean v3, p0, Lcom/android/sec/org/bouncycastle/asn1/cms/SignedData;->certsBer:Z

    .line 211
    invoke-static {v2, v4}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;->getInstance(Lcom/android/sec/org/bouncycastle/asn1/ASN1TaggedObject;Z)Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;

    move-result-object v3

    iput-object v3, p0, Lcom/android/sec/org/bouncycastle/asn1/cms/SignedData;->certificates:Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;

    goto :goto_0

    .line 214
    :pswitch_1
    instance-of v3, v2, Lcom/android/sec/org/bouncycastle/asn1/BERTaggedObject;

    iput-boolean v3, p0, Lcom/android/sec/org/bouncycastle/asn1/cms/SignedData;->crlsBer:Z

    .line 215
    invoke-static {v2, v4}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;->getInstance(Lcom/android/sec/org/bouncycastle/asn1/ASN1TaggedObject;Z)Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;

    move-result-object v3

    iput-object v3, p0, Lcom/android/sec/org/bouncycastle/asn1/cms/SignedData;->crls:Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;

    goto :goto_0

    .line 223
    .end local v2    # "tagged":Lcom/android/sec/org/bouncycastle/asn1/ASN1TaggedObject;
    :cond_0
    check-cast v1, Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;

    .end local v1    # "o":Lcom/android/sec/org/bouncycastle/asn1/ASN1Primitive;
    iput-object v1, p0, Lcom/android/sec/org/bouncycastle/asn1/cms/SignedData;->signerInfos:Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;

    goto :goto_0

    .line 226
    :cond_1
    return-void

    .line 207
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public constructor <init>(Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;Lcom/android/sec/org/bouncycastle/asn1/cms/ContentInfo;Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;)V
    .locals 1
    .param p1, "digestAlgorithms"    # Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;
    .param p2, "contentInfo"    # Lcom/android/sec/org/bouncycastle/asn1/cms/ContentInfo;
    .param p3, "certificates"    # Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;
    .param p4, "crls"    # Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;
    .param p5, "signerInfos"    # Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Object;-><init>()V

    .line 60
    invoke-virtual {p2}, Lcom/android/sec/org/bouncycastle/asn1/cms/ContentInfo;->getContentType()Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    move-result-object v0

    invoke-direct {p0, v0, p3, p4, p5}, Lcom/android/sec/org/bouncycastle/asn1/cms/SignedData;->calculateVersion(Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;)Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/asn1/cms/SignedData;->version:Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;

    .line 61
    iput-object p1, p0, Lcom/android/sec/org/bouncycastle/asn1/cms/SignedData;->digestAlgorithms:Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;

    .line 62
    iput-object p2, p0, Lcom/android/sec/org/bouncycastle/asn1/cms/SignedData;->contentInfo:Lcom/android/sec/org/bouncycastle/asn1/cms/ContentInfo;

    .line 63
    iput-object p3, p0, Lcom/android/sec/org/bouncycastle/asn1/cms/SignedData;->certificates:Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;

    .line 64
    iput-object p4, p0, Lcom/android/sec/org/bouncycastle/asn1/cms/SignedData;->crls:Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;

    .line 65
    iput-object p5, p0, Lcom/android/sec/org/bouncycastle/asn1/cms/SignedData;->signerInfos:Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;

    .line 66
    instance-of v0, p4, Lcom/android/sec/org/bouncycastle/asn1/BERSet;

    iput-boolean v0, p0, Lcom/android/sec/org/bouncycastle/asn1/cms/SignedData;->crlsBer:Z

    .line 67
    instance-of v0, p3, Lcom/android/sec/org/bouncycastle/asn1/BERSet;

    iput-boolean v0, p0, Lcom/android/sec/org/bouncycastle/asn1/cms/SignedData;->certsBer:Z

    .line 68
    return-void
.end method

.method private calculateVersion(Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;)Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;
    .locals 10
    .param p1, "contentOid"    # Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;
    .param p2, "certs"    # Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;
    .param p3, "crls"    # Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;
    .param p4, "signerInfs"    # Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;

    .prologue
    .line 95
    const/4 v4, 0x0

    .line 96
    .local v4, "otherCert":Z
    const/4 v5, 0x0

    .line 97
    .local v5, "otherCrl":Z
    const/4 v0, 0x0

    .line 98
    .local v0, "attrCertV1Found":Z
    const/4 v1, 0x0

    .line 100
    .local v1, "attrCertV2Found":Z
    if-eqz p2, :cond_3

    .line 102
    invoke-virtual {p2}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;->getObjects()Ljava/util/Enumeration;

    move-result-object v2

    .local v2, "en":Ljava/util/Enumeration;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v7

    if-eqz v7, :cond_3

    .line 104
    invoke-interface {v2}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v3

    .line 105
    .local v3, "obj":Ljava/lang/Object;
    instance-of v7, v3, Lcom/android/sec/org/bouncycastle/asn1/ASN1TaggedObject;

    if-eqz v7, :cond_0

    .line 107
    invoke-static {v3}, Lcom/android/sec/org/bouncycastle/asn1/ASN1TaggedObject;->getInstance(Ljava/lang/Object;)Lcom/android/sec/org/bouncycastle/asn1/ASN1TaggedObject;

    move-result-object v6

    .line 109
    .local v6, "tagged":Lcom/android/sec/org/bouncycastle/asn1/ASN1TaggedObject;
    invoke-virtual {v6}, Lcom/android/sec/org/bouncycastle/asn1/ASN1TaggedObject;->getTagNo()I

    move-result v7

    const/4 v8, 0x1

    if-ne v7, v8, :cond_1

    .line 111
    const/4 v0, 0x1

    goto :goto_0

    .line 113
    :cond_1
    invoke-virtual {v6}, Lcom/android/sec/org/bouncycastle/asn1/ASN1TaggedObject;->getTagNo()I

    move-result v7

    const/4 v8, 0x2

    if-ne v7, v8, :cond_2

    .line 115
    const/4 v1, 0x1

    goto :goto_0

    .line 117
    :cond_2
    invoke-virtual {v6}, Lcom/android/sec/org/bouncycastle/asn1/ASN1TaggedObject;->getTagNo()I

    move-result v7

    const/4 v8, 0x3

    if-ne v7, v8, :cond_0

    .line 119
    const/4 v4, 0x1

    goto :goto_0

    .line 125
    .end local v2    # "en":Ljava/util/Enumeration;
    .end local v3    # "obj":Ljava/lang/Object;
    .end local v6    # "tagged":Lcom/android/sec/org/bouncycastle/asn1/ASN1TaggedObject;
    :cond_3
    if-eqz v4, :cond_4

    .line 127
    new-instance v7, Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;

    const-wide/16 v8, 0x5

    invoke-direct {v7, v8, v9}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;-><init>(J)V

    .line 167
    :goto_1
    return-object v7

    .line 130
    :cond_4
    if-eqz p3, :cond_6

    .line 132
    invoke-virtual {p3}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;->getObjects()Ljava/util/Enumeration;

    move-result-object v2

    .restart local v2    # "en":Ljava/util/Enumeration;
    :cond_5
    :goto_2
    invoke-interface {v2}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v7

    if-eqz v7, :cond_6

    .line 134
    invoke-interface {v2}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v3

    .line 135
    .restart local v3    # "obj":Ljava/lang/Object;
    instance-of v7, v3, Lcom/android/sec/org/bouncycastle/asn1/ASN1TaggedObject;

    if-eqz v7, :cond_5

    .line 137
    const/4 v5, 0x1

    goto :goto_2

    .line 142
    .end local v2    # "en":Ljava/util/Enumeration;
    .end local v3    # "obj":Ljava/lang/Object;
    :cond_6
    if-eqz v5, :cond_7

    .line 144
    sget-object v7, Lcom/android/sec/org/bouncycastle/asn1/cms/SignedData;->VERSION_5:Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;

    goto :goto_1

    .line 147
    :cond_7
    if-eqz v1, :cond_8

    .line 149
    sget-object v7, Lcom/android/sec/org/bouncycastle/asn1/cms/SignedData;->VERSION_4:Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;

    goto :goto_1

    .line 152
    :cond_8
    if-eqz v0, :cond_9

    .line 154
    sget-object v7, Lcom/android/sec/org/bouncycastle/asn1/cms/SignedData;->VERSION_3:Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;

    goto :goto_1

    .line 157
    :cond_9
    invoke-direct {p0, p4}, Lcom/android/sec/org/bouncycastle/asn1/cms/SignedData;->checkForVersion3(Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;)Z

    move-result v7

    if-eqz v7, :cond_a

    .line 159
    sget-object v7, Lcom/android/sec/org/bouncycastle/asn1/cms/SignedData;->VERSION_3:Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;

    goto :goto_1

    .line 162
    :cond_a
    sget-object v7, Lcom/android/sec/org/bouncycastle/asn1/cms/CMSObjectIdentifiers;->data:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    invoke-virtual {v7, p1}, Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_b

    .line 164
    sget-object v7, Lcom/android/sec/org/bouncycastle/asn1/cms/SignedData;->VERSION_3:Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;

    goto :goto_1

    .line 167
    :cond_b
    sget-object v7, Lcom/android/sec/org/bouncycastle/asn1/cms/SignedData;->VERSION_1:Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;

    goto :goto_1
.end method

.method private checkForVersion3(Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;)Z
    .locals 4
    .param p1, "signerInfs"    # Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;

    .prologue
    .line 172
    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;->getObjects()Ljava/util/Enumeration;

    move-result-object v0

    .local v0, "e":Ljava/util/Enumeration;
    :cond_0
    invoke-interface {v0}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 174
    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2}, Lcom/android/sec/org/bouncycastle/asn1/cms/SignerInfo;->getInstance(Ljava/lang/Object;)Lcom/android/sec/org/bouncycastle/asn1/cms/SignerInfo;

    move-result-object v1

    .line 176
    .local v1, "s":Lcom/android/sec/org/bouncycastle/asn1/cms/SignerInfo;
    invoke-virtual {v1}, Lcom/android/sec/org/bouncycastle/asn1/cms/SignerInfo;->getVersion()Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;->getValue()Ljava/math/BigInteger;

    move-result-object v2

    invoke-virtual {v2}, Ljava/math/BigInteger;->intValue()I

    move-result v2

    const/4 v3, 0x3

    if-ne v2, v3, :cond_0

    .line 178
    const/4 v2, 0x1

    .line 182
    .end local v1    # "s":Lcom/android/sec/org/bouncycastle/asn1/cms/SignerInfo;
    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static getInstance(Ljava/lang/Object;)Lcom/android/sec/org/bouncycastle/asn1/cms/SignedData;
    .locals 2
    .param p0, "o"    # Ljava/lang/Object;

    .prologue
    .line 41
    instance-of v0, p0, Lcom/android/sec/org/bouncycastle/asn1/cms/SignedData;

    if-eqz v0, :cond_0

    .line 43
    check-cast p0, Lcom/android/sec/org/bouncycastle/asn1/cms/SignedData;

    .line 50
    .end local p0    # "o":Ljava/lang/Object;
    :goto_0
    return-object p0

    .line 45
    .restart local p0    # "o":Ljava/lang/Object;
    :cond_0
    if-eqz p0, :cond_1

    .line 47
    new-instance v0, Lcom/android/sec/org/bouncycastle/asn1/cms/SignedData;

    invoke-static {p0}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Sequence;->getInstance(Ljava/lang/Object;)Lcom/android/sec/org/bouncycastle/asn1/ASN1Sequence;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/sec/org/bouncycastle/asn1/cms/SignedData;-><init>(Lcom/android/sec/org/bouncycastle/asn1/ASN1Sequence;)V

    move-object p0, v0

    goto :goto_0

    .line 50
    :cond_1
    const/4 p0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getCRLs()Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;
    .locals 1

    .prologue
    .line 250
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/asn1/cms/SignedData;->crls:Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;

    return-object v0
.end method

.method public getCertificates()Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;
    .locals 1

    .prologue
    .line 245
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/asn1/cms/SignedData;->certificates:Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;

    return-object v0
.end method

.method public getDigestAlgorithms()Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;
    .locals 1

    .prologue
    .line 235
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/asn1/cms/SignedData;->digestAlgorithms:Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;

    return-object v0
.end method

.method public getEncapContentInfo()Lcom/android/sec/org/bouncycastle/asn1/cms/ContentInfo;
    .locals 1

    .prologue
    .line 240
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/asn1/cms/SignedData;->contentInfo:Lcom/android/sec/org/bouncycastle/asn1/cms/ContentInfo;

    return-object v0
.end method

.method public getSignerInfos()Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;
    .locals 1

    .prologue
    .line 255
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/asn1/cms/SignedData;->signerInfos:Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;

    return-object v0
.end method

.method public getVersion()Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;
    .locals 1

    .prologue
    .line 230
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/asn1/cms/SignedData;->version:Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;

    return-object v0
.end method

.method public toASN1Primitive()Lcom/android/sec/org/bouncycastle/asn1/ASN1Primitive;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 273
    new-instance v0, Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;

    invoke-direct {v0}, Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;-><init>()V

    .line 275
    .local v0, "v":Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/asn1/cms/SignedData;->version:Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;

    invoke-virtual {v0, v1}, Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;->add(Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;)V

    .line 276
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/asn1/cms/SignedData;->digestAlgorithms:Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;

    invoke-virtual {v0, v1}, Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;->add(Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;)V

    .line 277
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/asn1/cms/SignedData;->contentInfo:Lcom/android/sec/org/bouncycastle/asn1/cms/ContentInfo;

    invoke-virtual {v0, v1}, Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;->add(Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;)V

    .line 279
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/asn1/cms/SignedData;->certificates:Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;

    if-eqz v1, :cond_0

    .line 281
    iget-boolean v1, p0, Lcom/android/sec/org/bouncycastle/asn1/cms/SignedData;->certsBer:Z

    if-eqz v1, :cond_2

    .line 283
    new-instance v1, Lcom/android/sec/org/bouncycastle/asn1/BERTaggedObject;

    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/asn1/cms/SignedData;->certificates:Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;

    invoke-direct {v1, v3, v3, v2}, Lcom/android/sec/org/bouncycastle/asn1/BERTaggedObject;-><init>(ZILcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;)V

    invoke-virtual {v0, v1}, Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;->add(Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;)V

    .line 291
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/asn1/cms/SignedData;->crls:Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;

    if-eqz v1, :cond_1

    .line 293
    iget-boolean v1, p0, Lcom/android/sec/org/bouncycastle/asn1/cms/SignedData;->crlsBer:Z

    if-eqz v1, :cond_3

    .line 295
    new-instance v1, Lcom/android/sec/org/bouncycastle/asn1/BERTaggedObject;

    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/asn1/cms/SignedData;->crls:Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;

    invoke-direct {v1, v3, v4, v2}, Lcom/android/sec/org/bouncycastle/asn1/BERTaggedObject;-><init>(ZILcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;)V

    invoke-virtual {v0, v1}, Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;->add(Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;)V

    .line 303
    :cond_1
    :goto_1
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/asn1/cms/SignedData;->signerInfos:Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;

    invoke-virtual {v0, v1}, Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;->add(Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;)V

    .line 305
    new-instance v1, Lcom/android/sec/org/bouncycastle/asn1/BERSequence;

    invoke-direct {v1, v0}, Lcom/android/sec/org/bouncycastle/asn1/BERSequence;-><init>(Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;)V

    return-object v1

    .line 287
    :cond_2
    new-instance v1, Lcom/android/sec/org/bouncycastle/asn1/DERTaggedObject;

    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/asn1/cms/SignedData;->certificates:Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;

    invoke-direct {v1, v3, v3, v2}, Lcom/android/sec/org/bouncycastle/asn1/DERTaggedObject;-><init>(ZILcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;)V

    invoke-virtual {v0, v1}, Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;->add(Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;)V

    goto :goto_0

    .line 299
    :cond_3
    new-instance v1, Lcom/android/sec/org/bouncycastle/asn1/DERTaggedObject;

    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/asn1/cms/SignedData;->crls:Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;

    invoke-direct {v1, v3, v4, v2}, Lcom/android/sec/org/bouncycastle/asn1/DERTaggedObject;-><init>(ZILcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;)V

    invoke-virtual {v0, v1}, Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;->add(Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;)V

    goto :goto_1
.end method
