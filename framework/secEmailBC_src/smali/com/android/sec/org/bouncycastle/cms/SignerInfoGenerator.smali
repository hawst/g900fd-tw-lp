.class public Lcom/android/sec/org/bouncycastle/cms/SignerInfoGenerator;
.super Ljava/lang/Object;
.source "SignerInfoGenerator.java"


# instance fields
.field private calculatedDigest:[B

.field private certHolder:Lcom/android/sec/org/bouncycastle/cert/X509CertificateHolder;

.field private final digAlgFinder:Lcom/android/sec/org/bouncycastle/operator/DigestAlgorithmIdentifierFinder;

.field private final digester:Lcom/android/sec/org/bouncycastle/operator/DigestCalculator;

.field private final sAttrGen:Lcom/android/sec/org/bouncycastle/cms/CMSAttributeTableGenerator;

.field private final sigEncAlgFinder:Lcom/android/sec/org/bouncycastle/cms/CMSSignatureEncryptionAlgorithmFinder;

.field private final signer:Lcom/android/sec/org/bouncycastle/operator/ContentSigner;

.field private final signerIdentifier:Lcom/android/sec/org/bouncycastle/asn1/cms/SignerIdentifier;

.field private final unsAttrGen:Lcom/android/sec/org/bouncycastle/cms/CMSAttributeTableGenerator;


# direct methods
.method constructor <init>(Lcom/android/sec/org/bouncycastle/asn1/cms/SignerIdentifier;Lcom/android/sec/org/bouncycastle/operator/ContentSigner;Lcom/android/sec/org/bouncycastle/operator/DigestCalculatorProvider;Lcom/android/sec/org/bouncycastle/cms/CMSSignatureEncryptionAlgorithmFinder;)V
    .locals 6
    .param p1, "signerIdentifier"    # Lcom/android/sec/org/bouncycastle/asn1/cms/SignerIdentifier;
    .param p2, "signer"    # Lcom/android/sec/org/bouncycastle/operator/ContentSigner;
    .param p3, "digesterProvider"    # Lcom/android/sec/org/bouncycastle/operator/DigestCalculatorProvider;
    .param p4, "sigEncAlgFinder"    # Lcom/android/sec/org/bouncycastle/cms/CMSSignatureEncryptionAlgorithmFinder;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/operator/OperatorCreationException;
        }
    .end annotation

    .prologue
    .line 48
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/android/sec/org/bouncycastle/cms/SignerInfoGenerator;-><init>(Lcom/android/sec/org/bouncycastle/asn1/cms/SignerIdentifier;Lcom/android/sec/org/bouncycastle/operator/ContentSigner;Lcom/android/sec/org/bouncycastle/operator/DigestCalculatorProvider;Lcom/android/sec/org/bouncycastle/cms/CMSSignatureEncryptionAlgorithmFinder;Z)V

    .line 49
    return-void
.end method

.method constructor <init>(Lcom/android/sec/org/bouncycastle/asn1/cms/SignerIdentifier;Lcom/android/sec/org/bouncycastle/operator/ContentSigner;Lcom/android/sec/org/bouncycastle/operator/DigestCalculatorProvider;Lcom/android/sec/org/bouncycastle/cms/CMSSignatureEncryptionAlgorithmFinder;Lcom/android/sec/org/bouncycastle/cms/CMSAttributeTableGenerator;Lcom/android/sec/org/bouncycastle/cms/CMSAttributeTableGenerator;)V
    .locals 2
    .param p1, "signerIdentifier"    # Lcom/android/sec/org/bouncycastle/asn1/cms/SignerIdentifier;
    .param p2, "signer"    # Lcom/android/sec/org/bouncycastle/operator/ContentSigner;
    .param p3, "digesterProvider"    # Lcom/android/sec/org/bouncycastle/operator/DigestCalculatorProvider;
    .param p4, "sigEncAlgFinder"    # Lcom/android/sec/org/bouncycastle/cms/CMSSignatureEncryptionAlgorithmFinder;
    .param p5, "sAttrGen"    # Lcom/android/sec/org/bouncycastle/cms/CMSAttributeTableGenerator;
    .param p6, "unsAttrGen"    # Lcom/android/sec/org/bouncycastle/cms/CMSAttributeTableGenerator;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/operator/OperatorCreationException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 106
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    new-instance v0, Lcom/android/sec/org/bouncycastle/operator/DefaultDigestAlgorithmIdentifierFinder;

    invoke-direct {v0}, Lcom/android/sec/org/bouncycastle/operator/DefaultDigestAlgorithmIdentifierFinder;-><init>()V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInfoGenerator;->digAlgFinder:Lcom/android/sec/org/bouncycastle/operator/DigestAlgorithmIdentifierFinder;

    .line 38
    iput-object v1, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInfoGenerator;->calculatedDigest:[B

    .line 107
    iput-object p1, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInfoGenerator;->signerIdentifier:Lcom/android/sec/org/bouncycastle/asn1/cms/SignerIdentifier;

    .line 108
    iput-object p2, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInfoGenerator;->signer:Lcom/android/sec/org/bouncycastle/operator/ContentSigner;

    .line 110
    if-eqz p3, :cond_0

    .line 112
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInfoGenerator;->digAlgFinder:Lcom/android/sec/org/bouncycastle/operator/DigestAlgorithmIdentifierFinder;

    invoke-interface {p2}, Lcom/android/sec/org/bouncycastle/operator/ContentSigner;->getAlgorithmIdentifier()Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/android/sec/org/bouncycastle/operator/DigestAlgorithmIdentifierFinder;->find(Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;)Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;

    move-result-object v0

    invoke-interface {p3, v0}, Lcom/android/sec/org/bouncycastle/operator/DigestCalculatorProvider;->get(Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;)Lcom/android/sec/org/bouncycastle/operator/DigestCalculator;

    move-result-object v0

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInfoGenerator;->digester:Lcom/android/sec/org/bouncycastle/operator/DigestCalculator;

    .line 119
    :goto_0
    iput-object p5, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInfoGenerator;->sAttrGen:Lcom/android/sec/org/bouncycastle/cms/CMSAttributeTableGenerator;

    .line 120
    iput-object p6, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInfoGenerator;->unsAttrGen:Lcom/android/sec/org/bouncycastle/cms/CMSAttributeTableGenerator;

    .line 121
    iput-object p4, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInfoGenerator;->sigEncAlgFinder:Lcom/android/sec/org/bouncycastle/cms/CMSSignatureEncryptionAlgorithmFinder;

    .line 122
    return-void

    .line 116
    :cond_0
    iput-object v1, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInfoGenerator;->digester:Lcom/android/sec/org/bouncycastle/operator/DigestCalculator;

    goto :goto_0
.end method

.method constructor <init>(Lcom/android/sec/org/bouncycastle/asn1/cms/SignerIdentifier;Lcom/android/sec/org/bouncycastle/operator/ContentSigner;Lcom/android/sec/org/bouncycastle/operator/DigestCalculatorProvider;Lcom/android/sec/org/bouncycastle/cms/CMSSignatureEncryptionAlgorithmFinder;Z)V
    .locals 3
    .param p1, "signerIdentifier"    # Lcom/android/sec/org/bouncycastle/asn1/cms/SignerIdentifier;
    .param p2, "signer"    # Lcom/android/sec/org/bouncycastle/operator/ContentSigner;
    .param p3, "digesterProvider"    # Lcom/android/sec/org/bouncycastle/operator/DigestCalculatorProvider;
    .param p4, "sigEncAlgFinder"    # Lcom/android/sec/org/bouncycastle/cms/CMSSignatureEncryptionAlgorithmFinder;
    .param p5, "isDirectSignature"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/operator/OperatorCreationException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    new-instance v0, Lcom/android/sec/org/bouncycastle/operator/DefaultDigestAlgorithmIdentifierFinder;

    invoke-direct {v0}, Lcom/android/sec/org/bouncycastle/operator/DefaultDigestAlgorithmIdentifierFinder;-><init>()V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInfoGenerator;->digAlgFinder:Lcom/android/sec/org/bouncycastle/operator/DigestAlgorithmIdentifierFinder;

    .line 38
    iput-object v2, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInfoGenerator;->calculatedDigest:[B

    .line 59
    iput-object p1, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInfoGenerator;->signerIdentifier:Lcom/android/sec/org/bouncycastle/asn1/cms/SignerIdentifier;

    .line 60
    iput-object p2, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInfoGenerator;->signer:Lcom/android/sec/org/bouncycastle/operator/ContentSigner;

    .line 62
    if-eqz p3, :cond_0

    .line 64
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInfoGenerator;->digAlgFinder:Lcom/android/sec/org/bouncycastle/operator/DigestAlgorithmIdentifierFinder;

    invoke-interface {p2}, Lcom/android/sec/org/bouncycastle/operator/ContentSigner;->getAlgorithmIdentifier()Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/android/sec/org/bouncycastle/operator/DigestAlgorithmIdentifierFinder;->find(Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;)Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;

    move-result-object v0

    invoke-interface {p3, v0}, Lcom/android/sec/org/bouncycastle/operator/DigestCalculatorProvider;->get(Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;)Lcom/android/sec/org/bouncycastle/operator/DigestCalculator;

    move-result-object v0

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInfoGenerator;->digester:Lcom/android/sec/org/bouncycastle/operator/DigestCalculator;

    .line 71
    :goto_0
    if-eqz p5, :cond_1

    .line 73
    iput-object v2, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInfoGenerator;->sAttrGen:Lcom/android/sec/org/bouncycastle/cms/CMSAttributeTableGenerator;

    .line 74
    iput-object v2, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInfoGenerator;->unsAttrGen:Lcom/android/sec/org/bouncycastle/cms/CMSAttributeTableGenerator;

    .line 82
    :goto_1
    iput-object p4, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInfoGenerator;->sigEncAlgFinder:Lcom/android/sec/org/bouncycastle/cms/CMSSignatureEncryptionAlgorithmFinder;

    .line 83
    return-void

    .line 68
    :cond_0
    iput-object v2, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInfoGenerator;->digester:Lcom/android/sec/org/bouncycastle/operator/DigestCalculator;

    goto :goto_0

    .line 78
    :cond_1
    new-instance v0, Lcom/android/sec/org/bouncycastle/cms/DefaultSignedAttributeTableGenerator;

    invoke-direct {v0}, Lcom/android/sec/org/bouncycastle/cms/DefaultSignedAttributeTableGenerator;-><init>()V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInfoGenerator;->sAttrGen:Lcom/android/sec/org/bouncycastle/cms/CMSAttributeTableGenerator;

    .line 79
    iput-object v2, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInfoGenerator;->unsAttrGen:Lcom/android/sec/org/bouncycastle/cms/CMSAttributeTableGenerator;

    goto :goto_1
.end method

.method public constructor <init>(Lcom/android/sec/org/bouncycastle/cms/SignerInfoGenerator;Lcom/android/sec/org/bouncycastle/cms/CMSAttributeTableGenerator;Lcom/android/sec/org/bouncycastle/cms/CMSAttributeTableGenerator;)V
    .locals 1
    .param p1, "original"    # Lcom/android/sec/org/bouncycastle/cms/SignerInfoGenerator;
    .param p2, "sAttrGen"    # Lcom/android/sec/org/bouncycastle/cms/CMSAttributeTableGenerator;
    .param p3, "unsAttrGen"    # Lcom/android/sec/org/bouncycastle/cms/CMSAttributeTableGenerator;

    .prologue
    .line 89
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    new-instance v0, Lcom/android/sec/org/bouncycastle/operator/DefaultDigestAlgorithmIdentifierFinder;

    invoke-direct {v0}, Lcom/android/sec/org/bouncycastle/operator/DefaultDigestAlgorithmIdentifierFinder;-><init>()V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInfoGenerator;->digAlgFinder:Lcom/android/sec/org/bouncycastle/operator/DigestAlgorithmIdentifierFinder;

    .line 38
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInfoGenerator;->calculatedDigest:[B

    .line 90
    iget-object v0, p1, Lcom/android/sec/org/bouncycastle/cms/SignerInfoGenerator;->signerIdentifier:Lcom/android/sec/org/bouncycastle/asn1/cms/SignerIdentifier;

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInfoGenerator;->signerIdentifier:Lcom/android/sec/org/bouncycastle/asn1/cms/SignerIdentifier;

    .line 91
    iget-object v0, p1, Lcom/android/sec/org/bouncycastle/cms/SignerInfoGenerator;->signer:Lcom/android/sec/org/bouncycastle/operator/ContentSigner;

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInfoGenerator;->signer:Lcom/android/sec/org/bouncycastle/operator/ContentSigner;

    .line 92
    iget-object v0, p1, Lcom/android/sec/org/bouncycastle/cms/SignerInfoGenerator;->digester:Lcom/android/sec/org/bouncycastle/operator/DigestCalculator;

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInfoGenerator;->digester:Lcom/android/sec/org/bouncycastle/operator/DigestCalculator;

    .line 93
    iget-object v0, p1, Lcom/android/sec/org/bouncycastle/cms/SignerInfoGenerator;->sigEncAlgFinder:Lcom/android/sec/org/bouncycastle/cms/CMSSignatureEncryptionAlgorithmFinder;

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInfoGenerator;->sigEncAlgFinder:Lcom/android/sec/org/bouncycastle/cms/CMSSignatureEncryptionAlgorithmFinder;

    .line 94
    iput-object p2, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInfoGenerator;->sAttrGen:Lcom/android/sec/org/bouncycastle/cms/CMSAttributeTableGenerator;

    .line 95
    iput-object p3, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInfoGenerator;->unsAttrGen:Lcom/android/sec/org/bouncycastle/cms/CMSAttributeTableGenerator;

    .line 96
    return-void
.end method

.method private getAttributeSet(Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;)Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;
    .locals 2
    .param p1, "attr"    # Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;

    .prologue
    .line 250
    if-eqz p1, :cond_0

    .line 252
    new-instance v0, Lcom/android/sec/org/bouncycastle/asn1/DERSet;

    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;->toASN1EncodableVector()Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/sec/org/bouncycastle/asn1/DERSet;-><init>(Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;)V

    .line 255
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getBaseParameters(Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;[B)Ljava/util/Map;
    .locals 3
    .param p1, "contentType"    # Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;
    .param p2, "digAlgId"    # Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;
    .param p3, "hash"    # [B

    .prologue
    .line 260
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 262
    .local v0, "param":Ljava/util/Map;
    if-eqz p1, :cond_0

    .line 264
    const-string v1, "contentType"

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 267
    :cond_0
    const-string v1, "digestAlgID"

    invoke-interface {v0, v1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 268
    const-string v1, "digest"

    invoke-virtual {p3}, [B->clone()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 269
    return-object v0
.end method


# virtual methods
.method public generate(Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;)Lcom/android/sec/org/bouncycastle/asn1/cms/SignerInfo;
    .locals 13
    .param p1, "contentType"    # Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/cms/CMSException;
        }
    .end annotation

    .prologue
    .line 184
    const/4 v3, 0x0

    .line 186
    .local v3, "signedAttr":Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;
    const/4 v2, 0x0

    .line 188
    .local v2, "digestAlg":Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;
    :try_start_0
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInfoGenerator;->sAttrGen:Lcom/android/sec/org/bouncycastle/cms/CMSAttributeTableGenerator;

    if-eqz v0, :cond_1

    .line 190
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInfoGenerator;->digester:Lcom/android/sec/org/bouncycastle/operator/DigestCalculator;

    invoke-interface {v0}, Lcom/android/sec/org/bouncycastle/operator/DigestCalculator;->getAlgorithmIdentifier()Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;

    move-result-object v2

    .line 191
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInfoGenerator;->digester:Lcom/android/sec/org/bouncycastle/operator/DigestCalculator;

    invoke-interface {v0}, Lcom/android/sec/org/bouncycastle/operator/DigestCalculator;->getDigest()[B

    move-result-object v0

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInfoGenerator;->calculatedDigest:[B

    .line 192
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInfoGenerator;->digester:Lcom/android/sec/org/bouncycastle/operator/DigestCalculator;

    invoke-interface {v0}, Lcom/android/sec/org/bouncycastle/operator/DigestCalculator;->getAlgorithmIdentifier()Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;

    move-result-object v0

    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInfoGenerator;->calculatedDigest:[B

    invoke-direct {p0, p1, v0, v1}, Lcom/android/sec/org/bouncycastle/cms/SignerInfoGenerator;->getBaseParameters(Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;[B)Ljava/util/Map;

    move-result-object v8

    .line 193
    .local v8, "parameters":Ljava/util/Map;
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInfoGenerator;->sAttrGen:Lcom/android/sec/org/bouncycastle/cms/CMSAttributeTableGenerator;

    invoke-static {v8}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/android/sec/org/bouncycastle/cms/CMSAttributeTableGenerator;->getAttributes(Ljava/util/Map;)Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;

    move-result-object v11

    .line 195
    .local v11, "signed":Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;
    invoke-direct {p0, v11}, Lcom/android/sec/org/bouncycastle/cms/SignerInfoGenerator;->getAttributeSet(Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;)Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;

    move-result-object v3

    .line 198
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInfoGenerator;->signer:Lcom/android/sec/org/bouncycastle/operator/ContentSigner;

    invoke-interface {v0}, Lcom/android/sec/org/bouncycastle/operator/ContentSigner;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v9

    .line 200
    .local v9, "sOut":Ljava/io/OutputStream;
    const-string v0, "DER"

    invoke-virtual {v3, v0}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;->getEncoded(Ljava/lang/String;)[B

    move-result-object v0

    invoke-virtual {v9, v0}, Ljava/io/OutputStream;->write([B)V

    .line 202
    invoke-virtual {v9}, Ljava/io/OutputStream;->close()V

    .line 218
    .end local v8    # "parameters":Ljava/util/Map;
    .end local v9    # "sOut":Ljava/io/OutputStream;
    .end local v11    # "signed":Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;
    :goto_0
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInfoGenerator;->signer:Lcom/android/sec/org/bouncycastle/operator/ContentSigner;

    invoke-interface {v0}, Lcom/android/sec/org/bouncycastle/operator/ContentSigner;->getSignature()[B

    move-result-object v10

    .line 220
    .local v10, "sigBytes":[B
    const/4 v6, 0x0

    .line 221
    .local v6, "unsignedAttr":Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInfoGenerator;->unsAttrGen:Lcom/android/sec/org/bouncycastle/cms/CMSAttributeTableGenerator;

    if-eqz v0, :cond_0

    .line 223
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInfoGenerator;->calculatedDigest:[B

    invoke-direct {p0, p1, v2, v0}, Lcom/android/sec/org/bouncycastle/cms/SignerInfoGenerator;->getBaseParameters(Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;[B)Ljava/util/Map;

    move-result-object v8

    .line 224
    .restart local v8    # "parameters":Ljava/util/Map;
    const-string v0, "encryptedDigest"

    invoke-virtual {v10}, [B->clone()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v8, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 226
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInfoGenerator;->unsAttrGen:Lcom/android/sec/org/bouncycastle/cms/CMSAttributeTableGenerator;

    invoke-static {v8}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/android/sec/org/bouncycastle/cms/CMSAttributeTableGenerator;->getAttributes(Ljava/util/Map;)Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;

    move-result-object v12

    .line 228
    .local v12, "unsigned":Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;
    invoke-direct {p0, v12}, Lcom/android/sec/org/bouncycastle/cms/SignerInfoGenerator;->getAttributeSet(Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;)Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;

    move-result-object v6

    .line 231
    .end local v8    # "parameters":Ljava/util/Map;
    .end local v12    # "unsigned":Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;
    :cond_0
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInfoGenerator;->sigEncAlgFinder:Lcom/android/sec/org/bouncycastle/cms/CMSSignatureEncryptionAlgorithmFinder;

    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInfoGenerator;->signer:Lcom/android/sec/org/bouncycastle/operator/ContentSigner;

    invoke-interface {v1}, Lcom/android/sec/org/bouncycastle/operator/ContentSigner;->getAlgorithmIdentifier()Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/android/sec/org/bouncycastle/cms/CMSSignatureEncryptionAlgorithmFinder;->findEncryptionAlgorithm(Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;)Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;

    move-result-object v4

    .line 233
    .local v4, "digestEncryptionAlgorithm":Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;
    new-instance v0, Lcom/android/sec/org/bouncycastle/asn1/cms/SignerInfo;

    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInfoGenerator;->signerIdentifier:Lcom/android/sec/org/bouncycastle/asn1/cms/SignerIdentifier;

    new-instance v5, Lcom/android/sec/org/bouncycastle/asn1/DEROctetString;

    invoke-direct {v5, v10}, Lcom/android/sec/org/bouncycastle/asn1/DEROctetString;-><init>([B)V

    invoke-direct/range {v0 .. v6}, Lcom/android/sec/org/bouncycastle/asn1/cms/SignerInfo;-><init>(Lcom/android/sec/org/bouncycastle/asn1/cms/SignerIdentifier;Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;Lcom/android/sec/org/bouncycastle/asn1/ASN1OctetString;Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;)V

    return-object v0

    .line 206
    .end local v4    # "digestEncryptionAlgorithm":Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;
    .end local v6    # "unsignedAttr":Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;
    .end local v10    # "sigBytes":[B
    :cond_1
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInfoGenerator;->digester:Lcom/android/sec/org/bouncycastle/operator/DigestCalculator;

    if-eqz v0, :cond_2

    .line 208
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInfoGenerator;->digester:Lcom/android/sec/org/bouncycastle/operator/DigestCalculator;

    invoke-interface {v0}, Lcom/android/sec/org/bouncycastle/operator/DigestCalculator;->getAlgorithmIdentifier()Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;

    move-result-object v2

    .line 209
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInfoGenerator;->digester:Lcom/android/sec/org/bouncycastle/operator/DigestCalculator;

    invoke-interface {v0}, Lcom/android/sec/org/bouncycastle/operator/DigestCalculator;->getDigest()[B

    move-result-object v0

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInfoGenerator;->calculatedDigest:[B
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 236
    :catch_0
    move-exception v7

    .line 238
    .local v7, "e":Ljava/io/IOException;
    new-instance v0, Lcom/android/sec/org/bouncycastle/cms/CMSException;

    const-string v1, "encoding error."

    invoke-direct {v0, v1, v7}, Lcom/android/sec/org/bouncycastle/cms/CMSException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v0

    .line 213
    .end local v7    # "e":Ljava/io/IOException;
    :cond_2
    :try_start_1
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInfoGenerator;->digAlgFinder:Lcom/android/sec/org/bouncycastle/operator/DigestAlgorithmIdentifierFinder;

    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInfoGenerator;->signer:Lcom/android/sec/org/bouncycastle/operator/ContentSigner;

    invoke-interface {v1}, Lcom/android/sec/org/bouncycastle/operator/ContentSigner;->getAlgorithmIdentifier()Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/android/sec/org/bouncycastle/operator/DigestAlgorithmIdentifierFinder;->find(Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;)Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;

    move-result-object v2

    .line 214
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInfoGenerator;->calculatedDigest:[B
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method public getAssociatedCertificate()Lcom/android/sec/org/bouncycastle/cert/X509CertificateHolder;
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInfoGenerator;->certHolder:Lcom/android/sec/org/bouncycastle/cert/X509CertificateHolder;

    return-object v0
.end method

.method public getCalculatedDigest()[B
    .locals 1

    .prologue
    .line 274
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInfoGenerator;->calculatedDigest:[B

    if-eqz v0, :cond_0

    .line 276
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInfoGenerator;->calculatedDigest:[B

    invoke-virtual {v0}, [B->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    check-cast v0, [B

    .line 279
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getCalculatingOutputStream()Ljava/io/OutputStream;
    .locals 3

    .prologue
    .line 156
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInfoGenerator;->digester:Lcom/android/sec/org/bouncycastle/operator/DigestCalculator;

    if-eqz v0, :cond_1

    .line 158
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInfoGenerator;->sAttrGen:Lcom/android/sec/org/bouncycastle/cms/CMSAttributeTableGenerator;

    if-nez v0, :cond_0

    .line 160
    new-instance v0, Lcom/android/sec/org/bouncycastle/util/io/TeeOutputStream;

    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInfoGenerator;->digester:Lcom/android/sec/org/bouncycastle/operator/DigestCalculator;

    invoke-interface {v1}, Lcom/android/sec/org/bouncycastle/operator/DigestCalculator;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v1

    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInfoGenerator;->signer:Lcom/android/sec/org/bouncycastle/operator/ContentSigner;

    invoke-interface {v2}, Lcom/android/sec/org/bouncycastle/operator/ContentSigner;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/android/sec/org/bouncycastle/util/io/TeeOutputStream;-><init>(Ljava/io/OutputStream;Ljava/io/OutputStream;)V

    .line 166
    :goto_0
    return-object v0

    .line 162
    :cond_0
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInfoGenerator;->digester:Lcom/android/sec/org/bouncycastle/operator/DigestCalculator;

    invoke-interface {v0}, Lcom/android/sec/org/bouncycastle/operator/DigestCalculator;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v0

    goto :goto_0

    .line 166
    :cond_1
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInfoGenerator;->signer:Lcom/android/sec/org/bouncycastle/operator/ContentSigner;

    invoke-interface {v0}, Lcom/android/sec/org/bouncycastle/operator/ContentSigner;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v0

    goto :goto_0
.end method

.method public getDigestAlgorithm()Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;
    .locals 2

    .prologue
    .line 146
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInfoGenerator;->digester:Lcom/android/sec/org/bouncycastle/operator/DigestCalculator;

    if-eqz v0, :cond_0

    .line 148
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInfoGenerator;->digester:Lcom/android/sec/org/bouncycastle/operator/DigestCalculator;

    invoke-interface {v0}, Lcom/android/sec/org/bouncycastle/operator/DigestCalculator;->getAlgorithmIdentifier()Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;

    move-result-object v0

    .line 151
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInfoGenerator;->digAlgFinder:Lcom/android/sec/org/bouncycastle/operator/DigestAlgorithmIdentifierFinder;

    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInfoGenerator;->signer:Lcom/android/sec/org/bouncycastle/operator/ContentSigner;

    invoke-interface {v1}, Lcom/android/sec/org/bouncycastle/operator/ContentSigner;->getAlgorithmIdentifier()Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/android/sec/org/bouncycastle/operator/DigestAlgorithmIdentifierFinder;->find(Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;)Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;

    move-result-object v0

    goto :goto_0
.end method

.method public getGeneratedVersion()Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;
    .locals 3

    .prologue
    .line 131
    new-instance v2, Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;

    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInfoGenerator;->signerIdentifier:Lcom/android/sec/org/bouncycastle/asn1/cms/SignerIdentifier;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/cms/SignerIdentifier;->isTagged()Z

    move-result v0

    if-eqz v0, :cond_0

    const-wide/16 v0, 0x3

    :goto_0
    invoke-direct {v2, v0, v1}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;-><init>(J)V

    return-object v2

    :cond_0
    const-wide/16 v0, 0x1

    goto :goto_0
.end method

.method public getSID()Lcom/android/sec/org/bouncycastle/asn1/cms/SignerIdentifier;
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInfoGenerator;->signerIdentifier:Lcom/android/sec/org/bouncycastle/asn1/cms/SignerIdentifier;

    return-object v0
.end method

.method public getSignedAttributeTableGenerator()Lcom/android/sec/org/bouncycastle/cms/CMSAttributeTableGenerator;
    .locals 1

    .prologue
    .line 284
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInfoGenerator;->sAttrGen:Lcom/android/sec/org/bouncycastle/cms/CMSAttributeTableGenerator;

    return-object v0
.end method

.method public getUnsignedAttributeTableGenerator()Lcom/android/sec/org/bouncycastle/cms/CMSAttributeTableGenerator;
    .locals 1

    .prologue
    .line 289
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInfoGenerator;->unsAttrGen:Lcom/android/sec/org/bouncycastle/cms/CMSAttributeTableGenerator;

    return-object v0
.end method

.method public hasAssociatedCertificate()Z
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInfoGenerator;->certHolder:Lcom/android/sec/org/bouncycastle/cert/X509CertificateHolder;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method setAssociatedCertificate(Lcom/android/sec/org/bouncycastle/cert/X509CertificateHolder;)V
    .locals 0
    .param p1, "certHolder"    # Lcom/android/sec/org/bouncycastle/cert/X509CertificateHolder;

    .prologue
    .line 244
    iput-object p1, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInfoGenerator;->certHolder:Lcom/android/sec/org/bouncycastle/cert/X509CertificateHolder;

    .line 245
    return-void
.end method
