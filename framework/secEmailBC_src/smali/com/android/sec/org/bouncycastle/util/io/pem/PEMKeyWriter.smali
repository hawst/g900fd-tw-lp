.class public Lcom/android/sec/org/bouncycastle/util/io/pem/PEMKeyWriter;
.super Ljava/io/BufferedWriter;
.source "PEMKeyWriter.java"


# instance fields
.field private provider:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/io/Writer;)V
    .locals 1
    .param p1, "out"    # Ljava/io/Writer;

    .prologue
    .line 50
    const-string v0, "AndroidOpenSSL"

    invoke-direct {p0, p1, v0}, Lcom/android/sec/org/bouncycastle/util/io/pem/PEMKeyWriter;-><init>(Ljava/io/Writer;Ljava/lang/String;)V

    .line 51
    return-void
.end method

.method public constructor <init>(Ljava/io/Writer;Ljava/lang/String;)V
    .locals 0
    .param p1, "out"    # Ljava/io/Writer;
    .param p2, "provider"    # Ljava/lang/String;

    .prologue
    .line 54
    invoke-direct {p0, p1}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V

    .line 56
    iput-object p2, p0, Lcom/android/sec/org/bouncycastle/util/io/pem/PEMKeyWriter;->provider:Ljava/lang/String;

    .line 57
    return-void
.end method

.method private writeEncoded([B)V
    .locals 5
    .param p1, "bytes"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 68
    const/16 v3, 0x40

    new-array v0, v3, [C

    .line 70
    .local v0, "buf":[C
    invoke-static {p1}, Lcom/android/sec/org/bouncycastle/util/encoders/Base64;->encode([B)[B

    move-result-object p1

    .line 72
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v3, p1

    if-ge v1, v3, :cond_2

    .line 73
    const/4 v2, 0x0

    .line 75
    .local v2, "index":I
    :goto_1
    array-length v3, v0

    if-eq v2, v3, :cond_0

    .line 76
    add-int v3, v1, v2

    array-length v4, p1

    if-lt v3, v4, :cond_1

    .line 82
    :cond_0
    const/4 v3, 0x0

    invoke-virtual {p0, v0, v3, v2}, Lcom/android/sec/org/bouncycastle/util/io/pem/PEMKeyWriter;->write([CII)V

    .line 83
    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/util/io/pem/PEMKeyWriter;->newLine()V

    .line 72
    array-length v3, v0

    add-int/2addr v1, v3

    goto :goto_0

    .line 79
    :cond_1
    add-int v3, v1, v2

    aget-byte v3, p1, v3

    int-to-char v3, v3

    aput-char v3, v0, v2

    .line 80
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 85
    .end local v2    # "index":I
    :cond_2
    return-void
.end method

.method private writeFooter(Ljava/lang/String;)V
    .locals 2
    .param p1, "type"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 254
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "-----END "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "-----"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/sec/org/bouncycastle/util/io/pem/PEMKeyWriter;->write(Ljava/lang/String;)V

    .line 255
    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/util/io/pem/PEMKeyWriter;->newLine()V

    .line 256
    return-void
.end method

.method private writeHeader(Ljava/lang/String;)V
    .locals 2
    .param p1, "type"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 249
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "-----BEGIN "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "-----"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/sec/org/bouncycastle/util/io/pem/PEMKeyWriter;->write(Ljava/lang/String;)V

    .line 250
    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/util/io/pem/PEMKeyWriter;->newLine()V

    .line 251
    return-void
.end method

.method private writeHexEncoded([B)V
    .locals 2
    .param p1, "bytes"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 60
    invoke-static {p1}, Lcom/android/sec/org/bouncycastle/util/encoders/Hex;->encode([B)[B

    move-result-object p1

    .line 62
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v1, p1

    if-eq v0, v1, :cond_0

    .line 63
    aget-byte v1, p1, v0

    int-to-char v1, v1

    invoke-virtual {p0, v1}, Lcom/android/sec/org/bouncycastle/util/io/pem/PEMKeyWriter;->write(I)V

    .line 62
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 65
    :cond_0
    return-void
.end method


# virtual methods
.method public writeObject(Ljava/lang/Object;)V
    .locals 14
    .param p1, "o"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 91
    instance-of v10, p1, Ljava/security/cert/X509Certificate;

    if-eqz v10, :cond_0

    .line 92
    const-string v6, "CERTIFICATE"

    .line 94
    .local v6, "type":Ljava/lang/String;
    :try_start_0
    check-cast p1, Ljava/security/cert/X509Certificate;

    .end local p1    # "o":Ljava/lang/Object;
    invoke-virtual {p1}, Ljava/security/cert/X509Certificate;->getEncoded()[B
    :try_end_0
    .catch Ljava/security/cert/CertificateEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 159
    .local v3, "encoding":[B
    :goto_0
    invoke-direct {p0, v6}, Lcom/android/sec/org/bouncycastle/util/io/pem/PEMKeyWriter;->writeHeader(Ljava/lang/String;)V

    .line 160
    invoke-direct {p0, v3}, Lcom/android/sec/org/bouncycastle/util/io/pem/PEMKeyWriter;->writeEncoded([B)V

    .line 161
    invoke-direct {p0, v6}, Lcom/android/sec/org/bouncycastle/util/io/pem/PEMKeyWriter;->writeFooter(Ljava/lang/String;)V

    .line 162
    .end local v3    # "encoding":[B
    .end local v6    # "type":Ljava/lang/String;
    :goto_1
    return-void

    .line 95
    .restart local v6    # "type":Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 96
    .local v2, "e":Ljava/security/cert/CertificateEncodingException;
    new-instance v10, Ljava/io/IOException;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Cannot encode object: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v2}, Ljava/security/cert/CertificateEncodingException;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v10

    .line 98
    .end local v2    # "e":Ljava/security/cert/CertificateEncodingException;
    .end local v6    # "type":Ljava/lang/String;
    .restart local p1    # "o":Ljava/lang/Object;
    :cond_0
    instance-of v10, p1, Ljava/security/cert/X509CRL;

    if-eqz v10, :cond_1

    .line 99
    const-string v6, "X509 CRL"

    .line 101
    .restart local v6    # "type":Ljava/lang/String;
    :try_start_1
    check-cast p1, Ljava/security/cert/X509CRL;

    .end local p1    # "o":Ljava/lang/Object;
    invoke-virtual {p1}, Ljava/security/cert/X509CRL;->getEncoded()[B
    :try_end_1
    .catch Ljava/security/cert/CRLException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v3

    .restart local v3    # "encoding":[B
    goto :goto_0

    .line 102
    .end local v3    # "encoding":[B
    :catch_1
    move-exception v2

    .line 103
    .local v2, "e":Ljava/security/cert/CRLException;
    new-instance v10, Ljava/io/IOException;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Cannot encode object: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v2}, Ljava/security/cert/CRLException;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v10

    .line 105
    .end local v2    # "e":Ljava/security/cert/CRLException;
    .end local v6    # "type":Ljava/lang/String;
    .restart local p1    # "o":Ljava/lang/Object;
    :cond_1
    instance-of v10, p1, Ljava/security/KeyPair;

    if-eqz v10, :cond_2

    .line 106
    check-cast p1, Ljava/security/KeyPair;

    .end local p1    # "o":Ljava/lang/Object;
    invoke-virtual {p1}, Ljava/security/KeyPair;->getPrivate()Ljava/security/PrivateKey;

    move-result-object v10

    invoke-virtual {p0, v10}, Lcom/android/sec/org/bouncycastle/util/io/pem/PEMKeyWriter;->writeObject(Ljava/lang/Object;)V

    goto :goto_1

    .line 108
    .restart local p1    # "o":Ljava/lang/Object;
    :cond_2
    instance-of v10, p1, Ljava/security/PrivateKey;

    if-eqz v10, :cond_6

    .line 110
    new-instance v0, Lcom/android/sec/org/bouncycastle/asn1/ASN1InputStream;

    move-object v10, p1

    check-cast v10, Ljava/security/Key;

    invoke-interface {v10}, Ljava/security/Key;->getEncoded()[B

    move-result-object v10

    invoke-direct {v0, v10}, Lcom/android/sec/org/bouncycastle/asn1/ASN1InputStream;-><init>([B)V

    .line 111
    .local v0, "aIn":Lcom/android/sec/org/bouncycastle/asn1/ASN1InputStream;
    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/ASN1InputStream;->readObject()Lcom/android/sec/org/bouncycastle/asn1/ASN1Primitive;

    move-result-object v1

    .line 112
    .local v1, "asn1Object":Lcom/android/sec/org/bouncycastle/asn1/ASN1Object;
    new-instance v4, Lcom/android/sec/org/bouncycastle/asn1/pkcs/PrivateKeyInfo;

    check-cast v1, Lcom/android/sec/org/bouncycastle/asn1/ASN1Sequence;

    .end local v1    # "asn1Object":Lcom/android/sec/org/bouncycastle/asn1/ASN1Object;
    invoke-direct {v4, v1}, Lcom/android/sec/org/bouncycastle/asn1/pkcs/PrivateKeyInfo;-><init>(Lcom/android/sec/org/bouncycastle/asn1/ASN1Sequence;)V

    .line 113
    .local v4, "info":Lcom/android/sec/org/bouncycastle/asn1/pkcs/PrivateKeyInfo;
    instance-of v10, p1, Ljava/security/interfaces/RSAPrivateKey;

    if-eqz v10, :cond_3

    .line 114
    const-string v6, "RSA PRIVATE KEY"

    .line 116
    .restart local v6    # "type":Ljava/lang/String;
    invoke-virtual {v4}, Lcom/android/sec/org/bouncycastle/asn1/pkcs/PrivateKeyInfo;->getPrivateKey()Lcom/android/sec/org/bouncycastle/asn1/ASN1Primitive;

    move-result-object v10

    invoke-virtual {v10}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Primitive;->getEncoded()[B

    move-result-object v3

    .restart local v3    # "encoding":[B
    goto/16 :goto_0

    .line 117
    .end local v3    # "encoding":[B
    .end local v6    # "type":Ljava/lang/String;
    :cond_3
    instance-of v10, p1, Ljava/security/interfaces/DSAPrivateKey;

    if-eqz v10, :cond_4

    .line 118
    const-string v6, "DSA PRIVATE KEY"

    .line 120
    .restart local v6    # "type":Ljava/lang/String;
    invoke-virtual {v4}, Lcom/android/sec/org/bouncycastle/asn1/pkcs/PrivateKeyInfo;->getAlgorithmId()Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;

    move-result-object v10

    invoke-virtual {v10}, Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;->getParameters()Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;

    move-result-object v10

    invoke-static {v10}, Lcom/android/sec/org/bouncycastle/asn1/x509/DSAParameter;->getInstance(Ljava/lang/Object;)Lcom/android/sec/org/bouncycastle/asn1/x509/DSAParameter;

    move-result-object v5

    .line 121
    .local v5, "p":Lcom/android/sec/org/bouncycastle/asn1/x509/DSAParameter;
    new-instance v7, Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;

    invoke-direct {v7}, Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;-><init>()V

    .line 123
    .local v7, "v":Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;
    new-instance v10, Lcom/android/sec/org/bouncycastle/asn1/DERInteger;

    const-wide/16 v12, 0x0

    invoke-direct {v10, v12, v13}, Lcom/android/sec/org/bouncycastle/asn1/DERInteger;-><init>(J)V

    invoke-virtual {v7, v10}, Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;->add(Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;)V

    .line 124
    new-instance v10, Lcom/android/sec/org/bouncycastle/asn1/DERInteger;

    invoke-virtual {v5}, Lcom/android/sec/org/bouncycastle/asn1/x509/DSAParameter;->getP()Ljava/math/BigInteger;

    move-result-object v11

    invoke-direct {v10, v11}, Lcom/android/sec/org/bouncycastle/asn1/DERInteger;-><init>(Ljava/math/BigInteger;)V

    invoke-virtual {v7, v10}, Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;->add(Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;)V

    .line 125
    new-instance v10, Lcom/android/sec/org/bouncycastle/asn1/DERInteger;

    invoke-virtual {v5}, Lcom/android/sec/org/bouncycastle/asn1/x509/DSAParameter;->getQ()Ljava/math/BigInteger;

    move-result-object v11

    invoke-direct {v10, v11}, Lcom/android/sec/org/bouncycastle/asn1/DERInteger;-><init>(Ljava/math/BigInteger;)V

    invoke-virtual {v7, v10}, Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;->add(Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;)V

    .line 126
    new-instance v10, Lcom/android/sec/org/bouncycastle/asn1/DERInteger;

    invoke-virtual {v5}, Lcom/android/sec/org/bouncycastle/asn1/x509/DSAParameter;->getG()Ljava/math/BigInteger;

    move-result-object v11

    invoke-direct {v10, v11}, Lcom/android/sec/org/bouncycastle/asn1/DERInteger;-><init>(Ljava/math/BigInteger;)V

    invoke-virtual {v7, v10}, Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;->add(Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;)V

    .line 128
    check-cast p1, Ljava/security/interfaces/DSAPrivateKey;

    .end local p1    # "o":Ljava/lang/Object;
    invoke-interface {p1}, Ljava/security/interfaces/DSAPrivateKey;->getX()Ljava/math/BigInteger;

    move-result-object v8

    .line 129
    .local v8, "x":Ljava/math/BigInteger;
    invoke-virtual {v5}, Lcom/android/sec/org/bouncycastle/asn1/x509/DSAParameter;->getG()Ljava/math/BigInteger;

    move-result-object v10

    invoke-virtual {v5}, Lcom/android/sec/org/bouncycastle/asn1/x509/DSAParameter;->getP()Ljava/math/BigInteger;

    move-result-object v11

    invoke-virtual {v10, v8, v11}, Ljava/math/BigInteger;->modPow(Ljava/math/BigInteger;Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v9

    .line 131
    .local v9, "y":Ljava/math/BigInteger;
    new-instance v10, Lcom/android/sec/org/bouncycastle/asn1/DERInteger;

    invoke-direct {v10, v9}, Lcom/android/sec/org/bouncycastle/asn1/DERInteger;-><init>(Ljava/math/BigInteger;)V

    invoke-virtual {v7, v10}, Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;->add(Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;)V

    .line 132
    new-instance v10, Lcom/android/sec/org/bouncycastle/asn1/DERInteger;

    invoke-direct {v10, v8}, Lcom/android/sec/org/bouncycastle/asn1/DERInteger;-><init>(Ljava/math/BigInteger;)V

    invoke-virtual {v7, v10}, Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;->add(Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;)V

    .line 134
    new-instance v10, Lcom/android/sec/org/bouncycastle/asn1/DERSequence;

    invoke-direct {v10, v7}, Lcom/android/sec/org/bouncycastle/asn1/DERSequence;-><init>(Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;)V

    invoke-virtual {v10}, Lcom/android/sec/org/bouncycastle/asn1/DERSequence;->getEncoded()[B

    move-result-object v3

    .line 135
    .restart local v3    # "encoding":[B
    goto/16 :goto_0

    .end local v3    # "encoding":[B
    .end local v5    # "p":Lcom/android/sec/org/bouncycastle/asn1/x509/DSAParameter;
    .end local v6    # "type":Ljava/lang/String;
    .end local v7    # "v":Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;
    .end local v8    # "x":Ljava/math/BigInteger;
    .end local v9    # "y":Ljava/math/BigInteger;
    .restart local p1    # "o":Ljava/lang/Object;
    :cond_4
    check-cast p1, Ljava/security/PrivateKey;

    .end local p1    # "o":Ljava/lang/Object;
    invoke-interface {p1}, Ljava/security/PrivateKey;->getAlgorithm()Ljava/lang/String;

    move-result-object v10

    const-string v11, "ECDSA"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_5

    .line 136
    const-string v6, "EC PRIVATE KEY"

    .line 138
    .restart local v6    # "type":Ljava/lang/String;
    invoke-virtual {v4}, Lcom/android/sec/org/bouncycastle/asn1/pkcs/PrivateKeyInfo;->getPrivateKey()Lcom/android/sec/org/bouncycastle/asn1/ASN1Primitive;

    move-result-object v10

    invoke-virtual {v10}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Primitive;->getEncoded()[B

    move-result-object v3

    .restart local v3    # "encoding":[B
    goto/16 :goto_0

    .line 140
    .end local v3    # "encoding":[B
    .end local v6    # "type":Ljava/lang/String;
    :cond_5
    new-instance v10, Ljava/io/IOException;

    const-string v11, "Cannot identify private key"

    invoke-direct {v10, v11}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v10

    .line 142
    .end local v0    # "aIn":Lcom/android/sec/org/bouncycastle/asn1/ASN1InputStream;
    .end local v4    # "info":Lcom/android/sec/org/bouncycastle/asn1/pkcs/PrivateKeyInfo;
    .restart local p1    # "o":Ljava/lang/Object;
    :cond_6
    instance-of v10, p1, Ljava/security/PublicKey;

    if-eqz v10, :cond_7

    .line 143
    const-string v6, "PUBLIC KEY"

    .line 145
    .restart local v6    # "type":Ljava/lang/String;
    check-cast p1, Ljava/security/PublicKey;

    .end local p1    # "o":Ljava/lang/Object;
    invoke-interface {p1}, Ljava/security/PublicKey;->getEncoded()[B

    move-result-object v3

    .restart local v3    # "encoding":[B
    goto/16 :goto_0

    .line 146
    .end local v3    # "encoding":[B
    .end local v6    # "type":Ljava/lang/String;
    .restart local p1    # "o":Ljava/lang/Object;
    :cond_7
    instance-of v10, p1, Lcom/android/sec/org/bouncycastle/x509/X509AttributeCertificate;

    if-eqz v10, :cond_8

    .line 147
    const-string v6, "ATTRIBUTE CERTIFICATE"

    .line 148
    .restart local v6    # "type":Ljava/lang/String;
    check-cast p1, Lcom/android/sec/org/bouncycastle/x509/X509V2AttributeCertificate;

    .end local p1    # "o":Ljava/lang/Object;
    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/x509/X509V2AttributeCertificate;->getEncoded()[B

    move-result-object v3

    .restart local v3    # "encoding":[B
    goto/16 :goto_0

    .line 149
    .end local v3    # "encoding":[B
    .end local v6    # "type":Ljava/lang/String;
    .restart local p1    # "o":Ljava/lang/Object;
    :cond_8
    instance-of v10, p1, Lcom/android/sec/org/bouncycastle/jce/PKCS10CertificationRequest;

    if-eqz v10, :cond_9

    .line 150
    const-string v6, "CERTIFICATE REQUEST"

    .line 151
    .restart local v6    # "type":Ljava/lang/String;
    check-cast p1, Lcom/android/sec/org/bouncycastle/jce/PKCS10CertificationRequest;

    .end local p1    # "o":Ljava/lang/Object;
    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/jce/PKCS10CertificationRequest;->getEncoded()[B

    move-result-object v3

    .restart local v3    # "encoding":[B
    goto/16 :goto_0

    .line 152
    .end local v3    # "encoding":[B
    .end local v6    # "type":Ljava/lang/String;
    .restart local p1    # "o":Ljava/lang/Object;
    :cond_9
    instance-of v10, p1, Lcom/android/sec/org/bouncycastle/asn1/cms/ContentInfo;

    if-eqz v10, :cond_a

    .line 153
    const-string v6, "PKCS7"

    .line 154
    .restart local v6    # "type":Ljava/lang/String;
    check-cast p1, Lcom/android/sec/org/bouncycastle/asn1/cms/ContentInfo;

    .end local p1    # "o":Ljava/lang/Object;
    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/asn1/cms/ContentInfo;->getEncoded()[B

    move-result-object v3

    .restart local v3    # "encoding":[B
    goto/16 :goto_0

    .line 156
    .end local v3    # "encoding":[B
    .end local v6    # "type":Ljava/lang/String;
    .restart local p1    # "o":Ljava/lang/Object;
    :cond_a
    new-instance v10, Ljava/io/IOException;

    const-string v11, "unknown object passed - can\'t encode."

    invoke-direct {v10, v11}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v10
.end method

.method public writeObject(Ljava/lang/Object;Ljava/lang/String;[CLjava/security/SecureRandom;)V
    .locals 24
    .param p1, "obj"    # Ljava/lang/Object;
    .param p2, "algorithm"    # Ljava/lang/String;
    .param p3, "password"    # [C
    .param p4, "random"    # Ljava/security/SecureRandom;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 166
    move-object/from16 v0, p1

    instance-of v3, v0, Ljava/security/KeyPair;

    if-eqz v3, :cond_0

    .line 167
    check-cast p1, Ljava/security/KeyPair;

    .end local p1    # "obj":Ljava/lang/Object;
    invoke-virtual/range {p1 .. p1}, Ljava/security/KeyPair;->getPrivate()Ljava/security/PrivateKey;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/android/sec/org/bouncycastle/util/io/pem/PEMKeyWriter;->writeObject(Ljava/lang/Object;)V

    .line 246
    :goto_0
    return-void

    .line 171
    .restart local p1    # "obj":Ljava/lang/Object;
    :cond_0
    const/16 v18, 0x0

    .line 172
    .local v18, "type":Ljava/lang/String;
    const/4 v5, 0x0

    .line 174
    .local v5, "keyData":[B
    move-object/from16 v0, p1

    instance-of v3, v0, Ljava/security/interfaces/RSAPrivateCrtKey;

    if-eqz v3, :cond_3

    .line 175
    const-string v18, "RSA PRIVATE KEY"

    move-object/from16 v15, p1

    .line 177
    check-cast v15, Ljava/security/interfaces/RSAPrivateCrtKey;

    .line 179
    .local v15, "k":Ljava/security/interfaces/RSAPrivateCrtKey;
    new-instance v2, Lcom/android/sec/org/bouncycastle/asn1/pkcs/RSAPrivateKeyStructure;

    invoke-interface {v15}, Ljava/security/interfaces/RSAPrivateCrtKey;->getModulus()Ljava/math/BigInteger;

    move-result-object v3

    invoke-interface {v15}, Ljava/security/interfaces/RSAPrivateCrtKey;->getPublicExponent()Ljava/math/BigInteger;

    move-result-object v4

    invoke-interface {v15}, Ljava/security/interfaces/RSAPrivateCrtKey;->getPrivateExponent()Ljava/math/BigInteger;

    move-result-object v5

    .end local v5    # "keyData":[B
    invoke-interface {v15}, Ljava/security/interfaces/RSAPrivateCrtKey;->getPrimeP()Ljava/math/BigInteger;

    move-result-object v6

    invoke-interface {v15}, Ljava/security/interfaces/RSAPrivateCrtKey;->getPrimeQ()Ljava/math/BigInteger;

    move-result-object v7

    invoke-interface {v15}, Ljava/security/interfaces/RSAPrivateCrtKey;->getPrimeExponentP()Ljava/math/BigInteger;

    move-result-object v8

    invoke-interface {v15}, Ljava/security/interfaces/RSAPrivateCrtKey;->getPrimeExponentQ()Ljava/math/BigInteger;

    move-result-object v9

    invoke-interface {v15}, Ljava/security/interfaces/RSAPrivateCrtKey;->getCrtCoefficient()Ljava/math/BigInteger;

    move-result-object v10

    invoke-direct/range {v2 .. v10}, Lcom/android/sec/org/bouncycastle/asn1/pkcs/RSAPrivateKeyStructure;-><init>(Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/math/BigInteger;)V

    .line 184
    .local v2, "keyStruct":Lcom/android/sec/org/bouncycastle/asn1/pkcs/RSAPrivateKeyStructure;
    invoke-virtual {v2}, Lcom/android/sec/org/bouncycastle/asn1/pkcs/RSAPrivateKeyStructure;->getEncoded()[B

    move-result-object v5

    .line 214
    .end local v2    # "keyStruct":Lcom/android/sec/org/bouncycastle/asn1/pkcs/RSAPrivateKeyStructure;
    .end local v15    # "k":Ljava/security/interfaces/RSAPrivateCrtKey;
    .restart local v5    # "keyData":[B
    :cond_1
    :goto_1
    if-eqz v18, :cond_2

    if-nez v5, :cond_5

    .line 216
    :cond_2
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Object type not supported: "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {p1 .. p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 185
    :cond_3
    move-object/from16 v0, p1

    instance-of v3, v0, Ljava/security/interfaces/DSAPrivateKey;

    if-eqz v3, :cond_4

    .line 186
    const-string v18, "DSA PRIVATE KEY"

    move-object/from16 v15, p1

    .line 188
    check-cast v15, Ljava/security/interfaces/DSAPrivateKey;

    .line 189
    .local v15, "k":Ljava/security/interfaces/DSAPrivateKey;
    invoke-interface {v15}, Ljava/security/interfaces/DSAPrivateKey;->getParams()Ljava/security/interfaces/DSAParams;

    move-result-object v16

    .line 190
    .local v16, "p":Ljava/security/interfaces/DSAParams;
    new-instance v19, Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;

    invoke-direct/range {v19 .. v19}, Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;-><init>()V

    .line 192
    .local v19, "v":Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;
    new-instance v3, Lcom/android/sec/org/bouncycastle/asn1/DERInteger;

    const-wide/16 v22, 0x0

    move-wide/from16 v0, v22

    invoke-direct {v3, v0, v1}, Lcom/android/sec/org/bouncycastle/asn1/DERInteger;-><init>(J)V

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;->add(Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;)V

    .line 193
    new-instance v3, Lcom/android/sec/org/bouncycastle/asn1/DERInteger;

    invoke-interface/range {v16 .. v16}, Ljava/security/interfaces/DSAParams;->getP()Ljava/math/BigInteger;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/android/sec/org/bouncycastle/asn1/DERInteger;-><init>(Ljava/math/BigInteger;)V

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;->add(Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;)V

    .line 194
    new-instance v3, Lcom/android/sec/org/bouncycastle/asn1/DERInteger;

    invoke-interface/range {v16 .. v16}, Ljava/security/interfaces/DSAParams;->getQ()Ljava/math/BigInteger;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/android/sec/org/bouncycastle/asn1/DERInteger;-><init>(Ljava/math/BigInteger;)V

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;->add(Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;)V

    .line 195
    new-instance v3, Lcom/android/sec/org/bouncycastle/asn1/DERInteger;

    invoke-interface/range {v16 .. v16}, Ljava/security/interfaces/DSAParams;->getG()Ljava/math/BigInteger;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/android/sec/org/bouncycastle/asn1/DERInteger;-><init>(Ljava/math/BigInteger;)V

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;->add(Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;)V

    .line 197
    invoke-interface {v15}, Ljava/security/interfaces/DSAPrivateKey;->getX()Ljava/math/BigInteger;

    move-result-object v20

    .line 198
    .local v20, "x":Ljava/math/BigInteger;
    invoke-interface/range {v16 .. v16}, Ljava/security/interfaces/DSAParams;->getG()Ljava/math/BigInteger;

    move-result-object v3

    invoke-interface/range {v16 .. v16}, Ljava/security/interfaces/DSAParams;->getP()Ljava/math/BigInteger;

    move-result-object v4

    move-object/from16 v0, v20

    invoke-virtual {v3, v0, v4}, Ljava/math/BigInteger;->modPow(Ljava/math/BigInteger;Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v21

    .line 200
    .local v21, "y":Ljava/math/BigInteger;
    new-instance v3, Lcom/android/sec/org/bouncycastle/asn1/DERInteger;

    move-object/from16 v0, v21

    invoke-direct {v3, v0}, Lcom/android/sec/org/bouncycastle/asn1/DERInteger;-><init>(Ljava/math/BigInteger;)V

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;->add(Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;)V

    .line 201
    new-instance v3, Lcom/android/sec/org/bouncycastle/asn1/DERInteger;

    move-object/from16 v0, v20

    invoke-direct {v3, v0}, Lcom/android/sec/org/bouncycastle/asn1/DERInteger;-><init>(Ljava/math/BigInteger;)V

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;->add(Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;)V

    .line 203
    new-instance v3, Lcom/android/sec/org/bouncycastle/asn1/DERSequence;

    move-object/from16 v0, v19

    invoke-direct {v3, v0}, Lcom/android/sec/org/bouncycastle/asn1/DERSequence;-><init>(Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;)V

    invoke-virtual {v3}, Lcom/android/sec/org/bouncycastle/asn1/DERSequence;->getEncoded()[B

    move-result-object v5

    .line 204
    goto/16 :goto_1

    .end local v15    # "k":Ljava/security/interfaces/DSAPrivateKey;
    .end local v16    # "p":Ljava/security/interfaces/DSAParams;
    .end local v19    # "v":Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;
    .end local v20    # "x":Ljava/math/BigInteger;
    .end local v21    # "y":Ljava/math/BigInteger;
    :cond_4
    move-object/from16 v0, p1

    instance-of v3, v0, Ljava/security/PrivateKey;

    if-eqz v3, :cond_1

    const-string v4, "ECDSA"

    move-object/from16 v3, p1

    check-cast v3, Ljava/security/PrivateKey;

    invoke-interface {v3}, Ljava/security/PrivateKey;->getAlgorithm()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 205
    const-string v18, "EC PRIVATE KEY"

    .line 207
    new-instance v11, Lcom/android/sec/org/bouncycastle/asn1/ASN1InputStream;

    move-object/from16 v3, p1

    check-cast v3, Ljava/security/Key;

    invoke-interface {v3}, Ljava/security/Key;->getEncoded()[B

    move-result-object v3

    invoke-direct {v11, v3}, Lcom/android/sec/org/bouncycastle/asn1/ASN1InputStream;-><init>([B)V

    .line 208
    .local v11, "aIn":Lcom/android/sec/org/bouncycastle/asn1/ASN1InputStream;
    invoke-virtual {v11}, Lcom/android/sec/org/bouncycastle/asn1/ASN1InputStream;->readObject()Lcom/android/sec/org/bouncycastle/asn1/ASN1Primitive;

    move-result-object v12

    .line 209
    .local v12, "asn1Object":Lcom/android/sec/org/bouncycastle/asn1/ASN1Object;
    new-instance v17, Lcom/android/sec/org/bouncycastle/asn1/pkcs/PrivateKeyInfo;

    check-cast v12, Lcom/android/sec/org/bouncycastle/asn1/ASN1Sequence;

    .end local v12    # "asn1Object":Lcom/android/sec/org/bouncycastle/asn1/ASN1Object;
    move-object/from16 v0, v17

    invoke-direct {v0, v12}, Lcom/android/sec/org/bouncycastle/asn1/pkcs/PrivateKeyInfo;-><init>(Lcom/android/sec/org/bouncycastle/asn1/ASN1Sequence;)V

    .line 211
    .local v17, "privInfo":Lcom/android/sec/org/bouncycastle/asn1/pkcs/PrivateKeyInfo;
    invoke-virtual/range {v17 .. v17}, Lcom/android/sec/org/bouncycastle/asn1/pkcs/PrivateKeyInfo;->getPrivateKey()Lcom/android/sec/org/bouncycastle/asn1/ASN1Primitive;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Primitive;->getEncoded()[B

    move-result-object v5

    goto/16 :goto_1

    .line 220
    .end local v11    # "aIn":Lcom/android/sec/org/bouncycastle/asn1/ASN1InputStream;
    .end local v17    # "privInfo":Lcom/android/sec/org/bouncycastle/asn1/pkcs/PrivateKeyInfo;
    :cond_5
    invoke-static/range {p2 .. p2}, Lcom/android/sec/org/bouncycastle/util/Strings;->toUpperCase(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 223
    .local v7, "dekAlgName":Ljava/lang/String;
    const-string v3, "DESEDE"

    invoke-virtual {v7, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 224
    const-string v7, "DES-EDE3-CBC"

    .line 227
    :cond_6
    const-string v3, "AES-"

    invoke-virtual {v7, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_7

    const/16 v14, 0x10

    .line 229
    .local v14, "ivLength":I
    :goto_2
    new-array v8, v14, [B

    .line 230
    .local v8, "iv":[B
    move-object/from16 v0, p4

    invoke-virtual {v0, v8}, Ljava/security/SecureRandom;->nextBytes([B)V

    .line 232
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/sec/org/bouncycastle/util/io/pem/PEMKeyWriter;->provider:Ljava/lang/String;

    move-object/from16 v6, p3

    invoke-static/range {v3 .. v8}, Lcom/android/sec/org/bouncycastle/util/io/pem/PEMKeyUtilities;->crypt(ZLjava/lang/String;[B[CLjava/lang/String;[B)[B

    move-result-object v13

    .line 235
    .local v13, "encData":[B
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/android/sec/org/bouncycastle/util/io/pem/PEMKeyWriter;->writeHeader(Ljava/lang/String;)V

    .line 236
    const-string v3, "Proc-Type: 4,ENCRYPTED"

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/android/sec/org/bouncycastle/util/io/pem/PEMKeyWriter;->write(Ljava/lang/String;)V

    .line 237
    invoke-virtual/range {p0 .. p0}, Lcom/android/sec/org/bouncycastle/util/io/pem/PEMKeyWriter;->newLine()V

    .line 238
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "DEK-Info: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/android/sec/org/bouncycastle/util/io/pem/PEMKeyWriter;->write(Ljava/lang/String;)V

    .line 239
    move-object/from16 v0, p0

    invoke-direct {v0, v8}, Lcom/android/sec/org/bouncycastle/util/io/pem/PEMKeyWriter;->writeHexEncoded([B)V

    .line 240
    invoke-virtual/range {p0 .. p0}, Lcom/android/sec/org/bouncycastle/util/io/pem/PEMKeyWriter;->newLine()V

    .line 241
    invoke-virtual/range {p0 .. p0}, Lcom/android/sec/org/bouncycastle/util/io/pem/PEMKeyWriter;->newLine()V

    .line 242
    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lcom/android/sec/org/bouncycastle/util/io/pem/PEMKeyWriter;->writeEncoded([B)V

    .line 243
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/android/sec/org/bouncycastle/util/io/pem/PEMKeyWriter;->writeFooter(Ljava/lang/String;)V

    .line 244
    invoke-virtual/range {p0 .. p0}, Lcom/android/sec/org/bouncycastle/util/io/pem/PEMKeyWriter;->newLine()V

    .line 245
    invoke-virtual/range {p0 .. p0}, Lcom/android/sec/org/bouncycastle/util/io/pem/PEMKeyWriter;->flush()V

    goto/16 :goto_0

    .line 227
    .end local v8    # "iv":[B
    .end local v13    # "encData":[B
    .end local v14    # "ivLength":I
    :cond_7
    const/16 v14, 0x8

    goto :goto_2
.end method
