.class public Lcom/android/sec/org/bouncycastle/cert/AttributeCertificateHolder;
.super Ljava/lang/Object;
.source "AttributeCertificateHolder.java"

# interfaces
.implements Lcom/android/sec/org/bouncycastle/util/Selector;


# static fields
.field private static digestCalculatorProvider:Lcom/android/sec/org/bouncycastle/operator/DigestCalculatorProvider;


# instance fields
.field final holder:Lcom/android/sec/org/bouncycastle/asn1/x509/Holder;


# direct methods
.method public constructor <init>(ILcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;[B)V
    .locals 4
    .param p1, "digestedObjectType"    # I
    .param p2, "digestAlgorithm"    # Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;
    .param p3, "otherObjectTypeID"    # Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;
    .param p4, "objectDigest"    # [B

    .prologue
    .line 100
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 101
    new-instance v0, Lcom/android/sec/org/bouncycastle/asn1/x509/Holder;

    new-instance v1, Lcom/android/sec/org/bouncycastle/asn1/x509/ObjectDigestInfo;

    new-instance v2, Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;

    invoke-direct {v2, p2}, Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;-><init>(Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;)V

    invoke-static {p4}, Lcom/android/sec/org/bouncycastle/util/Arrays;->clone([B)[B

    move-result-object v3

    invoke-direct {v1, p1, p3, v2, v3}, Lcom/android/sec/org/bouncycastle/asn1/x509/ObjectDigestInfo;-><init>(ILcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;[B)V

    invoke-direct {v0, v1}, Lcom/android/sec/org/bouncycastle/asn1/x509/Holder;-><init>(Lcom/android/sec/org/bouncycastle/asn1/x509/ObjectDigestInfo;)V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/cert/AttributeCertificateHolder;->holder:Lcom/android/sec/org/bouncycastle/asn1/x509/Holder;

    .line 104
    return-void
.end method

.method constructor <init>(Lcom/android/sec/org/bouncycastle/asn1/ASN1Sequence;)V
    .locals 1
    .param p1, "seq"    # Lcom/android/sec/org/bouncycastle/asn1/ASN1Sequence;

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    invoke-static {p1}, Lcom/android/sec/org/bouncycastle/asn1/x509/Holder;->getInstance(Ljava/lang/Object;)Lcom/android/sec/org/bouncycastle/asn1/x509/Holder;

    move-result-object v0

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/cert/AttributeCertificateHolder;->holder:Lcom/android/sec/org/bouncycastle/asn1/x509/Holder;

    .line 54
    return-void
.end method

.method public constructor <init>(Lcom/android/sec/org/bouncycastle/asn1/x500/X500Name;)V
    .locals 2
    .param p1, "principal"    # Lcom/android/sec/org/bouncycastle/asn1/x500/X500Name;

    .prologue
    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    new-instance v0, Lcom/android/sec/org/bouncycastle/asn1/x509/Holder;

    invoke-direct {p0, p1}, Lcom/android/sec/org/bouncycastle/cert/AttributeCertificateHolder;->generateGeneralNames(Lcom/android/sec/org/bouncycastle/asn1/x500/X500Name;)Lcom/android/sec/org/bouncycastle/asn1/x509/GeneralNames;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/sec/org/bouncycastle/asn1/x509/Holder;-><init>(Lcom/android/sec/org/bouncycastle/asn1/x509/GeneralNames;)V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/cert/AttributeCertificateHolder;->holder:Lcom/android/sec/org/bouncycastle/asn1/x509/Holder;

    .line 73
    return-void
.end method

.method public constructor <init>(Lcom/android/sec/org/bouncycastle/asn1/x500/X500Name;Ljava/math/BigInteger;)V
    .locals 4
    .param p1, "issuerName"    # Lcom/android/sec/org/bouncycastle/asn1/x500/X500Name;
    .param p2, "serialNumber"    # Ljava/math/BigInteger;

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    new-instance v0, Lcom/android/sec/org/bouncycastle/asn1/x509/Holder;

    new-instance v1, Lcom/android/sec/org/bouncycastle/asn1/x509/IssuerSerial;

    new-instance v2, Lcom/android/sec/org/bouncycastle/asn1/x509/GeneralNames;

    new-instance v3, Lcom/android/sec/org/bouncycastle/asn1/x509/GeneralName;

    invoke-direct {v3, p1}, Lcom/android/sec/org/bouncycastle/asn1/x509/GeneralName;-><init>(Lcom/android/sec/org/bouncycastle/asn1/x500/X500Name;)V

    invoke-direct {v2, v3}, Lcom/android/sec/org/bouncycastle/asn1/x509/GeneralNames;-><init>(Lcom/android/sec/org/bouncycastle/asn1/x509/GeneralName;)V

    new-instance v3, Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;

    invoke-direct {v3, p2}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;-><init>(Ljava/math/BigInteger;)V

    invoke-direct {v1, v2, v3}, Lcom/android/sec/org/bouncycastle/asn1/x509/IssuerSerial;-><init>(Lcom/android/sec/org/bouncycastle/asn1/x509/GeneralNames;Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;)V

    invoke-direct {v0, v1}, Lcom/android/sec/org/bouncycastle/asn1/x509/Holder;-><init>(Lcom/android/sec/org/bouncycastle/asn1/x509/IssuerSerial;)V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/cert/AttributeCertificateHolder;->holder:Lcom/android/sec/org/bouncycastle/asn1/x509/Holder;

    .line 62
    return-void
.end method

.method public constructor <init>(Lcom/android/sec/org/bouncycastle/cert/X509CertificateHolder;)V
    .locals 5
    .param p1, "cert"    # Lcom/android/sec/org/bouncycastle/cert/X509CertificateHolder;

    .prologue
    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    new-instance v0, Lcom/android/sec/org/bouncycastle/asn1/x509/Holder;

    new-instance v1, Lcom/android/sec/org/bouncycastle/asn1/x509/IssuerSerial;

    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/cert/X509CertificateHolder;->getIssuer()Lcom/android/sec/org/bouncycastle/asn1/x500/X500Name;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/android/sec/org/bouncycastle/cert/AttributeCertificateHolder;->generateGeneralNames(Lcom/android/sec/org/bouncycastle/asn1/x500/X500Name;)Lcom/android/sec/org/bouncycastle/asn1/x509/GeneralNames;

    move-result-object v2

    new-instance v3, Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;

    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/cert/X509CertificateHolder;->getSerialNumber()Ljava/math/BigInteger;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;-><init>(Ljava/math/BigInteger;)V

    invoke-direct {v1, v2, v3}, Lcom/android/sec/org/bouncycastle/asn1/x509/IssuerSerial;-><init>(Lcom/android/sec/org/bouncycastle/asn1/x509/GeneralNames;Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;)V

    invoke-direct {v0, v1}, Lcom/android/sec/org/bouncycastle/asn1/x509/Holder;-><init>(Lcom/android/sec/org/bouncycastle/asn1/x509/IssuerSerial;)V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/cert/AttributeCertificateHolder;->holder:Lcom/android/sec/org/bouncycastle/asn1/x509/Holder;

    .line 68
    return-void
.end method

.method private generateGeneralNames(Lcom/android/sec/org/bouncycastle/asn1/x500/X500Name;)Lcom/android/sec/org/bouncycastle/asn1/x509/GeneralNames;
    .locals 2
    .param p1, "principal"    # Lcom/android/sec/org/bouncycastle/asn1/x500/X500Name;

    .prologue
    .line 175
    new-instance v0, Lcom/android/sec/org/bouncycastle/asn1/x509/GeneralNames;

    new-instance v1, Lcom/android/sec/org/bouncycastle/asn1/x509/GeneralName;

    invoke-direct {v1, p1}, Lcom/android/sec/org/bouncycastle/asn1/x509/GeneralName;-><init>(Lcom/android/sec/org/bouncycastle/asn1/x500/X500Name;)V

    invoke-direct {v0, v1}, Lcom/android/sec/org/bouncycastle/asn1/x509/GeneralNames;-><init>(Lcom/android/sec/org/bouncycastle/asn1/x509/GeneralName;)V

    return-object v0
.end method

.method private getPrincipals([Lcom/android/sec/org/bouncycastle/asn1/x509/GeneralName;)[Lcom/android/sec/org/bouncycastle/asn1/x500/X500Name;
    .locals 4
    .param p1, "names"    # [Lcom/android/sec/org/bouncycastle/asn1/x509/GeneralName;

    .prologue
    .line 200
    new-instance v1, Ljava/util/ArrayList;

    array-length v2, p1

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 202
    .local v1, "l":Ljava/util/List;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, p1

    if-eq v0, v2, :cond_1

    .line 204
    aget-object v2, p1, v0

    invoke-virtual {v2}, Lcom/android/sec/org/bouncycastle/asn1/x509/GeneralName;->getTagNo()I

    move-result v2

    const/4 v3, 0x4

    if-ne v2, v3, :cond_0

    .line 206
    aget-object v2, p1, v0

    invoke-virtual {v2}, Lcom/android/sec/org/bouncycastle/asn1/x509/GeneralName;->getName()Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;

    move-result-object v2

    invoke-static {v2}, Lcom/android/sec/org/bouncycastle/asn1/x500/X500Name;->getInstance(Ljava/lang/Object;)Lcom/android/sec/org/bouncycastle/asn1/x500/X500Name;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 202
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 210
    :cond_1
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    new-array v2, v2, [Lcom/android/sec/org/bouncycastle/asn1/x500/X500Name;

    invoke-interface {v1, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Lcom/android/sec/org/bouncycastle/asn1/x500/X500Name;

    check-cast v2, [Lcom/android/sec/org/bouncycastle/asn1/x500/X500Name;

    return-object v2
.end method

.method private matchesDN(Lcom/android/sec/org/bouncycastle/asn1/x500/X500Name;Lcom/android/sec/org/bouncycastle/asn1/x509/GeneralNames;)Z
    .locals 5
    .param p1, "subject"    # Lcom/android/sec/org/bouncycastle/asn1/x500/X500Name;
    .param p2, "targets"    # Lcom/android/sec/org/bouncycastle/asn1/x509/GeneralNames;

    .prologue
    .line 180
    invoke-virtual {p2}, Lcom/android/sec/org/bouncycastle/asn1/x509/GeneralNames;->getNames()[Lcom/android/sec/org/bouncycastle/asn1/x509/GeneralName;

    move-result-object v2

    .line 182
    .local v2, "names":[Lcom/android/sec/org/bouncycastle/asn1/x509/GeneralName;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v3, v2

    if-eq v1, v3, :cond_1

    .line 184
    aget-object v0, v2, v1

    .line 186
    .local v0, "gn":Lcom/android/sec/org/bouncycastle/asn1/x509/GeneralName;
    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/x509/GeneralName;->getTagNo()I

    move-result v3

    const/4 v4, 0x4

    if-ne v3, v4, :cond_0

    .line 188
    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/x509/GeneralName;->getName()Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;

    move-result-object v3

    invoke-static {v3}, Lcom/android/sec/org/bouncycastle/asn1/x500/X500Name;->getInstance(Ljava/lang/Object;)Lcom/android/sec/org/bouncycastle/asn1/x500/X500Name;

    move-result-object v3

    invoke-virtual {v3, p1}, Lcom/android/sec/org/bouncycastle/asn1/x500/X500Name;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 190
    const/4 v3, 0x1

    .line 195
    .end local v0    # "gn":Lcom/android/sec/org/bouncycastle/asn1/x509/GeneralName;
    :goto_1
    return v3

    .line 182
    .restart local v0    # "gn":Lcom/android/sec/org/bouncycastle/asn1/x509/GeneralName;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 195
    .end local v0    # "gn":Lcom/android/sec/org/bouncycastle/asn1/x509/GeneralName;
    :cond_1
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public static setDigestCalculatorProvider(Lcom/android/sec/org/bouncycastle/operator/DigestCalculatorProvider;)V
    .locals 0
    .param p0, "digCalcProvider"    # Lcom/android/sec/org/bouncycastle/operator/DigestCalculatorProvider;

    .prologue
    .line 355
    sput-object p0, Lcom/android/sec/org/bouncycastle/cert/AttributeCertificateHolder;->digestCalculatorProvider:Lcom/android/sec/org/bouncycastle/operator/DigestCalculatorProvider;

    .line 356
    return-void
.end method


# virtual methods
.method public clone()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 264
    new-instance v1, Lcom/android/sec/org/bouncycastle/cert/AttributeCertificateHolder;

    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cert/AttributeCertificateHolder;->holder:Lcom/android/sec/org/bouncycastle/asn1/x509/Holder;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/x509/Holder;->toASN1Primitive()Lcom/android/sec/org/bouncycastle/asn1/ASN1Primitive;

    move-result-object v0

    check-cast v0, Lcom/android/sec/org/bouncycastle/asn1/ASN1Sequence;

    invoke-direct {v1, v0}, Lcom/android/sec/org/bouncycastle/cert/AttributeCertificateHolder;-><init>(Lcom/android/sec/org/bouncycastle/asn1/ASN1Sequence;)V

    return-object v1
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    .line 327
    if-ne p1, p0, :cond_0

    .line 329
    const/4 v1, 0x1

    .line 339
    :goto_0
    return v1

    .line 332
    :cond_0
    instance-of v1, p1, Lcom/android/sec/org/bouncycastle/cert/AttributeCertificateHolder;

    if-nez v1, :cond_1

    .line 334
    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    move-object v0, p1

    .line 337
    check-cast v0, Lcom/android/sec/org/bouncycastle/cert/AttributeCertificateHolder;

    .line 339
    .local v0, "other":Lcom/android/sec/org/bouncycastle/cert/AttributeCertificateHolder;
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/cert/AttributeCertificateHolder;->holder:Lcom/android/sec/org/bouncycastle/asn1/x509/Holder;

    iget-object v2, v0, Lcom/android/sec/org/bouncycastle/cert/AttributeCertificateHolder;->holder:Lcom/android/sec/org/bouncycastle/asn1/x509/Holder;

    invoke-virtual {v1, v2}, Lcom/android/sec/org/bouncycastle/asn1/x509/Holder;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method

.method public getDigestAlgorithm()Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cert/AttributeCertificateHolder;->holder:Lcom/android/sec/org/bouncycastle/asn1/x509/Holder;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/x509/Holder;->getObjectDigestInfo()Lcom/android/sec/org/bouncycastle/asn1/x509/ObjectDigestInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 139
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cert/AttributeCertificateHolder;->holder:Lcom/android/sec/org/bouncycastle/asn1/x509/Holder;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/x509/Holder;->getObjectDigestInfo()Lcom/android/sec/org/bouncycastle/asn1/x509/ObjectDigestInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/x509/ObjectDigestInfo;->getDigestAlgorithm()Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;

    move-result-object v0

    .line 141
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getDigestedObjectType()I
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cert/AttributeCertificateHolder;->holder:Lcom/android/sec/org/bouncycastle/asn1/x509/Holder;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/x509/Holder;->getObjectDigestInfo()Lcom/android/sec/org/bouncycastle/asn1/x509/ObjectDigestInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 124
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cert/AttributeCertificateHolder;->holder:Lcom/android/sec/org/bouncycastle/asn1/x509/Holder;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/x509/Holder;->getObjectDigestInfo()Lcom/android/sec/org/bouncycastle/asn1/x509/ObjectDigestInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/x509/ObjectDigestInfo;->getDigestedObjectType()Lcom/android/sec/org/bouncycastle/asn1/ASN1Enumerated;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Enumerated;->getValue()Ljava/math/BigInteger;

    move-result-object v0

    invoke-virtual {v0}, Ljava/math/BigInteger;->intValue()I

    move-result v0

    .line 127
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public getEntityNames()[Lcom/android/sec/org/bouncycastle/asn1/x500/X500Name;
    .locals 1

    .prologue
    .line 222
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cert/AttributeCertificateHolder;->holder:Lcom/android/sec/org/bouncycastle/asn1/x509/Holder;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/x509/Holder;->getEntityName()Lcom/android/sec/org/bouncycastle/asn1/x509/GeneralNames;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 224
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cert/AttributeCertificateHolder;->holder:Lcom/android/sec/org/bouncycastle/asn1/x509/Holder;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/x509/Holder;->getEntityName()Lcom/android/sec/org/bouncycastle/asn1/x509/GeneralNames;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/x509/GeneralNames;->getNames()[Lcom/android/sec/org/bouncycastle/asn1/x509/GeneralName;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/sec/org/bouncycastle/cert/AttributeCertificateHolder;->getPrincipals([Lcom/android/sec/org/bouncycastle/asn1/x509/GeneralName;)[Lcom/android/sec/org/bouncycastle/asn1/x500/X500Name;

    move-result-object v0

    .line 227
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getIssuer()[Lcom/android/sec/org/bouncycastle/asn1/x500/X500Name;
    .locals 1

    .prologue
    .line 237
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cert/AttributeCertificateHolder;->holder:Lcom/android/sec/org/bouncycastle/asn1/x509/Holder;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/x509/Holder;->getBaseCertificateID()Lcom/android/sec/org/bouncycastle/asn1/x509/IssuerSerial;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 239
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cert/AttributeCertificateHolder;->holder:Lcom/android/sec/org/bouncycastle/asn1/x509/Holder;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/x509/Holder;->getBaseCertificateID()Lcom/android/sec/org/bouncycastle/asn1/x509/IssuerSerial;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/x509/IssuerSerial;->getIssuer()Lcom/android/sec/org/bouncycastle/asn1/x509/GeneralNames;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/x509/GeneralNames;->getNames()[Lcom/android/sec/org/bouncycastle/asn1/x509/GeneralName;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/sec/org/bouncycastle/cert/AttributeCertificateHolder;->getPrincipals([Lcom/android/sec/org/bouncycastle/asn1/x509/GeneralName;)[Lcom/android/sec/org/bouncycastle/asn1/x500/X500Name;

    move-result-object v0

    .line 242
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getObjectDigest()[B
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cert/AttributeCertificateHolder;->holder:Lcom/android/sec/org/bouncycastle/asn1/x509/Holder;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/x509/Holder;->getObjectDigestInfo()Lcom/android/sec/org/bouncycastle/asn1/x509/ObjectDigestInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 153
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cert/AttributeCertificateHolder;->holder:Lcom/android/sec/org/bouncycastle/asn1/x509/Holder;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/x509/Holder;->getObjectDigestInfo()Lcom/android/sec/org/bouncycastle/asn1/x509/ObjectDigestInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/x509/ObjectDigestInfo;->getObjectDigest()Lcom/android/sec/org/bouncycastle/asn1/DERBitString;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/DERBitString;->getBytes()[B

    move-result-object v0

    .line 155
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getOtherObjectTypeID()Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;
    .locals 2

    .prologue
    .line 166
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cert/AttributeCertificateHolder;->holder:Lcom/android/sec/org/bouncycastle/asn1/x509/Holder;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/x509/Holder;->getObjectDigestInfo()Lcom/android/sec/org/bouncycastle/asn1/x509/ObjectDigestInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 168
    new-instance v0, Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/cert/AttributeCertificateHolder;->holder:Lcom/android/sec/org/bouncycastle/asn1/x509/Holder;

    invoke-virtual {v1}, Lcom/android/sec/org/bouncycastle/asn1/x509/Holder;->getObjectDigestInfo()Lcom/android/sec/org/bouncycastle/asn1/x509/ObjectDigestInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/sec/org/bouncycastle/asn1/x509/ObjectDigestInfo;->getOtherObjectTypeID()Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;-><init>(Ljava/lang/String;)V

    .line 170
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public getSerialNumber()Ljava/math/BigInteger;
    .locals 1

    .prologue
    .line 254
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cert/AttributeCertificateHolder;->holder:Lcom/android/sec/org/bouncycastle/asn1/x509/Holder;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/x509/Holder;->getBaseCertificateID()Lcom/android/sec/org/bouncycastle/asn1/x509/IssuerSerial;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 256
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cert/AttributeCertificateHolder;->holder:Lcom/android/sec/org/bouncycastle/asn1/x509/Holder;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/x509/Holder;->getBaseCertificateID()Lcom/android/sec/org/bouncycastle/asn1/x509/IssuerSerial;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/x509/IssuerSerial;->getSerial()Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;->getValue()Ljava/math/BigInteger;

    move-result-object v0

    .line 259
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 344
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cert/AttributeCertificateHolder;->holder:Lcom/android/sec/org/bouncycastle/asn1/x509/Holder;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/x509/Holder;->hashCode()I

    move-result v0

    return v0
.end method

.method public match(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 269
    instance-of v6, p1, Lcom/android/sec/org/bouncycastle/cert/X509CertificateHolder;

    if-nez v6, :cond_1

    .line 322
    :cond_0
    :goto_0
    return v5

    :cond_1
    move-object v3, p1

    .line 274
    check-cast v3, Lcom/android/sec/org/bouncycastle/cert/X509CertificateHolder;

    .line 276
    .local v3, "x509Cert":Lcom/android/sec/org/bouncycastle/cert/X509CertificateHolder;
    iget-object v6, p0, Lcom/android/sec/org/bouncycastle/cert/AttributeCertificateHolder;->holder:Lcom/android/sec/org/bouncycastle/asn1/x509/Holder;

    invoke-virtual {v6}, Lcom/android/sec/org/bouncycastle/asn1/x509/Holder;->getBaseCertificateID()Lcom/android/sec/org/bouncycastle/asn1/x509/IssuerSerial;

    move-result-object v6

    if-eqz v6, :cond_3

    .line 278
    iget-object v6, p0, Lcom/android/sec/org/bouncycastle/cert/AttributeCertificateHolder;->holder:Lcom/android/sec/org/bouncycastle/asn1/x509/Holder;

    invoke-virtual {v6}, Lcom/android/sec/org/bouncycastle/asn1/x509/Holder;->getBaseCertificateID()Lcom/android/sec/org/bouncycastle/asn1/x509/IssuerSerial;

    move-result-object v6

    invoke-virtual {v6}, Lcom/android/sec/org/bouncycastle/asn1/x509/IssuerSerial;->getSerial()Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;

    move-result-object v6

    invoke-virtual {v6}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;->getValue()Ljava/math/BigInteger;

    move-result-object v6

    invoke-virtual {v3}, Lcom/android/sec/org/bouncycastle/cert/X509CertificateHolder;->getSerialNumber()Ljava/math/BigInteger;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/math/BigInteger;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-virtual {v3}, Lcom/android/sec/org/bouncycastle/cert/X509CertificateHolder;->getIssuer()Lcom/android/sec/org/bouncycastle/asn1/x500/X500Name;

    move-result-object v6

    iget-object v7, p0, Lcom/android/sec/org/bouncycastle/cert/AttributeCertificateHolder;->holder:Lcom/android/sec/org/bouncycastle/asn1/x509/Holder;

    invoke-virtual {v7}, Lcom/android/sec/org/bouncycastle/asn1/x509/Holder;->getBaseCertificateID()Lcom/android/sec/org/bouncycastle/asn1/x509/IssuerSerial;

    move-result-object v7

    invoke-virtual {v7}, Lcom/android/sec/org/bouncycastle/asn1/x509/IssuerSerial;->getIssuer()Lcom/android/sec/org/bouncycastle/asn1/x509/GeneralNames;

    move-result-object v7

    invoke-direct {p0, v6, v7}, Lcom/android/sec/org/bouncycastle/cert/AttributeCertificateHolder;->matchesDN(Lcom/android/sec/org/bouncycastle/asn1/x500/X500Name;Lcom/android/sec/org/bouncycastle/asn1/x509/GeneralNames;)Z

    move-result v6

    if-eqz v6, :cond_2

    :goto_1
    move v5, v4

    goto :goto_0

    :cond_2
    move v4, v5

    goto :goto_1

    .line 282
    :cond_3
    iget-object v6, p0, Lcom/android/sec/org/bouncycastle/cert/AttributeCertificateHolder;->holder:Lcom/android/sec/org/bouncycastle/asn1/x509/Holder;

    invoke-virtual {v6}, Lcom/android/sec/org/bouncycastle/asn1/x509/Holder;->getEntityName()Lcom/android/sec/org/bouncycastle/asn1/x509/GeneralNames;

    move-result-object v6

    if-eqz v6, :cond_4

    .line 284
    invoke-virtual {v3}, Lcom/android/sec/org/bouncycastle/cert/X509CertificateHolder;->getSubject()Lcom/android/sec/org/bouncycastle/asn1/x500/X500Name;

    move-result-object v6

    iget-object v7, p0, Lcom/android/sec/org/bouncycastle/cert/AttributeCertificateHolder;->holder:Lcom/android/sec/org/bouncycastle/asn1/x509/Holder;

    invoke-virtual {v7}, Lcom/android/sec/org/bouncycastle/asn1/x509/Holder;->getEntityName()Lcom/android/sec/org/bouncycastle/asn1/x509/GeneralNames;

    move-result-object v7

    invoke-direct {p0, v6, v7}, Lcom/android/sec/org/bouncycastle/cert/AttributeCertificateHolder;->matchesDN(Lcom/android/sec/org/bouncycastle/asn1/x500/X500Name;Lcom/android/sec/org/bouncycastle/asn1/x509/GeneralNames;)Z

    move-result v6

    if-eqz v6, :cond_4

    move v5, v4

    .line 287
    goto :goto_0

    .line 291
    :cond_4
    iget-object v4, p0, Lcom/android/sec/org/bouncycastle/cert/AttributeCertificateHolder;->holder:Lcom/android/sec/org/bouncycastle/asn1/x509/Holder;

    invoke-virtual {v4}, Lcom/android/sec/org/bouncycastle/asn1/x509/Holder;->getObjectDigestInfo()Lcom/android/sec/org/bouncycastle/asn1/x509/ObjectDigestInfo;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 295
    :try_start_0
    sget-object v4, Lcom/android/sec/org/bouncycastle/cert/AttributeCertificateHolder;->digestCalculatorProvider:Lcom/android/sec/org/bouncycastle/operator/DigestCalculatorProvider;

    iget-object v6, p0, Lcom/android/sec/org/bouncycastle/cert/AttributeCertificateHolder;->holder:Lcom/android/sec/org/bouncycastle/asn1/x509/Holder;

    invoke-virtual {v6}, Lcom/android/sec/org/bouncycastle/asn1/x509/Holder;->getObjectDigestInfo()Lcom/android/sec/org/bouncycastle/asn1/x509/ObjectDigestInfo;

    move-result-object v6

    invoke-virtual {v6}, Lcom/android/sec/org/bouncycastle/asn1/x509/ObjectDigestInfo;->getDigestAlgorithm()Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;

    move-result-object v6

    invoke-interface {v4, v6}, Lcom/android/sec/org/bouncycastle/operator/DigestCalculatorProvider;->get(Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;)Lcom/android/sec/org/bouncycastle/operator/DigestCalculator;

    move-result-object v0

    .line 296
    .local v0, "digCalc":Lcom/android/sec/org/bouncycastle/operator/DigestCalculator;
    invoke-interface {v0}, Lcom/android/sec/org/bouncycastle/operator/DigestCalculator;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v1

    .line 298
    .local v1, "digOut":Ljava/io/OutputStream;
    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/cert/AttributeCertificateHolder;->getDigestedObjectType()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    .line 309
    :goto_2
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V

    .line 311
    invoke-interface {v0}, Lcom/android/sec/org/bouncycastle/operator/DigestCalculator;->getDigest()[B

    move-result-object v4

    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/cert/AttributeCertificateHolder;->getObjectDigest()[B

    move-result-object v6

    invoke-static {v4, v6}, Lcom/android/sec/org/bouncycastle/util/Arrays;->areEqual([B[B)Z

    move-result v4

    if-nez v4, :cond_0

    goto/16 :goto_0

    .line 302
    :pswitch_0
    invoke-virtual {v3}, Lcom/android/sec/org/bouncycastle/cert/X509CertificateHolder;->getSubjectPublicKeyInfo()Lcom/android/sec/org/bouncycastle/asn1/x509/SubjectPublicKeyInfo;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/sec/org/bouncycastle/asn1/x509/SubjectPublicKeyInfo;->getEncoded()[B

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/io/OutputStream;->write([B)V

    goto :goto_2

    .line 316
    .end local v0    # "digCalc":Lcom/android/sec/org/bouncycastle/operator/DigestCalculator;
    .end local v1    # "digOut":Ljava/io/OutputStream;
    :catch_0
    move-exception v2

    .line 318
    .local v2, "e":Ljava/lang/Exception;
    goto/16 :goto_0

    .line 305
    .end local v2    # "e":Ljava/lang/Exception;
    .restart local v0    # "digCalc":Lcom/android/sec/org/bouncycastle/operator/DigestCalculator;
    .restart local v1    # "digOut":Ljava/io/OutputStream;
    :pswitch_1
    invoke-virtual {v3}, Lcom/android/sec/org/bouncycastle/cert/X509CertificateHolder;->getEncoded()[B

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/io/OutputStream;->write([B)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 298
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
