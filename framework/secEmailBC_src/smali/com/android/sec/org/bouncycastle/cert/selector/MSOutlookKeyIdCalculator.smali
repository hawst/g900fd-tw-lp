.class Lcom/android/sec/org/bouncycastle/cert/selector/MSOutlookKeyIdCalculator;
.super Ljava/lang/Object;
.source "MSOutlookKeyIdCalculator.java"


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static calculateKeyId(Lcom/android/sec/org/bouncycastle/asn1/x509/SubjectPublicKeyInfo;)[B
    .locals 6
    .param p0, "info"    # Lcom/android/sec/org/bouncycastle/asn1/x509/SubjectPublicKeyInfo;

    .prologue
    const/4 v5, 0x0

    .line 14
    new-instance v0, Lcom/android/sec/org/bouncycastle/crypto/digests/SHA1Digest;

    invoke-direct {v0}, Lcom/android/sec/org/bouncycastle/crypto/digests/SHA1Digest;-><init>()V

    .line 15
    .local v0, "dig":Lcom/android/sec/org/bouncycastle/crypto/Digest;
    invoke-interface {v0}, Lcom/android/sec/org/bouncycastle/crypto/Digest;->getDigestSize()I

    move-result v4

    new-array v2, v4, [B

    .line 16
    .local v2, "hash":[B
    new-array v3, v5, [B

    .line 19
    .local v3, "spkiEnc":[B
    :try_start_0
    const-string v4, "DER"

    invoke-virtual {p0, v4}, Lcom/android/sec/org/bouncycastle/asn1/x509/SubjectPublicKeyInfo;->getEncoded(Ljava/lang/String;)[B
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 27
    array-length v4, v3

    invoke-interface {v0, v3, v5, v4}, Lcom/android/sec/org/bouncycastle/crypto/Digest;->update([BII)V

    .line 29
    invoke-interface {v0, v2, v5}, Lcom/android/sec/org/bouncycastle/crypto/Digest;->doFinal([BI)I

    .line 31
    .end local v2    # "hash":[B
    :goto_0
    return-object v2

    .line 21
    .restart local v2    # "hash":[B
    :catch_0
    move-exception v1

    .line 23
    .local v1, "e":Ljava/io/IOException;
    new-array v2, v5, [B

    goto :goto_0
.end method
