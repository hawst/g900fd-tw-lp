.class public Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyEncSessionPacket;
.super Lcom/android/sec/org/bouncycastle/bcpg/ContainedPacket;
.source "PublicKeyEncSessionPacket.java"

# interfaces
.implements Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyAlgorithmTags;


# instance fields
.field private algorithm:I

.field private data:[Ljava/math/BigInteger;

.field private keyID:J

.field private version:I


# direct methods
.method public constructor <init>(JI[Ljava/math/BigInteger;)V
    .locals 1
    .param p1, "keyID"    # J
    .param p3, "algorithm"    # I
    .param p4, "data"    # [Ljava/math/BigInteger;

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/bcpg/ContainedPacket;-><init>()V

    .line 59
    const/4 v0, 0x3

    iput v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyEncSessionPacket;->version:I

    .line 60
    iput-wide p1, p0, Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyEncSessionPacket;->keyID:J

    .line 61
    iput p3, p0, Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyEncSessionPacket;->algorithm:I

    .line 62
    iput-object p4, p0, Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyEncSessionPacket;->data:[Ljava/math/BigInteger;

    .line 63
    return-void
.end method

.method constructor <init>(Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;)V
    .locals 7
    .param p1, "in"    # Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 20
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/bcpg/ContainedPacket;-><init>()V

    .line 21
    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->read()I

    move-result v0

    iput v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyEncSessionPacket;->version:I

    .line 23
    iget-wide v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyEncSessionPacket;->keyID:J

    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->read()I

    move-result v2

    int-to-long v2, v2

    const/16 v4, 0x38

    shl-long/2addr v2, v4

    or-long/2addr v0, v2

    iput-wide v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyEncSessionPacket;->keyID:J

    .line 24
    iget-wide v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyEncSessionPacket;->keyID:J

    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->read()I

    move-result v2

    int-to-long v2, v2

    const/16 v4, 0x30

    shl-long/2addr v2, v4

    or-long/2addr v0, v2

    iput-wide v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyEncSessionPacket;->keyID:J

    .line 25
    iget-wide v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyEncSessionPacket;->keyID:J

    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->read()I

    move-result v2

    int-to-long v2, v2

    const/16 v4, 0x28

    shl-long/2addr v2, v4

    or-long/2addr v0, v2

    iput-wide v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyEncSessionPacket;->keyID:J

    .line 26
    iget-wide v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyEncSessionPacket;->keyID:J

    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->read()I

    move-result v2

    int-to-long v2, v2

    const/16 v4, 0x20

    shl-long/2addr v2, v4

    or-long/2addr v0, v2

    iput-wide v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyEncSessionPacket;->keyID:J

    .line 27
    iget-wide v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyEncSessionPacket;->keyID:J

    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->read()I

    move-result v2

    int-to-long v2, v2

    const/16 v4, 0x18

    shl-long/2addr v2, v4

    or-long/2addr v0, v2

    iput-wide v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyEncSessionPacket;->keyID:J

    .line 28
    iget-wide v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyEncSessionPacket;->keyID:J

    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->read()I

    move-result v2

    int-to-long v2, v2

    const/16 v4, 0x10

    shl-long/2addr v2, v4

    or-long/2addr v0, v2

    iput-wide v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyEncSessionPacket;->keyID:J

    .line 29
    iget-wide v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyEncSessionPacket;->keyID:J

    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->read()I

    move-result v2

    int-to-long v2, v2

    const/16 v4, 0x8

    shl-long/2addr v2, v4

    or-long/2addr v0, v2

    iput-wide v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyEncSessionPacket;->keyID:J

    .line 30
    iget-wide v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyEncSessionPacket;->keyID:J

    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->read()I

    move-result v2

    int-to-long v2, v2

    or-long/2addr v0, v2

    iput-wide v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyEncSessionPacket;->keyID:J

    .line 32
    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->read()I

    move-result v0

    iput v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyEncSessionPacket;->algorithm:I

    .line 34
    iget v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyEncSessionPacket;->algorithm:I

    sparse-switch v0, :sswitch_data_0

    .line 50
    new-instance v0, Ljava/io/IOException;

    const-string v1, "unknown PGP public key algorithm encountered"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 38
    :sswitch_0
    new-array v0, v6, [Ljava/math/BigInteger;

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyEncSessionPacket;->data:[Ljava/math/BigInteger;

    .line 40
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyEncSessionPacket;->data:[Ljava/math/BigInteger;

    new-instance v1, Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;

    invoke-direct {v1, p1}, Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;-><init>(Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;)V

    invoke-virtual {v1}, Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;->getValue()Ljava/math/BigInteger;

    move-result-object v1

    aput-object v1, v0, v5

    .line 52
    :goto_0
    return-void

    .line 44
    :sswitch_1
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/math/BigInteger;

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyEncSessionPacket;->data:[Ljava/math/BigInteger;

    .line 46
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyEncSessionPacket;->data:[Ljava/math/BigInteger;

    new-instance v1, Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;

    invoke-direct {v1, p1}, Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;-><init>(Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;)V

    invoke-virtual {v1}, Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;->getValue()Ljava/math/BigInteger;

    move-result-object v1

    aput-object v1, v0, v5

    .line 47
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyEncSessionPacket;->data:[Ljava/math/BigInteger;

    new-instance v1, Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;

    invoke-direct {v1, p1}, Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;-><init>(Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;)V

    invoke-virtual {v1}, Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;->getValue()Ljava/math/BigInteger;

    move-result-object v1

    aput-object v1, v0, v6

    goto :goto_0

    .line 34
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_0
        0x10 -> :sswitch_1
        0x14 -> :sswitch_1
    .end sparse-switch
.end method


# virtual methods
.method public encode(Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;)V
    .locals 7
    .param p1, "out"    # Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    .line 89
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 90
    .local v0, "bOut":Ljava/io/ByteArrayOutputStream;
    new-instance v2, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;

    invoke-direct {v2, v0}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 92
    .local v2, "pOut":Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;
    iget v3, p0, Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyEncSessionPacket;->version:I

    invoke-virtual {v2, v3}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->write(I)V

    .line 94
    iget-wide v4, p0, Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyEncSessionPacket;->keyID:J

    const/16 v3, 0x38

    shr-long/2addr v4, v3

    long-to-int v3, v4

    int-to-byte v3, v3

    invoke-virtual {v2, v3}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->write(I)V

    .line 95
    iget-wide v4, p0, Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyEncSessionPacket;->keyID:J

    const/16 v3, 0x30

    shr-long/2addr v4, v3

    long-to-int v3, v4

    int-to-byte v3, v3

    invoke-virtual {v2, v3}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->write(I)V

    .line 96
    iget-wide v4, p0, Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyEncSessionPacket;->keyID:J

    const/16 v3, 0x28

    shr-long/2addr v4, v3

    long-to-int v3, v4

    int-to-byte v3, v3

    invoke-virtual {v2, v3}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->write(I)V

    .line 97
    iget-wide v4, p0, Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyEncSessionPacket;->keyID:J

    const/16 v3, 0x20

    shr-long/2addr v4, v3

    long-to-int v3, v4

    int-to-byte v3, v3

    invoke-virtual {v2, v3}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->write(I)V

    .line 98
    iget-wide v4, p0, Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyEncSessionPacket;->keyID:J

    const/16 v3, 0x18

    shr-long/2addr v4, v3

    long-to-int v3, v4

    int-to-byte v3, v3

    invoke-virtual {v2, v3}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->write(I)V

    .line 99
    iget-wide v4, p0, Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyEncSessionPacket;->keyID:J

    const/16 v3, 0x10

    shr-long/2addr v4, v3

    long-to-int v3, v4

    int-to-byte v3, v3

    invoke-virtual {v2, v3}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->write(I)V

    .line 100
    iget-wide v4, p0, Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyEncSessionPacket;->keyID:J

    const/16 v3, 0x8

    shr-long/2addr v4, v3

    long-to-int v3, v4

    int-to-byte v3, v3

    invoke-virtual {v2, v3}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->write(I)V

    .line 101
    iget-wide v4, p0, Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyEncSessionPacket;->keyID:J

    long-to-int v3, v4

    int-to-byte v3, v3

    invoke-virtual {v2, v3}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->write(I)V

    .line 103
    iget v3, p0, Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyEncSessionPacket;->algorithm:I

    invoke-virtual {v2, v3}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->write(I)V

    .line 105
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyEncSessionPacket;->data:[Ljava/math/BigInteger;

    array-length v3, v3

    if-eq v1, v3, :cond_0

    .line 107
    new-instance v3, Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;

    iget-object v4, p0, Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyEncSessionPacket;->data:[Ljava/math/BigInteger;

    aget-object v4, v4, v1

    invoke-direct {v3, v4}, Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;-><init>(Ljava/math/BigInteger;)V

    invoke-virtual {v2, v3}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->writeObject(Lcom/android/sec/org/bouncycastle/bcpg/BCPGObject;)V

    .line 105
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 110
    :cond_0
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v3

    invoke-virtual {p1, v6, v3, v6}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->writePacket(I[BZ)V

    .line 111
    return-void
.end method

.method public getAlgorithm()I
    .locals 1

    .prologue
    .line 77
    iget v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyEncSessionPacket;->algorithm:I

    return v0
.end method

.method public getEncSessionKey()[Ljava/math/BigInteger;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyEncSessionPacket;->data:[Ljava/math/BigInteger;

    return-object v0
.end method

.method public getKeyID()J
    .locals 2

    .prologue
    .line 72
    iget-wide v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyEncSessionPacket;->keyID:J

    return-wide v0
.end method

.method public getVersion()I
    .locals 1

    .prologue
    .line 67
    iget v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyEncSessionPacket;->version:I

    return v0
.end method
