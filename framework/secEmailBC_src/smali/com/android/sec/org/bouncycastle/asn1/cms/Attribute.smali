.class public Lcom/android/sec/org/bouncycastle/asn1/cms/Attribute;
.super Lcom/android/sec/org/bouncycastle/asn1/ASN1Object;
.source "Attribute.java"


# instance fields
.field private attrType:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

.field private attrValues:Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;


# direct methods
.method public constructor <init>(Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;)V
    .locals 0
    .param p1, "attrType"    # Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;
    .param p2, "attrValues"    # Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;

    .prologue
    .line 62
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Object;-><init>()V

    .line 63
    iput-object p1, p0, Lcom/android/sec/org/bouncycastle/asn1/cms/Attribute;->attrType:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 64
    iput-object p2, p0, Lcom/android/sec/org/bouncycastle/asn1/cms/Attribute;->attrValues:Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;

    .line 65
    return-void
.end method

.method private constructor <init>(Lcom/android/sec/org/bouncycastle/asn1/ASN1Sequence;)V
    .locals 1
    .param p1, "seq"    # Lcom/android/sec/org/bouncycastle/asn1/ASN1Sequence;

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Object;-><init>()V

    .line 44
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Sequence;->getObjectAt(I)Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;

    move-result-object v0

    check-cast v0, Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/asn1/cms/Attribute;->attrType:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 45
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Sequence;->getObjectAt(I)Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;

    move-result-object v0

    check-cast v0, Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/asn1/cms/Attribute;->attrValues:Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;

    .line 46
    return-void
.end method

.method public constructor <init>(Lcom/android/sec/org/bouncycastle/asn1/DERObjectIdentifier;Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;)V
    .locals 2
    .param p1, "attrType"    # Lcom/android/sec/org/bouncycastle/asn1/DERObjectIdentifier;
    .param p2, "attrValues"    # Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Object;-><init>()V

    .line 55
    new-instance v0, Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/asn1/DERObjectIdentifier;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/asn1/cms/Attribute;->attrType:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 56
    iput-object p2, p0, Lcom/android/sec/org/bouncycastle/asn1/cms/Attribute;->attrValues:Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;

    .line 57
    return-void
.end method

.method public static getInstance(Ljava/lang/Object;)Lcom/android/sec/org/bouncycastle/asn1/cms/Attribute;
    .locals 2
    .param p0, "o"    # Ljava/lang/Object;

    .prologue
    .line 28
    instance-of v0, p0, Lcom/android/sec/org/bouncycastle/asn1/cms/Attribute;

    if-eqz v0, :cond_0

    .line 30
    check-cast p0, Lcom/android/sec/org/bouncycastle/asn1/cms/Attribute;

    .line 38
    .end local p0    # "o":Ljava/lang/Object;
    :goto_0
    return-object p0

    .line 33
    .restart local p0    # "o":Ljava/lang/Object;
    :cond_0
    if-eqz p0, :cond_1

    .line 35
    new-instance v0, Lcom/android/sec/org/bouncycastle/asn1/cms/Attribute;

    invoke-static {p0}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Sequence;->getInstance(Ljava/lang/Object;)Lcom/android/sec/org/bouncycastle/asn1/ASN1Sequence;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/sec/org/bouncycastle/asn1/cms/Attribute;-><init>(Lcom/android/sec/org/bouncycastle/asn1/ASN1Sequence;)V

    move-object p0, v0

    goto :goto_0

    .line 38
    :cond_1
    const/4 p0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getAttrType()Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/asn1/cms/Attribute;->attrType:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    return-object v0
.end method

.method public getAttrValues()Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/asn1/cms/Attribute;->attrValues:Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;

    return-object v0
.end method

.method public getAttributeValues()[Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/asn1/cms/Attribute;->attrValues:Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;->toArray()[Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;

    move-result-object v0

    return-object v0
.end method

.method public toASN1Primitive()Lcom/android/sec/org/bouncycastle/asn1/ASN1Primitive;
    .locals 2

    .prologue
    .line 93
    new-instance v0, Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;

    invoke-direct {v0}, Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;-><init>()V

    .line 95
    .local v0, "v":Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/asn1/cms/Attribute;->attrType:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    invoke-virtual {v0, v1}, Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;->add(Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;)V

    .line 96
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/asn1/cms/Attribute;->attrValues:Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;

    invoke-virtual {v0, v1}, Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;->add(Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;)V

    .line 98
    new-instance v1, Lcom/android/sec/org/bouncycastle/asn1/DERSequence;

    invoke-direct {v1, v0}, Lcom/android/sec/org/bouncycastle/asn1/DERSequence;-><init>(Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;)V

    return-object v1
.end method
