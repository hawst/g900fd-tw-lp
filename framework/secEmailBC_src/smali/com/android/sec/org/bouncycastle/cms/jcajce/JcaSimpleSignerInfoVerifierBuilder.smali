.class public Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSimpleSignerInfoVerifierBuilder;
.super Ljava/lang/Object;
.source "JcaSimpleSignerInfoVerifierBuilder.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSimpleSignerInfoVerifierBuilder$1;,
        Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSimpleSignerInfoVerifierBuilder$ProviderHelper;,
        Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSimpleSignerInfoVerifierBuilder$NamedHelper;,
        Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSimpleSignerInfoVerifierBuilder$Helper;
    }
.end annotation


# instance fields
.field private helper:Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSimpleSignerInfoVerifierBuilder$Helper;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    new-instance v0, Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSimpleSignerInfoVerifierBuilder$Helper;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSimpleSignerInfoVerifierBuilder$Helper;-><init>(Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSimpleSignerInfoVerifierBuilder;Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSimpleSignerInfoVerifierBuilder$1;)V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSimpleSignerInfoVerifierBuilder;->helper:Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSimpleSignerInfoVerifierBuilder$Helper;

    .line 116
    return-void
.end method


# virtual methods
.method public build(Lcom/android/sec/org/bouncycastle/cert/X509CertificateHolder;)Lcom/android/sec/org/bouncycastle/cms/SignerInformationVerifier;
    .locals 5
    .param p1, "certHolder"    # Lcom/android/sec/org/bouncycastle/cert/X509CertificateHolder;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/operator/OperatorCreationException;,
            Ljava/security/cert/CertificateException;
        }
    .end annotation

    .prologue
    .line 39
    new-instance v0, Lcom/android/sec/org/bouncycastle/cms/SignerInformationVerifier;

    new-instance v1, Lcom/android/sec/org/bouncycastle/cms/DefaultCMSSignatureAlgorithmNameGenerator;

    invoke-direct {v1}, Lcom/android/sec/org/bouncycastle/cms/DefaultCMSSignatureAlgorithmNameGenerator;-><init>()V

    new-instance v2, Lcom/android/sec/org/bouncycastle/operator/DefaultSignatureAlgorithmIdentifierFinder;

    invoke-direct {v2}, Lcom/android/sec/org/bouncycastle/operator/DefaultSignatureAlgorithmIdentifierFinder;-><init>()V

    iget-object v3, p0, Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSimpleSignerInfoVerifierBuilder;->helper:Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSimpleSignerInfoVerifierBuilder$Helper;

    invoke-virtual {v3, p1}, Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSimpleSignerInfoVerifierBuilder$Helper;->createContentVerifierProvider(Lcom/android/sec/org/bouncycastle/cert/X509CertificateHolder;)Lcom/android/sec/org/bouncycastle/operator/ContentVerifierProvider;

    move-result-object v3

    iget-object v4, p0, Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSimpleSignerInfoVerifierBuilder;->helper:Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSimpleSignerInfoVerifierBuilder$Helper;

    invoke-virtual {v4}, Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSimpleSignerInfoVerifierBuilder$Helper;->createDigestCalculatorProvider()Lcom/android/sec/org/bouncycastle/operator/DigestCalculatorProvider;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/sec/org/bouncycastle/cms/SignerInformationVerifier;-><init>(Lcom/android/sec/org/bouncycastle/cms/CMSSignatureAlgorithmNameGenerator;Lcom/android/sec/org/bouncycastle/operator/SignatureAlgorithmIdentifierFinder;Lcom/android/sec/org/bouncycastle/operator/ContentVerifierProvider;Lcom/android/sec/org/bouncycastle/operator/DigestCalculatorProvider;)V

    return-object v0
.end method

.method public build(Ljava/security/PublicKey;)Lcom/android/sec/org/bouncycastle/cms/SignerInformationVerifier;
    .locals 5
    .param p1, "pubKey"    # Ljava/security/PublicKey;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/operator/OperatorCreationException;
        }
    .end annotation

    .prologue
    .line 51
    new-instance v0, Lcom/android/sec/org/bouncycastle/cms/SignerInformationVerifier;

    new-instance v1, Lcom/android/sec/org/bouncycastle/cms/DefaultCMSSignatureAlgorithmNameGenerator;

    invoke-direct {v1}, Lcom/android/sec/org/bouncycastle/cms/DefaultCMSSignatureAlgorithmNameGenerator;-><init>()V

    new-instance v2, Lcom/android/sec/org/bouncycastle/operator/DefaultSignatureAlgorithmIdentifierFinder;

    invoke-direct {v2}, Lcom/android/sec/org/bouncycastle/operator/DefaultSignatureAlgorithmIdentifierFinder;-><init>()V

    iget-object v3, p0, Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSimpleSignerInfoVerifierBuilder;->helper:Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSimpleSignerInfoVerifierBuilder$Helper;

    invoke-virtual {v3, p1}, Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSimpleSignerInfoVerifierBuilder$Helper;->createContentVerifierProvider(Ljava/security/PublicKey;)Lcom/android/sec/org/bouncycastle/operator/ContentVerifierProvider;

    move-result-object v3

    iget-object v4, p0, Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSimpleSignerInfoVerifierBuilder;->helper:Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSimpleSignerInfoVerifierBuilder$Helper;

    invoke-virtual {v4}, Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSimpleSignerInfoVerifierBuilder$Helper;->createDigestCalculatorProvider()Lcom/android/sec/org/bouncycastle/operator/DigestCalculatorProvider;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/sec/org/bouncycastle/cms/SignerInformationVerifier;-><init>(Lcom/android/sec/org/bouncycastle/cms/CMSSignatureAlgorithmNameGenerator;Lcom/android/sec/org/bouncycastle/operator/SignatureAlgorithmIdentifierFinder;Lcom/android/sec/org/bouncycastle/operator/ContentVerifierProvider;Lcom/android/sec/org/bouncycastle/operator/DigestCalculatorProvider;)V

    return-object v0
.end method

.method public build(Ljava/security/cert/X509Certificate;)Lcom/android/sec/org/bouncycastle/cms/SignerInformationVerifier;
    .locals 5
    .param p1, "certificate"    # Ljava/security/cert/X509Certificate;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/operator/OperatorCreationException;
        }
    .end annotation

    .prologue
    .line 45
    new-instance v0, Lcom/android/sec/org/bouncycastle/cms/SignerInformationVerifier;

    new-instance v1, Lcom/android/sec/org/bouncycastle/cms/DefaultCMSSignatureAlgorithmNameGenerator;

    invoke-direct {v1}, Lcom/android/sec/org/bouncycastle/cms/DefaultCMSSignatureAlgorithmNameGenerator;-><init>()V

    new-instance v2, Lcom/android/sec/org/bouncycastle/operator/DefaultSignatureAlgorithmIdentifierFinder;

    invoke-direct {v2}, Lcom/android/sec/org/bouncycastle/operator/DefaultSignatureAlgorithmIdentifierFinder;-><init>()V

    iget-object v3, p0, Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSimpleSignerInfoVerifierBuilder;->helper:Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSimpleSignerInfoVerifierBuilder$Helper;

    invoke-virtual {v3, p1}, Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSimpleSignerInfoVerifierBuilder$Helper;->createContentVerifierProvider(Ljava/security/cert/X509Certificate;)Lcom/android/sec/org/bouncycastle/operator/ContentVerifierProvider;

    move-result-object v3

    iget-object v4, p0, Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSimpleSignerInfoVerifierBuilder;->helper:Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSimpleSignerInfoVerifierBuilder$Helper;

    invoke-virtual {v4}, Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSimpleSignerInfoVerifierBuilder$Helper;->createDigestCalculatorProvider()Lcom/android/sec/org/bouncycastle/operator/DigestCalculatorProvider;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/sec/org/bouncycastle/cms/SignerInformationVerifier;-><init>(Lcom/android/sec/org/bouncycastle/cms/CMSSignatureAlgorithmNameGenerator;Lcom/android/sec/org/bouncycastle/operator/SignatureAlgorithmIdentifierFinder;Lcom/android/sec/org/bouncycastle/operator/ContentVerifierProvider;Lcom/android/sec/org/bouncycastle/operator/DigestCalculatorProvider;)V

    return-object v0
.end method

.method public setProvider(Ljava/lang/String;)Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSimpleSignerInfoVerifierBuilder;
    .locals 1
    .param p1, "providerName"    # Ljava/lang/String;

    .prologue
    .line 31
    new-instance v0, Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSimpleSignerInfoVerifierBuilder$NamedHelper;

    invoke-direct {v0, p0, p1}, Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSimpleSignerInfoVerifierBuilder$NamedHelper;-><init>(Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSimpleSignerInfoVerifierBuilder;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSimpleSignerInfoVerifierBuilder;->helper:Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSimpleSignerInfoVerifierBuilder$Helper;

    .line 33
    return-object p0
.end method

.method public setProvider(Ljava/security/Provider;)Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSimpleSignerInfoVerifierBuilder;
    .locals 1
    .param p1, "provider"    # Ljava/security/Provider;

    .prologue
    .line 24
    new-instance v0, Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSimpleSignerInfoVerifierBuilder$ProviderHelper;

    invoke-direct {v0, p0, p1}, Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSimpleSignerInfoVerifierBuilder$ProviderHelper;-><init>(Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSimpleSignerInfoVerifierBuilder;Ljava/security/Provider;)V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSimpleSignerInfoVerifierBuilder;->helper:Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSimpleSignerInfoVerifierBuilder$Helper;

    .line 26
    return-object p0
.end method
