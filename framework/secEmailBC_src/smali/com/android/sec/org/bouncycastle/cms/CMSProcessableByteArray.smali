.class public Lcom/android/sec/org/bouncycastle/cms/CMSProcessableByteArray;
.super Ljava/lang/Object;
.source "CMSProcessableByteArray.java"

# interfaces
.implements Lcom/android/sec/org/bouncycastle/cms/CMSReadable;
.implements Lcom/android/sec/org/bouncycastle/cms/CMSTypedData;


# instance fields
.field private final bytes:[B

.field private final type:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;


# direct methods
.method public constructor <init>(Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;[B)V
    .locals 0
    .param p1, "type"    # Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;
    .param p2, "bytes"    # [B

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/android/sec/org/bouncycastle/cms/CMSProcessableByteArray;->type:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 32
    iput-object p2, p0, Lcom/android/sec/org/bouncycastle/cms/CMSProcessableByteArray;->bytes:[B

    .line 33
    return-void
.end method

.method public constructor <init>([B)V
    .locals 2
    .param p1, "bytes"    # [B

    .prologue
    .line 24
    new-instance v0, Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    sget-object v1, Lcom/android/sec/org/bouncycastle/asn1/cms/CMSObjectIdentifiers;->data:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    invoke-virtual {v1}, Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0, p1}, Lcom/android/sec/org/bouncycastle/cms/CMSProcessableByteArray;-><init>(Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;[B)V

    .line 25
    return-void
.end method


# virtual methods
.method public getContent()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/CMSProcessableByteArray;->bytes:[B

    invoke-static {v0}, Lcom/android/sec/org/bouncycastle/util/Arrays;->clone([B)[B

    move-result-object v0

    return-object v0
.end method

.method public getContentType()Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/CMSProcessableByteArray;->type:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    return-object v0
.end method

.method public getInputStream()Ljava/io/InputStream;
    .locals 2

    .prologue
    .line 37
    new-instance v0, Ljava/io/ByteArrayInputStream;

    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/cms/CMSProcessableByteArray;->bytes:[B

    invoke-direct {v0, v1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    return-object v0
.end method

.method public write(Ljava/io/OutputStream;)V
    .locals 1
    .param p1, "zOut"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/android/sec/org/bouncycastle/cms/CMSException;
        }
    .end annotation

    .prologue
    .line 43
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/CMSProcessableByteArray;->bytes:[B

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 44
    return-void
.end method
