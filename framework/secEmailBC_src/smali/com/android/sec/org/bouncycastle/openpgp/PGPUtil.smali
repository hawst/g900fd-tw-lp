.class public Lcom/android/sec/org/bouncycastle/openpgp/PGPUtil;
.super Ljava/lang/Object;
.source "PGPUtil.java"

# interfaces
.implements Lcom/android/sec/org/bouncycastle/bcpg/HashAlgorithmTags;


# static fields
.field private static final READ_AHEAD:I = 0x3c

.field private static defProvider:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    const-string v0, "emailBC"

    sput-object v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPUtil;->defProvider:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static dsaSigToMpi([B)[Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;
    .locals 10
    .param p0, "encoding"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
        }
    .end annotation

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 66
    new-instance v0, Lcom/android/sec/org/bouncycastle/asn1/ASN1InputStream;

    invoke-direct {v0, p0}, Lcom/android/sec/org/bouncycastle/asn1/ASN1InputStream;-><init>([B)V

    .line 73
    .local v0, "aIn":Lcom/android/sec/org/bouncycastle/asn1/ASN1InputStream;
    :try_start_0
    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/ASN1InputStream;->readObject()Lcom/android/sec/org/bouncycastle/asn1/ASN1Primitive;

    move-result-object v4

    check-cast v4, Lcom/android/sec/org/bouncycastle/asn1/ASN1Sequence;

    .line 75
    .local v4, "s":Lcom/android/sec/org/bouncycastle/asn1/ASN1Sequence;
    const/4 v6, 0x0

    invoke-virtual {v4, v6}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Sequence;->getObjectAt(I)Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;

    move-result-object v2

    check-cast v2, Lcom/android/sec/org/bouncycastle/asn1/DERInteger;

    .line 76
    .local v2, "i1":Lcom/android/sec/org/bouncycastle/asn1/DERInteger;
    const/4 v6, 0x1

    invoke-virtual {v4, v6}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Sequence;->getObjectAt(I)Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;

    move-result-object v3

    check-cast v3, Lcom/android/sec/org/bouncycastle/asn1/DERInteger;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 83
    .local v3, "i2":Lcom/android/sec/org/bouncycastle/asn1/DERInteger;
    const/4 v6, 0x2

    new-array v5, v6, [Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;

    .line 85
    .local v5, "values":[Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;
    new-instance v6, Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;

    invoke-virtual {v2}, Lcom/android/sec/org/bouncycastle/asn1/DERInteger;->getValue()Ljava/math/BigInteger;

    move-result-object v7

    invoke-direct {v6, v7}, Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;-><init>(Ljava/math/BigInteger;)V

    aput-object v6, v5, v8

    .line 86
    new-instance v6, Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;

    invoke-virtual {v3}, Lcom/android/sec/org/bouncycastle/asn1/DERInteger;->getValue()Ljava/math/BigInteger;

    move-result-object v7

    invoke-direct {v6, v7}, Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;-><init>(Ljava/math/BigInteger;)V

    aput-object v6, v5, v9

    .line 88
    return-object v5

    .line 78
    .end local v2    # "i1":Lcom/android/sec/org/bouncycastle/asn1/DERInteger;
    .end local v3    # "i2":Lcom/android/sec/org/bouncycastle/asn1/DERInteger;
    .end local v4    # "s":Lcom/android/sec/org/bouncycastle/asn1/ASN1Sequence;
    .end local v5    # "values":[Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;
    :catch_0
    move-exception v1

    .line 80
    .local v1, "e":Ljava/io/IOException;
    new-instance v6, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;

    const-string v7, "exception encoding signature"

    invoke-direct {v6, v7, v1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v6
.end method

.method public static getDecoderStream(Ljava/io/InputStream;)Ljava/io/InputStream;
    .locals 11
    .param p0, "in"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v10, 0x3c

    const/4 v9, 0x0

    .line 547
    invoke-virtual {p0}, Ljava/io/InputStream;->markSupported()Z

    move-result v8

    if-nez v8, :cond_0

    .line 549
    new-instance v5, Ljava/io/BufferedInputStream;

    invoke-direct {v5, p0}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    .end local p0    # "in":Ljava/io/InputStream;
    .local v5, "in":Ljava/io/InputStream;
    move-object p0, v5

    .line 552
    .end local v5    # "in":Ljava/io/InputStream;
    .restart local p0    # "in":Ljava/io/InputStream;
    :cond_0
    invoke-virtual {p0, v10}, Ljava/io/InputStream;->mark(I)V

    .line 554
    invoke-virtual {p0}, Ljava/io/InputStream;->read()I

    move-result v1

    .line 557
    .local v1, "ch":I
    and-int/lit16 v8, v1, 0x80

    if-eqz v8, :cond_1

    .line 559
    invoke-virtual {p0}, Ljava/io/InputStream;->reset()V

    .line 621
    .end local p0    # "in":Ljava/io/InputStream;
    :goto_0
    return-object p0

    .line 565
    .restart local p0    # "in":Ljava/io/InputStream;
    :cond_1
    invoke-static {v1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPUtil;->isPossiblyBase64(I)Z

    move-result v8

    if-nez v8, :cond_2

    .line 567
    invoke-virtual {p0}, Ljava/io/InputStream;->reset()V

    .line 569
    new-instance v8, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;

    invoke-direct {v8, p0}, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;-><init>(Ljava/io/InputStream;)V

    move-object p0, v8

    goto :goto_0

    .line 572
    :cond_2
    new-array v0, v10, [B

    .line 573
    .local v0, "buf":[B
    const/4 v2, 0x1

    .line 574
    .local v2, "count":I
    const/4 v6, 0x1

    .line 576
    .local v6, "index":I
    int-to-byte v8, v1

    aput-byte v8, v0, v9

    move v7, v6

    .line 577
    .end local v6    # "index":I
    .local v7, "index":I
    :goto_1
    if-eq v2, v10, :cond_4

    invoke-virtual {p0}, Ljava/io/InputStream;->read()I

    move-result v1

    if-ltz v1, :cond_4

    .line 579
    invoke-static {v1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPUtil;->isPossiblyBase64(I)Z

    move-result v8

    if-nez v8, :cond_3

    .line 581
    invoke-virtual {p0}, Ljava/io/InputStream;->reset()V

    .line 583
    new-instance v8, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;

    invoke-direct {v8, p0}, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;-><init>(Ljava/io/InputStream;)V

    move-object p0, v8

    goto :goto_0

    .line 586
    :cond_3
    const/16 v8, 0xa

    if-eq v1, v8, :cond_7

    const/16 v8, 0xd

    if-eq v1, v8, :cond_7

    .line 588
    add-int/lit8 v6, v7, 0x1

    .end local v7    # "index":I
    .restart local v6    # "index":I
    int-to-byte v8, v1

    aput-byte v8, v0, v7

    .line 591
    :goto_2
    add-int/lit8 v2, v2, 0x1

    move v7, v6

    .end local v6    # "index":I
    .restart local v7    # "index":I
    goto :goto_1

    .line 594
    :cond_4
    invoke-virtual {p0}, Ljava/io/InputStream;->reset()V

    .line 599
    const/4 v8, 0x4

    if-ge v2, v8, :cond_5

    .line 601
    new-instance v8, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;

    invoke-direct {v8, p0}, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;-><init>(Ljava/io/InputStream;)V

    move-object p0, v8

    goto :goto_0

    .line 607
    :cond_5
    const/16 v8, 0x8

    new-array v4, v8, [B

    .line 609
    .local v4, "firstBlock":[B
    array-length v8, v4

    invoke-static {v0, v9, v4, v9, v8}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 611
    invoke-static {v4}, Lcom/android/sec/org/bouncycastle/util/encoders/Base64;->decode([B)[B

    move-result-object v3

    .line 616
    .local v3, "decoded":[B
    aget-byte v8, v3, v9

    and-int/lit16 v8, v8, 0x80

    if-eqz v8, :cond_6

    .line 618
    new-instance v8, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;

    invoke-direct {v8, p0, v9}, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;-><init>(Ljava/io/InputStream;Z)V

    move-object p0, v8

    goto :goto_0

    .line 621
    :cond_6
    new-instance v8, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;

    invoke-direct {v8, p0}, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;-><init>(Ljava/io/InputStream;)V

    move-object p0, v8

    goto :goto_0

    .end local v3    # "decoded":[B
    .end local v4    # "firstBlock":[B
    :cond_7
    move v6, v7

    .end local v7    # "index":I
    .restart local v6    # "index":I
    goto :goto_2
.end method

.method public static getDefaultProvider()Ljava/lang/String;
    .locals 1

    .prologue
    .line 47
    sget-object v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPUtil;->defProvider:Ljava/lang/String;

    return-object v0
.end method

.method static getDigestInstance(Ljava/lang/String;Ljava/security/Provider;)Ljava/security/MessageDigest;
    .locals 2
    .param p0, "digestName"    # Ljava/lang/String;
    .param p1, "provider"    # Ljava/security/Provider;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/NoSuchAlgorithmException;
        }
    .end annotation

    .prologue
    .line 446
    :try_start_0
    invoke-static {p0, p1}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;Ljava/security/Provider;)Ljava/security/MessageDigest;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 451
    :goto_0
    return-object v1

    .line 448
    :catch_0
    move-exception v0

    .line 451
    .local v0, "e":Ljava/security/NoSuchAlgorithmException;
    invoke-static {p0}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v1

    goto :goto_0
.end method

.method static getDigestName(I)Ljava/lang/String;
    .locals 3
    .param p0, "hashAlgorithm"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
        }
    .end annotation

    .prologue
    .line 95
    packed-switch p0, :pswitch_data_0

    .line 114
    :pswitch_0
    new-instance v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "unknown hash algorithm tag in getDigestName: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 98
    :pswitch_1
    const-string v0, "SHA1"

    .line 112
    :goto_0
    return-object v0

    .line 100
    :pswitch_2
    const-string v0, "MD2"

    goto :goto_0

    .line 102
    :pswitch_3
    const-string v0, "MD5"

    goto :goto_0

    .line 104
    :pswitch_4
    const-string v0, "RIPEMD160"

    goto :goto_0

    .line 106
    :pswitch_5
    const-string v0, "SHA256"

    goto :goto_0

    .line 108
    :pswitch_6
    const-string v0, "SHA384"

    goto :goto_0

    .line 110
    :pswitch_7
    const-string v0, "SHA512"

    goto :goto_0

    .line 112
    :pswitch_8
    const-string v0, "SHA224"

    goto :goto_0

    .line 95
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_1
        :pswitch_4
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method static getProvider(Ljava/lang/String;)Ljava/security/Provider;
    .locals 4
    .param p0, "providerName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/NoSuchProviderException;
        }
    .end annotation

    .prologue
    .line 628
    invoke-static {p0}, Ljava/security/Security;->getProvider(Ljava/lang/String;)Ljava/security/Provider;

    move-result-object v0

    .line 630
    .local v0, "prov":Ljava/security/Provider;
    if-nez v0, :cond_0

    .line 632
    new-instance v1, Ljava/security/NoSuchProviderException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "provider "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " not found."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/security/NoSuchProviderException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 635
    :cond_0
    return-object v0
.end method

.method private static getS2kDigestName(Lcom/android/sec/org/bouncycastle/bcpg/S2K;)Ljava/lang/String;
    .locals 3
    .param p0, "s2k"    # Lcom/android/sec/org/bouncycastle/bcpg/S2K;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
        }
    .end annotation

    .prologue
    .line 457
    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/bcpg/S2K;->getHashAlgorithm()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 464
    new-instance v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "unknown hash algorithm: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/bcpg/S2K;->getHashAlgorithm()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 460
    :pswitch_0
    const-string v0, "MD5"

    .line 462
    :goto_0
    return-object v0

    :pswitch_1
    const-string v0, "SHA1"

    goto :goto_0

    .line 457
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method static getSignatureName(II)Ljava/lang/String;
    .locals 4
    .param p0, "keyAlgorithm"    # I
    .param p1, "hashAlgorithm"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
        }
    .end annotation

    .prologue
    .line 125
    sparse-switch p0, :sswitch_data_0

    .line 139
    new-instance v1, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "unknown algorithm tag in signature:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 129
    :sswitch_0
    const-string v0, "RSA"

    .line 142
    .local v0, "encAlg":Ljava/lang/String;
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPUtil;->getDigestName(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "with"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 132
    .end local v0    # "encAlg":Ljava/lang/String;
    :sswitch_1
    const-string v0, "DSA"

    .line 133
    .restart local v0    # "encAlg":Ljava/lang/String;
    goto :goto_0

    .line 136
    .end local v0    # "encAlg":Ljava/lang/String;
    :sswitch_2
    const-string v0, "ElGamal"

    .line 137
    .restart local v0    # "encAlg":Ljava/lang/String;
    goto :goto_0

    .line 125
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x3 -> :sswitch_0
        0x10 -> :sswitch_2
        0x11 -> :sswitch_1
        0x14 -> :sswitch_2
    .end sparse-switch
.end method

.method static getSymmetricCipherName(I)Ljava/lang/String;
    .locals 3
    .param p0, "algorithm"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
        }
    .end annotation

    .prologue
    .line 149
    packed-switch p0, :pswitch_data_0

    .line 174
    new-instance v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "unknown symmetric algorithm: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 152
    :pswitch_0
    const/4 v0, 0x0

    .line 172
    :goto_0
    return-object v0

    .line 154
    :pswitch_1
    const-string v0, "DESEDE"

    goto :goto_0

    .line 156
    :pswitch_2
    const-string v0, "IDEA"

    goto :goto_0

    .line 158
    :pswitch_3
    const-string v0, "CAST5"

    goto :goto_0

    .line 160
    :pswitch_4
    const-string v0, "Blowfish"

    goto :goto_0

    .line 162
    :pswitch_5
    const-string v0, "SAFER"

    goto :goto_0

    .line 164
    :pswitch_6
    const-string v0, "DES"

    goto :goto_0

    .line 166
    :pswitch_7
    const-string v0, "AES"

    goto :goto_0

    .line 168
    :pswitch_8
    const-string v0, "AES"

    goto :goto_0

    .line 170
    :pswitch_9
    const-string v0, "AES"

    goto :goto_0

    .line 172
    :pswitch_a
    const-string v0, "Twofish"

    goto :goto_0

    .line 149
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
    .end packed-switch
.end method

.method private static isPossiblyBase64(I)Z
    .locals 1
    .param p0, "ch"    # I

    .prologue
    .line 530
    const/16 v0, 0x41

    if-lt p0, v0, :cond_0

    const/16 v0, 0x5a

    if-le p0, v0, :cond_3

    :cond_0
    const/16 v0, 0x61

    if-lt p0, v0, :cond_1

    const/16 v0, 0x7a

    if-le p0, v0, :cond_3

    :cond_1
    const/16 v0, 0x30

    if-lt p0, v0, :cond_2

    const/16 v0, 0x39

    if-le p0, v0, :cond_3

    :cond_2
    const/16 v0, 0x2b

    if-eq p0, v0, :cond_3

    const/16 v0, 0x2f

    if-eq p0, v0, :cond_3

    const/16 v0, 0xd

    if-eq p0, v0, :cond_3

    const/16 v0, 0xa

    if-ne p0, v0, :cond_4

    :cond_3
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static makeKeyFromPassPhrase(ILcom/android/sec/org/bouncycastle/bcpg/S2K;[CLjava/lang/String;)Ljavax/crypto/SecretKey;
    .locals 2
    .param p0, "algorithm"    # I
    .param p1, "s2k"    # Lcom/android/sec/org/bouncycastle/bcpg/S2K;
    .param p2, "passPhrase"    # [C
    .param p3, "provider"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;,
            Ljava/security/NoSuchProviderException;
        }
    .end annotation

    .prologue
    .line 255
    invoke-static {p3}, Lcom/android/sec/org/bouncycastle/openpgp/PGPUtil;->getProvider(Ljava/lang/String;)Ljava/security/Provider;

    move-result-object v0

    .line 257
    .local v0, "prov":Ljava/security/Provider;
    invoke-static {p0, p1, p2, v0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPUtil;->makeKeyFromPassPhrase(ILcom/android/sec/org/bouncycastle/bcpg/S2K;[CLjava/security/Provider;)Ljavax/crypto/SecretKey;

    move-result-object v1

    return-object v1
.end method

.method public static makeKeyFromPassPhrase(ILcom/android/sec/org/bouncycastle/bcpg/S2K;[CLjava/security/Provider;)Ljavax/crypto/SecretKey;
    .locals 19
    .param p0, "algorithm"    # I
    .param p1, "s2k"    # Lcom/android/sec/org/bouncycastle/bcpg/S2K;
    .param p2, "passPhrase"    # [C
    .param p3, "provider"    # Ljava/security/Provider;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;,
            Ljava/security/NoSuchProviderException;
        }
    .end annotation

    .prologue
    .line 267
    const/4 v2, 0x0

    .line 268
    .local v2, "algName":Ljava/lang/String;
    const/4 v13, 0x0

    .line 270
    .local v13, "keySize":I
    packed-switch p0, :pswitch_data_0

    .line 313
    new-instance v16, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "unknown symmetric algorithm: "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    move/from16 v1, p0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-direct/range {v16 .. v17}, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;-><init>(Ljava/lang/String;)V

    throw v16

    .line 273
    :pswitch_0
    const/16 v13, 0xc0

    .line 274
    const-string v2, "DES_EDE"

    .line 316
    :goto_0
    move-object/from16 v0, p2

    array-length v0, v0

    move/from16 v16, v0

    move/from16 v0, v16

    new-array v15, v0, [B

    .line 319
    .local v15, "pBytes":[B
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_1
    move-object/from16 v0, p2

    array-length v0, v0

    move/from16 v16, v0

    move/from16 v0, v16

    if-eq v10, v0, :cond_0

    .line 321
    aget-char v16, p2, v10

    move/from16 v0, v16

    int-to-byte v0, v0

    move/from16 v16, v0

    aput-byte v16, v15, v10

    .line 319
    add-int/lit8 v10, v10, 0x1

    goto :goto_1

    .line 277
    .end local v10    # "i":I
    .end local v15    # "pBytes":[B
    :pswitch_1
    const/16 v13, 0x80

    .line 278
    const-string v2, "IDEA"

    .line 279
    goto :goto_0

    .line 281
    :pswitch_2
    const/16 v13, 0x80

    .line 282
    const-string v2, "CAST5"

    .line 283
    goto :goto_0

    .line 285
    :pswitch_3
    const/16 v13, 0x80

    .line 286
    const-string v2, "Blowfish"

    .line 287
    goto :goto_0

    .line 289
    :pswitch_4
    const/16 v13, 0x80

    .line 290
    const-string v2, "SAFER"

    .line 291
    goto :goto_0

    .line 293
    :pswitch_5
    const/16 v13, 0x40

    .line 294
    const-string v2, "DES"

    .line 295
    goto :goto_0

    .line 297
    :pswitch_6
    const/16 v13, 0x80

    .line 298
    const-string v2, "AES"

    .line 299
    goto :goto_0

    .line 301
    :pswitch_7
    const/16 v13, 0xc0

    .line 302
    const-string v2, "AES"

    .line 303
    goto :goto_0

    .line 305
    :pswitch_8
    const/16 v13, 0x100

    .line 306
    const-string v2, "AES"

    .line 307
    goto :goto_0

    .line 309
    :pswitch_9
    const/16 v13, 0x100

    .line 310
    const-string v2, "Twofish"

    .line 311
    goto :goto_0

    .line 324
    .restart local v10    # "i":I
    .restart local v15    # "pBytes":[B
    :cond_0
    add-int/lit8 v16, v13, 0x7

    div-int/lit8 v16, v16, 0x8

    move/from16 v0, v16

    new-array v12, v0, [B

    .line 326
    .local v12, "keyBytes":[B
    const/4 v9, 0x0

    .line 327
    .local v9, "generatedBytes":I
    const/4 v14, 0x0

    .line 329
    .local v14, "loopCount":I
    :goto_2
    array-length v0, v12

    move/from16 v16, v0

    move/from16 v0, v16

    if-ge v9, v0, :cond_8

    .line 331
    if-eqz p1, :cond_5

    .line 333
    invoke-static/range {p1 .. p1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPUtil;->getS2kDigestName(Lcom/android/sec/org/bouncycastle/bcpg/S2K;)Ljava/lang/String;

    move-result-object v7

    .line 337
    .local v7, "digestName":Ljava/lang/String;
    :try_start_0
    move-object/from16 v0, p3

    invoke-static {v7, v0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPUtil;->getDigestInstance(Ljava/lang/String;Ljava/security/Provider;)Ljava/security/MessageDigest;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 344
    .local v6, "digest":Ljava/security/MessageDigest;
    const/4 v10, 0x0

    :goto_3
    if-eq v10, v14, :cond_1

    .line 346
    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-virtual {v6, v0}, Ljava/security/MessageDigest;->update(B)V

    .line 344
    add-int/lit8 v10, v10, 0x1

    goto :goto_3

    .line 339
    .end local v6    # "digest":Ljava/security/MessageDigest;
    :catch_0
    move-exception v8

    .line 341
    .local v8, "e":Ljava/security/NoSuchAlgorithmException;
    new-instance v16, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;

    const-string v17, "can\'t find S2K digest"

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-direct {v0, v1, v8}, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v16

    .line 349
    .end local v8    # "e":Ljava/security/NoSuchAlgorithmException;
    .restart local v6    # "digest":Ljava/security/MessageDigest;
    :cond_1
    invoke-virtual/range {p1 .. p1}, Lcom/android/sec/org/bouncycastle/bcpg/S2K;->getIV()[B

    move-result-object v11

    .line 351
    .local v11, "iv":[B
    invoke-virtual/range {p1 .. p1}, Lcom/android/sec/org/bouncycastle/bcpg/S2K;->getType()I

    move-result v16

    packed-switch v16, :pswitch_data_1

    .line 393
    :pswitch_a
    new-instance v16, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "unknown S2K type: "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {p1 .. p1}, Lcom/android/sec/org/bouncycastle/bcpg/S2K;->getType()I

    move-result v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-direct/range {v16 .. v17}, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;-><init>(Ljava/lang/String;)V

    throw v16

    .line 354
    :pswitch_b
    invoke-virtual {v6, v15}, Ljava/security/MessageDigest;->update([B)V

    .line 415
    .end local v7    # "digestName":Ljava/lang/String;
    .end local v11    # "iv":[B
    :cond_2
    :goto_4
    invoke-virtual {v6}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v3

    .line 417
    .local v3, "dig":[B
    array-length v0, v3

    move/from16 v16, v0

    array-length v0, v12

    move/from16 v17, v0

    sub-int v17, v17, v9

    move/from16 v0, v16

    move/from16 v1, v17

    if-le v0, v1, :cond_7

    .line 419
    const/16 v16, 0x0

    array-length v0, v12

    move/from16 v17, v0

    sub-int v17, v17, v9

    move/from16 v0, v16

    move/from16 v1, v17

    invoke-static {v3, v0, v12, v9, v1}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 426
    :goto_5
    array-length v0, v3

    move/from16 v16, v0

    add-int v9, v9, v16

    .line 428
    add-int/lit8 v14, v14, 0x1

    .line 429
    goto :goto_2

    .line 357
    .end local v3    # "dig":[B
    .restart local v7    # "digestName":Ljava/lang/String;
    .restart local v11    # "iv":[B
    :pswitch_c
    invoke-virtual {v6, v11}, Ljava/security/MessageDigest;->update([B)V

    .line 358
    invoke-virtual {v6, v15}, Ljava/security/MessageDigest;->update([B)V

    goto :goto_4

    .line 361
    :pswitch_d
    invoke-virtual/range {p1 .. p1}, Lcom/android/sec/org/bouncycastle/bcpg/S2K;->getIterationCount()J

    move-result-wide v4

    .line 362
    .local v4, "count":J
    invoke-virtual {v6, v11}, Ljava/security/MessageDigest;->update([B)V

    .line 363
    invoke-virtual {v6, v15}, Ljava/security/MessageDigest;->update([B)V

    .line 365
    array-length v0, v11

    move/from16 v16, v0

    array-length v0, v15

    move/from16 v17, v0

    add-int v16, v16, v17

    move/from16 v0, v16

    int-to-long v0, v0

    move-wide/from16 v16, v0

    sub-long v4, v4, v16

    .line 367
    :goto_6
    const-wide/16 v16, 0x0

    cmp-long v16, v4, v16

    if-lez v16, :cond_2

    .line 369
    array-length v0, v11

    move/from16 v16, v0

    move/from16 v0, v16

    int-to-long v0, v0

    move-wide/from16 v16, v0

    cmp-long v16, v4, v16

    if-gez v16, :cond_3

    .line 371
    const/16 v16, 0x0

    long-to-int v0, v4

    move/from16 v17, v0

    move/from16 v0, v16

    move/from16 v1, v17

    invoke-virtual {v6, v11, v0, v1}, Ljava/security/MessageDigest;->update([BII)V

    goto :goto_4

    .line 376
    :cond_3
    invoke-virtual {v6, v11}, Ljava/security/MessageDigest;->update([B)V

    .line 377
    array-length v0, v11

    move/from16 v16, v0

    move/from16 v0, v16

    int-to-long v0, v0

    move-wide/from16 v16, v0

    sub-long v4, v4, v16

    .line 380
    array-length v0, v15

    move/from16 v16, v0

    move/from16 v0, v16

    int-to-long v0, v0

    move-wide/from16 v16, v0

    cmp-long v16, v4, v16

    if-gez v16, :cond_4

    .line 382
    const/16 v16, 0x0

    long-to-int v0, v4

    move/from16 v17, v0

    move/from16 v0, v16

    move/from16 v1, v17

    invoke-virtual {v6, v15, v0, v1}, Ljava/security/MessageDigest;->update([BII)V

    .line 383
    const-wide/16 v4, 0x0

    goto :goto_6

    .line 387
    :cond_4
    invoke-virtual {v6, v15}, Ljava/security/MessageDigest;->update([B)V

    .line 388
    array-length v0, v15

    move/from16 v16, v0

    move/from16 v0, v16

    int-to-long v0, v0

    move-wide/from16 v16, v0

    sub-long v4, v4, v16

    goto :goto_6

    .line 400
    .end local v4    # "count":J
    .end local v6    # "digest":Ljava/security/MessageDigest;
    .end local v7    # "digestName":Ljava/lang/String;
    .end local v11    # "iv":[B
    :cond_5
    :try_start_1
    const-string v16, "MD5"

    move-object/from16 v0, v16

    move-object/from16 v1, p3

    invoke-static {v0, v1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPUtil;->getDigestInstance(Ljava/lang/String;Ljava/security/Provider;)Ljava/security/MessageDigest;
    :try_end_1
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v6

    .line 407
    .restart local v6    # "digest":Ljava/security/MessageDigest;
    const/4 v10, 0x0

    :goto_7
    if-eq v10, v14, :cond_6

    .line 409
    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-virtual {v6, v0}, Ljava/security/MessageDigest;->update(B)V

    .line 407
    add-int/lit8 v10, v10, 0x1

    goto :goto_7

    .line 402
    .end local v6    # "digest":Ljava/security/MessageDigest;
    :catch_1
    move-exception v8

    .line 404
    .restart local v8    # "e":Ljava/security/NoSuchAlgorithmException;
    new-instance v16, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;

    const-string v17, "can\'t find MD5 digest"

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-direct {v0, v1, v8}, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v16

    .line 412
    .end local v8    # "e":Ljava/security/NoSuchAlgorithmException;
    .restart local v6    # "digest":Ljava/security/MessageDigest;
    :cond_6
    invoke-virtual {v6, v15}, Ljava/security/MessageDigest;->update([B)V

    goto/16 :goto_4

    .line 423
    .restart local v3    # "dig":[B
    :cond_7
    const/16 v16, 0x0

    array-length v0, v3

    move/from16 v17, v0

    move/from16 v0, v16

    move/from16 v1, v17

    invoke-static {v3, v0, v12, v9, v1}, Ljava/lang/System;->arraycopy([BI[BII)V

    goto/16 :goto_5

    .line 431
    .end local v3    # "dig":[B
    .end local v6    # "digest":Ljava/security/MessageDigest;
    :cond_8
    const/4 v10, 0x0

    :goto_8
    array-length v0, v15

    move/from16 v16, v0

    move/from16 v0, v16

    if-eq v10, v0, :cond_9

    .line 433
    const/16 v16, 0x0

    aput-byte v16, v15, v10

    .line 431
    add-int/lit8 v10, v10, 0x1

    goto :goto_8

    .line 436
    :cond_9
    new-instance v16, Ljavax/crypto/spec/SecretKeySpec;

    move-object/from16 v0, v16

    invoke-direct {v0, v12, v2}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    return-object v16

    .line 270
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch

    .line 351
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_b
        :pswitch_c
        :pswitch_a
        :pswitch_d
    .end packed-switch
.end method

.method public static makeKeyFromPassPhrase(I[CLjava/lang/String;)Ljavax/crypto/SecretKey;
    .locals 1
    .param p0, "algorithm"    # I
    .param p1, "passPhrase"    # [C
    .param p2, "provider"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/NoSuchProviderException;,
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
        }
    .end annotation

    .prologue
    .line 245
    const/4 v0, 0x0

    invoke-static {p0, v0, p1, p2}, Lcom/android/sec/org/bouncycastle/openpgp/PGPUtil;->makeKeyFromPassPhrase(ILcom/android/sec/org/bouncycastle/bcpg/S2K;[CLjava/lang/String;)Ljavax/crypto/SecretKey;

    move-result-object v0

    return-object v0
.end method

.method public static makeRandomKey(ILjava/security/SecureRandom;)Ljavax/crypto/SecretKey;
    .locals 6
    .param p0, "algorithm"    # I
    .param p1, "random"    # Ljava/security/SecureRandom;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
        }
    .end annotation

    .prologue
    .line 183
    const/4 v0, 0x0

    .line 184
    .local v0, "algName":Ljava/lang/String;
    const/4 v2, 0x0

    .line 186
    .local v2, "keySize":I
    packed-switch p0, :pswitch_data_0

    .line 229
    new-instance v3, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "unknown symmetric algorithm: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 189
    :pswitch_0
    const/16 v2, 0xc0

    .line 190
    const-string v0, "DES_EDE"

    .line 232
    :goto_0
    add-int/lit8 v3, v2, 0x7

    div-int/lit8 v3, v3, 0x8

    new-array v1, v3, [B

    .line 234
    .local v1, "keyBytes":[B
    invoke-virtual {p1, v1}, Ljava/security/SecureRandom;->nextBytes([B)V

    .line 236
    new-instance v3, Ljavax/crypto/spec/SecretKeySpec;

    invoke-direct {v3, v1, v0}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    return-object v3

    .line 193
    .end local v1    # "keyBytes":[B
    :pswitch_1
    const/16 v2, 0x80

    .line 194
    const-string v0, "IDEA"

    .line 195
    goto :goto_0

    .line 197
    :pswitch_2
    const/16 v2, 0x80

    .line 198
    const-string v0, "CAST5"

    .line 199
    goto :goto_0

    .line 201
    :pswitch_3
    const/16 v2, 0x80

    .line 202
    const-string v0, "Blowfish"

    .line 203
    goto :goto_0

    .line 205
    :pswitch_4
    const/16 v2, 0x80

    .line 206
    const-string v0, "SAFER"

    .line 207
    goto :goto_0

    .line 209
    :pswitch_5
    const/16 v2, 0x40

    .line 210
    const-string v0, "DES"

    .line 211
    goto :goto_0

    .line 213
    :pswitch_6
    const/16 v2, 0x80

    .line 214
    const-string v0, "AES"

    .line 215
    goto :goto_0

    .line 217
    :pswitch_7
    const/16 v2, 0xc0

    .line 218
    const-string v0, "AES"

    .line 219
    goto :goto_0

    .line 221
    :pswitch_8
    const/16 v2, 0x100

    .line 222
    const-string v0, "AES"

    .line 223
    goto :goto_0

    .line 225
    :pswitch_9
    const/16 v2, 0x100

    .line 226
    const-string v0, "Twofish"

    .line 227
    goto :goto_0

    .line 186
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method public static setDefaultProvider(Ljava/lang/String;)V
    .locals 0
    .param p0, "provider"    # Ljava/lang/String;

    .prologue
    .line 59
    sput-object p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPUtil;->defProvider:Ljava/lang/String;

    .line 60
    return-void
.end method

.method public static writeFileToLiteralData(Ljava/io/OutputStream;CLjava/io/File;)V
    .locals 16
    .param p0, "out"    # Ljava/io/OutputStream;
    .param p1, "fileType"    # C
    .param p2, "file"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 479
    new-instance v2, Lcom/android/sec/org/bouncycastle/openpgp/PGPLiteralDataGenerator;

    invoke-direct {v2}, Lcom/android/sec/org/bouncycastle/openpgp/PGPLiteralDataGenerator;-><init>()V

    .line 480
    .local v2, "lData":Lcom/android/sec/org/bouncycastle/openpgp/PGPLiteralDataGenerator;
    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->length()J

    move-result-wide v6

    new-instance v8, Ljava/util/Date;

    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->lastModified()J

    move-result-wide v14

    invoke-direct {v8, v14, v15}, Ljava/util/Date;-><init>(J)V

    move-object/from16 v3, p0

    move/from16 v4, p1

    invoke-virtual/range {v2 .. v8}, Lcom/android/sec/org/bouncycastle/openpgp/PGPLiteralDataGenerator;->open(Ljava/io/OutputStream;CLjava/lang/String;JLjava/util/Date;)Ljava/io/OutputStream;

    move-result-object v12

    .line 482
    .local v12, "pOut":Ljava/io/OutputStream;
    new-instance v10, Ljava/io/FileInputStream;

    move-object/from16 v0, p2

    invoke-direct {v10, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 483
    .local v10, "in":Ljava/io/FileInputStream;
    const/16 v3, 0x1000

    new-array v9, v3, [B

    .line 487
    .local v9, "buf":[B
    :goto_0
    :try_start_0
    invoke-virtual {v10, v9}, Ljava/io/FileInputStream;->read([B)I

    move-result v11

    .local v11, "len":I
    if-lez v11, :cond_0

    .line 488
    const/4 v3, 0x0

    invoke-virtual {v12, v9, v3, v11}, Ljava/io/OutputStream;->write([BII)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 491
    .end local v11    # "len":I
    :catchall_0
    move-exception v3

    invoke-virtual {v2}, Lcom/android/sec/org/bouncycastle/openpgp/PGPLiteralDataGenerator;->close()V

    .line 492
    invoke-virtual {v10}, Ljava/io/FileInputStream;->close()V

    throw v3

    .line 491
    .restart local v11    # "len":I
    :cond_0
    invoke-virtual {v2}, Lcom/android/sec/org/bouncycastle/openpgp/PGPLiteralDataGenerator;->close()V

    .line 492
    invoke-virtual {v10}, Ljava/io/FileInputStream;->close()V

    .line 494
    return-void
.end method

.method public static writeFileToLiteralData(Ljava/io/OutputStream;CLjava/io/File;[B)V
    .locals 12
    .param p0, "out"    # Ljava/io/OutputStream;
    .param p1, "fileType"    # C
    .param p2, "file"    # Ljava/io/File;
    .param p3, "buffer"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 508
    new-instance v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPLiteralDataGenerator;

    invoke-direct {v0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPLiteralDataGenerator;-><init>()V

    .line 509
    .local v0, "lData":Lcom/android/sec/org/bouncycastle/openpgp/PGPLiteralDataGenerator;
    invoke-virtual {p2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/util/Date;

    invoke-virtual {p2}, Ljava/io/File;->lastModified()J

    move-result-wide v10

    invoke-direct {v4, v10, v11}, Ljava/util/Date;-><init>(J)V

    move-object v1, p0

    move v2, p1

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Lcom/android/sec/org/bouncycastle/openpgp/PGPLiteralDataGenerator;->open(Ljava/io/OutputStream;CLjava/lang/String;Ljava/util/Date;[B)Ljava/io/OutputStream;

    move-result-object v9

    .line 511
    .local v9, "pOut":Ljava/io/OutputStream;
    new-instance v7, Ljava/io/FileInputStream;

    invoke-direct {v7, p2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 512
    .local v7, "in":Ljava/io/FileInputStream;
    array-length v1, p3

    new-array v6, v1, [B

    .line 516
    .local v6, "buf":[B
    :goto_0
    :try_start_0
    invoke-virtual {v7, v6}, Ljava/io/FileInputStream;->read([B)I

    move-result v8

    .local v8, "len":I
    if-lez v8, :cond_0

    .line 517
    const/4 v1, 0x0

    invoke-virtual {v9, v6, v1, v8}, Ljava/io/OutputStream;->write([BII)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 520
    .end local v8    # "len":I
    :catchall_0
    move-exception v1

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPLiteralDataGenerator;->close()V

    .line 521
    invoke-virtual {v7}, Ljava/io/FileInputStream;->close()V

    throw v1

    .line 520
    .restart local v8    # "len":I
    :cond_0
    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPLiteralDataGenerator;->close()V

    .line 521
    invoke-virtual {v7}, Ljava/io/FileInputStream;->close()V

    .line 523
    return-void
.end method
