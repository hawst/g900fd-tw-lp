.class public interface abstract Lcom/android/sec/org/bouncycastle/asn1/cms/CMSAttributes;
.super Ljava/lang/Object;
.source "CMSAttributes.java"


# static fields
.field public static final contentHint:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

.field public static final contentType:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

.field public static final counterSignature:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

.field public static final messageDigest:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

.field public static final signingTime:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 8
    sget-object v0, Lcom/android/sec/org/bouncycastle/asn1/pkcs/PKCSObjectIdentifiers;->pkcs_9_at_contentType:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    sput-object v0, Lcom/android/sec/org/bouncycastle/asn1/cms/CMSAttributes;->contentType:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 9
    sget-object v0, Lcom/android/sec/org/bouncycastle/asn1/pkcs/PKCSObjectIdentifiers;->pkcs_9_at_messageDigest:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    sput-object v0, Lcom/android/sec/org/bouncycastle/asn1/cms/CMSAttributes;->messageDigest:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 10
    sget-object v0, Lcom/android/sec/org/bouncycastle/asn1/pkcs/PKCSObjectIdentifiers;->pkcs_9_at_signingTime:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    sput-object v0, Lcom/android/sec/org/bouncycastle/asn1/cms/CMSAttributes;->signingTime:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 11
    sget-object v0, Lcom/android/sec/org/bouncycastle/asn1/pkcs/PKCSObjectIdentifiers;->pkcs_9_at_counterSignature:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    sput-object v0, Lcom/android/sec/org/bouncycastle/asn1/cms/CMSAttributes;->counterSignature:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 12
    sget-object v0, Lcom/android/sec/org/bouncycastle/asn1/pkcs/PKCSObjectIdentifiers;->id_aa_contentHint:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    sput-object v0, Lcom/android/sec/org/bouncycastle/asn1/cms/CMSAttributes;->contentHint:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    return-void
.end method
