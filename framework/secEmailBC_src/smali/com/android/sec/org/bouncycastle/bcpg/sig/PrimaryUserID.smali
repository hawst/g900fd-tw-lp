.class public Lcom/android/sec/org/bouncycastle/bcpg/sig/PrimaryUserID;
.super Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;
.source "PrimaryUserID.java"


# direct methods
.method public constructor <init>(ZZ)V
    .locals 2
    .param p1, "critical"    # Z
    .param p2, "isPrimaryUserID"    # Z

    .prologue
    .line 39
    const/16 v0, 0x19

    invoke-static {p2}, Lcom/android/sec/org/bouncycastle/bcpg/sig/PrimaryUserID;->booleanToByteArray(Z)[B

    move-result-object v1

    invoke-direct {p0, v0, p1, v1}, Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;-><init>(IZ[B)V

    .line 40
    return-void
.end method

.method public constructor <init>(Z[B)V
    .locals 1
    .param p1, "critical"    # Z
    .param p2, "data"    # [B

    .prologue
    .line 32
    const/16 v0, 0x19

    invoke-direct {p0, v0, p1, p2}, Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;-><init>(IZ[B)V

    .line 33
    return-void
.end method

.method private static booleanToByteArray(Z)[B
    .locals 3
    .param p0, "value"    # Z

    .prologue
    const/4 v2, 0x1

    .line 15
    new-array v0, v2, [B

    .line 17
    .local v0, "data":[B
    if-eqz p0, :cond_0

    .line 19
    const/4 v1, 0x0

    aput-byte v2, v0, v1

    .line 24
    :cond_0
    return-object v0
.end method


# virtual methods
.method public isPrimaryUserID()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 44
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/bcpg/sig/PrimaryUserID;->data:[B

    aget-byte v1, v1, v0

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method
