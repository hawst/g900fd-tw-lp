.class Lcom/android/sec/org/bouncycastle/cms/CMSSignedData$1;
.super Ljava/lang/Object;
.source "CMSSignedData.java"

# interfaces
.implements Lcom/android/sec/org/bouncycastle/cms/CMSTypedData;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;-><init>(Lcom/android/sec/org/bouncycastle/cms/CMSProcessable;Lcom/android/sec/org/bouncycastle/asn1/cms/ContentInfo;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;

.field final synthetic val$signedContent:Lcom/android/sec/org/bouncycastle/cms/CMSProcessable;


# direct methods
.method constructor <init>(Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;Lcom/android/sec/org/bouncycastle/cms/CMSProcessable;)V
    .locals 0

    .prologue
    .line 153
    iput-object p1, p0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedData$1;->this$0:Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;

    iput-object p2, p0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedData$1;->val$signedContent:Lcom/android/sec/org/bouncycastle/cms/CMSProcessable;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getContent()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 167
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedData$1;->val$signedContent:Lcom/android/sec/org/bouncycastle/cms/CMSProcessable;

    invoke-interface {v0}, Lcom/android/sec/org/bouncycastle/cms/CMSProcessable;->getContent()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getContentType()Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;
    .locals 1

    .prologue
    .line 156
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedData$1;->this$0:Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;->signedData:Lcom/android/sec/org/bouncycastle/asn1/cms/SignedData;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/cms/SignedData;->getEncapContentInfo()Lcom/android/sec/org/bouncycastle/asn1/cms/ContentInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/cms/ContentInfo;->getContentType()Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    move-result-object v0

    return-object v0
.end method

.method public write(Ljava/io/OutputStream;)V
    .locals 1
    .param p1, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/android/sec/org/bouncycastle/cms/CMSException;
        }
    .end annotation

    .prologue
    .line 162
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedData$1;->val$signedContent:Lcom/android/sec/org/bouncycastle/cms/CMSProcessable;

    invoke-interface {v0, p1}, Lcom/android/sec/org/bouncycastle/cms/CMSProcessable;->write(Ljava/io/OutputStream;)V

    .line 163
    return-void
.end method
