.class public Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;
.super Ljava/lang/Object;
.source "CMSSignedData.java"


# static fields
.field private static final HELPER:Lcom/android/sec/org/bouncycastle/cms/CMSSignedHelper;


# instance fields
.field attributeStore:Lcom/android/sec/org/bouncycastle/x509/X509Store;

.field certificateStore:Lcom/android/sec/org/bouncycastle/x509/X509Store;

.field contentInfo:Lcom/android/sec/org/bouncycastle/asn1/cms/ContentInfo;

.field crlStore:Lcom/android/sec/org/bouncycastle/x509/X509Store;

.field private hashes:Ljava/util/Map;

.field signedContent:Lcom/android/sec/org/bouncycastle/cms/CMSTypedData;

.field signedData:Lcom/android/sec/org/bouncycastle/asn1/cms/SignedData;

.field signerInfoStore:Lcom/android/sec/org/bouncycastle/cms/SignerInformationStore;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 68
    sget-object v0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedHelper;->INSTANCE:Lcom/android/sec/org/bouncycastle/cms/CMSSignedHelper;

    sput-object v0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;->HELPER:Lcom/android/sec/org/bouncycastle/cms/CMSSignedHelper;

    return-void
.end method

.method public constructor <init>(Lcom/android/sec/org/bouncycastle/asn1/cms/ContentInfo;)V
    .locals 3
    .param p1, "sigData"    # Lcom/android/sec/org/bouncycastle/asn1/cms/ContentInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/cms/CMSException;
        }
    .end annotation

    .prologue
    .line 189
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 190
    iput-object p1, p0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;->contentInfo:Lcom/android/sec/org/bouncycastle/asn1/cms/ContentInfo;

    .line 191
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;->getSignedData()Lcom/android/sec/org/bouncycastle/asn1/cms/SignedData;

    move-result-object v0

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;->signedData:Lcom/android/sec/org/bouncycastle/asn1/cms/SignedData;

    .line 197
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;->signedData:Lcom/android/sec/org/bouncycastle/asn1/cms/SignedData;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/cms/SignedData;->getEncapContentInfo()Lcom/android/sec/org/bouncycastle/asn1/cms/ContentInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/cms/ContentInfo;->getContent()Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 199
    new-instance v1, Lcom/android/sec/org/bouncycastle/cms/CMSProcessableByteArray;

    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;->signedData:Lcom/android/sec/org/bouncycastle/asn1/cms/SignedData;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/cms/SignedData;->getEncapContentInfo()Lcom/android/sec/org/bouncycastle/asn1/cms/ContentInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/cms/ContentInfo;->getContentType()Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    move-result-object v2

    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;->signedData:Lcom/android/sec/org/bouncycastle/asn1/cms/SignedData;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/cms/SignedData;->getEncapContentInfo()Lcom/android/sec/org/bouncycastle/asn1/cms/ContentInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/cms/ContentInfo;->getContent()Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;

    move-result-object v0

    check-cast v0, Lcom/android/sec/org/bouncycastle/asn1/ASN1OctetString;

    check-cast v0, Lcom/android/sec/org/bouncycastle/asn1/ASN1OctetString;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/ASN1OctetString;->getOctets()[B

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lcom/android/sec/org/bouncycastle/cms/CMSProcessableByteArray;-><init>(Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;[B)V

    iput-object v1, p0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;->signedContent:Lcom/android/sec/org/bouncycastle/cms/CMSTypedData;

    .line 207
    :goto_0
    return-void

    .line 205
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;->signedContent:Lcom/android/sec/org/bouncycastle/cms/CMSTypedData;

    goto :goto_0
.end method

.method public constructor <init>(Lcom/android/sec/org/bouncycastle/cms/CMSProcessable;Lcom/android/sec/org/bouncycastle/asn1/cms/ContentInfo;)V
    .locals 1
    .param p1, "signedContent"    # Lcom/android/sec/org/bouncycastle/cms/CMSProcessable;
    .param p2, "sigData"    # Lcom/android/sec/org/bouncycastle/asn1/cms/ContentInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/cms/CMSException;
        }
    .end annotation

    .prologue
    .line 145
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 146
    instance-of v0, p1, Lcom/android/sec/org/bouncycastle/cms/CMSTypedData;

    if-eqz v0, :cond_0

    .line 148
    check-cast p1, Lcom/android/sec/org/bouncycastle/cms/CMSTypedData;

    .end local p1    # "signedContent":Lcom/android/sec/org/bouncycastle/cms/CMSProcessable;
    iput-object p1, p0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;->signedContent:Lcom/android/sec/org/bouncycastle/cms/CMSTypedData;

    .line 172
    :goto_0
    iput-object p2, p0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;->contentInfo:Lcom/android/sec/org/bouncycastle/asn1/cms/ContentInfo;

    .line 173
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;->getSignedData()Lcom/android/sec/org/bouncycastle/asn1/cms/SignedData;

    move-result-object v0

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;->signedData:Lcom/android/sec/org/bouncycastle/asn1/cms/SignedData;

    .line 174
    return-void

    .line 152
    .restart local p1    # "signedContent":Lcom/android/sec/org/bouncycastle/cms/CMSProcessable;
    :cond_0
    new-instance v0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedData$1;

    invoke-direct {v0, p0, p1}, Lcom/android/sec/org/bouncycastle/cms/CMSSignedData$1;-><init>(Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;Lcom/android/sec/org/bouncycastle/cms/CMSProcessable;)V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;->signedContent:Lcom/android/sec/org/bouncycastle/cms/CMSTypedData;

    goto :goto_0
.end method

.method public constructor <init>(Lcom/android/sec/org/bouncycastle/cms/CMSProcessable;Ljava/io/InputStream;)V
    .locals 1
    .param p1, "signedContent"    # Lcom/android/sec/org/bouncycastle/cms/CMSProcessable;
    .param p2, "sigData"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/cms/CMSException;
        }
    .end annotation

    .prologue
    .line 128
    new-instance v0, Lcom/android/sec/org/bouncycastle/asn1/ASN1InputStream;

    invoke-direct {v0, p2}, Lcom/android/sec/org/bouncycastle/asn1/ASN1InputStream;-><init>(Ljava/io/InputStream;)V

    invoke-static {v0}, Lcom/android/sec/org/bouncycastle/cms/CMSUtils;->readContentInfo(Ljava/io/InputStream;)Lcom/android/sec/org/bouncycastle/asn1/cms/ContentInfo;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;-><init>(Lcom/android/sec/org/bouncycastle/cms/CMSProcessable;Lcom/android/sec/org/bouncycastle/asn1/cms/ContentInfo;)V

    .line 129
    return-void
.end method

.method public constructor <init>(Lcom/android/sec/org/bouncycastle/cms/CMSProcessable;[B)V
    .locals 1
    .param p1, "signedContent"    # Lcom/android/sec/org/bouncycastle/cms/CMSProcessable;
    .param p2, "sigBlock"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/cms/CMSException;
        }
    .end annotation

    .prologue
    .line 100
    invoke-static {p2}, Lcom/android/sec/org/bouncycastle/cms/CMSUtils;->readContentInfo([B)Lcom/android/sec/org/bouncycastle/asn1/cms/ContentInfo;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;-><init>(Lcom/android/sec/org/bouncycastle/cms/CMSProcessable;Lcom/android/sec/org/bouncycastle/asn1/cms/ContentInfo;)V

    .line 101
    return-void
.end method

.method private constructor <init>(Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;)V
    .locals 1
    .param p1, "c"    # Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;

    .prologue
    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82
    iget-object v0, p1, Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;->signedData:Lcom/android/sec/org/bouncycastle/asn1/cms/SignedData;

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;->signedData:Lcom/android/sec/org/bouncycastle/asn1/cms/SignedData;

    .line 83
    iget-object v0, p1, Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;->contentInfo:Lcom/android/sec/org/bouncycastle/asn1/cms/ContentInfo;

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;->contentInfo:Lcom/android/sec/org/bouncycastle/asn1/cms/ContentInfo;

    .line 84
    iget-object v0, p1, Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;->signedContent:Lcom/android/sec/org/bouncycastle/cms/CMSTypedData;

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;->signedContent:Lcom/android/sec/org/bouncycastle/cms/CMSTypedData;

    .line 85
    iget-object v0, p1, Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;->signerInfoStore:Lcom/android/sec/org/bouncycastle/cms/SignerInformationStore;

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;->signerInfoStore:Lcom/android/sec/org/bouncycastle/cms/SignerInformationStore;

    .line 86
    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 1
    .param p1, "sigData"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/cms/CMSException;
        }
    .end annotation

    .prologue
    .line 138
    invoke-static {p1}, Lcom/android/sec/org/bouncycastle/cms/CMSUtils;->readContentInfo(Ljava/io/InputStream;)Lcom/android/sec/org/bouncycastle/asn1/cms/ContentInfo;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;-><init>(Lcom/android/sec/org/bouncycastle/asn1/cms/ContentInfo;)V

    .line 139
    return-void
.end method

.method public constructor <init>(Ljava/util/Map;Lcom/android/sec/org/bouncycastle/asn1/cms/ContentInfo;)V
    .locals 1
    .param p1, "hashes"    # Ljava/util/Map;
    .param p2, "sigData"    # Lcom/android/sec/org/bouncycastle/asn1/cms/ContentInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/cms/CMSException;
        }
    .end annotation

    .prologue
    .line 180
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 181
    iput-object p1, p0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;->hashes:Ljava/util/Map;

    .line 182
    iput-object p2, p0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;->contentInfo:Lcom/android/sec/org/bouncycastle/asn1/cms/ContentInfo;

    .line 183
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;->getSignedData()Lcom/android/sec/org/bouncycastle/asn1/cms/SignedData;

    move-result-object v0

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;->signedData:Lcom/android/sec/org/bouncycastle/asn1/cms/SignedData;

    .line 184
    return-void
.end method

.method public constructor <init>(Ljava/util/Map;[B)V
    .locals 1
    .param p1, "hashes"    # Ljava/util/Map;
    .param p2, "sigBlock"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/cms/CMSException;
        }
    .end annotation

    .prologue
    .line 114
    invoke-static {p2}, Lcom/android/sec/org/bouncycastle/cms/CMSUtils;->readContentInfo([B)Lcom/android/sec/org/bouncycastle/asn1/cms/ContentInfo;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;-><init>(Ljava/util/Map;Lcom/android/sec/org/bouncycastle/asn1/cms/ContentInfo;)V

    .line 115
    return-void
.end method

.method public constructor <init>([B)V
    .locals 1
    .param p1, "sigBlock"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/cms/CMSException;
        }
    .end annotation

    .prologue
    .line 92
    invoke-static {p1}, Lcom/android/sec/org/bouncycastle/cms/CMSUtils;->readContentInfo([B)Lcom/android/sec/org/bouncycastle/asn1/cms/ContentInfo;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;-><init>(Lcom/android/sec/org/bouncycastle/asn1/cms/ContentInfo;)V

    .line 93
    return-void
.end method

.method private getSignedData()Lcom/android/sec/org/bouncycastle/asn1/cms/SignedData;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/cms/CMSException;
        }
    .end annotation

    .prologue
    .line 214
    :try_start_0
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;->contentInfo:Lcom/android/sec/org/bouncycastle/asn1/cms/ContentInfo;

    invoke-virtual {v1}, Lcom/android/sec/org/bouncycastle/asn1/cms/ContentInfo;->getContent()Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;

    move-result-object v1

    invoke-static {v1}, Lcom/android/sec/org/bouncycastle/asn1/cms/SignedData;->getInstance(Ljava/lang/Object;)Lcom/android/sec/org/bouncycastle/asn1/cms/SignedData;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v1

    return-object v1

    .line 216
    :catch_0
    move-exception v0

    .line 218
    .local v0, "e":Ljava/lang/ClassCastException;
    new-instance v1, Lcom/android/sec/org/bouncycastle/cms/CMSException;

    const-string v2, "Malformed content."

    invoke-direct {v1, v2, v0}, Lcom/android/sec/org/bouncycastle/cms/CMSException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v1

    .line 220
    .end local v0    # "e":Ljava/lang/ClassCastException;
    :catch_1
    move-exception v0

    .line 222
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    new-instance v1, Lcom/android/sec/org/bouncycastle/cms/CMSException;

    const-string v2, "Malformed content."

    invoke-direct {v1, v2, v0}, Lcom/android/sec/org/bouncycastle/cms/CMSException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v1
.end method

.method public static replaceCertificatesAndCRLs(Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;Lcom/android/sec/org/bouncycastle/util/Store;Lcom/android/sec/org/bouncycastle/util/Store;Lcom/android/sec/org/bouncycastle/util/Store;)Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;
    .locals 9
    .param p0, "signedData"    # Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;
    .param p1, "certificates"    # Lcom/android/sec/org/bouncycastle/util/Store;
    .param p2, "attrCerts"    # Lcom/android/sec/org/bouncycastle/util/Store;
    .param p3, "crls"    # Lcom/android/sec/org/bouncycastle/util/Store;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/cms/CMSException;
        }
    .end annotation

    .prologue
    .line 772
    new-instance v7, Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;

    invoke-direct {v7, p0}, Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;-><init>(Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;)V

    .line 777
    .local v7, "cms":Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;
    const/4 v3, 0x0

    .line 778
    .local v3, "certSet":Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;
    const/4 v4, 0x0

    .line 780
    .local v4, "crlSet":Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;
    if-nez p1, :cond_0

    if-eqz p2, :cond_3

    .line 782
    :cond_0
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 784
    .local v6, "certs":Ljava/util/List;
    if-eqz p1, :cond_1

    .line 786
    invoke-static {p1}, Lcom/android/sec/org/bouncycastle/cms/CMSUtils;->getCertificatesFromStore(Lcom/android/sec/org/bouncycastle/util/Store;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 788
    :cond_1
    if-eqz p2, :cond_2

    .line 790
    invoke-static {p2}, Lcom/android/sec/org/bouncycastle/cms/CMSUtils;->getAttributeCertificatesFromStore(Lcom/android/sec/org/bouncycastle/util/Store;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 793
    :cond_2
    invoke-static {v6}, Lcom/android/sec/org/bouncycastle/cms/CMSUtils;->createBerSetFromList(Ljava/util/List;)Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;

    move-result-object v8

    .line 795
    .local v8, "set":Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;
    invoke-virtual {v8}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;->size()I

    move-result v0

    if-eqz v0, :cond_3

    .line 797
    move-object v3, v8

    .line 801
    .end local v6    # "certs":Ljava/util/List;
    .end local v8    # "set":Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;
    :cond_3
    if-eqz p3, :cond_4

    .line 803
    invoke-static {p3}, Lcom/android/sec/org/bouncycastle/cms/CMSUtils;->getCRLsFromStore(Lcom/android/sec/org/bouncycastle/util/Store;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/android/sec/org/bouncycastle/cms/CMSUtils;->createBerSetFromList(Ljava/util/List;)Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;

    move-result-object v8

    .line 805
    .restart local v8    # "set":Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;
    invoke-virtual {v8}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;->size()I

    move-result v0

    if-eqz v0, :cond_4

    .line 807
    move-object v4, v8

    .line 814
    .end local v8    # "set":Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;
    :cond_4
    new-instance v0, Lcom/android/sec/org/bouncycastle/asn1/cms/SignedData;

    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;->signedData:Lcom/android/sec/org/bouncycastle/asn1/cms/SignedData;

    invoke-virtual {v1}, Lcom/android/sec/org/bouncycastle/asn1/cms/SignedData;->getDigestAlgorithms()Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;

    move-result-object v1

    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;->signedData:Lcom/android/sec/org/bouncycastle/asn1/cms/SignedData;

    invoke-virtual {v2}, Lcom/android/sec/org/bouncycastle/asn1/cms/SignedData;->getEncapContentInfo()Lcom/android/sec/org/bouncycastle/asn1/cms/ContentInfo;

    move-result-object v2

    iget-object v5, p0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;->signedData:Lcom/android/sec/org/bouncycastle/asn1/cms/SignedData;

    invoke-virtual {v5}, Lcom/android/sec/org/bouncycastle/asn1/cms/SignedData;->getSignerInfos()Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/android/sec/org/bouncycastle/asn1/cms/SignedData;-><init>(Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;Lcom/android/sec/org/bouncycastle/asn1/cms/ContentInfo;Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;)V

    iput-object v0, v7, Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;->signedData:Lcom/android/sec/org/bouncycastle/asn1/cms/SignedData;

    .line 823
    new-instance v0, Lcom/android/sec/org/bouncycastle/asn1/cms/ContentInfo;

    iget-object v1, v7, Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;->contentInfo:Lcom/android/sec/org/bouncycastle/asn1/cms/ContentInfo;

    invoke-virtual {v1}, Lcom/android/sec/org/bouncycastle/asn1/cms/ContentInfo;->getContentType()Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    move-result-object v1

    iget-object v2, v7, Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;->signedData:Lcom/android/sec/org/bouncycastle/asn1/cms/SignedData;

    invoke-direct {v0, v1, v2}, Lcom/android/sec/org/bouncycastle/asn1/cms/ContentInfo;-><init>(Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;)V

    iput-object v0, v7, Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;->contentInfo:Lcom/android/sec/org/bouncycastle/asn1/cms/ContentInfo;

    .line 825
    return-object v7
.end method

.method public static replaceCertificatesAndCRLs(Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;Ljava/security/cert/CertStore;)Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;
    .locals 9
    .param p0, "signedData"    # Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;
    .param p1, "certsAndCrls"    # Ljava/security/cert/CertStore;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/cms/CMSException;
        }
    .end annotation

    .prologue
    .line 698
    new-instance v6, Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;

    invoke-direct {v6, p0}, Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;-><init>(Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;)V

    .line 703
    .local v6, "cms":Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;
    const/4 v3, 0x0

    .line 704
    .local v3, "certs":Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;
    const/4 v4, 0x0

    .line 708
    .local v4, "crls":Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;
    :try_start_0
    invoke-static {p1}, Lcom/android/sec/org/bouncycastle/cms/CMSUtils;->getCertificatesFromStore(Ljava/security/cert/CertStore;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/android/sec/org/bouncycastle/cms/CMSUtils;->createBerSetFromList(Ljava/util/List;)Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;

    move-result-object v8

    .line 710
    .local v8, "set":Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;
    invoke-virtual {v8}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;->size()I
    :try_end_0
    .catch Ljava/security/cert/CertStoreException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    if-eqz v0, :cond_0

    .line 712
    move-object v3, v8

    .line 722
    :cond_0
    :try_start_1
    invoke-static {p1}, Lcom/android/sec/org/bouncycastle/cms/CMSUtils;->getCRLsFromStore(Ljava/security/cert/CertStore;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/android/sec/org/bouncycastle/cms/CMSUtils;->createBerSetFromList(Ljava/util/List;)Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;

    move-result-object v8

    .line 724
    invoke-virtual {v8}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;->size()I
    :try_end_1
    .catch Ljava/security/cert/CertStoreException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v0

    if-eqz v0, :cond_1

    .line 726
    move-object v4, v8

    .line 737
    :cond_1
    new-instance v0, Lcom/android/sec/org/bouncycastle/asn1/cms/SignedData;

    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;->signedData:Lcom/android/sec/org/bouncycastle/asn1/cms/SignedData;

    invoke-virtual {v1}, Lcom/android/sec/org/bouncycastle/asn1/cms/SignedData;->getDigestAlgorithms()Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;

    move-result-object v1

    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;->signedData:Lcom/android/sec/org/bouncycastle/asn1/cms/SignedData;

    invoke-virtual {v2}, Lcom/android/sec/org/bouncycastle/asn1/cms/SignedData;->getEncapContentInfo()Lcom/android/sec/org/bouncycastle/asn1/cms/ContentInfo;

    move-result-object v2

    iget-object v5, p0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;->signedData:Lcom/android/sec/org/bouncycastle/asn1/cms/SignedData;

    invoke-virtual {v5}, Lcom/android/sec/org/bouncycastle/asn1/cms/SignedData;->getSignerInfos()Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/android/sec/org/bouncycastle/asn1/cms/SignedData;-><init>(Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;Lcom/android/sec/org/bouncycastle/asn1/cms/ContentInfo;Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;)V

    iput-object v0, v6, Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;->signedData:Lcom/android/sec/org/bouncycastle/asn1/cms/SignedData;

    .line 746
    new-instance v0, Lcom/android/sec/org/bouncycastle/asn1/cms/ContentInfo;

    iget-object v1, v6, Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;->contentInfo:Lcom/android/sec/org/bouncycastle/asn1/cms/ContentInfo;

    invoke-virtual {v1}, Lcom/android/sec/org/bouncycastle/asn1/cms/ContentInfo;->getContentType()Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    move-result-object v1

    iget-object v2, v6, Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;->signedData:Lcom/android/sec/org/bouncycastle/asn1/cms/SignedData;

    invoke-direct {v0, v1, v2}, Lcom/android/sec/org/bouncycastle/asn1/cms/ContentInfo;-><init>(Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;)V

    iput-object v0, v6, Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;->contentInfo:Lcom/android/sec/org/bouncycastle/asn1/cms/ContentInfo;

    .line 748
    return-object v6

    .line 715
    .end local v8    # "set":Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;
    :catch_0
    move-exception v7

    .line 717
    .local v7, "e":Ljava/security/cert/CertStoreException;
    new-instance v0, Lcom/android/sec/org/bouncycastle/cms/CMSException;

    const-string v1, "error getting certs from certStore"

    invoke-direct {v0, v1, v7}, Lcom/android/sec/org/bouncycastle/cms/CMSException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v0

    .line 729
    .end local v7    # "e":Ljava/security/cert/CertStoreException;
    .restart local v8    # "set":Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;
    :catch_1
    move-exception v7

    .line 731
    .restart local v7    # "e":Ljava/security/cert/CertStoreException;
    new-instance v0, Lcom/android/sec/org/bouncycastle/cms/CMSException;

    const-string v1, "error getting crls from certStore"

    invoke-direct {v0, v1, v7}, Lcom/android/sec/org/bouncycastle/cms/CMSException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v0
.end method

.method public static replaceSigners(Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;Lcom/android/sec/org/bouncycastle/cms/SignerInformationStore;)Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;
    .locals 12
    .param p0, "signedData"    # Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;
    .param p1, "signerInformationStore"    # Lcom/android/sec/org/bouncycastle/cms/SignerInformationStore;

    .prologue
    .line 630
    new-instance v0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;

    invoke-direct {v0, p0}, Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;-><init>(Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;)V

    .line 635
    .local v0, "cms":Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;
    iput-object p1, v0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;->signerInfoStore:Lcom/android/sec/org/bouncycastle/cms/SignerInformationStore;

    .line 640
    new-instance v1, Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;

    invoke-direct {v1}, Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;-><init>()V

    .line 641
    .local v1, "digestAlgs":Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;
    new-instance v8, Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;

    invoke-direct {v8}, Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;-><init>()V

    .line 643
    .local v8, "vec":Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;
    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/cms/SignerInformationStore;->getSigners()Ljava/util/Collection;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .line 644
    .local v4, "it":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_0

    .line 646
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/android/sec/org/bouncycastle/cms/SignerInformation;

    .line 647
    .local v6, "signer":Lcom/android/sec/org/bouncycastle/cms/SignerInformation;
    sget-object v9, Lcom/android/sec/org/bouncycastle/cms/CMSSignedHelper;->INSTANCE:Lcom/android/sec/org/bouncycastle/cms/CMSSignedHelper;

    invoke-virtual {v6}, Lcom/android/sec/org/bouncycastle/cms/SignerInformation;->getDigestAlgorithmID()Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/android/sec/org/bouncycastle/cms/CMSSignedHelper;->fixAlgID(Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;)Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;

    move-result-object v9

    invoke-virtual {v1, v9}, Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;->add(Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;)V

    .line 648
    invoke-virtual {v6}, Lcom/android/sec/org/bouncycastle/cms/SignerInformation;->toASN1Structure()Lcom/android/sec/org/bouncycastle/asn1/cms/SignerInfo;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;->add(Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;)V

    goto :goto_0

    .line 651
    .end local v6    # "signer":Lcom/android/sec/org/bouncycastle/cms/SignerInformation;
    :cond_0
    new-instance v2, Lcom/android/sec/org/bouncycastle/asn1/DERSet;

    invoke-direct {v2, v1}, Lcom/android/sec/org/bouncycastle/asn1/DERSet;-><init>(Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;)V

    .line 652
    .local v2, "digests":Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;
    new-instance v7, Lcom/android/sec/org/bouncycastle/asn1/DERSet;

    invoke-direct {v7, v8}, Lcom/android/sec/org/bouncycastle/asn1/DERSet;-><init>(Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;)V

    .line 653
    .local v7, "signers":Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;
    iget-object v9, p0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;->signedData:Lcom/android/sec/org/bouncycastle/asn1/cms/SignedData;

    invoke-virtual {v9}, Lcom/android/sec/org/bouncycastle/asn1/cms/SignedData;->toASN1Primitive()Lcom/android/sec/org/bouncycastle/asn1/ASN1Primitive;

    move-result-object v5

    check-cast v5, Lcom/android/sec/org/bouncycastle/asn1/ASN1Sequence;

    .line 655
    .local v5, "sD":Lcom/android/sec/org/bouncycastle/asn1/ASN1Sequence;
    new-instance v8, Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;

    .end local v8    # "vec":Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;
    invoke-direct {v8}, Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;-><init>()V

    .line 660
    .restart local v8    # "vec":Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;
    const/4 v9, 0x0

    invoke-virtual {v5, v9}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Sequence;->getObjectAt(I)Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;->add(Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;)V

    .line 661
    invoke-virtual {v8, v2}, Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;->add(Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;)V

    .line 663
    const/4 v3, 0x2

    .local v3, "i":I
    :goto_1
    invoke-virtual {v5}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Sequence;->size()I

    move-result v9

    add-int/lit8 v9, v9, -0x1

    if-eq v3, v9, :cond_1

    .line 665
    invoke-virtual {v5, v3}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Sequence;->getObjectAt(I)Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;->add(Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;)V

    .line 663
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 668
    :cond_1
    invoke-virtual {v8, v7}, Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;->add(Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;)V

    .line 670
    new-instance v9, Lcom/android/sec/org/bouncycastle/asn1/BERSequence;

    invoke-direct {v9, v8}, Lcom/android/sec/org/bouncycastle/asn1/BERSequence;-><init>(Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;)V

    invoke-static {v9}, Lcom/android/sec/org/bouncycastle/asn1/cms/SignedData;->getInstance(Ljava/lang/Object;)Lcom/android/sec/org/bouncycastle/asn1/cms/SignedData;

    move-result-object v9

    iput-object v9, v0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;->signedData:Lcom/android/sec/org/bouncycastle/asn1/cms/SignedData;

    .line 675
    new-instance v9, Lcom/android/sec/org/bouncycastle/asn1/cms/ContentInfo;

    iget-object v10, v0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;->contentInfo:Lcom/android/sec/org/bouncycastle/asn1/cms/ContentInfo;

    invoke-virtual {v10}, Lcom/android/sec/org/bouncycastle/asn1/cms/ContentInfo;->getContentType()Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    move-result-object v10

    iget-object v11, v0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;->signedData:Lcom/android/sec/org/bouncycastle/asn1/cms/SignedData;

    invoke-direct {v9, v10, v11}, Lcom/android/sec/org/bouncycastle/asn1/cms/ContentInfo;-><init>(Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;)V

    iput-object v9, v0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;->contentInfo:Lcom/android/sec/org/bouncycastle/asn1/cms/ContentInfo;

    .line 677
    return-object v0
.end method


# virtual methods
.method public getAttributeCertificates()Lcom/android/sec/org/bouncycastle/util/Store;
    .locals 2

    .prologue
    .line 485
    sget-object v0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;->HELPER:Lcom/android/sec/org/bouncycastle/cms/CMSSignedHelper;

    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;->signedData:Lcom/android/sec/org/bouncycastle/asn1/cms/SignedData;

    invoke-virtual {v1}, Lcom/android/sec/org/bouncycastle/asn1/cms/SignedData;->getCertificates()Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/sec/org/bouncycastle/cms/CMSSignedHelper;->getAttributeCertificates(Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;)Lcom/android/sec/org/bouncycastle/util/Store;

    move-result-object v0

    return-object v0
.end method

.method public getAttributeCertificates(Ljava/lang/String;Ljava/lang/String;)Lcom/android/sec/org/bouncycastle/x509/X509Store;
    .locals 1
    .param p1, "type"    # Ljava/lang/String;
    .param p2, "provider"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/x509/NoSuchStoreException;,
            Ljava/security/NoSuchProviderException;,
            Lcom/android/sec/org/bouncycastle/cms/CMSException;
        }
    .end annotation

    .prologue
    .line 287
    invoke-static {p2}, Lcom/android/sec/org/bouncycastle/cms/CMSUtils;->getProvider(Ljava/lang/String;)Ljava/security/Provider;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;->getAttributeCertificates(Ljava/lang/String;Ljava/security/Provider;)Lcom/android/sec/org/bouncycastle/x509/X509Store;

    move-result-object v0

    return-object v0
.end method

.method public getAttributeCertificates(Ljava/lang/String;Ljava/security/Provider;)Lcom/android/sec/org/bouncycastle/x509/X509Store;
    .locals 2
    .param p1, "type"    # Ljava/lang/String;
    .param p2, "provider"    # Ljava/security/Provider;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/x509/NoSuchStoreException;,
            Lcom/android/sec/org/bouncycastle/cms/CMSException;
        }
    .end annotation

    .prologue
    .line 306
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;->attributeStore:Lcom/android/sec/org/bouncycastle/x509/X509Store;

    if-nez v0, :cond_0

    .line 308
    sget-object v0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;->HELPER:Lcom/android/sec/org/bouncycastle/cms/CMSSignedHelper;

    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;->getAttributeCertificates()Lcom/android/sec/org/bouncycastle/util/Store;

    move-result-object v1

    invoke-virtual {v0, p1, p2, v1}, Lcom/android/sec/org/bouncycastle/cms/CMSSignedHelper;->createAttributeStore(Ljava/lang/String;Ljava/security/Provider;Lcom/android/sec/org/bouncycastle/util/Store;)Lcom/android/sec/org/bouncycastle/x509/X509Store;

    move-result-object v0

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;->attributeStore:Lcom/android/sec/org/bouncycastle/x509/X509Store;

    .line 311
    :cond_0
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;->attributeStore:Lcom/android/sec/org/bouncycastle/x509/X509Store;

    return-object v0
.end method

.method public getCRLs()Lcom/android/sec/org/bouncycastle/util/Store;
    .locals 2

    .prologue
    .line 475
    sget-object v0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;->HELPER:Lcom/android/sec/org/bouncycastle/cms/CMSSignedHelper;

    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;->signedData:Lcom/android/sec/org/bouncycastle/asn1/cms/SignedData;

    invoke-virtual {v1}, Lcom/android/sec/org/bouncycastle/asn1/cms/SignedData;->getCRLs()Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/sec/org/bouncycastle/cms/CMSSignedHelper;->getCRLs(Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;)Lcom/android/sec/org/bouncycastle/util/Store;

    move-result-object v0

    return-object v0
.end method

.method public getCertificates()Lcom/android/sec/org/bouncycastle/util/Store;
    .locals 2

    .prologue
    .line 465
    sget-object v0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;->HELPER:Lcom/android/sec/org/bouncycastle/cms/CMSSignedHelper;

    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;->signedData:Lcom/android/sec/org/bouncycastle/asn1/cms/SignedData;

    invoke-virtual {v1}, Lcom/android/sec/org/bouncycastle/asn1/cms/SignedData;->getCertificates()Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/sec/org/bouncycastle/cms/CMSSignedHelper;->getCertificates(Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;)Lcom/android/sec/org/bouncycastle/util/Store;

    move-result-object v0

    return-object v0
.end method

.method public getContentInfo()Lcom/android/sec/org/bouncycastle/asn1/cms/ContentInfo;
    .locals 1

    .prologue
    .line 525
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;->contentInfo:Lcom/android/sec/org/bouncycastle/asn1/cms/ContentInfo;

    return-object v0
.end method

.method public getEncoded()[B
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 542
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;->contentInfo:Lcom/android/sec/org/bouncycastle/asn1/cms/ContentInfo;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/cms/ContentInfo;->getEncoded()[B

    move-result-object v0

    return-object v0
.end method

.method public getSignedContent()Lcom/android/sec/org/bouncycastle/cms/CMSTypedData;
    .locals 1

    .prologue
    .line 516
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;->signedContent:Lcom/android/sec/org/bouncycastle/cms/CMSTypedData;

    return-object v0
.end method

.method public getSignedContentTypeOID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 511
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;->signedData:Lcom/android/sec/org/bouncycastle/asn1/cms/SignedData;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/cms/SignedData;->getEncapContentInfo()Lcom/android/sec/org/bouncycastle/asn1/cms/ContentInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/cms/ContentInfo;->getContentType()Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;->getId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSignerInfos()Lcom/android/sec/org/bouncycastle/cms/SignerInformationStore;
    .locals 11

    .prologue
    const/4 v10, 0x0

    .line 240
    iget-object v8, p0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;->signerInfoStore:Lcom/android/sec/org/bouncycastle/cms/SignerInformationStore;

    if-nez v8, :cond_3

    .line 242
    iget-object v8, p0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;->signedData:Lcom/android/sec/org/bouncycastle/asn1/cms/SignedData;

    invoke-virtual {v8}, Lcom/android/sec/org/bouncycastle/asn1/cms/SignedData;->getSignerInfos()Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;

    move-result-object v5

    .line 243
    .local v5, "s":Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 244
    .local v7, "signerInfos":Ljava/util/List;
    new-instance v6, Lcom/android/sec/org/bouncycastle/operator/DefaultSignatureAlgorithmIdentifierFinder;

    invoke-direct {v6}, Lcom/android/sec/org/bouncycastle/operator/DefaultSignatureAlgorithmIdentifierFinder;-><init>()V

    .line 246
    .local v6, "sigAlgFinder":Lcom/android/sec/org/bouncycastle/operator/SignatureAlgorithmIdentifierFinder;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {v5}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;->size()I

    move-result v8

    if-eq v2, v8, :cond_2

    .line 248
    invoke-virtual {v5, v2}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;->getObjectAt(I)Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;

    move-result-object v8

    invoke-static {v8}, Lcom/android/sec/org/bouncycastle/asn1/cms/SignerInfo;->getInstance(Ljava/lang/Object;)Lcom/android/sec/org/bouncycastle/asn1/cms/SignerInfo;

    move-result-object v3

    .line 249
    .local v3, "info":Lcom/android/sec/org/bouncycastle/asn1/cms/SignerInfo;
    iget-object v8, p0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;->signedData:Lcom/android/sec/org/bouncycastle/asn1/cms/SignedData;

    invoke-virtual {v8}, Lcom/android/sec/org/bouncycastle/asn1/cms/SignedData;->getEncapContentInfo()Lcom/android/sec/org/bouncycastle/asn1/cms/ContentInfo;

    move-result-object v8

    invoke-virtual {v8}, Lcom/android/sec/org/bouncycastle/asn1/cms/ContentInfo;->getContentType()Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    move-result-object v0

    .line 251
    .local v0, "contentType":Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;
    iget-object v8, p0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;->hashes:Ljava/util/Map;

    if-nez v8, :cond_0

    .line 253
    new-instance v8, Lcom/android/sec/org/bouncycastle/cms/SignerInformation;

    iget-object v9, p0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;->signedContent:Lcom/android/sec/org/bouncycastle/cms/CMSTypedData;

    invoke-direct {v8, v3, v0, v9, v10}, Lcom/android/sec/org/bouncycastle/cms/SignerInformation;-><init>(Lcom/android/sec/org/bouncycastle/asn1/cms/SignerInfo;Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;Lcom/android/sec/org/bouncycastle/cms/CMSProcessable;[B)V

    invoke-interface {v7, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 246
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 257
    :cond_0
    iget-object v8, p0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;->hashes:Ljava/util/Map;

    invoke-interface {v8}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    .line 258
    .local v4, "obj":Ljava/lang/Object;
    instance-of v8, v4, Ljava/lang/String;

    if-eqz v8, :cond_1

    iget-object v8, p0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;->hashes:Ljava/util/Map;

    invoke-virtual {v3}, Lcom/android/sec/org/bouncycastle/asn1/cms/SignerInfo;->getDigestAlgorithm()Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;

    move-result-object v9

    invoke-virtual {v9}, Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;->getAlgorithm()Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    move-result-object v9

    invoke-virtual {v9}, Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;->getId()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v8, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, [B

    check-cast v8, [B

    move-object v1, v8

    .line 260
    .local v1, "hash":[B
    :goto_2
    new-instance v8, Lcom/android/sec/org/bouncycastle/cms/SignerInformation;

    invoke-direct {v8, v3, v0, v10, v1}, Lcom/android/sec/org/bouncycastle/cms/SignerInformation;-><init>(Lcom/android/sec/org/bouncycastle/asn1/cms/SignerInfo;Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;Lcom/android/sec/org/bouncycastle/cms/CMSProcessable;[B)V

    invoke-interface {v7, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 258
    .end local v1    # "hash":[B
    :cond_1
    iget-object v8, p0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;->hashes:Ljava/util/Map;

    invoke-virtual {v3}, Lcom/android/sec/org/bouncycastle/asn1/cms/SignerInfo;->getDigestAlgorithm()Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;

    move-result-object v9

    invoke-virtual {v9}, Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;->getAlgorithm()Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    move-result-object v9

    invoke-interface {v8, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, [B

    check-cast v8, [B

    move-object v1, v8

    goto :goto_2

    .line 264
    .end local v0    # "contentType":Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;
    .end local v3    # "info":Lcom/android/sec/org/bouncycastle/asn1/cms/SignerInfo;
    .end local v4    # "obj":Ljava/lang/Object;
    :cond_2
    new-instance v8, Lcom/android/sec/org/bouncycastle/cms/SignerInformationStore;

    invoke-direct {v8, v7}, Lcom/android/sec/org/bouncycastle/cms/SignerInformationStore;-><init>(Ljava/util/Collection;)V

    iput-object v8, p0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;->signerInfoStore:Lcom/android/sec/org/bouncycastle/cms/SignerInformationStore;

    .line 267
    .end local v2    # "i":I
    .end local v5    # "s":Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;
    .end local v6    # "sigAlgFinder":Lcom/android/sec/org/bouncycastle/operator/SignatureAlgorithmIdentifierFinder;
    .end local v7    # "signerInfos":Ljava/util/List;
    :cond_3
    iget-object v8, p0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;->signerInfoStore:Lcom/android/sec/org/bouncycastle/cms/SignerInformationStore;

    return-object v8
.end method

.method public getVersion()I
    .locals 1

    .prologue
    .line 231
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;->signedData:Lcom/android/sec/org/bouncycastle/asn1/cms/SignedData;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/cms/SignedData;->getVersion()Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;->getValue()Ljava/math/BigInteger;

    move-result-object v0

    invoke-virtual {v0}, Ljava/math/BigInteger;->intValue()I

    move-result v0

    return v0
.end method

.method public toASN1Structure()Lcom/android/sec/org/bouncycastle/asn1/cms/ContentInfo;
    .locals 1

    .prologue
    .line 533
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;->contentInfo:Lcom/android/sec/org/bouncycastle/asn1/cms/ContentInfo;

    return-object v0
.end method
