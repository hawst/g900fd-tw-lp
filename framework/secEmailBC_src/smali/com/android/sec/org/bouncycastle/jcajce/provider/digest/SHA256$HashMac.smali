.class public Lcom/android/sec/org/bouncycastle/jcajce/provider/digest/SHA256$HashMac;
.super Lcom/android/sec/org/bouncycastle/jcajce/provider/symmetric/util/BaseMac;
.source "SHA256.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/sec/org/bouncycastle/jcajce/provider/digest/SHA256;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "HashMac"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 44
    new-instance v0, Lcom/android/sec/org/bouncycastle/crypto/macs/HMac;

    new-instance v1, Lcom/android/sec/org/bouncycastle/crypto/digests/SHA256Digest;

    invoke-direct {v1}, Lcom/android/sec/org/bouncycastle/crypto/digests/SHA256Digest;-><init>()V

    invoke-direct {v0, v1}, Lcom/android/sec/org/bouncycastle/crypto/macs/HMac;-><init>(Lcom/android/sec/org/bouncycastle/crypto/Digest;)V

    invoke-direct {p0, v0}, Lcom/android/sec/org/bouncycastle/jcajce/provider/symmetric/util/BaseMac;-><init>(Lcom/android/sec/org/bouncycastle/crypto/Mac;)V

    .line 45
    return-void
.end method
