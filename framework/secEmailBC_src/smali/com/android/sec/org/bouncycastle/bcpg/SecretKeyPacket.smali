.class public Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;
.super Lcom/android/sec/org/bouncycastle/bcpg/ContainedPacket;
.source "SecretKeyPacket.java"

# interfaces
.implements Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyAlgorithmTags;


# static fields
.field public static final USAGE_CHECKSUM:I = 0xff

.field public static final USAGE_NONE:I = 0x0

.field public static final USAGE_SHA1:I = 0xfe


# instance fields
.field private encAlgorithm:I

.field private iv:[B

.field private pubKeyPacket:Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;

.field private s2k:Lcom/android/sec/org/bouncycastle/bcpg/S2K;

.field private s2kUsage:I

.field private secKeyData:[B


# direct methods
.method constructor <init>(Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;)V
    .locals 3
    .param p1, "in"    # Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/bcpg/ContainedPacket;-><init>()V

    .line 31
    instance-of v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/SecretSubkeyPacket;

    if-eqz v0, :cond_4

    .line 33
    new-instance v0, Lcom/android/sec/org/bouncycastle/bcpg/PublicSubkeyPacket;

    invoke-direct {v0, p1}, Lcom/android/sec/org/bouncycastle/bcpg/PublicSubkeyPacket;-><init>(Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;)V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;->pubKeyPacket:Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;

    .line 40
    :goto_0
    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->read()I

    move-result v0

    iput v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;->s2kUsage:I

    .line 42
    iget v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;->s2kUsage:I

    const/16 v1, 0xff

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;->s2kUsage:I

    const/16 v1, 0xfe

    if-ne v0, v1, :cond_5

    .line 44
    :cond_0
    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->read()I

    move-result v0

    iput v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;->encAlgorithm:I

    .line 45
    new-instance v0, Lcom/android/sec/org/bouncycastle/bcpg/S2K;

    invoke-direct {v0, p1}, Lcom/android/sec/org/bouncycastle/bcpg/S2K;-><init>(Ljava/io/InputStream;)V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;->s2k:Lcom/android/sec/org/bouncycastle/bcpg/S2K;

    .line 52
    :goto_1
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;->s2k:Lcom/android/sec/org/bouncycastle/bcpg/S2K;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;->s2k:Lcom/android/sec/org/bouncycastle/bcpg/S2K;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/bcpg/S2K;->getType()I

    move-result v0

    const/16 v1, 0x65

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;->s2k:Lcom/android/sec/org/bouncycastle/bcpg/S2K;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/bcpg/S2K;->getProtectionMode()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    .line 54
    :cond_1
    iget v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;->s2kUsage:I

    if-eqz v0, :cond_2

    .line 56
    iget v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;->encAlgorithm:I

    const/4 v1, 0x7

    if-ge v0, v1, :cond_6

    .line 58
    const/16 v0, 0x8

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;->iv:[B

    .line 64
    :goto_2
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;->iv:[B

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;->iv:[B

    array-length v2, v2

    invoke-virtual {p1, v0, v1, v2}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->readFully([BII)V

    .line 68
    :cond_2
    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->available()I

    move-result v0

    if-eqz v0, :cond_3

    .line 70
    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->available()I

    move-result v0

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;->secKeyData:[B

    .line 72
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;->secKeyData:[B

    invoke-virtual {p1, v0}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->readFully([B)V

    .line 74
    :cond_3
    return-void

    .line 37
    :cond_4
    new-instance v0, Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;

    invoke-direct {v0, p1}, Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;-><init>(Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;)V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;->pubKeyPacket:Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;

    goto :goto_0

    .line 49
    :cond_5
    iget v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;->s2kUsage:I

    iput v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;->encAlgorithm:I

    goto :goto_1

    .line 62
    :cond_6
    const/16 v0, 0x10

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;->iv:[B

    goto :goto_2
.end method

.method public constructor <init>(Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;IILcom/android/sec/org/bouncycastle/bcpg/S2K;[B[B)V
    .locals 0
    .param p1, "pubKeyPacket"    # Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;
    .param p2, "encAlgorithm"    # I
    .param p3, "s2kUsage"    # I
    .param p4, "s2k"    # Lcom/android/sec/org/bouncycastle/bcpg/S2K;
    .param p5, "iv"    # [B
    .param p6, "secKeyData"    # [B

    .prologue
    .line 115
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/bcpg/ContainedPacket;-><init>()V

    .line 116
    iput-object p1, p0, Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;->pubKeyPacket:Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;

    .line 117
    iput p2, p0, Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;->encAlgorithm:I

    .line 118
    iput p3, p0, Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;->s2kUsage:I

    .line 119
    iput-object p4, p0, Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;->s2k:Lcom/android/sec/org/bouncycastle/bcpg/S2K;

    .line 120
    iput-object p5, p0, Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;->iv:[B

    .line 121
    iput-object p6, p0, Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;->secKeyData:[B

    .line 122
    return-void
.end method

.method public constructor <init>(Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;ILcom/android/sec/org/bouncycastle/bcpg/S2K;[B[B)V
    .locals 1
    .param p1, "pubKeyPacket"    # Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;
    .param p2, "encAlgorithm"    # I
    .param p3, "s2k"    # Lcom/android/sec/org/bouncycastle/bcpg/S2K;
    .param p4, "iv"    # [B
    .param p5, "secKeyData"    # [B

    .prologue
    .line 90
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/bcpg/ContainedPacket;-><init>()V

    .line 91
    iput-object p1, p0, Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;->pubKeyPacket:Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;

    .line 92
    iput p2, p0, Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;->encAlgorithm:I

    .line 94
    if-eqz p2, :cond_0

    .line 96
    const/16 v0, 0xff

    iput v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;->s2kUsage:I

    .line 103
    :goto_0
    iput-object p3, p0, Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;->s2k:Lcom/android/sec/org/bouncycastle/bcpg/S2K;

    .line 104
    iput-object p4, p0, Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;->iv:[B

    .line 105
    iput-object p5, p0, Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;->secKeyData:[B

    .line 106
    return-void

    .line 100
    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;->s2kUsage:I

    goto :goto_0
.end method


# virtual methods
.method public encode(Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;)V
    .locals 3
    .param p1, "out"    # Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 187
    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;->getEncodedContents()[B

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {p1, v0, v1, v2}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->writePacket(I[BZ)V

    .line 188
    return-void
.end method

.method public getEncAlgorithm()I
    .locals 1

    .prologue
    .line 126
    iget v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;->encAlgorithm:I

    return v0
.end method

.method public getEncodedContents()[B
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 157
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 158
    .local v0, "bOut":Ljava/io/ByteArrayOutputStream;
    new-instance v1, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;

    invoke-direct {v1, v0}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 160
    .local v1, "pOut":Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;->pubKeyPacket:Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;

    invoke-virtual {v2}, Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;->getEncodedContents()[B

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->write([B)V

    .line 162
    iget v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;->s2kUsage:I

    invoke-virtual {v1, v2}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->write(I)V

    .line 164
    iget v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;->s2kUsage:I

    const/16 v3, 0xff

    if-eq v2, v3, :cond_0

    iget v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;->s2kUsage:I

    const/16 v3, 0xfe

    if-ne v2, v3, :cond_1

    .line 166
    :cond_0
    iget v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;->encAlgorithm:I

    invoke-virtual {v1, v2}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->write(I)V

    .line 167
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;->s2k:Lcom/android/sec/org/bouncycastle/bcpg/S2K;

    invoke-virtual {v1, v2}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->writeObject(Lcom/android/sec/org/bouncycastle/bcpg/BCPGObject;)V

    .line 170
    :cond_1
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;->iv:[B

    if-eqz v2, :cond_2

    .line 172
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;->iv:[B

    invoke-virtual {v1, v2}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->write([B)V

    .line 175
    :cond_2
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;->secKeyData:[B

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;->secKeyData:[B

    array-length v2, v2

    if-lez v2, :cond_3

    .line 177
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;->secKeyData:[B

    invoke-virtual {v1, v2}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->write([B)V

    .line 180
    :cond_3
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v2

    return-object v2
.end method

.method public getIV()[B
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;->iv:[B

    return-object v0
.end method

.method public getPublicKeyPacket()Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;->pubKeyPacket:Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;

    return-object v0
.end method

.method public getS2K()Lcom/android/sec/org/bouncycastle/bcpg/S2K;
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;->s2k:Lcom/android/sec/org/bouncycastle/bcpg/S2K;

    return-object v0
.end method

.method public getS2KUsage()I
    .locals 1

    .prologue
    .line 131
    iget v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;->s2kUsage:I

    return v0
.end method

.method public getSecretKeyData()[B
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;->secKeyData:[B

    return-object v0
.end method
