.class public Lcom/android/sec/org/bouncycastle/cert/X509AttributeCertificateHolder;
.super Ljava/lang/Object;
.source "X509AttributeCertificateHolder.java"


# static fields
.field private static EMPTY_ARRAY:[Lcom/android/sec/org/bouncycastle/asn1/x509/Attribute;


# instance fields
.field private attrCert:Lcom/android/sec/org/bouncycastle/asn1/x509/AttributeCertificate;

.field private extensions:Lcom/android/sec/org/bouncycastle/asn1/x509/Extensions;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/android/sec/org/bouncycastle/asn1/x509/Attribute;

    sput-object v0, Lcom/android/sec/org/bouncycastle/cert/X509AttributeCertificateHolder;->EMPTY_ARRAY:[Lcom/android/sec/org/bouncycastle/asn1/x509/Attribute;

    return-void
.end method

.method public constructor <init>(Lcom/android/sec/org/bouncycastle/asn1/x509/AttributeCertificate;)V
    .locals 1
    .param p1, "attrCert"    # Lcom/android/sec/org/bouncycastle/asn1/x509/AttributeCertificate;

    .prologue
    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    iput-object p1, p0, Lcom/android/sec/org/bouncycastle/cert/X509AttributeCertificateHolder;->attrCert:Lcom/android/sec/org/bouncycastle/asn1/x509/AttributeCertificate;

    .line 72
    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/asn1/x509/AttributeCertificate;->getAcinfo()Lcom/android/sec/org/bouncycastle/asn1/x509/AttributeCertificateInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/x509/AttributeCertificateInfo;->getExtensions()Lcom/android/sec/org/bouncycastle/asn1/x509/Extensions;

    move-result-object v0

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/cert/X509AttributeCertificateHolder;->extensions:Lcom/android/sec/org/bouncycastle/asn1/x509/Extensions;

    .line 73
    return-void
.end method

.method public constructor <init>([B)V
    .locals 1
    .param p1, "certEncoding"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 61
    invoke-static {p1}, Lcom/android/sec/org/bouncycastle/cert/X509AttributeCertificateHolder;->parseBytes([B)Lcom/android/sec/org/bouncycastle/asn1/x509/AttributeCertificate;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/sec/org/bouncycastle/cert/X509AttributeCertificateHolder;-><init>(Lcom/android/sec/org/bouncycastle/asn1/x509/AttributeCertificate;)V

    .line 62
    return-void
.end method

.method private static parseBytes([B)Lcom/android/sec/org/bouncycastle/asn1/x509/AttributeCertificate;
    .locals 4
    .param p0, "certEncoding"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 40
    :try_start_0
    invoke-static {p0}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Primitive;->fromByteArray([B)Lcom/android/sec/org/bouncycastle/asn1/ASN1Primitive;

    move-result-object v1

    invoke-static {v1}, Lcom/android/sec/org/bouncycastle/asn1/x509/AttributeCertificate;->getInstance(Ljava/lang/Object;)Lcom/android/sec/org/bouncycastle/asn1/x509/AttributeCertificate;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v1

    return-object v1

    .line 42
    :catch_0
    move-exception v0

    .line 44
    .local v0, "e":Ljava/lang/ClassCastException;
    new-instance v1, Lcom/android/sec/org/bouncycastle/cert/CertIOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "malformed data: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/ClassCastException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/android/sec/org/bouncycastle/cert/CertIOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 46
    .end local v0    # "e":Ljava/lang/ClassCastException;
    :catch_1
    move-exception v0

    .line 48
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    new-instance v1, Lcom/android/sec/org/bouncycastle/cert/CertIOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "malformed data: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/android/sec/org/bouncycastle/cert/CertIOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 347
    if-ne p1, p0, :cond_0

    .line 349
    const/4 v1, 0x1

    .line 359
    :goto_0
    return v1

    .line 352
    :cond_0
    instance-of v1, p1, Lcom/android/sec/org/bouncycastle/cert/X509AttributeCertificateHolder;

    if-nez v1, :cond_1

    .line 354
    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    move-object v0, p1

    .line 357
    check-cast v0, Lcom/android/sec/org/bouncycastle/cert/X509AttributeCertificateHolder;

    .line 359
    .local v0, "other":Lcom/android/sec/org/bouncycastle/cert/X509AttributeCertificateHolder;
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/cert/X509AttributeCertificateHolder;->attrCert:Lcom/android/sec/org/bouncycastle/asn1/x509/AttributeCertificate;

    iget-object v2, v0, Lcom/android/sec/org/bouncycastle/cert/X509AttributeCertificateHolder;->attrCert:Lcom/android/sec/org/bouncycastle/asn1/x509/AttributeCertificate;

    invoke-virtual {v1, v2}, Lcom/android/sec/org/bouncycastle/asn1/x509/AttributeCertificate;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method

.method public getAttributes()[Lcom/android/sec/org/bouncycastle/asn1/x509/Attribute;
    .locals 4

    .prologue
    .line 149
    iget-object v3, p0, Lcom/android/sec/org/bouncycastle/cert/X509AttributeCertificateHolder;->attrCert:Lcom/android/sec/org/bouncycastle/asn1/x509/AttributeCertificate;

    invoke-virtual {v3}, Lcom/android/sec/org/bouncycastle/asn1/x509/AttributeCertificate;->getAcinfo()Lcom/android/sec/org/bouncycastle/asn1/x509/AttributeCertificateInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/sec/org/bouncycastle/asn1/x509/AttributeCertificateInfo;->getAttributes()Lcom/android/sec/org/bouncycastle/asn1/ASN1Sequence;

    move-result-object v2

    .line 150
    .local v2, "seq":Lcom/android/sec/org/bouncycastle/asn1/ASN1Sequence;
    invoke-virtual {v2}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Sequence;->size()I

    move-result v3

    new-array v0, v3, [Lcom/android/sec/org/bouncycastle/asn1/x509/Attribute;

    .line 152
    .local v0, "attrs":[Lcom/android/sec/org/bouncycastle/asn1/x509/Attribute;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {v2}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Sequence;->size()I

    move-result v3

    if-eq v1, v3, :cond_0

    .line 154
    invoke-virtual {v2, v1}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Sequence;->getObjectAt(I)Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;

    move-result-object v3

    invoke-static {v3}, Lcom/android/sec/org/bouncycastle/asn1/x509/Attribute;->getInstance(Ljava/lang/Object;)Lcom/android/sec/org/bouncycastle/asn1/x509/Attribute;

    move-result-object v3

    aput-object v3, v0, v1

    .line 152
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 157
    :cond_0
    return-object v0
.end method

.method public getAttributes(Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;)[Lcom/android/sec/org/bouncycastle/asn1/x509/Attribute;
    .locals 5
    .param p1, "type"    # Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    .prologue
    .line 168
    iget-object v4, p0, Lcom/android/sec/org/bouncycastle/cert/X509AttributeCertificateHolder;->attrCert:Lcom/android/sec/org/bouncycastle/asn1/x509/AttributeCertificate;

    invoke-virtual {v4}, Lcom/android/sec/org/bouncycastle/asn1/x509/AttributeCertificate;->getAcinfo()Lcom/android/sec/org/bouncycastle/asn1/x509/AttributeCertificateInfo;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/sec/org/bouncycastle/asn1/x509/AttributeCertificateInfo;->getAttributes()Lcom/android/sec/org/bouncycastle/asn1/ASN1Sequence;

    move-result-object v3

    .line 169
    .local v3, "seq":Lcom/android/sec/org/bouncycastle/asn1/ASN1Sequence;
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 171
    .local v2, "list":Ljava/util/List;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {v3}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Sequence;->size()I

    move-result v4

    if-eq v1, v4, :cond_1

    .line 173
    invoke-virtual {v3, v1}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Sequence;->getObjectAt(I)Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;

    move-result-object v4

    invoke-static {v4}, Lcom/android/sec/org/bouncycastle/asn1/x509/Attribute;->getInstance(Ljava/lang/Object;)Lcom/android/sec/org/bouncycastle/asn1/x509/Attribute;

    move-result-object v0

    .line 174
    .local v0, "attr":Lcom/android/sec/org/bouncycastle/asn1/x509/Attribute;
    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/x509/Attribute;->getAttrType()Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    move-result-object v4

    invoke-virtual {v4, p1}, Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 176
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 171
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 180
    .end local v0    # "attr":Lcom/android/sec/org/bouncycastle/asn1/x509/Attribute;
    :cond_1
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v4

    if-nez v4, :cond_2

    .line 182
    sget-object v4, Lcom/android/sec/org/bouncycastle/cert/X509AttributeCertificateHolder;->EMPTY_ARRAY:[Lcom/android/sec/org/bouncycastle/asn1/x509/Attribute;

    .line 185
    :goto_1
    return-object v4

    :cond_2
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v4

    new-array v4, v4, [Lcom/android/sec/org/bouncycastle/asn1/x509/Attribute;

    invoke-interface {v2, v4}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Lcom/android/sec/org/bouncycastle/asn1/x509/Attribute;

    check-cast v4, [Lcom/android/sec/org/bouncycastle/asn1/x509/Attribute;

    goto :goto_1
.end method

.method public getCriticalExtensionOIDs()Ljava/util/Set;
    .locals 1

    .prologue
    .line 244
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cert/X509AttributeCertificateHolder;->extensions:Lcom/android/sec/org/bouncycastle/asn1/x509/Extensions;

    invoke-static {v0}, Lcom/android/sec/org/bouncycastle/cert/CertUtils;->getCriticalExtensionOIDs(Lcom/android/sec/org/bouncycastle/asn1/x509/Extensions;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public getEncoded()[B
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 84
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cert/X509AttributeCertificateHolder;->attrCert:Lcom/android/sec/org/bouncycastle/asn1/x509/AttributeCertificate;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/x509/AttributeCertificate;->getEncoded()[B

    move-result-object v0

    return-object v0
.end method

.method public getExtension(Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;)Lcom/android/sec/org/bouncycastle/asn1/x509/Extension;
    .locals 1
    .param p1, "oid"    # Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    .prologue
    .line 207
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cert/X509AttributeCertificateHolder;->extensions:Lcom/android/sec/org/bouncycastle/asn1/x509/Extensions;

    if-eqz v0, :cond_0

    .line 209
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cert/X509AttributeCertificateHolder;->extensions:Lcom/android/sec/org/bouncycastle/asn1/x509/Extensions;

    invoke-virtual {v0, p1}, Lcom/android/sec/org/bouncycastle/asn1/x509/Extensions;->getExtension(Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;)Lcom/android/sec/org/bouncycastle/asn1/x509/Extension;

    move-result-object v0

    .line 212
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getExtensionOIDs()Ljava/util/List;
    .locals 1

    .prologue
    .line 233
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cert/X509AttributeCertificateHolder;->extensions:Lcom/android/sec/org/bouncycastle/asn1/x509/Extensions;

    invoke-static {v0}, Lcom/android/sec/org/bouncycastle/cert/CertUtils;->getExtensionOIDs(Lcom/android/sec/org/bouncycastle/asn1/x509/Extensions;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getExtensions()Lcom/android/sec/org/bouncycastle/asn1/x509/Extensions;
    .locals 1

    .prologue
    .line 222
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cert/X509AttributeCertificateHolder;->extensions:Lcom/android/sec/org/bouncycastle/asn1/x509/Extensions;

    return-object v0
.end method

.method public getHolder()Lcom/android/sec/org/bouncycastle/cert/AttributeCertificateHolder;
    .locals 2

    .prologue
    .line 109
    new-instance v1, Lcom/android/sec/org/bouncycastle/cert/AttributeCertificateHolder;

    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cert/X509AttributeCertificateHolder;->attrCert:Lcom/android/sec/org/bouncycastle/asn1/x509/AttributeCertificate;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/x509/AttributeCertificate;->getAcinfo()Lcom/android/sec/org/bouncycastle/asn1/x509/AttributeCertificateInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/x509/AttributeCertificateInfo;->getHolder()Lcom/android/sec/org/bouncycastle/asn1/x509/Holder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/x509/Holder;->toASN1Primitive()Lcom/android/sec/org/bouncycastle/asn1/ASN1Primitive;

    move-result-object v0

    check-cast v0, Lcom/android/sec/org/bouncycastle/asn1/ASN1Sequence;

    invoke-direct {v1, v0}, Lcom/android/sec/org/bouncycastle/cert/AttributeCertificateHolder;-><init>(Lcom/android/sec/org/bouncycastle/asn1/ASN1Sequence;)V

    return-object v1
.end method

.method public getIssuer()Lcom/android/sec/org/bouncycastle/cert/AttributeCertificateIssuer;
    .locals 2

    .prologue
    .line 119
    new-instance v0, Lcom/android/sec/org/bouncycastle/cert/AttributeCertificateIssuer;

    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/cert/X509AttributeCertificateHolder;->attrCert:Lcom/android/sec/org/bouncycastle/asn1/x509/AttributeCertificate;

    invoke-virtual {v1}, Lcom/android/sec/org/bouncycastle/asn1/x509/AttributeCertificate;->getAcinfo()Lcom/android/sec/org/bouncycastle/asn1/x509/AttributeCertificateInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/sec/org/bouncycastle/asn1/x509/AttributeCertificateInfo;->getIssuer()Lcom/android/sec/org/bouncycastle/asn1/x509/AttCertIssuer;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/sec/org/bouncycastle/cert/AttributeCertificateIssuer;-><init>(Lcom/android/sec/org/bouncycastle/asn1/x509/AttCertIssuer;)V

    return-object v0
.end method

.method public getIssuerUniqueID()[Z
    .locals 1

    .prologue
    .line 260
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cert/X509AttributeCertificateHolder;->attrCert:Lcom/android/sec/org/bouncycastle/asn1/x509/AttributeCertificate;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/x509/AttributeCertificate;->getAcinfo()Lcom/android/sec/org/bouncycastle/asn1/x509/AttributeCertificateInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/x509/AttributeCertificateInfo;->getIssuerUniqueID()Lcom/android/sec/org/bouncycastle/asn1/DERBitString;

    move-result-object v0

    invoke-static {v0}, Lcom/android/sec/org/bouncycastle/cert/CertUtils;->bitStringToBoolean(Lcom/android/sec/org/bouncycastle/asn1/DERBitString;)[Z

    move-result-object v0

    return-object v0
.end method

.method public getNonCriticalExtensionOIDs()Ljava/util/Set;
    .locals 1

    .prologue
    .line 255
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cert/X509AttributeCertificateHolder;->extensions:Lcom/android/sec/org/bouncycastle/asn1/x509/Extensions;

    invoke-static {v0}, Lcom/android/sec/org/bouncycastle/cert/CertUtils;->getNonCriticalExtensionOIDs(Lcom/android/sec/org/bouncycastle/asn1/x509/Extensions;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public getNotAfter()Ljava/util/Date;
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cert/X509AttributeCertificateHolder;->attrCert:Lcom/android/sec/org/bouncycastle/asn1/x509/AttributeCertificate;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/x509/AttributeCertificate;->getAcinfo()Lcom/android/sec/org/bouncycastle/asn1/x509/AttributeCertificateInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/x509/AttributeCertificateInfo;->getAttrCertValidityPeriod()Lcom/android/sec/org/bouncycastle/asn1/x509/AttCertValidityPeriod;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/x509/AttCertValidityPeriod;->getNotAfterTime()Lcom/android/sec/org/bouncycastle/asn1/ASN1GeneralizedTime;

    move-result-object v0

    invoke-static {v0}, Lcom/android/sec/org/bouncycastle/cert/CertUtils;->recoverDate(Lcom/android/sec/org/bouncycastle/asn1/ASN1GeneralizedTime;)Ljava/util/Date;

    move-result-object v0

    return-object v0
.end method

.method public getNotBefore()Ljava/util/Date;
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cert/X509AttributeCertificateHolder;->attrCert:Lcom/android/sec/org/bouncycastle/asn1/x509/AttributeCertificate;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/x509/AttributeCertificate;->getAcinfo()Lcom/android/sec/org/bouncycastle/asn1/x509/AttributeCertificateInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/x509/AttributeCertificateInfo;->getAttrCertValidityPeriod()Lcom/android/sec/org/bouncycastle/asn1/x509/AttCertValidityPeriod;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/x509/AttCertValidityPeriod;->getNotBeforeTime()Lcom/android/sec/org/bouncycastle/asn1/ASN1GeneralizedTime;

    move-result-object v0

    invoke-static {v0}, Lcom/android/sec/org/bouncycastle/cert/CertUtils;->recoverDate(Lcom/android/sec/org/bouncycastle/asn1/ASN1GeneralizedTime;)Ljava/util/Date;

    move-result-object v0

    return-object v0
.end method

.method public getSerialNumber()Ljava/math/BigInteger;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cert/X509AttributeCertificateHolder;->attrCert:Lcom/android/sec/org/bouncycastle/asn1/x509/AttributeCertificate;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/x509/AttributeCertificate;->getAcinfo()Lcom/android/sec/org/bouncycastle/asn1/x509/AttributeCertificateInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/x509/AttributeCertificateInfo;->getSerialNumber()Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;->getValue()Ljava/math/BigInteger;

    move-result-object v0

    return-object v0
.end method

.method public getSignature()[B
    .locals 1

    .prologue
    .line 280
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cert/X509AttributeCertificateHolder;->attrCert:Lcom/android/sec/org/bouncycastle/asn1/x509/AttributeCertificate;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/x509/AttributeCertificate;->getSignatureValue()Lcom/android/sec/org/bouncycastle/asn1/DERBitString;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/DERBitString;->getBytes()[B

    move-result-object v0

    return-object v0
.end method

.method public getSignatureAlgorithm()Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;
    .locals 1

    .prologue
    .line 270
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cert/X509AttributeCertificateHolder;->attrCert:Lcom/android/sec/org/bouncycastle/asn1/x509/AttributeCertificate;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/x509/AttributeCertificate;->getSignatureAlgorithm()Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;

    move-result-object v0

    return-object v0
.end method

.method public getVersion()I
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cert/X509AttributeCertificateHolder;->attrCert:Lcom/android/sec/org/bouncycastle/asn1/x509/AttributeCertificate;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/x509/AttributeCertificate;->getAcinfo()Lcom/android/sec/org/bouncycastle/asn1/x509/AttributeCertificateInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/x509/AttributeCertificateInfo;->getVersion()Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;->getValue()Ljava/math/BigInteger;

    move-result-object v0

    invoke-virtual {v0}, Ljava/math/BigInteger;->intValue()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public hasExtensions()Z
    .locals 1

    .prologue
    .line 195
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cert/X509AttributeCertificateHolder;->extensions:Lcom/android/sec/org/bouncycastle/asn1/x509/Extensions;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 364
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cert/X509AttributeCertificateHolder;->attrCert:Lcom/android/sec/org/bouncycastle/asn1/x509/AttributeCertificate;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/x509/AttributeCertificate;->hashCode()I

    move-result v0

    return v0
.end method

.method public isSignatureValid(Lcom/android/sec/org/bouncycastle/operator/ContentVerifierProvider;)Z
    .locals 8
    .param p1, "verifierProvider"    # Lcom/android/sec/org/bouncycastle/operator/ContentVerifierProvider;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/cert/CertException;
        }
    .end annotation

    .prologue
    .line 316
    iget-object v5, p0, Lcom/android/sec/org/bouncycastle/cert/X509AttributeCertificateHolder;->attrCert:Lcom/android/sec/org/bouncycastle/asn1/x509/AttributeCertificate;

    invoke-virtual {v5}, Lcom/android/sec/org/bouncycastle/asn1/x509/AttributeCertificate;->getAcinfo()Lcom/android/sec/org/bouncycastle/asn1/x509/AttributeCertificateInfo;

    move-result-object v0

    .line 318
    .local v0, "acinfo":Lcom/android/sec/org/bouncycastle/asn1/x509/AttributeCertificateInfo;
    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/x509/AttributeCertificateInfo;->getSignature()Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;

    move-result-object v5

    iget-object v6, p0, Lcom/android/sec/org/bouncycastle/cert/X509AttributeCertificateHolder;->attrCert:Lcom/android/sec/org/bouncycastle/asn1/x509/AttributeCertificate;

    invoke-virtual {v6}, Lcom/android/sec/org/bouncycastle/asn1/x509/AttributeCertificate;->getSignatureAlgorithm()Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/android/sec/org/bouncycastle/cert/CertUtils;->isAlgIdEqual(Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 320
    new-instance v5, Lcom/android/sec/org/bouncycastle/cert/CertException;

    const-string v6, "signature invalid - algorithm identifier mismatch"

    invoke-direct {v5, v6}, Lcom/android/sec/org/bouncycastle/cert/CertException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 327
    :cond_0
    :try_start_0
    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/x509/AttributeCertificateInfo;->getSignature()Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;

    move-result-object v5

    invoke-interface {p1, v5}, Lcom/android/sec/org/bouncycastle/operator/ContentVerifierProvider;->get(Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;)Lcom/android/sec/org/bouncycastle/operator/ContentVerifier;

    move-result-object v4

    .line 329
    .local v4, "verifier":Lcom/android/sec/org/bouncycastle/operator/ContentVerifier;
    invoke-interface {v4}, Lcom/android/sec/org/bouncycastle/operator/ContentVerifier;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v3

    .line 330
    .local v3, "sOut":Ljava/io/OutputStream;
    new-instance v1, Lcom/android/sec/org/bouncycastle/asn1/DEROutputStream;

    invoke-direct {v1, v3}, Lcom/android/sec/org/bouncycastle/asn1/DEROutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 332
    .local v1, "dOut":Lcom/android/sec/org/bouncycastle/asn1/DEROutputStream;
    invoke-virtual {v1, v0}, Lcom/android/sec/org/bouncycastle/asn1/DEROutputStream;->writeObject(Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;)V

    .line 334
    invoke-virtual {v3}, Ljava/io/OutputStream;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 341
    iget-object v5, p0, Lcom/android/sec/org/bouncycastle/cert/X509AttributeCertificateHolder;->attrCert:Lcom/android/sec/org/bouncycastle/asn1/x509/AttributeCertificate;

    invoke-virtual {v5}, Lcom/android/sec/org/bouncycastle/asn1/x509/AttributeCertificate;->getSignatureValue()Lcom/android/sec/org/bouncycastle/asn1/DERBitString;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/sec/org/bouncycastle/asn1/DERBitString;->getBytes()[B

    move-result-object v5

    invoke-interface {v4, v5}, Lcom/android/sec/org/bouncycastle/operator/ContentVerifier;->verify([B)Z

    move-result v5

    return v5

    .line 336
    .end local v1    # "dOut":Lcom/android/sec/org/bouncycastle/asn1/DEROutputStream;
    .end local v3    # "sOut":Ljava/io/OutputStream;
    .end local v4    # "verifier":Lcom/android/sec/org/bouncycastle/operator/ContentVerifier;
    :catch_0
    move-exception v2

    .line 338
    .local v2, "e":Ljava/lang/Exception;
    new-instance v5, Lcom/android/sec/org/bouncycastle/cert/CertException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "unable to process signature: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6, v2}, Lcom/android/sec/org/bouncycastle/cert/CertException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v5
.end method

.method public isValidOn(Ljava/util/Date;)Z
    .locals 2
    .param p1, "date"    # Ljava/util/Date;

    .prologue
    .line 301
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/cert/X509AttributeCertificateHolder;->attrCert:Lcom/android/sec/org/bouncycastle/asn1/x509/AttributeCertificate;

    invoke-virtual {v1}, Lcom/android/sec/org/bouncycastle/asn1/x509/AttributeCertificate;->getAcinfo()Lcom/android/sec/org/bouncycastle/asn1/x509/AttributeCertificateInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/sec/org/bouncycastle/asn1/x509/AttributeCertificateInfo;->getAttrCertValidityPeriod()Lcom/android/sec/org/bouncycastle/asn1/x509/AttCertValidityPeriod;

    move-result-object v0

    .line 303
    .local v0, "certValidityPeriod":Lcom/android/sec/org/bouncycastle/asn1/x509/AttCertValidityPeriod;
    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/x509/AttCertValidityPeriod;->getNotBeforeTime()Lcom/android/sec/org/bouncycastle/asn1/ASN1GeneralizedTime;

    move-result-object v1

    invoke-static {v1}, Lcom/android/sec/org/bouncycastle/cert/CertUtils;->recoverDate(Lcom/android/sec/org/bouncycastle/asn1/ASN1GeneralizedTime;)Ljava/util/Date;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/util/Date;->before(Ljava/util/Date;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/x509/AttCertValidityPeriod;->getNotAfterTime()Lcom/android/sec/org/bouncycastle/asn1/ASN1GeneralizedTime;

    move-result-object v1

    invoke-static {v1}, Lcom/android/sec/org/bouncycastle/cert/CertUtils;->recoverDate(Lcom/android/sec/org/bouncycastle/asn1/ASN1GeneralizedTime;)Ljava/util/Date;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/util/Date;->after(Ljava/util/Date;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public toASN1Structure()Lcom/android/sec/org/bouncycastle/asn1/x509/AttributeCertificate;
    .locals 1

    .prologue
    .line 290
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cert/X509AttributeCertificateHolder;->attrCert:Lcom/android/sec/org/bouncycastle/asn1/x509/AttributeCertificate;

    return-object v0
.end method
