.class public Lcom/android/sec/org/bouncycastle/jcajce/provider/symmetric/Twofish$PBEWithSHA;
.super Lcom/android/sec/org/bouncycastle/jcajce/provider/symmetric/util/BaseBlockCipher;
.source "Twofish.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/sec/org/bouncycastle/jcajce/provider/symmetric/Twofish;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PBEWithSHA"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 86
    new-instance v0, Lcom/android/sec/org/bouncycastle/crypto/modes/CBCBlockCipher;

    new-instance v1, Lcom/android/sec/org/bouncycastle/crypto/engines/TwofishEngine;

    invoke-direct {v1}, Lcom/android/sec/org/bouncycastle/crypto/engines/TwofishEngine;-><init>()V

    invoke-direct {v0, v1}, Lcom/android/sec/org/bouncycastle/crypto/modes/CBCBlockCipher;-><init>(Lcom/android/sec/org/bouncycastle/crypto/BlockCipher;)V

    invoke-direct {p0, v0}, Lcom/android/sec/org/bouncycastle/jcajce/provider/symmetric/util/BaseBlockCipher;-><init>(Lcom/android/sec/org/bouncycastle/crypto/BlockCipher;)V

    .line 87
    return-void
.end method
