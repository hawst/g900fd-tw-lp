.class public Lcom/android/sec/org/bouncycastle/cert/X509CRLHolder;
.super Ljava/lang/Object;
.source "X509CRLHolder.java"


# instance fields
.field private extensions:Lcom/android/sec/org/bouncycastle/asn1/x509/Extensions;

.field private isIndirect:Z

.field private issuerName:Lcom/android/sec/org/bouncycastle/asn1/x509/GeneralNames;

.field private x509CRL:Lcom/android/sec/org/bouncycastle/asn1/x509/CertificateList;


# direct methods
.method public constructor <init>(Lcom/android/sec/org/bouncycastle/asn1/x509/CertificateList;)V
    .locals 3
    .param p1, "x509CRL"    # Lcom/android/sec/org/bouncycastle/asn1/x509/CertificateList;

    .prologue
    .line 97
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 98
    iput-object p1, p0, Lcom/android/sec/org/bouncycastle/cert/X509CRLHolder;->x509CRL:Lcom/android/sec/org/bouncycastle/asn1/x509/CertificateList;

    .line 99
    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/asn1/x509/CertificateList;->getTBSCertList()Lcom/android/sec/org/bouncycastle/asn1/x509/TBSCertList;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/x509/TBSCertList;->getExtensions()Lcom/android/sec/org/bouncycastle/asn1/x509/Extensions;

    move-result-object v0

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/cert/X509CRLHolder;->extensions:Lcom/android/sec/org/bouncycastle/asn1/x509/Extensions;

    .line 100
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cert/X509CRLHolder;->extensions:Lcom/android/sec/org/bouncycastle/asn1/x509/Extensions;

    invoke-static {v0}, Lcom/android/sec/org/bouncycastle/cert/X509CRLHolder;->isIndirectCRL(Lcom/android/sec/org/bouncycastle/asn1/x509/Extensions;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/sec/org/bouncycastle/cert/X509CRLHolder;->isIndirect:Z

    .line 101
    new-instance v0, Lcom/android/sec/org/bouncycastle/asn1/x509/GeneralNames;

    new-instance v1, Lcom/android/sec/org/bouncycastle/asn1/x509/GeneralName;

    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/asn1/x509/CertificateList;->getIssuer()Lcom/android/sec/org/bouncycastle/asn1/x500/X500Name;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/android/sec/org/bouncycastle/asn1/x509/GeneralName;-><init>(Lcom/android/sec/org/bouncycastle/asn1/x500/X500Name;)V

    invoke-direct {v0, v1}, Lcom/android/sec/org/bouncycastle/asn1/x509/GeneralNames;-><init>(Lcom/android/sec/org/bouncycastle/asn1/x509/GeneralName;)V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/cert/X509CRLHolder;->issuerName:Lcom/android/sec/org/bouncycastle/asn1/x509/GeneralNames;

    .line 102
    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 1
    .param p1, "crlStream"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 88
    invoke-static {p1}, Lcom/android/sec/org/bouncycastle/cert/X509CRLHolder;->parseStream(Ljava/io/InputStream;)Lcom/android/sec/org/bouncycastle/asn1/x509/CertificateList;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/sec/org/bouncycastle/cert/X509CRLHolder;-><init>(Lcom/android/sec/org/bouncycastle/asn1/x509/CertificateList;)V

    .line 89
    return-void
.end method

.method public constructor <init>([B)V
    .locals 1
    .param p1, "crlEncoding"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 76
    new-instance v0, Ljava/io/ByteArrayInputStream;

    invoke-direct {v0, p1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-static {v0}, Lcom/android/sec/org/bouncycastle/cert/X509CRLHolder;->parseStream(Ljava/io/InputStream;)Lcom/android/sec/org/bouncycastle/asn1/x509/CertificateList;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/sec/org/bouncycastle/cert/X509CRLHolder;-><init>(Lcom/android/sec/org/bouncycastle/asn1/x509/CertificateList;)V

    .line 77
    return-void
.end method

.method private static isIndirectCRL(Lcom/android/sec/org/bouncycastle/asn1/x509/Extensions;)Z
    .locals 3
    .param p0, "extensions"    # Lcom/android/sec/org/bouncycastle/asn1/x509/Extensions;

    .prologue
    const/4 v1, 0x0

    .line 57
    if-nez p0, :cond_1

    .line 64
    :cond_0
    :goto_0
    return v1

    .line 62
    :cond_1
    sget-object v2, Lcom/android/sec/org/bouncycastle/asn1/x509/Extension;->issuingDistributionPoint:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    invoke-virtual {p0, v2}, Lcom/android/sec/org/bouncycastle/asn1/x509/Extensions;->getExtension(Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;)Lcom/android/sec/org/bouncycastle/asn1/x509/Extension;

    move-result-object v0

    .line 64
    .local v0, "ext":Lcom/android/sec/org/bouncycastle/asn1/x509/Extension;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/x509/Extension;->getParsedValue()Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;

    move-result-object v2

    invoke-static {v2}, Lcom/android/sec/org/bouncycastle/asn1/x509/IssuingDistributionPoint;->getInstance(Ljava/lang/Object;)Lcom/android/sec/org/bouncycastle/asn1/x509/IssuingDistributionPoint;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/sec/org/bouncycastle/asn1/x509/IssuingDistributionPoint;->isIndirectCRL()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method private static parseStream(Ljava/io/InputStream;)Lcom/android/sec/org/bouncycastle/asn1/x509/CertificateList;
    .locals 4
    .param p0, "stream"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 43
    :try_start_0
    new-instance v1, Lcom/android/sec/org/bouncycastle/asn1/ASN1InputStream;

    const/4 v2, 0x1

    invoke-direct {v1, p0, v2}, Lcom/android/sec/org/bouncycastle/asn1/ASN1InputStream;-><init>(Ljava/io/InputStream;Z)V

    invoke-virtual {v1}, Lcom/android/sec/org/bouncycastle/asn1/ASN1InputStream;->readObject()Lcom/android/sec/org/bouncycastle/asn1/ASN1Primitive;

    move-result-object v1

    invoke-static {v1}, Lcom/android/sec/org/bouncycastle/asn1/x509/CertificateList;->getInstance(Ljava/lang/Object;)Lcom/android/sec/org/bouncycastle/asn1/x509/CertificateList;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v1

    return-object v1

    .line 45
    :catch_0
    move-exception v0

    .line 47
    .local v0, "e":Ljava/lang/ClassCastException;
    new-instance v1, Lcom/android/sec/org/bouncycastle/cert/CertIOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "malformed data: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/ClassCastException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/android/sec/org/bouncycastle/cert/CertIOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 49
    .end local v0    # "e":Ljava/lang/ClassCastException;
    :catch_1
    move-exception v0

    .line 51
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    new-instance v1, Lcom/android/sec/org/bouncycastle/cert/CertIOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "malformed data: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/android/sec/org/bouncycastle/cert/CertIOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 298
    if-ne p1, p0, :cond_0

    .line 300
    const/4 v1, 0x1

    .line 310
    :goto_0
    return v1

    .line 303
    :cond_0
    instance-of v1, p1, Lcom/android/sec/org/bouncycastle/cert/X509CRLHolder;

    if-nez v1, :cond_1

    .line 305
    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    move-object v0, p1

    .line 308
    check-cast v0, Lcom/android/sec/org/bouncycastle/cert/X509CRLHolder;

    .line 310
    .local v0, "other":Lcom/android/sec/org/bouncycastle/cert/X509CRLHolder;
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/cert/X509CRLHolder;->x509CRL:Lcom/android/sec/org/bouncycastle/asn1/x509/CertificateList;

    iget-object v2, v0, Lcom/android/sec/org/bouncycastle/cert/X509CRLHolder;->x509CRL:Lcom/android/sec/org/bouncycastle/asn1/x509/CertificateList;

    invoke-virtual {v1, v2}, Lcom/android/sec/org/bouncycastle/asn1/x509/CertificateList;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method

.method public getCriticalExtensionOIDs()Ljava/util/Set;
    .locals 1

    .prologue
    .line 233
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cert/X509CRLHolder;->extensions:Lcom/android/sec/org/bouncycastle/asn1/x509/Extensions;

    invoke-static {v0}, Lcom/android/sec/org/bouncycastle/cert/CertUtils;->getCriticalExtensionOIDs(Lcom/android/sec/org/bouncycastle/asn1/x509/Extensions;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public getEncoded()[B
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 113
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cert/X509CRLHolder;->x509CRL:Lcom/android/sec/org/bouncycastle/asn1/x509/CertificateList;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/x509/CertificateList;->getEncoded()[B

    move-result-object v0

    return-object v0
.end method

.method public getExtension(Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;)Lcom/android/sec/org/bouncycastle/asn1/x509/Extension;
    .locals 1
    .param p1, "oid"    # Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    .prologue
    .line 196
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cert/X509CRLHolder;->extensions:Lcom/android/sec/org/bouncycastle/asn1/x509/Extensions;

    if-eqz v0, :cond_0

    .line 198
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cert/X509CRLHolder;->extensions:Lcom/android/sec/org/bouncycastle/asn1/x509/Extensions;

    invoke-virtual {v0, p1}, Lcom/android/sec/org/bouncycastle/asn1/x509/Extensions;->getExtension(Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;)Lcom/android/sec/org/bouncycastle/asn1/x509/Extension;

    move-result-object v0

    .line 201
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getExtensionOIDs()Ljava/util/List;
    .locals 1

    .prologue
    .line 222
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cert/X509CRLHolder;->extensions:Lcom/android/sec/org/bouncycastle/asn1/x509/Extensions;

    invoke-static {v0}, Lcom/android/sec/org/bouncycastle/cert/CertUtils;->getExtensionOIDs(Lcom/android/sec/org/bouncycastle/asn1/x509/Extensions;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getExtensions()Lcom/android/sec/org/bouncycastle/asn1/x509/Extensions;
    .locals 1

    .prologue
    .line 211
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cert/X509CRLHolder;->extensions:Lcom/android/sec/org/bouncycastle/asn1/x509/Extensions;

    return-object v0
.end method

.method public getIssuer()Lcom/android/sec/org/bouncycastle/asn1/x500/X500Name;
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cert/X509CRLHolder;->x509CRL:Lcom/android/sec/org/bouncycastle/asn1/x509/CertificateList;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/x509/CertificateList;->getIssuer()Lcom/android/sec/org/bouncycastle/asn1/x500/X500Name;

    move-result-object v0

    invoke-static {v0}, Lcom/android/sec/org/bouncycastle/asn1/x500/X500Name;->getInstance(Ljava/lang/Object;)Lcom/android/sec/org/bouncycastle/asn1/x500/X500Name;

    move-result-object v0

    return-object v0
.end method

.method public getNonCriticalExtensionOIDs()Ljava/util/Set;
    .locals 1

    .prologue
    .line 244
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cert/X509CRLHolder;->extensions:Lcom/android/sec/org/bouncycastle/asn1/x509/Extensions;

    invoke-static {v0}, Lcom/android/sec/org/bouncycastle/cert/CertUtils;->getNonCriticalExtensionOIDs(Lcom/android/sec/org/bouncycastle/asn1/x509/Extensions;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public getRevokedCertificate(Ljava/math/BigInteger;)Lcom/android/sec/org/bouncycastle/cert/X509CRLEntryHolder;
    .locals 6
    .param p1, "serialNumber"    # Ljava/math/BigInteger;

    .prologue
    .line 128
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cert/X509CRLHolder;->issuerName:Lcom/android/sec/org/bouncycastle/asn1/x509/GeneralNames;

    .line 129
    .local v0, "currentCA":Lcom/android/sec/org/bouncycastle/asn1/x509/GeneralNames;
    iget-object v4, p0, Lcom/android/sec/org/bouncycastle/cert/X509CRLHolder;->x509CRL:Lcom/android/sec/org/bouncycastle/asn1/x509/CertificateList;

    invoke-virtual {v4}, Lcom/android/sec/org/bouncycastle/asn1/x509/CertificateList;->getRevokedCertificateEnumeration()Ljava/util/Enumeration;

    move-result-object v2

    .local v2, "en":Ljava/util/Enumeration;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 131
    invoke-interface {v2}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/sec/org/bouncycastle/asn1/x509/TBSCertList$CRLEntry;

    .line 133
    .local v3, "entry":Lcom/android/sec/org/bouncycastle/asn1/x509/TBSCertList$CRLEntry;
    invoke-virtual {v3}, Lcom/android/sec/org/bouncycastle/asn1/x509/TBSCertList$CRLEntry;->getUserCertificate()Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;->getValue()Ljava/math/BigInteger;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/math/BigInteger;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 135
    new-instance v4, Lcom/android/sec/org/bouncycastle/cert/X509CRLEntryHolder;

    iget-boolean v5, p0, Lcom/android/sec/org/bouncycastle/cert/X509CRLHolder;->isIndirect:Z

    invoke-direct {v4, v3, v5, v0}, Lcom/android/sec/org/bouncycastle/cert/X509CRLEntryHolder;-><init>(Lcom/android/sec/org/bouncycastle/asn1/x509/TBSCertList$CRLEntry;ZLcom/android/sec/org/bouncycastle/asn1/x509/GeneralNames;)V

    .line 149
    .end local v3    # "entry":Lcom/android/sec/org/bouncycastle/asn1/x509/TBSCertList$CRLEntry;
    :goto_1
    return-object v4

    .line 138
    .restart local v3    # "entry":Lcom/android/sec/org/bouncycastle/asn1/x509/TBSCertList$CRLEntry;
    :cond_1
    iget-boolean v4, p0, Lcom/android/sec/org/bouncycastle/cert/X509CRLHolder;->isIndirect:Z

    if-eqz v4, :cond_0

    invoke-virtual {v3}, Lcom/android/sec/org/bouncycastle/asn1/x509/TBSCertList$CRLEntry;->hasExtensions()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 140
    invoke-virtual {v3}, Lcom/android/sec/org/bouncycastle/asn1/x509/TBSCertList$CRLEntry;->getExtensions()Lcom/android/sec/org/bouncycastle/asn1/x509/Extensions;

    move-result-object v4

    sget-object v5, Lcom/android/sec/org/bouncycastle/asn1/x509/Extension;->certificateIssuer:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    invoke-virtual {v4, v5}, Lcom/android/sec/org/bouncycastle/asn1/x509/Extensions;->getExtension(Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;)Lcom/android/sec/org/bouncycastle/asn1/x509/Extension;

    move-result-object v1

    .line 142
    .local v1, "currentCaName":Lcom/android/sec/org/bouncycastle/asn1/x509/Extension;
    if-eqz v1, :cond_0

    .line 144
    invoke-virtual {v1}, Lcom/android/sec/org/bouncycastle/asn1/x509/Extension;->getParsedValue()Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;

    move-result-object v4

    invoke-static {v4}, Lcom/android/sec/org/bouncycastle/asn1/x509/GeneralNames;->getInstance(Ljava/lang/Object;)Lcom/android/sec/org/bouncycastle/asn1/x509/GeneralNames;

    move-result-object v0

    goto :goto_0

    .line 149
    .end local v1    # "currentCaName":Lcom/android/sec/org/bouncycastle/asn1/x509/Extension;
    .end local v3    # "entry":Lcom/android/sec/org/bouncycastle/asn1/x509/TBSCertList$CRLEntry;
    :cond_2
    const/4 v4, 0x0

    goto :goto_1
.end method

.method public getRevokedCertificates()Ljava/util/Collection;
    .locals 7

    .prologue
    .line 160
    iget-object v6, p0, Lcom/android/sec/org/bouncycastle/cert/X509CRLHolder;->x509CRL:Lcom/android/sec/org/bouncycastle/asn1/x509/CertificateList;

    invoke-virtual {v6}, Lcom/android/sec/org/bouncycastle/asn1/x509/CertificateList;->getRevokedCertificates()[Lcom/android/sec/org/bouncycastle/asn1/x509/TBSCertList$CRLEntry;

    move-result-object v3

    .line 161
    .local v3, "entries":[Lcom/android/sec/org/bouncycastle/asn1/x509/TBSCertList$CRLEntry;
    new-instance v5, Ljava/util/ArrayList;

    array-length v6, v3

    invoke-direct {v5, v6}, Ljava/util/ArrayList;-><init>(I)V

    .line 162
    .local v5, "l":Ljava/util/List;
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/cert/X509CRLHolder;->issuerName:Lcom/android/sec/org/bouncycastle/asn1/x509/GeneralNames;

    .line 164
    .local v1, "currentCA":Lcom/android/sec/org/bouncycastle/asn1/x509/GeneralNames;
    iget-object v6, p0, Lcom/android/sec/org/bouncycastle/cert/X509CRLHolder;->x509CRL:Lcom/android/sec/org/bouncycastle/asn1/x509/CertificateList;

    invoke-virtual {v6}, Lcom/android/sec/org/bouncycastle/asn1/x509/CertificateList;->getRevokedCertificateEnumeration()Ljava/util/Enumeration;

    move-result-object v2

    .local v2, "en":Ljava/util/Enumeration;
    :goto_0
    invoke-interface {v2}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 166
    invoke-interface {v2}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/sec/org/bouncycastle/asn1/x509/TBSCertList$CRLEntry;

    .line 167
    .local v4, "entry":Lcom/android/sec/org/bouncycastle/asn1/x509/TBSCertList$CRLEntry;
    new-instance v0, Lcom/android/sec/org/bouncycastle/cert/X509CRLEntryHolder;

    iget-boolean v6, p0, Lcom/android/sec/org/bouncycastle/cert/X509CRLHolder;->isIndirect:Z

    invoke-direct {v0, v4, v6, v1}, Lcom/android/sec/org/bouncycastle/cert/X509CRLEntryHolder;-><init>(Lcom/android/sec/org/bouncycastle/asn1/x509/TBSCertList$CRLEntry;ZLcom/android/sec/org/bouncycastle/asn1/x509/GeneralNames;)V

    .line 169
    .local v0, "crlEntry":Lcom/android/sec/org/bouncycastle/cert/X509CRLEntryHolder;
    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 171
    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/cert/X509CRLEntryHolder;->getCertificateIssuer()Lcom/android/sec/org/bouncycastle/asn1/x509/GeneralNames;

    move-result-object v1

    .line 172
    goto :goto_0

    .line 174
    .end local v0    # "crlEntry":Lcom/android/sec/org/bouncycastle/cert/X509CRLEntryHolder;
    .end local v4    # "entry":Lcom/android/sec/org/bouncycastle/asn1/x509/TBSCertList$CRLEntry;
    :cond_0
    return-object v5
.end method

.method public hasExtensions()Z
    .locals 1

    .prologue
    .line 184
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cert/X509CRLHolder;->extensions:Lcom/android/sec/org/bouncycastle/asn1/x509/Extensions;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 315
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cert/X509CRLHolder;->x509CRL:Lcom/android/sec/org/bouncycastle/asn1/x509/CertificateList;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/x509/CertificateList;->hashCode()I

    move-result v0

    return v0
.end method

.method public isSignatureValid(Lcom/android/sec/org/bouncycastle/operator/ContentVerifierProvider;)Z
    .locals 8
    .param p1, "verifierProvider"    # Lcom/android/sec/org/bouncycastle/operator/ContentVerifierProvider;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/cert/CertException;
        }
    .end annotation

    .prologue
    .line 267
    iget-object v5, p0, Lcom/android/sec/org/bouncycastle/cert/X509CRLHolder;->x509CRL:Lcom/android/sec/org/bouncycastle/asn1/x509/CertificateList;

    invoke-virtual {v5}, Lcom/android/sec/org/bouncycastle/asn1/x509/CertificateList;->getTBSCertList()Lcom/android/sec/org/bouncycastle/asn1/x509/TBSCertList;

    move-result-object v3

    .line 269
    .local v3, "tbsCRL":Lcom/android/sec/org/bouncycastle/asn1/x509/TBSCertList;
    invoke-virtual {v3}, Lcom/android/sec/org/bouncycastle/asn1/x509/TBSCertList;->getSignature()Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;

    move-result-object v5

    iget-object v6, p0, Lcom/android/sec/org/bouncycastle/cert/X509CRLHolder;->x509CRL:Lcom/android/sec/org/bouncycastle/asn1/x509/CertificateList;

    invoke-virtual {v6}, Lcom/android/sec/org/bouncycastle/asn1/x509/CertificateList;->getSignatureAlgorithm()Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/android/sec/org/bouncycastle/cert/CertUtils;->isAlgIdEqual(Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 271
    new-instance v5, Lcom/android/sec/org/bouncycastle/cert/CertException;

    const-string v6, "signature invalid - algorithm identifier mismatch"

    invoke-direct {v5, v6}, Lcom/android/sec/org/bouncycastle/cert/CertException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 278
    :cond_0
    :try_start_0
    invoke-virtual {v3}, Lcom/android/sec/org/bouncycastle/asn1/x509/TBSCertList;->getSignature()Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;

    move-result-object v5

    invoke-interface {p1, v5}, Lcom/android/sec/org/bouncycastle/operator/ContentVerifierProvider;->get(Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;)Lcom/android/sec/org/bouncycastle/operator/ContentVerifier;

    move-result-object v4

    .line 280
    .local v4, "verifier":Lcom/android/sec/org/bouncycastle/operator/ContentVerifier;
    invoke-interface {v4}, Lcom/android/sec/org/bouncycastle/operator/ContentVerifier;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v2

    .line 281
    .local v2, "sOut":Ljava/io/OutputStream;
    new-instance v0, Lcom/android/sec/org/bouncycastle/asn1/DEROutputStream;

    invoke-direct {v0, v2}, Lcom/android/sec/org/bouncycastle/asn1/DEROutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 283
    .local v0, "dOut":Lcom/android/sec/org/bouncycastle/asn1/DEROutputStream;
    invoke-virtual {v0, v3}, Lcom/android/sec/org/bouncycastle/asn1/DEROutputStream;->writeObject(Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;)V

    .line 285
    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 292
    iget-object v5, p0, Lcom/android/sec/org/bouncycastle/cert/X509CRLHolder;->x509CRL:Lcom/android/sec/org/bouncycastle/asn1/x509/CertificateList;

    invoke-virtual {v5}, Lcom/android/sec/org/bouncycastle/asn1/x509/CertificateList;->getSignature()Lcom/android/sec/org/bouncycastle/asn1/DERBitString;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/sec/org/bouncycastle/asn1/DERBitString;->getBytes()[B

    move-result-object v5

    invoke-interface {v4, v5}, Lcom/android/sec/org/bouncycastle/operator/ContentVerifier;->verify([B)Z

    move-result v5

    return v5

    .line 287
    .end local v0    # "dOut":Lcom/android/sec/org/bouncycastle/asn1/DEROutputStream;
    .end local v2    # "sOut":Ljava/io/OutputStream;
    .end local v4    # "verifier":Lcom/android/sec/org/bouncycastle/operator/ContentVerifier;
    :catch_0
    move-exception v1

    .line 289
    .local v1, "e":Ljava/lang/Exception;
    new-instance v5, Lcom/android/sec/org/bouncycastle/cert/CertException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "unable to process signature: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6, v1}, Lcom/android/sec/org/bouncycastle/cert/CertException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v5
.end method

.method public toASN1Structure()Lcom/android/sec/org/bouncycastle/asn1/x509/CertificateList;
    .locals 1

    .prologue
    .line 254
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cert/X509CRLHolder;->x509CRL:Lcom/android/sec/org/bouncycastle/asn1/x509/CertificateList;

    return-object v0
.end method
