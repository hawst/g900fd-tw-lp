.class public Lcom/android/sec/org/bouncycastle/crypto/modes/CCMBlockCipher;
.super Ljava/lang/Object;
.source "CCMBlockCipher.java"

# interfaces
.implements Lcom/android/sec/org/bouncycastle/crypto/modes/AEADBlockCipher;


# instance fields
.field private associatedText:Ljava/io/ByteArrayOutputStream;

.field private blockSize:I

.field private cipher:Lcom/android/sec/org/bouncycastle/crypto/BlockCipher;

.field private data:Ljava/io/ByteArrayOutputStream;

.field private forEncryption:Z

.field private initialAssociatedText:[B

.field private keyParam:Lcom/android/sec/org/bouncycastle/crypto/CipherParameters;

.field private macBlock:[B

.field private macSize:I

.field private nonce:[B


# direct methods
.method public constructor <init>(Lcom/android/sec/org/bouncycastle/crypto/BlockCipher;)V
    .locals 2
    .param p1, "c"    # Lcom/android/sec/org/bouncycastle/crypto/BlockCipher;

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/crypto/modes/CCMBlockCipher;->associatedText:Ljava/io/ByteArrayOutputStream;

    .line 33
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/crypto/modes/CCMBlockCipher;->data:Ljava/io/ByteArrayOutputStream;

    .line 42
    iput-object p1, p0, Lcom/android/sec/org/bouncycastle/crypto/modes/CCMBlockCipher;->cipher:Lcom/android/sec/org/bouncycastle/crypto/BlockCipher;

    .line 43
    invoke-interface {p1}, Lcom/android/sec/org/bouncycastle/crypto/BlockCipher;->getBlockSize()I

    move-result v0

    iput v0, p0, Lcom/android/sec/org/bouncycastle/crypto/modes/CCMBlockCipher;->blockSize:I

    .line 44
    iget v0, p0, Lcom/android/sec/org/bouncycastle/crypto/modes/CCMBlockCipher;->blockSize:I

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/crypto/modes/CCMBlockCipher;->macBlock:[B

    .line 46
    iget v0, p0, Lcom/android/sec/org/bouncycastle/crypto/modes/CCMBlockCipher;->blockSize:I

    const/16 v1, 0x10

    if-eq v0, v1, :cond_0

    .line 48
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "cipher required with a block size of 16."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 50
    :cond_0
    return-void
.end method

.method private calculateMac([BII[B)I
    .locals 13
    .param p1, "data"    # [B
    .param p2, "dataOff"    # I
    .param p3, "dataLen"    # I
    .param p4, "macBlock"    # [B

    .prologue
    .line 283
    new-instance v2, Lcom/android/sec/org/bouncycastle/crypto/macs/CBCBlockCipherMac;

    iget-object v9, p0, Lcom/android/sec/org/bouncycastle/crypto/modes/CCMBlockCipher;->cipher:Lcom/android/sec/org/bouncycastle/crypto/BlockCipher;

    iget v10, p0, Lcom/android/sec/org/bouncycastle/crypto/modes/CCMBlockCipher;->macSize:I

    mul-int/lit8 v10, v10, 0x8

    invoke-direct {v2, v9, v10}, Lcom/android/sec/org/bouncycastle/crypto/macs/CBCBlockCipherMac;-><init>(Lcom/android/sec/org/bouncycastle/crypto/BlockCipher;I)V

    .line 285
    .local v2, "cMac":Lcom/android/sec/org/bouncycastle/crypto/Mac;
    iget-object v9, p0, Lcom/android/sec/org/bouncycastle/crypto/modes/CCMBlockCipher;->keyParam:Lcom/android/sec/org/bouncycastle/crypto/CipherParameters;

    invoke-interface {v2, v9}, Lcom/android/sec/org/bouncycastle/crypto/Mac;->init(Lcom/android/sec/org/bouncycastle/crypto/CipherParameters;)V

    .line 290
    const/16 v9, 0x10

    new-array v1, v9, [B

    .line 292
    .local v1, "b0":[B
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/crypto/modes/CCMBlockCipher;->hasAssociatedText()Z

    move-result v9

    if-eqz v9, :cond_0

    .line 294
    const/4 v9, 0x0

    aget-byte v10, v1, v9

    or-int/lit8 v10, v10, 0x40

    int-to-byte v10, v10

    aput-byte v10, v1, v9

    .line 297
    :cond_0
    const/4 v9, 0x0

    aget-byte v10, v1, v9

    invoke-interface {v2}, Lcom/android/sec/org/bouncycastle/crypto/Mac;->getMacSize()I

    move-result v11

    add-int/lit8 v11, v11, -0x2

    div-int/lit8 v11, v11, 0x2

    and-int/lit8 v11, v11, 0x7

    shl-int/lit8 v11, v11, 0x3

    or-int/2addr v10, v11

    int-to-byte v10, v10

    aput-byte v10, v1, v9

    .line 299
    const/4 v9, 0x0

    aget-byte v10, v1, v9

    iget-object v11, p0, Lcom/android/sec/org/bouncycastle/crypto/modes/CCMBlockCipher;->nonce:[B

    array-length v11, v11

    rsub-int/lit8 v11, v11, 0xf

    add-int/lit8 v11, v11, -0x1

    and-int/lit8 v11, v11, 0x7

    or-int/2addr v10, v11

    int-to-byte v10, v10

    aput-byte v10, v1, v9

    .line 301
    iget-object v9, p0, Lcom/android/sec/org/bouncycastle/crypto/modes/CCMBlockCipher;->nonce:[B

    const/4 v10, 0x0

    const/4 v11, 0x1

    iget-object v12, p0, Lcom/android/sec/org/bouncycastle/crypto/modes/CCMBlockCipher;->nonce:[B

    array-length v12, v12

    invoke-static {v9, v10, v1, v11, v12}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 303
    move/from16 v6, p3

    .line 304
    .local v6, "q":I
    const/4 v3, 0x1

    .line 305
    .local v3, "count":I
    :goto_0
    if-lez v6, :cond_1

    .line 307
    array-length v9, v1

    sub-int/2addr v9, v3

    and-int/lit16 v10, v6, 0xff

    int-to-byte v10, v10

    aput-byte v10, v1, v9

    .line 308
    ushr-int/lit8 v6, v6, 0x8

    .line 309
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 312
    :cond_1
    const/4 v9, 0x0

    array-length v10, v1

    invoke-interface {v2, v1, v9, v10}, Lcom/android/sec/org/bouncycastle/crypto/Mac;->update([BII)V

    .line 317
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/crypto/modes/CCMBlockCipher;->hasAssociatedText()Z

    move-result v9

    if-eqz v9, :cond_5

    .line 321
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/crypto/modes/CCMBlockCipher;->getAssociatedTextLength()I

    move-result v7

    .line 322
    .local v7, "textLength":I
    const v9, 0xff00

    if-ge v7, v9, :cond_4

    .line 324
    shr-int/lit8 v9, v7, 0x8

    int-to-byte v9, v9

    invoke-interface {v2, v9}, Lcom/android/sec/org/bouncycastle/crypto/Mac;->update(B)V

    .line 325
    int-to-byte v9, v7

    invoke-interface {v2, v9}, Lcom/android/sec/org/bouncycastle/crypto/Mac;->update(B)V

    .line 327
    const/4 v4, 0x2

    .line 341
    .local v4, "extra":I
    :goto_1
    iget-object v9, p0, Lcom/android/sec/org/bouncycastle/crypto/modes/CCMBlockCipher;->initialAssociatedText:[B

    if-eqz v9, :cond_2

    .line 343
    iget-object v9, p0, Lcom/android/sec/org/bouncycastle/crypto/modes/CCMBlockCipher;->initialAssociatedText:[B

    const/4 v10, 0x0

    iget-object v11, p0, Lcom/android/sec/org/bouncycastle/crypto/modes/CCMBlockCipher;->initialAssociatedText:[B

    array-length v11, v11

    invoke-interface {v2, v9, v10, v11}, Lcom/android/sec/org/bouncycastle/crypto/Mac;->update([BII)V

    .line 345
    :cond_2
    iget-object v9, p0, Lcom/android/sec/org/bouncycastle/crypto/modes/CCMBlockCipher;->associatedText:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v9}, Ljava/io/ByteArrayOutputStream;->size()I

    move-result v9

    if-lez v9, :cond_3

    .line 347
    iget-object v9, p0, Lcom/android/sec/org/bouncycastle/crypto/modes/CCMBlockCipher;->associatedText:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v9}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v8

    .line 348
    .local v8, "tmp":[B
    const/4 v9, 0x0

    array-length v10, v8

    invoke-interface {v2, v8, v9, v10}, Lcom/android/sec/org/bouncycastle/crypto/Mac;->update([BII)V

    .line 351
    .end local v8    # "tmp":[B
    :cond_3
    add-int v9, v4, v7

    rem-int/lit8 v4, v9, 0x10

    .line 352
    if-eqz v4, :cond_5

    .line 354
    move v5, v4

    .local v5, "i":I
    :goto_2
    const/16 v9, 0x10

    if-eq v5, v9, :cond_5

    .line 356
    const/4 v9, 0x0

    invoke-interface {v2, v9}, Lcom/android/sec/org/bouncycastle/crypto/Mac;->update(B)V

    .line 354
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 331
    .end local v4    # "extra":I
    .end local v5    # "i":I
    :cond_4
    const/4 v9, -0x1

    invoke-interface {v2, v9}, Lcom/android/sec/org/bouncycastle/crypto/Mac;->update(B)V

    .line 332
    const/4 v9, -0x2

    invoke-interface {v2, v9}, Lcom/android/sec/org/bouncycastle/crypto/Mac;->update(B)V

    .line 333
    shr-int/lit8 v9, v7, 0x18

    int-to-byte v9, v9

    invoke-interface {v2, v9}, Lcom/android/sec/org/bouncycastle/crypto/Mac;->update(B)V

    .line 334
    shr-int/lit8 v9, v7, 0x10

    int-to-byte v9, v9

    invoke-interface {v2, v9}, Lcom/android/sec/org/bouncycastle/crypto/Mac;->update(B)V

    .line 335
    shr-int/lit8 v9, v7, 0x8

    int-to-byte v9, v9

    invoke-interface {v2, v9}, Lcom/android/sec/org/bouncycastle/crypto/Mac;->update(B)V

    .line 336
    int-to-byte v9, v7

    invoke-interface {v2, v9}, Lcom/android/sec/org/bouncycastle/crypto/Mac;->update(B)V

    .line 338
    const/4 v4, 0x6

    .restart local v4    # "extra":I
    goto :goto_1

    .line 364
    .end local v4    # "extra":I
    .end local v7    # "textLength":I
    :cond_5
    move/from16 v0, p3

    invoke-interface {v2, p1, p2, v0}, Lcom/android/sec/org/bouncycastle/crypto/Mac;->update([BII)V

    .line 366
    const/4 v9, 0x0

    move-object/from16 v0, p4

    invoke-interface {v2, v0, v9}, Lcom/android/sec/org/bouncycastle/crypto/Mac;->doFinal([BI)I

    move-result v9

    return v9
.end method

.method private getAssociatedTextLength()I
    .locals 2

    .prologue
    .line 371
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/crypto/modes/CCMBlockCipher;->associatedText:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->size()I

    move-result v1

    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/crypto/modes/CCMBlockCipher;->initialAssociatedText:[B

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/2addr v0, v1

    return v0

    :cond_0
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/crypto/modes/CCMBlockCipher;->initialAssociatedText:[B

    array-length v0, v0

    goto :goto_0
.end method

.method private hasAssociatedText()Z
    .locals 1

    .prologue
    .line 376
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/crypto/modes/CCMBlockCipher;->getAssociatedTextLength()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public doFinal([BI)I
    .locals 4
    .param p1, "out"    # [B
    .param p2, "outOff"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;,
            Lcom/android/sec/org/bouncycastle/crypto/InvalidCipherTextException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 132
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/crypto/modes/CCMBlockCipher;->data:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    .line 133
    .local v1, "text":[B
    array-length v2, v1

    invoke-virtual {p0, v1, v3, v2}, Lcom/android/sec/org/bouncycastle/crypto/modes/CCMBlockCipher;->processPacket([BII)[B

    move-result-object v0

    .line 135
    .local v0, "enc":[B
    array-length v2, v0

    invoke-static {v0, v3, p1, p2, v2}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 137
    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/crypto/modes/CCMBlockCipher;->reset()V

    .line 139
    array-length v2, v0

    return v2
.end method

.method public getAlgorithmName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 99
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/crypto/modes/CCMBlockCipher;->cipher:Lcom/android/sec/org/bouncycastle/crypto/BlockCipher;

    invoke-interface {v1}, Lcom/android/sec/org/bouncycastle/crypto/BlockCipher;->getAlgorithmName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/CCM"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getMac()[B
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 157
    iget v1, p0, Lcom/android/sec/org/bouncycastle/crypto/modes/CCMBlockCipher;->macSize:I

    new-array v0, v1, [B

    .line 159
    .local v0, "mac":[B
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/crypto/modes/CCMBlockCipher;->macBlock:[B

    array-length v2, v0

    invoke-static {v1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 161
    return-object v0
.end method

.method public getOutputSize(I)I
    .locals 2
    .param p1, "len"    # I

    .prologue
    .line 171
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/crypto/modes/CCMBlockCipher;->data:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->size()I

    move-result v1

    add-int v0, p1, v1

    .line 173
    .local v0, "totalData":I
    iget-boolean v1, p0, Lcom/android/sec/org/bouncycastle/crypto/modes/CCMBlockCipher;->forEncryption:Z

    if-eqz v1, :cond_0

    .line 175
    iget v1, p0, Lcom/android/sec/org/bouncycastle/crypto/modes/CCMBlockCipher;->macSize:I

    add-int/2addr v1, v0

    .line 178
    :goto_0
    return v1

    :cond_0
    iget v1, p0, Lcom/android/sec/org/bouncycastle/crypto/modes/CCMBlockCipher;->macSize:I

    if-ge v0, v1, :cond_1

    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    iget v1, p0, Lcom/android/sec/org/bouncycastle/crypto/modes/CCMBlockCipher;->macSize:I

    sub-int v1, v0, v1

    goto :goto_0
.end method

.method public getUnderlyingCipher()Lcom/android/sec/org/bouncycastle/crypto/BlockCipher;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/crypto/modes/CCMBlockCipher;->cipher:Lcom/android/sec/org/bouncycastle/crypto/BlockCipher;

    return-object v0
.end method

.method public getUpdateOutputSize(I)I
    .locals 1
    .param p1, "len"    # I

    .prologue
    .line 166
    const/4 v0, 0x0

    return v0
.end method

.method public init(ZLcom/android/sec/org/bouncycastle/crypto/CipherParameters;)V
    .locals 3
    .param p1, "forEncryption"    # Z
    .param p2, "params"    # Lcom/android/sec/org/bouncycastle/crypto/CipherParameters;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 66
    iput-boolean p1, p0, Lcom/android/sec/org/bouncycastle/crypto/modes/CCMBlockCipher;->forEncryption:Z

    .line 68
    instance-of v1, p2, Lcom/android/sec/org/bouncycastle/crypto/params/AEADParameters;

    if-eqz v1, :cond_1

    move-object v0, p2

    .line 70
    check-cast v0, Lcom/android/sec/org/bouncycastle/crypto/params/AEADParameters;

    .line 72
    .local v0, "param":Lcom/android/sec/org/bouncycastle/crypto/params/AEADParameters;
    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/crypto/params/AEADParameters;->getNonce()[B

    move-result-object v1

    iput-object v1, p0, Lcom/android/sec/org/bouncycastle/crypto/modes/CCMBlockCipher;->nonce:[B

    .line 73
    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/crypto/params/AEADParameters;->getAssociatedText()[B

    move-result-object v1

    iput-object v1, p0, Lcom/android/sec/org/bouncycastle/crypto/modes/CCMBlockCipher;->initialAssociatedText:[B

    .line 74
    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/crypto/params/AEADParameters;->getMacSize()I

    move-result v1

    div-int/lit8 v1, v1, 0x8

    iput v1, p0, Lcom/android/sec/org/bouncycastle/crypto/modes/CCMBlockCipher;->macSize:I

    .line 75
    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/crypto/params/AEADParameters;->getKey()Lcom/android/sec/org/bouncycastle/crypto/params/KeyParameter;

    move-result-object v1

    iput-object v1, p0, Lcom/android/sec/org/bouncycastle/crypto/modes/CCMBlockCipher;->keyParam:Lcom/android/sec/org/bouncycastle/crypto/CipherParameters;

    .line 91
    .end local v0    # "param":Lcom/android/sec/org/bouncycastle/crypto/params/AEADParameters;
    :goto_0
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/crypto/modes/CCMBlockCipher;->nonce:[B

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/crypto/modes/CCMBlockCipher;->nonce:[B

    array-length v1, v1

    const/4 v2, 0x7

    if-lt v1, v2, :cond_0

    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/crypto/modes/CCMBlockCipher;->nonce:[B

    array-length v1, v1

    const/16 v2, 0xd

    if-le v1, v2, :cond_3

    .line 93
    :cond_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "nonce must have length from 7 to 13 octets"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 77
    :cond_1
    instance-of v1, p2, Lcom/android/sec/org/bouncycastle/crypto/params/ParametersWithIV;

    if-eqz v1, :cond_2

    move-object v0, p2

    .line 79
    check-cast v0, Lcom/android/sec/org/bouncycastle/crypto/params/ParametersWithIV;

    .line 81
    .local v0, "param":Lcom/android/sec/org/bouncycastle/crypto/params/ParametersWithIV;
    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/crypto/params/ParametersWithIV;->getIV()[B

    move-result-object v1

    iput-object v1, p0, Lcom/android/sec/org/bouncycastle/crypto/modes/CCMBlockCipher;->nonce:[B

    .line 82
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/sec/org/bouncycastle/crypto/modes/CCMBlockCipher;->initialAssociatedText:[B

    .line 83
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/crypto/modes/CCMBlockCipher;->macBlock:[B

    array-length v1, v1

    div-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/android/sec/org/bouncycastle/crypto/modes/CCMBlockCipher;->macSize:I

    .line 84
    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/crypto/params/ParametersWithIV;->getParameters()Lcom/android/sec/org/bouncycastle/crypto/CipherParameters;

    move-result-object v1

    iput-object v1, p0, Lcom/android/sec/org/bouncycastle/crypto/modes/CCMBlockCipher;->keyParam:Lcom/android/sec/org/bouncycastle/crypto/CipherParameters;

    goto :goto_0

    .line 88
    .end local v0    # "param":Lcom/android/sec/org/bouncycastle/crypto/params/ParametersWithIV;
    :cond_2
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "invalid parameters passed to CCM"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 95
    :cond_3
    return-void
.end method

.method public processAADByte(B)V
    .locals 1
    .param p1, "in"    # B

    .prologue
    .line 104
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/crypto/modes/CCMBlockCipher;->associatedText:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v0, p1}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 105
    return-void
.end method

.method public processAADBytes([BII)V
    .locals 1
    .param p1, "in"    # [B
    .param p2, "inOff"    # I
    .param p3, "len"    # I

    .prologue
    .line 110
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/crypto/modes/CCMBlockCipher;->associatedText:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    .line 111
    return-void
.end method

.method public processByte(B[BI)I
    .locals 1
    .param p1, "in"    # B
    .param p2, "out"    # [B
    .param p3, "outOff"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/crypto/DataLengthException;,
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 116
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/crypto/modes/CCMBlockCipher;->data:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v0, p1}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 118
    const/4 v0, 0x0

    return v0
.end method

.method public processBytes([BII[BI)I
    .locals 1
    .param p1, "in"    # [B
    .param p2, "inOff"    # I
    .param p3, "inLen"    # I
    .param p4, "out"    # [B
    .param p5, "outOff"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/crypto/DataLengthException;,
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 124
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/crypto/modes/CCMBlockCipher;->data:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    .line 126
    const/4 v0, 0x0

    return v0
.end method

.method public processPacket([BII)[B
    .locals 19
    .param p1, "in"    # [B
    .param p2, "inOff"    # I
    .param p3, "inLen"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;,
            Lcom/android/sec/org/bouncycastle/crypto/InvalidCipherTextException;
        }
    .end annotation

    .prologue
    .line 186
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/sec/org/bouncycastle/crypto/modes/CCMBlockCipher;->keyParam:Lcom/android/sec/org/bouncycastle/crypto/CipherParameters;

    if-nez v15, :cond_0

    .line 188
    new-instance v15, Ljava/lang/IllegalStateException;

    const-string v16, "CCM cipher unitialized."

    invoke-direct/range {v15 .. v16}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v15

    .line 191
    :cond_0
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/sec/org/bouncycastle/crypto/modes/CCMBlockCipher;->nonce:[B

    array-length v11, v15

    .line 192
    .local v11, "n":I
    rsub-int/lit8 v14, v11, 0xf

    .line 193
    .local v14, "q":I
    const/4 v15, 0x4

    if-ge v14, v15, :cond_1

    .line 195
    const/4 v15, 0x1

    mul-int/lit8 v16, v14, 0x8

    shl-int v10, v15, v16

    .line 196
    .local v10, "limitLen":I
    move/from16 v0, p3

    if-lt v0, v10, :cond_1

    .line 198
    new-instance v15, Ljava/lang/IllegalStateException;

    const-string v16, "CCM packet too large for choice of q."

    invoke-direct/range {v15 .. v16}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v15

    .line 202
    .end local v10    # "limitLen":I
    :cond_1
    move-object/from16 v0, p0

    iget v15, v0, Lcom/android/sec/org/bouncycastle/crypto/modes/CCMBlockCipher;->blockSize:I

    new-array v9, v15, [B

    .line 203
    .local v9, "iv":[B
    const/4 v15, 0x0

    add-int/lit8 v16, v14, -0x1

    and-int/lit8 v16, v16, 0x7

    move/from16 v0, v16

    int-to-byte v0, v0

    move/from16 v16, v0

    aput-byte v16, v9, v15

    .line 204
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/sec/org/bouncycastle/crypto/modes/CCMBlockCipher;->nonce:[B

    const/16 v16, 0x0

    const/16 v17, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/crypto/modes/CCMBlockCipher;->nonce:[B

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    array-length v0, v0

    move/from16 v18, v0

    move/from16 v0, v16

    move/from16 v1, v17

    move/from16 v2, v18

    invoke-static {v15, v0, v9, v1, v2}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 206
    new-instance v6, Lcom/android/sec/org/bouncycastle/crypto/modes/SICBlockCipher;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/sec/org/bouncycastle/crypto/modes/CCMBlockCipher;->cipher:Lcom/android/sec/org/bouncycastle/crypto/BlockCipher;

    invoke-direct {v6, v15}, Lcom/android/sec/org/bouncycastle/crypto/modes/SICBlockCipher;-><init>(Lcom/android/sec/org/bouncycastle/crypto/BlockCipher;)V

    .line 207
    .local v6, "ctrCipher":Lcom/android/sec/org/bouncycastle/crypto/BlockCipher;
    move-object/from16 v0, p0

    iget-boolean v15, v0, Lcom/android/sec/org/bouncycastle/crypto/modes/CCMBlockCipher;->forEncryption:Z

    new-instance v16, Lcom/android/sec/org/bouncycastle/crypto/params/ParametersWithIV;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/crypto/modes/CCMBlockCipher;->keyParam:Lcom/android/sec/org/bouncycastle/crypto/CipherParameters;

    move-object/from16 v17, v0

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-direct {v0, v1, v9}, Lcom/android/sec/org/bouncycastle/crypto/params/ParametersWithIV;-><init>(Lcom/android/sec/org/bouncycastle/crypto/CipherParameters;[B)V

    move-object/from16 v0, v16

    invoke-interface {v6, v15, v0}, Lcom/android/sec/org/bouncycastle/crypto/BlockCipher;->init(ZLcom/android/sec/org/bouncycastle/crypto/CipherParameters;)V

    .line 209
    move/from16 v8, p2

    .line 210
    .local v8, "index":I
    const/4 v12, 0x0

    .line 213
    .local v12, "outOff":I
    move-object/from16 v0, p0

    iget-boolean v15, v0, Lcom/android/sec/org/bouncycastle/crypto/modes/CCMBlockCipher;->forEncryption:Z

    if-eqz v15, :cond_4

    .line 215
    move-object/from16 v0, p0

    iget v15, v0, Lcom/android/sec/org/bouncycastle/crypto/modes/CCMBlockCipher;->macSize:I

    add-int v15, v15, p3

    new-array v13, v15, [B

    .line 217
    .local v13, "output":[B
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/sec/org/bouncycastle/crypto/modes/CCMBlockCipher;->macBlock:[B

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p2

    move/from16 v3, p3

    invoke-direct {v0, v1, v2, v3, v15}, Lcom/android/sec/org/bouncycastle/crypto/modes/CCMBlockCipher;->calculateMac([BII[B)I

    .line 219
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/sec/org/bouncycastle/crypto/modes/CCMBlockCipher;->macBlock:[B

    const/16 v16, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/crypto/modes/CCMBlockCipher;->macBlock:[B

    move-object/from16 v17, v0

    const/16 v18, 0x0

    move/from16 v0, v16

    move-object/from16 v1, v17

    move/from16 v2, v18

    invoke-interface {v6, v15, v0, v1, v2}, Lcom/android/sec/org/bouncycastle/crypto/BlockCipher;->processBlock([BI[BI)I

    .line 221
    :goto_0
    move-object/from16 v0, p0

    iget v15, v0, Lcom/android/sec/org/bouncycastle/crypto/modes/CCMBlockCipher;->blockSize:I

    sub-int v15, p3, v15

    if-ge v8, v15, :cond_2

    .line 223
    move-object/from16 v0, p1

    invoke-interface {v6, v0, v8, v13, v12}, Lcom/android/sec/org/bouncycastle/crypto/BlockCipher;->processBlock([BI[BI)I

    .line 224
    move-object/from16 v0, p0

    iget v15, v0, Lcom/android/sec/org/bouncycastle/crypto/modes/CCMBlockCipher;->blockSize:I

    add-int/2addr v12, v15

    .line 225
    move-object/from16 v0, p0

    iget v15, v0, Lcom/android/sec/org/bouncycastle/crypto/modes/CCMBlockCipher;->blockSize:I

    add-int/2addr v8, v15

    goto :goto_0

    .line 228
    :cond_2
    move-object/from16 v0, p0

    iget v15, v0, Lcom/android/sec/org/bouncycastle/crypto/modes/CCMBlockCipher;->blockSize:I

    new-array v4, v15, [B

    .line 230
    .local v4, "block":[B
    const/4 v15, 0x0

    sub-int v16, p3, v8

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-static {v0, v8, v4, v15, v1}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 232
    const/4 v15, 0x0

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-interface {v6, v4, v15, v4, v0}, Lcom/android/sec/org/bouncycastle/crypto/BlockCipher;->processBlock([BI[BI)I

    .line 234
    const/4 v15, 0x0

    sub-int v16, p3, v8

    move/from16 v0, v16

    invoke-static {v4, v15, v13, v12, v0}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 236
    sub-int v15, p3, v8

    add-int/2addr v12, v15

    .line 238
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/sec/org/bouncycastle/crypto/modes/CCMBlockCipher;->macBlock:[B

    const/16 v16, 0x0

    array-length v0, v13

    move/from16 v17, v0

    sub-int v17, v17, v12

    move/from16 v0, v16

    move/from16 v1, v17

    invoke-static {v15, v0, v13, v12, v1}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 278
    :cond_3
    return-object v13

    .line 242
    .end local v4    # "block":[B
    .end local v13    # "output":[B
    :cond_4
    move-object/from16 v0, p0

    iget v15, v0, Lcom/android/sec/org/bouncycastle/crypto/modes/CCMBlockCipher;->macSize:I

    sub-int v15, p3, v15

    new-array v13, v15, [B

    .line 244
    .restart local v13    # "output":[B
    add-int v15, p2, p3

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/sec/org/bouncycastle/crypto/modes/CCMBlockCipher;->macSize:I

    move/from16 v16, v0

    sub-int v15, v15, v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/crypto/modes/CCMBlockCipher;->macBlock:[B

    move-object/from16 v16, v0

    const/16 v17, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/sec/org/bouncycastle/crypto/modes/CCMBlockCipher;->macSize:I

    move/from16 v18, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    move/from16 v2, v17

    move/from16 v3, v18

    invoke-static {v0, v15, v1, v2, v3}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 246
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/sec/org/bouncycastle/crypto/modes/CCMBlockCipher;->macBlock:[B

    const/16 v16, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/crypto/modes/CCMBlockCipher;->macBlock:[B

    move-object/from16 v17, v0

    const/16 v18, 0x0

    move/from16 v0, v16

    move-object/from16 v1, v17

    move/from16 v2, v18

    invoke-interface {v6, v15, v0, v1, v2}, Lcom/android/sec/org/bouncycastle/crypto/BlockCipher;->processBlock([BI[BI)I

    .line 248
    move-object/from16 v0, p0

    iget v7, v0, Lcom/android/sec/org/bouncycastle/crypto/modes/CCMBlockCipher;->macSize:I

    .local v7, "i":I
    :goto_1
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/sec/org/bouncycastle/crypto/modes/CCMBlockCipher;->macBlock:[B

    array-length v15, v15

    if-eq v7, v15, :cond_5

    .line 250
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/sec/org/bouncycastle/crypto/modes/CCMBlockCipher;->macBlock:[B

    const/16 v16, 0x0

    aput-byte v16, v15, v7

    .line 248
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 253
    :cond_5
    :goto_2
    array-length v15, v13

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/sec/org/bouncycastle/crypto/modes/CCMBlockCipher;->blockSize:I

    move/from16 v16, v0

    sub-int v15, v15, v16

    if-ge v12, v15, :cond_6

    .line 255
    move-object/from16 v0, p1

    invoke-interface {v6, v0, v8, v13, v12}, Lcom/android/sec/org/bouncycastle/crypto/BlockCipher;->processBlock([BI[BI)I

    .line 256
    move-object/from16 v0, p0

    iget v15, v0, Lcom/android/sec/org/bouncycastle/crypto/modes/CCMBlockCipher;->blockSize:I

    add-int/2addr v12, v15

    .line 257
    move-object/from16 v0, p0

    iget v15, v0, Lcom/android/sec/org/bouncycastle/crypto/modes/CCMBlockCipher;->blockSize:I

    add-int/2addr v8, v15

    goto :goto_2

    .line 260
    :cond_6
    move-object/from16 v0, p0

    iget v15, v0, Lcom/android/sec/org/bouncycastle/crypto/modes/CCMBlockCipher;->blockSize:I

    new-array v4, v15, [B

    .line 262
    .restart local v4    # "block":[B
    const/4 v15, 0x0

    array-length v0, v13

    move/from16 v16, v0

    sub-int v16, v16, v12

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-static {v0, v8, v4, v15, v1}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 264
    const/4 v15, 0x0

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-interface {v6, v4, v15, v4, v0}, Lcom/android/sec/org/bouncycastle/crypto/BlockCipher;->processBlock([BI[BI)I

    .line 266
    const/4 v15, 0x0

    array-length v0, v13

    move/from16 v16, v0

    sub-int v16, v16, v12

    move/from16 v0, v16

    invoke-static {v4, v15, v13, v12, v0}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 268
    move-object/from16 v0, p0

    iget v15, v0, Lcom/android/sec/org/bouncycastle/crypto/modes/CCMBlockCipher;->blockSize:I

    new-array v5, v15, [B

    .line 270
    .local v5, "calculatedMacBlock":[B
    const/4 v15, 0x0

    array-length v0, v13

    move/from16 v16, v0

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v13, v15, v1, v5}, Lcom/android/sec/org/bouncycastle/crypto/modes/CCMBlockCipher;->calculateMac([BII[B)I

    .line 272
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/sec/org/bouncycastle/crypto/modes/CCMBlockCipher;->macBlock:[B

    invoke-static {v15, v5}, Lcom/android/sec/org/bouncycastle/util/Arrays;->constantTimeAreEqual([B[B)Z

    move-result v15

    if-nez v15, :cond_3

    .line 274
    new-instance v15, Lcom/android/sec/org/bouncycastle/crypto/InvalidCipherTextException;

    const-string v16, "mac check in CCM failed"

    invoke-direct/range {v15 .. v16}, Lcom/android/sec/org/bouncycastle/crypto/InvalidCipherTextException;-><init>(Ljava/lang/String;)V

    throw v15
.end method

.method public reset()V
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/crypto/modes/CCMBlockCipher;->cipher:Lcom/android/sec/org/bouncycastle/crypto/BlockCipher;

    invoke-interface {v0}, Lcom/android/sec/org/bouncycastle/crypto/BlockCipher;->reset()V

    .line 145
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/crypto/modes/CCMBlockCipher;->associatedText:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->reset()V

    .line 146
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/crypto/modes/CCMBlockCipher;->data:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->reset()V

    .line 147
    return-void
.end method
