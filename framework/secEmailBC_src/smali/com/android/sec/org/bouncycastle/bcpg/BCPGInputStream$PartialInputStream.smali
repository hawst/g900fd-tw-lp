.class Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream$PartialInputStream;
.super Ljava/io/InputStream;
.source "BCPGInputStream.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "PartialInputStream"
.end annotation


# instance fields
.field private dataLength:I

.field private in:Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;

.field private partial:Z


# direct methods
.method constructor <init>(Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;ZI)V
    .locals 0
    .param p1, "in"    # Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;
    .param p2, "partial"    # Z
    .param p3, "dataLength"    # I

    .prologue
    .line 285
    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    .line 286
    iput-object p1, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream$PartialInputStream;->in:Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;

    .line 287
    iput-boolean p2, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream$PartialInputStream;->partial:Z

    .line 288
    iput p3, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream$PartialInputStream;->dataLength:I

    .line 289
    return-void
.end method

.method private loadDataLength()I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 313
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream$PartialInputStream;->in:Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;

    invoke-virtual {v1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->read()I

    move-result v0

    .line 315
    .local v0, "l":I
    if-gez v0, :cond_0

    .line 317
    const/4 v1, -0x1

    .line 339
    :goto_0
    return v1

    .line 320
    :cond_0
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream$PartialInputStream;->partial:Z

    .line 321
    const/16 v1, 0xc0

    if-ge v0, v1, :cond_1

    .line 323
    iput v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream$PartialInputStream;->dataLength:I

    .line 339
    :goto_1
    iget v1, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream$PartialInputStream;->dataLength:I

    goto :goto_0

    .line 325
    :cond_1
    const/16 v1, 0xdf

    if-gt v0, v1, :cond_2

    .line 327
    add-int/lit16 v1, v0, -0xc0

    shl-int/lit8 v1, v1, 0x8

    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream$PartialInputStream;->in:Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;

    invoke-virtual {v2}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->read()I

    move-result v2

    add-int/2addr v1, v2

    add-int/lit16 v1, v1, 0xc0

    iput v1, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream$PartialInputStream;->dataLength:I

    goto :goto_1

    .line 329
    :cond_2
    const/16 v1, 0xff

    if-ne v0, v1, :cond_3

    .line 331
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream$PartialInputStream;->in:Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;

    invoke-virtual {v1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->read()I

    move-result v1

    shl-int/lit8 v1, v1, 0x18

    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream$PartialInputStream;->in:Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;

    invoke-virtual {v2}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->read()I

    move-result v2

    shl-int/lit8 v2, v2, 0x10

    or-int/2addr v1, v2

    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream$PartialInputStream;->in:Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;

    invoke-virtual {v2}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->read()I

    move-result v2

    shl-int/lit8 v2, v2, 0x8

    or-int/2addr v1, v2

    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream$PartialInputStream;->in:Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;

    invoke-virtual {v2}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->read()I

    move-result v2

    or-int/2addr v1, v2

    iput v1, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream$PartialInputStream;->dataLength:I

    goto :goto_1

    .line 335
    :cond_3
    iput-boolean v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream$PartialInputStream;->partial:Z

    .line 336
    and-int/lit8 v1, v0, 0x1f

    shl-int v1, v2, v1

    iput v1, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream$PartialInputStream;->dataLength:I

    goto :goto_1
.end method


# virtual methods
.method public available()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 294
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream$PartialInputStream;->in:Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;

    invoke-virtual {v1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->available()I

    move-result v0

    .line 296
    .local v0, "avail":I
    iget v1, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream$PartialInputStream;->dataLength:I

    if-le v0, v1, :cond_0

    iget v1, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream$PartialInputStream;->dataLength:I

    if-gez v1, :cond_1

    .line 306
    .end local v0    # "avail":I
    :cond_0
    :goto_0
    return v0

    .line 302
    .restart local v0    # "avail":I
    :cond_1
    iget-boolean v1, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream$PartialInputStream;->partial:Z

    if-eqz v1, :cond_2

    iget v1, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream$PartialInputStream;->dataLength:I

    if-nez v1, :cond_2

    .line 304
    const/4 v0, 0x1

    goto :goto_0

    .line 306
    :cond_2
    iget v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream$PartialInputStream;->dataLength:I

    goto :goto_0
.end method

.method public read()I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 369
    :cond_0
    iget v1, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream$PartialInputStream;->dataLength:I

    if-eqz v1, :cond_2

    .line 371
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream$PartialInputStream;->in:Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;

    invoke-virtual {v1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->read()I

    move-result v0

    .line 372
    .local v0, "ch":I
    if-gez v0, :cond_1

    .line 374
    new-instance v1, Ljava/io/EOFException;

    const-string v2, "premature end of stream in PartialInputStream"

    invoke-direct {v1, v2}, Ljava/io/EOFException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 376
    :cond_1
    iget v1, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream$PartialInputStream;->dataLength:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream$PartialInputStream;->dataLength:I

    .line 382
    .end local v0    # "ch":I
    :goto_0
    return v0

    .line 380
    :cond_2
    iget-boolean v1, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream$PartialInputStream;->partial:Z

    if-eqz v1, :cond_3

    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream$PartialInputStream;->loadDataLength()I

    move-result v1

    if-gez v1, :cond_0

    .line 382
    :cond_3
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public read([BII)I
    .locals 3
    .param p1, "buf"    # [B
    .param p2, "offset"    # I
    .param p3, "len"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 347
    :cond_0
    iget v1, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream$PartialInputStream;->dataLength:I

    if-eqz v1, :cond_4

    .line 349
    iget v1, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream$PartialInputStream;->dataLength:I

    if-gt v1, p3, :cond_1

    iget v1, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream$PartialInputStream;->dataLength:I

    if-gez v1, :cond_2

    :cond_1
    move v0, p3

    .line 350
    .local v0, "readLen":I
    :goto_0
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream$PartialInputStream;->in:Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;

    invoke-virtual {v1, p1, p2, v0}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->read([BII)I

    move-result v0

    .line 351
    if-gez v0, :cond_3

    .line 353
    new-instance v1, Ljava/io/EOFException;

    const-string v2, "premature end of stream in PartialInputStream"

    invoke-direct {v1, v2}, Ljava/io/EOFException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 349
    .end local v0    # "readLen":I
    :cond_2
    iget v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream$PartialInputStream;->dataLength:I

    goto :goto_0

    .line 355
    .restart local v0    # "readLen":I
    :cond_3
    iget v1, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream$PartialInputStream;->dataLength:I

    sub-int/2addr v1, v0

    iput v1, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream$PartialInputStream;->dataLength:I

    .line 361
    .end local v0    # "readLen":I
    :goto_1
    return v0

    .line 359
    :cond_4
    iget-boolean v1, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream$PartialInputStream;->partial:Z

    if-eqz v1, :cond_5

    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream$PartialInputStream;->loadDataLength()I

    move-result v1

    if-gez v1, :cond_0

    .line 361
    :cond_5
    const/4 v0, -0x1

    goto :goto_1
.end method
