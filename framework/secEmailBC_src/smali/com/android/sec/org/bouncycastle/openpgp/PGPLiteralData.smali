.class public Lcom/android/sec/org/bouncycastle/openpgp/PGPLiteralData;
.super Ljava/lang/Object;
.source "PGPLiteralData.java"


# static fields
.field public static final BINARY:C = 'b'

.field public static final CONSOLE:Ljava/lang/String; = "_CONSOLE"

.field public static final NOW:Ljava/util/Date;

.field public static final TEXT:C = 't'

.field public static final UTF8:C = 'u'


# instance fields
.field data:Lcom/android/sec/org/bouncycastle/bcpg/LiteralDataPacket;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 28
    new-instance v0, Ljava/util/Date;

    const-wide/16 v2, 0x0

    invoke-direct {v0, v2, v3}, Ljava/util/Date;-><init>(J)V

    sput-object v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPLiteralData;->NOW:Ljava/util/Date;

    return-void
.end method

.method public constructor <init>(Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;)V
    .locals 1
    .param p1, "pIn"    # Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->readPacket()Lcom/android/sec/org/bouncycastle/bcpg/Packet;

    move-result-object v0

    check-cast v0, Lcom/android/sec/org/bouncycastle/bcpg/LiteralDataPacket;

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPLiteralData;->data:Lcom/android/sec/org/bouncycastle/bcpg/LiteralDataPacket;

    .line 37
    return-void
.end method


# virtual methods
.method public getDataStream()Ljava/io/InputStream;
    .locals 1

    .prologue
    .line 94
    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPLiteralData;->getInputStream()Ljava/io/InputStream;

    move-result-object v0

    return-object v0
.end method

.method public getFileName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPLiteralData;->data:Lcom/android/sec/org/bouncycastle/bcpg/LiteralDataPacket;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/bcpg/LiteralDataPacket;->getFileName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getFormat()I
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPLiteralData;->data:Lcom/android/sec/org/bouncycastle/bcpg/LiteralDataPacket;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/bcpg/LiteralDataPacket;->getFormat()I

    move-result v0

    return v0
.end method

.method public getInputStream()Ljava/io/InputStream;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPLiteralData;->data:Lcom/android/sec/org/bouncycastle/bcpg/LiteralDataPacket;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/bcpg/LiteralDataPacket;->getInputStream()Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;

    move-result-object v0

    return-object v0
.end method

.method public getModificationTime()Ljava/util/Date;
    .locals 4

    .prologue
    .line 74
    new-instance v0, Ljava/util/Date;

    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPLiteralData;->data:Lcom/android/sec/org/bouncycastle/bcpg/LiteralDataPacket;

    invoke-virtual {v1}, Lcom/android/sec/org/bouncycastle/bcpg/LiteralDataPacket;->getModificationTime()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Ljava/util/Date;-><init>(J)V

    return-object v0
.end method

.method public getRawFileName()[B
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPLiteralData;->data:Lcom/android/sec/org/bouncycastle/bcpg/LiteralDataPacket;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/bcpg/LiteralDataPacket;->getRawFileName()[B

    move-result-object v0

    return-object v0
.end method
