.class public Lcom/android/sec/org/bouncycastle/mail/smime/SMIMEException;
.super Ljava/lang/Exception;
.source "SMIMEException.java"


# instance fields
.field e:Ljava/lang/Exception;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 8
    invoke-direct {p0, p1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    .line 9
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "e"    # Ljava/lang/Exception;

    .prologue
    .line 12
    invoke-direct {p0, p1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    .line 14
    iput-object p2, p0, Lcom/android/sec/org/bouncycastle/mail/smime/SMIMEException;->e:Ljava/lang/Exception;

    .line 15
    return-void
.end method


# virtual methods
.method public getCause()Ljava/lang/Throwable;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/mail/smime/SMIMEException;->e:Ljava/lang/Exception;

    return-object v0
.end method

.method public getUnderlyingException()Ljava/lang/Exception;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/mail/smime/SMIMEException;->e:Ljava/lang/Exception;

    return-object v0
.end method
