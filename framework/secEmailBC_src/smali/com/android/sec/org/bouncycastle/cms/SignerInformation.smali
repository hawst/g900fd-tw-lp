.class public Lcom/android/sec/org/bouncycastle/cms/SignerInformation;
.super Ljava/lang/Object;
.source "SignerInformation.java"


# instance fields
.field private content:Lcom/android/sec/org/bouncycastle/cms/CMSProcessable;

.field private contentType:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

.field private digestAlgorithm:Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;

.field private encryptionAlgorithm:Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;

.field private info:Lcom/android/sec/org/bouncycastle/asn1/cms/SignerInfo;

.field private isCounterSignature:Z

.field private resultDigest:[B

.field private sid:Lcom/android/sec/org/bouncycastle/cms/SignerId;

.field private signature:[B

.field private final signedAttributeSet:Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;

.field private signedAttributeValues:Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;

.field private final unsignedAttributeSet:Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;

.field private unsignedAttributeValues:Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;


# direct methods
.method constructor <init>(Lcom/android/sec/org/bouncycastle/asn1/cms/SignerInfo;Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;Lcom/android/sec/org/bouncycastle/cms/CMSProcessable;[B)V
    .locals 6
    .param p1, "info"    # Lcom/android/sec/org/bouncycastle/asn1/cms/SignerInfo;
    .param p2, "contentType"    # Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;
    .param p3, "content"    # Lcom/android/sec/org/bouncycastle/cms/CMSProcessable;
    .param p4, "resultDigest"    # [B

    .prologue
    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 73
    iput-object p1, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInformation;->info:Lcom/android/sec/org/bouncycastle/asn1/cms/SignerInfo;

    .line 74
    iput-object p2, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInformation;->contentType:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 75
    if-nez p2, :cond_0

    const/4 v3, 0x1

    :goto_0
    iput-boolean v3, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInformation;->isCounterSignature:Z

    .line 77
    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/asn1/cms/SignerInfo;->getSID()Lcom/android/sec/org/bouncycastle/asn1/cms/SignerIdentifier;

    move-result-object v2

    .line 79
    .local v2, "s":Lcom/android/sec/org/bouncycastle/asn1/cms/SignerIdentifier;
    invoke-virtual {v2}, Lcom/android/sec/org/bouncycastle/asn1/cms/SignerIdentifier;->isTagged()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 81
    invoke-virtual {v2}, Lcom/android/sec/org/bouncycastle/asn1/cms/SignerIdentifier;->getId()Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;

    move-result-object v3

    invoke-static {v3}, Lcom/android/sec/org/bouncycastle/asn1/ASN1OctetString;->getInstance(Ljava/lang/Object;)Lcom/android/sec/org/bouncycastle/asn1/ASN1OctetString;

    move-result-object v1

    .line 83
    .local v1, "octs":Lcom/android/sec/org/bouncycastle/asn1/ASN1OctetString;
    new-instance v3, Lcom/android/sec/org/bouncycastle/cms/SignerId;

    invoke-virtual {v1}, Lcom/android/sec/org/bouncycastle/asn1/ASN1OctetString;->getOctets()[B

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/android/sec/org/bouncycastle/cms/SignerId;-><init>([B)V

    iput-object v3, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInformation;->sid:Lcom/android/sec/org/bouncycastle/cms/SignerId;

    .line 92
    .end local v1    # "octs":Lcom/android/sec/org/bouncycastle/asn1/ASN1OctetString;
    :goto_1
    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/asn1/cms/SignerInfo;->getDigestAlgorithm()Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;

    move-result-object v3

    iput-object v3, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInformation;->digestAlgorithm:Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;

    .line 93
    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/asn1/cms/SignerInfo;->getAuthenticatedAttributes()Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;

    move-result-object v3

    iput-object v3, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInformation;->signedAttributeSet:Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;

    .line 94
    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/asn1/cms/SignerInfo;->getUnauthenticatedAttributes()Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;

    move-result-object v3

    iput-object v3, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInformation;->unsignedAttributeSet:Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;

    .line 95
    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/asn1/cms/SignerInfo;->getDigestEncryptionAlgorithm()Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;

    move-result-object v3

    iput-object v3, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInformation;->encryptionAlgorithm:Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;

    .line 96
    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/asn1/cms/SignerInfo;->getEncryptedDigest()Lcom/android/sec/org/bouncycastle/asn1/ASN1OctetString;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/sec/org/bouncycastle/asn1/ASN1OctetString;->getOctets()[B

    move-result-object v3

    iput-object v3, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInformation;->signature:[B

    .line 98
    iput-object p3, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInformation;->content:Lcom/android/sec/org/bouncycastle/cms/CMSProcessable;

    .line 99
    iput-object p4, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInformation;->resultDigest:[B

    .line 100
    return-void

    .line 75
    .end local v2    # "s":Lcom/android/sec/org/bouncycastle/asn1/cms/SignerIdentifier;
    :cond_0
    const/4 v3, 0x0

    goto :goto_0

    .line 87
    .restart local v2    # "s":Lcom/android/sec/org/bouncycastle/asn1/cms/SignerIdentifier;
    :cond_1
    invoke-virtual {v2}, Lcom/android/sec/org/bouncycastle/asn1/cms/SignerIdentifier;->getId()Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;

    move-result-object v3

    invoke-static {v3}, Lcom/android/sec/org/bouncycastle/asn1/cms/IssuerAndSerialNumber;->getInstance(Ljava/lang/Object;)Lcom/android/sec/org/bouncycastle/asn1/cms/IssuerAndSerialNumber;

    move-result-object v0

    .line 89
    .local v0, "iAnds":Lcom/android/sec/org/bouncycastle/asn1/cms/IssuerAndSerialNumber;
    new-instance v3, Lcom/android/sec/org/bouncycastle/cms/SignerId;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/cms/IssuerAndSerialNumber;->getName()Lcom/android/sec/org/bouncycastle/asn1/x500/X500Name;

    move-result-object v4

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/cms/IssuerAndSerialNumber;->getSerialNumber()Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;->getValue()Ljava/math/BigInteger;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/android/sec/org/bouncycastle/cms/SignerId;-><init>(Lcom/android/sec/org/bouncycastle/asn1/x500/X500Name;Ljava/math/BigInteger;)V

    iput-object v3, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInformation;->sid:Lcom/android/sec/org/bouncycastle/cms/SignerId;

    goto :goto_1
.end method

.method public static addCounterSigners(Lcom/android/sec/org/bouncycastle/cms/SignerInformation;Lcom/android/sec/org/bouncycastle/cms/SignerInformationStore;)Lcom/android/sec/org/bouncycastle/cms/SignerInformation;
    .locals 13
    .param p0, "signerInformation"    # Lcom/android/sec/org/bouncycastle/cms/SignerInformation;
    .param p1, "counterSigners"    # Lcom/android/sec/org/bouncycastle/cms/SignerInformationStore;

    .prologue
    .line 779
    iget-object v8, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInformation;->info:Lcom/android/sec/org/bouncycastle/asn1/cms/SignerInfo;

    .line 780
    .local v8, "sInfo":Lcom/android/sec/org/bouncycastle/asn1/cms/SignerInfo;
    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/cms/SignerInformation;->getUnsignedAttributes()Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;

    move-result-object v10

    .line 783
    .local v10, "unsignedAttr":Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;
    if-eqz v10, :cond_0

    .line 785
    invoke-virtual {v10}, Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;->toASN1EncodableVector()Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;

    move-result-object v11

    .line 792
    .local v11, "v":Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;
    :goto_0
    new-instance v9, Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;

    invoke-direct {v9}, Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;-><init>()V

    .line 794
    .local v9, "sigs":Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;
    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/cms/SignerInformationStore;->getSigners()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .local v7, "it":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 796
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/sec/org/bouncycastle/cms/SignerInformation;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/cms/SignerInformation;->toASN1Structure()Lcom/android/sec/org/bouncycastle/asn1/cms/SignerInfo;

    move-result-object v0

    invoke-virtual {v9, v0}, Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;->add(Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;)V

    goto :goto_1

    .line 789
    .end local v7    # "it":Ljava/util/Iterator;
    .end local v9    # "sigs":Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;
    .end local v11    # "v":Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;
    :cond_0
    new-instance v11, Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;

    invoke-direct {v11}, Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;-><init>()V

    .restart local v11    # "v":Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;
    goto :goto_0

    .line 799
    .restart local v7    # "it":Ljava/util/Iterator;
    .restart local v9    # "sigs":Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;
    :cond_1
    new-instance v0, Lcom/android/sec/org/bouncycastle/asn1/cms/Attribute;

    sget-object v1, Lcom/android/sec/org/bouncycastle/asn1/cms/CMSAttributes;->counterSignature:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    new-instance v2, Lcom/android/sec/org/bouncycastle/asn1/DERSet;

    invoke-direct {v2, v9}, Lcom/android/sec/org/bouncycastle/asn1/DERSet;-><init>(Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;)V

    invoke-direct {v0, v1, v2}, Lcom/android/sec/org/bouncycastle/asn1/cms/Attribute;-><init>(Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;)V

    invoke-virtual {v11, v0}, Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;->add(Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;)V

    .line 801
    new-instance v12, Lcom/android/sec/org/bouncycastle/cms/SignerInformation;

    new-instance v0, Lcom/android/sec/org/bouncycastle/asn1/cms/SignerInfo;

    invoke-virtual {v8}, Lcom/android/sec/org/bouncycastle/asn1/cms/SignerInfo;->getSID()Lcom/android/sec/org/bouncycastle/asn1/cms/SignerIdentifier;

    move-result-object v1

    invoke-virtual {v8}, Lcom/android/sec/org/bouncycastle/asn1/cms/SignerInfo;->getDigestAlgorithm()Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;

    move-result-object v2

    invoke-virtual {v8}, Lcom/android/sec/org/bouncycastle/asn1/cms/SignerInfo;->getAuthenticatedAttributes()Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;

    move-result-object v3

    invoke-virtual {v8}, Lcom/android/sec/org/bouncycastle/asn1/cms/SignerInfo;->getDigestEncryptionAlgorithm()Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;

    move-result-object v4

    invoke-virtual {v8}, Lcom/android/sec/org/bouncycastle/asn1/cms/SignerInfo;->getEncryptedDigest()Lcom/android/sec/org/bouncycastle/asn1/ASN1OctetString;

    move-result-object v5

    new-instance v6, Lcom/android/sec/org/bouncycastle/asn1/DERSet;

    invoke-direct {v6, v11}, Lcom/android/sec/org/bouncycastle/asn1/DERSet;-><init>(Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;)V

    invoke-direct/range {v0 .. v6}, Lcom/android/sec/org/bouncycastle/asn1/cms/SignerInfo;-><init>(Lcom/android/sec/org/bouncycastle/asn1/cms/SignerIdentifier;Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;Lcom/android/sec/org/bouncycastle/asn1/ASN1OctetString;Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;)V

    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInformation;->contentType:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInformation;->content:Lcom/android/sec/org/bouncycastle/cms/CMSProcessable;

    const/4 v3, 0x0

    invoke-direct {v12, v0, v1, v2, v3}, Lcom/android/sec/org/bouncycastle/cms/SignerInformation;-><init>(Lcom/android/sec/org/bouncycastle/asn1/cms/SignerInfo;Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;Lcom/android/sec/org/bouncycastle/cms/CMSProcessable;[B)V

    return-object v12
.end method

.method private doVerify(Lcom/android/sec/org/bouncycastle/cms/SignerInformationVerifier;)Z
    .locals 24
    .param p1, "verifier"    # Lcom/android/sec/org/bouncycastle/cms/SignerInformationVerifier;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/cms/CMSException;
        }
    .end annotation

    .prologue
    .line 361
    sget-object v21, Lcom/android/sec/org/bouncycastle/cms/CMSSignedHelper;->INSTANCE:Lcom/android/sec/org/bouncycastle/cms/CMSSignedHelper;

    invoke-virtual/range {p0 .. p0}, Lcom/android/sec/org/bouncycastle/cms/SignerInformation;->getEncryptionAlgOID()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Lcom/android/sec/org/bouncycastle/cms/CMSSignedHelper;->getEncryptionAlgName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 366
    .local v11, "encName":Ljava/lang/String;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/cms/SignerInformation;->encryptionAlgorithm:Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/cms/SignerInformation;->info:Lcom/android/sec/org/bouncycastle/asn1/cms/SignerInfo;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/android/sec/org/bouncycastle/asn1/cms/SignerInfo;->getDigestAlgorithm()Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;

    move-result-object v22

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Lcom/android/sec/org/bouncycastle/cms/SignerInformationVerifier;->getContentVerifier(Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;)Lcom/android/sec/org/bouncycastle/operator/ContentVerifier;
    :try_end_0
    .catch Lcom/android/sec/org/bouncycastle/operator/OperatorCreationException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v5

    .line 375
    .local v5, "contentVerifier":Lcom/android/sec/org/bouncycastle/operator/ContentVerifier;
    :try_start_1
    invoke-interface {v5}, Lcom/android/sec/org/bouncycastle/operator/ContentVerifier;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v14

    .line 377
    .local v14, "sigOut":Ljava/io/OutputStream;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/cms/SignerInformation;->resultDigest:[B

    move-object/from16 v21, v0

    if-nez v21, :cond_5

    .line 379
    invoke-virtual/range {p0 .. p0}, Lcom/android/sec/org/bouncycastle/cms/SignerInformation;->getDigestAlgorithmID()Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;

    move-result-object v21

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/android/sec/org/bouncycastle/cms/SignerInformationVerifier;->getDigestCalculator(Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;)Lcom/android/sec/org/bouncycastle/operator/DigestCalculator;

    move-result-object v4

    .line 380
    .local v4, "calc":Lcom/android/sec/org/bouncycastle/operator/DigestCalculator;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/cms/SignerInformation;->content:Lcom/android/sec/org/bouncycastle/cms/CMSProcessable;

    move-object/from16 v21, v0

    if-eqz v21, :cond_3

    .line 382
    invoke-interface {v4}, Lcom/android/sec/org/bouncycastle/operator/DigestCalculator;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v9

    .line 384
    .local v9, "digOut":Ljava/io/OutputStream;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/cms/SignerInformation;->signedAttributeSet:Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;

    move-object/from16 v21, v0

    if-nez v21, :cond_2

    .line 386
    instance-of v0, v5, Lcom/android/sec/org/bouncycastle/operator/RawContentVerifier;

    move/from16 v21, v0

    if-eqz v21, :cond_1

    .line 388
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/cms/SignerInformation;->content:Lcom/android/sec/org/bouncycastle/cms/CMSProcessable;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-interface {v0, v9}, Lcom/android/sec/org/bouncycastle/cms/CMSProcessable;->write(Ljava/io/OutputStream;)V

    .line 405
    :goto_0
    invoke-virtual {v9}, Ljava/io/OutputStream;->close()V

    .line 417
    .end local v9    # "digOut":Ljava/io/OutputStream;
    :goto_1
    invoke-interface {v4}, Lcom/android/sec/org/bouncycastle/operator/DigestCalculator;->getDigest()[B

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/sec/org/bouncycastle/cms/SignerInformation;->resultDigest:[B

    .line 434
    .end local v4    # "calc":Lcom/android/sec/org/bouncycastle/operator/DigestCalculator;
    :cond_0
    :goto_2
    invoke-virtual {v14}, Ljava/io/OutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lcom/android/sec/org/bouncycastle/operator/OperatorCreationException; {:try_start_1 .. :try_end_1} :catch_2

    .line 447
    sget-object v21, Lcom/android/sec/org/bouncycastle/asn1/cms/CMSAttributes;->contentType:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    const-string v22, "content-type"

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    invoke-direct {v0, v1, v2}, Lcom/android/sec/org/bouncycastle/cms/SignerInformation;->getSingleValuedSignedAttribute(Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;Ljava/lang/String;)Lcom/android/sec/org/bouncycastle/asn1/ASN1Primitive;

    move-result-object v19

    .line 449
    .local v19, "validContentType":Lcom/android/sec/org/bouncycastle/asn1/ASN1Primitive;
    if-nez v19, :cond_7

    .line 451
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/sec/org/bouncycastle/cms/SignerInformation;->isCounterSignature:Z

    move/from16 v21, v0

    if-nez v21, :cond_a

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/cms/SignerInformation;->signedAttributeSet:Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;

    move-object/from16 v21, v0

    if-eqz v21, :cond_a

    .line 453
    new-instance v21, Lcom/android/sec/org/bouncycastle/cms/CMSException;

    const-string v22, "The content-type attribute type MUST be present whenever signed attributes are present in signed-data"

    invoke-direct/range {v21 .. v22}, Lcom/android/sec/org/bouncycastle/cms/CMSException;-><init>(Ljava/lang/String;)V

    throw v21

    .line 368
    .end local v5    # "contentVerifier":Lcom/android/sec/org/bouncycastle/operator/ContentVerifier;
    .end local v14    # "sigOut":Ljava/io/OutputStream;
    .end local v19    # "validContentType":Lcom/android/sec/org/bouncycastle/asn1/ASN1Primitive;
    :catch_0
    move-exception v10

    .line 370
    .local v10, "e":Lcom/android/sec/org/bouncycastle/operator/OperatorCreationException;
    new-instance v21, Lcom/android/sec/org/bouncycastle/cms/CMSException;

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "can\'t create content verifier: "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual {v10}, Lcom/android/sec/org/bouncycastle/operator/OperatorCreationException;->getMessage()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-direct {v0, v1, v10}, Lcom/android/sec/org/bouncycastle/cms/CMSException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v21

    .line 392
    .end local v10    # "e":Lcom/android/sec/org/bouncycastle/operator/OperatorCreationException;
    .restart local v4    # "calc":Lcom/android/sec/org/bouncycastle/operator/DigestCalculator;
    .restart local v5    # "contentVerifier":Lcom/android/sec/org/bouncycastle/operator/ContentVerifier;
    .restart local v9    # "digOut":Ljava/io/OutputStream;
    .restart local v14    # "sigOut":Ljava/io/OutputStream;
    :cond_1
    :try_start_2
    new-instance v3, Lcom/android/sec/org/bouncycastle/util/io/TeeOutputStream;

    invoke-direct {v3, v9, v14}, Lcom/android/sec/org/bouncycastle/util/io/TeeOutputStream;-><init>(Ljava/io/OutputStream;Ljava/io/OutputStream;)V

    .line 394
    .local v3, "cOut":Ljava/io/OutputStream;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/cms/SignerInformation;->content:Lcom/android/sec/org/bouncycastle/cms/CMSProcessable;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-interface {v0, v3}, Lcom/android/sec/org/bouncycastle/cms/CMSProcessable;->write(Ljava/io/OutputStream;)V

    .line 396
    invoke-virtual {v3}, Ljava/io/OutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lcom/android/sec/org/bouncycastle/operator/OperatorCreationException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_0

    .line 436
    .end local v3    # "cOut":Ljava/io/OutputStream;
    .end local v4    # "calc":Lcom/android/sec/org/bouncycastle/operator/DigestCalculator;
    .end local v9    # "digOut":Ljava/io/OutputStream;
    .end local v14    # "sigOut":Ljava/io/OutputStream;
    :catch_1
    move-exception v10

    .line 438
    .local v10, "e":Ljava/io/IOException;
    new-instance v21, Lcom/android/sec/org/bouncycastle/cms/CMSException;

    const-string v22, "can\'t process mime object to create signature."

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-direct {v0, v1, v10}, Lcom/android/sec/org/bouncycastle/cms/CMSException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v21

    .line 401
    .end local v10    # "e":Ljava/io/IOException;
    .restart local v4    # "calc":Lcom/android/sec/org/bouncycastle/operator/DigestCalculator;
    .restart local v9    # "digOut":Ljava/io/OutputStream;
    .restart local v14    # "sigOut":Ljava/io/OutputStream;
    :cond_2
    :try_start_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/cms/SignerInformation;->content:Lcom/android/sec/org/bouncycastle/cms/CMSProcessable;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-interface {v0, v9}, Lcom/android/sec/org/bouncycastle/cms/CMSProcessable;->write(Ljava/io/OutputStream;)V

    .line 402
    invoke-virtual/range {p0 .. p0}, Lcom/android/sec/org/bouncycastle/cms/SignerInformation;->getEncodedSignedAttributes()[B

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v14, v0}, Ljava/io/OutputStream;->write([B)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Lcom/android/sec/org/bouncycastle/operator/OperatorCreationException; {:try_start_3 .. :try_end_3} :catch_2

    goto/16 :goto_0

    .line 440
    .end local v4    # "calc":Lcom/android/sec/org/bouncycastle/operator/DigestCalculator;
    .end local v9    # "digOut":Ljava/io/OutputStream;
    .end local v14    # "sigOut":Ljava/io/OutputStream;
    :catch_2
    move-exception v10

    .line 442
    .local v10, "e":Lcom/android/sec/org/bouncycastle/operator/OperatorCreationException;
    new-instance v21, Lcom/android/sec/org/bouncycastle/cms/CMSException;

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "can\'t create digest calculator: "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual {v10}, Lcom/android/sec/org/bouncycastle/operator/OperatorCreationException;->getMessage()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-direct {v0, v1, v10}, Lcom/android/sec/org/bouncycastle/cms/CMSException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v21

    .line 407
    .end local v10    # "e":Lcom/android/sec/org/bouncycastle/operator/OperatorCreationException;
    .restart local v4    # "calc":Lcom/android/sec/org/bouncycastle/operator/DigestCalculator;
    .restart local v14    # "sigOut":Ljava/io/OutputStream;
    :cond_3
    :try_start_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/cms/SignerInformation;->signedAttributeSet:Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;

    move-object/from16 v21, v0

    if-eqz v21, :cond_4

    .line 409
    invoke-virtual/range {p0 .. p0}, Lcom/android/sec/org/bouncycastle/cms/SignerInformation;->getEncodedSignedAttributes()[B

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v14, v0}, Ljava/io/OutputStream;->write([B)V

    goto/16 :goto_1

    .line 414
    :cond_4
    new-instance v21, Lcom/android/sec/org/bouncycastle/cms/CMSException;

    const-string v22, "data not encapsulated in signature - use detached constructor."

    invoke-direct/range {v21 .. v22}, Lcom/android/sec/org/bouncycastle/cms/CMSException;-><init>(Ljava/lang/String;)V

    throw v21

    .line 421
    .end local v4    # "calc":Lcom/android/sec/org/bouncycastle/operator/DigestCalculator;
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/cms/SignerInformation;->signedAttributeSet:Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;

    move-object/from16 v21, v0

    if-nez v21, :cond_6

    .line 423
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/cms/SignerInformation;->content:Lcom/android/sec/org/bouncycastle/cms/CMSProcessable;

    move-object/from16 v21, v0

    if-eqz v21, :cond_0

    .line 425
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/cms/SignerInformation;->content:Lcom/android/sec/org/bouncycastle/cms/CMSProcessable;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-interface {v0, v14}, Lcom/android/sec/org/bouncycastle/cms/CMSProcessable;->write(Ljava/io/OutputStream;)V

    goto/16 :goto_2

    .line 430
    :cond_6
    invoke-virtual/range {p0 .. p0}, Lcom/android/sec/org/bouncycastle/cms/SignerInformation;->getEncodedSignedAttributes()[B

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v14, v0}, Ljava/io/OutputStream;->write([B)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Lcom/android/sec/org/bouncycastle/operator/OperatorCreationException; {:try_start_4 .. :try_end_4} :catch_2

    goto/16 :goto_2

    .line 458
    .restart local v19    # "validContentType":Lcom/android/sec/org/bouncycastle/asn1/ASN1Primitive;
    :cond_7
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/sec/org/bouncycastle/cms/SignerInformation;->isCounterSignature:Z

    move/from16 v21, v0

    if-eqz v21, :cond_8

    .line 460
    new-instance v21, Lcom/android/sec/org/bouncycastle/cms/CMSException;

    const-string v22, "[For counter signatures,] the signedAttributes field MUST NOT contain a content-type attribute"

    invoke-direct/range {v21 .. v22}, Lcom/android/sec/org/bouncycastle/cms/CMSException;-><init>(Ljava/lang/String;)V

    throw v21

    .line 463
    :cond_8
    move-object/from16 v0, v19

    instance-of v0, v0, Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    move/from16 v21, v0

    if-nez v21, :cond_9

    .line 465
    new-instance v21, Lcom/android/sec/org/bouncycastle/cms/CMSException;

    const-string v22, "content-type attribute value not of ASN.1 type \'OBJECT IDENTIFIER\'"

    invoke-direct/range {v21 .. v22}, Lcom/android/sec/org/bouncycastle/cms/CMSException;-><init>(Ljava/lang/String;)V

    throw v21

    :cond_9
    move-object/from16 v16, v19

    .line 468
    check-cast v16, Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 470
    .local v16, "signedContentType":Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/cms/SignerInformation;->contentType:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    move-object/from16 v21, v0

    move-object/from16 v0, v16

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-nez v21, :cond_a

    .line 472
    new-instance v21, Lcom/android/sec/org/bouncycastle/cms/CMSException;

    const-string v22, "content-type attribute value does not match eContentType"

    invoke-direct/range {v21 .. v22}, Lcom/android/sec/org/bouncycastle/cms/CMSException;-><init>(Ljava/lang/String;)V

    throw v21

    .line 479
    .end local v16    # "signedContentType":Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;
    :cond_a
    sget-object v21, Lcom/android/sec/org/bouncycastle/asn1/cms/CMSAttributes;->messageDigest:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    const-string v22, "message-digest"

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    invoke-direct {v0, v1, v2}, Lcom/android/sec/org/bouncycastle/cms/SignerInformation;->getSingleValuedSignedAttribute(Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;Ljava/lang/String;)Lcom/android/sec/org/bouncycastle/asn1/ASN1Primitive;

    move-result-object v20

    .line 481
    .local v20, "validMessageDigest":Lcom/android/sec/org/bouncycastle/asn1/ASN1Primitive;
    if-nez v20, :cond_b

    .line 483
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/cms/SignerInformation;->signedAttributeSet:Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;

    move-object/from16 v21, v0

    if-eqz v21, :cond_d

    .line 485
    new-instance v21, Lcom/android/sec/org/bouncycastle/cms/CMSException;

    const-string v22, "the message-digest signed attribute type MUST be present when there are any signed attributes present"

    invoke-direct/range {v21 .. v22}, Lcom/android/sec/org/bouncycastle/cms/CMSException;-><init>(Ljava/lang/String;)V

    throw v21

    .line 490
    :cond_b
    move-object/from16 v0, v20

    instance-of v0, v0, Lcom/android/sec/org/bouncycastle/asn1/ASN1OctetString;

    move/from16 v21, v0

    if-nez v21, :cond_c

    .line 492
    new-instance v21, Lcom/android/sec/org/bouncycastle/cms/CMSException;

    const-string v22, "message-digest attribute value not of ASN.1 type \'OCTET STRING\'"

    invoke-direct/range {v21 .. v22}, Lcom/android/sec/org/bouncycastle/cms/CMSException;-><init>(Ljava/lang/String;)V

    throw v21

    :cond_c
    move-object/from16 v17, v20

    .line 495
    check-cast v17, Lcom/android/sec/org/bouncycastle/asn1/ASN1OctetString;

    .line 497
    .local v17, "signedMessageDigest":Lcom/android/sec/org/bouncycastle/asn1/ASN1OctetString;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/cms/SignerInformation;->resultDigest:[B

    move-object/from16 v21, v0

    invoke-virtual/range {v17 .. v17}, Lcom/android/sec/org/bouncycastle/asn1/ASN1OctetString;->getOctets()[B

    move-result-object v22

    invoke-static/range {v21 .. v22}, Lcom/android/sec/org/bouncycastle/util/Arrays;->constantTimeAreEqual([B[B)Z

    move-result v21

    if-nez v21, :cond_d

    .line 499
    new-instance v21, Lcom/android/sec/org/bouncycastle/cms/CMSSignerDigestMismatchException;

    const-string v22, "message-digest attribute value does not match calculated value"

    invoke-direct/range {v21 .. v22}, Lcom/android/sec/org/bouncycastle/cms/CMSSignerDigestMismatchException;-><init>(Ljava/lang/String;)V

    throw v21

    .line 506
    .end local v17    # "signedMessageDigest":Lcom/android/sec/org/bouncycastle/asn1/ASN1OctetString;
    :cond_d
    invoke-virtual/range {p0 .. p0}, Lcom/android/sec/org/bouncycastle/cms/SignerInformation;->getSignedAttributes()Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;

    move-result-object v15

    .line 507
    .local v15, "signedAttrTable":Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;
    if-eqz v15, :cond_e

    sget-object v21, Lcom/android/sec/org/bouncycastle/asn1/cms/CMSAttributes;->counterSignature:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    move-object/from16 v0, v21

    invoke-virtual {v15, v0}, Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;->getAll(Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;)Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;->size()I

    move-result v21

    if-lez v21, :cond_e

    .line 510
    new-instance v21, Lcom/android/sec/org/bouncycastle/cms/CMSException;

    const-string v22, "A countersignature attribute MUST NOT be a signed attribute"

    invoke-direct/range {v21 .. v22}, Lcom/android/sec/org/bouncycastle/cms/CMSException;-><init>(Ljava/lang/String;)V

    throw v21

    .line 513
    :cond_e
    invoke-virtual/range {p0 .. p0}, Lcom/android/sec/org/bouncycastle/cms/SignerInformation;->getUnsignedAttributes()Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;

    move-result-object v18

    .line 514
    .local v18, "unsignedAttrTable":Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;
    if-eqz v18, :cond_10

    .line 516
    sget-object v21, Lcom/android/sec/org/bouncycastle/asn1/cms/CMSAttributes;->counterSignature:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    move-object/from16 v0, v18

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;->getAll(Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;)Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;

    move-result-object v7

    .line 517
    .local v7, "csAttrs":Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;
    const/4 v12, 0x0

    .local v12, "i":I
    :goto_3
    invoke-virtual {v7}, Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;->size()I

    move-result v21

    move/from16 v0, v21

    if-ge v12, v0, :cond_10

    .line 519
    invoke-virtual {v7, v12}, Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;->get(I)Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;

    move-result-object v6

    check-cast v6, Lcom/android/sec/org/bouncycastle/asn1/cms/Attribute;

    .line 520
    .local v6, "csAttr":Lcom/android/sec/org/bouncycastle/asn1/cms/Attribute;
    invoke-virtual {v6}, Lcom/android/sec/org/bouncycastle/asn1/cms/Attribute;->getAttrValues()Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;->size()I

    move-result v21

    const/16 v22, 0x1

    move/from16 v0, v21

    move/from16 v1, v22

    if-ge v0, v1, :cond_f

    .line 522
    new-instance v21, Lcom/android/sec/org/bouncycastle/cms/CMSException;

    const-string v22, "A countersignature attribute MUST contain at least one AttributeValue"

    invoke-direct/range {v21 .. v22}, Lcom/android/sec/org/bouncycastle/cms/CMSException;-><init>(Ljava/lang/String;)V

    throw v21

    .line 517
    :cond_f
    add-int/lit8 v12, v12, 0x1

    goto :goto_3

    .line 532
    .end local v6    # "csAttr":Lcom/android/sec/org/bouncycastle/asn1/cms/Attribute;
    .end local v7    # "csAttrs":Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;
    .end local v12    # "i":I
    :cond_10
    :try_start_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/cms/SignerInformation;->signedAttributeSet:Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;

    move-object/from16 v21, v0

    if-nez v21, :cond_12

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/cms/SignerInformation;->resultDigest:[B

    move-object/from16 v21, v0

    if-eqz v21, :cond_12

    .line 534
    instance-of v0, v5, Lcom/android/sec/org/bouncycastle/operator/RawContentVerifier;

    move/from16 v21, v0

    if-eqz v21, :cond_12

    .line 536
    move-object v0, v5

    check-cast v0, Lcom/android/sec/org/bouncycastle/operator/RawContentVerifier;

    move-object v13, v0

    .line 538
    .local v13, "rawVerifier":Lcom/android/sec/org/bouncycastle/operator/RawContentVerifier;
    const-string v21, "RSA"

    move-object/from16 v0, v21

    invoke-virtual {v11, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_11

    .line 540
    new-instance v8, Lcom/android/sec/org/bouncycastle/asn1/x509/DigestInfo;

    new-instance v21, Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/cms/SignerInformation;->digestAlgorithm:Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;->getAlgorithm()Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    move-result-object v22

    sget-object v23, Lcom/android/sec/org/bouncycastle/asn1/DERNull;->INSTANCE:Lcom/android/sec/org/bouncycastle/asn1/DERNull;

    invoke-direct/range {v21 .. v23}, Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;-><init>(Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/cms/SignerInformation;->resultDigest:[B

    move-object/from16 v22, v0

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-direct {v8, v0, v1}, Lcom/android/sec/org/bouncycastle/asn1/x509/DigestInfo;-><init>(Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;[B)V

    .line 542
    .local v8, "digInfo":Lcom/android/sec/org/bouncycastle/asn1/x509/DigestInfo;
    const-string v21, "DER"

    move-object/from16 v0, v21

    invoke-virtual {v8, v0}, Lcom/android/sec/org/bouncycastle/asn1/x509/DigestInfo;->getEncoded(Ljava/lang/String;)[B

    move-result-object v21

    invoke-virtual/range {p0 .. p0}, Lcom/android/sec/org/bouncycastle/cms/SignerInformation;->getSignature()[B

    move-result-object v22

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-interface {v13, v0, v1}, Lcom/android/sec/org/bouncycastle/operator/RawContentVerifier;->verify([B[B)Z

    move-result v21

    .line 549
    .end local v8    # "digInfo":Lcom/android/sec/org/bouncycastle/asn1/x509/DigestInfo;
    .end local v13    # "rawVerifier":Lcom/android/sec/org/bouncycastle/operator/RawContentVerifier;
    :goto_4
    return v21

    .line 545
    .restart local v13    # "rawVerifier":Lcom/android/sec/org/bouncycastle/operator/RawContentVerifier;
    :cond_11
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/cms/SignerInformation;->resultDigest:[B

    move-object/from16 v21, v0

    invoke-virtual/range {p0 .. p0}, Lcom/android/sec/org/bouncycastle/cms/SignerInformation;->getSignature()[B

    move-result-object v22

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-interface {v13, v0, v1}, Lcom/android/sec/org/bouncycastle/operator/RawContentVerifier;->verify([B[B)Z

    move-result v21

    goto :goto_4

    .line 549
    .end local v13    # "rawVerifier":Lcom/android/sec/org/bouncycastle/operator/RawContentVerifier;
    :cond_12
    invoke-virtual/range {p0 .. p0}, Lcom/android/sec/org/bouncycastle/cms/SignerInformation;->getSignature()[B

    move-result-object v21

    move-object/from16 v0, v21

    invoke-interface {v5, v0}, Lcom/android/sec/org/bouncycastle/operator/ContentVerifier;->verify([B)Z
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    move-result v21

    goto :goto_4

    .line 551
    :catch_3
    move-exception v10

    .line 553
    .local v10, "e":Ljava/io/IOException;
    new-instance v21, Lcom/android/sec/org/bouncycastle/cms/CMSException;

    const-string v22, "can\'t process mime object to create signature."

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-direct {v0, v1, v10}, Lcom/android/sec/org/bouncycastle/cms/CMSException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v21
.end method

.method private doVerify(Ljava/security/PublicKey;Ljava/security/Provider;)Z
    .locals 5
    .param p1, "key"    # Ljava/security/PublicKey;
    .param p2, "sigProvider"    # Ljava/security/Provider;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/cms/CMSException;,
            Ljava/security/NoSuchAlgorithmException;
        }
    .end annotation

    .prologue
    .line 333
    if-eqz p2, :cond_1

    .line 335
    :try_start_0
    invoke-virtual {p2}, Ljava/security/Provider;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "emailBC"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 337
    new-instance v2, Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSignerInfoVerifierBuilder;

    new-instance v3, Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaDigestCalculatorProviderBuilder;

    invoke-direct {v3}, Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaDigestCalculatorProviderBuilder;-><init>()V

    invoke-virtual {v3}, Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaDigestCalculatorProviderBuilder;->build()Lcom/android/sec/org/bouncycastle/operator/DigestCalculatorProvider;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSignerInfoVerifierBuilder;-><init>(Lcom/android/sec/org/bouncycastle/operator/DigestCalculatorProvider;)V

    invoke-virtual {v2, p2}, Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSignerInfoVerifierBuilder;->setProvider(Ljava/security/Provider;)Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSignerInfoVerifierBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSignerInfoVerifierBuilder;->build(Ljava/security/PublicKey;)Lcom/android/sec/org/bouncycastle/cms/SignerInformationVerifier;

    move-result-object v1

    .line 349
    .local v1, "verifier":Lcom/android/sec/org/bouncycastle/cms/SignerInformationVerifier;
    :goto_0
    invoke-direct {p0, v1}, Lcom/android/sec/org/bouncycastle/cms/SignerInformation;->doVerify(Lcom/android/sec/org/bouncycastle/cms/SignerInformationVerifier;)Z

    move-result v2

    return v2

    .line 341
    .end local v1    # "verifier":Lcom/android/sec/org/bouncycastle/cms/SignerInformationVerifier;
    :cond_0
    new-instance v2, Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSimpleSignerInfoVerifierBuilder;

    invoke-direct {v2}, Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSimpleSignerInfoVerifierBuilder;-><init>()V

    invoke-virtual {v2, p2}, Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSimpleSignerInfoVerifierBuilder;->setProvider(Ljava/security/Provider;)Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSimpleSignerInfoVerifierBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSimpleSignerInfoVerifierBuilder;->build(Ljava/security/PublicKey;)Lcom/android/sec/org/bouncycastle/cms/SignerInformationVerifier;

    move-result-object v1

    .restart local v1    # "verifier":Lcom/android/sec/org/bouncycastle/cms/SignerInformationVerifier;
    goto :goto_0

    .line 346
    .end local v1    # "verifier":Lcom/android/sec/org/bouncycastle/cms/SignerInformationVerifier;
    :cond_1
    new-instance v2, Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSimpleSignerInfoVerifierBuilder;

    invoke-direct {v2}, Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSimpleSignerInfoVerifierBuilder;-><init>()V

    invoke-virtual {v2, p1}, Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSimpleSignerInfoVerifierBuilder;->build(Ljava/security/PublicKey;)Lcom/android/sec/org/bouncycastle/cms/SignerInformationVerifier;
    :try_end_0
    .catch Lcom/android/sec/org/bouncycastle/operator/OperatorCreationException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .restart local v1    # "verifier":Lcom/android/sec/org/bouncycastle/cms/SignerInformationVerifier;
    goto :goto_0

    .line 351
    .end local v1    # "verifier":Lcom/android/sec/org/bouncycastle/cms/SignerInformationVerifier;
    :catch_0
    move-exception v0

    .line 353
    .local v0, "e":Lcom/android/sec/org/bouncycastle/operator/OperatorCreationException;
    new-instance v2, Lcom/android/sec/org/bouncycastle/cms/CMSException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "unable to create verifier: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/operator/OperatorCreationException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, v0}, Lcom/android/sec/org/bouncycastle/cms/CMSException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v2
.end method

.method private encodeObj(Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;)[B
    .locals 1
    .param p1, "obj"    # Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 116
    if-eqz p1, :cond_0

    .line 118
    invoke-interface {p1}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;->toASN1Primitive()Lcom/android/sec/org/bouncycastle/asn1/ASN1Primitive;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Primitive;->getEncoded()[B

    move-result-object v0

    .line 121
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getSigningTime()Lcom/android/sec/org/bouncycastle/asn1/cms/Time;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/cms/CMSException;
        }
    .end annotation

    .prologue
    .line 720
    sget-object v2, Lcom/android/sec/org/bouncycastle/asn1/cms/CMSAttributes;->signingTime:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    const-string v3, "signing-time"

    invoke-direct {p0, v2, v3}, Lcom/android/sec/org/bouncycastle/cms/SignerInformation;->getSingleValuedSignedAttribute(Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;Ljava/lang/String;)Lcom/android/sec/org/bouncycastle/asn1/ASN1Primitive;

    move-result-object v1

    .line 723
    .local v1, "validSigningTime":Lcom/android/sec/org/bouncycastle/asn1/ASN1Primitive;
    if-nez v1, :cond_0

    .line 725
    const/4 v2, 0x0

    .line 730
    :goto_0
    return-object v2

    :cond_0
    :try_start_0
    invoke-static {v1}, Lcom/android/sec/org/bouncycastle/asn1/cms/Time;->getInstance(Ljava/lang/Object;)Lcom/android/sec/org/bouncycastle/asn1/cms/Time;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    goto :goto_0

    .line 732
    :catch_0
    move-exception v0

    .line 734
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    new-instance v2, Lcom/android/sec/org/bouncycastle/cms/CMSException;

    const-string v3, "signing-time attribute value not a valid \'Time\' structure"

    invoke-direct {v2, v3}, Lcom/android/sec/org/bouncycastle/cms/CMSException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method private getSingleValuedSignedAttribute(Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;Ljava/lang/String;)Lcom/android/sec/org/bouncycastle/asn1/ASN1Primitive;
    .locals 8
    .param p1, "attrOID"    # Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;
    .param p2, "printableName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/cms/CMSException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v7, 0x0

    .line 681
    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/cms/SignerInformation;->getUnsignedAttributes()Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;

    move-result-object v3

    .line 682
    .local v3, "unsignedAttrTable":Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;
    if-eqz v3, :cond_0

    invoke-virtual {v3, p1}, Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;->getAll(Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;)Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;

    move-result-object v6

    invoke-virtual {v6}, Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;->size()I

    move-result v6

    if-lez v6, :cond_0

    .line 685
    new-instance v5, Lcom/android/sec/org/bouncycastle/cms/CMSException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "The "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " attribute MUST NOT be an unsigned attribute"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/android/sec/org/bouncycastle/cms/CMSException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 689
    :cond_0
    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/cms/SignerInformation;->getSignedAttributes()Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;

    move-result-object v1

    .line 690
    .local v1, "signedAttrTable":Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;
    if-nez v1, :cond_1

    .line 710
    :goto_0
    :pswitch_0
    return-object v5

    .line 695
    :cond_1
    invoke-virtual {v1, p1}, Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;->getAll(Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;)Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;

    move-result-object v4

    .line 696
    .local v4, "v":Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;
    invoke-virtual {v4}, Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;->size()I

    move-result v6

    packed-switch v6, :pswitch_data_0

    .line 713
    new-instance v5, Lcom/android/sec/org/bouncycastle/cms/CMSException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "The SignedAttributes in a signerInfo MUST NOT include multiple instances of the "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " attribute"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/android/sec/org/bouncycastle/cms/CMSException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 702
    :pswitch_1
    invoke-virtual {v4, v7}, Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;->get(I)Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;

    move-result-object v2

    check-cast v2, Lcom/android/sec/org/bouncycastle/asn1/cms/Attribute;

    .line 703
    .local v2, "t":Lcom/android/sec/org/bouncycastle/asn1/cms/Attribute;
    invoke-virtual {v2}, Lcom/android/sec/org/bouncycastle/asn1/cms/Attribute;->getAttrValues()Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;

    move-result-object v0

    .line 704
    .local v0, "attrValues":Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;
    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;->size()I

    move-result v5

    const/4 v6, 0x1

    if-eq v5, v6, :cond_2

    .line 706
    new-instance v5, Lcom/android/sec/org/bouncycastle/cms/CMSException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "A "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " attribute MUST have a single attribute value"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/android/sec/org/bouncycastle/cms/CMSException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 710
    :cond_2
    invoke-virtual {v0, v7}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;->getObjectAt(I)Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;

    move-result-object v5

    invoke-interface {v5}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;->toASN1Primitive()Lcom/android/sec/org/bouncycastle/asn1/ASN1Primitive;

    move-result-object v5

    goto :goto_0

    .line 696
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static replaceUnsignedAttributes(Lcom/android/sec/org/bouncycastle/cms/SignerInformation;Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;)Lcom/android/sec/org/bouncycastle/cms/SignerInformation;
    .locals 9
    .param p0, "signerInformation"    # Lcom/android/sec/org/bouncycastle/cms/SignerInformation;
    .param p1, "unsignedAttributes"    # Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;

    .prologue
    .line 751
    iget-object v7, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInformation;->info:Lcom/android/sec/org/bouncycastle/asn1/cms/SignerInfo;

    .line 752
    .local v7, "sInfo":Lcom/android/sec/org/bouncycastle/asn1/cms/SignerInfo;
    const/4 v6, 0x0

    .line 754
    .local v6, "unsignedAttr":Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;
    if-eqz p1, :cond_0

    .line 756
    new-instance v6, Lcom/android/sec/org/bouncycastle/asn1/DERSet;

    .end local v6    # "unsignedAttr":Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;
    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;->toASN1EncodableVector()Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;

    move-result-object v0

    invoke-direct {v6, v0}, Lcom/android/sec/org/bouncycastle/asn1/DERSet;-><init>(Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;)V

    .line 759
    .restart local v6    # "unsignedAttr":Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;
    :cond_0
    new-instance v8, Lcom/android/sec/org/bouncycastle/cms/SignerInformation;

    new-instance v0, Lcom/android/sec/org/bouncycastle/asn1/cms/SignerInfo;

    invoke-virtual {v7}, Lcom/android/sec/org/bouncycastle/asn1/cms/SignerInfo;->getSID()Lcom/android/sec/org/bouncycastle/asn1/cms/SignerIdentifier;

    move-result-object v1

    invoke-virtual {v7}, Lcom/android/sec/org/bouncycastle/asn1/cms/SignerInfo;->getDigestAlgorithm()Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;

    move-result-object v2

    invoke-virtual {v7}, Lcom/android/sec/org/bouncycastle/asn1/cms/SignerInfo;->getAuthenticatedAttributes()Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;

    move-result-object v3

    invoke-virtual {v7}, Lcom/android/sec/org/bouncycastle/asn1/cms/SignerInfo;->getDigestEncryptionAlgorithm()Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;

    move-result-object v4

    invoke-virtual {v7}, Lcom/android/sec/org/bouncycastle/asn1/cms/SignerInfo;->getEncryptedDigest()Lcom/android/sec/org/bouncycastle/asn1/ASN1OctetString;

    move-result-object v5

    invoke-direct/range {v0 .. v6}, Lcom/android/sec/org/bouncycastle/asn1/cms/SignerInfo;-><init>(Lcom/android/sec/org/bouncycastle/asn1/cms/SignerIdentifier;Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;Lcom/android/sec/org/bouncycastle/asn1/ASN1OctetString;Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;)V

    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInformation;->contentType:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInformation;->content:Lcom/android/sec/org/bouncycastle/cms/CMSProcessable;

    const/4 v3, 0x0

    invoke-direct {v8, v0, v1, v2, v3}, Lcom/android/sec/org/bouncycastle/cms/SignerInformation;-><init>(Lcom/android/sec/org/bouncycastle/asn1/cms/SignerInfo;Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;Lcom/android/sec/org/bouncycastle/cms/CMSProcessable;[B)V

    return-object v8
.end method


# virtual methods
.method public getContentDigest()[B
    .locals 2

    .prologue
    .line 170
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInformation;->resultDigest:[B

    if-nez v0, :cond_0

    .line 172
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "method can only be called after verify."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 175
    :cond_0
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInformation;->resultDigest:[B

    invoke-virtual {v0}, [B->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    check-cast v0, [B

    return-object v0
.end method

.method public getContentType()Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInformation;->contentType:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    return-object v0
.end method

.method public getCounterSignatures()Lcom/android/sec/org/bouncycastle/cms/SignerInformationStore;
    .locals 12

    .prologue
    const/4 v11, 0x0

    .line 251
    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/cms/SignerInformation;->getUnsignedAttributes()Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;

    move-result-object v6

    .line 252
    .local v6, "unsignedAttributeTable":Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;
    if-nez v6, :cond_0

    .line 254
    new-instance v8, Lcom/android/sec/org/bouncycastle/cms/SignerInformationStore;

    new-instance v9, Ljava/util/ArrayList;

    const/4 v10, 0x0

    invoke-direct {v9, v10}, Ljava/util/ArrayList;-><init>(I)V

    invoke-direct {v8, v9}, Lcom/android/sec/org/bouncycastle/cms/SignerInformationStore;-><init>(Ljava/util/Collection;)V

    .line 303
    :goto_0
    return-object v8

    .line 257
    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 264
    .local v2, "counterSignatures":Ljava/util/List;
    sget-object v8, Lcom/android/sec/org/bouncycastle/asn1/cms/CMSAttributes;->counterSignature:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    invoke-virtual {v6, v8}, Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;->getAll(Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;)Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;

    move-result-object v0

    .line 266
    .local v0, "allCSAttrs":Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_1
    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;->size()I

    move-result v8

    if-ge v4, v8, :cond_3

    .line 268
    invoke-virtual {v0, v4}, Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;->get(I)Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;

    move-result-object v1

    check-cast v1, Lcom/android/sec/org/bouncycastle/asn1/cms/Attribute;

    .line 275
    .local v1, "counterSignatureAttribute":Lcom/android/sec/org/bouncycastle/asn1/cms/Attribute;
    invoke-virtual {v1}, Lcom/android/sec/org/bouncycastle/asn1/cms/Attribute;->getAttrValues()Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;

    move-result-object v7

    .line 276
    .local v7, "values":Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;
    invoke-virtual {v7}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;->size()I

    move-result v8

    const/4 v9, 0x1

    if-ge v8, v9, :cond_1

    .line 281
    :cond_1
    invoke-virtual {v7}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;->getObjects()Ljava/util/Enumeration;

    move-result-object v3

    .local v3, "en":Ljava/util/Enumeration;
    :goto_2
    invoke-interface {v3}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v8

    if-eqz v8, :cond_2

    .line 297
    invoke-interface {v3}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v8

    invoke-static {v8}, Lcom/android/sec/org/bouncycastle/asn1/cms/SignerInfo;->getInstance(Ljava/lang/Object;)Lcom/android/sec/org/bouncycastle/asn1/cms/SignerInfo;

    move-result-object v5

    .line 299
    .local v5, "si":Lcom/android/sec/org/bouncycastle/asn1/cms/SignerInfo;
    new-instance v8, Lcom/android/sec/org/bouncycastle/cms/SignerInformation;

    new-instance v9, Lcom/android/sec/org/bouncycastle/cms/CMSProcessableByteArray;

    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/cms/SignerInformation;->getSignature()[B

    move-result-object v10

    invoke-direct {v9, v10}, Lcom/android/sec/org/bouncycastle/cms/CMSProcessableByteArray;-><init>([B)V

    invoke-direct {v8, v5, v11, v9, v11}, Lcom/android/sec/org/bouncycastle/cms/SignerInformation;-><init>(Lcom/android/sec/org/bouncycastle/asn1/cms/SignerInfo;Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;Lcom/android/sec/org/bouncycastle/cms/CMSProcessable;[B)V

    invoke-interface {v2, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 266
    .end local v5    # "si":Lcom/android/sec/org/bouncycastle/asn1/cms/SignerInfo;
    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 303
    .end local v1    # "counterSignatureAttribute":Lcom/android/sec/org/bouncycastle/asn1/cms/Attribute;
    .end local v3    # "en":Ljava/util/Enumeration;
    .end local v7    # "values":Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;
    :cond_3
    new-instance v8, Lcom/android/sec/org/bouncycastle/cms/SignerInformationStore;

    invoke-direct {v8, v2}, Lcom/android/sec/org/bouncycastle/cms/SignerInformationStore;-><init>(Ljava/util/Collection;)V

    goto :goto_0
.end method

.method public getDigestAlgOID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInformation;->digestAlgorithm:Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;->getAlgorithm()Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;->getId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDigestAlgParams()[B
    .locals 4

    .prologue
    .line 157
    :try_start_0
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInformation;->digestAlgorithm:Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;

    invoke-virtual {v1}, Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;->getParameters()Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/android/sec/org/bouncycastle/cms/SignerInformation;->encodeObj(Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;)[B
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    .line 159
    :catch_0
    move-exception v0

    .line 161
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "exception getting digest parameters "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public getDigestAlgorithmID()Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInformation;->digestAlgorithm:Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;

    return-object v0
.end method

.method public getEncodedSignedAttributes()[B
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 313
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInformation;->signedAttributeSet:Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;

    if-eqz v0, :cond_0

    .line 315
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInformation;->signedAttributeSet:Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;->getEncoded()[B

    move-result-object v0

    .line 318
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getEncryptionAlgOID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 183
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInformation;->encryptionAlgorithm:Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;->getAlgorithm()Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;->getId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getEncryptionAlgParams()[B
    .locals 4

    .prologue
    .line 194
    :try_start_0
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInformation;->encryptionAlgorithm:Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;

    invoke-virtual {v1}, Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;->getParameters()Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/android/sec/org/bouncycastle/cms/SignerInformation;->encodeObj(Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;)[B
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    .line 196
    :catch_0
    move-exception v0

    .line 198
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "exception getting encryption parameters "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public getSID()Lcom/android/sec/org/bouncycastle/cms/SignerId;
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInformation;->sid:Lcom/android/sec/org/bouncycastle/cms/SignerId;

    return-object v0
.end method

.method public getSignature()[B
    .locals 1

    .prologue
    .line 235
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInformation;->signature:[B

    invoke-virtual {v0}, [B->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    check-cast v0, [B

    return-object v0
.end method

.method public getSignedAttributes()Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;
    .locals 2

    .prologue
    .line 208
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInformation;->signedAttributeSet:Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInformation;->signedAttributeValues:Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;

    if-nez v0, :cond_0

    .line 210
    new-instance v0, Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;

    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInformation;->signedAttributeSet:Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;

    invoke-direct {v0, v1}, Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;-><init>(Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;)V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInformation;->signedAttributeValues:Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;

    .line 213
    :cond_0
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInformation;->signedAttributeValues:Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;

    return-object v0
.end method

.method public getUnsignedAttributes()Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;
    .locals 2

    .prologue
    .line 222
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInformation;->unsignedAttributeSet:Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInformation;->unsignedAttributeValues:Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;

    if-nez v0, :cond_0

    .line 224
    new-instance v0, Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;

    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInformation;->unsignedAttributeSet:Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;

    invoke-direct {v0, v1}, Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;-><init>(Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;)V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInformation;->unsignedAttributeValues:Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;

    .line 227
    :cond_0
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInformation;->unsignedAttributeValues:Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;

    return-object v0
.end method

.method public getVersion()I
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInformation;->info:Lcom/android/sec/org/bouncycastle/asn1/cms/SignerInfo;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/cms/SignerInfo;->getVersion()Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;->getValue()Ljava/math/BigInteger;

    move-result-object v0

    invoke-virtual {v0}, Ljava/math/BigInteger;->intValue()I

    move-result v0

    return v0
.end method

.method public isCounterSignature()Z
    .locals 1

    .prologue
    .line 104
    iget-boolean v0, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInformation;->isCounterSignature:Z

    return v0
.end method

.method public toASN1Structure()Lcom/android/sec/org/bouncycastle/asn1/cms/SignerInfo;
    .locals 1

    .prologue
    .line 674
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInformation;->info:Lcom/android/sec/org/bouncycastle/asn1/cms/SignerInfo;

    return-object v0
.end method

.method public toSignerInfo()Lcom/android/sec/org/bouncycastle/asn1/cms/SignerInfo;
    .locals 1

    .prologue
    .line 664
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInformation;->info:Lcom/android/sec/org/bouncycastle/asn1/cms/SignerInfo;

    return-object v0
.end method

.method public verify(Lcom/android/sec/org/bouncycastle/cms/SignerInformationVerifier;)Z
    .locals 4
    .param p1, "verifier"    # Lcom/android/sec/org/bouncycastle/cms/SignerInformationVerifier;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/cms/CMSException;
        }
    .end annotation

    .prologue
    .line 638
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/cms/SignerInformation;->getSigningTime()Lcom/android/sec/org/bouncycastle/asn1/cms/Time;

    move-result-object v1

    .line 640
    .local v1, "signingTime":Lcom/android/sec/org/bouncycastle/asn1/cms/Time;
    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/cms/SignerInformationVerifier;->hasAssociatedCertificate()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 642
    if-eqz v1, :cond_0

    .line 644
    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/cms/SignerInformationVerifier;->getAssociatedCertificate()Lcom/android/sec/org/bouncycastle/cert/X509CertificateHolder;

    move-result-object v0

    .line 646
    .local v0, "dcv":Lcom/android/sec/org/bouncycastle/cert/X509CertificateHolder;
    invoke-virtual {v1}, Lcom/android/sec/org/bouncycastle/asn1/cms/Time;->getDate()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/android/sec/org/bouncycastle/cert/X509CertificateHolder;->isValidOn(Ljava/util/Date;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 648
    new-instance v2, Lcom/android/sec/org/bouncycastle/cms/CMSVerifierCertificateNotValidException;

    const-string v3, "verifier not valid at signingTime"

    invoke-direct {v2, v3}, Lcom/android/sec/org/bouncycastle/cms/CMSVerifierCertificateNotValidException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 653
    .end local v0    # "dcv":Lcom/android/sec/org/bouncycastle/cert/X509CertificateHolder;
    :cond_0
    invoke-direct {p0, p1}, Lcom/android/sec/org/bouncycastle/cms/SignerInformation;->doVerify(Lcom/android/sec/org/bouncycastle/cms/SignerInformationVerifier;)Z

    move-result v2

    return v2
.end method

.method public verify(Ljava/security/PublicKey;Ljava/lang/String;)Z
    .locals 1
    .param p1, "key"    # Ljava/security/PublicKey;
    .param p2, "sigProvider"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/NoSuchAlgorithmException;,
            Ljava/security/NoSuchProviderException;,
            Lcom/android/sec/org/bouncycastle/cms/CMSException;
        }
    .end annotation

    .prologue
    .line 567
    invoke-static {p2}, Lcom/android/sec/org/bouncycastle/cms/CMSUtils;->getProvider(Ljava/lang/String;)Ljava/security/Provider;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/android/sec/org/bouncycastle/cms/SignerInformation;->verify(Ljava/security/PublicKey;Ljava/security/Provider;)Z

    move-result v0

    return v0
.end method

.method public verify(Ljava/security/PublicKey;Ljava/security/Provider;)Z
    .locals 1
    .param p1, "key"    # Ljava/security/PublicKey;
    .param p2, "sigProvider"    # Ljava/security/Provider;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/NoSuchAlgorithmException;,
            Ljava/security/NoSuchProviderException;,
            Lcom/android/sec/org/bouncycastle/cms/CMSException;
        }
    .end annotation

    .prologue
    .line 581
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/cms/SignerInformation;->getSigningTime()Lcom/android/sec/org/bouncycastle/asn1/cms/Time;

    .line 583
    invoke-direct {p0, p1, p2}, Lcom/android/sec/org/bouncycastle/cms/SignerInformation;->doVerify(Ljava/security/PublicKey;Ljava/security/Provider;)Z

    move-result v0

    return v0
.end method

.method public verify(Ljava/security/cert/X509Certificate;Ljava/lang/String;)Z
    .locals 1
    .param p1, "cert"    # Ljava/security/cert/X509Certificate;
    .param p2, "sigProvider"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/NoSuchAlgorithmException;,
            Ljava/security/NoSuchProviderException;,
            Ljava/security/cert/CertificateExpiredException;,
            Ljava/security/cert/CertificateNotYetValidException;,
            Lcom/android/sec/org/bouncycastle/cms/CMSException;
        }
    .end annotation

    .prologue
    .line 600
    invoke-static {p2}, Lcom/android/sec/org/bouncycastle/cms/CMSUtils;->getProvider(Ljava/lang/String;)Ljava/security/Provider;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/android/sec/org/bouncycastle/cms/SignerInformation;->verify(Ljava/security/cert/X509Certificate;Ljava/security/Provider;)Z

    move-result v0

    return v0
.end method

.method public verify(Ljava/security/cert/X509Certificate;Ljava/security/Provider;)Z
    .locals 2
    .param p1, "cert"    # Ljava/security/cert/X509Certificate;
    .param p2, "sigProvider"    # Ljava/security/Provider;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/NoSuchAlgorithmException;,
            Ljava/security/cert/CertificateExpiredException;,
            Ljava/security/cert/CertificateNotYetValidException;,
            Lcom/android/sec/org/bouncycastle/cms/CMSException;
        }
    .end annotation

    .prologue
    .line 617
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/cms/SignerInformation;->getSigningTime()Lcom/android/sec/org/bouncycastle/asn1/cms/Time;

    move-result-object v0

    .line 618
    .local v0, "signingTime":Lcom/android/sec/org/bouncycastle/asn1/cms/Time;
    if-eqz v0, :cond_0

    .line 620
    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/cms/Time;->getDate()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/security/cert/X509Certificate;->checkValidity(Ljava/util/Date;)V

    .line 623
    :cond_0
    invoke-virtual {p1}, Ljava/security/cert/X509Certificate;->getPublicKey()Ljava/security/PublicKey;

    move-result-object v1

    invoke-direct {p0, v1, p2}, Lcom/android/sec/org/bouncycastle/cms/SignerInformation;->doVerify(Ljava/security/PublicKey;Ljava/security/Provider;)Z

    move-result v1

    return v1
.end method
