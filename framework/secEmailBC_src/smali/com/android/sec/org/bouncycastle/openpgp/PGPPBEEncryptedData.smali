.class public Lcom/android/sec/org/bouncycastle/openpgp/PGPPBEEncryptedData;
.super Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedData;
.source "PGPPBEEncryptedData.java"


# instance fields
.field keyData:Lcom/android/sec/org/bouncycastle/bcpg/SymmetricKeyEncSessionPacket;


# direct methods
.method constructor <init>(Lcom/android/sec/org/bouncycastle/bcpg/SymmetricKeyEncSessionPacket;Lcom/android/sec/org/bouncycastle/bcpg/InputStreamPacket;)V
    .locals 0
    .param p1, "keyData"    # Lcom/android/sec/org/bouncycastle/bcpg/SymmetricKeyEncSessionPacket;
    .param p2, "encData"    # Lcom/android/sec/org/bouncycastle/bcpg/InputStreamPacket;

    .prologue
    .line 36
    invoke-direct {p0, p2}, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedData;-><init>(Lcom/android/sec/org/bouncycastle/bcpg/InputStreamPacket;)V

    .line 38
    iput-object p1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPBEEncryptedData;->keyData:Lcom/android/sec/org/bouncycastle/bcpg/SymmetricKeyEncSessionPacket;

    .line 39
    return-void
.end method

.method private createStreamCipher(ILjava/security/Provider;)Ljavax/crypto/Cipher;
    .locals 4
    .param p1, "keyAlgorithm"    # I
    .param p2, "provider"    # Ljava/security/Provider;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/NoSuchAlgorithmException;,
            Ljavax/crypto/NoSuchPaddingException;,
            Ljava/security/NoSuchProviderException;,
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
        }
    .end annotation

    .prologue
    .line 171
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPBEEncryptedData;->encData:Lcom/android/sec/org/bouncycastle/bcpg/InputStreamPacket;

    instance-of v2, v2, Lcom/android/sec/org/bouncycastle/bcpg/SymmetricEncIntegrityPacket;

    if-eqz v2, :cond_0

    const-string v1, "CFB"

    .line 175
    .local v1, "mode":Ljava/lang/String;
    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPUtil;->getSymmetricCipherName(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/NoPadding"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 178
    .local v0, "cName":Ljava/lang/String;
    invoke-static {v0, p2}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;Ljava/security/Provider;)Ljavax/crypto/Cipher;

    move-result-object v2

    return-object v2

    .line 171
    .end local v0    # "cName":Ljava/lang/String;
    .end local v1    # "mode":Ljava/lang/String;
    :cond_0
    const-string v1, "OpenPGPCFB"

    goto :goto_0
.end method


# virtual methods
.method public getDataStream([CLjava/lang/String;)Ljava/io/InputStream;
    .locals 1
    .param p1, "passPhrase"    # [C
    .param p2, "provider"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;,
            Ljava/security/NoSuchProviderException;
        }
    .end annotation

    .prologue
    .line 65
    invoke-static {p2}, Lcom/android/sec/org/bouncycastle/openpgp/PGPUtil;->getProvider(Ljava/lang/String;)Ljava/security/Provider;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPBEEncryptedData;->getDataStream([CLjava/security/Provider;)Ljava/io/InputStream;

    move-result-object v0

    return-object v0
.end method

.method public getDataStream([CLjava/security/Provider;)Ljava/io/InputStream;
    .locals 22
    .param p1, "passPhrase"    # [C
    .param p2, "provider"    # Ljava/security/Provider;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
        }
    .end annotation

    .prologue
    .line 83
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPBEEncryptedData;->keyData:Lcom/android/sec/org/bouncycastle/bcpg/SymmetricKeyEncSessionPacket;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/android/sec/org/bouncycastle/bcpg/SymmetricKeyEncSessionPacket;->getEncAlgorithm()I

    move-result v11

    .line 84
    .local v11, "keyAlgorithm":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPBEEncryptedData;->keyData:Lcom/android/sec/org/bouncycastle/bcpg/SymmetricKeyEncSessionPacket;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/android/sec/org/bouncycastle/bcpg/SymmetricKeyEncSessionPacket;->getS2K()Lcom/android/sec/org/bouncycastle/bcpg/S2K;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-static {v11, v0, v1, v2}, Lcom/android/sec/org/bouncycastle/openpgp/PGPUtil;->makeKeyFromPassPhrase(ILcom/android/sec/org/bouncycastle/bcpg/S2K;[CLjava/security/Provider;)Ljavax/crypto/SecretKey;

    move-result-object v10

    .line 86
    .local v10, "key":Ljavax/crypto/SecretKey;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPBEEncryptedData;->keyData:Lcom/android/sec/org/bouncycastle/bcpg/SymmetricKeyEncSessionPacket;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/android/sec/org/bouncycastle/bcpg/SymmetricKeyEncSessionPacket;->getSecKeyData()[B

    move-result-object v15

    .line 87
    .local v15, "secKeyData":[B
    if-eqz v15, :cond_0

    array-length v0, v15

    move/from16 v19, v0

    if-lez v19, :cond_0

    .line 89
    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v11}, Lcom/android/sec/org/bouncycastle/openpgp/PGPUtil;->getSymmetricCipherName(I)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "/CFB/NoPadding"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, p2

    invoke-static {v0, v1}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;Ljava/security/Provider;)Ljavax/crypto/Cipher;

    move-result-object v13

    .line 93
    .local v13, "keyCipher":Ljavax/crypto/Cipher;
    const/16 v19, 0x2

    new-instance v20, Ljavax/crypto/spec/IvParameterSpec;

    invoke-virtual {v13}, Ljavax/crypto/Cipher;->getBlockSize()I

    move-result v21

    move/from16 v0, v21

    new-array v0, v0, [B

    move-object/from16 v21, v0

    invoke-direct/range {v20 .. v21}, Ljavax/crypto/spec/IvParameterSpec;-><init>([B)V

    move/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v13, v0, v10, v1}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;)V

    .line 95
    invoke-virtual {v13, v15}, Ljavax/crypto/Cipher;->doFinal([B)[B

    move-result-object v12

    .line 97
    .local v12, "keyBytes":[B
    const/16 v19, 0x0

    aget-byte v11, v12, v19

    .line 98
    new-instance v10, Ljavax/crypto/spec/SecretKeySpec;

    .end local v10    # "key":Ljavax/crypto/SecretKey;
    const/16 v19, 0x1

    array-length v0, v12

    move/from16 v20, v0

    add-int/lit8 v20, v20, -0x1

    invoke-static {v11}, Lcom/android/sec/org/bouncycastle/openpgp/PGPUtil;->getSymmetricCipherName(I)Ljava/lang/String;

    move-result-object v21

    move/from16 v0, v19

    move/from16 v1, v20

    move-object/from16 v2, v21

    invoke-direct {v10, v12, v0, v1, v2}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BIILjava/lang/String;)V

    .line 101
    .end local v12    # "keyBytes":[B
    .end local v13    # "keyCipher":Ljavax/crypto/Cipher;
    .restart local v10    # "key":Ljavax/crypto/SecretKey;
    :cond_0
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v11, v1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPBEEncryptedData;->createStreamCipher(ILjava/security/Provider;)Ljavax/crypto/Cipher;

    move-result-object v3

    .line 103
    .local v3, "c":Ljavax/crypto/Cipher;
    invoke-virtual {v3}, Ljavax/crypto/Cipher;->getBlockSize()I

    move-result v19

    move/from16 v0, v19

    new-array v9, v0, [B

    .line 105
    .local v9, "iv":[B
    const/16 v19, 0x2

    new-instance v20, Ljavax/crypto/spec/IvParameterSpec;

    move-object/from16 v0, v20

    invoke-direct {v0, v9}, Ljavax/crypto/spec/IvParameterSpec;-><init>([B)V

    move/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v3, v0, v10, v1}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;)V

    .line 107
    new-instance v19, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;

    new-instance v20, Ljavax/crypto/CipherInputStream;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPBEEncryptedData;->encData:Lcom/android/sec/org/bouncycastle/bcpg/InputStreamPacket;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/android/sec/org/bouncycastle/bcpg/InputStreamPacket;->getInputStream()Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;

    move-result-object v21

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-direct {v0, v1, v3}, Ljavax/crypto/CipherInputStream;-><init>(Ljava/io/InputStream;Ljavax/crypto/Cipher;)V

    invoke-direct/range {v19 .. v20}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;-><init>(Ljava/io/InputStream;)V

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/sec/org/bouncycastle/openpgp/PGPPBEEncryptedData;->encStream:Ljava/io/InputStream;

    .line 109
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPBEEncryptedData;->encData:Lcom/android/sec/org/bouncycastle/bcpg/InputStreamPacket;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    instance-of v0, v0, Lcom/android/sec/org/bouncycastle/bcpg/SymmetricEncIntegrityPacket;

    move/from16 v19, v0

    if-eqz v19, :cond_1

    .line 111
    new-instance v19, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedData$TruncatedStream;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPBEEncryptedData;->encStream:Ljava/io/InputStream;

    move-object/from16 v20, v0

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    move-object/from16 v2, v20

    invoke-direct {v0, v1, v2}, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedData$TruncatedStream;-><init>(Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedData;Ljava/io/InputStream;)V

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/sec/org/bouncycastle/openpgp/PGPPBEEncryptedData;->truncStream:Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedData$TruncatedStream;

    .line 113
    const/16 v19, 0x2

    invoke-static/range {v19 .. v19}, Lcom/android/sec/org/bouncycastle/openpgp/PGPUtil;->getDigestName(I)Ljava/lang/String;

    move-result-object v6

    .line 114
    .local v6, "digestName":Ljava/lang/String;
    move-object/from16 v0, p2

    invoke-static {v6, v0}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;Ljava/security/Provider;)Ljava/security/MessageDigest;

    move-result-object v5

    .line 116
    .local v5, "digest":Ljava/security/MessageDigest;
    new-instance v19, Ljava/security/DigestInputStream;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPBEEncryptedData;->truncStream:Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedData$TruncatedStream;

    move-object/from16 v20, v0

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-direct {v0, v1, v5}, Ljava/security/DigestInputStream;-><init>(Ljava/io/InputStream;Ljava/security/MessageDigest;)V

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/sec/org/bouncycastle/openpgp/PGPPBEEncryptedData;->encStream:Ljava/io/InputStream;

    .line 119
    .end local v5    # "digest":Ljava/security/MessageDigest;
    .end local v6    # "digestName":Ljava/lang/String;
    :cond_1
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_0
    array-length v0, v9

    move/from16 v19, v0

    move/from16 v0, v19

    if-eq v8, v0, :cond_3

    .line 121
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPBEEncryptedData;->encStream:Ljava/io/InputStream;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/io/InputStream;->read()I

    move-result v4

    .line 123
    .local v4, "ch":I
    if-gez v4, :cond_2

    .line 125
    new-instance v19, Ljava/io/EOFException;

    const-string v20, "unexpected end of stream."

    invoke-direct/range {v19 .. v20}, Ljava/io/EOFException;-><init>(Ljava/lang/String;)V

    throw v19
    :try_end_0
    .catch Lcom/android/sec/org/bouncycastle/openpgp/PGPException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 157
    .end local v3    # "c":Ljavax/crypto/Cipher;
    .end local v4    # "ch":I
    .end local v8    # "i":I
    .end local v9    # "iv":[B
    .end local v10    # "key":Ljavax/crypto/SecretKey;
    .end local v11    # "keyAlgorithm":I
    .end local v15    # "secKeyData":[B
    :catch_0
    move-exception v7

    .line 159
    .local v7, "e":Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
    throw v7

    .line 128
    .end local v7    # "e":Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
    .restart local v3    # "c":Ljavax/crypto/Cipher;
    .restart local v4    # "ch":I
    .restart local v8    # "i":I
    .restart local v9    # "iv":[B
    .restart local v10    # "key":Ljavax/crypto/SecretKey;
    .restart local v11    # "keyAlgorithm":I
    .restart local v15    # "secKeyData":[B
    :cond_2
    int-to-byte v0, v4

    move/from16 v19, v0

    :try_start_1
    aput-byte v19, v9, v8

    .line 119
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 131
    .end local v4    # "ch":I
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPBEEncryptedData;->encStream:Ljava/io/InputStream;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/io/InputStream;->read()I

    move-result v16

    .line 132
    .local v16, "v1":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPBEEncryptedData;->encStream:Ljava/io/InputStream;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/io/InputStream;->read()I

    move-result v17

    .line 134
    .local v17, "v2":I
    if-ltz v16, :cond_4

    if-gez v17, :cond_5

    .line 136
    :cond_4
    new-instance v19, Ljava/io/EOFException;

    const-string v20, "unexpected end of stream."

    invoke-direct/range {v19 .. v20}, Ljava/io/EOFException;-><init>(Ljava/lang/String;)V

    throw v19
    :try_end_1
    .catch Lcom/android/sec/org/bouncycastle/openpgp/PGPException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 161
    .end local v3    # "c":Ljavax/crypto/Cipher;
    .end local v8    # "i":I
    .end local v9    # "iv":[B
    .end local v10    # "key":Ljavax/crypto/SecretKey;
    .end local v11    # "keyAlgorithm":I
    .end local v15    # "secKeyData":[B
    .end local v16    # "v1":I
    .end local v17    # "v2":I
    :catch_1
    move-exception v7

    .line 163
    .local v7, "e":Ljava/lang/Exception;
    new-instance v19, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;

    const-string v20, "Exception creating cipher"

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-direct {v0, v1, v7}, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v19

    .line 143
    .end local v7    # "e":Ljava/lang/Exception;
    .restart local v3    # "c":Ljavax/crypto/Cipher;
    .restart local v8    # "i":I
    .restart local v9    # "iv":[B
    .restart local v10    # "key":Ljavax/crypto/SecretKey;
    .restart local v11    # "keyAlgorithm":I
    .restart local v15    # "secKeyData":[B
    .restart local v16    # "v1":I
    .restart local v17    # "v2":I
    :cond_5
    :try_start_2
    array-length v0, v9

    move/from16 v19, v0

    add-int/lit8 v19, v19, -0x2

    aget-byte v19, v9, v19

    move/from16 v0, v16

    int-to-byte v0, v0

    move/from16 v20, v0

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_6

    array-length v0, v9

    move/from16 v19, v0

    add-int/lit8 v19, v19, -0x1

    aget-byte v19, v9, v19

    move/from16 v0, v17

    int-to-byte v0, v0

    move/from16 v20, v0

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_6

    const/4 v14, 0x1

    .line 148
    .local v14, "repeatCheckPassed":Z
    :goto_1
    if-nez v16, :cond_7

    if-nez v17, :cond_7

    const/16 v18, 0x1

    .line 150
    .local v18, "zeroesCheckPassed":Z
    :goto_2
    if-nez v14, :cond_8

    if-nez v18, :cond_8

    .line 152
    new-instance v19, Lcom/android/sec/org/bouncycastle/openpgp/PGPDataValidationException;

    const-string v20, "data check failed."

    invoke-direct/range {v19 .. v20}, Lcom/android/sec/org/bouncycastle/openpgp/PGPDataValidationException;-><init>(Ljava/lang/String;)V

    throw v19

    .line 143
    .end local v14    # "repeatCheckPassed":Z
    .end local v18    # "zeroesCheckPassed":Z
    :cond_6
    const/4 v14, 0x0

    goto :goto_1

    .line 148
    .restart local v14    # "repeatCheckPassed":Z
    :cond_7
    const/16 v18, 0x0

    goto :goto_2

    .line 155
    .restart local v18    # "zeroesCheckPassed":Z
    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPBEEncryptedData;->encStream:Ljava/io/InputStream;

    move-object/from16 v19, v0
    :try_end_2
    .catch Lcom/android/sec/org/bouncycastle/openpgp/PGPException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    return-object v19
.end method

.method public getInputStream()Ljava/io/InputStream;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPBEEncryptedData;->encData:Lcom/android/sec/org/bouncycastle/bcpg/InputStreamPacket;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/bcpg/InputStreamPacket;->getInputStream()Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;

    move-result-object v0

    return-object v0
.end method
