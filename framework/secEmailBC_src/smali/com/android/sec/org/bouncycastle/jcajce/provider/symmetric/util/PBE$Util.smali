.class public Lcom/android/sec/org/bouncycastle/jcajce/provider/symmetric/util/PBE$Util;
.super Ljava/lang/Object;
.source "PBE.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/sec/org/bouncycastle/jcajce/provider/symmetric/util/PBE;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Util"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static convertPassword(ILjavax/crypto/spec/PBEKeySpec;)[B
    .locals 2
    .param p0, "type"    # I
    .param p1, "keySpec"    # Ljavax/crypto/spec/PBEKeySpec;

    .prologue
    .line 306
    const/4 v1, 0x2

    if-ne p0, v1, :cond_0

    .line 308
    invoke-virtual {p1}, Ljavax/crypto/spec/PBEKeySpec;->getPassword()[C

    move-result-object v1

    invoke-static {v1}, Lcom/android/sec/org/bouncycastle/crypto/PBEParametersGenerator;->PKCS12PasswordToBytes([C)[B

    move-result-object v0

    .line 318
    .local v0, "key":[B
    :goto_0
    return-object v0

    .line 310
    .end local v0    # "key":[B
    :cond_0
    const/4 v1, 0x5

    if-eq p0, v1, :cond_1

    const/4 v1, 0x4

    if-ne p0, v1, :cond_2

    .line 312
    :cond_1
    invoke-virtual {p1}, Ljavax/crypto/spec/PBEKeySpec;->getPassword()[C

    move-result-object v1

    invoke-static {v1}, Lcom/android/sec/org/bouncycastle/crypto/PBEParametersGenerator;->PKCS5PasswordToUTF8Bytes([C)[B

    move-result-object v0

    .restart local v0    # "key":[B
    goto :goto_0

    .line 316
    .end local v0    # "key":[B
    :cond_2
    invoke-virtual {p1}, Ljavax/crypto/spec/PBEKeySpec;->getPassword()[C

    move-result-object v1

    invoke-static {v1}, Lcom/android/sec/org/bouncycastle/crypto/PBEParametersGenerator;->PKCS5PasswordToBytes([C)[B

    move-result-object v0

    .restart local v0    # "key":[B
    goto :goto_0
.end method

.method private static makePBEGenerator(II)Lcom/android/sec/org/bouncycastle/crypto/PBEParametersGenerator;
    .locals 3
    .param p0, "type"    # I
    .param p1, "hash"    # I

    .prologue
    .line 65
    if-eqz p0, :cond_0

    const/4 v1, 0x4

    if-ne p0, v1, :cond_1

    .line 67
    :cond_0
    packed-switch p1, :pswitch_data_0

    .line 85
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "PKCS5 scheme 1 only supports MD2, MD5 and SHA1."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 76
    :pswitch_0
    new-instance v0, Lcom/android/sec/org/bouncycastle/crypto/generators/PKCS5S1ParametersGenerator;

    invoke-static {}, Lcom/android/sec/org/bouncycastle/crypto/digests/AndroidDigestFactory;->getMD5()Lcom/android/sec/org/bouncycastle/crypto/Digest;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/sec/org/bouncycastle/crypto/generators/PKCS5S1ParametersGenerator;-><init>(Lcom/android/sec/org/bouncycastle/crypto/Digest;)V

    .line 138
    .local v0, "generator":Lcom/android/sec/org/bouncycastle/crypto/PBEParametersGenerator;
    :goto_0
    return-object v0

    .line 81
    .end local v0    # "generator":Lcom/android/sec/org/bouncycastle/crypto/PBEParametersGenerator;
    :pswitch_1
    new-instance v0, Lcom/android/sec/org/bouncycastle/crypto/generators/PKCS5S1ParametersGenerator;

    invoke-static {}, Lcom/android/sec/org/bouncycastle/crypto/digests/AndroidDigestFactory;->getSHA1()Lcom/android/sec/org/bouncycastle/crypto/Digest;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/sec/org/bouncycastle/crypto/generators/PKCS5S1ParametersGenerator;-><init>(Lcom/android/sec/org/bouncycastle/crypto/Digest;)V

    .line 83
    .restart local v0    # "generator":Lcom/android/sec/org/bouncycastle/crypto/PBEParametersGenerator;
    goto :goto_0

    .line 88
    .end local v0    # "generator":Lcom/android/sec/org/bouncycastle/crypto/PBEParametersGenerator;
    :cond_1
    const/4 v1, 0x1

    if-eq p0, v1, :cond_2

    const/4 v1, 0x5

    if-ne p0, v1, :cond_3

    .line 90
    :cond_2
    new-instance v0, Lcom/android/sec/org/bouncycastle/crypto/generators/PKCS5S2ParametersGenerator;

    invoke-direct {v0}, Lcom/android/sec/org/bouncycastle/crypto/generators/PKCS5S2ParametersGenerator;-><init>()V

    .restart local v0    # "generator":Lcom/android/sec/org/bouncycastle/crypto/PBEParametersGenerator;
    goto :goto_0

    .line 92
    .end local v0    # "generator":Lcom/android/sec/org/bouncycastle/crypto/PBEParametersGenerator;
    :cond_3
    const/4 v1, 0x2

    if-ne p0, v1, :cond_4

    .line 94
    packed-switch p1, :pswitch_data_1

    .line 130
    :pswitch_2
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "unknown digest scheme for PBE encryption."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 103
    :pswitch_3
    new-instance v0, Lcom/android/sec/org/bouncycastle/crypto/generators/PKCS12ParametersGenerator;

    invoke-static {}, Lcom/android/sec/org/bouncycastle/crypto/digests/AndroidDigestFactory;->getMD5()Lcom/android/sec/org/bouncycastle/crypto/Digest;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/sec/org/bouncycastle/crypto/generators/PKCS12ParametersGenerator;-><init>(Lcom/android/sec/org/bouncycastle/crypto/Digest;)V

    .line 105
    .restart local v0    # "generator":Lcom/android/sec/org/bouncycastle/crypto/PBEParametersGenerator;
    goto :goto_0

    .line 108
    .end local v0    # "generator":Lcom/android/sec/org/bouncycastle/crypto/PBEParametersGenerator;
    :pswitch_4
    new-instance v0, Lcom/android/sec/org/bouncycastle/crypto/generators/PKCS12ParametersGenerator;

    invoke-static {}, Lcom/android/sec/org/bouncycastle/crypto/digests/AndroidDigestFactory;->getSHA1()Lcom/android/sec/org/bouncycastle/crypto/Digest;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/sec/org/bouncycastle/crypto/generators/PKCS12ParametersGenerator;-><init>(Lcom/android/sec/org/bouncycastle/crypto/Digest;)V

    .line 110
    .restart local v0    # "generator":Lcom/android/sec/org/bouncycastle/crypto/PBEParametersGenerator;
    goto :goto_0

    .line 121
    .end local v0    # "generator":Lcom/android/sec/org/bouncycastle/crypto/PBEParametersGenerator;
    :pswitch_5
    new-instance v0, Lcom/android/sec/org/bouncycastle/crypto/generators/PKCS12ParametersGenerator;

    invoke-static {}, Lcom/android/sec/org/bouncycastle/crypto/digests/AndroidDigestFactory;->getSHA256()Lcom/android/sec/org/bouncycastle/crypto/Digest;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/sec/org/bouncycastle/crypto/generators/PKCS12ParametersGenerator;-><init>(Lcom/android/sec/org/bouncycastle/crypto/Digest;)V

    .line 123
    .restart local v0    # "generator":Lcom/android/sec/org/bouncycastle/crypto/PBEParametersGenerator;
    goto :goto_0

    .line 135
    .end local v0    # "generator":Lcom/android/sec/org/bouncycastle/crypto/PBEParametersGenerator;
    :cond_4
    new-instance v0, Lcom/android/sec/org/bouncycastle/crypto/generators/OpenSSLPBEParametersGenerator;

    invoke-direct {v0}, Lcom/android/sec/org/bouncycastle/crypto/generators/OpenSSLPBEParametersGenerator;-><init>()V

    .restart local v0    # "generator":Lcom/android/sec/org/bouncycastle/crypto/PBEParametersGenerator;
    goto :goto_0

    .line 67
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 94
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_3
        :pswitch_4
        :pswitch_2
        :pswitch_2
        :pswitch_5
    .end packed-switch
.end method

.method public static makePBEMacParameters(Lcom/android/sec/org/bouncycastle/jcajce/provider/symmetric/util/BCPBEKey;Ljava/security/spec/AlgorithmParameterSpec;)Lcom/android/sec/org/bouncycastle/crypto/CipherParameters;
    .locals 7
    .param p0, "pbeKey"    # Lcom/android/sec/org/bouncycastle/jcajce/provider/symmetric/util/BCPBEKey;
    .param p1, "spec"    # Ljava/security/spec/AlgorithmParameterSpec;

    .prologue
    .line 209
    if-eqz p1, :cond_0

    instance-of v5, p1, Ljavax/crypto/spec/PBEParameterSpec;

    if-nez v5, :cond_1

    .line 211
    :cond_0
    new-instance v5, Ljava/lang/IllegalArgumentException;

    const-string v6, "Need a PBEParameter spec with a PBE key."

    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5

    :cond_1
    move-object v4, p1

    .line 214
    check-cast v4, Ljavax/crypto/spec/PBEParameterSpec;

    .line 215
    .local v4, "pbeParam":Ljavax/crypto/spec/PBEParameterSpec;
    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/jcajce/provider/symmetric/util/BCPBEKey;->getType()I

    move-result v5

    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/jcajce/provider/symmetric/util/BCPBEKey;->getDigest()I

    move-result v6

    invoke-static {v5, v6}, Lcom/android/sec/org/bouncycastle/jcajce/provider/symmetric/util/PBE$Util;->makePBEGenerator(II)Lcom/android/sec/org/bouncycastle/crypto/PBEParametersGenerator;

    move-result-object v0

    .line 216
    .local v0, "generator":Lcom/android/sec/org/bouncycastle/crypto/PBEParametersGenerator;
    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/jcajce/provider/symmetric/util/BCPBEKey;->getEncoded()[B

    move-result-object v2

    .line 219
    .local v2, "key":[B
    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/jcajce/provider/symmetric/util/BCPBEKey;->shouldTryWrongPKCS12()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 221
    const/4 v5, 0x2

    new-array v2, v5, [B

    .line 224
    :cond_2
    invoke-virtual {v4}, Ljavax/crypto/spec/PBEParameterSpec;->getSalt()[B

    move-result-object v5

    invoke-virtual {v4}, Ljavax/crypto/spec/PBEParameterSpec;->getIterationCount()I

    move-result v6

    invoke-virtual {v0, v2, v5, v6}, Lcom/android/sec/org/bouncycastle/crypto/PBEParametersGenerator;->init([B[BI)V

    .line 226
    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/jcajce/provider/symmetric/util/BCPBEKey;->getKeySize()I

    move-result v5

    invoke-virtual {v0, v5}, Lcom/android/sec/org/bouncycastle/crypto/PBEParametersGenerator;->generateDerivedMacParameters(I)Lcom/android/sec/org/bouncycastle/crypto/CipherParameters;

    move-result-object v3

    .line 228
    .local v3, "param":Lcom/android/sec/org/bouncycastle/crypto/CipherParameters;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v5, v2

    if-eq v1, v5, :cond_3

    .line 230
    const/4 v5, 0x0

    aput-byte v5, v2, v1

    .line 228
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 233
    :cond_3
    return-object v3
.end method

.method public static makePBEMacParameters(Ljavax/crypto/spec/PBEKeySpec;III)Lcom/android/sec/org/bouncycastle/crypto/CipherParameters;
    .locals 6
    .param p0, "keySpec"    # Ljavax/crypto/spec/PBEKeySpec;
    .param p1, "type"    # I
    .param p2, "hash"    # I
    .param p3, "keySize"    # I

    .prologue
    .line 284
    invoke-static {p1, p2}, Lcom/android/sec/org/bouncycastle/jcajce/provider/symmetric/util/PBE$Util;->makePBEGenerator(II)Lcom/android/sec/org/bouncycastle/crypto/PBEParametersGenerator;

    move-result-object v0

    .line 288
    .local v0, "generator":Lcom/android/sec/org/bouncycastle/crypto/PBEParametersGenerator;
    invoke-static {p1, p0}, Lcom/android/sec/org/bouncycastle/jcajce/provider/symmetric/util/PBE$Util;->convertPassword(ILjavax/crypto/spec/PBEKeySpec;)[B

    move-result-object v2

    .line 290
    .local v2, "key":[B
    invoke-virtual {p0}, Ljavax/crypto/spec/PBEKeySpec;->getSalt()[B

    move-result-object v4

    invoke-virtual {p0}, Ljavax/crypto/spec/PBEKeySpec;->getIterationCount()I

    move-result v5

    invoke-virtual {v0, v2, v4, v5}, Lcom/android/sec/org/bouncycastle/crypto/PBEParametersGenerator;->init([B[BI)V

    .line 292
    invoke-virtual {v0, p3}, Lcom/android/sec/org/bouncycastle/crypto/PBEParametersGenerator;->generateDerivedMacParameters(I)Lcom/android/sec/org/bouncycastle/crypto/CipherParameters;

    move-result-object v3

    .line 294
    .local v3, "param":Lcom/android/sec/org/bouncycastle/crypto/CipherParameters;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v4, v2

    if-eq v1, v4, :cond_0

    .line 296
    const/4 v4, 0x0

    aput-byte v4, v2, v1

    .line 294
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 299
    :cond_0
    return-object v3
.end method

.method public static makePBEParameters(Lcom/android/sec/org/bouncycastle/jcajce/provider/symmetric/util/BCPBEKey;Ljava/security/spec/AlgorithmParameterSpec;Ljava/lang/String;)Lcom/android/sec/org/bouncycastle/crypto/CipherParameters;
    .locals 8
    .param p0, "pbeKey"    # Lcom/android/sec/org/bouncycastle/jcajce/provider/symmetric/util/BCPBEKey;
    .param p1, "spec"    # Ljava/security/spec/AlgorithmParameterSpec;
    .param p2, "targetAlgorithm"    # Ljava/lang/String;

    .prologue
    .line 150
    if-eqz p1, :cond_0

    instance-of v6, p1, Ljavax/crypto/spec/PBEParameterSpec;

    if-nez v6, :cond_1

    .line 152
    :cond_0
    new-instance v6, Ljava/lang/IllegalArgumentException;

    const-string v7, "Need a PBEParameter spec with a PBE key."

    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v6

    :cond_1
    move-object v5, p1

    .line 155
    check-cast v5, Ljavax/crypto/spec/PBEParameterSpec;

    .line 156
    .local v5, "pbeParam":Ljavax/crypto/spec/PBEParameterSpec;
    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/jcajce/provider/symmetric/util/BCPBEKey;->getType()I

    move-result v6

    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/jcajce/provider/symmetric/util/BCPBEKey;->getDigest()I

    move-result v7

    invoke-static {v6, v7}, Lcom/android/sec/org/bouncycastle/jcajce/provider/symmetric/util/PBE$Util;->makePBEGenerator(II)Lcom/android/sec/org/bouncycastle/crypto/PBEParametersGenerator;

    move-result-object v0

    .line 157
    .local v0, "generator":Lcom/android/sec/org/bouncycastle/crypto/PBEParametersGenerator;
    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/jcajce/provider/symmetric/util/BCPBEKey;->getEncoded()[B

    move-result-object v3

    .line 160
    .local v3, "key":[B
    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/jcajce/provider/symmetric/util/BCPBEKey;->shouldTryWrongPKCS12()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 162
    const/4 v6, 0x2

    new-array v3, v6, [B

    .line 165
    :cond_2
    invoke-virtual {v5}, Ljavax/crypto/spec/PBEParameterSpec;->getSalt()[B

    move-result-object v6

    invoke-virtual {v5}, Ljavax/crypto/spec/PBEParameterSpec;->getIterationCount()I

    move-result v7

    invoke-virtual {v0, v3, v6, v7}, Lcom/android/sec/org/bouncycastle/crypto/PBEParametersGenerator;->init([B[BI)V

    .line 167
    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/jcajce/provider/symmetric/util/BCPBEKey;->getIvSize()I

    move-result v6

    if-eqz v6, :cond_4

    .line 169
    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/jcajce/provider/symmetric/util/BCPBEKey;->getKeySize()I

    move-result v6

    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/jcajce/provider/symmetric/util/BCPBEKey;->getIvSize()I

    move-result v7

    invoke-virtual {v0, v6, v7}, Lcom/android/sec/org/bouncycastle/crypto/PBEParametersGenerator;->generateDerivedParameters(II)Lcom/android/sec/org/bouncycastle/crypto/CipherParameters;

    move-result-object v4

    .line 176
    .local v4, "param":Lcom/android/sec/org/bouncycastle/crypto/CipherParameters;
    :goto_0
    const-string v6, "DES"

    invoke-virtual {p2, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 178
    instance-of v6, v4, Lcom/android/sec/org/bouncycastle/crypto/params/ParametersWithIV;

    if-eqz v6, :cond_5

    move-object v6, v4

    .line 180
    check-cast v6, Lcom/android/sec/org/bouncycastle/crypto/params/ParametersWithIV;

    invoke-virtual {v6}, Lcom/android/sec/org/bouncycastle/crypto/params/ParametersWithIV;->getParameters()Lcom/android/sec/org/bouncycastle/crypto/CipherParameters;

    move-result-object v2

    check-cast v2, Lcom/android/sec/org/bouncycastle/crypto/params/KeyParameter;

    .line 182
    .local v2, "kParam":Lcom/android/sec/org/bouncycastle/crypto/params/KeyParameter;
    invoke-virtual {v2}, Lcom/android/sec/org/bouncycastle/crypto/params/KeyParameter;->getKey()[B

    move-result-object v6

    invoke-static {v6}, Lcom/android/sec/org/bouncycastle/crypto/params/DESParameters;->setOddParity([B)V

    .line 192
    .end local v2    # "kParam":Lcom/android/sec/org/bouncycastle/crypto/params/KeyParameter;
    :cond_3
    :goto_1
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_2
    array-length v6, v3

    if-eq v1, v6, :cond_6

    .line 194
    const/4 v6, 0x0

    aput-byte v6, v3, v1

    .line 192
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 173
    .end local v1    # "i":I
    .end local v4    # "param":Lcom/android/sec/org/bouncycastle/crypto/CipherParameters;
    :cond_4
    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/jcajce/provider/symmetric/util/BCPBEKey;->getKeySize()I

    move-result v6

    invoke-virtual {v0, v6}, Lcom/android/sec/org/bouncycastle/crypto/PBEParametersGenerator;->generateDerivedParameters(I)Lcom/android/sec/org/bouncycastle/crypto/CipherParameters;

    move-result-object v4

    .restart local v4    # "param":Lcom/android/sec/org/bouncycastle/crypto/CipherParameters;
    goto :goto_0

    :cond_5
    move-object v2, v4

    .line 186
    check-cast v2, Lcom/android/sec/org/bouncycastle/crypto/params/KeyParameter;

    .line 188
    .restart local v2    # "kParam":Lcom/android/sec/org/bouncycastle/crypto/params/KeyParameter;
    invoke-virtual {v2}, Lcom/android/sec/org/bouncycastle/crypto/params/KeyParameter;->getKey()[B

    move-result-object v6

    invoke-static {v6}, Lcom/android/sec/org/bouncycastle/crypto/params/DESParameters;->setOddParity([B)V

    goto :goto_1

    .line 197
    .end local v2    # "kParam":Lcom/android/sec/org/bouncycastle/crypto/params/KeyParameter;
    .restart local v1    # "i":I
    :cond_6
    return-object v4
.end method

.method public static makePBEParameters(Ljavax/crypto/spec/PBEKeySpec;IIII)Lcom/android/sec/org/bouncycastle/crypto/CipherParameters;
    .locals 6
    .param p0, "keySpec"    # Ljavax/crypto/spec/PBEKeySpec;
    .param p1, "type"    # I
    .param p2, "hash"    # I
    .param p3, "keySize"    # I
    .param p4, "ivSize"    # I

    .prologue
    .line 247
    invoke-static {p1, p2}, Lcom/android/sec/org/bouncycastle/jcajce/provider/symmetric/util/PBE$Util;->makePBEGenerator(II)Lcom/android/sec/org/bouncycastle/crypto/PBEParametersGenerator;

    move-result-object v0

    .line 251
    .local v0, "generator":Lcom/android/sec/org/bouncycastle/crypto/PBEParametersGenerator;
    invoke-static {p1, p0}, Lcom/android/sec/org/bouncycastle/jcajce/provider/symmetric/util/PBE$Util;->convertPassword(ILjavax/crypto/spec/PBEKeySpec;)[B

    move-result-object v2

    .line 253
    .local v2, "key":[B
    invoke-virtual {p0}, Ljavax/crypto/spec/PBEKeySpec;->getSalt()[B

    move-result-object v4

    invoke-virtual {p0}, Ljavax/crypto/spec/PBEKeySpec;->getIterationCount()I

    move-result v5

    invoke-virtual {v0, v2, v4, v5}, Lcom/android/sec/org/bouncycastle/crypto/PBEParametersGenerator;->init([B[BI)V

    .line 255
    if-eqz p4, :cond_0

    .line 257
    invoke-virtual {v0, p3, p4}, Lcom/android/sec/org/bouncycastle/crypto/PBEParametersGenerator;->generateDerivedParameters(II)Lcom/android/sec/org/bouncycastle/crypto/CipherParameters;

    move-result-object v3

    .line 264
    .local v3, "param":Lcom/android/sec/org/bouncycastle/crypto/CipherParameters;
    :goto_0
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    array-length v4, v2

    if-eq v1, v4, :cond_1

    .line 266
    const/4 v4, 0x0

    aput-byte v4, v2, v1

    .line 264
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 261
    .end local v1    # "i":I
    .end local v3    # "param":Lcom/android/sec/org/bouncycastle/crypto/CipherParameters;
    :cond_0
    invoke-virtual {v0, p3}, Lcom/android/sec/org/bouncycastle/crypto/PBEParametersGenerator;->generateDerivedParameters(I)Lcom/android/sec/org/bouncycastle/crypto/CipherParameters;

    move-result-object v3

    .restart local v3    # "param":Lcom/android/sec/org/bouncycastle/crypto/CipherParameters;
    goto :goto_0

    .line 269
    .restart local v1    # "i":I
    :cond_1
    return-object v3
.end method
