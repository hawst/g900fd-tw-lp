.class public Lcom/android/sec/org/bouncycastle/bcpg/SecretSubkeyPacket;
.super Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;
.source "SecretSubkeyPacket.java"


# direct methods
.method constructor <init>(Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;)V
    .locals 0
    .param p1, "in"    # Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 20
    invoke-direct {p0, p1}, Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;-><init>(Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;)V

    .line 21
    return-void
.end method

.method public constructor <init>(Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;IILcom/android/sec/org/bouncycastle/bcpg/S2K;[B[B)V
    .locals 0
    .param p1, "pubKeyPacket"    # Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;
    .param p2, "encAlgorithm"    # I
    .param p3, "s2kUsage"    # I
    .param p4, "s2k"    # Lcom/android/sec/org/bouncycastle/bcpg/S2K;
    .param p5, "iv"    # [B
    .param p6, "secKeyData"    # [B

    .prologue
    .line 49
    invoke-direct/range {p0 .. p6}, Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;-><init>(Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;IILcom/android/sec/org/bouncycastle/bcpg/S2K;[B[B)V

    .line 50
    return-void
.end method

.method public constructor <init>(Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;ILcom/android/sec/org/bouncycastle/bcpg/S2K;[B[B)V
    .locals 0
    .param p1, "pubKeyPacket"    # Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;
    .param p2, "encAlgorithm"    # I
    .param p3, "s2k"    # Lcom/android/sec/org/bouncycastle/bcpg/S2K;
    .param p4, "iv"    # [B
    .param p5, "secKeyData"    # [B

    .prologue
    .line 38
    invoke-direct/range {p0 .. p5}, Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;-><init>(Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;ILcom/android/sec/org/bouncycastle/bcpg/S2K;[B[B)V

    .line 39
    return-void
.end method


# virtual methods
.method public encode(Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;)V
    .locals 3
    .param p1, "out"    # Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 56
    const/4 v0, 0x7

    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/bcpg/SecretSubkeyPacket;->getEncodedContents()[B

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {p1, v0, v1, v2}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->writePacket(I[BZ)V

    .line 57
    return-void
.end method
