.class public Lcom/android/sec/org/bouncycastle/asn1/x509/ExtendedKeyUsage;
.super Lcom/android/sec/org/bouncycastle/asn1/ASN1Object;
.source "ExtendedKeyUsage.java"


# instance fields
.field seq:Lcom/android/sec/org/bouncycastle/asn1/ASN1Sequence;

.field usageTable:Ljava/util/Hashtable;


# direct methods
.method private constructor <init>(Lcom/android/sec/org/bouncycastle/asn1/ASN1Sequence;)V
    .locals 4
    .param p1, "seq"    # Lcom/android/sec/org/bouncycastle/asn1/ASN1Sequence;

    .prologue
    .line 65
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Object;-><init>()V

    .line 25
    new-instance v2, Ljava/util/Hashtable;

    invoke-direct {v2}, Ljava/util/Hashtable;-><init>()V

    iput-object v2, p0, Lcom/android/sec/org/bouncycastle/asn1/x509/ExtendedKeyUsage;->usageTable:Ljava/util/Hashtable;

    .line 66
    iput-object p1, p0, Lcom/android/sec/org/bouncycastle/asn1/x509/ExtendedKeyUsage;->seq:Lcom/android/sec/org/bouncycastle/asn1/ASN1Sequence;

    .line 68
    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Sequence;->getObjects()Ljava/util/Enumeration;

    move-result-object v0

    .line 70
    .local v0, "e":Ljava/util/Enumeration;
    :goto_0
    invoke-interface {v0}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 72
    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;

    .line 73
    .local v1, "o":Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;
    invoke-interface {v1}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;->toASN1Primitive()Lcom/android/sec/org/bouncycastle/asn1/ASN1Primitive;

    move-result-object v2

    instance-of v2, v2, Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    if-nez v2, :cond_0

    .line 75
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Only ASN1ObjectIdentifiers allowed in ExtendedKeyUsage."

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 77
    :cond_0
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/asn1/x509/ExtendedKeyUsage;->usageTable:Ljava/util/Hashtable;

    invoke-virtual {v2, v1, v1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 79
    .end local v1    # "o":Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;
    :cond_1
    return-void
.end method

.method public constructor <init>(Lcom/android/sec/org/bouncycastle/asn1/x509/KeyPurposeId;)V
    .locals 1
    .param p1, "usage"    # Lcom/android/sec/org/bouncycastle/asn1/x509/KeyPurposeId;

    .prologue
    .line 57
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Object;-><init>()V

    .line 25
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/asn1/x509/ExtendedKeyUsage;->usageTable:Ljava/util/Hashtable;

    .line 58
    new-instance v0, Lcom/android/sec/org/bouncycastle/asn1/DERSequence;

    invoke-direct {v0, p1}, Lcom/android/sec/org/bouncycastle/asn1/DERSequence;-><init>(Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;)V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/asn1/x509/ExtendedKeyUsage;->seq:Lcom/android/sec/org/bouncycastle/asn1/ASN1Sequence;

    .line 60
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/asn1/x509/ExtendedKeyUsage;->usageTable:Ljava/util/Hashtable;

    invoke-virtual {v0, p1, p1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 61
    return-void
.end method

.method public constructor <init>(Ljava/util/Vector;)V
    .locals 4
    .param p1, "usages"    # Ljava/util/Vector;

    .prologue
    .line 100
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Object;-><init>()V

    .line 25
    new-instance v3, Ljava/util/Hashtable;

    invoke-direct {v3}, Ljava/util/Hashtable;-><init>()V

    iput-object v3, p0, Lcom/android/sec/org/bouncycastle/asn1/x509/ExtendedKeyUsage;->usageTable:Ljava/util/Hashtable;

    .line 101
    new-instance v2, Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;

    invoke-direct {v2}, Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;-><init>()V

    .line 102
    .local v2, "v":Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;
    invoke-virtual {p1}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object v0

    .line 104
    .local v0, "e":Ljava/util/Enumeration;
    :goto_0
    invoke-interface {v0}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 106
    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/sec/org/bouncycastle/asn1/ASN1Primitive;

    .line 108
    .local v1, "o":Lcom/android/sec/org/bouncycastle/asn1/ASN1Primitive;
    invoke-virtual {v2, v1}, Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;->add(Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;)V

    .line 109
    iget-object v3, p0, Lcom/android/sec/org/bouncycastle/asn1/x509/ExtendedKeyUsage;->usageTable:Ljava/util/Hashtable;

    invoke-virtual {v3, v1, v1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 112
    .end local v1    # "o":Lcom/android/sec/org/bouncycastle/asn1/ASN1Primitive;
    :cond_0
    new-instance v3, Lcom/android/sec/org/bouncycastle/asn1/DERSequence;

    invoke-direct {v3, v2}, Lcom/android/sec/org/bouncycastle/asn1/DERSequence;-><init>(Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;)V

    iput-object v3, p0, Lcom/android/sec/org/bouncycastle/asn1/x509/ExtendedKeyUsage;->seq:Lcom/android/sec/org/bouncycastle/asn1/ASN1Sequence;

    .line 113
    return-void
.end method

.method public constructor <init>([Lcom/android/sec/org/bouncycastle/asn1/x509/KeyPurposeId;)V
    .locals 5
    .param p1, "usages"    # [Lcom/android/sec/org/bouncycastle/asn1/x509/KeyPurposeId;

    .prologue
    .line 83
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Object;-><init>()V

    .line 25
    new-instance v2, Ljava/util/Hashtable;

    invoke-direct {v2}, Ljava/util/Hashtable;-><init>()V

    iput-object v2, p0, Lcom/android/sec/org/bouncycastle/asn1/x509/ExtendedKeyUsage;->usageTable:Ljava/util/Hashtable;

    .line 84
    new-instance v1, Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;

    invoke-direct {v1}, Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;-><init>()V

    .line 86
    .local v1, "v":Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, p1

    if-eq v0, v2, :cond_0

    .line 88
    aget-object v2, p1, v0

    invoke-virtual {v1, v2}, Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;->add(Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;)V

    .line 89
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/asn1/x509/ExtendedKeyUsage;->usageTable:Ljava/util/Hashtable;

    aget-object v3, p1, v0

    aget-object v4, p1, v0

    invoke-virtual {v2, v3, v4}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 86
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 92
    :cond_0
    new-instance v2, Lcom/android/sec/org/bouncycastle/asn1/DERSequence;

    invoke-direct {v2, v1}, Lcom/android/sec/org/bouncycastle/asn1/DERSequence;-><init>(Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;)V

    iput-object v2, p0, Lcom/android/sec/org/bouncycastle/asn1/x509/ExtendedKeyUsage;->seq:Lcom/android/sec/org/bouncycastle/asn1/ASN1Sequence;

    .line 93
    return-void
.end method

.method public static fromExtensions(Lcom/android/sec/org/bouncycastle/asn1/x509/Extensions;)Lcom/android/sec/org/bouncycastle/asn1/x509/ExtendedKeyUsage;
    .locals 1
    .param p0, "extensions"    # Lcom/android/sec/org/bouncycastle/asn1/x509/Extensions;

    .prologue
    .line 52
    sget-object v0, Lcom/android/sec/org/bouncycastle/asn1/x509/Extension;->extendedKeyUsage:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    invoke-virtual {p0, v0}, Lcom/android/sec/org/bouncycastle/asn1/x509/Extensions;->getExtensionParsedValue(Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;)Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;

    move-result-object v0

    invoke-static {v0}, Lcom/android/sec/org/bouncycastle/asn1/x509/ExtendedKeyUsage;->getInstance(Ljava/lang/Object;)Lcom/android/sec/org/bouncycastle/asn1/x509/ExtendedKeyUsage;

    move-result-object v0

    return-object v0
.end method

.method public static getInstance(Lcom/android/sec/org/bouncycastle/asn1/ASN1TaggedObject;Z)Lcom/android/sec/org/bouncycastle/asn1/x509/ExtendedKeyUsage;
    .locals 1
    .param p0, "obj"    # Lcom/android/sec/org/bouncycastle/asn1/ASN1TaggedObject;
    .param p1, "explicit"    # Z

    .prologue
    .line 32
    invoke-static {p0, p1}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Sequence;->getInstance(Lcom/android/sec/org/bouncycastle/asn1/ASN1TaggedObject;Z)Lcom/android/sec/org/bouncycastle/asn1/ASN1Sequence;

    move-result-object v0

    invoke-static {v0}, Lcom/android/sec/org/bouncycastle/asn1/x509/ExtendedKeyUsage;->getInstance(Ljava/lang/Object;)Lcom/android/sec/org/bouncycastle/asn1/x509/ExtendedKeyUsage;

    move-result-object v0

    return-object v0
.end method

.method public static getInstance(Ljava/lang/Object;)Lcom/android/sec/org/bouncycastle/asn1/x509/ExtendedKeyUsage;
    .locals 2
    .param p0, "obj"    # Ljava/lang/Object;

    .prologue
    .line 38
    instance-of v0, p0, Lcom/android/sec/org/bouncycastle/asn1/x509/ExtendedKeyUsage;

    if-eqz v0, :cond_0

    .line 40
    check-cast p0, Lcom/android/sec/org/bouncycastle/asn1/x509/ExtendedKeyUsage;

    .line 47
    .end local p0    # "obj":Ljava/lang/Object;
    :goto_0
    return-object p0

    .line 42
    .restart local p0    # "obj":Ljava/lang/Object;
    :cond_0
    if-eqz p0, :cond_1

    .line 44
    new-instance v0, Lcom/android/sec/org/bouncycastle/asn1/x509/ExtendedKeyUsage;

    invoke-static {p0}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Sequence;->getInstance(Ljava/lang/Object;)Lcom/android/sec/org/bouncycastle/asn1/ASN1Sequence;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/sec/org/bouncycastle/asn1/x509/ExtendedKeyUsage;-><init>(Lcom/android/sec/org/bouncycastle/asn1/ASN1Sequence;)V

    move-object p0, v0

    goto :goto_0

    .line 47
    :cond_1
    const/4 p0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getUsages()[Lcom/android/sec/org/bouncycastle/asn1/x509/KeyPurposeId;
    .locals 5

    .prologue
    .line 128
    iget-object v4, p0, Lcom/android/sec/org/bouncycastle/asn1/x509/ExtendedKeyUsage;->seq:Lcom/android/sec/org/bouncycastle/asn1/ASN1Sequence;

    invoke-virtual {v4}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Sequence;->size()I

    move-result v4

    new-array v3, v4, [Lcom/android/sec/org/bouncycastle/asn1/x509/KeyPurposeId;

    .line 130
    .local v3, "temp":[Lcom/android/sec/org/bouncycastle/asn1/x509/KeyPurposeId;
    const/4 v0, 0x0

    .line 131
    .local v0, "i":I
    iget-object v4, p0, Lcom/android/sec/org/bouncycastle/asn1/x509/ExtendedKeyUsage;->seq:Lcom/android/sec/org/bouncycastle/asn1/ASN1Sequence;

    invoke-virtual {v4}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Sequence;->getObjects()Ljava/util/Enumeration;

    move-result-object v2

    .local v2, "it":Ljava/util/Enumeration;
    :goto_0
    invoke-interface {v2}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 133
    add-int/lit8 v1, v0, 0x1

    .end local v0    # "i":I
    .local v1, "i":I
    invoke-interface {v2}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v4

    invoke-static {v4}, Lcom/android/sec/org/bouncycastle/asn1/x509/KeyPurposeId;->getInstance(Ljava/lang/Object;)Lcom/android/sec/org/bouncycastle/asn1/x509/KeyPurposeId;

    move-result-object v4

    aput-object v4, v3, v0

    move v0, v1

    .end local v1    # "i":I
    .restart local v0    # "i":I
    goto :goto_0

    .line 135
    :cond_0
    return-object v3
.end method

.method public hasKeyPurposeId(Lcom/android/sec/org/bouncycastle/asn1/x509/KeyPurposeId;)Z
    .locals 1
    .param p1, "keyPurposeId"    # Lcom/android/sec/org/bouncycastle/asn1/x509/KeyPurposeId;

    .prologue
    .line 118
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/asn1/x509/ExtendedKeyUsage;->usageTable:Ljava/util/Hashtable;

    invoke-virtual {v0, p1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/asn1/x509/ExtendedKeyUsage;->usageTable:Ljava/util/Hashtable;

    invoke-virtual {v0}, Ljava/util/Hashtable;->size()I

    move-result v0

    return v0
.end method

.method public toASN1Primitive()Lcom/android/sec/org/bouncycastle/asn1/ASN1Primitive;
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/asn1/x509/ExtendedKeyUsage;->seq:Lcom/android/sec/org/bouncycastle/asn1/ASN1Sequence;

    return-object v0
.end method
