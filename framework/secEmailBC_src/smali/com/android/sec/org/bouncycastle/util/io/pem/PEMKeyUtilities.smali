.class final Lcom/android/sec/org/bouncycastle/util/io/pem/PEMKeyUtilities;
.super Ljava/lang/Object;
.source "PEMKeyUtilities.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/sec/org/bouncycastle/util/io/pem/PEMKeyUtilities$PEMUtilException;
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 132
    return-void
.end method

.method static crypt(ZLjava/lang/String;[B[CLjava/lang/String;[B)[B
    .locals 17
    .param p0, "encrypt"    # Z
    .param p1, "provider"    # Ljava/lang/String;
    .param p2, "bytes"    # [B
    .param p3, "password"    # [C
    .param p4, "dekAlgName"    # Ljava/lang/String;
    .param p5, "iv"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 18
    new-instance v10, Ljavax/crypto/spec/IvParameterSpec;

    move-object/from16 v0, p5

    invoke-direct {v10, v0}, Ljavax/crypto/spec/IvParameterSpec;-><init>([B)V

    .line 20
    .local v10, "paramSpec":Ljava/security/spec/AlgorithmParameterSpec;
    const-string v3, "CBC"

    .line 21
    .local v3, "blockMode":Ljava/lang/String;
    const-string v9, "PKCS5Padding"

    .line 25
    .local v9, "padding":Ljava/lang/String;
    const-string v14, "-CFB"

    move-object/from16 v0, p4

    invoke-virtual {v0, v14}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_0

    .line 26
    const-string v3, "CFB"

    .line 27
    const-string v9, "NoPadding"

    .line 29
    :cond_0
    const-string v14, "-ECB"

    move-object/from16 v0, p4

    invoke-virtual {v0, v14}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v14

    if-nez v14, :cond_1

    const-string v14, "DES-EDE"

    move-object/from16 v0, p4

    invoke-virtual {v14, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-nez v14, :cond_1

    const-string v14, "DES-EDE3"

    move-object/from16 v0, p4

    invoke-virtual {v14, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_2

    .line 33
    :cond_1
    const-string v3, "ECB"

    .line 34
    const/4 v10, 0x0

    .line 36
    :cond_2
    const-string v14, "-OFB"

    move-object/from16 v0, p4

    invoke-virtual {v0, v14}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_3

    .line 37
    const-string v3, "OFB"

    .line 38
    const-string v9, "NoPadding"

    .line 42
    :cond_3
    const-string v14, "DES-EDE"

    move-object/from16 v0, p4

    invoke-virtual {v0, v14}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_5

    .line 43
    const-string v2, "DESede"

    .line 46
    .local v2, "alg":Ljava/lang/String;
    const-string v14, "DES-EDE3"

    move-object/from16 v0, p4

    invoke-virtual {v0, v14}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v14

    if-nez v14, :cond_4

    const/4 v5, 0x1

    .line 47
    .local v5, "des2":Z
    :goto_0
    const/16 v14, 0x18

    move-object/from16 v0, p3

    move-object/from16 v1, p5

    invoke-static {v0, v2, v14, v1, v5}, Lcom/android/sec/org/bouncycastle/util/io/pem/PEMKeyUtilities;->getKey([CLjava/lang/String;I[BZ)Ljavax/crypto/SecretKey;

    move-result-object v11

    .line 92
    .end local v5    # "des2":Z
    .local v11, "sKey":Ljava/security/Key;
    :goto_1
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v14, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "/"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "/"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    .line 95
    .local v13, "transformation":Ljava/lang/String;
    :try_start_0
    move-object/from16 v0, p1

    invoke-static {v13, v0}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v4

    .line 96
    .local v4, "c":Ljavax/crypto/Cipher;
    if-eqz p0, :cond_11

    const/4 v8, 0x1

    .line 98
    .local v8, "mode":I
    :goto_2
    if-nez v10, :cond_12

    .line 100
    invoke-virtual {v4, v8, v11}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;)V

    .line 104
    :goto_3
    move-object/from16 v0, p2

    invoke-virtual {v4, v0}, Ljavax/crypto/Cipher;->doFinal([B)[B
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v14

    return-object v14

    .line 46
    .end local v4    # "c":Ljavax/crypto/Cipher;
    .end local v8    # "mode":I
    .end local v11    # "sKey":Ljava/security/Key;
    .end local v13    # "transformation":Ljava/lang/String;
    :cond_4
    const/4 v5, 0x0

    goto :goto_0

    .line 48
    .end local v2    # "alg":Ljava/lang/String;
    :cond_5
    const-string v14, "DES-"

    move-object/from16 v0, p4

    invoke-virtual {v0, v14}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_6

    .line 49
    const-string v2, "DES"

    .line 50
    .restart local v2    # "alg":Ljava/lang/String;
    const/16 v14, 0x8

    move-object/from16 v0, p3

    move-object/from16 v1, p5

    invoke-static {v0, v2, v14, v1}, Lcom/android/sec/org/bouncycastle/util/io/pem/PEMKeyUtilities;->getKey([CLjava/lang/String;I[B)Ljavax/crypto/SecretKey;

    move-result-object v11

    .restart local v11    # "sKey":Ljava/security/Key;
    goto :goto_1

    .line 51
    .end local v2    # "alg":Ljava/lang/String;
    .end local v11    # "sKey":Ljava/security/Key;
    :cond_6
    const-string v14, "BF-"

    move-object/from16 v0, p4

    invoke-virtual {v0, v14}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_7

    .line 52
    const-string v2, "Blowfish"

    .line 53
    .restart local v2    # "alg":Ljava/lang/String;
    const/16 v14, 0x10

    move-object/from16 v0, p3

    move-object/from16 v1, p5

    invoke-static {v0, v2, v14, v1}, Lcom/android/sec/org/bouncycastle/util/io/pem/PEMKeyUtilities;->getKey([CLjava/lang/String;I[B)Ljavax/crypto/SecretKey;

    move-result-object v11

    .restart local v11    # "sKey":Ljava/security/Key;
    goto :goto_1

    .line 54
    .end local v2    # "alg":Ljava/lang/String;
    .end local v11    # "sKey":Ljava/security/Key;
    :cond_7
    const-string v14, "RC2-"

    move-object/from16 v0, p4

    invoke-virtual {v0, v14}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_b

    .line 55
    const-string v2, "RC2"

    .line 56
    .restart local v2    # "alg":Ljava/lang/String;
    const/16 v7, 0x80

    .line 57
    .local v7, "keyBits":I
    const-string v14, "RC2-40-"

    move-object/from16 v0, p4

    invoke-virtual {v0, v14}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_9

    .line 58
    const/16 v7, 0x28

    .line 62
    :cond_8
    :goto_4
    div-int/lit8 v14, v7, 0x8

    move-object/from16 v0, p3

    move-object/from16 v1, p5

    invoke-static {v0, v2, v14, v1}, Lcom/android/sec/org/bouncycastle/util/io/pem/PEMKeyUtilities;->getKey([CLjava/lang/String;I[B)Ljavax/crypto/SecretKey;

    move-result-object v11

    .line 63
    .restart local v11    # "sKey":Ljava/security/Key;
    if-nez v10, :cond_a

    .line 65
    new-instance v10, Ljavax/crypto/spec/RC2ParameterSpec;

    .end local v10    # "paramSpec":Ljava/security/spec/AlgorithmParameterSpec;
    invoke-direct {v10, v7}, Ljavax/crypto/spec/RC2ParameterSpec;-><init>(I)V

    .restart local v10    # "paramSpec":Ljava/security/spec/AlgorithmParameterSpec;
    goto/16 :goto_1

    .line 59
    .end local v11    # "sKey":Ljava/security/Key;
    :cond_9
    const-string v14, "RC2-64-"

    move-object/from16 v0, p4

    invoke-virtual {v0, v14}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_8

    .line 60
    const/16 v7, 0x40

    goto :goto_4

    .line 67
    .restart local v11    # "sKey":Ljava/security/Key;
    :cond_a
    new-instance v10, Ljavax/crypto/spec/RC2ParameterSpec;

    .end local v10    # "paramSpec":Ljava/security/spec/AlgorithmParameterSpec;
    move-object/from16 v0, p5

    invoke-direct {v10, v7, v0}, Ljavax/crypto/spec/RC2ParameterSpec;-><init>(I[B)V

    .restart local v10    # "paramSpec":Ljava/security/spec/AlgorithmParameterSpec;
    goto/16 :goto_1

    .line 69
    .end local v2    # "alg":Ljava/lang/String;
    .end local v7    # "keyBits":I
    .end local v11    # "sKey":Ljava/security/Key;
    :cond_b
    const-string v14, "AES-"

    move-object/from16 v0, p4

    invoke-virtual {v0, v14}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_10

    .line 70
    const-string v2, "AES"

    .line 71
    .restart local v2    # "alg":Ljava/lang/String;
    move-object/from16 v12, p5

    .line 72
    .local v12, "salt":[B
    array-length v14, v12

    const/16 v15, 0x8

    if-le v14, v15, :cond_c

    .line 73
    const/16 v14, 0x8

    new-array v12, v14, [B

    .line 74
    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x8

    move-object/from16 v0, p5

    move/from16 v1, v16

    invoke-static {v0, v14, v12, v15, v1}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 78
    :cond_c
    const-string v14, "AES-128-"

    move-object/from16 v0, p4

    invoke-virtual {v0, v14}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_d

    .line 79
    const/16 v7, 0x80

    .line 87
    .restart local v7    # "keyBits":I
    :goto_5
    const-string v14, "AES"

    div-int/lit8 v15, v7, 0x8

    move-object/from16 v0, p3

    invoke-static {v0, v14, v15, v12}, Lcom/android/sec/org/bouncycastle/util/io/pem/PEMKeyUtilities;->getKey([CLjava/lang/String;I[B)Ljavax/crypto/SecretKey;

    move-result-object v11

    .line 88
    .restart local v11    # "sKey":Ljava/security/Key;
    goto/16 :goto_1

    .line 80
    .end local v7    # "keyBits":I
    .end local v11    # "sKey":Ljava/security/Key;
    :cond_d
    const-string v14, "AES-192-"

    move-object/from16 v0, p4

    invoke-virtual {v0, v14}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_e

    .line 81
    const/16 v7, 0xc0

    .restart local v7    # "keyBits":I
    goto :goto_5

    .line 82
    .end local v7    # "keyBits":I
    :cond_e
    const-string v14, "AES-256-"

    move-object/from16 v0, p4

    invoke-virtual {v0, v14}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_f

    .line 83
    const/16 v7, 0x100

    .restart local v7    # "keyBits":I
    goto :goto_5

    .line 85
    .end local v7    # "keyBits":I
    :cond_f
    new-instance v14, Lcom/android/sec/org/bouncycastle/util/io/pem/PEMKeyUtilities$PEMUtilException;

    const-string v15, "unknown AES encryption with private key"

    invoke-direct {v14, v15}, Lcom/android/sec/org/bouncycastle/util/io/pem/PEMKeyUtilities$PEMUtilException;-><init>(Ljava/lang/String;)V

    throw v14

    .line 89
    .end local v2    # "alg":Ljava/lang/String;
    .end local v12    # "salt":[B
    :cond_10
    new-instance v14, Lcom/android/sec/org/bouncycastle/util/io/pem/PEMKeyUtilities$PEMUtilException;

    const-string v15, "unknown encryption with private key"

    invoke-direct {v14, v15}, Lcom/android/sec/org/bouncycastle/util/io/pem/PEMKeyUtilities$PEMUtilException;-><init>(Ljava/lang/String;)V

    throw v14

    .line 96
    .restart local v2    # "alg":Ljava/lang/String;
    .restart local v4    # "c":Ljavax/crypto/Cipher;
    .restart local v11    # "sKey":Ljava/security/Key;
    .restart local v13    # "transformation":Ljava/lang/String;
    :cond_11
    const/4 v8, 0x2

    goto/16 :goto_2

    .line 102
    .restart local v8    # "mode":I
    :cond_12
    :try_start_1
    invoke-virtual {v4, v8, v11, v10}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_3

    .line 105
    .end local v4    # "c":Ljavax/crypto/Cipher;
    .end local v8    # "mode":I
    :catch_0
    move-exception v6

    .line 107
    .local v6, "e":Ljava/lang/Exception;
    new-instance v14, Lcom/android/sec/org/bouncycastle/util/io/pem/PEMKeyUtilities$PEMUtilException;

    const-string v15, "exception using cipher - please check password and data."

    invoke-direct {v14, v15, v6}, Lcom/android/sec/org/bouncycastle/util/io/pem/PEMKeyUtilities$PEMUtilException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v14
.end method

.method private static getKey([CLjava/lang/String;I[B)Ljavax/crypto/SecretKey;
    .locals 1
    .param p0, "password"    # [C
    .param p1, "algorithm"    # Ljava/lang/String;
    .param p2, "keyLength"    # I
    .param p3, "salt"    # [B

    .prologue
    .line 113
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, p3, v0}, Lcom/android/sec/org/bouncycastle/util/io/pem/PEMKeyUtilities;->getKey([CLjava/lang/String;I[BZ)Ljavax/crypto/SecretKey;

    move-result-object v0

    return-object v0
.end method

.method private static getKey([CLjava/lang/String;I[BZ)Ljavax/crypto/SecretKey;
    .locals 6
    .param p0, "password"    # [C
    .param p1, "algorithm"    # Ljava/lang/String;
    .param p2, "keyLength"    # I
    .param p3, "salt"    # [B
    .param p4, "des2"    # Z

    .prologue
    .line 118
    new-instance v2, Lcom/android/sec/org/bouncycastle/crypto/generators/OpenSSLPBEParametersGenerator;

    invoke-direct {v2}, Lcom/android/sec/org/bouncycastle/crypto/generators/OpenSSLPBEParametersGenerator;-><init>()V

    .line 120
    .local v2, "pGen":Lcom/android/sec/org/bouncycastle/crypto/generators/OpenSSLPBEParametersGenerator;
    invoke-static {p0}, Lcom/android/sec/org/bouncycastle/crypto/PBEParametersGenerator;->PKCS5PasswordToBytes([C)[B

    move-result-object v3

    invoke-virtual {v2, v3, p3}, Lcom/android/sec/org/bouncycastle/crypto/generators/OpenSSLPBEParametersGenerator;->init([B[B)V

    .line 123
    mul-int/lit8 v3, p2, 0x8

    invoke-virtual {v2, v3}, Lcom/android/sec/org/bouncycastle/crypto/generators/OpenSSLPBEParametersGenerator;->generateDerivedParameters(I)Lcom/android/sec/org/bouncycastle/crypto/CipherParameters;

    move-result-object v1

    check-cast v1, Lcom/android/sec/org/bouncycastle/crypto/params/KeyParameter;

    .line 124
    .local v1, "keyParam":Lcom/android/sec/org/bouncycastle/crypto/params/KeyParameter;
    invoke-virtual {v1}, Lcom/android/sec/org/bouncycastle/crypto/params/KeyParameter;->getKey()[B

    move-result-object v0

    .line 125
    .local v0, "key":[B
    if-eqz p4, :cond_0

    array-length v3, v0

    const/16 v4, 0x18

    if-lt v3, v4, :cond_0

    .line 127
    const/4 v3, 0x0

    const/16 v4, 0x10

    const/16 v5, 0x8

    invoke-static {v0, v3, v0, v4, v5}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 129
    :cond_0
    new-instance v3, Ljavax/crypto/spec/SecretKeySpec;

    invoke-direct {v3, v0, p1}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    return-object v3
.end method
