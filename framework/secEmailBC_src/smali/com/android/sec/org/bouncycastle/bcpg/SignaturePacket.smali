.class public Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;
.super Lcom/android/sec/org/bouncycastle/bcpg/ContainedPacket;
.source "SignaturePacket.java"

# interfaces
.implements Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyAlgorithmTags;


# instance fields
.field private creationTime:J

.field private fingerPrint:[B

.field private hashAlgorithm:I

.field private hashedData:[Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;

.field private keyAlgorithm:I

.field private keyID:J

.field private signature:[Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;

.field private signatureEncoding:[B

.field private signatureType:I

.field private unhashedData:[Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;

.field private version:I


# direct methods
.method public constructor <init>(IIJIIJ[B[Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;)V
    .locals 15
    .param p1, "version"    # I
    .param p2, "signatureType"    # I
    .param p3, "keyID"    # J
    .param p5, "keyAlgorithm"    # I
    .param p6, "hashAlgorithm"    # I
    .param p7, "creationTime"    # J
    .param p9, "fingerPrint"    # [B
    .param p10, "signature"    # [Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;

    .prologue
    .line 219
    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object v3, p0

    move/from16 v4, p1

    move/from16 v5, p2

    move-wide/from16 v6, p3

    move/from16 v8, p5

    move/from16 v9, p6

    move-object/from16 v12, p9

    move-object/from16 v13, p10

    invoke-direct/range {v3 .. v13}, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;-><init>(IIJII[Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;[Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;[B[Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;)V

    .line 221
    move-wide/from16 v0, p7

    iput-wide v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->creationTime:J

    .line 222
    return-void
.end method

.method public constructor <init>(IIJII[Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;[Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;[B[Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;)V
    .locals 1
    .param p1, "version"    # I
    .param p2, "signatureType"    # I
    .param p3, "keyID"    # J
    .param p5, "keyAlgorithm"    # I
    .param p6, "hashAlgorithm"    # I
    .param p7, "hashedData"    # [Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;
    .param p8, "unhashedData"    # [Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;
    .param p9, "fingerPrint"    # [B
    .param p10, "signature"    # [Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;

    .prologue
    .line 234
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/bcpg/ContainedPacket;-><init>()V

    .line 235
    iput p1, p0, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->version:I

    .line 236
    iput p2, p0, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->signatureType:I

    .line 237
    iput-wide p3, p0, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->keyID:J

    .line 238
    iput p5, p0, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->keyAlgorithm:I

    .line 239
    iput p6, p0, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->hashAlgorithm:I

    .line 240
    iput-object p7, p0, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->hashedData:[Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;

    .line 241
    iput-object p8, p0, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->unhashedData:[Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;

    .line 242
    iput-object p9, p0, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->fingerPrint:[B

    .line 243
    iput-object p10, p0, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->signature:[Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;

    .line 245
    if-eqz p7, :cond_0

    .line 247
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->setCreationTime()V

    .line 249
    :cond_0
    return-void
.end method

.method public constructor <init>(IJII[Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;[Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;[B[Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;)V
    .locals 12
    .param p1, "signatureType"    # I
    .param p2, "keyID"    # J
    .param p4, "keyAlgorithm"    # I
    .param p5, "hashAlgorithm"    # I
    .param p6, "hashedData"    # [Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;
    .param p7, "unhashedData"    # [Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;
    .param p8, "fingerPrint"    # [B
    .param p9, "signature"    # [Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;

    .prologue
    .line 197
    const/4 v2, 0x4

    move-object v1, p0

    move v3, p1

    move-wide v4, p2

    move/from16 v6, p4

    move/from16 v7, p5

    move-object/from16 v8, p6

    move-object/from16 v9, p7

    move-object/from16 v10, p8

    move-object/from16 v11, p9

    invoke-direct/range {v1 .. v11}, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;-><init>(IIJII[Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;[Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;[B[Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;)V

    .line 198
    return-void
.end method

.method constructor <init>(Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;)V
    .locals 25
    .param p1, "in"    # Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 33
    invoke-direct/range {p0 .. p0}, Lcom/android/sec/org/bouncycastle/bcpg/ContainedPacket;-><init>()V

    .line 34
    invoke-virtual/range {p1 .. p1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->read()I

    move-result v20

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->version:I

    .line 36
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->version:I

    move/from16 v20, v0

    const/16 v21, 0x3

    move/from16 v0, v20

    move/from16 v1, v21

    if-eq v0, v1, :cond_0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->version:I

    move/from16 v20, v0

    const/16 v21, 0x2

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_2

    .line 38
    :cond_0
    invoke-virtual/range {p1 .. p1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->read()I

    move-result v10

    .line 40
    .local v10, "l":I
    invoke-virtual/range {p1 .. p1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->read()I

    move-result v20

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->signatureType:I

    .line 41
    invoke-virtual/range {p1 .. p1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->read()I

    move-result v20

    move/from16 v0, v20

    int-to-long v0, v0

    move-wide/from16 v20, v0

    const/16 v22, 0x18

    shl-long v20, v20, v22

    invoke-virtual/range {p1 .. p1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->read()I

    move-result v22

    shl-int/lit8 v22, v22, 0x10

    move/from16 v0, v22

    int-to-long v0, v0

    move-wide/from16 v22, v0

    or-long v20, v20, v22

    invoke-virtual/range {p1 .. p1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->read()I

    move-result v22

    shl-int/lit8 v22, v22, 0x8

    move/from16 v0, v22

    int-to-long v0, v0

    move-wide/from16 v22, v0

    or-long v20, v20, v22

    invoke-virtual/range {p1 .. p1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->read()I

    move-result v22

    move/from16 v0, v22

    int-to-long v0, v0

    move-wide/from16 v22, v0

    or-long v20, v20, v22

    const-wide/16 v22, 0x3e8

    mul-long v20, v20, v22

    move-wide/from16 v0, v20

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->creationTime:J

    .line 42
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->keyID:J

    move-wide/from16 v20, v0

    invoke-virtual/range {p1 .. p1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->read()I

    move-result v22

    move/from16 v0, v22

    int-to-long v0, v0

    move-wide/from16 v22, v0

    const/16 v24, 0x38

    shl-long v22, v22, v24

    or-long v20, v20, v22

    move-wide/from16 v0, v20

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->keyID:J

    .line 43
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->keyID:J

    move-wide/from16 v20, v0

    invoke-virtual/range {p1 .. p1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->read()I

    move-result v22

    move/from16 v0, v22

    int-to-long v0, v0

    move-wide/from16 v22, v0

    const/16 v24, 0x30

    shl-long v22, v22, v24

    or-long v20, v20, v22

    move-wide/from16 v0, v20

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->keyID:J

    .line 44
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->keyID:J

    move-wide/from16 v20, v0

    invoke-virtual/range {p1 .. p1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->read()I

    move-result v22

    move/from16 v0, v22

    int-to-long v0, v0

    move-wide/from16 v22, v0

    const/16 v24, 0x28

    shl-long v22, v22, v24

    or-long v20, v20, v22

    move-wide/from16 v0, v20

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->keyID:J

    .line 45
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->keyID:J

    move-wide/from16 v20, v0

    invoke-virtual/range {p1 .. p1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->read()I

    move-result v22

    move/from16 v0, v22

    int-to-long v0, v0

    move-wide/from16 v22, v0

    const/16 v24, 0x20

    shl-long v22, v22, v24

    or-long v20, v20, v22

    move-wide/from16 v0, v20

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->keyID:J

    .line 46
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->keyID:J

    move-wide/from16 v20, v0

    invoke-virtual/range {p1 .. p1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->read()I

    move-result v22

    move/from16 v0, v22

    int-to-long v0, v0

    move-wide/from16 v22, v0

    const/16 v24, 0x18

    shl-long v22, v22, v24

    or-long v20, v20, v22

    move-wide/from16 v0, v20

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->keyID:J

    .line 47
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->keyID:J

    move-wide/from16 v20, v0

    invoke-virtual/range {p1 .. p1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->read()I

    move-result v22

    move/from16 v0, v22

    int-to-long v0, v0

    move-wide/from16 v22, v0

    const/16 v24, 0x10

    shl-long v22, v22, v24

    or-long v20, v20, v22

    move-wide/from16 v0, v20

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->keyID:J

    .line 48
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->keyID:J

    move-wide/from16 v20, v0

    invoke-virtual/range {p1 .. p1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->read()I

    move-result v22

    move/from16 v0, v22

    int-to-long v0, v0

    move-wide/from16 v22, v0

    const/16 v24, 0x8

    shl-long v22, v22, v24

    or-long v20, v20, v22

    move-wide/from16 v0, v20

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->keyID:J

    .line 49
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->keyID:J

    move-wide/from16 v20, v0

    invoke-virtual/range {p1 .. p1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->read()I

    move-result v22

    move/from16 v0, v22

    int-to-long v0, v0

    move-wide/from16 v22, v0

    or-long v20, v20, v22

    move-wide/from16 v0, v20

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->keyID:J

    .line 50
    invoke-virtual/range {p1 .. p1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->read()I

    move-result v20

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->keyAlgorithm:I

    .line 51
    invoke-virtual/range {p1 .. p1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->read()I

    move-result v20

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->hashAlgorithm:I

    .line 126
    .end local v10    # "l":I
    :cond_1
    const/16 v20, 0x2

    move/from16 v0, v20

    new-array v0, v0, [B

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->fingerPrint:[B

    .line 127
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->fingerPrint:[B

    move-object/from16 v20, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->readFully([B)V

    .line 129
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->keyAlgorithm:I

    move/from16 v20, v0

    sparse-switch v20, :sswitch_data_0

    .line 158
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->keyAlgorithm:I

    move/from16 v20, v0

    const/16 v21, 0x64

    move/from16 v0, v20

    move/from16 v1, v21

    if-lt v0, v1, :cond_b

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->keyAlgorithm:I

    move/from16 v20, v0

    const/16 v21, 0x6e

    move/from16 v0, v20

    move/from16 v1, v21

    if-gt v0, v1, :cond_b

    .line 160
    const/16 v20, 0x0

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->signature:[Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;

    .line 161
    new-instance v4, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v4}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 163
    .local v4, "bOut":Ljava/io/ByteArrayOutputStream;
    :goto_0
    invoke-virtual/range {p1 .. p1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->read()I

    move-result v5

    .local v5, "ch":I
    if-ltz v5, :cond_a

    .line 165
    invoke-virtual {v4, v5}, Ljava/io/ByteArrayOutputStream;->write(I)V

    goto :goto_0

    .line 53
    .end local v4    # "bOut":Ljava/io/ByteArrayOutputStream;
    .end local v5    # "ch":I
    :cond_2
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->version:I

    move/from16 v20, v0

    const/16 v21, 0x4

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_9

    .line 55
    invoke-virtual/range {p1 .. p1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->read()I

    move-result v20

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->signatureType:I

    .line 56
    invoke-virtual/range {p1 .. p1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->read()I

    move-result v20

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->keyAlgorithm:I

    .line 57
    invoke-virtual/range {p1 .. p1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->read()I

    move-result v20

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->hashAlgorithm:I

    .line 59
    invoke-virtual/range {p1 .. p1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->read()I

    move-result v20

    shl-int/lit8 v20, v20, 0x8

    invoke-virtual/range {p1 .. p1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->read()I

    move-result v21

    or-int v8, v20, v21

    .line 60
    .local v8, "hashedLength":I
    new-array v7, v8, [B

    .line 62
    .local v7, "hashed":[B
    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->readFully([B)V

    .line 68
    new-instance v14, Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacketInputStream;

    new-instance v20, Ljava/io/ByteArrayInputStream;

    move-object/from16 v0, v20

    invoke-direct {v0, v7}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    move-object/from16 v0, v20

    invoke-direct {v14, v0}, Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacketInputStream;-><init>(Ljava/io/InputStream;)V

    .line 71
    .local v14, "sIn":Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacketInputStream;
    new-instance v18, Ljava/util/Vector;

    invoke-direct/range {v18 .. v18}, Ljava/util/Vector;-><init>()V

    .line 72
    .local v18, "v":Ljava/util/Vector;
    :goto_1
    invoke-virtual {v14}, Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacketInputStream;->readPacket()Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;

    move-result-object v15

    .local v15, "sub":Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;
    if-eqz v15, :cond_3

    .line 74
    move-object/from16 v0, v18

    invoke-virtual {v0, v15}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    goto :goto_1

    .line 77
    :cond_3
    invoke-virtual/range {v18 .. v18}, Ljava/util/Vector;->size()I

    move-result v20

    move/from16 v0, v20

    new-array v0, v0, [Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->hashedData:[Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;

    .line 79
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->hashedData:[Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    array-length v0, v0

    move/from16 v20, v0

    move/from16 v0, v20

    if-eq v9, v0, :cond_6

    .line 81
    move-object/from16 v0, v18

    invoke-virtual {v0, v9}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;

    .line 82
    .local v11, "p":Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;
    instance-of v0, v11, Lcom/android/sec/org/bouncycastle/bcpg/sig/IssuerKeyID;

    move/from16 v20, v0

    if-eqz v20, :cond_5

    move-object/from16 v20, v11

    .line 84
    check-cast v20, Lcom/android/sec/org/bouncycastle/bcpg/sig/IssuerKeyID;

    invoke-virtual/range {v20 .. v20}, Lcom/android/sec/org/bouncycastle/bcpg/sig/IssuerKeyID;->getKeyID()J

    move-result-wide v20

    move-wide/from16 v0, v20

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->keyID:J

    .line 91
    :cond_4
    :goto_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->hashedData:[Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;

    move-object/from16 v20, v0

    aput-object v11, v20, v9

    .line 79
    add-int/lit8 v9, v9, 0x1

    goto :goto_2

    .line 86
    :cond_5
    instance-of v0, v11, Lcom/android/sec/org/bouncycastle/bcpg/sig/SignatureCreationTime;

    move/from16 v20, v0

    if-eqz v20, :cond_4

    move-object/from16 v20, v11

    .line 88
    check-cast v20, Lcom/android/sec/org/bouncycastle/bcpg/sig/SignatureCreationTime;

    invoke-virtual/range {v20 .. v20}, Lcom/android/sec/org/bouncycastle/bcpg/sig/SignatureCreationTime;->getTime()Ljava/util/Date;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/util/Date;->getTime()J

    move-result-wide v20

    move-wide/from16 v0, v20

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->creationTime:J

    goto :goto_3

    .line 94
    .end local v11    # "p":Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;
    :cond_6
    invoke-virtual/range {p1 .. p1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->read()I

    move-result v20

    shl-int/lit8 v20, v20, 0x8

    invoke-virtual/range {p1 .. p1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->read()I

    move-result v21

    or-int v17, v20, v21

    .line 95
    .local v17, "unhashedLength":I
    move/from16 v0, v17

    new-array v0, v0, [B

    move-object/from16 v16, v0

    .line 97
    .local v16, "unhashed":[B
    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->readFully([B)V

    .line 99
    new-instance v14, Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacketInputStream;

    .end local v14    # "sIn":Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacketInputStream;
    new-instance v20, Ljava/io/ByteArrayInputStream;

    move-object/from16 v0, v20

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    move-object/from16 v0, v20

    invoke-direct {v14, v0}, Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacketInputStream;-><init>(Ljava/io/InputStream;)V

    .line 102
    .restart local v14    # "sIn":Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacketInputStream;
    invoke-virtual/range {v18 .. v18}, Ljava/util/Vector;->removeAllElements()V

    .line 103
    :goto_4
    invoke-virtual {v14}, Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacketInputStream;->readPacket()Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;

    move-result-object v15

    if-eqz v15, :cond_7

    .line 105
    move-object/from16 v0, v18

    invoke-virtual {v0, v15}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    goto :goto_4

    .line 108
    :cond_7
    invoke-virtual/range {v18 .. v18}, Ljava/util/Vector;->size()I

    move-result v20

    move/from16 v0, v20

    new-array v0, v0, [Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->unhashedData:[Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;

    .line 110
    const/4 v9, 0x0

    :goto_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->unhashedData:[Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    array-length v0, v0

    move/from16 v20, v0

    move/from16 v0, v20

    if-eq v9, v0, :cond_1

    .line 112
    move-object/from16 v0, v18

    invoke-virtual {v0, v9}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;

    .line 113
    .restart local v11    # "p":Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;
    instance-of v0, v11, Lcom/android/sec/org/bouncycastle/bcpg/sig/IssuerKeyID;

    move/from16 v20, v0

    if-eqz v20, :cond_8

    move-object/from16 v20, v11

    .line 115
    check-cast v20, Lcom/android/sec/org/bouncycastle/bcpg/sig/IssuerKeyID;

    invoke-virtual/range {v20 .. v20}, Lcom/android/sec/org/bouncycastle/bcpg/sig/IssuerKeyID;->getKeyID()J

    move-result-wide v20

    move-wide/from16 v0, v20

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->keyID:J

    .line 118
    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->unhashedData:[Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;

    move-object/from16 v20, v0

    aput-object v11, v20, v9

    .line 110
    add-int/lit8 v9, v9, 0x1

    goto :goto_5

    .line 123
    .end local v7    # "hashed":[B
    .end local v8    # "hashedLength":I
    .end local v9    # "i":I
    .end local v11    # "p":Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;
    .end local v14    # "sIn":Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacketInputStream;
    .end local v15    # "sub":Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;
    .end local v16    # "unhashed":[B
    .end local v17    # "unhashedLength":I
    .end local v18    # "v":Ljava/util/Vector;
    :cond_9
    new-instance v20, Ljava/lang/RuntimeException;

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "unsupported version: "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->version:I

    move/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-direct/range {v20 .. v21}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v20

    .line 133
    :sswitch_0
    new-instance v18, Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;

    move-object/from16 v0, v18

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;-><init>(Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;)V

    .line 135
    .local v18, "v":Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;
    const/16 v20, 0x1

    move/from16 v0, v20

    new-array v0, v0, [Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->signature:[Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;

    .line 136
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->signature:[Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    aput-object v18, v20, v21

    .line 174
    .end local v18    # "v":Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;
    :goto_6
    return-void

    .line 139
    :sswitch_1
    new-instance v12, Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;

    move-object/from16 v0, p1

    invoke-direct {v12, v0}, Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;-><init>(Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;)V

    .line 140
    .local v12, "r":Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;
    new-instance v13, Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;

    move-object/from16 v0, p1

    invoke-direct {v13, v0}, Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;-><init>(Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;)V

    .line 142
    .local v13, "s":Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;
    const/16 v20, 0x2

    move/from16 v0, v20

    new-array v0, v0, [Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->signature:[Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;

    .line 143
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->signature:[Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    aput-object v12, v20, v21

    .line 144
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->signature:[Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;

    move-object/from16 v20, v0

    const/16 v21, 0x1

    aput-object v13, v20, v21

    goto :goto_6

    .line 148
    .end local v12    # "r":Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;
    .end local v13    # "s":Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;
    :sswitch_2
    new-instance v11, Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;

    move-object/from16 v0, p1

    invoke-direct {v11, v0}, Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;-><init>(Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;)V

    .line 149
    .local v11, "p":Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;
    new-instance v6, Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;

    move-object/from16 v0, p1

    invoke-direct {v6, v0}, Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;-><init>(Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;)V

    .line 150
    .local v6, "g":Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;
    new-instance v19, Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;-><init>(Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;)V

    .line 152
    .local v19, "y":Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;
    const/16 v20, 0x3

    move/from16 v0, v20

    new-array v0, v0, [Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->signature:[Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;

    .line 153
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->signature:[Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    aput-object v11, v20, v21

    .line 154
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->signature:[Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;

    move-object/from16 v20, v0

    const/16 v21, 0x1

    aput-object v6, v20, v21

    .line 155
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->signature:[Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;

    move-object/from16 v20, v0

    const/16 v21, 0x2

    aput-object v19, v20, v21

    goto :goto_6

    .line 167
    .end local v6    # "g":Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;
    .end local v11    # "p":Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;
    .end local v19    # "y":Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;
    .restart local v4    # "bOut":Ljava/io/ByteArrayOutputStream;
    .restart local v5    # "ch":I
    :cond_a
    invoke-virtual {v4}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->signatureEncoding:[B

    goto :goto_6

    .line 171
    .end local v4    # "bOut":Ljava/io/ByteArrayOutputStream;
    .end local v5    # "ch":I
    :cond_b
    new-instance v20, Ljava/io/IOException;

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "unknown signature key algorithm: "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->keyAlgorithm:I

    move/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-direct/range {v20 .. v21}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v20

    .line 129
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x3 -> :sswitch_0
        0x10 -> :sswitch_2
        0x11 -> :sswitch_1
        0x14 -> :sswitch_2
    .end sparse-switch
.end method

.method private setCreationTime()V
    .locals 4

    .prologue
    .line 506
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->hashedData:[Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;

    array-length v1, v1

    if-eq v0, v1, :cond_0

    .line 508
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->hashedData:[Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;

    aget-object v1, v1, v0

    instance-of v1, v1, Lcom/android/sec/org/bouncycastle/bcpg/sig/SignatureCreationTime;

    if-eqz v1, :cond_1

    .line 510
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->hashedData:[Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;

    aget-object v1, v1, v0

    check-cast v1, Lcom/android/sec/org/bouncycastle/bcpg/sig/SignatureCreationTime;

    invoke-virtual {v1}, Lcom/android/sec/org/bouncycastle/bcpg/sig/SignatureCreationTime;->getTime()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->creationTime:J

    .line 514
    :cond_0
    return-void

    .line 506
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public encode(Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;)V
    .locals 12
    .param p1, "out"    # Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 421
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 422
    .local v0, "bOut":Ljava/io/ByteArrayOutputStream;
    new-instance v3, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;

    invoke-direct {v3, v0}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 424
    .local v3, "pOut":Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;
    iget v5, p0, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->version:I

    invoke-virtual {v3, v5}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->write(I)V

    .line 426
    iget v5, p0, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->version:I

    const/4 v8, 0x3

    if-eq v5, v8, :cond_0

    iget v5, p0, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->version:I

    const/4 v8, 0x2

    if-ne v5, v8, :cond_1

    .line 428
    :cond_0
    const/4 v5, 0x5

    invoke-virtual {v3, v5}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->write(I)V

    .line 430
    iget-wide v8, p0, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->creationTime:J

    const-wide/16 v10, 0x3e8

    div-long v6, v8, v10

    .line 432
    .local v6, "time":J
    iget v5, p0, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->signatureType:I

    invoke-virtual {v3, v5}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->write(I)V

    .line 433
    const/16 v5, 0x18

    shr-long v8, v6, v5

    long-to-int v5, v8

    int-to-byte v5, v5

    invoke-virtual {v3, v5}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->write(I)V

    .line 434
    const/16 v5, 0x10

    shr-long v8, v6, v5

    long-to-int v5, v8

    int-to-byte v5, v5

    invoke-virtual {v3, v5}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->write(I)V

    .line 435
    const/16 v5, 0x8

    shr-long v8, v6, v5

    long-to-int v5, v8

    int-to-byte v5, v5

    invoke-virtual {v3, v5}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->write(I)V

    .line 436
    long-to-int v5, v6

    int-to-byte v5, v5

    invoke-virtual {v3, v5}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->write(I)V

    .line 438
    iget-wide v8, p0, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->keyID:J

    const/16 v5, 0x38

    shr-long/2addr v8, v5

    long-to-int v5, v8

    int-to-byte v5, v5

    invoke-virtual {v3, v5}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->write(I)V

    .line 439
    iget-wide v8, p0, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->keyID:J

    const/16 v5, 0x30

    shr-long/2addr v8, v5

    long-to-int v5, v8

    int-to-byte v5, v5

    invoke-virtual {v3, v5}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->write(I)V

    .line 440
    iget-wide v8, p0, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->keyID:J

    const/16 v5, 0x28

    shr-long/2addr v8, v5

    long-to-int v5, v8

    int-to-byte v5, v5

    invoke-virtual {v3, v5}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->write(I)V

    .line 441
    iget-wide v8, p0, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->keyID:J

    const/16 v5, 0x20

    shr-long/2addr v8, v5

    long-to-int v5, v8

    int-to-byte v5, v5

    invoke-virtual {v3, v5}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->write(I)V

    .line 442
    iget-wide v8, p0, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->keyID:J

    const/16 v5, 0x18

    shr-long/2addr v8, v5

    long-to-int v5, v8

    int-to-byte v5, v5

    invoke-virtual {v3, v5}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->write(I)V

    .line 443
    iget-wide v8, p0, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->keyID:J

    const/16 v5, 0x10

    shr-long/2addr v8, v5

    long-to-int v5, v8

    int-to-byte v5, v5

    invoke-virtual {v3, v5}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->write(I)V

    .line 444
    iget-wide v8, p0, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->keyID:J

    const/16 v5, 0x8

    shr-long/2addr v8, v5

    long-to-int v5, v8

    int-to-byte v5, v5

    invoke-virtual {v3, v5}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->write(I)V

    .line 445
    iget-wide v8, p0, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->keyID:J

    long-to-int v5, v8

    int-to-byte v5, v5

    invoke-virtual {v3, v5}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->write(I)V

    .line 447
    iget v5, p0, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->keyAlgorithm:I

    invoke-virtual {v3, v5}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->write(I)V

    .line 448
    iget v5, p0, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->hashAlgorithm:I

    invoke-virtual {v3, v5}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->write(I)V

    .line 487
    .end local v6    # "time":J
    :goto_0
    iget-object v5, p0, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->fingerPrint:[B

    invoke-virtual {v3, v5}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->write([B)V

    .line 489
    iget-object v5, p0, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->signature:[Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;

    if-eqz v5, :cond_5

    .line 491
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    iget-object v5, p0, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->signature:[Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;

    array-length v5, v5

    if-eq v2, v5, :cond_6

    .line 493
    iget-object v5, p0, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->signature:[Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;

    aget-object v5, v5, v2

    invoke-virtual {v3, v5}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->writeObject(Lcom/android/sec/org/bouncycastle/bcpg/BCPGObject;)V

    .line 491
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 450
    .end local v2    # "i":I
    :cond_1
    iget v5, p0, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->version:I

    const/4 v8, 0x4

    if-ne v5, v8, :cond_4

    .line 452
    iget v5, p0, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->signatureType:I

    invoke-virtual {v3, v5}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->write(I)V

    .line 453
    iget v5, p0, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->keyAlgorithm:I

    invoke-virtual {v3, v5}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->write(I)V

    .line 454
    iget v5, p0, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->hashAlgorithm:I

    invoke-virtual {v3, v5}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->write(I)V

    .line 456
    new-instance v4, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v4}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 458
    .local v4, "sOut":Ljava/io/ByteArrayOutputStream;
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_2
    iget-object v5, p0, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->hashedData:[Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;

    array-length v5, v5

    if-eq v2, v5, :cond_2

    .line 460
    iget-object v5, p0, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->hashedData:[Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;

    aget-object v5, v5, v2

    invoke-virtual {v5, v4}, Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;->encode(Ljava/io/OutputStream;)V

    .line 458
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 463
    :cond_2
    invoke-virtual {v4}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    .line 465
    .local v1, "data":[B
    array-length v5, v1

    shr-int/lit8 v5, v5, 0x8

    invoke-virtual {v3, v5}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->write(I)V

    .line 466
    array-length v5, v1

    invoke-virtual {v3, v5}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->write(I)V

    .line 467
    invoke-virtual {v3, v1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->write([B)V

    .line 469
    invoke-virtual {v4}, Ljava/io/ByteArrayOutputStream;->reset()V

    .line 471
    const/4 v2, 0x0

    :goto_3
    iget-object v5, p0, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->unhashedData:[Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;

    array-length v5, v5

    if-eq v2, v5, :cond_3

    .line 473
    iget-object v5, p0, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->unhashedData:[Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;

    aget-object v5, v5, v2

    invoke-virtual {v5, v4}, Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;->encode(Ljava/io/OutputStream;)V

    .line 471
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 476
    :cond_3
    invoke-virtual {v4}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    .line 478
    array-length v5, v1

    shr-int/lit8 v5, v5, 0x8

    invoke-virtual {v3, v5}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->write(I)V

    .line 479
    array-length v5, v1

    invoke-virtual {v3, v5}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->write(I)V

    .line 480
    invoke-virtual {v3, v1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->write([B)V

    goto :goto_0

    .line 484
    .end local v1    # "data":[B
    .end local v2    # "i":I
    .end local v4    # "sOut":Ljava/io/ByteArrayOutputStream;
    :cond_4
    new-instance v5, Ljava/io/IOException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "unknown version: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, p0, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->version:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v5, v8}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 498
    :cond_5
    iget-object v5, p0, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->signatureEncoding:[B

    invoke-virtual {v3, v5}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->write([B)V

    .line 501
    :cond_6
    const/4 v5, 0x2

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v8

    const/4 v9, 0x1

    invoke-virtual {p1, v5, v8, v9}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->writePacket(I[BZ)V

    .line 502
    return-void
.end method

.method public getCreationTime()J
    .locals 2

    .prologue
    .line 414
    iget-wide v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->creationTime:J

    return-wide v0
.end method

.method public getHashAlgorithm()I
    .locals 1

    .prologue
    .line 356
    iget v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->hashAlgorithm:I

    return v0
.end method

.method public getHashedSubPackets()[Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;
    .locals 1

    .prologue
    .line 399
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->hashedData:[Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;

    return-object v0
.end method

.method public getKeyAlgorithm()I
    .locals 1

    .prologue
    .line 348
    iget v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->keyAlgorithm:I

    return v0
.end method

.method public getKeyID()J
    .locals 2

    .prologue
    .line 273
    iget-wide v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->keyID:J

    return-wide v0
.end method

.method public getSignature()[Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;
    .locals 1

    .prologue
    .line 365
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->signature:[Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;

    return-object v0
.end method

.method public getSignatureBytes()[B
    .locals 7

    .prologue
    .line 374
    iget-object v4, p0, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->signatureEncoding:[B

    if-nez v4, :cond_1

    .line 376
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 377
    .local v0, "bOut":Ljava/io/ByteArrayOutputStream;
    new-instance v1, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;

    invoke-direct {v1, v0}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 379
    .local v1, "bcOut":Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    iget-object v4, p0, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->signature:[Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;

    array-length v4, v4

    if-eq v3, v4, :cond_0

    .line 383
    :try_start_0
    iget-object v4, p0, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->signature:[Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;

    aget-object v4, v4, v3

    invoke-virtual {v1, v4}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->writeObject(Lcom/android/sec/org/bouncycastle/bcpg/BCPGObject;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 379
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 385
    :catch_0
    move-exception v2

    .line 387
    .local v2, "e":Ljava/io/IOException;
    new-instance v4, Ljava/lang/RuntimeException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "internal error: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 390
    .end local v2    # "e":Ljava/io/IOException;
    :cond_0
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v4

    .line 394
    .end local v0    # "bOut":Ljava/io/ByteArrayOutputStream;
    .end local v1    # "bcOut":Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;
    .end local v3    # "i":I
    :goto_1
    return-object v4

    :cond_1
    iget-object v4, p0, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->signatureEncoding:[B

    invoke-static {v4}, Lcom/android/sec/org/bouncycastle/util/Arrays;->clone([B)[B

    move-result-object v4

    goto :goto_1
.end method

.method public getSignatureTrailer()[B
    .locals 14

    .prologue
    .line 284
    const/4 v7, 0x0

    .line 286
    .local v7, "trailer":[B
    iget v10, p0, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->version:I

    const/4 v11, 0x3

    if-eq v10, v11, :cond_0

    iget v10, p0, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->version:I

    const/4 v11, 0x2

    if-ne v10, v11, :cond_1

    .line 288
    :cond_0
    const/4 v10, 0x5

    new-array v7, v10, [B

    .line 290
    iget-wide v10, p0, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->creationTime:J

    const-wide/16 v12, 0x3e8

    div-long v8, v10, v12

    .line 292
    .local v8, "time":J
    const/4 v10, 0x0

    iget v11, p0, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->signatureType:I

    int-to-byte v11, v11

    aput-byte v11, v7, v10

    .line 293
    const/4 v10, 0x1

    const/16 v11, 0x18

    shr-long v12, v8, v11

    long-to-int v11, v12

    int-to-byte v11, v11

    aput-byte v11, v7, v10

    .line 294
    const/4 v10, 0x2

    const/16 v11, 0x10

    shr-long v12, v8, v11

    long-to-int v11, v12

    int-to-byte v11, v11

    aput-byte v11, v7, v10

    .line 295
    const/4 v10, 0x3

    const/16 v11, 0x8

    shr-long v12, v8, v11

    long-to-int v11, v12

    int-to-byte v11, v11

    aput-byte v11, v7, v10

    .line 296
    const/4 v10, 0x4

    long-to-int v11, v8

    int-to-byte v11, v11

    aput-byte v11, v7, v10

    .line 340
    .end local v8    # "time":J
    :goto_0
    return-object v7

    .line 300
    :cond_1
    new-instance v6, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v6}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 304
    .local v6, "sOut":Ljava/io/ByteArrayOutputStream;
    :try_start_0
    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->getVersion()I

    move-result v10

    int-to-byte v10, v10

    invoke-virtual {v6, v10}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 305
    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->getSignatureType()I

    move-result v10

    int-to-byte v10, v10

    invoke-virtual {v6, v10}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 306
    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->getKeyAlgorithm()I

    move-result v10

    int-to-byte v10, v10

    invoke-virtual {v6, v10}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 307
    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->getHashAlgorithm()I

    move-result v10

    int-to-byte v10, v10

    invoke-virtual {v6, v10}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 309
    new-instance v3, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v3}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 310
    .local v3, "hOut":Ljava/io/ByteArrayOutputStream;
    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->getHashedSubPackets()[Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;

    move-result-object v4

    .line 312
    .local v4, "hashed":[Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_1
    array-length v10, v4

    if-eq v5, v10, :cond_2

    .line 314
    aget-object v10, v4, v5

    invoke-virtual {v10, v3}, Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;->encode(Ljava/io/OutputStream;)V

    .line 312
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 317
    :cond_2
    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    .line 319
    .local v0, "data":[B
    array-length v10, v0

    shr-int/lit8 v10, v10, 0x8

    int-to-byte v10, v10

    invoke-virtual {v6, v10}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 320
    array-length v10, v0

    int-to-byte v10, v10

    invoke-virtual {v6, v10}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 321
    invoke-virtual {v6, v0}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 323
    invoke-virtual {v6}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v2

    .line 325
    .local v2, "hData":[B
    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->getVersion()I

    move-result v10

    int-to-byte v10, v10

    invoke-virtual {v6, v10}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 326
    const/4 v10, -0x1

    invoke-virtual {v6, v10}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 327
    array-length v10, v2

    shr-int/lit8 v10, v10, 0x18

    int-to-byte v10, v10

    invoke-virtual {v6, v10}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 328
    array-length v10, v2

    shr-int/lit8 v10, v10, 0x10

    int-to-byte v10, v10

    invoke-virtual {v6, v10}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 329
    array-length v10, v2

    shr-int/lit8 v10, v10, 0x8

    int-to-byte v10, v10

    invoke-virtual {v6, v10}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 330
    array-length v10, v2

    int-to-byte v10, v10

    invoke-virtual {v6, v10}, Ljava/io/ByteArrayOutputStream;->write(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 337
    invoke-virtual {v6}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v7

    goto :goto_0

    .line 332
    .end local v0    # "data":[B
    .end local v2    # "hData":[B
    .end local v3    # "hOut":Ljava/io/ByteArrayOutputStream;
    .end local v4    # "hashed":[Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;
    .end local v5    # "i":I
    :catch_0
    move-exception v1

    .line 334
    .local v1, "e":Ljava/io/IOException;
    new-instance v10, Ljava/lang/RuntimeException;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "exception generating trailer: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v10
.end method

.method public getSignatureType()I
    .locals 1

    .prologue
    .line 264
    iget v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->signatureType:I

    return v0
.end method

.method public getUnhashedSubPackets()[Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;
    .locals 1

    .prologue
    .line 404
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->unhashedData:[Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;

    return-object v0
.end method

.method public getVersion()I
    .locals 1

    .prologue
    .line 256
    iget v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->version:I

    return v0
.end method
