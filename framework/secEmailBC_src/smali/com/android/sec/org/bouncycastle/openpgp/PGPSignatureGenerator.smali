.class public Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;
.super Ljava/lang/Object;
.source "PGPSignatureGenerator.java"


# instance fields
.field private dig:Ljava/security/MessageDigest;

.field private hashAlgorithm:I

.field hashed:[Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;

.field private keyAlgorithm:I

.field private lastb:B

.field private privKey:Lcom/android/sec/org/bouncycastle/openpgp/PGPPrivateKey;

.field private sig:Ljava/security/Signature;

.field private signatureType:I

.field unhashed:[Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;


# direct methods
.method public constructor <init>(IILjava/lang/String;)V
    .locals 0
    .param p1, "keyAlgorithm"    # I
    .param p2, "hashAlgorithm"    # I
    .param p3, "provider"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/NoSuchAlgorithmException;,
            Ljava/security/NoSuchProviderException;,
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
        }
    .end annotation

    .prologue
    .line 47
    invoke-direct {p0, p1, p3, p2, p3}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;-><init>(ILjava/lang/String;ILjava/lang/String;)V

    .line 48
    return-void
.end method

.method public constructor <init>(IILjava/security/Provider;)V
    .locals 0
    .param p1, "keyAlgorithm"    # I
    .param p2, "hashAlgorithm"    # I
    .param p3, "provider"    # Ljava/security/Provider;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/NoSuchAlgorithmException;,
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
        }
    .end annotation

    .prologue
    .line 56
    invoke-direct {p0, p1, p3, p2, p3}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;-><init>(ILjava/security/Provider;ILjava/security/Provider;)V

    .line 57
    return-void
.end method

.method public constructor <init>(ILjava/lang/String;ILjava/lang/String;)V
    .locals 2
    .param p1, "keyAlgorithm"    # I
    .param p2, "sigProvider"    # Ljava/lang/String;
    .param p3, "hashAlgorithm"    # I
    .param p4, "digProvider"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/NoSuchAlgorithmException;,
            Ljava/security/NoSuchProviderException;,
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
        }
    .end annotation

    .prologue
    .line 77
    invoke-static {p2}, Lcom/android/sec/org/bouncycastle/openpgp/PGPUtil;->getProvider(Ljava/lang/String;)Ljava/security/Provider;

    move-result-object v0

    invoke-static {p4}, Lcom/android/sec/org/bouncycastle/openpgp/PGPUtil;->getProvider(Ljava/lang/String;)Ljava/security/Provider;

    move-result-object v1

    invoke-direct {p0, p1, v0, p3, v1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;-><init>(ILjava/security/Provider;ILjava/security/Provider;)V

    .line 78
    return-void
.end method

.method public constructor <init>(ILjava/security/Provider;ILjava/security/Provider;)V
    .locals 2
    .param p1, "keyAlgorithm"    # I
    .param p2, "sigProvider"    # Ljava/security/Provider;
    .param p3, "hashAlgorithm"    # I
    .param p4, "digProvider"    # Ljava/security/Provider;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/NoSuchAlgorithmException;,
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 86
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    new-array v0, v1, [Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;->unhashed:[Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;

    .line 29
    new-array v0, v1, [Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;->hashed:[Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;

    .line 87
    iput p1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;->keyAlgorithm:I

    .line 88
    iput p3, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;->hashAlgorithm:I

    .line 90
    invoke-static {p3}, Lcom/android/sec/org/bouncycastle/openpgp/PGPUtil;->getDigestName(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p4}, Lcom/android/sec/org/bouncycastle/openpgp/PGPUtil;->getDigestInstance(Ljava/lang/String;Ljava/security/Provider;)Ljava/security/MessageDigest;

    move-result-object v0

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;->dig:Ljava/security/MessageDigest;

    .line 91
    invoke-static {p1, p3}, Lcom/android/sec/org/bouncycastle/openpgp/PGPUtil;->getSignatureName(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p2}, Ljava/security/Signature;->getInstance(Ljava/lang/String;Ljava/security/Provider;)Ljava/security/Signature;

    move-result-object v0

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;->sig:Ljava/security/Signature;

    .line 92
    return-void
.end method

.method private getEncodedPublicKey(Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;)[B
    .locals 4
    .param p1, "pubKey"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
        }
    .end annotation

    .prologue
    .line 449
    :try_start_0
    iget-object v2, p1, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->publicPk:Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;

    invoke-virtual {v2}, Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;->getEncodedContents()[B
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 456
    .local v1, "keyBytes":[B
    return-object v1

    .line 451
    .end local v1    # "keyBytes":[B
    :catch_0
    move-exception v0

    .line 453
    .local v0, "e":Ljava/io/IOException;
    new-instance v2, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;

    const-string v3, "exception preparing key."

    invoke-direct {v2, v3, v0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v2
.end method

.method private insertSubpacket([Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;)[Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;
    .locals 4
    .param p1, "packets"    # [Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;
    .param p2, "subpacket"    # Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;

    .prologue
    const/4 v3, 0x0

    .line 478
    array-length v1, p1

    add-int/lit8 v1, v1, 0x1

    new-array v0, v1, [Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;

    .line 480
    .local v0, "tmp":[Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;
    aput-object p2, v0, v3

    .line 481
    const/4 v1, 0x1

    array-length v2, p1

    invoke-static {p1, v3, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 483
    return-object v0
.end method

.method private packetPresent([Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;I)Z
    .locals 2
    .param p1, "packets"    # [Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;
    .param p2, "type"    # I

    .prologue
    .line 463
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v1, p1

    if-eq v0, v1, :cond_1

    .line 465
    aget-object v1, p1, v0

    invoke-virtual {v1}, Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;->getType()I

    move-result v1

    if-ne v1, p2, :cond_0

    .line 467
    const/4 v1, 0x1

    .line 471
    :goto_1
    return v1

    .line 463
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 471
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method private updateWithIdData(I[B)V
    .locals 1
    .param p1, "header"    # I
    .param p2, "idBytes"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/SignatureException;
        }
    .end annotation

    .prologue
    .line 489
    int-to-byte v0, p1

    invoke-virtual {p0, v0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;->update(B)V

    .line 490
    array-length v0, p2

    shr-int/lit8 v0, v0, 0x18

    int-to-byte v0, v0

    invoke-virtual {p0, v0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;->update(B)V

    .line 491
    array-length v0, p2

    shr-int/lit8 v0, v0, 0x10

    int-to-byte v0, v0

    invoke-virtual {p0, v0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;->update(B)V

    .line 492
    array-length v0, p2

    shr-int/lit8 v0, v0, 0x8

    int-to-byte v0, v0

    invoke-virtual {p0, v0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;->update(B)V

    .line 493
    array-length v0, p2

    int-to-byte v0, v0

    invoke-virtual {p0, v0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;->update(B)V

    .line 494
    invoke-virtual {p0, p2}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;->update([B)V

    .line 495
    return-void
.end method

.method private updateWithPublicKey(Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;)V
    .locals 2
    .param p1, "key"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;,
            Ljava/security/SignatureException;
        }
    .end annotation

    .prologue
    .line 500
    invoke-direct {p0, p1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;->getEncodedPublicKey(Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;)[B

    move-result-object v0

    .line 502
    .local v0, "keyBytes":[B
    const/16 v1, -0x67

    invoke-virtual {p0, v1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;->update(B)V

    .line 503
    array-length v1, v0

    shr-int/lit8 v1, v1, 0x8

    int-to-byte v1, v1

    invoke-virtual {p0, v1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;->update(B)V

    .line 504
    array-length v1, v0

    int-to-byte v1, v1

    invoke-virtual {p0, v1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;->update(B)V

    .line 505
    invoke-virtual {p0, v0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;->update([B)V

    .line 506
    return-void
.end method


# virtual methods
.method public generate()Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;
    .locals 22
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;,
            Ljava/security/SignatureException;
        }
    .end annotation

    .prologue
    .line 262
    const/16 v20, 0x4

    .line 263
    .local v20, "version":I
    new-instance v18, Ljava/io/ByteArrayOutputStream;

    invoke-direct/range {v18 .. v18}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 266
    .local v18, "sOut":Ljava/io/ByteArrayOutputStream;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;->hashed:[Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;

    const/4 v3, 0x2

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;->packetPresent([Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 268
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;->hashed:[Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;

    new-instance v3, Lcom/android/sec/org/bouncycastle/bcpg/sig/SignatureCreationTime;

    const/4 v4, 0x0

    new-instance v5, Ljava/util/Date;

    invoke-direct {v5}, Ljava/util/Date;-><init>()V

    invoke-direct {v3, v4, v5}, Lcom/android/sec/org/bouncycastle/bcpg/sig/SignatureCreationTime;-><init>(ZLjava/util/Date;)V

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;->insertSubpacket([Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;)[Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;

    move-result-object v8

    .line 275
    .local v8, "hPkts":[Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;
    :goto_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;->hashed:[Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;

    const/16 v3, 0x10

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;->packetPresent([Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;I)Z

    move-result v2

    if-nez v2, :cond_1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;->unhashed:[Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;

    const/16 v3, 0x10

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;->packetPresent([Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;I)Z

    move-result v2

    if-nez v2, :cond_1

    .line 277
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;->unhashed:[Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;

    new-instance v3, Lcom/android/sec/org/bouncycastle/bcpg/sig/IssuerKeyID;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;->privKey:Lcom/android/sec/org/bouncycastle/openpgp/PGPPrivateKey;

    invoke-virtual {v5}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPrivateKey;->getKeyID()J

    move-result-wide v6

    invoke-direct {v3, v4, v6, v7}, Lcom/android/sec/org/bouncycastle/bcpg/sig/IssuerKeyID;-><init>(ZJ)V

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;->insertSubpacket([Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;)[Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;

    move-result-object v9

    .line 286
    .local v9, "unhPkts":[Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;
    :goto_1
    move/from16 v0, v20

    int-to-byte v2, v0

    :try_start_0
    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 287
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;->signatureType:I

    int-to-byte v2, v2

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 288
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;->keyAlgorithm:I

    int-to-byte v2, v2

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 289
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;->hashAlgorithm:I

    int-to-byte v2, v2

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 291
    new-instance v16, Ljava/io/ByteArrayOutputStream;

    invoke-direct/range {v16 .. v16}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 293
    .local v16, "hOut":Ljava/io/ByteArrayOutputStream;
    const/16 v17, 0x0

    .local v17, "i":I
    :goto_2
    array-length v2, v8

    move/from16 v0, v17

    if-eq v0, v2, :cond_2

    .line 295
    aget-object v2, v8, v17

    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;->encode(Ljava/io/OutputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 293
    add-int/lit8 v17, v17, 0x1

    goto :goto_2

    .line 272
    .end local v8    # "hPkts":[Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;
    .end local v9    # "unhPkts":[Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;
    .end local v16    # "hOut":Ljava/io/ByteArrayOutputStream;
    .end local v17    # "i":I
    :cond_0
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;->hashed:[Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;

    .restart local v8    # "hPkts":[Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;
    goto :goto_0

    .line 281
    :cond_1
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;->unhashed:[Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;

    .restart local v9    # "unhPkts":[Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;
    goto :goto_1

    .line 298
    .restart local v16    # "hOut":Ljava/io/ByteArrayOutputStream;
    .restart local v17    # "i":I
    :cond_2
    :try_start_1
    invoke-virtual/range {v16 .. v16}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v12

    .line 300
    .local v12, "data":[B
    array-length v2, v12

    shr-int/lit8 v2, v2, 0x8

    int-to-byte v2, v2

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 301
    array-length v2, v12

    int-to-byte v2, v2

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 302
    move-object/from16 v0, v18

    invoke-virtual {v0, v12}, Ljava/io/ByteArrayOutputStream;->write([B)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 309
    invoke-virtual/range {v18 .. v18}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v15

    .line 311
    .local v15, "hData":[B
    move/from16 v0, v20

    int-to-byte v2, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 312
    const/4 v2, -0x1

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 313
    array-length v2, v15

    shr-int/lit8 v2, v2, 0x18

    int-to-byte v2, v2

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 314
    array-length v2, v15

    shr-int/lit8 v2, v2, 0x10

    int-to-byte v2, v2

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 315
    array-length v2, v15

    shr-int/lit8 v2, v2, 0x8

    int-to-byte v2, v2

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 316
    array-length v2, v15

    int-to-byte v2, v2

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 318
    invoke-virtual/range {v18 .. v18}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v19

    .line 320
    .local v19, "trailer":[B
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;->sig:Ljava/security/Signature;

    move-object/from16 v0, v19

    invoke-virtual {v2, v0}, Ljava/security/Signature;->update([B)V

    .line 321
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;->dig:Ljava/security/MessageDigest;

    move-object/from16 v0, v19

    invoke-virtual {v2, v0}, Ljava/security/MessageDigest;->update([B)V

    .line 323
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;->keyAlgorithm:I

    const/4 v3, 0x3

    if-eq v2, v3, :cond_3

    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;->keyAlgorithm:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_4

    .line 326
    :cond_3
    const/4 v2, 0x1

    new-array v11, v2, [Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;

    .line 327
    .local v11, "sigValues":[Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;
    const/4 v2, 0x0

    new-instance v3, Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;

    new-instance v4, Ljava/math/BigInteger;

    const/4 v5, 0x1

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;->sig:Ljava/security/Signature;

    invoke-virtual {v6}, Ljava/security/Signature;->sign()[B

    move-result-object v6

    invoke-direct {v4, v5, v6}, Ljava/math/BigInteger;-><init>(I[B)V

    invoke-direct {v3, v4}, Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;-><init>(Ljava/math/BigInteger;)V

    aput-object v3, v11, v2

    .line 334
    :goto_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;->dig:Ljava/security/MessageDigest;

    invoke-virtual {v2}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v13

    .line 335
    .local v13, "digest":[B
    const/4 v2, 0x2

    new-array v10, v2, [B

    .line 337
    .local v10, "fingerPrint":[B
    const/4 v2, 0x0

    const/4 v3, 0x0

    aget-byte v3, v13, v3

    aput-byte v3, v10, v2

    .line 338
    const/4 v2, 0x1

    const/4 v3, 0x1

    aget-byte v3, v13, v3

    aput-byte v3, v10, v2

    .line 340
    new-instance v21, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;

    new-instance v2, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;->signatureType:I

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;->privKey:Lcom/android/sec/org/bouncycastle/openpgp/PGPPrivateKey;

    invoke-virtual {v4}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPrivateKey;->getKeyID()J

    move-result-wide v4

    move-object/from16 v0, p0

    iget v6, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;->keyAlgorithm:I

    move-object/from16 v0, p0

    iget v7, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;->hashAlgorithm:I

    invoke-direct/range {v2 .. v11}, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;-><init>(IJII[Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;[Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;[B[Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;)V

    move-object/from16 v0, v21

    invoke-direct {v0, v2}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;-><init>(Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;)V

    return-object v21

    .line 304
    .end local v10    # "fingerPrint":[B
    .end local v11    # "sigValues":[Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;
    .end local v12    # "data":[B
    .end local v13    # "digest":[B
    .end local v15    # "hData":[B
    .end local v16    # "hOut":Ljava/io/ByteArrayOutputStream;
    .end local v17    # "i":I
    .end local v19    # "trailer":[B
    :catch_0
    move-exception v14

    .line 306
    .local v14, "e":Ljava/io/IOException;
    new-instance v2, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;

    const-string v3, "exception encoding hashed data."

    invoke-direct {v2, v3, v14}, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v2

    .line 331
    .end local v14    # "e":Ljava/io/IOException;
    .restart local v12    # "data":[B
    .restart local v15    # "hData":[B
    .restart local v16    # "hOut":Ljava/io/ByteArrayOutputStream;
    .restart local v17    # "i":I
    .restart local v19    # "trailer":[B
    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;->sig:Ljava/security/Signature;

    invoke-virtual {v2}, Ljava/security/Signature;->sign()[B

    move-result-object v2

    invoke-static {v2}, Lcom/android/sec/org/bouncycastle/openpgp/PGPUtil;->dsaSigToMpi([B)[Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;

    move-result-object v11

    .restart local v11    # "sigValues":[Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;
    goto :goto_3
.end method

.method public generateCertification(Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;)Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;
    .locals 1
    .param p1, "pubKey"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/SignatureException;,
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
        }
    .end annotation

    .prologue
    .line 436
    invoke-direct {p0, p1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;->updateWithPublicKey(Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;)V

    .line 438
    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;->generate()Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;

    move-result-object v0

    return-object v0
.end method

.method public generateCertification(Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;)Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;
    .locals 1
    .param p1, "masterKey"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;
    .param p2, "pubKey"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/SignatureException;,
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
        }
    .end annotation

    .prologue
    .line 418
    invoke-direct {p0, p1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;->updateWithPublicKey(Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;)V

    .line 419
    invoke-direct {p0, p2}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;->updateWithPublicKey(Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;)V

    .line 421
    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;->generate()Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;

    move-result-object v0

    return-object v0
.end method

.method public generateCertification(Lcom/android/sec/org/bouncycastle/openpgp/PGPUserAttributeSubpacketVector;Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;)Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;
    .locals 6
    .param p1, "userAttributes"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPUserAttributeSubpacketVector;
    .param p2, "pubKey"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/SignatureException;,
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
        }
    .end annotation

    .prologue
    .line 380
    invoke-direct {p0, p2}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;->updateWithPublicKey(Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;)V

    .line 387
    :try_start_0
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 388
    .local v0, "bOut":Ljava/io/ByteArrayOutputStream;
    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPUserAttributeSubpacketVector;->toSubpacketArray()[Lcom/android/sec/org/bouncycastle/bcpg/UserAttributeSubpacket;

    move-result-object v3

    .line 389
    .local v3, "packets":[Lcom/android/sec/org/bouncycastle/bcpg/UserAttributeSubpacket;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v4, v3

    if-eq v2, v4, :cond_0

    .line 391
    aget-object v4, v3, v2

    invoke-virtual {v4, v0}, Lcom/android/sec/org/bouncycastle/bcpg/UserAttributeSubpacket;->encode(Ljava/io/OutputStream;)V

    .line 389
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 393
    :cond_0
    const/16 v4, 0xd1

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v5

    invoke-direct {p0, v4, v5}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;->updateWithIdData(I[B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 400
    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;->generate()Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;

    move-result-object v4

    return-object v4

    .line 395
    .end local v0    # "bOut":Ljava/io/ByteArrayOutputStream;
    .end local v2    # "i":I
    .end local v3    # "packets":[Lcom/android/sec/org/bouncycastle/bcpg/UserAttributeSubpacket;
    :catch_0
    move-exception v1

    .line 397
    .local v1, "e":Ljava/io/IOException;
    new-instance v4, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;

    const-string v5, "cannot encode subpacket array"

    invoke-direct {v4, v5, v1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v4
.end method

.method public generateCertification(Ljava/lang/String;Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;)Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;
    .locals 2
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "pubKey"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/SignatureException;,
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
        }
    .end annotation

    .prologue
    .line 357
    invoke-direct {p0, p2}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;->updateWithPublicKey(Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;)V

    .line 362
    const/16 v0, 0xb4

    invoke-static {p1}, Lcom/android/sec/org/bouncycastle/util/Strings;->toByteArray(Ljava/lang/String;)[B

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;->updateWithIdData(I[B)V

    .line 364
    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;->generate()Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;

    move-result-object v0

    return-object v0
.end method

.method public generateOnePassVersion(Z)Lcom/android/sec/org/bouncycastle/openpgp/PGPOnePassSignature;
    .locals 8
    .param p1, "isNested"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
        }
    .end annotation

    .prologue
    .line 248
    new-instance v7, Lcom/android/sec/org/bouncycastle/openpgp/PGPOnePassSignature;

    new-instance v0, Lcom/android/sec/org/bouncycastle/bcpg/OnePassSignaturePacket;

    iget v1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;->signatureType:I

    iget v2, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;->hashAlgorithm:I

    iget v3, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;->keyAlgorithm:I

    iget-object v4, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;->privKey:Lcom/android/sec/org/bouncycastle/openpgp/PGPPrivateKey;

    invoke-virtual {v4}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPrivateKey;->getKeyID()J

    move-result-wide v4

    move v6, p1

    invoke-direct/range {v0 .. v6}, Lcom/android/sec/org/bouncycastle/bcpg/OnePassSignaturePacket;-><init>(IIIJZ)V

    invoke-direct {v7, v0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPOnePassSignature;-><init>(Lcom/android/sec/org/bouncycastle/bcpg/OnePassSignaturePacket;)V

    return-object v7
.end method

.method public initSign(ILcom/android/sec/org/bouncycastle/openpgp/PGPPrivateKey;)V
    .locals 1
    .param p1, "signatureType"    # I
    .param p2, "key"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPPrivateKey;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
        }
    .end annotation

    .prologue
    .line 106
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;->initSign(ILcom/android/sec/org/bouncycastle/openpgp/PGPPrivateKey;Ljava/security/SecureRandom;)V

    .line 107
    return-void
.end method

.method public initSign(ILcom/android/sec/org/bouncycastle/openpgp/PGPPrivateKey;Ljava/security/SecureRandom;)V
    .locals 3
    .param p1, "signatureType"    # I
    .param p2, "key"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPPrivateKey;
    .param p3, "random"    # Ljava/security/SecureRandom;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
        }
    .end annotation

    .prologue
    .line 123
    iput-object p2, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;->privKey:Lcom/android/sec/org/bouncycastle/openpgp/PGPPrivateKey;

    .line 124
    iput p1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;->signatureType:I

    .line 128
    if-nez p3, :cond_0

    .line 130
    :try_start_0
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;->sig:Ljava/security/Signature;

    invoke-virtual {p2}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPrivateKey;->getKey()Ljava/security/PrivateKey;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/security/Signature;->initSign(Ljava/security/PrivateKey;)V
    :try_end_0
    .catch Ljava/security/InvalidKeyException; {:try_start_0 .. :try_end_0} :catch_0

    .line 142
    :goto_0
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;->dig:Ljava/security/MessageDigest;

    invoke-virtual {v1}, Ljava/security/MessageDigest;->reset()V

    .line 143
    const/4 v1, 0x0

    iput-byte v1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;->lastb:B

    .line 144
    return-void

    .line 134
    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;->sig:Ljava/security/Signature;

    invoke-virtual {p2}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPrivateKey;->getKey()Ljava/security/PrivateKey;

    move-result-object v2

    invoke-virtual {v1, v2, p3}, Ljava/security/Signature;->initSign(Ljava/security/PrivateKey;Ljava/security/SecureRandom;)V
    :try_end_1
    .catch Ljava/security/InvalidKeyException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 137
    :catch_0
    move-exception v0

    .line 139
    .local v0, "e":Ljava/security/InvalidKeyException;
    new-instance v1, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;

    const-string v2, "invalid key."

    invoke-direct {v1, v2, v0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v1
.end method

.method public setHashedSubpackets(Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;)V
    .locals 1
    .param p1, "hashedPcks"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;

    .prologue
    .line 216
    if-nez p1, :cond_0

    .line 218
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;->hashed:[Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;

    .line 223
    :goto_0
    return-void

    .line 222
    :cond_0
    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;->toSubpacketArray()[Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;

    move-result-object v0

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;->hashed:[Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;

    goto :goto_0
.end method

.method public setUnhashedSubpackets(Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;)V
    .locals 1
    .param p1, "unhashedPcks"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;

    .prologue
    .line 228
    if-nez p1, :cond_0

    .line 230
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;->unhashed:[Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;

    .line 235
    :goto_0
    return-void

    .line 234
    :cond_0
    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;->toSubpacketArray()[Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;

    move-result-object v0

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;->unhashed:[Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;

    goto :goto_0
.end method

.method public update(B)V
    .locals 4
    .param p1, "b"    # B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/SignatureException;
        }
    .end annotation

    .prologue
    const/16 v3, 0xa

    const/16 v2, 0xd

    .line 150
    iget v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;->signatureType:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    .line 152
    if-ne p1, v2, :cond_1

    .line 154
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;->sig:Ljava/security/Signature;

    invoke-virtual {v0, v2}, Ljava/security/Signature;->update(B)V

    .line 155
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;->sig:Ljava/security/Signature;

    invoke-virtual {v0, v3}, Ljava/security/Signature;->update(B)V

    .line 156
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;->dig:Ljava/security/MessageDigest;

    invoke-virtual {v0, v2}, Ljava/security/MessageDigest;->update(B)V

    .line 157
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;->dig:Ljava/security/MessageDigest;

    invoke-virtual {v0, v3}, Ljava/security/MessageDigest;->update(B)V

    .line 175
    :cond_0
    :goto_0
    iput-byte p1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;->lastb:B

    .line 182
    :goto_1
    return-void

    .line 159
    :cond_1
    if-ne p1, v3, :cond_2

    .line 161
    iget-byte v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;->lastb:B

    if-eq v0, v2, :cond_0

    .line 163
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;->sig:Ljava/security/Signature;

    invoke-virtual {v0, v2}, Ljava/security/Signature;->update(B)V

    .line 164
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;->sig:Ljava/security/Signature;

    invoke-virtual {v0, v3}, Ljava/security/Signature;->update(B)V

    .line 165
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;->dig:Ljava/security/MessageDigest;

    invoke-virtual {v0, v2}, Ljava/security/MessageDigest;->update(B)V

    .line 166
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;->dig:Ljava/security/MessageDigest;

    invoke-virtual {v0, v3}, Ljava/security/MessageDigest;->update(B)V

    goto :goto_0

    .line 171
    :cond_2
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;->sig:Ljava/security/Signature;

    invoke-virtual {v0, p1}, Ljava/security/Signature;->update(B)V

    .line 172
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;->dig:Ljava/security/MessageDigest;

    invoke-virtual {v0, p1}, Ljava/security/MessageDigest;->update(B)V

    goto :goto_0

    .line 179
    :cond_3
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;->sig:Ljava/security/Signature;

    invoke-virtual {v0, p1}, Ljava/security/Signature;->update(B)V

    .line 180
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;->dig:Ljava/security/MessageDigest;

    invoke-virtual {v0, p1}, Ljava/security/MessageDigest;->update(B)V

    goto :goto_1
.end method

.method public update([B)V
    .locals 2
    .param p1, "b"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/SignatureException;
        }
    .end annotation

    .prologue
    .line 188
    const/4 v0, 0x0

    array-length v1, p1

    invoke-virtual {p0, p1, v0, v1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;->update([BII)V

    .line 189
    return-void
.end method

.method public update([BII)V
    .locals 4
    .param p1, "b"    # [B
    .param p2, "off"    # I
    .param p3, "len"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/SignatureException;
        }
    .end annotation

    .prologue
    .line 197
    iget v2, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;->signatureType:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 199
    add-int v0, p2, p3

    .line 201
    .local v0, "finish":I
    move v1, p2

    .local v1, "i":I
    :goto_0
    if-eq v1, v0, :cond_1

    .line 203
    aget-byte v2, p1, v1

    invoke-virtual {p0, v2}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;->update(B)V

    .line 201
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 208
    .end local v0    # "finish":I
    .end local v1    # "i":I
    :cond_0
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;->sig:Ljava/security/Signature;

    invoke-virtual {v2, p1, p2, p3}, Ljava/security/Signature;->update([BII)V

    .line 209
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;->dig:Ljava/security/MessageDigest;

    invoke-virtual {v2, p1, p2, p3}, Ljava/security/MessageDigest;->update([BII)V

    .line 211
    :cond_1
    return-void
.end method
