.class public Lcom/android/sec/org/bouncycastle/openpgp/PGPV3SignatureGenerator;
.super Ljava/lang/Object;
.source "PGPV3SignatureGenerator.java"


# instance fields
.field private dig:Ljava/security/MessageDigest;

.field private hashAlgorithm:I

.field private keyAlgorithm:I

.field private lastb:B

.field private privKey:Lcom/android/sec/org/bouncycastle/openpgp/PGPPrivateKey;

.field private sig:Ljava/security/Signature;

.field private signatureType:I


# direct methods
.method public constructor <init>(IILjava/lang/String;)V
    .locals 1
    .param p1, "keyAlgorithm"    # I
    .param p2, "hashAlgorithm"    # I
    .param p3, "provider"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/NoSuchAlgorithmException;,
            Ljava/security/NoSuchProviderException;,
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
        }
    .end annotation

    .prologue
    .line 43
    invoke-static {p3}, Lcom/android/sec/org/bouncycastle/openpgp/PGPUtil;->getProvider(Ljava/lang/String;)Ljava/security/Provider;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPV3SignatureGenerator;-><init>(IILjava/security/Provider;)V

    .line 44
    return-void
.end method

.method public constructor <init>(IILjava/security/Provider;)V
    .locals 1
    .param p1, "keyAlgorithm"    # I
    .param p2, "hashAlgorithm"    # I
    .param p3, "provider"    # Ljava/security/Provider;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/NoSuchAlgorithmException;,
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
        }
    .end annotation

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    iput p1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPV3SignatureGenerator;->keyAlgorithm:I

    .line 53
    iput p2, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPV3SignatureGenerator;->hashAlgorithm:I

    .line 55
    invoke-static {p2}, Lcom/android/sec/org/bouncycastle/openpgp/PGPUtil;->getDigestName(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p3}, Lcom/android/sec/org/bouncycastle/openpgp/PGPUtil;->getDigestInstance(Ljava/lang/String;Ljava/security/Provider;)Ljava/security/MessageDigest;

    move-result-object v0

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPV3SignatureGenerator;->dig:Ljava/security/MessageDigest;

    .line 56
    invoke-static {p1, p2}, Lcom/android/sec/org/bouncycastle/openpgp/PGPUtil;->getSignatureName(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p3}, Ljava/security/Signature;->getInstance(Ljava/lang/String;Ljava/security/Provider;)Ljava/security/Signature;

    move-result-object v0

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPV3SignatureGenerator;->sig:Ljava/security/Signature;

    .line 57
    return-void
.end method


# virtual methods
.method public generate()Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;
    .locals 19
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;,
            Ljava/security/SignatureException;
        }
    .end annotation

    .prologue
    .line 202
    new-instance v3, Ljava/util/Date;

    invoke-direct {v3}, Ljava/util/Date;-><init>()V

    invoke-virtual {v3}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    div-long v14, v4, v6

    .line 204
    .local v14, "creationTime":J
    new-instance v17, Ljava/io/ByteArrayOutputStream;

    invoke-direct/range {v17 .. v17}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 206
    .local v17, "sOut":Ljava/io/ByteArrayOutputStream;
    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPV3SignatureGenerator;->signatureType:I

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 207
    const/16 v3, 0x18

    shr-long v4, v14, v3

    long-to-int v3, v4

    int-to-byte v3, v3

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 208
    const/16 v3, 0x10

    shr-long v4, v14, v3

    long-to-int v3, v4

    int-to-byte v3, v3

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 209
    const/16 v3, 0x8

    shr-long v4, v14, v3

    long-to-int v3, v4

    int-to-byte v3, v3

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 210
    long-to-int v3, v14

    int-to-byte v3, v3

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 212
    invoke-virtual/range {v17 .. v17}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v16

    .line 214
    .local v16, "hData":[B
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPV3SignatureGenerator;->sig:Ljava/security/Signature;

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/security/Signature;->update([B)V

    .line 215
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPV3SignatureGenerator;->dig:Ljava/security/MessageDigest;

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/security/MessageDigest;->update([B)V

    .line 218
    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPV3SignatureGenerator;->keyAlgorithm:I

    const/4 v4, 0x3

    if-eq v3, v4, :cond_0

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPV3SignatureGenerator;->keyAlgorithm:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_1

    .line 222
    :cond_0
    const/4 v3, 0x1

    new-array v13, v3, [Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;

    .line 223
    .local v13, "sigValues":[Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;
    const/4 v3, 0x0

    new-instance v4, Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;

    new-instance v5, Ljava/math/BigInteger;

    const/4 v6, 0x1

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPV3SignatureGenerator;->sig:Ljava/security/Signature;

    invoke-virtual {v7}, Ljava/security/Signature;->sign()[B

    move-result-object v7

    invoke-direct {v5, v6, v7}, Ljava/math/BigInteger;-><init>(I[B)V

    invoke-direct {v4, v5}, Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;-><init>(Ljava/math/BigInteger;)V

    aput-object v4, v13, v3

    .line 230
    :goto_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPV3SignatureGenerator;->dig:Ljava/security/MessageDigest;

    invoke-virtual {v3}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v2

    .line 231
    .local v2, "digest":[B
    const/4 v3, 0x2

    new-array v12, v3, [B

    .line 233
    .local v12, "fingerPrint":[B
    const/4 v3, 0x0

    const/4 v4, 0x0

    aget-byte v4, v2, v4

    aput-byte v4, v12, v3

    .line 234
    const/4 v3, 0x1

    const/4 v4, 0x1

    aget-byte v4, v2, v4

    aput-byte v4, v12, v3

    .line 236
    new-instance v18, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;

    new-instance v3, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;

    const/4 v4, 0x3

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPV3SignatureGenerator;->signatureType:I

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPV3SignatureGenerator;->privKey:Lcom/android/sec/org/bouncycastle/openpgp/PGPPrivateKey;

    invoke-virtual {v6}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPrivateKey;->getKeyID()J

    move-result-wide v6

    move-object/from16 v0, p0

    iget v8, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPV3SignatureGenerator;->keyAlgorithm:I

    move-object/from16 v0, p0

    iget v9, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPV3SignatureGenerator;->hashAlgorithm:I

    const-wide/16 v10, 0x3e8

    mul-long/2addr v10, v14

    invoke-direct/range {v3 .. v13}, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;-><init>(IIJIIJ[B[Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;)V

    move-object/from16 v0, v18

    invoke-direct {v0, v3}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;-><init>(Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;)V

    return-object v18

    .line 227
    .end local v2    # "digest":[B
    .end local v12    # "fingerPrint":[B
    .end local v13    # "sigValues":[Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;
    :cond_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPV3SignatureGenerator;->sig:Ljava/security/Signature;

    invoke-virtual {v3}, Ljava/security/Signature;->sign()[B

    move-result-object v3

    invoke-static {v3}, Lcom/android/sec/org/bouncycastle/openpgp/PGPUtil;->dsaSigToMpi([B)[Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;

    move-result-object v13

    .restart local v13    # "sigValues":[Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;
    goto :goto_0
.end method

.method public generateOnePassVersion(Z)Lcom/android/sec/org/bouncycastle/openpgp/PGPOnePassSignature;
    .locals 8
    .param p1, "isNested"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
        }
    .end annotation

    .prologue
    .line 189
    new-instance v7, Lcom/android/sec/org/bouncycastle/openpgp/PGPOnePassSignature;

    new-instance v0, Lcom/android/sec/org/bouncycastle/bcpg/OnePassSignaturePacket;

    iget v1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPV3SignatureGenerator;->signatureType:I

    iget v2, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPV3SignatureGenerator;->hashAlgorithm:I

    iget v3, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPV3SignatureGenerator;->keyAlgorithm:I

    iget-object v4, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPV3SignatureGenerator;->privKey:Lcom/android/sec/org/bouncycastle/openpgp/PGPPrivateKey;

    invoke-virtual {v4}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPrivateKey;->getKeyID()J

    move-result-wide v4

    move v6, p1

    invoke-direct/range {v0 .. v6}, Lcom/android/sec/org/bouncycastle/bcpg/OnePassSignaturePacket;-><init>(IIIJZ)V

    invoke-direct {v7, v0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPOnePassSignature;-><init>(Lcom/android/sec/org/bouncycastle/bcpg/OnePassSignaturePacket;)V

    return-object v7
.end method

.method public initSign(ILcom/android/sec/org/bouncycastle/openpgp/PGPPrivateKey;)V
    .locals 1
    .param p1, "signatureType"    # I
    .param p2, "key"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPPrivateKey;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
        }
    .end annotation

    .prologue
    .line 71
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPV3SignatureGenerator;->initSign(ILcom/android/sec/org/bouncycastle/openpgp/PGPPrivateKey;Ljava/security/SecureRandom;)V

    .line 72
    return-void
.end method

.method public initSign(ILcom/android/sec/org/bouncycastle/openpgp/PGPPrivateKey;Ljava/security/SecureRandom;)V
    .locals 3
    .param p1, "signatureType"    # I
    .param p2, "key"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPPrivateKey;
    .param p3, "random"    # Ljava/security/SecureRandom;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
        }
    .end annotation

    .prologue
    .line 88
    iput-object p2, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPV3SignatureGenerator;->privKey:Lcom/android/sec/org/bouncycastle/openpgp/PGPPrivateKey;

    .line 89
    iput p1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPV3SignatureGenerator;->signatureType:I

    .line 93
    if-nez p3, :cond_0

    .line 95
    :try_start_0
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPV3SignatureGenerator;->sig:Ljava/security/Signature;

    invoke-virtual {p2}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPrivateKey;->getKey()Ljava/security/PrivateKey;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/security/Signature;->initSign(Ljava/security/PrivateKey;)V
    :try_end_0
    .catch Ljava/security/InvalidKeyException; {:try_start_0 .. :try_end_0} :catch_0

    .line 107
    :goto_0
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPV3SignatureGenerator;->dig:Ljava/security/MessageDigest;

    invoke-virtual {v1}, Ljava/security/MessageDigest;->reset()V

    .line 108
    const/4 v1, 0x0

    iput-byte v1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPV3SignatureGenerator;->lastb:B

    .line 109
    return-void

    .line 99
    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPV3SignatureGenerator;->sig:Ljava/security/Signature;

    invoke-virtual {p2}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPrivateKey;->getKey()Ljava/security/PrivateKey;

    move-result-object v2

    invoke-virtual {v1, v2, p3}, Ljava/security/Signature;->initSign(Ljava/security/PrivateKey;Ljava/security/SecureRandom;)V
    :try_end_1
    .catch Ljava/security/InvalidKeyException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 102
    :catch_0
    move-exception v0

    .line 104
    .local v0, "e":Ljava/security/InvalidKeyException;
    new-instance v1, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;

    const-string v2, "invalid key."

    invoke-direct {v1, v2, v0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v1
.end method

.method public update(B)V
    .locals 4
    .param p1, "b"    # B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/SignatureException;
        }
    .end annotation

    .prologue
    const/16 v3, 0xa

    const/16 v2, 0xd

    .line 115
    iget v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPV3SignatureGenerator;->signatureType:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    .line 117
    if-ne p1, v2, :cond_1

    .line 119
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPV3SignatureGenerator;->sig:Ljava/security/Signature;

    invoke-virtual {v0, v2}, Ljava/security/Signature;->update(B)V

    .line 120
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPV3SignatureGenerator;->sig:Ljava/security/Signature;

    invoke-virtual {v0, v3}, Ljava/security/Signature;->update(B)V

    .line 121
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPV3SignatureGenerator;->dig:Ljava/security/MessageDigest;

    invoke-virtual {v0, v2}, Ljava/security/MessageDigest;->update(B)V

    .line 122
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPV3SignatureGenerator;->dig:Ljava/security/MessageDigest;

    invoke-virtual {v0, v3}, Ljava/security/MessageDigest;->update(B)V

    .line 140
    :cond_0
    :goto_0
    iput-byte p1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPV3SignatureGenerator;->lastb:B

    .line 147
    :goto_1
    return-void

    .line 124
    :cond_1
    if-ne p1, v3, :cond_2

    .line 126
    iget-byte v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPV3SignatureGenerator;->lastb:B

    if-eq v0, v2, :cond_0

    .line 128
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPV3SignatureGenerator;->sig:Ljava/security/Signature;

    invoke-virtual {v0, v2}, Ljava/security/Signature;->update(B)V

    .line 129
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPV3SignatureGenerator;->sig:Ljava/security/Signature;

    invoke-virtual {v0, v3}, Ljava/security/Signature;->update(B)V

    .line 130
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPV3SignatureGenerator;->dig:Ljava/security/MessageDigest;

    invoke-virtual {v0, v2}, Ljava/security/MessageDigest;->update(B)V

    .line 131
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPV3SignatureGenerator;->dig:Ljava/security/MessageDigest;

    invoke-virtual {v0, v3}, Ljava/security/MessageDigest;->update(B)V

    goto :goto_0

    .line 136
    :cond_2
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPV3SignatureGenerator;->sig:Ljava/security/Signature;

    invoke-virtual {v0, p1}, Ljava/security/Signature;->update(B)V

    .line 137
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPV3SignatureGenerator;->dig:Ljava/security/MessageDigest;

    invoke-virtual {v0, p1}, Ljava/security/MessageDigest;->update(B)V

    goto :goto_0

    .line 144
    :cond_3
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPV3SignatureGenerator;->sig:Ljava/security/Signature;

    invoke-virtual {v0, p1}, Ljava/security/Signature;->update(B)V

    .line 145
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPV3SignatureGenerator;->dig:Ljava/security/MessageDigest;

    invoke-virtual {v0, p1}, Ljava/security/MessageDigest;->update(B)V

    goto :goto_1
.end method

.method public update([B)V
    .locals 2
    .param p1, "b"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/SignatureException;
        }
    .end annotation

    .prologue
    .line 153
    const/4 v0, 0x0

    array-length v1, p1

    invoke-virtual {p0, p1, v0, v1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPV3SignatureGenerator;->update([BII)V

    .line 154
    return-void
.end method

.method public update([BII)V
    .locals 4
    .param p1, "b"    # [B
    .param p2, "off"    # I
    .param p3, "len"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/SignatureException;
        }
    .end annotation

    .prologue
    .line 162
    iget v2, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPV3SignatureGenerator;->signatureType:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 164
    add-int v0, p2, p3

    .line 166
    .local v0, "finish":I
    move v1, p2

    .local v1, "i":I
    :goto_0
    if-eq v1, v0, :cond_1

    .line 168
    aget-byte v2, p1, v1

    invoke-virtual {p0, v2}, Lcom/android/sec/org/bouncycastle/openpgp/PGPV3SignatureGenerator;->update(B)V

    .line 166
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 173
    .end local v0    # "finish":I
    .end local v1    # "i":I
    :cond_0
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPV3SignatureGenerator;->sig:Ljava/security/Signature;

    invoke-virtual {v2, p1, p2, p3}, Ljava/security/Signature;->update([BII)V

    .line 174
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPV3SignatureGenerator;->dig:Ljava/security/MessageDigest;

    invoke-virtual {v2, p1, p2, p3}, Ljava/security/MessageDigest;->update([BII)V

    .line 176
    :cond_1
    return-void
.end method
