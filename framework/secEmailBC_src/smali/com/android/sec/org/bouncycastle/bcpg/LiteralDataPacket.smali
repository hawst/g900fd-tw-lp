.class public Lcom/android/sec/org/bouncycastle/bcpg/LiteralDataPacket;
.super Lcom/android/sec/org/bouncycastle/bcpg/InputStreamPacket;
.source "LiteralDataPacket.java"


# instance fields
.field fileName:[B

.field format:I

.field modDate:J


# direct methods
.method constructor <init>(Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;)V
    .locals 6
    .param p1, "in"    # Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 21
    invoke-direct {p0, p1}, Lcom/android/sec/org/bouncycastle/bcpg/InputStreamPacket;-><init>(Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;)V

    .line 23
    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->read()I

    move-result v2

    iput v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/LiteralDataPacket;->format:I

    .line 24
    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->read()I

    move-result v1

    .line 26
    .local v1, "l":I
    new-array v2, v1, [B

    iput-object v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/LiteralDataPacket;->fileName:[B

    .line 27
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/LiteralDataPacket;->fileName:[B

    array-length v2, v2

    if-eq v0, v2, :cond_0

    .line 29
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/LiteralDataPacket;->fileName:[B

    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->read()I

    move-result v3

    int-to-byte v3, v3

    aput-byte v3, v2, v0

    .line 27
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 32
    :cond_0
    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->read()I

    move-result v2

    int-to-long v2, v2

    const/16 v4, 0x18

    shl-long/2addr v2, v4

    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->read()I

    move-result v4

    shl-int/lit8 v4, v4, 0x10

    int-to-long v4, v4

    or-long/2addr v2, v4

    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->read()I

    move-result v4

    shl-int/lit8 v4, v4, 0x8

    int-to-long v4, v4

    or-long/2addr v2, v4

    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->read()I

    move-result v4

    int-to-long v4, v4

    or-long/2addr v2, v4

    iput-wide v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/LiteralDataPacket;->modDate:J

    .line 33
    return-void
.end method


# virtual methods
.method public getFileName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/LiteralDataPacket;->fileName:[B

    invoke-static {v0}, Lcom/android/sec/org/bouncycastle/util/Strings;->fromUTF8ByteArray([B)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getFormat()I
    .locals 1

    .prologue
    .line 42
    iget v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/LiteralDataPacket;->format:I

    return v0
.end method

.method public getModificationTime()J
    .locals 4

    .prologue
    .line 52
    iget-wide v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/LiteralDataPacket;->modDate:J

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    return-wide v0
.end method

.method public getRawFileName()[B
    .locals 3

    .prologue
    .line 65
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/LiteralDataPacket;->fileName:[B

    array-length v2, v2

    new-array v1, v2, [B

    .line 67
    .local v1, "tmp":[B
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, v1

    if-eq v0, v2, :cond_0

    .line 69
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/LiteralDataPacket;->fileName:[B

    aget-byte v2, v2, v0

    aput-byte v2, v1, v0

    .line 67
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 72
    :cond_0
    return-object v1
.end method
