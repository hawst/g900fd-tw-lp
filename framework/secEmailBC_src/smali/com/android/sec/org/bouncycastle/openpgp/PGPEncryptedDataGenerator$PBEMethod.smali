.class Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator$PBEMethod;
.super Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator$EncMethod;
.source "PGPEncryptedDataGenerator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PBEMethod"
.end annotation


# instance fields
.field s2k:Lcom/android/sec/org/bouncycastle/bcpg/S2K;

.field final synthetic this$0:Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;


# direct methods
.method constructor <init>(Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;ILcom/android/sec/org/bouncycastle/bcpg/S2K;Ljava/security/Key;)V
    .locals 1
    .param p2, "encAlgorithm"    # I
    .param p3, "s2k"    # Lcom/android/sec/org/bouncycastle/bcpg/S2K;
    .param p4, "key"    # Ljava/security/Key;

    .prologue
    .line 62
    iput-object p1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator$PBEMethod;->this$0:Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator$EncMethod;-><init>(Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator$1;)V

    .line 63
    iput p2, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator$PBEMethod;->encAlgorithm:I

    .line 64
    iput-object p3, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator$PBEMethod;->s2k:Lcom/android/sec/org/bouncycastle/bcpg/S2K;

    .line 65
    iput-object p4, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator$PBEMethod;->key:Ljava/security/Key;

    .line 66
    return-void
.end method


# virtual methods
.method public addSessionInfo([B)V
    .locals 6
    .param p1, "sessionInfo"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 77
    iget v2, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator$PBEMethod;->encAlgorithm:I

    invoke-static {v2}, Lcom/android/sec/org/bouncycastle/openpgp/PGPUtil;->getSymmetricCipherName(I)Ljava/lang/String;

    move-result-object v1

    .line 78
    .local v1, "cName":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/CFB/NoPadding"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator$PBEMethod;->this$0:Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;

    # getter for: Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;->defProvider:Ljava/security/Provider;
    invoke-static {v3}, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;->access$100(Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;)Ljava/security/Provider;

    move-result-object v3

    invoke-static {v2, v3}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;Ljava/security/Provider;)Ljavax/crypto/Cipher;

    move-result-object v0

    .line 80
    .local v0, "c":Ljavax/crypto/Cipher;
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator$PBEMethod;->key:Ljava/security/Key;

    new-instance v4, Ljavax/crypto/spec/IvParameterSpec;

    invoke-virtual {v0}, Ljavax/crypto/Cipher;->getBlockSize()I

    move-result v5

    new-array v5, v5, [B

    invoke-direct {v4, v5}, Ljavax/crypto/spec/IvParameterSpec;-><init>([B)V

    iget-object v5, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator$PBEMethod;->this$0:Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;

    # getter for: Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;->rand:Ljava/security/SecureRandom;
    invoke-static {v5}, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;->access$200(Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;)Ljava/security/SecureRandom;

    move-result-object v5

    invoke-virtual {v0, v2, v3, v4, v5}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;Ljava/security/SecureRandom;)V

    .line 82
    const/4 v2, 0x0

    array-length v3, p1

    add-int/lit8 v3, v3, -0x2

    invoke-virtual {v0, p1, v2, v3}, Ljavax/crypto/Cipher;->doFinal([BII)[B

    move-result-object v2

    iput-object v2, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator$PBEMethod;->sessionInfo:[B

    .line 83
    return-void
.end method

.method public encode(Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;)V
    .locals 4
    .param p1, "pOut"    # Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 88
    new-instance v0, Lcom/android/sec/org/bouncycastle/bcpg/SymmetricKeyEncSessionPacket;

    iget v1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator$PBEMethod;->encAlgorithm:I

    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator$PBEMethod;->s2k:Lcom/android/sec/org/bouncycastle/bcpg/S2K;

    iget-object v3, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator$PBEMethod;->sessionInfo:[B

    invoke-direct {v0, v1, v2, v3}, Lcom/android/sec/org/bouncycastle/bcpg/SymmetricKeyEncSessionPacket;-><init>(ILcom/android/sec/org/bouncycastle/bcpg/S2K;[B)V

    .line 90
    .local v0, "pk":Lcom/android/sec/org/bouncycastle/bcpg/SymmetricKeyEncSessionPacket;
    invoke-virtual {p1, v0}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->writePacket(Lcom/android/sec/org/bouncycastle/bcpg/ContainedPacket;)V

    .line 91
    return-void
.end method

.method public getKey()Ljava/security/Key;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator$PBEMethod;->key:Ljava/security/Key;

    return-object v0
.end method
