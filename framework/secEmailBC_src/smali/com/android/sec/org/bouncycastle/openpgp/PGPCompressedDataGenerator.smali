.class public Lcom/android/sec/org/bouncycastle/openpgp/PGPCompressedDataGenerator;
.super Ljava/lang/Object;
.source "PGPCompressedDataGenerator.java"

# interfaces
.implements Lcom/android/sec/org/bouncycastle/bcpg/CompressionAlgorithmTags;
.implements Lcom/android/sec/org/bouncycastle/openpgp/StreamGenerator;


# instance fields
.field private algorithm:I

.field private compression:I

.field private dOut:Ljava/io/OutputStream;

.field private out:Ljava/io/OutputStream;

.field private pkOut:Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;


# direct methods
.method public constructor <init>(I)V
    .locals 1
    .param p1, "algorithm"    # I

    .prologue
    .line 29
    const/4 v0, -0x1

    invoke-direct {p0, p1, v0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPCompressedDataGenerator;-><init>(II)V

    .line 30
    return-void
.end method

.method public constructor <init>(II)V
    .locals 3
    .param p1, "algorithm"    # I
    .param p2, "compression"    # I

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    if-eq p1, v0, :cond_0

    const/4 v0, 0x2

    if-eq p1, v0, :cond_0

    const/4 v0, 0x3

    if-eq p1, v0, :cond_0

    .line 41
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "unknown compression algorithm"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 44
    :cond_0
    const/4 v0, -0x1

    if-eq p2, v0, :cond_2

    .line 46
    if-ltz p2, :cond_1

    const/16 v0, 0x9

    if-le p2, v0, :cond_2

    .line 48
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "unknown compression level: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 52
    :cond_2
    iput p1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPCompressedDataGenerator;->algorithm:I

    .line 53
    iput p2, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPCompressedDataGenerator;->compression:I

    .line 54
    return-void
.end method


# virtual methods
.method public close()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 181
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPCompressedDataGenerator;->dOut:Ljava/io/OutputStream;

    if-eqz v2, :cond_1

    .line 183
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPCompressedDataGenerator;->dOut:Ljava/io/OutputStream;

    instance-of v2, v2, Ljava/util/zip/DeflaterOutputStream;

    if-eqz v2, :cond_2

    .line 185
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPCompressedDataGenerator;->dOut:Ljava/io/OutputStream;

    check-cast v1, Ljava/util/zip/DeflaterOutputStream;

    .line 187
    .local v1, "dfOut":Ljava/util/zip/DeflaterOutputStream;
    invoke-virtual {v1}, Ljava/util/zip/DeflaterOutputStream;->finish()V

    .line 196
    .end local v1    # "dfOut":Ljava/util/zip/DeflaterOutputStream;
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPCompressedDataGenerator;->dOut:Ljava/io/OutputStream;

    invoke-virtual {v2}, Ljava/io/OutputStream;->flush()V

    .line 198
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPCompressedDataGenerator;->pkOut:Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;

    invoke-virtual {v2}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->finish()V

    .line 199
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPCompressedDataGenerator;->pkOut:Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;

    invoke-virtual {v2}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->flush()V

    .line 200
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPCompressedDataGenerator;->out:Ljava/io/OutputStream;

    invoke-virtual {v2}, Ljava/io/OutputStream;->flush()V

    .line 202
    iput-object v3, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPCompressedDataGenerator;->dOut:Ljava/io/OutputStream;

    .line 203
    iput-object v3, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPCompressedDataGenerator;->pkOut:Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;

    .line 204
    iput-object v3, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPCompressedDataGenerator;->out:Ljava/io/OutputStream;

    .line 206
    :cond_1
    return-void

    .line 189
    :cond_2
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPCompressedDataGenerator;->dOut:Ljava/io/OutputStream;

    instance-of v2, v2, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;

    if-eqz v2, :cond_0

    .line 191
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPCompressedDataGenerator;->dOut:Ljava/io/OutputStream;

    check-cast v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;

    .line 193
    .local v0, "cbOut":Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;
    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->finish()V

    goto :goto_0
.end method

.method public open(Ljava/io/OutputStream;)Ljava/io/OutputStream;
    .locals 5
    .param p1, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/16 v1, 0x8

    .line 72
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPCompressedDataGenerator;->dOut:Ljava/io/OutputStream;

    if-eqz v0, :cond_0

    .line 74
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "generator already in open state"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 77
    :cond_0
    iput-object p1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPCompressedDataGenerator;->out:Ljava/io/OutputStream;

    .line 79
    iget v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPCompressedDataGenerator;->algorithm:I

    packed-switch v0, :pswitch_data_0

    .line 102
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "generator not initialised"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 82
    :pswitch_0
    new-instance v0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;

    invoke-direct {v0, p1, v1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;-><init>(Ljava/io/OutputStream;I)V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPCompressedDataGenerator;->pkOut:Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;

    .line 83
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPCompressedDataGenerator;->pkOut:Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;

    invoke-virtual {v0, v4}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->write(I)V

    .line 84
    new-instance v0, Ljava/util/zip/DeflaterOutputStream;

    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPCompressedDataGenerator;->pkOut:Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;

    new-instance v2, Ljava/util/zip/Deflater;

    iget v3, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPCompressedDataGenerator;->compression:I

    invoke-direct {v2, v3, v4}, Ljava/util/zip/Deflater;-><init>(IZ)V

    invoke-direct {v0, v1, v2}, Ljava/util/zip/DeflaterOutputStream;-><init>(Ljava/io/OutputStream;Ljava/util/zip/Deflater;)V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPCompressedDataGenerator;->dOut:Ljava/io/OutputStream;

    .line 105
    :goto_0
    new-instance v0, Lcom/android/sec/org/bouncycastle/openpgp/WrappedGeneratorStream;

    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPCompressedDataGenerator;->dOut:Ljava/io/OutputStream;

    invoke-direct {v0, v1, p0}, Lcom/android/sec/org/bouncycastle/openpgp/WrappedGeneratorStream;-><init>(Ljava/io/OutputStream;Lcom/android/sec/org/bouncycastle/openpgp/StreamGenerator;)V

    return-object v0

    .line 87
    :pswitch_1
    new-instance v0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;

    invoke-direct {v0, p1, v1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;-><init>(Ljava/io/OutputStream;I)V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPCompressedDataGenerator;->pkOut:Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;

    .line 88
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPCompressedDataGenerator;->pkOut:Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->write(I)V

    .line 89
    new-instance v0, Ljava/util/zip/DeflaterOutputStream;

    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPCompressedDataGenerator;->pkOut:Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;

    new-instance v2, Ljava/util/zip/Deflater;

    iget v3, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPCompressedDataGenerator;->compression:I

    invoke-direct {v2, v3}, Ljava/util/zip/Deflater;-><init>(I)V

    invoke-direct {v0, v1, v2}, Ljava/util/zip/DeflaterOutputStream;-><init>(Ljava/io/OutputStream;Ljava/util/zip/Deflater;)V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPCompressedDataGenerator;->dOut:Ljava/io/OutputStream;

    goto :goto_0

    .line 92
    :pswitch_2
    new-instance v0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;

    invoke-direct {v0, p1, v1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;-><init>(Ljava/io/OutputStream;I)V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPCompressedDataGenerator;->pkOut:Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;

    .line 93
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPCompressedDataGenerator;->pkOut:Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->write(I)V

    .line 94
    new-instance v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;

    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPCompressedDataGenerator;->pkOut:Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;

    invoke-direct {v0, v1}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;-><init>(Ljava/io/OutputStream;)V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPCompressedDataGenerator;->dOut:Ljava/io/OutputStream;

    goto :goto_0

    .line 97
    :pswitch_3
    new-instance v0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;

    invoke-direct {v0, p1, v1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;-><init>(Ljava/io/OutputStream;I)V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPCompressedDataGenerator;->pkOut:Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;

    .line 98
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPCompressedDataGenerator;->pkOut:Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->write(I)V

    .line 99
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPCompressedDataGenerator;->pkOut:Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPCompressedDataGenerator;->dOut:Ljava/io/OutputStream;

    goto :goto_0

    .line 79
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public open(Ljava/io/OutputStream;[B)Ljava/io/OutputStream;
    .locals 5
    .param p1, "out"    # Ljava/io/OutputStream;
    .param p2, "buffer"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/16 v1, 0x8

    .line 136
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPCompressedDataGenerator;->dOut:Ljava/io/OutputStream;

    if-eqz v0, :cond_0

    .line 138
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "generator already in open state"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 141
    :cond_0
    iput-object p1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPCompressedDataGenerator;->out:Ljava/io/OutputStream;

    .line 143
    iget v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPCompressedDataGenerator;->algorithm:I

    packed-switch v0, :pswitch_data_0

    .line 166
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "generator not initialised"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 146
    :pswitch_0
    new-instance v0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;

    invoke-direct {v0, p1, v1, p2}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;-><init>(Ljava/io/OutputStream;I[B)V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPCompressedDataGenerator;->pkOut:Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;

    .line 147
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPCompressedDataGenerator;->pkOut:Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;

    invoke-virtual {v0, v4}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->write(I)V

    .line 148
    new-instance v0, Ljava/util/zip/DeflaterOutputStream;

    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPCompressedDataGenerator;->pkOut:Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;

    new-instance v2, Ljava/util/zip/Deflater;

    iget v3, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPCompressedDataGenerator;->compression:I

    invoke-direct {v2, v3, v4}, Ljava/util/zip/Deflater;-><init>(IZ)V

    invoke-direct {v0, v1, v2}, Ljava/util/zip/DeflaterOutputStream;-><init>(Ljava/io/OutputStream;Ljava/util/zip/Deflater;)V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPCompressedDataGenerator;->dOut:Ljava/io/OutputStream;

    .line 169
    :goto_0
    new-instance v0, Lcom/android/sec/org/bouncycastle/openpgp/WrappedGeneratorStream;

    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPCompressedDataGenerator;->dOut:Ljava/io/OutputStream;

    invoke-direct {v0, v1, p0}, Lcom/android/sec/org/bouncycastle/openpgp/WrappedGeneratorStream;-><init>(Ljava/io/OutputStream;Lcom/android/sec/org/bouncycastle/openpgp/StreamGenerator;)V

    return-object v0

    .line 151
    :pswitch_1
    new-instance v0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;

    invoke-direct {v0, p1, v1, p2}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;-><init>(Ljava/io/OutputStream;I[B)V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPCompressedDataGenerator;->pkOut:Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;

    .line 152
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPCompressedDataGenerator;->pkOut:Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->write(I)V

    .line 153
    new-instance v0, Ljava/util/zip/DeflaterOutputStream;

    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPCompressedDataGenerator;->pkOut:Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;

    new-instance v2, Ljava/util/zip/Deflater;

    iget v3, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPCompressedDataGenerator;->compression:I

    invoke-direct {v2, v3}, Ljava/util/zip/Deflater;-><init>(I)V

    invoke-direct {v0, v1, v2}, Ljava/util/zip/DeflaterOutputStream;-><init>(Ljava/io/OutputStream;Ljava/util/zip/Deflater;)V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPCompressedDataGenerator;->dOut:Ljava/io/OutputStream;

    goto :goto_0

    .line 156
    :pswitch_2
    new-instance v0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;

    invoke-direct {v0, p1, v1, p2}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;-><init>(Ljava/io/OutputStream;I[B)V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPCompressedDataGenerator;->pkOut:Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;

    .line 157
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPCompressedDataGenerator;->pkOut:Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->write(I)V

    .line 158
    new-instance v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;

    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPCompressedDataGenerator;->pkOut:Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;

    invoke-direct {v0, v1}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;-><init>(Ljava/io/OutputStream;)V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPCompressedDataGenerator;->dOut:Ljava/io/OutputStream;

    goto :goto_0

    .line 161
    :pswitch_3
    new-instance v0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;

    invoke-direct {v0, p1, v1, p2}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;-><init>(Ljava/io/OutputStream;I[B)V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPCompressedDataGenerator;->pkOut:Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;

    .line 162
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPCompressedDataGenerator;->pkOut:Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->write(I)V

    .line 163
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPCompressedDataGenerator;->pkOut:Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPCompressedDataGenerator;->dOut:Ljava/io/OutputStream;

    goto :goto_0

    .line 143
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
