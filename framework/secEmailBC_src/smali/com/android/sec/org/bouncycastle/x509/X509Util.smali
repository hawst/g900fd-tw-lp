.class Lcom/android/sec/org/bouncycastle/x509/X509Util;
.super Ljava/lang/Object;
.source "X509Util.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/sec/org/bouncycastle/x509/X509Util$Implementation;
    }
.end annotation


# static fields
.field private static algorithms:Ljava/util/Hashtable;

.field private static noParams:Ljava/util/Set;

.field private static params:Ljava/util/Hashtable;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    .line 45
    new-instance v4, Ljava/util/Hashtable;

    invoke-direct {v4}, Ljava/util/Hashtable;-><init>()V

    sput-object v4, Lcom/android/sec/org/bouncycastle/x509/X509Util;->algorithms:Ljava/util/Hashtable;

    .line 46
    new-instance v4, Ljava/util/Hashtable;

    invoke-direct {v4}, Ljava/util/Hashtable;-><init>()V

    sput-object v4, Lcom/android/sec/org/bouncycastle/x509/X509Util;->params:Ljava/util/Hashtable;

    .line 47
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    sput-object v4, Lcom/android/sec/org/bouncycastle/x509/X509Util;->noParams:Ljava/util/Set;

    .line 55
    sget-object v4, Lcom/android/sec/org/bouncycastle/x509/X509Util;->algorithms:Ljava/util/Hashtable;

    const-string v5, "MD5WITHRSAENCRYPTION"

    sget-object v6, Lcom/android/sec/org/bouncycastle/asn1/pkcs/PKCSObjectIdentifiers;->md5WithRSAEncryption:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    invoke-virtual {v4, v5, v6}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 56
    sget-object v4, Lcom/android/sec/org/bouncycastle/x509/X509Util;->algorithms:Ljava/util/Hashtable;

    const-string v5, "MD5WITHRSA"

    sget-object v6, Lcom/android/sec/org/bouncycastle/asn1/pkcs/PKCSObjectIdentifiers;->md5WithRSAEncryption:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    invoke-virtual {v4, v5, v6}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 57
    sget-object v4, Lcom/android/sec/org/bouncycastle/x509/X509Util;->algorithms:Ljava/util/Hashtable;

    const-string v5, "SHA1WITHRSAENCRYPTION"

    sget-object v6, Lcom/android/sec/org/bouncycastle/asn1/pkcs/PKCSObjectIdentifiers;->sha1WithRSAEncryption:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    invoke-virtual {v4, v5, v6}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 58
    sget-object v4, Lcom/android/sec/org/bouncycastle/x509/X509Util;->algorithms:Ljava/util/Hashtable;

    const-string v5, "SHA1WITHRSA"

    sget-object v6, Lcom/android/sec/org/bouncycastle/asn1/pkcs/PKCSObjectIdentifiers;->sha1WithRSAEncryption:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    invoke-virtual {v4, v5, v6}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 63
    sget-object v4, Lcom/android/sec/org/bouncycastle/x509/X509Util;->algorithms:Ljava/util/Hashtable;

    const-string v5, "SHA256WITHRSAENCRYPTION"

    sget-object v6, Lcom/android/sec/org/bouncycastle/asn1/pkcs/PKCSObjectIdentifiers;->sha256WithRSAEncryption:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    invoke-virtual {v4, v5, v6}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 64
    sget-object v4, Lcom/android/sec/org/bouncycastle/x509/X509Util;->algorithms:Ljava/util/Hashtable;

    const-string v5, "SHA256WITHRSA"

    sget-object v6, Lcom/android/sec/org/bouncycastle/asn1/pkcs/PKCSObjectIdentifiers;->sha256WithRSAEncryption:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    invoke-virtual {v4, v5, v6}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 65
    sget-object v4, Lcom/android/sec/org/bouncycastle/x509/X509Util;->algorithms:Ljava/util/Hashtable;

    const-string v5, "SHA384WITHRSAENCRYPTION"

    sget-object v6, Lcom/android/sec/org/bouncycastle/asn1/pkcs/PKCSObjectIdentifiers;->sha384WithRSAEncryption:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    invoke-virtual {v4, v5, v6}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 66
    sget-object v4, Lcom/android/sec/org/bouncycastle/x509/X509Util;->algorithms:Ljava/util/Hashtable;

    const-string v5, "SHA384WITHRSA"

    sget-object v6, Lcom/android/sec/org/bouncycastle/asn1/pkcs/PKCSObjectIdentifiers;->sha384WithRSAEncryption:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    invoke-virtual {v4, v5, v6}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 67
    sget-object v4, Lcom/android/sec/org/bouncycastle/x509/X509Util;->algorithms:Ljava/util/Hashtable;

    const-string v5, "SHA512WITHRSAENCRYPTION"

    sget-object v6, Lcom/android/sec/org/bouncycastle/asn1/pkcs/PKCSObjectIdentifiers;->sha512WithRSAEncryption:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    invoke-virtual {v4, v5, v6}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 68
    sget-object v4, Lcom/android/sec/org/bouncycastle/x509/X509Util;->algorithms:Ljava/util/Hashtable;

    const-string v5, "SHA512WITHRSA"

    sget-object v6, Lcom/android/sec/org/bouncycastle/asn1/pkcs/PKCSObjectIdentifiers;->sha512WithRSAEncryption:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    invoke-virtual {v4, v5, v6}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 69
    sget-object v4, Lcom/android/sec/org/bouncycastle/x509/X509Util;->algorithms:Ljava/util/Hashtable;

    const-string v5, "SHA1WITHRSAANDMGF1"

    sget-object v6, Lcom/android/sec/org/bouncycastle/asn1/pkcs/PKCSObjectIdentifiers;->id_RSASSA_PSS:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    invoke-virtual {v4, v5, v6}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 73
    sget-object v4, Lcom/android/sec/org/bouncycastle/x509/X509Util;->algorithms:Ljava/util/Hashtable;

    const-string v5, "SHA256WITHRSAANDMGF1"

    sget-object v6, Lcom/android/sec/org/bouncycastle/asn1/pkcs/PKCSObjectIdentifiers;->id_RSASSA_PSS:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    invoke-virtual {v4, v5, v6}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 74
    sget-object v4, Lcom/android/sec/org/bouncycastle/x509/X509Util;->algorithms:Ljava/util/Hashtable;

    const-string v5, "SHA384WITHRSAANDMGF1"

    sget-object v6, Lcom/android/sec/org/bouncycastle/asn1/pkcs/PKCSObjectIdentifiers;->id_RSASSA_PSS:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    invoke-virtual {v4, v5, v6}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 75
    sget-object v4, Lcom/android/sec/org/bouncycastle/x509/X509Util;->algorithms:Ljava/util/Hashtable;

    const-string v5, "SHA512WITHRSAANDMGF1"

    sget-object v6, Lcom/android/sec/org/bouncycastle/asn1/pkcs/PKCSObjectIdentifiers;->id_RSASSA_PSS:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    invoke-virtual {v4, v5, v6}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 84
    sget-object v4, Lcom/android/sec/org/bouncycastle/x509/X509Util;->algorithms:Ljava/util/Hashtable;

    const-string v5, "SHA1WITHDSA"

    sget-object v6, Lcom/android/sec/org/bouncycastle/asn1/x9/X9ObjectIdentifiers;->id_dsa_with_sha1:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    invoke-virtual {v4, v5, v6}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 85
    sget-object v4, Lcom/android/sec/org/bouncycastle/x509/X509Util;->algorithms:Ljava/util/Hashtable;

    const-string v5, "DSAWITHSHA1"

    sget-object v6, Lcom/android/sec/org/bouncycastle/asn1/x9/X9ObjectIdentifiers;->id_dsa_with_sha1:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    invoke-virtual {v4, v5, v6}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 89
    sget-object v4, Lcom/android/sec/org/bouncycastle/x509/X509Util;->algorithms:Ljava/util/Hashtable;

    const-string v5, "SHA256WITHDSA"

    sget-object v6, Lcom/android/sec/org/bouncycastle/asn1/nist/NISTObjectIdentifiers;->dsa_with_sha256:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    invoke-virtual {v4, v5, v6}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 90
    sget-object v4, Lcom/android/sec/org/bouncycastle/x509/X509Util;->algorithms:Ljava/util/Hashtable;

    const-string v5, "SHA384WITHDSA"

    sget-object v6, Lcom/android/sec/org/bouncycastle/asn1/nist/NISTObjectIdentifiers;->dsa_with_sha384:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    invoke-virtual {v4, v5, v6}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 91
    sget-object v4, Lcom/android/sec/org/bouncycastle/x509/X509Util;->algorithms:Ljava/util/Hashtable;

    const-string v5, "SHA512WITHDSA"

    sget-object v6, Lcom/android/sec/org/bouncycastle/asn1/nist/NISTObjectIdentifiers;->dsa_with_sha512:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    invoke-virtual {v4, v5, v6}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 92
    sget-object v4, Lcom/android/sec/org/bouncycastle/x509/X509Util;->algorithms:Ljava/util/Hashtable;

    const-string v5, "SHA1WITHECDSA"

    sget-object v6, Lcom/android/sec/org/bouncycastle/asn1/x9/X9ObjectIdentifiers;->ecdsa_with_SHA1:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    invoke-virtual {v4, v5, v6}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 93
    sget-object v4, Lcom/android/sec/org/bouncycastle/x509/X509Util;->algorithms:Ljava/util/Hashtable;

    const-string v5, "ECDSAWITHSHA1"

    sget-object v6, Lcom/android/sec/org/bouncycastle/asn1/x9/X9ObjectIdentifiers;->ecdsa_with_SHA1:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    invoke-virtual {v4, v5, v6}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 97
    sget-object v4, Lcom/android/sec/org/bouncycastle/x509/X509Util;->algorithms:Ljava/util/Hashtable;

    const-string v5, "SHA256WITHECDSA"

    sget-object v6, Lcom/android/sec/org/bouncycastle/asn1/x9/X9ObjectIdentifiers;->ecdsa_with_SHA256:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    invoke-virtual {v4, v5, v6}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 98
    sget-object v4, Lcom/android/sec/org/bouncycastle/x509/X509Util;->algorithms:Ljava/util/Hashtable;

    const-string v5, "SHA384WITHECDSA"

    sget-object v6, Lcom/android/sec/org/bouncycastle/asn1/x9/X9ObjectIdentifiers;->ecdsa_with_SHA384:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    invoke-virtual {v4, v5, v6}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 99
    sget-object v4, Lcom/android/sec/org/bouncycastle/x509/X509Util;->algorithms:Ljava/util/Hashtable;

    const-string v5, "SHA512WITHECDSA"

    sget-object v6, Lcom/android/sec/org/bouncycastle/asn1/x9/X9ObjectIdentifiers;->ecdsa_with_SHA512:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    invoke-virtual {v4, v5, v6}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 112
    sget-object v4, Lcom/android/sec/org/bouncycastle/x509/X509Util;->noParams:Ljava/util/Set;

    sget-object v5, Lcom/android/sec/org/bouncycastle/asn1/x9/X9ObjectIdentifiers;->ecdsa_with_SHA1:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    invoke-interface {v4, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 116
    sget-object v4, Lcom/android/sec/org/bouncycastle/x509/X509Util;->noParams:Ljava/util/Set;

    sget-object v5, Lcom/android/sec/org/bouncycastle/asn1/x9/X9ObjectIdentifiers;->ecdsa_with_SHA256:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    invoke-interface {v4, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 117
    sget-object v4, Lcom/android/sec/org/bouncycastle/x509/X509Util;->noParams:Ljava/util/Set;

    sget-object v5, Lcom/android/sec/org/bouncycastle/asn1/x9/X9ObjectIdentifiers;->ecdsa_with_SHA384:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    invoke-interface {v4, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 118
    sget-object v4, Lcom/android/sec/org/bouncycastle/x509/X509Util;->noParams:Ljava/util/Set;

    sget-object v5, Lcom/android/sec/org/bouncycastle/asn1/x9/X9ObjectIdentifiers;->ecdsa_with_SHA512:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    invoke-interface {v4, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 119
    sget-object v4, Lcom/android/sec/org/bouncycastle/x509/X509Util;->noParams:Ljava/util/Set;

    sget-object v5, Lcom/android/sec/org/bouncycastle/asn1/x9/X9ObjectIdentifiers;->id_dsa_with_sha1:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    invoke-interface {v4, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 123
    sget-object v4, Lcom/android/sec/org/bouncycastle/x509/X509Util;->noParams:Ljava/util/Set;

    sget-object v5, Lcom/android/sec/org/bouncycastle/asn1/nist/NISTObjectIdentifiers;->dsa_with_sha256:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    invoke-interface {v4, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 124
    sget-object v4, Lcom/android/sec/org/bouncycastle/x509/X509Util;->noParams:Ljava/util/Set;

    sget-object v5, Lcom/android/sec/org/bouncycastle/asn1/nist/NISTObjectIdentifiers;->dsa_with_sha384:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    invoke-interface {v4, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 125
    sget-object v4, Lcom/android/sec/org/bouncycastle/x509/X509Util;->noParams:Ljava/util/Set;

    sget-object v5, Lcom/android/sec/org/bouncycastle/asn1/nist/NISTObjectIdentifiers;->dsa_with_sha512:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    invoke-interface {v4, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 138
    new-instance v0, Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;

    sget-object v4, Lcom/android/sec/org/bouncycastle/asn1/oiw/OIWObjectIdentifiers;->idSHA1:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    sget-object v5, Lcom/android/sec/org/bouncycastle/asn1/DERNull;->INSTANCE:Lcom/android/sec/org/bouncycastle/asn1/DERNull;

    invoke-direct {v0, v4, v5}, Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;-><init>(Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;)V

    .line 139
    .local v0, "sha1AlgId":Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;
    sget-object v4, Lcom/android/sec/org/bouncycastle/x509/X509Util;->params:Ljava/util/Hashtable;

    const-string v5, "SHA1WITHRSAANDMGF1"

    const/16 v6, 0x14

    invoke-static {v0, v6}, Lcom/android/sec/org/bouncycastle/x509/X509Util;->creatPSSParams(Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;I)Lcom/android/sec/org/bouncycastle/asn1/pkcs/RSASSAPSSparams;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 146
    new-instance v1, Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;

    sget-object v4, Lcom/android/sec/org/bouncycastle/asn1/nist/NISTObjectIdentifiers;->id_sha256:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    sget-object v5, Lcom/android/sec/org/bouncycastle/asn1/DERNull;->INSTANCE:Lcom/android/sec/org/bouncycastle/asn1/DERNull;

    invoke-direct {v1, v4, v5}, Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;-><init>(Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;)V

    .line 147
    .local v1, "sha256AlgId":Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;
    sget-object v4, Lcom/android/sec/org/bouncycastle/x509/X509Util;->params:Ljava/util/Hashtable;

    const-string v5, "SHA256WITHRSAANDMGF1"

    const/16 v6, 0x20

    invoke-static {v1, v6}, Lcom/android/sec/org/bouncycastle/x509/X509Util;->creatPSSParams(Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;I)Lcom/android/sec/org/bouncycastle/asn1/pkcs/RSASSAPSSparams;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 149
    new-instance v2, Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;

    sget-object v4, Lcom/android/sec/org/bouncycastle/asn1/nist/NISTObjectIdentifiers;->id_sha384:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    sget-object v5, Lcom/android/sec/org/bouncycastle/asn1/DERNull;->INSTANCE:Lcom/android/sec/org/bouncycastle/asn1/DERNull;

    invoke-direct {v2, v4, v5}, Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;-><init>(Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;)V

    .line 150
    .local v2, "sha384AlgId":Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;
    sget-object v4, Lcom/android/sec/org/bouncycastle/x509/X509Util;->params:Ljava/util/Hashtable;

    const-string v5, "SHA384WITHRSAANDMGF1"

    const/16 v6, 0x30

    invoke-static {v2, v6}, Lcom/android/sec/org/bouncycastle/x509/X509Util;->creatPSSParams(Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;I)Lcom/android/sec/org/bouncycastle/asn1/pkcs/RSASSAPSSparams;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 152
    new-instance v3, Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;

    sget-object v4, Lcom/android/sec/org/bouncycastle/asn1/nist/NISTObjectIdentifiers;->id_sha512:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    sget-object v5, Lcom/android/sec/org/bouncycastle/asn1/DERNull;->INSTANCE:Lcom/android/sec/org/bouncycastle/asn1/DERNull;

    invoke-direct {v3, v4, v5}, Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;-><init>(Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;)V

    .line 153
    .local v3, "sha512AlgId":Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;
    sget-object v4, Lcom/android/sec/org/bouncycastle/x509/X509Util;->params:Ljava/util/Hashtable;

    const-string v5, "SHA512WITHRSAANDMGF1"

    const/16 v6, 0x40

    invoke-static {v3, v6}, Lcom/android/sec/org/bouncycastle/x509/X509Util;->creatPSSParams(Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;I)Lcom/android/sec/org/bouncycastle/asn1/pkcs/RSASSAPSSparams;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 154
    return-void
.end method

.method constructor <init>()V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 310
    return-void
.end method

.method static calculateSignature(Lcom/android/sec/org/bouncycastle/asn1/DERObjectIdentifier;Ljava/lang/String;Ljava/lang/String;Ljava/security/PrivateKey;Ljava/security/SecureRandom;Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;)[B
    .locals 3
    .param p0, "sigOid"    # Lcom/android/sec/org/bouncycastle/asn1/DERObjectIdentifier;
    .param p1, "sigName"    # Ljava/lang/String;
    .param p2, "provider"    # Ljava/lang/String;
    .param p3, "key"    # Ljava/security/PrivateKey;
    .param p4, "random"    # Ljava/security/SecureRandom;
    .param p5, "object"    # Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/security/NoSuchProviderException;,
            Ljava/security/NoSuchAlgorithmException;,
            Ljava/security/InvalidKeyException;,
            Ljava/security/SignatureException;
        }
    .end annotation

    .prologue
    .line 276
    if-nez p0, :cond_0

    .line 278
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "no signature algorithm specified"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 281
    :cond_0
    invoke-static {p1, p2}, Lcom/android/sec/org/bouncycastle/x509/X509Util;->getSignatureInstance(Ljava/lang/String;Ljava/lang/String;)Ljava/security/Signature;

    move-result-object v0

    .line 283
    .local v0, "sig":Ljava/security/Signature;
    if-eqz p4, :cond_1

    .line 285
    invoke-virtual {v0, p3, p4}, Ljava/security/Signature;->initSign(Ljava/security/PrivateKey;Ljava/security/SecureRandom;)V

    .line 292
    :goto_0
    invoke-interface {p5}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;->toASN1Primitive()Lcom/android/sec/org/bouncycastle/asn1/ASN1Primitive;

    move-result-object v1

    const-string v2, "DER"

    invoke-virtual {v1, v2}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Primitive;->getEncoded(Ljava/lang/String;)[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/security/Signature;->update([B)V

    .line 294
    invoke-virtual {v0}, Ljava/security/Signature;->sign()[B

    move-result-object v1

    return-object v1

    .line 289
    :cond_1
    invoke-virtual {v0, p3}, Ljava/security/Signature;->initSign(Ljava/security/PrivateKey;)V

    goto :goto_0
.end method

.method static calculateSignature(Lcom/android/sec/org/bouncycastle/asn1/DERObjectIdentifier;Ljava/lang/String;Ljava/security/PrivateKey;Ljava/security/SecureRandom;Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;)[B
    .locals 3
    .param p0, "sigOid"    # Lcom/android/sec/org/bouncycastle/asn1/DERObjectIdentifier;
    .param p1, "sigName"    # Ljava/lang/String;
    .param p2, "key"    # Ljava/security/PrivateKey;
    .param p3, "random"    # Ljava/security/SecureRandom;
    .param p4, "object"    # Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/security/NoSuchAlgorithmException;,
            Ljava/security/InvalidKeyException;,
            Ljava/security/SignatureException;
        }
    .end annotation

    .prologue
    .line 244
    if-nez p0, :cond_0

    .line 246
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "no signature algorithm specified"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 249
    :cond_0
    invoke-static {p1}, Lcom/android/sec/org/bouncycastle/x509/X509Util;->getSignatureInstance(Ljava/lang/String;)Ljava/security/Signature;

    move-result-object v0

    .line 251
    .local v0, "sig":Ljava/security/Signature;
    if-eqz p3, :cond_1

    .line 253
    invoke-virtual {v0, p2, p3}, Ljava/security/Signature;->initSign(Ljava/security/PrivateKey;Ljava/security/SecureRandom;)V

    .line 260
    :goto_0
    invoke-interface {p4}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;->toASN1Primitive()Lcom/android/sec/org/bouncycastle/asn1/ASN1Primitive;

    move-result-object v1

    const-string v2, "DER"

    invoke-virtual {v1, v2}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Primitive;->getEncoded(Ljava/lang/String;)[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/security/Signature;->update([B)V

    .line 262
    invoke-virtual {v0}, Ljava/security/Signature;->sign()[B

    move-result-object v1

    return-object v1

    .line 257
    :cond_1
    invoke-virtual {v0, p2}, Ljava/security/Signature;->initSign(Ljava/security/PrivateKey;)V

    goto :goto_0
.end method

.method static convertPrincipal(Ljavax/security/auth/x500/X500Principal;)Lcom/android/sec/org/bouncycastle/jce/X509Principal;
    .locals 3
    .param p0, "principal"    # Ljavax/security/auth/x500/X500Principal;

    .prologue
    .line 302
    :try_start_0
    new-instance v1, Lcom/android/sec/org/bouncycastle/jce/X509Principal;

    invoke-virtual {p0}, Ljavax/security/auth/x500/X500Principal;->getEncoded()[B

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/android/sec/org/bouncycastle/jce/X509Principal;-><init>([B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    .line 304
    :catch_0
    move-exception v0

    .line 306
    .local v0, "e":Ljava/io/IOException;
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "cannot convert principal"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private static creatPSSParams(Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;I)Lcom/android/sec/org/bouncycastle/asn1/pkcs/RSASSAPSSparams;
    .locals 6
    .param p0, "hashAlgId"    # Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;
    .param p1, "saltSize"    # I

    .prologue
    .line 158
    new-instance v0, Lcom/android/sec/org/bouncycastle/asn1/pkcs/RSASSAPSSparams;

    new-instance v1, Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;

    sget-object v2, Lcom/android/sec/org/bouncycastle/asn1/pkcs/PKCSObjectIdentifiers;->id_mgf1:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    invoke-direct {v1, v2, p0}, Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;-><init>(Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;)V

    new-instance v2, Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;

    int-to-long v4, p1

    invoke-direct {v2, v4, v5}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;-><init>(J)V

    new-instance v3, Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;

    const-wide/16 v4, 0x1

    invoke-direct {v3, v4, v5}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;-><init>(J)V

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/android/sec/org/bouncycastle/asn1/pkcs/RSASSAPSSparams;-><init>(Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;)V

    return-object v0
.end method

.method static getAlgNames()Ljava/util/Iterator;
    .locals 3

    .prologue
    .line 201
    sget-object v2, Lcom/android/sec/org/bouncycastle/x509/X509Util;->algorithms:Ljava/util/Hashtable;

    invoke-virtual {v2}, Ljava/util/Hashtable;->keys()Ljava/util/Enumeration;

    move-result-object v0

    .line 202
    .local v0, "e":Ljava/util/Enumeration;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 204
    .local v1, "l":Ljava/util/List;
    :goto_0
    invoke-interface {v0}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 206
    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 209
    :cond_0
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    return-object v2
.end method

.method static getAlgorithmOID(Ljava/lang/String;)Lcom/android/sec/org/bouncycastle/asn1/DERObjectIdentifier;
    .locals 1
    .param p0, "algorithmName"    # Ljava/lang/String;

    .prologue
    .line 168
    invoke-static {p0}, Lcom/android/sec/org/bouncycastle/util/Strings;->toUpperCase(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 170
    sget-object v0, Lcom/android/sec/org/bouncycastle/x509/X509Util;->algorithms:Ljava/util/Hashtable;

    invoke-virtual {v0, p0}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 172
    sget-object v0, Lcom/android/sec/org/bouncycastle/x509/X509Util;->algorithms:Ljava/util/Hashtable;

    invoke-virtual {v0, p0}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/sec/org/bouncycastle/asn1/DERObjectIdentifier;

    .line 175
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/android/sec/org/bouncycastle/asn1/DERObjectIdentifier;

    invoke-direct {v0, p0}, Lcom/android/sec/org/bouncycastle/asn1/DERObjectIdentifier;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method static getImplementation(Ljava/lang/String;Ljava/lang/String;)Lcom/android/sec/org/bouncycastle/x509/X509Util$Implementation;
    .locals 6
    .param p0, "baseName"    # Ljava/lang/String;
    .param p1, "algorithm"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/NoSuchAlgorithmException;
        }
    .end annotation

    .prologue
    .line 397
    invoke-static {}, Ljava/security/Security;->getProviders()[Ljava/security/Provider;

    move-result-object v2

    .line 402
    .local v2, "prov":[Ljava/security/Provider;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v3, v2

    if-eq v0, v3, :cond_1

    .line 407
    invoke-static {p1}, Lcom/android/sec/org/bouncycastle/util/Strings;->toUpperCase(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aget-object v4, v2, v0

    invoke-static {p0, v3, v4}, Lcom/android/sec/org/bouncycastle/x509/X509Util;->getImplementation(Ljava/lang/String;Ljava/lang/String;Ljava/security/Provider;)Lcom/android/sec/org/bouncycastle/x509/X509Util$Implementation;

    move-result-object v1

    .line 408
    .local v1, "imp":Lcom/android/sec/org/bouncycastle/x509/X509Util$Implementation;
    if-eqz v1, :cond_0

    .line 410
    return-object v1

    .line 415
    :cond_0
    :try_start_0
    aget-object v3, v2, v0

    invoke-static {p0, p1, v3}, Lcom/android/sec/org/bouncycastle/x509/X509Util;->getImplementation(Ljava/lang/String;Ljava/lang/String;Ljava/security/Provider;)Lcom/android/sec/org/bouncycastle/x509/X509Util$Implementation;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 402
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 423
    .end local v1    # "imp":Lcom/android/sec/org/bouncycastle/x509/X509Util$Implementation;
    :cond_1
    new-instance v3, Ljava/security/NoSuchAlgorithmException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "cannot find implementation "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/security/NoSuchAlgorithmException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 417
    .restart local v1    # "imp":Lcom/android/sec/org/bouncycastle/x509/X509Util$Implementation;
    :catch_0
    move-exception v3

    goto :goto_1
.end method

.method static getImplementation(Ljava/lang/String;Ljava/lang/String;Ljava/security/Provider;)Lcom/android/sec/org/bouncycastle/x509/X509Util$Implementation;
    .locals 8
    .param p0, "baseName"    # Ljava/lang/String;
    .param p1, "algorithm"    # Ljava/lang/String;
    .param p2, "prov"    # Ljava/security/Provider;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/NoSuchAlgorithmException;
        }
    .end annotation

    .prologue
    .line 344
    invoke-static {p1}, Lcom/android/sec/org/bouncycastle/util/Strings;->toUpperCase(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 348
    :goto_0
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Alg.Alias."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p2, v5}, Ljava/security/Provider;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .local v0, "alias":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 350
    move-object p1, v0

    goto :goto_0

    .line 353
    :cond_0
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p2, v5}, Ljava/security/Provider;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 355
    .local v1, "className":Ljava/lang/String;
    if-eqz v1, :cond_2

    .line 360
    :try_start_0
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v3

    .line 362
    .local v3, "clsLoader":Ljava/lang/ClassLoader;
    if-eqz v3, :cond_1

    .line 364
    invoke-virtual {v3, v1}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    .line 371
    .local v2, "cls":Ljava/lang/Class;
    :goto_1
    new-instance v5, Lcom/android/sec/org/bouncycastle/x509/X509Util$Implementation;

    invoke-virtual {v2}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v6

    invoke-direct {v5, v6, p2}, Lcom/android/sec/org/bouncycastle/x509/X509Util$Implementation;-><init>(Ljava/lang/Object;Ljava/security/Provider;)V

    return-object v5

    .line 368
    .end local v2    # "cls":Ljava/lang/Class;
    :cond_1
    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v2

    .restart local v2    # "cls":Ljava/lang/Class;
    goto :goto_1

    .line 373
    .end local v2    # "cls":Ljava/lang/Class;
    .end local v3    # "clsLoader":Ljava/lang/ClassLoader;
    :catch_0
    move-exception v4

    .line 375
    .local v4, "e":Ljava/lang/ClassNotFoundException;
    new-instance v5, Ljava/lang/IllegalStateException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "algorithm "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " in provider "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p2}, Ljava/security/Provider;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " but no class \""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\" found!"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 378
    .end local v4    # "e":Ljava/lang/ClassNotFoundException;
    :catch_1
    move-exception v4

    .line 380
    .local v4, "e":Ljava/lang/Exception;
    new-instance v5, Ljava/lang/IllegalStateException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "algorithm "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " in provider "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p2}, Ljava/security/Provider;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " but class \""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\" inaccessible!"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 385
    .end local v4    # "e":Ljava/lang/Exception;
    :cond_2
    new-instance v5, Ljava/security/NoSuchAlgorithmException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "cannot find implementation "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " for provider "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p2}, Ljava/security/Provider;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/security/NoSuchAlgorithmException;-><init>(Ljava/lang/String;)V

    throw v5
.end method

.method static getProvider(Ljava/lang/String;)Ljava/security/Provider;
    .locals 4
    .param p0, "provider"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/NoSuchProviderException;
        }
    .end annotation

    .prologue
    .line 429
    invoke-static {p0}, Ljava/security/Security;->getProvider(Ljava/lang/String;)Ljava/security/Provider;

    move-result-object v0

    .line 431
    .local v0, "prov":Ljava/security/Provider;
    if-nez v0, :cond_0

    .line 433
    new-instance v1, Ljava/security/NoSuchProviderException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Provider "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " not found"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/security/NoSuchProviderException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 436
    :cond_0
    return-object v0
.end method

.method static getSigAlgID(Lcom/android/sec/org/bouncycastle/asn1/DERObjectIdentifier;Ljava/lang/String;)Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;
    .locals 2
    .param p0, "sigOid"    # Lcom/android/sec/org/bouncycastle/asn1/DERObjectIdentifier;
    .param p1, "algorithmName"    # Ljava/lang/String;

    .prologue
    .line 182
    sget-object v0, Lcom/android/sec/org/bouncycastle/x509/X509Util;->noParams:Ljava/util/Set;

    invoke-interface {v0, p0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 184
    new-instance v0, Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;

    invoke-direct {v0, p0}, Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;-><init>(Lcom/android/sec/org/bouncycastle/asn1/DERObjectIdentifier;)V

    .line 195
    :goto_0
    return-object v0

    .line 187
    :cond_0
    invoke-static {p1}, Lcom/android/sec/org/bouncycastle/util/Strings;->toUpperCase(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 189
    sget-object v0, Lcom/android/sec/org/bouncycastle/x509/X509Util;->params:Ljava/util/Hashtable;

    invoke-virtual {v0, p1}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 191
    new-instance v1, Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;

    sget-object v0, Lcom/android/sec/org/bouncycastle/x509/X509Util;->params:Ljava/util/Hashtable;

    invoke-virtual {v0, p1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;

    invoke-direct {v1, p0, v0}, Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;-><init>(Lcom/android/sec/org/bouncycastle/asn1/DERObjectIdentifier;Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;)V

    move-object v0, v1

    goto :goto_0

    .line 195
    :cond_1
    new-instance v0, Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;

    sget-object v1, Lcom/android/sec/org/bouncycastle/asn1/DERNull;->INSTANCE:Lcom/android/sec/org/bouncycastle/asn1/DERNull;

    invoke-direct {v0, p0, v1}, Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;-><init>(Lcom/android/sec/org/bouncycastle/asn1/DERObjectIdentifier;Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;)V

    goto :goto_0
.end method

.method static getSignatureInstance(Ljava/lang/String;)Ljava/security/Signature;
    .locals 1
    .param p0, "algorithm"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/NoSuchAlgorithmException;
        }
    .end annotation

    .prologue
    .line 216
    invoke-static {p0}, Ljava/security/Signature;->getInstance(Ljava/lang/String;)Ljava/security/Signature;

    move-result-object v0

    return-object v0
.end method

.method static getSignatureInstance(Ljava/lang/String;Ljava/lang/String;)Ljava/security/Signature;
    .locals 1
    .param p0, "algorithm"    # Ljava/lang/String;
    .param p1, "provider"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/NoSuchProviderException;,
            Ljava/security/NoSuchAlgorithmException;
        }
    .end annotation

    .prologue
    .line 224
    if-eqz p1, :cond_0

    .line 226
    invoke-static {p0, p1}, Ljava/security/Signature;->getInstance(Ljava/lang/String;Ljava/lang/String;)Ljava/security/Signature;

    move-result-object v0

    .line 230
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0}, Ljava/security/Signature;->getInstance(Ljava/lang/String;)Ljava/security/Signature;

    move-result-object v0

    goto :goto_0
.end method
