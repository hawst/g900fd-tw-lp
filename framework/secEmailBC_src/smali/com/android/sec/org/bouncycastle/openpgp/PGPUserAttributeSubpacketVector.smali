.class public Lcom/android/sec/org/bouncycastle/openpgp/PGPUserAttributeSubpacketVector;
.super Ljava/lang/Object;
.source "PGPUserAttributeSubpacketVector.java"


# instance fields
.field packets:[Lcom/android/sec/org/bouncycastle/bcpg/UserAttributeSubpacket;


# direct methods
.method constructor <init>([Lcom/android/sec/org/bouncycastle/bcpg/UserAttributeSubpacket;)V
    .locals 0
    .param p1, "packets"    # [Lcom/android/sec/org/bouncycastle/bcpg/UserAttributeSubpacket;

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-object p1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPUserAttributeSubpacketVector;->packets:[Lcom/android/sec/org/bouncycastle/bcpg/UserAttributeSubpacket;

    .line 18
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 6
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 54
    if-ne p1, p0, :cond_1

    .line 79
    :cond_0
    :goto_0
    return v2

    .line 59
    :cond_1
    instance-of v4, p1, Lcom/android/sec/org/bouncycastle/openpgp/PGPUserAttributeSubpacketVector;

    if-eqz v4, :cond_4

    move-object v1, p1

    .line 61
    check-cast v1, Lcom/android/sec/org/bouncycastle/openpgp/PGPUserAttributeSubpacketVector;

    .line 63
    .local v1, "other":Lcom/android/sec/org/bouncycastle/openpgp/PGPUserAttributeSubpacketVector;
    iget-object v4, v1, Lcom/android/sec/org/bouncycastle/openpgp/PGPUserAttributeSubpacketVector;->packets:[Lcom/android/sec/org/bouncycastle/bcpg/UserAttributeSubpacket;

    array-length v4, v4

    iget-object v5, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPUserAttributeSubpacketVector;->packets:[Lcom/android/sec/org/bouncycastle/bcpg/UserAttributeSubpacket;

    array-length v5, v5

    if-eq v4, v5, :cond_2

    move v2, v3

    .line 65
    goto :goto_0

    .line 68
    :cond_2
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v4, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPUserAttributeSubpacketVector;->packets:[Lcom/android/sec/org/bouncycastle/bcpg/UserAttributeSubpacket;

    array-length v4, v4

    if-eq v0, v4, :cond_0

    .line 70
    iget-object v4, v1, Lcom/android/sec/org/bouncycastle/openpgp/PGPUserAttributeSubpacketVector;->packets:[Lcom/android/sec/org/bouncycastle/bcpg/UserAttributeSubpacket;

    aget-object v4, v4, v0

    iget-object v5, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPUserAttributeSubpacketVector;->packets:[Lcom/android/sec/org/bouncycastle/bcpg/UserAttributeSubpacket;

    aget-object v5, v5, v0

    invoke-virtual {v4, v5}, Lcom/android/sec/org/bouncycastle/bcpg/UserAttributeSubpacket;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_3

    move v2, v3

    .line 72
    goto :goto_0

    .line 68
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .end local v0    # "i":I
    .end local v1    # "other":Lcom/android/sec/org/bouncycastle/openpgp/PGPUserAttributeSubpacketVector;
    :cond_4
    move v2, v3

    .line 79
    goto :goto_0
.end method

.method public getImageAttribute()Lcom/android/sec/org/bouncycastle/bcpg/attr/ImageAttribute;
    .locals 2

    .prologue
    .line 36
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPUserAttributeSubpacketVector;->getSubpacket(I)Lcom/android/sec/org/bouncycastle/bcpg/UserAttributeSubpacket;

    move-result-object v0

    .line 38
    .local v0, "p":Lcom/android/sec/org/bouncycastle/bcpg/UserAttributeSubpacket;
    if-nez v0, :cond_0

    .line 40
    const/4 v0, 0x0

    .line 43
    .end local v0    # "p":Lcom/android/sec/org/bouncycastle/bcpg/UserAttributeSubpacket;
    :goto_0
    return-object v0

    .restart local v0    # "p":Lcom/android/sec/org/bouncycastle/bcpg/UserAttributeSubpacket;
    :cond_0
    check-cast v0, Lcom/android/sec/org/bouncycastle/bcpg/attr/ImageAttribute;

    goto :goto_0
.end method

.method public getSubpacket(I)Lcom/android/sec/org/bouncycastle/bcpg/UserAttributeSubpacket;
    .locals 2
    .param p1, "type"    # I

    .prologue
    .line 23
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPUserAttributeSubpacketVector;->packets:[Lcom/android/sec/org/bouncycastle/bcpg/UserAttributeSubpacket;

    array-length v1, v1

    if-eq v0, v1, :cond_1

    .line 25
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPUserAttributeSubpacketVector;->packets:[Lcom/android/sec/org/bouncycastle/bcpg/UserAttributeSubpacket;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/android/sec/org/bouncycastle/bcpg/UserAttributeSubpacket;->getType()I

    move-result v1

    if-ne v1, p1, :cond_0

    .line 27
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPUserAttributeSubpacketVector;->packets:[Lcom/android/sec/org/bouncycastle/bcpg/UserAttributeSubpacket;

    aget-object v1, v1, v0

    .line 31
    :goto_1
    return-object v1

    .line 23
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 31
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 84
    const/4 v0, 0x0

    .line 86
    .local v0, "code":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPUserAttributeSubpacketVector;->packets:[Lcom/android/sec/org/bouncycastle/bcpg/UserAttributeSubpacket;

    array-length v2, v2

    if-eq v1, v2, :cond_0

    .line 88
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPUserAttributeSubpacketVector;->packets:[Lcom/android/sec/org/bouncycastle/bcpg/UserAttributeSubpacket;

    aget-object v2, v2, v1

    invoke-virtual {v2}, Lcom/android/sec/org/bouncycastle/bcpg/UserAttributeSubpacket;->hashCode()I

    move-result v2

    xor-int/2addr v0, v2

    .line 86
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 91
    :cond_0
    return v0
.end method

.method toSubpacketArray()[Lcom/android/sec/org/bouncycastle/bcpg/UserAttributeSubpacket;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPUserAttributeSubpacketVector;->packets:[Lcom/android/sec/org/bouncycastle/bcpg/UserAttributeSubpacket;

    return-object v0
.end method
