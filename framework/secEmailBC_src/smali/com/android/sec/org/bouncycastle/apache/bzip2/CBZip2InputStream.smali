.class public Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;
.super Ljava/io/InputStream;
.source "CBZip2InputStream.java"

# interfaces
.implements Lcom/android/sec/org/bouncycastle/apache/bzip2/BZip2Constants;


# static fields
.field private static final NO_RAND_PART_A_STATE:I = 0x5

.field private static final NO_RAND_PART_B_STATE:I = 0x6

.field private static final NO_RAND_PART_C_STATE:I = 0x7

.field private static final RAND_PART_A_STATE:I = 0x2

.field private static final RAND_PART_B_STATE:I = 0x3

.field private static final RAND_PART_C_STATE:I = 0x4

.field private static final START_BLOCK_STATE:I = 0x1


# instance fields
.field private base:[[I

.field private blockRandomised:Z

.field private blockSize100k:I

.field private bsBuff:I

.field private bsLive:I

.field private bsStream:Ljava/io/InputStream;

.field ch2:I

.field chPrev:I

.field private computedBlockCRC:I

.field private computedCombinedCRC:I

.field count:I

.field private currentChar:I

.field private currentState:I

.field i:I

.field i2:I

.field private inUse:[Z

.field j2:I

.field private last:I

.field private limit:[[I

.field private ll8:[C

.field private mCrc:Lcom/android/sec/org/bouncycastle/apache/bzip2/CRC;

.field private minLens:[I

.field private nInUse:I

.field private origPtr:I

.field private perm:[[I

.field rNToGo:I

.field rTPos:I

.field private selector:[C

.field private selectorMtf:[C

.field private seqToUnseq:[C

.field private storedBlockCRC:I

.field private storedCombinedCRC:I

.field private streamEnd:Z

.field tPos:I

.field private tt:[I

.field private unseqToSeq:[C

.field private unzftab:[I

.field z:C


# direct methods
.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 7
    .param p1, "zStream"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v6, 0x4652

    const/16 v5, 0x102

    const/4 v4, 0x0

    const/16 v2, 0x100

    const/4 v3, 0x6

    .line 142
    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    .line 89
    new-instance v1, Lcom/android/sec/org/bouncycastle/apache/bzip2/CRC;

    invoke-direct {v1}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CRC;-><init>()V

    iput-object v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->mCrc:Lcom/android/sec/org/bouncycastle/apache/bzip2/CRC;

    .line 91
    new-array v1, v2, [Z

    iput-object v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->inUse:[Z

    .line 94
    new-array v1, v2, [C

    iput-object v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->seqToUnseq:[C

    .line 95
    new-array v1, v2, [C

    iput-object v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->unseqToSeq:[C

    .line 97
    new-array v1, v6, [C

    iput-object v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->selector:[C

    .line 98
    new-array v1, v6, [C

    iput-object v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->selectorMtf:[C

    .line 107
    new-array v1, v2, [I

    iput-object v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->unzftab:[I

    .line 109
    filled-new-array {v3, v5}, [I

    move-result-object v1

    sget-object v2, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {v2, v1}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [[I

    iput-object v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->limit:[[I

    .line 110
    filled-new-array {v3, v5}, [I

    move-result-object v1

    sget-object v2, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {v2, v1}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [[I

    iput-object v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->base:[[I

    .line 111
    filled-new-array {v3, v5}, [I

    move-result-object v1

    sget-object v2, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {v2, v1}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [[I

    iput-object v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->perm:[[I

    .line 112
    new-array v1, v3, [I

    iput-object v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->minLens:[I

    .line 116
    iput-boolean v4, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->streamEnd:Z

    .line 118
    const/4 v1, -0x1

    iput v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->currentChar:I

    .line 128
    const/4 v1, 0x1

    iput v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->currentState:I

    .line 135
    iput v4, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->rNToGo:I

    .line 136
    iput v4, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->rTPos:I

    .line 143
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->ll8:[C

    .line 144
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->tt:[I

    .line 145
    invoke-direct {p0, p1}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->bsSetStream(Ljava/io/InputStream;)V

    .line 147
    :try_start_0
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->initialize()V

    .line 148
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->initBlock()V

    .line 149
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->setupBlock()V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 153
    :goto_0
    return-void

    .line 150
    :catch_0
    move-exception v0

    .line 151
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-static {}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->compressedStreamEOF()V

    goto :goto_0
.end method

.method private static badBGLengths()V
    .locals 0

    .prologue
    .line 45
    invoke-static {}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->cadvise()V

    .line 46
    return-void
.end method

.method private static badBlockHeader()V
    .locals 0

    .prologue
    .line 270
    invoke-static {}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->cadvise()V

    .line 271
    return-void
.end method

.method private static bitStreamEOF()V
    .locals 0

    .prologue
    .line 49
    invoke-static {}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->cadvise()V

    .line 50
    return-void
.end method

.method private static blockOverrun()V
    .locals 0

    .prologue
    .line 266
    invoke-static {}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->cadvise()V

    .line 267
    return-void
.end method

.method private bsFinishedWithStream()V
    .locals 2

    .prologue
    .line 279
    :try_start_0
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->bsStream:Ljava/io/InputStream;

    if-eqz v0, :cond_0

    .line 280
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->bsStream:Ljava/io/InputStream;

    sget-object v1, Ljava/lang/System;->in:Ljava/io/InputStream;

    if-eq v0, v1, :cond_0

    .line 281
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->bsStream:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    .line 282
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->bsStream:Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 288
    :cond_0
    :goto_0
    return-void

    .line 285
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private bsGetInt32()I
    .locals 1

    .prologue
    .line 337
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->bsGetint()I

    move-result v0

    return v0
.end method

.method private bsGetIntVS(I)I
    .locals 1
    .param p1, "numBits"    # I

    .prologue
    .line 333
    invoke-direct {p0, p1}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->bsR(I)I

    move-result v0

    return v0
.end method

.method private bsGetUChar()C
    .locals 1

    .prologue
    .line 320
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->bsR(I)I

    move-result v0

    int-to-char v0, v0

    return v0
.end method

.method private bsGetint()I
    .locals 4

    .prologue
    const/16 v3, 0x8

    .line 324
    const/4 v0, 0x0

    .line 325
    .local v0, "u":I
    invoke-direct {p0, v3}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->bsR(I)I

    move-result v1

    or-int/lit8 v0, v1, 0x0

    .line 326
    shl-int/lit8 v1, v0, 0x8

    invoke-direct {p0, v3}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->bsR(I)I

    move-result v2

    or-int v0, v1, v2

    .line 327
    shl-int/lit8 v1, v0, 0x8

    invoke-direct {p0, v3}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->bsR(I)I

    move-result v2

    or-int v0, v1, v2

    .line 328
    shl-int/lit8 v1, v0, 0x8

    invoke-direct {p0, v3}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->bsR(I)I

    move-result v2

    or-int v0, v1, v2

    .line 329
    return v0
.end method

.method private bsR(I)I
    .locals 6
    .param p1, "n"    # I

    .prologue
    .line 298
    :goto_0
    iget v4, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->bsLive:I

    if-ge v4, p1, :cond_1

    .line 300
    const/4 v1, 0x0

    .line 302
    .local v1, "thech":C
    :try_start_0
    iget-object v4, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->bsStream:Ljava/io/InputStream;

    invoke-virtual {v4}, Ljava/io/InputStream;->read()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    int-to-char v1, v4

    .line 306
    :goto_1
    const/4 v4, -0x1

    if-ne v1, v4, :cond_0

    .line 307
    invoke-static {}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->compressedStreamEOF()V

    .line 309
    :cond_0
    move v3, v1

    .line 310
    .local v3, "zzi":I
    iget v4, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->bsBuff:I

    shl-int/lit8 v4, v4, 0x8

    and-int/lit16 v5, v3, 0xff

    or-int/2addr v4, v5

    iput v4, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->bsBuff:I

    .line 311
    iget v4, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->bsLive:I

    add-int/lit8 v4, v4, 0x8

    iput v4, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->bsLive:I

    goto :goto_0

    .line 303
    .end local v3    # "zzi":I
    :catch_0
    move-exception v0

    .line 304
    .local v0, "e":Ljava/io/IOException;
    invoke-static {}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->compressedStreamEOF()V

    goto :goto_1

    .line 314
    .end local v0    # "e":Ljava/io/IOException;
    .end local v1    # "thech":C
    :cond_1
    iget v4, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->bsBuff:I

    iget v5, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->bsLive:I

    sub-int/2addr v5, p1

    shr-int/2addr v4, v5

    const/4 v5, 0x1

    shl-int/2addr v5, p1

    add-int/lit8 v5, v5, -0x1

    and-int v2, v4, v5

    .line 315
    .local v2, "v":I
    iget v4, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->bsLive:I

    sub-int/2addr v4, p1

    iput v4, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->bsLive:I

    .line 316
    return v2
.end method

.method private bsSetStream(Ljava/io/InputStream;)V
    .locals 1
    .param p1, "f"    # Ljava/io/InputStream;

    .prologue
    const/4 v0, 0x0

    .line 291
    iput-object p1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->bsStream:Ljava/io/InputStream;

    .line 292
    iput v0, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->bsLive:I

    .line 293
    iput v0, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->bsBuff:I

    .line 294
    return-void
.end method

.method private static cadvise()V
    .locals 2

    .prologue
    .line 40
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "CRC Error"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 42
    return-void
.end method

.method private complete()V
    .locals 2

    .prologue
    .line 256
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->bsGetInt32()I

    move-result v0

    iput v0, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->storedCombinedCRC:I

    .line 257
    iget v0, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->storedCombinedCRC:I

    iget v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->computedCombinedCRC:I

    if-eq v0, v1, :cond_0

    .line 258
    invoke-static {}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->crcError()V

    .line 261
    :cond_0
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->bsFinishedWithStream()V

    .line 262
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->streamEnd:Z

    .line 263
    return-void
.end method

.method private static compressedStreamEOF()V
    .locals 0

    .prologue
    .line 53
    invoke-static {}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->cadvise()V

    .line 54
    return-void
.end method

.method private static crcError()V
    .locals 0

    .prologue
    .line 274
    invoke-static {}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->cadvise()V

    .line 275
    return-void
.end method

.method private endBlock()V
    .locals 2

    .prologue
    .line 244
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->mCrc:Lcom/android/sec/org/bouncycastle/apache/bzip2/CRC;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CRC;->getFinalCRC()I

    move-result v0

    iput v0, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->computedBlockCRC:I

    .line 246
    iget v0, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->storedBlockCRC:I

    iget v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->computedBlockCRC:I

    if-eq v0, v1, :cond_0

    .line 247
    invoke-static {}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->crcError()V

    .line 250
    :cond_0
    iget v0, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->computedCombinedCRC:I

    shl-int/lit8 v0, v0, 0x1

    iget v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->computedCombinedCRC:I

    ushr-int/lit8 v1, v1, 0x1f

    or-int/2addr v0, v1

    iput v0, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->computedCombinedCRC:I

    .line 252
    iget v0, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->computedCombinedCRC:I

    iget v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->computedBlockCRC:I

    xor-int/2addr v0, v1

    iput v0, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->computedCombinedCRC:I

    .line 253
    return-void
.end method

.method private getAndMoveToFrontDecode()V
    .locals 24

    .prologue
    .line 478
    const/16 v21, 0x100

    move/from16 v0, v21

    new-array v15, v0, [C

    .line 482
    .local v15, "yy":[C
    const v21, 0x186a0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->blockSize100k:I

    move/from16 v22, v0

    mul-int v10, v21, v22

    .line 483
    .local v10, "limitLast":I
    const/16 v21, 0x18

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-direct {v0, v1}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->bsGetIntVS(I)I

    move-result v21

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->origPtr:I

    .line 485
    invoke-direct/range {p0 .. p0}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->recvDecodingTables()V

    .line 486
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->nInUse:I

    move/from16 v21, v0

    add-int/lit8 v2, v21, 0x1

    .line 487
    .local v2, "EOB":I
    const/4 v6, -0x1

    .line 488
    .local v6, "groupNo":I
    const/4 v7, 0x0

    .line 496
    .local v7, "groupPos":I
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_0
    const/16 v21, 0xff

    move/from16 v0, v21

    if-gt v8, v0, :cond_0

    .line 497
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->unzftab:[I

    move-object/from16 v21, v0

    const/16 v22, 0x0

    aput v22, v21, v8

    .line 496
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 500
    :cond_0
    const/4 v8, 0x0

    :goto_1
    const/16 v21, 0xff

    move/from16 v0, v21

    if-gt v8, v0, :cond_1

    .line 501
    int-to-char v0, v8

    move/from16 v21, v0

    aput-char v21, v15, v8

    .line 500
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    .line 504
    :cond_1
    const/16 v21, -0x1

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->last:I

    .line 508
    if-nez v7, :cond_2

    .line 509
    add-int/lit8 v6, v6, 0x1

    .line 510
    const/16 v7, 0x32

    .line 512
    :cond_2
    add-int/lit8 v7, v7, -0x1

    .line 513
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->selector:[C

    move-object/from16 v21, v0

    aget-char v18, v21, v6

    .line 514
    .local v18, "zt":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->minLens:[I

    move-object/from16 v21, v0

    aget v17, v21, v18

    .line 515
    .local v17, "zn":I
    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->bsR(I)I

    move-result v19

    .line 516
    .local v19, "zvec":I
    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->limit:[[I

    move-object/from16 v21, v0

    aget-object v21, v21, v18

    aget v21, v21, v17

    move/from16 v0, v19

    move/from16 v1, v21

    if-le v0, v1, :cond_5

    .line 517
    add-int/lit8 v17, v17, 0x1

    .line 520
    :goto_3
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->bsLive:I

    move/from16 v21, v0

    const/16 v22, 0x1

    move/from16 v0, v21

    move/from16 v1, v22

    if-ge v0, v1, :cond_4

    .line 522
    const/4 v13, 0x0

    .line 524
    .local v13, "thech":C
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->bsStream:Ljava/io/InputStream;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Ljava/io/InputStream;->read()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v21

    move/from16 v0, v21

    int-to-char v13, v0

    .line 528
    :goto_4
    const/16 v21, -0x1

    move/from16 v0, v21

    if-ne v13, v0, :cond_3

    .line 529
    invoke-static {}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->compressedStreamEOF()V

    .line 531
    :cond_3
    move/from16 v20, v13

    .line 532
    .local v20, "zzi":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->bsBuff:I

    move/from16 v21, v0

    shl-int/lit8 v21, v21, 0x8

    move/from16 v0, v20

    and-int/lit16 v0, v0, 0xff

    move/from16 v22, v0

    or-int v21, v21, v22

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->bsBuff:I

    .line 533
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->bsLive:I

    move/from16 v21, v0

    add-int/lit8 v21, v21, 0x8

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->bsLive:I

    goto :goto_3

    .line 525
    .end local v20    # "zzi":I
    :catch_0
    move-exception v5

    .line 526
    .local v5, "e":Ljava/io/IOException;
    invoke-static {}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->compressedStreamEOF()V

    goto :goto_4

    .line 536
    .end local v5    # "e":Ljava/io/IOException;
    .end local v13    # "thech":C
    :cond_4
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->bsBuff:I

    move/from16 v21, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->bsLive:I

    move/from16 v22, v0

    add-int/lit8 v22, v22, -0x1

    shr-int v21, v21, v22

    and-int/lit8 v16, v21, 0x1

    .line 537
    .local v16, "zj":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->bsLive:I

    move/from16 v21, v0

    add-int/lit8 v21, v21, -0x1

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->bsLive:I

    .line 539
    shl-int/lit8 v21, v19, 0x1

    or-int v19, v21, v16

    goto/16 :goto_2

    .line 541
    .end local v16    # "zj":I
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->perm:[[I

    move-object/from16 v21, v0

    aget-object v21, v21, v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->base:[[I

    move-object/from16 v22, v0

    aget-object v22, v22, v18

    aget v22, v22, v17

    sub-int v22, v19, v22

    aget v11, v21, v22

    .line 546
    .local v11, "nextSym":I
    :cond_6
    :goto_5
    if-ne v11, v2, :cond_7

    .line 681
    return-void

    .line 550
    :cond_7
    if-eqz v11, :cond_8

    const/16 v21, 0x1

    move/from16 v0, v21

    if-ne v11, v0, :cond_11

    .line 552
    :cond_8
    const/4 v12, -0x1

    .line 553
    .local v12, "s":I
    const/4 v3, 0x1

    .line 555
    .local v3, "N":I
    :cond_9
    if-nez v11, :cond_d

    .line 556
    mul-int/lit8 v21, v3, 0x1

    add-int v12, v12, v21

    .line 560
    :cond_a
    :goto_6
    mul-int/lit8 v3, v3, 0x2

    .line 563
    if-nez v7, :cond_b

    .line 564
    add-int/lit8 v6, v6, 0x1

    .line 565
    const/16 v7, 0x32

    .line 567
    :cond_b
    add-int/lit8 v7, v7, -0x1

    .line 568
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->selector:[C

    move-object/from16 v21, v0

    aget-char v18, v21, v6

    .line 569
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->minLens:[I

    move-object/from16 v21, v0

    aget v17, v21, v18

    .line 570
    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->bsR(I)I

    move-result v19

    .line 571
    :goto_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->limit:[[I

    move-object/from16 v21, v0

    aget-object v21, v21, v18

    aget v21, v21, v17

    move/from16 v0, v19

    move/from16 v1, v21

    if-le v0, v1, :cond_f

    .line 572
    add-int/lit8 v17, v17, 0x1

    .line 575
    :goto_8
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->bsLive:I

    move/from16 v21, v0

    const/16 v22, 0x1

    move/from16 v0, v21

    move/from16 v1, v22

    if-ge v0, v1, :cond_e

    .line 577
    const/4 v13, 0x0

    .line 579
    .restart local v13    # "thech":C
    :try_start_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->bsStream:Ljava/io/InputStream;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Ljava/io/InputStream;->read()I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v21

    move/from16 v0, v21

    int-to-char v13, v0

    .line 583
    :goto_9
    const/16 v21, -0x1

    move/from16 v0, v21

    if-ne v13, v0, :cond_c

    .line 584
    invoke-static {}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->compressedStreamEOF()V

    .line 586
    :cond_c
    move/from16 v20, v13

    .line 587
    .restart local v20    # "zzi":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->bsBuff:I

    move/from16 v21, v0

    shl-int/lit8 v21, v21, 0x8

    move/from16 v0, v20

    and-int/lit16 v0, v0, 0xff

    move/from16 v22, v0

    or-int v21, v21, v22

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->bsBuff:I

    .line 588
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->bsLive:I

    move/from16 v21, v0

    add-int/lit8 v21, v21, 0x8

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->bsLive:I

    goto :goto_8

    .line 557
    .end local v13    # "thech":C
    .end local v20    # "zzi":I
    :cond_d
    const/16 v21, 0x1

    move/from16 v0, v21

    if-ne v11, v0, :cond_a

    .line 558
    mul-int/lit8 v21, v3, 0x2

    add-int v12, v12, v21

    goto/16 :goto_6

    .line 580
    .restart local v13    # "thech":C
    :catch_1
    move-exception v5

    .line 581
    .restart local v5    # "e":Ljava/io/IOException;
    invoke-static {}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->compressedStreamEOF()V

    goto :goto_9

    .line 591
    .end local v5    # "e":Ljava/io/IOException;
    .end local v13    # "thech":C
    :cond_e
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->bsBuff:I

    move/from16 v21, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->bsLive:I

    move/from16 v22, v0

    add-int/lit8 v22, v22, -0x1

    shr-int v21, v21, v22

    and-int/lit8 v16, v21, 0x1

    .line 592
    .restart local v16    # "zj":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->bsLive:I

    move/from16 v21, v0

    add-int/lit8 v21, v21, -0x1

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->bsLive:I

    .line 594
    shl-int/lit8 v21, v19, 0x1

    or-int v19, v21, v16

    goto/16 :goto_7

    .line 596
    .end local v16    # "zj":I
    :cond_f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->perm:[[I

    move-object/from16 v21, v0

    aget-object v21, v21, v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->base:[[I

    move-object/from16 v22, v0

    aget-object v22, v22, v18

    aget v22, v22, v17

    sub-int v22, v19, v22

    aget v11, v21, v22

    .line 598
    if-eqz v11, :cond_9

    const/16 v21, 0x1

    move/from16 v0, v21

    if-eq v11, v0, :cond_9

    .line 600
    add-int/lit8 v12, v12, 0x1

    .line 601
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->seqToUnseq:[C

    move-object/from16 v21, v0

    const/16 v22, 0x0

    aget-char v22, v15, v22

    aget-char v4, v21, v22

    .line 602
    .local v4, "ch":C
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->unzftab:[I

    move-object/from16 v21, v0

    aget v22, v21, v4

    add-int v22, v22, v12

    aput v22, v21, v4

    .line 604
    :goto_a
    if-lez v12, :cond_10

    .line 605
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->last:I

    move/from16 v21, v0

    add-int/lit8 v21, v21, 0x1

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->last:I

    .line 606
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->ll8:[C

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->last:I

    move/from16 v22, v0

    aput-char v4, v21, v22

    .line 607
    add-int/lit8 v12, v12, -0x1

    goto :goto_a

    .line 610
    :cond_10
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->last:I

    move/from16 v21, v0

    move/from16 v0, v21

    if-lt v0, v10, :cond_6

    .line 611
    invoke-static {}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->blockOverrun()V

    goto/16 :goto_5

    .line 616
    .end local v3    # "N":I
    .end local v4    # "ch":C
    .end local v12    # "s":I
    :cond_11
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->last:I

    move/from16 v21, v0

    add-int/lit8 v21, v21, 0x1

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->last:I

    .line 617
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->last:I

    move/from16 v21, v0

    move/from16 v0, v21

    if-lt v0, v10, :cond_12

    .line 618
    invoke-static {}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->blockOverrun()V

    .line 621
    :cond_12
    add-int/lit8 v21, v11, -0x1

    aget-char v14, v15, v21

    .line 622
    .local v14, "tmp":C
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->unzftab:[I

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->seqToUnseq:[C

    move-object/from16 v22, v0

    aget-char v22, v22, v14

    aget v23, v21, v22

    add-int/lit8 v23, v23, 0x1

    aput v23, v21, v22

    .line 623
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->ll8:[C

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->last:I

    move/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->seqToUnseq:[C

    move-object/from16 v23, v0

    aget-char v23, v23, v14

    aput-char v23, v21, v22

    .line 632
    add-int/lit8 v9, v11, -0x1

    .line 633
    .local v9, "j":I
    :goto_b
    const/16 v21, 0x3

    move/from16 v0, v21

    if-le v9, v0, :cond_13

    .line 634
    add-int/lit8 v21, v9, -0x1

    aget-char v21, v15, v21

    aput-char v21, v15, v9

    .line 635
    add-int/lit8 v21, v9, -0x1

    add-int/lit8 v22, v9, -0x2

    aget-char v22, v15, v22

    aput-char v22, v15, v21

    .line 636
    add-int/lit8 v21, v9, -0x2

    add-int/lit8 v22, v9, -0x3

    aget-char v22, v15, v22

    aput-char v22, v15, v21

    .line 637
    add-int/lit8 v21, v9, -0x3

    add-int/lit8 v22, v9, -0x4

    aget-char v22, v15, v22

    aput-char v22, v15, v21

    .line 633
    add-int/lit8 v9, v9, -0x4

    goto :goto_b

    .line 639
    :cond_13
    :goto_c
    if-lez v9, :cond_14

    .line 640
    add-int/lit8 v21, v9, -0x1

    aget-char v21, v15, v21

    aput-char v21, v15, v9

    .line 639
    add-int/lit8 v9, v9, -0x1

    goto :goto_c

    .line 643
    :cond_14
    const/16 v21, 0x0

    aput-char v14, v15, v21

    .line 646
    if-nez v7, :cond_15

    .line 647
    add-int/lit8 v6, v6, 0x1

    .line 648
    const/16 v7, 0x32

    .line 650
    :cond_15
    add-int/lit8 v7, v7, -0x1

    .line 651
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->selector:[C

    move-object/from16 v21, v0

    aget-char v18, v21, v6

    .line 652
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->minLens:[I

    move-object/from16 v21, v0

    aget v17, v21, v18

    .line 653
    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->bsR(I)I

    move-result v19

    .line 654
    :goto_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->limit:[[I

    move-object/from16 v21, v0

    aget-object v21, v21, v18

    aget v21, v21, v17

    move/from16 v0, v19

    move/from16 v1, v21

    if-le v0, v1, :cond_17

    .line 655
    add-int/lit8 v17, v17, 0x1

    .line 658
    :goto_e
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->bsLive:I

    move/from16 v21, v0

    const/16 v22, 0x1

    move/from16 v0, v21

    move/from16 v1, v22

    if-ge v0, v1, :cond_16

    .line 660
    const/4 v13, 0x0

    .line 662
    .restart local v13    # "thech":C
    :try_start_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->bsStream:Ljava/io/InputStream;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Ljava/io/InputStream;->read()I
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    move-result v21

    move/from16 v0, v21

    int-to-char v13, v0

    .line 666
    :goto_f
    move/from16 v20, v13

    .line 667
    .restart local v20    # "zzi":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->bsBuff:I

    move/from16 v21, v0

    shl-int/lit8 v21, v21, 0x8

    move/from16 v0, v20

    and-int/lit16 v0, v0, 0xff

    move/from16 v22, v0

    or-int v21, v21, v22

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->bsBuff:I

    .line 668
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->bsLive:I

    move/from16 v21, v0

    add-int/lit8 v21, v21, 0x8

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->bsLive:I

    goto :goto_e

    .line 663
    .end local v20    # "zzi":I
    :catch_2
    move-exception v5

    .line 664
    .restart local v5    # "e":Ljava/io/IOException;
    invoke-static {}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->compressedStreamEOF()V

    goto :goto_f

    .line 671
    .end local v5    # "e":Ljava/io/IOException;
    .end local v13    # "thech":C
    :cond_16
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->bsBuff:I

    move/from16 v21, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->bsLive:I

    move/from16 v22, v0

    add-int/lit8 v22, v22, -0x1

    shr-int v21, v21, v22

    and-int/lit8 v16, v21, 0x1

    .line 672
    .restart local v16    # "zj":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->bsLive:I

    move/from16 v21, v0

    add-int/lit8 v21, v21, -0x1

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->bsLive:I

    .line 674
    shl-int/lit8 v21, v19, 0x1

    or-int v19, v21, v16

    goto :goto_d

    .line 676
    .end local v16    # "zj":I
    :cond_17
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->perm:[[I

    move-object/from16 v21, v0

    aget-object v21, v21, v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->base:[[I

    move-object/from16 v22, v0

    aget-object v22, v22, v18

    aget v22, v22, v17

    sub-int v22, v19, v22

    aget v11, v21, v22

    .line 678
    goto/16 :goto_5
.end method

.method private hbCreateDecodeTables([I[I[I[CIII)V
    .locals 8
    .param p1, "limit"    # [I
    .param p2, "base"    # [I
    .param p3, "perm"    # [I
    .param p4, "length"    # [C
    .param p5, "minLen"    # I
    .param p6, "maxLen"    # I
    .param p7, "alphaSize"    # I

    .prologue
    const/4 v7, 0x0

    const/16 v6, 0x17

    .line 345
    const/4 v2, 0x0

    .line 346
    .local v2, "pp":I
    move v0, p5

    .local v0, "i":I
    :goto_0
    if-gt v0, p6, :cond_2

    .line 347
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_1
    if-ge v1, p7, :cond_1

    .line 348
    aget-char v4, p4, v1

    if-ne v4, v0, :cond_0

    .line 349
    aput v1, p3, v2

    .line 350
    add-int/lit8 v2, v2, 0x1

    .line 347
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 346
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 355
    .end local v1    # "j":I
    :cond_2
    const/4 v0, 0x0

    :goto_2
    if-ge v0, v6, :cond_3

    .line 356
    aput v7, p2, v0

    .line 355
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 358
    :cond_3
    const/4 v0, 0x0

    :goto_3
    if-ge v0, p7, :cond_4

    .line 359
    aget-char v4, p4, v0

    add-int/lit8 v4, v4, 0x1

    aget v5, p2, v4

    add-int/lit8 v5, v5, 0x1

    aput v5, p2, v4

    .line 358
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 362
    :cond_4
    const/4 v0, 0x1

    :goto_4
    if-ge v0, v6, :cond_5

    .line 363
    aget v4, p2, v0

    add-int/lit8 v5, v0, -0x1

    aget v5, p2, v5

    add-int/2addr v4, v5

    aput v4, p2, v0

    .line 362
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 366
    :cond_5
    const/4 v0, 0x0

    :goto_5
    if-ge v0, v6, :cond_6

    .line 367
    aput v7, p1, v0

    .line 366
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 369
    :cond_6
    const/4 v3, 0x0

    .line 371
    .local v3, "vec":I
    move v0, p5

    :goto_6
    if-gt v0, p6, :cond_7

    .line 372
    add-int/lit8 v4, v0, 0x1

    aget v4, p2, v4

    aget v5, p2, v0

    sub-int/2addr v4, v5

    add-int/2addr v3, v4

    .line 373
    add-int/lit8 v4, v3, -0x1

    aput v4, p1, v0

    .line 374
    shl-int/lit8 v3, v3, 0x1

    .line 371
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 376
    :cond_7
    add-int/lit8 v0, p5, 0x1

    :goto_7
    if-gt v0, p6, :cond_8

    .line 377
    add-int/lit8 v4, v0, -0x1

    aget v4, p1, v4

    add-int/lit8 v4, v4, 0x1

    shl-int/lit8 v4, v4, 0x1

    aget v5, p2, v0

    sub-int/2addr v4, v5

    aput v4, p2, v0

    .line 376
    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    .line 379
    :cond_8
    return-void
.end method

.method private initBlock()V
    .locals 9

    .prologue
    const/16 v8, 0x59

    const/4 v7, 0x1

    .line 209
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->bsGetUChar()C

    move-result v0

    .line 210
    .local v0, "magic1":C
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->bsGetUChar()C

    move-result v1

    .line 211
    .local v1, "magic2":C
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->bsGetUChar()C

    move-result v2

    .line 212
    .local v2, "magic3":C
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->bsGetUChar()C

    move-result v3

    .line 213
    .local v3, "magic4":C
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->bsGetUChar()C

    move-result v4

    .line 214
    .local v4, "magic5":C
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->bsGetUChar()C

    move-result v5

    .line 215
    .local v5, "magic6":C
    const/16 v6, 0x17

    if-ne v0, v6, :cond_0

    const/16 v6, 0x72

    if-ne v1, v6, :cond_0

    const/16 v6, 0x45

    if-ne v2, v6, :cond_0

    const/16 v6, 0x38

    if-ne v3, v6, :cond_0

    const/16 v6, 0x50

    if-ne v4, v6, :cond_0

    const/16 v6, 0x90

    if-ne v5, v6, :cond_0

    .line 217
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->complete()V

    .line 241
    :goto_0
    return-void

    .line 221
    :cond_0
    const/16 v6, 0x31

    if-ne v0, v6, :cond_1

    const/16 v6, 0x41

    if-ne v1, v6, :cond_1

    if-ne v2, v8, :cond_1

    const/16 v6, 0x26

    if-ne v3, v6, :cond_1

    const/16 v6, 0x53

    if-ne v4, v6, :cond_1

    if-eq v5, v8, :cond_2

    .line 223
    :cond_1
    invoke-static {}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->badBlockHeader()V

    .line 224
    iput-boolean v7, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->streamEnd:Z

    goto :goto_0

    .line 228
    :cond_2
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->bsGetInt32()I

    move-result v6

    iput v6, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->storedBlockCRC:I

    .line 230
    invoke-direct {p0, v7}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->bsR(I)I

    move-result v6

    if-ne v6, v7, :cond_3

    .line 231
    iput-boolean v7, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->blockRandomised:Z

    .line 237
    :goto_1
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->getAndMoveToFrontDecode()V

    .line 239
    iget-object v6, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->mCrc:Lcom/android/sec/org/bouncycastle/apache/bzip2/CRC;

    invoke-virtual {v6}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CRC;->initialiseCRC()V

    .line 240
    iput v7, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->currentState:I

    goto :goto_0

    .line 233
    :cond_3
    const/4 v6, 0x0

    iput-boolean v6, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->blockRandomised:Z

    goto :goto_1
.end method

.method private initialize()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 188
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->bsGetUChar()C

    move-result v0

    .line 189
    .local v0, "magic3":C
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->bsGetUChar()C

    move-result v1

    .line 190
    .local v1, "magic4":C
    const/16 v2, 0x42

    if-eq v0, v2, :cond_0

    const/16 v2, 0x5a

    if-eq v1, v2, :cond_0

    .line 192
    new-instance v2, Ljava/io/IOException;

    const-string v3, "Not a BZIP2 marked stream"

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 194
    :cond_0
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->bsGetUChar()C

    move-result v0

    .line 195
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->bsGetUChar()C

    move-result v1

    .line 196
    const/16 v2, 0x68

    if-ne v0, v2, :cond_1

    const/16 v2, 0x31

    if-lt v1, v2, :cond_1

    const/16 v2, 0x39

    if-le v1, v2, :cond_2

    .line 197
    :cond_1
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->bsFinishedWithStream()V

    .line 198
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->streamEnd:Z

    .line 204
    :goto_0
    return-void

    .line 202
    :cond_2
    add-int/lit8 v2, v1, -0x30

    invoke-direct {p0, v2}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->setDecompressStructureSizes(I)V

    .line 203
    const/4 v2, 0x0

    iput v2, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->computedCombinedCRC:I

    goto :goto_0
.end method

.method private makeMaps()V
    .locals 4

    .prologue
    .line 58
    const/4 v1, 0x0

    iput v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->nInUse:I

    .line 59
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/16 v1, 0x100

    if-ge v0, v1, :cond_1

    .line 60
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->inUse:[Z

    aget-boolean v1, v1, v0

    if-eqz v1, :cond_0

    .line 61
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->seqToUnseq:[C

    iget v2, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->nInUse:I

    int-to-char v3, v0

    aput-char v3, v1, v2

    .line 62
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->unseqToSeq:[C

    iget v2, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->nInUse:I

    int-to-char v2, v2

    aput-char v2, v1, v0

    .line 63
    iget v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->nInUse:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->nInUse:I

    .line 59
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 66
    :cond_1
    return-void
.end method

.method private recvDecodingTables()V
    .locals 20

    .prologue
    .line 382
    const/4 v1, 0x6

    const/16 v2, 0x102

    filled-new-array {v1, v2}, [I

    move-result-object v1

    sget-object v2, Ljava/lang/Character;->TYPE:Ljava/lang/Class;

    invoke-static {v2, v1}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, [[C

    .line 385
    .local v13, "len":[[C
    const/16 v1, 0x10

    new-array v11, v1, [Z

    .line 388
    .local v11, "inUse16":[Z
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_0
    const/16 v1, 0x10

    if-ge v10, v1, :cond_1

    .line 389
    const/4 v1, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->bsR(I)I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 390
    const/4 v1, 0x1

    aput-boolean v1, v11, v10

    .line 388
    :goto_1
    add-int/lit8 v10, v10, 0x1

    goto :goto_0

    .line 392
    :cond_0
    const/4 v1, 0x0

    aput-boolean v1, v11, v10

    goto :goto_1

    .line 396
    :cond_1
    const/4 v10, 0x0

    :goto_2
    const/16 v1, 0x100

    if-ge v10, v1, :cond_2

    .line 397
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->inUse:[Z

    const/4 v2, 0x0

    aput-boolean v2, v1, v10

    .line 396
    add-int/lit8 v10, v10, 0x1

    goto :goto_2

    .line 400
    :cond_2
    const/4 v10, 0x0

    :goto_3
    const/16 v1, 0x10

    if-ge v10, v1, :cond_5

    .line 401
    aget-boolean v1, v11, v10

    if-eqz v1, :cond_4

    .line 402
    const/4 v12, 0x0

    .local v12, "j":I
    :goto_4
    const/16 v1, 0x10

    if-ge v12, v1, :cond_4

    .line 403
    const/4 v1, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->bsR(I)I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_3

    .line 404
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->inUse:[Z

    mul-int/lit8 v2, v10, 0x10

    add-int/2addr v2, v12

    const/4 v3, 0x1

    aput-boolean v3, v1, v2

    .line 402
    :cond_3
    add-int/lit8 v12, v12, 0x1

    goto :goto_4

    .line 400
    .end local v12    # "j":I
    :cond_4
    add-int/lit8 v10, v10, 0x1

    goto :goto_3

    .line 410
    :cond_5
    invoke-direct/range {p0 .. p0}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->makeMaps()V

    .line 411
    move-object/from16 v0, p0

    iget v1, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->nInUse:I

    add-int/lit8 v8, v1, 0x2

    .line 414
    .local v8, "alphaSize":I
    const/4 v1, 0x3

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->bsR(I)I

    move-result v14

    .line 415
    .local v14, "nGroups":I
    const/16 v1, 0xf

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->bsR(I)I

    move-result v15

    .line 416
    .local v15, "nSelectors":I
    const/4 v10, 0x0

    :goto_5
    if-ge v10, v15, :cond_7

    .line 417
    const/4 v12, 0x0

    .line 418
    .restart local v12    # "j":I
    :goto_6
    const/4 v1, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->bsR(I)I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_6

    .line 419
    add-int/lit8 v12, v12, 0x1

    goto :goto_6

    .line 421
    :cond_6
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->selectorMtf:[C

    int-to-char v2, v12

    aput-char v2, v1, v10

    .line 416
    add-int/lit8 v10, v10, 0x1

    goto :goto_5

    .line 426
    .end local v12    # "j":I
    :cond_7
    const/4 v1, 0x6

    new-array v0, v1, [C

    move-object/from16 v16, v0

    .line 428
    .local v16, "pos":[C
    const/16 v19, 0x0

    .local v19, "v":C
    :goto_7
    move/from16 v0, v19

    if-ge v0, v14, :cond_8

    .line 429
    aput-char v19, v16, v19

    .line 428
    add-int/lit8 v1, v19, 0x1

    int-to-char v0, v1

    move/from16 v19, v0

    goto :goto_7

    .line 432
    :cond_8
    const/4 v10, 0x0

    :goto_8
    if-ge v10, v15, :cond_a

    .line 433
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->selectorMtf:[C

    aget-char v19, v1, v10

    .line 434
    aget-char v18, v16, v19

    .line 435
    .local v18, "tmp":C
    :goto_9
    if-lez v19, :cond_9

    .line 436
    add-int/lit8 v1, v19, -0x1

    aget-char v1, v16, v1

    aput-char v1, v16, v19

    .line 437
    add-int/lit8 v1, v19, -0x1

    int-to-char v0, v1

    move/from16 v19, v0

    goto :goto_9

    .line 439
    :cond_9
    const/4 v1, 0x0

    aput-char v18, v16, v1

    .line 440
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->selector:[C

    aput-char v18, v1, v10

    .line 432
    add-int/lit8 v10, v10, 0x1

    goto :goto_8

    .line 445
    .end local v18    # "tmp":C
    :cond_a
    const/16 v17, 0x0

    .local v17, "t":I
    :goto_a
    move/from16 v0, v17

    if-ge v0, v14, :cond_e

    .line 446
    const/4 v1, 0x5

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->bsR(I)I

    move-result v9

    .line 447
    .local v9, "curr":I
    const/4 v10, 0x0

    :goto_b
    if-ge v10, v8, :cond_d

    .line 448
    :goto_c
    const/4 v1, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->bsR(I)I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_c

    .line 449
    const/4 v1, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->bsR(I)I

    move-result v1

    if-nez v1, :cond_b

    .line 450
    add-int/lit8 v9, v9, 0x1

    goto :goto_c

    .line 452
    :cond_b
    add-int/lit8 v9, v9, -0x1

    goto :goto_c

    .line 455
    :cond_c
    aget-object v1, v13, v17

    int-to-char v2, v9

    aput-char v2, v1, v10

    .line 447
    add-int/lit8 v10, v10, 0x1

    goto :goto_b

    .line 445
    :cond_d
    add-int/lit8 v17, v17, 0x1

    goto :goto_a

    .line 460
    .end local v9    # "curr":I
    :cond_e
    const/16 v17, 0x0

    :goto_d
    move/from16 v0, v17

    if-ge v0, v14, :cond_12

    .line 461
    const/16 v6, 0x20

    .line 462
    .local v6, "minLen":I
    const/4 v7, 0x0

    .line 463
    .local v7, "maxLen":I
    const/4 v10, 0x0

    :goto_e
    if-ge v10, v8, :cond_11

    .line 464
    aget-object v1, v13, v17

    aget-char v1, v1, v10

    if-le v1, v7, :cond_f

    .line 465
    aget-object v1, v13, v17

    aget-char v7, v1, v10

    .line 467
    :cond_f
    aget-object v1, v13, v17

    aget-char v1, v1, v10

    if-ge v1, v6, :cond_10

    .line 468
    aget-object v1, v13, v17

    aget-char v6, v1, v10

    .line 463
    :cond_10
    add-int/lit8 v10, v10, 0x1

    goto :goto_e

    .line 471
    :cond_11
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->limit:[[I

    aget-object v2, v1, v17

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->base:[[I

    aget-object v3, v1, v17

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->perm:[[I

    aget-object v4, v1, v17

    aget-object v5, v13, v17

    move-object/from16 v1, p0

    invoke-direct/range {v1 .. v8}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->hbCreateDecodeTables([I[I[I[CIII)V

    .line 473
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->minLens:[I

    aput v6, v1, v17

    .line 460
    add-int/lit8 v17, v17, 0x1

    goto :goto_d

    .line 475
    .end local v6    # "minLen":I
    .end local v7    # "maxLen":I
    :cond_12
    return-void
.end method

.method private setDecompressStructureSizes(I)V
    .locals 3
    .param p1, "newSize100k"    # I

    .prologue
    const/16 v2, 0x9

    .line 836
    if-ltz p1, :cond_0

    if-gt p1, v2, :cond_0

    iget v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->blockSize100k:I

    if-ltz v1, :cond_0

    iget v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->blockSize100k:I

    if-le v1, v2, :cond_0

    .line 841
    :cond_0
    iput p1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->blockSize100k:I

    .line 843
    if-nez p1, :cond_1

    .line 850
    :goto_0
    return-void

    .line 847
    :cond_1
    const v1, 0x186a0

    mul-int v0, v1, p1

    .line 848
    .local v0, "n":I
    new-array v1, v0, [C

    iput-object v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->ll8:[C

    .line 849
    new-array v1, v0, [I

    iput-object v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->tt:[I

    goto :goto_0
.end method

.method private setupBlock()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/16 v6, 0x100

    const/4 v5, 0x0

    .line 684
    const/16 v2, 0x101

    new-array v0, v2, [I

    .line 687
    .local v0, "cftab":[I
    aput v5, v0, v5

    .line 688
    iput v7, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->i:I

    :goto_0
    iget v2, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->i:I

    if-gt v2, v6, :cond_0

    .line 689
    iget v2, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->i:I

    iget-object v3, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->unzftab:[I

    iget v4, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->i:I

    add-int/lit8 v4, v4, -0x1

    aget v3, v3, v4

    aput v3, v0, v2

    .line 688
    iget v2, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->i:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->i:I

    goto :goto_0

    .line 691
    :cond_0
    iput v7, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->i:I

    :goto_1
    iget v2, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->i:I

    if-gt v2, v6, :cond_1

    .line 692
    iget v2, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->i:I

    aget v3, v0, v2

    iget v4, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->i:I

    add-int/lit8 v4, v4, -0x1

    aget v4, v0, v4

    add-int/2addr v3, v4

    aput v3, v0, v2

    .line 691
    iget v2, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->i:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->i:I

    goto :goto_1

    .line 695
    :cond_1
    iput v5, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->i:I

    :goto_2
    iget v2, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->i:I

    iget v3, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->last:I

    if-gt v2, v3, :cond_2

    .line 696
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->ll8:[C

    iget v3, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->i:I

    aget-char v1, v2, v3

    .line 697
    .local v1, "ch":C
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->tt:[I

    aget v3, v0, v1

    iget v4, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->i:I

    aput v4, v2, v3

    .line 698
    aget v2, v0, v1

    add-int/lit8 v2, v2, 0x1

    aput v2, v0, v1

    .line 695
    iget v2, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->i:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->i:I

    goto :goto_2

    .line 700
    .end local v1    # "ch":C
    :cond_2
    const/4 v0, 0x0

    .line 702
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->tt:[I

    iget v3, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->origPtr:I

    aget v2, v2, v3

    iput v2, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->tPos:I

    .line 704
    iput v5, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->count:I

    .line 705
    iput v5, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->i2:I

    .line 706
    iput v6, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->ch2:I

    .line 708
    iget-boolean v2, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->blockRandomised:Z

    if-eqz v2, :cond_3

    .line 709
    iput v5, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->rNToGo:I

    .line 710
    iput v5, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->rTPos:I

    .line 711
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->setupRandPartA()V

    .line 715
    :goto_3
    return-void

    .line 713
    :cond_3
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->setupNoRandPartA()V

    goto :goto_3
.end method

.method private setupNoRandPartA()V
    .locals 2

    .prologue
    .line 744
    iget v0, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->i2:I

    iget v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->last:I

    if-gt v0, v1, :cond_0

    .line 745
    iget v0, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->ch2:I

    iput v0, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->chPrev:I

    .line 746
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->ll8:[C

    iget v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->tPos:I

    aget-char v0, v0, v1

    iput v0, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->ch2:I

    .line 747
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->tt:[I

    iget v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->tPos:I

    aget v0, v0, v1

    iput v0, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->tPos:I

    .line 748
    iget v0, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->i2:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->i2:I

    .line 750
    iget v0, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->ch2:I

    iput v0, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->currentChar:I

    .line 751
    const/4 v0, 0x6

    iput v0, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->currentState:I

    .line 752
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->mCrc:Lcom/android/sec/org/bouncycastle/apache/bzip2/CRC;

    iget v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->ch2:I

    invoke-virtual {v0, v1}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CRC;->updateCRC(I)V

    .line 758
    :goto_0
    return-void

    .line 754
    :cond_0
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->endBlock()V

    .line 755
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->initBlock()V

    .line 756
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->setupBlock()V

    goto :goto_0
.end method

.method private setupNoRandPartB()V
    .locals 3

    .prologue
    const/4 v2, 0x5

    .line 803
    iget v0, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->ch2:I

    iget v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->chPrev:I

    if-eq v0, v1, :cond_0

    .line 804
    iput v2, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->currentState:I

    .line 805
    const/4 v0, 0x1

    iput v0, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->count:I

    .line 806
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->setupNoRandPartA()V

    .line 820
    :goto_0
    return-void

    .line 808
    :cond_0
    iget v0, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->count:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->count:I

    .line 809
    iget v0, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->count:I

    const/4 v1, 0x4

    if-lt v0, v1, :cond_1

    .line 810
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->ll8:[C

    iget v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->tPos:I

    aget-char v0, v0, v1

    iput-char v0, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->z:C

    .line 811
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->tt:[I

    iget v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->tPos:I

    aget v0, v0, v1

    iput v0, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->tPos:I

    .line 812
    const/4 v0, 0x7

    iput v0, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->currentState:I

    .line 813
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->j2:I

    .line 814
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->setupNoRandPartC()V

    goto :goto_0

    .line 816
    :cond_1
    iput v2, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->currentState:I

    .line 817
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->setupNoRandPartA()V

    goto :goto_0
.end method

.method private setupNoRandPartC()V
    .locals 2

    .prologue
    .line 823
    iget v0, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->j2:I

    iget-char v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->z:C

    if-ge v0, v1, :cond_0

    .line 824
    iget v0, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->ch2:I

    iput v0, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->currentChar:I

    .line 825
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->mCrc:Lcom/android/sec/org/bouncycastle/apache/bzip2/CRC;

    iget v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->ch2:I

    invoke-virtual {v0, v1}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CRC;->updateCRC(I)V

    .line 826
    iget v0, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->j2:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->j2:I

    .line 833
    :goto_0
    return-void

    .line 828
    :cond_0
    const/4 v0, 0x5

    iput v0, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->currentState:I

    .line 829
    iget v0, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->i2:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->i2:I

    .line 830
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->count:I

    .line 831
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->setupNoRandPartA()V

    goto :goto_0
.end method

.method private setupRandPartA()V
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 718
    iget v2, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->i2:I

    iget v3, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->last:I

    if-gt v2, v3, :cond_2

    .line 719
    iget v2, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->ch2:I

    iput v2, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->chPrev:I

    .line 720
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->ll8:[C

    iget v3, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->tPos:I

    aget-char v2, v2, v3

    iput v2, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->ch2:I

    .line 721
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->tt:[I

    iget v3, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->tPos:I

    aget v2, v2, v3

    iput v2, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->tPos:I

    .line 722
    iget v2, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->rNToGo:I

    if-nez v2, :cond_0

    .line 723
    sget-object v2, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->rNums:[I

    iget v3, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->rTPos:I

    aget v2, v2, v3

    iput v2, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->rNToGo:I

    .line 724
    iget v2, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->rTPos:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->rTPos:I

    .line 725
    iget v2, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->rTPos:I

    const/16 v3, 0x200

    if-ne v2, v3, :cond_0

    .line 726
    iput v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->rTPos:I

    .line 729
    :cond_0
    iget v2, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->rNToGo:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->rNToGo:I

    .line 730
    iget v2, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->ch2:I

    iget v3, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->rNToGo:I

    if-ne v3, v0, :cond_1

    :goto_0
    xor-int/2addr v0, v2

    iput v0, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->ch2:I

    .line 731
    iget v0, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->i2:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->i2:I

    .line 733
    iget v0, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->ch2:I

    iput v0, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->currentChar:I

    .line 734
    const/4 v0, 0x3

    iput v0, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->currentState:I

    .line 735
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->mCrc:Lcom/android/sec/org/bouncycastle/apache/bzip2/CRC;

    iget v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->ch2:I

    invoke-virtual {v0, v1}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CRC;->updateCRC(I)V

    .line 741
    :goto_1
    return-void

    :cond_1
    move v0, v1

    .line 730
    goto :goto_0

    .line 737
    :cond_2
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->endBlock()V

    .line 738
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->initBlock()V

    .line 739
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->setupBlock()V

    goto :goto_1
.end method

.method private setupRandPartB()V
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 761
    iget v2, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->ch2:I

    iget v3, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->chPrev:I

    if-eq v2, v3, :cond_0

    .line 762
    iput v4, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->currentState:I

    .line 763
    iput v0, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->count:I

    .line 764
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->setupRandPartA()V

    .line 787
    :goto_0
    return-void

    .line 766
    :cond_0
    iget v2, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->count:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->count:I

    .line 767
    iget v2, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->count:I

    if-lt v2, v5, :cond_3

    .line 768
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->ll8:[C

    iget v3, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->tPos:I

    aget-char v2, v2, v3

    iput-char v2, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->z:C

    .line 769
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->tt:[I

    iget v3, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->tPos:I

    aget v2, v2, v3

    iput v2, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->tPos:I

    .line 770
    iget v2, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->rNToGo:I

    if-nez v2, :cond_1

    .line 771
    sget-object v2, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->rNums:[I

    iget v3, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->rTPos:I

    aget v2, v2, v3

    iput v2, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->rNToGo:I

    .line 772
    iget v2, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->rTPos:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->rTPos:I

    .line 773
    iget v2, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->rTPos:I

    const/16 v3, 0x200

    if-ne v2, v3, :cond_1

    .line 774
    iput v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->rTPos:I

    .line 777
    :cond_1
    iget v2, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->rNToGo:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->rNToGo:I

    .line 778
    iget-char v2, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->z:C

    iget v3, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->rNToGo:I

    if-ne v3, v0, :cond_2

    :goto_1
    xor-int/2addr v0, v2

    int-to-char v0, v0

    iput-char v0, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->z:C

    .line 779
    iput v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->j2:I

    .line 780
    iput v5, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->currentState:I

    .line 781
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->setupRandPartC()V

    goto :goto_0

    :cond_2
    move v0, v1

    .line 778
    goto :goto_1

    .line 783
    :cond_3
    iput v4, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->currentState:I

    .line 784
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->setupRandPartA()V

    goto :goto_0
.end method

.method private setupRandPartC()V
    .locals 2

    .prologue
    .line 790
    iget v0, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->j2:I

    iget-char v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->z:C

    if-ge v0, v1, :cond_0

    .line 791
    iget v0, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->ch2:I

    iput v0, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->currentChar:I

    .line 792
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->mCrc:Lcom/android/sec/org/bouncycastle/apache/bzip2/CRC;

    iget v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->ch2:I

    invoke-virtual {v0, v1}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CRC;->updateCRC(I)V

    .line 793
    iget v0, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->j2:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->j2:I

    .line 800
    :goto_0
    return-void

    .line 795
    :cond_0
    const/4 v0, 0x2

    iput v0, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->currentState:I

    .line 796
    iget v0, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->i2:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->i2:I

    .line 797
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->count:I

    .line 798
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->setupRandPartA()V

    goto :goto_0
.end method


# virtual methods
.method public read()I
    .locals 2

    .prologue
    .line 156
    iget-boolean v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->streamEnd:Z

    if-eqz v1, :cond_0

    .line 157
    const/4 v0, -0x1

    .line 182
    :goto_0
    :pswitch_0
    return v0

    .line 159
    :cond_0
    iget v0, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->currentChar:I

    .line 160
    .local v0, "retChar":I
    iget v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->currentState:I

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 166
    :pswitch_1
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->setupRandPartB()V

    goto :goto_0

    .line 169
    :pswitch_2
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->setupRandPartC()V

    goto :goto_0

    .line 174
    :pswitch_3
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->setupNoRandPartB()V

    goto :goto_0

    .line 177
    :pswitch_4
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;->setupNoRandPartC()V

    goto :goto_0

    .line 160
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
