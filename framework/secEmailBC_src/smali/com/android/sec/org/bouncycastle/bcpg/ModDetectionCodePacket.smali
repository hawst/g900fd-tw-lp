.class public Lcom/android/sec/org/bouncycastle/bcpg/ModDetectionCodePacket;
.super Lcom/android/sec/org/bouncycastle/bcpg/ContainedPacket;
.source "ModDetectionCodePacket.java"


# instance fields
.field private digest:[B


# direct methods
.method constructor <init>(Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;)V
    .locals 1
    .param p1, "in"    # Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/bcpg/ContainedPacket;-><init>()V

    .line 17
    const/16 v0, 0x14

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/ModDetectionCodePacket;->digest:[B

    .line 18
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/ModDetectionCodePacket;->digest:[B

    invoke-virtual {p1, v0}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->readFully([B)V

    .line 19
    return-void
.end method

.method public constructor <init>([B)V
    .locals 3
    .param p1, "digest"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 24
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/bcpg/ContainedPacket;-><init>()V

    .line 25
    array-length v0, p1

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/ModDetectionCodePacket;->digest:[B

    .line 27
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/ModDetectionCodePacket;->digest:[B

    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/bcpg/ModDetectionCodePacket;->digest:[B

    array-length v1, v1

    invoke-static {p1, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 28
    return-void
.end method


# virtual methods
.method public encode(Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;)V
    .locals 3
    .param p1, "out"    # Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 43
    const/16 v0, 0x13

    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/bcpg/ModDetectionCodePacket;->digest:[B

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v1, v2}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->writePacket(I[BZ)V

    .line 44
    return-void
.end method

.method public getDigest()[B
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 32
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/bcpg/ModDetectionCodePacket;->digest:[B

    array-length v1, v1

    new-array v0, v1, [B

    .line 34
    .local v0, "tmp":[B
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/bcpg/ModDetectionCodePacket;->digest:[B

    array-length v2, v0

    invoke-static {v1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 36
    return-object v0
.end method
