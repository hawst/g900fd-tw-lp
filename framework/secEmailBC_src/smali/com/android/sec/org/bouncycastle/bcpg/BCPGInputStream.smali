.class public Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;
.super Ljava/io/InputStream;
.source "BCPGInputStream.java"

# interfaces
.implements Lcom/android/sec/org/bouncycastle/bcpg/PacketTags;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream$PartialInputStream;
    }
.end annotation


# instance fields
.field in:Ljava/io/InputStream;

.field next:Z

.field nextB:I


# direct methods
.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 1
    .param p1, "in"    # Ljava/io/InputStream;

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    .line 16
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->next:Z

    .line 22
    iput-object p1, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->in:Ljava/io/InputStream;

    .line 23
    return-void
.end method


# virtual methods
.method public available()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 28
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->in:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->available()I

    move-result v0

    return v0
.end method

.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 266
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->in:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    .line 267
    return-void
.end method

.method public nextPacketTag()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 104
    iget-boolean v1, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->next:Z

    if-nez v1, :cond_0

    .line 108
    :try_start_0
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->in:Ljava/io/InputStream;

    invoke-virtual {v1}, Ljava/io/InputStream;->read()I

    move-result v1

    iput v1, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->nextB:I
    :try_end_0
    .catch Ljava/io/EOFException; {:try_start_0 .. :try_end_0} :catch_0

    .line 116
    :cond_0
    :goto_0
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->next:Z

    .line 118
    iget v1, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->nextB:I

    if-ltz v1, :cond_2

    .line 120
    iget v1, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->nextB:I

    and-int/lit8 v1, v1, 0x40

    if-eqz v1, :cond_1

    .line 122
    iget v1, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->nextB:I

    and-int/lit8 v1, v1, 0x3f

    .line 130
    :goto_1
    return v1

    .line 110
    :catch_0
    move-exception v0

    .line 112
    .local v0, "e":Ljava/io/EOFException;
    const/4 v1, -0x1

    iput v1, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->nextB:I

    goto :goto_0

    .line 126
    .end local v0    # "e":Ljava/io/EOFException;
    :cond_1
    iget v1, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->nextB:I

    and-int/lit8 v1, v1, 0x3f

    shr-int/lit8 v1, v1, 0x2

    goto :goto_1

    .line 130
    :cond_2
    iget v1, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->nextB:I

    goto :goto_1
.end method

.method public read()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 34
    iget-boolean v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->next:Z

    if-eqz v0, :cond_0

    .line 36
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->next:Z

    .line 38
    iget v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->nextB:I

    .line 42
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->in:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->read()I

    move-result v0

    goto :goto_0
.end method

.method public read([BII)I
    .locals 2
    .param p1, "buf"    # [B
    .param p2, "off"    # I
    .param p3, "len"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 52
    if-nez p3, :cond_0

    .line 72
    :goto_0
    return v0

    .line 57
    :cond_0
    iget-boolean v1, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->next:Z

    if-nez v1, :cond_1

    .line 59
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->in:Ljava/io/InputStream;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/InputStream;->read([BII)I

    move-result v0

    goto :goto_0

    .line 64
    :cond_1
    iget v1, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->nextB:I

    if-gez v1, :cond_2

    .line 66
    const/4 v0, -0x1

    goto :goto_0

    .line 69
    :cond_2
    iget v1, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->nextB:I

    int-to-byte v1, v1

    aput-byte v1, p1, p2

    .line 70
    iput-boolean v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->next:Z

    .line 72
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public readFully([B)V
    .locals 2
    .param p1, "buf"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 91
    const/4 v0, 0x0

    array-length v1, p1

    invoke-virtual {p0, p1, v0, v1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->readFully([BII)V

    .line 92
    return-void
.end method

.method public readFully([BII)V
    .locals 1
    .param p1, "buf"    # [B
    .param p2, "off"    # I
    .param p3, "len"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 81
    invoke-static {p0, p1, p2, p3}, Lcom/android/sec/org/bouncycastle/util/io/Streams;->readFully(Ljava/io/InputStream;[BII)I

    move-result v0

    if-ge v0, p3, :cond_0

    .line 83
    new-instance v0, Ljava/io/EOFException;

    invoke-direct {v0}, Ljava/io/EOFException;-><init>()V

    throw v0

    .line 85
    :cond_0
    return-void
.end method

.method public readPacket()Lcom/android/sec/org/bouncycastle/bcpg/Packet;
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v9, 0x1

    .line 136
    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->read()I

    move-result v2

    .line 138
    .local v2, "hdr":I
    if-gez v2, :cond_0

    .line 140
    const/4 v9, 0x0

    .line 257
    :goto_0
    return-object v9

    .line 143
    :cond_0
    and-int/lit16 v10, v2, 0x80

    if-nez v10, :cond_1

    .line 145
    new-instance v9, Ljava/io/IOException;

    const-string v10, "invalid header encountered"

    invoke-direct {v9, v10}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v9

    .line 148
    :cond_1
    and-int/lit8 v10, v2, 0x40

    if-eqz v10, :cond_2

    move v5, v9

    .line 149
    .local v5, "newPacket":Z
    :goto_1
    const/4 v8, 0x0

    .line 150
    .local v8, "tag":I
    const/4 v1, 0x0

    .line 151
    .local v1, "bodyLen":I
    const/4 v7, 0x0

    .line 153
    .local v7, "partial":Z
    if-eqz v5, :cond_6

    .line 155
    and-int/lit8 v8, v2, 0x3f

    .line 157
    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->read()I

    move-result v3

    .line 159
    .local v3, "l":I
    const/16 v10, 0xc0

    if-ge v3, v10, :cond_3

    .line 161
    move v1, v3

    .line 206
    .end local v3    # "l":I
    :goto_2
    if-nez v1, :cond_7

    if-eqz v7, :cond_7

    .line 208
    move-object v6, p0

    .line 215
    .local v6, "objStream":Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;
    :goto_3
    sparse-switch v8, :sswitch_data_0

    .line 259
    new-instance v9, Ljava/io/IOException;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "unknown packet type encountered: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v9

    .line 148
    .end local v1    # "bodyLen":I
    .end local v5    # "newPacket":Z
    .end local v6    # "objStream":Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;
    .end local v7    # "partial":Z
    .end local v8    # "tag":I
    :cond_2
    const/4 v5, 0x0

    goto :goto_1

    .line 163
    .restart local v1    # "bodyLen":I
    .restart local v3    # "l":I
    .restart local v5    # "newPacket":Z
    .restart local v7    # "partial":Z
    .restart local v8    # "tag":I
    :cond_3
    const/16 v10, 0xdf

    if-gt v3, v10, :cond_4

    .line 165
    iget-object v9, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->in:Ljava/io/InputStream;

    invoke-virtual {v9}, Ljava/io/InputStream;->read()I

    move-result v0

    .line 167
    .local v0, "b":I
    add-int/lit16 v9, v3, -0xc0

    shl-int/lit8 v9, v9, 0x8

    add-int/2addr v9, v0

    add-int/lit16 v1, v9, 0xc0

    .line 168
    goto :goto_2

    .line 169
    .end local v0    # "b":I
    :cond_4
    const/16 v10, 0xff

    if-ne v3, v10, :cond_5

    .line 171
    iget-object v9, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->in:Ljava/io/InputStream;

    invoke-virtual {v9}, Ljava/io/InputStream;->read()I

    move-result v9

    shl-int/lit8 v9, v9, 0x18

    iget-object v10, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->in:Ljava/io/InputStream;

    invoke-virtual {v10}, Ljava/io/InputStream;->read()I

    move-result v10

    shl-int/lit8 v10, v10, 0x10

    or-int/2addr v9, v10

    iget-object v10, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->in:Ljava/io/InputStream;

    invoke-virtual {v10}, Ljava/io/InputStream;->read()I

    move-result v10

    shl-int/lit8 v10, v10, 0x8

    or-int/2addr v9, v10

    iget-object v10, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->in:Ljava/io/InputStream;

    invoke-virtual {v10}, Ljava/io/InputStream;->read()I

    move-result v10

    or-int v1, v9, v10

    goto :goto_2

    .line 175
    :cond_5
    const/4 v7, 0x1

    .line 176
    and-int/lit8 v10, v3, 0x1f

    shl-int v1, v9, v10

    goto :goto_2

    .line 181
    .end local v3    # "l":I
    :cond_6
    and-int/lit8 v4, v2, 0x3

    .line 183
    .local v4, "lengthType":I
    and-int/lit8 v9, v2, 0x3f

    shr-int/lit8 v8, v9, 0x2

    .line 185
    packed-switch v4, :pswitch_data_0

    .line 200
    new-instance v9, Ljava/io/IOException;

    const-string v10, "unknown length type encountered"

    invoke-direct {v9, v10}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v9

    .line 188
    :pswitch_0
    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->read()I

    move-result v1

    .line 189
    goto :goto_2

    .line 191
    :pswitch_1
    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->read()I

    move-result v9

    shl-int/lit8 v9, v9, 0x8

    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->read()I

    move-result v10

    or-int v1, v9, v10

    .line 192
    goto/16 :goto_2

    .line 194
    :pswitch_2
    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->read()I

    move-result v9

    shl-int/lit8 v9, v9, 0x18

    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->read()I

    move-result v10

    shl-int/lit8 v10, v10, 0x10

    or-int/2addr v9, v10

    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->read()I

    move-result v10

    shl-int/lit8 v10, v10, 0x8

    or-int/2addr v9, v10

    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->read()I

    move-result v10

    or-int v1, v9, v10

    .line 195
    goto/16 :goto_2

    .line 197
    :pswitch_3
    const/4 v7, 0x1

    .line 198
    goto/16 :goto_2

    .line 212
    .end local v4    # "lengthType":I
    :cond_7
    new-instance v6, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;

    new-instance v9, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream$PartialInputStream;

    invoke-direct {v9, p0, v7, v1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream$PartialInputStream;-><init>(Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;ZI)V

    invoke-direct {v6, v9}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;-><init>(Ljava/io/InputStream;)V

    .restart local v6    # "objStream":Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;
    goto/16 :goto_3

    .line 218
    :sswitch_0
    new-instance v9, Lcom/android/sec/org/bouncycastle/bcpg/InputStreamPacket;

    invoke-direct {v9, v6}, Lcom/android/sec/org/bouncycastle/bcpg/InputStreamPacket;-><init>(Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;)V

    goto/16 :goto_0

    .line 220
    :sswitch_1
    new-instance v9, Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyEncSessionPacket;

    invoke-direct {v9, v6}, Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyEncSessionPacket;-><init>(Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;)V

    goto/16 :goto_0

    .line 222
    :sswitch_2
    new-instance v9, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;

    invoke-direct {v9, v6}, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;-><init>(Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;)V

    goto/16 :goto_0

    .line 224
    :sswitch_3
    new-instance v9, Lcom/android/sec/org/bouncycastle/bcpg/SymmetricKeyEncSessionPacket;

    invoke-direct {v9, v6}, Lcom/android/sec/org/bouncycastle/bcpg/SymmetricKeyEncSessionPacket;-><init>(Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;)V

    goto/16 :goto_0

    .line 226
    :sswitch_4
    new-instance v9, Lcom/android/sec/org/bouncycastle/bcpg/OnePassSignaturePacket;

    invoke-direct {v9, v6}, Lcom/android/sec/org/bouncycastle/bcpg/OnePassSignaturePacket;-><init>(Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;)V

    goto/16 :goto_0

    .line 228
    :sswitch_5
    new-instance v9, Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;

    invoke-direct {v9, v6}, Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;-><init>(Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;)V

    goto/16 :goto_0

    .line 230
    :sswitch_6
    new-instance v9, Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;

    invoke-direct {v9, v6}, Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;-><init>(Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;)V

    goto/16 :goto_0

    .line 232
    :sswitch_7
    new-instance v9, Lcom/android/sec/org/bouncycastle/bcpg/SecretSubkeyPacket;

    invoke-direct {v9, v6}, Lcom/android/sec/org/bouncycastle/bcpg/SecretSubkeyPacket;-><init>(Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;)V

    goto/16 :goto_0

    .line 234
    :sswitch_8
    new-instance v9, Lcom/android/sec/org/bouncycastle/bcpg/CompressedDataPacket;

    invoke-direct {v9, v6}, Lcom/android/sec/org/bouncycastle/bcpg/CompressedDataPacket;-><init>(Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;)V

    goto/16 :goto_0

    .line 236
    :sswitch_9
    new-instance v9, Lcom/android/sec/org/bouncycastle/bcpg/SymmetricEncDataPacket;

    invoke-direct {v9, v6}, Lcom/android/sec/org/bouncycastle/bcpg/SymmetricEncDataPacket;-><init>(Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;)V

    goto/16 :goto_0

    .line 238
    :sswitch_a
    new-instance v9, Lcom/android/sec/org/bouncycastle/bcpg/MarkerPacket;

    invoke-direct {v9, v6}, Lcom/android/sec/org/bouncycastle/bcpg/MarkerPacket;-><init>(Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;)V

    goto/16 :goto_0

    .line 240
    :sswitch_b
    new-instance v9, Lcom/android/sec/org/bouncycastle/bcpg/LiteralDataPacket;

    invoke-direct {v9, v6}, Lcom/android/sec/org/bouncycastle/bcpg/LiteralDataPacket;-><init>(Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;)V

    goto/16 :goto_0

    .line 242
    :sswitch_c
    new-instance v9, Lcom/android/sec/org/bouncycastle/bcpg/TrustPacket;

    invoke-direct {v9, v6}, Lcom/android/sec/org/bouncycastle/bcpg/TrustPacket;-><init>(Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;)V

    goto/16 :goto_0

    .line 244
    :sswitch_d
    new-instance v9, Lcom/android/sec/org/bouncycastle/bcpg/UserIDPacket;

    invoke-direct {v9, v6}, Lcom/android/sec/org/bouncycastle/bcpg/UserIDPacket;-><init>(Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;)V

    goto/16 :goto_0

    .line 246
    :sswitch_e
    new-instance v9, Lcom/android/sec/org/bouncycastle/bcpg/UserAttributePacket;

    invoke-direct {v9, v6}, Lcom/android/sec/org/bouncycastle/bcpg/UserAttributePacket;-><init>(Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;)V

    goto/16 :goto_0

    .line 248
    :sswitch_f
    new-instance v9, Lcom/android/sec/org/bouncycastle/bcpg/PublicSubkeyPacket;

    invoke-direct {v9, v6}, Lcom/android/sec/org/bouncycastle/bcpg/PublicSubkeyPacket;-><init>(Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;)V

    goto/16 :goto_0

    .line 250
    :sswitch_10
    new-instance v9, Lcom/android/sec/org/bouncycastle/bcpg/SymmetricEncIntegrityPacket;

    invoke-direct {v9, v6}, Lcom/android/sec/org/bouncycastle/bcpg/SymmetricEncIntegrityPacket;-><init>(Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;)V

    goto/16 :goto_0

    .line 252
    :sswitch_11
    new-instance v9, Lcom/android/sec/org/bouncycastle/bcpg/ModDetectionCodePacket;

    invoke-direct {v9, v6}, Lcom/android/sec/org/bouncycastle/bcpg/ModDetectionCodePacket;-><init>(Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;)V

    goto/16 :goto_0

    .line 257
    :sswitch_12
    new-instance v9, Lcom/android/sec/org/bouncycastle/bcpg/ExperimentalPacket;

    invoke-direct {v9, v8, v6}, Lcom/android/sec/org/bouncycastle/bcpg/ExperimentalPacket;-><init>(ILcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;)V

    goto/16 :goto_0

    .line 215
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x7 -> :sswitch_7
        0x8 -> :sswitch_8
        0x9 -> :sswitch_9
        0xa -> :sswitch_a
        0xb -> :sswitch_b
        0xc -> :sswitch_c
        0xd -> :sswitch_d
        0xe -> :sswitch_f
        0x11 -> :sswitch_e
        0x12 -> :sswitch_10
        0x13 -> :sswitch_11
        0x3c -> :sswitch_12
        0x3d -> :sswitch_12
        0x3e -> :sswitch_12
        0x3f -> :sswitch_12
    .end sparse-switch

    .line 185
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
