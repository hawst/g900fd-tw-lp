.class public Lcom/android/sec/org/bouncycastle/openpgp/PGPLiteralDataGenerator;
.super Ljava/lang/Object;
.source "PGPLiteralDataGenerator.java"

# interfaces
.implements Lcom/android/sec/org/bouncycastle/openpgp/StreamGenerator;


# static fields
.field public static final BINARY:C = 'b'

.field public static final CONSOLE:Ljava/lang/String; = "_CONSOLE"

.field public static final NOW:Ljava/util/Date;

.field public static final TEXT:C = 't'


# instance fields
.field private oldFormat:Z

.field private pkOut:Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPLiteralData;->NOW:Ljava/util/Date;

    sput-object v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPLiteralDataGenerator;->NOW:Ljava/util/Date;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPLiteralDataGenerator;->oldFormat:Z

    .line 36
    return-void
.end method

.method public constructor <init>(Z)V
    .locals 1
    .param p1, "oldFormat"    # Z

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPLiteralDataGenerator;->oldFormat:Z

    .line 47
    iput-boolean p1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPLiteralDataGenerator;->oldFormat:Z

    .line 48
    return-void
.end method

.method private writeHeader(Ljava/io/OutputStream;CLjava/lang/String;J)V
    .locals 6
    .param p1, "out"    # Ljava/io/OutputStream;
    .param p2, "format"    # C
    .param p3, "name"    # Ljava/lang/String;
    .param p4, "modificationTime"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 57
    invoke-virtual {p1, p2}, Ljava/io/OutputStream;->write(I)V

    .line 59
    invoke-static {p3}, Lcom/android/sec/org/bouncycastle/util/Strings;->toUTF8ByteArray(Ljava/lang/String;)[B

    move-result-object v0

    .line 61
    .local v0, "encName":[B
    array-length v4, v0

    int-to-byte v4, v4

    invoke-virtual {p1, v4}, Ljava/io/OutputStream;->write(I)V

    .line 63
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v4, v0

    if-eq v1, v4, :cond_0

    .line 65
    aget-byte v4, v0, v1

    invoke-virtual {p1, v4}, Ljava/io/OutputStream;->write(I)V

    .line 63
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 68
    :cond_0
    const-wide/16 v4, 0x3e8

    div-long v2, p4, v4

    .line 70
    .local v2, "modDate":J
    const/16 v4, 0x18

    shr-long v4, v2, v4

    long-to-int v4, v4

    int-to-byte v4, v4

    invoke-virtual {p1, v4}, Ljava/io/OutputStream;->write(I)V

    .line 71
    const/16 v4, 0x10

    shr-long v4, v2, v4

    long-to-int v4, v4

    int-to-byte v4, v4

    invoke-virtual {p1, v4}, Ljava/io/OutputStream;->write(I)V

    .line 72
    const/16 v4, 0x8

    shr-long v4, v2, v4

    long-to-int v4, v4

    int-to-byte v4, v4

    invoke-virtual {p1, v4}, Ljava/io/OutputStream;->write(I)V

    .line 73
    long-to-int v4, v2

    int-to-byte v4, v4

    invoke-virtual {p1, v4}, Ljava/io/OutputStream;->write(I)V

    .line 74
    return-void
.end method


# virtual methods
.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 190
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPLiteralDataGenerator;->pkOut:Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;

    if-eqz v0, :cond_0

    .line 192
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPLiteralDataGenerator;->pkOut:Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->finish()V

    .line 193
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPLiteralDataGenerator;->pkOut:Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->flush()V

    .line 194
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPLiteralDataGenerator;->pkOut:Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;

    .line 196
    :cond_0
    return-void
.end method

.method public open(Ljava/io/OutputStream;CLjava/io/File;)Ljava/io/OutputStream;
    .locals 8
    .param p1, "out"    # Ljava/io/OutputStream;
    .param p2, "format"    # C
    .param p3, "file"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 169
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPLiteralDataGenerator;->pkOut:Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;

    if-eqz v0, :cond_0

    .line 171
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "generator already in open state"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 174
    :cond_0
    new-instance v1, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;

    const/16 v3, 0xb

    invoke-virtual {p3}, Ljava/io/File;->length()J

    move-result-wide v4

    const-wide/16 v6, 0x2

    add-long/2addr v4, v6

    invoke-virtual {p3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    int-to-long v6, v0

    add-long/2addr v4, v6

    const-wide/16 v6, 0x4

    add-long/2addr v4, v6

    iget-boolean v6, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPLiteralDataGenerator;->oldFormat:Z

    move-object v2, p1

    invoke-direct/range {v1 .. v6}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;-><init>(Ljava/io/OutputStream;IJZ)V

    iput-object v1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPLiteralDataGenerator;->pkOut:Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;

    .line 176
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPLiteralDataGenerator;->pkOut:Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;

    invoke-virtual {p3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p3}, Ljava/io/File;->lastModified()J

    move-result-wide v4

    move-object v0, p0

    move v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/android/sec/org/bouncycastle/openpgp/PGPLiteralDataGenerator;->writeHeader(Ljava/io/OutputStream;CLjava/lang/String;J)V

    .line 178
    new-instance v0, Lcom/android/sec/org/bouncycastle/openpgp/WrappedGeneratorStream;

    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPLiteralDataGenerator;->pkOut:Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;

    invoke-direct {v0, v1, p0}, Lcom/android/sec/org/bouncycastle/openpgp/WrappedGeneratorStream;-><init>(Ljava/io/OutputStream;Lcom/android/sec/org/bouncycastle/openpgp/StreamGenerator;)V

    return-object v0
.end method

.method public open(Ljava/io/OutputStream;CLjava/lang/String;JLjava/util/Date;)Ljava/io/OutputStream;
    .locals 8
    .param p1, "out"    # Ljava/io/OutputStream;
    .param p2, "format"    # C
    .param p3, "name"    # Ljava/lang/String;
    .param p4, "length"    # J
    .param p6, "modificationTime"    # Ljava/util/Date;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 98
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPLiteralDataGenerator;->pkOut:Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;

    if-eqz v0, :cond_0

    .line 100
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "generator already in open state"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 103
    :cond_0
    new-instance v1, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;

    const/16 v3, 0xb

    const-wide/16 v4, 0x2

    add-long/2addr v4, p4

    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v0

    int-to-long v6, v0

    add-long/2addr v4, v6

    const-wide/16 v6, 0x4

    add-long/2addr v4, v6

    iget-boolean v6, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPLiteralDataGenerator;->oldFormat:Z

    move-object v2, p1

    invoke-direct/range {v1 .. v6}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;-><init>(Ljava/io/OutputStream;IJZ)V

    iput-object v1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPLiteralDataGenerator;->pkOut:Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;

    .line 105
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPLiteralDataGenerator;->pkOut:Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;

    invoke-virtual {p6}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    move-object v0, p0

    move v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v5}, Lcom/android/sec/org/bouncycastle/openpgp/PGPLiteralDataGenerator;->writeHeader(Ljava/io/OutputStream;CLjava/lang/String;J)V

    .line 107
    new-instance v0, Lcom/android/sec/org/bouncycastle/openpgp/WrappedGeneratorStream;

    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPLiteralDataGenerator;->pkOut:Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;

    invoke-direct {v0, v1, p0}, Lcom/android/sec/org/bouncycastle/openpgp/WrappedGeneratorStream;-><init>(Ljava/io/OutputStream;Lcom/android/sec/org/bouncycastle/openpgp/StreamGenerator;)V

    return-object v0
.end method

.method public open(Ljava/io/OutputStream;CLjava/lang/String;Ljava/util/Date;[B)Ljava/io/OutputStream;
    .locals 6
    .param p1, "out"    # Ljava/io/OutputStream;
    .param p2, "format"    # C
    .param p3, "name"    # Ljava/lang/String;
    .param p4, "modificationTime"    # Ljava/util/Date;
    .param p5, "buffer"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 137
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPLiteralDataGenerator;->pkOut:Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;

    if-eqz v0, :cond_0

    .line 139
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "generator already in open state"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 142
    :cond_0
    new-instance v0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;

    const/16 v1, 0xb

    invoke-direct {v0, p1, v1, p5}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;-><init>(Ljava/io/OutputStream;I[B)V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPLiteralDataGenerator;->pkOut:Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;

    .line 144
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPLiteralDataGenerator;->pkOut:Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;

    invoke-virtual {p4}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    move-object v0, p0

    move v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v5}, Lcom/android/sec/org/bouncycastle/openpgp/PGPLiteralDataGenerator;->writeHeader(Ljava/io/OutputStream;CLjava/lang/String;J)V

    .line 146
    new-instance v0, Lcom/android/sec/org/bouncycastle/openpgp/WrappedGeneratorStream;

    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPLiteralDataGenerator;->pkOut:Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;

    invoke-direct {v0, v1, p0}, Lcom/android/sec/org/bouncycastle/openpgp/WrappedGeneratorStream;-><init>(Ljava/io/OutputStream;Lcom/android/sec/org/bouncycastle/openpgp/StreamGenerator;)V

    return-object v0
.end method
