.class public Lcom/android/sec/org/bouncycastle/jce/spec/ElGamalPrivateKeySpec;
.super Lcom/android/sec/org/bouncycastle/jce/spec/ElGamalKeySpec;
.source "ElGamalPrivateKeySpec.java"


# instance fields
.field private x:Ljava/math/BigInteger;


# direct methods
.method public constructor <init>(Ljava/math/BigInteger;Lcom/android/sec/org/bouncycastle/jce/spec/ElGamalParameterSpec;)V
    .locals 0
    .param p1, "x"    # Ljava/math/BigInteger;
    .param p2, "spec"    # Lcom/android/sec/org/bouncycastle/jce/spec/ElGamalParameterSpec;

    .prologue
    .line 17
    invoke-direct {p0, p2}, Lcom/android/sec/org/bouncycastle/jce/spec/ElGamalKeySpec;-><init>(Lcom/android/sec/org/bouncycastle/jce/spec/ElGamalParameterSpec;)V

    .line 19
    iput-object p1, p0, Lcom/android/sec/org/bouncycastle/jce/spec/ElGamalPrivateKeySpec;->x:Ljava/math/BigInteger;

    .line 20
    return-void
.end method


# virtual methods
.method public getX()Ljava/math/BigInteger;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/jce/spec/ElGamalPrivateKeySpec;->x:Ljava/math/BigInteger;

    return-object v0
.end method
