.class public Lcom/android/sec/org/bouncycastle/bcpg/SymmetricKeyEncSessionPacket;
.super Lcom/android/sec/org/bouncycastle/bcpg/ContainedPacket;
.source "SymmetricKeyEncSessionPacket.java"


# instance fields
.field private encAlgorithm:I

.field private s2k:Lcom/android/sec/org/bouncycastle/bcpg/S2K;

.field private secKeyData:[B

.field private version:I


# direct methods
.method public constructor <init>(ILcom/android/sec/org/bouncycastle/bcpg/S2K;[B)V
    .locals 1
    .param p1, "encAlgorithm"    # I
    .param p2, "s2k"    # Lcom/android/sec/org/bouncycastle/bcpg/S2K;
    .param p3, "secKeyData"    # [B

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/bcpg/ContainedPacket;-><init>()V

    .line 38
    const/4 v0, 0x4

    iput v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/SymmetricKeyEncSessionPacket;->version:I

    .line 39
    iput p1, p0, Lcom/android/sec/org/bouncycastle/bcpg/SymmetricKeyEncSessionPacket;->encAlgorithm:I

    .line 40
    iput-object p2, p0, Lcom/android/sec/org/bouncycastle/bcpg/SymmetricKeyEncSessionPacket;->s2k:Lcom/android/sec/org/bouncycastle/bcpg/S2K;

    .line 41
    iput-object p3, p0, Lcom/android/sec/org/bouncycastle/bcpg/SymmetricKeyEncSessionPacket;->secKeyData:[B

    .line 42
    return-void
.end method

.method public constructor <init>(Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;)V
    .locals 3
    .param p1, "in"    # Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/bcpg/ContainedPacket;-><init>()V

    .line 21
    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->read()I

    move-result v0

    iput v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/SymmetricKeyEncSessionPacket;->version:I

    .line 22
    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->read()I

    move-result v0

    iput v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/SymmetricKeyEncSessionPacket;->encAlgorithm:I

    .line 24
    new-instance v0, Lcom/android/sec/org/bouncycastle/bcpg/S2K;

    invoke-direct {v0, p1}, Lcom/android/sec/org/bouncycastle/bcpg/S2K;-><init>(Ljava/io/InputStream;)V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/SymmetricKeyEncSessionPacket;->s2k:Lcom/android/sec/org/bouncycastle/bcpg/S2K;

    .line 26
    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->available()I

    move-result v0

    if-eqz v0, :cond_0

    .line 28
    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->available()I

    move-result v0

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/SymmetricKeyEncSessionPacket;->secKeyData:[B

    .line 29
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/SymmetricKeyEncSessionPacket;->secKeyData:[B

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/SymmetricKeyEncSessionPacket;->secKeyData:[B

    array-length v2, v2

    invoke-virtual {p1, v0, v1, v2}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->readFully([BII)V

    .line 31
    :cond_0
    return-void
.end method


# virtual methods
.method public encode(Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;)V
    .locals 5
    .param p1, "out"    # Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 80
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 81
    .local v0, "bOut":Ljava/io/ByteArrayOutputStream;
    new-instance v1, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;

    invoke-direct {v1, v0}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 83
    .local v1, "pOut":Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;
    iget v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/SymmetricKeyEncSessionPacket;->version:I

    invoke-virtual {v1, v2}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->write(I)V

    .line 84
    iget v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/SymmetricKeyEncSessionPacket;->encAlgorithm:I

    invoke-virtual {v1, v2}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->write(I)V

    .line 85
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/SymmetricKeyEncSessionPacket;->s2k:Lcom/android/sec/org/bouncycastle/bcpg/S2K;

    invoke-virtual {v1, v2}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->writeObject(Lcom/android/sec/org/bouncycastle/bcpg/BCPGObject;)V

    .line 87
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/SymmetricKeyEncSessionPacket;->secKeyData:[B

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/SymmetricKeyEncSessionPacket;->secKeyData:[B

    array-length v2, v2

    if-lez v2, :cond_0

    .line 89
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/SymmetricKeyEncSessionPacket;->secKeyData:[B

    invoke-virtual {v1, v2}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->write([B)V

    .line 92
    :cond_0
    const/4 v2, 0x3

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {p1, v2, v3, v4}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->writePacket(I[BZ)V

    .line 93
    return-void
.end method

.method public getEncAlgorithm()I
    .locals 1

    .prologue
    .line 49
    iget v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/SymmetricKeyEncSessionPacket;->encAlgorithm:I

    return v0
.end method

.method public getS2K()Lcom/android/sec/org/bouncycastle/bcpg/S2K;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/SymmetricKeyEncSessionPacket;->s2k:Lcom/android/sec/org/bouncycastle/bcpg/S2K;

    return-object v0
.end method

.method public getSecKeyData()[B
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/SymmetricKeyEncSessionPacket;->secKeyData:[B

    return-object v0
.end method

.method public getVersion()I
    .locals 1

    .prologue
    .line 73
    iget v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/SymmetricKeyEncSessionPacket;->version:I

    return v0
.end method
