.class public Lcom/android/sec/org/bouncycastle/cms/SimpleAttributeTableGenerator;
.super Ljava/lang/Object;
.source "SimpleAttributeTableGenerator.java"

# interfaces
.implements Lcom/android/sec/org/bouncycastle/cms/CMSAttributeTableGenerator;


# instance fields
.field private final attributes:Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;


# direct methods
.method public constructor <init>(Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;)V
    .locals 0
    .param p1, "attributes"    # Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object p1, p0, Lcom/android/sec/org/bouncycastle/cms/SimpleAttributeTableGenerator;->attributes:Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;

    .line 19
    return-void
.end method


# virtual methods
.method public getAttributes(Ljava/util/Map;)Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;
    .locals 1
    .param p1, "parameters"    # Ljava/util/Map;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/SimpleAttributeTableGenerator;->attributes:Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;

    return-object v0
.end method
