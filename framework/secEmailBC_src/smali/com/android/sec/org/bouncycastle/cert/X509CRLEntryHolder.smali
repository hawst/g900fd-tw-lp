.class public Lcom/android/sec/org/bouncycastle/cert/X509CRLEntryHolder;
.super Ljava/lang/Object;
.source "X509CRLEntryHolder.java"


# instance fields
.field private ca:Lcom/android/sec/org/bouncycastle/asn1/x509/GeneralNames;

.field private entry:Lcom/android/sec/org/bouncycastle/asn1/x509/TBSCertList$CRLEntry;


# direct methods
.method constructor <init>(Lcom/android/sec/org/bouncycastle/asn1/x509/TBSCertList$CRLEntry;ZLcom/android/sec/org/bouncycastle/asn1/x509/GeneralNames;)V
    .locals 3
    .param p1, "entry"    # Lcom/android/sec/org/bouncycastle/asn1/x509/TBSCertList$CRLEntry;
    .param p2, "isIndirect"    # Z
    .param p3, "previousCA"    # Lcom/android/sec/org/bouncycastle/asn1/x509/GeneralNames;

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/android/sec/org/bouncycastle/cert/X509CRLEntryHolder;->entry:Lcom/android/sec/org/bouncycastle/asn1/x509/TBSCertList$CRLEntry;

    .line 25
    iput-object p3, p0, Lcom/android/sec/org/bouncycastle/cert/X509CRLEntryHolder;->ca:Lcom/android/sec/org/bouncycastle/asn1/x509/GeneralNames;

    .line 27
    if-eqz p2, :cond_0

    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/asn1/x509/TBSCertList$CRLEntry;->hasExtensions()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 29
    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/asn1/x509/TBSCertList$CRLEntry;->getExtensions()Lcom/android/sec/org/bouncycastle/asn1/x509/Extensions;

    move-result-object v1

    sget-object v2, Lcom/android/sec/org/bouncycastle/asn1/x509/Extension;->certificateIssuer:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    invoke-virtual {v1, v2}, Lcom/android/sec/org/bouncycastle/asn1/x509/Extensions;->getExtension(Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;)Lcom/android/sec/org/bouncycastle/asn1/x509/Extension;

    move-result-object v0

    .line 31
    .local v0, "currentCaName":Lcom/android/sec/org/bouncycastle/asn1/x509/Extension;
    if-eqz v0, :cond_0

    .line 33
    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/x509/Extension;->getParsedValue()Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;

    move-result-object v1

    invoke-static {v1}, Lcom/android/sec/org/bouncycastle/asn1/x509/GeneralNames;->getInstance(Ljava/lang/Object;)Lcom/android/sec/org/bouncycastle/asn1/x509/GeneralNames;

    move-result-object v1

    iput-object v1, p0, Lcom/android/sec/org/bouncycastle/cert/X509CRLEntryHolder;->ca:Lcom/android/sec/org/bouncycastle/asn1/x509/GeneralNames;

    .line 36
    .end local v0    # "currentCaName":Lcom/android/sec/org/bouncycastle/asn1/x509/Extension;
    :cond_0
    return-void
.end method


# virtual methods
.method public getCertificateIssuer()Lcom/android/sec/org/bouncycastle/asn1/x509/GeneralNames;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cert/X509CRLEntryHolder;->ca:Lcom/android/sec/org/bouncycastle/asn1/x509/GeneralNames;

    return-object v0
.end method

.method public getCriticalExtensionOIDs()Ljava/util/Set;
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cert/X509CRLEntryHolder;->entry:Lcom/android/sec/org/bouncycastle/asn1/x509/TBSCertList$CRLEntry;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/x509/TBSCertList$CRLEntry;->getExtensions()Lcom/android/sec/org/bouncycastle/asn1/x509/Extensions;

    move-result-object v0

    invoke-static {v0}, Lcom/android/sec/org/bouncycastle/cert/CertUtils;->getCriticalExtensionOIDs(Lcom/android/sec/org/bouncycastle/asn1/x509/Extensions;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public getExtension(Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;)Lcom/android/sec/org/bouncycastle/asn1/x509/Extension;
    .locals 2
    .param p1, "oid"    # Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    .prologue
    .line 92
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/cert/X509CRLEntryHolder;->entry:Lcom/android/sec/org/bouncycastle/asn1/x509/TBSCertList$CRLEntry;

    invoke-virtual {v1}, Lcom/android/sec/org/bouncycastle/asn1/x509/TBSCertList$CRLEntry;->getExtensions()Lcom/android/sec/org/bouncycastle/asn1/x509/Extensions;

    move-result-object v0

    .line 94
    .local v0, "extensions":Lcom/android/sec/org/bouncycastle/asn1/x509/Extensions;
    if-eqz v0, :cond_0

    .line 96
    invoke-virtual {v0, p1}, Lcom/android/sec/org/bouncycastle/asn1/x509/Extensions;->getExtension(Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;)Lcom/android/sec/org/bouncycastle/asn1/x509/Extension;

    move-result-object v1

    .line 99
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getExtensionOIDs()Ljava/util/List;
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cert/X509CRLEntryHolder;->entry:Lcom/android/sec/org/bouncycastle/asn1/x509/TBSCertList$CRLEntry;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/x509/TBSCertList$CRLEntry;->getExtensions()Lcom/android/sec/org/bouncycastle/asn1/x509/Extensions;

    move-result-object v0

    invoke-static {v0}, Lcom/android/sec/org/bouncycastle/cert/CertUtils;->getExtensionOIDs(Lcom/android/sec/org/bouncycastle/asn1/x509/Extensions;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getExtensions()Lcom/android/sec/org/bouncycastle/asn1/x509/Extensions;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cert/X509CRLEntryHolder;->entry:Lcom/android/sec/org/bouncycastle/asn1/x509/TBSCertList$CRLEntry;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/x509/TBSCertList$CRLEntry;->getExtensions()Lcom/android/sec/org/bouncycastle/asn1/x509/Extensions;

    move-result-object v0

    return-object v0
.end method

.method public getNonCriticalExtensionOIDs()Ljava/util/Set;
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cert/X509CRLEntryHolder;->entry:Lcom/android/sec/org/bouncycastle/asn1/x509/TBSCertList$CRLEntry;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/x509/TBSCertList$CRLEntry;->getExtensions()Lcom/android/sec/org/bouncycastle/asn1/x509/Extensions;

    move-result-object v0

    invoke-static {v0}, Lcom/android/sec/org/bouncycastle/cert/CertUtils;->getNonCriticalExtensionOIDs(Lcom/android/sec/org/bouncycastle/asn1/x509/Extensions;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public getRevocationDate()Ljava/util/Date;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cert/X509CRLEntryHolder;->entry:Lcom/android/sec/org/bouncycastle/asn1/x509/TBSCertList$CRLEntry;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/x509/TBSCertList$CRLEntry;->getRevocationDate()Lcom/android/sec/org/bouncycastle/asn1/x509/Time;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/x509/Time;->getDate()Ljava/util/Date;

    move-result-object v0

    return-object v0
.end method

.method public getSerialNumber()Ljava/math/BigInteger;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cert/X509CRLEntryHolder;->entry:Lcom/android/sec/org/bouncycastle/asn1/x509/TBSCertList$CRLEntry;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/x509/TBSCertList$CRLEntry;->getUserCertificate()Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;->getValue()Ljava/math/BigInteger;

    move-result-object v0

    return-object v0
.end method

.method public hasExtensions()Z
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cert/X509CRLEntryHolder;->entry:Lcom/android/sec/org/bouncycastle/asn1/x509/TBSCertList$CRLEntry;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/x509/TBSCertList$CRLEntry;->hasExtensions()Z

    move-result v0

    return v0
.end method
