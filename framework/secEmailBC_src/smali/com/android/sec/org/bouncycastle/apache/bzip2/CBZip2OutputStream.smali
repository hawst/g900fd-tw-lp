.class public Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;
.super Ljava/io/OutputStream;
.source "CBZip2OutputStream.java"

# interfaces
.implements Lcom/android/sec/org/bouncycastle/apache/bzip2/BZip2Constants;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream$1;,
        Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream$StackElem;
    }
.end annotation


# static fields
.field protected static final CLEARMASK:I = -0x200001

.field protected static final DEPTH_THRESH:I = 0xa

.field protected static final GREATER_ICOST:I = 0xf

.field protected static final LESSER_ICOST:I = 0x0

.field protected static final QSORT_STACK_SIZE:I = 0x3e8

.field protected static final SETMASK:I = 0x200000

.field protected static final SMALL_THRESH:I = 0x14


# instance fields
.field private allowableBlockSize:I

.field private block:[C

.field private blockCRC:I

.field blockRandomised:Z

.field blockSize100k:I

.field bsBuff:I

.field bsLive:I

.field private bsStream:Ljava/io/OutputStream;

.field bytesOut:I

.field closed:Z

.field private combinedCRC:I

.field private currentChar:I

.field private finished:Z

.field private firstAttempt:Z

.field private ftab:[I

.field private inUse:[Z

.field private incs:[I

.field last:I

.field mCrc:Lcom/android/sec/org/bouncycastle/apache/bzip2/CRC;

.field private mtfFreq:[I

.field private nBlocksRandomised:I

.field private nInUse:I

.field private nMTF:I

.field origPtr:I

.field private quadrant:[I

.field private runLength:I

.field private selector:[C

.field private selectorMtf:[C

.field private seqToUnseq:[C

.field private szptr:[S

.field private unseqToSeq:[C

.field private workDone:I

.field private workFactor:I

.field private workLimit:I

.field private zptr:[I


# direct methods
.method public constructor <init>(Ljava/io/OutputStream;)V
    .locals 1
    .param p1, "inStream"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 281
    const/16 v0, 0x9

    invoke-direct {p0, p1, v0}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;-><init>(Ljava/io/OutputStream;I)V

    .line 282
    return-void
.end method

.method public constructor <init>(Ljava/io/OutputStream;I)V
    .locals 5
    .param p1, "inStream"    # Ljava/io/OutputStream;
    .param p2, "inBlockSize"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v4, 0x4652

    const/4 v3, 0x0

    const/16 v2, 0x100

    const/4 v1, 0x0

    .line 285
    invoke-direct {p0}, Ljava/io/OutputStream;-><init>()V

    .line 245
    new-instance v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CRC;

    invoke-direct {v0}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CRC;-><init>()V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->mCrc:Lcom/android/sec/org/bouncycastle/apache/bzip2/CRC;

    .line 247
    new-array v0, v2, [Z

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->inUse:[Z

    .line 250
    new-array v0, v2, [C

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->seqToUnseq:[C

    .line 251
    new-array v0, v2, [C

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->unseqToSeq:[C

    .line 253
    new-array v0, v4, [C

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->selector:[C

    .line 254
    new-array v0, v4, [C

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->selectorMtf:[C

    .line 264
    const/16 v0, 0x102

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->mtfFreq:[I

    .line 277
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->currentChar:I

    .line 278
    iput v3, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->runLength:I

    .line 381
    iput-boolean v3, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->closed:Z

    .line 1519
    const/16 v0, 0xe

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->incs:[I

    .line 286
    iput-object v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->block:[C

    .line 287
    iput-object v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->quadrant:[I

    .line 288
    iput-object v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->zptr:[I

    .line 289
    iput-object v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->ftab:[I

    .line 291
    const/16 v0, 0x42

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write(I)V

    .line 292
    const/16 v0, 0x5a

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write(I)V

    .line 294
    invoke-direct {p0, p1}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->bsSetStream(Ljava/io/OutputStream;)V

    .line 296
    const/16 v0, 0x32

    iput v0, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->workFactor:I

    .line 297
    const/16 v0, 0x9

    if-le p2, v0, :cond_0

    .line 298
    const/16 p2, 0x9

    .line 300
    :cond_0
    const/4 v0, 0x1

    if-ge p2, v0, :cond_1

    .line 301
    const/4 p2, 0x1

    .line 303
    :cond_1
    iput p2, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->blockSize100k:I

    .line 304
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->allocateCompressStructures()V

    .line 305
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->initialize()V

    .line 306
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->initBlock()V

    .line 307
    return-void

    .line 1519
    nop

    :array_0
    .array-data 4
        0x1
        0x4
        0xd
        0x28
        0x79
        0x16c
        0x445
        0xcd0
        0x2671
        0x7354
        0x159fd
        0x40df8
        0xc29e9
        0x247dbc
    .end array-data
.end method

.method private allocateCompressStructures()V
    .locals 3

    .prologue
    .line 1524
    const v1, 0x186a0

    iget v2, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->blockSize100k:I

    mul-int v0, v1, v2

    .line 1525
    .local v0, "n":I
    add-int/lit8 v1, v0, 0x1

    add-int/lit8 v1, v1, 0x14

    new-array v1, v1, [C

    iput-object v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->block:[C

    .line 1526
    add-int/lit8 v1, v0, 0x14

    new-array v1, v1, [I

    iput-object v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->quadrant:[I

    .line 1527
    new-array v1, v0, [I

    iput-object v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->zptr:[I

    .line 1528
    const v1, 0x10001

    new-array v1, v1, [I

    iput-object v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->ftab:[I

    .line 1530
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->block:[C

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->quadrant:[I

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->zptr:[I

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->ftab:[I

    if-nez v1, :cond_0

    .line 1549
    :cond_0
    mul-int/lit8 v1, v0, 0x2

    new-array v1, v1, [S

    iput-object v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->szptr:[S

    .line 1550
    return-void
.end method

.method private bsFinishedWithStream()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 538
    :goto_0
    iget v2, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->bsLive:I

    if-lez v2, :cond_0

    .line 539
    iget v2, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->bsBuff:I

    shr-int/lit8 v0, v2, 0x18

    .line 541
    .local v0, "ch":I
    :try_start_0
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->bsStream:Ljava/io/OutputStream;

    invoke-virtual {v2, v0}, Ljava/io/OutputStream;->write(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 545
    iget v2, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->bsBuff:I

    shl-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->bsBuff:I

    .line 546
    iget v2, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->bsLive:I

    add-int/lit8 v2, v2, -0x8

    iput v2, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->bsLive:I

    .line 547
    iget v2, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->bytesOut:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->bytesOut:I

    goto :goto_0

    .line 542
    :catch_0
    move-exception v1

    .line 543
    .local v1, "e":Ljava/io/IOException;
    throw v1

    .line 549
    .end local v0    # "ch":I
    .end local v1    # "e":Ljava/io/IOException;
    :cond_0
    return-void
.end method

.method private bsPutIntVS(II)V
    .locals 0
    .param p1, "numBits"    # I
    .param p2, "c"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 579
    invoke-direct {p0, p1, p2}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->bsW(II)V

    .line 580
    return-void
.end method

.method private bsPutUChar(I)V
    .locals 1
    .param p1, "c"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 568
    const/16 v0, 0x8

    invoke-direct {p0, v0, p1}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->bsW(II)V

    .line 569
    return-void
.end method

.method private bsPutint(I)V
    .locals 2
    .param p1, "u"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v1, 0x8

    .line 572
    shr-int/lit8 v0, p1, 0x18

    and-int/lit16 v0, v0, 0xff

    invoke-direct {p0, v1, v0}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->bsW(II)V

    .line 573
    shr-int/lit8 v0, p1, 0x10

    and-int/lit16 v0, v0, 0xff

    invoke-direct {p0, v1, v0}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->bsW(II)V

    .line 574
    shr-int/lit8 v0, p1, 0x8

    and-int/lit16 v0, v0, 0xff

    invoke-direct {p0, v1, v0}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->bsW(II)V

    .line 575
    and-int/lit16 v0, p1, 0xff

    invoke-direct {p0, v1, v0}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->bsW(II)V

    .line 576
    return-void
.end method

.method private bsSetStream(Ljava/io/OutputStream;)V
    .locals 1
    .param p1, "f"    # Ljava/io/OutputStream;

    .prologue
    const/4 v0, 0x0

    .line 531
    iput-object p1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->bsStream:Ljava/io/OutputStream;

    .line 532
    iput v0, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->bsLive:I

    .line 533
    iput v0, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->bsBuff:I

    .line 534
    iput v0, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->bytesOut:I

    .line 535
    return-void
.end method

.method private bsW(II)V
    .locals 4
    .param p1, "n"    # I
    .param p2, "v"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 552
    :goto_0
    iget v2, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->bsLive:I

    const/16 v3, 0x8

    if-lt v2, v3, :cond_0

    .line 553
    iget v2, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->bsBuff:I

    shr-int/lit8 v0, v2, 0x18

    .line 555
    .local v0, "ch":I
    :try_start_0
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->bsStream:Ljava/io/OutputStream;

    invoke-virtual {v2, v0}, Ljava/io/OutputStream;->write(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 559
    iget v2, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->bsBuff:I

    shl-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->bsBuff:I

    .line 560
    iget v2, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->bsLive:I

    add-int/lit8 v2, v2, -0x8

    iput v2, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->bsLive:I

    .line 561
    iget v2, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->bytesOut:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->bytesOut:I

    goto :goto_0

    .line 556
    :catch_0
    move-exception v1

    .line 557
    .local v1, "e":Ljava/io/IOException;
    throw v1

    .line 563
    .end local v0    # "ch":I
    .end local v1    # "e":Ljava/io/IOException;
    :cond_0
    iget v2, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->bsBuff:I

    iget v3, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->bsLive:I

    rsub-int/lit8 v3, v3, 0x20

    sub-int/2addr v3, p1

    shl-int v3, p2, v3

    or-int/2addr v2, v3

    iput v2, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->bsBuff:I

    .line 564
    iget v2, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->bsLive:I

    add-int/2addr v2, p1

    iput v2, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->bsLive:I

    .line 565
    return-void
.end method

.method private doReversibleTransformation()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, -0x1

    const/4 v3, 0x0

    .line 1361
    iget v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->workFactor:I

    iget v2, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->last:I

    mul-int/2addr v1, v2

    iput v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->workLimit:I

    .line 1362
    iput v3, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->workDone:I

    .line 1363
    iput-boolean v3, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->blockRandomised:Z

    .line 1364
    iput-boolean v5, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->firstAttempt:Z

    .line 1366
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->mainSort()V

    .line 1368
    iget v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->workDone:I

    iget v2, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->workLimit:I

    if-le v1, v2, :cond_0

    iget-boolean v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->firstAttempt:Z

    if-eqz v1, :cond_0

    .line 1369
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->randomiseBlock()V

    .line 1370
    iput v3, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->workDone:I

    iput v3, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->workLimit:I

    .line 1371
    iput-boolean v5, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->blockRandomised:Z

    .line 1372
    iput-boolean v3, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->firstAttempt:Z

    .line 1373
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->mainSort()V

    .line 1376
    :cond_0
    iput v4, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->origPtr:I

    .line 1377
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->last:I

    if-gt v0, v1, :cond_1

    .line 1378
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->zptr:[I

    aget v1, v1, v0

    if-nez v1, :cond_3

    .line 1379
    iput v0, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->origPtr:I

    .line 1384
    :cond_1
    iget v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->origPtr:I

    if-ne v1, v4, :cond_2

    .line 1385
    invoke-static {}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->panic()V

    .line 1387
    :cond_2
    return-void

    .line 1377
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private endBlock()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v3, 0x59

    const/4 v2, 0x1

    .line 452
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->mCrc:Lcom/android/sec/org/bouncycastle/apache/bzip2/CRC;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CRC;->getFinalCRC()I

    move-result v0

    iput v0, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->blockCRC:I

    .line 453
    iget v0, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->combinedCRC:I

    shl-int/lit8 v0, v0, 0x1

    iget v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->combinedCRC:I

    ushr-int/lit8 v1, v1, 0x1f

    or-int/2addr v0, v1

    iput v0, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->combinedCRC:I

    .line 454
    iget v0, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->combinedCRC:I

    iget v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->blockCRC:I

    xor-int/2addr v0, v1

    iput v0, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->combinedCRC:I

    .line 457
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->doReversibleTransformation()V

    .line 472
    const/16 v0, 0x31

    invoke-direct {p0, v0}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->bsPutUChar(I)V

    .line 473
    const/16 v0, 0x41

    invoke-direct {p0, v0}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->bsPutUChar(I)V

    .line 474
    invoke-direct {p0, v3}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->bsPutUChar(I)V

    .line 475
    const/16 v0, 0x26

    invoke-direct {p0, v0}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->bsPutUChar(I)V

    .line 476
    const/16 v0, 0x53

    invoke-direct {p0, v0}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->bsPutUChar(I)V

    .line 477
    invoke-direct {p0, v3}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->bsPutUChar(I)V

    .line 480
    iget v0, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->blockCRC:I

    invoke-direct {p0, v0}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->bsPutint(I)V

    .line 483
    iget-boolean v0, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->blockRandomised:Z

    if-eqz v0, :cond_0

    .line 484
    invoke-direct {p0, v2, v2}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->bsW(II)V

    .line 485
    iget v0, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->nBlocksRandomised:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->nBlocksRandomised:I

    .line 491
    :goto_0
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->moveToFrontCodeAndSend()V

    .line 492
    return-void

    .line 487
    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v2, v0}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->bsW(II)V

    goto :goto_0
.end method

.method private endCompression()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 502
    const/16 v0, 0x17

    invoke-direct {p0, v0}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->bsPutUChar(I)V

    .line 503
    const/16 v0, 0x72

    invoke-direct {p0, v0}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->bsPutUChar(I)V

    .line 504
    const/16 v0, 0x45

    invoke-direct {p0, v0}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->bsPutUChar(I)V

    .line 505
    const/16 v0, 0x38

    invoke-direct {p0, v0}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->bsPutUChar(I)V

    .line 506
    const/16 v0, 0x50

    invoke-direct {p0, v0}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->bsPutUChar(I)V

    .line 507
    const/16 v0, 0x90

    invoke-direct {p0, v0}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->bsPutUChar(I)V

    .line 509
    iget v0, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->combinedCRC:I

    invoke-direct {p0, v0}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->bsPutint(I)V

    .line 511
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->bsFinishedWithStream()V

    .line 512
    return-void
.end method

.method private fullGtU(II)Z
    .locals 9
    .param p1, "i1"    # I
    .param p2, "i2"    # I

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 1394
    iget-object v7, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->block:[C

    add-int/lit8 v8, p1, 0x1

    aget-char v0, v7, v8

    .line 1395
    .local v0, "c1":C
    iget-object v7, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->block:[C

    add-int/lit8 v8, p2, 0x1

    aget-char v1, v7, v8

    .line 1396
    .local v1, "c2":C
    if-eq v0, v1, :cond_2

    .line 1397
    if-le v0, v1, :cond_1

    .line 1510
    :cond_0
    :goto_0
    return v5

    :cond_1
    move v5, v6

    .line 1397
    goto :goto_0

    .line 1399
    :cond_2
    add-int/lit8 p1, p1, 0x1

    .line 1400
    add-int/lit8 p2, p2, 0x1

    .line 1402
    iget-object v7, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->block:[C

    add-int/lit8 v8, p1, 0x1

    aget-char v0, v7, v8

    .line 1403
    iget-object v7, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->block:[C

    add-int/lit8 v8, p2, 0x1

    aget-char v1, v7, v8

    .line 1404
    if-eq v0, v1, :cond_3

    .line 1405
    if-gt v0, v1, :cond_0

    move v5, v6

    goto :goto_0

    .line 1407
    :cond_3
    add-int/lit8 p1, p1, 0x1

    .line 1408
    add-int/lit8 p2, p2, 0x1

    .line 1410
    iget-object v7, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->block:[C

    add-int/lit8 v8, p1, 0x1

    aget-char v0, v7, v8

    .line 1411
    iget-object v7, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->block:[C

    add-int/lit8 v8, p2, 0x1

    aget-char v1, v7, v8

    .line 1412
    if-eq v0, v1, :cond_4

    .line 1413
    if-gt v0, v1, :cond_0

    move v5, v6

    goto :goto_0

    .line 1415
    :cond_4
    add-int/lit8 p1, p1, 0x1

    .line 1416
    add-int/lit8 p2, p2, 0x1

    .line 1418
    iget-object v7, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->block:[C

    add-int/lit8 v8, p1, 0x1

    aget-char v0, v7, v8

    .line 1419
    iget-object v7, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->block:[C

    add-int/lit8 v8, p2, 0x1

    aget-char v1, v7, v8

    .line 1420
    if-eq v0, v1, :cond_5

    .line 1421
    if-gt v0, v1, :cond_0

    move v5, v6

    goto :goto_0

    .line 1423
    :cond_5
    add-int/lit8 p1, p1, 0x1

    .line 1424
    add-int/lit8 p2, p2, 0x1

    .line 1426
    iget-object v7, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->block:[C

    add-int/lit8 v8, p1, 0x1

    aget-char v0, v7, v8

    .line 1427
    iget-object v7, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->block:[C

    add-int/lit8 v8, p2, 0x1

    aget-char v1, v7, v8

    .line 1428
    if-eq v0, v1, :cond_6

    .line 1429
    if-gt v0, v1, :cond_0

    move v5, v6

    goto :goto_0

    .line 1431
    :cond_6
    add-int/lit8 p1, p1, 0x1

    .line 1432
    add-int/lit8 p2, p2, 0x1

    .line 1434
    iget-object v7, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->block:[C

    add-int/lit8 v8, p1, 0x1

    aget-char v0, v7, v8

    .line 1435
    iget-object v7, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->block:[C

    add-int/lit8 v8, p2, 0x1

    aget-char v1, v7, v8

    .line 1436
    if-eq v0, v1, :cond_7

    .line 1437
    if-gt v0, v1, :cond_0

    move v5, v6

    goto :goto_0

    .line 1439
    :cond_7
    add-int/lit8 p1, p1, 0x1

    .line 1440
    add-int/lit8 p2, p2, 0x1

    .line 1442
    iget v7, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->last:I

    add-int/lit8 v2, v7, 0x1

    .line 1445
    .local v2, "k":I
    :cond_8
    iget-object v7, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->block:[C

    add-int/lit8 v8, p1, 0x1

    aget-char v0, v7, v8

    .line 1446
    iget-object v7, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->block:[C

    add-int/lit8 v8, p2, 0x1

    aget-char v1, v7, v8

    .line 1447
    if-eq v0, v1, :cond_9

    .line 1448
    if-gt v0, v1, :cond_0

    move v5, v6

    goto/16 :goto_0

    .line 1450
    :cond_9
    iget-object v7, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->quadrant:[I

    aget v3, v7, p1

    .line 1451
    .local v3, "s1":I
    iget-object v7, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->quadrant:[I

    aget v4, v7, p2

    .line 1452
    .local v4, "s2":I
    if-eq v3, v4, :cond_a

    .line 1453
    if-gt v3, v4, :cond_0

    move v5, v6

    goto/16 :goto_0

    .line 1455
    :cond_a
    add-int/lit8 p1, p1, 0x1

    .line 1456
    add-int/lit8 p2, p2, 0x1

    .line 1458
    iget-object v7, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->block:[C

    add-int/lit8 v8, p1, 0x1

    aget-char v0, v7, v8

    .line 1459
    iget-object v7, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->block:[C

    add-int/lit8 v8, p2, 0x1

    aget-char v1, v7, v8

    .line 1460
    if-eq v0, v1, :cond_b

    .line 1461
    if-gt v0, v1, :cond_0

    move v5, v6

    goto/16 :goto_0

    .line 1463
    :cond_b
    iget-object v7, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->quadrant:[I

    aget v3, v7, p1

    .line 1464
    iget-object v7, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->quadrant:[I

    aget v4, v7, p2

    .line 1465
    if-eq v3, v4, :cond_c

    .line 1466
    if-gt v3, v4, :cond_0

    move v5, v6

    goto/16 :goto_0

    .line 1468
    :cond_c
    add-int/lit8 p1, p1, 0x1

    .line 1469
    add-int/lit8 p2, p2, 0x1

    .line 1471
    iget-object v7, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->block:[C

    add-int/lit8 v8, p1, 0x1

    aget-char v0, v7, v8

    .line 1472
    iget-object v7, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->block:[C

    add-int/lit8 v8, p2, 0x1

    aget-char v1, v7, v8

    .line 1473
    if-eq v0, v1, :cond_d

    .line 1474
    if-gt v0, v1, :cond_0

    move v5, v6

    goto/16 :goto_0

    .line 1476
    :cond_d
    iget-object v7, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->quadrant:[I

    aget v3, v7, p1

    .line 1477
    iget-object v7, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->quadrant:[I

    aget v4, v7, p2

    .line 1478
    if-eq v3, v4, :cond_e

    .line 1479
    if-gt v3, v4, :cond_0

    move v5, v6

    goto/16 :goto_0

    .line 1481
    :cond_e
    add-int/lit8 p1, p1, 0x1

    .line 1482
    add-int/lit8 p2, p2, 0x1

    .line 1484
    iget-object v7, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->block:[C

    add-int/lit8 v8, p1, 0x1

    aget-char v0, v7, v8

    .line 1485
    iget-object v7, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->block:[C

    add-int/lit8 v8, p2, 0x1

    aget-char v1, v7, v8

    .line 1486
    if-eq v0, v1, :cond_f

    .line 1487
    if-gt v0, v1, :cond_0

    move v5, v6

    goto/16 :goto_0

    .line 1489
    :cond_f
    iget-object v7, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->quadrant:[I

    aget v3, v7, p1

    .line 1490
    iget-object v7, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->quadrant:[I

    aget v4, v7, p2

    .line 1491
    if-eq v3, v4, :cond_10

    .line 1492
    if-gt v3, v4, :cond_0

    move v5, v6

    goto/16 :goto_0

    .line 1494
    :cond_10
    add-int/lit8 p1, p1, 0x1

    .line 1495
    add-int/lit8 p2, p2, 0x1

    .line 1497
    iget v7, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->last:I

    if-le p1, v7, :cond_11

    .line 1498
    iget v7, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->last:I

    sub-int/2addr p1, v7

    .line 1499
    add-int/lit8 p1, p1, -0x1

    .line 1501
    :cond_11
    iget v7, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->last:I

    if-le p2, v7, :cond_12

    .line 1502
    iget v7, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->last:I

    sub-int/2addr p2, v7

    .line 1503
    add-int/lit8 p2, p2, -0x1

    .line 1506
    :cond_12
    add-int/lit8 v2, v2, -0x4

    .line 1507
    iget v7, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->workDone:I

    add-int/lit8 v7, v7, 0x1

    iput v7, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->workDone:I

    .line 1508
    if-gez v2, :cond_8

    move v5, v6

    .line 1510
    goto/16 :goto_0
.end method

.method private generateMTFValues()V
    .locals 15

    .prologue
    const/4 v14, 0x2

    const/4 v13, 0x1

    const/4 v12, 0x0

    .line 1553
    const/16 v9, 0x100

    new-array v7, v9, [C

    .line 1561
    .local v7, "yy":[C
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->makeMaps()V

    .line 1562
    iget v9, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->nInUse:I

    add-int/lit8 v0, v9, 0x1

    .line 1564
    .local v0, "EOB":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-gt v1, v0, :cond_0

    .line 1565
    iget-object v9, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->mtfFreq:[I

    aput v12, v9, v1

    .line 1564
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1568
    :cond_0
    const/4 v6, 0x0

    .line 1569
    .local v6, "wr":I
    const/4 v8, 0x0

    .line 1570
    .local v8, "zPend":I
    const/4 v1, 0x0

    :goto_1
    iget v9, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->nInUse:I

    if-ge v1, v9, :cond_1

    .line 1571
    int-to-char v9, v1

    aput-char v9, v7, v1

    .line 1570
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1575
    :cond_1
    const/4 v1, 0x0

    :goto_2
    iget v9, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->last:I

    if-gt v1, v9, :cond_6

    .line 1578
    iget-object v9, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->unseqToSeq:[C

    iget-object v10, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->block:[C

    iget-object v11, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->zptr:[I

    aget v11, v11, v1

    aget-char v10, v10, v11

    aget-char v3, v9, v10

    .line 1580
    .local v3, "ll_i":C
    const/4 v2, 0x0

    .line 1581
    .local v2, "j":I
    aget-char v4, v7, v2

    .line 1582
    .local v4, "tmp":C
    :goto_3
    if-eq v3, v4, :cond_2

    .line 1583
    add-int/lit8 v2, v2, 0x1

    .line 1584
    move v5, v4

    .line 1585
    .local v5, "tmp2":C
    aget-char v4, v7, v2

    .line 1586
    aput-char v5, v7, v2

    goto :goto_3

    .line 1588
    .end local v5    # "tmp2":C
    :cond_2
    aput-char v4, v7, v12

    .line 1590
    if-nez v2, :cond_3

    .line 1591
    add-int/lit8 v8, v8, 0x1

    .line 1575
    :goto_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1593
    :cond_3
    if-lez v8, :cond_4

    .line 1594
    add-int/lit8 v8, v8, -0x1

    .line 1596
    :goto_5
    rem-int/lit8 v9, v8, 0x2

    packed-switch v9, :pswitch_data_0

    .line 1608
    :goto_6
    if-ge v8, v14, :cond_5

    .line 1613
    const/4 v8, 0x0

    .line 1615
    :cond_4
    iget-object v9, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->szptr:[S

    add-int/lit8 v10, v2, 0x1

    int-to-short v10, v10

    aput-short v10, v9, v6

    .line 1616
    add-int/lit8 v6, v6, 0x1

    .line 1617
    iget-object v9, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->mtfFreq:[I

    add-int/lit8 v10, v2, 0x1

    aget v11, v9, v10

    add-int/lit8 v11, v11, 0x1

    aput v11, v9, v10

    goto :goto_4

    .line 1598
    :pswitch_0
    iget-object v9, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->szptr:[S

    aput-short v12, v9, v6

    .line 1599
    add-int/lit8 v6, v6, 0x1

    .line 1600
    iget-object v9, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->mtfFreq:[I

    aget v10, v9, v12

    add-int/lit8 v10, v10, 0x1

    aput v10, v9, v12

    goto :goto_6

    .line 1603
    :pswitch_1
    iget-object v9, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->szptr:[S

    aput-short v13, v9, v6

    .line 1604
    add-int/lit8 v6, v6, 0x1

    .line 1605
    iget-object v9, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->mtfFreq:[I

    aget v10, v9, v13

    add-int/lit8 v10, v10, 0x1

    aput v10, v9, v13

    goto :goto_6

    .line 1611
    :cond_5
    add-int/lit8 v9, v8, -0x2

    div-int/lit8 v8, v9, 0x2

    goto :goto_5

    .line 1621
    .end local v2    # "j":I
    .end local v3    # "ll_i":C
    .end local v4    # "tmp":C
    :cond_6
    if-lez v8, :cond_7

    .line 1622
    add-int/lit8 v8, v8, -0x1

    .line 1624
    :goto_7
    rem-int/lit8 v9, v8, 0x2

    packed-switch v9, :pswitch_data_1

    .line 1636
    :goto_8
    if-ge v8, v14, :cond_8

    .line 1643
    :cond_7
    iget-object v9, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->szptr:[S

    int-to-short v10, v0

    aput-short v10, v9, v6

    .line 1644
    add-int/lit8 v6, v6, 0x1

    .line 1645
    iget-object v9, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->mtfFreq:[I

    aget v10, v9, v0

    add-int/lit8 v10, v10, 0x1

    aput v10, v9, v0

    .line 1647
    iput v6, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->nMTF:I

    .line 1648
    return-void

    .line 1626
    :pswitch_2
    iget-object v9, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->szptr:[S

    aput-short v12, v9, v6

    .line 1627
    add-int/lit8 v6, v6, 0x1

    .line 1628
    iget-object v9, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->mtfFreq:[I

    aget v10, v9, v12

    add-int/lit8 v10, v10, 0x1

    aput v10, v9, v12

    goto :goto_8

    .line 1631
    :pswitch_3
    iget-object v9, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->szptr:[S

    aput-short v13, v9, v6

    .line 1632
    add-int/lit8 v6, v6, 0x1

    .line 1633
    iget-object v9, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->mtfFreq:[I

    aget v10, v9, v13

    add-int/lit8 v10, v10, 0x1

    aput v10, v9, v13

    goto :goto_8

    .line 1639
    :cond_8
    add-int/lit8 v9, v8, -0x2

    div-int/lit8 v8, v9, 0x2

    goto :goto_7

    .line 1596
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 1624
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private hbAssignCodes([I[CIII)V
    .locals 4
    .param p1, "code"    # [I
    .param p2, "length"    # [C
    .param p3, "minLen"    # I
    .param p4, "maxLen"    # I
    .param p5, "alphaSize"    # I

    .prologue
    .line 518
    const/4 v2, 0x0

    .line 519
    .local v2, "vec":I
    move v1, p3

    .local v1, "n":I
    :goto_0
    if-gt v1, p4, :cond_2

    .line 520
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-ge v0, p5, :cond_1

    .line 521
    aget-char v3, p2, v0

    if-ne v3, v1, :cond_0

    .line 522
    aput v2, p1, v0

    .line 523
    add-int/lit8 v2, v2, 0x1

    .line 520
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 526
    :cond_1
    shl-int/lit8 v2, v2, 0x1

    .line 519
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 528
    .end local v0    # "i":I
    :cond_2
    return-void
.end method

.method protected static hbMakeCodeLengths([C[III)V
    .locals 18
    .param p0, "len"    # [C
    .param p1, "freq"    # [I
    .param p2, "alphaSize"    # I
    .param p3, "maxLen"    # I

    .prologue
    .line 85
    const/16 v15, 0x104

    new-array v1, v15, [I

    .line 86
    .local v1, "heap":[I
    const/16 v15, 0x204

    new-array v12, v15, [I

    .line 87
    .local v12, "weight":[I
    const/16 v15, 0x204

    new-array v9, v15, [I

    .line 89
    .local v9, "parent":[I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    move/from16 v0, p2

    if-ge v2, v0, :cond_1

    .line 90
    add-int/lit8 v16, v2, 0x1

    aget v15, p1, v2

    if-nez v15, :cond_0

    const/4 v15, 0x1

    :goto_1
    shl-int/lit8 v15, v15, 0x8

    aput v15, v12, v16

    .line 89
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 90
    :cond_0
    aget v15, p1, v2

    goto :goto_1

    .line 94
    :cond_1
    move/from16 v8, p2

    .line 95
    .local v8, "nNodes":I
    const/4 v7, 0x0

    .line 97
    .local v7, "nHeap":I
    const/4 v15, 0x0

    const/16 v16, 0x0

    aput v16, v1, v15

    .line 98
    const/4 v15, 0x0

    const/16 v16, 0x0

    aput v16, v12, v15

    .line 99
    const/4 v15, 0x0

    const/16 v16, -0x2

    aput v16, v9, v15

    .line 101
    const/4 v2, 0x1

    :goto_2
    move/from16 v0, p2

    if-gt v2, v0, :cond_3

    .line 102
    const/4 v15, -0x1

    aput v15, v9, v2

    .line 103
    add-int/lit8 v7, v7, 0x1

    .line 104
    aput v2, v1, v7

    .line 107
    move v14, v7

    .line 108
    .local v14, "zz":I
    aget v10, v1, v14

    .line 109
    .local v10, "tmp":I
    :goto_3
    aget v15, v12, v10

    shr-int/lit8 v16, v14, 0x1

    aget v16, v1, v16

    aget v16, v12, v16

    move/from16 v0, v16

    if-ge v15, v0, :cond_2

    .line 110
    shr-int/lit8 v15, v14, 0x1

    aget v15, v1, v15

    aput v15, v1, v14

    .line 111
    shr-int/lit8 v14, v14, 0x1

    goto :goto_3

    .line 113
    :cond_2
    aput v10, v1, v14

    .line 101
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 116
    .end local v10    # "tmp":I
    .end local v14    # "zz":I
    :cond_3
    const/16 v15, 0x104

    if-lt v7, v15, :cond_4

    .line 117
    invoke-static {}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->panic()V

    .line 120
    :cond_4
    :goto_4
    const/4 v15, 0x1

    if-le v7, v15, :cond_d

    .line 121
    const/4 v15, 0x1

    aget v5, v1, v15

    .line 122
    .local v5, "n1":I
    const/4 v15, 0x1

    aget v16, v1, v7

    aput v16, v1, v15

    .line 123
    add-int/lit8 v7, v7, -0x1

    .line 125
    const/4 v14, 0x0

    .restart local v14    # "zz":I
    const/4 v13, 0x0

    .local v13, "yy":I
    const/4 v10, 0x0

    .line 126
    .restart local v10    # "tmp":I
    const/4 v14, 0x1

    .line 127
    aget v10, v1, v14

    .line 129
    :goto_5
    shl-int/lit8 v13, v14, 0x1

    .line 130
    if-le v13, v7, :cond_7

    .line 143
    :cond_5
    aput v10, v1, v14

    .line 145
    const/4 v15, 0x1

    aget v6, v1, v15

    .line 146
    .local v6, "n2":I
    const/4 v15, 0x1

    aget v16, v1, v7

    aput v16, v1, v15

    .line 147
    add-int/lit8 v7, v7, -0x1

    .line 149
    const/4 v14, 0x0

    const/4 v13, 0x0

    const/4 v10, 0x0

    .line 150
    const/4 v14, 0x1

    .line 151
    aget v10, v1, v14

    .line 153
    :goto_6
    shl-int/lit8 v13, v14, 0x1

    .line 154
    if-le v13, v7, :cond_9

    .line 167
    :cond_6
    aput v10, v1, v14

    .line 169
    add-int/lit8 v8, v8, 0x1

    .line 170
    aput v8, v9, v6

    aput v8, v9, v5

    .line 172
    aget v15, v12, v5

    and-int/lit16 v15, v15, -0x100

    aget v16, v12, v6

    move/from16 v0, v16

    and-int/lit16 v0, v0, -0x100

    move/from16 v16, v0

    add-int v16, v16, v15

    aget v15, v12, v5

    and-int/lit16 v15, v15, 0xff

    aget v17, v12, v6

    move/from16 v0, v17

    and-int/lit16 v0, v0, 0xff

    move/from16 v17, v0

    move/from16 v0, v17

    if-le v15, v0, :cond_b

    aget v15, v12, v5

    and-int/lit16 v15, v15, 0xff

    :goto_7
    add-int/lit8 v15, v15, 0x1

    or-int v15, v15, v16

    aput v15, v12, v8

    .line 179
    const/4 v15, -0x1

    aput v15, v9, v8

    .line 180
    add-int/lit8 v7, v7, 0x1

    .line 181
    aput v8, v1, v7

    .line 183
    const/4 v14, 0x0

    const/4 v10, 0x0

    .line 184
    move v14, v7

    .line 185
    aget v10, v1, v14

    .line 186
    :goto_8
    aget v15, v12, v10

    shr-int/lit8 v16, v14, 0x1

    aget v16, v1, v16

    aget v16, v12, v16

    move/from16 v0, v16

    if-ge v15, v0, :cond_c

    .line 187
    shr-int/lit8 v15, v14, 0x1

    aget v15, v1, v15

    aput v15, v1, v14

    .line 188
    shr-int/lit8 v14, v14, 0x1

    goto :goto_8

    .line 133
    .end local v6    # "n2":I
    :cond_7
    if-ge v13, v7, :cond_8

    add-int/lit8 v15, v13, 0x1

    aget v15, v1, v15

    aget v15, v12, v15

    aget v16, v1, v13

    aget v16, v12, v16

    move/from16 v0, v16

    if-ge v15, v0, :cond_8

    .line 135
    add-int/lit8 v13, v13, 0x1

    .line 137
    :cond_8
    aget v15, v12, v10

    aget v16, v1, v13

    aget v16, v12, v16

    move/from16 v0, v16

    if-lt v15, v0, :cond_5

    .line 140
    aget v15, v1, v13

    aput v15, v1, v14

    .line 141
    move v14, v13

    goto/16 :goto_5

    .line 157
    .restart local v6    # "n2":I
    :cond_9
    if-ge v13, v7, :cond_a

    add-int/lit8 v15, v13, 0x1

    aget v15, v1, v15

    aget v15, v12, v15

    aget v16, v1, v13

    aget v16, v12, v16

    move/from16 v0, v16

    if-ge v15, v0, :cond_a

    .line 159
    add-int/lit8 v13, v13, 0x1

    .line 161
    :cond_a
    aget v15, v12, v10

    aget v16, v1, v13

    aget v16, v12, v16

    move/from16 v0, v16

    if-lt v15, v0, :cond_6

    .line 164
    aget v15, v1, v13

    aput v15, v1, v14

    .line 165
    move v14, v13

    goto/16 :goto_6

    .line 172
    :cond_b
    aget v15, v12, v6

    and-int/lit16 v15, v15, 0xff

    goto :goto_7

    .line 190
    :cond_c
    aput v10, v1, v14

    goto/16 :goto_4

    .line 193
    .end local v5    # "n1":I
    .end local v6    # "n2":I
    .end local v10    # "tmp":I
    .end local v13    # "yy":I
    .end local v14    # "zz":I
    :cond_d
    const/16 v15, 0x204

    if-lt v8, v15, :cond_e

    .line 194
    invoke-static {}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->panic()V

    .line 197
    :cond_e
    const/4 v11, 0x0

    .line 198
    .local v11, "tooLong":Z
    const/4 v2, 0x1

    :goto_9
    move/from16 v0, p2

    if-gt v2, v0, :cond_11

    .line 199
    const/4 v3, 0x0

    .line 200
    .local v3, "j":I
    move v4, v2

    .line 201
    .local v4, "k":I
    :goto_a
    aget v15, v9, v4

    if-ltz v15, :cond_f

    .line 202
    aget v4, v9, v4

    .line 203
    add-int/lit8 v3, v3, 0x1

    goto :goto_a

    .line 205
    :cond_f
    add-int/lit8 v15, v2, -0x1

    int-to-char v0, v3

    move/from16 v16, v0

    aput-char v16, p0, v15

    .line 206
    move/from16 v0, p3

    if-le v3, v0, :cond_10

    .line 207
    const/4 v11, 0x1

    .line 198
    :cond_10
    add-int/lit8 v2, v2, 0x1

    goto :goto_9

    .line 211
    .end local v3    # "j":I
    .end local v4    # "k":I
    :cond_11
    if-nez v11, :cond_12

    .line 221
    return-void

    .line 215
    :cond_12
    const/4 v2, 0x1

    :goto_b
    move/from16 v0, p2

    if-ge v2, v0, :cond_1

    .line 216
    aget v15, v12, v2

    shr-int/lit8 v3, v15, 0x8

    .line 217
    .restart local v3    # "j":I
    div-int/lit8 v15, v3, 0x2

    add-int/lit8 v3, v15, 0x1

    .line 218
    shl-int/lit8 v15, v3, 0x8

    aput v15, v12, v2

    .line 215
    add-int/lit8 v2, v2, 0x1

    goto :goto_b
.end method

.method private initBlock()V
    .locals 3

    .prologue
    .line 439
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->mCrc:Lcom/android/sec/org/bouncycastle/apache/bzip2/CRC;

    invoke-virtual {v1}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CRC;->initialiseCRC()V

    .line 440
    const/4 v1, -0x1

    iput v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->last:I

    .line 443
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/16 v1, 0x100

    if-ge v0, v1, :cond_0

    .line 444
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->inUse:[Z

    const/4 v2, 0x0

    aput-boolean v2, v1, v0

    .line 443
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 448
    :cond_0
    const v1, 0x186a0

    iget v2, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->blockSize100k:I

    mul-int/2addr v1, v2

    add-int/lit8 v1, v1, -0x14

    iput v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->allowableBlockSize:I

    .line 449
    return-void
.end method

.method private initialize()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 423
    iput v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->bytesOut:I

    .line 424
    iput v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->nBlocksRandomised:I

    .line 429
    const/16 v0, 0x68

    invoke-direct {p0, v0}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->bsPutUChar(I)V

    .line 430
    iget v0, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->blockSize100k:I

    add-int/lit8 v0, v0, 0x30

    invoke-direct {p0, v0}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->bsPutUChar(I)V

    .line 432
    iput v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->combinedCRC:I

    .line 433
    return-void
.end method

.method private mainSort()V
    .locals 27

    .prologue
    .line 1140
    const/16 v23, 0x100

    move/from16 v0, v23

    new-array v0, v0, [I

    move-object/from16 v18, v0

    .line 1141
    .local v18, "runningOrder":[I
    const/16 v23, 0x100

    move/from16 v0, v23

    new-array v10, v0, [I

    .line 1142
    .local v10, "copy":[I
    const/16 v23, 0x100

    move/from16 v0, v23

    new-array v7, v0, [Z

    .line 1153
    .local v7, "bigDone":[Z
    const/4 v13, 0x0

    .local v13, "i":I
    :goto_0
    const/16 v23, 0x14

    move/from16 v0, v23

    if-ge v13, v0, :cond_0

    .line 1154
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->block:[C

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->last:I

    move/from16 v24, v0

    add-int v24, v24, v13

    add-int/lit8 v24, v24, 0x2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->block:[C

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->last:I

    move/from16 v26, v0

    add-int/lit8 v26, v26, 0x1

    rem-int v26, v13, v26

    add-int/lit8 v26, v26, 0x1

    aget-char v25, v25, v26

    aput-char v25, v23, v24

    .line 1153
    add-int/lit8 v13, v13, 0x1

    goto :goto_0

    .line 1156
    :cond_0
    const/4 v13, 0x0

    :goto_1
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->last:I

    move/from16 v23, v0

    add-int/lit8 v23, v23, 0x14

    move/from16 v0, v23

    if-gt v13, v0, :cond_1

    .line 1157
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->quadrant:[I

    move-object/from16 v23, v0

    const/16 v24, 0x0

    aput v24, v23, v13

    .line 1156
    add-int/lit8 v13, v13, 0x1

    goto :goto_1

    .line 1160
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->block:[C

    move-object/from16 v23, v0

    const/16 v24, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->block:[C

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->last:I

    move/from16 v26, v0

    add-int/lit8 v26, v26, 0x1

    aget-char v25, v25, v26

    aput-char v25, v23, v24

    .line 1162
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->last:I

    move/from16 v23, v0

    const/16 v24, 0xfa0

    move/from16 v0, v23

    move/from16 v1, v24

    if-ge v0, v1, :cond_4

    .line 1167
    const/4 v13, 0x0

    :goto_2
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->last:I

    move/from16 v23, v0

    move/from16 v0, v23

    if-gt v13, v0, :cond_2

    .line 1168
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->zptr:[I

    move-object/from16 v23, v0

    aput v13, v23, v13

    .line 1167
    add-int/lit8 v13, v13, 0x1

    goto :goto_2

    .line 1170
    :cond_2
    const/16 v23, 0x0

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->firstAttempt:Z

    .line 1171
    const/16 v23, 0x0

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->workLimit:I

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->workDone:I

    .line 1172
    const/16 v23, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->last:I

    move/from16 v24, v0

    const/16 v25, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v23

    move/from16 v2, v24

    move/from16 v3, v25

    invoke-direct {v0, v1, v2, v3}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->simpleSort(III)V

    .line 1331
    :cond_3
    return-void

    .line 1174
    :cond_4
    const/16 v16, 0x0

    .line 1175
    .local v16, "numQSorted":I
    const/4 v13, 0x0

    :goto_3
    const/16 v23, 0xff

    move/from16 v0, v23

    if-gt v13, v0, :cond_5

    .line 1176
    const/16 v23, 0x0

    aput-boolean v23, v7, v13

    .line 1175
    add-int/lit8 v13, v13, 0x1

    goto :goto_3

    .line 1179
    :cond_5
    const/4 v13, 0x0

    :goto_4
    const/high16 v23, 0x10000

    move/from16 v0, v23

    if-gt v13, v0, :cond_6

    .line 1180
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->ftab:[I

    move-object/from16 v23, v0

    const/16 v24, 0x0

    aput v24, v23, v13

    .line 1179
    add-int/lit8 v13, v13, 0x1

    goto :goto_4

    .line 1183
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->block:[C

    move-object/from16 v23, v0

    const/16 v24, 0x0

    aget-char v8, v23, v24

    .line 1184
    .local v8, "c1":I
    const/4 v13, 0x0

    :goto_5
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->last:I

    move/from16 v23, v0

    move/from16 v0, v23

    if-gt v13, v0, :cond_7

    .line 1185
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->block:[C

    move-object/from16 v23, v0

    add-int/lit8 v24, v13, 0x1

    aget-char v9, v23, v24

    .line 1186
    .local v9, "c2":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->ftab:[I

    move-object/from16 v23, v0

    shl-int/lit8 v24, v8, 0x8

    add-int v24, v24, v9

    aget v25, v23, v24

    add-int/lit8 v25, v25, 0x1

    aput v25, v23, v24

    .line 1187
    move v8, v9

    .line 1184
    add-int/lit8 v13, v13, 0x1

    goto :goto_5

    .line 1190
    .end local v9    # "c2":I
    :cond_7
    const/4 v13, 0x1

    :goto_6
    const/high16 v23, 0x10000

    move/from16 v0, v23

    if-gt v13, v0, :cond_8

    .line 1191
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->ftab:[I

    move-object/from16 v23, v0

    aget v24, v23, v13

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->ftab:[I

    move-object/from16 v25, v0

    add-int/lit8 v26, v13, -0x1

    aget v25, v25, v26

    add-int v24, v24, v25

    aput v24, v23, v13

    .line 1190
    add-int/lit8 v13, v13, 0x1

    goto :goto_6

    .line 1194
    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->block:[C

    move-object/from16 v23, v0

    const/16 v24, 0x1

    aget-char v8, v23, v24

    .line 1195
    const/4 v13, 0x0

    :goto_7
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->last:I

    move/from16 v23, v0

    move/from16 v0, v23

    if-ge v13, v0, :cond_9

    .line 1196
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->block:[C

    move-object/from16 v23, v0

    add-int/lit8 v24, v13, 0x2

    aget-char v9, v23, v24

    .line 1197
    .restart local v9    # "c2":I
    shl-int/lit8 v23, v8, 0x8

    add-int v14, v23, v9

    .line 1198
    .local v14, "j":I
    move v8, v9

    .line 1199
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->ftab:[I

    move-object/from16 v23, v0

    aget v24, v23, v14

    add-int/lit8 v24, v24, -0x1

    aput v24, v23, v14

    .line 1200
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->zptr:[I

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->ftab:[I

    move-object/from16 v24, v0

    aget v24, v24, v14

    aput v13, v23, v24

    .line 1195
    add-int/lit8 v13, v13, 0x1

    goto :goto_7

    .line 1203
    .end local v9    # "c2":I
    .end local v14    # "j":I
    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->block:[C

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->last:I

    move/from16 v24, v0

    add-int/lit8 v24, v24, 0x1

    aget-char v23, v23, v24

    shl-int/lit8 v23, v23, 0x8

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->block:[C

    move-object/from16 v24, v0

    const/16 v25, 0x1

    aget-char v24, v24, v25

    add-int v14, v23, v24

    .line 1204
    .restart local v14    # "j":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->ftab:[I

    move-object/from16 v23, v0

    aget v24, v23, v14

    add-int/lit8 v24, v24, -0x1

    aput v24, v23, v14

    .line 1205
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->zptr:[I

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->ftab:[I

    move-object/from16 v24, v0

    aget v24, v24, v14

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->last:I

    move/from16 v25, v0

    aput v25, v23, v24

    .line 1213
    const/4 v13, 0x0

    :goto_8
    const/16 v23, 0xff

    move/from16 v0, v23

    if-gt v13, v0, :cond_a

    .line 1214
    aput v13, v18, v13

    .line 1213
    add-int/lit8 v13, v13, 0x1

    goto :goto_8

    .line 1219
    :cond_a
    const/4 v11, 0x1

    .line 1221
    .local v11, "h":I
    :cond_b
    mul-int/lit8 v23, v11, 0x3

    add-int/lit8 v11, v23, 0x1

    .line 1223
    const/16 v23, 0x100

    move/from16 v0, v23

    if-le v11, v0, :cond_b

    .line 1225
    :cond_c
    div-int/lit8 v11, v11, 0x3

    .line 1226
    move v13, v11

    :goto_9
    const/16 v23, 0xff

    move/from16 v0, v23

    if-gt v13, v0, :cond_f

    .line 1227
    aget v22, v18, v13

    .line 1228
    .local v22, "vv":I
    move v14, v13

    .line 1230
    :cond_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->ftab:[I

    move-object/from16 v23, v0

    sub-int v24, v14, v11

    aget v24, v18, v24

    add-int/lit8 v24, v24, 0x1

    shl-int/lit8 v24, v24, 0x8

    aget v23, v23, v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->ftab:[I

    move-object/from16 v24, v0

    sub-int v25, v14, v11

    aget v25, v18, v25

    shl-int/lit8 v25, v25, 0x8

    aget v24, v24, v25

    sub-int v23, v23, v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->ftab:[I

    move-object/from16 v24, v0

    add-int/lit8 v25, v22, 0x1

    shl-int/lit8 v25, v25, 0x8

    aget v24, v24, v25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->ftab:[I

    move-object/from16 v25, v0

    shl-int/lit8 v26, v22, 0x8

    aget v25, v25, v26

    sub-int v24, v24, v25

    move/from16 v0, v23

    move/from16 v1, v24

    if-le v0, v1, :cond_e

    .line 1232
    sub-int v23, v14, v11

    aget v23, v18, v23

    aput v23, v18, v14

    .line 1233
    sub-int/2addr v14, v11

    .line 1234
    add-int/lit8 v23, v11, -0x1

    move/from16 v0, v23

    if-gt v14, v0, :cond_d

    .line 1238
    :cond_e
    aput v22, v18, v14

    .line 1226
    add-int/lit8 v13, v13, 0x1

    goto :goto_9

    .line 1240
    .end local v22    # "vv":I
    :cond_f
    const/16 v23, 0x1

    move/from16 v0, v23

    if-ne v11, v0, :cond_c

    .line 1246
    const/4 v13, 0x0

    :goto_a
    const/16 v23, 0xff

    move/from16 v0, v23

    if-gt v13, v0, :cond_3

    .line 1251
    aget v21, v18, v13

    .line 1260
    .local v21, "ss":I
    const/4 v14, 0x0

    :goto_b
    const/16 v23, 0xff

    move/from16 v0, v23

    if-gt v14, v0, :cond_12

    .line 1261
    shl-int/lit8 v23, v21, 0x8

    add-int v19, v23, v14

    .line 1262
    .local v19, "sb":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->ftab:[I

    move-object/from16 v23, v0

    aget v23, v23, v19

    const/high16 v24, 0x200000

    and-int v23, v23, v24

    const/high16 v24, 0x200000

    move/from16 v0, v23

    move/from16 v1, v24

    if-eq v0, v1, :cond_11

    .line 1263
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->ftab:[I

    move-object/from16 v23, v0

    aget v23, v23, v19

    const v24, -0x200001

    and-int v15, v23, v24

    .line 1264
    .local v15, "lo":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->ftab:[I

    move-object/from16 v23, v0

    add-int/lit8 v24, v19, 0x1

    aget v23, v23, v24

    const v24, -0x200001

    and-int v23, v23, v24

    add-int/lit8 v12, v23, -0x1

    .line 1265
    .local v12, "hi":I
    if-le v12, v15, :cond_10

    .line 1266
    const/16 v23, 0x2

    move-object/from16 v0, p0

    move/from16 v1, v23

    invoke-direct {v0, v15, v12, v1}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->qSort3(III)V

    .line 1267
    sub-int v23, v12, v15

    add-int/lit8 v23, v23, 0x1

    add-int v16, v16, v23

    .line 1268
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->workDone:I

    move/from16 v23, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->workLimit:I

    move/from16 v24, v0

    move/from16 v0, v23

    move/from16 v1, v24

    if-le v0, v1, :cond_10

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->firstAttempt:Z

    move/from16 v23, v0

    if-nez v23, :cond_3

    .line 1272
    :cond_10
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->ftab:[I

    move-object/from16 v23, v0

    aget v24, v23, v19

    const/high16 v25, 0x200000

    or-int v24, v24, v25

    aput v24, v23, v19

    .line 1260
    .end local v12    # "hi":I
    .end local v15    # "lo":I
    :cond_11
    add-int/lit8 v14, v14, 0x1

    goto :goto_b

    .line 1284
    .end local v19    # "sb":I
    :cond_12
    const/16 v23, 0x1

    aput-boolean v23, v7, v21

    .line 1286
    const/16 v23, 0xff

    move/from16 v0, v23

    if-ge v13, v0, :cond_16

    .line 1287
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->ftab:[I

    move-object/from16 v23, v0

    shl-int/lit8 v24, v21, 0x8

    aget v23, v23, v24

    const v24, -0x200001

    and-int v6, v23, v24

    .line 1288
    .local v6, "bbStart":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->ftab:[I

    move-object/from16 v23, v0

    add-int/lit8 v24, v21, 0x1

    shl-int/lit8 v24, v24, 0x8

    aget v23, v23, v24

    const v24, -0x200001

    and-int v23, v23, v24

    sub-int v5, v23, v6

    .line 1289
    .local v5, "bbSize":I
    const/16 v20, 0x0

    .line 1291
    .local v20, "shifts":I
    :goto_c
    shr-int v23, v5, v20

    const v24, 0xfffe

    move/from16 v0, v23

    move/from16 v1, v24

    if-le v0, v1, :cond_13

    .line 1292
    add-int/lit8 v20, v20, 0x1

    goto :goto_c

    .line 1295
    :cond_13
    const/4 v14, 0x0

    :goto_d
    if-ge v14, v5, :cond_15

    .line 1296
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->zptr:[I

    move-object/from16 v23, v0

    add-int v24, v6, v14

    aget v4, v23, v24

    .line 1297
    .local v4, "a2update":I
    shr-int v17, v14, v20

    .line 1298
    .local v17, "qVal":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->quadrant:[I

    move-object/from16 v23, v0

    aput v17, v23, v4

    .line 1299
    const/16 v23, 0x14

    move/from16 v0, v23

    if-ge v4, v0, :cond_14

    .line 1300
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->quadrant:[I

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->last:I

    move/from16 v24, v0

    add-int v24, v24, v4

    add-int/lit8 v24, v24, 0x1

    aput v17, v23, v24

    .line 1295
    :cond_14
    add-int/lit8 v14, v14, 0x1

    goto :goto_d

    .line 1304
    .end local v4    # "a2update":I
    .end local v17    # "qVal":I
    :cond_15
    add-int/lit8 v23, v5, -0x1

    shr-int v23, v23, v20

    const v24, 0xffff

    move/from16 v0, v23

    move/from16 v1, v24

    if-le v0, v1, :cond_16

    .line 1305
    invoke-static {}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->panic()V

    .line 1313
    .end local v5    # "bbSize":I
    .end local v6    # "bbStart":I
    .end local v20    # "shifts":I
    :cond_16
    const/4 v14, 0x0

    :goto_e
    const/16 v23, 0xff

    move/from16 v0, v23

    if-gt v14, v0, :cond_17

    .line 1314
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->ftab:[I

    move-object/from16 v23, v0

    shl-int/lit8 v24, v14, 0x8

    add-int v24, v24, v21

    aget v23, v23, v24

    const v24, -0x200001

    and-int v23, v23, v24

    aput v23, v10, v14

    .line 1313
    add-int/lit8 v14, v14, 0x1

    goto :goto_e

    .line 1317
    :cond_17
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->ftab:[I

    move-object/from16 v23, v0

    shl-int/lit8 v24, v21, 0x8

    aget v23, v23, v24

    const v24, -0x200001

    and-int v14, v23, v24

    .line 1318
    :goto_f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->ftab:[I

    move-object/from16 v23, v0

    add-int/lit8 v24, v21, 0x1

    shl-int/lit8 v24, v24, 0x8

    aget v23, v23, v24

    const v24, -0x200001

    and-int v23, v23, v24

    move/from16 v0, v23

    if-ge v14, v0, :cond_1a

    .line 1319
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->block:[C

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->zptr:[I

    move-object/from16 v24, v0

    aget v24, v24, v14

    aget-char v8, v23, v24

    .line 1320
    aget-boolean v23, v7, v8

    if-nez v23, :cond_18

    .line 1321
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->zptr:[I

    move-object/from16 v24, v0

    aget v25, v10, v8

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->zptr:[I

    move-object/from16 v23, v0

    aget v23, v23, v14

    if-nez v23, :cond_19

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->last:I

    move/from16 v23, v0

    :goto_10
    aput v23, v24, v25

    .line 1322
    aget v23, v10, v8

    add-int/lit8 v23, v23, 0x1

    aput v23, v10, v8

    .line 1318
    :cond_18
    add-int/lit8 v14, v14, 0x1

    goto :goto_f

    .line 1321
    :cond_19
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->zptr:[I

    move-object/from16 v23, v0

    aget v23, v23, v14

    add-int/lit8 v23, v23, -0x1

    goto :goto_10

    .line 1326
    :cond_1a
    const/4 v14, 0x0

    :goto_11
    const/16 v23, 0xff

    move/from16 v0, v23

    if-gt v14, v0, :cond_1b

    .line 1327
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->ftab:[I

    move-object/from16 v23, v0

    shl-int/lit8 v24, v14, 0x8

    add-int v24, v24, v21

    aget v25, v23, v24

    const/high16 v26, 0x200000

    or-int v25, v25, v26

    aput v25, v23, v24

    .line 1326
    add-int/lit8 v14, v14, 0x1

    goto :goto_11

    .line 1246
    :cond_1b
    add-int/lit8 v13, v13, 0x1

    goto/16 :goto_a
.end method

.method private makeMaps()V
    .locals 4

    .prologue
    .line 66
    const/4 v1, 0x0

    iput v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->nInUse:I

    .line 67
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/16 v1, 0x100

    if-ge v0, v1, :cond_1

    .line 68
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->inUse:[Z

    aget-boolean v1, v1, v0

    if-eqz v1, :cond_0

    .line 69
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->seqToUnseq:[C

    iget v2, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->nInUse:I

    int-to-char v3, v0

    aput-char v3, v1, v2

    .line 70
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->unseqToSeq:[C

    iget v2, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->nInUse:I

    int-to-char v2, v2

    aput-char v2, v1, v0

    .line 71
    iget v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->nInUse:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->nInUse:I

    .line 67
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 74
    :cond_1
    return-void
.end method

.method private med3(CCC)C
    .locals 1
    .param p1, "a"    # C
    .param p2, "b"    # C
    .param p3, "c"    # C

    .prologue
    .line 993
    if-le p1, p2, :cond_0

    .line 994
    move v0, p1

    .line 995
    .local v0, "t":C
    move p1, p2

    .line 996
    move p2, v0

    .line 998
    .end local v0    # "t":C
    :cond_0
    if-le p2, p3, :cond_1

    .line 999
    move v0, p2

    .line 1000
    .restart local v0    # "t":C
    move p2, p3

    .line 1001
    move p3, v0

    .line 1003
    .end local v0    # "t":C
    :cond_1
    if-le p1, p2, :cond_2

    .line 1004
    move p2, p1

    .line 1006
    :cond_2
    return p2
.end method

.method private moveToFrontCodeAndSend()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 897
    const/16 v0, 0x18

    iget v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->origPtr:I

    invoke-direct {p0, v0, v1}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->bsPutIntVS(II)V

    .line 898
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->generateMTFValues()V

    .line 899
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->sendMTFValues()V

    .line 900
    return-void
.end method

.method private static panic()V
    .locals 2

    .prologue
    .line 60
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "panic"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 62
    return-void
.end method

.method private qSort3(III)V
    .locals 23
    .param p1, "loSt"    # I
    .param p2, "hiSt"    # I
    .param p3, "dSt"    # I

    .prologue
    .line 1018
    const/16 v18, 0x3e8

    move/from16 v0, v18

    new-array v14, v0, [Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream$StackElem;

    .line 1019
    .local v14, "stack":[Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream$StackElem;
    const/4 v4, 0x0

    .local v4, "count":I
    :goto_0
    const/16 v18, 0x3e8

    move/from16 v0, v18

    if-ge v4, v0, :cond_0

    .line 1020
    new-instance v18, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream$StackElem;

    const/16 v19, 0x0

    invoke-direct/range {v18 .. v19}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream$StackElem;-><init>(Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream$1;)V

    aput-object v18, v14, v4

    .line 1019
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 1023
    :cond_0
    const/4 v13, 0x0

    .line 1025
    .local v13, "sp":I
    aget-object v18, v14, v13

    move/from16 v0, p1

    move-object/from16 v1, v18

    iput v0, v1, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream$StackElem;->ll:I

    .line 1026
    aget-object v18, v14, v13

    move/from16 v0, p2

    move-object/from16 v1, v18

    iput v0, v1, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream$StackElem;->hh:I

    .line 1027
    aget-object v18, v14, v13

    move/from16 v0, p3

    move-object/from16 v1, v18

    iput v0, v1, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream$StackElem;->dd:I

    .line 1028
    add-int/lit8 v13, v13, 0x1

    .line 1030
    :cond_1
    :goto_1
    if-lez v13, :cond_4

    .line 1031
    const/16 v18, 0x3e8

    move/from16 v0, v18

    if-lt v13, v0, :cond_2

    .line 1032
    invoke-static {}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->panic()V

    .line 1035
    :cond_2
    add-int/lit8 v13, v13, -0x1

    .line 1036
    aget-object v18, v14, v13

    move-object/from16 v0, v18

    iget v8, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream$StackElem;->ll:I

    .line 1037
    .local v8, "lo":I
    aget-object v18, v14, v13

    move-object/from16 v0, v18

    iget v7, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream$StackElem;->hh:I

    .line 1038
    .local v7, "hi":I
    aget-object v18, v14, v13

    move-object/from16 v0, v18

    iget v5, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream$StackElem;->dd:I

    .line 1040
    .local v5, "d":I
    sub-int v18, v7, v8

    const/16 v19, 0x14

    move/from16 v0, v18

    move/from16 v1, v19

    if-lt v0, v1, :cond_3

    const/16 v18, 0xa

    move/from16 v0, v18

    if-le v5, v0, :cond_5

    .line 1041
    :cond_3
    move-object/from16 v0, p0

    invoke-direct {v0, v8, v7, v5}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->simpleSort(III)V

    .line 1042
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->workDone:I

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->workLimit:I

    move/from16 v19, v0

    move/from16 v0, v18

    move/from16 v1, v19

    if-le v0, v1, :cond_1

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->firstAttempt:Z

    move/from16 v18, v0

    if-eqz v18, :cond_1

    .line 1136
    .end local v5    # "d":I
    .end local v7    # "hi":I
    .end local v8    # "lo":I
    :cond_4
    return-void

    .line 1048
    .restart local v5    # "d":I
    .restart local v7    # "hi":I
    .restart local v8    # "lo":I
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->block:[C

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->zptr:[I

    move-object/from16 v19, v0

    aget v19, v19, v8

    add-int v19, v19, v5

    add-int/lit8 v19, v19, 0x1

    aget-char v18, v18, v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->block:[C

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->zptr:[I

    move-object/from16 v20, v0

    aget v20, v20, v7

    add-int v20, v20, v5

    add-int/lit8 v20, v20, 0x1

    aget-char v19, v19, v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->block:[C

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->zptr:[I

    move-object/from16 v21, v0

    add-int v22, v8, v7

    shr-int/lit8 v22, v22, 0x1

    aget v21, v21, v22

    add-int v21, v21, v5

    add-int/lit8 v21, v21, 0x1

    aget-char v20, v20, v21

    move-object/from16 v0, p0

    move/from16 v1, v18

    move/from16 v2, v19

    move/from16 v3, v20

    invoke-direct {v0, v1, v2, v3}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->med3(CCC)C

    move-result v11

    .line 1052
    .local v11, "med":I
    move v9, v8

    .local v9, "ltLo":I
    move/from16 v17, v8

    .line 1053
    .local v17, "unLo":I
    move v6, v7

    .local v6, "gtHi":I
    move/from16 v16, v7

    .line 1057
    .local v16, "unHi":I
    :goto_2
    move/from16 v0, v17

    move/from16 v1, v16

    if-le v0, v1, :cond_8

    .line 1076
    :cond_6
    :goto_3
    move/from16 v0, v17

    move/from16 v1, v16

    if-le v0, v1, :cond_a

    .line 1094
    :cond_7
    move/from16 v0, v17

    move/from16 v1, v16

    if-le v0, v1, :cond_c

    .line 1105
    if-ge v6, v9, :cond_d

    .line 1106
    aget-object v18, v14, v13

    move-object/from16 v0, v18

    iput v8, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream$StackElem;->ll:I

    .line 1107
    aget-object v18, v14, v13

    move-object/from16 v0, v18

    iput v7, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream$StackElem;->hh:I

    .line 1108
    aget-object v18, v14, v13

    add-int/lit8 v19, v5, 0x1

    move/from16 v0, v19

    move-object/from16 v1, v18

    iput v0, v1, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream$StackElem;->dd:I

    .line 1109
    add-int/lit8 v13, v13, 0x1

    .line 1110
    goto/16 :goto_1

    .line 1060
    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->block:[C

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->zptr:[I

    move-object/from16 v19, v0

    aget v19, v19, v17

    add-int v19, v19, v5

    add-int/lit8 v19, v19, 0x1

    aget-char v18, v18, v19

    sub-int v12, v18, v11

    .line 1061
    .local v12, "n":I
    if-nez v12, :cond_9

    .line 1062
    const/4 v15, 0x0

    .line 1063
    .local v15, "temp":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->zptr:[I

    move-object/from16 v18, v0

    aget v15, v18, v17

    .line 1064
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->zptr:[I

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->zptr:[I

    move-object/from16 v19, v0

    aget v19, v19, v9

    aput v19, v18, v17

    .line 1065
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->zptr:[I

    move-object/from16 v18, v0

    aput v15, v18, v9

    .line 1066
    add-int/lit8 v9, v9, 0x1

    .line 1067
    add-int/lit8 v17, v17, 0x1

    .line 1068
    goto :goto_2

    .line 1070
    .end local v15    # "temp":I
    :cond_9
    if-gtz v12, :cond_6

    .line 1073
    add-int/lit8 v17, v17, 0x1

    goto :goto_2

    .line 1079
    .end local v12    # "n":I
    :cond_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->block:[C

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->zptr:[I

    move-object/from16 v19, v0

    aget v19, v19, v16

    add-int v19, v19, v5

    add-int/lit8 v19, v19, 0x1

    aget-char v18, v18, v19

    sub-int v12, v18, v11

    .line 1080
    .restart local v12    # "n":I
    if-nez v12, :cond_b

    .line 1081
    const/4 v15, 0x0

    .line 1082
    .restart local v15    # "temp":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->zptr:[I

    move-object/from16 v18, v0

    aget v15, v18, v16

    .line 1083
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->zptr:[I

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->zptr:[I

    move-object/from16 v19, v0

    aget v19, v19, v6

    aput v19, v18, v16

    .line 1084
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->zptr:[I

    move-object/from16 v18, v0

    aput v15, v18, v6

    .line 1085
    add-int/lit8 v6, v6, -0x1

    .line 1086
    add-int/lit8 v16, v16, -0x1

    .line 1087
    goto/16 :goto_3

    .line 1089
    .end local v15    # "temp":I
    :cond_b
    if-ltz v12, :cond_7

    .line 1092
    add-int/lit8 v16, v16, -0x1

    goto/16 :goto_3

    .line 1097
    .end local v12    # "n":I
    :cond_c
    const/4 v15, 0x0

    .line 1098
    .restart local v15    # "temp":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->zptr:[I

    move-object/from16 v18, v0

    aget v15, v18, v17

    .line 1099
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->zptr:[I

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->zptr:[I

    move-object/from16 v19, v0

    aget v19, v19, v16

    aput v19, v18, v17

    .line 1100
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->zptr:[I

    move-object/from16 v18, v0

    aput v15, v18, v16

    .line 1101
    add-int/lit8 v17, v17, 0x1

    .line 1102
    add-int/lit8 v16, v16, -0x1

    .line 1103
    goto/16 :goto_2

    .line 1113
    .end local v15    # "temp":I
    :cond_d
    sub-int v18, v9, v8

    sub-int v19, v17, v9

    move/from16 v0, v18

    move/from16 v1, v19

    if-ge v0, v1, :cond_e

    sub-int v12, v9, v8

    .line 1114
    .restart local v12    # "n":I
    :goto_4
    sub-int v18, v17, v12

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-direct {v0, v8, v1, v12}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->vswap(III)V

    .line 1115
    sub-int v18, v7, v6

    sub-int v19, v6, v16

    move/from16 v0, v18

    move/from16 v1, v19

    if-ge v0, v1, :cond_f

    sub-int v10, v7, v6

    .line 1116
    .local v10, "m":I
    :goto_5
    sub-int v18, v7, v10

    add-int/lit8 v18, v18, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v17

    move/from16 v2, v18

    invoke-direct {v0, v1, v2, v10}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->vswap(III)V

    .line 1118
    add-int v18, v8, v17

    sub-int v18, v18, v9

    add-int/lit8 v12, v18, -0x1

    .line 1119
    sub-int v18, v6, v16

    sub-int v18, v7, v18

    add-int/lit8 v10, v18, 0x1

    .line 1121
    aget-object v18, v14, v13

    move-object/from16 v0, v18

    iput v8, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream$StackElem;->ll:I

    .line 1122
    aget-object v18, v14, v13

    move-object/from16 v0, v18

    iput v12, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream$StackElem;->hh:I

    .line 1123
    aget-object v18, v14, v13

    move-object/from16 v0, v18

    iput v5, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream$StackElem;->dd:I

    .line 1124
    add-int/lit8 v13, v13, 0x1

    .line 1126
    aget-object v18, v14, v13

    add-int/lit8 v19, v12, 0x1

    move/from16 v0, v19

    move-object/from16 v1, v18

    iput v0, v1, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream$StackElem;->ll:I

    .line 1127
    aget-object v18, v14, v13

    add-int/lit8 v19, v10, -0x1

    move/from16 v0, v19

    move-object/from16 v1, v18

    iput v0, v1, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream$StackElem;->hh:I

    .line 1128
    aget-object v18, v14, v13

    add-int/lit8 v19, v5, 0x1

    move/from16 v0, v19

    move-object/from16 v1, v18

    iput v0, v1, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream$StackElem;->dd:I

    .line 1129
    add-int/lit8 v13, v13, 0x1

    .line 1131
    aget-object v18, v14, v13

    move-object/from16 v0, v18

    iput v10, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream$StackElem;->ll:I

    .line 1132
    aget-object v18, v14, v13

    move-object/from16 v0, v18

    iput v7, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream$StackElem;->hh:I

    .line 1133
    aget-object v18, v14, v13

    move-object/from16 v0, v18

    iput v5, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream$StackElem;->dd:I

    .line 1134
    add-int/lit8 v13, v13, 0x1

    goto/16 :goto_1

    .line 1113
    .end local v10    # "m":I
    .end local v12    # "n":I
    :cond_e
    sub-int v12, v17, v9

    goto :goto_4

    .line 1115
    .restart local v12    # "n":I
    :cond_f
    sub-int v10, v6, v16

    goto :goto_5
.end method

.method private randomiseBlock()V
    .locals 9

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 1335
    const/4 v1, 0x0

    .line 1336
    .local v1, "rNToGo":I
    const/4 v2, 0x0

    .line 1337
    .local v2, "rTPos":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/16 v3, 0x100

    if-ge v0, v3, :cond_0

    .line 1338
    iget-object v3, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->inUse:[Z

    aput-boolean v5, v3, v0

    .line 1337
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1341
    :cond_0
    const/4 v0, 0x0

    :goto_1
    iget v3, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->last:I

    if-gt v0, v3, :cond_3

    .line 1342
    if-nez v1, :cond_1

    .line 1343
    sget-object v3, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->rNums:[I

    aget v3, v3, v2

    int-to-char v1, v3

    .line 1344
    add-int/lit8 v2, v2, 0x1

    .line 1345
    const/16 v3, 0x200

    if-ne v2, v3, :cond_1

    .line 1346
    const/4 v2, 0x0

    .line 1349
    :cond_1
    add-int/lit8 v1, v1, -0x1

    .line 1350
    iget-object v6, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->block:[C

    add-int/lit8 v7, v0, 0x1

    aget-char v8, v6, v7

    if-ne v1, v4, :cond_2

    move v3, v4

    :goto_2
    xor-int/2addr v3, v8

    int-to-char v3, v3

    aput-char v3, v6, v7

    .line 1352
    iget-object v3, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->block:[C

    add-int/lit8 v6, v0, 0x1

    aget-char v7, v3, v6

    and-int/lit16 v7, v7, 0xff

    int-to-char v7, v7

    aput-char v7, v3, v6

    .line 1354
    iget-object v3, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->inUse:[Z

    iget-object v6, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->block:[C

    add-int/lit8 v7, v0, 0x1

    aget-char v6, v6, v7

    aput-boolean v4, v3, v6

    .line 1341
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    move v3, v5

    .line 1350
    goto :goto_2

    .line 1356
    :cond_3
    return-void
.end method

.method private sendMTFValues()V
    .locals 44
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 583
    const/4 v2, 0x6

    const/16 v3, 0x102

    filled-new-array {v2, v3}, [I

    move-result-object v2

    sget-object v3, Ljava/lang/Character;->TYPE:Ljava/lang/Class;

    invoke-static {v3, v2}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v28

    check-cast v28, [[C

    .line 586
    .local v28, "len":[[C
    const/16 v33, 0x0

    .line 589
    .local v33, "nSelectors":I
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->nInUse:I

    add-int/lit8 v7, v2, 0x2

    .line 590
    .local v7, "alphaSize":I
    const/16 v38, 0x0

    .local v38, "t":I
    :goto_0
    const/4 v2, 0x6

    move/from16 v0, v38

    if-ge v0, v2, :cond_1

    .line 591
    const/16 v43, 0x0

    .local v43, "v":I
    :goto_1
    move/from16 v0, v43

    if-ge v0, v7, :cond_0

    .line 592
    aget-object v2, v28, v38

    const/16 v3, 0xf

    aput-char v3, v2, v43

    .line 591
    add-int/lit8 v43, v43, 0x1

    goto :goto_1

    .line 590
    :cond_0
    add-int/lit8 v38, v38, 0x1

    goto :goto_0

    .line 597
    .end local v43    # "v":I
    :cond_1
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->nMTF:I

    if-gtz v2, :cond_2

    .line 598
    invoke-static {}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->panic()V

    .line 601
    :cond_2
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->nMTF:I

    const/16 v3, 0xc8

    if-ge v2, v3, :cond_3

    .line 602
    const/16 v31, 0x2

    .line 616
    .local v31, "nGroups":I
    :goto_2
    move/from16 v32, v31

    .line 617
    .local v32, "nPart":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->nMTF:I

    move/from16 v35, v0

    .line 618
    .local v35, "remF":I
    const/16 v22, 0x0

    .line 619
    .local v22, "gs":I
    :goto_3
    if-lez v32, :cond_b

    .line 620
    div-int v39, v35, v32

    .line 621
    .local v39, "tFreq":I
    add-int/lit8 v21, v22, -0x1

    .line 622
    .local v21, "ge":I
    const/4 v8, 0x0

    .line 623
    .local v8, "aFreq":I
    :goto_4
    move/from16 v0, v39

    if-ge v8, v0, :cond_7

    add-int/lit8 v2, v7, -0x1

    move/from16 v0, v21

    if-ge v0, v2, :cond_7

    .line 624
    add-int/lit8 v21, v21, 0x1

    .line 625
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->mtfFreq:[I

    aget v2, v2, v21

    add-int/2addr v8, v2

    goto :goto_4

    .line 603
    .end local v8    # "aFreq":I
    .end local v21    # "ge":I
    .end local v22    # "gs":I
    .end local v31    # "nGroups":I
    .end local v32    # "nPart":I
    .end local v35    # "remF":I
    .end local v39    # "tFreq":I
    :cond_3
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->nMTF:I

    const/16 v3, 0x258

    if-ge v2, v3, :cond_4

    .line 604
    const/16 v31, 0x3

    .restart local v31    # "nGroups":I
    goto :goto_2

    .line 605
    .end local v31    # "nGroups":I
    :cond_4
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->nMTF:I

    const/16 v3, 0x4b0

    if-ge v2, v3, :cond_5

    .line 606
    const/16 v31, 0x4

    .restart local v31    # "nGroups":I
    goto :goto_2

    .line 607
    .end local v31    # "nGroups":I
    :cond_5
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->nMTF:I

    const/16 v3, 0x960

    if-ge v2, v3, :cond_6

    .line 608
    const/16 v31, 0x5

    .restart local v31    # "nGroups":I
    goto :goto_2

    .line 610
    .end local v31    # "nGroups":I
    :cond_6
    const/16 v31, 0x6

    .restart local v31    # "nGroups":I
    goto :goto_2

    .line 628
    .restart local v8    # "aFreq":I
    .restart local v21    # "ge":I
    .restart local v22    # "gs":I
    .restart local v32    # "nPart":I
    .restart local v35    # "remF":I
    .restart local v39    # "tFreq":I
    :cond_7
    move/from16 v0, v21

    move/from16 v1, v22

    if-le v0, v1, :cond_8

    move/from16 v0, v32

    move/from16 v1, v31

    if-eq v0, v1, :cond_8

    const/4 v2, 0x1

    move/from16 v0, v32

    if-eq v0, v2, :cond_8

    sub-int v2, v31, v32

    rem-int/lit8 v2, v2, 0x2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_8

    .line 630
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->mtfFreq:[I

    aget v2, v2, v21

    sub-int/2addr v8, v2

    .line 631
    add-int/lit8 v21, v21, -0x1

    .line 634
    :cond_8
    const/16 v43, 0x0

    .restart local v43    # "v":I
    :goto_5
    move/from16 v0, v43

    if-ge v0, v7, :cond_a

    .line 635
    move/from16 v0, v43

    move/from16 v1, v22

    if-lt v0, v1, :cond_9

    move/from16 v0, v43

    move/from16 v1, v21

    if-gt v0, v1, :cond_9

    .line 636
    add-int/lit8 v2, v32, -0x1

    aget-object v2, v28, v2

    const/4 v3, 0x0

    aput-char v3, v2, v43

    .line 634
    :goto_6
    add-int/lit8 v43, v43, 0x1

    goto :goto_5

    .line 638
    :cond_9
    add-int/lit8 v2, v32, -0x1

    aget-object v2, v28, v2

    const/16 v3, 0xf

    aput-char v3, v2, v43

    goto :goto_6

    .line 642
    :cond_a
    add-int/lit8 v32, v32, -0x1

    .line 643
    add-int/lit8 v22, v21, 0x1

    .line 644
    sub-int v35, v35, v8

    goto/16 :goto_3

    .line 648
    .end local v8    # "aFreq":I
    .end local v21    # "ge":I
    .end local v39    # "tFreq":I
    .end local v43    # "v":I
    :cond_b
    const/4 v2, 0x6

    const/16 v3, 0x102

    filled-new-array {v2, v3}, [I

    move-result-object v2

    sget-object v3, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {v3, v2}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v36

    check-cast v36, [[I

    .line 649
    .local v36, "rfreq":[[I
    const/4 v2, 0x6

    new-array v0, v2, [I

    move-object/from16 v20, v0

    .line 650
    .local v20, "fave":[I
    const/4 v2, 0x6

    new-array v12, v2, [S

    .line 654
    .local v12, "cost":[S
    const/16 v26, 0x0

    .local v26, "iter":I
    :goto_7
    const/4 v2, 0x4

    move/from16 v0, v26

    if-ge v0, v2, :cond_1a

    .line 655
    const/16 v38, 0x0

    :goto_8
    move/from16 v0, v38

    move/from16 v1, v31

    if-ge v0, v1, :cond_c

    .line 656
    const/4 v2, 0x0

    aput v2, v20, v38

    .line 655
    add-int/lit8 v38, v38, 0x1

    goto :goto_8

    .line 659
    :cond_c
    const/16 v38, 0x0

    :goto_9
    move/from16 v0, v38

    move/from16 v1, v31

    if-ge v0, v1, :cond_e

    .line 660
    const/16 v43, 0x0

    .restart local v43    # "v":I
    :goto_a
    move/from16 v0, v43

    if-ge v0, v7, :cond_d

    .line 661
    aget-object v2, v36, v38

    const/4 v3, 0x0

    aput v3, v2, v43

    .line 660
    add-int/lit8 v43, v43, 0x1

    goto :goto_a

    .line 659
    :cond_d
    add-int/lit8 v38, v38, 0x1

    goto :goto_9

    .line 665
    .end local v43    # "v":I
    :cond_e
    const/16 v33, 0x0

    .line 666
    const/16 v42, 0x0

    .line 667
    .local v42, "totc":I
    const/16 v22, 0x0

    .line 671
    :goto_b
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->nMTF:I

    move/from16 v0, v22

    if-lt v0, v2, :cond_f

    .line 744
    const/16 v38, 0x0

    :goto_c
    move/from16 v0, v38

    move/from16 v1, v31

    if-ge v0, v1, :cond_19

    .line 745
    aget-object v2, v28, v38

    aget-object v3, v36, v38

    const/16 v4, 0x14

    invoke-static {v2, v3, v7, v4}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->hbMakeCodeLengths([C[III)V

    .line 744
    add-int/lit8 v38, v38, 0x1

    goto :goto_c

    .line 674
    :cond_f
    add-int/lit8 v2, v22, 0x32

    add-int/lit8 v21, v2, -0x1

    .line 675
    .restart local v21    # "ge":I
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->nMTF:I

    move/from16 v0, v21

    if-lt v0, v2, :cond_10

    .line 676
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->nMTF:I

    add-int/lit8 v21, v2, -0x1

    .line 683
    :cond_10
    const/16 v38, 0x0

    :goto_d
    move/from16 v0, v38

    move/from16 v1, v31

    if-ge v0, v1, :cond_11

    .line 684
    const/4 v2, 0x0

    aput-short v2, v12, v38

    .line 683
    add-int/lit8 v38, v38, 0x1

    goto :goto_d

    .line 687
    :cond_11
    const/4 v2, 0x6

    move/from16 v0, v31

    if-ne v0, v2, :cond_15

    .line 689
    const/16 v18, 0x0

    .local v18, "cost5":S
    move/from16 v17, v18

    .local v17, "cost4":I
    move/from16 v16, v18

    .local v16, "cost3":I
    move/from16 v15, v18

    .local v15, "cost2":I
    move/from16 v14, v18

    .local v14, "cost1":I
    move/from16 v13, v18

    .line 690
    .local v13, "cost0":I
    move/from16 v23, v22

    .end local v13    # "cost0":I
    .end local v14    # "cost1":I
    .end local v15    # "cost2":I
    .end local v16    # "cost3":I
    .end local v17    # "cost4":I
    .local v23, "i":I
    :goto_e
    move/from16 v0, v23

    move/from16 v1, v21

    if-gt v0, v1, :cond_12

    .line 691
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->szptr:[S

    aget-short v24, v2, v23

    .line 692
    .local v24, "icv":S
    const/4 v2, 0x0

    aget-object v2, v28, v2

    aget-char v2, v2, v24

    add-int/2addr v2, v13

    int-to-short v13, v2

    .line 693
    .local v13, "cost0":S
    const/4 v2, 0x1

    aget-object v2, v28, v2

    aget-char v2, v2, v24

    add-int/2addr v2, v14

    int-to-short v14, v2

    .line 694
    .local v14, "cost1":S
    const/4 v2, 0x2

    aget-object v2, v28, v2

    aget-char v2, v2, v24

    add-int/2addr v2, v15

    int-to-short v15, v2

    .line 695
    .local v15, "cost2":S
    const/4 v2, 0x3

    aget-object v2, v28, v2

    aget-char v2, v2, v24

    add-int v2, v2, v16

    int-to-short v0, v2

    move/from16 v16, v0

    .line 696
    .local v16, "cost3":S
    const/4 v2, 0x4

    aget-object v2, v28, v2

    aget-char v2, v2, v24

    add-int v2, v2, v17

    int-to-short v0, v2

    move/from16 v17, v0

    .line 697
    .local v17, "cost4":S
    const/4 v2, 0x5

    aget-object v2, v28, v2

    aget-char v2, v2, v24

    add-int v2, v2, v18

    int-to-short v0, v2

    move/from16 v18, v0

    .line 690
    add-int/lit8 v23, v23, 0x1

    goto :goto_e

    .line 699
    .end local v13    # "cost0":S
    .end local v14    # "cost1":S
    .end local v15    # "cost2":S
    .end local v16    # "cost3":S
    .end local v17    # "cost4":S
    .end local v24    # "icv":S
    :cond_12
    const/4 v2, 0x0

    aput-short v13, v12, v2

    .line 700
    const/4 v2, 0x1

    aput-short v14, v12, v2

    .line 701
    const/4 v2, 0x2

    aput-short v15, v12, v2

    .line 702
    const/4 v2, 0x3

    aput-short v16, v12, v2

    .line 703
    const/4 v2, 0x4

    aput-short v17, v12, v2

    .line 704
    const/4 v2, 0x5

    aput-short v18, v12, v2

    .line 718
    .end local v18    # "cost5":S
    :cond_13
    const v9, 0x3b9ac9ff

    .line 719
    .local v9, "bc":I
    const/4 v10, -0x1

    .line 720
    .local v10, "bt":I
    const/16 v38, 0x0

    :goto_f
    move/from16 v0, v38

    move/from16 v1, v31

    if-ge v0, v1, :cond_17

    .line 721
    aget-short v2, v12, v38

    if-ge v2, v9, :cond_14

    .line 722
    aget-short v9, v12, v38

    .line 723
    move/from16 v10, v38

    .line 720
    :cond_14
    add-int/lit8 v38, v38, 0x1

    goto :goto_f

    .line 706
    .end local v9    # "bc":I
    .end local v10    # "bt":I
    .end local v23    # "i":I
    :cond_15
    move/from16 v23, v22

    .restart local v23    # "i":I
    :goto_10
    move/from16 v0, v23

    move/from16 v1, v21

    if-gt v0, v1, :cond_13

    .line 707
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->szptr:[S

    aget-short v24, v2, v23

    .line 708
    .restart local v24    # "icv":S
    const/16 v38, 0x0

    :goto_11
    move/from16 v0, v38

    move/from16 v1, v31

    if-ge v0, v1, :cond_16

    .line 709
    aget-short v2, v12, v38

    aget-object v3, v28, v38

    aget-char v3, v3, v24

    add-int/2addr v2, v3

    int-to-short v2, v2

    aput-short v2, v12, v38

    .line 708
    add-int/lit8 v38, v38, 0x1

    goto :goto_11

    .line 706
    :cond_16
    add-int/lit8 v23, v23, 0x1

    goto :goto_10

    .line 726
    .end local v24    # "icv":S
    .restart local v9    # "bc":I
    .restart local v10    # "bt":I
    :cond_17
    add-int v42, v42, v9

    .line 727
    aget v2, v20, v10

    add-int/lit8 v2, v2, 0x1

    aput v2, v20, v10

    .line 728
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->selector:[C

    int-to-char v3, v10

    aput-char v3, v2, v33

    .line 729
    add-int/lit8 v33, v33, 0x1

    .line 734
    move/from16 v23, v22

    :goto_12
    move/from16 v0, v23

    move/from16 v1, v21

    if-gt v0, v1, :cond_18

    .line 735
    aget-object v2, v36, v10

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->szptr:[S

    aget-short v3, v3, v23

    aget v4, v2, v3

    add-int/lit8 v4, v4, 0x1

    aput v4, v2, v3

    .line 734
    add-int/lit8 v23, v23, 0x1

    goto :goto_12

    .line 738
    :cond_18
    add-int/lit8 v22, v21, 0x1

    goto/16 :goto_b

    .line 654
    .end local v9    # "bc":I
    .end local v10    # "bt":I
    .end local v21    # "ge":I
    .end local v23    # "i":I
    :cond_19
    add-int/lit8 v26, v26, 0x1

    goto/16 :goto_7

    .line 749
    .end local v42    # "totc":I
    :cond_1a
    const/16 v36, 0x0

    check-cast v36, [[I

    .line 750
    const/16 v20, 0x0

    .line 751
    const/4 v12, 0x0

    .line 753
    const/16 v2, 0x8

    move/from16 v0, v31

    if-lt v0, v2, :cond_1b

    .line 754
    invoke-static {}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->panic()V

    .line 756
    :cond_1b
    const v2, 0x8000

    move/from16 v0, v33

    if-ge v0, v2, :cond_1c

    const/16 v2, 0x4652

    move/from16 v0, v33

    if-le v0, v2, :cond_1d

    .line 757
    :cond_1c
    invoke-static {}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->panic()V

    .line 763
    :cond_1d
    const/4 v2, 0x6

    new-array v0, v2, [C

    move-object/from16 v34, v0

    .line 765
    .local v34, "pos":[C
    const/16 v23, 0x0

    .restart local v23    # "i":I
    :goto_13
    move/from16 v0, v23

    move/from16 v1, v31

    if-ge v0, v1, :cond_1e

    .line 766
    move/from16 v0, v23

    int-to-char v2, v0

    aput-char v2, v34, v23

    .line 765
    add-int/lit8 v23, v23, 0x1

    goto :goto_13

    .line 768
    :cond_1e
    const/16 v23, 0x0

    :goto_14
    move/from16 v0, v23

    move/from16 v1, v33

    if-ge v0, v1, :cond_20

    .line 769
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->selector:[C

    aget-char v29, v2, v23

    .line 770
    .local v29, "ll_i":C
    const/16 v27, 0x0

    .line 771
    .local v27, "j":I
    aget-char v40, v34, v27

    .line 772
    .local v40, "tmp":C
    :goto_15
    move/from16 v0, v29

    move/from16 v1, v40

    if-eq v0, v1, :cond_1f

    .line 773
    add-int/lit8 v27, v27, 0x1

    .line 774
    move/from16 v41, v40

    .line 775
    .local v41, "tmp2":C
    aget-char v40, v34, v27

    .line 776
    aput-char v41, v34, v27

    goto :goto_15

    .line 778
    .end local v41    # "tmp2":C
    :cond_1f
    const/4 v2, 0x0

    aput-char v40, v34, v2

    .line 779
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->selectorMtf:[C

    move/from16 v0, v27

    int-to-char v3, v0

    aput-char v3, v2, v23

    .line 768
    add-int/lit8 v23, v23, 0x1

    goto :goto_14

    .line 783
    .end local v27    # "j":I
    .end local v29    # "ll_i":C
    .end local v40    # "tmp":C
    :cond_20
    const/4 v2, 0x6

    const/16 v3, 0x102

    filled-new-array {v2, v3}, [I

    move-result-object v2

    sget-object v3, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {v3, v2}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, [[I

    .line 786
    .local v11, "code":[[I
    const/16 v38, 0x0

    :goto_16
    move/from16 v0, v38

    move/from16 v1, v31

    if-ge v0, v1, :cond_26

    .line 787
    const/16 v5, 0x20

    .line 788
    .local v5, "minLen":I
    const/4 v6, 0x0

    .line 789
    .local v6, "maxLen":I
    const/16 v23, 0x0

    :goto_17
    move/from16 v0, v23

    if-ge v0, v7, :cond_23

    .line 790
    aget-object v2, v28, v38

    aget-char v2, v2, v23

    if-le v2, v6, :cond_21

    .line 791
    aget-object v2, v28, v38

    aget-char v6, v2, v23

    .line 793
    :cond_21
    aget-object v2, v28, v38

    aget-char v2, v2, v23

    if-ge v2, v5, :cond_22

    .line 794
    aget-object v2, v28, v38

    aget-char v5, v2, v23

    .line 789
    :cond_22
    add-int/lit8 v23, v23, 0x1

    goto :goto_17

    .line 797
    :cond_23
    const/16 v2, 0x14

    if-le v6, v2, :cond_24

    .line 798
    invoke-static {}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->panic()V

    .line 800
    :cond_24
    const/4 v2, 0x1

    if-ge v5, v2, :cond_25

    .line 801
    invoke-static {}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->panic()V

    .line 803
    :cond_25
    aget-object v3, v11, v38

    aget-object v4, v28, v38

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v7}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->hbAssignCodes([I[CIII)V

    .line 786
    add-int/lit8 v38, v38, 0x1

    goto :goto_16

    .line 808
    .end local v5    # "minLen":I
    .end local v6    # "maxLen":I
    :cond_26
    const/16 v2, 0x10

    new-array v0, v2, [Z

    move-object/from16 v25, v0

    .line 809
    .local v25, "inUse16":[Z
    const/16 v23, 0x0

    :goto_18
    const/16 v2, 0x10

    move/from16 v0, v23

    if-ge v0, v2, :cond_29

    .line 810
    const/4 v2, 0x0

    aput-boolean v2, v25, v23

    .line 811
    const/16 v27, 0x0

    .restart local v27    # "j":I
    :goto_19
    const/16 v2, 0x10

    move/from16 v0, v27

    if-ge v0, v2, :cond_28

    .line 812
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->inUse:[Z

    mul-int/lit8 v3, v23, 0x10

    add-int v3, v3, v27

    aget-boolean v2, v2, v3

    if-eqz v2, :cond_27

    .line 813
    const/4 v2, 0x1

    aput-boolean v2, v25, v23

    .line 811
    :cond_27
    add-int/lit8 v27, v27, 0x1

    goto :goto_19

    .line 809
    :cond_28
    add-int/lit8 v23, v23, 0x1

    goto :goto_18

    .line 818
    .end local v27    # "j":I
    :cond_29
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->bytesOut:I

    move/from16 v30, v0

    .line 819
    .local v30, "nBytes":I
    const/16 v23, 0x0

    :goto_1a
    const/16 v2, 0x10

    move/from16 v0, v23

    if-ge v0, v2, :cond_2b

    .line 820
    aget-boolean v2, v25, v23

    if-eqz v2, :cond_2a

    .line 821
    const/4 v2, 0x1

    const/4 v3, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->bsW(II)V

    .line 819
    :goto_1b
    add-int/lit8 v23, v23, 0x1

    goto :goto_1a

    .line 823
    :cond_2a
    const/4 v2, 0x1

    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->bsW(II)V

    goto :goto_1b

    .line 827
    :cond_2b
    const/16 v23, 0x0

    :goto_1c
    const/16 v2, 0x10

    move/from16 v0, v23

    if-ge v0, v2, :cond_2e

    .line 828
    aget-boolean v2, v25, v23

    if-eqz v2, :cond_2d

    .line 829
    const/16 v27, 0x0

    .restart local v27    # "j":I
    :goto_1d
    const/16 v2, 0x10

    move/from16 v0, v27

    if-ge v0, v2, :cond_2d

    .line 830
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->inUse:[Z

    mul-int/lit8 v3, v23, 0x10

    add-int v3, v3, v27

    aget-boolean v2, v2, v3

    if-eqz v2, :cond_2c

    .line 831
    const/4 v2, 0x1

    const/4 v3, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->bsW(II)V

    .line 829
    :goto_1e
    add-int/lit8 v27, v27, 0x1

    goto :goto_1d

    .line 833
    :cond_2c
    const/4 v2, 0x1

    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->bsW(II)V

    goto :goto_1e

    .line 827
    .end local v27    # "j":I
    :cond_2d
    add-int/lit8 v23, v23, 0x1

    goto :goto_1c

    .line 842
    :cond_2e
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->bytesOut:I

    move/from16 v30, v0

    .line 843
    const/4 v2, 0x3

    move-object/from16 v0, p0

    move/from16 v1, v31

    invoke-direct {v0, v2, v1}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->bsW(II)V

    .line 844
    const/16 v2, 0xf

    move-object/from16 v0, p0

    move/from16 v1, v33

    invoke-direct {v0, v2, v1}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->bsW(II)V

    .line 845
    const/16 v23, 0x0

    :goto_1f
    move/from16 v0, v23

    move/from16 v1, v33

    if-ge v0, v1, :cond_30

    .line 846
    const/16 v27, 0x0

    .restart local v27    # "j":I
    :goto_20
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->selectorMtf:[C

    aget-char v2, v2, v23

    move/from16 v0, v27

    if-ge v0, v2, :cond_2f

    .line 847
    const/4 v2, 0x1

    const/4 v3, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->bsW(II)V

    .line 846
    add-int/lit8 v27, v27, 0x1

    goto :goto_20

    .line 849
    :cond_2f
    const/4 v2, 0x1

    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->bsW(II)V

    .line 845
    add-int/lit8 v23, v23, 0x1

    goto :goto_1f

    .line 853
    .end local v27    # "j":I
    :cond_30
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->bytesOut:I

    move/from16 v30, v0

    .line 855
    const/16 v38, 0x0

    :goto_21
    move/from16 v0, v38

    move/from16 v1, v31

    if-ge v0, v1, :cond_34

    .line 856
    aget-object v2, v28, v38

    const/4 v3, 0x0

    aget-char v19, v2, v3

    .line 857
    .local v19, "curr":I
    const/4 v2, 0x5

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-direct {v0, v2, v1}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->bsW(II)V

    .line 858
    const/16 v23, 0x0

    :goto_22
    move/from16 v0, v23

    if-ge v0, v7, :cond_33

    .line 859
    :goto_23
    aget-object v2, v28, v38

    aget-char v2, v2, v23

    move/from16 v0, v19

    if-ge v0, v2, :cond_31

    .line 860
    const/4 v2, 0x2

    const/4 v3, 0x2

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->bsW(II)V

    .line 861
    add-int/lit8 v19, v19, 0x1

    goto :goto_23

    .line 863
    :cond_31
    :goto_24
    aget-object v2, v28, v38

    aget-char v2, v2, v23

    move/from16 v0, v19

    if-le v0, v2, :cond_32

    .line 864
    const/4 v2, 0x2

    const/4 v3, 0x3

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->bsW(II)V

    .line 865
    add-int/lit8 v19, v19, -0x1

    goto :goto_24

    .line 867
    :cond_32
    const/4 v2, 0x1

    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->bsW(II)V

    .line 858
    add-int/lit8 v23, v23, 0x1

    goto :goto_22

    .line 855
    :cond_33
    add-int/lit8 v38, v38, 0x1

    goto :goto_21

    .line 872
    .end local v19    # "curr":I
    :cond_34
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->bytesOut:I

    move/from16 v30, v0

    .line 873
    const/16 v37, 0x0

    .line 874
    .local v37, "selCtr":I
    const/16 v22, 0x0

    .line 876
    :goto_25
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->nMTF:I

    move/from16 v0, v22

    if-lt v0, v2, :cond_36

    .line 891
    move/from16 v0, v37

    move/from16 v1, v33

    if-eq v0, v1, :cond_35

    .line 892
    invoke-static {}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->panic()V

    .line 894
    :cond_35
    return-void

    .line 879
    :cond_36
    add-int/lit8 v2, v22, 0x32

    add-int/lit8 v21, v2, -0x1

    .line 880
    .restart local v21    # "ge":I
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->nMTF:I

    move/from16 v0, v21

    if-lt v0, v2, :cond_37

    .line 881
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->nMTF:I

    add-int/lit8 v21, v2, -0x1

    .line 883
    :cond_37
    move/from16 v23, v22

    :goto_26
    move/from16 v0, v23

    move/from16 v1, v21

    if-gt v0, v1, :cond_38

    .line 884
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->selector:[C

    aget-char v2, v2, v37

    aget-object v2, v28, v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->szptr:[S

    aget-short v3, v3, v23

    aget-char v2, v2, v3

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->selector:[C

    aget-char v3, v3, v37

    aget-object v3, v11, v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->szptr:[S

    aget-short v4, v4, v23

    aget v3, v3, v4

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->bsW(II)V

    .line 883
    add-int/lit8 v23, v23, 0x1

    goto :goto_26

    .line 888
    :cond_38
    add-int/lit8 v22, v21, 0x1

    .line 889
    add-int/lit8 v37, v37, 0x1

    goto :goto_25
.end method

.method private simpleSort(III)V
    .locals 9
    .param p1, "lo"    # I
    .param p2, "hi"    # I
    .param p3, "d"    # I

    .prologue
    .line 908
    sub-int v6, p2, p1

    add-int/lit8 v0, v6, 0x1

    .line 909
    .local v0, "bigN":I
    const/4 v6, 0x2

    if-ge v0, v6, :cond_1

    .line 977
    :cond_0
    :goto_0
    return-void

    .line 913
    :cond_1
    const/4 v2, 0x0

    .line 914
    .local v2, "hp":I
    :goto_1
    iget-object v6, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->incs:[I

    aget v6, v6, v2

    if-ge v6, v0, :cond_2

    .line 915
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 917
    :cond_2
    add-int/lit8 v2, v2, -0x1

    .line 919
    :goto_2
    if-ltz v2, :cond_0

    .line 920
    iget-object v6, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->incs:[I

    aget v1, v6, v2

    .line 922
    .local v1, "h":I
    add-int v3, p1, v1

    .line 925
    .local v3, "i":I
    :cond_3
    if-le v3, p2, :cond_5

    .line 919
    :cond_4
    add-int/lit8 v2, v2, -0x1

    goto :goto_2

    .line 928
    :cond_5
    iget-object v6, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->zptr:[I

    aget v5, v6, v3

    .line 929
    .local v5, "v":I
    move v4, v3

    .line 930
    .local v4, "j":I
    :cond_6
    iget-object v6, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->zptr:[I

    sub-int v7, v4, v1

    aget v6, v6, v7

    add-int/2addr v6, p3

    add-int v7, v5, p3

    invoke-direct {p0, v6, v7}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->fullGtU(II)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 931
    iget-object v6, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->zptr:[I

    iget-object v7, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->zptr:[I

    sub-int v8, v4, v1

    aget v7, v7, v8

    aput v7, v6, v4

    .line 932
    sub-int/2addr v4, v1

    .line 933
    add-int v6, p1, v1

    add-int/lit8 v6, v6, -0x1

    if-gt v4, v6, :cond_6

    .line 937
    :cond_7
    iget-object v6, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->zptr:[I

    aput v5, v6, v4

    .line 938
    add-int/lit8 v3, v3, 0x1

    .line 941
    if-gt v3, p2, :cond_4

    .line 944
    iget-object v6, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->zptr:[I

    aget v5, v6, v3

    .line 945
    move v4, v3

    .line 946
    :cond_8
    iget-object v6, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->zptr:[I

    sub-int v7, v4, v1

    aget v6, v6, v7

    add-int/2addr v6, p3

    add-int v7, v5, p3

    invoke-direct {p0, v6, v7}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->fullGtU(II)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 947
    iget-object v6, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->zptr:[I

    iget-object v7, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->zptr:[I

    sub-int v8, v4, v1

    aget v7, v7, v8

    aput v7, v6, v4

    .line 948
    sub-int/2addr v4, v1

    .line 949
    add-int v6, p1, v1

    add-int/lit8 v6, v6, -0x1

    if-gt v4, v6, :cond_8

    .line 953
    :cond_9
    iget-object v6, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->zptr:[I

    aput v5, v6, v4

    .line 954
    add-int/lit8 v3, v3, 0x1

    .line 957
    if-gt v3, p2, :cond_4

    .line 960
    iget-object v6, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->zptr:[I

    aget v5, v6, v3

    .line 961
    move v4, v3

    .line 962
    :cond_a
    iget-object v6, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->zptr:[I

    sub-int v7, v4, v1

    aget v6, v6, v7

    add-int/2addr v6, p3

    add-int v7, v5, p3

    invoke-direct {p0, v6, v7}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->fullGtU(II)Z

    move-result v6

    if-eqz v6, :cond_b

    .line 963
    iget-object v6, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->zptr:[I

    iget-object v7, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->zptr:[I

    sub-int v8, v4, v1

    aget v7, v7, v8

    aput v7, v6, v4

    .line 964
    sub-int/2addr v4, v1

    .line 965
    add-int v6, p1, v1

    add-int/lit8 v6, v6, -0x1

    if-gt v4, v6, :cond_a

    .line 969
    :cond_b
    iget-object v6, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->zptr:[I

    aput v5, v6, v4

    .line 970
    add-int/lit8 v3, v3, 0x1

    .line 972
    iget v6, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->workDone:I

    iget v7, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->workLimit:I

    if-le v6, v7, :cond_3

    iget-boolean v6, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->firstAttempt:Z

    if-eqz v6, :cond_3

    goto/16 :goto_0
.end method

.method private vswap(III)V
    .locals 3
    .param p1, "p1"    # I
    .param p2, "p2"    # I
    .param p3, "n"    # I

    .prologue
    .line 980
    const/4 v0, 0x0

    .line 981
    .local v0, "temp":I
    :goto_0
    if-lez p3, :cond_0

    .line 982
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->zptr:[I

    aget v0, v1, p1

    .line 983
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->zptr:[I

    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->zptr:[I

    aget v2, v2, p2

    aput v2, v1, p1

    .line 984
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->zptr:[I

    aput v0, v1, p2

    .line 985
    add-int/lit8 p1, p1, 0x1

    .line 986
    add-int/lit8 p2, p2, 0x1

    .line 987
    add-int/lit8 p3, p3, -0x1

    goto :goto_0

    .line 989
    :cond_0
    return-void
.end method

.method private writeRun()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 336
    iget v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->last:I

    iget v2, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->allowableBlockSize:I

    if-ge v1, v2, :cond_1

    .line 337
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->inUse:[Z

    iget v2, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->currentChar:I

    aput-boolean v3, v1, v2

    .line 338
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->runLength:I

    if-ge v0, v1, :cond_0

    .line 339
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->mCrc:Lcom/android/sec/org/bouncycastle/apache/bzip2/CRC;

    iget v2, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->currentChar:I

    int-to-char v2, v2

    invoke-virtual {v1, v2}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CRC;->updateCRC(I)V

    .line 338
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 341
    :cond_0
    iget v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->runLength:I

    packed-switch v1, :pswitch_data_0

    .line 361
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->inUse:[Z

    iget v2, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->runLength:I

    add-int/lit8 v2, v2, -0x4

    aput-boolean v3, v1, v2

    .line 362
    iget v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->last:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->last:I

    .line 363
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->block:[C

    iget v2, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->last:I

    add-int/lit8 v2, v2, 0x1

    iget v3, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->currentChar:I

    int-to-char v3, v3

    aput-char v3, v1, v2

    .line 364
    iget v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->last:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->last:I

    .line 365
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->block:[C

    iget v2, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->last:I

    add-int/lit8 v2, v2, 0x1

    iget v3, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->currentChar:I

    int-to-char v3, v3

    aput-char v3, v1, v2

    .line 366
    iget v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->last:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->last:I

    .line 367
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->block:[C

    iget v2, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->last:I

    add-int/lit8 v2, v2, 0x1

    iget v3, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->currentChar:I

    int-to-char v3, v3

    aput-char v3, v1, v2

    .line 368
    iget v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->last:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->last:I

    .line 369
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->block:[C

    iget v2, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->last:I

    add-int/lit8 v2, v2, 0x1

    iget v3, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->currentChar:I

    int-to-char v3, v3

    aput-char v3, v1, v2

    .line 370
    iget v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->last:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->last:I

    .line 371
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->block:[C

    iget v2, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->last:I

    add-int/lit8 v2, v2, 0x1

    iget v3, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->runLength:I

    add-int/lit8 v3, v3, -0x4

    int-to-char v3, v3

    aput-char v3, v1, v2

    .line 379
    .end local v0    # "i":I
    :goto_1
    return-void

    .line 343
    .restart local v0    # "i":I
    :pswitch_0
    iget v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->last:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->last:I

    .line 344
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->block:[C

    iget v2, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->last:I

    add-int/lit8 v2, v2, 0x1

    iget v3, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->currentChar:I

    int-to-char v3, v3

    aput-char v3, v1, v2

    goto :goto_1

    .line 347
    :pswitch_1
    iget v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->last:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->last:I

    .line 348
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->block:[C

    iget v2, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->last:I

    add-int/lit8 v2, v2, 0x1

    iget v3, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->currentChar:I

    int-to-char v3, v3

    aput-char v3, v1, v2

    .line 349
    iget v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->last:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->last:I

    .line 350
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->block:[C

    iget v2, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->last:I

    add-int/lit8 v2, v2, 0x1

    iget v3, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->currentChar:I

    int-to-char v3, v3

    aput-char v3, v1, v2

    goto :goto_1

    .line 353
    :pswitch_2
    iget v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->last:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->last:I

    .line 354
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->block:[C

    iget v2, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->last:I

    add-int/lit8 v2, v2, 0x1

    iget v3, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->currentChar:I

    int-to-char v3, v3

    aput-char v3, v1, v2

    .line 355
    iget v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->last:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->last:I

    .line 356
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->block:[C

    iget v2, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->last:I

    add-int/lit8 v2, v2, 0x1

    iget v3, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->currentChar:I

    int-to-char v3, v3

    aput-char v3, v1, v2

    .line 357
    iget v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->last:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->last:I

    .line 358
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->block:[C

    iget v2, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->last:I

    add-int/lit8 v2, v2, 0x1

    iget v3, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->currentChar:I

    int-to-char v3, v3

    aput-char v3, v1, v2

    goto :goto_1

    .line 375
    .end local v0    # "i":I
    :cond_1
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->endBlock()V

    .line 376
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->initBlock()V

    .line 377
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->writeRun()V

    goto :goto_1

    .line 341
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 389
    iget-boolean v0, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->closed:Z

    if-eqz v0, :cond_0

    .line 398
    :goto_0
    return-void

    .line 393
    :cond_0
    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->finish()V

    .line 395
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->closed:Z

    .line 396
    invoke-super {p0}, Ljava/io/OutputStream;->close()V

    .line 397
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->bsStream:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V

    goto :goto_0
.end method

.method protected finalize()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 384
    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->close()V

    .line 385
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 386
    return-void
.end method

.method public finish()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 401
    iget-boolean v0, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->finished:Z

    if-eqz v0, :cond_0

    .line 413
    :goto_0
    return-void

    .line 405
    :cond_0
    iget v0, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->runLength:I

    if-lez v0, :cond_1

    .line 406
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->writeRun()V

    .line 408
    :cond_1
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->currentChar:I

    .line 409
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->endBlock()V

    .line 410
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->endCompression()V

    .line 411
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->finished:Z

    .line 412
    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->flush()V

    goto :goto_0
.end method

.method public flush()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 416
    invoke-super {p0}, Ljava/io/OutputStream;->flush()V

    .line 417
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->bsStream:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->flush()V

    .line 418
    return-void
.end method

.method public write(I)V
    .locals 4
    .param p1, "bv"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, -0x1

    .line 315
    add-int/lit16 v1, p1, 0x100

    rem-int/lit16 v0, v1, 0x100

    .line 316
    .local v0, "b":I
    iget v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->currentChar:I

    if-eq v1, v3, :cond_2

    .line 317
    iget v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->currentChar:I

    if-ne v1, v0, :cond_1

    .line 318
    iget v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->runLength:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->runLength:I

    .line 319
    iget v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->runLength:I

    const/16 v2, 0xfe

    if-le v1, v2, :cond_0

    .line 320
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->writeRun()V

    .line 321
    iput v3, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->currentChar:I

    .line 322
    const/4 v1, 0x0

    iput v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->runLength:I

    .line 333
    :cond_0
    :goto_0
    return-void

    .line 325
    :cond_1
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->writeRun()V

    .line 326
    const/4 v1, 0x1

    iput v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->runLength:I

    .line 327
    iput v0, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->currentChar:I

    goto :goto_0

    .line 330
    :cond_2
    iput v0, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->currentChar:I

    .line 331
    iget v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->runLength:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2OutputStream;->runLength:I

    goto :goto_0
.end method
