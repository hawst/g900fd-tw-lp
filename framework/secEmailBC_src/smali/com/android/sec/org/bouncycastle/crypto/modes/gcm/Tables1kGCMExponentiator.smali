.class public Lcom/android/sec/org/bouncycastle/crypto/modes/gcm/Tables1kGCMExponentiator;
.super Ljava/lang/Object;
.source "Tables1kGCMExponentiator.java"

# interfaces
.implements Lcom/android/sec/org/bouncycastle/crypto/modes/gcm/GCMExponentiator;


# instance fields
.field private lookupPowX2:Ljava/util/Vector;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private ensureAvailable(I)V
    .locals 4
    .param p1, "bit"    # I

    .prologue
    .line 44
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/crypto/modes/gcm/Tables1kGCMExponentiator;->lookupPowX2:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v0

    .line 45
    .local v0, "count":I
    if-gt v0, p1, :cond_1

    .line 47
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/crypto/modes/gcm/Tables1kGCMExponentiator;->lookupPowX2:Ljava/util/Vector;

    add-int/lit8 v3, v0, -0x1

    invoke-virtual {v2, v3}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [B

    move-object v1, v2

    check-cast v1, [B

    .line 50
    .local v1, "tmp":[B
    :cond_0
    invoke-static {v1}, Lcom/android/sec/org/bouncycastle/util/Arrays;->clone([B)[B

    move-result-object v1

    .line 51
    invoke-static {v1, v1}, Lcom/android/sec/org/bouncycastle/crypto/modes/gcm/GCMUtil;->multiply([B[B)V

    .line 52
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/crypto/modes/gcm/Tables1kGCMExponentiator;->lookupPowX2:Ljava/util/Vector;

    invoke-virtual {v2, v1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 54
    add-int/lit8 v0, v0, 0x1

    if-le v0, p1, :cond_0

    .line 56
    .end local v1    # "tmp":[B
    :cond_1
    return-void
.end method


# virtual methods
.method public exponentiateX(J[B)V
    .locals 9
    .param p1, "pow"    # J
    .param p3, "output"    # [B

    .prologue
    const-wide/16 v6, 0x0

    const/4 v4, 0x0

    .line 26
    invoke-static {}, Lcom/android/sec/org/bouncycastle/crypto/modes/gcm/GCMUtil;->oneAsBytes()[B

    move-result-object v1

    .line 27
    .local v1, "y":[B
    const/4 v0, 0x0

    .line 28
    .local v0, "bit":I
    :goto_0
    cmp-long v2, p1, v6

    if-lez v2, :cond_1

    .line 30
    const-wide/16 v2, 0x1

    and-long/2addr v2, p1

    cmp-long v2, v2, v6

    if-eqz v2, :cond_0

    .line 32
    invoke-direct {p0, v0}, Lcom/android/sec/org/bouncycastle/crypto/modes/gcm/Tables1kGCMExponentiator;->ensureAvailable(I)V

    .line 33
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/crypto/modes/gcm/Tables1kGCMExponentiator;->lookupPowX2:Ljava/util/Vector;

    invoke-virtual {v2, v0}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [B

    check-cast v2, [B

    invoke-static {v1, v2}, Lcom/android/sec/org/bouncycastle/crypto/modes/gcm/GCMUtil;->multiply([B[B)V

    .line 35
    :cond_0
    add-int/lit8 v0, v0, 0x1

    .line 36
    const/4 v2, 0x1

    ushr-long/2addr p1, v2

    goto :goto_0

    .line 39
    :cond_1
    const/16 v2, 0x10

    invoke-static {v1, v4, p3, v4, v2}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 40
    return-void
.end method

.method public init([B)V
    .locals 2
    .param p1, "x"    # [B

    .prologue
    .line 15
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/crypto/modes/gcm/Tables1kGCMExponentiator;->lookupPowX2:Ljava/util/Vector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/crypto/modes/gcm/Tables1kGCMExponentiator;->lookupPowX2:Ljava/util/Vector;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    check-cast v0, [B

    invoke-static {p1, v0}, Lcom/android/sec/org/bouncycastle/util/Arrays;->areEqual([B[B)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 22
    :goto_0
    return-void

    .line 20
    :cond_0
    new-instance v0, Ljava/util/Vector;

    const/16 v1, 0x8

    invoke-direct {v0, v1}, Ljava/util/Vector;-><init>(I)V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/crypto/modes/gcm/Tables1kGCMExponentiator;->lookupPowX2:Ljava/util/Vector;

    .line 21
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/crypto/modes/gcm/Tables1kGCMExponentiator;->lookupPowX2:Ljava/util/Vector;

    invoke-static {p1}, Lcom/android/sec/org/bouncycastle/util/Arrays;->clone([B)[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    goto :goto_0
.end method
