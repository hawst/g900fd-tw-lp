.class public Lcom/android/sec/org/bouncycastle/cms/DefaultSignedAttributeTableGenerator;
.super Ljava/lang/Object;
.source "DefaultSignedAttributeTableGenerator.java"

# interfaces
.implements Lcom/android/sec/org/bouncycastle/cms/CMSAttributeTableGenerator;


# instance fields
.field private final table:Ljava/util/Hashtable;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/DefaultSignedAttributeTableGenerator;->table:Ljava/util/Hashtable;

    .line 29
    return-void
.end method

.method public constructor <init>(Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;)V
    .locals 1
    .param p1, "attributeTable"    # Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    if-eqz p1, :cond_0

    .line 41
    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;->toHashtable()Ljava/util/Hashtable;

    move-result-object v0

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/DefaultSignedAttributeTableGenerator;->table:Ljava/util/Hashtable;

    .line 47
    :goto_0
    return-void

    .line 45
    :cond_0
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/DefaultSignedAttributeTableGenerator;->table:Ljava/util/Hashtable;

    goto :goto_0
.end method


# virtual methods
.method protected createStandardAttributeTable(Ljava/util/Map;)Ljava/util/Hashtable;
    .locals 8
    .param p1, "parameters"    # Ljava/util/Map;

    .prologue
    .line 62
    iget-object v5, p0, Lcom/android/sec/org/bouncycastle/cms/DefaultSignedAttributeTableGenerator;->table:Ljava/util/Hashtable;

    invoke-virtual {v5}, Ljava/util/Hashtable;->clone()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Hashtable;

    .line 64
    .local v4, "std":Ljava/util/Hashtable;
    sget-object v5, Lcom/android/sec/org/bouncycastle/asn1/cms/CMSAttributes;->contentType:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    invoke-virtual {v4, v5}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 66
    const-string v5, "contentType"

    invoke-interface {p1, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    invoke-static {v5}, Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;->getInstance(Ljava/lang/Object;)Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    move-result-object v1

    .line 70
    .local v1, "contentType":Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;
    if-eqz v1, :cond_0

    .line 72
    new-instance v0, Lcom/android/sec/org/bouncycastle/asn1/cms/Attribute;

    sget-object v5, Lcom/android/sec/org/bouncycastle/asn1/cms/CMSAttributes;->contentType:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    new-instance v6, Lcom/android/sec/org/bouncycastle/asn1/DERSet;

    invoke-direct {v6, v1}, Lcom/android/sec/org/bouncycastle/asn1/DERSet;-><init>(Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;)V

    invoke-direct {v0, v5, v6}, Lcom/android/sec/org/bouncycastle/asn1/cms/Attribute;-><init>(Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;)V

    .line 74
    .local v0, "attr":Lcom/android/sec/org/bouncycastle/asn1/cms/Attribute;
    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/cms/Attribute;->getAttrType()Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    move-result-object v5

    invoke-virtual {v4, v5, v0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 78
    .end local v0    # "attr":Lcom/android/sec/org/bouncycastle/asn1/cms/Attribute;
    .end local v1    # "contentType":Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;
    :cond_0
    sget-object v5, Lcom/android/sec/org/bouncycastle/asn1/cms/CMSAttributes;->signingTime:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    invoke-virtual {v4, v5}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 80
    new-instance v3, Ljava/util/Date;

    invoke-direct {v3}, Ljava/util/Date;-><init>()V

    .line 81
    .local v3, "signingTime":Ljava/util/Date;
    new-instance v0, Lcom/android/sec/org/bouncycastle/asn1/cms/Attribute;

    sget-object v5, Lcom/android/sec/org/bouncycastle/asn1/cms/CMSAttributes;->signingTime:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    new-instance v6, Lcom/android/sec/org/bouncycastle/asn1/DERSet;

    new-instance v7, Lcom/android/sec/org/bouncycastle/asn1/cms/Time;

    invoke-direct {v7, v3}, Lcom/android/sec/org/bouncycastle/asn1/cms/Time;-><init>(Ljava/util/Date;)V

    invoke-direct {v6, v7}, Lcom/android/sec/org/bouncycastle/asn1/DERSet;-><init>(Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;)V

    invoke-direct {v0, v5, v6}, Lcom/android/sec/org/bouncycastle/asn1/cms/Attribute;-><init>(Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;)V

    .line 83
    .restart local v0    # "attr":Lcom/android/sec/org/bouncycastle/asn1/cms/Attribute;
    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/cms/Attribute;->getAttrType()Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    move-result-object v5

    invoke-virtual {v4, v5, v0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 86
    .end local v0    # "attr":Lcom/android/sec/org/bouncycastle/asn1/cms/Attribute;
    .end local v3    # "signingTime":Ljava/util/Date;
    :cond_1
    sget-object v5, Lcom/android/sec/org/bouncycastle/asn1/cms/CMSAttributes;->messageDigest:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    invoke-virtual {v4, v5}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 88
    const-string v5, "digest"

    invoke-interface {p1, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [B

    move-object v2, v5

    check-cast v2, [B

    .line 90
    .local v2, "messageDigest":[B
    new-instance v0, Lcom/android/sec/org/bouncycastle/asn1/cms/Attribute;

    sget-object v5, Lcom/android/sec/org/bouncycastle/asn1/cms/CMSAttributes;->messageDigest:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    new-instance v6, Lcom/android/sec/org/bouncycastle/asn1/DERSet;

    new-instance v7, Lcom/android/sec/org/bouncycastle/asn1/DEROctetString;

    invoke-direct {v7, v2}, Lcom/android/sec/org/bouncycastle/asn1/DEROctetString;-><init>([B)V

    invoke-direct {v6, v7}, Lcom/android/sec/org/bouncycastle/asn1/DERSet;-><init>(Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;)V

    invoke-direct {v0, v5, v6}, Lcom/android/sec/org/bouncycastle/asn1/cms/Attribute;-><init>(Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;)V

    .line 92
    .restart local v0    # "attr":Lcom/android/sec/org/bouncycastle/asn1/cms/Attribute;
    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/cms/Attribute;->getAttrType()Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    move-result-object v5

    invoke-virtual {v4, v5, v0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 95
    .end local v0    # "attr":Lcom/android/sec/org/bouncycastle/asn1/cms/Attribute;
    .end local v2    # "messageDigest":[B
    :cond_2
    return-object v4
.end method

.method public getAttributes(Ljava/util/Map;)Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;
    .locals 2
    .param p1, "parameters"    # Ljava/util/Map;

    .prologue
    .line 104
    new-instance v0, Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;

    invoke-virtual {p0, p1}, Lcom/android/sec/org/bouncycastle/cms/DefaultSignedAttributeTableGenerator;->createStandardAttributeTable(Ljava/util/Map;)Ljava/util/Hashtable;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;-><init>(Ljava/util/Hashtable;)V

    return-object v0
.end method
