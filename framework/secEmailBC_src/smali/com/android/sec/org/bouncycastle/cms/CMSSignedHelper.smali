.class Lcom/android/sec/org/bouncycastle/cms/CMSSignedHelper;
.super Ljava/lang/Object;
.source "CMSSignedHelper.java"


# static fields
.field static final INSTANCE:Lcom/android/sec/org/bouncycastle/cms/CMSSignedHelper;

.field private static final digestAlgs:Ljava/util/Map;

.field private static final digestAliases:Ljava/util/Map;

.field private static final encryptionAlgs:Ljava/util/Map;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 53
    new-instance v0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedHelper;

    invoke-direct {v0}, Lcom/android/sec/org/bouncycastle/cms/CMSSignedHelper;-><init>()V

    sput-object v0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedHelper;->INSTANCE:Lcom/android/sec/org/bouncycastle/cms/CMSSignedHelper;

    .line 55
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedHelper;->encryptionAlgs:Ljava/util/Map;

    .line 56
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedHelper;->digestAlgs:Ljava/util/Map;

    .line 57
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedHelper;->digestAliases:Ljava/util/Map;

    .line 70
    sget-object v0, Lcom/android/sec/org/bouncycastle/asn1/nist/NISTObjectIdentifiers;->dsa_with_sha256:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    const-string v1, "SHA256"

    const-string v2, "DSA"

    invoke-static {v0, v1, v2}, Lcom/android/sec/org/bouncycastle/cms/CMSSignedHelper;->addEntries(Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    sget-object v0, Lcom/android/sec/org/bouncycastle/asn1/nist/NISTObjectIdentifiers;->dsa_with_sha384:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    const-string v1, "SHA384"

    const-string v2, "DSA"

    invoke-static {v0, v1, v2}, Lcom/android/sec/org/bouncycastle/cms/CMSSignedHelper;->addEntries(Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    sget-object v0, Lcom/android/sec/org/bouncycastle/asn1/nist/NISTObjectIdentifiers;->dsa_with_sha512:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    const-string v1, "SHA512"

    const-string v2, "DSA"

    invoke-static {v0, v1, v2}, Lcom/android/sec/org/bouncycastle/cms/CMSSignedHelper;->addEntries(Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    sget-object v0, Lcom/android/sec/org/bouncycastle/asn1/oiw/OIWObjectIdentifiers;->dsaWithSHA1:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    const-string v1, "SHA1"

    const-string v2, "DSA"

    invoke-static {v0, v1, v2}, Lcom/android/sec/org/bouncycastle/cms/CMSSignedHelper;->addEntries(Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    sget-object v0, Lcom/android/sec/org/bouncycastle/asn1/oiw/OIWObjectIdentifiers;->md5WithRSA:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    const-string v1, "MD5"

    const-string v2, "RSA"

    invoke-static {v0, v1, v2}, Lcom/android/sec/org/bouncycastle/cms/CMSSignedHelper;->addEntries(Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    sget-object v0, Lcom/android/sec/org/bouncycastle/asn1/oiw/OIWObjectIdentifiers;->sha1WithRSA:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    const-string v1, "SHA1"

    const-string v2, "RSA"

    invoke-static {v0, v1, v2}, Lcom/android/sec/org/bouncycastle/cms/CMSSignedHelper;->addEntries(Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    sget-object v0, Lcom/android/sec/org/bouncycastle/asn1/pkcs/PKCSObjectIdentifiers;->md5WithRSAEncryption:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    const-string v1, "MD5"

    const-string v2, "RSA"

    invoke-static {v0, v1, v2}, Lcom/android/sec/org/bouncycastle/cms/CMSSignedHelper;->addEntries(Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    sget-object v0, Lcom/android/sec/org/bouncycastle/asn1/pkcs/PKCSObjectIdentifiers;->sha1WithRSAEncryption:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    const-string v1, "SHA1"

    const-string v2, "RSA"

    invoke-static {v0, v1, v2}, Lcom/android/sec/org/bouncycastle/cms/CMSSignedHelper;->addEntries(Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    sget-object v0, Lcom/android/sec/org/bouncycastle/asn1/pkcs/PKCSObjectIdentifiers;->sha256WithRSAEncryption:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    const-string v1, "SHA256"

    const-string v2, "RSA"

    invoke-static {v0, v1, v2}, Lcom/android/sec/org/bouncycastle/cms/CMSSignedHelper;->addEntries(Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    sget-object v0, Lcom/android/sec/org/bouncycastle/asn1/pkcs/PKCSObjectIdentifiers;->sha384WithRSAEncryption:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    const-string v1, "SHA384"

    const-string v2, "RSA"

    invoke-static {v0, v1, v2}, Lcom/android/sec/org/bouncycastle/cms/CMSSignedHelper;->addEntries(Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;Ljava/lang/String;Ljava/lang/String;)V

    .line 91
    sget-object v0, Lcom/android/sec/org/bouncycastle/asn1/pkcs/PKCSObjectIdentifiers;->sha512WithRSAEncryption:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    const-string v1, "SHA512"

    const-string v2, "RSA"

    invoke-static {v0, v1, v2}, Lcom/android/sec/org/bouncycastle/cms/CMSSignedHelper;->addEntries(Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    sget-object v0, Lcom/android/sec/org/bouncycastle/asn1/x9/X9ObjectIdentifiers;->ecdsa_with_SHA1:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    const-string v1, "SHA1"

    const-string v2, "ECDSA"

    invoke-static {v0, v1, v2}, Lcom/android/sec/org/bouncycastle/cms/CMSSignedHelper;->addEntries(Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    sget-object v0, Lcom/android/sec/org/bouncycastle/asn1/x9/X9ObjectIdentifiers;->ecdsa_with_SHA256:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    const-string v1, "SHA256"

    const-string v2, "ECDSA"

    invoke-static {v0, v1, v2}, Lcom/android/sec/org/bouncycastle/cms/CMSSignedHelper;->addEntries(Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    sget-object v0, Lcom/android/sec/org/bouncycastle/asn1/x9/X9ObjectIdentifiers;->ecdsa_with_SHA384:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    const-string v1, "SHA384"

    const-string v2, "ECDSA"

    invoke-static {v0, v1, v2}, Lcom/android/sec/org/bouncycastle/cms/CMSSignedHelper;->addEntries(Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;Ljava/lang/String;Ljava/lang/String;)V

    .line 98
    sget-object v0, Lcom/android/sec/org/bouncycastle/asn1/x9/X9ObjectIdentifiers;->ecdsa_with_SHA512:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    const-string v1, "SHA512"

    const-string v2, "ECDSA"

    invoke-static {v0, v1, v2}, Lcom/android/sec/org/bouncycastle/cms/CMSSignedHelper;->addEntries(Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;Ljava/lang/String;Ljava/lang/String;)V

    .line 99
    sget-object v0, Lcom/android/sec/org/bouncycastle/asn1/x9/X9ObjectIdentifiers;->id_dsa_with_sha1:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    const-string v1, "SHA1"

    const-string v2, "DSA"

    invoke-static {v0, v1, v2}, Lcom/android/sec/org/bouncycastle/cms/CMSSignedHelper;->addEntries(Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    sget-object v0, Lcom/android/sec/org/bouncycastle/asn1/eac/EACObjectIdentifiers;->id_TA_ECDSA_SHA_1:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    const-string v1, "SHA1"

    const-string v2, "ECDSA"

    invoke-static {v0, v1, v2}, Lcom/android/sec/org/bouncycastle/cms/CMSSignedHelper;->addEntries(Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;Ljava/lang/String;Ljava/lang/String;)V

    .line 104
    sget-object v0, Lcom/android/sec/org/bouncycastle/asn1/eac/EACObjectIdentifiers;->id_TA_ECDSA_SHA_256:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    const-string v1, "SHA256"

    const-string v2, "ECDSA"

    invoke-static {v0, v1, v2}, Lcom/android/sec/org/bouncycastle/cms/CMSSignedHelper;->addEntries(Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    sget-object v0, Lcom/android/sec/org/bouncycastle/asn1/eac/EACObjectIdentifiers;->id_TA_ECDSA_SHA_384:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    const-string v1, "SHA384"

    const-string v2, "ECDSA"

    invoke-static {v0, v1, v2}, Lcom/android/sec/org/bouncycastle/cms/CMSSignedHelper;->addEntries(Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;Ljava/lang/String;Ljava/lang/String;)V

    .line 106
    sget-object v0, Lcom/android/sec/org/bouncycastle/asn1/eac/EACObjectIdentifiers;->id_TA_ECDSA_SHA_512:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    const-string v1, "SHA512"

    const-string v2, "ECDSA"

    invoke-static {v0, v1, v2}, Lcom/android/sec/org/bouncycastle/cms/CMSSignedHelper;->addEntries(Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;Ljava/lang/String;Ljava/lang/String;)V

    .line 107
    sget-object v0, Lcom/android/sec/org/bouncycastle/asn1/eac/EACObjectIdentifiers;->id_TA_RSA_v1_5_SHA_1:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    const-string v1, "SHA1"

    const-string v2, "RSA"

    invoke-static {v0, v1, v2}, Lcom/android/sec/org/bouncycastle/cms/CMSSignedHelper;->addEntries(Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    sget-object v0, Lcom/android/sec/org/bouncycastle/asn1/eac/EACObjectIdentifiers;->id_TA_RSA_v1_5_SHA_256:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    const-string v1, "SHA256"

    const-string v2, "RSA"

    invoke-static {v0, v1, v2}, Lcom/android/sec/org/bouncycastle/cms/CMSSignedHelper;->addEntries(Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    sget-object v0, Lcom/android/sec/org/bouncycastle/asn1/eac/EACObjectIdentifiers;->id_TA_RSA_PSS_SHA_1:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    const-string v1, "SHA1"

    const-string v2, "RSAandMGF1"

    invoke-static {v0, v1, v2}, Lcom/android/sec/org/bouncycastle/cms/CMSSignedHelper;->addEntries(Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;Ljava/lang/String;Ljava/lang/String;)V

    .line 110
    sget-object v0, Lcom/android/sec/org/bouncycastle/asn1/eac/EACObjectIdentifiers;->id_TA_RSA_PSS_SHA_256:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    const-string v1, "SHA256"

    const-string v2, "RSAandMGF1"

    invoke-static {v0, v1, v2}, Lcom/android/sec/org/bouncycastle/cms/CMSSignedHelper;->addEntries(Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    sget-object v0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedHelper;->encryptionAlgs:Ljava/util/Map;

    sget-object v1, Lcom/android/sec/org/bouncycastle/asn1/x9/X9ObjectIdentifiers;->id_dsa:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    invoke-virtual {v1}, Lcom/android/sec/org/bouncycastle/asn1/DERObjectIdentifier;->getId()Ljava/lang/String;

    move-result-object v1

    const-string v2, "DSA"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 113
    sget-object v0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedHelper;->encryptionAlgs:Ljava/util/Map;

    sget-object v1, Lcom/android/sec/org/bouncycastle/asn1/pkcs/PKCSObjectIdentifiers;->rsaEncryption:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    invoke-virtual {v1}, Lcom/android/sec/org/bouncycastle/asn1/DERObjectIdentifier;->getId()Ljava/lang/String;

    move-result-object v1

    const-string v2, "RSA"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 114
    sget-object v0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedHelper;->encryptionAlgs:Ljava/util/Map;

    sget-object v1, Lcom/android/sec/org/bouncycastle/asn1/teletrust/TeleTrusTObjectIdentifiers;->teleTrusTRSAsignatureAlgorithm:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    const-string v2, "RSA"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 115
    sget-object v0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedHelper;->encryptionAlgs:Ljava/util/Map;

    sget-object v1, Lcom/android/sec/org/bouncycastle/asn1/x509/X509ObjectIdentifiers;->id_ea_rsa:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    invoke-virtual {v1}, Lcom/android/sec/org/bouncycastle/asn1/DERObjectIdentifier;->getId()Ljava/lang/String;

    move-result-object v1

    const-string v2, "RSA"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 128
    sget-object v0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedHelper;->digestAlgs:Ljava/util/Map;

    sget-object v1, Lcom/android/sec/org/bouncycastle/asn1/pkcs/PKCSObjectIdentifiers;->md5:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    invoke-virtual {v1}, Lcom/android/sec/org/bouncycastle/asn1/DERObjectIdentifier;->getId()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MD5"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 129
    sget-object v0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedHelper;->digestAlgs:Ljava/util/Map;

    sget-object v1, Lcom/android/sec/org/bouncycastle/asn1/oiw/OIWObjectIdentifiers;->idSHA1:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    invoke-virtual {v1}, Lcom/android/sec/org/bouncycastle/asn1/DERObjectIdentifier;->getId()Ljava/lang/String;

    move-result-object v1

    const-string v2, "SHA1"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 133
    sget-object v0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedHelper;->digestAlgs:Ljava/util/Map;

    sget-object v1, Lcom/android/sec/org/bouncycastle/asn1/nist/NISTObjectIdentifiers;->id_sha256:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    invoke-virtual {v1}, Lcom/android/sec/org/bouncycastle/asn1/DERObjectIdentifier;->getId()Ljava/lang/String;

    move-result-object v1

    const-string v2, "SHA256"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 134
    sget-object v0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedHelper;->digestAlgs:Ljava/util/Map;

    sget-object v1, Lcom/android/sec/org/bouncycastle/asn1/nist/NISTObjectIdentifiers;->id_sha384:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    invoke-virtual {v1}, Lcom/android/sec/org/bouncycastle/asn1/DERObjectIdentifier;->getId()Ljava/lang/String;

    move-result-object v1

    const-string v2, "SHA384"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 135
    sget-object v0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedHelper;->digestAlgs:Ljava/util/Map;

    sget-object v1, Lcom/android/sec/org/bouncycastle/asn1/nist/NISTObjectIdentifiers;->id_sha512:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    invoke-virtual {v1}, Lcom/android/sec/org/bouncycastle/asn1/DERObjectIdentifier;->getId()Ljava/lang/String;

    move-result-object v1

    const-string v2, "SHA512"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 144
    sget-object v0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedHelper;->digestAliases:Ljava/util/Map;

    const-string v1, "SHA1"

    new-array v2, v5, [Ljava/lang/String;

    const-string v3, "SHA-1"

    aput-object v3, v2, v4

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 148
    sget-object v0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedHelper;->digestAliases:Ljava/util/Map;

    const-string v1, "SHA256"

    new-array v2, v5, [Ljava/lang/String;

    const-string v3, "SHA-256"

    aput-object v3, v2, v4

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 149
    sget-object v0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedHelper;->digestAliases:Ljava/util/Map;

    const-string v1, "SHA384"

    new-array v2, v5, [Ljava/lang/String;

    const-string v3, "SHA-384"

    aput-object v3, v2, v4

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 150
    sget-object v0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedHelper;->digestAliases:Ljava/util/Map;

    const-string v1, "SHA512"

    new-array v2, v5, [Ljava/lang/String;

    const-string v3, "SHA-512"

    aput-object v3, v2, v4

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 151
    return-void
.end method

.method constructor <init>()V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static addEntries(Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p0, "alias"    # Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;
    .param p1, "digest"    # Ljava/lang/String;
    .param p2, "encryption"    # Ljava/lang/String;

    .prologue
    .line 61
    sget-object v0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedHelper;->digestAlgs:Ljava/util/Map;

    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 62
    sget-object v0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedHelper;->encryptionAlgs:Ljava/util/Map;

    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 63
    return-void
.end method


# virtual methods
.method createAttributeStore(Ljava/lang/String;Ljava/security/Provider;Lcom/android/sec/org/bouncycastle/util/Store;)Lcom/android/sec/org/bouncycastle/x509/X509Store;
    .locals 6
    .param p1, "type"    # Ljava/lang/String;
    .param p2, "provider"    # Ljava/security/Provider;
    .param p3, "certStore"    # Lcom/android/sec/org/bouncycastle/util/Store;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/x509/NoSuchStoreException;,
            Lcom/android/sec/org/bouncycastle/cms/CMSException;
        }
    .end annotation

    .prologue
    .line 196
    const/4 v4, 0x0

    :try_start_0
    invoke-interface {p3, v4}, Lcom/android/sec/org/bouncycastle/util/Store;->getMatches(Lcom/android/sec/org/bouncycastle/util/Selector;)Ljava/util/Collection;

    move-result-object v0

    .line 197
    .local v0, "certHldrs":Ljava/util/Collection;
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v4

    invoke-direct {v1, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 199
    .local v1, "certs":Ljava/util/List;
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "it":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 201
    new-instance v5, Lcom/android/sec/org/bouncycastle/x509/X509V2AttributeCertificate;

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/sec/org/bouncycastle/cert/X509AttributeCertificateHolder;

    invoke-virtual {v4}, Lcom/android/sec/org/bouncycastle/cert/X509AttributeCertificateHolder;->getEncoded()[B

    move-result-object v4

    invoke-direct {v5, v4}, Lcom/android/sec/org/bouncycastle/x509/X509V2AttributeCertificate;-><init>([B)V

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 207
    .end local v0    # "certHldrs":Ljava/util/Collection;
    .end local v1    # "certs":Ljava/util/List;
    .end local v3    # "it":Ljava/util/Iterator;
    :catch_0
    move-exception v2

    .line 209
    .local v2, "e":Ljava/lang/IllegalArgumentException;
    new-instance v4, Lcom/android/sec/org/bouncycastle/cms/CMSException;

    const-string v5, "can\'t setup the X509Store"

    invoke-direct {v4, v5, v2}, Lcom/android/sec/org/bouncycastle/cms/CMSException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v4

    .line 204
    .end local v2    # "e":Ljava/lang/IllegalArgumentException;
    .restart local v0    # "certHldrs":Ljava/util/Collection;
    .restart local v1    # "certs":Ljava/util/List;
    .restart local v3    # "it":Ljava/util/Iterator;
    :cond_0
    :try_start_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "AttributeCertificate/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lcom/android/sec/org/bouncycastle/x509/X509CollectionStoreParameters;

    invoke-direct {v5, v1}, Lcom/android/sec/org/bouncycastle/x509/X509CollectionStoreParameters;-><init>(Ljava/util/Collection;)V

    invoke-static {v4, v5, p2}, Lcom/android/sec/org/bouncycastle/x509/X509Store;->getInstance(Ljava/lang/String;Lcom/android/sec/org/bouncycastle/x509/X509StoreParameters;Ljava/security/Provider;)Lcom/android/sec/org/bouncycastle/x509/X509Store;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v4

    return-object v4

    .line 211
    .end local v0    # "certHldrs":Ljava/util/Collection;
    .end local v1    # "certs":Ljava/util/List;
    .end local v3    # "it":Ljava/util/Iterator;
    :catch_1
    move-exception v2

    .line 213
    .local v2, "e":Ljava/io/IOException;
    new-instance v4, Lcom/android/sec/org/bouncycastle/cms/CMSException;

    const-string v5, "can\'t setup the X509Store"

    invoke-direct {v4, v5, v2}, Lcom/android/sec/org/bouncycastle/cms/CMSException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v4
.end method

.method fixAlgID(Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;)Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;
    .locals 3
    .param p1, "algId"    # Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;

    .prologue
    .line 281
    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;->getParameters()Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;

    move-result-object v0

    if-nez v0, :cond_0

    .line 283
    new-instance v0, Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;

    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;->getAlgorithm()Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    move-result-object v1

    sget-object v2, Lcom/android/sec/org/bouncycastle/asn1/DERNull;->INSTANCE:Lcom/android/sec/org/bouncycastle/asn1/DERNull;

    invoke-direct {v0, v1, v2}, Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;-><init>(Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;)V

    move-object p1, v0

    .line 286
    .end local p1    # "algId":Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;
    :cond_0
    return-object p1
.end method

.method getAttributeCertificates(Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;)Lcom/android/sec/org/bouncycastle/util/Store;
    .locals 5
    .param p1, "certSet"    # Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;

    .prologue
    .line 323
    if-eqz p1, :cond_2

    .line 325
    new-instance v0, Ljava/util/ArrayList;

    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;->size()I

    move-result v3

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 327
    .local v0, "certList":Ljava/util/List;
    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;->getObjects()Ljava/util/Enumeration;

    move-result-object v1

    .local v1, "en":Ljava/util/Enumeration;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 329
    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;

    invoke-interface {v3}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;->toASN1Primitive()Lcom/android/sec/org/bouncycastle/asn1/ASN1Primitive;

    move-result-object v2

    .line 331
    .local v2, "obj":Lcom/android/sec/org/bouncycastle/asn1/ASN1Primitive;
    instance-of v3, v2, Lcom/android/sec/org/bouncycastle/asn1/ASN1TaggedObject;

    if-eqz v3, :cond_0

    .line 333
    new-instance v3, Lcom/android/sec/org/bouncycastle/cert/X509AttributeCertificateHolder;

    check-cast v2, Lcom/android/sec/org/bouncycastle/asn1/ASN1TaggedObject;

    .end local v2    # "obj":Lcom/android/sec/org/bouncycastle/asn1/ASN1Primitive;
    invoke-virtual {v2}, Lcom/android/sec/org/bouncycastle/asn1/ASN1TaggedObject;->getObject()Lcom/android/sec/org/bouncycastle/asn1/ASN1Primitive;

    move-result-object v4

    invoke-static {v4}, Lcom/android/sec/org/bouncycastle/asn1/x509/AttributeCertificate;->getInstance(Ljava/lang/Object;)Lcom/android/sec/org/bouncycastle/asn1/x509/AttributeCertificate;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/android/sec/org/bouncycastle/cert/X509AttributeCertificateHolder;-><init>(Lcom/android/sec/org/bouncycastle/asn1/x509/AttributeCertificate;)V

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 337
    :cond_1
    new-instance v3, Lcom/android/sec/org/bouncycastle/util/CollectionStore;

    invoke-direct {v3, v0}, Lcom/android/sec/org/bouncycastle/util/CollectionStore;-><init>(Ljava/util/Collection;)V

    .line 340
    .end local v0    # "certList":Ljava/util/List;
    .end local v1    # "en":Ljava/util/Enumeration;
    :goto_1
    return-object v3

    :cond_2
    new-instance v3, Lcom/android/sec/org/bouncycastle/util/CollectionStore;

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {v3, v4}, Lcom/android/sec/org/bouncycastle/util/CollectionStore;-><init>(Ljava/util/Collection;)V

    goto :goto_1
.end method

.method getCRLs(Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;)Lcom/android/sec/org/bouncycastle/util/Store;
    .locals 5
    .param p1, "crlSet"    # Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;

    .prologue
    .line 345
    if-eqz p1, :cond_2

    .line 347
    new-instance v0, Ljava/util/ArrayList;

    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;->size()I

    move-result v3

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 349
    .local v0, "crlList":Ljava/util/List;
    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;->getObjects()Ljava/util/Enumeration;

    move-result-object v1

    .local v1, "en":Ljava/util/Enumeration;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 351
    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;

    invoke-interface {v3}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;->toASN1Primitive()Lcom/android/sec/org/bouncycastle/asn1/ASN1Primitive;

    move-result-object v2

    .line 353
    .local v2, "obj":Lcom/android/sec/org/bouncycastle/asn1/ASN1Primitive;
    instance-of v3, v2, Lcom/android/sec/org/bouncycastle/asn1/ASN1Sequence;

    if-eqz v3, :cond_0

    .line 355
    new-instance v3, Lcom/android/sec/org/bouncycastle/cert/X509CRLHolder;

    invoke-static {v2}, Lcom/android/sec/org/bouncycastle/asn1/x509/CertificateList;->getInstance(Ljava/lang/Object;)Lcom/android/sec/org/bouncycastle/asn1/x509/CertificateList;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/android/sec/org/bouncycastle/cert/X509CRLHolder;-><init>(Lcom/android/sec/org/bouncycastle/asn1/x509/CertificateList;)V

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 359
    .end local v2    # "obj":Lcom/android/sec/org/bouncycastle/asn1/ASN1Primitive;
    :cond_1
    new-instance v3, Lcom/android/sec/org/bouncycastle/util/CollectionStore;

    invoke-direct {v3, v0}, Lcom/android/sec/org/bouncycastle/util/CollectionStore;-><init>(Ljava/util/Collection;)V

    .line 362
    .end local v0    # "crlList":Ljava/util/List;
    .end local v1    # "en":Ljava/util/Enumeration;
    :goto_1
    return-object v3

    :cond_2
    new-instance v3, Lcom/android/sec/org/bouncycastle/util/CollectionStore;

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {v3, v4}, Lcom/android/sec/org/bouncycastle/util/CollectionStore;-><init>(Ljava/util/Collection;)V

    goto :goto_1
.end method

.method getCertificates(Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;)Lcom/android/sec/org/bouncycastle/util/Store;
    .locals 5
    .param p1, "certSet"    # Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;

    .prologue
    .line 301
    if-eqz p1, :cond_2

    .line 303
    new-instance v0, Ljava/util/ArrayList;

    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;->size()I

    move-result v3

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 305
    .local v0, "certList":Ljava/util/List;
    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;->getObjects()Ljava/util/Enumeration;

    move-result-object v1

    .local v1, "en":Ljava/util/Enumeration;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 307
    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;

    invoke-interface {v3}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;->toASN1Primitive()Lcom/android/sec/org/bouncycastle/asn1/ASN1Primitive;

    move-result-object v2

    .line 309
    .local v2, "obj":Lcom/android/sec/org/bouncycastle/asn1/ASN1Primitive;
    instance-of v3, v2, Lcom/android/sec/org/bouncycastle/asn1/ASN1Sequence;

    if-eqz v3, :cond_0

    .line 311
    new-instance v3, Lcom/android/sec/org/bouncycastle/cert/X509CertificateHolder;

    invoke-static {v2}, Lcom/android/sec/org/bouncycastle/asn1/x509/Certificate;->getInstance(Ljava/lang/Object;)Lcom/android/sec/org/bouncycastle/asn1/x509/Certificate;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/android/sec/org/bouncycastle/cert/X509CertificateHolder;-><init>(Lcom/android/sec/org/bouncycastle/asn1/x509/Certificate;)V

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 315
    .end local v2    # "obj":Lcom/android/sec/org/bouncycastle/asn1/ASN1Primitive;
    :cond_1
    new-instance v3, Lcom/android/sec/org/bouncycastle/util/CollectionStore;

    invoke-direct {v3, v0}, Lcom/android/sec/org/bouncycastle/util/CollectionStore;-><init>(Ljava/util/Collection;)V

    .line 318
    .end local v0    # "certList":Ljava/util/List;
    .end local v1    # "en":Ljava/util/Enumeration;
    :goto_1
    return-object v3

    :cond_2
    new-instance v3, Lcom/android/sec/org/bouncycastle/util/CollectionStore;

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {v3, v4}, Lcom/android/sec/org/bouncycastle/util/CollectionStore;-><init>(Ljava/util/Collection;)V

    goto :goto_1
.end method

.method getDigestAlgName(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "digestAlgOID"    # Ljava/lang/String;

    .prologue
    .line 160
    sget-object v1, Lcom/android/sec/org/bouncycastle/cms/CMSSignedHelper;->digestAlgs:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 162
    .local v0, "algName":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 167
    .end local v0    # "algName":Ljava/lang/String;
    :goto_0
    return-object v0

    .restart local v0    # "algName":Ljava/lang/String;
    :cond_0
    move-object v0, p1

    goto :goto_0
.end method

.method getEncryptionAlgName(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "encryptionAlgOID"    # Ljava/lang/String;

    .prologue
    .line 178
    sget-object v1, Lcom/android/sec/org/bouncycastle/cms/CMSSignedHelper;->encryptionAlgs:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 180
    .local v0, "algName":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 185
    .end local v0    # "algName":Ljava/lang/String;
    :goto_0
    return-object v0

    .restart local v0    # "algName":Ljava/lang/String;
    :cond_0
    move-object v0, p1

    goto :goto_0
.end method

.method setSigningDigestAlgorithmMapping(Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;Ljava/lang/String;)V
    .locals 2
    .param p1, "oid"    # Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;
    .param p2, "algorithmName"    # Ljava/lang/String;

    .prologue
    .line 296
    sget-object v0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedHelper;->digestAlgs:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 297
    return-void
.end method

.method setSigningEncryptionAlgorithmMapping(Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;Ljava/lang/String;)V
    .locals 2
    .param p1, "oid"    # Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;
    .param p2, "algorithmName"    # Ljava/lang/String;

    .prologue
    .line 291
    sget-object v0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedHelper;->encryptionAlgs:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 292
    return-void
.end method
