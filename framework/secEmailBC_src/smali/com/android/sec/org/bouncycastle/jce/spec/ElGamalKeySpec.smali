.class public Lcom/android/sec/org/bouncycastle/jce/spec/ElGamalKeySpec;
.super Ljava/lang/Object;
.source "ElGamalKeySpec.java"

# interfaces
.implements Ljava/security/spec/KeySpec;


# instance fields
.field private spec:Lcom/android/sec/org/bouncycastle/jce/spec/ElGamalParameterSpec;


# direct methods
.method public constructor <init>(Lcom/android/sec/org/bouncycastle/jce/spec/ElGamalParameterSpec;)V
    .locals 0
    .param p1, "spec"    # Lcom/android/sec/org/bouncycastle/jce/spec/ElGamalParameterSpec;

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    iput-object p1, p0, Lcom/android/sec/org/bouncycastle/jce/spec/ElGamalKeySpec;->spec:Lcom/android/sec/org/bouncycastle/jce/spec/ElGamalParameterSpec;

    .line 11
    return-void
.end method


# virtual methods
.method public getParams()Lcom/android/sec/org/bouncycastle/jce/spec/ElGamalParameterSpec;
    .locals 1

    .prologue
    .line 14
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/jce/spec/ElGamalKeySpec;->spec:Lcom/android/sec/org/bouncycastle/jce/spec/ElGamalParameterSpec;

    return-object v0
.end method
