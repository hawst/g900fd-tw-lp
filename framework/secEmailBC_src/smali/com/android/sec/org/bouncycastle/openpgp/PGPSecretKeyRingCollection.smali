.class public Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRingCollection;
.super Ljava/lang/Object;
.source "PGPSecretKeyRingCollection.java"


# instance fields
.field private order:Ljava/util/List;

.field private secretRings:Ljava/util/Map;


# direct methods
.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 7
    .param p1, "in"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
        }
    .end annotation

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    iput-object v4, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRingCollection;->secretRings:Ljava/util/Map;

    .line 25
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRingCollection;->order:Ljava/util/List;

    .line 53
    new-instance v2, Lcom/android/sec/org/bouncycastle/openpgp/PGPObjectFactory;

    invoke-direct {v2, p1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPObjectFactory;-><init>(Ljava/io/InputStream;)V

    .line 56
    .local v2, "pgpFact":Lcom/android/sec/org/bouncycastle/openpgp/PGPObjectFactory;
    :goto_0
    invoke-virtual {v2}, Lcom/android/sec/org/bouncycastle/openpgp/PGPObjectFactory;->nextObject()Ljava/lang/Object;

    move-result-object v1

    .local v1, "obj":Ljava/lang/Object;
    if-eqz v1, :cond_1

    .line 58
    instance-of v4, v1, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;

    if-nez v4, :cond_0

    .line 60
    new-instance v4, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " found where PGPSecretKeyRing expected"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_0
    move-object v3, v1

    .line 63
    check-cast v3, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;

    .line 64
    .local v3, "pgpSecret":Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;
    new-instance v0, Ljava/lang/Long;

    invoke-virtual {v3}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;->getPublicKey()Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->getKeyID()J

    move-result-wide v4

    invoke-direct {v0, v4, v5}, Ljava/lang/Long;-><init>(J)V

    .line 66
    .local v0, "key":Ljava/lang/Long;
    iget-object v4, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRingCollection;->secretRings:Ljava/util/Map;

    invoke-interface {v4, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 67
    iget-object v4, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRingCollection;->order:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 69
    .end local v0    # "key":Ljava/lang/Long;
    .end local v3    # "pgpSecret":Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;
    :cond_1
    return-void
.end method

.method public constructor <init>(Ljava/util/Collection;)V
    .locals 6
    .param p1, "collection"    # Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
        }
    .end annotation

    .prologue
    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    iput-object v3, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRingCollection;->secretRings:Ljava/util/Map;

    .line 25
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRingCollection;->order:Ljava/util/List;

    .line 75
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 77
    .local v0, "it":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 79
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;

    .line 80
    .local v2, "pgpSecret":Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;
    new-instance v1, Ljava/lang/Long;

    invoke-virtual {v2}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;->getPublicKey()Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->getKeyID()J

    move-result-wide v4

    invoke-direct {v1, v4, v5}, Ljava/lang/Long;-><init>(J)V

    .line 82
    .local v1, "key":Ljava/lang/Long;
    iget-object v3, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRingCollection;->secretRings:Ljava/util/Map;

    invoke-interface {v3, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 83
    iget-object v3, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRingCollection;->order:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 85
    .end local v1    # "key":Ljava/lang/Long;
    .end local v2    # "pgpSecret":Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;
    :cond_0
    return-void
.end method

.method private constructor <init>(Ljava/util/Map;Ljava/util/List;)V
    .locals 1
    .param p1, "secretRings"    # Ljava/util/Map;
    .param p2, "order"    # Ljava/util/List;

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRingCollection;->secretRings:Ljava/util/Map;

    .line 25
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRingCollection;->order:Ljava/util/List;

    .line 31
    iput-object p1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRingCollection;->secretRings:Ljava/util/Map;

    .line 32
    iput-object p2, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRingCollection;->order:Ljava/util/List;

    .line 33
    return-void
.end method

.method public constructor <init>([B)V
    .locals 1
    .param p1, "encoding"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
        }
    .end annotation

    .prologue
    .line 39
    new-instance v0, Ljava/io/ByteArrayInputStream;

    invoke-direct {v0, p1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {p0, v0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRingCollection;-><init>(Ljava/io/InputStream;)V

    .line 40
    return-void
.end method

.method public static addSecretKeyRing(Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRingCollection;Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;)Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRingCollection;
    .locals 6
    .param p0, "ringCollection"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRingCollection;
    .param p1, "secretKeyRing"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;

    .prologue
    .line 313
    new-instance v0, Ljava/lang/Long;

    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;->getPublicKey()Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->getKeyID()J

    move-result-wide v4

    invoke-direct {v0, v4, v5}, Ljava/lang/Long;-><init>(J)V

    .line 315
    .local v0, "key":Ljava/lang/Long;
    iget-object v3, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRingCollection;->secretRings:Ljava/util/Map;

    invoke-interface {v3, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 317
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "Collection already contains a key with a keyID for the passed in ring."

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 320
    :cond_0
    new-instance v2, Ljava/util/HashMap;

    iget-object v3, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRingCollection;->secretRings:Ljava/util/Map;

    invoke-direct {v2, v3}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    .line 321
    .local v2, "newSecretRings":Ljava/util/Map;
    new-instance v1, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRingCollection;->order:Ljava/util/List;

    invoke-direct {v1, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 323
    .local v1, "newOrder":Ljava/util/List;
    invoke-interface {v2, v0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 324
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 326
    new-instance v3, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRingCollection;

    invoke-direct {v3, v2, v1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRingCollection;-><init>(Ljava/util/Map;Ljava/util/List;)V

    return-object v3
.end method

.method public static removeSecretKeyRing(Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRingCollection;Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;)Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRingCollection;
    .locals 10
    .param p0, "ringCollection"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRingCollection;
    .param p1, "secretKeyRing"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;

    .prologue
    .line 342
    new-instance v1, Ljava/lang/Long;

    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;->getPublicKey()Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->getKeyID()J

    move-result-wide v6

    invoke-direct {v1, v6, v7}, Ljava/lang/Long;-><init>(J)V

    .line 344
    .local v1, "key":Ljava/lang/Long;
    iget-object v5, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRingCollection;->secretRings:Ljava/util/Map;

    invoke-interface {v5, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 346
    new-instance v5, Ljava/lang/IllegalArgumentException;

    const-string v6, "Collection does not contain a key with a keyID for the passed in ring."

    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 349
    :cond_0
    new-instance v3, Ljava/util/HashMap;

    iget-object v5, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRingCollection;->secretRings:Ljava/util/Map;

    invoke-direct {v3, v5}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    .line 350
    .local v3, "newSecretRings":Ljava/util/Map;
    new-instance v2, Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRingCollection;->order:Ljava/util/List;

    invoke-direct {v2, v5}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 352
    .local v2, "newOrder":Ljava/util/List;
    invoke-interface {v3, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 354
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v5

    if-ge v0, v5, :cond_1

    .line 356
    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    .line 358
    .local v4, "r":Ljava/lang/Long;
    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    cmp-long v5, v6, v8

    if-nez v5, :cond_2

    .line 360
    invoke-interface {v2, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 365
    .end local v4    # "r":Ljava/lang/Long;
    :cond_1
    new-instance v5, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRingCollection;

    invoke-direct {v5, v3, v2}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRingCollection;-><init>(Ljava/util/Map;Ljava/util/List;)V

    return-object v5

    .line 354
    .restart local v4    # "r":Ljava/lang/Long;
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public contains(J)Z
    .locals 1
    .param p1, "keyID"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
        }
    .end annotation

    .prologue
    .line 263
    invoke-virtual {p0, p1, p2}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRingCollection;->getSecretKey(J)Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public encode(Ljava/io/OutputStream;)V
    .locals 5
    .param p1, "outStream"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 282
    instance-of v3, p1, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;

    if-eqz v3, :cond_0

    move-object v1, p1

    .line 284
    check-cast v1, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;

    .line 291
    .local v1, "out":Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;
    :goto_0
    iget-object v3, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRingCollection;->order:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 292
    .local v0, "it":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 294
    iget-object v3, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRingCollection;->secretRings:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;

    .line 296
    .local v2, "sr":Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;
    invoke-virtual {v2, v1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;->encode(Ljava/io/OutputStream;)V

    goto :goto_1

    .line 288
    .end local v0    # "it":Ljava/util/Iterator;
    .end local v1    # "out":Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;
    .end local v2    # "sr":Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;
    :cond_0
    new-instance v1, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;

    invoke-direct {v1, p1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;-><init>(Ljava/io/OutputStream;)V

    .restart local v1    # "out":Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;
    goto :goto_0

    .line 298
    .restart local v0    # "it":Ljava/util/Iterator;
    :cond_1
    return-void
.end method

.method public getEncoded()[B
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 269
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 271
    .local v0, "bOut":Ljava/io/ByteArrayOutputStream;
    invoke-virtual {p0, v0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRingCollection;->encode(Ljava/io/OutputStream;)V

    .line 273
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    return-object v1
.end method

.method public getKeyRings()Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRingCollection;->secretRings:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public getKeyRings(Ljava/lang/String;)Ljava/util/Iterator;
    .locals 1
    .param p1, "userID"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 116
    invoke-virtual {p0, p1, v0, v0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRingCollection;->getKeyRings(Ljava/lang/String;ZZ)Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public getKeyRings(Ljava/lang/String;Z)Ljava/util/Iterator;
    .locals 1
    .param p1, "userID"    # Ljava/lang/String;
    .param p2, "matchPartial"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
        }
    .end annotation

    .prologue
    .line 133
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRingCollection;->getKeyRings(Ljava/lang/String;ZZ)Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public getKeyRings(Ljava/lang/String;ZZ)Ljava/util/Iterator;
    .locals 7
    .param p1, "userID"    # Ljava/lang/String;
    .param p2, "matchPartial"    # Z
    .param p3, "ignoreCase"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
        }
    .end annotation

    .prologue
    .line 152
    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRingCollection;->getKeyRings()Ljava/util/Iterator;

    move-result-object v0

    .line 153
    .local v0, "it":Ljava/util/Iterator;
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 155
    .local v2, "rings":Ljava/util/List;
    if-eqz p3, :cond_0

    .line 157
    invoke-static {p1}, Lcom/android/sec/org/bouncycastle/util/Strings;->toLowerCase(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 160
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 162
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;

    .line 163
    .local v3, "secRing":Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;
    invoke-virtual {v3}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;->getSecretKey()Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;->getUserIDs()Ljava/util/Iterator;

    move-result-object v4

    .line 165
    .local v4, "uIt":Ljava/util/Iterator;
    :cond_1
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 167
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 168
    .local v1, "next":Ljava/lang/String;
    if-eqz p3, :cond_2

    .line 170
    invoke-static {v1}, Lcom/android/sec/org/bouncycastle/util/Strings;->toLowerCase(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 173
    :cond_2
    if-eqz p2, :cond_3

    .line 175
    invoke-virtual {v1, p1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    const/4 v6, -0x1

    if-le v5, v6, :cond_1

    .line 177
    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 182
    :cond_3
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 184
    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 190
    .end local v1    # "next":Ljava/lang/String;
    .end local v3    # "secRing":Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;
    .end local v4    # "uIt":Ljava/util/Iterator;
    :cond_4
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    return-object v5
.end method

.method public getSecretKey(J)Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;
    .locals 5
    .param p1, "keyID"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
        }
    .end annotation

    .prologue
    .line 204
    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRingCollection;->getKeyRings()Ljava/util/Iterator;

    move-result-object v0

    .line 206
    .local v0, "it":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 208
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;

    .line 209
    .local v2, "secRing":Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;
    invoke-virtual {v2, p1, p2}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;->getSecretKey(J)Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;

    move-result-object v1

    .line 211
    .local v1, "sec":Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;
    if-eqz v1, :cond_0

    .line 217
    .end local v1    # "sec":Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;
    .end local v2    # "secRing":Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;
    :goto_0
    return-object v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getSecretKeyRing(J)Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;
    .locals 5
    .param p1, "keyID"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
        }
    .end annotation

    .prologue
    .line 231
    new-instance v0, Ljava/lang/Long;

    invoke-direct {v0, p1, p2}, Ljava/lang/Long;-><init>(J)V

    .line 233
    .local v0, "id":Ljava/lang/Long;
    iget-object v4, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRingCollection;->secretRings:Ljava/util/Map;

    invoke-interface {v4, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 235
    iget-object v4, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRingCollection;->secretRings:Ljava/util/Map;

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;

    .line 251
    :goto_0
    return-object v4

    .line 238
    :cond_0
    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRingCollection;->getKeyRings()Ljava/util/Iterator;

    move-result-object v1

    .line 240
    .local v1, "it":Ljava/util/Iterator;
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 242
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;

    .line 243
    .local v3, "secretRing":Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;
    invoke-virtual {v3, p1, p2}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;->getSecretKey(J)Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;

    move-result-object v2

    .line 245
    .local v2, "secret":Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;
    if-eqz v2, :cond_1

    move-object v4, v3

    .line 247
    goto :goto_0

    .line 251
    .end local v2    # "secret":Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;
    .end local v3    # "secretRing":Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;
    :cond_2
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRingCollection;->order:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method
