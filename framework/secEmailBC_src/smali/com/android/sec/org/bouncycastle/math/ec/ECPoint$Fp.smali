.class public Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$Fp;
.super Lcom/android/sec/org/bouncycastle/math/ec/ECPoint;
.source "ECPoint.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/sec/org/bouncycastle/math/ec/ECPoint;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Fp"
.end annotation


# direct methods
.method public constructor <init>(Lcom/android/sec/org/bouncycastle/math/ec/ECCurve;Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;)V
    .locals 1
    .param p1, "curve"    # Lcom/android/sec/org/bouncycastle/math/ec/ECCurve;
    .param p2, "x"    # Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;
    .param p3, "y"    # Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    .prologue
    .line 175
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$Fp;-><init>(Lcom/android/sec/org/bouncycastle/math/ec/ECCurve;Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;Z)V

    .line 176
    return-void
.end method

.method public constructor <init>(Lcom/android/sec/org/bouncycastle/math/ec/ECCurve;Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;Z)V
    .locals 2
    .param p1, "curve"    # Lcom/android/sec/org/bouncycastle/math/ec/ECCurve;
    .param p2, "x"    # Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;
    .param p3, "y"    # Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;
    .param p4, "withCompression"    # Z

    .prologue
    .line 188
    invoke-direct {p0, p1, p2, p3}, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint;-><init>(Lcom/android/sec/org/bouncycastle/math/ec/ECCurve;Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;)V

    .line 190
    if-eqz p2, :cond_0

    if-eqz p3, :cond_1

    :cond_0
    if-nez p2, :cond_2

    if-eqz p3, :cond_2

    .line 192
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Exactly one of the field elements is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 195
    :cond_2
    iput-boolean p4, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$Fp;->withCompression:Z

    .line 196
    return-void
.end method


# virtual methods
.method public add(Lcom/android/sec/org/bouncycastle/math/ec/ECPoint;)Lcom/android/sec/org/bouncycastle/math/ec/ECPoint;
    .locals 6
    .param p1, "b"    # Lcom/android/sec/org/bouncycastle/math/ec/ECPoint;

    .prologue
    .line 248
    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$Fp;->isInfinity()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 276
    .end local p1    # "b":Lcom/android/sec/org/bouncycastle/math/ec/ECPoint;
    :goto_0
    return-object p1

    .line 253
    .restart local p1    # "b":Lcom/android/sec/org/bouncycastle/math/ec/ECPoint;
    :cond_0
    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint;->isInfinity()Z

    move-result v3

    if-eqz v3, :cond_1

    move-object p1, p0

    .line 255
    goto :goto_0

    .line 259
    :cond_1
    iget-object v3, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$Fp;->x:Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    iget-object v4, p1, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint;->x:Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 261
    iget-object v3, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$Fp;->y:Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    iget-object v4, p1, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint;->y:Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 264
    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$Fp;->twice()Lcom/android/sec/org/bouncycastle/math/ec/ECPoint;

    move-result-object p1

    goto :goto_0

    .line 268
    :cond_2
    iget-object v3, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$Fp;->curve:Lcom/android/sec/org/bouncycastle/math/ec/ECCurve;

    invoke-virtual {v3}, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve;->getInfinity()Lcom/android/sec/org/bouncycastle/math/ec/ECPoint;

    move-result-object p1

    goto :goto_0

    .line 271
    :cond_3
    iget-object v3, p1, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint;->y:Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    iget-object v4, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$Fp;->y:Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    invoke-virtual {v3, v4}, Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;->subtract(Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;)Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    move-result-object v3

    iget-object v4, p1, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint;->x:Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    iget-object v5, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$Fp;->x:Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    invoke-virtual {v4, v5}, Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;->subtract(Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;)Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;->divide(Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;)Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    move-result-object v0

    .line 273
    .local v0, "gamma":Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;
    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;->square()Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    move-result-object v3

    iget-object v4, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$Fp;->x:Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    invoke-virtual {v3, v4}, Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;->subtract(Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;)Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    move-result-object v3

    iget-object v4, p1, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint;->x:Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    invoke-virtual {v3, v4}, Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;->subtract(Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;)Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    move-result-object v1

    .line 274
    .local v1, "x3":Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;
    iget-object v3, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$Fp;->x:Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    invoke-virtual {v3, v1}, Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;->subtract(Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;)Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;->multiply(Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;)Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    move-result-object v3

    iget-object v4, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$Fp;->y:Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    invoke-virtual {v3, v4}, Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;->subtract(Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;)Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    move-result-object v2

    .line 276
    .local v2, "y3":Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;
    new-instance p1, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$Fp;

    .end local p1    # "b":Lcom/android/sec/org/bouncycastle/math/ec/ECPoint;
    iget-object v3, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$Fp;->curve:Lcom/android/sec/org/bouncycastle/math/ec/ECCurve;

    iget-boolean v4, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$Fp;->withCompression:Z

    invoke-direct {p1, v3, v1, v2, v4}, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$Fp;-><init>(Lcom/android/sec/org/bouncycastle/math/ec/ECCurve;Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;Z)V

    goto :goto_0
.end method

.method declared-synchronized assertECMultiplier()V
    .locals 1

    .prologue
    .line 327
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$Fp;->multiplier:Lcom/android/sec/org/bouncycastle/math/ec/ECMultiplier;

    if-nez v0, :cond_0

    .line 329
    new-instance v0, Lcom/android/sec/org/bouncycastle/math/ec/WNafMultiplier;

    invoke-direct {v0}, Lcom/android/sec/org/bouncycastle/math/ec/WNafMultiplier;-><init>()V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$Fp;->multiplier:Lcom/android/sec/org/bouncycastle/math/ec/ECMultiplier;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 331
    :cond_0
    monitor-exit p0

    return-void

    .line 327
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getEncoded(Z)[B
    .locals 9
    .param p1, "compressed"    # Z

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 203
    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$Fp;->isInfinity()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 205
    new-array v1, v8, [B

    .line 241
    :goto_0
    return-object v1

    .line 208
    :cond_0
    # getter for: Lcom/android/sec/org/bouncycastle/math/ec/ECPoint;->converter:Lcom/android/sec/org/bouncycastle/asn1/x9/X9IntegerConverter;
    invoke-static {}, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint;->access$000()Lcom/android/sec/org/bouncycastle/asn1/x9/X9IntegerConverter;

    move-result-object v5

    iget-object v6, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$Fp;->x:Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    invoke-virtual {v5, v6}, Lcom/android/sec/org/bouncycastle/asn1/x9/X9IntegerConverter;->getByteLength(Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;)I

    move-result v4

    .line 210
    .local v4, "qLength":I
    if-eqz p1, :cond_2

    .line 214
    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$Fp;->getY()Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;->toBigInteger()Ljava/math/BigInteger;

    move-result-object v5

    invoke-virtual {v5, v7}, Ljava/math/BigInteger;->testBit(I)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 216
    const/4 v0, 0x3

    .line 223
    .local v0, "PC":B
    :goto_1
    # getter for: Lcom/android/sec/org/bouncycastle/math/ec/ECPoint;->converter:Lcom/android/sec/org/bouncycastle/asn1/x9/X9IntegerConverter;
    invoke-static {}, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint;->access$000()Lcom/android/sec/org/bouncycastle/asn1/x9/X9IntegerConverter;

    move-result-object v5

    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$Fp;->getX()Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    move-result-object v6

    invoke-virtual {v6}, Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;->toBigInteger()Ljava/math/BigInteger;

    move-result-object v6

    invoke-virtual {v5, v6, v4}, Lcom/android/sec/org/bouncycastle/asn1/x9/X9IntegerConverter;->integerToBytes(Ljava/math/BigInteger;I)[B

    move-result-object v2

    .line 224
    .local v2, "X":[B
    array-length v5, v2

    add-int/lit8 v5, v5, 0x1

    new-array v1, v5, [B

    .line 226
    .local v1, "PO":[B
    aput-byte v0, v1, v7

    .line 227
    array-length v5, v2

    invoke-static {v2, v7, v1, v8, v5}, Ljava/lang/System;->arraycopy([BI[BII)V

    goto :goto_0

    .line 220
    .end local v0    # "PC":B
    .end local v1    # "PO":[B
    .end local v2    # "X":[B
    :cond_1
    const/4 v0, 0x2

    .restart local v0    # "PC":B
    goto :goto_1

    .line 233
    .end local v0    # "PC":B
    :cond_2
    # getter for: Lcom/android/sec/org/bouncycastle/math/ec/ECPoint;->converter:Lcom/android/sec/org/bouncycastle/asn1/x9/X9IntegerConverter;
    invoke-static {}, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint;->access$000()Lcom/android/sec/org/bouncycastle/asn1/x9/X9IntegerConverter;

    move-result-object v5

    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$Fp;->getX()Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    move-result-object v6

    invoke-virtual {v6}, Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;->toBigInteger()Ljava/math/BigInteger;

    move-result-object v6

    invoke-virtual {v5, v6, v4}, Lcom/android/sec/org/bouncycastle/asn1/x9/X9IntegerConverter;->integerToBytes(Ljava/math/BigInteger;I)[B

    move-result-object v2

    .line 234
    .restart local v2    # "X":[B
    # getter for: Lcom/android/sec/org/bouncycastle/math/ec/ECPoint;->converter:Lcom/android/sec/org/bouncycastle/asn1/x9/X9IntegerConverter;
    invoke-static {}, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint;->access$000()Lcom/android/sec/org/bouncycastle/asn1/x9/X9IntegerConverter;

    move-result-object v5

    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$Fp;->getY()Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    move-result-object v6

    invoke-virtual {v6}, Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;->toBigInteger()Ljava/math/BigInteger;

    move-result-object v6

    invoke-virtual {v5, v6, v4}, Lcom/android/sec/org/bouncycastle/asn1/x9/X9IntegerConverter;->integerToBytes(Ljava/math/BigInteger;I)[B

    move-result-object v3

    .line 235
    .local v3, "Y":[B
    array-length v5, v2

    array-length v6, v3

    add-int/2addr v5, v6

    add-int/lit8 v5, v5, 0x1

    new-array v1, v5, [B

    .line 237
    .restart local v1    # "PO":[B
    const/4 v5, 0x4

    aput-byte v5, v1, v7

    .line 238
    array-length v5, v2

    invoke-static {v2, v7, v1, v8, v5}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 239
    array-length v5, v2

    add-int/lit8 v5, v5, 0x1

    array-length v6, v3

    invoke-static {v3, v7, v1, v5, v6}, Ljava/lang/System;->arraycopy([BI[BII)V

    goto :goto_0
.end method

.method public negate()Lcom/android/sec/org/bouncycastle/math/ec/ECPoint;
    .locals 5

    .prologue
    .line 319
    new-instance v0, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$Fp;

    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$Fp;->curve:Lcom/android/sec/org/bouncycastle/math/ec/ECCurve;

    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$Fp;->x:Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    iget-object v3, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$Fp;->y:Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    invoke-virtual {v3}, Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;->negate()Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    move-result-object v3

    iget-boolean v4, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$Fp;->withCompression:Z

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$Fp;-><init>(Lcom/android/sec/org/bouncycastle/math/ec/ECCurve;Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;Z)V

    return-object v0
.end method

.method public subtract(Lcom/android/sec/org/bouncycastle/math/ec/ECPoint;)Lcom/android/sec/org/bouncycastle/math/ec/ECPoint;
    .locals 1
    .param p1, "b"    # Lcom/android/sec/org/bouncycastle/math/ec/ECPoint;

    .prologue
    .line 308
    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint;->isInfinity()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 314
    .end local p0    # "this":Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$Fp;
    :goto_0
    return-object p0

    .restart local p0    # "this":Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$Fp;
    :cond_0
    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint;->negate()Lcom/android/sec/org/bouncycastle/math/ec/ECPoint;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$Fp;->add(Lcom/android/sec/org/bouncycastle/math/ec/ECPoint;)Lcom/android/sec/org/bouncycastle/math/ec/ECPoint;

    move-result-object p0

    goto :goto_0
.end method

.method public twice()Lcom/android/sec/org/bouncycastle/math/ec/ECPoint;
    .locals 8

    .prologue
    .line 282
    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$Fp;->isInfinity()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 302
    .end local p0    # "this":Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$Fp;
    .local v0, "THREE":Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;
    .local v1, "TWO":Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;
    .local v2, "gamma":Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;
    .local v3, "x3":Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;
    .local v4, "y3":Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;
    :goto_0
    return-object p0

    .line 288
    .end local v0    # "THREE":Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;
    .end local v1    # "TWO":Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;
    .end local v2    # "gamma":Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;
    .end local v3    # "x3":Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;
    .end local v4    # "y3":Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;
    .restart local p0    # "this":Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$Fp;
    :cond_0
    iget-object v5, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$Fp;->y:Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    invoke-virtual {v5}, Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;->toBigInteger()Ljava/math/BigInteger;

    move-result-object v5

    invoke-virtual {v5}, Ljava/math/BigInteger;->signum()I

    move-result v5

    if-nez v5, :cond_1

    .line 292
    iget-object v5, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$Fp;->curve:Lcom/android/sec/org/bouncycastle/math/ec/ECCurve;

    invoke-virtual {v5}, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve;->getInfinity()Lcom/android/sec/org/bouncycastle/math/ec/ECPoint;

    move-result-object p0

    goto :goto_0

    .line 295
    :cond_1
    iget-object v5, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$Fp;->curve:Lcom/android/sec/org/bouncycastle/math/ec/ECCurve;

    const-wide/16 v6, 0x2

    invoke-static {v6, v7}, Ljava/math/BigInteger;->valueOf(J)Ljava/math/BigInteger;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve;->fromBigInteger(Ljava/math/BigInteger;)Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    move-result-object v1

    .line 296
    .restart local v1    # "TWO":Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;
    iget-object v5, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$Fp;->curve:Lcom/android/sec/org/bouncycastle/math/ec/ECCurve;

    const-wide/16 v6, 0x3

    invoke-static {v6, v7}, Ljava/math/BigInteger;->valueOf(J)Ljava/math/BigInteger;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve;->fromBigInteger(Ljava/math/BigInteger;)Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    move-result-object v0

    .line 297
    .restart local v0    # "THREE":Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;
    iget-object v5, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$Fp;->x:Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    invoke-virtual {v5}, Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;->square()Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    move-result-object v5

    invoke-virtual {v5, v0}, Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;->multiply(Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;)Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    move-result-object v5

    iget-object v6, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$Fp;->curve:Lcom/android/sec/org/bouncycastle/math/ec/ECCurve;

    iget-object v6, v6, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve;->a:Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    invoke-virtual {v5, v6}, Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;->add(Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;)Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    move-result-object v5

    iget-object v6, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$Fp;->y:Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    invoke-virtual {v6, v1}, Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;->multiply(Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;)Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;->divide(Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;)Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    move-result-object v2

    .line 299
    .restart local v2    # "gamma":Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;
    invoke-virtual {v2}, Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;->square()Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    move-result-object v5

    iget-object v6, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$Fp;->x:Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    invoke-virtual {v6, v1}, Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;->multiply(Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;)Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;->subtract(Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;)Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    move-result-object v3

    .line 300
    .restart local v3    # "x3":Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;
    iget-object v5, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$Fp;->x:Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    invoke-virtual {v5, v3}, Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;->subtract(Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;)Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    move-result-object v5

    invoke-virtual {v2, v5}, Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;->multiply(Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;)Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    move-result-object v5

    iget-object v6, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$Fp;->y:Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    invoke-virtual {v5, v6}, Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;->subtract(Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;)Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    move-result-object v4

    .line 302
    .restart local v4    # "y3":Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;
    new-instance v5, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$Fp;

    iget-object v6, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$Fp;->curve:Lcom/android/sec/org/bouncycastle/math/ec/ECCurve;

    iget-boolean v7, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$Fp;->withCompression:Z

    invoke-direct {v5, v6, v3, v4, v7}, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$Fp;-><init>(Lcom/android/sec/org/bouncycastle/math/ec/ECCurve;Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;Z)V

    move-object p0, v5

    goto :goto_0
.end method
