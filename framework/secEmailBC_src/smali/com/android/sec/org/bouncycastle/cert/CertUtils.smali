.class Lcom/android/sec/org/bouncycastle/cert/CertUtils;
.super Ljava/lang/Object;
.source "CertUtils.java"


# static fields
.field private static EMPTY_LIST:Ljava/util/List;

.field private static EMPTY_SET:Ljava/util/Set;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lcom/android/sec/org/bouncycastle/cert/CertUtils;->EMPTY_SET:Ljava/util/Set;

    .line 36
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/android/sec/org/bouncycastle/cert/CertUtils;->EMPTY_LIST:Ljava/util/List;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static addExtension(Lcom/android/sec/org/bouncycastle/asn1/x509/ExtensionsGenerator;Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;ZLcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;)V
    .locals 4
    .param p0, "extGenerator"    # Lcom/android/sec/org/bouncycastle/asn1/x509/ExtensionsGenerator;
    .param p1, "oid"    # Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;
    .param p2, "isCritical"    # Z
    .param p3, "value"    # Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/cert/CertIOException;
        }
    .end annotation

    .prologue
    .line 156
    :try_start_0
    invoke-virtual {p0, p1, p2, p3}, Lcom/android/sec/org/bouncycastle/asn1/x509/ExtensionsGenerator;->addExtension(Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;ZLcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 162
    return-void

    .line 158
    :catch_0
    move-exception v0

    .line 160
    .local v0, "e":Ljava/io/IOException;
    new-instance v1, Lcom/android/sec/org/bouncycastle/cert/CertIOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "cannot encode extension: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/android/sec/org/bouncycastle/cert/CertIOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method static bitStringToBoolean(Lcom/android/sec/org/bouncycastle/asn1/DERBitString;)[Z
    .locals 6
    .param p0, "bitString"    # Lcom/android/sec/org/bouncycastle/asn1/DERBitString;

    .prologue
    .line 187
    if-eqz p0, :cond_1

    .line 189
    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/asn1/DERBitString;->getBytes()[B

    move-result-object v1

    .line 190
    .local v1, "bytes":[B
    array-length v3, v1

    mul-int/lit8 v3, v3, 0x8

    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/asn1/DERBitString;->getPadBits()I

    move-result v4

    sub-int/2addr v3, v4

    new-array v0, v3, [Z

    .line 192
    .local v0, "boolId":[Z
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v3, v0

    if-eq v2, v3, :cond_2

    .line 194
    div-int/lit8 v3, v2, 0x8

    aget-byte v3, v1, v3

    const/16 v4, 0x80

    rem-int/lit8 v5, v2, 0x8

    ushr-int/2addr v4, v5

    and-int/2addr v3, v4

    if-eqz v3, :cond_0

    const/4 v3, 0x1

    :goto_1
    aput-boolean v3, v0, v2

    .line 192
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 194
    :cond_0
    const/4 v3, 0x0

    goto :goto_1

    .line 200
    .end local v0    # "boolId":[Z
    .end local v1    # "bytes":[B
    .end local v2    # "i":I
    :cond_1
    const/4 v0, 0x0

    :cond_2
    return-object v0
.end method

.method static booleanToBitString([Z)Lcom/android/sec/org/bouncycastle/asn1/DERBitString;
    .locals 7
    .param p0, "id"    # [Z

    .prologue
    .line 166
    array-length v3, p0

    add-int/lit8 v3, v3, 0x7

    div-int/lit8 v3, v3, 0x8

    new-array v0, v3, [B

    .line 168
    .local v0, "bytes":[B
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v3, p0

    if-eq v1, v3, :cond_1

    .line 170
    div-int/lit8 v4, v1, 0x8

    aget-byte v5, v0, v4

    aget-boolean v3, p0, v1

    if-eqz v3, :cond_0

    const/4 v3, 0x1

    rem-int/lit8 v6, v1, 0x8

    rsub-int/lit8 v6, v6, 0x7

    shl-int/2addr v3, v6

    :goto_1
    or-int/2addr v3, v5

    int-to-byte v3, v3

    aput-byte v3, v0, v4

    .line 168
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 170
    :cond_0
    const/4 v3, 0x0

    goto :goto_1

    .line 173
    :cond_1
    array-length v3, p0

    rem-int/lit8 v2, v3, 0x8

    .line 175
    .local v2, "pad":I
    if-nez v2, :cond_2

    .line 177
    new-instance v3, Lcom/android/sec/org/bouncycastle/asn1/DERBitString;

    invoke-direct {v3, v0}, Lcom/android/sec/org/bouncycastle/asn1/DERBitString;-><init>([B)V

    .line 181
    :goto_2
    return-object v3

    :cond_2
    new-instance v3, Lcom/android/sec/org/bouncycastle/asn1/DERBitString;

    rsub-int/lit8 v4, v2, 0x8

    invoke-direct {v3, v0, v4}, Lcom/android/sec/org/bouncycastle/asn1/DERBitString;-><init>([BI)V

    goto :goto_2
.end method

.method private static generateAttrStructure(Lcom/android/sec/org/bouncycastle/asn1/x509/AttributeCertificateInfo;Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;[B)Lcom/android/sec/org/bouncycastle/asn1/x509/AttributeCertificate;
    .locals 2
    .param p0, "attrInfo"    # Lcom/android/sec/org/bouncycastle/asn1/x509/AttributeCertificateInfo;
    .param p1, "sigAlgId"    # Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;
    .param p2, "signature"    # [B

    .prologue
    .line 100
    new-instance v0, Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;

    invoke-direct {v0}, Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;-><init>()V

    .line 102
    .local v0, "v":Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;
    invoke-virtual {v0, p0}, Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;->add(Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;)V

    .line 103
    invoke-virtual {v0, p1}, Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;->add(Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;)V

    .line 104
    new-instance v1, Lcom/android/sec/org/bouncycastle/asn1/DERBitString;

    invoke-direct {v1, p2}, Lcom/android/sec/org/bouncycastle/asn1/DERBitString;-><init>([B)V

    invoke-virtual {v0, v1}, Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;->add(Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;)V

    .line 106
    new-instance v1, Lcom/android/sec/org/bouncycastle/asn1/DERSequence;

    invoke-direct {v1, v0}, Lcom/android/sec/org/bouncycastle/asn1/DERSequence;-><init>(Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;)V

    invoke-static {v1}, Lcom/android/sec/org/bouncycastle/asn1/x509/AttributeCertificate;->getInstance(Ljava/lang/Object;)Lcom/android/sec/org/bouncycastle/asn1/x509/AttributeCertificate;

    move-result-object v1

    return-object v1
.end method

.method private static generateCRLStructure(Lcom/android/sec/org/bouncycastle/asn1/x509/TBSCertList;Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;[B)Lcom/android/sec/org/bouncycastle/asn1/x509/CertificateList;
    .locals 2
    .param p0, "tbsCertList"    # Lcom/android/sec/org/bouncycastle/asn1/x509/TBSCertList;
    .param p1, "sigAlgId"    # Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;
    .param p2, "signature"    # [B

    .prologue
    .line 111
    new-instance v0, Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;

    invoke-direct {v0}, Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;-><init>()V

    .line 113
    .local v0, "v":Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;
    invoke-virtual {v0, p0}, Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;->add(Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;)V

    .line 114
    invoke-virtual {v0, p1}, Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;->add(Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;)V

    .line 115
    new-instance v1, Lcom/android/sec/org/bouncycastle/asn1/DERBitString;

    invoke-direct {v1, p2}, Lcom/android/sec/org/bouncycastle/asn1/DERBitString;-><init>([B)V

    invoke-virtual {v0, v1}, Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;->add(Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;)V

    .line 117
    new-instance v1, Lcom/android/sec/org/bouncycastle/asn1/DERSequence;

    invoke-direct {v1, v0}, Lcom/android/sec/org/bouncycastle/asn1/DERSequence;-><init>(Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;)V

    invoke-static {v1}, Lcom/android/sec/org/bouncycastle/asn1/x509/CertificateList;->getInstance(Ljava/lang/Object;)Lcom/android/sec/org/bouncycastle/asn1/x509/CertificateList;

    move-result-object v1

    return-object v1
.end method

.method static generateFullAttrCert(Lcom/android/sec/org/bouncycastle/operator/ContentSigner;Lcom/android/sec/org/bouncycastle/asn1/x509/AttributeCertificateInfo;)Lcom/android/sec/org/bouncycastle/cert/X509AttributeCertificateHolder;
    .locals 4
    .param p0, "signer"    # Lcom/android/sec/org/bouncycastle/operator/ContentSigner;
    .param p1, "attrInfo"    # Lcom/android/sec/org/bouncycastle/asn1/x509/AttributeCertificateInfo;

    .prologue
    .line 54
    :try_start_0
    new-instance v1, Lcom/android/sec/org/bouncycastle/cert/X509AttributeCertificateHolder;

    invoke-interface {p0}, Lcom/android/sec/org/bouncycastle/operator/ContentSigner;->getAlgorithmIdentifier()Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;

    move-result-object v2

    invoke-static {p0, p1}, Lcom/android/sec/org/bouncycastle/cert/CertUtils;->generateSig(Lcom/android/sec/org/bouncycastle/operator/ContentSigner;Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;)[B

    move-result-object v3

    invoke-static {p1, v2, v3}, Lcom/android/sec/org/bouncycastle/cert/CertUtils;->generateAttrStructure(Lcom/android/sec/org/bouncycastle/asn1/x509/AttributeCertificateInfo;Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;[B)Lcom/android/sec/org/bouncycastle/asn1/x509/AttributeCertificate;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/android/sec/org/bouncycastle/cert/X509AttributeCertificateHolder;-><init>(Lcom/android/sec/org/bouncycastle/asn1/x509/AttributeCertificate;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    .line 56
    :catch_0
    move-exception v0

    .line 58
    .local v0, "e":Ljava/io/IOException;
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "cannot produce attribute certificate signature"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method static generateFullCRL(Lcom/android/sec/org/bouncycastle/operator/ContentSigner;Lcom/android/sec/org/bouncycastle/asn1/x509/TBSCertList;)Lcom/android/sec/org/bouncycastle/cert/X509CRLHolder;
    .locals 4
    .param p0, "signer"    # Lcom/android/sec/org/bouncycastle/operator/ContentSigner;
    .param p1, "tbsCertList"    # Lcom/android/sec/org/bouncycastle/asn1/x509/TBSCertList;

    .prologue
    .line 66
    :try_start_0
    new-instance v1, Lcom/android/sec/org/bouncycastle/cert/X509CRLHolder;

    invoke-interface {p0}, Lcom/android/sec/org/bouncycastle/operator/ContentSigner;->getAlgorithmIdentifier()Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;

    move-result-object v2

    invoke-static {p0, p1}, Lcom/android/sec/org/bouncycastle/cert/CertUtils;->generateSig(Lcom/android/sec/org/bouncycastle/operator/ContentSigner;Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;)[B

    move-result-object v3

    invoke-static {p1, v2, v3}, Lcom/android/sec/org/bouncycastle/cert/CertUtils;->generateCRLStructure(Lcom/android/sec/org/bouncycastle/asn1/x509/TBSCertList;Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;[B)Lcom/android/sec/org/bouncycastle/asn1/x509/CertificateList;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/android/sec/org/bouncycastle/cert/X509CRLHolder;-><init>(Lcom/android/sec/org/bouncycastle/asn1/x509/CertificateList;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    .line 68
    :catch_0
    move-exception v0

    .line 70
    .local v0, "e":Ljava/io/IOException;
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "cannot produce certificate signature"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method static generateFullCert(Lcom/android/sec/org/bouncycastle/operator/ContentSigner;Lcom/android/sec/org/bouncycastle/asn1/x509/TBSCertificate;)Lcom/android/sec/org/bouncycastle/cert/X509CertificateHolder;
    .locals 4
    .param p0, "signer"    # Lcom/android/sec/org/bouncycastle/operator/ContentSigner;
    .param p1, "tbsCert"    # Lcom/android/sec/org/bouncycastle/asn1/x509/TBSCertificate;

    .prologue
    .line 42
    :try_start_0
    new-instance v1, Lcom/android/sec/org/bouncycastle/cert/X509CertificateHolder;

    invoke-interface {p0}, Lcom/android/sec/org/bouncycastle/operator/ContentSigner;->getAlgorithmIdentifier()Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;

    move-result-object v2

    invoke-static {p0, p1}, Lcom/android/sec/org/bouncycastle/cert/CertUtils;->generateSig(Lcom/android/sec/org/bouncycastle/operator/ContentSigner;Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;)[B

    move-result-object v3

    invoke-static {p1, v2, v3}, Lcom/android/sec/org/bouncycastle/cert/CertUtils;->generateStructure(Lcom/android/sec/org/bouncycastle/asn1/x509/TBSCertificate;Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;[B)Lcom/android/sec/org/bouncycastle/asn1/x509/Certificate;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/android/sec/org/bouncycastle/cert/X509CertificateHolder;-><init>(Lcom/android/sec/org/bouncycastle/asn1/x509/Certificate;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    .line 44
    :catch_0
    move-exception v0

    .line 46
    .local v0, "e":Ljava/io/IOException;
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "cannot produce certificate signature"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private static generateSig(Lcom/android/sec/org/bouncycastle/operator/ContentSigner;Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;)[B
    .locals 3
    .param p0, "signer"    # Lcom/android/sec/org/bouncycastle/operator/ContentSigner;
    .param p1, "tbsObj"    # Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 77
    invoke-interface {p0}, Lcom/android/sec/org/bouncycastle/operator/ContentSigner;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v1

    .line 78
    .local v1, "sOut":Ljava/io/OutputStream;
    new-instance v0, Lcom/android/sec/org/bouncycastle/asn1/DEROutputStream;

    invoke-direct {v0, v1}, Lcom/android/sec/org/bouncycastle/asn1/DEROutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 80
    .local v0, "dOut":Lcom/android/sec/org/bouncycastle/asn1/DEROutputStream;
    invoke-virtual {v0, p1}, Lcom/android/sec/org/bouncycastle/asn1/DEROutputStream;->writeObject(Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;)V

    .line 82
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V

    .line 84
    invoke-interface {p0}, Lcom/android/sec/org/bouncycastle/operator/ContentSigner;->getSignature()[B

    move-result-object v2

    return-object v2
.end method

.method private static generateStructure(Lcom/android/sec/org/bouncycastle/asn1/x509/TBSCertificate;Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;[B)Lcom/android/sec/org/bouncycastle/asn1/x509/Certificate;
    .locals 2
    .param p0, "tbsCert"    # Lcom/android/sec/org/bouncycastle/asn1/x509/TBSCertificate;
    .param p1, "sigAlgId"    # Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;
    .param p2, "signature"    # [B

    .prologue
    .line 89
    new-instance v0, Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;

    invoke-direct {v0}, Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;-><init>()V

    .line 91
    .local v0, "v":Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;
    invoke-virtual {v0, p0}, Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;->add(Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;)V

    .line 92
    invoke-virtual {v0, p1}, Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;->add(Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;)V

    .line 93
    new-instance v1, Lcom/android/sec/org/bouncycastle/asn1/DERBitString;

    invoke-direct {v1, p2}, Lcom/android/sec/org/bouncycastle/asn1/DERBitString;-><init>([B)V

    invoke-virtual {v0, v1}, Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;->add(Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;)V

    .line 95
    new-instance v1, Lcom/android/sec/org/bouncycastle/asn1/DERSequence;

    invoke-direct {v1, v0}, Lcom/android/sec/org/bouncycastle/asn1/DERSequence;-><init>(Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;)V

    invoke-static {v1}, Lcom/android/sec/org/bouncycastle/asn1/x509/Certificate;->getInstance(Ljava/lang/Object;)Lcom/android/sec/org/bouncycastle/asn1/x509/Certificate;

    move-result-object v1

    return-object v1
.end method

.method static getCriticalExtensionOIDs(Lcom/android/sec/org/bouncycastle/asn1/x509/Extensions;)Ljava/util/Set;
    .locals 2
    .param p0, "extensions"    # Lcom/android/sec/org/bouncycastle/asn1/x509/Extensions;

    .prologue
    .line 122
    if-nez p0, :cond_0

    .line 124
    sget-object v0, Lcom/android/sec/org/bouncycastle/cert/CertUtils;->EMPTY_SET:Ljava/util/Set;

    .line 127
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/util/HashSet;

    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/asn1/x509/Extensions;->getCriticalExtensionOIDs()[Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    goto :goto_0
.end method

.method static getExtensionOIDs(Lcom/android/sec/org/bouncycastle/asn1/x509/Extensions;)Ljava/util/List;
    .locals 1
    .param p0, "extensions"    # Lcom/android/sec/org/bouncycastle/asn1/x509/Extensions;

    .prologue
    .line 143
    if-nez p0, :cond_0

    .line 145
    sget-object v0, Lcom/android/sec/org/bouncycastle/cert/CertUtils;->EMPTY_LIST:Ljava/util/List;

    .line 148
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/asn1/x509/Extensions;->getExtensionOIDs()[Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method static getNonCriticalExtensionOIDs(Lcom/android/sec/org/bouncycastle/asn1/x509/Extensions;)Ljava/util/Set;
    .locals 2
    .param p0, "extensions"    # Lcom/android/sec/org/bouncycastle/asn1/x509/Extensions;

    .prologue
    .line 132
    if-nez p0, :cond_0

    .line 134
    sget-object v0, Lcom/android/sec/org/bouncycastle/cert/CertUtils;->EMPTY_SET:Ljava/util/Set;

    .line 138
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/util/HashSet;

    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/asn1/x509/Extensions;->getNonCriticalExtensionOIDs()[Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    goto :goto_0
.end method

.method static isAlgIdEqual(Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;)Z
    .locals 4
    .param p0, "id1"    # Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;
    .param p1, "id2"    # Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 217
    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;->getAlgorithm()Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    move-result-object v2

    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;->getAlgorithm()Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 242
    :cond_0
    :goto_0
    return v0

    .line 222
    :cond_1
    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;->getParameters()Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;

    move-result-object v2

    if-nez v2, :cond_3

    .line 224
    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;->getParameters()Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;->getParameters()Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;

    move-result-object v2

    sget-object v3, Lcom/android/sec/org/bouncycastle/asn1/DERNull;->INSTANCE:Lcom/android/sec/org/bouncycastle/asn1/DERNull;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_2
    move v0, v1

    .line 229
    goto :goto_0

    .line 232
    :cond_3
    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;->getParameters()Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;

    move-result-object v2

    if-nez v2, :cond_5

    .line 234
    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;->getParameters()Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;

    move-result-object v2

    if-eqz v2, :cond_4

    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;->getParameters()Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;

    move-result-object v2

    sget-object v3, Lcom/android/sec/org/bouncycastle/asn1/DERNull;->INSTANCE:Lcom/android/sec/org/bouncycastle/asn1/DERNull;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_4
    move v0, v1

    .line 239
    goto :goto_0

    .line 242
    :cond_5
    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;->getParameters()Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;

    move-result-object v0

    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;->getParameters()Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method static recoverDate(Lcom/android/sec/org/bouncycastle/asn1/ASN1GeneralizedTime;)Ljava/util/Date;
    .locals 4
    .param p0, "time"    # Lcom/android/sec/org/bouncycastle/asn1/ASN1GeneralizedTime;

    .prologue
    .line 207
    :try_start_0
    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/asn1/ASN1GeneralizedTime;->getDate()Ljava/util/Date;
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    .line 209
    :catch_0
    move-exception v0

    .line 211
    .local v0, "e":Ljava/text/ParseException;
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "unable to recover date: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/text/ParseException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1
.end method
