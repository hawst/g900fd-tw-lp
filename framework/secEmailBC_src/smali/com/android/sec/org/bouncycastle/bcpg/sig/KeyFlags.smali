.class public Lcom/android/sec/org/bouncycastle/bcpg/sig/KeyFlags;
.super Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;
.source "KeyFlags.java"


# static fields
.field public static final AUTHENTICATION:I = 0x20

.field public static final CERTIFY_OTHER:I = 0x1

.field public static final ENCRYPT_COMMS:I = 0x4

.field public static final ENCRYPT_STORAGE:I = 0x8

.field public static final SHARED:I = 0x80

.field public static final SIGN_DATA:I = 0x2

.field public static final SPLIT:I = 0x10


# direct methods
.method public constructor <init>(ZI)V
    .locals 2
    .param p1, "critical"    # Z
    .param p2, "flags"    # I

    .prologue
    .line 53
    const/16 v0, 0x1b

    invoke-static {p2}, Lcom/android/sec/org/bouncycastle/bcpg/sig/KeyFlags;->intToByteArray(I)[B

    move-result-object v1

    invoke-direct {p0, v0, p1, v1}, Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;-><init>(IZ[B)V

    .line 54
    return-void
.end method

.method public constructor <init>(Z[B)V
    .locals 1
    .param p1, "critical"    # Z
    .param p2, "data"    # [B

    .prologue
    .line 46
    const/16 v0, 0x1b

    invoke-direct {p0, v0, p1, p2}, Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;-><init>(IZ[B)V

    .line 47
    return-void
.end method

.method private static intToByteArray(I)[B
    .locals 7
    .param p0, "v"    # I

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x0

    .line 23
    new-array v3, v6, [B

    .line 24
    .local v3, "tmp":[B
    const/4 v2, 0x0

    .line 26
    .local v2, "size":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-eq v1, v6, :cond_1

    .line 28
    mul-int/lit8 v4, v1, 0x8

    shr-int v4, p0, v4

    int-to-byte v4, v4

    aput-byte v4, v3, v1

    .line 29
    aget-byte v4, v3, v1

    if-eqz v4, :cond_0

    .line 31
    move v2, v1

    .line 26
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 35
    :cond_1
    add-int/lit8 v4, v2, 0x1

    new-array v0, v4, [B

    .line 37
    .local v0, "data":[B
    array-length v4, v0

    invoke-static {v3, v5, v0, v5, v4}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 39
    return-object v0
.end method


# virtual methods
.method public getFlags()I
    .locals 4

    .prologue
    .line 64
    const/4 v0, 0x0

    .line 66
    .local v0, "flags":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/sig/KeyFlags;->data:[B

    array-length v2, v2

    if-eq v1, v2, :cond_0

    .line 68
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/sig/KeyFlags;->data:[B

    aget-byte v2, v2, v1

    and-int/lit16 v2, v2, 0xff

    mul-int/lit8 v3, v1, 0x8

    shl-int/2addr v2, v3

    or-int/2addr v0, v2

    .line 66
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 71
    :cond_0
    return v0
.end method
