.class public Lcom/android/sec/org/bouncycastle/openpgp/PGPMarker;
.super Ljava/lang/Object;
.source "PGPMarker.java"


# instance fields
.field private p:Lcom/android/sec/org/bouncycastle/bcpg/MarkerPacket;


# direct methods
.method public constructor <init>(Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;)V
    .locals 1
    .param p1, "in"    # Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->readPacket()Lcom/android/sec/org/bouncycastle/bcpg/Packet;

    move-result-object v0

    check-cast v0, Lcom/android/sec/org/bouncycastle/bcpg/MarkerPacket;

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPMarker;->p:Lcom/android/sec/org/bouncycastle/bcpg/MarkerPacket;

    .line 33
    return-void
.end method
