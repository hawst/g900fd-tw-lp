.class Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaContentVerifierProviderBuilder$1;
.super Ljava/lang/Object;
.source "JcaContentVerifierProviderBuilder.java"

# interfaces
.implements Lcom/android/sec/org/bouncycastle/operator/ContentVerifierProvider;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaContentVerifierProviderBuilder;->build(Ljava/security/cert/X509Certificate;)Lcom/android/sec/org/bouncycastle/operator/ContentVerifierProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private stream:Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaContentVerifierProviderBuilder$SignatureOutputStream;

.field final synthetic this$0:Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaContentVerifierProviderBuilder;

.field final synthetic val$certHolder:Lcom/android/sec/org/bouncycastle/cert/X509CertificateHolder;

.field final synthetic val$certificate:Ljava/security/cert/X509Certificate;


# direct methods
.method constructor <init>(Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaContentVerifierProviderBuilder;Lcom/android/sec/org/bouncycastle/cert/X509CertificateHolder;Ljava/security/cert/X509Certificate;)V
    .locals 0

    .prologue
    .line 70
    iput-object p1, p0, Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaContentVerifierProviderBuilder$1;->this$0:Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaContentVerifierProviderBuilder;

    iput-object p2, p0, Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaContentVerifierProviderBuilder$1;->val$certHolder:Lcom/android/sec/org/bouncycastle/cert/X509CertificateHolder;

    iput-object p3, p0, Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaContentVerifierProviderBuilder$1;->val$certificate:Ljava/security/cert/X509Certificate;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public get(Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;)Lcom/android/sec/org/bouncycastle/operator/ContentVerifier;
    .locals 6
    .param p1, "algorithm"    # Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/operator/OperatorCreationException;
        }
    .end annotation

    .prologue
    .line 88
    :try_start_0
    iget-object v3, p0, Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaContentVerifierProviderBuilder$1;->this$0:Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaContentVerifierProviderBuilder;

    # getter for: Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaContentVerifierProviderBuilder;->helper:Lcom/android/sec/org/bouncycastle/operator/jcajce/OperatorHelper;
    invoke-static {v3}, Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaContentVerifierProviderBuilder;->access$000(Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaContentVerifierProviderBuilder;)Lcom/android/sec/org/bouncycastle/operator/jcajce/OperatorHelper;

    move-result-object v3

    invoke-virtual {v3, p1}, Lcom/android/sec/org/bouncycastle/operator/jcajce/OperatorHelper;->createSignature(Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;)Ljava/security/Signature;

    move-result-object v2

    .line 90
    .local v2, "sig":Ljava/security/Signature;
    iget-object v3, p0, Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaContentVerifierProviderBuilder$1;->val$certificate:Ljava/security/cert/X509Certificate;

    invoke-virtual {v3}, Ljava/security/cert/X509Certificate;->getPublicKey()Ljava/security/PublicKey;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/security/Signature;->initVerify(Ljava/security/PublicKey;)V

    .line 92
    new-instance v3, Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaContentVerifierProviderBuilder$SignatureOutputStream;

    iget-object v4, p0, Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaContentVerifierProviderBuilder$1;->this$0:Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaContentVerifierProviderBuilder;

    invoke-direct {v3, v4, v2}, Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaContentVerifierProviderBuilder$SignatureOutputStream;-><init>(Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaContentVerifierProviderBuilder;Ljava/security/Signature;)V

    iput-object v3, p0, Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaContentVerifierProviderBuilder$1;->stream:Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaContentVerifierProviderBuilder$SignatureOutputStream;
    :try_end_0
    .catch Ljava/security/GeneralSecurityException; {:try_start_0 .. :try_end_0} :catch_0

    .line 99
    iget-object v3, p0, Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaContentVerifierProviderBuilder$1;->this$0:Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaContentVerifierProviderBuilder;

    iget-object v4, p0, Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaContentVerifierProviderBuilder$1;->val$certificate:Ljava/security/cert/X509Certificate;

    invoke-virtual {v4}, Ljava/security/cert/X509Certificate;->getPublicKey()Ljava/security/PublicKey;

    move-result-object v4

    # invokes: Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaContentVerifierProviderBuilder;->createRawSig(Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;Ljava/security/PublicKey;)Ljava/security/Signature;
    invoke-static {v3, p1, v4}, Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaContentVerifierProviderBuilder;->access$100(Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaContentVerifierProviderBuilder;Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;Ljava/security/PublicKey;)Ljava/security/Signature;

    move-result-object v1

    .line 101
    .local v1, "rawSig":Ljava/security/Signature;
    if-eqz v1, :cond_0

    .line 103
    new-instance v3, Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaContentVerifierProviderBuilder$RawSigVerifier;

    iget-object v4, p0, Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaContentVerifierProviderBuilder$1;->this$0:Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaContentVerifierProviderBuilder;

    iget-object v5, p0, Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaContentVerifierProviderBuilder$1;->stream:Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaContentVerifierProviderBuilder$SignatureOutputStream;

    invoke-direct {v3, v4, p1, v5, v1}, Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaContentVerifierProviderBuilder$RawSigVerifier;-><init>(Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaContentVerifierProviderBuilder;Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaContentVerifierProviderBuilder$SignatureOutputStream;Ljava/security/Signature;)V

    .line 107
    :goto_0
    return-object v3

    .line 94
    .end local v1    # "rawSig":Ljava/security/Signature;
    .end local v2    # "sig":Ljava/security/Signature;
    :catch_0
    move-exception v0

    .line 96
    .local v0, "e":Ljava/security/GeneralSecurityException;
    new-instance v3, Lcom/android/sec/org/bouncycastle/operator/OperatorCreationException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "exception on setup: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4, v0}, Lcom/android/sec/org/bouncycastle/operator/OperatorCreationException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3

    .line 107
    .end local v0    # "e":Ljava/security/GeneralSecurityException;
    .restart local v1    # "rawSig":Ljava/security/Signature;
    .restart local v2    # "sig":Ljava/security/Signature;
    :cond_0
    new-instance v3, Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaContentVerifierProviderBuilder$SigVerifier;

    iget-object v4, p0, Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaContentVerifierProviderBuilder$1;->this$0:Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaContentVerifierProviderBuilder;

    iget-object v5, p0, Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaContentVerifierProviderBuilder$1;->stream:Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaContentVerifierProviderBuilder$SignatureOutputStream;

    invoke-direct {v3, v4, p1, v5}, Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaContentVerifierProviderBuilder$SigVerifier;-><init>(Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaContentVerifierProviderBuilder;Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaContentVerifierProviderBuilder$SignatureOutputStream;)V

    goto :goto_0
.end method

.method public getAssociatedCertificate()Lcom/android/sec/org/bouncycastle/cert/X509CertificateHolder;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaContentVerifierProviderBuilder$1;->val$certHolder:Lcom/android/sec/org/bouncycastle/cert/X509CertificateHolder;

    return-object v0
.end method

.method public hasAssociatedCertificate()Z
    .locals 1

    .prologue
    .line 75
    const/4 v0, 0x1

    return v0
.end method
