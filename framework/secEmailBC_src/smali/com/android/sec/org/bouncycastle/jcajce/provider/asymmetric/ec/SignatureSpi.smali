.class public Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/ec/SignatureSpi;
.super Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/util/DSABase;
.source "SignatureSpi.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/ec/SignatureSpi$1;,
        Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/ec/SignatureSpi$CVCDSAEncoder;,
        Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/ec/SignatureSpi$StdDSAEncoder;,
        Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/ec/SignatureSpi$ecDSA512;,
        Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/ec/SignatureSpi$ecDSA384;,
        Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/ec/SignatureSpi$ecDSA256;,
        Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/ec/SignatureSpi$ecDSAnone;,
        Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/ec/SignatureSpi$ecDSA;
    }
.end annotation


# direct methods
.method constructor <init>(Lcom/android/sec/org/bouncycastle/crypto/Digest;Lcom/android/sec/org/bouncycastle/crypto/DSA;Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/util/DSAEncoder;)V
    .locals 0
    .param p1, "digest"    # Lcom/android/sec/org/bouncycastle/crypto/Digest;
    .param p2, "signer"    # Lcom/android/sec/org/bouncycastle/crypto/DSA;
    .param p3, "encoder"    # Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/util/DSAEncoder;

    .prologue
    .line 44
    invoke-direct {p0, p1, p2, p3}, Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/util/DSABase;-><init>(Lcom/android/sec/org/bouncycastle/crypto/Digest;Lcom/android/sec/org/bouncycastle/crypto/DSA;Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/util/DSAEncoder;)V

    .line 45
    return-void
.end method


# virtual methods
.method protected engineInitSign(Ljava/security/PrivateKey;)V
    .locals 5
    .param p1, "privateKey"    # Ljava/security/PrivateKey;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/InvalidKeyException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 60
    invoke-static {p1}, Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/util/ECUtil;->generatePrivateKeyParameter(Ljava/security/PrivateKey;)Lcom/android/sec/org/bouncycastle/crypto/params/AsymmetricKeyParameter;

    move-result-object v0

    .line 62
    .local v0, "param":Lcom/android/sec/org/bouncycastle/crypto/CipherParameters;
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/ec/SignatureSpi;->digest:Lcom/android/sec/org/bouncycastle/crypto/Digest;

    invoke-interface {v1}, Lcom/android/sec/org/bouncycastle/crypto/Digest;->reset()V

    .line 64
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/ec/SignatureSpi;->appRandom:Ljava/security/SecureRandom;

    if-eqz v1, :cond_0

    .line 66
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/ec/SignatureSpi;->signer:Lcom/android/sec/org/bouncycastle/crypto/DSA;

    new-instance v2, Lcom/android/sec/org/bouncycastle/crypto/params/ParametersWithRandom;

    iget-object v3, p0, Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/ec/SignatureSpi;->appRandom:Ljava/security/SecureRandom;

    invoke-direct {v2, v0, v3}, Lcom/android/sec/org/bouncycastle/crypto/params/ParametersWithRandom;-><init>(Lcom/android/sec/org/bouncycastle/crypto/CipherParameters;Ljava/security/SecureRandom;)V

    invoke-interface {v1, v4, v2}, Lcom/android/sec/org/bouncycastle/crypto/DSA;->init(ZLcom/android/sec/org/bouncycastle/crypto/CipherParameters;)V

    .line 72
    :goto_0
    return-void

    .line 70
    :cond_0
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/ec/SignatureSpi;->signer:Lcom/android/sec/org/bouncycastle/crypto/DSA;

    invoke-interface {v1, v4, v0}, Lcom/android/sec/org/bouncycastle/crypto/DSA;->init(ZLcom/android/sec/org/bouncycastle/crypto/CipherParameters;)V

    goto :goto_0
.end method

.method protected engineInitVerify(Ljava/security/PublicKey;)V
    .locals 3
    .param p1, "publicKey"    # Ljava/security/PublicKey;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/InvalidKeyException;
        }
    .end annotation

    .prologue
    .line 50
    invoke-static {p1}, Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/util/ECUtil;->generatePublicKeyParameter(Ljava/security/PublicKey;)Lcom/android/sec/org/bouncycastle/crypto/params/AsymmetricKeyParameter;

    move-result-object v0

    .line 52
    .local v0, "param":Lcom/android/sec/org/bouncycastle/crypto/CipherParameters;
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/ec/SignatureSpi;->digest:Lcom/android/sec/org/bouncycastle/crypto/Digest;

    invoke-interface {v1}, Lcom/android/sec/org/bouncycastle/crypto/Digest;->reset()V

    .line 53
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/ec/SignatureSpi;->signer:Lcom/android/sec/org/bouncycastle/crypto/DSA;

    const/4 v2, 0x0

    invoke-interface {v1, v2, v0}, Lcom/android/sec/org/bouncycastle/crypto/DSA;->init(ZLcom/android/sec/org/bouncycastle/crypto/CipherParameters;)V

    .line 54
    return-void
.end method
