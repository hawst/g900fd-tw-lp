.class public Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;
.super Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyRing;
.source "PGPSecretKeyRing.java"


# instance fields
.field extraPubKeys:Ljava/util/List;

.field keys:Ljava/util/List;


# direct methods
.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 15
    .param p1, "in"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
        }
    .end annotation

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyRing;-><init>()V

    .line 57
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;->keys:Ljava/util/List;

    .line 58
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;->extraPubKeys:Ljava/util/List;

    .line 60
    invoke-static/range {p1 .. p1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;->wrap(Ljava/io/InputStream;)Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;

    move-result-object v8

    .line 62
    .local v8, "pIn":Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;
    invoke-virtual {v8}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->nextPacketTag()I

    move-result v7

    .line 63
    .local v7, "initialTag":I
    const/4 v0, 0x5

    if-eq v7, v0, :cond_0

    const/4 v0, 0x7

    if-eq v7, v0, :cond_0

    .line 65
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "secret key ring doesn\'t start with secret key tag: tag 0x"

    invoke-virtual {v1, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v7}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v1, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 70
    :cond_0
    invoke-virtual {v8}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->readPacket()Lcom/android/sec/org/bouncycastle/bcpg/Packet;

    move-result-object v9

    check-cast v9, Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;

    .line 75
    .local v9, "secret":Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;
    :goto_0
    invoke-virtual {v8}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->nextPacketTag()I

    move-result v0

    const/16 v1, 0x3d

    if-ne v0, v1, :cond_1

    .line 77
    invoke-virtual {v8}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->readPacket()Lcom/android/sec/org/bouncycastle/bcpg/Packet;

    goto :goto_0

    .line 80
    :cond_1
    invoke-static {v8}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;->readOptionalTrustPacket(Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;)Lcom/android/sec/org/bouncycastle/bcpg/TrustPacket;

    move-result-object v2

    .line 83
    .local v2, "trust":Lcom/android/sec/org/bouncycastle/bcpg/TrustPacket;
    invoke-static {v8}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;->readSignaturesAndTrust(Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;)Ljava/util/List;

    move-result-object v3

    .line 85
    .local v3, "keySigs":Ljava/util/List;
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 86
    .local v4, "ids":Ljava/util/List;
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 87
    .local v5, "idTrusts":Ljava/util/List;
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 88
    .local v6, "idSigs":Ljava/util/List;
    invoke-static {v8, v4, v5, v6}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;->readUserIDs(Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V

    .line 90
    iget-object v13, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;->keys:Ljava/util/List;

    new-instance v14, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;

    new-instance v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;

    invoke-virtual {v9}, Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;->getPublicKeyPacket()Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;

    move-result-object v1

    invoke-direct/range {v0 .. v6}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;-><init>(Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;Lcom/android/sec/org/bouncycastle/bcpg/TrustPacket;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V

    invoke-direct {v14, v9, v0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;-><init>(Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;)V

    invoke-interface {v13, v14}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 95
    :goto_1
    invoke-virtual {v8}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->nextPacketTag()I

    move-result v0

    const/4 v1, 0x7

    if-eq v0, v1, :cond_2

    invoke-virtual {v8}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->nextPacketTag()I

    move-result v0

    const/16 v1, 0xe

    if-ne v0, v1, :cond_5

    .line 97
    :cond_2
    invoke-virtual {v8}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->nextPacketTag()I

    move-result v0

    const/4 v1, 0x7

    if-ne v0, v1, :cond_4

    .line 99
    invoke-virtual {v8}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->readPacket()Lcom/android/sec/org/bouncycastle/bcpg/Packet;

    move-result-object v11

    check-cast v11, Lcom/android/sec/org/bouncycastle/bcpg/SecretSubkeyPacket;

    .line 104
    .local v11, "sub":Lcom/android/sec/org/bouncycastle/bcpg/SecretSubkeyPacket;
    :goto_2
    invoke-virtual {v8}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->nextPacketTag()I

    move-result v0

    const/16 v1, 0x3d

    if-ne v0, v1, :cond_3

    .line 106
    invoke-virtual {v8}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->readPacket()Lcom/android/sec/org/bouncycastle/bcpg/Packet;

    goto :goto_2

    .line 109
    :cond_3
    invoke-static {v8}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;->readOptionalTrustPacket(Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;)Lcom/android/sec/org/bouncycastle/bcpg/TrustPacket;

    move-result-object v12

    .line 110
    .local v12, "subTrust":Lcom/android/sec/org/bouncycastle/bcpg/TrustPacket;
    invoke-static {v8}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;->readSignaturesAndTrust(Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;)Ljava/util/List;

    move-result-object v10

    .line 112
    .local v10, "sigList":Ljava/util/List;
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;->keys:Ljava/util/List;

    new-instance v1, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;

    new-instance v13, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;

    invoke-virtual {v11}, Lcom/android/sec/org/bouncycastle/bcpg/SecretSubkeyPacket;->getPublicKeyPacket()Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;

    move-result-object v14

    invoke-direct {v13, v14, v12, v10}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;-><init>(Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;Lcom/android/sec/org/bouncycastle/bcpg/TrustPacket;Ljava/util/List;)V

    invoke-direct {v1, v11, v13}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;-><init>(Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 116
    .end local v10    # "sigList":Ljava/util/List;
    .end local v11    # "sub":Lcom/android/sec/org/bouncycastle/bcpg/SecretSubkeyPacket;
    .end local v12    # "subTrust":Lcom/android/sec/org/bouncycastle/bcpg/TrustPacket;
    :cond_4
    invoke-virtual {v8}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->readPacket()Lcom/android/sec/org/bouncycastle/bcpg/Packet;

    move-result-object v11

    check-cast v11, Lcom/android/sec/org/bouncycastle/bcpg/PublicSubkeyPacket;

    .line 118
    .local v11, "sub":Lcom/android/sec/org/bouncycastle/bcpg/PublicSubkeyPacket;
    invoke-static {v8}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;->readOptionalTrustPacket(Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;)Lcom/android/sec/org/bouncycastle/bcpg/TrustPacket;

    move-result-object v12

    .line 119
    .restart local v12    # "subTrust":Lcom/android/sec/org/bouncycastle/bcpg/TrustPacket;
    invoke-static {v8}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;->readSignaturesAndTrust(Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;)Ljava/util/List;

    move-result-object v10

    .line 121
    .restart local v10    # "sigList":Ljava/util/List;
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;->extraPubKeys:Ljava/util/List;

    new-instance v1, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;

    invoke-direct {v1, v11, v12, v10}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;-><init>(Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;Lcom/android/sec/org/bouncycastle/bcpg/TrustPacket;Ljava/util/List;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 124
    .end local v10    # "sigList":Ljava/util/List;
    .end local v11    # "sub":Lcom/android/sec/org/bouncycastle/bcpg/PublicSubkeyPacket;
    .end local v12    # "subTrust":Lcom/android/sec/org/bouncycastle/bcpg/TrustPacket;
    :cond_5
    return-void
.end method

.method public constructor <init>(Ljava/util/List;)V
    .locals 1
    .param p1, "keys"    # Ljava/util/List;

    .prologue
    .line 37
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {p0, p1, v0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;-><init>(Ljava/util/List;Ljava/util/List;)V

    .line 38
    return-void
.end method

.method private constructor <init>(Ljava/util/List;Ljava/util/List;)V
    .locals 0
    .param p1, "keys"    # Ljava/util/List;
    .param p2, "extraPubKeys"    # Ljava/util/List;

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyRing;-><init>()V

    .line 42
    iput-object p1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;->keys:Ljava/util/List;

    .line 43
    iput-object p2, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;->extraPubKeys:Ljava/util/List;

    .line 44
    return-void
.end method

.method public constructor <init>([B)V
    .locals 1
    .param p1, "encoding"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
        }
    .end annotation

    .prologue
    .line 50
    new-instance v0, Ljava/io/ByteArrayInputStream;

    invoke-direct {v0, p1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {p0, v0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;-><init>(Ljava/io/InputStream;)V

    .line 51
    return-void
.end method

.method public static copyWithNewPassword(Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;[C[CILjava/security/SecureRandom;Ljava/lang/String;)Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;
    .locals 6
    .param p0, "ring"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;
    .param p1, "oldPassPhrase"    # [C
    .param p2, "newPassPhrase"    # [C
    .param p3, "newEncAlgorithm"    # I
    .param p4, "rand"    # Ljava/security/SecureRandom;
    .param p5, "provider"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;,
            Ljava/security/NoSuchProviderException;
        }
    .end annotation

    .prologue
    .line 254
    invoke-static {p5}, Lcom/android/sec/org/bouncycastle/openpgp/PGPUtil;->getProvider(Ljava/lang/String;)Ljava/security/Provider;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    invoke-static/range {v0 .. v5}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;->copyWithNewPassword(Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;[C[CILjava/security/SecureRandom;Ljava/security/Provider;)Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;

    move-result-object v0

    return-object v0
.end method

.method public static copyWithNewPassword(Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;[C[CILjava/security/SecureRandom;Ljava/security/Provider;)Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;
    .locals 8
    .param p0, "ring"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;
    .param p1, "oldPassPhrase"    # [C
    .param p2, "newPassPhrase"    # [C
    .param p3, "newEncAlgorithm"    # I
    .param p4, "rand"    # Ljava/security/SecureRandom;
    .param p5, "provider"    # Ljava/security/Provider;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
        }
    .end annotation

    .prologue
    .line 277
    new-instance v7, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;->keys:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v7, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 279
    .local v7, "newKeys":Ljava/util/List;
    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;->getSecretKeys()Ljava/util/Iterator;

    move-result-object v6

    .local v6, "keys":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 281
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-static/range {v0 .. v5}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;->copyWithNewPassword(Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;[C[CILjava/security/SecureRandom;Ljava/security/Provider;)Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;

    move-result-object v0

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 284
    :cond_0
    new-instance v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;

    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;->extraPubKeys:Ljava/util/List;

    invoke-direct {v0, v7, v1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;-><init>(Ljava/util/List;Ljava/util/List;)V

    return-object v0
.end method

.method public static insertSecretKey(Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;)Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;
    .locals 10
    .param p0, "secRing"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;
    .param p1, "secKey"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;

    .prologue
    .line 299
    new-instance v3, Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;->keys:Ljava/util/List;

    invoke-direct {v3, v5}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 300
    .local v3, "keys":Ljava/util/List;
    const/4 v0, 0x0

    .line 301
    .local v0, "found":Z
    const/4 v4, 0x0

    .line 303
    .local v4, "masterFound":Z
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v5

    if-eq v1, v5, :cond_2

    .line 305
    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;

    .line 307
    .local v2, "key":Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;
    invoke-virtual {v2}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;->getKeyID()J

    move-result-wide v6

    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;->getKeyID()J

    move-result-wide v8

    cmp-long v5, v6, v8

    if-nez v5, :cond_0

    .line 309
    const/4 v0, 0x1

    .line 310
    invoke-interface {v3, v1, p1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 312
    :cond_0
    invoke-virtual {v2}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;->isMasterKey()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 314
    const/4 v4, 0x1

    .line 303
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 318
    .end local v2    # "key":Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;
    :cond_2
    if-nez v0, :cond_4

    .line 320
    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;->isMasterKey()Z

    move-result v5

    if-eqz v5, :cond_5

    .line 322
    if-eqz v4, :cond_3

    .line 324
    new-instance v5, Ljava/lang/IllegalArgumentException;

    const-string v6, "cannot add a master key to a ring that already has one"

    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 327
    :cond_3
    const/4 v5, 0x0

    invoke-interface {v3, v5, p1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 335
    :cond_4
    :goto_1
    new-instance v5, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;

    iget-object v6, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;->extraPubKeys:Ljava/util/List;

    invoke-direct {v5, v3, v6}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;-><init>(Ljava/util/List;Ljava/util/List;)V

    return-object v5

    .line 331
    :cond_5
    invoke-interface {v3, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method public static removeSecretKey(Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;)Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;
    .locals 8
    .param p0, "secRing"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;
    .param p1, "secKey"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;

    .prologue
    .line 350
    new-instance v3, Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;->keys:Ljava/util/List;

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 351
    .local v3, "keys":Ljava/util/List;
    const/4 v0, 0x0

    .line 353
    .local v0, "found":Z
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    if-ge v1, v4, :cond_1

    .line 355
    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;

    .line 357
    .local v2, "key":Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;
    invoke-virtual {v2}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;->getKeyID()J

    move-result-wide v4

    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;->getKeyID()J

    move-result-wide v6

    cmp-long v4, v4, v6

    if-nez v4, :cond_0

    .line 359
    const/4 v0, 0x1

    .line 360
    invoke-interface {v3, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 353
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 364
    .end local v2    # "key":Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;
    :cond_1
    if-nez v0, :cond_2

    .line 366
    const/4 v4, 0x0

    .line 369
    :goto_1
    return-object v4

    :cond_2
    new-instance v4, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;

    iget-object v5, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;->extraPubKeys:Ljava/util/List;

    invoke-direct {v4, v3, v5}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;-><init>(Ljava/util/List;Ljava/util/List;)V

    goto :goto_1
.end method

.method public static replacePublicKeys(Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyRing;)Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;
    .locals 6
    .param p0, "secretRing"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;
    .param p1, "publicRing"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyRing;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
        }
    .end annotation

    .prologue
    .line 220
    new-instance v1, Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;->keys:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    invoke-direct {v1, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 222
    .local v1, "newList":Ljava/util/List;
    iget-object v4, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;->keys:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "it":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 224
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;

    .line 225
    .local v3, "sk":Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;
    const/4 v2, 0x0

    .line 226
    .local v2, "pk":Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;
    invoke-virtual {v3}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;->getKeyID()J

    move-result-wide v4

    invoke-virtual {p1, v4, v5}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyRing;->getPublicKey(J)Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;

    move-result-object v2

    .line 228
    invoke-static {v3, v2}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;->replacePublicKey(Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;)Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 231
    .end local v2    # "pk":Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;
    .end local v3    # "sk":Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;
    :cond_0
    new-instance v4, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;

    invoke-direct {v4, v1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;-><init>(Ljava/util/List;)V

    return-object v4
.end method


# virtual methods
.method public encode(Ljava/io/OutputStream;)V
    .locals 3
    .param p1, "outStream"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 198
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;->keys:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-eq v0, v2, :cond_0

    .line 200
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;->keys:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;

    .line 202
    .local v1, "k":Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;
    invoke-virtual {v1, p1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;->encode(Ljava/io/OutputStream;)V

    .line 198
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 204
    .end local v1    # "k":Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;
    :cond_0
    const/4 v0, 0x0

    :goto_1
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;->extraPubKeys:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-eq v0, v2, :cond_1

    .line 206
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;->extraPubKeys:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;

    .line 208
    .local v1, "k":Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;
    invoke-virtual {v1, p1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->encode(Ljava/io/OutputStream;)V

    .line 204
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 210
    .end local v1    # "k":Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;
    :cond_1
    return-void
.end method

.method public getEncoded()[B
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 187
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 189
    .local v0, "bOut":Ljava/io/ByteArrayOutputStream;
    invoke-virtual {p0, v0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;->encode(Ljava/io/OutputStream;)V

    .line 191
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    return-object v1
.end method

.method public getExtraPublicKeys()Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 181
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;->extraPubKeys:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public getPublicKey()Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;
    .locals 2

    .prologue
    .line 133
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;->keys:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;->getPublicKey()Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;

    move-result-object v0

    return-object v0
.end method

.method public getSecretKey()Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;
    .locals 2

    .prologue
    .line 143
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;->keys:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;

    return-object v0
.end method

.method public getSecretKey(J)Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;
    .locals 5
    .param p1, "keyId"    # J

    .prologue
    .line 159
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;->keys:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-eq v0, v2, :cond_1

    .line 161
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;->keys:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;

    .line 163
    .local v1, "k":Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;
    invoke-virtual {v1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;->getKeyID()J

    move-result-wide v2

    cmp-long v2, p1, v2

    if-nez v2, :cond_0

    .line 169
    .end local v1    # "k":Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;
    :goto_1
    return-object v1

    .line 159
    .restart local v1    # "k":Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 169
    .end local v1    # "k":Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public getSecretKeys()Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 153
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;->keys:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method
