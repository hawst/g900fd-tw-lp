.class public Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacketInputStream;
.super Ljava/io/InputStream;
.source "SignatureSubpacketInputStream.java"

# interfaces
.implements Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacketTags;


# instance fields
.field in:Ljava/io/InputStream;


# direct methods
.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 0
    .param p1, "in"    # Ljava/io/InputStream;

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacketInputStream;->in:Ljava/io/InputStream;

    .line 33
    return-void
.end method


# virtual methods
.method public available()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 38
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacketInputStream;->in:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->available()I

    move-result v0

    return v0
.end method

.method public read()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 44
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacketInputStream;->in:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->read()I

    move-result v0

    return v0
.end method

.method public readPacket()Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 50
    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacketInputStream;->read()I

    move-result v3

    .line 51
    .local v3, "l":I
    const/4 v0, 0x0

    .line 53
    .local v0, "bodyLen":I
    if-gez v3, :cond_0

    .line 55
    const/4 v6, 0x0

    .line 121
    :goto_0
    return-object v6

    .line 58
    :cond_0
    const/16 v6, 0xc0

    if-ge v3, v6, :cond_2

    .line 60
    move v0, v3

    .line 75
    :cond_1
    :goto_1
    iget-object v6, p0, Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacketInputStream;->in:Ljava/io/InputStream;

    invoke-virtual {v6}, Ljava/io/InputStream;->read()I

    move-result v4

    .line 77
    .local v4, "tag":I
    if-gez v4, :cond_4

    .line 79
    new-instance v6, Ljava/io/EOFException;

    const-string v7, "unexpected EOF reading signature sub packet"

    invoke-direct {v6, v7}, Ljava/io/EOFException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 62
    .end local v4    # "tag":I
    :cond_2
    const/16 v6, 0xdf

    if-gt v3, v6, :cond_3

    .line 64
    add-int/lit16 v6, v3, -0xc0

    shl-int/lit8 v6, v6, 0x8

    iget-object v7, p0, Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacketInputStream;->in:Ljava/io/InputStream;

    invoke-virtual {v7}, Ljava/io/InputStream;->read()I

    move-result v7

    add-int/2addr v6, v7

    add-int/lit16 v0, v6, 0xc0

    goto :goto_1

    .line 66
    :cond_3
    const/16 v6, 0xff

    if-ne v3, v6, :cond_1

    .line 68
    iget-object v6, p0, Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacketInputStream;->in:Ljava/io/InputStream;

    invoke-virtual {v6}, Ljava/io/InputStream;->read()I

    move-result v6

    shl-int/lit8 v6, v6, 0x18

    iget-object v7, p0, Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacketInputStream;->in:Ljava/io/InputStream;

    invoke-virtual {v7}, Ljava/io/InputStream;->read()I

    move-result v7

    shl-int/lit8 v7, v7, 0x10

    or-int/2addr v6, v7

    iget-object v7, p0, Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacketInputStream;->in:Ljava/io/InputStream;

    invoke-virtual {v7}, Ljava/io/InputStream;->read()I

    move-result v7

    shl-int/lit8 v7, v7, 0x8

    or-int/2addr v6, v7

    iget-object v7, p0, Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacketInputStream;->in:Ljava/io/InputStream;

    invoke-virtual {v7}, Ljava/io/InputStream;->read()I

    move-result v7

    or-int v0, v6, v7

    goto :goto_1

    .line 82
    .restart local v4    # "tag":I
    :cond_4
    add-int/lit8 v6, v0, -0x1

    new-array v1, v6, [B

    .line 83
    .local v1, "data":[B
    iget-object v6, p0, Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacketInputStream;->in:Ljava/io/InputStream;

    invoke-static {v6, v1}, Lcom/android/sec/org/bouncycastle/util/io/Streams;->readFully(Ljava/io/InputStream;[B)I

    move-result v6

    array-length v7, v1

    if-ge v6, v7, :cond_5

    .line 85
    new-instance v6, Ljava/io/EOFException;

    invoke-direct {v6}, Ljava/io/EOFException;-><init>()V

    throw v6

    .line 88
    :cond_5
    and-int/lit16 v6, v4, 0x80

    if-eqz v6, :cond_6

    const/4 v2, 0x1

    .line 89
    .local v2, "isCritical":Z
    :goto_2
    and-int/lit8 v5, v4, 0x7f

    .line 91
    .local v5, "type":I
    packed-switch v5, :pswitch_data_0

    .line 121
    :pswitch_0
    new-instance v6, Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;

    invoke-direct {v6, v5, v2, v1}, Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;-><init>(IZ[B)V

    goto :goto_0

    .line 88
    .end local v2    # "isCritical":Z
    .end local v5    # "type":I
    :cond_6
    const/4 v2, 0x0

    goto :goto_2

    .line 94
    .restart local v2    # "isCritical":Z
    .restart local v5    # "type":I
    :pswitch_1
    new-instance v6, Lcom/android/sec/org/bouncycastle/bcpg/sig/SignatureCreationTime;

    invoke-direct {v6, v2, v1}, Lcom/android/sec/org/bouncycastle/bcpg/sig/SignatureCreationTime;-><init>(Z[B)V

    goto :goto_0

    .line 96
    :pswitch_2
    new-instance v6, Lcom/android/sec/org/bouncycastle/bcpg/sig/KeyExpirationTime;

    invoke-direct {v6, v2, v1}, Lcom/android/sec/org/bouncycastle/bcpg/sig/KeyExpirationTime;-><init>(Z[B)V

    goto :goto_0

    .line 98
    :pswitch_3
    new-instance v6, Lcom/android/sec/org/bouncycastle/bcpg/sig/SignatureExpirationTime;

    invoke-direct {v6, v2, v1}, Lcom/android/sec/org/bouncycastle/bcpg/sig/SignatureExpirationTime;-><init>(Z[B)V

    goto/16 :goto_0

    .line 100
    :pswitch_4
    new-instance v6, Lcom/android/sec/org/bouncycastle/bcpg/sig/Revocable;

    invoke-direct {v6, v2, v1}, Lcom/android/sec/org/bouncycastle/bcpg/sig/Revocable;-><init>(Z[B)V

    goto/16 :goto_0

    .line 102
    :pswitch_5
    new-instance v6, Lcom/android/sec/org/bouncycastle/bcpg/sig/Exportable;

    invoke-direct {v6, v2, v1}, Lcom/android/sec/org/bouncycastle/bcpg/sig/Exportable;-><init>(Z[B)V

    goto/16 :goto_0

    .line 104
    :pswitch_6
    new-instance v6, Lcom/android/sec/org/bouncycastle/bcpg/sig/IssuerKeyID;

    invoke-direct {v6, v2, v1}, Lcom/android/sec/org/bouncycastle/bcpg/sig/IssuerKeyID;-><init>(Z[B)V

    goto/16 :goto_0

    .line 106
    :pswitch_7
    new-instance v6, Lcom/android/sec/org/bouncycastle/bcpg/sig/TrustSignature;

    invoke-direct {v6, v2, v1}, Lcom/android/sec/org/bouncycastle/bcpg/sig/TrustSignature;-><init>(Z[B)V

    goto/16 :goto_0

    .line 110
    :pswitch_8
    new-instance v6, Lcom/android/sec/org/bouncycastle/bcpg/sig/PreferredAlgorithms;

    invoke-direct {v6, v5, v2, v1}, Lcom/android/sec/org/bouncycastle/bcpg/sig/PreferredAlgorithms;-><init>(IZ[B)V

    goto/16 :goto_0

    .line 112
    :pswitch_9
    new-instance v6, Lcom/android/sec/org/bouncycastle/bcpg/sig/KeyFlags;

    invoke-direct {v6, v2, v1}, Lcom/android/sec/org/bouncycastle/bcpg/sig/KeyFlags;-><init>(Z[B)V

    goto/16 :goto_0

    .line 114
    :pswitch_a
    new-instance v6, Lcom/android/sec/org/bouncycastle/bcpg/sig/PrimaryUserID;

    invoke-direct {v6, v2, v1}, Lcom/android/sec/org/bouncycastle/bcpg/sig/PrimaryUserID;-><init>(Z[B)V

    goto/16 :goto_0

    .line 116
    :pswitch_b
    new-instance v6, Lcom/android/sec/org/bouncycastle/bcpg/sig/SignerUserID;

    invoke-direct {v6, v2, v1}, Lcom/android/sec/org/bouncycastle/bcpg/sig/SignerUserID;-><init>(Z[B)V

    goto/16 :goto_0

    .line 118
    :pswitch_c
    new-instance v6, Lcom/android/sec/org/bouncycastle/bcpg/sig/NotationData;

    invoke-direct {v6, v2, v1}, Lcom/android/sec/org/bouncycastle/bcpg/sig/NotationData;-><init>(Z[B)V

    goto/16 :goto_0

    .line 91
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_3
        :pswitch_5
        :pswitch_7
        :pswitch_0
        :pswitch_4
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_8
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_6
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_c
        :pswitch_8
        :pswitch_8
        :pswitch_0
        :pswitch_0
        :pswitch_a
        :pswitch_0
        :pswitch_9
        :pswitch_b
    .end packed-switch
.end method
