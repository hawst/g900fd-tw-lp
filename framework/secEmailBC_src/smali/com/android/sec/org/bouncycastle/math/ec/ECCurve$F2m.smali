.class public Lcom/android/sec/org/bouncycastle/math/ec/ECCurve$F2m;
.super Lcom/android/sec/org/bouncycastle/math/ec/ECCurve;
.source "ECCurve.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/sec/org/bouncycastle/math/ec/ECCurve;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "F2m"
.end annotation


# instance fields
.field private h:Ljava/math/BigInteger;

.field private infinity:Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$F2m;

.field private k1:I

.field private k2:I

.field private k3:I

.field private m:I

.field private mu:B

.field private n:Ljava/math/BigInteger;

.field private si:[Ljava/math/BigInteger;


# direct methods
.method public constructor <init>(IIIILjava/math/BigInteger;Ljava/math/BigInteger;)V
    .locals 9
    .param p1, "m"    # I
    .param p2, "k1"    # I
    .param p3, "k2"    # I
    .param p4, "k3"    # I
    .param p5, "a"    # Ljava/math/BigInteger;
    .param p6, "b"    # Ljava/math/BigInteger;

    .prologue
    const/4 v7, 0x0

    .line 336
    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    move-object v6, p6

    move-object v8, v7

    invoke-direct/range {v0 .. v8}, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve$F2m;-><init>(IIIILjava/math/BigInteger;Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/math/BigInteger;)V

    .line 337
    return-void
.end method

.method public constructor <init>(IIIILjava/math/BigInteger;Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/math/BigInteger;)V
    .locals 2
    .param p1, "m"    # I
    .param p2, "k1"    # I
    .param p3, "k2"    # I
    .param p4, "k3"    # I
    .param p5, "a"    # Ljava/math/BigInteger;
    .param p6, "b"    # Ljava/math/BigInteger;
    .param p7, "n"    # Ljava/math/BigInteger;
    .param p8, "h"    # Ljava/math/BigInteger;

    .prologue
    const/4 v1, 0x0

    .line 371
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve;-><init>()V

    .line 248
    const/4 v0, 0x0

    iput-byte v0, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve$F2m;->mu:B

    .line 255
    iput-object v1, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve$F2m;->si:[Ljava/math/BigInteger;

    .line 372
    iput p1, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve$F2m;->m:I

    .line 373
    iput p2, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve$F2m;->k1:I

    .line 374
    iput p3, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve$F2m;->k2:I

    .line 375
    iput p4, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve$F2m;->k3:I

    .line 376
    iput-object p7, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve$F2m;->n:Ljava/math/BigInteger;

    .line 377
    iput-object p8, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve$F2m;->h:Ljava/math/BigInteger;

    .line 379
    if-nez p2, :cond_0

    .line 381
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "k1 must be > 0"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 384
    :cond_0
    if-nez p3, :cond_1

    .line 386
    if-eqz p4, :cond_3

    .line 388
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "k3 must be 0 if k2 == 0"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 393
    :cond_1
    if-gt p3, p2, :cond_2

    .line 395
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "k2 must be > k1"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 398
    :cond_2
    if-gt p4, p3, :cond_3

    .line 400
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "k3 must be > k2"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 404
    :cond_3
    invoke-virtual {p0, p5}, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve$F2m;->fromBigInteger(Ljava/math/BigInteger;)Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    move-result-object v0

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve$F2m;->a:Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    .line 405
    invoke-virtual {p0, p6}, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve$F2m;->fromBigInteger(Ljava/math/BigInteger;)Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    move-result-object v0

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve$F2m;->b:Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    .line 406
    new-instance v0, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$F2m;

    invoke-direct {v0, p0, v1, v1}, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$F2m;-><init>(Lcom/android/sec/org/bouncycastle/math/ec/ECCurve;Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;)V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve$F2m;->infinity:Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$F2m;

    .line 407
    return-void
.end method

.method public constructor <init>(IILjava/math/BigInteger;Ljava/math/BigInteger;)V
    .locals 9
    .param p1, "m"    # I
    .param p2, "k"    # I
    .param p3, "a"    # Ljava/math/BigInteger;
    .param p4, "b"    # Ljava/math/BigInteger;

    .prologue
    const/4 v7, 0x0

    const/4 v3, 0x0

    .line 277
    move-object v0, p0

    move v1, p1

    move v2, p2

    move v4, v3

    move-object v5, p3

    move-object v6, p4

    move-object v8, v7

    invoke-direct/range {v0 .. v8}, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve$F2m;-><init>(IIIILjava/math/BigInteger;Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/math/BigInteger;)V

    .line 278
    return-void
.end method

.method public constructor <init>(IILjava/math/BigInteger;Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/math/BigInteger;)V
    .locals 9
    .param p1, "m"    # I
    .param p2, "k"    # I
    .param p3, "a"    # Ljava/math/BigInteger;
    .param p4, "b"    # Ljava/math/BigInteger;
    .param p5, "n"    # Ljava/math/BigInteger;
    .param p6, "h"    # Ljava/math/BigInteger;

    .prologue
    const/4 v3, 0x0

    .line 305
    move-object v0, p0

    move v1, p1

    move v2, p2

    move v4, v3

    move-object v5, p3

    move-object v6, p4

    move-object v7, p5

    move-object v8, p6

    invoke-direct/range {v0 .. v8}, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve$F2m;-><init>(IIIILjava/math/BigInteger;Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/math/BigInteger;)V

    .line 306
    return-void
.end method

.method private solveQuadradicEquation(Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;)Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;
    .locals 14
    .param p1, "beta"    # Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    .prologue
    .line 521
    new-instance v0, Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement$F2m;

    iget v1, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve$F2m;->m:I

    iget v2, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve$F2m;->k1:I

    iget v3, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve$F2m;->k2:I

    iget v4, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve$F2m;->k3:I

    sget-object v5, Lcom/android/sec/org/bouncycastle/math/ec/ECConstants;->ZERO:Ljava/math/BigInteger;

    invoke-direct/range {v0 .. v5}, Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement$F2m;-><init>(IIIILjava/math/BigInteger;)V

    .line 524
    .local v0, "zeroElement":Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;
    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;->toBigInteger()Ljava/math/BigInteger;

    move-result-object v2

    sget-object v3, Lcom/android/sec/org/bouncycastle/math/ec/ECConstants;->ZERO:Ljava/math/BigInteger;

    invoke-virtual {v2, v3}, Ljava/math/BigInteger;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 553
    .end local v0    # "zeroElement":Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;
    :goto_0
    return-object v0

    .line 529
    .restart local v0    # "zeroElement":Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;
    :cond_0
    const/4 v12, 0x0

    .line 530
    .local v12, "z":Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;
    move-object v7, v0

    .line 532
    .local v7, "gamma":Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;
    new-instance v9, Ljava/util/Random;

    invoke-direct {v9}, Ljava/util/Random;-><init>()V

    .line 535
    .local v9, "rand":Ljava/util/Random;
    :cond_1
    new-instance v1, Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement$F2m;

    iget v2, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve$F2m;->m:I

    iget v3, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve$F2m;->k1:I

    iget v4, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve$F2m;->k2:I

    iget v5, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve$F2m;->k3:I

    new-instance v6, Ljava/math/BigInteger;

    iget v13, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve$F2m;->m:I

    invoke-direct {v6, v13, v9}, Ljava/math/BigInteger;-><init>(ILjava/util/Random;)V

    invoke-direct/range {v1 .. v6}, Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement$F2m;-><init>(IIIILjava/math/BigInteger;)V

    .line 537
    .local v1, "t":Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;
    move-object v12, v0

    .line 538
    move-object v10, p1

    .line 539
    .local v10, "w":Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;
    const/4 v8, 0x1

    .local v8, "i":I
    :goto_1
    iget v2, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve$F2m;->m:I

    add-int/lit8 v2, v2, -0x1

    if-gt v8, v2, :cond_2

    .line 541
    invoke-virtual {v10}, Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;->square()Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    move-result-object v11

    .line 542
    .local v11, "w2":Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;
    invoke-virtual {v12}, Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;->square()Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    move-result-object v2

    invoke-virtual {v11, v1}, Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;->multiply(Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;)Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;->add(Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;)Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    move-result-object v12

    .line 543
    invoke-virtual {v11, p1}, Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;->add(Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;)Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    move-result-object v10

    .line 539
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    .line 545
    .end local v11    # "w2":Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;
    :cond_2
    invoke-virtual {v10}, Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;->toBigInteger()Ljava/math/BigInteger;

    move-result-object v2

    sget-object v3, Lcom/android/sec/org/bouncycastle/math/ec/ECConstants;->ZERO:Ljava/math/BigInteger;

    invoke-virtual {v2, v3}, Ljava/math/BigInteger;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 547
    const/4 v0, 0x0

    goto :goto_0

    .line 549
    :cond_3
    invoke-virtual {v12}, Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;->square()Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    move-result-object v2

    invoke-virtual {v2, v12}, Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;->add(Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;)Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    move-result-object v7

    .line 551
    invoke-virtual {v7}, Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;->toBigInteger()Ljava/math/BigInteger;

    move-result-object v2

    sget-object v3, Lcom/android/sec/org/bouncycastle/math/ec/ECConstants;->ZERO:Ljava/math/BigInteger;

    invoke-virtual {v2, v3}, Ljava/math/BigInteger;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    move-object v0, v12

    .line 553
    goto :goto_0
.end method


# virtual methods
.method public createPoint(Ljava/math/BigInteger;Ljava/math/BigInteger;Z)Lcom/android/sec/org/bouncycastle/math/ec/ECPoint;
    .locals 3
    .param p1, "x"    # Ljava/math/BigInteger;
    .param p2, "y"    # Ljava/math/BigInteger;
    .param p3, "withCompression"    # Z

    .prologue
    .line 421
    new-instance v0, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$F2m;

    invoke-virtual {p0, p1}, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve$F2m;->fromBigInteger(Ljava/math/BigInteger;)Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    move-result-object v1

    invoke-virtual {p0, p2}, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve$F2m;->fromBigInteger(Ljava/math/BigInteger;)Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    move-result-object v2

    invoke-direct {v0, p0, v1, v2, p3}, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$F2m;-><init>(Lcom/android/sec/org/bouncycastle/math/ec/ECCurve;Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;Z)V

    return-object v0
.end method

.method protected decompressPoint(ILjava/math/BigInteger;)Lcom/android/sec/org/bouncycastle/math/ec/ECPoint;
    .locals 10
    .param p1, "yTilde"    # I
    .param p2, "X1"    # Ljava/math/BigInteger;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 481
    invoke-virtual {p0, p2}, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve$F2m;->fromBigInteger(Ljava/math/BigInteger;)Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    move-result-object v2

    .line 482
    .local v2, "xp":Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;
    const/4 v3, 0x0

    .line 483
    .local v3, "yp":Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;
    invoke-virtual {v2}, Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;->toBigInteger()Ljava/math/BigInteger;

    move-result-object v7

    sget-object v8, Lcom/android/sec/org/bouncycastle/math/ec/ECConstants;->ZERO:Ljava/math/BigInteger;

    invoke-virtual {v7, v8}, Ljava/math/BigInteger;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 485
    iget-object v3, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve$F2m;->b:Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    .end local v3    # "yp":Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;
    check-cast v3, Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement$F2m;

    .line 486
    .restart local v3    # "yp":Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget v7, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve$F2m;->m:I

    add-int/lit8 v7, v7, -0x1

    if-ge v1, v7, :cond_4

    .line 488
    invoke-virtual {v3}, Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;->square()Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    move-result-object v3

    .line 486
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 493
    .end local v1    # "i":I
    :cond_0
    iget-object v7, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve$F2m;->a:Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    invoke-virtual {v2, v7}, Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;->add(Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;)Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    move-result-object v7

    iget-object v8, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve$F2m;->b:Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    invoke-virtual {v2}, Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;->square()Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    move-result-object v9

    invoke-virtual {v9}, Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;->invert()Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;->multiply(Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;)Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;->add(Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;)Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    move-result-object v0

    .line 494
    .local v0, "beta":Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;
    invoke-direct {p0, v0}, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve$F2m;->solveQuadradicEquation(Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;)Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    move-result-object v4

    .line 495
    .local v4, "z":Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;
    if-nez v4, :cond_1

    .line 497
    new-instance v6, Ljava/lang/IllegalArgumentException;

    const-string v7, "Invalid point compression"

    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 499
    :cond_1
    invoke-virtual {v4}, Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;->toBigInteger()Ljava/math/BigInteger;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/math/BigInteger;->testBit(I)Z

    move-result v7

    if-eqz v7, :cond_2

    move v5, v6

    .line 500
    .local v5, "zBit":I
    :cond_2
    if-eq v5, p1, :cond_3

    .line 502
    sget-object v7, Lcom/android/sec/org/bouncycastle/math/ec/ECConstants;->ONE:Ljava/math/BigInteger;

    invoke-virtual {p0, v7}, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve$F2m;->fromBigInteger(Ljava/math/BigInteger;)Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    move-result-object v7

    invoke-virtual {v4, v7}, Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;->add(Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;)Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    move-result-object v4

    .line 504
    :cond_3
    invoke-virtual {v2, v4}, Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;->multiply(Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;)Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    move-result-object v3

    .line 507
    .end local v0    # "beta":Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;
    .end local v4    # "z":Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;
    .end local v5    # "zBit":I
    :cond_4
    new-instance v7, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$F2m;

    invoke-direct {v7, p0, v2, v3, v6}, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$F2m;-><init>(Lcom/android/sec/org/bouncycastle/math/ec/ECCurve;Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;Z)V

    return-object v7
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "anObject"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 559
    if-ne p1, p0, :cond_1

    .line 571
    :cond_0
    :goto_0
    return v1

    .line 564
    :cond_1
    instance-of v3, p1, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve$F2m;

    if-nez v3, :cond_2

    move v1, v2

    .line 566
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 569
    check-cast v0, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve$F2m;

    .line 571
    .local v0, "other":Lcom/android/sec/org/bouncycastle/math/ec/ECCurve$F2m;
    iget v3, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve$F2m;->m:I

    iget v4, v0, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve$F2m;->m:I

    if-ne v3, v4, :cond_3

    iget v3, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve$F2m;->k1:I

    iget v4, v0, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve$F2m;->k1:I

    if-ne v3, v4, :cond_3

    iget v3, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve$F2m;->k2:I

    iget v4, v0, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve$F2m;->k2:I

    if-ne v3, v4, :cond_3

    iget v3, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve$F2m;->k3:I

    iget v4, v0, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve$F2m;->k3:I

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve$F2m;->a:Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    iget-object v4, v0, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve$F2m;->a:Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve$F2m;->b:Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    iget-object v4, v0, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve$F2m;->b:Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public fromBigInteger(Ljava/math/BigInteger;)Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;
    .locals 6
    .param p1, "x"    # Ljava/math/BigInteger;

    .prologue
    .line 416
    new-instance v0, Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement$F2m;

    iget v1, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve$F2m;->m:I

    iget v2, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve$F2m;->k1:I

    iget v3, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve$F2m;->k2:I

    iget v4, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve$F2m;->k3:I

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement$F2m;-><init>(IIIILjava/math/BigInteger;)V

    return-object v0
.end method

.method public getFieldSize()I
    .locals 1

    .prologue
    .line 411
    iget v0, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve$F2m;->m:I

    return v0
.end method

.method public getH()Ljava/math/BigInteger;
    .locals 1

    .prologue
    .line 618
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve$F2m;->h:Ljava/math/BigInteger;

    return-object v0
.end method

.method public getInfinity()Lcom/android/sec/org/bouncycastle/math/ec/ECPoint;
    .locals 1

    .prologue
    .line 426
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve$F2m;->infinity:Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$F2m;

    return-object v0
.end method

.method public getK1()I
    .locals 1

    .prologue
    .line 598
    iget v0, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve$F2m;->k1:I

    return v0
.end method

.method public getK2()I
    .locals 1

    .prologue
    .line 603
    iget v0, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve$F2m;->k2:I

    return v0
.end method

.method public getK3()I
    .locals 1

    .prologue
    .line 608
    iget v0, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve$F2m;->k3:I

    return v0
.end method

.method public getM()I
    .locals 1

    .prologue
    .line 583
    iget v0, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve$F2m;->m:I

    return v0
.end method

.method declared-synchronized getMu()B
    .locals 1

    .prologue
    .line 449
    monitor-enter p0

    :try_start_0
    iget-byte v0, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve$F2m;->mu:B

    if-nez v0, :cond_0

    .line 451
    invoke-static {p0}, Lcom/android/sec/org/bouncycastle/math/ec/Tnaf;->getMu(Lcom/android/sec/org/bouncycastle/math/ec/ECCurve$F2m;)B

    move-result v0

    iput-byte v0, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve$F2m;->mu:B

    .line 453
    :cond_0
    iget-byte v0, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve$F2m;->mu:B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    .line 449
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getN()Ljava/math/BigInteger;
    .locals 1

    .prologue
    .line 613
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve$F2m;->n:Ljava/math/BigInteger;

    return-object v0
.end method

.method declared-synchronized getSi()[Ljava/math/BigInteger;
    .locals 1

    .prologue
    .line 463
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve$F2m;->si:[Ljava/math/BigInteger;

    if-nez v0, :cond_0

    .line 465
    invoke-static {p0}, Lcom/android/sec/org/bouncycastle/math/ec/Tnaf;->getSi(Lcom/android/sec/org/bouncycastle/math/ec/ECCurve$F2m;)[Ljava/math/BigInteger;

    move-result-object v0

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve$F2m;->si:[Ljava/math/BigInteger;

    .line 467
    :cond_0
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve$F2m;->si:[Ljava/math/BigInteger;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 463
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 578
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve$F2m;->a:Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve$F2m;->b:Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    iget v1, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve$F2m;->m:I

    xor-int/2addr v0, v1

    iget v1, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve$F2m;->k1:I

    xor-int/2addr v0, v1

    iget v1, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve$F2m;->k2:I

    xor-int/2addr v0, v1

    iget v1, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve$F2m;->k3:I

    xor-int/2addr v0, v1

    return v0
.end method

.method public isKoblitz()Z
    .locals 2

    .prologue
    .line 435
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve$F2m;->n:Ljava/math/BigInteger;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve$F2m;->h:Ljava/math/BigInteger;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve$F2m;->a:Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;->toBigInteger()Ljava/math/BigInteger;

    move-result-object v0

    sget-object v1, Lcom/android/sec/org/bouncycastle/math/ec/ECConstants;->ZERO:Ljava/math/BigInteger;

    invoke-virtual {v0, v1}, Ljava/math/BigInteger;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve$F2m;->a:Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;->toBigInteger()Ljava/math/BigInteger;

    move-result-object v0

    sget-object v1, Lcom/android/sec/org/bouncycastle/math/ec/ECConstants;->ONE:Ljava/math/BigInteger;

    invoke-virtual {v0, v1}, Ljava/math/BigInteger;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve$F2m;->b:Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;->toBigInteger()Ljava/math/BigInteger;

    move-result-object v0

    sget-object v1, Lcom/android/sec/org/bouncycastle/math/ec/ECConstants;->ONE:Ljava/math/BigInteger;

    invoke-virtual {v0, v1}, Ljava/math/BigInteger;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isTrinomial()Z
    .locals 1

    .prologue
    .line 593
    iget v0, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve$F2m;->k2:I

    if-nez v0, :cond_0

    iget v0, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve$F2m;->k3:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
