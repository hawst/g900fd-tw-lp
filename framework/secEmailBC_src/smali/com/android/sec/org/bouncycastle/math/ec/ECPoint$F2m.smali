.class public Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$F2m;
.super Lcom/android/sec/org/bouncycastle/math/ec/ECPoint;
.source "ECPoint.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/sec/org/bouncycastle/math/ec/ECPoint;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "F2m"
.end annotation


# direct methods
.method public constructor <init>(Lcom/android/sec/org/bouncycastle/math/ec/ECCurve;Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;)V
    .locals 1
    .param p1, "curve"    # Lcom/android/sec/org/bouncycastle/math/ec/ECCurve;
    .param p2, "x"    # Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;
    .param p3, "y"    # Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    .prologue
    .line 346
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$F2m;-><init>(Lcom/android/sec/org/bouncycastle/math/ec/ECCurve;Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;Z)V

    .line 347
    return-void
.end method

.method public constructor <init>(Lcom/android/sec/org/bouncycastle/math/ec/ECCurve;Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;Z)V
    .locals 2
    .param p1, "curve"    # Lcom/android/sec/org/bouncycastle/math/ec/ECCurve;
    .param p2, "x"    # Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;
    .param p3, "y"    # Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;
    .param p4, "withCompression"    # Z

    .prologue
    .line 357
    invoke-direct {p0, p1, p2, p3}, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint;-><init>(Lcom/android/sec/org/bouncycastle/math/ec/ECCurve;Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;)V

    .line 359
    if-eqz p2, :cond_0

    if-eqz p3, :cond_1

    :cond_0
    if-nez p2, :cond_2

    if-eqz p3, :cond_2

    .line 361
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Exactly one of the field elements is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 364
    :cond_2
    if-eqz p2, :cond_3

    .line 367
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$F2m;->x:Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$F2m;->y:Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    invoke-static {v0, v1}, Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement$F2m;->checkFieldElements(Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;)V

    .line 370
    if-eqz p1, :cond_3

    .line 372
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$F2m;->x:Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$F2m;->curve:Lcom/android/sec/org/bouncycastle/math/ec/ECCurve;

    invoke-virtual {v1}, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve;->getA()Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement$F2m;->checkFieldElements(Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;)V

    .line 376
    :cond_3
    iput-boolean p4, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$F2m;->withCompression:Z

    .line 377
    return-void
.end method

.method private static checkPoints(Lcom/android/sec/org/bouncycastle/math/ec/ECPoint;Lcom/android/sec/org/bouncycastle/math/ec/ECPoint;)V
    .locals 2
    .param p0, "a"    # Lcom/android/sec/org/bouncycastle/math/ec/ECPoint;
    .param p1, "b"    # Lcom/android/sec/org/bouncycastle/math/ec/ECPoint;

    .prologue
    .line 440
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint;->curve:Lcom/android/sec/org/bouncycastle/math/ec/ECCurve;

    iget-object v1, p1, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint;->curve:Lcom/android/sec/org/bouncycastle/math/ec/ECCurve;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 442
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Only points on the same curve can be added or subtracted"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 447
    :cond_0
    return-void
.end method


# virtual methods
.method public add(Lcom/android/sec/org/bouncycastle/math/ec/ECPoint;)Lcom/android/sec/org/bouncycastle/math/ec/ECPoint;
    .locals 1
    .param p1, "b"    # Lcom/android/sec/org/bouncycastle/math/ec/ECPoint;

    .prologue
    .line 454
    invoke-static {p0, p1}, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$F2m;->checkPoints(Lcom/android/sec/org/bouncycastle/math/ec/ECPoint;Lcom/android/sec/org/bouncycastle/math/ec/ECPoint;)V

    .line 455
    check-cast p1, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$F2m;

    .end local p1    # "b":Lcom/android/sec/org/bouncycastle/math/ec/ECPoint;
    invoke-virtual {p0, p1}, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$F2m;->addSimple(Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$F2m;)Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$F2m;

    move-result-object v0

    return-object v0
.end method

.method public addSimple(Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$F2m;)Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$F2m;
    .locals 9
    .param p1, "b"    # Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$F2m;

    .prologue
    .line 469
    move-object v1, p1

    .line 470
    .local v1, "other":Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$F2m;
    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$F2m;->isInfinity()Z

    move-result v6

    if-eqz v6, :cond_0

    move-object v6, v1

    .line 505
    :goto_0
    return-object v6

    .line 475
    :cond_0
    invoke-virtual {v1}, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$F2m;->isInfinity()Z

    move-result v6

    if-eqz v6, :cond_1

    move-object v6, p0

    .line 477
    goto :goto_0

    .line 480
    :cond_1
    invoke-virtual {v1}, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$F2m;->getX()Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    move-result-object v2

    check-cast v2, Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement$F2m;

    .line 481
    .local v2, "x2":Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement$F2m;
    invoke-virtual {v1}, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$F2m;->getY()Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    move-result-object v4

    check-cast v4, Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement$F2m;

    .line 484
    .local v4, "y2":Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement$F2m;
    iget-object v6, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$F2m;->x:Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    invoke-virtual {v6, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 486
    iget-object v6, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$F2m;->y:Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    invoke-virtual {v6, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 489
    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$F2m;->twice()Lcom/android/sec/org/bouncycastle/math/ec/ECPoint;

    move-result-object v6

    check-cast v6, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$F2m;

    goto :goto_0

    .line 493
    :cond_2
    iget-object v6, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$F2m;->curve:Lcom/android/sec/org/bouncycastle/math/ec/ECCurve;

    invoke-virtual {v6}, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve;->getInfinity()Lcom/android/sec/org/bouncycastle/math/ec/ECPoint;

    move-result-object v6

    check-cast v6, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$F2m;

    goto :goto_0

    .line 496
    :cond_3
    iget-object v6, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$F2m;->y:Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    invoke-virtual {v6, v4}, Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;->add(Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;)Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    move-result-object v6

    iget-object v7, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$F2m;->x:Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    invoke-virtual {v7, v2}, Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;->add(Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;)Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;->divide(Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;)Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    move-result-object v0

    check-cast v0, Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement$F2m;

    .line 499
    .local v0, "lambda":Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement$F2m;
    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement$F2m;->square()Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    move-result-object v6

    invoke-virtual {v6, v0}, Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;->add(Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;)Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    move-result-object v6

    iget-object v7, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$F2m;->x:Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    invoke-virtual {v6, v7}, Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;->add(Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;)Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    move-result-object v6

    invoke-virtual {v6, v2}, Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;->add(Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;)Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    move-result-object v6

    iget-object v7, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$F2m;->curve:Lcom/android/sec/org/bouncycastle/math/ec/ECCurve;

    invoke-virtual {v7}, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve;->getA()Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;->add(Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;)Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    move-result-object v3

    check-cast v3, Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement$F2m;

    .line 502
    .local v3, "x3":Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement$F2m;
    iget-object v6, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$F2m;->x:Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    invoke-virtual {v6, v3}, Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;->add(Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;)Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    move-result-object v6

    invoke-virtual {v0, v6}, Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement$F2m;->multiply(Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;)Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    move-result-object v6

    invoke-virtual {v6, v3}, Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;->add(Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;)Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    move-result-object v6

    iget-object v7, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$F2m;->y:Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    invoke-virtual {v6, v7}, Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;->add(Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;)Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    move-result-object v5

    check-cast v5, Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement$F2m;

    .line 505
    .local v5, "y3":Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement$F2m;
    new-instance v6, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$F2m;

    iget-object v7, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$F2m;->curve:Lcom/android/sec/org/bouncycastle/math/ec/ECCurve;

    iget-boolean v8, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$F2m;->withCompression:Z

    invoke-direct {v6, v7, v3, v5, v8}, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$F2m;-><init>(Lcom/android/sec/org/bouncycastle/math/ec/ECCurve;Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;Z)V

    goto/16 :goto_0
.end method

.method declared-synchronized assertECMultiplier()V
    .locals 1

    .prologue
    .line 580
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$F2m;->multiplier:Lcom/android/sec/org/bouncycastle/math/ec/ECMultiplier;

    if-nez v0, :cond_0

    .line 582
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$F2m;->curve:Lcom/android/sec/org/bouncycastle/math/ec/ECCurve;

    check-cast v0, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve$F2m;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve$F2m;->isKoblitz()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 584
    new-instance v0, Lcom/android/sec/org/bouncycastle/math/ec/WTauNafMultiplier;

    invoke-direct {v0}, Lcom/android/sec/org/bouncycastle/math/ec/WTauNafMultiplier;-><init>()V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$F2m;->multiplier:Lcom/android/sec/org/bouncycastle/math/ec/ECMultiplier;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 591
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 588
    :cond_1
    :try_start_1
    new-instance v0, Lcom/android/sec/org/bouncycastle/math/ec/WNafMultiplier;

    invoke-direct {v0}, Lcom/android/sec/org/bouncycastle/math/ec/WNafMultiplier;-><init>()V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$F2m;->multiplier:Lcom/android/sec/org/bouncycastle/math/ec/ECMultiplier;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 580
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getEncoded(Z)[B
    .locals 8
    .param p1, "compressed"    # Z

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 384
    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$F2m;->isInfinity()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 386
    new-array v0, v7, [B

    .line 427
    :goto_0
    return-object v0

    .line 389
    :cond_0
    # getter for: Lcom/android/sec/org/bouncycastle/math/ec/ECPoint;->converter:Lcom/android/sec/org/bouncycastle/asn1/x9/X9IntegerConverter;
    invoke-static {}, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint;->access$000()Lcom/android/sec/org/bouncycastle/asn1/x9/X9IntegerConverter;

    move-result-object v4

    iget-object v5, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$F2m;->x:Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    invoke-virtual {v4, v5}, Lcom/android/sec/org/bouncycastle/asn1/x9/X9IntegerConverter;->getByteLength(Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;)I

    move-result v3

    .line 390
    .local v3, "byteCount":I
    # getter for: Lcom/android/sec/org/bouncycastle/math/ec/ECPoint;->converter:Lcom/android/sec/org/bouncycastle/asn1/x9/X9IntegerConverter;
    invoke-static {}, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint;->access$000()Lcom/android/sec/org/bouncycastle/asn1/x9/X9IntegerConverter;

    move-result-object v4

    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$F2m;->getX()Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;->toBigInteger()Ljava/math/BigInteger;

    move-result-object v5

    invoke-virtual {v4, v5, v3}, Lcom/android/sec/org/bouncycastle/asn1/x9/X9IntegerConverter;->integerToBytes(Ljava/math/BigInteger;I)[B

    move-result-object v1

    .line 393
    .local v1, "X":[B
    if-eqz p1, :cond_2

    .line 396
    add-int/lit8 v4, v3, 0x1

    new-array v0, v4, [B

    .line 398
    .local v0, "PO":[B
    const/4 v4, 0x2

    aput-byte v4, v0, v6

    .line 404
    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$F2m;->getX()Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;->toBigInteger()Ljava/math/BigInteger;

    move-result-object v4

    sget-object v5, Lcom/android/sec/org/bouncycastle/math/ec/ECConstants;->ZERO:Ljava/math/BigInteger;

    invoke-virtual {v4, v5}, Ljava/math/BigInteger;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 406
    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$F2m;->getY()Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    move-result-object v4

    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$F2m;->getX()Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;->invert()Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;->multiply(Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;)Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;->toBigInteger()Ljava/math/BigInteger;

    move-result-object v4

    invoke-virtual {v4, v6}, Ljava/math/BigInteger;->testBit(I)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 410
    const/4 v4, 0x3

    aput-byte v4, v0, v6

    .line 414
    :cond_1
    invoke-static {v1, v6, v0, v7, v3}, Ljava/lang/System;->arraycopy([BI[BII)V

    goto :goto_0

    .line 418
    .end local v0    # "PO":[B
    :cond_2
    # getter for: Lcom/android/sec/org/bouncycastle/math/ec/ECPoint;->converter:Lcom/android/sec/org/bouncycastle/asn1/x9/X9IntegerConverter;
    invoke-static {}, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint;->access$000()Lcom/android/sec/org/bouncycastle/asn1/x9/X9IntegerConverter;

    move-result-object v4

    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$F2m;->getY()Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;->toBigInteger()Ljava/math/BigInteger;

    move-result-object v5

    invoke-virtual {v4, v5, v3}, Lcom/android/sec/org/bouncycastle/asn1/x9/X9IntegerConverter;->integerToBytes(Ljava/math/BigInteger;I)[B

    move-result-object v2

    .line 420
    .local v2, "Y":[B
    add-int v4, v3, v3

    add-int/lit8 v4, v4, 0x1

    new-array v0, v4, [B

    .line 422
    .restart local v0    # "PO":[B
    const/4 v4, 0x4

    aput-byte v4, v0, v6

    .line 423
    invoke-static {v1, v6, v0, v7, v3}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 424
    add-int/lit8 v4, v3, 0x1

    invoke-static {v2, v6, v0, v4, v3}, Ljava/lang/System;->arraycopy([BI[BII)V

    goto :goto_0
.end method

.method public negate()Lcom/android/sec/org/bouncycastle/math/ec/ECPoint;
    .locals 5

    .prologue
    .line 572
    new-instance v0, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$F2m;

    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$F2m;->curve:Lcom/android/sec/org/bouncycastle/math/ec/ECCurve;

    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$F2m;->getX()Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    move-result-object v2

    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$F2m;->getY()Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    move-result-object v3

    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$F2m;->getX()Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;->add(Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;)Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    move-result-object v3

    iget-boolean v4, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$F2m;->withCompression:Z

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$F2m;-><init>(Lcom/android/sec/org/bouncycastle/math/ec/ECCurve;Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;Z)V

    return-object v0
.end method

.method public subtract(Lcom/android/sec/org/bouncycastle/math/ec/ECPoint;)Lcom/android/sec/org/bouncycastle/math/ec/ECPoint;
    .locals 1
    .param p1, "b"    # Lcom/android/sec/org/bouncycastle/math/ec/ECPoint;

    .prologue
    .line 513
    invoke-static {p0, p1}, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$F2m;->checkPoints(Lcom/android/sec/org/bouncycastle/math/ec/ECPoint;Lcom/android/sec/org/bouncycastle/math/ec/ECPoint;)V

    .line 514
    check-cast p1, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$F2m;

    .end local p1    # "b":Lcom/android/sec/org/bouncycastle/math/ec/ECPoint;
    invoke-virtual {p0, p1}, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$F2m;->subtractSimple(Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$F2m;)Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$F2m;

    move-result-object v0

    return-object v0
.end method

.method public subtractSimple(Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$F2m;)Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$F2m;
    .locals 1
    .param p1, "b"    # Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$F2m;

    .prologue
    .line 528
    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$F2m;->isInfinity()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 534
    .end local p0    # "this":Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$F2m;
    :goto_0
    return-object p0

    .restart local p0    # "this":Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$F2m;
    :cond_0
    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$F2m;->negate()Lcom/android/sec/org/bouncycastle/math/ec/ECPoint;

    move-result-object v0

    check-cast v0, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$F2m;

    invoke-virtual {p0, v0}, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$F2m;->addSimple(Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$F2m;)Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$F2m;

    move-result-object p0

    goto :goto_0
.end method

.method public twice()Lcom/android/sec/org/bouncycastle/math/ec/ECPoint;
    .locals 7

    .prologue
    .line 542
    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$F2m;->isInfinity()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 567
    .end local p0    # "this":Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$F2m;
    .local v0, "ONE":Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;
    .local v1, "lambda":Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement$F2m;
    .local v2, "x3":Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement$F2m;
    .local v3, "y3":Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement$F2m;
    :goto_0
    return-object p0

    .line 548
    .end local v0    # "ONE":Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;
    .end local v1    # "lambda":Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement$F2m;
    .end local v2    # "x3":Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement$F2m;
    .end local v3    # "y3":Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement$F2m;
    .restart local p0    # "this":Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$F2m;
    :cond_0
    iget-object v4, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$F2m;->x:Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    invoke-virtual {v4}, Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;->toBigInteger()Ljava/math/BigInteger;

    move-result-object v4

    invoke-virtual {v4}, Ljava/math/BigInteger;->signum()I

    move-result v4

    if-nez v4, :cond_1

    .line 552
    iget-object v4, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$F2m;->curve:Lcom/android/sec/org/bouncycastle/math/ec/ECCurve;

    invoke-virtual {v4}, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve;->getInfinity()Lcom/android/sec/org/bouncycastle/math/ec/ECPoint;

    move-result-object p0

    goto :goto_0

    .line 555
    :cond_1
    iget-object v4, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$F2m;->x:Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    iget-object v5, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$F2m;->y:Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    iget-object v6, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$F2m;->x:Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    invoke-virtual {v5, v6}, Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;->divide(Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;)Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;->add(Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;)Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    move-result-object v1

    check-cast v1, Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement$F2m;

    .line 558
    .restart local v1    # "lambda":Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement$F2m;
    invoke-virtual {v1}, Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement$F2m;->square()Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    move-result-object v4

    invoke-virtual {v4, v1}, Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;->add(Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;)Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    move-result-object v4

    iget-object v5, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$F2m;->curve:Lcom/android/sec/org/bouncycastle/math/ec/ECCurve;

    invoke-virtual {v5}, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve;->getA()Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;->add(Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;)Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    move-result-object v2

    check-cast v2, Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement$F2m;

    .line 562
    .restart local v2    # "x3":Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement$F2m;
    iget-object v4, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$F2m;->curve:Lcom/android/sec/org/bouncycastle/math/ec/ECCurve;

    sget-object v5, Lcom/android/sec/org/bouncycastle/math/ec/ECConstants;->ONE:Ljava/math/BigInteger;

    invoke-virtual {v4, v5}, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve;->fromBigInteger(Ljava/math/BigInteger;)Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    move-result-object v0

    .line 563
    .restart local v0    # "ONE":Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;
    iget-object v4, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$F2m;->x:Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    invoke-virtual {v4}, Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;->square()Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    move-result-object v4

    invoke-virtual {v1, v0}, Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement$F2m;->add(Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;)Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    move-result-object v5

    invoke-virtual {v2, v5}, Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement$F2m;->multiply(Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;)Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;->add(Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;)Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    move-result-object v3

    check-cast v3, Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement$F2m;

    .line 567
    .restart local v3    # "y3":Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement$F2m;
    new-instance v4, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$F2m;

    iget-object v5, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$F2m;->curve:Lcom/android/sec/org/bouncycastle/math/ec/ECCurve;

    iget-boolean v6, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$F2m;->withCompression:Z

    invoke-direct {v4, v5, v2, v3, v6}, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$F2m;-><init>(Lcom/android/sec/org/bouncycastle/math/ec/ECCurve;Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;Z)V

    move-object p0, v4

    goto :goto_0
.end method
