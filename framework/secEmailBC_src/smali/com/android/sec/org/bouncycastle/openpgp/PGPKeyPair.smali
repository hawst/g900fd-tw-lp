.class public Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyPair;
.super Ljava/lang/Object;
.source "PGPKeyPair.java"


# instance fields
.field priv:Lcom/android/sec/org/bouncycastle/openpgp/PGPPrivateKey;

.field pub:Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;


# direct methods
.method public constructor <init>(ILjava/security/KeyPair;Ljava/util/Date;)V
    .locals 2
    .param p1, "algorithm"    # I
    .param p2, "keyPair"    # Ljava/security/KeyPair;
    .param p3, "time"    # Ljava/util/Date;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
        }
    .end annotation

    .prologue
    .line 43
    invoke-virtual {p2}, Ljava/security/KeyPair;->getPublic()Ljava/security/PublicKey;

    move-result-object v0

    invoke-virtual {p2}, Ljava/security/KeyPair;->getPrivate()Ljava/security/PrivateKey;

    move-result-object v1

    invoke-direct {p0, p1, v0, v1, p3}, Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyPair;-><init>(ILjava/security/PublicKey;Ljava/security/PrivateKey;Ljava/util/Date;)V

    .line 44
    return-void
.end method

.method public constructor <init>(ILjava/security/KeyPair;Ljava/util/Date;Ljava/lang/String;)V
    .locals 6
    .param p1, "algorithm"    # I
    .param p2, "keyPair"    # Ljava/security/KeyPair;
    .param p3, "time"    # Ljava/util/Date;
    .param p4, "provider"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;,
            Ljava/security/NoSuchProviderException;
        }
    .end annotation

    .prologue
    .line 34
    invoke-virtual {p2}, Ljava/security/KeyPair;->getPublic()Ljava/security/PublicKey;

    move-result-object v2

    invoke-virtual {p2}, Ljava/security/KeyPair;->getPrivate()Ljava/security/PrivateKey;

    move-result-object v3

    move-object v0, p0

    move v1, p1

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyPair;-><init>(ILjava/security/PublicKey;Ljava/security/PrivateKey;Ljava/util/Date;Ljava/lang/String;)V

    .line 35
    return-void
.end method

.method public constructor <init>(ILjava/security/PublicKey;Ljava/security/PrivateKey;Ljava/util/Date;)V
    .locals 4
    .param p1, "algorithm"    # I
    .param p2, "pubKey"    # Ljava/security/PublicKey;
    .param p3, "privKey"    # Ljava/security/PrivateKey;
    .param p4, "time"    # Ljava/util/Date;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
        }
    .end annotation

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    new-instance v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;

    invoke-direct {v0, p1, p2, p4}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;-><init>(ILjava/security/PublicKey;Ljava/util/Date;)V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyPair;->pub:Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;

    .line 68
    new-instance v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPrivateKey;

    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyPair;->pub:Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;

    invoke-virtual {v1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->getKeyID()J

    move-result-wide v2

    invoke-direct {v0, p3, v2, v3}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPrivateKey;-><init>(Ljava/security/PrivateKey;J)V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyPair;->priv:Lcom/android/sec/org/bouncycastle/openpgp/PGPPrivateKey;

    .line 69
    return-void
.end method

.method public constructor <init>(ILjava/security/PublicKey;Ljava/security/PrivateKey;Ljava/util/Date;Ljava/lang/String;)V
    .locals 0
    .param p1, "algorithm"    # I
    .param p2, "pubKey"    # Ljava/security/PublicKey;
    .param p3, "privKey"    # Ljava/security/PrivateKey;
    .param p4, "time"    # Ljava/util/Date;
    .param p5, "provider"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;,
            Ljava/security/NoSuchProviderException;
        }
    .end annotation

    .prologue
    .line 57
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyPair;-><init>(ILjava/security/PublicKey;Ljava/security/PrivateKey;Ljava/util/Date;)V

    .line 58
    return-void
.end method

.method public constructor <init>(Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;Lcom/android/sec/org/bouncycastle/openpgp/PGPPrivateKey;)V
    .locals 0
    .param p1, "pub"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;
    .param p2, "priv"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPPrivateKey;

    .prologue
    .line 80
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 81
    iput-object p1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyPair;->pub:Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;

    .line 82
    iput-object p2, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyPair;->priv:Lcom/android/sec/org/bouncycastle/openpgp/PGPPrivateKey;

    .line 83
    return-void
.end method


# virtual methods
.method public getKeyID()J
    .locals 2

    .prologue
    .line 92
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyPair;->pub:Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->getKeyID()J

    move-result-wide v0

    return-wide v0
.end method

.method public getPrivateKey()Lcom/android/sec/org/bouncycastle/openpgp/PGPPrivateKey;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyPair;->priv:Lcom/android/sec/org/bouncycastle/openpgp/PGPPrivateKey;

    return-object v0
.end method

.method public getPublicKey()Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyPair;->pub:Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;

    return-object v0
.end method
