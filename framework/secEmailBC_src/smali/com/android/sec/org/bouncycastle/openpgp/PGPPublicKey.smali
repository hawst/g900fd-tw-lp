.class public Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;
.super Ljava/lang/Object;
.source "PGPPublicKey.java"

# interfaces
.implements Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyAlgorithmTags;


# static fields
.field private static final MASTER_KEY_CERTIFICATION_TYPES:[I


# instance fields
.field private fingerprint:[B

.field idSigs:Ljava/util/List;

.field idTrusts:Ljava/util/List;

.field ids:Ljava/util/List;

.field private keyID:J

.field keySigs:Ljava/util/List;

.field private keyStrength:I

.field publicPk:Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;

.field subSigs:Ljava/util/List;

.field trustPk:Lcom/android/sec/org/bouncycastle/bcpg/TrustPacket;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 47
    const/4 v0, 0x4

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->MASTER_KEY_CERTIFICATION_TYPES:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x13
        0x12
        0x11
        0x10
    .end array-data
.end method

.method public constructor <init>(ILjava/security/PublicKey;Ljava/util/Date;)V
    .locals 11
    .param p1, "algorithm"    # I
    .param p2, "pubKey"    # Ljava/security/PublicKey;
    .param p3, "time"    # Ljava/util/Date;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
        }
    .end annotation

    .prologue
    .line 164
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    iput-object v7, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->keySigs:Ljava/util/List;

    .line 52
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    iput-object v7, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->ids:Ljava/util/List;

    .line 53
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    iput-object v7, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->idTrusts:Ljava/util/List;

    .line 54
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    iput-object v7, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->idSigs:Ljava/util/List;

    .line 56
    const/4 v7, 0x0

    iput-object v7, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->subSigs:Ljava/util/List;

    .line 167
    instance-of v7, p2, Ljava/security/interfaces/RSAPublicKey;

    if-eqz v7, :cond_0

    move-object v6, p2

    .line 169
    check-cast v6, Ljava/security/interfaces/RSAPublicKey;

    .line 171
    .local v6, "rK":Ljava/security/interfaces/RSAPublicKey;
    new-instance v0, Lcom/android/sec/org/bouncycastle/bcpg/RSAPublicBCPGKey;

    invoke-interface {v6}, Ljava/security/interfaces/RSAPublicKey;->getModulus()Ljava/math/BigInteger;

    move-result-object v7

    invoke-interface {v6}, Ljava/security/interfaces/RSAPublicKey;->getPublicExponent()Ljava/math/BigInteger;

    move-result-object v8

    invoke-direct {v0, v7, v8}, Lcom/android/sec/org/bouncycastle/bcpg/RSAPublicBCPGKey;-><init>(Ljava/math/BigInteger;Ljava/math/BigInteger;)V

    .line 192
    .end local v6    # "rK":Ljava/security/interfaces/RSAPublicKey;
    .local v0, "bcpgKey":Lcom/android/sec/org/bouncycastle/bcpg/BCPGKey;
    :goto_0
    new-instance v7, Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;

    invoke-direct {v7, p1, p3, v0}, Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;-><init>(ILjava/util/Date;Lcom/android/sec/org/bouncycastle/bcpg/BCPGKey;)V

    iput-object v7, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->publicPk:Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;

    .line 193
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    iput-object v7, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->ids:Ljava/util/List;

    .line 194
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    iput-object v7, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->idSigs:Ljava/util/List;

    .line 198
    :try_start_0
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->init()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 204
    return-void

    .line 173
    .end local v0    # "bcpgKey":Lcom/android/sec/org/bouncycastle/bcpg/BCPGKey;
    :cond_0
    instance-of v7, p2, Ljava/security/interfaces/DSAPublicKey;

    if-eqz v7, :cond_1

    move-object v1, p2

    .line 175
    check-cast v1, Ljava/security/interfaces/DSAPublicKey;

    .line 176
    .local v1, "dK":Ljava/security/interfaces/DSAPublicKey;
    invoke-interface {v1}, Ljava/security/interfaces/DSAPublicKey;->getParams()Ljava/security/interfaces/DSAParams;

    move-result-object v2

    .line 178
    .local v2, "dP":Ljava/security/interfaces/DSAParams;
    new-instance v0, Lcom/android/sec/org/bouncycastle/bcpg/DSAPublicBCPGKey;

    invoke-interface {v2}, Ljava/security/interfaces/DSAParams;->getP()Ljava/math/BigInteger;

    move-result-object v7

    invoke-interface {v2}, Ljava/security/interfaces/DSAParams;->getQ()Ljava/math/BigInteger;

    move-result-object v8

    invoke-interface {v2}, Ljava/security/interfaces/DSAParams;->getG()Ljava/math/BigInteger;

    move-result-object v9

    invoke-interface {v1}, Ljava/security/interfaces/DSAPublicKey;->getY()Ljava/math/BigInteger;

    move-result-object v10

    invoke-direct {v0, v7, v8, v9, v10}, Lcom/android/sec/org/bouncycastle/bcpg/DSAPublicBCPGKey;-><init>(Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/math/BigInteger;)V

    .line 179
    .restart local v0    # "bcpgKey":Lcom/android/sec/org/bouncycastle/bcpg/BCPGKey;
    goto :goto_0

    .line 180
    .end local v0    # "bcpgKey":Lcom/android/sec/org/bouncycastle/bcpg/BCPGKey;
    .end local v1    # "dK":Ljava/security/interfaces/DSAPublicKey;
    .end local v2    # "dP":Ljava/security/interfaces/DSAParams;
    :cond_1
    instance-of v7, p2, Lcom/android/sec/org/bouncycastle/jce/interfaces/ElGamalPublicKey;

    if-eqz v7, :cond_2

    move-object v4, p2

    .line 182
    check-cast v4, Lcom/android/sec/org/bouncycastle/jce/interfaces/ElGamalPublicKey;

    .line 183
    .local v4, "eK":Lcom/android/sec/org/bouncycastle/jce/interfaces/ElGamalPublicKey;
    invoke-interface {v4}, Lcom/android/sec/org/bouncycastle/jce/interfaces/ElGamalPublicKey;->getParameters()Lcom/android/sec/org/bouncycastle/jce/spec/ElGamalParameterSpec;

    move-result-object v5

    .line 185
    .local v5, "eS":Lcom/android/sec/org/bouncycastle/jce/spec/ElGamalParameterSpec;
    new-instance v0, Lcom/android/sec/org/bouncycastle/bcpg/ElGamalPublicBCPGKey;

    invoke-virtual {v5}, Lcom/android/sec/org/bouncycastle/jce/spec/ElGamalParameterSpec;->getP()Ljava/math/BigInteger;

    move-result-object v7

    invoke-virtual {v5}, Lcom/android/sec/org/bouncycastle/jce/spec/ElGamalParameterSpec;->getG()Ljava/math/BigInteger;

    move-result-object v8

    invoke-interface {v4}, Lcom/android/sec/org/bouncycastle/jce/interfaces/ElGamalPublicKey;->getY()Ljava/math/BigInteger;

    move-result-object v9

    invoke-direct {v0, v7, v8, v9}, Lcom/android/sec/org/bouncycastle/bcpg/ElGamalPublicBCPGKey;-><init>(Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/math/BigInteger;)V

    .line 186
    .restart local v0    # "bcpgKey":Lcom/android/sec/org/bouncycastle/bcpg/BCPGKey;
    goto :goto_0

    .line 189
    .end local v0    # "bcpgKey":Lcom/android/sec/org/bouncycastle/bcpg/BCPGKey;
    .end local v4    # "eK":Lcom/android/sec/org/bouncycastle/jce/interfaces/ElGamalPublicKey;
    .end local v5    # "eS":Lcom/android/sec/org/bouncycastle/jce/spec/ElGamalParameterSpec;
    :cond_2
    new-instance v7, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;

    const-string v8, "unknown key class"

    invoke-direct {v7, v8}, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 200
    .restart local v0    # "bcpgKey":Lcom/android/sec/org/bouncycastle/bcpg/BCPGKey;
    :catch_0
    move-exception v3

    .line 202
    .local v3, "e":Ljava/io/IOException;
    new-instance v7, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;

    const-string v8, "exception calculating keyID"

    invoke-direct {v7, v8, v3}, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v7
.end method

.method public constructor <init>(ILjava/security/PublicKey;Ljava/util/Date;Ljava/lang/String;)V
    .locals 0
    .param p1, "algorithm"    # I
    .param p2, "pubKey"    # Ljava/security/PublicKey;
    .param p3, "time"    # Ljava/util/Date;
    .param p4, "provider"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;,
            Ljava/security/NoSuchProviderException;
        }
    .end annotation

    .prologue
    .line 156
    invoke-direct {p0, p1, p2, p3}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;-><init>(ILjava/security/PublicKey;Ljava/util/Date;)V

    .line 157
    return-void
.end method

.method constructor <init>(Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;Lcom/android/sec/org/bouncycastle/bcpg/TrustPacket;Ljava/util/List;)V
    .locals 1
    .param p1, "publicPk"    # Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;
    .param p2, "trustPk"    # Lcom/android/sec/org/bouncycastle/bcpg/TrustPacket;
    .param p3, "sigs"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 214
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->keySigs:Ljava/util/List;

    .line 52
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->ids:Ljava/util/List;

    .line 53
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->idTrusts:Ljava/util/List;

    .line 54
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->idSigs:Ljava/util/List;

    .line 56
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->subSigs:Ljava/util/List;

    .line 215
    iput-object p1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->publicPk:Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;

    .line 216
    iput-object p2, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->trustPk:Lcom/android/sec/org/bouncycastle/bcpg/TrustPacket;

    .line 217
    iput-object p3, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->subSigs:Ljava/util/List;

    .line 219
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->init()V

    .line 220
    return-void
.end method

.method constructor <init>(Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;Lcom/android/sec/org/bouncycastle/bcpg/TrustPacket;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
    .locals 1
    .param p1, "publicPk"    # Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;
    .param p2, "trustPk"    # Lcom/android/sec/org/bouncycastle/bcpg/TrustPacket;
    .param p3, "keySigs"    # Ljava/util/List;
    .param p4, "ids"    # Ljava/util/List;
    .param p5, "idTrusts"    # Ljava/util/List;
    .param p6, "idSigs"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 276
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->keySigs:Ljava/util/List;

    .line 52
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->ids:Ljava/util/List;

    .line 53
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->idTrusts:Ljava/util/List;

    .line 54
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->idSigs:Ljava/util/List;

    .line 56
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->subSigs:Ljava/util/List;

    .line 277
    iput-object p1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->publicPk:Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;

    .line 278
    iput-object p2, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->trustPk:Lcom/android/sec/org/bouncycastle/bcpg/TrustPacket;

    .line 279
    iput-object p3, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->keySigs:Ljava/util/List;

    .line 280
    iput-object p4, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->ids:Ljava/util/List;

    .line 281
    iput-object p5, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->idTrusts:Ljava/util/List;

    .line 282
    iput-object p6, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->idSigs:Ljava/util/List;

    .line 284
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->init()V

    .line 285
    return-void
.end method

.method constructor <init>(Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;Ljava/util/List;Ljava/util/List;)V
    .locals 1
    .param p1, "publicPk"    # Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;
    .param p2, "ids"    # Ljava/util/List;
    .param p3, "idSigs"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 292
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->keySigs:Ljava/util/List;

    .line 52
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->ids:Ljava/util/List;

    .line 53
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->idTrusts:Ljava/util/List;

    .line 54
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->idSigs:Ljava/util/List;

    .line 56
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->subSigs:Ljava/util/List;

    .line 293
    iput-object p1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->publicPk:Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;

    .line 294
    iput-object p2, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->ids:Ljava/util/List;

    .line 295
    iput-object p3, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->idSigs:Ljava/util/List;

    .line 297
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->init()V

    .line 298
    return-void
.end method

.method constructor <init>(Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;)V
    .locals 4
    .param p1, "pubKey"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;

    .prologue
    .line 242
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->keySigs:Ljava/util/List;

    .line 52
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->ids:Ljava/util/List;

    .line 53
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->idTrusts:Ljava/util/List;

    .line 54
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->idSigs:Ljava/util/List;

    .line 56
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->subSigs:Ljava/util/List;

    .line 243
    iget-object v1, p1, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->publicPk:Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;

    iput-object v1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->publicPk:Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;

    .line 245
    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p1, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->keySigs:Ljava/util/List;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->keySigs:Ljava/util/List;

    .line 246
    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p1, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->ids:Ljava/util/List;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->ids:Ljava/util/List;

    .line 247
    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p1, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->idTrusts:Ljava/util/List;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->idTrusts:Ljava/util/List;

    .line 248
    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p1, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->idSigs:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->idSigs:Ljava/util/List;

    .line 249
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p1, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->idSigs:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 251
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->idSigs:Ljava/util/List;

    new-instance v3, Ljava/util/ArrayList;

    iget-object v1, p1, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->idSigs:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    invoke-direct {v3, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 249
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 254
    :cond_0
    iget-object v1, p1, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->subSigs:Ljava/util/List;

    if-eqz v1, :cond_1

    .line 256
    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p1, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->subSigs:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->subSigs:Ljava/util/List;

    .line 257
    const/4 v0, 0x0

    :goto_1
    iget-object v1, p1, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->subSigs:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-eq v0, v1, :cond_1

    .line 259
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->subSigs:Ljava/util/List;

    iget-object v2, p1, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->subSigs:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 257
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 263
    :cond_1
    iget-object v1, p1, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->fingerprint:[B

    iput-object v1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->fingerprint:[B

    .line 264
    iget-wide v2, p1, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->keyID:J

    iput-wide v2, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->keyID:J

    .line 265
    iget v1, p1, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->keyStrength:I

    iput v1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->keyStrength:I

    .line 266
    return-void
.end method

.method constructor <init>(Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;Lcom/android/sec/org/bouncycastle/bcpg/TrustPacket;Ljava/util/List;)V
    .locals 2
    .param p1, "key"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;
    .param p2, "trust"    # Lcom/android/sec/org/bouncycastle/bcpg/TrustPacket;
    .param p3, "subSigs"    # Ljava/util/List;

    .prologue
    .line 226
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->keySigs:Ljava/util/List;

    .line 52
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->ids:Ljava/util/List;

    .line 53
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->idTrusts:Ljava/util/List;

    .line 54
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->idSigs:Ljava/util/List;

    .line 56
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->subSigs:Ljava/util/List;

    .line 227
    iget-object v0, p1, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->publicPk:Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->publicPk:Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;

    .line 228
    iput-object p2, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->trustPk:Lcom/android/sec/org/bouncycastle/bcpg/TrustPacket;

    .line 229
    iput-object p3, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->subSigs:Ljava/util/List;

    .line 231
    iget-object v0, p1, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->fingerprint:[B

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->fingerprint:[B

    .line 232
    iget-wide v0, p1, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->keyID:J

    iput-wide v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->keyID:J

    .line 233
    iget v0, p1, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->keyStrength:I

    iput v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->keyStrength:I

    .line 234
    return-void
.end method

.method private static addCert(Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;Ljava/lang/Object;Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;)Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;
    .locals 5
    .param p0, "key"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;
    .param p1, "id"    # Ljava/lang/Object;
    .param p2, "certification"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;

    .prologue
    .line 832
    new-instance v1, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;

    invoke-direct {v1, p0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;-><init>(Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;)V

    .line 833
    .local v1, "returnKey":Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;
    const/4 v2, 0x0

    .line 835
    .local v2, "sigList":Ljava/util/List;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v3, v1, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->ids:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-eq v0, v3, :cond_1

    .line 837
    iget-object v3, v1, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->ids:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 839
    iget-object v3, v1, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->idSigs:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "sigList":Ljava/util/List;
    check-cast v2, Ljava/util/List;

    .line 835
    .restart local v2    # "sigList":Ljava/util/List;
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 843
    :cond_1
    if-eqz v2, :cond_2

    .line 845
    invoke-interface {v2, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 857
    :goto_1
    return-object v1

    .line 849
    :cond_2
    new-instance v2, Ljava/util/ArrayList;

    .end local v2    # "sigList":Ljava/util/List;
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 851
    .restart local v2    # "sigList":Ljava/util/List;
    invoke-interface {v2, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 852
    iget-object v3, v1, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->ids:Ljava/util/List;

    invoke-interface {v3, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 853
    iget-object v3, v1, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->idTrusts:Ljava/util/List;

    const/4 v4, 0x0

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 854
    iget-object v3, v1, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->idSigs:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method public static addCertification(Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;)Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;
    .locals 3
    .param p0, "key"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;
    .param p1, "certification"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;

    .prologue
    .line 982
    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->isMasterKey()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 984
    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;->getSignatureType()I

    move-result v1

    const/16 v2, 0x28

    if-ne v1, v2, :cond_1

    .line 986
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "signature type incorrect for master key revocation."

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 991
    :cond_0
    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;->getSignatureType()I

    move-result v1

    const/16 v2, 0x20

    if-ne v1, v2, :cond_1

    .line 993
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "signature type incorrect for sub-key revocation."

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 997
    :cond_1
    new-instance v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;

    invoke-direct {v0, p0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;-><init>(Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;)V

    .line 999
    .local v0, "returnKey":Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;
    iget-object v1, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->subSigs:Ljava/util/List;

    if-eqz v1, :cond_2

    .line 1001
    iget-object v1, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->subSigs:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1008
    :goto_0
    return-object v0

    .line 1005
    :cond_2
    iget-object v1, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->keySigs:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public static addCertification(Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;Lcom/android/sec/org/bouncycastle/openpgp/PGPUserAttributeSubpacketVector;Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;)Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;
    .locals 1
    .param p0, "key"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;
    .param p1, "userAttributes"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPUserAttributeSubpacketVector;
    .param p2, "certification"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;

    .prologue
    .line 824
    invoke-static {p0, p1, p2}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->addCert(Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;Ljava/lang/Object;Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;)Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;

    move-result-object v0

    return-object v0
.end method

.method public static addCertification(Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;Ljava/lang/String;Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;)Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;
    .locals 1
    .param p0, "key"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "certification"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;

    .prologue
    .line 808
    invoke-static {p0, p1, p2}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->addCert(Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;Ljava/lang/Object;Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;)Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;

    move-result-object v0

    return-object v0
.end method

.method private getExpirationTimeFromSig(ZI)J
    .locals 8
    .param p1, "selfSigned"    # Z
    .param p2, "signatureType"    # I

    .prologue
    .line 388
    invoke-virtual {p0, p2}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->getSignaturesOfType(I)Ljava/util/Iterator;

    move-result-object v2

    .line 390
    .local v2, "signatures":Ljava/util/Iterator;
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 392
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;

    .line 394
    .local v1, "sig":Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;
    if-eqz p1, :cond_0

    invoke-virtual {v1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;->getKeyID()J

    move-result-wide v4

    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->getKeyID()J

    move-result-wide v6

    cmp-long v3, v4, v6

    if-nez v3, :cond_2

    .line 396
    :cond_0
    invoke-virtual {v1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;->getHashedSubPackets()Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;

    move-result-object v0

    .line 398
    .local v0, "hashed":Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;
    if-eqz v0, :cond_1

    .line 400
    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;->getKeyExpirationTime()J

    move-result-wide v4

    .line 407
    .end local v0    # "hashed":Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;
    .end local v1    # "sig":Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;
    :goto_0
    return-wide v4

    .line 403
    .restart local v0    # "hashed":Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;
    .restart local v1    # "sig":Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;
    :cond_1
    const-wide/16 v4, 0x0

    goto :goto_0

    .line 407
    .end local v0    # "hashed":Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;
    .end local v1    # "sig":Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;
    :cond_2
    const-wide/16 v4, -0x1

    goto :goto_0
.end method

.method private init()V
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 65
    iget-object v6, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->publicPk:Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;

    invoke-virtual {v6}, Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;->getKey()Lcom/android/sec/org/bouncycastle/bcpg/BCPGKey;

    move-result-object v4

    .line 67
    .local v4, "key":Lcom/android/sec/org/bouncycastle/bcpg/BCPGKey;
    iget-object v6, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->publicPk:Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;

    invoke-virtual {v6}, Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;->getVersion()I

    move-result v6

    const/4 v7, 0x3

    if-gt v6, v7, :cond_1

    move-object v5, v4

    .line 69
    check-cast v5, Lcom/android/sec/org/bouncycastle/bcpg/RSAPublicBCPGKey;

    .line 71
    .local v5, "rK":Lcom/android/sec/org/bouncycastle/bcpg/RSAPublicBCPGKey;
    invoke-virtual {v5}, Lcom/android/sec/org/bouncycastle/bcpg/RSAPublicBCPGKey;->getModulus()Ljava/math/BigInteger;

    move-result-object v6

    invoke-virtual {v6}, Ljava/math/BigInteger;->longValue()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->keyID:J

    .line 75
    :try_start_0
    const-string v6, "MD5"

    invoke-static {v6}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v1

    .line 77
    .local v1, "digest":Ljava/security/MessageDigest;
    new-instance v6, Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;

    invoke-virtual {v5}, Lcom/android/sec/org/bouncycastle/bcpg/RSAPublicBCPGKey;->getModulus()Ljava/math/BigInteger;

    move-result-object v7

    invoke-direct {v6, v7}, Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;-><init>(Ljava/math/BigInteger;)V

    invoke-virtual {v6}, Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;->getEncoded()[B

    move-result-object v0

    .line 78
    .local v0, "bytes":[B
    const/4 v6, 0x2

    array-length v7, v0

    add-int/lit8 v7, v7, -0x2

    invoke-virtual {v1, v0, v6, v7}, Ljava/security/MessageDigest;->update([BII)V

    .line 80
    new-instance v6, Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;

    invoke-virtual {v5}, Lcom/android/sec/org/bouncycastle/bcpg/RSAPublicBCPGKey;->getPublicExponent()Ljava/math/BigInteger;

    move-result-object v7

    invoke-direct {v6, v7}, Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;-><init>(Ljava/math/BigInteger;)V

    invoke-virtual {v6}, Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;->getEncoded()[B

    move-result-object v0

    .line 81
    const/4 v6, 0x2

    array-length v7, v0

    add-int/lit8 v7, v7, -0x2

    invoke-virtual {v1, v0, v6, v7}, Ljava/security/MessageDigest;->update([BII)V

    .line 83
    invoke-virtual {v1}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v6

    iput-object v6, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->fingerprint:[B
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    .line 90
    invoke-virtual {v5}, Lcom/android/sec/org/bouncycastle/bcpg/RSAPublicBCPGKey;->getModulus()Ljava/math/BigInteger;

    move-result-object v6

    invoke-virtual {v6}, Ljava/math/BigInteger;->bitLength()I

    move-result v6

    iput v6, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->keyStrength:I

    .line 134
    .end local v0    # "bytes":[B
    .end local v4    # "key":Lcom/android/sec/org/bouncycastle/bcpg/BCPGKey;
    .end local v5    # "rK":Lcom/android/sec/org/bouncycastle/bcpg/RSAPublicBCPGKey;
    :cond_0
    :goto_0
    return-void

    .line 85
    .end local v1    # "digest":Ljava/security/MessageDigest;
    .restart local v4    # "key":Lcom/android/sec/org/bouncycastle/bcpg/BCPGKey;
    .restart local v5    # "rK":Lcom/android/sec/org/bouncycastle/bcpg/RSAPublicBCPGKey;
    :catch_0
    move-exception v2

    .line 87
    .local v2, "e":Ljava/security/NoSuchAlgorithmException;
    new-instance v6, Ljava/io/IOException;

    const-string v7, "can\'t find MD5"

    invoke-direct {v6, v7}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 94
    .end local v2    # "e":Ljava/security/NoSuchAlgorithmException;
    .end local v5    # "rK":Lcom/android/sec/org/bouncycastle/bcpg/RSAPublicBCPGKey;
    :cond_1
    iget-object v6, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->publicPk:Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;

    invoke-virtual {v6}, Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;->getEncodedContents()[B

    move-result-object v3

    .line 98
    .local v3, "kBytes":[B
    :try_start_1
    const-string v6, "SHA1"

    invoke-static {v6}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v1

    .line 100
    .restart local v1    # "digest":Ljava/security/MessageDigest;
    const/16 v6, -0x67

    invoke-virtual {v1, v6}, Ljava/security/MessageDigest;->update(B)V

    .line 101
    array-length v6, v3

    shr-int/lit8 v6, v6, 0x8

    int-to-byte v6, v6

    invoke-virtual {v1, v6}, Ljava/security/MessageDigest;->update(B)V

    .line 102
    array-length v6, v3

    int-to-byte v6, v6

    invoke-virtual {v1, v6}, Ljava/security/MessageDigest;->update(B)V

    .line 103
    invoke-virtual {v1, v3}, Ljava/security/MessageDigest;->update([B)V

    .line 105
    invoke-virtual {v1}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v6

    iput-object v6, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->fingerprint:[B
    :try_end_1
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_1 .. :try_end_1} :catch_1

    .line 112
    iget-object v6, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->fingerprint:[B

    iget-object v7, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->fingerprint:[B

    array-length v7, v7

    add-int/lit8 v7, v7, -0x8

    aget-byte v6, v6, v7

    and-int/lit16 v6, v6, 0xff

    int-to-long v6, v6

    const/16 v8, 0x38

    shl-long/2addr v6, v8

    iget-object v8, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->fingerprint:[B

    iget-object v9, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->fingerprint:[B

    array-length v9, v9

    add-int/lit8 v9, v9, -0x7

    aget-byte v8, v8, v9

    and-int/lit16 v8, v8, 0xff

    int-to-long v8, v8

    const/16 v10, 0x30

    shl-long/2addr v8, v10

    or-long/2addr v6, v8

    iget-object v8, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->fingerprint:[B

    iget-object v9, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->fingerprint:[B

    array-length v9, v9

    add-int/lit8 v9, v9, -0x6

    aget-byte v8, v8, v9

    and-int/lit16 v8, v8, 0xff

    int-to-long v8, v8

    const/16 v10, 0x28

    shl-long/2addr v8, v10

    or-long/2addr v6, v8

    iget-object v8, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->fingerprint:[B

    iget-object v9, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->fingerprint:[B

    array-length v9, v9

    add-int/lit8 v9, v9, -0x5

    aget-byte v8, v8, v9

    and-int/lit16 v8, v8, 0xff

    int-to-long v8, v8

    const/16 v10, 0x20

    shl-long/2addr v8, v10

    or-long/2addr v6, v8

    iget-object v8, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->fingerprint:[B

    iget-object v9, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->fingerprint:[B

    array-length v9, v9

    add-int/lit8 v9, v9, -0x4

    aget-byte v8, v8, v9

    and-int/lit16 v8, v8, 0xff

    int-to-long v8, v8

    const/16 v10, 0x18

    shl-long/2addr v8, v10

    or-long/2addr v6, v8

    iget-object v8, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->fingerprint:[B

    iget-object v9, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->fingerprint:[B

    array-length v9, v9

    add-int/lit8 v9, v9, -0x3

    aget-byte v8, v8, v9

    and-int/lit16 v8, v8, 0xff

    int-to-long v8, v8

    const/16 v10, 0x10

    shl-long/2addr v8, v10

    or-long/2addr v6, v8

    iget-object v8, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->fingerprint:[B

    iget-object v9, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->fingerprint:[B

    array-length v9, v9

    add-int/lit8 v9, v9, -0x2

    aget-byte v8, v8, v9

    and-int/lit16 v8, v8, 0xff

    int-to-long v8, v8

    const/16 v10, 0x8

    shl-long/2addr v8, v10

    or-long/2addr v6, v8

    iget-object v8, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->fingerprint:[B

    iget-object v9, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->fingerprint:[B

    array-length v9, v9

    add-int/lit8 v9, v9, -0x1

    aget-byte v8, v8, v9

    and-int/lit16 v8, v8, 0xff

    int-to-long v8, v8

    or-long/2addr v6, v8

    iput-wide v6, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->keyID:J

    .line 121
    instance-of v6, v4, Lcom/android/sec/org/bouncycastle/bcpg/RSAPublicBCPGKey;

    if-eqz v6, :cond_2

    .line 123
    check-cast v4, Lcom/android/sec/org/bouncycastle/bcpg/RSAPublicBCPGKey;

    .end local v4    # "key":Lcom/android/sec/org/bouncycastle/bcpg/BCPGKey;
    invoke-virtual {v4}, Lcom/android/sec/org/bouncycastle/bcpg/RSAPublicBCPGKey;->getModulus()Ljava/math/BigInteger;

    move-result-object v6

    invoke-virtual {v6}, Ljava/math/BigInteger;->bitLength()I

    move-result v6

    iput v6, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->keyStrength:I

    goto/16 :goto_0

    .line 107
    .end local v1    # "digest":Ljava/security/MessageDigest;
    .restart local v4    # "key":Lcom/android/sec/org/bouncycastle/bcpg/BCPGKey;
    :catch_1
    move-exception v2

    .line 109
    .restart local v2    # "e":Ljava/security/NoSuchAlgorithmException;
    new-instance v6, Ljava/io/IOException;

    const-string v7, "can\'t find SHA1"

    invoke-direct {v6, v7}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 125
    .end local v2    # "e":Ljava/security/NoSuchAlgorithmException;
    .restart local v1    # "digest":Ljava/security/MessageDigest;
    :cond_2
    instance-of v6, v4, Lcom/android/sec/org/bouncycastle/bcpg/DSAPublicBCPGKey;

    if-eqz v6, :cond_3

    .line 127
    check-cast v4, Lcom/android/sec/org/bouncycastle/bcpg/DSAPublicBCPGKey;

    .end local v4    # "key":Lcom/android/sec/org/bouncycastle/bcpg/BCPGKey;
    invoke-virtual {v4}, Lcom/android/sec/org/bouncycastle/bcpg/DSAPublicBCPGKey;->getP()Ljava/math/BigInteger;

    move-result-object v6

    invoke-virtual {v6}, Ljava/math/BigInteger;->bitLength()I

    move-result v6

    iput v6, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->keyStrength:I

    goto/16 :goto_0

    .line 129
    .restart local v4    # "key":Lcom/android/sec/org/bouncycastle/bcpg/BCPGKey;
    :cond_3
    instance-of v6, v4, Lcom/android/sec/org/bouncycastle/bcpg/ElGamalPublicBCPGKey;

    if-eqz v6, :cond_0

    .line 131
    check-cast v4, Lcom/android/sec/org/bouncycastle/bcpg/ElGamalPublicBCPGKey;

    .end local v4    # "key":Lcom/android/sec/org/bouncycastle/bcpg/BCPGKey;
    invoke-virtual {v4}, Lcom/android/sec/org/bouncycastle/bcpg/ElGamalPublicBCPGKey;->getP()Ljava/math/BigInteger;

    move-result-object v6

    invoke-virtual {v6}, Ljava/math/BigInteger;->bitLength()I

    move-result v6

    iput v6, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->keyStrength:I

    goto/16 :goto_0
.end method

.method private static removeCert(Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;Ljava/lang/Object;)Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;
    .locals 4
    .param p0, "key"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;
    .param p1, "id"    # Ljava/lang/Object;

    .prologue
    .line 893
    new-instance v2, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;

    invoke-direct {v2, p0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;-><init>(Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;)V

    .line 894
    .local v2, "returnKey":Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;
    const/4 v0, 0x0

    .line 896
    .local v0, "found":Z
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, v2, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->ids:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v1, v3, :cond_1

    .line 898
    iget-object v3, v2, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->ids:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 900
    const/4 v0, 0x1

    .line 901
    iget-object v3, v2, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->ids:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 902
    iget-object v3, v2, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->idTrusts:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 903
    iget-object v3, v2, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->idSigs:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 896
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 907
    :cond_1
    if-nez v0, :cond_2

    .line 909
    const/4 v2, 0x0

    .line 912
    .end local v2    # "returnKey":Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;
    :cond_2
    return-object v2
.end method

.method private static removeCert(Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;Ljava/lang/Object;Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;)Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;
    .locals 4
    .param p0, "key"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;
    .param p1, "id"    # Ljava/lang/Object;
    .param p2, "certification"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;

    .prologue
    .line 952
    new-instance v2, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;

    invoke-direct {v2, p0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;-><init>(Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;)V

    .line 953
    .local v2, "returnKey":Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;
    const/4 v0, 0x0

    .line 955
    .local v0, "found":Z
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, v2, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->ids:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v1, v3, :cond_1

    .line 957
    iget-object v3, v2, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->ids:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 959
    iget-object v3, v2, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->idSigs:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    invoke-interface {v3, p2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-result v0

    .line 955
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 963
    :cond_1
    if-nez v0, :cond_2

    .line 965
    const/4 v2, 0x0

    .line 968
    .end local v2    # "returnKey":Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;
    :cond_2
    return-object v2
.end method

.method public static removeCertification(Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;)Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;
    .locals 6
    .param p0, "key"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;
    .param p1, "certification"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;

    .prologue
    .line 1022
    new-instance v3, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;

    invoke-direct {v3, p0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;-><init>(Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;)V

    .line 1025
    .local v3, "returnKey":Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;
    if-eqz v3, :cond_6

    .line 1026
    iget-object v5, v3, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->subSigs:Ljava/util/List;

    if-eqz v5, :cond_2

    .line 1027
    iget-object v5, v3, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->subSigs:Ljava/util/List;

    invoke-interface {v5, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-result v0

    .line 1032
    .local v0, "found":Z
    :goto_0
    if-nez v0, :cond_6

    .line 1033
    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->getUserIDs()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "it":Ljava/util/Iterator;
    :cond_0
    if-eqz v2, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1034
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1035
    .local v1, "id":Ljava/lang/String;
    invoke-virtual {p0, v1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->getSignaturesForID(Ljava/lang/String;)Ljava/util/Iterator;

    move-result-object v4

    .local v4, "sIt":Ljava/util/Iterator;
    :cond_1
    :goto_1
    if-eqz v4, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1036
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    if-ne p1, v5, :cond_1

    .line 1037
    const/4 v0, 0x1

    .line 1038
    invoke-static {v3, v1, p1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->removeCertification(Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;Ljava/lang/String;Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;)Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;

    move-result-object v3

    goto :goto_1

    .line 1029
    .end local v0    # "found":Z
    .end local v1    # "id":Ljava/lang/String;
    .end local v2    # "it":Ljava/util/Iterator;
    .end local v4    # "sIt":Ljava/util/Iterator;
    :cond_2
    iget-object v5, v3, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->keySigs:Ljava/util/List;

    invoke-interface {v5, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-result v0

    .restart local v0    # "found":Z
    goto :goto_0

    .line 1044
    .restart local v2    # "it":Ljava/util/Iterator;
    :cond_3
    if-nez v0, :cond_6

    .line 1045
    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->getUserAttributes()Ljava/util/Iterator;

    move-result-object v2

    :cond_4
    if-eqz v2, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_6

    .line 1046
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/sec/org/bouncycastle/openpgp/PGPUserAttributeSubpacketVector;

    .line 1048
    .local v1, "id":Lcom/android/sec/org/bouncycastle/openpgp/PGPUserAttributeSubpacketVector;
    invoke-virtual {p0, v1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->getSignaturesForUserAttribute(Lcom/android/sec/org/bouncycastle/openpgp/PGPUserAttributeSubpacketVector;)Ljava/util/Iterator;

    move-result-object v4

    .restart local v4    # "sIt":Ljava/util/Iterator;
    :cond_5
    :goto_2
    if-eqz v4, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 1049
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    if-ne p1, v5, :cond_5

    .line 1050
    const/4 v0, 0x1

    .line 1051
    invoke-static {v3, v1, p1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->removeCertification(Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;Lcom/android/sec/org/bouncycastle/openpgp/PGPUserAttributeSubpacketVector;Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;)Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;

    move-result-object v3

    goto :goto_2

    .line 1060
    .end local v0    # "found":Z
    .end local v1    # "id":Lcom/android/sec/org/bouncycastle/openpgp/PGPUserAttributeSubpacketVector;
    .end local v2    # "it":Ljava/util/Iterator;
    .end local v4    # "sIt":Ljava/util/Iterator;
    :cond_6
    return-object v3
.end method

.method public static removeCertification(Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;Lcom/android/sec/org/bouncycastle/openpgp/PGPUserAttributeSubpacketVector;)Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;
    .locals 1
    .param p0, "key"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;
    .param p1, "userAttributes"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPUserAttributeSubpacketVector;

    .prologue
    .line 872
    invoke-static {p0, p1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->removeCert(Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;Ljava/lang/Object;)Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;

    move-result-object v0

    return-object v0
.end method

.method public static removeCertification(Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;Lcom/android/sec/org/bouncycastle/openpgp/PGPUserAttributeSubpacketVector;Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;)Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;
    .locals 1
    .param p0, "key"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;
    .param p1, "userAttributes"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPUserAttributeSubpacketVector;
    .param p2, "certification"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;

    .prologue
    .line 944
    invoke-static {p0, p1, p2}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->removeCert(Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;Ljava/lang/Object;Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;)Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;

    move-result-object v0

    return-object v0
.end method

.method public static removeCertification(Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;Ljava/lang/String;)Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;
    .locals 1
    .param p0, "key"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 886
    invoke-static {p0, p1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->removeCert(Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;Ljava/lang/Object;)Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;

    move-result-object v0

    return-object v0
.end method

.method public static removeCertification(Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;Ljava/lang/String;Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;)Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;
    .locals 1
    .param p0, "key"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "certification"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;

    .prologue
    .line 928
    invoke-static {p0, p1, p2}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->removeCert(Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;Ljava/lang/Object;Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;)Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public encode(Ljava/io/OutputStream;)V
    .locals 8
    .param p1, "outStream"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 702
    instance-of v6, p1, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;

    if-eqz v6, :cond_1

    move-object v3, p1

    .line 704
    check-cast v3, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;

    .line 711
    .local v3, "out":Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;
    :goto_0
    iget-object v6, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->publicPk:Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;

    invoke-virtual {v3, v6}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->writePacket(Lcom/android/sec/org/bouncycastle/bcpg/ContainedPacket;)V

    .line 712
    iget-object v6, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->trustPk:Lcom/android/sec/org/bouncycastle/bcpg/TrustPacket;

    if-eqz v6, :cond_0

    .line 714
    iget-object v6, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->trustPk:Lcom/android/sec/org/bouncycastle/bcpg/TrustPacket;

    invoke-virtual {v3, v6}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->writePacket(Lcom/android/sec/org/bouncycastle/bcpg/ContainedPacket;)V

    .line 717
    :cond_0
    iget-object v6, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->subSigs:Ljava/util/List;

    if-nez v6, :cond_6

    .line 719
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v6, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->keySigs:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    if-eq v0, v6, :cond_2

    .line 721
    iget-object v6, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->keySigs:Ljava/util/List;

    invoke-interface {v6, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;

    invoke-virtual {v6, v3}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;->encode(Ljava/io/OutputStream;)V

    .line 719
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 708
    .end local v0    # "i":I
    .end local v3    # "out":Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;
    :cond_1
    new-instance v3, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;

    invoke-direct {v3, p1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;-><init>(Ljava/io/OutputStream;)V

    .restart local v3    # "out":Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;
    goto :goto_0

    .line 724
    .restart local v0    # "i":I
    :cond_2
    const/4 v0, 0x0

    :goto_2
    iget-object v6, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->ids:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    if-eq v0, v6, :cond_7

    .line 726
    iget-object v6, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->ids:Ljava/util/List;

    invoke-interface {v6, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    instance-of v6, v6, Ljava/lang/String;

    if-eqz v6, :cond_4

    .line 728
    iget-object v6, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->ids:Ljava/util/List;

    invoke-interface {v6, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 730
    .local v1, "id":Ljava/lang/String;
    new-instance v6, Lcom/android/sec/org/bouncycastle/bcpg/UserIDPacket;

    invoke-direct {v6, v1}, Lcom/android/sec/org/bouncycastle/bcpg/UserIDPacket;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v6}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->writePacket(Lcom/android/sec/org/bouncycastle/bcpg/ContainedPacket;)V

    .line 739
    .end local v1    # "id":Ljava/lang/String;
    :goto_3
    iget-object v6, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->idTrusts:Ljava/util/List;

    invoke-interface {v6, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    if-eqz v6, :cond_3

    .line 741
    iget-object v6, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->idTrusts:Ljava/util/List;

    invoke-interface {v6, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/android/sec/org/bouncycastle/bcpg/ContainedPacket;

    invoke-virtual {v3, v6}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->writePacket(Lcom/android/sec/org/bouncycastle/bcpg/ContainedPacket;)V

    .line 744
    :cond_3
    iget-object v6, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->idSigs:Ljava/util/List;

    invoke-interface {v6, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/List;

    .line 745
    .local v4, "sigs":Ljava/util/List;
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_4
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v6

    if-eq v2, v6, :cond_5

    .line 747
    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;

    invoke-virtual {v6, v3}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;->encode(Ljava/io/OutputStream;)V

    .line 745
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 734
    .end local v2    # "j":I
    .end local v4    # "sigs":Ljava/util/List;
    :cond_4
    iget-object v6, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->ids:Ljava/util/List;

    invoke-interface {v6, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/sec/org/bouncycastle/openpgp/PGPUserAttributeSubpacketVector;

    .line 736
    .local v5, "v":Lcom/android/sec/org/bouncycastle/openpgp/PGPUserAttributeSubpacketVector;
    new-instance v6, Lcom/android/sec/org/bouncycastle/bcpg/UserAttributePacket;

    invoke-virtual {v5}, Lcom/android/sec/org/bouncycastle/openpgp/PGPUserAttributeSubpacketVector;->toSubpacketArray()[Lcom/android/sec/org/bouncycastle/bcpg/UserAttributeSubpacket;

    move-result-object v7

    invoke-direct {v6, v7}, Lcom/android/sec/org/bouncycastle/bcpg/UserAttributePacket;-><init>([Lcom/android/sec/org/bouncycastle/bcpg/UserAttributeSubpacket;)V

    invoke-virtual {v3, v6}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->writePacket(Lcom/android/sec/org/bouncycastle/bcpg/ContainedPacket;)V

    goto :goto_3

    .line 724
    .end local v5    # "v":Lcom/android/sec/org/bouncycastle/openpgp/PGPUserAttributeSubpacketVector;
    .restart local v2    # "j":I
    .restart local v4    # "sigs":Ljava/util/List;
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 753
    .end local v0    # "i":I
    .end local v2    # "j":I
    .end local v4    # "sigs":Ljava/util/List;
    :cond_6
    const/4 v2, 0x0

    .restart local v2    # "j":I
    :goto_5
    iget-object v6, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->subSigs:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    if-eq v2, v6, :cond_7

    .line 755
    iget-object v6, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->subSigs:Ljava/util/List;

    invoke-interface {v6, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;

    invoke-virtual {v6, v3}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;->encode(Ljava/io/OutputStream;)V

    .line 753
    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    .line 758
    .end local v2    # "j":I
    :cond_7
    return-void
.end method

.method public getAlgorithm()I
    .locals 1

    .prologue
    .line 479
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->publicPk:Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;->getAlgorithm()I

    move-result v0

    return v0
.end method

.method public getBitStrength()I
    .locals 1

    .prologue
    .line 489
    iget v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->keyStrength:I

    return v0
.end method

.method public getCreationTime()Ljava/util/Date;
    .locals 1

    .prologue
    .line 313
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->publicPk:Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;->getTime()Ljava/util/Date;

    move-result-object v0

    return-object v0
.end method

.method public getEncoded()[B
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 689
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 691
    .local v0, "bOut":Ljava/io/ByteArrayOutputStream;
    invoke-virtual {p0, v0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->encode(Ljava/io/OutputStream;)V

    .line 693
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    return-object v1
.end method

.method public getFingerprint()[B
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 427
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->fingerprint:[B

    array-length v1, v1

    new-array v0, v1, [B

    .line 429
    .local v0, "tmp":[B
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->fingerprint:[B

    array-length v2, v0

    invoke-static {v1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 431
    return-object v0
.end method

.method public getKey(Ljava/lang/String;)Ljava/security/PublicKey;
    .locals 1
    .param p1, "provider"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;,
            Ljava/security/NoSuchProviderException;
        }
    .end annotation

    .prologue
    .line 504
    invoke-static {p1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPUtil;->getProvider(Ljava/lang/String;)Ljava/security/Provider;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->getKey(Ljava/security/Provider;)Ljava/security/PublicKey;

    move-result-object v0

    return-object v0
.end method

.method public getKey(Ljava/security/Provider;)Ljava/security/PublicKey;
    .locals 12
    .param p1, "provider"    # Ljava/security/Provider;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
        }
    .end annotation

    .prologue
    .line 515
    :try_start_0
    iget-object v8, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->publicPk:Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;

    invoke-virtual {v8}, Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;->getAlgorithm()I

    move-result v8

    sparse-switch v8, :sswitch_data_0

    .line 542
    new-instance v8, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;

    const-string v9, "unknown public key algorithm encountered"

    invoke-direct {v8, v9}, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;-><init>(Ljava/lang/String;)V

    throw v8
    :try_end_0
    .catch Lcom/android/sec/org/bouncycastle/openpgp/PGPException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 545
    :catch_0
    move-exception v2

    .line 547
    .local v2, "e":Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
    throw v2

    .line 520
    .end local v2    # "e":Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
    :sswitch_0
    :try_start_1
    iget-object v8, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->publicPk:Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;

    invoke-virtual {v8}, Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;->getKey()Lcom/android/sec/org/bouncycastle/bcpg/BCPGKey;

    move-result-object v6

    check-cast v6, Lcom/android/sec/org/bouncycastle/bcpg/RSAPublicBCPGKey;

    .line 521
    .local v6, "rsaK":Lcom/android/sec/org/bouncycastle/bcpg/RSAPublicBCPGKey;
    new-instance v7, Ljava/security/spec/RSAPublicKeySpec;

    invoke-virtual {v6}, Lcom/android/sec/org/bouncycastle/bcpg/RSAPublicBCPGKey;->getModulus()Ljava/math/BigInteger;

    move-result-object v8

    invoke-virtual {v6}, Lcom/android/sec/org/bouncycastle/bcpg/RSAPublicBCPGKey;->getPublicExponent()Ljava/math/BigInteger;

    move-result-object v9

    invoke-direct {v7, v8, v9}, Ljava/security/spec/RSAPublicKeySpec;-><init>(Ljava/math/BigInteger;Ljava/math/BigInteger;)V

    .line 523
    .local v7, "rsaSpec":Ljava/security/spec/RSAPublicKeySpec;
    const-string v8, "RSA"

    invoke-static {v8, p1}, Ljava/security/KeyFactory;->getInstance(Ljava/lang/String;Ljava/security/Provider;)Ljava/security/KeyFactory;

    move-result-object v5

    .line 525
    .local v5, "fact":Ljava/security/KeyFactory;
    invoke-virtual {v5, v7}, Ljava/security/KeyFactory;->generatePublic(Ljava/security/spec/KeySpec;)Ljava/security/PublicKey;

    move-result-object v8

    .line 540
    .end local v6    # "rsaK":Lcom/android/sec/org/bouncycastle/bcpg/RSAPublicBCPGKey;
    .end local v7    # "rsaSpec":Ljava/security/spec/RSAPublicKeySpec;
    :goto_0
    return-object v8

    .line 527
    .end local v5    # "fact":Ljava/security/KeyFactory;
    :sswitch_1
    iget-object v8, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->publicPk:Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;

    invoke-virtual {v8}, Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;->getKey()Lcom/android/sec/org/bouncycastle/bcpg/BCPGKey;

    move-result-object v0

    check-cast v0, Lcom/android/sec/org/bouncycastle/bcpg/DSAPublicBCPGKey;

    .line 528
    .local v0, "dsaK":Lcom/android/sec/org/bouncycastle/bcpg/DSAPublicBCPGKey;
    new-instance v1, Ljava/security/spec/DSAPublicKeySpec;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/bcpg/DSAPublicBCPGKey;->getY()Ljava/math/BigInteger;

    move-result-object v8

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/bcpg/DSAPublicBCPGKey;->getP()Ljava/math/BigInteger;

    move-result-object v9

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/bcpg/DSAPublicBCPGKey;->getQ()Ljava/math/BigInteger;

    move-result-object v10

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/bcpg/DSAPublicBCPGKey;->getG()Ljava/math/BigInteger;

    move-result-object v11

    invoke-direct {v1, v8, v9, v10, v11}, Ljava/security/spec/DSAPublicKeySpec;-><init>(Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/math/BigInteger;)V

    .line 530
    .local v1, "dsaSpec":Ljava/security/spec/DSAPublicKeySpec;
    const-string v8, "DSA"

    invoke-static {v8, p1}, Ljava/security/KeyFactory;->getInstance(Ljava/lang/String;Ljava/security/Provider;)Ljava/security/KeyFactory;

    move-result-object v5

    .line 532
    .restart local v5    # "fact":Ljava/security/KeyFactory;
    invoke-virtual {v5, v1}, Ljava/security/KeyFactory;->generatePublic(Ljava/security/spec/KeySpec;)Ljava/security/PublicKey;

    move-result-object v8

    goto :goto_0

    .line 535
    .end local v0    # "dsaK":Lcom/android/sec/org/bouncycastle/bcpg/DSAPublicBCPGKey;
    .end local v1    # "dsaSpec":Ljava/security/spec/DSAPublicKeySpec;
    .end local v5    # "fact":Ljava/security/KeyFactory;
    :sswitch_2
    iget-object v8, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->publicPk:Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;

    invoke-virtual {v8}, Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;->getKey()Lcom/android/sec/org/bouncycastle/bcpg/BCPGKey;

    move-result-object v3

    check-cast v3, Lcom/android/sec/org/bouncycastle/bcpg/ElGamalPublicBCPGKey;

    .line 536
    .local v3, "elK":Lcom/android/sec/org/bouncycastle/bcpg/ElGamalPublicBCPGKey;
    new-instance v4, Lcom/android/sec/org/bouncycastle/jce/spec/ElGamalPublicKeySpec;

    invoke-virtual {v3}, Lcom/android/sec/org/bouncycastle/bcpg/ElGamalPublicBCPGKey;->getY()Ljava/math/BigInteger;

    move-result-object v8

    new-instance v9, Lcom/android/sec/org/bouncycastle/jce/spec/ElGamalParameterSpec;

    invoke-virtual {v3}, Lcom/android/sec/org/bouncycastle/bcpg/ElGamalPublicBCPGKey;->getP()Ljava/math/BigInteger;

    move-result-object v10

    invoke-virtual {v3}, Lcom/android/sec/org/bouncycastle/bcpg/ElGamalPublicBCPGKey;->getG()Ljava/math/BigInteger;

    move-result-object v11

    invoke-direct {v9, v10, v11}, Lcom/android/sec/org/bouncycastle/jce/spec/ElGamalParameterSpec;-><init>(Ljava/math/BigInteger;Ljava/math/BigInteger;)V

    invoke-direct {v4, v8, v9}, Lcom/android/sec/org/bouncycastle/jce/spec/ElGamalPublicKeySpec;-><init>(Ljava/math/BigInteger;Lcom/android/sec/org/bouncycastle/jce/spec/ElGamalParameterSpec;)V

    .line 538
    .local v4, "elSpec":Lcom/android/sec/org/bouncycastle/jce/spec/ElGamalPublicKeySpec;
    const-string v8, "ElGamal"

    invoke-static {v8, p1}, Ljava/security/KeyFactory;->getInstance(Ljava/lang/String;Ljava/security/Provider;)Ljava/security/KeyFactory;

    move-result-object v5

    .line 540
    .restart local v5    # "fact":Ljava/security/KeyFactory;
    invoke-virtual {v5, v4}, Ljava/security/KeyFactory;->generatePublic(Ljava/security/spec/KeySpec;)Ljava/security/PublicKey;
    :try_end_1
    .catch Lcom/android/sec/org/bouncycastle/openpgp/PGPException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v8

    goto :goto_0

    .line 549
    .end local v3    # "elK":Lcom/android/sec/org/bouncycastle/bcpg/ElGamalPublicBCPGKey;
    .end local v4    # "elSpec":Lcom/android/sec/org/bouncycastle/jce/spec/ElGamalPublicKeySpec;
    .end local v5    # "fact":Ljava/security/KeyFactory;
    :catch_1
    move-exception v2

    .line 551
    .local v2, "e":Ljava/lang/Exception;
    new-instance v8, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;

    const-string v9, "exception constructing public key"

    invoke-direct {v8, v9, v2}, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v8

    .line 515
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_0
        0x3 -> :sswitch_0
        0x10 -> :sswitch_2
        0x11 -> :sswitch_1
        0x14 -> :sswitch_2
    .end sparse-switch
.end method

.method public getKeyFlags()I
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 1065
    iget-object v3, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->publicPk:Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;

    invoke-virtual {v3}, Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;->getVersion()I

    move-result v3

    const/4 v4, 0x3

    if-le v3, v4, :cond_4

    .line 1067
    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->isMasterKey()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1069
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    sget-object v3, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->MASTER_KEY_CERTIFICATION_TYPES:[I

    array-length v3, v3

    if-eq v0, v3, :cond_3

    .line 1071
    const/4 v3, 0x1

    sget-object v4, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->MASTER_KEY_CERTIFICATION_TYPES:[I

    aget v4, v4, v0

    invoke-virtual {p0, v3, v4}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->getKeyFlagsFromSig(ZI)I

    move-result v1

    .line 1073
    .local v1, "keyFlags":I
    if-lez v1, :cond_1

    .line 1092
    .end local v0    # "i":I
    .end local v1    # "keyFlags":I
    :cond_0
    :goto_1
    return v1

    .line 1069
    .restart local v0    # "i":I
    .restart local v1    # "keyFlags":I
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1081
    .end local v0    # "i":I
    .end local v1    # "keyFlags":I
    :cond_2
    const/16 v3, 0x18

    invoke-virtual {p0, v2, v3}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->getKeyFlagsFromSig(ZI)I

    move-result v1

    .line 1083
    .restart local v1    # "keyFlags":I
    if-gtz v1, :cond_0

    .end local v1    # "keyFlags":I
    :cond_3
    move v1, v2

    .line 1089
    goto :goto_1

    :cond_4
    move v1, v2

    .line 1092
    goto :goto_1
.end method

.method public getKeyFlagsFromSig(ZI)I
    .locals 8
    .param p1, "selfSigned"    # Z
    .param p2, "signatureType"    # I

    .prologue
    const/4 v3, 0x0

    .line 1098
    invoke-virtual {p0, p2}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->getSignaturesOfType(I)Ljava/util/Iterator;

    move-result-object v2

    .line 1100
    .local v2, "signatures":Ljava/util/Iterator;
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1102
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;

    .line 1104
    .local v1, "sig":Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;
    if-eqz p1, :cond_0

    invoke-virtual {v1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;->getKeyID()J

    move-result-wide v4

    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->getKeyID()J

    move-result-wide v6

    cmp-long v4, v4, v6

    if-nez v4, :cond_1

    .line 1106
    :cond_0
    invoke-virtual {v1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;->getHashedSubPackets()Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;

    move-result-object v0

    .line 1108
    .local v0, "hashed":Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;
    if-eqz v0, :cond_1

    .line 1110
    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;->getKeyFlags()I

    move-result v3

    .line 1117
    .end local v0    # "hashed":Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;
    .end local v1    # "sig":Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;
    :cond_1
    return v3
.end method

.method public getKeyID()J
    .locals 2

    .prologue
    .line 417
    iget-wide v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->keyID:J

    return-wide v0
.end method

.method public getPreferredCompressionAlgorithms()[I
    .locals 4

    .prologue
    .line 1138
    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->getSignatures()Ljava/util/Iterator;

    move-result-object v2

    .line 1140
    .local v2, "signatures":Ljava/util/Iterator;
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1141
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;

    .line 1143
    .local v1, "sig":Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;
    invoke-virtual {v1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;->getHashedSubPackets()Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;

    move-result-object v0

    .line 1145
    .local v0, "hashed":Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;
    if-eqz v0, :cond_0

    .line 1146
    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;->getPreferredCompressionAlgorithms()[I

    move-result-object v3

    .line 1150
    .end local v0    # "hashed":Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;
    .end local v1    # "sig":Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;
    :goto_0
    return-object v3

    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public getPreferredSymmetricAlgorithms()[I
    .locals 4

    .prologue
    .line 1122
    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->getSignatures()Ljava/util/Iterator;

    move-result-object v2

    .line 1124
    .local v2, "signatures":Ljava/util/Iterator;
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1125
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;

    .line 1127
    .local v1, "sig":Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;
    invoke-virtual {v1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;->getHashedSubPackets()Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;

    move-result-object v0

    .line 1129
    .local v0, "hashed":Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;
    if-eqz v0, :cond_0

    .line 1130
    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;->getPreferredSymmetricAlgorithms()[I

    move-result-object v3

    .line 1134
    .end local v0    # "hashed":Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;
    .end local v1    # "sig":Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;
    :goto_0
    return-object v3

    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public getSignatures()Ljava/util/Iterator;
    .locals 3

    .prologue
    .line 667
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->subSigs:Ljava/util/List;

    if-nez v2, :cond_1

    .line 669
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 671
    .local v1, "sigs":Ljava/util/List;
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->keySigs:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 673
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->idSigs:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-eq v0, v2, :cond_0

    .line 675
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->idSigs:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Collection;

    invoke-interface {v1, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 673
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 678
    :cond_0
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 682
    .end local v0    # "i":I
    .end local v1    # "sigs":Ljava/util/List;
    :goto_1
    return-object v2

    :cond_1
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->subSigs:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    goto :goto_1
.end method

.method public getSignaturesForID(Ljava/lang/String;)Ljava/util/Iterator;
    .locals 2
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 604
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->ids:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-eq v0, v1, :cond_1

    .line 606
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->ids:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 608
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->idSigs:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 612
    :goto_1
    return-object v1

    .line 604
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 612
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public getSignaturesForUserAttribute(Lcom/android/sec/org/bouncycastle/openpgp/PGPUserAttributeSubpacketVector;)Ljava/util/Iterator;
    .locals 2
    .param p1, "userAttributes"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPUserAttributeSubpacketVector;

    .prologue
    .line 624
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->ids:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-eq v0, v1, :cond_1

    .line 626
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->ids:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPUserAttributeSubpacketVector;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 628
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->idSigs:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 632
    :goto_1
    return-object v1

    .line 624
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 632
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public getSignaturesOfType(I)Ljava/util/Iterator;
    .locals 4
    .param p1, "signatureType"    # I

    .prologue
    .line 644
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 645
    .local v1, "l":Ljava/util/List;
    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->getSignatures()Ljava/util/Iterator;

    move-result-object v0

    .line 647
    .local v0, "it":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 649
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;

    .line 651
    .local v2, "sig":Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;
    invoke-virtual {v2}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;->getSignatureType()I

    move-result v3

    if-ne v3, p1, :cond_0

    .line 653
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 657
    .end local v2    # "sig":Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;
    :cond_1
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    return-object v3
.end method

.method public getTrustData()[B
    .locals 1

    .prologue
    .line 338
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->trustPk:Lcom/android/sec/org/bouncycastle/bcpg/TrustPacket;

    if-nez v0, :cond_0

    .line 340
    const/4 v0, 0x0

    .line 343
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->trustPk:Lcom/android/sec/org/bouncycastle/bcpg/TrustPacket;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/bcpg/TrustPacket;->getLevelAndTrustAmount()[B

    move-result-object v0

    invoke-static {v0}, Lcom/android/sec/org/bouncycastle/util/Arrays;->clone([B)[B

    move-result-object v0

    goto :goto_0
.end method

.method public getUserAttributes()Ljava/util/Iterator;
    .locals 3

    .prologue
    .line 582
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 584
    .local v1, "temp":Ljava/util/List;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->ids:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-eq v0, v2, :cond_1

    .line 586
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->ids:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    instance-of v2, v2, Lcom/android/sec/org/bouncycastle/openpgp/PGPUserAttributeSubpacketVector;

    if-eqz v2, :cond_0

    .line 588
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->ids:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 584
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 592
    :cond_1
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    return-object v2
.end method

.method public getUserIDs()Ljava/util/Iterator;
    .locals 3

    .prologue
    .line 562
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 564
    .local v1, "temp":Ljava/util/List;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->ids:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-eq v0, v2, :cond_1

    .line 566
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->ids:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    instance-of v2, v2, Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 568
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->ids:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 564
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 572
    :cond_1
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    return-object v2
.end method

.method public getValidDays()I
    .locals 4

    .prologue
    .line 322
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->publicPk:Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;->getVersion()I

    move-result v0

    const/4 v1, 0x3

    if-le v0, v1, :cond_0

    .line 324
    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->getValidSeconds()J

    move-result-wide v0

    const-wide/32 v2, 0x15180

    div-long/2addr v0, v2

    long-to-int v0, v0

    .line 328
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->publicPk:Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;->getValidDays()I

    move-result v0

    goto :goto_0
.end method

.method public getValidSeconds()J
    .locals 10

    .prologue
    const-wide/16 v8, 0x3c

    const-wide/16 v4, 0x0

    .line 352
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->publicPk:Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;

    invoke-virtual {v1}, Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;->getVersion()I

    move-result v1

    const/4 v6, 0x3

    if-le v1, v6, :cond_4

    .line 354
    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->isMasterKey()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 356
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    sget-object v1, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->MASTER_KEY_CERTIFICATION_TYPES:[I

    array-length v1, v1

    if-eq v0, v1, :cond_3

    .line 358
    const/4 v1, 0x1

    sget-object v6, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->MASTER_KEY_CERTIFICATION_TYPES:[I

    aget v6, v6, v0

    invoke-direct {p0, v1, v6}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->getExpirationTimeFromSig(ZI)J

    move-result-wide v2

    .line 360
    .local v2, "seconds":J
    cmp-long v1, v2, v4

    if-ltz v1, :cond_1

    .line 380
    .end local v0    # "i":I
    .end local v2    # "seconds":J
    :cond_0
    :goto_1
    return-wide v2

    .line 356
    .restart local v0    # "i":I
    .restart local v2    # "seconds":J
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 368
    .end local v0    # "i":I
    .end local v2    # "seconds":J
    :cond_2
    const/4 v1, 0x0

    const/16 v6, 0x18

    invoke-direct {p0, v1, v6}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->getExpirationTimeFromSig(ZI)J

    move-result-wide v2

    .line 370
    .restart local v2    # "seconds":J
    cmp-long v1, v2, v4

    if-gez v1, :cond_0

    .end local v2    # "seconds":J
    :cond_3
    move-wide v2, v4

    .line 376
    goto :goto_1

    .line 380
    :cond_4
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->publicPk:Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;

    invoke-virtual {v1}, Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;->getValidDays()I

    move-result v1

    int-to-long v4, v1

    const-wide/16 v6, 0x18

    mul-long/2addr v4, v6

    mul-long/2addr v4, v8

    mul-long v2, v4, v8

    goto :goto_1
.end method

.method public getVersion()I
    .locals 1

    .prologue
    .line 305
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->publicPk:Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;->getVersion()I

    move-result v0

    return v0
.end method

.method public isEncryptionKey()Z
    .locals 9

    .prologue
    const/16 v8, 0x14

    const/16 v7, 0x10

    const/4 v6, 0x2

    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 444
    iget-object v4, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->publicPk:Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;

    invoke-virtual {v4}, Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;->getAlgorithm()I

    move-result v0

    .line 446
    .local v0, "algorithm":I
    iget-object v4, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->publicPk:Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;

    invoke-virtual {v4}, Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;->getVersion()I

    move-result v4

    const/4 v5, 0x3

    if-le v4, v5, :cond_2

    .line 448
    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->getKeyFlags()I

    move-result v1

    .line 450
    .local v1, "keyFlags":I
    if-eq v0, v3, :cond_0

    if-eq v0, v6, :cond_0

    if-eq v0, v7, :cond_0

    if-ne v0, v8, :cond_1

    :cond_0
    and-int/lit8 v4, v1, 0xc

    if-eqz v4, :cond_1

    move v2, v3

    .line 457
    .end local v1    # "keyFlags":I
    :cond_1
    :goto_0
    return v2

    :cond_2
    if-eq v0, v3, :cond_3

    if-eq v0, v6, :cond_3

    if-eq v0, v7, :cond_3

    if-ne v0, v8, :cond_1

    :cond_3
    move v2, v3

    goto :goto_0
.end method

.method public isMasterKey()Z
    .locals 1

    .prologue
    .line 469
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->subSigs:Ljava/util/List;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isRevoked()Z
    .locals 5

    .prologue
    .line 767
    const/4 v0, 0x0

    .line 768
    .local v0, "ns":I
    const/4 v2, 0x0

    .line 770
    .local v2, "revoked":Z
    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->isMasterKey()Z

    move-result v3

    if-eqz v3, :cond_2

    :cond_0
    move v1, v0

    .line 772
    .end local v0    # "ns":I
    .local v1, "ns":I
    :goto_0
    if-nez v2, :cond_1

    iget-object v3, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->keySigs:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v1, v3, :cond_1

    .line 774
    iget-object v3, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->keySigs:Ljava/util/List;

    add-int/lit8 v0, v1, 0x1

    .end local v1    # "ns":I
    .restart local v0    # "ns":I
    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;

    invoke-virtual {v3}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;->getSignatureType()I

    move-result v3

    const/16 v4, 0x20

    if-ne v3, v4, :cond_0

    .line 776
    const/4 v2, 0x1

    move v1, v0

    .end local v0    # "ns":I
    .restart local v1    # "ns":I
    goto :goto_0

    .line 782
    :goto_1
    if-nez v2, :cond_1

    iget-object v3, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->subSigs:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v1, v3, :cond_1

    .line 784
    iget-object v3, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->subSigs:Ljava/util/List;

    add-int/lit8 v0, v1, 0x1

    .end local v1    # "ns":I
    .restart local v0    # "ns":I
    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;

    invoke-virtual {v3}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;->getSignatureType()I

    move-result v3

    const/16 v4, 0x28

    if-ne v3, v4, :cond_2

    .line 786
    const/4 v2, 0x1

    move v1, v0

    .end local v0    # "ns":I
    .restart local v1    # "ns":I
    goto :goto_1

    :cond_1
    move v0, v1

    .line 791
    .end local v1    # "ns":I
    .restart local v0    # "ns":I
    return v2

    :cond_2
    move v1, v0

    .end local v0    # "ns":I
    .restart local v1    # "ns":I
    goto :goto_1
.end method
