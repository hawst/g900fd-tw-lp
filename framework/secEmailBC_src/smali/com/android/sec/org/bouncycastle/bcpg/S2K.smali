.class public Lcom/android/sec/org/bouncycastle/bcpg/S2K;
.super Lcom/android/sec/org/bouncycastle/bcpg/BCPGObject;
.source "S2K.java"


# static fields
.field private static final EXPBIAS:I = 0x6

.field public static final GNU_DUMMY_S2K:I = 0x65

.field public static final SALTED:I = 0x1

.field public static final SALTED_AND_ITERATED:I = 0x3

.field public static final SIMPLE:I


# instance fields
.field algorithm:I

.field itCount:I

.field iv:[B

.field protectionMode:I

.field type:I


# direct methods
.method public constructor <init>(I)V
    .locals 1
    .param p1, "algorithm"    # I

    .prologue
    const/4 v0, -0x1

    .line 60
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGObject;-><init>()V

    .line 21
    iput v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/S2K;->itCount:I

    .line 22
    iput v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/S2K;->protectionMode:I

    .line 61
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/S2K;->type:I

    .line 62
    iput p1, p0, Lcom/android/sec/org/bouncycastle/bcpg/S2K;->algorithm:I

    .line 63
    return-void
.end method

.method public constructor <init>(I[B)V
    .locals 1
    .param p1, "algorithm"    # I
    .param p2, "iv"    # [B

    .prologue
    const/4 v0, -0x1

    .line 68
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGObject;-><init>()V

    .line 21
    iput v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/S2K;->itCount:I

    .line 22
    iput v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/S2K;->protectionMode:I

    .line 69
    const/4 v0, 0x1

    iput v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/S2K;->type:I

    .line 70
    iput p1, p0, Lcom/android/sec/org/bouncycastle/bcpg/S2K;->algorithm:I

    .line 71
    iput-object p2, p0, Lcom/android/sec/org/bouncycastle/bcpg/S2K;->iv:[B

    .line 72
    return-void
.end method

.method public constructor <init>(I[BI)V
    .locals 1
    .param p1, "algorithm"    # I
    .param p2, "iv"    # [B
    .param p3, "itCount"    # I

    .prologue
    const/4 v0, -0x1

    .line 78
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGObject;-><init>()V

    .line 21
    iput v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/S2K;->itCount:I

    .line 22
    iput v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/S2K;->protectionMode:I

    .line 79
    const/4 v0, 0x3

    iput v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/S2K;->type:I

    .line 80
    iput p1, p0, Lcom/android/sec/org/bouncycastle/bcpg/S2K;->algorithm:I

    .line 81
    iput-object p2, p0, Lcom/android/sec/org/bouncycastle/bcpg/S2K;->iv:[B

    .line 82
    iput p3, p0, Lcom/android/sec/org/bouncycastle/bcpg/S2K;->itCount:I

    .line 83
    return-void
.end method

.method constructor <init>(Ljava/io/InputStream;)V
    .locals 4
    .param p1, "in"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, -0x1

    .line 27
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGObject;-><init>()V

    .line 21
    iput v1, p0, Lcom/android/sec/org/bouncycastle/bcpg/S2K;->itCount:I

    .line 22
    iput v1, p0, Lcom/android/sec/org/bouncycastle/bcpg/S2K;->protectionMode:I

    .line 28
    new-instance v0, Ljava/io/DataInputStream;

    invoke-direct {v0, p1}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    .line 30
    .local v0, "dIn":Ljava/io/DataInputStream;
    invoke-virtual {v0}, Ljava/io/DataInputStream;->read()I

    move-result v1

    iput v1, p0, Lcom/android/sec/org/bouncycastle/bcpg/S2K;->type:I

    .line 31
    invoke-virtual {v0}, Ljava/io/DataInputStream;->read()I

    move-result v1

    iput v1, p0, Lcom/android/sec/org/bouncycastle/bcpg/S2K;->algorithm:I

    .line 36
    iget v1, p0, Lcom/android/sec/org/bouncycastle/bcpg/S2K;->type:I

    const/16 v2, 0x65

    if-eq v1, v2, :cond_1

    .line 38
    iget v1, p0, Lcom/android/sec/org/bouncycastle/bcpg/S2K;->type:I

    if-eqz v1, :cond_0

    .line 40
    const/16 v1, 0x8

    new-array v1, v1, [B

    iput-object v1, p0, Lcom/android/sec/org/bouncycastle/bcpg/S2K;->iv:[B

    .line 41
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/bcpg/S2K;->iv:[B

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/android/sec/org/bouncycastle/bcpg/S2K;->iv:[B

    array-length v3, v3

    invoke-virtual {v0, v1, v2, v3}, Ljava/io/DataInputStream;->readFully([BII)V

    .line 43
    iget v1, p0, Lcom/android/sec/org/bouncycastle/bcpg/S2K;->type:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_0

    .line 45
    invoke-virtual {v0}, Ljava/io/DataInputStream;->read()I

    move-result v1

    iput v1, p0, Lcom/android/sec/org/bouncycastle/bcpg/S2K;->itCount:I

    .line 56
    :cond_0
    :goto_0
    return-void

    .line 51
    :cond_1
    invoke-virtual {v0}, Ljava/io/DataInputStream;->read()I

    .line 52
    invoke-virtual {v0}, Ljava/io/DataInputStream;->read()I

    .line 53
    invoke-virtual {v0}, Ljava/io/DataInputStream;->read()I

    .line 54
    invoke-virtual {v0}, Ljava/io/DataInputStream;->read()I

    move-result v1

    iput v1, p0, Lcom/android/sec/org/bouncycastle/bcpg/S2K;->protectionMode:I

    goto :goto_0
.end method


# virtual methods
.method public encode(Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;)V
    .locals 2
    .param p1, "out"    # Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 126
    iget v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/S2K;->type:I

    invoke-virtual {p1, v0}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->write(I)V

    .line 127
    iget v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/S2K;->algorithm:I

    invoke-virtual {p1, v0}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->write(I)V

    .line 129
    iget v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/S2K;->type:I

    const/16 v1, 0x65

    if-eq v0, v1, :cond_2

    .line 131
    iget v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/S2K;->type:I

    if-eqz v0, :cond_0

    .line 133
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/S2K;->iv:[B

    invoke-virtual {p1, v0}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->write([B)V

    .line 136
    :cond_0
    iget v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/S2K;->type:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    .line 138
    iget v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/S2K;->itCount:I

    invoke-virtual {p1, v0}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->write(I)V

    .line 148
    :cond_1
    :goto_0
    return-void

    .line 143
    :cond_2
    const/16 v0, 0x47

    invoke-virtual {p1, v0}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->write(I)V

    .line 144
    const/16 v0, 0x4e

    invoke-virtual {p1, v0}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->write(I)V

    .line 145
    const/16 v0, 0x55

    invoke-virtual {p1, v0}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->write(I)V

    .line 146
    iget v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/S2K;->protectionMode:I

    invoke-virtual {p1, v0}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->write(I)V

    goto :goto_0
.end method

.method public getHashAlgorithm()I
    .locals 1

    .prologue
    .line 95
    iget v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/S2K;->algorithm:I

    return v0
.end method

.method public getIV()[B
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/S2K;->iv:[B

    return-object v0
.end method

.method public getIterationCount()J
    .locals 2

    .prologue
    .line 111
    iget v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/S2K;->itCount:I

    and-int/lit8 v0, v0, 0xf

    add-int/lit8 v0, v0, 0x10

    iget v1, p0, Lcom/android/sec/org/bouncycastle/bcpg/S2K;->itCount:I

    shr-int/lit8 v1, v1, 0x4

    add-int/lit8 v1, v1, 0x6

    shl-int/2addr v0, v1

    int-to-long v0, v0

    return-wide v0
.end method

.method public getProtectionMode()I
    .locals 1

    .prologue
    .line 119
    iget v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/S2K;->protectionMode:I

    return v0
.end method

.method public getType()I
    .locals 1

    .prologue
    .line 87
    iget v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/S2K;->type:I

    return v0
.end method
