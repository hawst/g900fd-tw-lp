.class public Lcom/android/sec/org/bouncycastle/asn1/misc/CAST5CBCParameters;
.super Lcom/android/sec/org/bouncycastle/asn1/ASN1Object;
.source "CAST5CBCParameters.java"


# instance fields
.field iv:Lcom/android/sec/org/bouncycastle/asn1/ASN1OctetString;

.field keyLength:Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;


# direct methods
.method public constructor <init>(Lcom/android/sec/org/bouncycastle/asn1/ASN1Sequence;)V
    .locals 1
    .param p1, "paramASN1Sequence"    # Lcom/android/sec/org/bouncycastle/asn1/ASN1Sequence;

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Object;-><init>()V

    .line 38
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Sequence;->getObjectAt(I)Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;

    move-result-object v0

    check-cast v0, Lcom/android/sec/org/bouncycastle/asn1/ASN1OctetString;

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/asn1/misc/CAST5CBCParameters;->iv:Lcom/android/sec/org/bouncycastle/asn1/ASN1OctetString;

    .line 39
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Sequence;->getObjectAt(I)Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;

    move-result-object v0

    check-cast v0, Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/asn1/misc/CAST5CBCParameters;->keyLength:Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;

    .line 40
    return-void
.end method

.method public constructor <init>([BI)V
    .locals 4
    .param p1, "paramArrayOfByte"    # [B
    .param p2, "paramInt"    # I

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Object;-><init>()V

    .line 32
    new-instance v0, Lcom/android/sec/org/bouncycastle/asn1/DEROctetString;

    invoke-direct {v0, p1}, Lcom/android/sec/org/bouncycastle/asn1/DEROctetString;-><init>([B)V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/asn1/misc/CAST5CBCParameters;->iv:Lcom/android/sec/org/bouncycastle/asn1/ASN1OctetString;

    .line 33
    new-instance v0, Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;

    int-to-long v2, p2

    invoke-direct {v0, v2, v3}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;-><init>(J)V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/asn1/misc/CAST5CBCParameters;->keyLength:Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;

    .line 34
    return-void
.end method

.method public static getInstance(Ljava/lang/Object;)Lcom/android/sec/org/bouncycastle/asn1/misc/CAST5CBCParameters;
    .locals 2
    .param p0, "paramObject"    # Ljava/lang/Object;

    .prologue
    .line 21
    instance-of v0, p0, Lcom/android/sec/org/bouncycastle/asn1/misc/CAST5CBCParameters;

    if-eqz v0, :cond_0

    .line 22
    check-cast p0, Lcom/android/sec/org/bouncycastle/asn1/misc/CAST5CBCParameters;

    .line 27
    .end local p0    # "paramObject":Ljava/lang/Object;
    :goto_0
    return-object p0

    .line 24
    .restart local p0    # "paramObject":Ljava/lang/Object;
    :cond_0
    if-eqz p0, :cond_1

    .line 25
    new-instance v0, Lcom/android/sec/org/bouncycastle/asn1/misc/CAST5CBCParameters;

    invoke-static {p0}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Sequence;->getInstance(Ljava/lang/Object;)Lcom/android/sec/org/bouncycastle/asn1/ASN1Sequence;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/sec/org/bouncycastle/asn1/misc/CAST5CBCParameters;-><init>(Lcom/android/sec/org/bouncycastle/asn1/ASN1Sequence;)V

    move-object p0, v0

    goto :goto_0

    .line 27
    :cond_1
    const/4 p0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getIV()[B
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/asn1/misc/CAST5CBCParameters;->iv:Lcom/android/sec/org/bouncycastle/asn1/ASN1OctetString;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/ASN1OctetString;->getOctets()[B

    move-result-object v0

    return-object v0
.end method

.method public getKeyLength()I
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/asn1/misc/CAST5CBCParameters;->keyLength:Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;->getValue()Ljava/math/BigInteger;

    move-result-object v0

    invoke-virtual {v0}, Ljava/math/BigInteger;->intValue()I

    move-result v0

    return v0
.end method

.method public toASN1Primitive()Lcom/android/sec/org/bouncycastle/asn1/ASN1Primitive;
    .locals 2

    .prologue
    .line 54
    new-instance v0, Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;

    invoke-direct {v0}, Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;-><init>()V

    .line 55
    .local v0, "localASN1EncodableVector":Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/asn1/misc/CAST5CBCParameters;->iv:Lcom/android/sec/org/bouncycastle/asn1/ASN1OctetString;

    invoke-virtual {v0, v1}, Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;->add(Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;)V

    .line 56
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/asn1/misc/CAST5CBCParameters;->keyLength:Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;

    invoke-virtual {v0, v1}, Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;->add(Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;)V

    .line 57
    new-instance v1, Lcom/android/sec/org/bouncycastle/asn1/DERSequence;

    invoke-direct {v1, v0}, Lcom/android/sec/org/bouncycastle/asn1/DERSequence;-><init>(Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;)V

    return-object v1
.end method
