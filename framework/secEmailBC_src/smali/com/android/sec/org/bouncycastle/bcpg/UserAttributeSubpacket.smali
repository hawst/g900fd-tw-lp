.class public Lcom/android/sec/org/bouncycastle/bcpg/UserAttributeSubpacket;
.super Ljava/lang/Object;
.source "UserAttributeSubpacket.java"


# instance fields
.field protected data:[B

.field type:I


# direct methods
.method protected constructor <init>(I[B)V
    .locals 0
    .param p1, "type"    # I
    .param p2, "data"    # [B

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput p1, p0, Lcom/android/sec/org/bouncycastle/bcpg/UserAttributeSubpacket;->type:I

    .line 22
    iput-object p2, p0, Lcom/android/sec/org/bouncycastle/bcpg/UserAttributeSubpacket;->data:[B

    .line 23
    return-void
.end method


# virtual methods
.method public encode(Ljava/io/OutputStream;)V
    .locals 2
    .param p1, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 42
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/bcpg/UserAttributeSubpacket;->data:[B

    array-length v1, v1

    add-int/lit8 v0, v1, 0x1

    .line 44
    .local v0, "bodyLen":I
    const/16 v1, 0xc0

    if-ge v0, v1, :cond_0

    .line 46
    int-to-byte v1, v0

    invoke-virtual {p1, v1}, Ljava/io/OutputStream;->write(I)V

    .line 64
    :goto_0
    iget v1, p0, Lcom/android/sec/org/bouncycastle/bcpg/UserAttributeSubpacket;->type:I

    invoke-virtual {p1, v1}, Ljava/io/OutputStream;->write(I)V

    .line 65
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/bcpg/UserAttributeSubpacket;->data:[B

    invoke-virtual {p1, v1}, Ljava/io/OutputStream;->write([B)V

    .line 66
    return-void

    .line 48
    :cond_0
    const/16 v1, 0x20bf

    if-gt v0, v1, :cond_1

    .line 50
    add-int/lit16 v0, v0, -0xc0

    .line 52
    shr-int/lit8 v1, v0, 0x8

    and-int/lit16 v1, v1, 0xff

    add-int/lit16 v1, v1, 0xc0

    int-to-byte v1, v1

    invoke-virtual {p1, v1}, Ljava/io/OutputStream;->write(I)V

    .line 53
    int-to-byte v1, v0

    invoke-virtual {p1, v1}, Ljava/io/OutputStream;->write(I)V

    goto :goto_0

    .line 57
    :cond_1
    const/16 v1, 0xff

    invoke-virtual {p1, v1}, Ljava/io/OutputStream;->write(I)V

    .line 58
    shr-int/lit8 v1, v0, 0x18

    int-to-byte v1, v1

    invoke-virtual {p1, v1}, Ljava/io/OutputStream;->write(I)V

    .line 59
    shr-int/lit8 v1, v0, 0x10

    int-to-byte v1, v1

    invoke-virtual {p1, v1}, Ljava/io/OutputStream;->write(I)V

    .line 60
    shr-int/lit8 v1, v0, 0x8

    int-to-byte v1, v1

    invoke-virtual {p1, v1}, Ljava/io/OutputStream;->write(I)V

    .line 61
    int-to-byte v1, v0

    invoke-virtual {p1, v1}, Ljava/io/OutputStream;->write(I)V

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 71
    if-ne p1, p0, :cond_1

    .line 83
    :cond_0
    :goto_0
    return v1

    .line 76
    :cond_1
    instance-of v3, p1, Lcom/android/sec/org/bouncycastle/bcpg/UserAttributeSubpacket;

    if-nez v3, :cond_2

    move v1, v2

    .line 78
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 81
    check-cast v0, Lcom/android/sec/org/bouncycastle/bcpg/UserAttributeSubpacket;

    .line 83
    .local v0, "other":Lcom/android/sec/org/bouncycastle/bcpg/UserAttributeSubpacket;
    iget v3, p0, Lcom/android/sec/org/bouncycastle/bcpg/UserAttributeSubpacket;->type:I

    iget v4, v0, Lcom/android/sec/org/bouncycastle/bcpg/UserAttributeSubpacket;->type:I

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lcom/android/sec/org/bouncycastle/bcpg/UserAttributeSubpacket;->data:[B

    iget-object v4, v0, Lcom/android/sec/org/bouncycastle/bcpg/UserAttributeSubpacket;->data:[B

    invoke-static {v3, v4}, Lcom/android/sec/org/bouncycastle/util/Arrays;->areEqual([B[B)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public getData()[B
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/UserAttributeSubpacket;->data:[B

    return-object v0
.end method

.method public getType()I
    .locals 1

    .prologue
    .line 27
    iget v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/UserAttributeSubpacket;->type:I

    return v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 89
    iget v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/UserAttributeSubpacket;->type:I

    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/bcpg/UserAttributeSubpacket;->data:[B

    invoke-static {v1}, Lcom/android/sec/org/bouncycastle/util/Arrays;->hashCode([B)I

    move-result v1

    xor-int/2addr v0, v1

    return v0
.end method
