.class public Lcom/android/sec/org/bouncycastle/bcpg/sig/SignatureCreationTime;
.super Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;
.source "SignatureCreationTime.java"


# direct methods
.method public constructor <init>(ZLjava/util/Date;)V
    .locals 2
    .param p1, "critical"    # Z
    .param p2, "date"    # Ljava/util/Date;

    .prologue
    .line 39
    const/4 v0, 0x2

    invoke-static {p2}, Lcom/android/sec/org/bouncycastle/bcpg/sig/SignatureCreationTime;->timeToBytes(Ljava/util/Date;)[B

    move-result-object v1

    invoke-direct {p0, v0, p1, v1}, Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;-><init>(IZ[B)V

    .line 40
    return-void
.end method

.method public constructor <init>(Z[B)V
    .locals 1
    .param p1, "critical"    # Z
    .param p2, "data"    # [B

    .prologue
    .line 32
    const/4 v0, 0x2

    invoke-direct {p0, v0, p1, p2}, Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;-><init>(IZ[B)V

    .line 33
    return-void
.end method

.method protected static timeToBytes(Ljava/util/Date;)[B
    .locals 8
    .param p0, "date"    # Ljava/util/Date;

    .prologue
    .line 17
    const/4 v1, 0x4

    new-array v0, v1, [B

    .line 18
    .local v0, "data":[B
    invoke-virtual {p0}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    div-long v2, v4, v6

    .line 20
    .local v2, "t":J
    const/4 v1, 0x0

    const/16 v4, 0x18

    shr-long v4, v2, v4

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v0, v1

    .line 21
    const/4 v1, 0x1

    const/16 v4, 0x10

    shr-long v4, v2, v4

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v0, v1

    .line 22
    const/4 v1, 0x2

    const/16 v4, 0x8

    shr-long v4, v2, v4

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v0, v1

    .line 23
    const/4 v1, 0x3

    long-to-int v4, v2

    int-to-byte v4, v4

    aput-byte v4, v0, v1

    .line 25
    return-object v0
.end method


# virtual methods
.method public getTime()Ljava/util/Date;
    .locals 6

    .prologue
    .line 44
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/sig/SignatureCreationTime;->data:[B

    const/4 v3, 0x0

    aget-byte v2, v2, v3

    and-int/lit16 v2, v2, 0xff

    int-to-long v2, v2

    const/16 v4, 0x18

    shl-long/2addr v2, v4

    iget-object v4, p0, Lcom/android/sec/org/bouncycastle/bcpg/sig/SignatureCreationTime;->data:[B

    const/4 v5, 0x1

    aget-byte v4, v4, v5

    and-int/lit16 v4, v4, 0xff

    shl-int/lit8 v4, v4, 0x10

    int-to-long v4, v4

    or-long/2addr v2, v4

    iget-object v4, p0, Lcom/android/sec/org/bouncycastle/bcpg/sig/SignatureCreationTime;->data:[B

    const/4 v5, 0x2

    aget-byte v4, v4, v5

    and-int/lit16 v4, v4, 0xff

    shl-int/lit8 v4, v4, 0x8

    int-to-long v4, v4

    or-long/2addr v2, v4

    iget-object v4, p0, Lcom/android/sec/org/bouncycastle/bcpg/sig/SignatureCreationTime;->data:[B

    const/4 v5, 0x3

    aget-byte v4, v4, v5

    and-int/lit16 v4, v4, 0xff

    int-to-long v4, v4

    or-long v0, v2, v4

    .line 46
    .local v0, "time":J
    new-instance v2, Ljava/util/Date;

    const-wide/16 v4, 0x3e8

    mul-long/2addr v4, v0

    invoke-direct {v2, v4, v5}, Ljava/util/Date;-><init>(J)V

    return-object v2
.end method
