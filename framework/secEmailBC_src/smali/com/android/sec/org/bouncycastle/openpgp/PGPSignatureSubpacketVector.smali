.class public Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;
.super Ljava/lang/Object;
.source "PGPSignatureSubpacketVector.java"


# instance fields
.field packets:[Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;


# direct methods
.method constructor <init>([Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;)V
    .locals 0
    .param p1, "packets"    # [Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;->packets:[Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;

    .line 30
    return-void
.end method


# virtual methods
.method public getCriticalTags()[I
    .locals 5

    .prologue
    .line 225
    const/4 v0, 0x0

    .line 227
    .local v0, "count":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v4, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;->packets:[Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;

    array-length v4, v4

    if-eq v2, v4, :cond_1

    .line 229
    iget-object v4, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;->packets:[Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;

    aget-object v4, v4, v2

    invoke-virtual {v4}, Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;->isCritical()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 231
    add-int/lit8 v0, v0, 0x1

    .line 227
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 235
    :cond_1
    new-array v3, v0, [I

    .line 237
    .local v3, "list":[I
    const/4 v0, 0x0

    .line 239
    const/4 v2, 0x0

    :goto_1
    iget-object v4, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;->packets:[Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;

    array-length v4, v4

    if-eq v2, v4, :cond_3

    .line 241
    iget-object v4, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;->packets:[Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;

    aget-object v4, v4, v2

    invoke-virtual {v4}, Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;->isCritical()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 243
    add-int/lit8 v1, v0, 0x1

    .end local v0    # "count":I
    .local v1, "count":I
    iget-object v4, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;->packets:[Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;

    aget-object v4, v4, v2

    invoke-virtual {v4}, Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;->getType()I

    move-result v4

    aput v4, v3, v0

    move v0, v1

    .line 239
    .end local v1    # "count":I
    .restart local v0    # "count":I
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 247
    :cond_3
    return-object v3
.end method

.method public getIssuerKeyID()J
    .locals 4

    .prologue
    .line 93
    const/16 v1, 0x10

    invoke-virtual {p0, v1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;->getSubpacket(I)Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;

    move-result-object v0

    .line 95
    .local v0, "p":Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;
    if-nez v0, :cond_0

    .line 97
    const-wide/16 v2, 0x0

    .line 100
    .end local v0    # "p":Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;
    :goto_0
    return-wide v2

    .restart local v0    # "p":Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;
    :cond_0
    check-cast v0, Lcom/android/sec/org/bouncycastle/bcpg/sig/IssuerKeyID;

    .end local v0    # "p":Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;
    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/bcpg/sig/IssuerKeyID;->getKeyID()J

    move-result-wide v2

    goto :goto_0
.end method

.method public getKeyExpirationTime()J
    .locals 4

    .prologue
    .line 141
    const/16 v1, 0x9

    invoke-virtual {p0, v1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;->getSubpacket(I)Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;

    move-result-object v0

    .line 143
    .local v0, "p":Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;
    if-nez v0, :cond_0

    .line 145
    const-wide/16 v2, 0x0

    .line 148
    .end local v0    # "p":Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;
    :goto_0
    return-wide v2

    .restart local v0    # "p":Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;
    :cond_0
    check-cast v0, Lcom/android/sec/org/bouncycastle/bcpg/sig/KeyExpirationTime;

    .end local v0    # "p":Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;
    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/bcpg/sig/KeyExpirationTime;->getTime()J

    move-result-wide v2

    goto :goto_0
.end method

.method public getKeyFlags()I
    .locals 2

    .prologue
    .line 189
    const/16 v1, 0x1b

    invoke-virtual {p0, v1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;->getSubpacket(I)Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;

    move-result-object v0

    .line 191
    .local v0, "p":Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;
    if-nez v0, :cond_0

    .line 193
    const/4 v1, 0x0

    .line 196
    .end local v0    # "p":Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;
    :goto_0
    return v1

    .restart local v0    # "p":Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;
    :cond_0
    check-cast v0, Lcom/android/sec/org/bouncycastle/bcpg/sig/KeyFlags;

    .end local v0    # "p":Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;
    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/bcpg/sig/KeyFlags;->getFlags()I

    move-result v1

    goto :goto_0
.end method

.method public getNotationDataOccurences()[Lcom/android/sec/org/bouncycastle/bcpg/sig/NotationData;
    .locals 4

    .prologue
    .line 81
    const/16 v3, 0x14

    invoke-virtual {p0, v3}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;->getSubpackets(I)[Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;

    move-result-object v1

    .line 82
    .local v1, "notations":[Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;
    array-length v3, v1

    new-array v2, v3, [Lcom/android/sec/org/bouncycastle/bcpg/sig/NotationData;

    .line 83
    .local v2, "vals":[Lcom/android/sec/org/bouncycastle/bcpg/sig/NotationData;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v3, v1

    if-ge v0, v3, :cond_0

    .line 85
    aget-object v3, v1, v0

    check-cast v3, Lcom/android/sec/org/bouncycastle/bcpg/sig/NotationData;

    aput-object v3, v2, v0

    .line 83
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 88
    :cond_0
    return-object v2
.end method

.method public getPreferredCompressionAlgorithms()[I
    .locals 2

    .prologue
    .line 177
    const/16 v1, 0x16

    invoke-virtual {p0, v1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;->getSubpacket(I)Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;

    move-result-object v0

    .line 179
    .local v0, "p":Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;
    if-nez v0, :cond_0

    .line 181
    const/4 v1, 0x0

    .line 184
    .end local v0    # "p":Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;
    :goto_0
    return-object v1

    .restart local v0    # "p":Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;
    :cond_0
    check-cast v0, Lcom/android/sec/org/bouncycastle/bcpg/sig/PreferredAlgorithms;

    .end local v0    # "p":Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;
    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/bcpg/sig/PreferredAlgorithms;->getPreferences()[I

    move-result-object v1

    goto :goto_0
.end method

.method public getPreferredHashAlgorithms()[I
    .locals 2

    .prologue
    .line 153
    const/16 v1, 0x15

    invoke-virtual {p0, v1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;->getSubpacket(I)Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;

    move-result-object v0

    .line 155
    .local v0, "p":Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;
    if-nez v0, :cond_0

    .line 157
    const/4 v1, 0x0

    .line 160
    .end local v0    # "p":Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;
    :goto_0
    return-object v1

    .restart local v0    # "p":Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;
    :cond_0
    check-cast v0, Lcom/android/sec/org/bouncycastle/bcpg/sig/PreferredAlgorithms;

    .end local v0    # "p":Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;
    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/bcpg/sig/PreferredAlgorithms;->getPreferences()[I

    move-result-object v1

    goto :goto_0
.end method

.method public getPreferredSymmetricAlgorithms()[I
    .locals 2

    .prologue
    .line 165
    const/16 v1, 0xb

    invoke-virtual {p0, v1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;->getSubpacket(I)Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;

    move-result-object v0

    .line 167
    .local v0, "p":Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;
    if-nez v0, :cond_0

    .line 169
    const/4 v1, 0x0

    .line 172
    .end local v0    # "p":Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;
    :goto_0
    return-object v1

    .restart local v0    # "p":Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;
    :cond_0
    check-cast v0, Lcom/android/sec/org/bouncycastle/bcpg/sig/PreferredAlgorithms;

    .end local v0    # "p":Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;
    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/bcpg/sig/PreferredAlgorithms;->getPreferences()[I

    move-result-object v1

    goto :goto_0
.end method

.method public getSignatureCreationTime()Ljava/util/Date;
    .locals 2

    .prologue
    .line 105
    const/4 v1, 0x2

    invoke-virtual {p0, v1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;->getSubpacket(I)Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;

    move-result-object v0

    .line 107
    .local v0, "p":Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;
    if-nez v0, :cond_0

    .line 109
    const/4 v1, 0x0

    .line 112
    .end local v0    # "p":Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;
    :goto_0
    return-object v1

    .restart local v0    # "p":Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;
    :cond_0
    check-cast v0, Lcom/android/sec/org/bouncycastle/bcpg/sig/SignatureCreationTime;

    .end local v0    # "p":Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;
    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/bcpg/sig/SignatureCreationTime;->getTime()Ljava/util/Date;

    move-result-object v1

    goto :goto_0
.end method

.method public getSignatureExpirationTime()J
    .locals 4

    .prologue
    .line 123
    const/4 v1, 0x3

    invoke-virtual {p0, v1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;->getSubpacket(I)Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;

    move-result-object v0

    .line 125
    .local v0, "p":Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;
    if-nez v0, :cond_0

    .line 127
    const-wide/16 v2, 0x0

    .line 130
    .end local v0    # "p":Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;
    :goto_0
    return-wide v2

    .restart local v0    # "p":Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;
    :cond_0
    check-cast v0, Lcom/android/sec/org/bouncycastle/bcpg/sig/SignatureExpirationTime;

    .end local v0    # "p":Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;
    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/bcpg/sig/SignatureExpirationTime;->getTime()J

    move-result-wide v2

    goto :goto_0
.end method

.method public getSignerUserID()Ljava/lang/String;
    .locals 2

    .prologue
    .line 201
    const/16 v1, 0x1c

    invoke-virtual {p0, v1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;->getSubpacket(I)Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;

    move-result-object v0

    .line 203
    .local v0, "p":Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;
    if-nez v0, :cond_0

    .line 205
    const/4 v1, 0x0

    .line 208
    .end local v0    # "p":Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;
    :goto_0
    return-object v1

    .restart local v0    # "p":Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;
    :cond_0
    check-cast v0, Lcom/android/sec/org/bouncycastle/bcpg/sig/SignerUserID;

    .end local v0    # "p":Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;
    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/bcpg/sig/SignerUserID;->getID()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public getSubpacket(I)Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;
    .locals 2
    .param p1, "type"    # I

    .prologue
    .line 35
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;->packets:[Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;

    array-length v1, v1

    if-eq v0, v1, :cond_1

    .line 37
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;->packets:[Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;->getType()I

    move-result v1

    if-ne v1, p1, :cond_0

    .line 39
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;->packets:[Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;

    aget-object v1, v1, v0

    .line 43
    :goto_1
    return-object v1

    .line 35
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 43
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public getSubpackets(I)[Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;
    .locals 3
    .param p1, "type"    # I

    .prologue
    .line 66
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 68
    .local v1, "list":Ljava/util/List;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;->packets:[Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;

    array-length v2, v2

    if-eq v0, v2, :cond_1

    .line 70
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;->packets:[Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;->getType()I

    move-result v2

    if-ne v2, p1, :cond_0

    .line 72
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;->packets:[Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 68
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 76
    :cond_1
    const/4 v2, 0x0

    new-array v2, v2, [Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;

    invoke-interface {v1, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;

    check-cast v2, [Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;

    return-object v2
.end method

.method public hasSubpacket(I)Z
    .locals 1
    .param p1, "type"    # I

    .prologue
    .line 55
    invoke-virtual {p0, p1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;->getSubpacket(I)Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isPrimaryUserID()Z
    .locals 2

    .prologue
    .line 213
    const/16 v1, 0x19

    invoke-virtual {p0, v1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;->getSubpacket(I)Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;

    move-result-object v0

    check-cast v0, Lcom/android/sec/org/bouncycastle/bcpg/sig/PrimaryUserID;

    .line 215
    .local v0, "primaryId":Lcom/android/sec/org/bouncycastle/bcpg/sig/PrimaryUserID;
    if-eqz v0, :cond_0

    .line 217
    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/bcpg/sig/PrimaryUserID;->isPrimaryUserID()Z

    move-result v1

    .line 220
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 257
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;->packets:[Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;

    array-length v0, v0

    return v0
.end method

.method toSubpacketArray()[Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;
    .locals 1

    .prologue
    .line 262
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;->packets:[Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;

    return-object v0
.end method
