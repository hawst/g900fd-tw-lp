.class public Lcom/android/sec/org/bouncycastle/jcajce/provider/symmetric/util/BaseMac;
.super Ljavax/crypto/MacSpi;
.source "BaseMac.java"

# interfaces
.implements Lcom/android/sec/org/bouncycastle/jcajce/provider/symmetric/util/PBE;


# instance fields
.field private keySize:I

.field private macEngine:Lcom/android/sec/org/bouncycastle/crypto/Mac;

.field private pbeHash:I

.field private pbeType:I


# direct methods
.method protected constructor <init>(Lcom/android/sec/org/bouncycastle/crypto/Mac;)V
    .locals 1
    .param p1, "macEngine"    # Lcom/android/sec/org/bouncycastle/crypto/Mac;

    .prologue
    .line 28
    invoke-direct {p0}, Ljavax/crypto/MacSpi;-><init>()V

    .line 22
    const/4 v0, 0x2

    iput v0, p0, Lcom/android/sec/org/bouncycastle/jcajce/provider/symmetric/util/BaseMac;->pbeType:I

    .line 23
    const/4 v0, 0x1

    iput v0, p0, Lcom/android/sec/org/bouncycastle/jcajce/provider/symmetric/util/BaseMac;->pbeHash:I

    .line 24
    const/16 v0, 0xa0

    iput v0, p0, Lcom/android/sec/org/bouncycastle/jcajce/provider/symmetric/util/BaseMac;->keySize:I

    .line 29
    iput-object p1, p0, Lcom/android/sec/org/bouncycastle/jcajce/provider/symmetric/util/BaseMac;->macEngine:Lcom/android/sec/org/bouncycastle/crypto/Mac;

    .line 30
    return-void
.end method

.method protected constructor <init>(Lcom/android/sec/org/bouncycastle/crypto/Mac;III)V
    .locals 1
    .param p1, "macEngine"    # Lcom/android/sec/org/bouncycastle/crypto/Mac;
    .param p2, "pbeType"    # I
    .param p3, "pbeHash"    # I
    .param p4, "keySize"    # I

    .prologue
    .line 37
    invoke-direct {p0}, Ljavax/crypto/MacSpi;-><init>()V

    .line 22
    const/4 v0, 0x2

    iput v0, p0, Lcom/android/sec/org/bouncycastle/jcajce/provider/symmetric/util/BaseMac;->pbeType:I

    .line 23
    const/4 v0, 0x1

    iput v0, p0, Lcom/android/sec/org/bouncycastle/jcajce/provider/symmetric/util/BaseMac;->pbeHash:I

    .line 24
    const/16 v0, 0xa0

    iput v0, p0, Lcom/android/sec/org/bouncycastle/jcajce/provider/symmetric/util/BaseMac;->keySize:I

    .line 38
    iput-object p1, p0, Lcom/android/sec/org/bouncycastle/jcajce/provider/symmetric/util/BaseMac;->macEngine:Lcom/android/sec/org/bouncycastle/crypto/Mac;

    .line 39
    iput p2, p0, Lcom/android/sec/org/bouncycastle/jcajce/provider/symmetric/util/BaseMac;->pbeType:I

    .line 40
    iput p3, p0, Lcom/android/sec/org/bouncycastle/jcajce/provider/symmetric/util/BaseMac;->pbeHash:I

    .line 41
    iput p4, p0, Lcom/android/sec/org/bouncycastle/jcajce/provider/symmetric/util/BaseMac;->keySize:I

    .line 42
    return-void
.end method


# virtual methods
.method protected engineDoFinal()[B
    .locals 3

    .prologue
    .line 115
    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/jcajce/provider/symmetric/util/BaseMac;->engineGetMacLength()I

    move-result v1

    new-array v0, v1, [B

    .line 117
    .local v0, "out":[B
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/jcajce/provider/symmetric/util/BaseMac;->macEngine:Lcom/android/sec/org/bouncycastle/crypto/Mac;

    const/4 v2, 0x0

    invoke-interface {v1, v0, v2}, Lcom/android/sec/org/bouncycastle/crypto/Mac;->doFinal([BI)I

    .line 119
    return-object v0
.end method

.method protected engineGetMacLength()I
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/jcajce/provider/symmetric/util/BaseMac;->macEngine:Lcom/android/sec/org/bouncycastle/crypto/Mac;

    invoke-interface {v0}, Lcom/android/sec/org/bouncycastle/crypto/Mac;->getMacSize()I

    move-result v0

    return v0
.end method

.method protected engineInit(Ljava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;)V
    .locals 4
    .param p1, "key"    # Ljava/security/Key;
    .param p2, "params"    # Ljava/security/spec/AlgorithmParameterSpec;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/InvalidKeyException;,
            Ljava/security/InvalidAlgorithmParameterException;
        }
    .end annotation

    .prologue
    .line 51
    if-nez p1, :cond_0

    .line 53
    new-instance v2, Ljava/security/InvalidKeyException;

    const-string v3, "key is null"

    invoke-direct {v2, v3}, Ljava/security/InvalidKeyException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 56
    :cond_0
    instance-of v2, p1, Lcom/android/sec/org/bouncycastle/jcajce/provider/symmetric/util/BCPBEKey;

    if-eqz v2, :cond_3

    move-object v0, p1

    .line 58
    check-cast v0, Lcom/android/sec/org/bouncycastle/jcajce/provider/symmetric/util/BCPBEKey;

    .line 60
    .local v0, "k":Lcom/android/sec/org/bouncycastle/jcajce/provider/symmetric/util/BCPBEKey;
    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/jcajce/provider/symmetric/util/BCPBEKey;->getParam()Lcom/android/sec/org/bouncycastle/crypto/CipherParameters;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 62
    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/jcajce/provider/symmetric/util/BCPBEKey;->getParam()Lcom/android/sec/org/bouncycastle/crypto/CipherParameters;

    move-result-object v1

    .line 86
    .end local v0    # "k":Lcom/android/sec/org/bouncycastle/jcajce/provider/symmetric/util/BCPBEKey;
    .end local p2    # "params":Ljava/security/spec/AlgorithmParameterSpec;
    .local v1, "param":Lcom/android/sec/org/bouncycastle/crypto/CipherParameters;
    :goto_0
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/jcajce/provider/symmetric/util/BaseMac;->macEngine:Lcom/android/sec/org/bouncycastle/crypto/Mac;

    invoke-interface {v2, v1}, Lcom/android/sec/org/bouncycastle/crypto/Mac;->init(Lcom/android/sec/org/bouncycastle/crypto/CipherParameters;)V

    .line 87
    return-void

    .line 64
    .end local v1    # "param":Lcom/android/sec/org/bouncycastle/crypto/CipherParameters;
    .restart local v0    # "k":Lcom/android/sec/org/bouncycastle/jcajce/provider/symmetric/util/BCPBEKey;
    .restart local p2    # "params":Ljava/security/spec/AlgorithmParameterSpec;
    :cond_1
    instance-of v2, p2, Ljavax/crypto/spec/PBEParameterSpec;

    if-eqz v2, :cond_2

    .line 66
    invoke-static {v0, p2}, Lcom/android/sec/org/bouncycastle/jcajce/provider/symmetric/util/PBE$Util;->makePBEMacParameters(Lcom/android/sec/org/bouncycastle/jcajce/provider/symmetric/util/BCPBEKey;Ljava/security/spec/AlgorithmParameterSpec;)Lcom/android/sec/org/bouncycastle/crypto/CipherParameters;

    move-result-object v1

    .restart local v1    # "param":Lcom/android/sec/org/bouncycastle/crypto/CipherParameters;
    goto :goto_0

    .line 70
    .end local v1    # "param":Lcom/android/sec/org/bouncycastle/crypto/CipherParameters;
    :cond_2
    new-instance v2, Ljava/security/InvalidAlgorithmParameterException;

    const-string v3, "PBE requires PBE parameters to be set."

    invoke-direct {v2, v3}, Ljava/security/InvalidAlgorithmParameterException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 73
    .end local v0    # "k":Lcom/android/sec/org/bouncycastle/jcajce/provider/symmetric/util/BCPBEKey;
    :cond_3
    instance-of v2, p2, Ljavax/crypto/spec/IvParameterSpec;

    if-eqz v2, :cond_4

    .line 75
    new-instance v1, Lcom/android/sec/org/bouncycastle/crypto/params/ParametersWithIV;

    new-instance v2, Lcom/android/sec/org/bouncycastle/crypto/params/KeyParameter;

    invoke-interface {p1}, Ljava/security/Key;->getEncoded()[B

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/android/sec/org/bouncycastle/crypto/params/KeyParameter;-><init>([B)V

    check-cast p2, Ljavax/crypto/spec/IvParameterSpec;

    .end local p2    # "params":Ljava/security/spec/AlgorithmParameterSpec;
    invoke-virtual {p2}, Ljavax/crypto/spec/IvParameterSpec;->getIV()[B

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/android/sec/org/bouncycastle/crypto/params/ParametersWithIV;-><init>(Lcom/android/sec/org/bouncycastle/crypto/CipherParameters;[B)V

    .restart local v1    # "param":Lcom/android/sec/org/bouncycastle/crypto/CipherParameters;
    goto :goto_0

    .line 77
    .end local v1    # "param":Lcom/android/sec/org/bouncycastle/crypto/CipherParameters;
    .restart local p2    # "params":Ljava/security/spec/AlgorithmParameterSpec;
    :cond_4
    if-nez p2, :cond_5

    .line 79
    new-instance v1, Lcom/android/sec/org/bouncycastle/crypto/params/KeyParameter;

    invoke-interface {p1}, Ljava/security/Key;->getEncoded()[B

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/android/sec/org/bouncycastle/crypto/params/KeyParameter;-><init>([B)V

    .restart local v1    # "param":Lcom/android/sec/org/bouncycastle/crypto/CipherParameters;
    goto :goto_0

    .line 83
    .end local v1    # "param":Lcom/android/sec/org/bouncycastle/crypto/CipherParameters;
    :cond_5
    new-instance v2, Ljava/security/InvalidAlgorithmParameterException;

    const-string v3, "unknown parameter type."

    invoke-direct {v2, v3}, Ljava/security/InvalidAlgorithmParameterException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method protected engineReset()V
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/jcajce/provider/symmetric/util/BaseMac;->macEngine:Lcom/android/sec/org/bouncycastle/crypto/Mac;

    invoke-interface {v0}, Lcom/android/sec/org/bouncycastle/crypto/Mac;->reset()V

    .line 97
    return-void
.end method

.method protected engineUpdate(B)V
    .locals 1
    .param p1, "input"    # B

    .prologue
    .line 102
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/jcajce/provider/symmetric/util/BaseMac;->macEngine:Lcom/android/sec/org/bouncycastle/crypto/Mac;

    invoke-interface {v0, p1}, Lcom/android/sec/org/bouncycastle/crypto/Mac;->update(B)V

    .line 103
    return-void
.end method

.method protected engineUpdate([BII)V
    .locals 1
    .param p1, "input"    # [B
    .param p2, "offset"    # I
    .param p3, "len"    # I

    .prologue
    .line 110
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/jcajce/provider/symmetric/util/BaseMac;->macEngine:Lcom/android/sec/org/bouncycastle/crypto/Mac;

    invoke-interface {v0, p1, p2, p3}, Lcom/android/sec/org/bouncycastle/crypto/Mac;->update([BII)V

    .line 111
    return-void
.end method
