.class public Lcom/android/sec/org/bouncycastle/bcpg/attr/ImageAttribute;
.super Lcom/android/sec/org/bouncycastle/bcpg/UserAttributeSubpacket;
.source "ImageAttribute.java"


# static fields
.field public static final JPEG:I = 0x1

.field private static final ZEROES:[B


# instance fields
.field private encoding:I

.field private hdrLength:I

.field private imageData:[B

.field private version:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    const/16 v0, 0xc

    new-array v0, v0, [B

    sput-object v0, Lcom/android/sec/org/bouncycastle/bcpg/attr/ImageAttribute;->ZEROES:[B

    return-void
.end method

.method public constructor <init>(I[B)V
    .locals 1
    .param p1, "imageType"    # I
    .param p2, "imageData"    # [B

    .prologue
    .line 41
    invoke-static {p1, p2}, Lcom/android/sec/org/bouncycastle/bcpg/attr/ImageAttribute;->toByteArray(I[B)[B

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/sec/org/bouncycastle/bcpg/attr/ImageAttribute;-><init>([B)V

    .line 42
    return-void
.end method

.method public constructor <init>([B)V
    .locals 4
    .param p1, "data"    # [B

    .prologue
    const/4 v0, 0x1

    const/4 v3, 0x0

    .line 27
    invoke-direct {p0, v0, p1}, Lcom/android/sec/org/bouncycastle/bcpg/UserAttributeSubpacket;-><init>(I[B)V

    .line 29
    aget-byte v0, p1, v0

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x8

    aget-byte v1, p1, v3

    and-int/lit16 v1, v1, 0xff

    or-int/2addr v0, v1

    iput v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/attr/ImageAttribute;->hdrLength:I

    .line 30
    const/4 v0, 0x2

    aget-byte v0, p1, v0

    and-int/lit16 v0, v0, 0xff

    iput v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/attr/ImageAttribute;->version:I

    .line 31
    const/4 v0, 0x3

    aget-byte v0, p1, v0

    and-int/lit16 v0, v0, 0xff

    iput v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/attr/ImageAttribute;->encoding:I

    .line 33
    array-length v0, p1

    iget v1, p0, Lcom/android/sec/org/bouncycastle/bcpg/attr/ImageAttribute;->hdrLength:I

    sub-int/2addr v0, v1

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/attr/ImageAttribute;->imageData:[B

    .line 34
    iget v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/attr/ImageAttribute;->hdrLength:I

    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/bcpg/attr/ImageAttribute;->imageData:[B

    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/attr/ImageAttribute;->imageData:[B

    array-length v2, v2

    invoke-static {p1, v0, v1, v3, v2}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 35
    return-void
.end method

.method private static toByteArray(I[B)[B
    .locals 4
    .param p0, "imageType"    # I
    .param p1, "imageData"    # [B

    .prologue
    .line 46
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 50
    .local v0, "bOut":Ljava/io/ByteArrayOutputStream;
    const/16 v2, 0x10

    :try_start_0
    invoke-virtual {v0, v2}, Ljava/io/ByteArrayOutputStream;->write(I)V

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ljava/io/ByteArrayOutputStream;->write(I)V

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 51
    invoke-virtual {v0, p0}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 52
    sget-object v2, Lcom/android/sec/org/bouncycastle/bcpg/attr/ImageAttribute;->ZEROES:[B

    invoke-virtual {v0, v2}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 53
    invoke-virtual {v0, p1}, Ljava/io/ByteArrayOutputStream;->write([B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 60
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v2

    return-object v2

    .line 55
    :catch_0
    move-exception v1

    .line 57
    .local v1, "e":Ljava/io/IOException;
    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "unable to encode to byte array!"

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2
.end method


# virtual methods
.method public getEncoding()I
    .locals 1

    .prologue
    .line 70
    iget v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/attr/ImageAttribute;->encoding:I

    return v0
.end method

.method public getImageData()[B
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/attr/ImageAttribute;->imageData:[B

    return-object v0
.end method

.method public version()I
    .locals 1

    .prologue
    .line 65
    iget v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/attr/ImageAttribute;->version:I

    return v0
.end method
