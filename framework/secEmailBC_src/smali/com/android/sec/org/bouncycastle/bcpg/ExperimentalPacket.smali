.class public Lcom/android/sec/org/bouncycastle/bcpg/ExperimentalPacket;
.super Lcom/android/sec/org/bouncycastle/bcpg/ContainedPacket;
.source "ExperimentalPacket.java"

# interfaces
.implements Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyAlgorithmTags;


# instance fields
.field private contents:[B

.field private tag:I


# direct methods
.method constructor <init>(ILcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;)V
    .locals 3
    .param p1, "tag"    # I
    .param p2, "in"    # Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/bcpg/ContainedPacket;-><init>()V

    .line 24
    iput p1, p0, Lcom/android/sec/org/bouncycastle/bcpg/ExperimentalPacket;->tag:I

    .line 26
    invoke-virtual {p2}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->available()I

    move-result v2

    if-eqz v2, :cond_1

    .line 28
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-virtual {p2}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->available()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    .line 31
    .local v1, "bOut":Ljava/io/ByteArrayOutputStream;
    :goto_0
    invoke-virtual {p2}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->read()I

    move-result v0

    .local v0, "b":I
    if-ltz v0, :cond_0

    .line 33
    invoke-virtual {v1, v0}, Ljava/io/ByteArrayOutputStream;->write(I)V

    goto :goto_0

    .line 36
    :cond_0
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v2

    iput-object v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/ExperimentalPacket;->contents:[B

    .line 42
    .end local v0    # "b":I
    .end local v1    # "bOut":Ljava/io/ByteArrayOutputStream;
    :goto_1
    return-void

    .line 40
    :cond_1
    const/4 v2, 0x0

    new-array v2, v2, [B

    iput-object v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/ExperimentalPacket;->contents:[B

    goto :goto_1
.end method


# virtual methods
.method public encode(Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;)V
    .locals 3
    .param p1, "out"    # Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 62
    iget v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/ExperimentalPacket;->tag:I

    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/bcpg/ExperimentalPacket;->contents:[B

    const/4 v2, 0x1

    invoke-virtual {p1, v0, v1, v2}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->writePacket(I[BZ)V

    .line 63
    return-void
.end method

.method public getContents()[B
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 51
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/bcpg/ExperimentalPacket;->contents:[B

    array-length v1, v1

    new-array v0, v1, [B

    .line 53
    .local v0, "tmp":[B
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/bcpg/ExperimentalPacket;->contents:[B

    array-length v2, v0

    invoke-static {v1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 55
    return-object v0
.end method

.method public getTag()I
    .locals 1

    .prologue
    .line 46
    iget v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/ExperimentalPacket;->tag:I

    return v0
.end method
