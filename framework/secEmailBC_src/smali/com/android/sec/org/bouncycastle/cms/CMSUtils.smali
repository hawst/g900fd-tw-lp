.class Lcom/android/sec/org/bouncycastle/cms/CMSUtils;
.super Ljava/lang/Object;
.source "CMSUtils.java"


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static attachDigestsToInputStream(Ljava/util/Collection;Ljava/io/InputStream;)Ljava/io/InputStream;
    .locals 5
    .param p0, "digests"    # Ljava/util/Collection;
    .param p1, "s"    # Ljava/io/InputStream;

    .prologue
    .line 335
    move-object v2, p1

    .line 336
    .local v2, "result":Ljava/io/InputStream;
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 337
    .local v1, "it":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 339
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/sec/org/bouncycastle/operator/DigestCalculator;

    .line 340
    .local v0, "digest":Lcom/android/sec/org/bouncycastle/operator/DigestCalculator;
    new-instance v3, Lcom/android/sec/org/bouncycastle/util/io/TeeInputStream;

    invoke-interface {v0}, Lcom/android/sec/org/bouncycastle/operator/DigestCalculator;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v4

    invoke-direct {v3, v2, v4}, Lcom/android/sec/org/bouncycastle/util/io/TeeInputStream;-><init>(Ljava/io/InputStream;Ljava/io/OutputStream;)V

    .end local v2    # "result":Ljava/io/InputStream;
    .local v3, "result":Ljava/io/InputStream;
    move-object v2, v3

    .line 341
    .end local v3    # "result":Ljava/io/InputStream;
    .restart local v2    # "result":Ljava/io/InputStream;
    goto :goto_0

    .line 342
    .end local v0    # "digest":Lcom/android/sec/org/bouncycastle/operator/DigestCalculator;
    :cond_0
    return-object v2
.end method

.method static attachSignersToOutputStream(Ljava/util/Collection;Ljava/io/OutputStream;)Ljava/io/OutputStream;
    .locals 4
    .param p0, "signers"    # Ljava/util/Collection;
    .param p1, "s"    # Ljava/io/OutputStream;

    .prologue
    .line 347
    move-object v1, p1

    .line 348
    .local v1, "result":Ljava/io/OutputStream;
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 349
    .local v0, "it":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 351
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/sec/org/bouncycastle/cms/SignerInfoGenerator;

    .line 352
    .local v2, "signerGen":Lcom/android/sec/org/bouncycastle/cms/SignerInfoGenerator;
    invoke-virtual {v2}, Lcom/android/sec/org/bouncycastle/cms/SignerInfoGenerator;->getCalculatingOutputStream()Ljava/io/OutputStream;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/android/sec/org/bouncycastle/cms/CMSUtils;->getSafeTeeOutputStream(Ljava/io/OutputStream;Ljava/io/OutputStream;)Ljava/io/OutputStream;

    move-result-object v1

    .line 353
    goto :goto_0

    .line 354
    .end local v2    # "signerGen":Lcom/android/sec/org/bouncycastle/cms/SignerInfoGenerator;
    :cond_0
    return-object v1
.end method

.method static createBEROctetOutputStream(Ljava/io/OutputStream;IZI)Ljava/io/OutputStream;
    .locals 2
    .param p0, "s"    # Ljava/io/OutputStream;
    .param p1, "tagNo"    # I
    .param p2, "isExplicit"    # Z
    .param p3, "bufferSize"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 247
    new-instance v0, Lcom/android/sec/org/bouncycastle/asn1/BEROctetStringGenerator;

    invoke-direct {v0, p0, p1, p2}, Lcom/android/sec/org/bouncycastle/asn1/BEROctetStringGenerator;-><init>(Ljava/io/OutputStream;IZ)V

    .line 249
    .local v0, "octGen":Lcom/android/sec/org/bouncycastle/asn1/BEROctetStringGenerator;
    if-eqz p3, :cond_0

    .line 251
    new-array v1, p3, [B

    invoke-virtual {v0, v1}, Lcom/android/sec/org/bouncycastle/asn1/BEROctetStringGenerator;->getOctetOutputStream([B)Ljava/io/OutputStream;

    move-result-object v1

    .line 254
    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/BEROctetStringGenerator;->getOctetOutputStream()Ljava/io/OutputStream;

    move-result-object v1

    goto :goto_0
.end method

.method static createBerSetFromList(Ljava/util/List;)Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;
    .locals 3
    .param p0, "derObjects"    # Ljava/util/List;

    .prologue
    .line 222
    new-instance v1, Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;

    invoke-direct {v1}, Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;-><init>()V

    .line 224
    .local v1, "v":Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "it":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 226
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;

    invoke-virtual {v1, v2}, Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;->add(Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;)V

    goto :goto_0

    .line 229
    :cond_0
    new-instance v2, Lcom/android/sec/org/bouncycastle/asn1/BERSet;

    invoke-direct {v2, v1}, Lcom/android/sec/org/bouncycastle/asn1/BERSet;-><init>(Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;)V

    return-object v2
.end method

.method static createDerSetFromList(Ljava/util/List;)Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;
    .locals 3
    .param p0, "derObjects"    # Ljava/util/List;

    .prologue
    .line 234
    new-instance v1, Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;

    invoke-direct {v1}, Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;-><init>()V

    .line 236
    .local v1, "v":Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "it":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 238
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;

    invoke-virtual {v1, v2}, Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;->add(Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;)V

    goto :goto_0

    .line 241
    :cond_0
    new-instance v2, Lcom/android/sec/org/bouncycastle/asn1/DERSet;

    invoke-direct {v2, v1}, Lcom/android/sec/org/bouncycastle/asn1/DERSet;-><init>(Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;)V

    return-object v2
.end method

.method static getAttributeCertificatesFromStore(Lcom/android/sec/org/bouncycastle/util/Store;)Ljava/util/List;
    .locals 8
    .param p0, "attrStore"    # Lcom/android/sec/org/bouncycastle/util/Store;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/cms/CMSException;
        }
    .end annotation

    .prologue
    .line 123
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 127
    .local v1, "certs":Ljava/util/List;
    const/4 v4, 0x0

    :try_start_0
    invoke-interface {p0, v4}, Lcom/android/sec/org/bouncycastle/util/Store;->getMatches(Lcom/android/sec/org/bouncycastle/util/Selector;)Ljava/util/Collection;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "it":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 129
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/sec/org/bouncycastle/cert/X509AttributeCertificateHolder;

    .line 131
    .local v0, "attrCert":Lcom/android/sec/org/bouncycastle/cert/X509AttributeCertificateHolder;
    new-instance v4, Lcom/android/sec/org/bouncycastle/asn1/DERTaggedObject;

    const/4 v5, 0x0

    const/4 v6, 0x2

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/cert/X509AttributeCertificateHolder;->toASN1Structure()Lcom/android/sec/org/bouncycastle/asn1/x509/AttributeCertificate;

    move-result-object v7

    invoke-direct {v4, v5, v6, v7}, Lcom/android/sec/org/bouncycastle/asn1/DERTaggedObject;-><init>(ZILcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;)V

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 136
    .end local v0    # "attrCert":Lcom/android/sec/org/bouncycastle/cert/X509AttributeCertificateHolder;
    .end local v3    # "it":Ljava/util/Iterator;
    :catch_0
    move-exception v2

    .line 138
    .local v2, "e":Ljava/lang/ClassCastException;
    new-instance v4, Lcom/android/sec/org/bouncycastle/cms/CMSException;

    const-string v5, "error processing certs"

    invoke-direct {v4, v5, v2}, Lcom/android/sec/org/bouncycastle/cms/CMSException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v4

    .line 134
    .end local v2    # "e":Ljava/lang/ClassCastException;
    .restart local v3    # "it":Ljava/util/Iterator;
    :cond_0
    return-object v1
.end method

.method static getCRLsFromStore(Lcom/android/sec/org/bouncycastle/util/Store;)Ljava/util/List;
    .locals 6
    .param p0, "crlStore"    # Lcom/android/sec/org/bouncycastle/util/Store;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/cms/CMSException;
        }
    .end annotation

    .prologue
    .line 175
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 179
    .local v1, "certs":Ljava/util/List;
    const/4 v4, 0x0

    :try_start_0
    invoke-interface {p0, v4}, Lcom/android/sec/org/bouncycastle/util/Store;->getMatches(Lcom/android/sec/org/bouncycastle/util/Selector;)Ljava/util/Collection;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "it":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 181
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/sec/org/bouncycastle/cert/X509CRLHolder;

    .line 183
    .local v0, "c":Lcom/android/sec/org/bouncycastle/cert/X509CRLHolder;
    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/cert/X509CRLHolder;->toASN1Structure()Lcom/android/sec/org/bouncycastle/asn1/x509/CertificateList;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 188
    .end local v0    # "c":Lcom/android/sec/org/bouncycastle/cert/X509CRLHolder;
    .end local v3    # "it":Ljava/util/Iterator;
    :catch_0
    move-exception v2

    .line 190
    .local v2, "e":Ljava/lang/ClassCastException;
    new-instance v4, Lcom/android/sec/org/bouncycastle/cms/CMSException;

    const-string v5, "error processing certs"

    invoke-direct {v4, v5, v2}, Lcom/android/sec/org/bouncycastle/cms/CMSException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v4

    .line 186
    .end local v2    # "e":Ljava/lang/ClassCastException;
    .restart local v3    # "it":Ljava/util/Iterator;
    :cond_0
    return-object v1
.end method

.method static getCRLsFromStore(Ljava/security/cert/CertStore;)Ljava/util/List;
    .locals 6
    .param p0, "certStore"    # Ljava/security/cert/CertStore;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/cert/CertStoreException;,
            Lcom/android/sec/org/bouncycastle/cms/CMSException;
        }
    .end annotation

    .prologue
    .line 145
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 149
    .local v1, "crls":Ljava/util/List;
    const/4 v4, 0x0

    :try_start_0
    invoke-virtual {p0, v4}, Ljava/security/cert/CertStore;->getCRLs(Ljava/security/cert/CRLSelector;)Ljava/util/Collection;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "it":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 151
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/security/cert/X509CRL;

    .line 153
    .local v0, "c":Ljava/security/cert/X509CRL;
    invoke-virtual {v0}, Ljava/security/cert/X509CRL;->getEncoded()[B

    move-result-object v4

    invoke-static {v4}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Primitive;->fromByteArray([B)Lcom/android/sec/org/bouncycastle/asn1/ASN1Primitive;

    move-result-object v4

    invoke-static {v4}, Lcom/android/sec/org/bouncycastle/asn1/x509/CertificateList;->getInstance(Ljava/lang/Object;)Lcom/android/sec/org/bouncycastle/asn1/x509/CertificateList;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/security/cert/CRLException; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_0

    .line 158
    .end local v0    # "c":Ljava/security/cert/X509CRL;
    .end local v3    # "it":Ljava/util/Iterator;
    :catch_0
    move-exception v2

    .line 160
    .local v2, "e":Ljava/lang/IllegalArgumentException;
    new-instance v4, Lcom/android/sec/org/bouncycastle/cms/CMSException;

    const-string v5, "error processing crls"

    invoke-direct {v4, v5, v2}, Lcom/android/sec/org/bouncycastle/cms/CMSException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v4

    .line 162
    .end local v2    # "e":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v2

    .line 164
    .local v2, "e":Ljava/io/IOException;
    new-instance v4, Lcom/android/sec/org/bouncycastle/cms/CMSException;

    const-string v5, "error processing crls"

    invoke-direct {v4, v5, v2}, Lcom/android/sec/org/bouncycastle/cms/CMSException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v4

    .line 166
    .end local v2    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v2

    .line 168
    .local v2, "e":Ljava/security/cert/CRLException;
    new-instance v4, Lcom/android/sec/org/bouncycastle/cms/CMSException;

    const-string v5, "error encoding crls"

    invoke-direct {v4, v5, v2}, Lcom/android/sec/org/bouncycastle/cms/CMSException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v4

    .line 156
    .end local v2    # "e":Ljava/security/cert/CRLException;
    .restart local v3    # "it":Ljava/util/Iterator;
    :cond_0
    return-object v1
.end method

.method static getCertificatesFromStore(Lcom/android/sec/org/bouncycastle/util/Store;)Ljava/util/List;
    .locals 6
    .param p0, "certStore"    # Lcom/android/sec/org/bouncycastle/util/Store;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/cms/CMSException;
        }
    .end annotation

    .prologue
    .line 101
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 105
    .local v1, "certs":Ljava/util/List;
    const/4 v4, 0x0

    :try_start_0
    invoke-interface {p0, v4}, Lcom/android/sec/org/bouncycastle/util/Store;->getMatches(Lcom/android/sec/org/bouncycastle/util/Selector;)Ljava/util/Collection;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "it":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 107
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/sec/org/bouncycastle/cert/X509CertificateHolder;

    .line 109
    .local v0, "c":Lcom/android/sec/org/bouncycastle/cert/X509CertificateHolder;
    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/cert/X509CertificateHolder;->toASN1Structure()Lcom/android/sec/org/bouncycastle/asn1/x509/Certificate;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 114
    .end local v0    # "c":Lcom/android/sec/org/bouncycastle/cert/X509CertificateHolder;
    .end local v3    # "it":Ljava/util/Iterator;
    :catch_0
    move-exception v2

    .line 116
    .local v2, "e":Ljava/lang/ClassCastException;
    new-instance v4, Lcom/android/sec/org/bouncycastle/cms/CMSException;

    const-string v5, "error processing certs"

    invoke-direct {v4, v5, v2}, Lcom/android/sec/org/bouncycastle/cms/CMSException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v4

    .line 112
    .end local v2    # "e":Ljava/lang/ClassCastException;
    .restart local v3    # "it":Ljava/util/Iterator;
    :cond_0
    return-object v1
.end method

.method static getCertificatesFromStore(Ljava/security/cert/CertStore;)Ljava/util/List;
    .locals 6
    .param p0, "certStore"    # Ljava/security/cert/CertStore;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/cert/CertStoreException;,
            Lcom/android/sec/org/bouncycastle/cms/CMSException;
        }
    .end annotation

    .prologue
    .line 71
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 75
    .local v1, "certs":Ljava/util/List;
    const/4 v4, 0x0

    :try_start_0
    invoke-virtual {p0, v4}, Ljava/security/cert/CertStore;->getCertificates(Ljava/security/cert/CertSelector;)Ljava/util/Collection;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "it":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 77
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/security/cert/X509Certificate;

    .line 79
    .local v0, "c":Ljava/security/cert/X509Certificate;
    invoke-virtual {v0}, Ljava/security/cert/X509Certificate;->getEncoded()[B

    move-result-object v4

    invoke-static {v4}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Primitive;->fromByteArray([B)Lcom/android/sec/org/bouncycastle/asn1/ASN1Primitive;

    move-result-object v4

    invoke-static {v4}, Lcom/android/sec/org/bouncycastle/asn1/x509/Certificate;->getInstance(Ljava/lang/Object;)Lcom/android/sec/org/bouncycastle/asn1/x509/Certificate;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/security/cert/CertificateEncodingException; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_0

    .line 84
    .end local v0    # "c":Ljava/security/cert/X509Certificate;
    .end local v3    # "it":Ljava/util/Iterator;
    :catch_0
    move-exception v2

    .line 86
    .local v2, "e":Ljava/lang/IllegalArgumentException;
    new-instance v4, Lcom/android/sec/org/bouncycastle/cms/CMSException;

    const-string v5, "error processing certs"

    invoke-direct {v4, v5, v2}, Lcom/android/sec/org/bouncycastle/cms/CMSException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v4

    .line 88
    .end local v2    # "e":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v2

    .line 90
    .local v2, "e":Ljava/io/IOException;
    new-instance v4, Lcom/android/sec/org/bouncycastle/cms/CMSException;

    const-string v5, "error processing certs"

    invoke-direct {v4, v5, v2}, Lcom/android/sec/org/bouncycastle/cms/CMSException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v4

    .line 92
    .end local v2    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v2

    .line 94
    .local v2, "e":Ljava/security/cert/CertificateEncodingException;
    new-instance v4, Lcom/android/sec/org/bouncycastle/cms/CMSException;

    const-string v5, "error encoding certs"

    invoke-direct {v4, v5, v2}, Lcom/android/sec/org/bouncycastle/cms/CMSException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v4

    .line 82
    .end local v2    # "e":Ljava/security/cert/CertificateEncodingException;
    .restart local v3    # "it":Ljava/util/Iterator;
    :cond_0
    return-object v1
.end method

.method static getIssuerAndSerialNumber(Ljava/security/cert/X509Certificate;)Lcom/android/sec/org/bouncycastle/asn1/cms/IssuerAndSerialNumber;
    .locals 4
    .param p0, "cert"    # Ljava/security/cert/X509Certificate;

    .prologue
    .line 274
    invoke-static {p0}, Lcom/android/sec/org/bouncycastle/cms/CMSUtils;->getTBSCertificateStructure(Ljava/security/cert/X509Certificate;)Lcom/android/sec/org/bouncycastle/asn1/x509/TBSCertificate;

    move-result-object v0

    .line 275
    .local v0, "tbsCert":Lcom/android/sec/org/bouncycastle/asn1/x509/TBSCertificate;
    new-instance v1, Lcom/android/sec/org/bouncycastle/asn1/cms/IssuerAndSerialNumber;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/x509/TBSCertificate;->getIssuer()Lcom/android/sec/org/bouncycastle/asn1/x500/X500Name;

    move-result-object v2

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/x509/TBSCertificate;->getSerialNumber()Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;->getValue()Ljava/math/BigInteger;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/android/sec/org/bouncycastle/asn1/cms/IssuerAndSerialNumber;-><init>(Lcom/android/sec/org/bouncycastle/asn1/x500/X500Name;Ljava/math/BigInteger;)V

    return-object v1
.end method

.method public static getProvider(Ljava/lang/String;)Ljava/security/Provider;
    .locals 4
    .param p0, "providerName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/NoSuchProviderException;
        }
    .end annotation

    .prologue
    .line 318
    if-eqz p0, :cond_1

    .line 320
    invoke-static {p0}, Ljava/security/Security;->getProvider(Ljava/lang/String;)Ljava/security/Provider;

    move-result-object v0

    .line 322
    .local v0, "prov":Ljava/security/Provider;
    if-eqz v0, :cond_0

    .line 330
    .end local v0    # "prov":Ljava/security/Provider;
    :goto_0
    return-object v0

    .line 327
    .restart local v0    # "prov":Ljava/security/Provider;
    :cond_0
    new-instance v1, Ljava/security/NoSuchProviderException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "provider "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " not found."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/security/NoSuchProviderException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 330
    .end local v0    # "prov":Ljava/security/Provider;
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static getSafeOutputStream(Ljava/io/OutputStream;)Ljava/io/OutputStream;
    .locals 0
    .param p0, "s"    # Ljava/io/OutputStream;

    .prologue
    .line 359
    if-nez p0, :cond_0

    new-instance p0, Lcom/android/sec/org/bouncycastle/cms/NullOutputStream;

    .end local p0    # "s":Ljava/io/OutputStream;
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/cms/NullOutputStream;-><init>()V

    :cond_0
    return-object p0
.end method

.method static getSafeTeeOutputStream(Ljava/io/OutputStream;Ljava/io/OutputStream;)Ljava/io/OutputStream;
    .locals 1
    .param p0, "s1"    # Ljava/io/OutputStream;
    .param p1, "s2"    # Ljava/io/OutputStream;

    .prologue
    .line 365
    if-nez p0, :cond_0

    invoke-static {p1}, Lcom/android/sec/org/bouncycastle/cms/CMSUtils;->getSafeOutputStream(Ljava/io/OutputStream;)Ljava/io/OutputStream;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    if-nez p1, :cond_1

    invoke-static {p0}, Lcom/android/sec/org/bouncycastle/cms/CMSUtils;->getSafeOutputStream(Ljava/io/OutputStream;)Ljava/io/OutputStream;

    move-result-object v0

    goto :goto_0

    :cond_1
    new-instance v0, Lcom/android/sec/org/bouncycastle/util/io/TeeOutputStream;

    invoke-direct {v0, p0, p1}, Lcom/android/sec/org/bouncycastle/util/io/TeeOutputStream;-><init>(Ljava/io/OutputStream;Ljava/io/OutputStream;)V

    goto :goto_0
.end method

.method static getTBSCertificateStructure(Ljava/security/cert/X509Certificate;)Lcom/android/sec/org/bouncycastle/asn1/x509/TBSCertificate;
    .locals 3
    .param p0, "cert"    # Ljava/security/cert/X509Certificate;

    .prologue
    .line 262
    :try_start_0
    invoke-virtual {p0}, Ljava/security/cert/X509Certificate;->getTBSCertificate()[B

    move-result-object v1

    invoke-static {v1}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Primitive;->fromByteArray([B)Lcom/android/sec/org/bouncycastle/asn1/ASN1Primitive;

    move-result-object v1

    invoke-static {v1}, Lcom/android/sec/org/bouncycastle/asn1/x509/TBSCertificate;->getInstance(Ljava/lang/Object;)Lcom/android/sec/org/bouncycastle/asn1/x509/TBSCertificate;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    .line 265
    :catch_0
    move-exception v0

    .line 267
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "can\'t extract TBS structure from this cert"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private static readContentInfo(Lcom/android/sec/org/bouncycastle/asn1/ASN1InputStream;)Lcom/android/sec/org/bouncycastle/asn1/cms/ContentInfo;
    .locals 3
    .param p0, "in"    # Lcom/android/sec/org/bouncycastle/asn1/ASN1InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/cms/CMSException;
        }
    .end annotation

    .prologue
    .line 284
    :try_start_0
    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/asn1/ASN1InputStream;->readObject()Lcom/android/sec/org/bouncycastle/asn1/ASN1Primitive;

    move-result-object v1

    invoke-static {v1}, Lcom/android/sec/org/bouncycastle/asn1/cms/ContentInfo;->getInstance(Ljava/lang/Object;)Lcom/android/sec/org/bouncycastle/asn1/cms/ContentInfo;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v1

    return-object v1

    .line 286
    :catch_0
    move-exception v0

    .line 288
    .local v0, "e":Ljava/io/IOException;
    new-instance v1, Lcom/android/sec/org/bouncycastle/cms/CMSException;

    const-string v2, "IOException reading content."

    invoke-direct {v1, v2, v0}, Lcom/android/sec/org/bouncycastle/cms/CMSException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v1

    .line 290
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 292
    .local v0, "e":Ljava/lang/ClassCastException;
    new-instance v1, Lcom/android/sec/org/bouncycastle/cms/CMSException;

    const-string v2, "Malformed content."

    invoke-direct {v1, v2, v0}, Lcom/android/sec/org/bouncycastle/cms/CMSException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v1

    .line 294
    .end local v0    # "e":Ljava/lang/ClassCastException;
    :catch_2
    move-exception v0

    .line 296
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    new-instance v1, Lcom/android/sec/org/bouncycastle/cms/CMSException;

    const-string v2, "Malformed content."

    invoke-direct {v1, v2, v0}, Lcom/android/sec/org/bouncycastle/cms/CMSException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v1
.end method

.method static readContentInfo(Ljava/io/InputStream;)Lcom/android/sec/org/bouncycastle/asn1/cms/ContentInfo;
    .locals 1
    .param p0, "input"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/cms/CMSException;
        }
    .end annotation

    .prologue
    .line 65
    new-instance v0, Lcom/android/sec/org/bouncycastle/asn1/ASN1InputStream;

    invoke-direct {v0, p0}, Lcom/android/sec/org/bouncycastle/asn1/ASN1InputStream;-><init>(Ljava/io/InputStream;)V

    invoke-static {v0}, Lcom/android/sec/org/bouncycastle/cms/CMSUtils;->readContentInfo(Lcom/android/sec/org/bouncycastle/asn1/ASN1InputStream;)Lcom/android/sec/org/bouncycastle/asn1/cms/ContentInfo;

    move-result-object v0

    return-object v0
.end method

.method static readContentInfo([B)Lcom/android/sec/org/bouncycastle/asn1/cms/ContentInfo;
    .locals 1
    .param p0, "input"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/cms/CMSException;
        }
    .end annotation

    .prologue
    .line 57
    new-instance v0, Lcom/android/sec/org/bouncycastle/asn1/ASN1InputStream;

    invoke-direct {v0, p0}, Lcom/android/sec/org/bouncycastle/asn1/ASN1InputStream;-><init>([B)V

    invoke-static {v0}, Lcom/android/sec/org/bouncycastle/cms/CMSUtils;->readContentInfo(Lcom/android/sec/org/bouncycastle/asn1/ASN1InputStream;)Lcom/android/sec/org/bouncycastle/asn1/cms/ContentInfo;

    move-result-object v0

    return-object v0
.end method

.method public static streamToByteArray(Ljava/io/InputStream;)[B
    .locals 1
    .param p0, "in"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 304
    invoke-static {p0}, Lcom/android/sec/org/bouncycastle/util/io/Streams;->readAll(Ljava/io/InputStream;)[B

    move-result-object v0

    return-object v0
.end method

.method public static streamToByteArray(Ljava/io/InputStream;I)[B
    .locals 1
    .param p0, "in"    # Ljava/io/InputStream;
    .param p1, "limit"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 312
    invoke-static {p0, p1}, Lcom/android/sec/org/bouncycastle/util/io/Streams;->readAllLimited(Ljava/io/InputStream;I)[B

    move-result-object v0

    return-object v0
.end method
