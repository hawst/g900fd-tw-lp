.class public Lcom/android/sec/org/bouncycastle/bcpg/sig/PreferredAlgorithms;
.super Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;
.source "PreferredAlgorithms.java"


# direct methods
.method public constructor <init>(IZ[B)V
    .locals 0
    .param p1, "type"    # I
    .param p2, "critical"    # Z
    .param p3, "data"    # [B

    .prologue
    .line 29
    invoke-direct {p0, p1, p2, p3}, Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;-><init>(IZ[B)V

    .line 30
    return-void
.end method

.method public constructor <init>(IZ[I)V
    .locals 1
    .param p1, "type"    # I
    .param p2, "critical"    # Z
    .param p3, "preferrences"    # [I

    .prologue
    .line 37
    invoke-static {p3}, Lcom/android/sec/org/bouncycastle/bcpg/sig/PreferredAlgorithms;->intToByteArray([I)[B

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;-><init>(IZ[B)V

    .line 38
    return-void
.end method

.method private static intToByteArray([I)[B
    .locals 3
    .param p0, "v"    # [I

    .prologue
    .line 14
    array-length v2, p0

    new-array v0, v2, [B

    .line 16
    .local v0, "data":[B
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v2, p0

    if-eq v1, v2, :cond_0

    .line 18
    aget v2, p0, v1

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 16
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 21
    :cond_0
    return-object v0
.end method


# virtual methods
.method public getPreferences()[I
    .locals 3

    .prologue
    .line 50
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/sig/PreferredAlgorithms;->data:[B

    array-length v2, v2

    new-array v1, v2, [I

    .line 52
    .local v1, "v":[I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, v1

    if-eq v0, v2, :cond_0

    .line 54
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/sig/PreferredAlgorithms;->data:[B

    aget-byte v2, v2, v0

    and-int/lit16 v2, v2, 0xff

    aput v2, v1, v0

    .line 52
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 57
    :cond_0
    return-object v1
.end method

.method public getPreferrences()[I
    .locals 1

    .prologue
    .line 45
    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/bcpg/sig/PreferredAlgorithms;->getPreferences()[I

    move-result-object v0

    return-object v0
.end method
