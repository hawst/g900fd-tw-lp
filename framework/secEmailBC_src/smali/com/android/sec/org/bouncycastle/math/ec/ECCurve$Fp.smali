.class public Lcom/android/sec/org/bouncycastle/math/ec/ECCurve$Fp;
.super Lcom/android/sec/org/bouncycastle/math/ec/ECCurve;
.source "ECCurve.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/sec/org/bouncycastle/math/ec/ECCurve;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Fp"
.end annotation


# instance fields
.field infinity:Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$Fp;

.field q:Ljava/math/BigInteger;


# direct methods
.method public constructor <init>(Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/math/BigInteger;)V
    .locals 2
    .param p1, "q"    # Ljava/math/BigInteger;
    .param p2, "a"    # Ljava/math/BigInteger;
    .param p3, "b"    # Ljava/math/BigInteger;

    .prologue
    const/4 v1, 0x0

    .line 108
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve;-><init>()V

    .line 109
    iput-object p1, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve$Fp;->q:Ljava/math/BigInteger;

    .line 110
    invoke-virtual {p0, p2}, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve$Fp;->fromBigInteger(Ljava/math/BigInteger;)Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    move-result-object v0

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve$Fp;->a:Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    .line 111
    invoke-virtual {p0, p3}, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve$Fp;->fromBigInteger(Ljava/math/BigInteger;)Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    move-result-object v0

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve$Fp;->b:Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    .line 112
    new-instance v0, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$Fp;

    invoke-direct {v0, p0, v1, v1}, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$Fp;-><init>(Lcom/android/sec/org/bouncycastle/math/ec/ECCurve;Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;)V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve$Fp;->infinity:Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$Fp;

    .line 113
    return-void
.end method


# virtual methods
.method public createPoint(Ljava/math/BigInteger;Ljava/math/BigInteger;Z)Lcom/android/sec/org/bouncycastle/math/ec/ECPoint;
    .locals 3
    .param p1, "x"    # Ljava/math/BigInteger;
    .param p2, "y"    # Ljava/math/BigInteger;
    .param p3, "withCompression"    # Z

    .prologue
    .line 132
    new-instance v0, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$Fp;

    invoke-virtual {p0, p1}, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve$Fp;->fromBigInteger(Ljava/math/BigInteger;)Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    move-result-object v1

    invoke-virtual {p0, p2}, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve$Fp;->fromBigInteger(Ljava/math/BigInteger;)Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    move-result-object v2

    invoke-direct {v0, p0, v1, v2, p3}, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$Fp;-><init>(Lcom/android/sec/org/bouncycastle/math/ec/ECCurve;Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;Z)V

    return-object v0
.end method

.method protected decompressPoint(ILjava/math/BigInteger;)Lcom/android/sec/org/bouncycastle/math/ec/ECPoint;
    .locals 8
    .param p1, "yTilde"    # I
    .param p2, "X1"    # Ljava/math/BigInteger;

    .prologue
    const/4 v5, 0x1

    const/4 v3, 0x0

    .line 137
    invoke-virtual {p0, p2}, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve$Fp;->fromBigInteger(Ljava/math/BigInteger;)Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    move-result-object v4

    .line 138
    .local v4, "x":Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;
    invoke-virtual {v4}, Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;->square()Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    move-result-object v6

    iget-object v7, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve$Fp;->a:Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    invoke-virtual {v6, v7}, Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;->add(Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;)Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;->multiply(Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;)Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    move-result-object v6

    iget-object v7, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve$Fp;->b:Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    invoke-virtual {v6, v7}, Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;->add(Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;)Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    move-result-object v0

    .line 139
    .local v0, "alpha":Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;
    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;->sqrt()Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    move-result-object v1

    .line 145
    .local v1, "beta":Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;
    if-nez v1, :cond_0

    .line 147
    new-instance v5, Ljava/lang/RuntimeException;

    const-string v6, "Invalid point compression"

    invoke-direct {v5, v6}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 150
    :cond_0
    invoke-virtual {v1}, Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;->toBigInteger()Ljava/math/BigInteger;

    move-result-object v2

    .line 151
    .local v2, "betaValue":Ljava/math/BigInteger;
    invoke-virtual {v2, v3}, Ljava/math/BigInteger;->testBit(I)Z

    move-result v6

    if-eqz v6, :cond_1

    move v3, v5

    .line 153
    .local v3, "bit0":I
    :cond_1
    if-eq v3, p1, :cond_2

    .line 156
    iget-object v6, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve$Fp;->q:Ljava/math/BigInteger;

    invoke-virtual {v6, v2}, Ljava/math/BigInteger;->subtract(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve$Fp;->fromBigInteger(Ljava/math/BigInteger;)Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    move-result-object v1

    .line 159
    :cond_2
    new-instance v6, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$Fp;

    invoke-direct {v6, p0, v4, v1, v5}, Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$Fp;-><init>(Lcom/android/sec/org/bouncycastle/math/ec/ECCurve;Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;Z)V

    return-object v6
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "anObject"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 170
    if-ne p1, p0, :cond_1

    .line 182
    :cond_0
    :goto_0
    return v1

    .line 175
    :cond_1
    instance-of v3, p1, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve$Fp;

    if-nez v3, :cond_2

    move v1, v2

    .line 177
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 180
    check-cast v0, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve$Fp;

    .line 182
    .local v0, "other":Lcom/android/sec/org/bouncycastle/math/ec/ECCurve$Fp;
    iget-object v3, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve$Fp;->q:Ljava/math/BigInteger;

    iget-object v4, v0, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve$Fp;->q:Ljava/math/BigInteger;

    invoke-virtual {v3, v4}, Ljava/math/BigInteger;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve$Fp;->a:Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    iget-object v4, v0, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve$Fp;->a:Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve$Fp;->b:Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    iget-object v4, v0, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve$Fp;->b:Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public fromBigInteger(Ljava/math/BigInteger;)Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;
    .locals 2
    .param p1, "x"    # Ljava/math/BigInteger;

    .prologue
    .line 127
    new-instance v0, Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement$Fp;

    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve$Fp;->q:Ljava/math/BigInteger;

    invoke-direct {v0, v1, p1}, Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement$Fp;-><init>(Ljava/math/BigInteger;Ljava/math/BigInteger;)V

    return-object v0
.end method

.method public getFieldSize()I
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve$Fp;->q:Ljava/math/BigInteger;

    invoke-virtual {v0}, Ljava/math/BigInteger;->bitLength()I

    move-result v0

    return v0
.end method

.method public getInfinity()Lcom/android/sec/org/bouncycastle/math/ec/ECPoint;
    .locals 1

    .prologue
    .line 164
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve$Fp;->infinity:Lcom/android/sec/org/bouncycastle/math/ec/ECPoint$Fp;

    return-object v0
.end method

.method public getQ()Ljava/math/BigInteger;
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve$Fp;->q:Ljava/math/BigInteger;

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 188
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve$Fp;->a:Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve$Fp;->b:Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve$Fp;->q:Ljava/math/BigInteger;

    invoke-virtual {v1}, Ljava/math/BigInteger;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    return v0
.end method
