.class public Lcom/android/sec/org/bouncycastle/bcpg/PublicSubkeyPacket;
.super Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;
.source "PublicSubkeyPacket.java"


# direct methods
.method public constructor <init>(ILjava/util/Date;Lcom/android/sec/org/bouncycastle/bcpg/BCPGKey;)V
    .locals 0
    .param p1, "algorithm"    # I
    .param p2, "time"    # Ljava/util/Date;
    .param p3, "key"    # Lcom/android/sec/org/bouncycastle/bcpg/BCPGKey;

    .prologue
    .line 31
    invoke-direct {p0, p1, p2, p3}, Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;-><init>(ILjava/util/Date;Lcom/android/sec/org/bouncycastle/bcpg/BCPGKey;)V

    .line 32
    return-void
.end method

.method constructor <init>(Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;)V
    .locals 0
    .param p1, "in"    # Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 16
    invoke-direct {p0, p1}, Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;-><init>(Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;)V

    .line 17
    return-void
.end method


# virtual methods
.method public encode(Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;)V
    .locals 3
    .param p1, "out"    # Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 38
    const/16 v0, 0xe

    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/bcpg/PublicSubkeyPacket;->getEncodedContents()[B

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {p1, v0, v1, v2}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->writePacket(I[BZ)V

    .line 39
    return-void
.end method
