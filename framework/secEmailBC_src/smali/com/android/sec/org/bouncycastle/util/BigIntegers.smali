.class public final Lcom/android/sec/org/bouncycastle/util/BigIntegers;
.super Ljava/lang/Object;
.source "BigIntegers.java"


# static fields
.field private static final MAX_ITERATIONS:I = 0x3e8

.field private static final ZERO:Ljava/math/BigInteger;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 12
    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Ljava/math/BigInteger;->valueOf(J)Ljava/math/BigInteger;

    move-result-object v0

    sput-object v0, Lcom/android/sec/org/bouncycastle/util/BigIntegers;->ZERO:Ljava/math/BigInteger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static asUnsignedByteArray(ILjava/math/BigInteger;)[B
    .locals 5
    .param p0, "length"    # I
    .param p1, "value"    # Ljava/math/BigInteger;

    .prologue
    const/4 v4, 0x0

    .line 47
    invoke-virtual {p1}, Ljava/math/BigInteger;->toByteArray()[B

    move-result-object v0

    .line 49
    .local v0, "bytes":[B
    aget-byte v2, v0, v4

    if-nez v2, :cond_1

    .line 51
    array-length v2, v0

    add-int/lit8 v2, v2, -0x1

    if-le v2, p0, :cond_0

    .line 53
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "standard length exceeded for value"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 56
    :cond_0
    new-array v1, p0, [B

    .line 58
    .local v1, "tmp":[B
    const/4 v2, 0x1

    array-length v3, v1

    array-length v4, v0

    add-int/lit8 v4, v4, -0x1

    sub-int/2addr v3, v4

    array-length v4, v0

    add-int/lit8 v4, v4, -0x1

    invoke-static {v0, v2, v1, v3, v4}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 78
    .end local v1    # "tmp":[B
    :goto_0
    return-object v1

    .line 64
    :cond_1
    array-length v2, v0

    if-ne v2, p0, :cond_2

    move-object v1, v0

    .line 66
    goto :goto_0

    .line 69
    :cond_2
    array-length v2, v0

    if-le v2, p0, :cond_3

    .line 71
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "standard length exceeded for value"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 74
    :cond_3
    new-array v1, p0, [B

    .line 76
    .restart local v1    # "tmp":[B
    array-length v2, v1

    array-length v3, v0

    sub-int/2addr v2, v3

    array-length v3, v0

    invoke-static {v0, v4, v1, v2, v3}, Ljava/lang/System;->arraycopy([BI[BII)V

    goto :goto_0
.end method

.method public static asUnsignedByteArray(Ljava/math/BigInteger;)[B
    .locals 5
    .param p0, "value"    # Ljava/math/BigInteger;

    .prologue
    const/4 v4, 0x0

    .line 23
    invoke-virtual {p0}, Ljava/math/BigInteger;->toByteArray()[B

    move-result-object v0

    .line 25
    .local v0, "bytes":[B
    aget-byte v2, v0, v4

    if-nez v2, :cond_0

    .line 27
    array-length v2, v0

    add-int/lit8 v2, v2, -0x1

    new-array v1, v2, [B

    .line 29
    .local v1, "tmp":[B
    const/4 v2, 0x1

    array-length v3, v1

    invoke-static {v0, v2, v1, v4, v3}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 34
    .end local v1    # "tmp":[B
    :goto_0
    return-object v1

    :cond_0
    move-object v1, v0

    goto :goto_0
.end method

.method public static createRandomInRange(Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/security/SecureRandom;)Ljava/math/BigInteger;
    .locals 5
    .param p0, "min"    # Ljava/math/BigInteger;
    .param p1, "max"    # Ljava/math/BigInteger;
    .param p2, "random"    # Ljava/security/SecureRandom;

    .prologue
    .line 95
    invoke-virtual {p0, p1}, Ljava/math/BigInteger;->compareTo(Ljava/math/BigInteger;)I

    move-result v0

    .line 96
    .local v0, "cmp":I
    if-ltz v0, :cond_0

    .line 98
    if-lez v0, :cond_1

    .line 100
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "\'min\' may not be greater than \'max\'"

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 106
    :cond_0
    invoke-virtual {p0}, Ljava/math/BigInteger;->bitLength()I

    move-result v3

    invoke-virtual {p1}, Ljava/math/BigInteger;->bitLength()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    if-le v3, v4, :cond_2

    .line 108
    sget-object v3, Lcom/android/sec/org/bouncycastle/util/BigIntegers;->ZERO:Ljava/math/BigInteger;

    invoke-virtual {p1, p0}, Ljava/math/BigInteger;->subtract(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v4

    invoke-static {v3, v4, p2}, Lcom/android/sec/org/bouncycastle/util/BigIntegers;->createRandomInRange(Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/security/SecureRandom;)Ljava/math/BigInteger;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/math/BigInteger;->add(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object p0

    .line 121
    .end local p0    # "min":Ljava/math/BigInteger;
    :cond_1
    :goto_0
    return-object p0

    .line 111
    .restart local p0    # "min":Ljava/math/BigInteger;
    :cond_2
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    const/16 v3, 0x3e8

    if-ge v1, v3, :cond_4

    .line 113
    new-instance v2, Ljava/math/BigInteger;

    invoke-virtual {p1}, Ljava/math/BigInteger;->bitLength()I

    move-result v3

    invoke-direct {v2, v3, p2}, Ljava/math/BigInteger;-><init>(ILjava/util/Random;)V

    .line 114
    .local v2, "x":Ljava/math/BigInteger;
    invoke-virtual {v2, p0}, Ljava/math/BigInteger;->compareTo(Ljava/math/BigInteger;)I

    move-result v3

    if-ltz v3, :cond_3

    invoke-virtual {v2, p1}, Ljava/math/BigInteger;->compareTo(Ljava/math/BigInteger;)I

    move-result v3

    if-gtz v3, :cond_3

    move-object p0, v2

    .line 116
    goto :goto_0

    .line 111
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 121
    .end local v2    # "x":Ljava/math/BigInteger;
    :cond_4
    new-instance v3, Ljava/math/BigInteger;

    invoke-virtual {p1, p0}, Ljava/math/BigInteger;->subtract(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v4

    invoke-virtual {v4}, Ljava/math/BigInteger;->bitLength()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-direct {v3, v4, p2}, Ljava/math/BigInteger;-><init>(ILjava/util/Random;)V

    invoke-virtual {v3, p0}, Ljava/math/BigInteger;->add(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object p0

    goto :goto_0
.end method
