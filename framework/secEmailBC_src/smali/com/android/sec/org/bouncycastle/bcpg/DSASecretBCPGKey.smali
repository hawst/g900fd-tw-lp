.class public Lcom/android/sec/org/bouncycastle/bcpg/DSASecretBCPGKey;
.super Lcom/android/sec/org/bouncycastle/bcpg/BCPGObject;
.source "DSASecretBCPGKey.java"

# interfaces
.implements Lcom/android/sec/org/bouncycastle/bcpg/BCPGKey;


# instance fields
.field x:Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;


# direct methods
.method public constructor <init>(Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;)V
    .locals 1
    .param p1, "in"    # Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGObject;-><init>()V

    .line 23
    new-instance v0, Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;

    invoke-direct {v0, p1}, Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;-><init>(Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;)V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/DSASecretBCPGKey;->x:Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;

    .line 24
    return-void
.end method

.method public constructor <init>(Ljava/math/BigInteger;)V
    .locals 1
    .param p1, "x"    # Ljava/math/BigInteger;

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGObject;-><init>()V

    .line 33
    new-instance v0, Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;

    invoke-direct {v0, p1}, Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;-><init>(Ljava/math/BigInteger;)V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/DSASecretBCPGKey;->x:Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;

    .line 34
    return-void
.end method


# virtual methods
.method public encode(Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;)V
    .locals 1
    .param p1, "out"    # Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 72
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/DSASecretBCPGKey;->x:Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;

    invoke-virtual {p1, v0}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->writeObject(Lcom/android/sec/org/bouncycastle/bcpg/BCPGObject;)V

    .line 73
    return-void
.end method

.method public getEncoded()[B
    .locals 4

    .prologue
    .line 55
    :try_start_0
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 56
    .local v0, "bOut":Ljava/io/ByteArrayOutputStream;
    new-instance v2, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;

    invoke-direct {v2, v0}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 58
    .local v2, "pgpOut":Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;
    invoke-virtual {v2, p0}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->writeObject(Lcom/android/sec/org/bouncycastle/bcpg/BCPGObject;)V

    .line 60
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 64
    .end local v0    # "bOut":Ljava/io/ByteArrayOutputStream;
    .end local v2    # "pgpOut":Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;
    :goto_0
    return-object v3

    .line 62
    :catch_0
    move-exception v1

    .line 64
    .local v1, "e":Ljava/io/IOException;
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public getFormat()Ljava/lang/String;
    .locals 1

    .prologue
    .line 43
    const-string v0, "PGP"

    return-object v0
.end method

.method public getX()Ljava/math/BigInteger;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/DSASecretBCPGKey;->x:Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;->getValue()Ljava/math/BigInteger;

    move-result-object v0

    return-object v0
.end method
