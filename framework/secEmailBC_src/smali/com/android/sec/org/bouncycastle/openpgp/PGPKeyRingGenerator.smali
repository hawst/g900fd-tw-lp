.class public Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyRingGenerator;
.super Ljava/lang/Object;
.source "PGPKeyRingGenerator.java"


# instance fields
.field private certificationLevel:I

.field private encAlgorithm:I

.field private hashedPcks:Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;

.field private id:Ljava/lang/String;

.field keys:Ljava/util/List;

.field private masterKey:Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyPair;

.field private passPhrase:[C

.field private provider:Ljava/security/Provider;

.field private rand:Ljava/security/SecureRandom;

.field private unhashedPcks:Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;

.field private useSHA1:Z


# direct methods
.method public constructor <init>(ILcom/android/sec/org/bouncycastle/openpgp/PGPKeyPair;Ljava/lang/String;I[CLcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;Ljava/security/SecureRandom;Ljava/lang/String;)V
    .locals 11
    .param p1, "certificationLevel"    # I
    .param p2, "masterKey"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyPair;
    .param p3, "id"    # Ljava/lang/String;
    .param p4, "encAlgorithm"    # I
    .param p5, "passPhrase"    # [C
    .param p6, "hashedPcks"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;
    .param p7, "unhashedPcks"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;
    .param p8, "rand"    # Ljava/security/SecureRandom;
    .param p9, "provider"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;,
            Ljava/security/NoSuchProviderException;
        }
    .end annotation

    .prologue
    .line 60
    const/4 v6, 0x0

    move-object v0, p0

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move-object/from16 v5, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    invoke-direct/range {v0 .. v10}, Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyRingGenerator;-><init>(ILcom/android/sec/org/bouncycastle/openpgp/PGPKeyPair;Ljava/lang/String;I[CZLcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;Ljava/security/SecureRandom;Ljava/lang/String;)V

    .line 61
    return-void
.end method

.method public constructor <init>(ILcom/android/sec/org/bouncycastle/openpgp/PGPKeyPair;Ljava/lang/String;I[CZLcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;Ljava/security/SecureRandom;Ljava/lang/String;)V
    .locals 11
    .param p1, "certificationLevel"    # I
    .param p2, "masterKey"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyPair;
    .param p3, "id"    # Ljava/lang/String;
    .param p4, "encAlgorithm"    # I
    .param p5, "passPhrase"    # [C
    .param p6, "useSHA1"    # Z
    .param p7, "hashedPcks"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;
    .param p8, "unhashedPcks"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;
    .param p9, "rand"    # Ljava/security/SecureRandom;
    .param p10, "provider"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;,
            Ljava/security/NoSuchProviderException;
        }
    .end annotation

    .prologue
    .line 93
    invoke-static/range {p10 .. p10}, Lcom/android/sec/org/bouncycastle/openpgp/PGPUtil;->getProvider(Ljava/lang/String;)Ljava/security/Provider;

    move-result-object v10

    move-object v0, p0

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move-object/from16 v5, p5

    move/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    invoke-direct/range {v0 .. v10}, Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyRingGenerator;-><init>(ILcom/android/sec/org/bouncycastle/openpgp/PGPKeyPair;Ljava/lang/String;I[CZLcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;Ljava/security/SecureRandom;Ljava/security/Provider;)V

    .line 94
    return-void
.end method

.method public constructor <init>(ILcom/android/sec/org/bouncycastle/openpgp/PGPKeyPair;Ljava/lang/String;I[CZLcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;Ljava/security/SecureRandom;Ljava/security/Provider;)V
    .locals 13
    .param p1, "certificationLevel"    # I
    .param p2, "masterKey"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyPair;
    .param p3, "id"    # Ljava/lang/String;
    .param p4, "encAlgorithm"    # I
    .param p5, "passPhrase"    # [C
    .param p6, "useSHA1"    # Z
    .param p7, "hashedPcks"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;
    .param p8, "unhashedPcks"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;
    .param p9, "rand"    # Ljava/security/SecureRandom;
    .param p10, "provider"    # Ljava/security/Provider;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;,
            Ljava/security/NoSuchProviderException;
        }
    .end annotation

    .prologue
    .line 125
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyRingGenerator;->keys:Ljava/util/List;

    .line 126
    iput p1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyRingGenerator;->certificationLevel:I

    .line 127
    iput-object p2, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyRingGenerator;->masterKey:Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyPair;

    .line 128
    move-object/from16 v0, p3

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyRingGenerator;->id:Ljava/lang/String;

    .line 129
    move/from16 v0, p4

    iput v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyRingGenerator;->encAlgorithm:I

    .line 130
    move-object/from16 v0, p5

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyRingGenerator;->passPhrase:[C

    .line 131
    move/from16 v0, p6

    iput-boolean v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyRingGenerator;->useSHA1:Z

    .line 132
    move-object/from16 v0, p7

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyRingGenerator;->hashedPcks:Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;

    .line 133
    move-object/from16 v0, p8

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyRingGenerator;->unhashedPcks:Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;

    .line 134
    move-object/from16 v0, p9

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyRingGenerator;->rand:Ljava/security/SecureRandom;

    .line 135
    move-object/from16 v0, p10

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyRingGenerator;->provider:Ljava/security/Provider;

    .line 137
    iget-object v12, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyRingGenerator;->keys:Ljava/util/List;

    new-instance v1, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;

    move v2, p1

    move-object v3, p2

    move-object/from16 v4, p3

    move/from16 v5, p4

    move-object/from16 v6, p5

    move/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    invoke-direct/range {v1 .. v11}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;-><init>(ILcom/android/sec/org/bouncycastle/openpgp/PGPKeyPair;Ljava/lang/String;I[CZLcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;Ljava/security/SecureRandom;Ljava/security/Provider;)V

    invoke-interface {v12, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 138
    return-void
.end method


# virtual methods
.method public addSubKey(Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyPair;)V
    .locals 2
    .param p1, "keyPair"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyPair;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
        }
    .end annotation

    .prologue
    .line 151
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyRingGenerator;->hashedPcks:Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;

    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyRingGenerator;->unhashedPcks:Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;

    invoke-virtual {p0, p1, v0, v1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyRingGenerator;->addSubKey(Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyPair;Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;)V

    .line 152
    return-void
.end method

.method public addSubKey(Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyPair;Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;)V
    .locals 12
    .param p1, "keyPair"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyPair;
    .param p2, "hashedPcks"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;
    .param p3, "unhashedPcks"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
        }
    .end annotation

    .prologue
    .line 171
    :try_start_0
    new-instance v9, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;

    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyRingGenerator;->masterKey:Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyPair;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyPair;->getPublicKey()Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->getAlgorithm()I

    move-result v0

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyRingGenerator;->provider:Ljava/security/Provider;

    invoke-direct {v9, v0, v1, v2}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;-><init>(IILjava/security/Provider;)V

    .line 176
    .local v9, "sGen":Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;
    const/16 v0, 0x18

    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyRingGenerator;->masterKey:Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyPair;

    invoke-virtual {v1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyPair;->getPrivateKey()Lcom/android/sec/org/bouncycastle/openpgp/PGPPrivateKey;

    move-result-object v1

    invoke-virtual {v9, v0, v1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;->initSign(ILcom/android/sec/org/bouncycastle/openpgp/PGPPrivateKey;)V

    .line 178
    invoke-virtual {v9, p2}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;->setHashedSubpackets(Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;)V

    .line 179
    invoke-virtual {v9, p3}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;->setUnhashedSubpackets(Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;)V

    .line 181
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 183
    .local v10, "subSigs":Ljava/util/List;
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyRingGenerator;->masterKey:Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyPair;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyPair;->getPublicKey()Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;

    move-result-object v0

    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyPair;->getPublicKey()Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;

    move-result-object v1

    invoke-virtual {v9, v0, v1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;->generateCertification(Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;)Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;

    move-result-object v0

    invoke-interface {v10, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 185
    iget-object v11, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyRingGenerator;->keys:Ljava/util/List;

    new-instance v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;

    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyPair;->getPrivateKey()Lcom/android/sec/org/bouncycastle/openpgp/PGPPrivateKey;

    move-result-object v1

    new-instance v2, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;

    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyPair;->getPublicKey()Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;

    move-result-object v3

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4, v10}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;-><init>(Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;Lcom/android/sec/org/bouncycastle/bcpg/TrustPacket;Ljava/util/List;)V

    iget v3, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyRingGenerator;->encAlgorithm:I

    iget-object v4, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyRingGenerator;->passPhrase:[C

    iget-boolean v5, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyRingGenerator;->useSHA1:Z

    iget-object v6, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyRingGenerator;->rand:Ljava/security/SecureRandom;

    iget-object v7, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyRingGenerator;->provider:Ljava/security/Provider;

    invoke-direct/range {v0 .. v7}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;-><init>(Lcom/android/sec/org/bouncycastle/openpgp/PGPPrivateKey;Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;I[CZLjava/security/SecureRandom;Ljava/security/Provider;)V

    invoke-interface {v11, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lcom/android/sec/org/bouncycastle/openpgp/PGPException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 195
    return-void

    .line 187
    .end local v9    # "sGen":Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;
    .end local v10    # "subSigs":Ljava/util/List;
    :catch_0
    move-exception v8

    .line 189
    .local v8, "e":Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
    throw v8

    .line 191
    .end local v8    # "e":Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
    :catch_1
    move-exception v8

    .line 193
    .local v8, "e":Ljava/lang/Exception;
    new-instance v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;

    const-string v1, "exception adding subkey: "

    invoke-direct {v0, v1, v8}, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v0
.end method

.method public generatePublicKeyRing()Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyRing;
    .locals 7

    .prologue
    .line 214
    iget-object v3, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyRingGenerator;->keys:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 215
    .local v0, "it":Ljava/util/Iterator;
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 217
    .local v2, "pubKeys":Ljava/util/List;
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;

    invoke-virtual {v3}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;->getPublicKey()Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 219
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 221
    new-instance v1, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;

    invoke-virtual {v3}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;->getPublicKey()Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;

    move-result-object v3

    invoke-direct {v1, v3}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;-><init>(Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;)V

    .line 223
    .local v1, "k":Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;
    new-instance v3, Lcom/android/sec/org/bouncycastle/bcpg/PublicSubkeyPacket;

    invoke-virtual {v1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->getAlgorithm()I

    move-result v4

    invoke-virtual {v1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->getCreationTime()Ljava/util/Date;

    move-result-object v5

    iget-object v6, v1, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->publicPk:Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;

    invoke-virtual {v6}, Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;->getKey()Lcom/android/sec/org/bouncycastle/bcpg/BCPGKey;

    move-result-object v6

    invoke-direct {v3, v4, v5, v6}, Lcom/android/sec/org/bouncycastle/bcpg/PublicSubkeyPacket;-><init>(ILjava/util/Date;Lcom/android/sec/org/bouncycastle/bcpg/BCPGKey;)V

    iput-object v3, v1, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->publicPk:Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;

    .line 225
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 228
    .end local v1    # "k":Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;
    :cond_0
    new-instance v3, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyRing;

    invoke-direct {v3, v2}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyRing;-><init>(Ljava/util/List;)V

    return-object v3
.end method

.method public generateSecretKeyRing()Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;
    .locals 2

    .prologue
    .line 204
    new-instance v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;

    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyRingGenerator;->keys:Ljava/util/List;

    invoke-direct {v0, v1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;-><init>(Ljava/util/List;)V

    return-object v0
.end method
