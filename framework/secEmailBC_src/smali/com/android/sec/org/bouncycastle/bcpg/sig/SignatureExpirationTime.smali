.class public Lcom/android/sec/org/bouncycastle/bcpg/sig/SignatureExpirationTime;
.super Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;
.source "SignatureExpirationTime.java"


# direct methods
.method public constructor <init>(ZJ)V
    .locals 2
    .param p1, "critical"    # Z
    .param p2, "seconds"    # J

    .prologue
    .line 36
    const/4 v0, 0x3

    invoke-static {p2, p3}, Lcom/android/sec/org/bouncycastle/bcpg/sig/SignatureExpirationTime;->timeToBytes(J)[B

    move-result-object v1

    invoke-direct {p0, v0, p1, v1}, Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;-><init>(IZ[B)V

    .line 37
    return-void
.end method

.method public constructor <init>(Z[B)V
    .locals 1
    .param p1, "critical"    # Z
    .param p2, "data"    # [B

    .prologue
    .line 29
    const/4 v0, 0x3

    invoke-direct {p0, v0, p1, p2}, Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;-><init>(IZ[B)V

    .line 30
    return-void
.end method

.method protected static timeToBytes(J)[B
    .locals 4
    .param p0, "t"    # J

    .prologue
    .line 15
    const/4 v1, 0x4

    new-array v0, v1, [B

    .line 17
    .local v0, "data":[B
    const/4 v1, 0x0

    const/16 v2, 0x18

    shr-long v2, p0, v2

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 18
    const/4 v1, 0x1

    const/16 v2, 0x10

    shr-long v2, p0, v2

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 19
    const/4 v1, 0x2

    const/16 v2, 0x8

    shr-long v2, p0, v2

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 20
    const/4 v1, 0x3

    long-to-int v2, p0

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 22
    return-object v0
.end method


# virtual methods
.method public getTime()J
    .locals 6

    .prologue
    .line 44
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/sig/SignatureExpirationTime;->data:[B

    const/4 v3, 0x0

    aget-byte v2, v2, v3

    and-int/lit16 v2, v2, 0xff

    int-to-long v2, v2

    const/16 v4, 0x18

    shl-long/2addr v2, v4

    iget-object v4, p0, Lcom/android/sec/org/bouncycastle/bcpg/sig/SignatureExpirationTime;->data:[B

    const/4 v5, 0x1

    aget-byte v4, v4, v5

    and-int/lit16 v4, v4, 0xff

    shl-int/lit8 v4, v4, 0x10

    int-to-long v4, v4

    or-long/2addr v2, v4

    iget-object v4, p0, Lcom/android/sec/org/bouncycastle/bcpg/sig/SignatureExpirationTime;->data:[B

    const/4 v5, 0x2

    aget-byte v4, v4, v5

    and-int/lit16 v4, v4, 0xff

    shl-int/lit8 v4, v4, 0x8

    int-to-long v4, v4

    or-long/2addr v2, v4

    iget-object v4, p0, Lcom/android/sec/org/bouncycastle/bcpg/sig/SignatureExpirationTime;->data:[B

    const/4 v5, 0x3

    aget-byte v4, v4, v5

    and-int/lit16 v4, v4, 0xff

    int-to-long v4, v4

    or-long v0, v2, v4

    .line 46
    .local v0, "time":J
    return-wide v0
.end method
