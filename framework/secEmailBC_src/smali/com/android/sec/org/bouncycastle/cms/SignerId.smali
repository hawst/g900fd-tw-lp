.class public Lcom/android/sec/org/bouncycastle/cms/SignerId;
.super Ljava/lang/Object;
.source "SignerId.java"

# interfaces
.implements Lcom/android/sec/org/bouncycastle/util/Selector;


# instance fields
.field private baseSelector:Lcom/android/sec/org/bouncycastle/cert/selector/X509CertificateHolderSelector;


# direct methods
.method public constructor <init>(Lcom/android/sec/org/bouncycastle/asn1/x500/X500Name;Ljava/math/BigInteger;)V
    .locals 1
    .param p1, "issuer"    # Lcom/android/sec/org/bouncycastle/asn1/x500/X500Name;
    .param p2, "serialNumber"    # Ljava/math/BigInteger;

    .prologue
    .line 41
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/sec/org/bouncycastle/cms/SignerId;-><init>(Lcom/android/sec/org/bouncycastle/asn1/x500/X500Name;Ljava/math/BigInteger;[B)V

    .line 42
    return-void
.end method

.method public constructor <init>(Lcom/android/sec/org/bouncycastle/asn1/x500/X500Name;Ljava/math/BigInteger;[B)V
    .locals 1
    .param p1, "issuer"    # Lcom/android/sec/org/bouncycastle/asn1/x500/X500Name;
    .param p2, "serialNumber"    # Ljava/math/BigInteger;
    .param p3, "subjectKeyId"    # [B

    .prologue
    .line 54
    new-instance v0, Lcom/android/sec/org/bouncycastle/cert/selector/X509CertificateHolderSelector;

    invoke-direct {v0, p1, p2, p3}, Lcom/android/sec/org/bouncycastle/cert/selector/X509CertificateHolderSelector;-><init>(Lcom/android/sec/org/bouncycastle/asn1/x500/X500Name;Ljava/math/BigInteger;[B)V

    invoke-direct {p0, v0}, Lcom/android/sec/org/bouncycastle/cms/SignerId;-><init>(Lcom/android/sec/org/bouncycastle/cert/selector/X509CertificateHolderSelector;)V

    .line 55
    return-void
.end method

.method private constructor <init>(Lcom/android/sec/org/bouncycastle/cert/selector/X509CertificateHolderSelector;)V
    .locals 0
    .param p1, "baseSelector"    # Lcom/android/sec/org/bouncycastle/cert/selector/X509CertificateHolderSelector;

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/android/sec/org/bouncycastle/cms/SignerId;->baseSelector:Lcom/android/sec/org/bouncycastle/cert/selector/X509CertificateHolderSelector;

    .line 20
    return-void
.end method

.method public constructor <init>([B)V
    .locals 1
    .param p1, "subjectKeyId"    # [B

    .prologue
    const/4 v0, 0x0

    .line 29
    invoke-direct {p0, v0, v0, p1}, Lcom/android/sec/org/bouncycastle/cms/SignerId;-><init>(Lcom/android/sec/org/bouncycastle/asn1/x500/X500Name;Ljava/math/BigInteger;[B)V

    .line 30
    return-void
.end method


# virtual methods
.method public clone()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 102
    new-instance v0, Lcom/android/sec/org/bouncycastle/cms/SignerId;

    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/cms/SignerId;->baseSelector:Lcom/android/sec/org/bouncycastle/cert/selector/X509CertificateHolderSelector;

    invoke-direct {v0, v1}, Lcom/android/sec/org/bouncycastle/cms/SignerId;-><init>(Lcom/android/sec/org/bouncycastle/cert/selector/X509CertificateHolderSelector;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 80
    instance-of v1, p1, Lcom/android/sec/org/bouncycastle/cms/SignerId;

    if-nez v1, :cond_0

    .line 82
    const/4 v1, 0x0

    .line 87
    :goto_0
    return v1

    :cond_0
    move-object v0, p1

    .line 85
    check-cast v0, Lcom/android/sec/org/bouncycastle/cms/SignerId;

    .line 87
    .local v0, "id":Lcom/android/sec/org/bouncycastle/cms/SignerId;
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/cms/SignerId;->baseSelector:Lcom/android/sec/org/bouncycastle/cert/selector/X509CertificateHolderSelector;

    iget-object v2, v0, Lcom/android/sec/org/bouncycastle/cms/SignerId;->baseSelector:Lcom/android/sec/org/bouncycastle/cert/selector/X509CertificateHolderSelector;

    invoke-virtual {v1, v2}, Lcom/android/sec/org/bouncycastle/cert/selector/X509CertificateHolderSelector;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method

.method public getIssuer()Lcom/android/sec/org/bouncycastle/asn1/x500/X500Name;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/SignerId;->baseSelector:Lcom/android/sec/org/bouncycastle/cert/selector/X509CertificateHolderSelector;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/cert/selector/X509CertificateHolderSelector;->getIssuer()Lcom/android/sec/org/bouncycastle/asn1/x500/X500Name;

    move-result-object v0

    return-object v0
.end method

.method public getSerialNumber()Ljava/math/BigInteger;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/SignerId;->baseSelector:Lcom/android/sec/org/bouncycastle/cert/selector/X509CertificateHolderSelector;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/cert/selector/X509CertificateHolderSelector;->getSerialNumber()Ljava/math/BigInteger;

    move-result-object v0

    return-object v0
.end method

.method public getSubjectKeyIdentifier()[B
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/SignerId;->baseSelector:Lcom/android/sec/org/bouncycastle/cert/selector/X509CertificateHolderSelector;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/cert/selector/X509CertificateHolderSelector;->getSubjectKeyIdentifier()[B

    move-result-object v0

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/SignerId;->baseSelector:Lcom/android/sec/org/bouncycastle/cert/selector/X509CertificateHolderSelector;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/cert/selector/X509CertificateHolderSelector;->hashCode()I

    move-result v0

    return v0
.end method

.method public match(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    .line 92
    instance-of v0, p1, Lcom/android/sec/org/bouncycastle/cms/SignerInformation;

    if-eqz v0, :cond_0

    .line 94
    check-cast p1, Lcom/android/sec/org/bouncycastle/cms/SignerInformation;

    .end local p1    # "obj":Ljava/lang/Object;
    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/cms/SignerInformation;->getSID()Lcom/android/sec/org/bouncycastle/cms/SignerId;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/android/sec/org/bouncycastle/cms/SignerId;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 97
    :goto_0
    return v0

    .restart local p1    # "obj":Ljava/lang/Object;
    :cond_0
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/SignerId;->baseSelector:Lcom/android/sec/org/bouncycastle/cert/selector/X509CertificateHolderSelector;

    invoke-virtual {v0, p1}, Lcom/android/sec/org/bouncycastle/cert/selector/X509CertificateHolderSelector;->match(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method
