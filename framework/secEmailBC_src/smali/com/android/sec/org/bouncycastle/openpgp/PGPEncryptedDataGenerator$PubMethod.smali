.class Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator$PubMethod;
.super Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator$EncMethod;
.source "PGPEncryptedDataGenerator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PubMethod"
.end annotation


# instance fields
.field data:[Ljava/math/BigInteger;

.field pubKey:Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;

.field final synthetic this$0:Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;


# direct methods
.method constructor <init>(Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;)V
    .locals 1
    .param p2, "pubKey"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;

    .prologue
    .line 102
    iput-object p1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator$PubMethod;->this$0:Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator$EncMethod;-><init>(Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator$1;)V

    .line 103
    iput-object p2, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator$PubMethod;->pubKey:Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;

    .line 104
    return-void
.end method


# virtual methods
.method public addSessionInfo([B)V
    .locals 9
    .param p1, "sessionInfo"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 112
    iget-object v5, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator$PubMethod;->pubKey:Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;

    invoke-virtual {v5}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->getAlgorithm()I

    move-result v5

    sparse-switch v5, :sswitch_data_0

    .line 127
    new-instance v5, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "unknown asymmetric algorithm: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator$PubMethod;->pubKey:Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;

    invoke-virtual {v7}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->getAlgorithm()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 116
    :sswitch_0
    const-string v5, "RSA/ECB/PKCS1Padding"

    iget-object v6, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator$PubMethod;->this$0:Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;

    # getter for: Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;->defProvider:Ljava/security/Provider;
    invoke-static {v6}, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;->access$100(Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;)Ljava/security/Provider;

    move-result-object v6

    invoke-static {v5, v6}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;Ljava/security/Provider;)Ljavax/crypto/Cipher;

    move-result-object v2

    .line 130
    .local v2, "c":Ljavax/crypto/Cipher;
    :goto_0
    iget-object v5, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator$PubMethod;->pubKey:Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;

    iget-object v6, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator$PubMethod;->this$0:Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;

    # getter for: Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;->defProvider:Ljava/security/Provider;
    invoke-static {v6}, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;->access$100(Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;)Ljava/security/Provider;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->getKey(Ljava/security/Provider;)Ljava/security/PublicKey;

    move-result-object v4

    .line 132
    .local v4, "key":Ljava/security/Key;
    iget-object v5, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator$PubMethod;->this$0:Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;

    # getter for: Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;->rand:Ljava/security/SecureRandom;
    invoke-static {v5}, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;->access$200(Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;)Ljava/security/SecureRandom;

    move-result-object v5

    invoke-virtual {v2, v7, v4, v5}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;Ljava/security/SecureRandom;)V

    .line 134
    invoke-virtual {v2, p1}, Ljavax/crypto/Cipher;->doFinal([B)[B

    move-result-object v3

    .line 136
    .local v3, "encKey":[B
    iget-object v5, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator$PubMethod;->pubKey:Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;

    invoke-virtual {v5}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->getAlgorithm()I

    move-result v5

    sparse-switch v5, :sswitch_data_1

    .line 157
    new-instance v5, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "unknown asymmetric algorithm: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator$PubMethod;->encAlgorithm:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 120
    .end local v2    # "c":Ljavax/crypto/Cipher;
    .end local v3    # "encKey":[B
    .end local v4    # "key":Ljava/security/Key;
    :sswitch_1
    const-string v5, "ElGamal/ECB/PKCS1Padding"

    iget-object v6, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator$PubMethod;->this$0:Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;

    # getter for: Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;->defProvider:Ljava/security/Provider;
    invoke-static {v6}, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;->access$100(Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;)Ljava/security/Provider;

    move-result-object v6

    invoke-static {v5, v6}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;Ljava/security/Provider;)Ljavax/crypto/Cipher;

    move-result-object v2

    .line 121
    .restart local v2    # "c":Ljavax/crypto/Cipher;
    goto :goto_0

    .line 123
    .end local v2    # "c":Ljavax/crypto/Cipher;
    :sswitch_2
    new-instance v5, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;

    const-string v6, "Can\'t use DSA for encryption."

    invoke-direct {v5, v6}, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 125
    :sswitch_3
    new-instance v5, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;

    const-string v6, "Can\'t use ECDSA for encryption."

    invoke-direct {v5, v6}, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 140
    .restart local v2    # "c":Ljavax/crypto/Cipher;
    .restart local v3    # "encKey":[B
    .restart local v4    # "key":Ljava/security/Key;
    :sswitch_4
    new-array v5, v7, [Ljava/math/BigInteger;

    iput-object v5, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator$PubMethod;->data:[Ljava/math/BigInteger;

    .line 142
    iget-object v5, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator$PubMethod;->data:[Ljava/math/BigInteger;

    new-instance v6, Ljava/math/BigInteger;

    invoke-direct {v6, v7, v3}, Ljava/math/BigInteger;-><init>(I[B)V

    aput-object v6, v5, v8

    .line 159
    :goto_1
    return-void

    .line 146
    :sswitch_5
    array-length v5, v3

    div-int/lit8 v5, v5, 0x2

    new-array v0, v5, [B

    .line 147
    .local v0, "b1":[B
    array-length v5, v3

    div-int/lit8 v5, v5, 0x2

    new-array v1, v5, [B

    .line 149
    .local v1, "b2":[B
    array-length v5, v0

    invoke-static {v3, v8, v0, v8, v5}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 150
    array-length v5, v0

    array-length v6, v1

    invoke-static {v3, v5, v1, v8, v6}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 152
    const/4 v5, 0x2

    new-array v5, v5, [Ljava/math/BigInteger;

    iput-object v5, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator$PubMethod;->data:[Ljava/math/BigInteger;

    .line 153
    iget-object v5, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator$PubMethod;->data:[Ljava/math/BigInteger;

    new-instance v6, Ljava/math/BigInteger;

    invoke-direct {v6, v7, v0}, Ljava/math/BigInteger;-><init>(I[B)V

    aput-object v6, v5, v8

    .line 154
    iget-object v5, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator$PubMethod;->data:[Ljava/math/BigInteger;

    new-instance v6, Ljava/math/BigInteger;

    invoke-direct {v6, v7, v1}, Ljava/math/BigInteger;-><init>(I[B)V

    aput-object v6, v5, v7

    goto :goto_1

    .line 112
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_0
        0x10 -> :sswitch_1
        0x11 -> :sswitch_2
        0x13 -> :sswitch_3
        0x14 -> :sswitch_1
    .end sparse-switch

    .line 136
    :sswitch_data_1
    .sparse-switch
        0x1 -> :sswitch_4
        0x2 -> :sswitch_4
        0x10 -> :sswitch_5
        0x14 -> :sswitch_5
    .end sparse-switch
.end method

.method public encode(Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;)V
    .locals 5
    .param p1, "pOut"    # Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 164
    new-instance v0, Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyEncSessionPacket;

    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator$PubMethod;->pubKey:Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;

    invoke-virtual {v1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->getKeyID()J

    move-result-wide v2

    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator$PubMethod;->pubKey:Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;

    invoke-virtual {v1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->getAlgorithm()I

    move-result v1

    iget-object v4, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator$PubMethod;->data:[Ljava/math/BigInteger;

    invoke-direct {v0, v2, v3, v1, v4}, Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyEncSessionPacket;-><init>(JI[Ljava/math/BigInteger;)V

    .line 166
    .local v0, "pk":Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyEncSessionPacket;
    invoke-virtual {p1, v0}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->writePacket(Lcom/android/sec/org/bouncycastle/bcpg/ContainedPacket;)V

    .line 167
    return-void
.end method
