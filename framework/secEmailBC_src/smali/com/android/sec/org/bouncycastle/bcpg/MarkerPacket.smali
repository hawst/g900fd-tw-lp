.class public Lcom/android/sec/org/bouncycastle/bcpg/MarkerPacket;
.super Lcom/android/sec/org/bouncycastle/bcpg/ContainedPacket;
.source "MarkerPacket.java"


# instance fields
.field marker:[B


# direct methods
.method public constructor <init>(Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;)V
    .locals 1
    .param p1, "in"    # Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/bcpg/ContainedPacket;-><init>()V

    .line 13
    const/4 v0, 0x3

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/MarkerPacket;->marker:[B

    .line 19
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/MarkerPacket;->marker:[B

    invoke-virtual {p1, v0}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->readFully([B)V

    .line 20
    return-void

    .line 13
    nop

    :array_0
    .array-data 1
        0x50t
        0x47t
        0x50t
    .end array-data
.end method


# virtual methods
.method public encode(Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;)V
    .locals 3
    .param p1, "out"    # Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 26
    const/16 v0, 0xa

    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/bcpg/MarkerPacket;->marker:[B

    const/4 v2, 0x1

    invoke-virtual {p1, v0, v1, v2}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->writePacket(I[BZ)V

    .line 27
    return-void
.end method
