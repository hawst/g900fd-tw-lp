.class public Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/ec/SignatureSpi$ecDSA384;
.super Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/ec/SignatureSpi;
.source "SignatureSpi.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/ec/SignatureSpi;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ecDSA384"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    .line 122
    invoke-static {}, Lcom/android/sec/org/bouncycastle/crypto/digests/AndroidDigestFactory;->getSHA384()Lcom/android/sec/org/bouncycastle/crypto/Digest;

    move-result-object v0

    new-instance v1, Lcom/android/sec/org/bouncycastle/crypto/signers/ECDSASigner;

    invoke-direct {v1}, Lcom/android/sec/org/bouncycastle/crypto/signers/ECDSASigner;-><init>()V

    new-instance v2, Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/ec/SignatureSpi$StdDSAEncoder;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/ec/SignatureSpi$StdDSAEncoder;-><init>(Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/ec/SignatureSpi$1;)V

    invoke-direct {p0, v0, v1, v2}, Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/ec/SignatureSpi;-><init>(Lcom/android/sec/org/bouncycastle/crypto/Digest;Lcom/android/sec/org/bouncycastle/crypto/DSA;Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/util/DSAEncoder;)V

    .line 124
    return-void
.end method
