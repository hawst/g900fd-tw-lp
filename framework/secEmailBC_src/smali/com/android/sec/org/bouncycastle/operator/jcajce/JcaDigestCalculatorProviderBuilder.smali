.class public Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaDigestCalculatorProviderBuilder;
.super Ljava/lang/Object;
.source "JcaDigestCalculatorProviderBuilder.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaDigestCalculatorProviderBuilder$DigestOutputStream;
    }
.end annotation


# instance fields
.field private helper:Lcom/android/sec/org/bouncycastle/operator/jcajce/OperatorHelper;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    new-instance v0, Lcom/android/sec/org/bouncycastle/operator/jcajce/OperatorHelper;

    new-instance v1, Lcom/android/sec/org/bouncycastle/jcajce/DefaultJcaJceHelper;

    invoke-direct {v1}, Lcom/android/sec/org/bouncycastle/jcajce/DefaultJcaJceHelper;-><init>()V

    invoke-direct {v0, v1}, Lcom/android/sec/org/bouncycastle/operator/jcajce/OperatorHelper;-><init>(Lcom/android/sec/org/bouncycastle/jcajce/JcaJceHelper;)V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaDigestCalculatorProviderBuilder;->helper:Lcom/android/sec/org/bouncycastle/operator/jcajce/OperatorHelper;

    .line 23
    return-void
.end method

.method static synthetic access$000(Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaDigestCalculatorProviderBuilder;)Lcom/android/sec/org/bouncycastle/operator/jcajce/OperatorHelper;
    .locals 1
    .param p0, "x0"    # Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaDigestCalculatorProviderBuilder;

    .prologue
    .line 17
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaDigestCalculatorProviderBuilder;->helper:Lcom/android/sec/org/bouncycastle/operator/jcajce/OperatorHelper;

    return-object v0
.end method


# virtual methods
.method public build()Lcom/android/sec/org/bouncycastle/operator/DigestCalculatorProvider;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/operator/OperatorCreationException;
        }
    .end annotation

    .prologue
    .line 42
    new-instance v0, Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaDigestCalculatorProviderBuilder$1;

    invoke-direct {v0, p0}, Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaDigestCalculatorProviderBuilder$1;-><init>(Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaDigestCalculatorProviderBuilder;)V

    return-object v0
.end method

.method public setProvider(Ljava/lang/String;)Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaDigestCalculatorProviderBuilder;
    .locals 2
    .param p1, "providerName"    # Ljava/lang/String;

    .prologue
    .line 34
    new-instance v0, Lcom/android/sec/org/bouncycastle/operator/jcajce/OperatorHelper;

    new-instance v1, Lcom/android/sec/org/bouncycastle/jcajce/NamedJcaJceHelper;

    invoke-direct {v1, p1}, Lcom/android/sec/org/bouncycastle/jcajce/NamedJcaJceHelper;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v1}, Lcom/android/sec/org/bouncycastle/operator/jcajce/OperatorHelper;-><init>(Lcom/android/sec/org/bouncycastle/jcajce/JcaJceHelper;)V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaDigestCalculatorProviderBuilder;->helper:Lcom/android/sec/org/bouncycastle/operator/jcajce/OperatorHelper;

    .line 36
    return-object p0
.end method

.method public setProvider(Ljava/security/Provider;)Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaDigestCalculatorProviderBuilder;
    .locals 2
    .param p1, "provider"    # Ljava/security/Provider;

    .prologue
    .line 27
    new-instance v0, Lcom/android/sec/org/bouncycastle/operator/jcajce/OperatorHelper;

    new-instance v1, Lcom/android/sec/org/bouncycastle/jcajce/ProviderJcaJceHelper;

    invoke-direct {v1, p1}, Lcom/android/sec/org/bouncycastle/jcajce/ProviderJcaJceHelper;-><init>(Ljava/security/Provider;)V

    invoke-direct {v0, v1}, Lcom/android/sec/org/bouncycastle/operator/jcajce/OperatorHelper;-><init>(Lcom/android/sec/org/bouncycastle/jcajce/JcaJceHelper;)V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaDigestCalculatorProviderBuilder;->helper:Lcom/android/sec/org/bouncycastle/operator/jcajce/OperatorHelper;

    .line 29
    return-object p0
.end method
