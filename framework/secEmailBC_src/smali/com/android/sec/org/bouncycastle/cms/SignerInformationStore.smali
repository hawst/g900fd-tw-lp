.class public Lcom/android/sec/org/bouncycastle/cms/SignerInformationStore;
.super Ljava/lang/Object;
.source "SignerInformationStore.java"


# instance fields
.field private all:Ljava/util/List;

.field private table:Ljava/util/Map;


# direct methods
.method public constructor <init>(Ljava/util/Collection;)V
    .locals 5
    .param p1, "signerInfos"    # Ljava/util/Collection;

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInformationStore;->all:Ljava/util/List;

    .line 13
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    iput-object v4, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInformationStore;->table:Ljava/util/Map;

    .line 18
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 20
    .local v0, "it":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 22
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/sec/org/bouncycastle/cms/SignerInformation;

    .line 23
    .local v3, "signer":Lcom/android/sec/org/bouncycastle/cms/SignerInformation;
    invoke-virtual {v3}, Lcom/android/sec/org/bouncycastle/cms/SignerInformation;->getSID()Lcom/android/sec/org/bouncycastle/cms/SignerId;

    move-result-object v2

    .line 25
    .local v2, "sid":Lcom/android/sec/org/bouncycastle/cms/SignerId;
    iget-object v4, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInformationStore;->table:Ljava/util/Map;

    invoke-interface {v4, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    .line 26
    .local v1, "list":Ljava/util/List;
    if-nez v1, :cond_0

    .line 28
    new-instance v1, Ljava/util/ArrayList;

    .end local v1    # "list":Ljava/util/List;
    const/4 v4, 0x1

    invoke-direct {v1, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 29
    .restart local v1    # "list":Ljava/util/List;
    iget-object v4, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInformationStore;->table:Ljava/util/Map;

    invoke-interface {v4, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 32
    :cond_0
    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 35
    .end local v1    # "list":Ljava/util/List;
    .end local v2    # "sid":Lcom/android/sec/org/bouncycastle/cms/SignerId;
    .end local v3    # "signer":Lcom/android/sec/org/bouncycastle/cms/SignerInformation;
    :cond_1
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v4, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInformationStore;->all:Ljava/util/List;

    .line 36
    return-void
.end method


# virtual methods
.method public get(Lcom/android/sec/org/bouncycastle/cms/SignerId;)Lcom/android/sec/org/bouncycastle/cms/SignerInformation;
    .locals 2
    .param p1, "selector"    # Lcom/android/sec/org/bouncycastle/cms/SignerId;

    .prologue
    .line 48
    invoke-virtual {p0, p1}, Lcom/android/sec/org/bouncycastle/cms/SignerInformationStore;->getSigners(Lcom/android/sec/org/bouncycastle/cms/SignerId;)Ljava/util/Collection;

    move-result-object v0

    .line 50
    .local v0, "list":Ljava/util/Collection;
    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/sec/org/bouncycastle/cms/SignerInformation;

    goto :goto_0
.end method

.method public getSigners()Ljava/util/Collection;
    .locals 2

    .prologue
    .line 70
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInformationStore;->all:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public getSigners(Lcom/android/sec/org/bouncycastle/cms/SignerId;)Ljava/util/Collection;
    .locals 7
    .param p1, "selector"    # Lcom/android/sec/org/bouncycastle/cms/SignerId;

    .prologue
    .line 82
    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/cms/SignerId;->getIssuer()Lcom/android/sec/org/bouncycastle/asn1/x500/X500Name;

    move-result-object v4

    if-eqz v4, :cond_2

    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/cms/SignerId;->getSubjectKeyIdentifier()[B

    move-result-object v4

    if-eqz v4, :cond_2

    .line 84
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 86
    .local v3, "results":Ljava/util/List;
    new-instance v4, Lcom/android/sec/org/bouncycastle/cms/SignerId;

    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/cms/SignerId;->getIssuer()Lcom/android/sec/org/bouncycastle/asn1/x500/X500Name;

    move-result-object v5

    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/cms/SignerId;->getSerialNumber()Ljava/math/BigInteger;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/android/sec/org/bouncycastle/cms/SignerId;-><init>(Lcom/android/sec/org/bouncycastle/asn1/x500/X500Name;Ljava/math/BigInteger;)V

    invoke-virtual {p0, v4}, Lcom/android/sec/org/bouncycastle/cms/SignerInformationStore;->getSigners(Lcom/android/sec/org/bouncycastle/cms/SignerId;)Ljava/util/Collection;

    move-result-object v1

    .line 88
    .local v1, "match1":Ljava/util/Collection;
    if-eqz v1, :cond_0

    .line 90
    invoke-interface {v3, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 93
    :cond_0
    new-instance v4, Lcom/android/sec/org/bouncycastle/cms/SignerId;

    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/cms/SignerId;->getSubjectKeyIdentifier()[B

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/android/sec/org/bouncycastle/cms/SignerId;-><init>([B)V

    invoke-virtual {p0, v4}, Lcom/android/sec/org/bouncycastle/cms/SignerInformationStore;->getSigners(Lcom/android/sec/org/bouncycastle/cms/SignerId;)Ljava/util/Collection;

    move-result-object v2

    .line 95
    .local v2, "match2":Ljava/util/Collection;
    if-eqz v2, :cond_1

    .line 97
    invoke-interface {v3, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 106
    .end local v1    # "match1":Ljava/util/Collection;
    .end local v2    # "match2":Ljava/util/Collection;
    .end local v3    # "results":Ljava/util/List;
    :cond_1
    :goto_0
    return-object v3

    .line 104
    :cond_2
    iget-object v4, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInformationStore;->table:Ljava/util/Map;

    invoke-interface {v4, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 106
    .local v0, "list":Ljava/util/List;
    if-nez v0, :cond_3

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    :goto_1
    move-object v3, v4

    goto :goto_0

    :cond_3
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    goto :goto_1
.end method

.method public size()I
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInformationStore;->all:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method
