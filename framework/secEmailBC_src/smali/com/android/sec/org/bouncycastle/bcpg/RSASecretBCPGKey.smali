.class public Lcom/android/sec/org/bouncycastle/bcpg/RSASecretBCPGKey;
.super Lcom/android/sec/org/bouncycastle/bcpg/BCPGObject;
.source "RSASecretBCPGKey.java"

# interfaces
.implements Lcom/android/sec/org/bouncycastle/bcpg/BCPGKey;


# instance fields
.field crt:Ljava/math/BigInteger;

.field d:Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;

.field expP:Ljava/math/BigInteger;

.field expQ:Ljava/math/BigInteger;

.field p:Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;

.field q:Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;

.field u:Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;


# direct methods
.method public constructor <init>(Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;)V
    .locals 6
    .param p1, "in"    # Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v4, 0x1

    .line 27
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGObject;-><init>()V

    .line 28
    new-instance v0, Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;

    invoke-direct {v0, p1}, Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;-><init>(Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;)V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/RSASecretBCPGKey;->d:Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;

    .line 29
    new-instance v0, Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;

    invoke-direct {v0, p1}, Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;-><init>(Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;)V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/RSASecretBCPGKey;->p:Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;

    .line 30
    new-instance v0, Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;

    invoke-direct {v0, p1}, Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;-><init>(Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;)V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/RSASecretBCPGKey;->q:Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;

    .line 31
    new-instance v0, Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;

    invoke-direct {v0, p1}, Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;-><init>(Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;)V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/RSASecretBCPGKey;->u:Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;

    .line 33
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/RSASecretBCPGKey;->d:Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;->getValue()Ljava/math/BigInteger;

    move-result-object v0

    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/bcpg/RSASecretBCPGKey;->p:Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;

    invoke-virtual {v1}, Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;->getValue()Ljava/math/BigInteger;

    move-result-object v1

    invoke-static {v4, v5}, Ljava/math/BigInteger;->valueOf(J)Ljava/math/BigInteger;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/math/BigInteger;->subtract(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/math/BigInteger;->remainder(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v0

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/RSASecretBCPGKey;->expP:Ljava/math/BigInteger;

    .line 34
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/RSASecretBCPGKey;->d:Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;->getValue()Ljava/math/BigInteger;

    move-result-object v0

    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/bcpg/RSASecretBCPGKey;->q:Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;

    invoke-virtual {v1}, Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;->getValue()Ljava/math/BigInteger;

    move-result-object v1

    invoke-static {v4, v5}, Ljava/math/BigInteger;->valueOf(J)Ljava/math/BigInteger;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/math/BigInteger;->subtract(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/math/BigInteger;->remainder(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v0

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/RSASecretBCPGKey;->expQ:Ljava/math/BigInteger;

    .line 35
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/RSASecretBCPGKey;->q:Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;->getValue()Ljava/math/BigInteger;

    move-result-object v0

    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/bcpg/RSASecretBCPGKey;->p:Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;

    invoke-virtual {v1}, Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;->getValue()Ljava/math/BigInteger;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/math/BigInteger;->modInverse(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v0

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/RSASecretBCPGKey;->crt:Ljava/math/BigInteger;

    .line 36
    return-void
.end method

.method public constructor <init>(Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/math/BigInteger;)V
    .locals 6
    .param p1, "d"    # Ljava/math/BigInteger;
    .param p2, "p"    # Ljava/math/BigInteger;
    .param p3, "q"    # Ljava/math/BigInteger;

    .prologue
    const-wide/16 v4, 0x1

    .line 48
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGObject;-><init>()V

    .line 52
    invoke-virtual {p2, p3}, Ljava/math/BigInteger;->compareTo(Ljava/math/BigInteger;)I

    move-result v0

    .line 53
    .local v0, "cmp":I
    if-ltz v0, :cond_1

    .line 55
    if-nez v0, :cond_0

    .line 57
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "p and q cannot be equal"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 60
    :cond_0
    move-object v1, p2

    .line 61
    .local v1, "tmp":Ljava/math/BigInteger;
    move-object p2, p3

    .line 62
    move-object p3, v1

    .line 65
    .end local v1    # "tmp":Ljava/math/BigInteger;
    :cond_1
    new-instance v2, Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;

    invoke-direct {v2, p1}, Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;-><init>(Ljava/math/BigInteger;)V

    iput-object v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/RSASecretBCPGKey;->d:Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;

    .line 66
    new-instance v2, Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;

    invoke-direct {v2, p2}, Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;-><init>(Ljava/math/BigInteger;)V

    iput-object v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/RSASecretBCPGKey;->p:Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;

    .line 67
    new-instance v2, Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;

    invoke-direct {v2, p3}, Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;-><init>(Ljava/math/BigInteger;)V

    iput-object v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/RSASecretBCPGKey;->q:Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;

    .line 68
    new-instance v2, Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;

    invoke-virtual {p2, p3}, Ljava/math/BigInteger;->modInverse(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;-><init>(Ljava/math/BigInteger;)V

    iput-object v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/RSASecretBCPGKey;->u:Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;

    .line 70
    invoke-static {v4, v5}, Ljava/math/BigInteger;->valueOf(J)Ljava/math/BigInteger;

    move-result-object v2

    invoke-virtual {p2, v2}, Ljava/math/BigInteger;->subtract(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/math/BigInteger;->remainder(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v2

    iput-object v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/RSASecretBCPGKey;->expP:Ljava/math/BigInteger;

    .line 71
    invoke-static {v4, v5}, Ljava/math/BigInteger;->valueOf(J)Ljava/math/BigInteger;

    move-result-object v2

    invoke-virtual {p3, v2}, Ljava/math/BigInteger;->subtract(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/math/BigInteger;->remainder(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v2

    iput-object v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/RSASecretBCPGKey;->expQ:Ljava/math/BigInteger;

    .line 72
    invoke-virtual {p3, p2}, Ljava/math/BigInteger;->modInverse(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v2

    iput-object v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/RSASecretBCPGKey;->crt:Ljava/math/BigInteger;

    .line 73
    return-void
.end method


# virtual methods
.method public encode(Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;)V
    .locals 1
    .param p1, "out"    # Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 171
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/RSASecretBCPGKey;->d:Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;

    invoke-virtual {p1, v0}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->writeObject(Lcom/android/sec/org/bouncycastle/bcpg/BCPGObject;)V

    .line 172
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/RSASecretBCPGKey;->p:Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;

    invoke-virtual {p1, v0}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->writeObject(Lcom/android/sec/org/bouncycastle/bcpg/BCPGObject;)V

    .line 173
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/RSASecretBCPGKey;->q:Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;

    invoke-virtual {p1, v0}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->writeObject(Lcom/android/sec/org/bouncycastle/bcpg/BCPGObject;)V

    .line 174
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/RSASecretBCPGKey;->u:Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;

    invoke-virtual {p1, v0}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->writeObject(Lcom/android/sec/org/bouncycastle/bcpg/BCPGObject;)V

    .line 175
    return-void
.end method

.method public getCrtCoefficient()Ljava/math/BigInteger;
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/RSASecretBCPGKey;->crt:Ljava/math/BigInteger;

    return-object v0
.end method

.method public getEncoded()[B
    .locals 4

    .prologue
    .line 154
    :try_start_0
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 155
    .local v0, "bOut":Ljava/io/ByteArrayOutputStream;
    new-instance v2, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;

    invoke-direct {v2, v0}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 157
    .local v2, "pgpOut":Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;
    invoke-virtual {v2, p0}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->writeObject(Lcom/android/sec/org/bouncycastle/bcpg/BCPGObject;)V

    .line 159
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 163
    .end local v0    # "bOut":Ljava/io/ByteArrayOutputStream;
    .end local v2    # "pgpOut":Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;
    :goto_0
    return-object v3

    .line 161
    :catch_0
    move-exception v1

    .line 163
    .local v1, "e":Ljava/io/IOException;
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public getFormat()Ljava/lang/String;
    .locals 1

    .prologue
    .line 142
    const-string v0, "PGP"

    return-object v0
.end method

.method public getModulus()Ljava/math/BigInteger;
    .locals 2

    .prologue
    .line 82
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/RSASecretBCPGKey;->p:Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;->getValue()Ljava/math/BigInteger;

    move-result-object v0

    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/bcpg/RSASecretBCPGKey;->q:Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;

    invoke-virtual {v1}, Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;->getValue()Ljava/math/BigInteger;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/math/BigInteger;->multiply(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v0

    return-object v0
.end method

.method public getPrimeExponentP()Ljava/math/BigInteger;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/RSASecretBCPGKey;->expP:Ljava/math/BigInteger;

    return-object v0
.end method

.method public getPrimeExponentQ()Ljava/math/BigInteger;
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/RSASecretBCPGKey;->expQ:Ljava/math/BigInteger;

    return-object v0
.end method

.method public getPrimeP()Ljava/math/BigInteger;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/RSASecretBCPGKey;->p:Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;->getValue()Ljava/math/BigInteger;

    move-result-object v0

    return-object v0
.end method

.method public getPrimeQ()Ljava/math/BigInteger;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/RSASecretBCPGKey;->q:Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;->getValue()Ljava/math/BigInteger;

    move-result-object v0

    return-object v0
.end method

.method public getPrivateExponent()Ljava/math/BigInteger;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/RSASecretBCPGKey;->d:Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;->getValue()Ljava/math/BigInteger;

    move-result-object v0

    return-object v0
.end method
