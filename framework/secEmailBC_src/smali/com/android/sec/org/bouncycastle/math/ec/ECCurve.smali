.class public abstract Lcom/android/sec/org/bouncycastle/math/ec/ECCurve;
.super Ljava/lang/Object;
.source "ECCurve.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/sec/org/bouncycastle/math/ec/ECCurve$F2m;,
        Lcom/android/sec/org/bouncycastle/math/ec/ECCurve$Fp;
    }
.end annotation


# instance fields
.field a:Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

.field b:Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 196
    return-void
.end method

.method private static fromArray([BII)Ljava/math/BigInteger;
    .locals 3
    .param p0, "buf"    # [B
    .param p1, "off"    # I
    .param p2, "length"    # I

    .prologue
    .line 94
    new-array v0, p2, [B

    .line 95
    .local v0, "mag":[B
    const/4 v1, 0x0

    invoke-static {p0, p1, v0, v1, p2}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 96
    new-instance v1, Ljava/math/BigInteger;

    const/4 v2, 0x1

    invoke-direct {v1, v2, v0}, Ljava/math/BigInteger;-><init>(I[B)V

    return-object v1
.end method


# virtual methods
.method public abstract createPoint(Ljava/math/BigInteger;Ljava/math/BigInteger;Z)Lcom/android/sec/org/bouncycastle/math/ec/ECPoint;
.end method

.method public decodePoint([B)Lcom/android/sec/org/bouncycastle/math/ec/ECPoint;
    .locals 9
    .param p1, "encoded"    # [B

    .prologue
    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 41
    const/4 v3, 0x0

    .line 42
    .local v3, "p":Lcom/android/sec/org/bouncycastle/math/ec/ECPoint;
    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve;->getFieldSize()I

    move-result v5

    add-int/lit8 v5, v5, 0x7

    div-int/lit8 v2, v5, 0x8

    .line 44
    .local v2, "expectedLength":I
    aget-byte v5, p1, v8

    packed-switch v5, :pswitch_data_0

    .line 86
    :pswitch_0
    new-instance v5, Ljava/lang/IllegalArgumentException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Invalid point encoding 0x"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    aget-byte v7, p1, v8

    const/16 v8, 0x10

    invoke-static {v7, v8}, Ljava/lang/Integer;->toString(II)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 48
    :pswitch_1
    array-length v5, p1

    if-eq v5, v7, :cond_0

    .line 50
    new-instance v5, Ljava/lang/IllegalArgumentException;

    const-string v6, "Incorrect length for infinity encoding"

    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 53
    :cond_0
    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve;->getInfinity()Lcom/android/sec/org/bouncycastle/math/ec/ECPoint;

    move-result-object v3

    .line 89
    :goto_0
    return-object v3

    .line 59
    :pswitch_2
    array-length v5, p1

    add-int/lit8 v6, v2, 0x1

    if-eq v5, v6, :cond_1

    .line 61
    new-instance v5, Ljava/lang/IllegalArgumentException;

    const-string v6, "Incorrect length for compressed encoding"

    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 64
    :cond_1
    aget-byte v5, p1, v8

    and-int/lit8 v4, v5, 0x1

    .line 65
    .local v4, "yTilde":I
    invoke-static {p1, v7, v2}, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve;->fromArray([BII)Ljava/math/BigInteger;

    move-result-object v0

    .line 67
    .local v0, "X1":Ljava/math/BigInteger;
    invoke-virtual {p0, v4, v0}, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve;->decompressPoint(ILjava/math/BigInteger;)Lcom/android/sec/org/bouncycastle/math/ec/ECPoint;

    move-result-object v3

    .line 68
    goto :goto_0

    .line 74
    .end local v0    # "X1":Ljava/math/BigInteger;
    .end local v4    # "yTilde":I
    :pswitch_3
    array-length v5, p1

    mul-int/lit8 v6, v2, 0x2

    add-int/lit8 v6, v6, 0x1

    if-eq v5, v6, :cond_2

    .line 76
    new-instance v5, Ljava/lang/IllegalArgumentException;

    const-string v6, "Incorrect length for uncompressed/hybrid encoding"

    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 79
    :cond_2
    invoke-static {p1, v7, v2}, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve;->fromArray([BII)Ljava/math/BigInteger;

    move-result-object v0

    .line 80
    .restart local v0    # "X1":Ljava/math/BigInteger;
    add-int/lit8 v5, v2, 0x1

    invoke-static {p1, v5, v2}, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve;->fromArray([BII)Ljava/math/BigInteger;

    move-result-object v1

    .line 82
    .local v1, "Y1":Ljava/math/BigInteger;
    invoke-virtual {p0, v0, v1, v8}, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve;->createPoint(Ljava/math/BigInteger;Ljava/math/BigInteger;Z)Lcom/android/sec/org/bouncycastle/math/ec/ECPoint;

    move-result-object v3

    .line 83
    goto :goto_0

    .line 44
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method protected abstract decompressPoint(ILjava/math/BigInteger;)Lcom/android/sec/org/bouncycastle/math/ec/ECPoint;
.end method

.method public abstract fromBigInteger(Ljava/math/BigInteger;)Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;
.end method

.method public getA()Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve;->a:Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    return-object v0
.end method

.method public getB()Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/math/ec/ECCurve;->b:Lcom/android/sec/org/bouncycastle/math/ec/ECFieldElement;

    return-object v0
.end method

.method public abstract getFieldSize()I
.end method

.method public abstract getInfinity()Lcom/android/sec/org/bouncycastle/math/ec/ECPoint;
.end method
