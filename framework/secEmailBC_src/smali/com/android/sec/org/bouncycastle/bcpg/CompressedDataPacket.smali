.class public Lcom/android/sec/org/bouncycastle/bcpg/CompressedDataPacket;
.super Lcom/android/sec/org/bouncycastle/bcpg/InputStreamPacket;
.source "CompressedDataPacket.java"


# instance fields
.field algorithm:I


# direct methods
.method constructor <init>(Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;)V
    .locals 1
    .param p1, "in"    # Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 17
    invoke-direct {p0, p1}, Lcom/android/sec/org/bouncycastle/bcpg/InputStreamPacket;-><init>(Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;)V

    .line 19
    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->read()I

    move-result v0

    iput v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/CompressedDataPacket;->algorithm:I

    .line 20
    return-void
.end method


# virtual methods
.method public getAlgorithm()I
    .locals 1

    .prologue
    .line 29
    iget v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/CompressedDataPacket;->algorithm:I

    return v0
.end method
