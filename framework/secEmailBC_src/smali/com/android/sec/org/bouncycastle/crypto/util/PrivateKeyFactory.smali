.class public Lcom/android/sec/org/bouncycastle/crypto/util/PrivateKeyFactory;
.super Ljava/lang/Object;
.source "PrivateKeyFactory.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static createKey(Lcom/android/sec/org/bouncycastle/asn1/pkcs/PrivateKeyInfo;)Lcom/android/sec/org/bouncycastle/crypto/params/AsymmetricKeyParameter;
    .locals 23
    .param p0, "keyInfo"    # Lcom/android/sec/org/bouncycastle/asn1/pkcs/PrivateKeyInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 85
    invoke-virtual/range {p0 .. p0}, Lcom/android/sec/org/bouncycastle/asn1/pkcs/PrivateKeyInfo;->getPrivateKeyAlgorithm()Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;

    move-result-object v10

    .line 87
    .local v10, "algId":Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;
    invoke-virtual {v10}, Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;->getAlgorithm()Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    move-result-object v2

    sget-object v3, Lcom/android/sec/org/bouncycastle/asn1/pkcs/PKCSObjectIdentifiers;->rsaEncryption:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    invoke-virtual {v2, v3}, Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 89
    invoke-virtual/range {p0 .. p0}, Lcom/android/sec/org/bouncycastle/asn1/pkcs/PrivateKeyInfo;->parsePrivateKey()Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;

    move-result-object v2

    invoke-static {v2}, Lcom/android/sec/org/bouncycastle/asn1/pkcs/RSAPrivateKey;->getInstance(Ljava/lang/Object;)Lcom/android/sec/org/bouncycastle/asn1/pkcs/RSAPrivateKey;

    move-result-object v16

    .line 91
    .local v16, "keyStructure":Lcom/android/sec/org/bouncycastle/asn1/pkcs/RSAPrivateKey;
    new-instance v1, Lcom/android/sec/org/bouncycastle/crypto/params/RSAPrivateCrtKeyParameters;

    invoke-virtual/range {v16 .. v16}, Lcom/android/sec/org/bouncycastle/asn1/pkcs/RSAPrivateKey;->getModulus()Ljava/math/BigInteger;

    move-result-object v2

    invoke-virtual/range {v16 .. v16}, Lcom/android/sec/org/bouncycastle/asn1/pkcs/RSAPrivateKey;->getPublicExponent()Ljava/math/BigInteger;

    move-result-object v3

    invoke-virtual/range {v16 .. v16}, Lcom/android/sec/org/bouncycastle/asn1/pkcs/RSAPrivateKey;->getPrivateExponent()Ljava/math/BigInteger;

    move-result-object v4

    invoke-virtual/range {v16 .. v16}, Lcom/android/sec/org/bouncycastle/asn1/pkcs/RSAPrivateKey;->getPrime1()Ljava/math/BigInteger;

    move-result-object v5

    invoke-virtual/range {v16 .. v16}, Lcom/android/sec/org/bouncycastle/asn1/pkcs/RSAPrivateKey;->getPrime2()Ljava/math/BigInteger;

    move-result-object v6

    invoke-virtual/range {v16 .. v16}, Lcom/android/sec/org/bouncycastle/asn1/pkcs/RSAPrivateKey;->getExponent1()Ljava/math/BigInteger;

    move-result-object v7

    invoke-virtual/range {v16 .. v16}, Lcom/android/sec/org/bouncycastle/asn1/pkcs/RSAPrivateKey;->getExponent2()Ljava/math/BigInteger;

    move-result-object v8

    invoke-virtual/range {v16 .. v16}, Lcom/android/sec/org/bouncycastle/asn1/pkcs/RSAPrivateKey;->getCoefficient()Ljava/math/BigInteger;

    move-result-object v9

    invoke-direct/range {v1 .. v9}, Lcom/android/sec/org/bouncycastle/crypto/params/RSAPrivateCrtKeyParameters;-><init>(Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/math/BigInteger;)V

    .line 173
    .end local v16    # "keyStructure":Lcom/android/sec/org/bouncycastle/asn1/pkcs/RSAPrivateKey;
    :goto_0
    return-object v1

    .line 98
    :cond_0
    invoke-virtual {v10}, Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;->getAlgorithm()Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    move-result-object v2

    sget-object v3, Lcom/android/sec/org/bouncycastle/asn1/pkcs/PKCSObjectIdentifiers;->dhKeyAgreement:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    invoke-virtual {v2, v3}, Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 100
    invoke-virtual {v10}, Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;->getParameters()Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;

    move-result-object v2

    invoke-static {v2}, Lcom/android/sec/org/bouncycastle/asn1/pkcs/DHParameter;->getInstance(Ljava/lang/Object;)Lcom/android/sec/org/bouncycastle/asn1/pkcs/DHParameter;

    move-result-object v21

    .line 101
    .local v21, "params":Lcom/android/sec/org/bouncycastle/asn1/pkcs/DHParameter;
    invoke-virtual/range {p0 .. p0}, Lcom/android/sec/org/bouncycastle/asn1/pkcs/PrivateKeyInfo;->parsePrivateKey()Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;

    move-result-object v13

    check-cast v13, Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;

    .line 103
    .local v13, "derX":Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;
    invoke-virtual/range {v21 .. v21}, Lcom/android/sec/org/bouncycastle/asn1/pkcs/DHParameter;->getL()Ljava/math/BigInteger;

    move-result-object v18

    .line 104
    .local v18, "lVal":Ljava/math/BigInteger;
    if-nez v18, :cond_1

    const/16 v17, 0x0

    .line 105
    .local v17, "l":I
    :goto_1
    new-instance v14, Lcom/android/sec/org/bouncycastle/crypto/params/DHParameters;

    invoke-virtual/range {v21 .. v21}, Lcom/android/sec/org/bouncycastle/asn1/pkcs/DHParameter;->getP()Ljava/math/BigInteger;

    move-result-object v2

    invoke-virtual/range {v21 .. v21}, Lcom/android/sec/org/bouncycastle/asn1/pkcs/DHParameter;->getG()Ljava/math/BigInteger;

    move-result-object v3

    const/4 v4, 0x0

    move/from16 v0, v17

    invoke-direct {v14, v2, v3, v4, v0}, Lcom/android/sec/org/bouncycastle/crypto/params/DHParameters;-><init>(Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/math/BigInteger;I)V

    .line 107
    .local v14, "dhParams":Lcom/android/sec/org/bouncycastle/crypto/params/DHParameters;
    new-instance v1, Lcom/android/sec/org/bouncycastle/crypto/params/DHPrivateKeyParameters;

    invoke-virtual {v13}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;->getValue()Ljava/math/BigInteger;

    move-result-object v2

    invoke-direct {v1, v2, v14}, Lcom/android/sec/org/bouncycastle/crypto/params/DHPrivateKeyParameters;-><init>(Ljava/math/BigInteger;Lcom/android/sec/org/bouncycastle/crypto/params/DHParameters;)V

    goto :goto_0

    .line 104
    .end local v14    # "dhParams":Lcom/android/sec/org/bouncycastle/crypto/params/DHParameters;
    .end local v17    # "l":I
    :cond_1
    invoke-virtual/range {v18 .. v18}, Ljava/math/BigInteger;->intValue()I

    move-result v17

    goto :goto_1

    .line 119
    .end local v13    # "derX":Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;
    .end local v18    # "lVal":Ljava/math/BigInteger;
    .end local v21    # "params":Lcom/android/sec/org/bouncycastle/asn1/pkcs/DHParameter;
    :cond_2
    invoke-virtual {v10}, Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;->getAlgorithm()Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    move-result-object v2

    sget-object v3, Lcom/android/sec/org/bouncycastle/asn1/x9/X9ObjectIdentifiers;->id_dsa:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    invoke-virtual {v2, v3}, Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 121
    invoke-virtual/range {p0 .. p0}, Lcom/android/sec/org/bouncycastle/asn1/pkcs/PrivateKeyInfo;->parsePrivateKey()Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;

    move-result-object v13

    check-cast v13, Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;

    .line 122
    .restart local v13    # "derX":Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;
    invoke-virtual {v10}, Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;->getParameters()Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;

    move-result-object v12

    .line 124
    .local v12, "de":Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;
    const/16 v20, 0x0

    .line 125
    .local v20, "parameters":Lcom/android/sec/org/bouncycastle/crypto/params/DSAParameters;
    if-eqz v12, :cond_3

    .line 127
    invoke-interface {v12}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;->toASN1Primitive()Lcom/android/sec/org/bouncycastle/asn1/ASN1Primitive;

    move-result-object v2

    invoke-static {v2}, Lcom/android/sec/org/bouncycastle/asn1/x509/DSAParameter;->getInstance(Ljava/lang/Object;)Lcom/android/sec/org/bouncycastle/asn1/x509/DSAParameter;

    move-result-object v21

    .line 128
    .local v21, "params":Lcom/android/sec/org/bouncycastle/asn1/x509/DSAParameter;
    new-instance v20, Lcom/android/sec/org/bouncycastle/crypto/params/DSAParameters;

    .end local v20    # "parameters":Lcom/android/sec/org/bouncycastle/crypto/params/DSAParameters;
    invoke-virtual/range {v21 .. v21}, Lcom/android/sec/org/bouncycastle/asn1/x509/DSAParameter;->getP()Ljava/math/BigInteger;

    move-result-object v2

    invoke-virtual/range {v21 .. v21}, Lcom/android/sec/org/bouncycastle/asn1/x509/DSAParameter;->getQ()Ljava/math/BigInteger;

    move-result-object v3

    invoke-virtual/range {v21 .. v21}, Lcom/android/sec/org/bouncycastle/asn1/x509/DSAParameter;->getG()Ljava/math/BigInteger;

    move-result-object v4

    move-object/from16 v0, v20

    invoke-direct {v0, v2, v3, v4}, Lcom/android/sec/org/bouncycastle/crypto/params/DSAParameters;-><init>(Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/math/BigInteger;)V

    .line 131
    .end local v21    # "params":Lcom/android/sec/org/bouncycastle/asn1/x509/DSAParameter;
    .restart local v20    # "parameters":Lcom/android/sec/org/bouncycastle/crypto/params/DSAParameters;
    :cond_3
    new-instance v1, Lcom/android/sec/org/bouncycastle/crypto/params/DSAPrivateKeyParameters;

    invoke-virtual {v13}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;->getValue()Ljava/math/BigInteger;

    move-result-object v2

    move-object/from16 v0, v20

    invoke-direct {v1, v2, v0}, Lcom/android/sec/org/bouncycastle/crypto/params/DSAPrivateKeyParameters;-><init>(Ljava/math/BigInteger;Lcom/android/sec/org/bouncycastle/crypto/params/DSAParameters;)V

    goto/16 :goto_0

    .line 133
    .end local v12    # "de":Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;
    .end local v13    # "derX":Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;
    .end local v20    # "parameters":Lcom/android/sec/org/bouncycastle/crypto/params/DSAParameters;
    :cond_4
    invoke-virtual {v10}, Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;->getAlgorithm()Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    move-result-object v2

    sget-object v3, Lcom/android/sec/org/bouncycastle/asn1/x9/X9ObjectIdentifiers;->id_ecPublicKey:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    invoke-virtual {v2, v3}, Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 135
    new-instance v21, Lcom/android/sec/org/bouncycastle/asn1/x9/X962Parameters;

    invoke-virtual {v10}, Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;->getParameters()Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;

    move-result-object v2

    check-cast v2, Lcom/android/sec/org/bouncycastle/asn1/ASN1Primitive;

    move-object/from16 v0, v21

    invoke-direct {v0, v2}, Lcom/android/sec/org/bouncycastle/asn1/x9/X962Parameters;-><init>(Lcom/android/sec/org/bouncycastle/asn1/ASN1Primitive;)V

    .line 138
    .local v21, "params":Lcom/android/sec/org/bouncycastle/asn1/x9/X962Parameters;
    invoke-virtual/range {v21 .. v21}, Lcom/android/sec/org/bouncycastle/asn1/x9/X962Parameters;->isNamedCurve()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 140
    invoke-virtual/range {v21 .. v21}, Lcom/android/sec/org/bouncycastle/asn1/x9/X962Parameters;->getParameters()Lcom/android/sec/org/bouncycastle/asn1/ASN1Primitive;

    move-result-object v2

    invoke-static {v2}, Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;->getInstance(Ljava/lang/Object;)Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    move-result-object v19

    .line 141
    .local v19, "oid":Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;
    invoke-static/range {v19 .. v19}, Lcom/android/sec/org/bouncycastle/asn1/x9/X962NamedCurves;->getByOID(Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;)Lcom/android/sec/org/bouncycastle/asn1/x9/X9ECParameters;

    move-result-object v22

    .line 143
    .local v22, "x9":Lcom/android/sec/org/bouncycastle/asn1/x9/X9ECParameters;
    if-nez v22, :cond_5

    .line 145
    invoke-static/range {v19 .. v19}, Lcom/android/sec/org/bouncycastle/asn1/sec/SECNamedCurves;->getByOID(Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;)Lcom/android/sec/org/bouncycastle/asn1/x9/X9ECParameters;

    move-result-object v22

    .line 147
    if-nez v22, :cond_5

    .line 149
    invoke-static/range {v19 .. v19}, Lcom/android/sec/org/bouncycastle/asn1/nist/NISTNamedCurves;->getByOID(Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;)Lcom/android/sec/org/bouncycastle/asn1/x9/X9ECParameters;

    move-result-object v22

    .line 165
    .end local v19    # "oid":Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;
    :cond_5
    :goto_2
    invoke-virtual/range {p0 .. p0}, Lcom/android/sec/org/bouncycastle/asn1/pkcs/PrivateKeyInfo;->parsePrivateKey()Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;

    move-result-object v2

    invoke-static {v2}, Lcom/android/sec/org/bouncycastle/asn1/sec/ECPrivateKey;->getInstance(Ljava/lang/Object;)Lcom/android/sec/org/bouncycastle/asn1/sec/ECPrivateKey;

    move-result-object v15

    .line 166
    .local v15, "ec":Lcom/android/sec/org/bouncycastle/asn1/sec/ECPrivateKey;
    invoke-virtual {v15}, Lcom/android/sec/org/bouncycastle/asn1/sec/ECPrivateKey;->getKey()Ljava/math/BigInteger;

    move-result-object v11

    .line 170
    .local v11, "d":Ljava/math/BigInteger;
    new-instance v1, Lcom/android/sec/org/bouncycastle/crypto/params/ECDomainParameters;

    invoke-virtual/range {v22 .. v22}, Lcom/android/sec/org/bouncycastle/asn1/x9/X9ECParameters;->getCurve()Lcom/android/sec/org/bouncycastle/math/ec/ECCurve;

    move-result-object v2

    invoke-virtual/range {v22 .. v22}, Lcom/android/sec/org/bouncycastle/asn1/x9/X9ECParameters;->getG()Lcom/android/sec/org/bouncycastle/math/ec/ECPoint;

    move-result-object v3

    invoke-virtual/range {v22 .. v22}, Lcom/android/sec/org/bouncycastle/asn1/x9/X9ECParameters;->getN()Ljava/math/BigInteger;

    move-result-object v4

    invoke-virtual/range {v22 .. v22}, Lcom/android/sec/org/bouncycastle/asn1/x9/X9ECParameters;->getH()Ljava/math/BigInteger;

    move-result-object v5

    invoke-virtual/range {v22 .. v22}, Lcom/android/sec/org/bouncycastle/asn1/x9/X9ECParameters;->getSeed()[B

    move-result-object v6

    invoke-direct/range {v1 .. v6}, Lcom/android/sec/org/bouncycastle/crypto/params/ECDomainParameters;-><init>(Lcom/android/sec/org/bouncycastle/math/ec/ECCurve;Lcom/android/sec/org/bouncycastle/math/ec/ECPoint;Ljava/math/BigInteger;Ljava/math/BigInteger;[B)V

    .line 173
    .local v1, "dParams":Lcom/android/sec/org/bouncycastle/crypto/params/ECDomainParameters;
    new-instance v2, Lcom/android/sec/org/bouncycastle/crypto/params/ECPrivateKeyParameters;

    invoke-direct {v2, v11, v1}, Lcom/android/sec/org/bouncycastle/crypto/params/ECPrivateKeyParameters;-><init>(Ljava/math/BigInteger;Lcom/android/sec/org/bouncycastle/crypto/params/ECDomainParameters;)V

    move-object v1, v2

    goto/16 :goto_0

    .line 162
    .end local v1    # "dParams":Lcom/android/sec/org/bouncycastle/crypto/params/ECDomainParameters;
    .end local v11    # "d":Ljava/math/BigInteger;
    .end local v15    # "ec":Lcom/android/sec/org/bouncycastle/asn1/sec/ECPrivateKey;
    .end local v22    # "x9":Lcom/android/sec/org/bouncycastle/asn1/x9/X9ECParameters;
    :cond_6
    invoke-virtual/range {v21 .. v21}, Lcom/android/sec/org/bouncycastle/asn1/x9/X962Parameters;->getParameters()Lcom/android/sec/org/bouncycastle/asn1/ASN1Primitive;

    move-result-object v2

    invoke-static {v2}, Lcom/android/sec/org/bouncycastle/asn1/x9/X9ECParameters;->getInstance(Ljava/lang/Object;)Lcom/android/sec/org/bouncycastle/asn1/x9/X9ECParameters;

    move-result-object v22

    .restart local v22    # "x9":Lcom/android/sec/org/bouncycastle/asn1/x9/X9ECParameters;
    goto :goto_2

    .line 177
    .end local v21    # "params":Lcom/android/sec/org/bouncycastle/asn1/x9/X962Parameters;
    .end local v22    # "x9":Lcom/android/sec/org/bouncycastle/asn1/x9/X9ECParameters;
    :cond_7
    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "algorithm identifier in key not recognised"

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public static createKey(Ljava/io/InputStream;)Lcom/android/sec/org/bouncycastle/crypto/params/AsymmetricKeyParameter;
    .locals 1
    .param p0, "inStr"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 73
    new-instance v0, Lcom/android/sec/org/bouncycastle/asn1/ASN1InputStream;

    invoke-direct {v0, p0}, Lcom/android/sec/org/bouncycastle/asn1/ASN1InputStream;-><init>(Ljava/io/InputStream;)V

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/ASN1InputStream;->readObject()Lcom/android/sec/org/bouncycastle/asn1/ASN1Primitive;

    move-result-object v0

    invoke-static {v0}, Lcom/android/sec/org/bouncycastle/asn1/pkcs/PrivateKeyInfo;->getInstance(Ljava/lang/Object;)Lcom/android/sec/org/bouncycastle/asn1/pkcs/PrivateKeyInfo;

    move-result-object v0

    invoke-static {v0}, Lcom/android/sec/org/bouncycastle/crypto/util/PrivateKeyFactory;->createKey(Lcom/android/sec/org/bouncycastle/asn1/pkcs/PrivateKeyInfo;)Lcom/android/sec/org/bouncycastle/crypto/params/AsymmetricKeyParameter;

    move-result-object v0

    return-object v0
.end method

.method public static createKey([B)Lcom/android/sec/org/bouncycastle/crypto/params/AsymmetricKeyParameter;
    .locals 1
    .param p0, "privateKeyInfoData"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 60
    invoke-static {p0}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Primitive;->fromByteArray([B)Lcom/android/sec/org/bouncycastle/asn1/ASN1Primitive;

    move-result-object v0

    invoke-static {v0}, Lcom/android/sec/org/bouncycastle/asn1/pkcs/PrivateKeyInfo;->getInstance(Ljava/lang/Object;)Lcom/android/sec/org/bouncycastle/asn1/pkcs/PrivateKeyInfo;

    move-result-object v0

    invoke-static {v0}, Lcom/android/sec/org/bouncycastle/crypto/util/PrivateKeyFactory;->createKey(Lcom/android/sec/org/bouncycastle/asn1/pkcs/PrivateKeyInfo;)Lcom/android/sec/org/bouncycastle/crypto/params/AsymmetricKeyParameter;

    move-result-object v0

    return-object v0
.end method
