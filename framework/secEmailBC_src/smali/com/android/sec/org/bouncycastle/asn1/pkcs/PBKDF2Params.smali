.class public Lcom/android/sec/org/bouncycastle/asn1/pkcs/PBKDF2Params;
.super Lcom/android/sec/org/bouncycastle/asn1/ASN1Object;
.source "PBKDF2Params.java"


# instance fields
.field private iterationCount:Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;

.field private keyLength:Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;

.field private octStr:Lcom/android/sec/org/bouncycastle/asn1/ASN1OctetString;


# direct methods
.method private constructor <init>(Lcom/android/sec/org/bouncycastle/asn1/ASN1Sequence;)V
    .locals 2
    .param p1, "seq"    # Lcom/android/sec/org/bouncycastle/asn1/ASN1Sequence;

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Object;-><init>()V

    .line 59
    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Sequence;->getObjects()Ljava/util/Enumeration;

    move-result-object v0

    .line 61
    .local v0, "e":Ljava/util/Enumeration;
    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/sec/org/bouncycastle/asn1/ASN1OctetString;

    iput-object v1, p0, Lcom/android/sec/org/bouncycastle/asn1/pkcs/PBKDF2Params;->octStr:Lcom/android/sec/org/bouncycastle/asn1/ASN1OctetString;

    .line 62
    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;

    iput-object v1, p0, Lcom/android/sec/org/bouncycastle/asn1/pkcs/PBKDF2Params;->iterationCount:Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;

    .line 64
    invoke-interface {v0}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 66
    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;

    iput-object v1, p0, Lcom/android/sec/org/bouncycastle/asn1/pkcs/PBKDF2Params;->keyLength:Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;

    .line 72
    :goto_0
    return-void

    .line 70
    :cond_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/sec/org/bouncycastle/asn1/pkcs/PBKDF2Params;->keyLength:Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;

    goto :goto_0
.end method

.method public constructor <init>([BI)V
    .locals 4
    .param p1, "salt"    # [B
    .param p2, "iterationCount"    # I

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Object;-><init>()V

    .line 42
    new-instance v0, Lcom/android/sec/org/bouncycastle/asn1/DEROctetString;

    invoke-direct {v0, p1}, Lcom/android/sec/org/bouncycastle/asn1/DEROctetString;-><init>([B)V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/asn1/pkcs/PBKDF2Params;->octStr:Lcom/android/sec/org/bouncycastle/asn1/ASN1OctetString;

    .line 43
    new-instance v0, Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;

    int-to-long v2, p2

    invoke-direct {v0, v2, v3}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;-><init>(J)V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/asn1/pkcs/PBKDF2Params;->iterationCount:Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;

    .line 44
    return-void
.end method

.method public constructor <init>([BII)V
    .locals 4
    .param p1, "salt"    # [B
    .param p2, "iterationCount"    # I
    .param p3, "keyLength"    # I

    .prologue
    .line 51
    invoke-direct {p0, p1, p2}, Lcom/android/sec/org/bouncycastle/asn1/pkcs/PBKDF2Params;-><init>([BI)V

    .line 53
    new-instance v0, Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;

    int-to-long v2, p3

    invoke-direct {v0, v2, v3}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;-><init>(J)V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/asn1/pkcs/PBKDF2Params;->keyLength:Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;

    .line 54
    return-void
.end method

.method public static getInstance(Ljava/lang/Object;)Lcom/android/sec/org/bouncycastle/asn1/pkcs/PBKDF2Params;
    .locals 2
    .param p0, "obj"    # Ljava/lang/Object;

    .prologue
    .line 25
    instance-of v0, p0, Lcom/android/sec/org/bouncycastle/asn1/pkcs/PBKDF2Params;

    if-eqz v0, :cond_0

    .line 27
    check-cast p0, Lcom/android/sec/org/bouncycastle/asn1/pkcs/PBKDF2Params;

    .line 35
    .end local p0    # "obj":Ljava/lang/Object;
    :goto_0
    return-object p0

    .line 30
    .restart local p0    # "obj":Ljava/lang/Object;
    :cond_0
    if-eqz p0, :cond_1

    .line 32
    new-instance v0, Lcom/android/sec/org/bouncycastle/asn1/pkcs/PBKDF2Params;

    invoke-static {p0}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Sequence;->getInstance(Ljava/lang/Object;)Lcom/android/sec/org/bouncycastle/asn1/ASN1Sequence;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/sec/org/bouncycastle/asn1/pkcs/PBKDF2Params;-><init>(Lcom/android/sec/org/bouncycastle/asn1/ASN1Sequence;)V

    move-object p0, v0

    goto :goto_0

    .line 35
    :cond_1
    const/4 p0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getIterationCount()Ljava/math/BigInteger;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/asn1/pkcs/PBKDF2Params;->iterationCount:Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;->getValue()Ljava/math/BigInteger;

    move-result-object v0

    return-object v0
.end method

.method public getKeyLength()Ljava/math/BigInteger;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/asn1/pkcs/PBKDF2Params;->keyLength:Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;

    if-eqz v0, :cond_0

    .line 88
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/asn1/pkcs/PBKDF2Params;->keyLength:Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;->getValue()Ljava/math/BigInteger;

    move-result-object v0

    .line 91
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getSalt()[B
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/asn1/pkcs/PBKDF2Params;->octStr:Lcom/android/sec/org/bouncycastle/asn1/ASN1OctetString;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/ASN1OctetString;->getOctets()[B

    move-result-object v0

    return-object v0
.end method

.method public toASN1Primitive()Lcom/android/sec/org/bouncycastle/asn1/ASN1Primitive;
    .locals 2

    .prologue
    .line 96
    new-instance v0, Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;

    invoke-direct {v0}, Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;-><init>()V

    .line 98
    .local v0, "v":Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/asn1/pkcs/PBKDF2Params;->octStr:Lcom/android/sec/org/bouncycastle/asn1/ASN1OctetString;

    invoke-virtual {v0, v1}, Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;->add(Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;)V

    .line 99
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/asn1/pkcs/PBKDF2Params;->iterationCount:Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;

    invoke-virtual {v0, v1}, Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;->add(Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;)V

    .line 101
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/asn1/pkcs/PBKDF2Params;->keyLength:Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;

    if-eqz v1, :cond_0

    .line 103
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/asn1/pkcs/PBKDF2Params;->keyLength:Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;

    invoke-virtual {v0, v1}, Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;->add(Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;)V

    .line 106
    :cond_0
    new-instance v1, Lcom/android/sec/org/bouncycastle/asn1/DERSequence;

    invoke-direct {v1, v0}, Lcom/android/sec/org/bouncycastle/asn1/DERSequence;-><init>(Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;)V

    return-object v1
.end method
