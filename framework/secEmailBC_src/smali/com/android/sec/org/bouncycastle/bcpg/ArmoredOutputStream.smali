.class public Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;
.super Ljava/io/OutputStream;
.source "ArmoredOutputStream.java"


# static fields
.field private static final encodingTable:[B


# instance fields
.field buf:[I

.field bufPtr:I

.field chunkCount:I

.field clearText:Z

.field crc:Lcom/android/sec/org/bouncycastle/bcpg/CRC24;

.field footerStart:Ljava/lang/String;

.field footerTail:Ljava/lang/String;

.field headerStart:Ljava/lang/String;

.field headerTail:Ljava/lang/String;

.field headers:Ljava/util/Hashtable;

.field lastb:I

.field newLine:Z

.field nl:Ljava/lang/String;

.field out:Ljava/io/OutputStream;

.field start:Z

.field type:Ljava/lang/String;

.field version:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 13
    const/16 v0, 0x40

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->encodingTable:[B

    return-void

    :array_0
    .array-data 1
        0x41t
        0x42t
        0x43t
        0x44t
        0x45t
        0x46t
        0x47t
        0x48t
        0x49t
        0x4at
        0x4bt
        0x4ct
        0x4dt
        0x4et
        0x4ft
        0x50t
        0x51t
        0x52t
        0x53t
        0x54t
        0x55t
        0x56t
        0x57t
        0x58t
        0x59t
        0x5at
        0x61t
        0x62t
        0x63t
        0x64t
        0x65t
        0x66t
        0x67t
        0x68t
        0x69t
        0x6at
        0x6bt
        0x6ct
        0x6dt
        0x6et
        0x6ft
        0x70t
        0x71t
        0x72t
        0x73t
        0x74t
        0x75t
        0x76t
        0x77t
        0x78t
        0x79t
        0x7at
        0x30t
        0x31t
        0x32t
        0x33t
        0x34t
        0x35t
        0x36t
        0x37t
        0x38t
        0x39t
        0x2bt
        0x2ft
    .end array-data
.end method

.method public constructor <init>(Ljava/io/OutputStream;)V
    .locals 2
    .param p1, "out"    # Ljava/io/OutputStream;

    .prologue
    const/4 v1, 0x0

    .line 101
    invoke-direct {p0}, Ljava/io/OutputStream;-><init>()V

    .line 77
    const/4 v0, 0x3

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->buf:[I

    .line 78
    iput v1, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->bufPtr:I

    .line 79
    new-instance v0, Lcom/android/sec/org/bouncycastle/bcpg/CRC24;

    invoke-direct {v0}, Lcom/android/sec/org/bouncycastle/bcpg/CRC24;-><init>()V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->crc:Lcom/android/sec/org/bouncycastle/bcpg/CRC24;

    .line 80
    iput v1, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->chunkCount:I

    .line 83
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->start:Z

    .line 84
    iput-boolean v1, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->clearText:Z

    .line 85
    iput-boolean v1, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->newLine:Z

    .line 87
    const-string v0, "line.separator"

    invoke-static {v0}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->nl:Ljava/lang/String;

    .line 90
    const-string v0, "-----BEGIN PGP "

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->headerStart:Ljava/lang/String;

    .line 91
    const-string v0, "-----"

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->headerTail:Ljava/lang/String;

    .line 92
    const-string v0, "-----END PGP "

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->footerStart:Ljava/lang/String;

    .line 93
    const-string v0, "-----"

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->footerTail:Ljava/lang/String;

    .line 95
    const-string v0, "BCPG v1.45"

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->version:Ljava/lang/String;

    .line 97
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->headers:Ljava/util/Hashtable;

    .line 102
    iput-object p1, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->out:Ljava/io/OutputStream;

    .line 104
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->nl:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 106
    const-string v0, "\r\n"

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->nl:Ljava/lang/String;

    .line 109
    :cond_0
    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->resetHeaders()V

    .line 110
    return-void
.end method

.method public constructor <init>(Ljava/io/OutputStream;Ljava/util/Hashtable;)V
    .locals 4
    .param p1, "out"    # Ljava/io/OutputStream;
    .param p2, "headers"    # Ljava/util/Hashtable;

    .prologue
    .line 116
    invoke-direct {p0, p1}, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 118
    invoke-virtual {p2}, Ljava/util/Hashtable;->keys()Ljava/util/Enumeration;

    move-result-object v0

    .line 120
    .local v0, "e":Ljava/util/Enumeration;
    :goto_0
    invoke-interface {v0}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 122
    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v1

    .line 124
    .local v1, "key":Ljava/lang/Object;
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->headers:Ljava/util/Hashtable;

    invoke-virtual {p2, v1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 126
    .end local v1    # "key":Ljava/lang/Object;
    :cond_0
    return-void
.end method

.method private encode(Ljava/io/OutputStream;[II)V
    .locals 7
    .param p1, "out"    # Ljava/io/OutputStream;
    .param p2, "data"    # [I
    .param p3, "len"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/16 v6, 0x3d

    const/4 v3, 0x0

    .line 40
    packed-switch p3, :pswitch_data_0

    .line 72
    new-instance v3, Ljava/io/IOException;

    const-string v4, "unknown length in encode"

    invoke-direct {v3, v4}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 45
    :pswitch_0
    aget v0, p2, v3

    .line 47
    .local v0, "d1":I
    sget-object v3, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->encodingTable:[B

    ushr-int/lit8 v4, v0, 0x2

    and-int/lit8 v4, v4, 0x3f

    aget-byte v3, v3, v4

    invoke-virtual {p1, v3}, Ljava/io/OutputStream;->write(I)V

    .line 48
    sget-object v3, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->encodingTable:[B

    shl-int/lit8 v4, v0, 0x4

    and-int/lit8 v4, v4, 0x3f

    aget-byte v3, v3, v4

    invoke-virtual {p1, v3}, Ljava/io/OutputStream;->write(I)V

    .line 49
    invoke-virtual {p1, v6}, Ljava/io/OutputStream;->write(I)V

    .line 50
    invoke-virtual {p1, v6}, Ljava/io/OutputStream;->write(I)V

    .line 74
    .end local v0    # "d1":I
    :goto_0
    :pswitch_1
    return-void

    .line 53
    :pswitch_2
    aget v0, p2, v3

    .line 54
    .restart local v0    # "d1":I
    aget v1, p2, v4

    .line 56
    .local v1, "d2":I
    sget-object v3, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->encodingTable:[B

    ushr-int/lit8 v4, v0, 0x2

    and-int/lit8 v4, v4, 0x3f

    aget-byte v3, v3, v4

    invoke-virtual {p1, v3}, Ljava/io/OutputStream;->write(I)V

    .line 57
    sget-object v3, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->encodingTable:[B

    shl-int/lit8 v4, v0, 0x4

    ushr-int/lit8 v5, v1, 0x4

    or-int/2addr v4, v5

    and-int/lit8 v4, v4, 0x3f

    aget-byte v3, v3, v4

    invoke-virtual {p1, v3}, Ljava/io/OutputStream;->write(I)V

    .line 58
    sget-object v3, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->encodingTable:[B

    shl-int/lit8 v4, v1, 0x2

    and-int/lit8 v4, v4, 0x3f

    aget-byte v3, v3, v4

    invoke-virtual {p1, v3}, Ljava/io/OutputStream;->write(I)V

    .line 59
    invoke-virtual {p1, v6}, Ljava/io/OutputStream;->write(I)V

    goto :goto_0

    .line 62
    .end local v0    # "d1":I
    .end local v1    # "d2":I
    :pswitch_3
    aget v0, p2, v3

    .line 63
    .restart local v0    # "d1":I
    aget v1, p2, v4

    .line 64
    .restart local v1    # "d2":I
    const/4 v3, 0x2

    aget v2, p2, v3

    .line 66
    .local v2, "d3":I
    sget-object v3, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->encodingTable:[B

    ushr-int/lit8 v4, v0, 0x2

    and-int/lit8 v4, v4, 0x3f

    aget-byte v3, v3, v4

    invoke-virtual {p1, v3}, Ljava/io/OutputStream;->write(I)V

    .line 67
    sget-object v3, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->encodingTable:[B

    shl-int/lit8 v4, v0, 0x4

    ushr-int/lit8 v5, v1, 0x4

    or-int/2addr v4, v5

    and-int/lit8 v4, v4, 0x3f

    aget-byte v3, v3, v4

    invoke-virtual {p1, v3}, Ljava/io/OutputStream;->write(I)V

    .line 68
    sget-object v3, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->encodingTable:[B

    shl-int/lit8 v4, v1, 0x2

    ushr-int/lit8 v5, v2, 0x6

    or-int/2addr v4, v5

    and-int/lit8 v4, v4, 0x3f

    aget-byte v3, v3, v4

    invoke-virtual {p1, v3}, Ljava/io/OutputStream;->write(I)V

    .line 69
    sget-object v3, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->encodingTable:[B

    and-int/lit8 v4, v2, 0x3f

    aget-byte v3, v3, v4

    invoke-virtual {p1, v3}, Ljava/io/OutputStream;->write(I)V

    goto :goto_0

    .line 40
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private writeHeaderEntry(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 215
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 217
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->out:Ljava/io/OutputStream;

    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-virtual {v1, v2}, Ljava/io/OutputStream;->write(I)V

    .line 215
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 220
    :cond_0
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->out:Ljava/io/OutputStream;

    const/16 v2, 0x3a

    invoke-virtual {v1, v2}, Ljava/io/OutputStream;->write(I)V

    .line 221
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->out:Ljava/io/OutputStream;

    const/16 v2, 0x20

    invoke-virtual {v1, v2}, Ljava/io/OutputStream;->write(I)V

    .line 223
    const/4 v0, 0x0

    :goto_1
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v1

    if-eq v0, v1, :cond_1

    .line 225
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->out:Ljava/io/OutputStream;

    invoke-virtual {p2, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-virtual {v1, v2}, Ljava/io/OutputStream;->write(I)V

    .line 223
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 228
    :cond_1
    const/4 v0, 0x0

    :goto_2
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->nl:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-eq v0, v1, :cond_2

    .line 230
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->out:Ljava/io/OutputStream;

    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->nl:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-virtual {v1, v2}, Ljava/io/OutputStream;->write(I)V

    .line 228
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 232
    :cond_2
    return-void
.end method


# virtual methods
.method public beginClearText(I)V
    .locals 7
    .param p1, "hashAlgorithm"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    .line 160
    packed-switch p1, :pswitch_data_0

    .line 184
    :pswitch_0
    new-instance v4, Ljava/io/IOException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "unknown hash algorithm tag in beginClearText: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 163
    :pswitch_1
    const-string v1, "SHA1"

    .line 187
    .local v1, "hash":Ljava/lang/String;
    :goto_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "-----BEGIN PGP SIGNED MESSAGE-----"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->nl:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 188
    .local v0, "armorHdr":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Hash: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->nl:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->nl:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 190
    .local v2, "hdrs":Ljava/lang/String;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eq v3, v4, :cond_0

    .line 192
    iget-object v4, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->out:Ljava/io/OutputStream;

    invoke-virtual {v0, v3}, Ljava/lang/String;->charAt(I)C

    move-result v5

    invoke-virtual {v4, v5}, Ljava/io/OutputStream;->write(I)V

    .line 190
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 166
    .end local v0    # "armorHdr":Ljava/lang/String;
    .end local v1    # "hash":Ljava/lang/String;
    .end local v2    # "hdrs":Ljava/lang/String;
    .end local v3    # "i":I
    :pswitch_2
    const-string v1, "SHA256"

    .line 167
    .restart local v1    # "hash":Ljava/lang/String;
    goto :goto_0

    .line 169
    .end local v1    # "hash":Ljava/lang/String;
    :pswitch_3
    const-string v1, "SHA384"

    .line 170
    .restart local v1    # "hash":Ljava/lang/String;
    goto :goto_0

    .line 172
    .end local v1    # "hash":Ljava/lang/String;
    :pswitch_4
    const-string v1, "SHA512"

    .line 173
    .restart local v1    # "hash":Ljava/lang/String;
    goto :goto_0

    .line 175
    .end local v1    # "hash":Ljava/lang/String;
    :pswitch_5
    const-string v1, "MD2"

    .line 176
    .restart local v1    # "hash":Ljava/lang/String;
    goto :goto_0

    .line 178
    .end local v1    # "hash":Ljava/lang/String;
    :pswitch_6
    const-string v1, "MD5"

    .line 179
    .restart local v1    # "hash":Ljava/lang/String;
    goto :goto_0

    .line 181
    .end local v1    # "hash":Ljava/lang/String;
    :pswitch_7
    const-string v1, "RIPEMD160"

    .line 182
    .restart local v1    # "hash":Ljava/lang/String;
    goto :goto_0

    .line 195
    .restart local v0    # "armorHdr":Ljava/lang/String;
    .restart local v2    # "hdrs":Ljava/lang/String;
    .restart local v3    # "i":I
    :cond_0
    const/4 v3, 0x0

    :goto_2
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    if-eq v3, v4, :cond_1

    .line 197
    iget-object v4, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->out:Ljava/io/OutputStream;

    invoke-virtual {v2, v3}, Ljava/lang/String;->charAt(I)C

    move-result v5

    invoke-virtual {v4, v5}, Ljava/io/OutputStream;->write(I)V

    .line 195
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 200
    :cond_1
    iput-boolean v6, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->clearText:Z

    .line 201
    iput-boolean v6, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->newLine:Z

    .line 202
    const/4 v4, 0x0

    iput v4, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->lastb:I

    .line 203
    return-void

    .line 160
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_1
        :pswitch_7
        :pswitch_0
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public close()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    .line 361
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->type:Ljava/lang/String;

    if-eqz v2, :cond_6

    .line 363
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->out:Ljava/io/OutputStream;

    iget-object v3, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->buf:[I

    iget v4, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->bufPtr:I

    invoke-direct {p0, v2, v3, v4}, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->encode(Ljava/io/OutputStream;[II)V

    .line 365
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->nl:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-eq v1, v2, :cond_0

    .line 367
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->out:Ljava/io/OutputStream;

    iget-object v3, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->nl:Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-virtual {v2, v3}, Ljava/io/OutputStream;->write(I)V

    .line 365
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 369
    :cond_0
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->out:Ljava/io/OutputStream;

    const/16 v3, 0x3d

    invoke-virtual {v2, v3}, Ljava/io/OutputStream;->write(I)V

    .line 371
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->crc:Lcom/android/sec/org/bouncycastle/bcpg/CRC24;

    invoke-virtual {v2}, Lcom/android/sec/org/bouncycastle/bcpg/CRC24;->getValue()I

    move-result v0

    .line 373
    .local v0, "crcV":I
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->buf:[I

    const/4 v3, 0x0

    shr-int/lit8 v4, v0, 0x10

    and-int/lit16 v4, v4, 0xff

    aput v4, v2, v3

    .line 374
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->buf:[I

    shr-int/lit8 v3, v0, 0x8

    and-int/lit16 v3, v3, 0xff

    aput v3, v2, v5

    .line 375
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->buf:[I

    const/4 v3, 0x2

    and-int/lit16 v4, v0, 0xff

    aput v4, v2, v3

    .line 377
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->out:Ljava/io/OutputStream;

    iget-object v3, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->buf:[I

    const/4 v4, 0x3

    invoke-direct {p0, v2, v3, v4}, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->encode(Ljava/io/OutputStream;[II)V

    .line 379
    const/4 v1, 0x0

    :goto_1
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->nl:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-eq v1, v2, :cond_1

    .line 381
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->out:Ljava/io/OutputStream;

    iget-object v3, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->nl:Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-virtual {v2, v3}, Ljava/io/OutputStream;->write(I)V

    .line 379
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 384
    :cond_1
    const/4 v1, 0x0

    :goto_2
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->footerStart:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-eq v1, v2, :cond_2

    .line 386
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->out:Ljava/io/OutputStream;

    iget-object v3, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->footerStart:Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-virtual {v2, v3}, Ljava/io/OutputStream;->write(I)V

    .line 384
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 389
    :cond_2
    const/4 v1, 0x0

    :goto_3
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->type:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-eq v1, v2, :cond_3

    .line 391
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->out:Ljava/io/OutputStream;

    iget-object v3, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->type:Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-virtual {v2, v3}, Ljava/io/OutputStream;->write(I)V

    .line 389
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 394
    :cond_3
    const/4 v1, 0x0

    :goto_4
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->footerTail:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-eq v1, v2, :cond_4

    .line 396
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->out:Ljava/io/OutputStream;

    iget-object v3, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->footerTail:Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-virtual {v2, v3}, Ljava/io/OutputStream;->write(I)V

    .line 394
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 399
    :cond_4
    const/4 v1, 0x0

    :goto_5
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->nl:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-eq v1, v2, :cond_5

    .line 401
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->out:Ljava/io/OutputStream;

    iget-object v3, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->nl:Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-virtual {v2, v3}, Ljava/io/OutputStream;->write(I)V

    .line 399
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    .line 404
    :cond_5
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->out:Ljava/io/OutputStream;

    invoke-virtual {v2}, Ljava/io/OutputStream;->flush()V

    .line 406
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->type:Ljava/lang/String;

    .line 407
    iput-boolean v5, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->start:Z

    .line 409
    .end local v0    # "crcV":I
    .end local v1    # "i":I
    :cond_6
    return-void
.end method

.method public endClearText()V
    .locals 1

    .prologue
    .line 207
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->clearText:Z

    .line 208
    return-void
.end method

.method public flush()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 352
    return-void
.end method

.method public resetHeaders()V
    .locals 3

    .prologue
    .line 146
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->headers:Ljava/util/Hashtable;

    invoke-virtual {v0}, Ljava/util/Hashtable;->clear()V

    .line 147
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->headers:Ljava/util/Hashtable;

    const-string v1, "Version"

    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->version:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 148
    return-void
.end method

.method public setHeader(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 138
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->headers:Ljava/util/Hashtable;

    invoke-virtual {v0, p1, p2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 139
    return-void
.end method

.method public write(I)V
    .locals 10
    .param p1, "b"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v9, 0x2d

    const/16 v8, 0xa

    const/4 v3, 0x1

    const/16 v7, 0xd

    const/4 v6, 0x0

    .line 238
    iget-boolean v5, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->clearText:Z

    if-eqz v5, :cond_5

    .line 240
    iget-object v5, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->out:Ljava/io/OutputStream;

    invoke-virtual {v5, p1}, Ljava/io/OutputStream;->write(I)V

    .line 242
    iget-boolean v5, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->newLine:Z

    if-eqz v5, :cond_2

    .line 244
    if-ne p1, v8, :cond_0

    iget v5, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->lastb:I

    if-eq v5, v7, :cond_1

    .line 246
    :cond_0
    iput-boolean v6, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->newLine:Z

    .line 248
    :cond_1
    if-ne p1, v9, :cond_2

    .line 250
    iget-object v5, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->out:Ljava/io/OutputStream;

    const/16 v6, 0x20

    invoke-virtual {v5, v6}, Ljava/io/OutputStream;->write(I)V

    .line 251
    iget-object v5, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->out:Ljava/io/OutputStream;

    invoke-virtual {v5, v9}, Ljava/io/OutputStream;->write(I)V

    .line 254
    :cond_2
    if-eq p1, v7, :cond_3

    if-ne p1, v8, :cond_4

    iget v5, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->lastb:I

    if-eq v5, v7, :cond_4

    .line 256
    :cond_3
    iput-boolean v3, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->newLine:Z

    .line 258
    :cond_4
    iput p1, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->lastb:I

    .line 347
    :goto_0
    return-void

    .line 262
    :cond_5
    iget-boolean v5, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->start:Z

    if-eqz v5, :cond_f

    .line 264
    and-int/lit8 v5, p1, 0x40

    if-eqz v5, :cond_6

    .line 265
    .local v3, "newPacket":Z
    :goto_1
    const/4 v4, 0x0

    .line 267
    .local v4, "tag":I
    if-eqz v3, :cond_7

    .line 269
    and-int/lit8 v4, p1, 0x3f

    .line 276
    :goto_2
    packed-switch v4, :pswitch_data_0

    .line 288
    :pswitch_0
    const-string v5, "MESSAGE"

    iput-object v5, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->type:Ljava/lang/String;

    .line 291
    :goto_3
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_4
    iget-object v5, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->headerStart:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    if-eq v1, v5, :cond_8

    .line 293
    iget-object v5, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->out:Ljava/io/OutputStream;

    iget-object v7, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->headerStart:Ljava/lang/String;

    invoke-virtual {v7, v1}, Ljava/lang/String;->charAt(I)C

    move-result v7

    invoke-virtual {v5, v7}, Ljava/io/OutputStream;->write(I)V

    .line 291
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .end local v1    # "i":I
    .end local v3    # "newPacket":Z
    .end local v4    # "tag":I
    :cond_6
    move v3, v6

    .line 264
    goto :goto_1

    .line 273
    .restart local v3    # "newPacket":Z
    .restart local v4    # "tag":I
    :cond_7
    and-int/lit8 v5, p1, 0x3f

    shr-int/lit8 v4, v5, 0x2

    goto :goto_2

    .line 279
    :pswitch_1
    const-string v5, "PUBLIC KEY BLOCK"

    iput-object v5, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->type:Ljava/lang/String;

    goto :goto_3

    .line 282
    :pswitch_2
    const-string v5, "PRIVATE KEY BLOCK"

    iput-object v5, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->type:Ljava/lang/String;

    goto :goto_3

    .line 285
    :pswitch_3
    const-string v5, "SIGNATURE"

    iput-object v5, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->type:Ljava/lang/String;

    goto :goto_3

    .line 296
    .restart local v1    # "i":I
    :cond_8
    const/4 v1, 0x0

    :goto_5
    iget-object v5, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->type:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    if-eq v1, v5, :cond_9

    .line 298
    iget-object v5, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->out:Ljava/io/OutputStream;

    iget-object v7, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->type:Ljava/lang/String;

    invoke-virtual {v7, v1}, Ljava/lang/String;->charAt(I)C

    move-result v7

    invoke-virtual {v5, v7}, Ljava/io/OutputStream;->write(I)V

    .line 296
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    .line 301
    :cond_9
    const/4 v1, 0x0

    :goto_6
    iget-object v5, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->headerTail:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    if-eq v1, v5, :cond_a

    .line 303
    iget-object v5, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->out:Ljava/io/OutputStream;

    iget-object v7, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->headerTail:Ljava/lang/String;

    invoke-virtual {v7, v1}, Ljava/lang/String;->charAt(I)C

    move-result v7

    invoke-virtual {v5, v7}, Ljava/io/OutputStream;->write(I)V

    .line 301
    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    .line 306
    :cond_a
    const/4 v1, 0x0

    :goto_7
    iget-object v5, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->nl:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    if-eq v1, v5, :cond_b

    .line 308
    iget-object v5, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->out:Ljava/io/OutputStream;

    iget-object v7, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->nl:Ljava/lang/String;

    invoke-virtual {v7, v1}, Ljava/lang/String;->charAt(I)C

    move-result v7

    invoke-virtual {v5, v7}, Ljava/io/OutputStream;->write(I)V

    .line 306
    add-int/lit8 v1, v1, 0x1

    goto :goto_7

    .line 311
    :cond_b
    const-string v7, "Version"

    iget-object v5, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->headers:Ljava/util/Hashtable;

    const-string v8, "Version"

    invoke-virtual {v5, v8}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-direct {p0, v7, v5}, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->writeHeaderEntry(Ljava/lang/String;Ljava/lang/String;)V

    .line 313
    iget-object v5, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->headers:Ljava/util/Hashtable;

    invoke-virtual {v5}, Ljava/util/Hashtable;->keys()Ljava/util/Enumeration;

    move-result-object v0

    .line 314
    .local v0, "e":Ljava/util/Enumeration;
    :cond_c
    :goto_8
    invoke-interface {v0}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v5

    if-eqz v5, :cond_d

    .line 316
    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 318
    .local v2, "key":Ljava/lang/String;
    const-string v5, "Version"

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_c

    .line 320
    iget-object v5, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->headers:Ljava/util/Hashtable;

    invoke-virtual {v5, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-direct {p0, v2, v5}, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->writeHeaderEntry(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_8

    .line 324
    .end local v2    # "key":Ljava/lang/String;
    :cond_d
    const/4 v1, 0x0

    :goto_9
    iget-object v5, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->nl:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    if-eq v1, v5, :cond_e

    .line 326
    iget-object v5, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->out:Ljava/io/OutputStream;

    iget-object v7, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->nl:Ljava/lang/String;

    invoke-virtual {v7, v1}, Ljava/lang/String;->charAt(I)C

    move-result v7

    invoke-virtual {v5, v7}, Ljava/io/OutputStream;->write(I)V

    .line 324
    add-int/lit8 v1, v1, 0x1

    goto :goto_9

    .line 329
    :cond_e
    iput-boolean v6, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->start:Z

    .line 332
    .end local v0    # "e":Ljava/util/Enumeration;
    .end local v1    # "i":I
    .end local v3    # "newPacket":Z
    .end local v4    # "tag":I
    :cond_f
    iget v5, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->bufPtr:I

    const/4 v7, 0x3

    if-ne v5, v7, :cond_10

    .line 334
    iget-object v5, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->out:Ljava/io/OutputStream;

    iget-object v7, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->buf:[I

    iget v8, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->bufPtr:I

    invoke-direct {p0, v5, v7, v8}, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->encode(Ljava/io/OutputStream;[II)V

    .line 335
    iput v6, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->bufPtr:I

    .line 336
    iget v5, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->chunkCount:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->chunkCount:I

    and-int/lit8 v5, v5, 0xf

    if-nez v5, :cond_10

    .line 338
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_a
    iget-object v5, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->nl:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    if-eq v1, v5, :cond_10

    .line 340
    iget-object v5, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->out:Ljava/io/OutputStream;

    iget-object v6, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->nl:Ljava/lang/String;

    invoke-virtual {v6, v1}, Ljava/lang/String;->charAt(I)C

    move-result v6

    invoke-virtual {v5, v6}, Ljava/io/OutputStream;->write(I)V

    .line 338
    add-int/lit8 v1, v1, 0x1

    goto :goto_a

    .line 345
    .end local v1    # "i":I
    :cond_10
    iget-object v5, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->crc:Lcom/android/sec/org/bouncycastle/bcpg/CRC24;

    invoke-virtual {v5, p1}, Lcom/android/sec/org/bouncycastle/bcpg/CRC24;->update(I)V

    .line 346
    iget-object v5, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->buf:[I

    iget v6, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->bufPtr:I

    add-int/lit8 v7, v6, 0x1

    iput v7, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->bufPtr:I

    and-int/lit16 v7, p1, 0xff

    aput v7, v5, v6

    goto/16 :goto_0

    .line 276
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
