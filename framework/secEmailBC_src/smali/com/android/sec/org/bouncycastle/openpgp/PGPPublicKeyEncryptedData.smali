.class public Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyEncryptedData;
.super Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedData;
.source "PGPPublicKeyEncryptedData.java"


# instance fields
.field keyData:Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyEncSessionPacket;


# direct methods
.method constructor <init>(Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyEncSessionPacket;Lcom/android/sec/org/bouncycastle/bcpg/InputStreamPacket;)V
    .locals 0
    .param p1, "keyData"    # Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyEncSessionPacket;
    .param p2, "encData"    # Lcom/android/sec/org/bouncycastle/bcpg/InputStreamPacket;

    .prologue
    .line 36
    invoke-direct {p0, p2}, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedData;-><init>(Lcom/android/sec/org/bouncycastle/bcpg/InputStreamPacket;)V

    .line 38
    iput-object p1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyEncryptedData;->keyData:Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyEncSessionPacket;

    .line 39
    return-void
.end method

.method private confirmCheckSum([B)Z
    .locals 4
    .param p1, "sessionInfo"    # [B

    .prologue
    .line 73
    const/4 v0, 0x0

    .line 75
    .local v0, "check":I
    const/4 v1, 0x1

    .local v1, "i":I
    :goto_0
    array-length v2, p1

    add-int/lit8 v2, v2, -0x2

    if-eq v1, v2, :cond_0

    .line 77
    aget-byte v2, p1, v1

    and-int/lit16 v2, v2, 0xff

    add-int/2addr v0, v2

    .line 75
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 80
    :cond_0
    array-length v2, p1

    add-int/lit8 v2, v2, -0x2

    aget-byte v2, p1, v2

    shr-int/lit8 v3, v0, 0x8

    int-to-byte v3, v3

    if-ne v2, v3, :cond_1

    array-length v2, p1

    add-int/lit8 v2, v2, -0x1

    aget-byte v2, p1, v2

    int-to-byte v3, v0

    if-ne v2, v3, :cond_1

    const/4 v2, 0x1

    :goto_1
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method private fetchSymmetricKeyData(Lcom/android/sec/org/bouncycastle/openpgp/PGPPrivateKey;Ljava/security/Provider;)[B
    .locals 12
    .param p1, "privKey"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPPrivateKey;
    .param p2, "asymProvider"    # Ljava/security/Provider;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
        }
    .end annotation

    .prologue
    .line 272
    iget-object v9, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyEncryptedData;->keyData:Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyEncSessionPacket;

    invoke-virtual {v9}, Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyEncSessionPacket;->getAlgorithm()I

    move-result v9

    invoke-static {v9, p2}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyEncryptedData;->getKeyCipher(ILjava/security/Provider;)Ljavax/crypto/Cipher;

    move-result-object v1

    .line 276
    .local v1, "c1":Ljavax/crypto/Cipher;
    const/4 v9, 0x2

    :try_start_0
    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPrivateKey;->getKey()Ljava/security/PrivateKey;

    move-result-object v10

    invoke-virtual {v1, v9, v10}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;)V
    :try_end_0
    .catch Ljava/security/InvalidKeyException; {:try_start_0 .. :try_end_0} :catch_0

    .line 283
    iget-object v9, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyEncryptedData;->keyData:Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyEncSessionPacket;

    invoke-virtual {v9}, Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyEncSessionPacket;->getEncSessionKey()[Ljava/math/BigInteger;

    move-result-object v5

    .line 285
    .local v5, "keyD":[Ljava/math/BigInteger;
    iget-object v9, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyEncryptedData;->keyData:Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyEncSessionPacket;

    invoke-virtual {v9}, Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyEncSessionPacket;->getAlgorithm()I

    move-result v9

    const/4 v10, 0x2

    if-eq v9, v10, :cond_0

    iget-object v9, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyEncryptedData;->keyData:Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyEncSessionPacket;

    invoke-virtual {v9}, Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyEncSessionPacket;->getAlgorithm()I

    move-result v9

    const/4 v10, 0x1

    if-ne v9, v10, :cond_2

    .line 288
    :cond_0
    const/4 v9, 0x0

    aget-object v9, v5, v9

    invoke-virtual {v9}, Ljava/math/BigInteger;->toByteArray()[B

    move-result-object v0

    .line 290
    .local v0, "bi":[B
    const/4 v9, 0x0

    aget-byte v9, v0, v9

    if-nez v9, :cond_1

    .line 292
    const/4 v9, 0x1

    array-length v10, v0

    add-int/lit8 v10, v10, -0x1

    invoke-virtual {v1, v0, v9, v10}, Ljavax/crypto/Cipher;->update([BII)[B

    .line 336
    :goto_0
    :try_start_1
    invoke-virtual {v1}, Ljavax/crypto/Cipher;->doFinal()[B
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v6

    .line 343
    .local v6, "plain":[B
    invoke-direct {p0, v6}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyEncryptedData;->confirmCheckSum([B)Z

    move-result v9

    if-nez v9, :cond_6

    .line 345
    new-instance v9, Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyValidationException;

    const-string v10, "key checksum failed"

    invoke-direct {v9, v10}, Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyValidationException;-><init>(Ljava/lang/String;)V

    throw v9

    .line 278
    .end local v0    # "bi":[B
    .end local v5    # "keyD":[Ljava/math/BigInteger;
    .end local v6    # "plain":[B
    :catch_0
    move-exception v2

    .line 280
    .local v2, "e":Ljava/security/InvalidKeyException;
    new-instance v9, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;

    const-string v10, "error setting asymmetric cipher"

    invoke-direct {v9, v10, v2}, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v9

    .line 296
    .end local v2    # "e":Ljava/security/InvalidKeyException;
    .restart local v0    # "bi":[B
    .restart local v5    # "keyD":[Ljava/math/BigInteger;
    :cond_1
    invoke-virtual {v1, v0}, Ljavax/crypto/Cipher;->update([B)[B

    goto :goto_0

    .line 301
    .end local v0    # "bi":[B
    :cond_2
    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPrivateKey;->getKey()Ljava/security/PrivateKey;

    move-result-object v4

    check-cast v4, Lcom/android/sec/org/bouncycastle/jce/interfaces/ElGamalKey;

    .line 302
    .local v4, "k":Lcom/android/sec/org/bouncycastle/jce/interfaces/ElGamalKey;
    invoke-interface {v4}, Lcom/android/sec/org/bouncycastle/jce/interfaces/ElGamalKey;->getParameters()Lcom/android/sec/org/bouncycastle/jce/spec/ElGamalParameterSpec;

    move-result-object v9

    invoke-virtual {v9}, Lcom/android/sec/org/bouncycastle/jce/spec/ElGamalParameterSpec;->getP()Ljava/math/BigInteger;

    move-result-object v9

    invoke-virtual {v9}, Ljava/math/BigInteger;->bitLength()I

    move-result v9

    add-int/lit8 v9, v9, 0x7

    div-int/lit8 v7, v9, 0x8

    .line 303
    .local v7, "size":I
    new-array v8, v7, [B

    .line 305
    .local v8, "tmp":[B
    const/4 v9, 0x0

    aget-object v9, v5, v9

    invoke-virtual {v9}, Ljava/math/BigInteger;->toByteArray()[B

    move-result-object v0

    .line 306
    .restart local v0    # "bi":[B
    array-length v9, v0

    if-le v9, v7, :cond_3

    .line 308
    const/4 v9, 0x1

    array-length v10, v0

    add-int/lit8 v10, v10, -0x1

    invoke-virtual {v1, v0, v9, v10}, Ljavax/crypto/Cipher;->update([BII)[B

    .line 316
    :goto_1
    const/4 v9, 0x1

    aget-object v9, v5, v9

    invoke-virtual {v9}, Ljava/math/BigInteger;->toByteArray()[B

    move-result-object v0

    .line 317
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_2
    array-length v9, v8

    if-eq v3, v9, :cond_4

    .line 319
    const/4 v9, 0x0

    aput-byte v9, v8, v3

    .line 317
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 312
    .end local v3    # "i":I
    :cond_3
    const/4 v9, 0x0

    array-length v10, v8

    array-length v11, v0

    sub-int/2addr v10, v11

    array-length v11, v0

    invoke-static {v0, v9, v8, v10, v11}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 313
    invoke-virtual {v1, v8}, Ljavax/crypto/Cipher;->update([B)[B

    goto :goto_1

    .line 322
    .restart local v3    # "i":I
    :cond_4
    array-length v9, v0

    if-le v9, v7, :cond_5

    .line 324
    const/4 v9, 0x1

    array-length v10, v0

    add-int/lit8 v10, v10, -0x1

    invoke-virtual {v1, v0, v9, v10}, Ljavax/crypto/Cipher;->update([BII)[B

    goto :goto_0

    .line 328
    :cond_5
    const/4 v9, 0x0

    array-length v10, v8

    array-length v11, v0

    sub-int/2addr v10, v11

    array-length v11, v0

    invoke-static {v0, v9, v8, v10, v11}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 329
    invoke-virtual {v1, v8}, Ljavax/crypto/Cipher;->update([B)[B

    goto :goto_0

    .line 338
    .end local v3    # "i":I
    .end local v4    # "k":Lcom/android/sec/org/bouncycastle/jce/interfaces/ElGamalKey;
    .end local v7    # "size":I
    .end local v8    # "tmp":[B
    :catch_1
    move-exception v2

    .line 340
    .local v2, "e":Ljava/lang/Exception;
    new-instance v9, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;

    const-string v10, "exception decrypting secret key"

    invoke-direct {v9, v10, v2}, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v9

    .line 348
    .end local v2    # "e":Ljava/lang/Exception;
    .restart local v6    # "plain":[B
    :cond_6
    return-object v6
.end method

.method private static getKeyCipher(ILjava/security/Provider;)Ljavax/crypto/Cipher;
    .locals 4
    .param p0, "algorithm"    # I
    .param p1, "provider"    # Ljava/security/Provider;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
        }
    .end annotation

    .prologue
    .line 48
    sparse-switch p0, :sswitch_data_0

    .line 57
    :try_start_0
    new-instance v1, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "unknown asymmetric algorithm: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_0
    .catch Lcom/android/sec/org/bouncycastle/openpgp/PGPException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 60
    :catch_0
    move-exception v0

    .line 62
    .local v0, "e":Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
    throw v0

    .line 52
    .end local v0    # "e":Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
    :sswitch_0
    :try_start_1
    const-string v1, "RSA/ECB/PKCS1Padding"

    invoke-static {v1, p1}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;Ljava/security/Provider;)Ljavax/crypto/Cipher;

    move-result-object v1

    .line 55
    :goto_0
    return-object v1

    :sswitch_1
    const-string v1, "ElGamal/ECB/PKCS1Padding"

    invoke-static {v1, p1}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;Ljava/security/Provider;)Ljavax/crypto/Cipher;
    :try_end_1
    .catch Lcom/android/sec/org/bouncycastle/openpgp/PGPException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v1

    goto :goto_0

    .line 64
    :catch_1
    move-exception v0

    .line 66
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;

    const-string v2, "Exception creating cipher"

    invoke-direct {v1, v2, v0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v1

    .line 48
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_0
        0x10 -> :sswitch_1
        0x14 -> :sswitch_1
    .end sparse-switch
.end method


# virtual methods
.method public getDataStream(Lcom/android/sec/org/bouncycastle/openpgp/PGPPrivateKey;Ljava/lang/String;)Ljava/io/InputStream;
    .locals 1
    .param p1, "privKey"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPPrivateKey;
    .param p2, "provider"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;,
            Ljava/security/NoSuchProviderException;
        }
    .end annotation

    .prologue
    .line 131
    invoke-virtual {p0, p1, p2, p2}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyEncryptedData;->getDataStream(Lcom/android/sec/org/bouncycastle/openpgp/PGPPrivateKey;Ljava/lang/String;Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v0

    return-object v0
.end method

.method public getDataStream(Lcom/android/sec/org/bouncycastle/openpgp/PGPPrivateKey;Ljava/lang/String;Ljava/lang/String;)Ljava/io/InputStream;
    .locals 2
    .param p1, "privKey"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPPrivateKey;
    .param p2, "asymProvider"    # Ljava/lang/String;
    .param p3, "provider"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;,
            Ljava/security/NoSuchProviderException;
        }
    .end annotation

    .prologue
    .line 158
    invoke-static {p2}, Lcom/android/sec/org/bouncycastle/openpgp/PGPUtil;->getProvider(Ljava/lang/String;)Ljava/security/Provider;

    move-result-object v0

    invoke-static {p3}, Lcom/android/sec/org/bouncycastle/openpgp/PGPUtil;->getProvider(Ljava/lang/String;)Ljava/security/Provider;

    move-result-object v1

    invoke-virtual {p0, p1, v0, v1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyEncryptedData;->getDataStream(Lcom/android/sec/org/bouncycastle/openpgp/PGPPrivateKey;Ljava/security/Provider;Ljava/security/Provider;)Ljava/io/InputStream;

    move-result-object v0

    return-object v0
.end method

.method public getDataStream(Lcom/android/sec/org/bouncycastle/openpgp/PGPPrivateKey;Ljava/security/Provider;)Ljava/io/InputStream;
    .locals 1
    .param p1, "privKey"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPPrivateKey;
    .param p2, "provider"    # Ljava/security/Provider;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
        }
    .end annotation

    .prologue
    .line 139
    invoke-virtual {p0, p1, p2, p2}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyEncryptedData;->getDataStream(Lcom/android/sec/org/bouncycastle/openpgp/PGPPrivateKey;Ljava/security/Provider;Ljava/security/Provider;)Ljava/io/InputStream;

    move-result-object v0

    return-object v0
.end method

.method public getDataStream(Lcom/android/sec/org/bouncycastle/openpgp/PGPPrivateKey;Ljava/security/Provider;Ljava/security/Provider;)Ljava/io/InputStream;
    .locals 12
    .param p1, "privKey"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPPrivateKey;
    .param p2, "asymProvider"    # Ljava/security/Provider;
    .param p3, "provider"    # Ljava/security/Provider;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
        }
    .end annotation

    .prologue
    .line 167
    invoke-direct {p0, p1, p2}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyEncryptedData;->fetchSymmetricKeyData(Lcom/android/sec/org/bouncycastle/openpgp/PGPPrivateKey;Ljava/security/Provider;)[B

    move-result-object v6

    .line 173
    .local v6, "plain":[B
    :try_start_0
    iget-object v9, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyEncryptedData;->encData:Lcom/android/sec/org/bouncycastle/bcpg/InputStreamPacket;

    instance-of v9, v9, Lcom/android/sec/org/bouncycastle/bcpg/SymmetricEncIntegrityPacket;

    if-eqz v9, :cond_1

    .line 175
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v10, 0x0

    aget-byte v10, v6, v10

    invoke-static {v10}, Lcom/android/sec/org/bouncycastle/openpgp/PGPUtil;->getSymmetricCipherName(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "/CFB/NoPadding"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9, p3}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;Ljava/security/Provider;)Ljavax/crypto/Cipher;
    :try_end_0
    .catch Lcom/android/sec/org/bouncycastle/openpgp/PGPException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v0

    .line 197
    .local v0, "c2":Ljavax/crypto/Cipher;
    :goto_0
    if-eqz v0, :cond_6

    .line 201
    :try_start_1
    new-instance v5, Ljavax/crypto/spec/SecretKeySpec;

    const/4 v9, 0x1

    array-length v10, v6

    add-int/lit8 v10, v10, -0x3

    const/4 v11, 0x0

    aget-byte v11, v6, v11

    invoke-static {v11}, Lcom/android/sec/org/bouncycastle/openpgp/PGPUtil;->getSymmetricCipherName(I)Ljava/lang/String;

    move-result-object v11

    invoke-direct {v5, v6, v9, v10, v11}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BIILjava/lang/String;)V

    .line 203
    .local v5, "key":Ljavax/crypto/SecretKey;
    invoke-virtual {v0}, Ljavax/crypto/Cipher;->getBlockSize()I

    move-result v9

    new-array v4, v9, [B

    .line 205
    .local v4, "iv":[B
    const/4 v9, 0x2

    new-instance v10, Ljavax/crypto/spec/IvParameterSpec;

    invoke-direct {v10, v4}, Ljavax/crypto/spec/IvParameterSpec;-><init>([B)V

    invoke-virtual {v0, v9, v5, v10}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;)V

    .line 207
    new-instance v9, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;

    new-instance v10, Ljavax/crypto/CipherInputStream;

    iget-object v11, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyEncryptedData;->encData:Lcom/android/sec/org/bouncycastle/bcpg/InputStreamPacket;

    invoke-virtual {v11}, Lcom/android/sec/org/bouncycastle/bcpg/InputStreamPacket;->getInputStream()Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;

    move-result-object v11

    invoke-direct {v10, v11, v0}, Ljavax/crypto/CipherInputStream;-><init>(Ljava/io/InputStream;Ljavax/crypto/Cipher;)V

    invoke-direct {v9, v10}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;-><init>(Ljava/io/InputStream;)V

    iput-object v9, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyEncryptedData;->encStream:Ljava/io/InputStream;

    .line 209
    iget-object v9, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyEncryptedData;->encData:Lcom/android/sec/org/bouncycastle/bcpg/InputStreamPacket;

    instance-of v9, v9, Lcom/android/sec/org/bouncycastle/bcpg/SymmetricEncIntegrityPacket;

    if-eqz v9, :cond_0

    .line 211
    new-instance v9, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedData$TruncatedStream;

    iget-object v10, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyEncryptedData;->encStream:Ljava/io/InputStream;

    invoke-direct {v9, p0, v10}, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedData$TruncatedStream;-><init>(Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedData;Ljava/io/InputStream;)V

    iput-object v9, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyEncryptedData;->truncStream:Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedData$TruncatedStream;

    .line 212
    new-instance v9, Ljava/security/DigestInputStream;

    iget-object v10, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyEncryptedData;->truncStream:Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedData$TruncatedStream;

    const/4 v11, 0x2

    invoke-static {v11}, Lcom/android/sec/org/bouncycastle/openpgp/PGPUtil;->getDigestName(I)Ljava/lang/String;

    move-result-object v11

    invoke-static {v11, p3}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;Ljava/security/Provider;)Ljava/security/MessageDigest;

    move-result-object v11

    invoke-direct {v9, v10, v11}, Ljava/security/DigestInputStream;-><init>(Ljava/io/InputStream;Ljava/security/MessageDigest;)V

    iput-object v9, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyEncryptedData;->encStream:Ljava/io/InputStream;

    .line 215
    :cond_0
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    array-length v9, v4

    if-eq v3, v9, :cond_3

    .line 217
    iget-object v9, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyEncryptedData;->encStream:Ljava/io/InputStream;

    invoke-virtual {v9}, Ljava/io/InputStream;->read()I

    move-result v1

    .line 219
    .local v1, "ch":I
    if-gez v1, :cond_2

    .line 221
    new-instance v9, Ljava/io/EOFException;

    const-string v10, "unexpected end of stream."

    invoke-direct {v9, v10}, Ljava/io/EOFException;-><init>(Ljava/lang/String;)V

    throw v9
    :try_end_1
    .catch Lcom/android/sec/org/bouncycastle/openpgp/PGPException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3

    .line 254
    .end local v1    # "ch":I
    .end local v3    # "i":I
    .end local v4    # "iv":[B
    .end local v5    # "key":Ljavax/crypto/SecretKey;
    :catch_0
    move-exception v2

    .line 256
    .local v2, "e":Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
    throw v2

    .line 182
    .end local v0    # "c2":Ljavax/crypto/Cipher;
    .end local v2    # "e":Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
    :cond_1
    :try_start_2
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v10, 0x0

    aget-byte v10, v6, v10

    invoke-static {v10}, Lcom/android/sec/org/bouncycastle/openpgp/PGPUtil;->getSymmetricCipherName(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "/OpenPGPCFB/NoPadding"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9, p3}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;Ljava/security/Provider;)Ljavax/crypto/Cipher;
    :try_end_2
    .catch Lcom/android/sec/org/bouncycastle/openpgp/PGPException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    move-result-object v0

    .restart local v0    # "c2":Ljavax/crypto/Cipher;
    goto/16 :goto_0

    .line 188
    .end local v0    # "c2":Ljavax/crypto/Cipher;
    :catch_1
    move-exception v2

    .line 190
    .restart local v2    # "e":Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
    throw v2

    .line 192
    .end local v2    # "e":Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
    :catch_2
    move-exception v2

    .line 194
    .local v2, "e":Ljava/lang/Exception;
    new-instance v9, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;

    const-string v10, "exception creating cipher"

    invoke-direct {v9, v10, v2}, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v9

    .line 224
    .end local v2    # "e":Ljava/lang/Exception;
    .restart local v0    # "c2":Ljavax/crypto/Cipher;
    .restart local v1    # "ch":I
    .restart local v3    # "i":I
    .restart local v4    # "iv":[B
    .restart local v5    # "key":Ljavax/crypto/SecretKey;
    :cond_2
    int-to-byte v9, v1

    :try_start_3
    aput-byte v9, v4, v3

    .line 215
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 227
    .end local v1    # "ch":I
    :cond_3
    iget-object v9, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyEncryptedData;->encStream:Ljava/io/InputStream;

    invoke-virtual {v9}, Ljava/io/InputStream;->read()I

    move-result v7

    .line 228
    .local v7, "v1":I
    iget-object v9, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyEncryptedData;->encStream:Ljava/io/InputStream;

    invoke-virtual {v9}, Ljava/io/InputStream;->read()I

    move-result v8

    .line 230
    .local v8, "v2":I
    if-ltz v7, :cond_4

    if-gez v8, :cond_5

    .line 232
    :cond_4
    new-instance v9, Ljava/io/EOFException;

    const-string v10, "unexpected end of stream."

    invoke-direct {v9, v10}, Ljava/io/EOFException;-><init>(Ljava/lang/String;)V

    throw v9
    :try_end_3
    .catch Lcom/android/sec/org/bouncycastle/openpgp/PGPException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3

    .line 258
    .end local v3    # "i":I
    .end local v4    # "iv":[B
    .end local v5    # "key":Ljavax/crypto/SecretKey;
    .end local v7    # "v1":I
    .end local v8    # "v2":I
    :catch_3
    move-exception v2

    .line 260
    .restart local v2    # "e":Ljava/lang/Exception;
    new-instance v9, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;

    const-string v10, "Exception starting decryption"

    invoke-direct {v9, v10, v2}, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v9

    .line 252
    .end local v2    # "e":Ljava/lang/Exception;
    .restart local v3    # "i":I
    .restart local v4    # "iv":[B
    .restart local v5    # "key":Ljavax/crypto/SecretKey;
    .restart local v7    # "v1":I
    .restart local v8    # "v2":I
    :cond_5
    :try_start_4
    iget-object v9, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyEncryptedData;->encStream:Ljava/io/InputStream;
    :try_end_4
    .catch Lcom/android/sec/org/bouncycastle/openpgp/PGPException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3

    .line 265
    .end local v3    # "i":I
    .end local v4    # "iv":[B
    .end local v5    # "key":Ljavax/crypto/SecretKey;
    .end local v7    # "v1":I
    .end local v8    # "v2":I
    :goto_2
    return-object v9

    :cond_6
    iget-object v9, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyEncryptedData;->encData:Lcom/android/sec/org/bouncycastle/bcpg/InputStreamPacket;

    invoke-virtual {v9}, Lcom/android/sec/org/bouncycastle/bcpg/InputStreamPacket;->getInputStream()Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;

    move-result-object v9

    goto :goto_2
.end method

.method public getKeyID()J
    .locals 2

    .prologue
    .line 91
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyEncryptedData;->keyData:Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyEncSessionPacket;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyEncSessionPacket;->getKeyID()J

    move-result-wide v0

    return-wide v0
.end method

.method public getSymmetricAlgorithm(Lcom/android/sec/org/bouncycastle/openpgp/PGPPrivateKey;Ljava/lang/String;)I
    .locals 1
    .param p1, "privKey"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPPrivateKey;
    .param p2, "provider"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;,
            Ljava/security/NoSuchProviderException;
        }
    .end annotation

    .prologue
    .line 104
    invoke-static {p2}, Lcom/android/sec/org/bouncycastle/openpgp/PGPUtil;->getProvider(Ljava/lang/String;)Ljava/security/Provider;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyEncryptedData;->getSymmetricAlgorithm(Lcom/android/sec/org/bouncycastle/openpgp/PGPPrivateKey;Ljava/security/Provider;)I

    move-result v0

    return v0
.end method

.method public getSymmetricAlgorithm(Lcom/android/sec/org/bouncycastle/openpgp/PGPPrivateKey;Ljava/security/Provider;)I
    .locals 2
    .param p1, "privKey"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPPrivateKey;
    .param p2, "provider"    # Ljava/security/Provider;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;,
            Ljava/security/NoSuchProviderException;
        }
    .end annotation

    .prologue
    .line 112
    invoke-direct {p0, p1, p2}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyEncryptedData;->fetchSymmetricKeyData(Lcom/android/sec/org/bouncycastle/openpgp/PGPPrivateKey;Ljava/security/Provider;)[B

    move-result-object v0

    .line 114
    .local v0, "plain":[B
    const/4 v1, 0x0

    aget-byte v1, v0, v1

    return v1
.end method
