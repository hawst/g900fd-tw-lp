.class public Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;
.super Lcom/android/sec/org/bouncycastle/bcpg/BCPGObject;
.source "MPInteger.java"


# instance fields
.field value:Ljava/math/BigInteger;


# direct methods
.method public constructor <init>(Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;)V
    .locals 4
    .param p1, "in"    # Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGObject;-><init>()V

    .line 12
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;->value:Ljava/math/BigInteger;

    .line 18
    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->read()I

    move-result v2

    shl-int/lit8 v2, v2, 0x8

    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->read()I

    move-result v3

    or-int v1, v2, v3

    .line 19
    .local v1, "length":I
    add-int/lit8 v2, v1, 0x7

    div-int/lit8 v2, v2, 0x8

    new-array v0, v2, [B

    .line 21
    .local v0, "bytes":[B
    invoke-virtual {p1, v0}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->readFully([B)V

    .line 23
    new-instance v2, Ljava/math/BigInteger;

    const/4 v3, 0x1

    invoke-direct {v2, v3, v0}, Ljava/math/BigInteger;-><init>(I[B)V

    iput-object v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;->value:Ljava/math/BigInteger;

    .line 24
    return-void
.end method

.method public constructor <init>(Ljava/math/BigInteger;)V
    .locals 2
    .param p1, "value"    # Ljava/math/BigInteger;

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGObject;-><init>()V

    .line 12
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;->value:Ljava/math/BigInteger;

    .line 29
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/math/BigInteger;->signum()I

    move-result v0

    if-gez v0, :cond_1

    .line 31
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "value must not be null, or negative"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 34
    :cond_1
    iput-object p1, p0, Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;->value:Ljava/math/BigInteger;

    .line 35
    return-void
.end method


# virtual methods
.method public encode(Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;)V
    .locals 4
    .param p1, "out"    # Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 46
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;->value:Ljava/math/BigInteger;

    invoke-virtual {v2}, Ljava/math/BigInteger;->bitLength()I

    move-result v1

    .line 48
    .local v1, "length":I
    shr-int/lit8 v2, v1, 0x8

    invoke-virtual {p1, v2}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->write(I)V

    .line 49
    invoke-virtual {p1, v1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->write(I)V

    .line 51
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;->value:Ljava/math/BigInteger;

    invoke-virtual {v2}, Ljava/math/BigInteger;->toByteArray()[B

    move-result-object v0

    .line 53
    .local v0, "bytes":[B
    aget-byte v2, v0, v3

    if-nez v2, :cond_0

    .line 55
    const/4 v2, 0x1

    array-length v3, v0

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {p1, v0, v2, v3}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->write([BII)V

    .line 61
    :goto_0
    return-void

    .line 59
    :cond_0
    array-length v2, v0

    invoke-virtual {p1, v0, v3, v2}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->write([BII)V

    goto :goto_0
.end method

.method public getValue()Ljava/math/BigInteger;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;->value:Ljava/math/BigInteger;

    return-object v0
.end method
