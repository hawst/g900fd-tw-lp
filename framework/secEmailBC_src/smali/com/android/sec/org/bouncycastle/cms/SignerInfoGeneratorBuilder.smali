.class public Lcom/android/sec/org/bouncycastle/cms/SignerInfoGeneratorBuilder;
.super Ljava/lang/Object;
.source "SignerInfoGeneratorBuilder.java"


# instance fields
.field private digestProvider:Lcom/android/sec/org/bouncycastle/operator/DigestCalculatorProvider;

.field private directSignature:Z

.field private sigEncAlgFinder:Lcom/android/sec/org/bouncycastle/cms/CMSSignatureEncryptionAlgorithmFinder;

.field private signedGen:Lcom/android/sec/org/bouncycastle/cms/CMSAttributeTableGenerator;

.field private unsignedGen:Lcom/android/sec/org/bouncycastle/cms/CMSAttributeTableGenerator;


# direct methods
.method public constructor <init>(Lcom/android/sec/org/bouncycastle/operator/DigestCalculatorProvider;)V
    .locals 1
    .param p1, "digestProvider"    # Lcom/android/sec/org/bouncycastle/operator/DigestCalculatorProvider;

    .prologue
    .line 29
    new-instance v0, Lcom/android/sec/org/bouncycastle/cms/DefaultCMSSignatureEncryptionAlgorithmFinder;

    invoke-direct {v0}, Lcom/android/sec/org/bouncycastle/cms/DefaultCMSSignatureEncryptionAlgorithmFinder;-><init>()V

    invoke-direct {p0, p1, v0}, Lcom/android/sec/org/bouncycastle/cms/SignerInfoGeneratorBuilder;-><init>(Lcom/android/sec/org/bouncycastle/operator/DigestCalculatorProvider;Lcom/android/sec/org/bouncycastle/cms/CMSSignatureEncryptionAlgorithmFinder;)V

    .line 30
    return-void
.end method

.method public constructor <init>(Lcom/android/sec/org/bouncycastle/operator/DigestCalculatorProvider;Lcom/android/sec/org/bouncycastle/cms/CMSSignatureEncryptionAlgorithmFinder;)V
    .locals 0
    .param p1, "digestProvider"    # Lcom/android/sec/org/bouncycastle/operator/DigestCalculatorProvider;
    .param p2, "sigEncAlgFinder"    # Lcom/android/sec/org/bouncycastle/cms/CMSSignatureEncryptionAlgorithmFinder;

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object p1, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInfoGeneratorBuilder;->digestProvider:Lcom/android/sec/org/bouncycastle/operator/DigestCalculatorProvider;

    .line 40
    iput-object p2, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInfoGeneratorBuilder;->sigEncAlgFinder:Lcom/android/sec/org/bouncycastle/cms/CMSSignatureEncryptionAlgorithmFinder;

    .line 41
    return-void
.end method

.method private createGenerator(Lcom/android/sec/org/bouncycastle/operator/ContentSigner;Lcom/android/sec/org/bouncycastle/asn1/cms/SignerIdentifier;)Lcom/android/sec/org/bouncycastle/cms/SignerInfoGenerator;
    .locals 7
    .param p1, "contentSigner"    # Lcom/android/sec/org/bouncycastle/operator/ContentSigner;
    .param p2, "sigId"    # Lcom/android/sec/org/bouncycastle/asn1/cms/SignerIdentifier;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/operator/OperatorCreationException;
        }
    .end annotation

    .prologue
    .line 122
    iget-boolean v0, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInfoGeneratorBuilder;->directSignature:Z

    if-eqz v0, :cond_0

    .line 124
    new-instance v0, Lcom/android/sec/org/bouncycastle/cms/SignerInfoGenerator;

    iget-object v3, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInfoGeneratorBuilder;->digestProvider:Lcom/android/sec/org/bouncycastle/operator/DigestCalculatorProvider;

    iget-object v4, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInfoGeneratorBuilder;->sigEncAlgFinder:Lcom/android/sec/org/bouncycastle/cms/CMSSignatureEncryptionAlgorithmFinder;

    const/4 v5, 0x1

    move-object v1, p2

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, Lcom/android/sec/org/bouncycastle/cms/SignerInfoGenerator;-><init>(Lcom/android/sec/org/bouncycastle/asn1/cms/SignerIdentifier;Lcom/android/sec/org/bouncycastle/operator/ContentSigner;Lcom/android/sec/org/bouncycastle/operator/DigestCalculatorProvider;Lcom/android/sec/org/bouncycastle/cms/CMSSignatureEncryptionAlgorithmFinder;Z)V

    .line 137
    :goto_0
    return-object v0

    .line 127
    :cond_0
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInfoGeneratorBuilder;->signedGen:Lcom/android/sec/org/bouncycastle/cms/CMSAttributeTableGenerator;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInfoGeneratorBuilder;->unsignedGen:Lcom/android/sec/org/bouncycastle/cms/CMSAttributeTableGenerator;

    if-eqz v0, :cond_3

    .line 129
    :cond_1
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInfoGeneratorBuilder;->signedGen:Lcom/android/sec/org/bouncycastle/cms/CMSAttributeTableGenerator;

    if-nez v0, :cond_2

    .line 131
    new-instance v0, Lcom/android/sec/org/bouncycastle/cms/DefaultSignedAttributeTableGenerator;

    invoke-direct {v0}, Lcom/android/sec/org/bouncycastle/cms/DefaultSignedAttributeTableGenerator;-><init>()V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInfoGeneratorBuilder;->signedGen:Lcom/android/sec/org/bouncycastle/cms/CMSAttributeTableGenerator;

    .line 134
    :cond_2
    new-instance v0, Lcom/android/sec/org/bouncycastle/cms/SignerInfoGenerator;

    iget-object v3, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInfoGeneratorBuilder;->digestProvider:Lcom/android/sec/org/bouncycastle/operator/DigestCalculatorProvider;

    iget-object v4, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInfoGeneratorBuilder;->sigEncAlgFinder:Lcom/android/sec/org/bouncycastle/cms/CMSSignatureEncryptionAlgorithmFinder;

    iget-object v5, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInfoGeneratorBuilder;->signedGen:Lcom/android/sec/org/bouncycastle/cms/CMSAttributeTableGenerator;

    iget-object v6, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInfoGeneratorBuilder;->unsignedGen:Lcom/android/sec/org/bouncycastle/cms/CMSAttributeTableGenerator;

    move-object v1, p2

    move-object v2, p1

    invoke-direct/range {v0 .. v6}, Lcom/android/sec/org/bouncycastle/cms/SignerInfoGenerator;-><init>(Lcom/android/sec/org/bouncycastle/asn1/cms/SignerIdentifier;Lcom/android/sec/org/bouncycastle/operator/ContentSigner;Lcom/android/sec/org/bouncycastle/operator/DigestCalculatorProvider;Lcom/android/sec/org/bouncycastle/cms/CMSSignatureEncryptionAlgorithmFinder;Lcom/android/sec/org/bouncycastle/cms/CMSAttributeTableGenerator;Lcom/android/sec/org/bouncycastle/cms/CMSAttributeTableGenerator;)V

    goto :goto_0

    .line 137
    :cond_3
    new-instance v0, Lcom/android/sec/org/bouncycastle/cms/SignerInfoGenerator;

    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInfoGeneratorBuilder;->digestProvider:Lcom/android/sec/org/bouncycastle/operator/DigestCalculatorProvider;

    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInfoGeneratorBuilder;->sigEncAlgFinder:Lcom/android/sec/org/bouncycastle/cms/CMSSignatureEncryptionAlgorithmFinder;

    invoke-direct {v0, p2, p1, v1, v2}, Lcom/android/sec/org/bouncycastle/cms/SignerInfoGenerator;-><init>(Lcom/android/sec/org/bouncycastle/asn1/cms/SignerIdentifier;Lcom/android/sec/org/bouncycastle/operator/ContentSigner;Lcom/android/sec/org/bouncycastle/operator/DigestCalculatorProvider;Lcom/android/sec/org/bouncycastle/cms/CMSSignatureEncryptionAlgorithmFinder;)V

    goto :goto_0
.end method


# virtual methods
.method public build(Lcom/android/sec/org/bouncycastle/operator/ContentSigner;Lcom/android/sec/org/bouncycastle/cert/X509CertificateHolder;)Lcom/android/sec/org/bouncycastle/cms/SignerInfoGenerator;
    .locals 4
    .param p1, "contentSigner"    # Lcom/android/sec/org/bouncycastle/operator/ContentSigner;
    .param p2, "certHolder"    # Lcom/android/sec/org/bouncycastle/cert/X509CertificateHolder;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/operator/OperatorCreationException;
        }
    .end annotation

    .prologue
    .line 93
    new-instance v0, Lcom/android/sec/org/bouncycastle/asn1/cms/SignerIdentifier;

    new-instance v2, Lcom/android/sec/org/bouncycastle/asn1/cms/IssuerAndSerialNumber;

    invoke-virtual {p2}, Lcom/android/sec/org/bouncycastle/cert/X509CertificateHolder;->toASN1Structure()Lcom/android/sec/org/bouncycastle/asn1/x509/Certificate;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/android/sec/org/bouncycastle/asn1/cms/IssuerAndSerialNumber;-><init>(Lcom/android/sec/org/bouncycastle/asn1/x509/Certificate;)V

    invoke-direct {v0, v2}, Lcom/android/sec/org/bouncycastle/asn1/cms/SignerIdentifier;-><init>(Lcom/android/sec/org/bouncycastle/asn1/cms/IssuerAndSerialNumber;)V

    .line 95
    .local v0, "sigId":Lcom/android/sec/org/bouncycastle/asn1/cms/SignerIdentifier;
    invoke-direct {p0, p1, v0}, Lcom/android/sec/org/bouncycastle/cms/SignerInfoGeneratorBuilder;->createGenerator(Lcom/android/sec/org/bouncycastle/operator/ContentSigner;Lcom/android/sec/org/bouncycastle/asn1/cms/SignerIdentifier;)Lcom/android/sec/org/bouncycastle/cms/SignerInfoGenerator;

    move-result-object v1

    .line 97
    .local v1, "sigInfoGen":Lcom/android/sec/org/bouncycastle/cms/SignerInfoGenerator;
    invoke-virtual {v1, p2}, Lcom/android/sec/org/bouncycastle/cms/SignerInfoGenerator;->setAssociatedCertificate(Lcom/android/sec/org/bouncycastle/cert/X509CertificateHolder;)V

    .line 99
    return-object v1
.end method

.method public build(Lcom/android/sec/org/bouncycastle/operator/ContentSigner;[B)Lcom/android/sec/org/bouncycastle/cms/SignerInfoGenerator;
    .locals 2
    .param p1, "contentSigner"    # Lcom/android/sec/org/bouncycastle/operator/ContentSigner;
    .param p2, "subjectKeyIdentifier"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/operator/OperatorCreationException;
        }
    .end annotation

    .prologue
    .line 114
    new-instance v0, Lcom/android/sec/org/bouncycastle/asn1/cms/SignerIdentifier;

    new-instance v1, Lcom/android/sec/org/bouncycastle/asn1/DEROctetString;

    invoke-direct {v1, p2}, Lcom/android/sec/org/bouncycastle/asn1/DEROctetString;-><init>([B)V

    invoke-direct {v0, v1}, Lcom/android/sec/org/bouncycastle/asn1/cms/SignerIdentifier;-><init>(Lcom/android/sec/org/bouncycastle/asn1/ASN1OctetString;)V

    .line 116
    .local v0, "sigId":Lcom/android/sec/org/bouncycastle/asn1/cms/SignerIdentifier;
    invoke-direct {p0, p1, v0}, Lcom/android/sec/org/bouncycastle/cms/SignerInfoGeneratorBuilder;->createGenerator(Lcom/android/sec/org/bouncycastle/operator/ContentSigner;Lcom/android/sec/org/bouncycastle/asn1/cms/SignerIdentifier;)Lcom/android/sec/org/bouncycastle/cms/SignerInfoGenerator;

    move-result-object v1

    return-object v1
.end method

.method public setDirectSignature(Z)Lcom/android/sec/org/bouncycastle/cms/SignerInfoGeneratorBuilder;
    .locals 0
    .param p1, "hasNoSignedAttributes"    # Z

    .prologue
    .line 51
    iput-boolean p1, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInfoGeneratorBuilder;->directSignature:Z

    .line 53
    return-object p0
.end method

.method public setSignedAttributeGenerator(Lcom/android/sec/org/bouncycastle/cms/CMSAttributeTableGenerator;)Lcom/android/sec/org/bouncycastle/cms/SignerInfoGeneratorBuilder;
    .locals 0
    .param p1, "signedGen"    # Lcom/android/sec/org/bouncycastle/cms/CMSAttributeTableGenerator;

    .prologue
    .line 64
    iput-object p1, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInfoGeneratorBuilder;->signedGen:Lcom/android/sec/org/bouncycastle/cms/CMSAttributeTableGenerator;

    .line 66
    return-object p0
.end method

.method public setUnsignedAttributeGenerator(Lcom/android/sec/org/bouncycastle/cms/CMSAttributeTableGenerator;)Lcom/android/sec/org/bouncycastle/cms/SignerInfoGeneratorBuilder;
    .locals 0
    .param p1, "unsignedGen"    # Lcom/android/sec/org/bouncycastle/cms/CMSAttributeTableGenerator;

    .prologue
    .line 77
    iput-object p1, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInfoGeneratorBuilder;->unsignedGen:Lcom/android/sec/org/bouncycastle/cms/CMSAttributeTableGenerator;

    .line 79
    return-object p0
.end method
