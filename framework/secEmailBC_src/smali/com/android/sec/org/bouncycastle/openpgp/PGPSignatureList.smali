.class public Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureList;
.super Ljava/lang/Object;
.source "PGPSignatureList.java"


# instance fields
.field sigs:[Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;


# direct methods
.method public constructor <init>(Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;)V
    .locals 2
    .param p1, "sig"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureList;->sigs:[Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;

    .line 22
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureList;->sigs:[Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    .line 23
    return-void
.end method

.method public constructor <init>([Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;)V
    .locals 3
    .param p1, "sigs"    # [Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;

    .prologue
    const/4 v2, 0x0

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    array-length v0, p1

    new-array v0, v0, [Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureList;->sigs:[Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;

    .line 15
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureList;->sigs:[Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;

    array-length v1, p1

    invoke-static {p1, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 16
    return-void
.end method


# virtual methods
.method public get(I)Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 28
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureList;->sigs:[Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureList;->sigs:[Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;

    array-length v0, v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureList;->sigs:[Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;

    array-length v0, v0

    return v0
.end method
