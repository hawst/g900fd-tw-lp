.class public Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;
.super Ljava/lang/Object;
.source "PGPEncryptedDataGenerator.java"

# interfaces
.implements Lcom/android/sec/org/bouncycastle/bcpg/SymmetricKeyAlgorithmTags;
.implements Lcom/android/sec/org/bouncycastle/openpgp/StreamGenerator;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator$1;,
        Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator$PubMethod;,
        Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator$PBEMethod;,
        Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator$EncMethod;
    }
.end annotation


# instance fields
.field private c:Ljavax/crypto/Cipher;

.field private cOut:Ljavax/crypto/CipherOutputStream;

.field private defAlgorithm:I

.field private defProvider:Ljava/security/Provider;

.field private digestOut:Ljava/security/DigestOutputStream;

.field private methods:Ljava/util/List;

.field private oldFormat:Z

.field private pOut:Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;

.field private rand:Ljava/security/SecureRandom;

.field private withIntegrityPacket:Z


# direct methods
.method public constructor <init>(ILjava/security/SecureRandom;Ljava/lang/String;)V
    .locals 1
    .param p1, "encAlgorithm"    # I
    .param p2, "rand"    # Ljava/security/SecureRandom;
    .param p3, "provider"    # Ljava/lang/String;

    .prologue
    .line 187
    invoke-static {p3}, Ljava/security/Security;->getProvider(Ljava/lang/String;)Ljava/security/Provider;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;-><init>(ILjava/security/SecureRandom;Ljava/security/Provider;)V

    .line 188
    return-void
.end method

.method public constructor <init>(ILjava/security/SecureRandom;Ljava/security/Provider;)V
    .locals 1
    .param p1, "encAlgorithm"    # I
    .param p2, "rand"    # Ljava/security/SecureRandom;
    .param p3, "provider"    # Ljava/security/Provider;

    .prologue
    const/4 v0, 0x0

    .line 194
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-boolean v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;->withIntegrityPacket:Z

    .line 38
    iput-boolean v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;->oldFormat:Z

    .line 170
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;->methods:Ljava/util/List;

    .line 195
    iput p1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;->defAlgorithm:I

    .line 196
    iput-object p2, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;->rand:Ljava/security/SecureRandom;

    .line 197
    iput-object p3, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;->defProvider:Ljava/security/Provider;

    .line 198
    return-void
.end method

.method public constructor <init>(ILjava/security/SecureRandom;ZLjava/lang/String;)V
    .locals 1
    .param p1, "encAlgorithm"    # I
    .param p2, "rand"    # Ljava/security/SecureRandom;
    .param p3, "oldFormat"    # Z
    .param p4, "provider"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 243
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-boolean v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;->withIntegrityPacket:Z

    .line 38
    iput-boolean v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;->oldFormat:Z

    .line 170
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;->methods:Ljava/util/List;

    .line 244
    iput p1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;->defAlgorithm:I

    .line 245
    iput-object p2, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;->rand:Ljava/security/SecureRandom;

    .line 246
    invoke-static {p4}, Ljava/security/Security;->getProvider(Ljava/lang/String;)Ljava/security/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;->defProvider:Ljava/security/Provider;

    .line 247
    iput-boolean p3, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;->oldFormat:Z

    .line 248
    return-void
.end method

.method public constructor <init>(ILjava/security/SecureRandom;ZLjava/security/Provider;)V
    .locals 1
    .param p1, "encAlgorithm"    # I
    .param p2, "rand"    # Ljava/security/SecureRandom;
    .param p3, "oldFormat"    # Z
    .param p4, "provider"    # Ljava/security/Provider;

    .prologue
    const/4 v0, 0x0

    .line 255
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-boolean v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;->withIntegrityPacket:Z

    .line 38
    iput-boolean v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;->oldFormat:Z

    .line 170
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;->methods:Ljava/util/List;

    .line 256
    iput p1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;->defAlgorithm:I

    .line 257
    iput-object p2, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;->rand:Ljava/security/SecureRandom;

    .line 258
    iput-object p4, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;->defProvider:Ljava/security/Provider;

    .line 259
    iput-boolean p3, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;->oldFormat:Z

    .line 260
    return-void
.end method

.method public constructor <init>(IZLjava/security/SecureRandom;Ljava/lang/String;)V
    .locals 1
    .param p1, "encAlgorithm"    # I
    .param p2, "withIntegrityPacket"    # Z
    .param p3, "rand"    # Ljava/security/SecureRandom;
    .param p4, "provider"    # Ljava/lang/String;

    .prologue
    .line 215
    invoke-static {p4}, Ljava/security/Security;->getProvider(Ljava/lang/String;)Ljava/security/Provider;

    move-result-object v0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;-><init>(IZLjava/security/SecureRandom;Ljava/security/Provider;)V

    .line 216
    return-void
.end method

.method public constructor <init>(IZLjava/security/SecureRandom;Ljava/security/Provider;)V
    .locals 1
    .param p1, "encAlgorithm"    # I
    .param p2, "withIntegrityPacket"    # Z
    .param p3, "rand"    # Ljava/security/SecureRandom;
    .param p4, "provider"    # Ljava/security/Provider;

    .prologue
    const/4 v0, 0x0

    .line 223
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-boolean v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;->withIntegrityPacket:Z

    .line 38
    iput-boolean v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;->oldFormat:Z

    .line 170
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;->methods:Ljava/util/List;

    .line 224
    iput p1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;->defAlgorithm:I

    .line 225
    iput-object p3, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;->rand:Ljava/security/SecureRandom;

    .line 226
    iput-object p4, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;->defProvider:Ljava/security/Provider;

    .line 227
    iput-boolean p2, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;->withIntegrityPacket:Z

    .line 228
    return-void
.end method

.method static synthetic access$100(Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;)Ljava/security/Provider;
    .locals 1
    .param p0, "x0"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;->defProvider:Ljava/security/Provider;

    return-object v0
.end method

.method static synthetic access$200(Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;)Ljava/security/SecureRandom;
    .locals 1
    .param p0, "x0"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;->rand:Ljava/security/SecureRandom;

    return-object v0
.end method

.method private addCheckSum([B)V
    .locals 4
    .param p1, "sessionInfo"    # [B

    .prologue
    .line 314
    const/4 v0, 0x0

    .line 316
    .local v0, "check":I
    const/4 v1, 0x1

    .local v1, "i":I
    :goto_0
    array-length v2, p1

    add-int/lit8 v2, v2, -0x2

    if-eq v1, v2, :cond_0

    .line 318
    aget-byte v2, p1, v1

    and-int/lit16 v2, v2, 0xff

    add-int/2addr v0, v2

    .line 316
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 321
    :cond_0
    array-length v2, p1

    add-int/lit8 v2, v2, -0x2

    shr-int/lit8 v3, v0, 0x8

    int-to-byte v3, v3

    aput-byte v3, p1, v2

    .line 322
    array-length v2, p1

    add-int/lit8 v2, v2, -0x1

    int-to-byte v3, v0

    aput-byte v3, p1, v2

    .line 323
    return-void
.end method

.method private createSessionInfo(ILjava/security/Key;)[B
    .locals 5
    .param p1, "algorithm"    # I
    .param p2, "key"    # Ljava/security/Key;

    .prologue
    const/4 v4, 0x0

    .line 329
    invoke-interface {p2}, Ljava/security/Key;->getEncoded()[B

    move-result-object v0

    .line 330
    .local v0, "keyBytes":[B
    array-length v2, v0

    add-int/lit8 v2, v2, 0x3

    new-array v1, v2, [B

    .line 331
    .local v1, "sessionInfo":[B
    int-to-byte v2, p1

    aput-byte v2, v1, v4

    .line 332
    const/4 v2, 0x1

    array-length v3, v0

    invoke-static {v0, v4, v1, v2, v3}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 333
    invoke-direct {p0, v1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;->addCheckSum([B)V

    .line 334
    return-object v1
.end method

.method private open(Ljava/io/OutputStream;J[B)Ljava/io/OutputStream;
    .locals 22
    .param p1, "out"    # Ljava/io/OutputStream;
    .param p2, "length"    # J
    .param p4, "buffer"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;,
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 359
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;->cOut:Ljavax/crypto/CipherOutputStream;

    if-eqz v3, :cond_0

    .line 361
    new-instance v3, Ljava/lang/IllegalStateException;

    const-string v4, "generator already in open state"

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 364
    :cond_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;->methods:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-nez v3, :cond_1

    .line 366
    new-instance v3, Ljava/lang/IllegalStateException;

    const-string v4, "no encryption methods specified"

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 369
    :cond_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;->defProvider:Ljava/security/Provider;

    if-nez v3, :cond_2

    .line 371
    new-instance v3, Ljava/lang/IllegalStateException;

    const-string v4, "provider resolves to null"

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 374
    :cond_2
    const/16 v16, 0x0

    .line 376
    .local v16, "key":Ljava/security/Key;
    new-instance v3, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;

    move-object/from16 v0, p1

    invoke-direct {v3, v0}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;-><init>(Ljava/io/OutputStream;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;->pOut:Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;

    .line 378
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;->methods:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_5

    .line 380
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;->methods:Ljava/util/List;

    const/4 v4, 0x0

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    instance-of v3, v3, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator$PBEMethod;

    if-eqz v3, :cond_4

    .line 382
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;->methods:Ljava/util/List;

    const/4 v4, 0x0

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator$PBEMethod;

    .line 384
    .local v17, "m":Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator$PBEMethod;
    invoke-virtual/range {v17 .. v17}, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator$PBEMethod;->getKey()Ljava/security/Key;

    move-result-object v16

    .line 403
    .end local v17    # "m":Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator$PBEMethod;
    :goto_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;->pOut:Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;->methods:Ljava/util/List;

    const/4 v5, 0x0

    invoke-interface {v3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/sec/org/bouncycastle/bcpg/ContainedPacket;

    invoke-virtual {v4, v3}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->writePacket(Lcom/android/sec/org/bouncycastle/bcpg/ContainedPacket;)V

    .line 427
    :cond_3
    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;->defAlgorithm:I

    invoke-static {v3}, Lcom/android/sec/org/bouncycastle/openpgp/PGPUtil;->getSymmetricCipherName(I)Ljava/lang/String;

    move-result-object v2

    .line 429
    .local v2, "cName":Ljava/lang/String;
    if-nez v2, :cond_6

    .line 431
    new-instance v3, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;

    const-string v4, "null cipher specified"

    invoke-direct {v3, v4}, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 388
    .end local v2    # "cName":Ljava/lang/String;
    :cond_4
    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;->defAlgorithm:I

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;->rand:Ljava/security/SecureRandom;

    invoke-static {v3, v4}, Lcom/android/sec/org/bouncycastle/openpgp/PGPUtil;->makeRandomKey(ILjava/security/SecureRandom;)Ljavax/crypto/SecretKey;

    move-result-object v16

    .line 389
    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;->defAlgorithm:I

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v3, v1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;->createSessionInfo(ILjava/security/Key;)[B

    move-result-object v18

    .line 391
    .local v18, "sessionInfo":[B
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;->methods:Ljava/util/List;

    const/4 v4, 0x0

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator$PubMethod;

    .line 395
    .local v17, "m":Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator$PubMethod;
    :try_start_0
    invoke-virtual/range {v17 .. v18}, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator$PubMethod;->addSessionInfo([B)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 397
    :catch_0
    move-exception v11

    .line 399
    .local v11, "e":Ljava/lang/Exception;
    new-instance v3, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;

    const-string v4, "exception encrypting session key"

    invoke-direct {v3, v4, v11}, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v3

    .line 407
    .end local v11    # "e":Ljava/lang/Exception;
    .end local v17    # "m":Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator$PubMethod;
    .end local v18    # "sessionInfo":[B
    :cond_5
    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;->defAlgorithm:I

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;->rand:Ljava/security/SecureRandom;

    invoke-static {v3, v4}, Lcom/android/sec/org/bouncycastle/openpgp/PGPUtil;->makeRandomKey(ILjava/security/SecureRandom;)Ljavax/crypto/SecretKey;

    move-result-object v16

    .line 408
    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;->defAlgorithm:I

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v3, v1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;->createSessionInfo(ILjava/security/Key;)[B

    move-result-object v18

    .line 410
    .restart local v18    # "sessionInfo":[B
    const/4 v13, 0x0

    .local v13, "i":I
    :goto_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;->methods:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-eq v13, v3, :cond_3

    .line 412
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;->methods:Ljava/util/List;

    invoke-interface {v3, v13}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator$EncMethod;

    .line 416
    .local v17, "m":Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator$EncMethod;
    :try_start_1
    invoke-virtual/range {v17 .. v18}, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator$EncMethod;->addSessionInfo([B)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 423
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;->pOut:Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->writePacket(Lcom/android/sec/org/bouncycastle/bcpg/ContainedPacket;)V

    .line 410
    add-int/lit8 v13, v13, 0x1

    goto :goto_1

    .line 418
    :catch_1
    move-exception v11

    .line 420
    .restart local v11    # "e":Ljava/lang/Exception;
    new-instance v3, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;

    const-string v4, "exception encrypting session key"

    invoke-direct {v3, v4, v11}, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v3

    .line 436
    .end local v11    # "e":Ljava/lang/Exception;
    .end local v13    # "i":I
    .end local v17    # "m":Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator$EncMethod;
    .end local v18    # "sessionInfo":[B
    .restart local v2    # "cName":Ljava/lang/String;
    :cond_6
    :try_start_2
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;->withIntegrityPacket:Z

    if-eqz v3, :cond_8

    .line 438
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/CFB/NoPadding"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;->defProvider:Ljava/security/Provider;

    invoke-static {v3, v4}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;Ljava/security/Provider;)Ljavax/crypto/Cipher;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;->c:Ljavax/crypto/Cipher;

    .line 445
    :goto_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;->c:Ljavax/crypto/Cipher;

    invoke-virtual {v3}, Ljavax/crypto/Cipher;->getBlockSize()I

    move-result v3

    new-array v15, v3, [B

    .line 446
    .local v15, "iv":[B
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;->c:Ljavax/crypto/Cipher;

    const/4 v4, 0x1

    new-instance v5, Ljavax/crypto/spec/IvParameterSpec;

    invoke-direct {v5, v15}, Ljavax/crypto/spec/IvParameterSpec;-><init>([B)V

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;->rand:Ljava/security/SecureRandom;

    move-object/from16 v0, v16

    invoke-virtual {v3, v4, v0, v5, v6}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;Ljava/security/SecureRandom;)V

    .line 448
    if-nez p4, :cond_a

    .line 453
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;->withIntegrityPacket:Z

    if-eqz v3, :cond_9

    .line 455
    new-instance v3, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;

    const/16 v4, 0x12

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;->c:Ljavax/crypto/Cipher;

    invoke-virtual {v5}, Ljavax/crypto/Cipher;->getBlockSize()I

    move-result v5

    int-to-long v6, v5

    add-long v6, v6, p2

    const-wide/16 v20, 0x2

    add-long v6, v6, v20

    const-wide/16 v20, 0x1

    add-long v6, v6, v20

    const-wide/16 v20, 0x16

    add-long v6, v6, v20

    move-object/from16 v0, p1

    invoke-direct {v3, v0, v4, v6, v7}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;-><init>(Ljava/io/OutputStream;IJ)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;->pOut:Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;

    .line 456
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;->pOut:Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->write(I)V

    .line 477
    :goto_3
    new-instance v12, Ljavax/crypto/CipherOutputStream;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;->pOut:Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;->c:Ljavax/crypto/Cipher;

    invoke-direct {v12, v3, v4}, Ljavax/crypto/CipherOutputStream;-><init>(Ljava/io/OutputStream;Ljavax/crypto/Cipher;)V

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;->cOut:Ljavax/crypto/CipherOutputStream;

    .line 479
    .local v12, "genOut":Ljava/io/OutputStream;
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;->withIntegrityPacket:Z

    if-eqz v3, :cond_7

    .line 481
    const/4 v3, 0x2

    invoke-static {v3}, Lcom/android/sec/org/bouncycastle/openpgp/PGPUtil;->getDigestName(I)Ljava/lang/String;

    move-result-object v10

    .line 482
    .local v10, "digestName":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;->defProvider:Ljava/security/Provider;

    invoke-static {v10, v3}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;Ljava/security/Provider;)Ljava/security/MessageDigest;

    move-result-object v9

    .line 483
    .local v9, "digest":Ljava/security/MessageDigest;
    new-instance v12, Ljava/security/DigestOutputStream;

    .end local v12    # "genOut":Ljava/io/OutputStream;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;->cOut:Ljavax/crypto/CipherOutputStream;

    invoke-direct {v12, v3, v9}, Ljava/security/DigestOutputStream;-><init>(Ljava/io/OutputStream;Ljava/security/MessageDigest;)V

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;->digestOut:Ljava/security/DigestOutputStream;

    .line 486
    .end local v9    # "digest":Ljava/security/MessageDigest;
    .end local v10    # "digestName":Ljava/lang/String;
    .restart local v12    # "genOut":Ljava/io/OutputStream;
    :cond_7
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;->c:Ljavax/crypto/Cipher;

    invoke-virtual {v3}, Ljavax/crypto/Cipher;->getBlockSize()I

    move-result v3

    add-int/lit8 v3, v3, 0x2

    new-array v14, v3, [B

    .line 487
    .local v14, "inLineIv":[B
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;->rand:Ljava/security/SecureRandom;

    invoke-virtual {v3, v14}, Ljava/security/SecureRandom;->nextBytes([B)V

    .line 488
    array-length v3, v14

    add-int/lit8 v3, v3, -0x1

    array-length v4, v14

    add-int/lit8 v4, v4, -0x3

    aget-byte v4, v14, v4

    aput-byte v4, v14, v3

    .line 489
    array-length v3, v14

    add-int/lit8 v3, v3, -0x2

    array-length v4, v14

    add-int/lit8 v4, v4, -0x4

    aget-byte v4, v14, v4

    aput-byte v4, v14, v3

    .line 491
    invoke-virtual {v12, v14}, Ljava/io/OutputStream;->write([B)V

    .line 493
    new-instance v3, Lcom/android/sec/org/bouncycastle/openpgp/WrappedGeneratorStream;

    move-object/from16 v0, p0

    invoke-direct {v3, v12, v0}, Lcom/android/sec/org/bouncycastle/openpgp/WrappedGeneratorStream;-><init>(Ljava/io/OutputStream;Lcom/android/sec/org/bouncycastle/openpgp/StreamGenerator;)V

    return-object v3

    .line 442
    .end local v12    # "genOut":Ljava/io/OutputStream;
    .end local v14    # "inLineIv":[B
    .end local v15    # "iv":[B
    :cond_8
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/OpenPGPCFB/NoPadding"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;->defProvider:Ljava/security/Provider;

    invoke-static {v3, v4}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;Ljava/security/Provider;)Ljavax/crypto/Cipher;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;->c:Ljavax/crypto/Cipher;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    goto/16 :goto_2

    .line 495
    :catch_2
    move-exception v11

    .line 497
    .restart local v11    # "e":Ljava/lang/Exception;
    new-instance v3, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;

    const-string v4, "Exception creating cipher"

    invoke-direct {v3, v4, v11}, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v3

    .line 460
    .end local v11    # "e":Ljava/lang/Exception;
    .restart local v15    # "iv":[B
    :cond_9
    :try_start_3
    new-instance v3, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;

    const/16 v5, 0x9

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;->c:Ljavax/crypto/Cipher;

    invoke-virtual {v4}, Ljavax/crypto/Cipher;->getBlockSize()I

    move-result v4

    int-to-long v6, v4

    add-long v6, v6, p2

    const-wide/16 v20, 0x2

    add-long v6, v6, v20

    move-object/from16 v0, p0

    iget-boolean v8, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;->oldFormat:Z

    move-object/from16 v4, p1

    invoke-direct/range {v3 .. v8}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;-><init>(Ljava/io/OutputStream;IJZ)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;->pOut:Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;

    goto/16 :goto_3

    .line 465
    :cond_a
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;->withIntegrityPacket:Z

    if-eqz v3, :cond_b

    .line 467
    new-instance v3, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;

    const/16 v4, 0x12

    move-object/from16 v0, p1

    move-object/from16 v1, p4

    invoke-direct {v3, v0, v4, v1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;-><init>(Ljava/io/OutputStream;I[B)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;->pOut:Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;

    .line 468
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;->pOut:Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->write(I)V

    goto/16 :goto_3

    .line 472
    :cond_b
    new-instance v3, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;

    const/16 v4, 0x9

    move-object/from16 v0, p1

    move-object/from16 v1, p4

    invoke-direct {v3, v0, v4, v1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;-><init>(Ljava/io/OutputStream;I[B)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;->pOut:Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    goto/16 :goto_3
.end method


# virtual methods
.method public addMethod(Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;)V
    .locals 2
    .param p1, "key"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/NoSuchProviderException;,
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
        }
    .end annotation

    .prologue
    .line 298
    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->isEncryptionKey()Z

    move-result v0

    if-nez v0, :cond_0

    .line 300
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "passed in key not an encryption key!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 303
    :cond_0
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;->defProvider:Ljava/security/Provider;

    if-nez v0, :cond_1

    .line 305
    new-instance v0, Ljava/security/NoSuchProviderException;

    const-string v1, "unable to find provider."

    invoke-direct {v0, v1}, Ljava/security/NoSuchProviderException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 308
    :cond_1
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;->methods:Ljava/util/List;

    new-instance v1, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator$PubMethod;

    invoke-direct {v1, p0, p1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator$PubMethod;-><init>(Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 309
    return-void
.end method

.method public addMethod([C)V
    .locals 7
    .param p1, "passPhrase"    # [C
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/NoSuchProviderException;,
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
        }
    .end annotation

    .prologue
    .line 273
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;->defProvider:Ljava/security/Provider;

    if-nez v2, :cond_0

    .line 275
    new-instance v2, Ljava/security/NoSuchProviderException;

    const-string v3, "unable to find provider."

    invoke-direct {v2, v3}, Ljava/security/NoSuchProviderException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 278
    :cond_0
    const/16 v2, 0x8

    new-array v0, v2, [B

    .line 280
    .local v0, "iv":[B
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;->rand:Ljava/security/SecureRandom;

    invoke-virtual {v2, v0}, Ljava/security/SecureRandom;->nextBytes([B)V

    .line 282
    new-instance v1, Lcom/android/sec/org/bouncycastle/bcpg/S2K;

    const/4 v2, 0x2

    const/16 v3, 0x60

    invoke-direct {v1, v2, v0, v3}, Lcom/android/sec/org/bouncycastle/bcpg/S2K;-><init>(I[BI)V

    .line 284
    .local v1, "s2k":Lcom/android/sec/org/bouncycastle/bcpg/S2K;
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;->methods:Ljava/util/List;

    new-instance v3, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator$PBEMethod;

    iget v4, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;->defAlgorithm:I

    iget v5, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;->defAlgorithm:I

    iget-object v6, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;->defProvider:Ljava/security/Provider;

    invoke-static {v5, v1, p1, v6}, Lcom/android/sec/org/bouncycastle/openpgp/PGPUtil;->makeKeyFromPassPhrase(ILcom/android/sec/org/bouncycastle/bcpg/S2K;[CLjava/security/Provider;)Ljavax/crypto/SecretKey;

    move-result-object v5

    invoke-direct {v3, p0, v4, v1, v5}, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator$PBEMethod;-><init>(Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;ILcom/android/sec/org/bouncycastle/bcpg/S2K;Ljava/security/Key;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 285
    return-void
.end method

.method public close()V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 559
    iget-object v3, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;->cOut:Ljavax/crypto/CipherOutputStream;

    if-eqz v3, :cond_1

    .line 561
    iget-object v3, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;->digestOut:Ljava/security/DigestOutputStream;

    if-eqz v3, :cond_0

    .line 566
    new-instance v0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;

    iget-object v3, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;->digestOut:Ljava/security/DigestOutputStream;

    const/16 v4, 0x13

    const-wide/16 v6, 0x14

    invoke-direct {v0, v3, v4, v6, v7}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;-><init>(Ljava/io/OutputStream;IJ)V

    .line 568
    .local v0, "bOut":Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;
    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->flush()V

    .line 569
    iget-object v3, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;->digestOut:Ljava/security/DigestOutputStream;

    invoke-virtual {v3}, Ljava/security/DigestOutputStream;->flush()V

    .line 571
    iget-object v3, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;->digestOut:Ljava/security/DigestOutputStream;

    invoke-virtual {v3}, Ljava/security/DigestOutputStream;->getMessageDigest()Ljava/security/MessageDigest;

    move-result-object v3

    invoke-virtual {v3}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v1

    .line 573
    .local v1, "dig":[B
    iget-object v3, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;->cOut:Ljavax/crypto/CipherOutputStream;

    invoke-virtual {v3, v1}, Ljavax/crypto/CipherOutputStream;->write([B)V

    .line 576
    .end local v0    # "bOut":Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;
    .end local v1    # "dig":[B
    :cond_0
    iget-object v3, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;->cOut:Ljavax/crypto/CipherOutputStream;

    invoke-virtual {v3}, Ljavax/crypto/CipherOutputStream;->flush()V

    .line 580
    :try_start_0
    iget-object v3, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;->pOut:Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;

    iget-object v4, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;->c:Ljavax/crypto/Cipher;

    invoke-virtual {v4}, Ljavax/crypto/Cipher;->doFinal()[B

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->write([B)V

    .line 581
    iget-object v3, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;->pOut:Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;

    invoke-virtual {v3}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->finish()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 588
    iput-object v5, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;->cOut:Ljavax/crypto/CipherOutputStream;

    .line 589
    iput-object v5, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;->pOut:Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;

    .line 591
    :cond_1
    return-void

    .line 583
    :catch_0
    move-exception v2

    .line 585
    .local v2, "e":Ljava/lang/Exception;
    new-instance v3, Ljava/io/IOException;

    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v3
.end method

.method public open(Ljava/io/OutputStream;J)Ljava/io/OutputStream;
    .locals 2
    .param p1, "out"    # Ljava/io/OutputStream;
    .param p2, "length"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
        }
    .end annotation

    .prologue
    .line 520
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;->open(Ljava/io/OutputStream;J[B)Ljava/io/OutputStream;

    move-result-object v0

    return-object v0
.end method

.method public open(Ljava/io/OutputStream;[B)Ljava/io/OutputStream;
    .locals 2
    .param p1, "out"    # Ljava/io/OutputStream;
    .param p2, "buffer"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
        }
    .end annotation

    .prologue
    .line 546
    const-wide/16 v0, 0x0

    invoke-direct {p0, p1, v0, v1, p2}, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;->open(Ljava/io/OutputStream;J[B)Ljava/io/OutputStream;

    move-result-object v0

    return-object v0
.end method
