.class public Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;
.super Ljava/io/InputStream;
.source "ArmoredInputStream.java"


# static fields
.field private static final decodingTable:[B


# instance fields
.field bufPtr:I

.field clearText:Z

.field crc:Lcom/android/sec/org/bouncycastle/bcpg/CRC24;

.field crcFound:Z

.field hasHeaders:Z

.field header:Ljava/lang/String;

.field headerList:Ljava/util/Vector;

.field in:Ljava/io/InputStream;

.field isEndOfStream:Z

.field lastC:I

.field newLineFound:Z

.field outBuf:[I

.field restart:Z

.field start:Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 21
    const/16 v1, 0x80

    new-array v1, v1, [B

    sput-object v1, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;->decodingTable:[B

    .line 23
    const/16 v0, 0x41

    .local v0, "i":I
    :goto_0
    const/16 v1, 0x5a

    if-gt v0, v1, :cond_0

    .line 25
    sget-object v1, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;->decodingTable:[B

    add-int/lit8 v2, v0, -0x41

    int-to-byte v2, v2

    aput-byte v2, v1, v0

    .line 23
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 28
    :cond_0
    const/16 v0, 0x61

    :goto_1
    const/16 v1, 0x7a

    if-gt v0, v1, :cond_1

    .line 30
    sget-object v1, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;->decodingTable:[B

    add-int/lit8 v2, v0, -0x61

    add-int/lit8 v2, v2, 0x1a

    int-to-byte v2, v2

    aput-byte v2, v1, v0

    .line 28
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 33
    :cond_1
    const/16 v0, 0x30

    :goto_2
    const/16 v1, 0x39

    if-gt v0, v1, :cond_2

    .line 35
    sget-object v1, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;->decodingTable:[B

    add-int/lit8 v2, v0, -0x30

    add-int/lit8 v2, v2, 0x34

    int-to-byte v2, v2

    aput-byte v2, v1, v0

    .line 33
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 38
    :cond_2
    sget-object v1, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;->decodingTable:[B

    const/16 v2, 0x2b

    const/16 v3, 0x3e

    aput-byte v3, v1, v2

    .line 39
    sget-object v1, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;->decodingTable:[B

    const/16 v2, 0x2f

    const/16 v3, 0x3f

    aput-byte v3, v1, v2

    .line 40
    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 1
    .param p1, "in"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 122
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;-><init>(Ljava/io/InputStream;Z)V

    .line 123
    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;Z)V
    .locals 4
    .param p1, "in"    # Ljava/io/InputStream;
    .param p2, "hasHeaders"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x3

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 137
    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    .line 98
    iput-boolean v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;->start:Z

    .line 99
    new-array v0, v3, [I

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;->outBuf:[I

    .line 100
    iput v3, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;->bufPtr:I

    .line 101
    new-instance v0, Lcom/android/sec/org/bouncycastle/bcpg/CRC24;

    invoke-direct {v0}, Lcom/android/sec/org/bouncycastle/bcpg/CRC24;-><init>()V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;->crc:Lcom/android/sec/org/bouncycastle/bcpg/CRC24;

    .line 102
    iput-boolean v1, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;->crcFound:Z

    .line 103
    iput-boolean v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;->hasHeaders:Z

    .line 104
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;->header:Ljava/lang/String;

    .line 105
    iput-boolean v1, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;->newLineFound:Z

    .line 106
    iput-boolean v1, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;->clearText:Z

    .line 107
    iput-boolean v1, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;->restart:Z

    .line 108
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;->headerList:Ljava/util/Vector;

    .line 109
    iput v1, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;->lastC:I

    .line 138
    iput-object p1, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;->in:Ljava/io/InputStream;

    .line 139
    iput-boolean p2, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;->hasHeaders:Z

    .line 141
    if-eqz p2, :cond_0

    .line 143
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;->parseHeaders()Z

    .line 146
    :cond_0
    iput-boolean v1, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;->start:Z

    .line 147
    return-void
.end method

.method private decode(IIII[I)I
    .locals 9
    .param p1, "in0"    # I
    .param p2, "in1"    # I
    .param p3, "in2"    # I
    .param p4, "in3"    # I
    .param p5, "out"    # [I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/EOFException;
        }
    .end annotation

    .prologue
    const/16 v7, 0x3d

    const/4 v6, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x2

    .line 57
    if-gez p4, :cond_0

    .line 59
    new-instance v4, Ljava/io/EOFException;

    const-string v5, "unexpected end of file in armored stream."

    invoke-direct {v4, v5}, Ljava/io/EOFException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 62
    :cond_0
    if-ne p3, v7, :cond_1

    .line 64
    sget-object v5, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;->decodingTable:[B

    aget-byte v5, v5, p1

    and-int/lit16 v0, v5, 0xff

    .line 65
    .local v0, "b1":I
    sget-object v5, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;->decodingTable:[B

    aget-byte v5, v5, p2

    and-int/lit16 v1, v5, 0xff

    .line 67
    .local v1, "b2":I
    shl-int/lit8 v5, v0, 0x2

    shr-int/lit8 v6, v1, 0x4

    or-int/2addr v5, v6

    and-int/lit16 v5, v5, 0xff

    aput v5, p5, v4

    .line 93
    :goto_0
    return v4

    .line 71
    .end local v0    # "b1":I
    .end local v1    # "b2":I
    :cond_1
    if-ne p4, v7, :cond_2

    .line 73
    sget-object v6, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;->decodingTable:[B

    aget-byte v0, v6, p1

    .line 74
    .restart local v0    # "b1":I
    sget-object v6, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;->decodingTable:[B

    aget-byte v1, v6, p2

    .line 75
    .restart local v1    # "b2":I
    sget-object v6, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;->decodingTable:[B

    aget-byte v2, v6, p3

    .line 77
    .local v2, "b3":I
    shl-int/lit8 v6, v0, 0x2

    shr-int/lit8 v7, v1, 0x4

    or-int/2addr v6, v7

    and-int/lit16 v6, v6, 0xff

    aput v6, p5, v5

    .line 78
    shl-int/lit8 v6, v1, 0x4

    shr-int/lit8 v7, v2, 0x2

    or-int/2addr v6, v7

    and-int/lit16 v6, v6, 0xff

    aput v6, p5, v4

    move v4, v5

    .line 80
    goto :goto_0

    .line 84
    .end local v0    # "b1":I
    .end local v1    # "b2":I
    .end local v2    # "b3":I
    :cond_2
    sget-object v7, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;->decodingTable:[B

    aget-byte v0, v7, p1

    .line 85
    .restart local v0    # "b1":I
    sget-object v7, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;->decodingTable:[B

    aget-byte v1, v7, p2

    .line 86
    .restart local v1    # "b2":I
    sget-object v7, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;->decodingTable:[B

    aget-byte v2, v7, p3

    .line 87
    .restart local v2    # "b3":I
    sget-object v7, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;->decodingTable:[B

    aget-byte v3, v7, p4

    .line 89
    .local v3, "b4":I
    shl-int/lit8 v7, v0, 0x2

    shr-int/lit8 v8, v1, 0x4

    or-int/2addr v7, v8

    and-int/lit16 v7, v7, 0xff

    aput v7, p5, v6

    .line 90
    shl-int/lit8 v7, v1, 0x4

    shr-int/lit8 v8, v2, 0x2

    or-int/2addr v7, v8

    and-int/lit16 v7, v7, 0xff

    aput v7, p5, v5

    .line 91
    shl-int/lit8 v5, v2, 0x6

    or-int/2addr v5, v3

    and-int/lit16 v5, v5, 0xff

    aput v5, p5, v4

    move v4, v6

    .line 93
    goto :goto_0
.end method

.method private parseHeaders()Z
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v11, 0x2d

    const/4 v10, 0x0

    const/16 v9, 0xa

    const/16 v8, 0xd

    .line 158
    const/4 v7, 0x0

    iput-object v7, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;->header:Ljava/lang/String;

    .line 161
    const/4 v5, 0x0

    .line 162
    .local v5, "last":I
    const/4 v4, 0x0

    .line 164
    .local v4, "headerFound":Z
    new-instance v7, Ljava/util/Vector;

    invoke-direct {v7}, Ljava/util/Vector;-><init>()V

    iput-object v7, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;->headerList:Ljava/util/Vector;

    .line 169
    iget-boolean v7, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;->restart:Z

    if-eqz v7, :cond_7

    .line 171
    const/4 v4, 0x1

    .line 187
    :cond_0
    :goto_0
    if-eqz v4, :cond_4

    .line 189
    new-instance v0, Ljava/lang/StringBuffer;

    const-string v7, "-"

    invoke-direct {v0, v7}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 190
    .local v0, "buf":Ljava/lang/StringBuffer;
    const/4 v3, 0x0

    .line 191
    .local v3, "eolReached":Z
    const/4 v2, 0x0

    .line 193
    .local v2, "crLf":Z
    iget-boolean v7, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;->restart:Z

    if-eqz v7, :cond_1

    .line 195
    invoke-virtual {v0, v11}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 198
    :cond_1
    :goto_1
    iget-object v7, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;->in:Ljava/io/InputStream;

    invoke-virtual {v7}, Ljava/io/InputStream;->read()I

    move-result v1

    .local v1, "c":I
    if-ltz v1, :cond_3

    .line 200
    if-ne v5, v8, :cond_2

    if-ne v1, v9, :cond_2

    .line 202
    const/4 v2, 0x1

    .line 204
    :cond_2
    if-eqz v3, :cond_9

    if-eq v5, v8, :cond_9

    if-ne v1, v9, :cond_9

    .line 239
    :cond_3
    if-eqz v2, :cond_4

    .line 241
    iget-object v7, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;->in:Ljava/io/InputStream;

    invoke-virtual {v7}, Ljava/io/InputStream;->read()I

    .line 245
    .end local v0    # "buf":Ljava/lang/StringBuffer;
    .end local v1    # "c":I
    .end local v2    # "crLf":Z
    .end local v3    # "eolReached":Z
    :cond_4
    iget-object v7, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;->headerList:Ljava/util/Vector;

    invoke-virtual {v7}, Ljava/util/Vector;->size()I

    move-result v7

    if-lez v7, :cond_5

    .line 247
    iget-object v7, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;->headerList:Ljava/util/Vector;

    invoke-virtual {v7, v10}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    iput-object v7, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;->header:Ljava/lang/String;

    .line 250
    :cond_5
    const-string v7, "-----BEGIN PGP SIGNED MESSAGE-----"

    iget-object v8, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;->header:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    iput-boolean v7, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;->clearText:Z

    .line 251
    const/4 v7, 0x1

    iput-boolean v7, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;->newLineFound:Z

    .line 253
    return v4

    .line 183
    .restart local v1    # "c":I
    :cond_6
    move v5, v1

    .line 175
    .end local v1    # "c":I
    :cond_7
    iget-object v7, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;->in:Ljava/io/InputStream;

    invoke-virtual {v7}, Ljava/io/InputStream;->read()I

    move-result v1

    .restart local v1    # "c":I
    if-ltz v1, :cond_0

    .line 177
    if-ne v1, v11, :cond_6

    if-eqz v5, :cond_8

    if-eq v5, v9, :cond_8

    if-ne v5, v8, :cond_6

    .line 179
    :cond_8
    const/4 v4, 0x1

    .line 180
    goto :goto_0

    .line 208
    .restart local v0    # "buf":Ljava/lang/StringBuffer;
    .restart local v2    # "crLf":Z
    .restart local v3    # "eolReached":Z
    :cond_9
    if-eqz v3, :cond_a

    if-eq v1, v8, :cond_3

    .line 212
    :cond_a
    if-eq v1, v8, :cond_b

    if-eq v5, v8, :cond_c

    if-ne v1, v9, :cond_c

    .line 214
    :cond_b
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    .line 215
    .local v6, "line":Ljava/lang/String;
    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    if-eqz v7, :cond_3

    .line 219
    iget-object v7, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;->headerList:Ljava/util/Vector;

    invoke-virtual {v7, v6}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 220
    invoke-virtual {v0, v10}, Ljava/lang/StringBuffer;->setLength(I)V

    .line 223
    .end local v6    # "line":Ljava/lang/String;
    :cond_c
    if-eq v1, v9, :cond_e

    if-eq v1, v8, :cond_e

    .line 225
    int-to-char v7, v1

    invoke-virtual {v0, v7}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 226
    const/4 v3, 0x0

    .line 236
    :cond_d
    :goto_2
    move v5, v1

    goto :goto_1

    .line 230
    :cond_e
    if-eq v1, v8, :cond_f

    if-eq v5, v8, :cond_d

    if-ne v1, v9, :cond_d

    .line 232
    :cond_f
    const/4 v3, 0x1

    goto :goto_2
.end method

.method private readIgnoreSpace()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 306
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;->in:Ljava/io/InputStream;

    invoke-virtual {v1}, Ljava/io/InputStream;->read()I

    move-result v0

    .line 308
    .local v0, "c":I
    :goto_0
    const/16 v1, 0x20

    if-eq v0, v1, :cond_0

    const/16 v1, 0x9

    if-ne v0, v1, :cond_1

    .line 310
    :cond_0
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;->in:Ljava/io/InputStream;

    invoke-virtual {v1}, Ljava/io/InputStream;->read()I

    move-result v0

    goto :goto_0

    .line 313
    :cond_1
    return v0
.end method


# virtual methods
.method public available()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 152
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;->in:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->available()I

    move-result v0

    return v0
.end method

.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 469
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;->in:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    .line 470
    return-void
.end method

.method public getArmorHeaderLine()Ljava/lang/String;
    .locals 1

    .prologue
    .line 279
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;->header:Ljava/lang/String;

    return-object v0
.end method

.method public getArmorHeaders()[Ljava/lang/String;
    .locals 4

    .prologue
    .line 288
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;->headerList:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v2

    const/4 v3, 0x1

    if-gt v2, v3, :cond_1

    .line 290
    const/4 v0, 0x0

    .line 300
    :cond_0
    return-object v0

    .line 293
    :cond_1
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;->headerList:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    new-array v0, v2, [Ljava/lang/String;

    .line 295
    .local v0, "hdrs":[Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v2, v0

    if-eq v1, v2, :cond_0

    .line 297
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;->headerList:Ljava/util/Vector;

    add-int/lit8 v3, v1, 0x1

    invoke-virtual {v2, v3}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    aput-object v2, v0, v1

    .line 295
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public isClearText()Z
    .locals 1

    .prologue
    .line 262
    iget-boolean v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;->clearText:Z

    return v0
.end method

.method public isEndOfStream()Z
    .locals 1

    .prologue
    .line 270
    iget-boolean v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;->isEndOfStream:Z

    return v0
.end method

.method public read()I
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v0, -0x1

    const/16 v5, 0xa

    const/16 v4, 0xd

    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 321
    iget-boolean v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;->start:Z

    if-eqz v2, :cond_1

    .line 323
    iget-boolean v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;->hasHeaders:Z

    if-eqz v2, :cond_0

    .line 325
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;->parseHeaders()Z

    .line 328
    :cond_0
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;->crc:Lcom/android/sec/org/bouncycastle/bcpg/CRC24;

    invoke-virtual {v2}, Lcom/android/sec/org/bouncycastle/bcpg/CRC24;->reset()V

    .line 329
    iput-boolean v9, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;->start:Z

    .line 332
    :cond_1
    iget-boolean v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;->clearText:Z

    if-eqz v2, :cond_9

    .line 334
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;->in:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->read()I

    move-result v1

    .line 336
    .local v1, "c":I
    if-eq v1, v4, :cond_2

    if-ne v1, v5, :cond_6

    iget v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;->lastC:I

    if-eq v0, v4, :cond_6

    .line 338
    :cond_2
    iput-boolean v8, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;->newLineFound:Z

    .line 363
    :cond_3
    :goto_0
    iput v1, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;->lastC:I

    .line 365
    if-gez v1, :cond_4

    .line 367
    iput-boolean v8, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;->isEndOfStream:Z

    :cond_4
    move v0, v1

    .line 463
    :cond_5
    :goto_1
    return v0

    .line 340
    :cond_6
    iget-boolean v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;->newLineFound:Z

    if-eqz v0, :cond_8

    const/16 v0, 0x2d

    if-ne v1, v0, :cond_8

    .line 342
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;->in:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->read()I

    move-result v1

    .line 343
    const/16 v0, 0x2d

    if-ne v1, v0, :cond_7

    .line 345
    iput-boolean v9, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;->clearText:Z

    .line 346
    iput-boolean v8, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;->start:Z

    .line 347
    iput-boolean v8, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;->restart:Z

    .line 353
    :goto_2
    iput-boolean v9, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;->newLineFound:Z

    goto :goto_0

    .line 351
    :cond_7
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;->in:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->read()I

    move-result v1

    goto :goto_2

    .line 357
    :cond_8
    if-eq v1, v5, :cond_3

    iget v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;->lastC:I

    if-eq v0, v4, :cond_3

    .line 359
    iput-boolean v9, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;->newLineFound:Z

    goto :goto_0

    .line 373
    .end local v1    # "c":I
    :cond_9
    iget v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;->bufPtr:I

    const/4 v3, 0x2

    if-gt v2, v3, :cond_a

    iget-boolean v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;->crcFound:Z

    if-eqz v2, :cond_16

    .line 375
    :cond_a
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;->readIgnoreSpace()I

    move-result v1

    .line 377
    .restart local v1    # "c":I
    if-eq v1, v4, :cond_b

    if-ne v1, v5, :cond_17

    .line 379
    :cond_b
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;->readIgnoreSpace()I

    move-result v1

    move v6, v1

    .line 381
    .end local v1    # "c":I
    .local v6, "c":I
    :goto_3
    if-eq v6, v5, :cond_c

    if-ne v6, v4, :cond_d

    .line 383
    :cond_c
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;->readIgnoreSpace()I

    move-result v1

    .end local v6    # "c":I
    .restart local v1    # "c":I
    move v6, v1

    .end local v1    # "c":I
    .restart local v6    # "c":I
    goto :goto_3

    .line 386
    :cond_d
    if-gez v6, :cond_e

    .line 388
    iput-boolean v8, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;->isEndOfStream:Z

    move v1, v6

    .line 389
    .end local v6    # "c":I
    .restart local v1    # "c":I
    goto :goto_1

    .line 392
    .end local v1    # "c":I
    .restart local v6    # "c":I
    :cond_e
    const/16 v2, 0x3d

    if-ne v6, v2, :cond_11

    .line 394
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;->readIgnoreSpace()I

    move-result v1

    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;->readIgnoreSpace()I

    move-result v2

    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;->readIgnoreSpace()I

    move-result v3

    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;->readIgnoreSpace()I

    move-result v4

    iget-object v5, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;->outBuf:[I

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;->decode(IIII[I)I

    move-result v0

    iput v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;->bufPtr:I

    .line 395
    iget v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;->bufPtr:I

    if-nez v0, :cond_10

    .line 397
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;->outBuf:[I

    aget v0, v0, v9

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x10

    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;->outBuf:[I

    aget v2, v2, v8

    and-int/lit16 v2, v2, 0xff

    shl-int/lit8 v2, v2, 0x8

    or-int/2addr v0, v2

    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;->outBuf:[I

    const/4 v3, 0x2

    aget v2, v2, v3

    and-int/lit16 v2, v2, 0xff

    or-int v7, v0, v2

    .line 401
    .local v7, "i":I
    iput-boolean v8, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;->crcFound:Z

    .line 403
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;->crc:Lcom/android/sec/org/bouncycastle/bcpg/CRC24;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/bcpg/CRC24;->getValue()I

    move-result v0

    if-eq v7, v0, :cond_f

    .line 405
    new-instance v0, Ljava/io/IOException;

    const-string v2, "crc check failed in armored message."

    invoke-direct {v0, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 407
    :cond_f
    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;->read()I

    move-result v1

    move v0, v1

    move v1, v6

    .end local v6    # "c":I
    .restart local v1    # "c":I
    goto/16 :goto_1

    .line 411
    .end local v1    # "c":I
    .end local v7    # "i":I
    .restart local v6    # "c":I
    :cond_10
    new-instance v0, Ljava/io/IOException;

    const-string v2, "no crc found in armored message."

    invoke-direct {v0, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 414
    :cond_11
    const/16 v2, 0x2d

    if-ne v6, v2, :cond_15

    move v1, v6

    .line 416
    .end local v6    # "c":I
    .restart local v1    # "c":I
    :cond_12
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;->in:Ljava/io/InputStream;

    invoke-virtual {v2}, Ljava/io/InputStream;->read()I

    move-result v1

    if-ltz v1, :cond_13

    .line 418
    if-eq v1, v5, :cond_13

    if-ne v1, v4, :cond_12

    .line 424
    :cond_13
    iget-boolean v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;->crcFound:Z

    if-nez v2, :cond_14

    .line 426
    new-instance v0, Ljava/io/IOException;

    const-string v2, "crc check not found."

    invoke-direct {v0, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 429
    :cond_14
    iput-boolean v9, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;->crcFound:Z

    .line 430
    iput-boolean v8, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;->start:Z

    .line 431
    const/4 v2, 0x3

    iput v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;->bufPtr:I

    .line 433
    if-gez v1, :cond_5

    .line 435
    iput-boolean v8, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;->isEndOfStream:Z

    goto/16 :goto_1

    .line 442
    .end local v1    # "c":I
    .restart local v6    # "c":I
    :cond_15
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;->readIgnoreSpace()I

    move-result v2

    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;->readIgnoreSpace()I

    move-result v3

    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;->readIgnoreSpace()I

    move-result v4

    iget-object v5, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;->outBuf:[I

    move-object v0, p0

    move v1, v6

    invoke-direct/range {v0 .. v5}, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;->decode(IIII[I)I

    move-result v0

    iput v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;->bufPtr:I

    .line 459
    .end local v6    # "c":I
    :cond_16
    :goto_4
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;->outBuf:[I

    iget v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;->bufPtr:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;->bufPtr:I

    aget v1, v0, v2

    .line 461
    .restart local v1    # "c":I
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;->crc:Lcom/android/sec/org/bouncycastle/bcpg/CRC24;

    invoke-virtual {v0, v1}, Lcom/android/sec/org/bouncycastle/bcpg/CRC24;->update(I)V

    move v0, v1

    .line 463
    goto/16 :goto_1

    .line 447
    :cond_17
    if-ltz v1, :cond_18

    .line 449
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;->readIgnoreSpace()I

    move-result v2

    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;->readIgnoreSpace()I

    move-result v3

    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;->readIgnoreSpace()I

    move-result v4

    iget-object v5, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;->outBuf:[I

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;->decode(IIII[I)I

    move-result v0

    iput v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;->bufPtr:I

    goto :goto_4

    .line 453
    :cond_18
    iput-boolean v8, p0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredInputStream;->isEndOfStream:Z

    goto/16 :goto_1
.end method
