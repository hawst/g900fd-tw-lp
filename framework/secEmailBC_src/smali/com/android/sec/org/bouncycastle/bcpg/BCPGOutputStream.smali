.class public Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;
.super Ljava/io/OutputStream;
.source "BCPGOutputStream.java"

# interfaces
.implements Lcom/android/sec/org/bouncycastle/bcpg/CompressionAlgorithmTags;
.implements Lcom/android/sec/org/bouncycastle/bcpg/PacketTags;


# static fields
.field private static final BUF_SIZE_POWER:I = 0x10


# instance fields
.field out:Ljava/io/OutputStream;

.field private partialBuffer:[B

.field private partialBufferLength:I

.field private partialOffset:I

.field private partialPower:I


# direct methods
.method public constructor <init>(Ljava/io/OutputStream;)V
    .locals 0
    .param p1, "out"    # Ljava/io/OutputStream;

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/io/OutputStream;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->out:Ljava/io/OutputStream;

    .line 24
    return-void
.end method

.method public constructor <init>(Ljava/io/OutputStream;I)V
    .locals 6
    .param p1, "out"    # Ljava/io/OutputStream;
    .param p2, "tag"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 35
    invoke-direct {p0}, Ljava/io/OutputStream;-><init>()V

    .line 36
    iput-object p1, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->out:Ljava/io/OutputStream;

    .line 37
    const-wide/16 v4, 0x0

    move-object v0, p0

    move v1, p2

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->writeHeader(IZZJ)V

    .line 38
    return-void
.end method

.method public constructor <init>(Ljava/io/OutputStream;IJ)V
    .locals 7
    .param p1, "out"    # Ljava/io/OutputStream;
    .param p2, "tag"    # I
    .param p3, "length"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 83
    invoke-direct {p0}, Ljava/io/OutputStream;-><init>()V

    .line 84
    iput-object p1, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->out:Ljava/io/OutputStream;

    move-object v0, p0

    move v1, p2

    move v3, v2

    move-wide v4, p3

    .line 86
    invoke-direct/range {v0 .. v5}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->writeHeader(IZZJ)V

    .line 87
    return-void
.end method

.method public constructor <init>(Ljava/io/OutputStream;IJZ)V
    .locals 11
    .param p1, "out"    # Ljava/io/OutputStream;
    .param p2, "tag"    # I
    .param p3, "length"    # J
    .param p5, "oldFormat"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 55
    invoke-direct {p0}, Ljava/io/OutputStream;-><init>()V

    .line 56
    iput-object p1, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->out:Ljava/io/OutputStream;

    .line 58
    const-wide v0, 0xffffffffL

    cmp-long v0, p3, v0

    if-lez v0, :cond_0

    .line 60
    const/4 v3, 0x1

    const-wide/16 v4, 0x0

    move-object v0, p0

    move v1, p2

    invoke-direct/range {v0 .. v5}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->writeHeader(IZZJ)V

    .line 61
    const/high16 v0, 0x10000

    iput v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->partialBufferLength:I

    .line 62
    iget v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->partialBufferLength:I

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->partialBuffer:[B

    .line 63
    const/16 v0, 0x10

    iput v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->partialPower:I

    .line 64
    iput v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->partialOffset:I

    .line 70
    :goto_0
    return-void

    :cond_0
    move-object v4, p0

    move v5, p2

    move/from16 v6, p5

    move v7, v2

    move-wide v8, p3

    .line 68
    invoke-direct/range {v4 .. v9}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->writeHeader(IZZJ)V

    goto :goto_0
.end method

.method public constructor <init>(Ljava/io/OutputStream;I[B)V
    .locals 7
    .param p1, "out"    # Ljava/io/OutputStream;
    .param p2, "tag"    # I
    .param p3, "buffer"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 102
    invoke-direct {p0}, Ljava/io/OutputStream;-><init>()V

    .line 103
    iput-object p1, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->out:Ljava/io/OutputStream;

    .line 104
    const-wide/16 v4, 0x0

    move-object v0, p0

    move v1, p2

    invoke-direct/range {v0 .. v5}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->writeHeader(IZZJ)V

    .line 106
    iput-object p3, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->partialBuffer:[B

    .line 108
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->partialBuffer:[B

    array-length v6, v0

    .line 110
    .local v6, "length":I
    iput v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->partialPower:I

    :goto_0
    if-eq v6, v3, :cond_0

    .line 112
    ushr-int/lit8 v6, v6, 0x1

    .line 110
    iget v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->partialPower:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->partialPower:I

    goto :goto_0

    .line 115
    :cond_0
    iget v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->partialPower:I

    const/16 v1, 0x1e

    if-le v0, v1, :cond_1

    .line 117
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Buffer cannot be greater than 2^30 in length."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 120
    :cond_1
    iget v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->partialPower:I

    shl-int v0, v3, v0

    iput v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->partialBufferLength:I

    .line 121
    iput v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->partialOffset:I

    .line 122
    return-void
.end method

.method private partialFlush(Z)V
    .locals 4
    .param p1, "isLast"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 215
    if-eqz p1, :cond_0

    .line 217
    iget v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->partialOffset:I

    int-to-long v0, v0

    invoke-direct {p0, v0, v1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->writeNewPacketLength(J)V

    .line 218
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->out:Ljava/io/OutputStream;

    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->partialBuffer:[B

    iget v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->partialOffset:I

    invoke-virtual {v0, v1, v3, v2}, Ljava/io/OutputStream;->write([BII)V

    .line 226
    :goto_0
    iput v3, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->partialOffset:I

    .line 227
    return-void

    .line 222
    :cond_0
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->out:Ljava/io/OutputStream;

    iget v1, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->partialPower:I

    or-int/lit16 v1, v1, 0xe0

    invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write(I)V

    .line 223
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->out:Ljava/io/OutputStream;

    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->partialBuffer:[B

    iget v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->partialBufferLength:I

    invoke-virtual {v0, v1, v3, v2}, Ljava/io/OutputStream;->write([BII)V

    goto :goto_0
.end method

.method private writeHeader(IZZJ)V
    .locals 6
    .param p1, "tag"    # I
    .param p2, "oldPackets"    # Z
    .param p3, "partial"    # Z
    .param p4, "bodyLen"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v4, 0x8

    .line 156
    const/16 v0, 0x80

    .line 158
    .local v0, "hdr":I
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->partialBuffer:[B

    if-eqz v1, :cond_0

    .line 160
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->partialFlush(Z)V

    .line 161
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->partialBuffer:[B

    .line 164
    :cond_0
    if-eqz p2, :cond_4

    .line 166
    shl-int/lit8 v1, p1, 0x2

    or-int/2addr v0, v1

    .line 168
    if-eqz p3, :cond_1

    .line 170
    or-int/lit8 v1, v0, 0x3

    invoke-virtual {p0, v1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->write(I)V

    .line 209
    :goto_0
    return-void

    .line 174
    :cond_1
    const-wide/16 v2, 0xff

    cmp-long v1, p4, v2

    if-gtz v1, :cond_2

    .line 176
    invoke-virtual {p0, v0}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->write(I)V

    .line 177
    long-to-int v1, p4

    int-to-byte v1, v1

    invoke-virtual {p0, v1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->write(I)V

    goto :goto_0

    .line 179
    :cond_2
    const-wide/32 v2, 0xffff

    cmp-long v1, p4, v2

    if-gtz v1, :cond_3

    .line 181
    or-int/lit8 v1, v0, 0x1

    invoke-virtual {p0, v1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->write(I)V

    .line 182
    shr-long v2, p4, v4

    long-to-int v1, v2

    int-to-byte v1, v1

    invoke-virtual {p0, v1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->write(I)V

    .line 183
    long-to-int v1, p4

    int-to-byte v1, v1

    invoke-virtual {p0, v1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->write(I)V

    goto :goto_0

    .line 187
    :cond_3
    or-int/lit8 v1, v0, 0x2

    invoke-virtual {p0, v1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->write(I)V

    .line 188
    const/16 v1, 0x18

    shr-long v2, p4, v1

    long-to-int v1, v2

    int-to-byte v1, v1

    invoke-virtual {p0, v1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->write(I)V

    .line 189
    const/16 v1, 0x10

    shr-long v2, p4, v1

    long-to-int v1, v2

    int-to-byte v1, v1

    invoke-virtual {p0, v1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->write(I)V

    .line 190
    shr-long v2, p4, v4

    long-to-int v1, v2

    int-to-byte v1, v1

    invoke-virtual {p0, v1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->write(I)V

    .line 191
    long-to-int v1, p4

    int-to-byte v1, v1

    invoke-virtual {p0, v1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->write(I)V

    goto :goto_0

    .line 197
    :cond_4
    or-int/lit8 v1, p1, 0x40

    or-int/2addr v0, v1

    .line 198
    invoke-virtual {p0, v0}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->write(I)V

    .line 200
    if-eqz p3, :cond_5

    .line 202
    const/4 v1, 0x0

    iput v1, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->partialOffset:I

    goto :goto_0

    .line 206
    :cond_5
    invoke-direct {p0, p4, p5}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->writeNewPacketLength(J)V

    goto :goto_0
.end method

.method private writeNewPacketLength(J)V
    .locals 9
    .param p1, "bodyLen"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v4, 0x8

    const-wide/16 v6, 0xc0

    .line 128
    cmp-long v0, p1, v6

    if-gez v0, :cond_0

    .line 130
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->out:Ljava/io/OutputStream;

    long-to-int v1, p1

    int-to-byte v1, v1

    invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write(I)V

    .line 147
    :goto_0
    return-void

    .line 132
    :cond_0
    const-wide/16 v0, 0x20bf

    cmp-long v0, p1, v0

    if-gtz v0, :cond_1

    .line 134
    sub-long/2addr p1, v6

    .line 136
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->out:Ljava/io/OutputStream;

    shr-long v2, p1, v4

    const-wide/16 v4, 0xff

    and-long/2addr v2, v4

    add-long/2addr v2, v6

    long-to-int v1, v2

    int-to-byte v1, v1

    invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write(I)V

    .line 137
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->out:Ljava/io/OutputStream;

    long-to-int v1, p1

    int-to-byte v1, v1

    invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write(I)V

    goto :goto_0

    .line 141
    :cond_1
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->out:Ljava/io/OutputStream;

    const/16 v1, 0xff

    invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write(I)V

    .line 142
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->out:Ljava/io/OutputStream;

    const/16 v1, 0x18

    shr-long v2, p1, v1

    long-to-int v1, v2

    int-to-byte v1, v1

    invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write(I)V

    .line 143
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->out:Ljava/io/OutputStream;

    const/16 v1, 0x10

    shr-long v2, p1, v1

    long-to-int v1, v2

    int-to-byte v1, v1

    invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write(I)V

    .line 144
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->out:Ljava/io/OutputStream;

    shr-long v2, p1, v4

    long-to-int v1, v2

    int-to-byte v1, v1

    invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write(I)V

    .line 145
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->out:Ljava/io/OutputStream;

    long-to-int v1, p1

    int-to-byte v1, v1

    invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write(I)V

    goto :goto_0
.end method

.method private writePartial(B)V
    .locals 3
    .param p1, "b"    # B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 233
    iget v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->partialOffset:I

    iget v1, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->partialBufferLength:I

    if-ne v0, v1, :cond_0

    .line 235
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->partialFlush(Z)V

    .line 238
    :cond_0
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->partialBuffer:[B

    iget v1, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->partialOffset:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->partialOffset:I

    aput-byte p1, v0, v1

    .line 239
    return-void
.end method

.method private writePartial([BII)V
    .locals 5
    .param p1, "buf"    # [B
    .param p2, "off"    # I
    .param p3, "len"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 247
    iget v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->partialOffset:I

    iget v1, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->partialBufferLength:I

    if-ne v0, v1, :cond_0

    .line 249
    invoke-direct {p0, v4}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->partialFlush(Z)V

    .line 252
    :cond_0
    iget v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->partialBufferLength:I

    iget v1, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->partialOffset:I

    sub-int/2addr v0, v1

    if-gt p3, v0, :cond_1

    .line 254
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->partialBuffer:[B

    iget v1, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->partialOffset:I

    invoke-static {p1, p2, v0, v1, p3}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 255
    iget v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->partialOffset:I

    add-int/2addr v0, p3

    iput v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->partialOffset:I

    .line 275
    :goto_0
    return-void

    .line 259
    :cond_1
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->partialBuffer:[B

    iget v1, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->partialOffset:I

    iget v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->partialBufferLength:I

    iget v3, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->partialOffset:I

    sub-int/2addr v2, v3

    invoke-static {p1, p2, v0, v1, v2}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 260
    iget v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->partialBufferLength:I

    iget v1, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->partialOffset:I

    sub-int/2addr v0, v1

    add-int/2addr p2, v0

    .line 261
    iget v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->partialBufferLength:I

    iget v1, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->partialOffset:I

    sub-int/2addr v0, v1

    sub-int/2addr p3, v0

    .line 262
    invoke-direct {p0, v4}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->partialFlush(Z)V

    .line 264
    :goto_1
    iget v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->partialBufferLength:I

    if-le p3, v0, :cond_2

    .line 266
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->partialBuffer:[B

    iget v1, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->partialBufferLength:I

    invoke-static {p1, p2, v0, v4, v1}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 267
    iget v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->partialBufferLength:I

    add-int/2addr p2, v0

    .line 268
    iget v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->partialBufferLength:I

    sub-int/2addr p3, v0

    .line 269
    invoke-direct {p0, v4}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->partialFlush(Z)V

    goto :goto_1

    .line 272
    :cond_2
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->partialBuffer:[B

    invoke-static {p1, p2, v0, v4, p3}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 273
    iget v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->partialOffset:I

    add-int/2addr v0, p3

    iput v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->partialOffset:I

    goto :goto_0
.end method


# virtual methods
.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 356
    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->finish()V

    .line 357
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->out:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->flush()V

    .line 358
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->out:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V

    .line 359
    return-void
.end method

.method public finish()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 346
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->partialBuffer:[B

    if-eqz v0, :cond_0

    .line 348
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->partialFlush(Z)V

    .line 349
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->partialBuffer:[B

    .line 351
    :cond_0
    return-void
.end method

.method public flush()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 337
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->out:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->flush()V

    .line 338
    return-void
.end method

.method public write(I)V
    .locals 1
    .param p1, "b"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 281
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->partialBuffer:[B

    if-eqz v0, :cond_0

    .line 283
    int-to-byte v0, p1

    invoke-direct {p0, v0}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->writePartial(B)V

    .line 289
    :goto_0
    return-void

    .line 287
    :cond_0
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->out:Ljava/io/OutputStream;

    invoke-virtual {v0, p1}, Ljava/io/OutputStream;->write(I)V

    goto :goto_0
.end method

.method public write([BII)V
    .locals 1
    .param p1, "bytes"    # [B
    .param p2, "off"    # I
    .param p3, "len"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 297
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->partialBuffer:[B

    if-eqz v0, :cond_0

    .line 299
    invoke-direct {p0, p1, p2, p3}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->writePartial([BII)V

    .line 305
    :goto_0
    return-void

    .line 303
    :cond_0
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->out:Ljava/io/OutputStream;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/OutputStream;->write([BII)V

    goto :goto_0
.end method

.method public writeObject(Lcom/android/sec/org/bouncycastle/bcpg/BCPGObject;)V
    .locals 0
    .param p1, "o"    # Lcom/android/sec/org/bouncycastle/bcpg/BCPGObject;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 328
    invoke-virtual {p1, p0}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGObject;->encode(Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;)V

    .line 329
    return-void
.end method

.method writePacket(I[BZ)V
    .locals 6
    .param p1, "tag"    # I
    .param p2, "body"    # [B
    .param p3, "oldFormat"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 320
    const/4 v3, 0x0

    array-length v0, p2

    int-to-long v4, v0

    move-object v0, p0

    move v1, p1

    move v2, p3

    invoke-direct/range {v0 .. v5}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->writeHeader(IZZJ)V

    .line 321
    invoke-virtual {p0, p2}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->write([B)V

    .line 322
    return-void
.end method

.method public writePacket(Lcom/android/sec/org/bouncycastle/bcpg/ContainedPacket;)V
    .locals 0
    .param p1, "p"    # Lcom/android/sec/org/bouncycastle/bcpg/ContainedPacket;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 311
    invoke-virtual {p1, p0}, Lcom/android/sec/org/bouncycastle/bcpg/ContainedPacket;->encode(Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;)V

    .line 312
    return-void
.end method
