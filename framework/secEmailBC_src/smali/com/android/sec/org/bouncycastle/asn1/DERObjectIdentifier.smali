.class public Lcom/android/sec/org/bouncycastle/asn1/DERObjectIdentifier;
.super Lcom/android/sec/org/bouncycastle/asn1/ASN1Primitive;
.source "DERObjectIdentifier.java"


# static fields
.field private static final LONG_LIMIT:J = 0xffffffffffff80L

.field private static cache:[[Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;


# instance fields
.field private body:[B

.field identifier:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 370
    const/16 v0, 0x100

    new-array v0, v0, [[Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    sput-object v0, Lcom/android/sec/org/bouncycastle/asn1/DERObjectIdentifier;->cache:[[Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    return-void
.end method

.method constructor <init>(Lcom/android/sec/org/bouncycastle/asn1/DERObjectIdentifier;Ljava/lang/String;)V
    .locals 3
    .param p1, "oid"    # Lcom/android/sec/org/bouncycastle/asn1/DERObjectIdentifier;
    .param p2, "branchID"    # Ljava/lang/String;

    .prologue
    .line 179
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Primitive;-><init>()V

    .line 180
    const/4 v0, 0x0

    invoke-static {p2, v0}, Lcom/android/sec/org/bouncycastle/asn1/DERObjectIdentifier;->isValidBranchID(Ljava/lang/String;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 182
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "string "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not a valid OID branch"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 185
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/asn1/DERObjectIdentifier;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/asn1/DERObjectIdentifier;->identifier:Ljava/lang/String;

    .line 186
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 3
    .param p1, "identifier"    # Ljava/lang/String;

    .prologue
    .line 159
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Primitive;-><init>()V

    .line 160
    if-nez p1, :cond_0

    .line 162
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "\'identifier\' cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 164
    :cond_0
    invoke-static {p1}, Lcom/android/sec/org/bouncycastle/asn1/DERObjectIdentifier;->isValidIdentifier(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 166
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "string "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not an OID"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 174
    :cond_1
    invoke-virtual {p1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/asn1/DERObjectIdentifier;->identifier:Ljava/lang/String;

    .line 176
    return-void
.end method

.method constructor <init>([B)V
    .locals 10
    .param p1, "bytes"    # [B

    .prologue
    .line 76
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Primitive;-><init>()V

    .line 77
    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    .line 78
    .local v4, "objId":Ljava/lang/StringBuffer;
    const-wide/16 v6, 0x0

    .line 79
    .local v6, "value":J
    const/4 v1, 0x0

    .line 80
    .local v1, "bigValue":Ljava/math/BigInteger;
    const/4 v2, 0x1

    .line 82
    .local v2, "first":Z
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    array-length v5, p1

    if-eq v3, v5, :cond_8

    .line 84
    aget-byte v5, p1, v3

    and-int/lit16 v0, v5, 0xff

    .line 86
    .local v0, "b":I
    const-wide v8, 0xffffffffffff80L

    cmp-long v5, v6, v8

    if-gtz v5, :cond_4

    .line 88
    and-int/lit8 v5, v0, 0x7f

    int-to-long v8, v5

    add-long/2addr v6, v8

    .line 89
    and-int/lit16 v5, v0, 0x80

    if-nez v5, :cond_3

    .line 91
    if-eqz v2, :cond_0

    .line 93
    const-wide/16 v8, 0x28

    cmp-long v5, v6, v8

    if-gez v5, :cond_1

    .line 95
    const/16 v5, 0x30

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 107
    :goto_1
    const/4 v2, 0x0

    .line 110
    :cond_0
    const/16 v5, 0x2e

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 111
    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    .line 112
    const-wide/16 v6, 0x0

    .line 82
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 97
    :cond_1
    const-wide/16 v8, 0x50

    cmp-long v5, v6, v8

    if-gez v5, :cond_2

    .line 99
    const/16 v5, 0x31

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 100
    const-wide/16 v8, 0x28

    sub-long/2addr v6, v8

    goto :goto_1

    .line 104
    :cond_2
    const/16 v5, 0x32

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 105
    const-wide/16 v8, 0x50

    sub-long/2addr v6, v8

    goto :goto_1

    .line 116
    :cond_3
    const/4 v5, 0x7

    shl-long/2addr v6, v5

    goto :goto_2

    .line 121
    :cond_4
    if-nez v1, :cond_5

    .line 123
    invoke-static {v6, v7}, Ljava/math/BigInteger;->valueOf(J)Ljava/math/BigInteger;

    move-result-object v1

    .line 125
    :cond_5
    and-int/lit8 v5, v0, 0x7f

    int-to-long v8, v5

    invoke-static {v8, v9}, Ljava/math/BigInteger;->valueOf(J)Ljava/math/BigInteger;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/math/BigInteger;->or(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v1

    .line 126
    and-int/lit16 v5, v0, 0x80

    if-nez v5, :cond_7

    .line 128
    if-eqz v2, :cond_6

    .line 130
    const/16 v5, 0x32

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 131
    const-wide/16 v8, 0x50

    invoke-static {v8, v9}, Ljava/math/BigInteger;->valueOf(J)Ljava/math/BigInteger;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/math/BigInteger;->subtract(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v1

    .line 132
    const/4 v2, 0x0

    .line 135
    :cond_6
    const/16 v5, 0x2e

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 136
    invoke-virtual {v4, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    .line 137
    const/4 v1, 0x0

    .line 138
    const-wide/16 v6, 0x0

    goto :goto_2

    .line 142
    :cond_7
    const/4 v5, 0x7

    invoke-virtual {v1, v5}, Ljava/math/BigInteger;->shiftLeft(I)Ljava/math/BigInteger;

    move-result-object v1

    goto :goto_2

    .line 152
    .end local v0    # "b":I
    :cond_8
    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/android/sec/org/bouncycastle/asn1/DERObjectIdentifier;->identifier:Ljava/lang/String;

    .line 154
    invoke-static {p1}, Lcom/android/sec/org/bouncycastle/util/Arrays;->clone([B)[B

    move-result-object v5

    iput-object v5, p0, Lcom/android/sec/org/bouncycastle/asn1/DERObjectIdentifier;->body:[B

    .line 155
    return-void
.end method

.method private doOutput(Ljava/io/ByteArrayOutputStream;)V
    .locals 9
    .param p1, "aOut"    # Ljava/io/ByteArrayOutputStream;

    .prologue
    const/16 v8, 0x12

    .line 233
    new-instance v2, Lcom/android/sec/org/bouncycastle/asn1/OIDTokenizer;

    iget-object v4, p0, Lcom/android/sec/org/bouncycastle/asn1/DERObjectIdentifier;->identifier:Ljava/lang/String;

    invoke-direct {v2, v4}, Lcom/android/sec/org/bouncycastle/asn1/OIDTokenizer;-><init>(Ljava/lang/String;)V

    .line 234
    .local v2, "tok":Lcom/android/sec/org/bouncycastle/asn1/OIDTokenizer;
    invoke-virtual {v2}, Lcom/android/sec/org/bouncycastle/asn1/OIDTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    mul-int/lit8 v0, v4, 0x28

    .line 236
    .local v0, "first":I
    invoke-virtual {v2}, Lcom/android/sec/org/bouncycastle/asn1/OIDTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v1

    .line 237
    .local v1, "secondToken":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    if-gt v4, v8, :cond_0

    .line 239
    int-to-long v4, v0

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    add-long/2addr v4, v6

    invoke-direct {p0, p1, v4, v5}, Lcom/android/sec/org/bouncycastle/asn1/DERObjectIdentifier;->writeField(Ljava/io/ByteArrayOutputStream;J)V

    .line 246
    :goto_0
    invoke-virtual {v2}, Lcom/android/sec/org/bouncycastle/asn1/OIDTokenizer;->hasMoreTokens()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 248
    invoke-virtual {v2}, Lcom/android/sec/org/bouncycastle/asn1/OIDTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v3

    .line 249
    .local v3, "token":Ljava/lang/String;
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    if-gt v4, v8, :cond_1

    .line 251
    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-direct {p0, p1, v4, v5}, Lcom/android/sec/org/bouncycastle/asn1/DERObjectIdentifier;->writeField(Ljava/io/ByteArrayOutputStream;J)V

    goto :goto_0

    .line 243
    .end local v3    # "token":Ljava/lang/String;
    :cond_0
    new-instance v4, Ljava/math/BigInteger;

    invoke-direct {v4, v1}, Ljava/math/BigInteger;-><init>(Ljava/lang/String;)V

    int-to-long v6, v0

    invoke-static {v6, v7}, Ljava/math/BigInteger;->valueOf(J)Ljava/math/BigInteger;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/math/BigInteger;->add(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v4

    invoke-direct {p0, p1, v4}, Lcom/android/sec/org/bouncycastle/asn1/DERObjectIdentifier;->writeField(Ljava/io/ByteArrayOutputStream;Ljava/math/BigInteger;)V

    goto :goto_0

    .line 255
    .restart local v3    # "token":Ljava/lang/String;
    :cond_1
    new-instance v4, Ljava/math/BigInteger;

    invoke-direct {v4, v3}, Ljava/math/BigInteger;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, p1, v4}, Lcom/android/sec/org/bouncycastle/asn1/DERObjectIdentifier;->writeField(Ljava/io/ByteArrayOutputStream;Ljava/math/BigInteger;)V

    goto :goto_0

    .line 258
    .end local v3    # "token":Ljava/lang/String;
    :cond_2
    return-void
.end method

.method static fromOctetString([B)Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;
    .locals 7
    .param p0, "enc"    # [B

    .prologue
    .line 374
    array-length v4, p0

    const/4 v5, 0x3

    if-ge v4, v5, :cond_1

    .line 376
    new-instance v3, Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    invoke-direct {v3, p0}, Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;-><init>([B)V

    .line 435
    :cond_0
    :goto_0
    return-object v3

    .line 379
    :cond_1
    array-length v4, p0

    add-int/lit8 v4, v4, -0x2

    aget-byte v4, p0, v4

    and-int/lit16 v1, v4, 0xff

    .line 381
    .local v1, "idx1":I
    array-length v4, p0

    add-int/lit8 v4, v4, -0x1

    aget-byte v4, p0, v4

    and-int/lit8 v2, v4, 0x7f

    .line 385
    .local v2, "idx2":I
    sget-object v5, Lcom/android/sec/org/bouncycastle/asn1/DERObjectIdentifier;->cache:[[Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    monitor-enter v5

    .line 387
    :try_start_0
    sget-object v4, Lcom/android/sec/org/bouncycastle/asn1/DERObjectIdentifier;->cache:[[Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    aget-object v0, v4, v1

    .line 388
    .local v0, "first":[Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;
    if-nez v0, :cond_2

    .line 390
    sget-object v4, Lcom/android/sec/org/bouncycastle/asn1/DERObjectIdentifier;->cache:[[Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    const/16 v6, 0x80

    new-array v0, v6, [Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    .end local v0    # "first":[Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;
    aput-object v0, v4, v1

    .line 393
    .restart local v0    # "first":[Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;
    :cond_2
    aget-object v3, v0, v2

    .line 394
    .local v3, "possibleMatch":Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;
    if-nez v3, :cond_3

    .line 396
    new-instance v3, Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    .end local v3    # "possibleMatch":Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;
    invoke-direct {v3, p0}, Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;-><init>([B)V

    aput-object v3, v0, v2

    monitor-exit v5

    goto :goto_0

    .line 428
    .end local v0    # "first":[Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4

    .line 399
    .restart local v0    # "first":[Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;
    .restart local v3    # "possibleMatch":Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;
    :cond_3
    :try_start_1
    invoke-virtual {v3}, Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;->getBody()[B

    move-result-object v4

    invoke-static {p0, v4}, Lcom/android/sec/org/bouncycastle/util/Arrays;->areEqual([B[B)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 401
    monitor-exit v5

    goto :goto_0

    .line 404
    :cond_4
    add-int/lit8 v4, v1, 0x1

    and-int/lit16 v1, v4, 0xff

    .line 405
    sget-object v4, Lcom/android/sec/org/bouncycastle/asn1/DERObjectIdentifier;->cache:[[Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    aget-object v0, v4, v1

    .line 406
    if-nez v0, :cond_5

    .line 408
    sget-object v4, Lcom/android/sec/org/bouncycastle/asn1/DERObjectIdentifier;->cache:[[Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    const/16 v6, 0x80

    new-array v0, v6, [Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    .end local v0    # "first":[Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;
    aput-object v0, v4, v1

    .line 411
    .restart local v0    # "first":[Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;
    :cond_5
    aget-object v3, v0, v2

    .line 412
    if-nez v3, :cond_6

    .line 414
    new-instance v3, Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    .end local v3    # "possibleMatch":Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;
    invoke-direct {v3, p0}, Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;-><init>([B)V

    aput-object v3, v0, v2

    monitor-exit v5

    goto :goto_0

    .line 417
    .restart local v3    # "possibleMatch":Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;
    :cond_6
    invoke-virtual {v3}, Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;->getBody()[B

    move-result-object v4

    invoke-static {p0, v4}, Lcom/android/sec/org/bouncycastle/util/Arrays;->areEqual([B[B)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 419
    monitor-exit v5

    goto :goto_0

    .line 422
    :cond_7
    add-int/lit8 v4, v2, 0x1

    and-int/lit8 v2, v4, 0x7f

    .line 423
    aget-object v3, v0, v2

    .line 424
    if-nez v3, :cond_8

    .line 426
    new-instance v3, Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    .end local v3    # "possibleMatch":Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;
    invoke-direct {v3, p0}, Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;-><init>([B)V

    aput-object v3, v0, v2

    monitor-exit v5

    goto :goto_0

    .line 428
    .restart local v3    # "possibleMatch":Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;
    :cond_8
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 430
    invoke-virtual {v3}, Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;->getBody()[B

    move-result-object v4

    invoke-static {p0, v4}, Lcom/android/sec/org/bouncycastle/util/Arrays;->areEqual([B[B)Z

    move-result v4

    if-nez v4, :cond_0

    .line 435
    new-instance v3, Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    .end local v3    # "possibleMatch":Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;
    invoke-direct {v3, p0}, Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;-><init>([B)V

    goto/16 :goto_0
.end method

.method public static getInstance(Lcom/android/sec/org/bouncycastle/asn1/ASN1TaggedObject;Z)Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;
    .locals 2
    .param p0, "obj"    # Lcom/android/sec/org/bouncycastle/asn1/ASN1TaggedObject;
    .param p1, "explicit"    # Z

    .prologue
    .line 60
    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/asn1/ASN1TaggedObject;->getObject()Lcom/android/sec/org/bouncycastle/asn1/ASN1Primitive;

    move-result-object v0

    .line 62
    .local v0, "o":Lcom/android/sec/org/bouncycastle/asn1/ASN1Primitive;
    if-nez p1, :cond_0

    instance-of v1, v0, Lcom/android/sec/org/bouncycastle/asn1/DERObjectIdentifier;

    if-eqz v1, :cond_1

    .line 64
    :cond_0
    invoke-static {v0}, Lcom/android/sec/org/bouncycastle/asn1/DERObjectIdentifier;->getInstance(Ljava/lang/Object;)Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    move-result-object v1

    .line 68
    :goto_0
    return-object v1

    :cond_1
    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/asn1/ASN1TaggedObject;->getObject()Lcom/android/sec/org/bouncycastle/asn1/ASN1Primitive;

    move-result-object v1

    invoke-static {v1}, Lcom/android/sec/org/bouncycastle/asn1/ASN1OctetString;->getInstance(Ljava/lang/Object;)Lcom/android/sec/org/bouncycastle/asn1/ASN1OctetString;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/sec/org/bouncycastle/asn1/ASN1OctetString;->getOctets()[B

    move-result-object v1

    invoke-static {v1}, Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;->fromOctetString([B)Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    move-result-object v1

    goto :goto_0
.end method

.method public static getInstance(Ljava/lang/Object;)Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;
    .locals 3
    .param p0, "obj"    # Ljava/lang/Object;

    .prologue
    .line 24
    if-eqz p0, :cond_0

    instance-of v0, p0, Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    if-eqz v0, :cond_1

    .line 26
    :cond_0
    check-cast p0, Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    .end local p0    # "obj":Ljava/lang/Object;
    move-object v0, p0

    .line 41
    :goto_0
    return-object v0

    .line 29
    .restart local p0    # "obj":Ljava/lang/Object;
    :cond_1
    instance-of v0, p0, Lcom/android/sec/org/bouncycastle/asn1/DERObjectIdentifier;

    if-eqz v0, :cond_2

    .line 31
    new-instance v0, Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    check-cast p0, Lcom/android/sec/org/bouncycastle/asn1/DERObjectIdentifier;

    .end local p0    # "obj":Ljava/lang/Object;
    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/asn1/DERObjectIdentifier;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 34
    .restart local p0    # "obj":Ljava/lang/Object;
    :cond_2
    instance-of v0, p0, Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;

    if-eqz v0, :cond_3

    move-object v0, p0

    check-cast v0, Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;

    invoke-interface {v0}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;->toASN1Primitive()Lcom/android/sec/org/bouncycastle/asn1/ASN1Primitive;

    move-result-object v0

    instance-of v0, v0, Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    if-eqz v0, :cond_3

    .line 36
    check-cast p0, Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;

    .end local p0    # "obj":Ljava/lang/Object;
    invoke-interface {p0}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;->toASN1Primitive()Lcom/android/sec/org/bouncycastle/asn1/ASN1Primitive;

    move-result-object v0

    check-cast v0, Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    goto :goto_0

    .line 39
    .restart local p0    # "obj":Ljava/lang/Object;
    :cond_3
    instance-of v0, p0, [B

    if-eqz v0, :cond_4

    .line 41
    check-cast p0, [B

    .end local p0    # "obj":Ljava/lang/Object;
    check-cast p0, [B

    invoke-static {p0}, Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;->fromOctetString([B)Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    move-result-object v0

    goto :goto_0

    .line 44
    .restart local p0    # "obj":Ljava/lang/Object;
    :cond_4
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "illegal object in getInstance: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static isValidBranchID(Ljava/lang/String;I)Z
    .locals 5
    .param p0, "branchID"    # Ljava/lang/String;
    .param p1, "start"    # I

    .prologue
    const/4 v3, 0x0

    .line 322
    const/4 v1, 0x0

    .line 324
    .local v1, "periodAllowed":Z
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    .line 325
    .local v2, "pos":I
    :goto_0
    add-int/lit8 v2, v2, -0x1

    if-lt v2, p1, :cond_1

    .line 327
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 330
    .local v0, "ch":C
    const/16 v4, 0x30

    if-gt v4, v0, :cond_0

    const/16 v4, 0x39

    if-gt v0, v4, :cond_0

    .line 332
    const/4 v1, 0x1

    .line 333
    goto :goto_0

    .line 336
    :cond_0
    const/16 v4, 0x2e

    if-ne v0, v4, :cond_3

    .line 338
    if-nez v1, :cond_2

    move v1, v3

    .line 350
    .end local v0    # "ch":C
    .end local v1    # "periodAllowed":Z
    :cond_1
    :goto_1
    return v1

    .line 343
    .restart local v0    # "ch":C
    .restart local v1    # "periodAllowed":Z
    :cond_2
    const/4 v1, 0x0

    .line 344
    goto :goto_0

    :cond_3
    move v1, v3

    .line 347
    goto :goto_1
.end method

.method private static isValidIdentifier(Ljava/lang/String;)Z
    .locals 4
    .param p0, "identifier"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 356
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v3, 0x3

    if-lt v2, v3, :cond_0

    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x2e

    if-eq v2, v3, :cond_1

    .line 367
    :cond_0
    :goto_0
    return v1

    .line 361
    :cond_1
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 362
    .local v0, "first":C
    const/16 v2, 0x30

    if-lt v0, v2, :cond_0

    const/16 v2, 0x32

    if-gt v0, v2, :cond_0

    .line 367
    const/4 v1, 0x2

    invoke-static {p0, v1}, Lcom/android/sec/org/bouncycastle/asn1/DERObjectIdentifier;->isValidBranchID(Ljava/lang/String;I)Z

    move-result v1

    goto :goto_0
.end method

.method private writeField(Ljava/io/ByteArrayOutputStream;J)V
    .locals 4
    .param p1, "out"    # Ljava/io/ByteArrayOutputStream;
    .param p2, "fieldValue"    # J

    .prologue
    .line 197
    const/16 v2, 0x9

    new-array v1, v2, [B

    .line 198
    .local v1, "result":[B
    const/16 v0, 0x8

    .line 199
    .local v0, "pos":I
    long-to-int v2, p2

    and-int/lit8 v2, v2, 0x7f

    int-to-byte v2, v2

    aput-byte v2, v1, v0

    .line 200
    :goto_0
    const-wide/16 v2, 0x80

    cmp-long v2, p2, v2

    if-ltz v2, :cond_0

    .line 202
    const/4 v2, 0x7

    shr-long/2addr p2, v2

    .line 203
    add-int/lit8 v0, v0, -0x1

    long-to-int v2, p2

    and-int/lit8 v2, v2, 0x7f

    or-int/lit16 v2, v2, 0x80

    int-to-byte v2, v2

    aput-byte v2, v1, v0

    goto :goto_0

    .line 205
    :cond_0
    rsub-int/lit8 v2, v0, 0x9

    invoke-virtual {p1, v1, v0, v2}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    .line 206
    return-void
.end method

.method private writeField(Ljava/io/ByteArrayOutputStream;Ljava/math/BigInteger;)V
    .locals 7
    .param p1, "out"    # Ljava/io/ByteArrayOutputStream;
    .param p2, "fieldValue"    # Ljava/math/BigInteger;

    .prologue
    const/4 v6, 0x0

    .line 212
    invoke-virtual {p2}, Ljava/math/BigInteger;->bitLength()I

    move-result v4

    add-int/lit8 v4, v4, 0x6

    div-int/lit8 v0, v4, 0x7

    .line 213
    .local v0, "byteCount":I
    if-nez v0, :cond_0

    .line 215
    invoke-virtual {p1, v6}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 229
    :goto_0
    return-void

    .line 219
    :cond_0
    move-object v3, p2

    .line 220
    .local v3, "tmpValue":Ljava/math/BigInteger;
    new-array v2, v0, [B

    .line 221
    .local v2, "tmp":[B
    add-int/lit8 v1, v0, -0x1

    .local v1, "i":I
    :goto_1
    if-ltz v1, :cond_1

    .line 223
    invoke-virtual {v3}, Ljava/math/BigInteger;->intValue()I

    move-result v4

    and-int/lit8 v4, v4, 0x7f

    or-int/lit16 v4, v4, 0x80

    int-to-byte v4, v4

    aput-byte v4, v2, v1

    .line 224
    const/4 v4, 0x7

    invoke-virtual {v3, v4}, Ljava/math/BigInteger;->shiftRight(I)Ljava/math/BigInteger;

    move-result-object v3

    .line 221
    add-int/lit8 v1, v1, -0x1

    goto :goto_1

    .line 226
    :cond_1
    add-int/lit8 v4, v0, -0x1

    aget-byte v5, v2, v4

    and-int/lit8 v5, v5, 0x7f

    int-to-byte v5, v5

    aput-byte v5, v2, v4

    .line 227
    array-length v4, v2

    invoke-virtual {p1, v2, v6, v4}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    goto :goto_0
.end method


# virtual methods
.method asn1Equals(Lcom/android/sec/org/bouncycastle/asn1/ASN1Primitive;)Z
    .locals 2
    .param p1, "o"    # Lcom/android/sec/org/bouncycastle/asn1/ASN1Primitive;

    .prologue
    .line 306
    instance-of v0, p1, Lcom/android/sec/org/bouncycastle/asn1/DERObjectIdentifier;

    if-nez v0, :cond_0

    .line 308
    const/4 v0, 0x0

    .line 311
    .end local p1    # "o":Lcom/android/sec/org/bouncycastle/asn1/ASN1Primitive;
    :goto_0
    return v0

    .restart local p1    # "o":Lcom/android/sec/org/bouncycastle/asn1/ASN1Primitive;
    :cond_0
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/asn1/DERObjectIdentifier;->identifier:Ljava/lang/String;

    check-cast p1, Lcom/android/sec/org/bouncycastle/asn1/DERObjectIdentifier;

    .end local p1    # "o":Lcom/android/sec/org/bouncycastle/asn1/ASN1Primitive;
    iget-object v1, p1, Lcom/android/sec/org/bouncycastle/asn1/DERObjectIdentifier;->identifier:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method encode(Lcom/android/sec/org/bouncycastle/asn1/ASN1OutputStream;)V
    .locals 2
    .param p1, "out"    # Lcom/android/sec/org/bouncycastle/asn1/ASN1OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 291
    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/asn1/DERObjectIdentifier;->getBody()[B

    move-result-object v0

    .line 293
    .local v0, "enc":[B
    const/4 v1, 0x6

    invoke-virtual {p1, v1}, Lcom/android/sec/org/bouncycastle/asn1/ASN1OutputStream;->write(I)V

    .line 294
    array-length v1, v0

    invoke-virtual {p1, v1}, Lcom/android/sec/org/bouncycastle/asn1/ASN1OutputStream;->writeLength(I)V

    .line 295
    invoke-virtual {p1, v0}, Lcom/android/sec/org/bouncycastle/asn1/ASN1OutputStream;->write([B)V

    .line 296
    return-void
.end method

.method encodedLength()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 282
    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/asn1/DERObjectIdentifier;->getBody()[B

    move-result-object v1

    array-length v0, v1

    .line 284
    .local v0, "length":I
    invoke-static {v0}, Lcom/android/sec/org/bouncycastle/asn1/StreamUtil;->calculateBodyLength(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v1, v0

    return v1
.end method

.method protected declared-synchronized getBody()[B
    .locals 2

    .prologue
    .line 262
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/asn1/DERObjectIdentifier;->body:[B

    if-nez v1, :cond_0

    .line 264
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 266
    .local v0, "bOut":Ljava/io/ByteArrayOutputStream;
    invoke-direct {p0, v0}, Lcom/android/sec/org/bouncycastle/asn1/DERObjectIdentifier;->doOutput(Ljava/io/ByteArrayOutputStream;)V

    .line 268
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    iput-object v1, p0, Lcom/android/sec/org/bouncycastle/asn1/DERObjectIdentifier;->body:[B

    .line 271
    .end local v0    # "bOut":Ljava/io/ByteArrayOutputStream;
    :cond_0
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/asn1/DERObjectIdentifier;->body:[B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v1

    .line 262
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 190
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/asn1/DERObjectIdentifier;->identifier:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 300
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/asn1/DERObjectIdentifier;->identifier:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method isConstructed()Z
    .locals 1

    .prologue
    .line 276
    const/4 v0, 0x0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 316
    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/asn1/DERObjectIdentifier;->getId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
