.class public Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/ec/KeyPairGeneratorSpi$EC;
.super Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/ec/KeyPairGeneratorSpi;
.source "KeyPairGeneratorSpi.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/ec/KeyPairGeneratorSpi;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "EC"
.end annotation


# static fields
.field private static ecParameters:Ljava/util/Hashtable;


# instance fields
.field algorithm:Ljava/lang/String;

.field certainty:I

.field configuration:Lcom/android/sec/org/bouncycastle/jcajce/provider/config/ProviderConfiguration;

.field ecParams:Ljava/lang/Object;

.field engine:Lcom/android/sec/org/bouncycastle/crypto/generators/ECKeyPairGenerator;

.field initialised:Z

.field param:Lcom/android/sec/org/bouncycastle/crypto/params/ECKeyGenerationParameters;

.field random:Ljava/security/SecureRandom;

.field strength:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 60
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    sput-object v0, Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/ec/KeyPairGeneratorSpi$EC;->ecParameters:Ljava/util/Hashtable;

    .line 62
    sget-object v0, Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/ec/KeyPairGeneratorSpi$EC;->ecParameters:Ljava/util/Hashtable;

    const/16 v1, 0xc0

    invoke-static {v1}, Lcom/android/sec/org/bouncycastle/util/Integers;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v2, Ljava/security/spec/ECGenParameterSpec;

    const-string v3, "prime192v1"

    invoke-direct {v2, v3}, Ljava/security/spec/ECGenParameterSpec;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 63
    sget-object v0, Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/ec/KeyPairGeneratorSpi$EC;->ecParameters:Ljava/util/Hashtable;

    const/16 v1, 0xef

    invoke-static {v1}, Lcom/android/sec/org/bouncycastle/util/Integers;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v2, Ljava/security/spec/ECGenParameterSpec;

    const-string v3, "prime239v1"

    invoke-direct {v2, v3}, Ljava/security/spec/ECGenParameterSpec;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 64
    sget-object v0, Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/ec/KeyPairGeneratorSpi$EC;->ecParameters:Ljava/util/Hashtable;

    const/16 v1, 0x100

    invoke-static {v1}, Lcom/android/sec/org/bouncycastle/util/Integers;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v2, Ljava/security/spec/ECGenParameterSpec;

    const-string v3, "prime256v1"

    invoke-direct {v2, v3}, Ljava/security/spec/ECGenParameterSpec;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 66
    sget-object v0, Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/ec/KeyPairGeneratorSpi$EC;->ecParameters:Ljava/util/Hashtable;

    const/16 v1, 0xe0

    invoke-static {v1}, Lcom/android/sec/org/bouncycastle/util/Integers;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v2, Ljava/security/spec/ECGenParameterSpec;

    const-string v3, "P-224"

    invoke-direct {v2, v3}, Ljava/security/spec/ECGenParameterSpec;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 67
    sget-object v0, Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/ec/KeyPairGeneratorSpi$EC;->ecParameters:Ljava/util/Hashtable;

    const/16 v1, 0x180

    invoke-static {v1}, Lcom/android/sec/org/bouncycastle/util/Integers;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v2, Ljava/security/spec/ECGenParameterSpec;

    const-string v3, "P-384"

    invoke-direct {v2, v3}, Ljava/security/spec/ECGenParameterSpec;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 68
    sget-object v0, Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/ec/KeyPairGeneratorSpi$EC;->ecParameters:Ljava/util/Hashtable;

    const/16 v1, 0x209

    invoke-static {v1}, Lcom/android/sec/org/bouncycastle/util/Integers;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v2, Ljava/security/spec/ECGenParameterSpec;

    const-string v3, "P-521"

    invoke-direct {v2, v3}, Ljava/security/spec/ECGenParameterSpec;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 69
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 73
    const-string v0, "EC"

    invoke-direct {p0, v0}, Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/ec/KeyPairGeneratorSpi;-><init>(Ljava/lang/String;)V

    .line 48
    new-instance v0, Lcom/android/sec/org/bouncycastle/crypto/generators/ECKeyPairGenerator;

    invoke-direct {v0}, Lcom/android/sec/org/bouncycastle/crypto/generators/ECKeyPairGenerator;-><init>()V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/ec/KeyPairGeneratorSpi$EC;->engine:Lcom/android/sec/org/bouncycastle/crypto/generators/ECKeyPairGenerator;

    .line 49
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/ec/KeyPairGeneratorSpi$EC;->ecParams:Ljava/lang/Object;

    .line 50
    const/16 v0, 0xef

    iput v0, p0, Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/ec/KeyPairGeneratorSpi$EC;->strength:I

    .line 51
    const/16 v0, 0x32

    iput v0, p0, Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/ec/KeyPairGeneratorSpi$EC;->certainty:I

    .line 52
    new-instance v0, Ljava/security/SecureRandom;

    invoke-direct {v0}, Ljava/security/SecureRandom;-><init>()V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/ec/KeyPairGeneratorSpi$EC;->random:Ljava/security/SecureRandom;

    .line 53
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/ec/KeyPairGeneratorSpi$EC;->initialised:Z

    .line 74
    const-string v0, "EC"

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/ec/KeyPairGeneratorSpi$EC;->algorithm:Ljava/lang/String;

    .line 75
    sget-object v0, Lcom/android/sec/org/bouncycastle/jce/provider/BouncyCastleProvider;->CONFIGURATION:Lcom/android/sec/org/bouncycastle/jcajce/provider/config/ProviderConfiguration;

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/ec/KeyPairGeneratorSpi$EC;->configuration:Lcom/android/sec/org/bouncycastle/jcajce/provider/config/ProviderConfiguration;

    .line 76
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/android/sec/org/bouncycastle/jcajce/provider/config/ProviderConfiguration;)V
    .locals 1
    .param p1, "algorithm"    # Ljava/lang/String;
    .param p2, "configuration"    # Lcom/android/sec/org/bouncycastle/jcajce/provider/config/ProviderConfiguration;

    .prologue
    .line 82
    invoke-direct {p0, p1}, Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/ec/KeyPairGeneratorSpi;-><init>(Ljava/lang/String;)V

    .line 48
    new-instance v0, Lcom/android/sec/org/bouncycastle/crypto/generators/ECKeyPairGenerator;

    invoke-direct {v0}, Lcom/android/sec/org/bouncycastle/crypto/generators/ECKeyPairGenerator;-><init>()V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/ec/KeyPairGeneratorSpi$EC;->engine:Lcom/android/sec/org/bouncycastle/crypto/generators/ECKeyPairGenerator;

    .line 49
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/ec/KeyPairGeneratorSpi$EC;->ecParams:Ljava/lang/Object;

    .line 50
    const/16 v0, 0xef

    iput v0, p0, Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/ec/KeyPairGeneratorSpi$EC;->strength:I

    .line 51
    const/16 v0, 0x32

    iput v0, p0, Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/ec/KeyPairGeneratorSpi$EC;->certainty:I

    .line 52
    new-instance v0, Ljava/security/SecureRandom;

    invoke-direct {v0}, Ljava/security/SecureRandom;-><init>()V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/ec/KeyPairGeneratorSpi$EC;->random:Ljava/security/SecureRandom;

    .line 53
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/ec/KeyPairGeneratorSpi$EC;->initialised:Z

    .line 83
    iput-object p1, p0, Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/ec/KeyPairGeneratorSpi$EC;->algorithm:Ljava/lang/String;

    .line 84
    iput-object p2, p0, Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/ec/KeyPairGeneratorSpi$EC;->configuration:Lcom/android/sec/org/bouncycastle/jcajce/provider/config/ProviderConfiguration;

    .line 85
    return-void
.end method


# virtual methods
.method public generateKeyPair()Ljava/security/KeyPair;
    .locals 10

    .prologue
    .line 251
    iget-boolean v0, p0, Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/ec/KeyPairGeneratorSpi$EC;->initialised:Z

    if-nez v0, :cond_0

    .line 253
    iget v0, p0, Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/ec/KeyPairGeneratorSpi$EC;->strength:I

    new-instance v1, Ljava/security/SecureRandom;

    invoke-direct {v1}, Ljava/security/SecureRandom;-><init>()V

    invoke-virtual {p0, v0, v1}, Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/ec/KeyPairGeneratorSpi$EC;->initialize(ILjava/security/SecureRandom;)V

    .line 256
    :cond_0
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/ec/KeyPairGeneratorSpi$EC;->engine:Lcom/android/sec/org/bouncycastle/crypto/generators/ECKeyPairGenerator;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/crypto/generators/ECKeyPairGenerator;->generateKeyPair()Lcom/android/sec/org/bouncycastle/crypto/AsymmetricCipherKeyPair;

    move-result-object v6

    .line 257
    .local v6, "pair":Lcom/android/sec/org/bouncycastle/crypto/AsymmetricCipherKeyPair;
    invoke-virtual {v6}, Lcom/android/sec/org/bouncycastle/crypto/AsymmetricCipherKeyPair;->getPublic()Lcom/android/sec/org/bouncycastle/crypto/params/AsymmetricKeyParameter;

    move-result-object v7

    check-cast v7, Lcom/android/sec/org/bouncycastle/crypto/params/ECPublicKeyParameters;

    .line 258
    .local v7, "pub":Lcom/android/sec/org/bouncycastle/crypto/params/ECPublicKeyParameters;
    invoke-virtual {v6}, Lcom/android/sec/org/bouncycastle/crypto/AsymmetricCipherKeyPair;->getPrivate()Lcom/android/sec/org/bouncycastle/crypto/params/AsymmetricKeyParameter;

    move-result-object v2

    check-cast v2, Lcom/android/sec/org/bouncycastle/crypto/params/ECPrivateKeyParameters;

    .line 260
    .local v2, "priv":Lcom/android/sec/org/bouncycastle/crypto/params/ECPrivateKeyParameters;
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/ec/KeyPairGeneratorSpi$EC;->ecParams:Ljava/lang/Object;

    instance-of v0, v0, Lcom/android/sec/org/bouncycastle/jce/spec/ECParameterSpec;

    if-eqz v0, :cond_1

    .line 262
    iget-object v4, p0, Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/ec/KeyPairGeneratorSpi$EC;->ecParams:Ljava/lang/Object;

    check-cast v4, Lcom/android/sec/org/bouncycastle/jce/spec/ECParameterSpec;

    .line 264
    .local v4, "p":Lcom/android/sec/org/bouncycastle/jce/spec/ECParameterSpec;
    new-instance v3, Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/ec/BCECPublicKey;

    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/ec/KeyPairGeneratorSpi$EC;->algorithm:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/ec/KeyPairGeneratorSpi$EC;->configuration:Lcom/android/sec/org/bouncycastle/jcajce/provider/config/ProviderConfiguration;

    invoke-direct {v3, v0, v7, v4, v1}, Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/ec/BCECPublicKey;-><init>(Ljava/lang/String;Lcom/android/sec/org/bouncycastle/crypto/params/ECPublicKeyParameters;Lcom/android/sec/org/bouncycastle/jce/spec/ECParameterSpec;Lcom/android/sec/org/bouncycastle/jcajce/provider/config/ProviderConfiguration;)V

    .line 265
    .local v3, "pubKey":Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/ec/BCECPublicKey;
    new-instance v8, Ljava/security/KeyPair;

    new-instance v0, Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/ec/BCECPrivateKey;

    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/ec/KeyPairGeneratorSpi$EC;->algorithm:Ljava/lang/String;

    iget-object v5, p0, Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/ec/KeyPairGeneratorSpi$EC;->configuration:Lcom/android/sec/org/bouncycastle/jcajce/provider/config/ProviderConfiguration;

    invoke-direct/range {v0 .. v5}, Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/ec/BCECPrivateKey;-><init>(Ljava/lang/String;Lcom/android/sec/org/bouncycastle/crypto/params/ECPrivateKeyParameters;Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/ec/BCECPublicKey;Lcom/android/sec/org/bouncycastle/jce/spec/ECParameterSpec;Lcom/android/sec/org/bouncycastle/jcajce/provider/config/ProviderConfiguration;)V

    invoke-direct {v8, v3, v0}, Ljava/security/KeyPair;-><init>(Ljava/security/PublicKey;Ljava/security/PrivateKey;)V

    move-object v0, v8

    .line 279
    .end local v3    # "pubKey":Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/ec/BCECPublicKey;
    .end local v4    # "p":Lcom/android/sec/org/bouncycastle/jce/spec/ECParameterSpec;
    :goto_0
    return-object v0

    .line 268
    :cond_1
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/ec/KeyPairGeneratorSpi$EC;->ecParams:Ljava/lang/Object;

    if-nez v0, :cond_2

    .line 270
    new-instance v0, Ljava/security/KeyPair;

    new-instance v1, Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/ec/BCECPublicKey;

    iget-object v5, p0, Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/ec/KeyPairGeneratorSpi$EC;->algorithm:Ljava/lang/String;

    iget-object v8, p0, Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/ec/KeyPairGeneratorSpi$EC;->configuration:Lcom/android/sec/org/bouncycastle/jcajce/provider/config/ProviderConfiguration;

    invoke-direct {v1, v5, v7, v8}, Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/ec/BCECPublicKey;-><init>(Ljava/lang/String;Lcom/android/sec/org/bouncycastle/crypto/params/ECPublicKeyParameters;Lcom/android/sec/org/bouncycastle/jcajce/provider/config/ProviderConfiguration;)V

    new-instance v5, Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/ec/BCECPrivateKey;

    iget-object v8, p0, Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/ec/KeyPairGeneratorSpi$EC;->algorithm:Ljava/lang/String;

    iget-object v9, p0, Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/ec/KeyPairGeneratorSpi$EC;->configuration:Lcom/android/sec/org/bouncycastle/jcajce/provider/config/ProviderConfiguration;

    invoke-direct {v5, v8, v2, v9}, Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/ec/BCECPrivateKey;-><init>(Ljava/lang/String;Lcom/android/sec/org/bouncycastle/crypto/params/ECPrivateKeyParameters;Lcom/android/sec/org/bouncycastle/jcajce/provider/config/ProviderConfiguration;)V

    invoke-direct {v0, v1, v5}, Ljava/security/KeyPair;-><init>(Ljava/security/PublicKey;Ljava/security/PrivateKey;)V

    goto :goto_0

    .line 275
    :cond_2
    iget-object v4, p0, Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/ec/KeyPairGeneratorSpi$EC;->ecParams:Ljava/lang/Object;

    check-cast v4, Ljava/security/spec/ECParameterSpec;

    .line 277
    .local v4, "p":Ljava/security/spec/ECParameterSpec;
    new-instance v3, Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/ec/BCECPublicKey;

    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/ec/KeyPairGeneratorSpi$EC;->algorithm:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/ec/KeyPairGeneratorSpi$EC;->configuration:Lcom/android/sec/org/bouncycastle/jcajce/provider/config/ProviderConfiguration;

    invoke-direct {v3, v0, v7, v4, v1}, Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/ec/BCECPublicKey;-><init>(Ljava/lang/String;Lcom/android/sec/org/bouncycastle/crypto/params/ECPublicKeyParameters;Ljava/security/spec/ECParameterSpec;Lcom/android/sec/org/bouncycastle/jcajce/provider/config/ProviderConfiguration;)V

    .line 279
    .restart local v3    # "pubKey":Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/ec/BCECPublicKey;
    new-instance v8, Ljava/security/KeyPair;

    new-instance v0, Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/ec/BCECPrivateKey;

    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/ec/KeyPairGeneratorSpi$EC;->algorithm:Ljava/lang/String;

    iget-object v5, p0, Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/ec/KeyPairGeneratorSpi$EC;->configuration:Lcom/android/sec/org/bouncycastle/jcajce/provider/config/ProviderConfiguration;

    invoke-direct/range {v0 .. v5}, Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/ec/BCECPrivateKey;-><init>(Ljava/lang/String;Lcom/android/sec/org/bouncycastle/crypto/params/ECPrivateKeyParameters;Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/ec/BCECPublicKey;Ljava/security/spec/ECParameterSpec;Lcom/android/sec/org/bouncycastle/jcajce/provider/config/ProviderConfiguration;)V

    invoke-direct {v8, v3, v0}, Ljava/security/KeyPair;-><init>(Ljava/security/PublicKey;Ljava/security/PrivateKey;)V

    move-object v0, v8

    goto :goto_0
.end method

.method public initialize(ILjava/security/SecureRandom;)V
    .locals 4
    .param p1, "strength"    # I
    .param p2, "random"    # Ljava/security/SecureRandom;

    .prologue
    .line 91
    iput p1, p0, Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/ec/KeyPairGeneratorSpi$EC;->strength:I

    .line 93
    if-eqz p2, :cond_0

    .line 95
    iput-object p2, p0, Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/ec/KeyPairGeneratorSpi$EC;->random:Ljava/security/SecureRandom;

    .line 99
    :cond_0
    sget-object v2, Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/ec/KeyPairGeneratorSpi$EC;->ecParameters:Ljava/util/Hashtable;

    invoke-static {p1}, Lcom/android/sec/org/bouncycastle/util/Integers;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/security/spec/ECGenParameterSpec;

    .line 101
    .local v1, "ecParams":Ljava/security/spec/ECGenParameterSpec;
    if-eqz v1, :cond_1

    .line 105
    :try_start_0
    invoke-virtual {p0, v1, p2}, Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/ec/KeyPairGeneratorSpi$EC;->initialize(Ljava/security/spec/AlgorithmParameterSpec;Ljava/security/SecureRandom;)V
    :try_end_0
    .catch Ljava/security/InvalidAlgorithmParameterException; {:try_start_0 .. :try_end_0} :catch_0

    .line 116
    return-void

    .line 107
    :catch_0
    move-exception v0

    .line 109
    .local v0, "e":Ljava/security/InvalidAlgorithmParameterException;
    new-instance v2, Ljava/security/InvalidParameterException;

    const-string v3, "key size not configurable."

    invoke-direct {v2, v3}, Ljava/security/InvalidParameterException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 114
    .end local v0    # "e":Ljava/security/InvalidAlgorithmParameterException;
    :cond_1
    new-instance v2, Ljava/security/InvalidParameterException;

    const-string v3, "unknown key size."

    invoke-direct {v2, v3}, Ljava/security/InvalidParameterException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public initialize(Ljava/security/spec/AlgorithmParameterSpec;Ljava/security/SecureRandom;)V
    .locals 13
    .param p1, "params"    # Ljava/security/spec/AlgorithmParameterSpec;
    .param p2, "random"    # Ljava/security/SecureRandom;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/InvalidAlgorithmParameterException;
        }
    .end annotation

    .prologue
    .line 124
    if-nez p2, :cond_0

    .line 125
    iget-object p2, p0, Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/ec/KeyPairGeneratorSpi$EC;->random:Ljava/security/SecureRandom;

    .line 128
    :cond_0
    instance-of v0, p1, Lcom/android/sec/org/bouncycastle/jce/spec/ECParameterSpec;

    if-eqz v0, :cond_1

    move-object v12, p1

    .line 130
    check-cast v12, Lcom/android/sec/org/bouncycastle/jce/spec/ECParameterSpec;

    .line 131
    .local v12, "p":Lcom/android/sec/org/bouncycastle/jce/spec/ECParameterSpec;
    iput-object p1, p0, Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/ec/KeyPairGeneratorSpi$EC;->ecParams:Ljava/lang/Object;

    .line 133
    new-instance v0, Lcom/android/sec/org/bouncycastle/crypto/params/ECKeyGenerationParameters;

    new-instance v2, Lcom/android/sec/org/bouncycastle/crypto/params/ECDomainParameters;

    invoke-virtual {v12}, Lcom/android/sec/org/bouncycastle/jce/spec/ECParameterSpec;->getCurve()Lcom/android/sec/org/bouncycastle/math/ec/ECCurve;

    move-result-object v3

    invoke-virtual {v12}, Lcom/android/sec/org/bouncycastle/jce/spec/ECParameterSpec;->getG()Lcom/android/sec/org/bouncycastle/math/ec/ECPoint;

    move-result-object v4

    invoke-virtual {v12}, Lcom/android/sec/org/bouncycastle/jce/spec/ECParameterSpec;->getN()Ljava/math/BigInteger;

    move-result-object v5

    invoke-direct {v2, v3, v4, v5}, Lcom/android/sec/org/bouncycastle/crypto/params/ECDomainParameters;-><init>(Lcom/android/sec/org/bouncycastle/math/ec/ECCurve;Lcom/android/sec/org/bouncycastle/math/ec/ECPoint;Ljava/math/BigInteger;)V

    invoke-direct {v0, v2, p2}, Lcom/android/sec/org/bouncycastle/crypto/params/ECKeyGenerationParameters;-><init>(Lcom/android/sec/org/bouncycastle/crypto/params/ECDomainParameters;Ljava/security/SecureRandom;)V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/ec/KeyPairGeneratorSpi$EC;->param:Lcom/android/sec/org/bouncycastle/crypto/params/ECKeyGenerationParameters;

    .line 135
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/ec/KeyPairGeneratorSpi$EC;->engine:Lcom/android/sec/org/bouncycastle/crypto/generators/ECKeyPairGenerator;

    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/ec/KeyPairGeneratorSpi$EC;->param:Lcom/android/sec/org/bouncycastle/crypto/params/ECKeyGenerationParameters;

    invoke-virtual {v0, v2}, Lcom/android/sec/org/bouncycastle/crypto/generators/ECKeyPairGenerator;->init(Lcom/android/sec/org/bouncycastle/crypto/KeyGenerationParameters;)V

    .line 136
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/ec/KeyPairGeneratorSpi$EC;->initialised:Z

    .line 247
    .end local v12    # "p":Lcom/android/sec/org/bouncycastle/jce/spec/ECParameterSpec;
    .end local p1    # "params":Ljava/security/spec/AlgorithmParameterSpec;
    :goto_0
    return-void

    .line 138
    .restart local p1    # "params":Ljava/security/spec/AlgorithmParameterSpec;
    :cond_1
    instance-of v0, p1, Ljava/security/spec/ECParameterSpec;

    if-eqz v0, :cond_2

    move-object v12, p1

    .line 140
    check-cast v12, Ljava/security/spec/ECParameterSpec;

    .line 141
    .local v12, "p":Ljava/security/spec/ECParameterSpec;
    iput-object p1, p0, Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/ec/KeyPairGeneratorSpi$EC;->ecParams:Ljava/lang/Object;

    .line 143
    invoke-virtual {v12}, Ljava/security/spec/ECParameterSpec;->getCurve()Ljava/security/spec/EllipticCurve;

    move-result-object v0

    invoke-static {v0}, Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/util/EC5Util;->convertCurve(Ljava/security/spec/EllipticCurve;)Lcom/android/sec/org/bouncycastle/math/ec/ECCurve;

    move-result-object v7

    .line 144
    .local v7, "curve":Lcom/android/sec/org/bouncycastle/math/ec/ECCurve;
    invoke-virtual {v12}, Ljava/security/spec/ECParameterSpec;->getGenerator()Ljava/security/spec/ECPoint;

    move-result-object v0

    const/4 v2, 0x0

    invoke-static {v7, v0, v2}, Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/util/EC5Util;->convertPoint(Lcom/android/sec/org/bouncycastle/math/ec/ECCurve;Ljava/security/spec/ECPoint;Z)Lcom/android/sec/org/bouncycastle/math/ec/ECPoint;

    move-result-object v10

    .line 146
    .local v10, "g":Lcom/android/sec/org/bouncycastle/math/ec/ECPoint;
    new-instance v0, Lcom/android/sec/org/bouncycastle/crypto/params/ECKeyGenerationParameters;

    new-instance v2, Lcom/android/sec/org/bouncycastle/crypto/params/ECDomainParameters;

    invoke-virtual {v12}, Ljava/security/spec/ECParameterSpec;->getOrder()Ljava/math/BigInteger;

    move-result-object v3

    invoke-virtual {v12}, Ljava/security/spec/ECParameterSpec;->getCofactor()I

    move-result v4

    int-to-long v4, v4

    invoke-static {v4, v5}, Ljava/math/BigInteger;->valueOf(J)Ljava/math/BigInteger;

    move-result-object v4

    invoke-direct {v2, v7, v10, v3, v4}, Lcom/android/sec/org/bouncycastle/crypto/params/ECDomainParameters;-><init>(Lcom/android/sec/org/bouncycastle/math/ec/ECCurve;Lcom/android/sec/org/bouncycastle/math/ec/ECPoint;Ljava/math/BigInteger;Ljava/math/BigInteger;)V

    invoke-direct {v0, v2, p2}, Lcom/android/sec/org/bouncycastle/crypto/params/ECKeyGenerationParameters;-><init>(Lcom/android/sec/org/bouncycastle/crypto/params/ECDomainParameters;Ljava/security/SecureRandom;)V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/ec/KeyPairGeneratorSpi$EC;->param:Lcom/android/sec/org/bouncycastle/crypto/params/ECKeyGenerationParameters;

    .line 148
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/ec/KeyPairGeneratorSpi$EC;->engine:Lcom/android/sec/org/bouncycastle/crypto/generators/ECKeyPairGenerator;

    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/ec/KeyPairGeneratorSpi$EC;->param:Lcom/android/sec/org/bouncycastle/crypto/params/ECKeyGenerationParameters;

    invoke-virtual {v0, v2}, Lcom/android/sec/org/bouncycastle/crypto/generators/ECKeyPairGenerator;->init(Lcom/android/sec/org/bouncycastle/crypto/KeyGenerationParameters;)V

    .line 149
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/ec/KeyPairGeneratorSpi$EC;->initialised:Z

    goto :goto_0

    .line 151
    .end local v7    # "curve":Lcom/android/sec/org/bouncycastle/math/ec/ECCurve;
    .end local v10    # "g":Lcom/android/sec/org/bouncycastle/math/ec/ECPoint;
    .end local v12    # "p":Ljava/security/spec/ECParameterSpec;
    :cond_2
    instance-of v0, p1, Ljava/security/spec/ECGenParameterSpec;

    if-nez v0, :cond_3

    instance-of v0, p1, Lcom/android/sec/org/bouncycastle/jce/spec/ECNamedCurveGenParameterSpec;

    if-eqz v0, :cond_9

    .line 155
    :cond_3
    instance-of v0, p1, Ljava/security/spec/ECGenParameterSpec;

    if-eqz v0, :cond_7

    .line 157
    check-cast p1, Ljava/security/spec/ECGenParameterSpec;

    .end local p1    # "params":Ljava/security/spec/AlgorithmParameterSpec;
    invoke-virtual {p1}, Ljava/security/spec/ECGenParameterSpec;->getName()Ljava/lang/String;

    move-result-object v1

    .line 164
    .local v1, "curveName":Ljava/lang/String;
    :goto_1
    invoke-static {v1}, Lcom/android/sec/org/bouncycastle/asn1/x9/X962NamedCurves;->getByName(Ljava/lang/String;)Lcom/android/sec/org/bouncycastle/asn1/x9/X9ECParameters;

    move-result-object v8

    .line 165
    .local v8, "ecP":Lcom/android/sec/org/bouncycastle/asn1/x9/X9ECParameters;
    if-nez v8, :cond_8

    .line 167
    invoke-static {v1}, Lcom/android/sec/org/bouncycastle/asn1/sec/SECNamedCurves;->getByName(Ljava/lang/String;)Lcom/android/sec/org/bouncycastle/asn1/x9/X9ECParameters;

    move-result-object v8

    .line 168
    if-nez v8, :cond_4

    .line 170
    invoke-static {v1}, Lcom/android/sec/org/bouncycastle/asn1/nist/NISTNamedCurves;->getByName(Ljava/lang/String;)Lcom/android/sec/org/bouncycastle/asn1/x9/X9ECParameters;

    move-result-object v8

    .line 178
    :cond_4
    if-nez v8, :cond_8

    .line 183
    :try_start_0
    new-instance v11, Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    invoke-direct {v11, v1}, Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;-><init>(Ljava/lang/String;)V

    .line 184
    .local v11, "oid":Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;
    invoke-static {v11}, Lcom/android/sec/org/bouncycastle/asn1/x9/X962NamedCurves;->getByOID(Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;)Lcom/android/sec/org/bouncycastle/asn1/x9/X9ECParameters;

    move-result-object v8

    .line 185
    if-nez v8, :cond_5

    .line 187
    invoke-static {v11}, Lcom/android/sec/org/bouncycastle/asn1/sec/SECNamedCurves;->getByOID(Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;)Lcom/android/sec/org/bouncycastle/asn1/x9/X9ECParameters;

    move-result-object v8

    .line 189
    :cond_5
    if-nez v8, :cond_6

    .line 191
    invoke-static {v11}, Lcom/android/sec/org/bouncycastle/asn1/nist/NISTNamedCurves;->getByOID(Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;)Lcom/android/sec/org/bouncycastle/asn1/x9/X9ECParameters;

    move-result-object v8

    .line 199
    :cond_6
    if-nez v8, :cond_8

    .line 201
    new-instance v0, Ljava/security/InvalidAlgorithmParameterException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "unknown curve OID: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/security/InvalidAlgorithmParameterException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 204
    .end local v11    # "oid":Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;
    :catch_0
    move-exception v9

    .line 206
    .local v9, "ex":Ljava/lang/IllegalArgumentException;
    new-instance v0, Ljava/security/InvalidAlgorithmParameterException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "unknown curve name: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/security/InvalidAlgorithmParameterException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 161
    .end local v1    # "curveName":Ljava/lang/String;
    .end local v8    # "ecP":Lcom/android/sec/org/bouncycastle/asn1/x9/X9ECParameters;
    .end local v9    # "ex":Ljava/lang/IllegalArgumentException;
    .restart local p1    # "params":Ljava/security/spec/AlgorithmParameterSpec;
    :cond_7
    check-cast p1, Lcom/android/sec/org/bouncycastle/jce/spec/ECNamedCurveGenParameterSpec;

    .end local p1    # "params":Ljava/security/spec/AlgorithmParameterSpec;
    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/jce/spec/ECNamedCurveGenParameterSpec;->getName()Ljava/lang/String;

    move-result-object v1

    .restart local v1    # "curveName":Ljava/lang/String;
    goto :goto_1

    .line 211
    .restart local v8    # "ecP":Lcom/android/sec/org/bouncycastle/asn1/x9/X9ECParameters;
    :cond_8
    new-instance v0, Lcom/android/sec/org/bouncycastle/jce/spec/ECNamedCurveSpec;

    invoke-virtual {v8}, Lcom/android/sec/org/bouncycastle/asn1/x9/X9ECParameters;->getCurve()Lcom/android/sec/org/bouncycastle/math/ec/ECCurve;

    move-result-object v2

    invoke-virtual {v8}, Lcom/android/sec/org/bouncycastle/asn1/x9/X9ECParameters;->getG()Lcom/android/sec/org/bouncycastle/math/ec/ECPoint;

    move-result-object v3

    invoke-virtual {v8}, Lcom/android/sec/org/bouncycastle/asn1/x9/X9ECParameters;->getN()Ljava/math/BigInteger;

    move-result-object v4

    invoke-virtual {v8}, Lcom/android/sec/org/bouncycastle/asn1/x9/X9ECParameters;->getH()Ljava/math/BigInteger;

    move-result-object v5

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v6}, Lcom/android/sec/org/bouncycastle/jce/spec/ECNamedCurveSpec;-><init>(Ljava/lang/String;Lcom/android/sec/org/bouncycastle/math/ec/ECCurve;Lcom/android/sec/org/bouncycastle/math/ec/ECPoint;Ljava/math/BigInteger;Ljava/math/BigInteger;[B)V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/ec/KeyPairGeneratorSpi$EC;->ecParams:Ljava/lang/Object;

    .line 219
    iget-object v12, p0, Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/ec/KeyPairGeneratorSpi$EC;->ecParams:Ljava/lang/Object;

    check-cast v12, Ljava/security/spec/ECParameterSpec;

    .line 221
    .restart local v12    # "p":Ljava/security/spec/ECParameterSpec;
    invoke-virtual {v12}, Ljava/security/spec/ECParameterSpec;->getCurve()Ljava/security/spec/EllipticCurve;

    move-result-object v0

    invoke-static {v0}, Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/util/EC5Util;->convertCurve(Ljava/security/spec/EllipticCurve;)Lcom/android/sec/org/bouncycastle/math/ec/ECCurve;

    move-result-object v7

    .line 222
    .restart local v7    # "curve":Lcom/android/sec/org/bouncycastle/math/ec/ECCurve;
    invoke-virtual {v12}, Ljava/security/spec/ECParameterSpec;->getGenerator()Ljava/security/spec/ECPoint;

    move-result-object v0

    const/4 v2, 0x0

    invoke-static {v7, v0, v2}, Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/util/EC5Util;->convertPoint(Lcom/android/sec/org/bouncycastle/math/ec/ECCurve;Ljava/security/spec/ECPoint;Z)Lcom/android/sec/org/bouncycastle/math/ec/ECPoint;

    move-result-object v10

    .line 224
    .restart local v10    # "g":Lcom/android/sec/org/bouncycastle/math/ec/ECPoint;
    new-instance v0, Lcom/android/sec/org/bouncycastle/crypto/params/ECKeyGenerationParameters;

    new-instance v2, Lcom/android/sec/org/bouncycastle/crypto/params/ECDomainParameters;

    invoke-virtual {v12}, Ljava/security/spec/ECParameterSpec;->getOrder()Ljava/math/BigInteger;

    move-result-object v3

    invoke-virtual {v12}, Ljava/security/spec/ECParameterSpec;->getCofactor()I

    move-result v4

    int-to-long v4, v4

    invoke-static {v4, v5}, Ljava/math/BigInteger;->valueOf(J)Ljava/math/BigInteger;

    move-result-object v4

    invoke-direct {v2, v7, v10, v3, v4}, Lcom/android/sec/org/bouncycastle/crypto/params/ECDomainParameters;-><init>(Lcom/android/sec/org/bouncycastle/math/ec/ECCurve;Lcom/android/sec/org/bouncycastle/math/ec/ECPoint;Ljava/math/BigInteger;Ljava/math/BigInteger;)V

    invoke-direct {v0, v2, p2}, Lcom/android/sec/org/bouncycastle/crypto/params/ECKeyGenerationParameters;-><init>(Lcom/android/sec/org/bouncycastle/crypto/params/ECDomainParameters;Ljava/security/SecureRandom;)V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/ec/KeyPairGeneratorSpi$EC;->param:Lcom/android/sec/org/bouncycastle/crypto/params/ECKeyGenerationParameters;

    .line 226
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/ec/KeyPairGeneratorSpi$EC;->engine:Lcom/android/sec/org/bouncycastle/crypto/generators/ECKeyPairGenerator;

    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/ec/KeyPairGeneratorSpi$EC;->param:Lcom/android/sec/org/bouncycastle/crypto/params/ECKeyGenerationParameters;

    invoke-virtual {v0, v2}, Lcom/android/sec/org/bouncycastle/crypto/generators/ECKeyPairGenerator;->init(Lcom/android/sec/org/bouncycastle/crypto/KeyGenerationParameters;)V

    .line 227
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/ec/KeyPairGeneratorSpi$EC;->initialised:Z

    goto/16 :goto_0

    .line 229
    .end local v1    # "curveName":Ljava/lang/String;
    .end local v7    # "curve":Lcom/android/sec/org/bouncycastle/math/ec/ECCurve;
    .end local v8    # "ecP":Lcom/android/sec/org/bouncycastle/asn1/x9/X9ECParameters;
    .end local v10    # "g":Lcom/android/sec/org/bouncycastle/math/ec/ECPoint;
    .end local v12    # "p":Ljava/security/spec/ECParameterSpec;
    .restart local p1    # "params":Ljava/security/spec/AlgorithmParameterSpec;
    :cond_9
    if-nez p1, :cond_a

    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/ec/KeyPairGeneratorSpi$EC;->configuration:Lcom/android/sec/org/bouncycastle/jcajce/provider/config/ProviderConfiguration;

    invoke-interface {v0}, Lcom/android/sec/org/bouncycastle/jcajce/provider/config/ProviderConfiguration;->getEcImplicitlyCa()Lcom/android/sec/org/bouncycastle/jce/spec/ECParameterSpec;

    move-result-object v0

    if-eqz v0, :cond_a

    .line 231
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/ec/KeyPairGeneratorSpi$EC;->configuration:Lcom/android/sec/org/bouncycastle/jcajce/provider/config/ProviderConfiguration;

    invoke-interface {v0}, Lcom/android/sec/org/bouncycastle/jcajce/provider/config/ProviderConfiguration;->getEcImplicitlyCa()Lcom/android/sec/org/bouncycastle/jce/spec/ECParameterSpec;

    move-result-object v12

    .line 232
    .local v12, "p":Lcom/android/sec/org/bouncycastle/jce/spec/ECParameterSpec;
    iput-object p1, p0, Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/ec/KeyPairGeneratorSpi$EC;->ecParams:Ljava/lang/Object;

    .line 234
    new-instance v0, Lcom/android/sec/org/bouncycastle/crypto/params/ECKeyGenerationParameters;

    new-instance v2, Lcom/android/sec/org/bouncycastle/crypto/params/ECDomainParameters;

    invoke-virtual {v12}, Lcom/android/sec/org/bouncycastle/jce/spec/ECParameterSpec;->getCurve()Lcom/android/sec/org/bouncycastle/math/ec/ECCurve;

    move-result-object v3

    invoke-virtual {v12}, Lcom/android/sec/org/bouncycastle/jce/spec/ECParameterSpec;->getG()Lcom/android/sec/org/bouncycastle/math/ec/ECPoint;

    move-result-object v4

    invoke-virtual {v12}, Lcom/android/sec/org/bouncycastle/jce/spec/ECParameterSpec;->getN()Ljava/math/BigInteger;

    move-result-object v5

    invoke-direct {v2, v3, v4, v5}, Lcom/android/sec/org/bouncycastle/crypto/params/ECDomainParameters;-><init>(Lcom/android/sec/org/bouncycastle/math/ec/ECCurve;Lcom/android/sec/org/bouncycastle/math/ec/ECPoint;Ljava/math/BigInteger;)V

    invoke-direct {v0, v2, p2}, Lcom/android/sec/org/bouncycastle/crypto/params/ECKeyGenerationParameters;-><init>(Lcom/android/sec/org/bouncycastle/crypto/params/ECDomainParameters;Ljava/security/SecureRandom;)V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/ec/KeyPairGeneratorSpi$EC;->param:Lcom/android/sec/org/bouncycastle/crypto/params/ECKeyGenerationParameters;

    .line 236
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/ec/KeyPairGeneratorSpi$EC;->engine:Lcom/android/sec/org/bouncycastle/crypto/generators/ECKeyPairGenerator;

    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/ec/KeyPairGeneratorSpi$EC;->param:Lcom/android/sec/org/bouncycastle/crypto/params/ECKeyGenerationParameters;

    invoke-virtual {v0, v2}, Lcom/android/sec/org/bouncycastle/crypto/generators/ECKeyPairGenerator;->init(Lcom/android/sec/org/bouncycastle/crypto/KeyGenerationParameters;)V

    .line 237
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/ec/KeyPairGeneratorSpi$EC;->initialised:Z

    goto/16 :goto_0

    .line 239
    .end local v12    # "p":Lcom/android/sec/org/bouncycastle/jce/spec/ECParameterSpec;
    :cond_a
    if-nez p1, :cond_b

    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/ec/KeyPairGeneratorSpi$EC;->configuration:Lcom/android/sec/org/bouncycastle/jcajce/provider/config/ProviderConfiguration;

    invoke-interface {v0}, Lcom/android/sec/org/bouncycastle/jcajce/provider/config/ProviderConfiguration;->getEcImplicitlyCa()Lcom/android/sec/org/bouncycastle/jce/spec/ECParameterSpec;

    move-result-object v0

    if-nez v0, :cond_b

    .line 241
    new-instance v0, Ljava/security/InvalidAlgorithmParameterException;

    const-string v2, "null parameter passed but no implicitCA set"

    invoke-direct {v0, v2}, Ljava/security/InvalidAlgorithmParameterException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 245
    :cond_b
    new-instance v0, Ljava/security/InvalidAlgorithmParameterException;

    const-string v2, "parameter object not a ECParameterSpec"

    invoke-direct {v0, v2}, Ljava/security/InvalidAlgorithmParameterException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
