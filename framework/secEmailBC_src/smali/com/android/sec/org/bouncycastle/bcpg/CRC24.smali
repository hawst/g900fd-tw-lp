.class public Lcom/android/sec/org/bouncycastle/bcpg/CRC24;
.super Ljava/lang/Object;
.source "CRC24.java"


# static fields
.field private static final CRC24_INIT:I = 0xb704ce

.field private static final CRC24_POLY:I = 0x1864cfb


# instance fields
.field private crc:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 8
    const v0, 0xb704ce

    iput v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/CRC24;->crc:I

    .line 12
    return-void
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 30
    iget v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/CRC24;->crc:I

    return v0
.end method

.method public reset()V
    .locals 1

    .prologue
    .line 35
    const v0, 0xb704ce

    iput v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/CRC24;->crc:I

    .line 36
    return-void
.end method

.method public update(I)V
    .locals 3
    .param p1, "b"    # I

    .prologue
    .line 17
    iget v1, p0, Lcom/android/sec/org/bouncycastle/bcpg/CRC24;->crc:I

    shl-int/lit8 v2, p1, 0x10

    xor-int/2addr v1, v2

    iput v1, p0, Lcom/android/sec/org/bouncycastle/bcpg/CRC24;->crc:I

    .line 18
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/16 v1, 0x8

    if-ge v0, v1, :cond_1

    .line 20
    iget v1, p0, Lcom/android/sec/org/bouncycastle/bcpg/CRC24;->crc:I

    shl-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/android/sec/org/bouncycastle/bcpg/CRC24;->crc:I

    .line 21
    iget v1, p0, Lcom/android/sec/org/bouncycastle/bcpg/CRC24;->crc:I

    const/high16 v2, 0x1000000

    and-int/2addr v1, v2

    if-eqz v1, :cond_0

    .line 23
    iget v1, p0, Lcom/android/sec/org/bouncycastle/bcpg/CRC24;->crc:I

    const v2, 0x1864cfb

    xor-int/2addr v1, v2

    iput v1, p0, Lcom/android/sec/org/bouncycastle/bcpg/CRC24;->crc:I

    .line 18
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 26
    :cond_1
    return-void
.end method
