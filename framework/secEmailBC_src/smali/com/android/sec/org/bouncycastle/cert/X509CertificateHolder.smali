.class public Lcom/android/sec/org/bouncycastle/cert/X509CertificateHolder;
.super Ljava/lang/Object;
.source "X509CertificateHolder.java"


# instance fields
.field private extensions:Lcom/android/sec/org/bouncycastle/asn1/x509/Extensions;

.field private x509Certificate:Lcom/android/sec/org/bouncycastle/asn1/x509/Certificate;


# direct methods
.method public constructor <init>(Lcom/android/sec/org/bouncycastle/asn1/x509/Certificate;)V
    .locals 1
    .param p1, "x509Certificate"    # Lcom/android/sec/org/bouncycastle/asn1/x509/Certificate;

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    iput-object p1, p0, Lcom/android/sec/org/bouncycastle/cert/X509CertificateHolder;->x509Certificate:Lcom/android/sec/org/bouncycastle/asn1/x509/Certificate;

    .line 68
    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/asn1/x509/Certificate;->getTBSCertificate()Lcom/android/sec/org/bouncycastle/asn1/x509/TBSCertificate;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/x509/TBSCertificate;->getExtensions()Lcom/android/sec/org/bouncycastle/asn1/x509/Extensions;

    move-result-object v0

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/cert/X509CertificateHolder;->extensions:Lcom/android/sec/org/bouncycastle/asn1/x509/Extensions;

    .line 69
    return-void
.end method

.method public constructor <init>([B)V
    .locals 1
    .param p1, "certEncoding"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 57
    invoke-static {p1}, Lcom/android/sec/org/bouncycastle/cert/X509CertificateHolder;->parseBytes([B)Lcom/android/sec/org/bouncycastle/asn1/x509/Certificate;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/sec/org/bouncycastle/cert/X509CertificateHolder;-><init>(Lcom/android/sec/org/bouncycastle/asn1/x509/Certificate;)V

    .line 58
    return-void
.end method

.method private static parseBytes([B)Lcom/android/sec/org/bouncycastle/asn1/x509/Certificate;
    .locals 4
    .param p0, "certEncoding"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 36
    :try_start_0
    invoke-static {p0}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Primitive;->fromByteArray([B)Lcom/android/sec/org/bouncycastle/asn1/ASN1Primitive;

    move-result-object v1

    invoke-static {v1}, Lcom/android/sec/org/bouncycastle/asn1/x509/Certificate;->getInstance(Ljava/lang/Object;)Lcom/android/sec/org/bouncycastle/asn1/x509/Certificate;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v1

    return-object v1

    .line 38
    :catch_0
    move-exception v0

    .line 40
    .local v0, "e":Ljava/lang/ClassCastException;
    new-instance v1, Lcom/android/sec/org/bouncycastle/cert/CertIOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "malformed data: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/ClassCastException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/android/sec/org/bouncycastle/cert/CertIOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 42
    .end local v0    # "e":Ljava/lang/ClassCastException;
    :catch_1
    move-exception v0

    .line 44
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    new-instance v1, Lcom/android/sec/org/bouncycastle/cert/CertIOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "malformed data: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/android/sec/org/bouncycastle/cert/CertIOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 296
    if-ne p1, p0, :cond_0

    .line 298
    const/4 v1, 0x1

    .line 308
    :goto_0
    return v1

    .line 301
    :cond_0
    instance-of v1, p1, Lcom/android/sec/org/bouncycastle/cert/X509CertificateHolder;

    if-nez v1, :cond_1

    .line 303
    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    move-object v0, p1

    .line 306
    check-cast v0, Lcom/android/sec/org/bouncycastle/cert/X509CertificateHolder;

    .line 308
    .local v0, "other":Lcom/android/sec/org/bouncycastle/cert/X509CertificateHolder;
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/cert/X509CertificateHolder;->x509Certificate:Lcom/android/sec/org/bouncycastle/asn1/x509/Certificate;

    iget-object v2, v0, Lcom/android/sec/org/bouncycastle/cert/X509CertificateHolder;->x509Certificate:Lcom/android/sec/org/bouncycastle/asn1/x509/Certificate;

    invoke-virtual {v1, v2}, Lcom/android/sec/org/bouncycastle/asn1/x509/Certificate;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method

.method public getCriticalExtensionOIDs()Ljava/util/Set;
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cert/X509CertificateHolder;->extensions:Lcom/android/sec/org/bouncycastle/asn1/x509/Extensions;

    invoke-static {v0}, Lcom/android/sec/org/bouncycastle/cert/CertUtils;->getCriticalExtensionOIDs(Lcom/android/sec/org/bouncycastle/asn1/x509/Extensions;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public getEncoded()[B
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 325
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cert/X509CertificateHolder;->x509Certificate:Lcom/android/sec/org/bouncycastle/asn1/x509/Certificate;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/x509/Certificate;->getEncoded()[B

    move-result-object v0

    return-object v0
.end method

.method public getExtension(Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;)Lcom/android/sec/org/bouncycastle/asn1/x509/Extension;
    .locals 1
    .param p1, "oid"    # Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    .prologue
    .line 103
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cert/X509CertificateHolder;->extensions:Lcom/android/sec/org/bouncycastle/asn1/x509/Extensions;

    if-eqz v0, :cond_0

    .line 105
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cert/X509CertificateHolder;->extensions:Lcom/android/sec/org/bouncycastle/asn1/x509/Extensions;

    invoke-virtual {v0, p1}, Lcom/android/sec/org/bouncycastle/asn1/x509/Extensions;->getExtension(Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;)Lcom/android/sec/org/bouncycastle/asn1/x509/Extension;

    move-result-object v0

    .line 108
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getExtensionOIDs()Ljava/util/List;
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cert/X509CertificateHolder;->extensions:Lcom/android/sec/org/bouncycastle/asn1/x509/Extensions;

    invoke-static {v0}, Lcom/android/sec/org/bouncycastle/cert/CertUtils;->getExtensionOIDs(Lcom/android/sec/org/bouncycastle/asn1/x509/Extensions;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getExtensions()Lcom/android/sec/org/bouncycastle/asn1/x509/Extensions;
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cert/X509CertificateHolder;->extensions:Lcom/android/sec/org/bouncycastle/asn1/x509/Extensions;

    return-object v0
.end method

.method public getIssuer()Lcom/android/sec/org/bouncycastle/asn1/x500/X500Name;
    .locals 1

    .prologue
    .line 171
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cert/X509CertificateHolder;->x509Certificate:Lcom/android/sec/org/bouncycastle/asn1/x509/Certificate;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/x509/Certificate;->getIssuer()Lcom/android/sec/org/bouncycastle/asn1/x500/X500Name;

    move-result-object v0

    invoke-static {v0}, Lcom/android/sec/org/bouncycastle/asn1/x500/X500Name;->getInstance(Ljava/lang/Object;)Lcom/android/sec/org/bouncycastle/asn1/x500/X500Name;

    move-result-object v0

    return-object v0
.end method

.method public getNonCriticalExtensionOIDs()Ljava/util/Set;
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cert/X509CertificateHolder;->extensions:Lcom/android/sec/org/bouncycastle/asn1/x509/Extensions;

    invoke-static {v0}, Lcom/android/sec/org/bouncycastle/cert/CertUtils;->getNonCriticalExtensionOIDs(Lcom/android/sec/org/bouncycastle/asn1/x509/Extensions;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public getNotAfter()Ljava/util/Date;
    .locals 1

    .prologue
    .line 201
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cert/X509CertificateHolder;->x509Certificate:Lcom/android/sec/org/bouncycastle/asn1/x509/Certificate;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/x509/Certificate;->getEndDate()Lcom/android/sec/org/bouncycastle/asn1/x509/Time;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/x509/Time;->getDate()Ljava/util/Date;

    move-result-object v0

    return-object v0
.end method

.method public getNotBefore()Ljava/util/Date;
    .locals 1

    .prologue
    .line 191
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cert/X509CertificateHolder;->x509Certificate:Lcom/android/sec/org/bouncycastle/asn1/x509/Certificate;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/x509/Certificate;->getStartDate()Lcom/android/sec/org/bouncycastle/asn1/x509/Time;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/x509/Time;->getDate()Ljava/util/Date;

    move-result-object v0

    return-object v0
.end method

.method public getSerialNumber()Ljava/math/BigInteger;
    .locals 1

    .prologue
    .line 161
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cert/X509CertificateHolder;->x509Certificate:Lcom/android/sec/org/bouncycastle/asn1/x509/Certificate;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/x509/Certificate;->getSerialNumber()Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;->getValue()Ljava/math/BigInteger;

    move-result-object v0

    return-object v0
.end method

.method public getSignature()[B
    .locals 1

    .prologue
    .line 241
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cert/X509CertificateHolder;->x509Certificate:Lcom/android/sec/org/bouncycastle/asn1/x509/Certificate;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/x509/Certificate;->getSignature()Lcom/android/sec/org/bouncycastle/asn1/DERBitString;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/DERBitString;->getBytes()[B

    move-result-object v0

    return-object v0
.end method

.method public getSignatureAlgorithm()Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;
    .locals 1

    .prologue
    .line 231
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cert/X509CertificateHolder;->x509Certificate:Lcom/android/sec/org/bouncycastle/asn1/x509/Certificate;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/x509/Certificate;->getSignatureAlgorithm()Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;

    move-result-object v0

    return-object v0
.end method

.method public getSubject()Lcom/android/sec/org/bouncycastle/asn1/x500/X500Name;
    .locals 1

    .prologue
    .line 181
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cert/X509CertificateHolder;->x509Certificate:Lcom/android/sec/org/bouncycastle/asn1/x509/Certificate;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/x509/Certificate;->getSubject()Lcom/android/sec/org/bouncycastle/asn1/x500/X500Name;

    move-result-object v0

    invoke-static {v0}, Lcom/android/sec/org/bouncycastle/asn1/x500/X500Name;->getInstance(Ljava/lang/Object;)Lcom/android/sec/org/bouncycastle/asn1/x500/X500Name;

    move-result-object v0

    return-object v0
.end method

.method public getSubjectPublicKeyInfo()Lcom/android/sec/org/bouncycastle/asn1/x509/SubjectPublicKeyInfo;
    .locals 1

    .prologue
    .line 211
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cert/X509CertificateHolder;->x509Certificate:Lcom/android/sec/org/bouncycastle/asn1/x509/Certificate;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/x509/Certificate;->getSubjectPublicKeyInfo()Lcom/android/sec/org/bouncycastle/asn1/x509/SubjectPublicKeyInfo;

    move-result-object v0

    return-object v0
.end method

.method public getVersion()I
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cert/X509CertificateHolder;->x509Certificate:Lcom/android/sec/org/bouncycastle/asn1/x509/Certificate;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/x509/Certificate;->getVersionNumber()I

    move-result v0

    return v0
.end method

.method public getVersionNumber()I
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cert/X509CertificateHolder;->x509Certificate:Lcom/android/sec/org/bouncycastle/asn1/x509/Certificate;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/x509/Certificate;->getVersionNumber()I

    move-result v0

    return v0
.end method

.method public hasExtensions()Z
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cert/X509CertificateHolder;->extensions:Lcom/android/sec/org/bouncycastle/asn1/x509/Extensions;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 313
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cert/X509CertificateHolder;->x509Certificate:Lcom/android/sec/org/bouncycastle/asn1/x509/Certificate;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/x509/Certificate;->hashCode()I

    move-result v0

    return v0
.end method

.method public isSignatureValid(Lcom/android/sec/org/bouncycastle/operator/ContentVerifierProvider;)Z
    .locals 8
    .param p1, "verifierProvider"    # Lcom/android/sec/org/bouncycastle/operator/ContentVerifierProvider;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/cert/CertException;
        }
    .end annotation

    .prologue
    .line 265
    iget-object v5, p0, Lcom/android/sec/org/bouncycastle/cert/X509CertificateHolder;->x509Certificate:Lcom/android/sec/org/bouncycastle/asn1/x509/Certificate;

    invoke-virtual {v5}, Lcom/android/sec/org/bouncycastle/asn1/x509/Certificate;->getTBSCertificate()Lcom/android/sec/org/bouncycastle/asn1/x509/TBSCertificate;

    move-result-object v3

    .line 267
    .local v3, "tbsCert":Lcom/android/sec/org/bouncycastle/asn1/x509/TBSCertificate;
    invoke-virtual {v3}, Lcom/android/sec/org/bouncycastle/asn1/x509/TBSCertificate;->getSignature()Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;

    move-result-object v5

    iget-object v6, p0, Lcom/android/sec/org/bouncycastle/cert/X509CertificateHolder;->x509Certificate:Lcom/android/sec/org/bouncycastle/asn1/x509/Certificate;

    invoke-virtual {v6}, Lcom/android/sec/org/bouncycastle/asn1/x509/Certificate;->getSignatureAlgorithm()Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/android/sec/org/bouncycastle/cert/CertUtils;->isAlgIdEqual(Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 269
    new-instance v5, Lcom/android/sec/org/bouncycastle/cert/CertException;

    const-string v6, "signature invalid - algorithm identifier mismatch"

    invoke-direct {v5, v6}, Lcom/android/sec/org/bouncycastle/cert/CertException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 276
    :cond_0
    :try_start_0
    invoke-virtual {v3}, Lcom/android/sec/org/bouncycastle/asn1/x509/TBSCertificate;->getSignature()Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;

    move-result-object v5

    invoke-interface {p1, v5}, Lcom/android/sec/org/bouncycastle/operator/ContentVerifierProvider;->get(Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;)Lcom/android/sec/org/bouncycastle/operator/ContentVerifier;

    move-result-object v4

    .line 278
    .local v4, "verifier":Lcom/android/sec/org/bouncycastle/operator/ContentVerifier;
    invoke-interface {v4}, Lcom/android/sec/org/bouncycastle/operator/ContentVerifier;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v2

    .line 279
    .local v2, "sOut":Ljava/io/OutputStream;
    new-instance v0, Lcom/android/sec/org/bouncycastle/asn1/DEROutputStream;

    invoke-direct {v0, v2}, Lcom/android/sec/org/bouncycastle/asn1/DEROutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 281
    .local v0, "dOut":Lcom/android/sec/org/bouncycastle/asn1/DEROutputStream;
    invoke-virtual {v0, v3}, Lcom/android/sec/org/bouncycastle/asn1/DEROutputStream;->writeObject(Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;)V

    .line 283
    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 290
    iget-object v5, p0, Lcom/android/sec/org/bouncycastle/cert/X509CertificateHolder;->x509Certificate:Lcom/android/sec/org/bouncycastle/asn1/x509/Certificate;

    invoke-virtual {v5}, Lcom/android/sec/org/bouncycastle/asn1/x509/Certificate;->getSignature()Lcom/android/sec/org/bouncycastle/asn1/DERBitString;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/sec/org/bouncycastle/asn1/DERBitString;->getBytes()[B

    move-result-object v5

    invoke-interface {v4, v5}, Lcom/android/sec/org/bouncycastle/operator/ContentVerifier;->verify([B)Z

    move-result v5

    return v5

    .line 285
    .end local v0    # "dOut":Lcom/android/sec/org/bouncycastle/asn1/DEROutputStream;
    .end local v2    # "sOut":Ljava/io/OutputStream;
    .end local v4    # "verifier":Lcom/android/sec/org/bouncycastle/operator/ContentVerifier;
    :catch_0
    move-exception v1

    .line 287
    .local v1, "e":Ljava/lang/Exception;
    new-instance v5, Lcom/android/sec/org/bouncycastle/cert/CertException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "unable to process signature: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6, v1}, Lcom/android/sec/org/bouncycastle/cert/CertException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v5
.end method

.method public isValidOn(Ljava/util/Date;)Z
    .locals 1
    .param p1, "date"    # Ljava/util/Date;

    .prologue
    .line 252
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cert/X509CertificateHolder;->x509Certificate:Lcom/android/sec/org/bouncycastle/asn1/x509/Certificate;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/x509/Certificate;->getStartDate()Lcom/android/sec/org/bouncycastle/asn1/x509/Time;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/x509/Time;->getDate()Ljava/util/Date;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/Date;->before(Ljava/util/Date;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cert/X509CertificateHolder;->x509Certificate:Lcom/android/sec/org/bouncycastle/asn1/x509/Certificate;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/x509/Certificate;->getEndDate()Lcom/android/sec/org/bouncycastle/asn1/x509/Time;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/x509/Time;->getDate()Ljava/util/Date;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/Date;->after(Ljava/util/Date;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toASN1Structure()Lcom/android/sec/org/bouncycastle/asn1/x509/Certificate;
    .locals 1

    .prologue
    .line 221
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cert/X509CertificateHolder;->x509Certificate:Lcom/android/sec/org/bouncycastle/asn1/x509/Certificate;

    return-object v0
.end method
