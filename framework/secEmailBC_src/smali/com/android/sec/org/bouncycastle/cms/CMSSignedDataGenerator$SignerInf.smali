.class Lcom/android/sec/org/bouncycastle/cms/CMSSignedDataGenerator$SignerInf;
.super Ljava/lang/Object;
.source "CMSSignedDataGenerator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/sec/org/bouncycastle/cms/CMSSignedDataGenerator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SignerInf"
.end annotation


# instance fields
.field final baseSignedTable:Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;

.field final digestOID:Ljava/lang/String;

.field final encOID:Ljava/lang/String;

.field final key:Ljava/security/PrivateKey;

.field final sAttr:Lcom/android/sec/org/bouncycastle/cms/CMSAttributeTableGenerator;

.field final signerIdentifier:Ljava/lang/Object;

.field final synthetic this$0:Lcom/android/sec/org/bouncycastle/cms/CMSSignedDataGenerator;

.field final unsAttr:Lcom/android/sec/org/bouncycastle/cms/CMSAttributeTableGenerator;


# direct methods
.method constructor <init>(Lcom/android/sec/org/bouncycastle/cms/CMSSignedDataGenerator;Ljava/security/PrivateKey;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;Lcom/android/sec/org/bouncycastle/cms/CMSAttributeTableGenerator;Lcom/android/sec/org/bouncycastle/cms/CMSAttributeTableGenerator;Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;)V
    .locals 0
    .param p2, "key"    # Ljava/security/PrivateKey;
    .param p3, "signerIdentifier"    # Ljava/lang/Object;
    .param p4, "digestOID"    # Ljava/lang/String;
    .param p5, "encOID"    # Ljava/lang/String;
    .param p6, "sAttr"    # Lcom/android/sec/org/bouncycastle/cms/CMSAttributeTableGenerator;
    .param p7, "unsAttr"    # Lcom/android/sec/org/bouncycastle/cms/CMSAttributeTableGenerator;
    .param p8, "baseSignedTable"    # Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;

    .prologue
    .line 83
    iput-object p1, p0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedDataGenerator$SignerInf;->this$0:Lcom/android/sec/org/bouncycastle/cms/CMSSignedDataGenerator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 84
    iput-object p2, p0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedDataGenerator$SignerInf;->key:Ljava/security/PrivateKey;

    .line 85
    iput-object p3, p0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedDataGenerator$SignerInf;->signerIdentifier:Ljava/lang/Object;

    .line 86
    iput-object p4, p0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedDataGenerator$SignerInf;->digestOID:Ljava/lang/String;

    .line 87
    iput-object p5, p0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedDataGenerator$SignerInf;->encOID:Ljava/lang/String;

    .line 88
    iput-object p6, p0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedDataGenerator$SignerInf;->sAttr:Lcom/android/sec/org/bouncycastle/cms/CMSAttributeTableGenerator;

    .line 89
    iput-object p7, p0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedDataGenerator$SignerInf;->unsAttr:Lcom/android/sec/org/bouncycastle/cms/CMSAttributeTableGenerator;

    .line 90
    iput-object p8, p0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedDataGenerator$SignerInf;->baseSignedTable:Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;

    .line 91
    return-void
.end method


# virtual methods
.method toSignerInfoGenerator(Ljava/security/SecureRandom;Ljava/security/Provider;Z)Lcom/android/sec/org/bouncycastle/cms/SignerInfoGenerator;
    .locals 9
    .param p1, "random"    # Ljava/security/SecureRandom;
    .param p2, "sigProvider"    # Ljava/security/Provider;
    .param p3, "addDefaultAttributes"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/security/cert/CertificateEncodingException;,
            Lcom/android/sec/org/bouncycastle/cms/CMSException;,
            Lcom/android/sec/org/bouncycastle/operator/OperatorCreationException;,
            Ljava/security/NoSuchAlgorithmException;
        }
    .end annotation

    .prologue
    .line 99
    sget-object v6, Lcom/android/sec/org/bouncycastle/cms/CMSSignedHelper;->INSTANCE:Lcom/android/sec/org/bouncycastle/cms/CMSSignedHelper;

    iget-object v7, p0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedDataGenerator$SignerInf;->digestOID:Ljava/lang/String;

    invoke-virtual {v6, v7}, Lcom/android/sec/org/bouncycastle/cms/CMSSignedHelper;->getDigestAlgName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 100
    .local v2, "digestName":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "with"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-object v7, Lcom/android/sec/org/bouncycastle/cms/CMSSignedHelper;->INSTANCE:Lcom/android/sec/org/bouncycastle/cms/CMSSignedHelper;

    iget-object v8, p0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedDataGenerator$SignerInf;->encOID:Ljava/lang/String;

    invoke-virtual {v7, v8}, Lcom/android/sec/org/bouncycastle/cms/CMSSignedHelper;->getEncryptionAlgName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 102
    .local v4, "signatureName":Ljava/lang/String;
    new-instance v0, Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSignerInfoGeneratorBuilder;

    new-instance v6, Lcom/android/sec/org/bouncycastle/operator/bc/BcDigestCalculatorProvider;

    invoke-direct {v6}, Lcom/android/sec/org/bouncycastle/operator/bc/BcDigestCalculatorProvider;-><init>()V

    invoke-direct {v0, v6}, Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSignerInfoGeneratorBuilder;-><init>(Lcom/android/sec/org/bouncycastle/operator/DigestCalculatorProvider;)V

    .line 104
    .local v0, "builder":Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSignerInfoGeneratorBuilder;
    if-eqz p3, :cond_0

    .line 106
    iget-object v6, p0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedDataGenerator$SignerInf;->sAttr:Lcom/android/sec/org/bouncycastle/cms/CMSAttributeTableGenerator;

    invoke-virtual {v0, v6}, Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSignerInfoGeneratorBuilder;->setSignedAttributeGenerator(Lcom/android/sec/org/bouncycastle/cms/CMSAttributeTableGenerator;)Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSignerInfoGeneratorBuilder;

    .line 108
    :cond_0
    if-nez p3, :cond_2

    const/4 v6, 0x1

    :goto_0
    invoke-virtual {v0, v6}, Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSignerInfoGeneratorBuilder;->setDirectSignature(Z)Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSignerInfoGeneratorBuilder;

    .line 110
    iget-object v6, p0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedDataGenerator$SignerInf;->unsAttr:Lcom/android/sec/org/bouncycastle/cms/CMSAttributeTableGenerator;

    invoke-virtual {v0, v6}, Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSignerInfoGeneratorBuilder;->setUnsignedAttributeGenerator(Lcom/android/sec/org/bouncycastle/cms/CMSAttributeTableGenerator;)Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSignerInfoGeneratorBuilder;

    .line 116
    :try_start_0
    new-instance v6, Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaContentSignerBuilder;

    invoke-direct {v6, v4}, Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaContentSignerBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, p1}, Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaContentSignerBuilder;->setSecureRandom(Ljava/security/SecureRandom;)Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaContentSignerBuilder;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v5

    .line 123
    .local v5, "signerBuilder":Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaContentSignerBuilder;
    if-eqz p2, :cond_1

    .line 125
    invoke-virtual {v5, p2}, Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaContentSignerBuilder;->setProvider(Ljava/security/Provider;)Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaContentSignerBuilder;

    .line 128
    :cond_1
    iget-object v6, p0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedDataGenerator$SignerInf;->key:Ljava/security/PrivateKey;

    invoke-virtual {v5, v6}, Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaContentSignerBuilder;->build(Ljava/security/PrivateKey;)Lcom/android/sec/org/bouncycastle/operator/ContentSigner;

    move-result-object v1

    .line 129
    .local v1, "contentSigner":Lcom/android/sec/org/bouncycastle/operator/ContentSigner;
    iget-object v6, p0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedDataGenerator$SignerInf;->signerIdentifier:Ljava/lang/Object;

    instance-of v6, v6, Ljava/security/cert/X509Certificate;

    if-eqz v6, :cond_3

    .line 131
    iget-object v6, p0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedDataGenerator$SignerInf;->signerIdentifier:Ljava/lang/Object;

    check-cast v6, Ljava/security/cert/X509Certificate;

    invoke-virtual {v0, v1, v6}, Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSignerInfoGeneratorBuilder;->build(Lcom/android/sec/org/bouncycastle/operator/ContentSigner;Ljava/security/cert/X509Certificate;)Lcom/android/sec/org/bouncycastle/cms/SignerInfoGenerator;

    move-result-object v6

    .line 135
    :goto_1
    return-object v6

    .line 108
    .end local v1    # "contentSigner":Lcom/android/sec/org/bouncycastle/operator/ContentSigner;
    .end local v5    # "signerBuilder":Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaContentSignerBuilder;
    :cond_2
    const/4 v6, 0x0

    goto :goto_0

    .line 118
    :catch_0
    move-exception v3

    .line 120
    .local v3, "e":Ljava/lang/IllegalArgumentException;
    new-instance v6, Ljava/security/NoSuchAlgorithmException;

    invoke-virtual {v3}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/security/NoSuchAlgorithmException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 135
    .end local v3    # "e":Ljava/lang/IllegalArgumentException;
    .restart local v1    # "contentSigner":Lcom/android/sec/org/bouncycastle/operator/ContentSigner;
    .restart local v5    # "signerBuilder":Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaContentSignerBuilder;
    :cond_3
    iget-object v6, p0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedDataGenerator$SignerInf;->signerIdentifier:Ljava/lang/Object;

    check-cast v6, [B

    check-cast v6, [B

    invoke-virtual {v0, v1, v6}, Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSignerInfoGeneratorBuilder;->build(Lcom/android/sec/org/bouncycastle/operator/ContentSigner;[B)Lcom/android/sec/org/bouncycastle/cms/SignerInfoGenerator;

    move-result-object v6

    goto :goto_1
.end method
