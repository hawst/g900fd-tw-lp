.class public abstract Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyRing;
.super Ljava/lang/Object;
.source "PGPKeyRing.java"


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    return-void
.end method

.method static readOptionalTrustPacket(Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;)Lcom/android/sec/org/bouncycastle/bcpg/TrustPacket;
    .locals 2
    .param p0, "pIn"    # Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 36
    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->nextPacketTag()I

    move-result v0

    const/16 v1, 0xc

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->readPacket()Lcom/android/sec/org/bouncycastle/bcpg/Packet;

    move-result-object v0

    check-cast v0, Lcom/android/sec/org/bouncycastle/bcpg/TrustPacket;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static readSignaturesAndTrust(Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;)Ljava/util/List;
    .locals 7
    .param p0, "pIn"    # Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 47
    :try_start_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 49
    .local v1, "sigList":Ljava/util/List;
    :goto_0
    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->nextPacketTag()I

    move-result v4

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    .line 51
    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->readPacket()Lcom/android/sec/org/bouncycastle/bcpg/Packet;

    move-result-object v2

    check-cast v2, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;

    .line 52
    .local v2, "signaturePacket":Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;
    invoke-static {p0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyRing;->readOptionalTrustPacket(Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;)Lcom/android/sec/org/bouncycastle/bcpg/TrustPacket;

    move-result-object v3

    .line 54
    .local v3, "trustPacket":Lcom/android/sec/org/bouncycastle/bcpg/TrustPacket;
    new-instance v4, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;

    invoke-direct {v4, v2, v3}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;-><init>(Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;Lcom/android/sec/org/bouncycastle/bcpg/TrustPacket;)V

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lcom/android/sec/org/bouncycastle/openpgp/PGPException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 59
    .end local v1    # "sigList":Ljava/util/List;
    .end local v2    # "signaturePacket":Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;
    .end local v3    # "trustPacket":Lcom/android/sec/org/bouncycastle/bcpg/TrustPacket;
    :catch_0
    move-exception v0

    .line 61
    .local v0, "e":Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
    new-instance v4, Ljava/io/IOException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "can\'t create signature object: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", cause: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;->getUnderlyingException()Ljava/lang/Exception;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 57
    .end local v0    # "e":Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
    .restart local v1    # "sigList":Ljava/util/List;
    :cond_0
    return-object v1
.end method

.method static readUserIDs(Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
    .locals 5
    .param p0, "pIn"    # Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;
    .param p1, "ids"    # Ljava/util/List;
    .param p2, "idTrusts"    # Ljava/util/List;
    .param p3, "idSigs"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 74
    :goto_0
    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->nextPacketTag()I

    move-result v3

    const/16 v4, 0xd

    if-eq v3, v4, :cond_0

    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->nextPacketTag()I

    move-result v3

    const/16 v4, 0x11

    if-ne v3, v4, :cond_2

    .line 76
    :cond_0
    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->readPacket()Lcom/android/sec/org/bouncycastle/bcpg/Packet;

    move-result-object v1

    .line 77
    .local v1, "obj":Lcom/android/sec/org/bouncycastle/bcpg/Packet;
    instance-of v3, v1, Lcom/android/sec/org/bouncycastle/bcpg/UserIDPacket;

    if-eqz v3, :cond_1

    move-object v0, v1

    .line 79
    check-cast v0, Lcom/android/sec/org/bouncycastle/bcpg/UserIDPacket;

    .line 80
    .local v0, "id":Lcom/android/sec/org/bouncycastle/bcpg/UserIDPacket;
    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/bcpg/UserIDPacket;->getID()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 88
    .end local v0    # "id":Lcom/android/sec/org/bouncycastle/bcpg/UserIDPacket;
    :goto_1
    invoke-static {p0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyRing;->readOptionalTrustPacket(Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;)Lcom/android/sec/org/bouncycastle/bcpg/TrustPacket;

    move-result-object v3

    invoke-interface {p2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 89
    invoke-static {p0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyRing;->readSignaturesAndTrust(Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;)Ljava/util/List;

    move-result-object v3

    invoke-interface {p3, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    move-object v2, v1

    .line 84
    check-cast v2, Lcom/android/sec/org/bouncycastle/bcpg/UserAttributePacket;

    .line 85
    .local v2, "user":Lcom/android/sec/org/bouncycastle/bcpg/UserAttributePacket;
    new-instance v3, Lcom/android/sec/org/bouncycastle/openpgp/PGPUserAttributeSubpacketVector;

    invoke-virtual {v2}, Lcom/android/sec/org/bouncycastle/bcpg/UserAttributePacket;->getSubpackets()[Lcom/android/sec/org/bouncycastle/bcpg/UserAttributeSubpacket;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/android/sec/org/bouncycastle/openpgp/PGPUserAttributeSubpacketVector;-><init>([Lcom/android/sec/org/bouncycastle/bcpg/UserAttributeSubpacket;)V

    invoke-interface {p1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 91
    .end local v1    # "obj":Lcom/android/sec/org/bouncycastle/bcpg/Packet;
    .end local v2    # "user":Lcom/android/sec/org/bouncycastle/bcpg/UserAttributePacket;
    :cond_2
    return-void
.end method

.method static wrap(Ljava/io/InputStream;)Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;
    .locals 1
    .param p0, "in"    # Ljava/io/InputStream;

    .prologue
    .line 24
    instance-of v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;

    if-eqz v0, :cond_0

    .line 26
    check-cast p0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;

    .line 29
    .end local p0    # "in":Ljava/io/InputStream;
    :goto_0
    return-object p0

    .restart local p0    # "in":Ljava/io/InputStream;
    :cond_0
    new-instance v0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;

    invoke-direct {v0, p0}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;-><init>(Ljava/io/InputStream;)V

    move-object p0, v0

    goto :goto_0
.end method
