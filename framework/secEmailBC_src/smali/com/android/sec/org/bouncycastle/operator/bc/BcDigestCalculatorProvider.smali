.class public Lcom/android/sec/org/bouncycastle/operator/bc/BcDigestCalculatorProvider;
.super Ljava/lang/Object;
.source "BcDigestCalculatorProvider.java"

# interfaces
.implements Lcom/android/sec/org/bouncycastle/operator/DigestCalculatorProvider;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/sec/org/bouncycastle/operator/bc/BcDigestCalculatorProvider$DigestOutputStream;
    }
.end annotation


# instance fields
.field private digestProvider:Lcom/android/sec/org/bouncycastle/operator/bc/BcDigestProvider;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    sget-object v0, Lcom/android/sec/org/bouncycastle/operator/bc/BcDefaultDigestProvider;->INSTANCE:Lcom/android/sec/org/bouncycastle/operator/bc/BcDigestProvider;

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/operator/bc/BcDigestCalculatorProvider;->digestProvider:Lcom/android/sec/org/bouncycastle/operator/bc/BcDigestProvider;

    .line 45
    return-void
.end method


# virtual methods
.method public get(Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;)Lcom/android/sec/org/bouncycastle/operator/DigestCalculator;
    .locals 3
    .param p1, "algorithm"    # Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/operator/OperatorCreationException;
        }
    .end annotation

    .prologue
    .line 22
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/operator/bc/BcDigestCalculatorProvider;->digestProvider:Lcom/android/sec/org/bouncycastle/operator/bc/BcDigestProvider;

    invoke-interface {v2, p1}, Lcom/android/sec/org/bouncycastle/operator/bc/BcDigestProvider;->get(Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;)Lcom/android/sec/org/bouncycastle/crypto/ExtendedDigest;

    move-result-object v0

    .line 24
    .local v0, "dig":Lcom/android/sec/org/bouncycastle/crypto/Digest;
    new-instance v1, Lcom/android/sec/org/bouncycastle/operator/bc/BcDigestCalculatorProvider$DigestOutputStream;

    invoke-direct {v1, p0, v0}, Lcom/android/sec/org/bouncycastle/operator/bc/BcDigestCalculatorProvider$DigestOutputStream;-><init>(Lcom/android/sec/org/bouncycastle/operator/bc/BcDigestCalculatorProvider;Lcom/android/sec/org/bouncycastle/crypto/Digest;)V

    .line 26
    .local v1, "stream":Lcom/android/sec/org/bouncycastle/operator/bc/BcDigestCalculatorProvider$DigestOutputStream;
    new-instance v2, Lcom/android/sec/org/bouncycastle/operator/bc/BcDigestCalculatorProvider$1;

    invoke-direct {v2, p0, p1, v1}, Lcom/android/sec/org/bouncycastle/operator/bc/BcDigestCalculatorProvider$1;-><init>(Lcom/android/sec/org/bouncycastle/operator/bc/BcDigestCalculatorProvider;Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;Lcom/android/sec/org/bouncycastle/operator/bc/BcDigestCalculatorProvider$DigestOutputStream;)V

    return-object v2
.end method
