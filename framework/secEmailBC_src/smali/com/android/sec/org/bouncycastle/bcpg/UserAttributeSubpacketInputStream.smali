.class public Lcom/android/sec/org/bouncycastle/bcpg/UserAttributeSubpacketInputStream;
.super Ljava/io/InputStream;
.source "UserAttributeSubpacketInputStream.java"

# interfaces
.implements Lcom/android/sec/org/bouncycastle/bcpg/UserAttributeSubpacketTags;


# instance fields
.field in:Ljava/io/InputStream;


# direct methods
.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 0
    .param p1, "in"    # Ljava/io/InputStream;

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    .line 18
    iput-object p1, p0, Lcom/android/sec/org/bouncycastle/bcpg/UserAttributeSubpacketInputStream;->in:Ljava/io/InputStream;

    .line 19
    return-void
.end method

.method private readFully([BII)V
    .locals 3
    .param p1, "buf"    # [B
    .param p2, "off"    # I
    .param p3, "len"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 39
    if-lez p3, :cond_1

    .line 41
    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/bcpg/UserAttributeSubpacketInputStream;->read()I

    move-result v0

    .line 43
    .local v0, "b":I
    if-gez v0, :cond_0

    .line 45
    new-instance v2, Ljava/io/EOFException;

    invoke-direct {v2}, Ljava/io/EOFException;-><init>()V

    throw v2

    .line 48
    :cond_0
    int-to-byte v2, v0

    aput-byte v2, p1, p2

    .line 49
    add-int/lit8 p2, p2, 0x1

    .line 50
    add-int/lit8 p3, p3, -0x1

    .line 53
    .end local v0    # "b":I
    :cond_1
    :goto_0
    if-lez p3, :cond_3

    .line 55
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/UserAttributeSubpacketInputStream;->in:Ljava/io/InputStream;

    invoke-virtual {v2, p1, p2, p3}, Ljava/io/InputStream;->read([BII)I

    move-result v1

    .line 57
    .local v1, "l":I
    if-gez v1, :cond_2

    .line 59
    new-instance v2, Ljava/io/EOFException;

    invoke-direct {v2}, Ljava/io/EOFException;-><init>()V

    throw v2

    .line 62
    :cond_2
    add-int/2addr p2, v1

    .line 63
    sub-int/2addr p3, v1

    .line 64
    goto :goto_0

    .line 65
    .end local v1    # "l":I
    :cond_3
    return-void
.end method


# virtual methods
.method public available()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 24
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/UserAttributeSubpacketInputStream;->in:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->available()I

    move-result v0

    return v0
.end method

.method public read()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 30
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/UserAttributeSubpacketInputStream;->in:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->read()I

    move-result v0

    return v0
.end method

.method public readPacket()Lcom/android/sec/org/bouncycastle/bcpg/UserAttributeSubpacket;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 70
    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/bcpg/UserAttributeSubpacketInputStream;->read()I

    move-result v2

    .line 71
    .local v2, "l":I
    const/4 v0, 0x0

    .line 73
    .local v0, "bodyLen":I
    if-gez v2, :cond_0

    .line 75
    const/4 v5, 0x0

    .line 114
    :goto_0
    return-object v5

    .line 78
    :cond_0
    const/16 v5, 0xc0

    if-ge v2, v5, :cond_2

    .line 80
    move v0, v2

    .line 95
    :cond_1
    :goto_1
    iget-object v5, p0, Lcom/android/sec/org/bouncycastle/bcpg/UserAttributeSubpacketInputStream;->in:Ljava/io/InputStream;

    invoke-virtual {v5}, Ljava/io/InputStream;->read()I

    move-result v3

    .line 97
    .local v3, "tag":I
    if-gez v3, :cond_4

    .line 99
    new-instance v5, Ljava/io/EOFException;

    const-string v6, "unexpected EOF reading user attribute sub packet"

    invoke-direct {v5, v6}, Ljava/io/EOFException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 82
    .end local v3    # "tag":I
    :cond_2
    const/16 v5, 0xdf

    if-gt v2, v5, :cond_3

    .line 84
    add-int/lit16 v5, v2, -0xc0

    shl-int/lit8 v5, v5, 0x8

    iget-object v6, p0, Lcom/android/sec/org/bouncycastle/bcpg/UserAttributeSubpacketInputStream;->in:Ljava/io/InputStream;

    invoke-virtual {v6}, Ljava/io/InputStream;->read()I

    move-result v6

    add-int/2addr v5, v6

    add-int/lit16 v0, v5, 0xc0

    goto :goto_1

    .line 86
    :cond_3
    const/16 v5, 0xff

    if-ne v2, v5, :cond_1

    .line 88
    iget-object v5, p0, Lcom/android/sec/org/bouncycastle/bcpg/UserAttributeSubpacketInputStream;->in:Ljava/io/InputStream;

    invoke-virtual {v5}, Ljava/io/InputStream;->read()I

    move-result v5

    shl-int/lit8 v5, v5, 0x18

    iget-object v6, p0, Lcom/android/sec/org/bouncycastle/bcpg/UserAttributeSubpacketInputStream;->in:Ljava/io/InputStream;

    invoke-virtual {v6}, Ljava/io/InputStream;->read()I

    move-result v6

    shl-int/lit8 v6, v6, 0x10

    or-int/2addr v5, v6

    iget-object v6, p0, Lcom/android/sec/org/bouncycastle/bcpg/UserAttributeSubpacketInputStream;->in:Ljava/io/InputStream;

    invoke-virtual {v6}, Ljava/io/InputStream;->read()I

    move-result v6

    shl-int/lit8 v6, v6, 0x8

    or-int/2addr v5, v6

    iget-object v6, p0, Lcom/android/sec/org/bouncycastle/bcpg/UserAttributeSubpacketInputStream;->in:Ljava/io/InputStream;

    invoke-virtual {v6}, Ljava/io/InputStream;->read()I

    move-result v6

    or-int v0, v5, v6

    goto :goto_1

    .line 102
    .restart local v3    # "tag":I
    :cond_4
    add-int/lit8 v5, v0, -0x1

    new-array v1, v5, [B

    .line 104
    .local v1, "data":[B
    const/4 v5, 0x0

    array-length v6, v1

    invoke-direct {p0, v1, v5, v6}, Lcom/android/sec/org/bouncycastle/bcpg/UserAttributeSubpacketInputStream;->readFully([BII)V

    .line 106
    move v4, v3

    .line 108
    .local v4, "type":I
    packed-switch v4, :pswitch_data_0

    .line 114
    new-instance v5, Lcom/android/sec/org/bouncycastle/bcpg/UserAttributeSubpacket;

    invoke-direct {v5, v4, v1}, Lcom/android/sec/org/bouncycastle/bcpg/UserAttributeSubpacket;-><init>(I[B)V

    goto :goto_0

    .line 111
    :pswitch_0
    new-instance v5, Lcom/android/sec/org/bouncycastle/bcpg/attr/ImageAttribute;

    invoke-direct {v5, v1}, Lcom/android/sec/org/bouncycastle/bcpg/attr/ImageAttribute;-><init>([B)V

    goto :goto_0

    .line 108
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
