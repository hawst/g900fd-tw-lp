.class public Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;
.super Ljava/lang/Object;
.source "PGPSignature.java"


# static fields
.field public static final BINARY_DOCUMENT:I = 0x0

.field public static final CANONICAL_TEXT_DOCUMENT:I = 0x1

.field public static final CASUAL_CERTIFICATION:I = 0x12

.field public static final CERTIFICATION_REVOCATION:I = 0x30

.field public static final DEFAULT_CERTIFICATION:I = 0x10

.field public static final DIRECT_KEY:I = 0x1f

.field public static final KEY_REVOCATION:I = 0x20

.field public static final NO_CERTIFICATION:I = 0x11

.field public static final POSITIVE_CERTIFICATION:I = 0x13

.field public static final PRIMARYKEY_BINDING:I = 0x19

.field public static final STAND_ALONE:I = 0x2

.field public static final SUBKEY_BINDING:I = 0x18

.field public static final SUBKEY_REVOCATION:I = 0x28

.field public static final TIMESTAMP:I = 0x40


# instance fields
.field private lastb:B

.field private sig:Ljava/security/Signature;

.field private sigPck:Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;

.field private signatureType:I

.field private trustPck:Lcom/android/sec/org/bouncycastle/bcpg/TrustPacket;


# direct methods
.method constructor <init>(Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;)V
    .locals 1
    .param p1, "pIn"    # Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
        }
    .end annotation

    .prologue
    .line 53
    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->readPacket()Lcom/android/sec/org/bouncycastle/bcpg/Packet;

    move-result-object v0

    check-cast v0, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;

    invoke-direct {p0, v0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;-><init>(Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;)V

    .line 54
    return-void
.end method

.method constructor <init>(Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;)V
    .locals 1
    .param p1, "sigPacket"    # Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
        }
    .end annotation

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    iput-object p1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;->sigPck:Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;

    .line 61
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;->sigPck:Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->getSignatureType()I

    move-result v0

    iput v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;->signatureType:I

    .line 62
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;->trustPck:Lcom/android/sec/org/bouncycastle/bcpg/TrustPacket;

    .line 63
    return-void
.end method

.method constructor <init>(Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;Lcom/android/sec/org/bouncycastle/bcpg/TrustPacket;)V
    .locals 0
    .param p1, "sigPacket"    # Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;
    .param p2, "trustPacket"    # Lcom/android/sec/org/bouncycastle/bcpg/TrustPacket;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
        }
    .end annotation

    .prologue
    .line 70
    invoke-direct {p0, p1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;-><init>(Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;)V

    .line 72
    iput-object p2, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;->trustPck:Lcom/android/sec/org/bouncycastle/bcpg/TrustPacket;

    .line 73
    return-void
.end method

.method private createSubpacketVector([Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;)Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;
    .locals 1
    .param p1, "pcks"    # [Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;

    .prologue
    .line 404
    if-eqz p1, :cond_0

    .line 406
    new-instance v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;

    invoke-direct {v0, p1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;-><init>([Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;)V

    .line 409
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getEncodedPublicKey(Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;)[B
    .locals 4
    .param p1, "pubKey"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
        }
    .end annotation

    .prologue
    .line 488
    :try_start_0
    iget-object v2, p1, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->publicPk:Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;

    invoke-virtual {v2}, Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;->getEncodedContents()[B
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 495
    .local v1, "keyBytes":[B
    return-object v1

    .line 490
    .end local v1    # "keyBytes":[B
    :catch_0
    move-exception v0

    .line 492
    .local v0, "e":Ljava/io/IOException;
    new-instance v2, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;

    const-string v3, "exception preparing key."

    invoke-direct {v2, v3, v0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v2
.end method

.method private getSig(Ljava/security/Provider;)V
    .locals 3
    .param p1, "provider"    # Ljava/security/Provider;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
        }
    .end annotation

    .prologue
    .line 81
    :try_start_0
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;->sigPck:Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;

    invoke-virtual {v1}, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->getKeyAlgorithm()I

    move-result v1

    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;->sigPck:Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;

    invoke-virtual {v2}, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->getHashAlgorithm()I

    move-result v2

    invoke-static {v1, v2}, Lcom/android/sec/org/bouncycastle/openpgp/PGPUtil;->getSignatureName(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, p1}, Ljava/security/Signature;->getInstance(Ljava/lang/String;Ljava/security/Provider;)Ljava/security/Signature;

    move-result-object v1

    iput-object v1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;->sig:Ljava/security/Signature;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 87
    return-void

    .line 83
    :catch_0
    move-exception v0

    .line 85
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;

    const-string v2, "can\'t set up signature object."

    invoke-direct {v1, v2, v0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v1
.end method

.method private updateWithIdData(I[B)V
    .locals 1
    .param p1, "header"    # I
    .param p2, "idBytes"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/SignatureException;
        }
    .end annotation

    .prologue
    .line 219
    int-to-byte v0, p1

    invoke-virtual {p0, v0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;->update(B)V

    .line 220
    array-length v0, p2

    shr-int/lit8 v0, v0, 0x18

    int-to-byte v0, v0

    invoke-virtual {p0, v0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;->update(B)V

    .line 221
    array-length v0, p2

    shr-int/lit8 v0, v0, 0x10

    int-to-byte v0, v0

    invoke-virtual {p0, v0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;->update(B)V

    .line 222
    array-length v0, p2

    shr-int/lit8 v0, v0, 0x8

    int-to-byte v0, v0

    invoke-virtual {p0, v0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;->update(B)V

    .line 223
    array-length v0, p2

    int-to-byte v0, v0

    invoke-virtual {p0, v0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;->update(B)V

    .line 224
    invoke-virtual {p0, p2}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;->update([B)V

    .line 225
    return-void
.end method

.method private updateWithPublicKey(Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;)V
    .locals 2
    .param p1, "key"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;,
            Ljava/security/SignatureException;
        }
    .end annotation

    .prologue
    .line 230
    invoke-direct {p0, p1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;->getEncodedPublicKey(Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;)[B

    move-result-object v0

    .line 232
    .local v0, "keyBytes":[B
    const/16 v1, -0x67

    invoke-virtual {p0, v1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;->update(B)V

    .line 233
    array-length v1, v0

    shr-int/lit8 v1, v1, 0x8

    int-to-byte v1, v1

    invoke-virtual {p0, v1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;->update(B)V

    .line 234
    array-length v1, v0

    int-to-byte v1, v1

    invoke-virtual {p0, v1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;->update(B)V

    .line 235
    invoke-virtual {p0, v0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;->update([B)V

    .line 236
    return-void
.end method


# virtual methods
.method public encode(Ljava/io/OutputStream;)V
    .locals 2
    .param p1, "outStream"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 464
    instance-of v1, p1, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;

    if-eqz v1, :cond_1

    move-object v0, p1

    .line 466
    check-cast v0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;

    .line 473
    .local v0, "out":Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;
    :goto_0
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;->sigPck:Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;

    invoke-virtual {v0, v1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->writePacket(Lcom/android/sec/org/bouncycastle/bcpg/ContainedPacket;)V

    .line 474
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;->trustPck:Lcom/android/sec/org/bouncycastle/bcpg/TrustPacket;

    if-eqz v1, :cond_0

    .line 476
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;->trustPck:Lcom/android/sec/org/bouncycastle/bcpg/TrustPacket;

    invoke-virtual {v0, v1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->writePacket(Lcom/android/sec/org/bouncycastle/bcpg/ContainedPacket;)V

    .line 478
    :cond_0
    return-void

    .line 470
    .end local v0    # "out":Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;
    :cond_1
    new-instance v0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;

    invoke-direct {v0, p1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;-><init>(Ljava/io/OutputStream;)V

    .restart local v0    # "out":Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;
    goto :goto_0
.end method

.method public getCreationTime()Ljava/util/Date;
    .locals 4

    .prologue
    .line 374
    new-instance v0, Ljava/util/Date;

    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;->sigPck:Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;

    invoke-virtual {v1}, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->getCreationTime()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Ljava/util/Date;-><init>(J)V

    return-object v0
.end method

.method public getEncoded()[B
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 451
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 453
    .local v0, "bOut":Ljava/io/ByteArrayOutputStream;
    invoke-virtual {p0, v0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;->encode(Ljava/io/OutputStream;)V

    .line 455
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    return-object v1
.end method

.method public getHashAlgorithm()I
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;->sigPck:Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->getHashAlgorithm()I

    move-result v0

    return v0
.end method

.method public getHashedSubPackets()Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;
    .locals 1

    .prologue
    .line 394
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;->sigPck:Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->getHashedSubPackets()[Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;->createSubpacketVector([Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;)Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;

    move-result-object v0

    return-object v0
.end method

.method public getKeyAlgorithm()I
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;->sigPck:Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->getKeyAlgorithm()I

    move-result v0

    return v0
.end method

.method public getKeyID()J
    .locals 2

    .prologue
    .line 364
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;->sigPck:Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->getKeyID()J

    move-result-wide v0

    return-wide v0
.end method

.method public getSignature()[B
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 415
    iget-object v4, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;->sigPck:Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;

    invoke-virtual {v4}, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->getSignature()[Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;

    move-result-object v1

    .line 418
    .local v1, "sigValues":[Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;
    if-eqz v1, :cond_1

    .line 420
    array-length v4, v1

    if-ne v4, v6, :cond_0

    .line 422
    aget-object v4, v1, v5

    invoke-virtual {v4}, Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;->getValue()Ljava/math/BigInteger;

    move-result-object v4

    invoke-static {v4}, Lcom/android/sec/org/bouncycastle/util/BigIntegers;->asUnsignedByteArray(Ljava/math/BigInteger;)[B

    move-result-object v2

    .line 445
    .local v2, "signature":[B
    :goto_0
    return-object v2

    .line 428
    .end local v2    # "signature":[B
    :cond_0
    :try_start_0
    new-instance v3, Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;

    invoke-direct {v3}, Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;-><init>()V

    .line 429
    .local v3, "v":Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;
    new-instance v4, Lcom/android/sec/org/bouncycastle/asn1/DERInteger;

    const/4 v5, 0x0

    aget-object v5, v1, v5

    invoke-virtual {v5}, Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;->getValue()Ljava/math/BigInteger;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/android/sec/org/bouncycastle/asn1/DERInteger;-><init>(Ljava/math/BigInteger;)V

    invoke-virtual {v3, v4}, Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;->add(Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;)V

    .line 430
    new-instance v4, Lcom/android/sec/org/bouncycastle/asn1/DERInteger;

    const/4 v5, 0x1

    aget-object v5, v1, v5

    invoke-virtual {v5}, Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;->getValue()Ljava/math/BigInteger;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/android/sec/org/bouncycastle/asn1/DERInteger;-><init>(Ljava/math/BigInteger;)V

    invoke-virtual {v3, v4}, Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;->add(Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;)V

    .line 432
    new-instance v4, Lcom/android/sec/org/bouncycastle/asn1/DERSequence;

    invoke-direct {v4, v3}, Lcom/android/sec/org/bouncycastle/asn1/DERSequence;-><init>(Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;)V

    invoke-virtual {v4}, Lcom/android/sec/org/bouncycastle/asn1/DERSequence;->getEncoded()[B
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .restart local v2    # "signature":[B
    goto :goto_0

    .line 434
    .end local v2    # "signature":[B
    .end local v3    # "v":Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;
    :catch_0
    move-exception v0

    .line 436
    .local v0, "e":Ljava/io/IOException;
    new-instance v4, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;

    const-string v5, "exception encoding DSA sig."

    invoke-direct {v4, v5, v0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v4

    .line 442
    .end local v0    # "e":Ljava/io/IOException;
    :cond_1
    iget-object v4, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;->sigPck:Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;

    invoke-virtual {v4}, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->getSignatureBytes()[B

    move-result-object v2

    .restart local v2    # "signature":[B
    goto :goto_0
.end method

.method public getSignatureTrailer()[B
    .locals 1

    .prologue
    .line 379
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;->sigPck:Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->getSignatureTrailer()[B

    move-result-object v0

    return-object v0
.end method

.method public getSignatureType()I
    .locals 1

    .prologue
    .line 355
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;->sigPck:Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->getSignatureType()I

    move-result v0

    return v0
.end method

.method public getUnhashedSubPackets()Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;
    .locals 1

    .prologue
    .line 399
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;->sigPck:Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->getUnhashedSubPackets()[Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;->createSubpacketVector([Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;)Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;

    move-result-object v0

    return-object v0
.end method

.method public getVersion()I
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;->sigPck:Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->getVersion()I

    move-result v0

    return v0
.end method

.method public hasSubpackets()Z
    .locals 1

    .prologue
    .line 389
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;->sigPck:Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->getHashedSubPackets()[Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;->sigPck:Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->getUnhashedSubPackets()[Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;

    move-result-object v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public initVerify(Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;Ljava/lang/String;)V
    .locals 1
    .param p1, "pubKey"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;
    .param p2, "provider"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/NoSuchProviderException;,
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
        }
    .end annotation

    .prologue
    .line 122
    invoke-static {p2}, Lcom/android/sec/org/bouncycastle/openpgp/PGPUtil;->getProvider(Ljava/lang/String;)Ljava/security/Provider;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;->initVerify(Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;Ljava/security/Provider;)V

    .line 123
    return-void
.end method

.method public initVerify(Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;Ljava/security/Provider;)V
    .locals 3
    .param p1, "pubKey"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;
    .param p2, "provider"    # Ljava/security/Provider;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
        }
    .end annotation

    .prologue
    .line 130
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;->sig:Ljava/security/Signature;

    if-nez v1, :cond_0

    .line 132
    invoke-direct {p0, p2}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;->getSig(Ljava/security/Provider;)V

    .line 137
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;->sig:Ljava/security/Signature;

    invoke-virtual {p1, p2}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->getKey(Ljava/security/Provider;)Ljava/security/PublicKey;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/security/Signature;->initVerify(Ljava/security/PublicKey;)V
    :try_end_0
    .catch Ljava/security/InvalidKeyException; {:try_start_0 .. :try_end_0} :catch_0

    .line 144
    const/4 v1, 0x0

    iput-byte v1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;->lastb:B

    .line 145
    return-void

    .line 139
    :catch_0
    move-exception v0

    .line 141
    .local v0, "e":Ljava/security/InvalidKeyException;
    new-instance v1, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;

    const-string v2, "invalid key."

    invoke-direct {v1, v2, v0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v1
.end method

.method public update(B)V
    .locals 4
    .param p1, "b"    # B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/SignatureException;
        }
    .end annotation

    .prologue
    const/16 v3, 0xa

    const/16 v2, 0xd

    .line 151
    iget v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;->signatureType:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    .line 153
    if-ne p1, v2, :cond_1

    .line 155
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;->sig:Ljava/security/Signature;

    invoke-virtual {v0, v2}, Ljava/security/Signature;->update(B)V

    .line 156
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;->sig:Ljava/security/Signature;

    invoke-virtual {v0, v3}, Ljava/security/Signature;->update(B)V

    .line 171
    :cond_0
    :goto_0
    iput-byte p1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;->lastb:B

    .line 177
    :goto_1
    return-void

    .line 158
    :cond_1
    if-ne p1, v3, :cond_2

    .line 160
    iget-byte v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;->lastb:B

    if-eq v0, v2, :cond_0

    .line 162
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;->sig:Ljava/security/Signature;

    invoke-virtual {v0, v2}, Ljava/security/Signature;->update(B)V

    .line 163
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;->sig:Ljava/security/Signature;

    invoke-virtual {v0, v3}, Ljava/security/Signature;->update(B)V

    goto :goto_0

    .line 168
    :cond_2
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;->sig:Ljava/security/Signature;

    invoke-virtual {v0, p1}, Ljava/security/Signature;->update(B)V

    goto :goto_0

    .line 175
    :cond_3
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;->sig:Ljava/security/Signature;

    invoke-virtual {v0, p1}, Ljava/security/Signature;->update(B)V

    goto :goto_1
.end method

.method public update([B)V
    .locals 2
    .param p1, "bytes"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/SignatureException;
        }
    .end annotation

    .prologue
    .line 183
    const/4 v0, 0x0

    array-length v1, p1

    invoke-virtual {p0, p1, v0, v1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;->update([BII)V

    .line 184
    return-void
.end method

.method public update([BII)V
    .locals 4
    .param p1, "bytes"    # [B
    .param p2, "off"    # I
    .param p3, "length"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/SignatureException;
        }
    .end annotation

    .prologue
    .line 192
    iget v2, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;->signatureType:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 194
    add-int v0, p2, p3

    .line 196
    .local v0, "finish":I
    move v1, p2

    .local v1, "i":I
    :goto_0
    if-eq v1, v0, :cond_1

    .line 198
    aget-byte v2, p1, v1

    invoke-virtual {p0, v2}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;->update(B)V

    .line 196
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 203
    .end local v0    # "finish":I
    .end local v1    # "i":I
    :cond_0
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;->sig:Ljava/security/Signature;

    invoke-virtual {v2, p1, p2, p3}, Ljava/security/Signature;->update([BII)V

    .line 205
    :cond_1
    return-void
.end method

.method public verify()Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;,
            Ljava/security/SignatureException;
        }
    .end annotation

    .prologue
    .line 210
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;->sig:Ljava/security/Signature;

    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;->getSignatureTrailer()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/security/Signature;->update([B)V

    .line 212
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;->sig:Ljava/security/Signature;

    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;->getSignature()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/security/Signature;->verify([B)Z

    move-result v0

    return v0
.end method

.method public verifyCertification(Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;)Z
    .locals 2
    .param p1, "pubKey"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/SignatureException;,
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
        }
    .end annotation

    .prologue
    .line 340
    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;->getSignatureType()I

    move-result v0

    const/16 v1, 0x20

    if-eq v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;->getSignatureType()I

    move-result v0

    const/16 v1, 0x28

    if-eq v0, v1, :cond_0

    .line 343
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "signature is not a key signature"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 346
    :cond_0
    invoke-direct {p0, p1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;->updateWithPublicKey(Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;)V

    .line 348
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;->sigPck:Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->getSignatureTrailer()[B

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;->update([B)V

    .line 350
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;->sig:Ljava/security/Signature;

    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;->getSignature()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/security/Signature;->verify([B)Z

    move-result v0

    return v0
.end method

.method public verifyCertification(Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;)Z
    .locals 2
    .param p1, "masterKey"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;
    .param p2, "pubKey"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/SignatureException;,
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
        }
    .end annotation

    .prologue
    .line 320
    invoke-direct {p0, p1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;->updateWithPublicKey(Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;)V

    .line 321
    invoke-direct {p0, p2}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;->updateWithPublicKey(Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;)V

    .line 323
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;->sigPck:Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->getSignatureTrailer()[B

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;->update([B)V

    .line 325
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;->sig:Ljava/security/Signature;

    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;->getSignature()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/security/Signature;->verify([B)Z

    move-result v0

    return v0
.end method

.method public verifyCertification(Lcom/android/sec/org/bouncycastle/openpgp/PGPUserAttributeSubpacketVector;Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;)Z
    .locals 6
    .param p1, "userAttributes"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPUserAttributeSubpacketVector;
    .param p2, "key"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;,
            Ljava/security/SignatureException;
        }
    .end annotation

    .prologue
    .line 253
    invoke-direct {p0, p2}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;->updateWithPublicKey(Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;)V

    .line 260
    :try_start_0
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 261
    .local v0, "bOut":Ljava/io/ByteArrayOutputStream;
    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPUserAttributeSubpacketVector;->toSubpacketArray()[Lcom/android/sec/org/bouncycastle/bcpg/UserAttributeSubpacket;

    move-result-object v3

    .line 262
    .local v3, "packets":[Lcom/android/sec/org/bouncycastle/bcpg/UserAttributeSubpacket;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v4, v3

    if-eq v2, v4, :cond_0

    .line 264
    aget-object v4, v3, v2

    invoke-virtual {v4, v0}, Lcom/android/sec/org/bouncycastle/bcpg/UserAttributeSubpacket;->encode(Ljava/io/OutputStream;)V

    .line 262
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 266
    :cond_0
    const/16 v4, 0xd1

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v5

    invoke-direct {p0, v4, v5}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;->updateWithIdData(I[B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 273
    iget-object v4, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;->sigPck:Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;

    invoke-virtual {v4}, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->getSignatureTrailer()[B

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;->update([B)V

    .line 275
    iget-object v4, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;->sig:Ljava/security/Signature;

    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;->getSignature()[B

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/security/Signature;->verify([B)Z

    move-result v4

    return v4

    .line 268
    .end local v0    # "bOut":Ljava/io/ByteArrayOutputStream;
    .end local v2    # "i":I
    .end local v3    # "packets":[Lcom/android/sec/org/bouncycastle/bcpg/UserAttributeSubpacket;
    :catch_0
    move-exception v1

    .line 270
    .local v1, "e":Ljava/io/IOException;
    new-instance v4, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;

    const-string v5, "cannot encode subpacket array"

    invoke-direct {v4, v5, v1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v4
.end method

.method public verifyCertification(Ljava/lang/String;Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;)Z
    .locals 2
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "key"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;,
            Ljava/security/SignatureException;
        }
    .end annotation

    .prologue
    .line 293
    invoke-direct {p0, p2}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;->updateWithPublicKey(Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;)V

    .line 298
    const/16 v0, 0xb4

    invoke-static {p1}, Lcom/android/sec/org/bouncycastle/util/Strings;->toByteArray(Ljava/lang/String;)[B

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;->updateWithIdData(I[B)V

    .line 300
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;->sigPck:Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/bcpg/SignaturePacket;->getSignatureTrailer()[B

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;->update([B)V

    .line 302
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;->sig:Ljava/security/Signature;

    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;->getSignature()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/security/Signature;->verify([B)Z

    move-result v0

    return v0
.end method
