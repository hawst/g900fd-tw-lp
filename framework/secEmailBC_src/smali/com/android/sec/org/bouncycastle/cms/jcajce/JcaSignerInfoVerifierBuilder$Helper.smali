.class Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSignerInfoVerifierBuilder$Helper;
.super Ljava/lang/Object;
.source "JcaSignerInfoVerifierBuilder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSignerInfoVerifierBuilder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "Helper"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSignerInfoVerifierBuilder;


# direct methods
.method private constructor <init>(Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSignerInfoVerifierBuilder;)V
    .locals 0

    .prologue
    .line 84
    iput-object p1, p0, Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSignerInfoVerifierBuilder$Helper;->this$0:Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSignerInfoVerifierBuilder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSignerInfoVerifierBuilder;Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSignerInfoVerifierBuilder$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSignerInfoVerifierBuilder;
    .param p2, "x1"    # Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSignerInfoVerifierBuilder$1;

    .prologue
    .line 84
    invoke-direct {p0, p1}, Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSignerInfoVerifierBuilder$Helper;-><init>(Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSignerInfoVerifierBuilder;)V

    return-void
.end method


# virtual methods
.method createContentVerifierProvider(Lcom/android/sec/org/bouncycastle/cert/X509CertificateHolder;)Lcom/android/sec/org/bouncycastle/operator/ContentVerifierProvider;
    .locals 1
    .param p1, "certHolder"    # Lcom/android/sec/org/bouncycastle/cert/X509CertificateHolder;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/operator/OperatorCreationException;,
            Ljava/security/cert/CertificateException;
        }
    .end annotation

    .prologue
    .line 101
    new-instance v0, Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaContentVerifierProviderBuilder;

    invoke-direct {v0}, Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaContentVerifierProviderBuilder;-><init>()V

    invoke-virtual {v0, p1}, Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaContentVerifierProviderBuilder;->build(Lcom/android/sec/org/bouncycastle/cert/X509CertificateHolder;)Lcom/android/sec/org/bouncycastle/operator/ContentVerifierProvider;

    move-result-object v0

    return-object v0
.end method

.method createContentVerifierProvider(Ljava/security/PublicKey;)Lcom/android/sec/org/bouncycastle/operator/ContentVerifierProvider;
    .locals 1
    .param p1, "publicKey"    # Ljava/security/PublicKey;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/operator/OperatorCreationException;
        }
    .end annotation

    .prologue
    .line 89
    new-instance v0, Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaContentVerifierProviderBuilder;

    invoke-direct {v0}, Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaContentVerifierProviderBuilder;-><init>()V

    invoke-virtual {v0, p1}, Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaContentVerifierProviderBuilder;->build(Ljava/security/PublicKey;)Lcom/android/sec/org/bouncycastle/operator/ContentVerifierProvider;

    move-result-object v0

    return-object v0
.end method

.method createContentVerifierProvider(Ljava/security/cert/X509Certificate;)Lcom/android/sec/org/bouncycastle/operator/ContentVerifierProvider;
    .locals 1
    .param p1, "certificate"    # Ljava/security/cert/X509Certificate;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/operator/OperatorCreationException;
        }
    .end annotation

    .prologue
    .line 95
    new-instance v0, Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaContentVerifierProviderBuilder;

    invoke-direct {v0}, Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaContentVerifierProviderBuilder;-><init>()V

    invoke-virtual {v0, p1}, Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaContentVerifierProviderBuilder;->build(Ljava/security/cert/X509Certificate;)Lcom/android/sec/org/bouncycastle/operator/ContentVerifierProvider;

    move-result-object v0

    return-object v0
.end method

.method createDigestCalculatorProvider()Lcom/android/sec/org/bouncycastle/operator/DigestCalculatorProvider;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/operator/OperatorCreationException;
        }
    .end annotation

    .prologue
    .line 107
    new-instance v0, Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaDigestCalculatorProviderBuilder;

    invoke-direct {v0}, Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaDigestCalculatorProviderBuilder;-><init>()V

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaDigestCalculatorProviderBuilder;->build()Lcom/android/sec/org/bouncycastle/operator/DigestCalculatorProvider;

    move-result-object v0

    return-object v0
.end method
