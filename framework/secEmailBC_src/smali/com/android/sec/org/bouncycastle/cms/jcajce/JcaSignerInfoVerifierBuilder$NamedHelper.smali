.class Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSignerInfoVerifierBuilder$NamedHelper;
.super Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSignerInfoVerifierBuilder$Helper;
.source "JcaSignerInfoVerifierBuilder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSignerInfoVerifierBuilder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "NamedHelper"
.end annotation


# instance fields
.field private final providerName:Ljava/lang/String;

.field final synthetic this$0:Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSignerInfoVerifierBuilder;


# direct methods
.method public constructor <init>(Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSignerInfoVerifierBuilder;Ljava/lang/String;)V
    .locals 1
    .param p2, "providerName"    # Ljava/lang/String;

    .prologue
    .line 117
    iput-object p1, p0, Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSignerInfoVerifierBuilder$NamedHelper;->this$0:Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSignerInfoVerifierBuilder;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSignerInfoVerifierBuilder$Helper;-><init>(Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSignerInfoVerifierBuilder;Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSignerInfoVerifierBuilder$1;)V

    .line 118
    iput-object p2, p0, Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSignerInfoVerifierBuilder$NamedHelper;->providerName:Ljava/lang/String;

    .line 119
    return-void
.end method


# virtual methods
.method createContentVerifierProvider(Lcom/android/sec/org/bouncycastle/cert/X509CertificateHolder;)Lcom/android/sec/org/bouncycastle/operator/ContentVerifierProvider;
    .locals 2
    .param p1, "certHolder"    # Lcom/android/sec/org/bouncycastle/cert/X509CertificateHolder;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/operator/OperatorCreationException;,
            Ljava/security/cert/CertificateException;
        }
    .end annotation

    .prologue
    .line 142
    new-instance v0, Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaContentVerifierProviderBuilder;

    invoke-direct {v0}, Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaContentVerifierProviderBuilder;-><init>()V

    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSignerInfoVerifierBuilder$NamedHelper;->providerName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaContentVerifierProviderBuilder;->setProvider(Ljava/lang/String;)Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaContentVerifierProviderBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaContentVerifierProviderBuilder;->build(Lcom/android/sec/org/bouncycastle/cert/X509CertificateHolder;)Lcom/android/sec/org/bouncycastle/operator/ContentVerifierProvider;

    move-result-object v0

    return-object v0
.end method

.method createContentVerifierProvider(Ljava/security/PublicKey;)Lcom/android/sec/org/bouncycastle/operator/ContentVerifierProvider;
    .locals 2
    .param p1, "publicKey"    # Ljava/security/PublicKey;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/operator/OperatorCreationException;
        }
    .end annotation

    .prologue
    .line 124
    new-instance v0, Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaContentVerifierProviderBuilder;

    invoke-direct {v0}, Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaContentVerifierProviderBuilder;-><init>()V

    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSignerInfoVerifierBuilder$NamedHelper;->providerName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaContentVerifierProviderBuilder;->setProvider(Ljava/lang/String;)Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaContentVerifierProviderBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaContentVerifierProviderBuilder;->build(Ljava/security/PublicKey;)Lcom/android/sec/org/bouncycastle/operator/ContentVerifierProvider;

    move-result-object v0

    return-object v0
.end method

.method createContentVerifierProvider(Ljava/security/cert/X509Certificate;)Lcom/android/sec/org/bouncycastle/operator/ContentVerifierProvider;
    .locals 2
    .param p1, "certificate"    # Ljava/security/cert/X509Certificate;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/operator/OperatorCreationException;
        }
    .end annotation

    .prologue
    .line 130
    new-instance v0, Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaContentVerifierProviderBuilder;

    invoke-direct {v0}, Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaContentVerifierProviderBuilder;-><init>()V

    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSignerInfoVerifierBuilder$NamedHelper;->providerName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaContentVerifierProviderBuilder;->setProvider(Ljava/lang/String;)Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaContentVerifierProviderBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaContentVerifierProviderBuilder;->build(Ljava/security/cert/X509Certificate;)Lcom/android/sec/org/bouncycastle/operator/ContentVerifierProvider;

    move-result-object v0

    return-object v0
.end method

.method createDigestCalculatorProvider()Lcom/android/sec/org/bouncycastle/operator/DigestCalculatorProvider;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/operator/OperatorCreationException;
        }
    .end annotation

    .prologue
    .line 136
    new-instance v0, Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaDigestCalculatorProviderBuilder;

    invoke-direct {v0}, Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaDigestCalculatorProviderBuilder;-><init>()V

    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSignerInfoVerifierBuilder$NamedHelper;->providerName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaDigestCalculatorProviderBuilder;->setProvider(Ljava/lang/String;)Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaDigestCalculatorProviderBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaDigestCalculatorProviderBuilder;->build()Lcom/android/sec/org/bouncycastle/operator/DigestCalculatorProvider;

    move-result-object v0

    return-object v0
.end method
