.class public interface abstract Lcom/android/sec/org/bouncycastle/crypto/BasicAgreement;
.super Ljava/lang/Object;
.source "BasicAgreement.java"


# virtual methods
.method public abstract calculateAgreement(Lcom/android/sec/org/bouncycastle/crypto/CipherParameters;)Ljava/math/BigInteger;
.end method

.method public abstract getFieldSize()I
.end method

.method public abstract init(Lcom/android/sec/org/bouncycastle/crypto/CipherParameters;)V
.end method
