.class public Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataList;
.super Ljava/lang/Object;
.source "PGPEncryptedDataList.java"


# instance fields
.field data:Lcom/android/sec/org/bouncycastle/bcpg/InputStreamPacket;

.field list:Ljava/util/List;


# direct methods
.method public constructor <init>(Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;)V
    .locals 5
    .param p1, "pIn"    # Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataList;->list:Ljava/util/List;

    .line 27
    :goto_0
    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->nextPacketTag()I

    move-result v1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_0

    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->nextPacketTag()I

    move-result v1

    const/4 v2, 0x3

    if-ne v1, v2, :cond_1

    .line 29
    :cond_0
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataList;->list:Ljava/util/List;

    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->readPacket()Lcom/android/sec/org/bouncycastle/bcpg/Packet;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 32
    :cond_1
    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->readPacket()Lcom/android/sec/org/bouncycastle/bcpg/Packet;

    move-result-object v1

    check-cast v1, Lcom/android/sec/org/bouncycastle/bcpg/InputStreamPacket;

    iput-object v1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataList;->data:Lcom/android/sec/org/bouncycastle/bcpg/InputStreamPacket;

    .line 34
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataList;->list:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-eq v0, v1, :cond_3

    .line 36
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataList;->list:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    instance-of v1, v1, Lcom/android/sec/org/bouncycastle/bcpg/SymmetricKeyEncSessionPacket;

    if-eqz v1, :cond_2

    .line 38
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataList;->list:Ljava/util/List;

    new-instance v3, Lcom/android/sec/org/bouncycastle/openpgp/PGPPBEEncryptedData;

    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataList;->list:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/sec/org/bouncycastle/bcpg/SymmetricKeyEncSessionPacket;

    iget-object v4, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataList;->data:Lcom/android/sec/org/bouncycastle/bcpg/InputStreamPacket;

    invoke-direct {v3, v1, v4}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPBEEncryptedData;-><init>(Lcom/android/sec/org/bouncycastle/bcpg/SymmetricKeyEncSessionPacket;Lcom/android/sec/org/bouncycastle/bcpg/InputStreamPacket;)V

    invoke-interface {v2, v0, v3}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 34
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 42
    :cond_2
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataList;->list:Ljava/util/List;

    new-instance v3, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyEncryptedData;

    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataList;->list:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyEncSessionPacket;

    iget-object v4, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataList;->data:Lcom/android/sec/org/bouncycastle/bcpg/InputStreamPacket;

    invoke-direct {v3, v1, v4}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyEncryptedData;-><init>(Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyEncSessionPacket;Lcom/android/sec/org/bouncycastle/bcpg/InputStreamPacket;)V

    invoke-interface {v2, v0, v3}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 45
    :cond_3
    return-void
.end method


# virtual methods
.method public get(I)Ljava/lang/Object;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 50
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataList;->list:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getEncryptedDataObjects()Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataList;->list:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public getEncyptedDataObjects()Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataList;->list:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataList;->list:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataList;->list:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method
