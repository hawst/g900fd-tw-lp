.class public Lcom/android/sec/org/bouncycastle/bcpg/DSAPublicBCPGKey;
.super Lcom/android/sec/org/bouncycastle/bcpg/BCPGObject;
.source "DSAPublicBCPGKey.java"

# interfaces
.implements Lcom/android/sec/org/bouncycastle/bcpg/BCPGKey;


# instance fields
.field g:Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;

.field p:Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;

.field q:Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;

.field y:Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;


# direct methods
.method public constructor <init>(Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;)V
    .locals 1
    .param p1, "in"    # Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGObject;-><init>()V

    .line 24
    new-instance v0, Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;

    invoke-direct {v0, p1}, Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;-><init>(Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;)V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/DSAPublicBCPGKey;->p:Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;

    .line 25
    new-instance v0, Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;

    invoke-direct {v0, p1}, Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;-><init>(Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;)V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/DSAPublicBCPGKey;->q:Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;

    .line 26
    new-instance v0, Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;

    invoke-direct {v0, p1}, Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;-><init>(Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;)V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/DSAPublicBCPGKey;->g:Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;

    .line 27
    new-instance v0, Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;

    invoke-direct {v0, p1}, Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;-><init>(Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;)V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/DSAPublicBCPGKey;->y:Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;

    .line 28
    return-void
.end method

.method public constructor <init>(Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/math/BigInteger;)V
    .locals 1
    .param p1, "p"    # Ljava/math/BigInteger;
    .param p2, "q"    # Ljava/math/BigInteger;
    .param p3, "g"    # Ljava/math/BigInteger;
    .param p4, "y"    # Ljava/math/BigInteger;

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGObject;-><init>()V

    .line 36
    new-instance v0, Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;

    invoke-direct {v0, p1}, Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;-><init>(Ljava/math/BigInteger;)V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/DSAPublicBCPGKey;->p:Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;

    .line 37
    new-instance v0, Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;

    invoke-direct {v0, p2}, Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;-><init>(Ljava/math/BigInteger;)V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/DSAPublicBCPGKey;->q:Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;

    .line 38
    new-instance v0, Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;

    invoke-direct {v0, p3}, Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;-><init>(Ljava/math/BigInteger;)V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/DSAPublicBCPGKey;->g:Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;

    .line 39
    new-instance v0, Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;

    invoke-direct {v0, p4}, Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;-><init>(Ljava/math/BigInteger;)V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/DSAPublicBCPGKey;->y:Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;

    .line 40
    return-void
.end method


# virtual methods
.method public encode(Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;)V
    .locals 1
    .param p1, "out"    # Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 78
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/DSAPublicBCPGKey;->p:Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;

    invoke-virtual {p1, v0}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->writeObject(Lcom/android/sec/org/bouncycastle/bcpg/BCPGObject;)V

    .line 79
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/DSAPublicBCPGKey;->q:Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;

    invoke-virtual {p1, v0}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->writeObject(Lcom/android/sec/org/bouncycastle/bcpg/BCPGObject;)V

    .line 80
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/DSAPublicBCPGKey;->g:Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;

    invoke-virtual {p1, v0}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->writeObject(Lcom/android/sec/org/bouncycastle/bcpg/BCPGObject;)V

    .line 81
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/DSAPublicBCPGKey;->y:Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;

    invoke-virtual {p1, v0}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->writeObject(Lcom/android/sec/org/bouncycastle/bcpg/BCPGObject;)V

    .line 82
    return-void
.end method

.method public getEncoded()[B
    .locals 4

    .prologue
    .line 61
    :try_start_0
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 62
    .local v0, "bOut":Ljava/io/ByteArrayOutputStream;
    new-instance v2, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;

    invoke-direct {v2, v0}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 64
    .local v2, "pgpOut":Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;
    invoke-virtual {v2, p0}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->writeObject(Lcom/android/sec/org/bouncycastle/bcpg/BCPGObject;)V

    .line 66
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 70
    .end local v0    # "bOut":Ljava/io/ByteArrayOutputStream;
    .end local v2    # "pgpOut":Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;
    :goto_0
    return-object v3

    .line 68
    :catch_0
    move-exception v1

    .line 70
    .local v1, "e":Ljava/io/IOException;
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public getFormat()Ljava/lang/String;
    .locals 1

    .prologue
    .line 49
    const-string v0, "PGP"

    return-object v0
.end method

.method public getG()Ljava/math/BigInteger;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/DSAPublicBCPGKey;->g:Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;->getValue()Ljava/math/BigInteger;

    move-result-object v0

    return-object v0
.end method

.method public getP()Ljava/math/BigInteger;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/DSAPublicBCPGKey;->p:Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;->getValue()Ljava/math/BigInteger;

    move-result-object v0

    return-object v0
.end method

.method public getQ()Ljava/math/BigInteger;
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/DSAPublicBCPGKey;->q:Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;->getValue()Ljava/math/BigInteger;

    move-result-object v0

    return-object v0
.end method

.method public getY()Ljava/math/BigInteger;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/DSAPublicBCPGKey;->y:Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/bcpg/MPInteger;->getValue()Ljava/math/BigInteger;

    move-result-object v0

    return-object v0
.end method
