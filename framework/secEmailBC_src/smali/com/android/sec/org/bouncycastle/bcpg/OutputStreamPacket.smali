.class public abstract Lcom/android/sec/org/bouncycastle/bcpg/OutputStreamPacket;
.super Ljava/lang/Object;
.source "OutputStreamPacket.java"


# instance fields
.field protected out:Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;


# direct methods
.method public constructor <init>(Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;)V
    .locals 0
    .param p1, "out"    # Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    iput-object p1, p0, Lcom/android/sec/org/bouncycastle/bcpg/OutputStreamPacket;->out:Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;

    .line 13
    return-void
.end method


# virtual methods
.method public abstract close()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract open()Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method
