.class public Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSignerInfoGeneratorBuilder;
.super Ljava/lang/Object;
.source "JcaSignerInfoGeneratorBuilder.java"


# instance fields
.field private builder:Lcom/android/sec/org/bouncycastle/cms/SignerInfoGeneratorBuilder;


# direct methods
.method public constructor <init>(Lcom/android/sec/org/bouncycastle/operator/DigestCalculatorProvider;)V
    .locals 1
    .param p1, "digestProvider"    # Lcom/android/sec/org/bouncycastle/operator/DigestCalculatorProvider;

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    new-instance v0, Lcom/android/sec/org/bouncycastle/cms/SignerInfoGeneratorBuilder;

    invoke-direct {v0, p1}, Lcom/android/sec/org/bouncycastle/cms/SignerInfoGeneratorBuilder;-><init>(Lcom/android/sec/org/bouncycastle/operator/DigestCalculatorProvider;)V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSignerInfoGeneratorBuilder;->builder:Lcom/android/sec/org/bouncycastle/cms/SignerInfoGeneratorBuilder;

    .line 22
    return-void
.end method


# virtual methods
.method public build(Lcom/android/sec/org/bouncycastle/operator/ContentSigner;Lcom/android/sec/org/bouncycastle/cert/X509CertificateHolder;)Lcom/android/sec/org/bouncycastle/cms/SignerInfoGenerator;
    .locals 1
    .param p1, "contentSigner"    # Lcom/android/sec/org/bouncycastle/operator/ContentSigner;
    .param p2, "certHolder"    # Lcom/android/sec/org/bouncycastle/cert/X509CertificateHolder;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/operator/OperatorCreationException;
        }
    .end annotation

    .prologue
    .line 54
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSignerInfoGeneratorBuilder;->builder:Lcom/android/sec/org/bouncycastle/cms/SignerInfoGeneratorBuilder;

    invoke-virtual {v0, p1, p2}, Lcom/android/sec/org/bouncycastle/cms/SignerInfoGeneratorBuilder;->build(Lcom/android/sec/org/bouncycastle/operator/ContentSigner;Lcom/android/sec/org/bouncycastle/cert/X509CertificateHolder;)Lcom/android/sec/org/bouncycastle/cms/SignerInfoGenerator;

    move-result-object v0

    return-object v0
.end method

.method public build(Lcom/android/sec/org/bouncycastle/operator/ContentSigner;Ljava/security/cert/X509Certificate;)Lcom/android/sec/org/bouncycastle/cms/SignerInfoGenerator;
    .locals 1
    .param p1, "contentSigner"    # Lcom/android/sec/org/bouncycastle/operator/ContentSigner;
    .param p2, "certificate"    # Ljava/security/cert/X509Certificate;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/operator/OperatorCreationException;,
            Ljava/security/cert/CertificateEncodingException;
        }
    .end annotation

    .prologue
    .line 66
    new-instance v0, Lcom/android/sec/org/bouncycastle/cert/jcajce/JcaX509CertificateHolder;

    invoke-direct {v0, p2}, Lcom/android/sec/org/bouncycastle/cert/jcajce/JcaX509CertificateHolder;-><init>(Ljava/security/cert/X509Certificate;)V

    invoke-virtual {p0, p1, v0}, Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSignerInfoGeneratorBuilder;->build(Lcom/android/sec/org/bouncycastle/operator/ContentSigner;Lcom/android/sec/org/bouncycastle/cert/X509CertificateHolder;)Lcom/android/sec/org/bouncycastle/cms/SignerInfoGenerator;

    move-result-object v0

    return-object v0
.end method

.method public build(Lcom/android/sec/org/bouncycastle/operator/ContentSigner;[B)Lcom/android/sec/org/bouncycastle/cms/SignerInfoGenerator;
    .locals 1
    .param p1, "contentSigner"    # Lcom/android/sec/org/bouncycastle/operator/ContentSigner;
    .param p2, "keyIdentifier"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/operator/OperatorCreationException;
        }
    .end annotation

    .prologue
    .line 60
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSignerInfoGeneratorBuilder;->builder:Lcom/android/sec/org/bouncycastle/cms/SignerInfoGeneratorBuilder;

    invoke-virtual {v0, p1, p2}, Lcom/android/sec/org/bouncycastle/cms/SignerInfoGeneratorBuilder;->build(Lcom/android/sec/org/bouncycastle/operator/ContentSigner;[B)Lcom/android/sec/org/bouncycastle/cms/SignerInfoGenerator;

    move-result-object v0

    return-object v0
.end method

.method public setDirectSignature(Z)Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSignerInfoGeneratorBuilder;
    .locals 1
    .param p1, "hasNoSignedAttributes"    # Z

    .prologue
    .line 32
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSignerInfoGeneratorBuilder;->builder:Lcom/android/sec/org/bouncycastle/cms/SignerInfoGeneratorBuilder;

    invoke-virtual {v0, p1}, Lcom/android/sec/org/bouncycastle/cms/SignerInfoGeneratorBuilder;->setDirectSignature(Z)Lcom/android/sec/org/bouncycastle/cms/SignerInfoGeneratorBuilder;

    .line 34
    return-object p0
.end method

.method public setSignedAttributeGenerator(Lcom/android/sec/org/bouncycastle/cms/CMSAttributeTableGenerator;)Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSignerInfoGeneratorBuilder;
    .locals 1
    .param p1, "signedGen"    # Lcom/android/sec/org/bouncycastle/cms/CMSAttributeTableGenerator;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSignerInfoGeneratorBuilder;->builder:Lcom/android/sec/org/bouncycastle/cms/SignerInfoGeneratorBuilder;

    invoke-virtual {v0, p1}, Lcom/android/sec/org/bouncycastle/cms/SignerInfoGeneratorBuilder;->setSignedAttributeGenerator(Lcom/android/sec/org/bouncycastle/cms/CMSAttributeTableGenerator;)Lcom/android/sec/org/bouncycastle/cms/SignerInfoGeneratorBuilder;

    .line 41
    return-object p0
.end method

.method public setUnsignedAttributeGenerator(Lcom/android/sec/org/bouncycastle/cms/CMSAttributeTableGenerator;)Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSignerInfoGeneratorBuilder;
    .locals 1
    .param p1, "unsignedGen"    # Lcom/android/sec/org/bouncycastle/cms/CMSAttributeTableGenerator;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSignerInfoGeneratorBuilder;->builder:Lcom/android/sec/org/bouncycastle/cms/SignerInfoGeneratorBuilder;

    invoke-virtual {v0, p1}, Lcom/android/sec/org/bouncycastle/cms/SignerInfoGeneratorBuilder;->setUnsignedAttributeGenerator(Lcom/android/sec/org/bouncycastle/cms/CMSAttributeTableGenerator;)Lcom/android/sec/org/bouncycastle/cms/SignerInfoGeneratorBuilder;

    .line 48
    return-object p0
.end method
