.class public Lcom/android/sec/org/bouncycastle/bcpg/TrustPacket;
.super Lcom/android/sec/org/bouncycastle/bcpg/ContainedPacket;
.source "TrustPacket.java"


# instance fields
.field levelAndTrustAmount:[B


# direct methods
.method public constructor <init>(I)V
    .locals 3
    .param p1, "trustCode"    # I

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/bcpg/ContainedPacket;-><init>()V

    .line 32
    const/4 v0, 0x1

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/TrustPacket;->levelAndTrustAmount:[B

    .line 34
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/TrustPacket;->levelAndTrustAmount:[B

    const/4 v1, 0x0

    int-to-byte v2, p1

    aput-byte v2, v0, v1

    .line 35
    return-void
.end method

.method public constructor <init>(Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;)V
    .locals 3
    .param p1, "in"    # Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/bcpg/ContainedPacket;-><init>()V

    .line 18
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 21
    .local v0, "bOut":Ljava/io/ByteArrayOutputStream;
    :goto_0
    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->read()I

    move-result v1

    .local v1, "ch":I
    if-ltz v1, :cond_0

    .line 23
    invoke-virtual {v0, v1}, Ljava/io/ByteArrayOutputStream;->write(I)V

    goto :goto_0

    .line 26
    :cond_0
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v2

    iput-object v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/TrustPacket;->levelAndTrustAmount:[B

    .line 27
    return-void
.end method


# virtual methods
.method public encode(Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;)V
    .locals 3
    .param p1, "out"    # Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 46
    const/16 v0, 0xc

    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/bcpg/TrustPacket;->levelAndTrustAmount:[B

    const/4 v2, 0x1

    invoke-virtual {p1, v0, v1, v2}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->writePacket(I[BZ)V

    .line 47
    return-void
.end method

.method public getLevelAndTrustAmount()[B
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/TrustPacket;->levelAndTrustAmount:[B

    return-object v0
.end method
