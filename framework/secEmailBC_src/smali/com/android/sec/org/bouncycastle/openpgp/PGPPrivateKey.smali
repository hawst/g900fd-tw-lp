.class public Lcom/android/sec/org/bouncycastle/openpgp/PGPPrivateKey;
.super Ljava/lang/Object;
.source "PGPPrivateKey.java"


# instance fields
.field private keyID:J

.field private privateKey:Ljava/security/PrivateKey;


# direct methods
.method public constructor <init>(Ljava/security/PrivateKey;J)V
    .locals 0
    .param p1, "privateKey"    # Ljava/security/PrivateKey;
    .param p2, "keyID"    # J

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPrivateKey;->privateKey:Ljava/security/PrivateKey;

    .line 26
    iput-wide p2, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPrivateKey;->keyID:J

    .line 27
    return-void
.end method


# virtual methods
.method public getKey()Ljava/security/PrivateKey;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPrivateKey;->privateKey:Ljava/security/PrivateKey;

    return-object v0
.end method

.method public getKeyID()J
    .locals 2

    .prologue
    .line 36
    iget-wide v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPrivateKey;->keyID:J

    return-wide v0
.end method
