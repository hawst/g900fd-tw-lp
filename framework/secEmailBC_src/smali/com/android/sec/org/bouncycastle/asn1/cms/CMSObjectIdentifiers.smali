.class public interface abstract Lcom/android/sec/org/bouncycastle/asn1/cms/CMSObjectIdentifiers;
.super Ljava/lang/Object;
.source "CMSObjectIdentifiers.java"


# static fields
.field public static final authEnvelopedData:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

.field public static final authenticatedData:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

.field public static final compressedData:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

.field public static final data:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

.field public static final digestedData:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

.field public static final encryptedData:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

.field public static final envelopedData:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

.field public static final id_ri:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

.field public static final id_ri_ocsp_response:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

.field public static final id_ri_scvp:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

.field public static final signedAndEnvelopedData:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

.field public static final signedData:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

.field public static final timestampedData:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 8
    sget-object v0, Lcom/android/sec/org/bouncycastle/asn1/pkcs/PKCSObjectIdentifiers;->data:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    sput-object v0, Lcom/android/sec/org/bouncycastle/asn1/cms/CMSObjectIdentifiers;->data:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 9
    sget-object v0, Lcom/android/sec/org/bouncycastle/asn1/pkcs/PKCSObjectIdentifiers;->signedData:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    sput-object v0, Lcom/android/sec/org/bouncycastle/asn1/cms/CMSObjectIdentifiers;->signedData:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 10
    sget-object v0, Lcom/android/sec/org/bouncycastle/asn1/pkcs/PKCSObjectIdentifiers;->envelopedData:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    sput-object v0, Lcom/android/sec/org/bouncycastle/asn1/cms/CMSObjectIdentifiers;->envelopedData:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 11
    sget-object v0, Lcom/android/sec/org/bouncycastle/asn1/pkcs/PKCSObjectIdentifiers;->signedAndEnvelopedData:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    sput-object v0, Lcom/android/sec/org/bouncycastle/asn1/cms/CMSObjectIdentifiers;->signedAndEnvelopedData:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 12
    sget-object v0, Lcom/android/sec/org/bouncycastle/asn1/pkcs/PKCSObjectIdentifiers;->digestedData:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    sput-object v0, Lcom/android/sec/org/bouncycastle/asn1/cms/CMSObjectIdentifiers;->digestedData:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 13
    sget-object v0, Lcom/android/sec/org/bouncycastle/asn1/pkcs/PKCSObjectIdentifiers;->encryptedData:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    sput-object v0, Lcom/android/sec/org/bouncycastle/asn1/cms/CMSObjectIdentifiers;->encryptedData:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 14
    sget-object v0, Lcom/android/sec/org/bouncycastle/asn1/pkcs/PKCSObjectIdentifiers;->id_ct_authData:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    sput-object v0, Lcom/android/sec/org/bouncycastle/asn1/cms/CMSObjectIdentifiers;->authenticatedData:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 15
    sget-object v0, Lcom/android/sec/org/bouncycastle/asn1/pkcs/PKCSObjectIdentifiers;->id_ct_compressedData:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    sput-object v0, Lcom/android/sec/org/bouncycastle/asn1/cms/CMSObjectIdentifiers;->compressedData:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 16
    sget-object v0, Lcom/android/sec/org/bouncycastle/asn1/pkcs/PKCSObjectIdentifiers;->id_ct_authEnvelopedData:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    sput-object v0, Lcom/android/sec/org/bouncycastle/asn1/cms/CMSObjectIdentifiers;->authEnvelopedData:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 17
    sget-object v0, Lcom/android/sec/org/bouncycastle/asn1/pkcs/PKCSObjectIdentifiers;->id_ct_timestampedData:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    sput-object v0, Lcom/android/sec/org/bouncycastle/asn1/cms/CMSObjectIdentifiers;->timestampedData:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 24
    new-instance v0, Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    const-string v1, "1.3.6.1.5.5.7.16"

    invoke-direct {v0, v1}, Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/android/sec/org/bouncycastle/asn1/cms/CMSObjectIdentifiers;->id_ri:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 26
    sget-object v0, Lcom/android/sec/org/bouncycastle/asn1/cms/CMSObjectIdentifiers;->id_ri:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    const-string v1, "2"

    invoke-virtual {v0, v1}, Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;->branch(Ljava/lang/String;)Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    move-result-object v0

    sput-object v0, Lcom/android/sec/org/bouncycastle/asn1/cms/CMSObjectIdentifiers;->id_ri_ocsp_response:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 27
    sget-object v0, Lcom/android/sec/org/bouncycastle/asn1/cms/CMSObjectIdentifiers;->id_ri:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    const-string v1, "4"

    invoke-virtual {v0, v1}, Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;->branch(Ljava/lang/String;)Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    move-result-object v0

    sput-object v0, Lcom/android/sec/org/bouncycastle/asn1/cms/CMSObjectIdentifiers;->id_ri_scvp:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    return-void
.end method
