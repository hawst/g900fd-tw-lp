.class public Lcom/android/sec/org/bouncycastle/cms/CMSSignedDataGenerator;
.super Lcom/android/sec/org/bouncycastle/cms/CMSSignedGenerator;
.source "CMSSignedDataGenerator.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/sec/org/bouncycastle/cms/CMSSignedDataGenerator$SignerInf;
    }
.end annotation


# instance fields
.field private signerInfs:Ljava/util/List;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 143
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/cms/CMSSignedGenerator;-><init>()V

    .line 63
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedDataGenerator;->signerInfs:Ljava/util/List;

    .line 144
    return-void
.end method

.method public constructor <init>(Ljava/security/SecureRandom;)V
    .locals 1
    .param p1, "rand"    # Ljava/security/SecureRandom;

    .prologue
    .line 154
    invoke-direct {p0, p1}, Lcom/android/sec/org/bouncycastle/cms/CMSSignedGenerator;-><init>(Ljava/security/SecureRandom;)V

    .line 63
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedDataGenerator;->signerInfs:Ljava/util/List;

    .line 155
    return-void
.end method

.method private doAddSigner(Ljava/security/PrivateKey;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;Lcom/android/sec/org/bouncycastle/cms/CMSAttributeTableGenerator;Lcom/android/sec/org/bouncycastle/cms/CMSAttributeTableGenerator;Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;)V
    .locals 10
    .param p1, "key"    # Ljava/security/PrivateKey;
    .param p2, "signerIdentifier"    # Ljava/lang/Object;
    .param p3, "encryptionOID"    # Ljava/lang/String;
    .param p4, "digestOID"    # Ljava/lang/String;
    .param p5, "signedAttrGen"    # Lcom/android/sec/org/bouncycastle/cms/CMSAttributeTableGenerator;
    .param p6, "unsignedAttrGen"    # Lcom/android/sec/org/bouncycastle/cms/CMSAttributeTableGenerator;
    .param p7, "baseSignedTable"    # Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 394
    iget-object v9, p0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedDataGenerator;->signerInfs:Ljava/util/List;

    new-instance v0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedDataGenerator$SignerInf;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p4

    move-object v5, p3

    move-object v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcom/android/sec/org/bouncycastle/cms/CMSSignedDataGenerator$SignerInf;-><init>(Lcom/android/sec/org/bouncycastle/cms/CMSSignedDataGenerator;Ljava/security/PrivateKey;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;Lcom/android/sec/org/bouncycastle/cms/CMSAttributeTableGenerator;Lcom/android/sec/org/bouncycastle/cms/CMSAttributeTableGenerator;Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;)V

    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 396
    return-void
.end method


# virtual methods
.method public addSigner(Ljava/security/PrivateKey;Ljava/security/cert/X509Certificate;Ljava/lang/String;)V
    .locals 1
    .param p1, "key"    # Ljava/security/PrivateKey;
    .param p2, "cert"    # Ljava/security/cert/X509Certificate;
    .param p3, "digestOID"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 172
    invoke-virtual {p0, p1, p3}, Lcom/android/sec/org/bouncycastle/cms/CMSSignedDataGenerator;->getEncOID(Ljava/security/PrivateKey;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, p2, v0, p3}, Lcom/android/sec/org/bouncycastle/cms/CMSSignedDataGenerator;->addSigner(Ljava/security/PrivateKey;Ljava/security/cert/X509Certificate;Ljava/lang/String;Ljava/lang/String;)V

    .line 173
    return-void
.end method

.method public addSigner(Ljava/security/PrivateKey;Ljava/security/cert/X509Certificate;Ljava/lang/String;Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;)V
    .locals 7
    .param p1, "key"    # Ljava/security/PrivateKey;
    .param p2, "cert"    # Ljava/security/cert/X509Certificate;
    .param p3, "digestOID"    # Ljava/lang/String;
    .param p4, "signedAttr"    # Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;
    .param p5, "unsignedAttr"    # Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 244
    invoke-virtual {p0, p1, p3}, Lcom/android/sec/org/bouncycastle/cms/CMSSignedDataGenerator;->getEncOID(Ljava/security/PrivateKey;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-virtual/range {v0 .. v6}, Lcom/android/sec/org/bouncycastle/cms/CMSSignedDataGenerator;->addSigner(Ljava/security/PrivateKey;Ljava/security/cert/X509Certificate;Ljava/lang/String;Ljava/lang/String;Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;)V

    .line 245
    return-void
.end method

.method public addSigner(Ljava/security/PrivateKey;Ljava/security/cert/X509Certificate;Ljava/lang/String;Lcom/android/sec/org/bouncycastle/cms/CMSAttributeTableGenerator;Lcom/android/sec/org/bouncycastle/cms/CMSAttributeTableGenerator;)V
    .locals 7
    .param p1, "key"    # Ljava/security/PrivateKey;
    .param p2, "cert"    # Ljava/security/cert/X509Certificate;
    .param p3, "digestOID"    # Ljava/lang/String;
    .param p4, "signedAttrGen"    # Lcom/android/sec/org/bouncycastle/cms/CMSAttributeTableGenerator;
    .param p5, "unsignedAttrGen"    # Lcom/android/sec/org/bouncycastle/cms/CMSAttributeTableGenerator;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 331
    invoke-virtual {p0, p1, p3}, Lcom/android/sec/org/bouncycastle/cms/CMSSignedDataGenerator;->getEncOID(Ljava/security/PrivateKey;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-virtual/range {v0 .. v6}, Lcom/android/sec/org/bouncycastle/cms/CMSSignedDataGenerator;->addSigner(Ljava/security/PrivateKey;Ljava/security/cert/X509Certificate;Ljava/lang/String;Ljava/lang/String;Lcom/android/sec/org/bouncycastle/cms/CMSAttributeTableGenerator;Lcom/android/sec/org/bouncycastle/cms/CMSAttributeTableGenerator;)V

    .line 332
    return-void
.end method

.method public addSigner(Ljava/security/PrivateKey;Ljava/security/cert/X509Certificate;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8
    .param p1, "key"    # Ljava/security/PrivateKey;
    .param p2, "cert"    # Ljava/security/cert/X509Certificate;
    .param p3, "encryptionOID"    # Ljava/lang/String;
    .param p4, "digestOID"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 192
    new-instance v5, Lcom/android/sec/org/bouncycastle/cms/DefaultSignedAttributeTableGenerator;

    invoke-direct {v5}, Lcom/android/sec/org/bouncycastle/cms/DefaultSignedAttributeTableGenerator;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v7, v6

    invoke-direct/range {v0 .. v7}, Lcom/android/sec/org/bouncycastle/cms/CMSSignedDataGenerator;->doAddSigner(Ljava/security/PrivateKey;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;Lcom/android/sec/org/bouncycastle/cms/CMSAttributeTableGenerator;Lcom/android/sec/org/bouncycastle/cms/CMSAttributeTableGenerator;Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;)V

    .line 194
    return-void
.end method

.method public addSigner(Ljava/security/PrivateKey;Ljava/security/cert/X509Certificate;Ljava/lang/String;Ljava/lang/String;Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;)V
    .locals 8
    .param p1, "key"    # Ljava/security/PrivateKey;
    .param p2, "cert"    # Ljava/security/cert/X509Certificate;
    .param p3, "encryptionOID"    # Ljava/lang/String;
    .param p4, "digestOID"    # Ljava/lang/String;
    .param p5, "signedAttr"    # Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;
    .param p6, "unsignedAttr"    # Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 267
    new-instance v5, Lcom/android/sec/org/bouncycastle/cms/DefaultSignedAttributeTableGenerator;

    invoke-direct {v5, p5}, Lcom/android/sec/org/bouncycastle/cms/DefaultSignedAttributeTableGenerator;-><init>(Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;)V

    new-instance v6, Lcom/android/sec/org/bouncycastle/cms/SimpleAttributeTableGenerator;

    invoke-direct {v6, p6}, Lcom/android/sec/org/bouncycastle/cms/SimpleAttributeTableGenerator;-><init>(Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;)V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v7, p5

    invoke-direct/range {v0 .. v7}, Lcom/android/sec/org/bouncycastle/cms/CMSSignedDataGenerator;->doAddSigner(Ljava/security/PrivateKey;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;Lcom/android/sec/org/bouncycastle/cms/CMSAttributeTableGenerator;Lcom/android/sec/org/bouncycastle/cms/CMSAttributeTableGenerator;Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;)V

    .line 270
    return-void
.end method

.method public addSigner(Ljava/security/PrivateKey;Ljava/security/cert/X509Certificate;Ljava/lang/String;Ljava/lang/String;Lcom/android/sec/org/bouncycastle/cms/CMSAttributeTableGenerator;Lcom/android/sec/org/bouncycastle/cms/CMSAttributeTableGenerator;)V
    .locals 8
    .param p1, "key"    # Ljava/security/PrivateKey;
    .param p2, "cert"    # Ljava/security/cert/X509Certificate;
    .param p3, "encryptionOID"    # Ljava/lang/String;
    .param p4, "digestOID"    # Ljava/lang/String;
    .param p5, "signedAttrGen"    # Lcom/android/sec/org/bouncycastle/cms/CMSAttributeTableGenerator;
    .param p6, "unsignedAttrGen"    # Lcom/android/sec/org/bouncycastle/cms/CMSAttributeTableGenerator;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 347
    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v7}, Lcom/android/sec/org/bouncycastle/cms/CMSSignedDataGenerator;->doAddSigner(Ljava/security/PrivateKey;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;Lcom/android/sec/org/bouncycastle/cms/CMSAttributeTableGenerator;Lcom/android/sec/org/bouncycastle/cms/CMSAttributeTableGenerator;Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;)V

    .line 349
    return-void
.end method

.method public addSigner(Ljava/security/PrivateKey;[BLjava/lang/String;)V
    .locals 1
    .param p1, "key"    # Ljava/security/PrivateKey;
    .param p2, "subjectKeyID"    # [B
    .param p3, "digestOID"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 207
    invoke-virtual {p0, p1, p3}, Lcom/android/sec/org/bouncycastle/cms/CMSSignedDataGenerator;->getEncOID(Ljava/security/PrivateKey;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, p2, v0, p3}, Lcom/android/sec/org/bouncycastle/cms/CMSSignedDataGenerator;->addSigner(Ljava/security/PrivateKey;[BLjava/lang/String;Ljava/lang/String;)V

    .line 208
    return-void
.end method

.method public addSigner(Ljava/security/PrivateKey;[BLjava/lang/String;Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;)V
    .locals 7
    .param p1, "key"    # Ljava/security/PrivateKey;
    .param p2, "subjectKeyID"    # [B
    .param p3, "digestOID"    # Ljava/lang/String;
    .param p4, "signedAttr"    # Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;
    .param p5, "unsignedAttr"    # Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 290
    invoke-virtual {p0, p1, p3}, Lcom/android/sec/org/bouncycastle/cms/CMSSignedDataGenerator;->getEncOID(Ljava/security/PrivateKey;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-virtual/range {v0 .. v6}, Lcom/android/sec/org/bouncycastle/cms/CMSSignedDataGenerator;->addSigner(Ljava/security/PrivateKey;[BLjava/lang/String;Ljava/lang/String;Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;)V

    .line 292
    return-void
.end method

.method public addSigner(Ljava/security/PrivateKey;[BLjava/lang/String;Lcom/android/sec/org/bouncycastle/cms/CMSAttributeTableGenerator;Lcom/android/sec/org/bouncycastle/cms/CMSAttributeTableGenerator;)V
    .locals 7
    .param p1, "key"    # Ljava/security/PrivateKey;
    .param p2, "subjectKeyID"    # [B
    .param p3, "digestOID"    # Ljava/lang/String;
    .param p4, "signedAttrGen"    # Lcom/android/sec/org/bouncycastle/cms/CMSAttributeTableGenerator;
    .param p5, "unsignedAttrGen"    # Lcom/android/sec/org/bouncycastle/cms/CMSAttributeTableGenerator;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 363
    invoke-virtual {p0, p1, p3}, Lcom/android/sec/org/bouncycastle/cms/CMSSignedDataGenerator;->getEncOID(Ljava/security/PrivateKey;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-virtual/range {v0 .. v6}, Lcom/android/sec/org/bouncycastle/cms/CMSSignedDataGenerator;->addSigner(Ljava/security/PrivateKey;[BLjava/lang/String;Ljava/lang/String;Lcom/android/sec/org/bouncycastle/cms/CMSAttributeTableGenerator;Lcom/android/sec/org/bouncycastle/cms/CMSAttributeTableGenerator;)V

    .line 365
    return-void
.end method

.method public addSigner(Ljava/security/PrivateKey;[BLjava/lang/String;Ljava/lang/String;)V
    .locals 8
    .param p1, "key"    # Ljava/security/PrivateKey;
    .param p2, "subjectKeyID"    # [B
    .param p3, "encryptionOID"    # Ljava/lang/String;
    .param p4, "digestOID"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 222
    new-instance v5, Lcom/android/sec/org/bouncycastle/cms/DefaultSignedAttributeTableGenerator;

    invoke-direct {v5}, Lcom/android/sec/org/bouncycastle/cms/DefaultSignedAttributeTableGenerator;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v7, v6

    invoke-direct/range {v0 .. v7}, Lcom/android/sec/org/bouncycastle/cms/CMSSignedDataGenerator;->doAddSigner(Ljava/security/PrivateKey;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;Lcom/android/sec/org/bouncycastle/cms/CMSAttributeTableGenerator;Lcom/android/sec/org/bouncycastle/cms/CMSAttributeTableGenerator;Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;)V

    .line 224
    return-void
.end method

.method public addSigner(Ljava/security/PrivateKey;[BLjava/lang/String;Ljava/lang/String;Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;)V
    .locals 8
    .param p1, "key"    # Ljava/security/PrivateKey;
    .param p2, "subjectKeyID"    # [B
    .param p3, "encryptionOID"    # Ljava/lang/String;
    .param p4, "digestOID"    # Ljava/lang/String;
    .param p5, "signedAttr"    # Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;
    .param p6, "unsignedAttr"    # Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 314
    new-instance v5, Lcom/android/sec/org/bouncycastle/cms/DefaultSignedAttributeTableGenerator;

    invoke-direct {v5, p5}, Lcom/android/sec/org/bouncycastle/cms/DefaultSignedAttributeTableGenerator;-><init>(Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;)V

    new-instance v6, Lcom/android/sec/org/bouncycastle/cms/SimpleAttributeTableGenerator;

    invoke-direct {v6, p6}, Lcom/android/sec/org/bouncycastle/cms/SimpleAttributeTableGenerator;-><init>(Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;)V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v7, p5

    invoke-direct/range {v0 .. v7}, Lcom/android/sec/org/bouncycastle/cms/CMSSignedDataGenerator;->doAddSigner(Ljava/security/PrivateKey;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;Lcom/android/sec/org/bouncycastle/cms/CMSAttributeTableGenerator;Lcom/android/sec/org/bouncycastle/cms/CMSAttributeTableGenerator;Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;)V

    .line 317
    return-void
.end method

.method public addSigner(Ljava/security/PrivateKey;[BLjava/lang/String;Ljava/lang/String;Lcom/android/sec/org/bouncycastle/cms/CMSAttributeTableGenerator;Lcom/android/sec/org/bouncycastle/cms/CMSAttributeTableGenerator;)V
    .locals 8
    .param p1, "key"    # Ljava/security/PrivateKey;
    .param p2, "subjectKeyID"    # [B
    .param p3, "encryptionOID"    # Ljava/lang/String;
    .param p4, "digestOID"    # Ljava/lang/String;
    .param p5, "signedAttrGen"    # Lcom/android/sec/org/bouncycastle/cms/CMSAttributeTableGenerator;
    .param p6, "unsignedAttrGen"    # Lcom/android/sec/org/bouncycastle/cms/CMSAttributeTableGenerator;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 380
    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v7}, Lcom/android/sec/org/bouncycastle/cms/CMSSignedDataGenerator;->doAddSigner(Ljava/security/PrivateKey;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;Lcom/android/sec/org/bouncycastle/cms/CMSAttributeTableGenerator;Lcom/android/sec/org/bouncycastle/cms/CMSAttributeTableGenerator;Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;)V

    .line 382
    return-void
.end method

.method public generate(Lcom/android/sec/org/bouncycastle/cms/CMSProcessable;Ljava/lang/String;)Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;
    .locals 1
    .param p1, "content"    # Lcom/android/sec/org/bouncycastle/cms/CMSProcessable;
    .param p2, "sigProvider"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/NoSuchAlgorithmException;,
            Ljava/security/NoSuchProviderException;,
            Lcom/android/sec/org/bouncycastle/cms/CMSException;
        }
    .end annotation

    .prologue
    .line 408
    invoke-static {p2}, Lcom/android/sec/org/bouncycastle/cms/CMSUtils;->getProvider(Ljava/lang/String;)Ljava/security/Provider;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/android/sec/org/bouncycastle/cms/CMSSignedDataGenerator;->generate(Lcom/android/sec/org/bouncycastle/cms/CMSProcessable;Ljava/security/Provider;)Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;

    move-result-object v0

    return-object v0
.end method

.method public generate(Lcom/android/sec/org/bouncycastle/cms/CMSProcessable;Ljava/security/Provider;)Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;
    .locals 1
    .param p1, "content"    # Lcom/android/sec/org/bouncycastle/cms/CMSProcessable;
    .param p2, "sigProvider"    # Ljava/security/Provider;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/NoSuchAlgorithmException;,
            Lcom/android/sec/org/bouncycastle/cms/CMSException;
        }
    .end annotation

    .prologue
    .line 421
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, p2}, Lcom/android/sec/org/bouncycastle/cms/CMSSignedDataGenerator;->generate(Lcom/android/sec/org/bouncycastle/cms/CMSProcessable;ZLjava/security/Provider;)Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;

    move-result-object v0

    return-object v0
.end method

.method public generate(Lcom/android/sec/org/bouncycastle/cms/CMSProcessable;ZLjava/lang/String;)Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;
    .locals 1
    .param p1, "content"    # Lcom/android/sec/org/bouncycastle/cms/CMSProcessable;
    .param p2, "encapsulate"    # Z
    .param p3, "sigProvider"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/NoSuchAlgorithmException;,
            Ljava/security/NoSuchProviderException;,
            Lcom/android/sec/org/bouncycastle/cms/CMSException;
        }
    .end annotation

    .prologue
    .line 564
    instance-of v0, p1, Lcom/android/sec/org/bouncycastle/cms/CMSTypedData;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 566
    check-cast v0, Lcom/android/sec/org/bouncycastle/cms/CMSTypedData;

    invoke-interface {v0}, Lcom/android/sec/org/bouncycastle/cms/CMSTypedData;->getContentType()Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p1, p2, p3}, Lcom/android/sec/org/bouncycastle/cms/CMSSignedDataGenerator;->generate(Ljava/lang/String;Lcom/android/sec/org/bouncycastle/cms/CMSProcessable;ZLjava/lang/String;)Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;

    move-result-object v0

    .line 570
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedDataGenerator;->DATA:Ljava/lang/String;

    invoke-virtual {p0, v0, p1, p2, p3}, Lcom/android/sec/org/bouncycastle/cms/CMSSignedDataGenerator;->generate(Ljava/lang/String;Lcom/android/sec/org/bouncycastle/cms/CMSProcessable;ZLjava/lang/String;)Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;

    move-result-object v0

    goto :goto_0
.end method

.method public generate(Lcom/android/sec/org/bouncycastle/cms/CMSProcessable;ZLjava/security/Provider;)Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;
    .locals 1
    .param p1, "content"    # Lcom/android/sec/org/bouncycastle/cms/CMSProcessable;
    .param p2, "encapsulate"    # Z
    .param p3, "sigProvider"    # Ljava/security/Provider;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/NoSuchAlgorithmException;,
            Lcom/android/sec/org/bouncycastle/cms/CMSException;
        }
    .end annotation

    .prologue
    .line 587
    instance-of v0, p1, Lcom/android/sec/org/bouncycastle/cms/CMSTypedData;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 589
    check-cast v0, Lcom/android/sec/org/bouncycastle/cms/CMSTypedData;

    invoke-interface {v0}, Lcom/android/sec/org/bouncycastle/cms/CMSTypedData;->getContentType()Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p1, p2, p3}, Lcom/android/sec/org/bouncycastle/cms/CMSSignedDataGenerator;->generate(Ljava/lang/String;Lcom/android/sec/org/bouncycastle/cms/CMSProcessable;ZLjava/security/Provider;)Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;

    move-result-object v0

    .line 593
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedDataGenerator;->DATA:Ljava/lang/String;

    invoke-virtual {p0, v0, p1, p2, p3}, Lcom/android/sec/org/bouncycastle/cms/CMSSignedDataGenerator;->generate(Ljava/lang/String;Lcom/android/sec/org/bouncycastle/cms/CMSProcessable;ZLjava/security/Provider;)Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;

    move-result-object v0

    goto :goto_0
.end method

.method public generate(Lcom/android/sec/org/bouncycastle/cms/CMSTypedData;)Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;
    .locals 1
    .param p1, "content"    # Lcom/android/sec/org/bouncycastle/cms/CMSTypedData;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/cms/CMSException;
        }
    .end annotation

    .prologue
    .line 601
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/android/sec/org/bouncycastle/cms/CMSSignedDataGenerator;->generate(Lcom/android/sec/org/bouncycastle/cms/CMSTypedData;Z)Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;

    move-result-object v0

    return-object v0
.end method

.method public generate(Lcom/android/sec/org/bouncycastle/cms/CMSTypedData;Z)Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;
    .locals 21
    .param p1, "content"    # Lcom/android/sec/org/bouncycastle/cms/CMSTypedData;
    .param p2, "encapsulate"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/cms/CMSException;
        }
    .end annotation

    .prologue
    .line 610
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedDataGenerator;->signerInfs:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 612
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v6, "this method can only be used with SignerInfoGenerator"

    invoke-direct {v2, v6}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 645
    :cond_0
    new-instance v12, Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;

    invoke-direct {v12}, Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;-><init>()V

    .line 646
    .local v12, "digestAlgs":Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;
    new-instance v19, Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;

    invoke-direct/range {v19 .. v19}, Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;-><init>()V

    .line 648
    .local v19, "signerInfos":Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedDataGenerator;->digests:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->clear()V

    .line 653
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedDataGenerator;->_signers:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v15

    .local v15, "it":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 655
    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/android/sec/org/bouncycastle/cms/SignerInformation;

    .line 656
    .local v18, "signer":Lcom/android/sec/org/bouncycastle/cms/SignerInformation;
    sget-object v2, Lcom/android/sec/org/bouncycastle/cms/CMSSignedHelper;->INSTANCE:Lcom/android/sec/org/bouncycastle/cms/CMSSignedHelper;

    invoke-virtual/range {v18 .. v18}, Lcom/android/sec/org/bouncycastle/cms/SignerInformation;->getDigestAlgorithmID()Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;

    move-result-object v6

    invoke-virtual {v2, v6}, Lcom/android/sec/org/bouncycastle/cms/CMSSignedHelper;->fixAlgID(Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;)Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;

    move-result-object v2

    invoke-virtual {v12, v2}, Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;->add(Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;)V

    .line 659
    invoke-virtual/range {v18 .. v18}, Lcom/android/sec/org/bouncycastle/cms/SignerInformation;->toASN1Structure()Lcom/android/sec/org/bouncycastle/asn1/cms/SignerInfo;

    move-result-object v2

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;->add(Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;)V

    goto :goto_0

    .line 665
    .end local v18    # "signer":Lcom/android/sec/org/bouncycastle/cms/SignerInformation;
    :cond_1
    invoke-interface/range {p1 .. p1}, Lcom/android/sec/org/bouncycastle/cms/CMSTypedData;->getContentType()Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    move-result-object v11

    .line 667
    .local v11, "contentTypeOID":Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;
    const/16 v16, 0x0

    .line 669
    .local v16, "octs":Lcom/android/sec/org/bouncycastle/asn1/ASN1OctetString;
    if-eqz p1, :cond_3

    .line 671
    const/4 v7, 0x0

    .line 673
    .local v7, "bOut":Ljava/io/ByteArrayOutputStream;
    if-eqz p2, :cond_2

    .line 675
    new-instance v7, Ljava/io/ByteArrayOutputStream;

    .end local v7    # "bOut":Ljava/io/ByteArrayOutputStream;
    invoke-direct {v7}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 678
    .restart local v7    # "bOut":Ljava/io/ByteArrayOutputStream;
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedDataGenerator;->signerGens:Ljava/util/List;

    invoke-static {v2, v7}, Lcom/android/sec/org/bouncycastle/cms/CMSUtils;->attachSignersToOutputStream(Ljava/util/Collection;Ljava/io/OutputStream;)Ljava/io/OutputStream;

    move-result-object v8

    .line 681
    .local v8, "cOut":Ljava/io/OutputStream;
    invoke-static {v8}, Lcom/android/sec/org/bouncycastle/cms/CMSUtils;->getSafeOutputStream(Ljava/io/OutputStream;)Ljava/io/OutputStream;

    move-result-object v8

    .line 685
    :try_start_0
    move-object/from16 v0, p1

    invoke-interface {v0, v8}, Lcom/android/sec/org/bouncycastle/cms/CMSTypedData;->write(Ljava/io/OutputStream;)V

    .line 687
    invoke-virtual {v8}, Ljava/io/OutputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 694
    if-eqz p2, :cond_3

    .line 696
    new-instance v16, Lcom/android/sec/org/bouncycastle/asn1/BEROctetString;

    .end local v16    # "octs":Lcom/android/sec/org/bouncycastle/asn1/ASN1OctetString;
    invoke-virtual {v7}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v2

    move-object/from16 v0, v16

    invoke-direct {v0, v2}, Lcom/android/sec/org/bouncycastle/asn1/BEROctetString;-><init>([B)V

    .line 700
    .end local v7    # "bOut":Ljava/io/ByteArrayOutputStream;
    .end local v8    # "cOut":Ljava/io/OutputStream;
    .restart local v16    # "octs":Lcom/android/sec/org/bouncycastle/asn1/ASN1OctetString;
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedDataGenerator;->signerGens:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v15

    :cond_4
    :goto_1
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 702
    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lcom/android/sec/org/bouncycastle/cms/SignerInfoGenerator;

    .line 703
    .local v17, "sGen":Lcom/android/sec/org/bouncycastle/cms/SignerInfoGenerator;
    move-object/from16 v0, v17

    invoke-virtual {v0, v11}, Lcom/android/sec/org/bouncycastle/cms/SignerInfoGenerator;->generate(Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;)Lcom/android/sec/org/bouncycastle/asn1/cms/SignerInfo;

    move-result-object v14

    .line 705
    .local v14, "inf":Lcom/android/sec/org/bouncycastle/asn1/cms/SignerInfo;
    invoke-virtual {v14}, Lcom/android/sec/org/bouncycastle/asn1/cms/SignerInfo;->getDigestAlgorithm()Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;

    move-result-object v2

    invoke-virtual {v12, v2}, Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;->add(Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;)V

    .line 706
    move-object/from16 v0, v19

    invoke-virtual {v0, v14}, Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;->add(Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;)V

    .line 708
    invoke-virtual/range {v17 .. v17}, Lcom/android/sec/org/bouncycastle/cms/SignerInfoGenerator;->getCalculatedDigest()[B

    move-result-object v9

    .line 710
    .local v9, "calcDigest":[B
    if-eqz v9, :cond_4

    .line 712
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedDataGenerator;->digests:Ljava/util/Map;

    invoke-virtual {v14}, Lcom/android/sec/org/bouncycastle/asn1/cms/SignerInfo;->getDigestAlgorithm()Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;

    move-result-object v6

    invoke-virtual {v6}, Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;->getAlgorithm()Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    move-result-object v6

    invoke-virtual {v6}, Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;->getId()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v2, v6, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 689
    .end local v9    # "calcDigest":[B
    .end local v14    # "inf":Lcom/android/sec/org/bouncycastle/asn1/cms/SignerInfo;
    .end local v17    # "sGen":Lcom/android/sec/org/bouncycastle/cms/SignerInfoGenerator;
    .restart local v7    # "bOut":Ljava/io/ByteArrayOutputStream;
    .restart local v8    # "cOut":Ljava/io/OutputStream;
    :catch_0
    move-exception v13

    .line 691
    .local v13, "e":Ljava/io/IOException;
    new-instance v2, Lcom/android/sec/org/bouncycastle/cms/CMSException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "data processing exception: "

    move-object/from16 v0, v20

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v13}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v2, v6, v13}, Lcom/android/sec/org/bouncycastle/cms/CMSException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v2

    .line 716
    .end local v7    # "bOut":Ljava/io/ByteArrayOutputStream;
    .end local v8    # "cOut":Ljava/io/OutputStream;
    .end local v13    # "e":Ljava/io/IOException;
    :cond_5
    const/4 v4, 0x0

    .line 718
    .local v4, "certificates":Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedDataGenerator;->certs:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-eqz v2, :cond_6

    .line 720
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedDataGenerator;->certs:Ljava/util/List;

    invoke-static {v2}, Lcom/android/sec/org/bouncycastle/cms/CMSUtils;->createBerSetFromList(Ljava/util/List;)Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;

    move-result-object v4

    .line 723
    :cond_6
    const/4 v5, 0x0

    .line 725
    .local v5, "certrevlist":Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedDataGenerator;->crls:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-eqz v2, :cond_7

    .line 727
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedDataGenerator;->crls:Ljava/util/List;

    invoke-static {v2}, Lcom/android/sec/org/bouncycastle/cms/CMSUtils;->createBerSetFromList(Ljava/util/List;)Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;

    move-result-object v5

    .line 730
    :cond_7
    new-instance v3, Lcom/android/sec/org/bouncycastle/asn1/cms/ContentInfo;

    move-object/from16 v0, v16

    invoke-direct {v3, v11, v0}, Lcom/android/sec/org/bouncycastle/asn1/cms/ContentInfo;-><init>(Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;)V

    .line 732
    .local v3, "encInfo":Lcom/android/sec/org/bouncycastle/asn1/cms/ContentInfo;
    new-instance v1, Lcom/android/sec/org/bouncycastle/asn1/cms/SignedData;

    new-instance v2, Lcom/android/sec/org/bouncycastle/asn1/DERSet;

    invoke-direct {v2, v12}, Lcom/android/sec/org/bouncycastle/asn1/DERSet;-><init>(Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;)V

    new-instance v6, Lcom/android/sec/org/bouncycastle/asn1/DERSet;

    move-object/from16 v0, v19

    invoke-direct {v6, v0}, Lcom/android/sec/org/bouncycastle/asn1/DERSet;-><init>(Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;)V

    invoke-direct/range {v1 .. v6}, Lcom/android/sec/org/bouncycastle/asn1/cms/SignedData;-><init>(Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;Lcom/android/sec/org/bouncycastle/asn1/cms/ContentInfo;Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;)V

    .line 739
    .local v1, "sd":Lcom/android/sec/org/bouncycastle/asn1/cms/SignedData;
    new-instance v10, Lcom/android/sec/org/bouncycastle/asn1/cms/ContentInfo;

    sget-object v2, Lcom/android/sec/org/bouncycastle/asn1/cms/CMSObjectIdentifiers;->signedData:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    invoke-direct {v10, v2, v1}, Lcom/android/sec/org/bouncycastle/asn1/cms/ContentInfo;-><init>(Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;)V

    .line 742
    .local v10, "contentInfo":Lcom/android/sec/org/bouncycastle/asn1/cms/ContentInfo;
    new-instance v2, Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;

    move-object/from16 v0, p1

    invoke-direct {v2, v0, v10}, Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;-><init>(Lcom/android/sec/org/bouncycastle/cms/CMSProcessable;Lcom/android/sec/org/bouncycastle/asn1/cms/ContentInfo;)V

    return-object v2
.end method

.method public generate(Ljava/lang/String;Lcom/android/sec/org/bouncycastle/cms/CMSProcessable;ZLjava/lang/String;)Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;
    .locals 6
    .param p1, "eContentType"    # Ljava/lang/String;
    .param p2, "content"    # Lcom/android/sec/org/bouncycastle/cms/CMSProcessable;
    .param p3, "encapsulate"    # Z
    .param p4, "sigProvider"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/NoSuchAlgorithmException;,
            Ljava/security/NoSuchProviderException;,
            Lcom/android/sec/org/bouncycastle/cms/CMSException;
        }
    .end annotation

    .prologue
    .line 438
    invoke-static {p4}, Lcom/android/sec/org/bouncycastle/cms/CMSUtils;->getProvider(Ljava/lang/String;)Ljava/security/Provider;

    move-result-object v4

    const/4 v5, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    invoke-virtual/range {v0 .. v5}, Lcom/android/sec/org/bouncycastle/cms/CMSSignedDataGenerator;->generate(Ljava/lang/String;Lcom/android/sec/org/bouncycastle/cms/CMSProcessable;ZLjava/security/Provider;Z)Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;

    move-result-object v0

    return-object v0
.end method

.method public generate(Ljava/lang/String;Lcom/android/sec/org/bouncycastle/cms/CMSProcessable;ZLjava/lang/String;Z)Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;
    .locals 6
    .param p1, "eContentType"    # Ljava/lang/String;
    .param p2, "content"    # Lcom/android/sec/org/bouncycastle/cms/CMSProcessable;
    .param p3, "encapsulate"    # Z
    .param p4, "sigProvider"    # Ljava/lang/String;
    .param p5, "addDefaultAttributes"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/NoSuchAlgorithmException;,
            Ljava/security/NoSuchProviderException;,
            Lcom/android/sec/org/bouncycastle/cms/CMSException;
        }
    .end annotation

    .prologue
    .line 474
    invoke-static {p4}, Lcom/android/sec/org/bouncycastle/cms/CMSUtils;->getProvider(Ljava/lang/String;)Ljava/security/Provider;

    move-result-object v4

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/android/sec/org/bouncycastle/cms/CMSSignedDataGenerator;->generate(Ljava/lang/String;Lcom/android/sec/org/bouncycastle/cms/CMSProcessable;ZLjava/security/Provider;Z)Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;

    move-result-object v0

    return-object v0
.end method

.method public generate(Ljava/lang/String;Lcom/android/sec/org/bouncycastle/cms/CMSProcessable;ZLjava/security/Provider;)Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;
    .locals 6
    .param p1, "eContentType"    # Ljava/lang/String;
    .param p2, "content"    # Lcom/android/sec/org/bouncycastle/cms/CMSProcessable;
    .param p3, "encapsulate"    # Z
    .param p4, "sigProvider"    # Ljava/security/Provider;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/NoSuchAlgorithmException;,
            Lcom/android/sec/org/bouncycastle/cms/CMSException;
        }
    .end annotation

    .prologue
    .line 456
    const/4 v5, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/android/sec/org/bouncycastle/cms/CMSSignedDataGenerator;->generate(Ljava/lang/String;Lcom/android/sec/org/bouncycastle/cms/CMSProcessable;ZLjava/security/Provider;Z)Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;

    move-result-object v0

    return-object v0
.end method

.method public generate(Ljava/lang/String;Lcom/android/sec/org/bouncycastle/cms/CMSProcessable;ZLjava/security/Provider;Z)Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;
    .locals 7
    .param p1, "eContentType"    # Ljava/lang/String;
    .param p2, "content"    # Lcom/android/sec/org/bouncycastle/cms/CMSProcessable;
    .param p3, "encapsulate"    # Z
    .param p4, "sigProvider"    # Ljava/security/Provider;
    .param p5, "addDefaultAttributes"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/NoSuchAlgorithmException;,
            Lcom/android/sec/org/bouncycastle/cms/CMSException;
        }
    .end annotation

    .prologue
    .line 493
    if-nez p1, :cond_0

    const/4 v2, 0x1

    .line 495
    .local v2, "isCounterSignature":Z
    :goto_0
    if-eqz v2, :cond_1

    const/4 v0, 0x0

    .line 499
    .local v0, "contentTypeOID":Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;
    :goto_1
    iget-object v5, p0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedDataGenerator;->signerInfs:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "it":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 501
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/sec/org/bouncycastle/cms/CMSSignedDataGenerator$SignerInf;

    .line 505
    .local v4, "signer":Lcom/android/sec/org/bouncycastle/cms/CMSSignedDataGenerator$SignerInf;
    :try_start_0
    iget-object v5, p0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedDataGenerator;->signerGens:Ljava/util/List;

    iget-object v6, p0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedDataGenerator;->rand:Ljava/security/SecureRandom;

    invoke-virtual {v4, v6, p4, p5}, Lcom/android/sec/org/bouncycastle/cms/CMSSignedDataGenerator$SignerInf;->toSignerInfoGenerator(Ljava/security/SecureRandom;Ljava/security/Provider;Z)Lcom/android/sec/org/bouncycastle/cms/SignerInfoGenerator;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lcom/android/sec/org/bouncycastle/operator/OperatorCreationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/security/cert/CertificateEncodingException; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_2

    .line 508
    :catch_0
    move-exception v1

    .line 510
    .local v1, "e":Lcom/android/sec/org/bouncycastle/operator/OperatorCreationException;
    new-instance v5, Lcom/android/sec/org/bouncycastle/cms/CMSException;

    const-string v6, "exception creating signerInf"

    invoke-direct {v5, v6, v1}, Lcom/android/sec/org/bouncycastle/cms/CMSException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v5

    .line 493
    .end local v0    # "contentTypeOID":Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;
    .end local v1    # "e":Lcom/android/sec/org/bouncycastle/operator/OperatorCreationException;
    .end local v2    # "isCounterSignature":Z
    .end local v3    # "it":Ljava/util/Iterator;
    .end local v4    # "signer":Lcom/android/sec/org/bouncycastle/cms/CMSSignedDataGenerator$SignerInf;
    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    .line 495
    .restart local v2    # "isCounterSignature":Z
    :cond_1
    new-instance v0, Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    invoke-direct {v0, p1}, Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 512
    .restart local v0    # "contentTypeOID":Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;
    .restart local v3    # "it":Ljava/util/Iterator;
    .restart local v4    # "signer":Lcom/android/sec/org/bouncycastle/cms/CMSSignedDataGenerator$SignerInf;
    :catch_1
    move-exception v1

    .line 514
    .local v1, "e":Ljava/io/IOException;
    new-instance v5, Lcom/android/sec/org/bouncycastle/cms/CMSException;

    const-string v6, "exception encoding attributes"

    invoke-direct {v5, v6, v1}, Lcom/android/sec/org/bouncycastle/cms/CMSException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v5

    .line 516
    .end local v1    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v1

    .line 518
    .local v1, "e":Ljava/security/cert/CertificateEncodingException;
    new-instance v5, Lcom/android/sec/org/bouncycastle/cms/CMSException;

    const-string v6, "error creating sid."

    invoke-direct {v5, v6, v1}, Lcom/android/sec/org/bouncycastle/cms/CMSException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v5

    .line 522
    .end local v1    # "e":Ljava/security/cert/CertificateEncodingException;
    .end local v4    # "signer":Lcom/android/sec/org/bouncycastle/cms/CMSSignedDataGenerator$SignerInf;
    :cond_2
    iget-object v5, p0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedDataGenerator;->signerInfs:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->clear()V

    .line 524
    if-eqz p2, :cond_3

    .line 526
    new-instance v5, Lcom/android/sec/org/bouncycastle/cms/CMSSignedDataGenerator$1;

    invoke-direct {v5, p0, v0, p2}, Lcom/android/sec/org/bouncycastle/cms/CMSSignedDataGenerator$1;-><init>(Lcom/android/sec/org/bouncycastle/cms/CMSSignedDataGenerator;Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;Lcom/android/sec/org/bouncycastle/cms/CMSProcessable;)V

    invoke-virtual {p0, v5, p3}, Lcom/android/sec/org/bouncycastle/cms/CMSSignedDataGenerator;->generate(Lcom/android/sec/org/bouncycastle/cms/CMSTypedData;Z)Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;

    move-result-object v5

    .line 547
    :goto_3
    return-object v5

    :cond_3
    new-instance v5, Lcom/android/sec/org/bouncycastle/cms/CMSAbsentContent;

    invoke-direct {v5, v0}, Lcom/android/sec/org/bouncycastle/cms/CMSAbsentContent;-><init>(Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;)V

    invoke-virtual {p0, v5, p3}, Lcom/android/sec/org/bouncycastle/cms/CMSSignedDataGenerator;->generate(Lcom/android/sec/org/bouncycastle/cms/CMSTypedData;Z)Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;

    move-result-object v5

    goto :goto_3
.end method

.method public generateCounterSigners(Lcom/android/sec/org/bouncycastle/cms/SignerInformation;)Lcom/android/sec/org/bouncycastle/cms/SignerInformationStore;
    .locals 3
    .param p1, "signer"    # Lcom/android/sec/org/bouncycastle/cms/SignerInformation;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/cms/CMSException;
        }
    .end annotation

    .prologue
    .line 785
    new-instance v0, Lcom/android/sec/org/bouncycastle/cms/CMSProcessableByteArray;

    const/4 v1, 0x0

    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/cms/SignerInformation;->getSignature()[B

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/android/sec/org/bouncycastle/cms/CMSProcessableByteArray;-><init>(Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;[B)V

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/android/sec/org/bouncycastle/cms/CMSSignedDataGenerator;->generate(Lcom/android/sec/org/bouncycastle/cms/CMSTypedData;Z)Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;->getSignerInfos()Lcom/android/sec/org/bouncycastle/cms/SignerInformationStore;

    move-result-object v0

    return-object v0
.end method

.method public generateCounterSigners(Lcom/android/sec/org/bouncycastle/cms/SignerInformation;Ljava/lang/String;)Lcom/android/sec/org/bouncycastle/cms/SignerInformationStore;
    .locals 4
    .param p1, "signer"    # Lcom/android/sec/org/bouncycastle/cms/SignerInformation;
    .param p2, "sigProvider"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/NoSuchAlgorithmException;,
            Ljava/security/NoSuchProviderException;,
            Lcom/android/sec/org/bouncycastle/cms/CMSException;
        }
    .end annotation

    .prologue
    .line 772
    const/4 v0, 0x0

    new-instance v1, Lcom/android/sec/org/bouncycastle/cms/CMSProcessableByteArray;

    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/cms/SignerInformation;->getSignature()[B

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/android/sec/org/bouncycastle/cms/CMSProcessableByteArray;-><init>([B)V

    const/4 v2, 0x0

    invoke-static {p2}, Lcom/android/sec/org/bouncycastle/cms/CMSUtils;->getProvider(Ljava/lang/String;)Ljava/security/Provider;

    move-result-object v3

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/android/sec/org/bouncycastle/cms/CMSSignedDataGenerator;->generate(Ljava/lang/String;Lcom/android/sec/org/bouncycastle/cms/CMSProcessable;ZLjava/security/Provider;)Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;->getSignerInfos()Lcom/android/sec/org/bouncycastle/cms/SignerInformationStore;

    move-result-object v0

    return-object v0
.end method

.method public generateCounterSigners(Lcom/android/sec/org/bouncycastle/cms/SignerInformation;Ljava/security/Provider;)Lcom/android/sec/org/bouncycastle/cms/SignerInformationStore;
    .locals 3
    .param p1, "signer"    # Lcom/android/sec/org/bouncycastle/cms/SignerInformation;
    .param p2, "sigProvider"    # Ljava/security/Provider;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/NoSuchAlgorithmException;,
            Lcom/android/sec/org/bouncycastle/cms/CMSException;
        }
    .end annotation

    .prologue
    .line 757
    const/4 v0, 0x0

    new-instance v1, Lcom/android/sec/org/bouncycastle/cms/CMSProcessableByteArray;

    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/cms/SignerInformation;->getSignature()[B

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/android/sec/org/bouncycastle/cms/CMSProcessableByteArray;-><init>([B)V

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2, p2}, Lcom/android/sec/org/bouncycastle/cms/CMSSignedDataGenerator;->generate(Ljava/lang/String;Lcom/android/sec/org/bouncycastle/cms/CMSProcessable;ZLjava/security/Provider;)Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;->getSignerInfos()Lcom/android/sec/org/bouncycastle/cms/SignerInformationStore;

    move-result-object v0

    return-object v0
.end method
