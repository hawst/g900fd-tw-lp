.class public Lcom/android/sec/org/bouncycastle/bcpg/OnePassSignaturePacket;
.super Lcom/android/sec/org/bouncycastle/bcpg/ContainedPacket;
.source "OnePassSignaturePacket.java"


# instance fields
.field private hashAlgorithm:I

.field private keyAlgorithm:I

.field private keyID:J

.field private nested:I

.field private sigType:I

.field private version:I


# direct methods
.method public constructor <init>(IIIJZ)V
    .locals 2
    .param p1, "sigType"    # I
    .param p2, "hashAlgorithm"    # I
    .param p3, "keyAlgorithm"    # I
    .param p4, "keyID"    # J
    .param p6, "isNested"    # Z

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/bcpg/ContainedPacket;-><init>()V

    .line 46
    const/4 v0, 0x3

    iput v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/OnePassSignaturePacket;->version:I

    .line 47
    iput p1, p0, Lcom/android/sec/org/bouncycastle/bcpg/OnePassSignaturePacket;->sigType:I

    .line 48
    iput p2, p0, Lcom/android/sec/org/bouncycastle/bcpg/OnePassSignaturePacket;->hashAlgorithm:I

    .line 49
    iput p3, p0, Lcom/android/sec/org/bouncycastle/bcpg/OnePassSignaturePacket;->keyAlgorithm:I

    .line 50
    iput-wide p4, p0, Lcom/android/sec/org/bouncycastle/bcpg/OnePassSignaturePacket;->keyID:J

    .line 51
    if-eqz p6, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/OnePassSignaturePacket;->nested:I

    .line 52
    return-void

    .line 51
    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method constructor <init>(Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;)V
    .locals 5
    .param p1, "in"    # Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/bcpg/ContainedPacket;-><init>()V

    .line 22
    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->read()I

    move-result v0

    iput v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/OnePassSignaturePacket;->version:I

    .line 23
    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->read()I

    move-result v0

    iput v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/OnePassSignaturePacket;->sigType:I

    .line 24
    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->read()I

    move-result v0

    iput v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/OnePassSignaturePacket;->hashAlgorithm:I

    .line 25
    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->read()I

    move-result v0

    iput v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/OnePassSignaturePacket;->keyAlgorithm:I

    .line 27
    iget-wide v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/OnePassSignaturePacket;->keyID:J

    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->read()I

    move-result v2

    int-to-long v2, v2

    const/16 v4, 0x38

    shl-long/2addr v2, v4

    or-long/2addr v0, v2

    iput-wide v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/OnePassSignaturePacket;->keyID:J

    .line 28
    iget-wide v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/OnePassSignaturePacket;->keyID:J

    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->read()I

    move-result v2

    int-to-long v2, v2

    const/16 v4, 0x30

    shl-long/2addr v2, v4

    or-long/2addr v0, v2

    iput-wide v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/OnePassSignaturePacket;->keyID:J

    .line 29
    iget-wide v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/OnePassSignaturePacket;->keyID:J

    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->read()I

    move-result v2

    int-to-long v2, v2

    const/16 v4, 0x28

    shl-long/2addr v2, v4

    or-long/2addr v0, v2

    iput-wide v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/OnePassSignaturePacket;->keyID:J

    .line 30
    iget-wide v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/OnePassSignaturePacket;->keyID:J

    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->read()I

    move-result v2

    int-to-long v2, v2

    const/16 v4, 0x20

    shl-long/2addr v2, v4

    or-long/2addr v0, v2

    iput-wide v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/OnePassSignaturePacket;->keyID:J

    .line 31
    iget-wide v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/OnePassSignaturePacket;->keyID:J

    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->read()I

    move-result v2

    int-to-long v2, v2

    const/16 v4, 0x18

    shl-long/2addr v2, v4

    or-long/2addr v0, v2

    iput-wide v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/OnePassSignaturePacket;->keyID:J

    .line 32
    iget-wide v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/OnePassSignaturePacket;->keyID:J

    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->read()I

    move-result v2

    int-to-long v2, v2

    const/16 v4, 0x10

    shl-long/2addr v2, v4

    or-long/2addr v0, v2

    iput-wide v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/OnePassSignaturePacket;->keyID:J

    .line 33
    iget-wide v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/OnePassSignaturePacket;->keyID:J

    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->read()I

    move-result v2

    int-to-long v2, v2

    const/16 v4, 0x8

    shl-long/2addr v2, v4

    or-long/2addr v0, v2

    iput-wide v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/OnePassSignaturePacket;->keyID:J

    .line 34
    iget-wide v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/OnePassSignaturePacket;->keyID:J

    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->read()I

    move-result v2

    int-to-long v2, v2

    or-long/2addr v0, v2

    iput-wide v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/OnePassSignaturePacket;->keyID:J

    .line 36
    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->read()I

    move-result v0

    iput v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/OnePassSignaturePacket;->nested:I

    .line 37
    return-void
.end method


# virtual methods
.method public encode(Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;)V
    .locals 5
    .param p1, "out"    # Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 94
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 95
    .local v0, "bOut":Ljava/io/ByteArrayOutputStream;
    new-instance v1, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;

    invoke-direct {v1, v0}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 97
    .local v1, "pOut":Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;
    iget v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/OnePassSignaturePacket;->version:I

    invoke-virtual {v1, v2}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->write(I)V

    .line 98
    iget v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/OnePassSignaturePacket;->sigType:I

    invoke-virtual {v1, v2}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->write(I)V

    .line 99
    iget v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/OnePassSignaturePacket;->hashAlgorithm:I

    invoke-virtual {v1, v2}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->write(I)V

    .line 100
    iget v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/OnePassSignaturePacket;->keyAlgorithm:I

    invoke-virtual {v1, v2}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->write(I)V

    .line 102
    iget-wide v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/OnePassSignaturePacket;->keyID:J

    const/16 v4, 0x38

    shr-long/2addr v2, v4

    long-to-int v2, v2

    int-to-byte v2, v2

    invoke-virtual {v1, v2}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->write(I)V

    .line 103
    iget-wide v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/OnePassSignaturePacket;->keyID:J

    const/16 v4, 0x30

    shr-long/2addr v2, v4

    long-to-int v2, v2

    int-to-byte v2, v2

    invoke-virtual {v1, v2}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->write(I)V

    .line 104
    iget-wide v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/OnePassSignaturePacket;->keyID:J

    const/16 v4, 0x28

    shr-long/2addr v2, v4

    long-to-int v2, v2

    int-to-byte v2, v2

    invoke-virtual {v1, v2}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->write(I)V

    .line 105
    iget-wide v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/OnePassSignaturePacket;->keyID:J

    const/16 v4, 0x20

    shr-long/2addr v2, v4

    long-to-int v2, v2

    int-to-byte v2, v2

    invoke-virtual {v1, v2}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->write(I)V

    .line 106
    iget-wide v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/OnePassSignaturePacket;->keyID:J

    const/16 v4, 0x18

    shr-long/2addr v2, v4

    long-to-int v2, v2

    int-to-byte v2, v2

    invoke-virtual {v1, v2}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->write(I)V

    .line 107
    iget-wide v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/OnePassSignaturePacket;->keyID:J

    const/16 v4, 0x10

    shr-long/2addr v2, v4

    long-to-int v2, v2

    int-to-byte v2, v2

    invoke-virtual {v1, v2}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->write(I)V

    .line 108
    iget-wide v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/OnePassSignaturePacket;->keyID:J

    const/16 v4, 0x8

    shr-long/2addr v2, v4

    long-to-int v2, v2

    int-to-byte v2, v2

    invoke-virtual {v1, v2}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->write(I)V

    .line 109
    iget-wide v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/OnePassSignaturePacket;->keyID:J

    long-to-int v2, v2

    int-to-byte v2, v2

    invoke-virtual {v1, v2}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->write(I)V

    .line 111
    iget v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/OnePassSignaturePacket;->nested:I

    invoke-virtual {v1, v2}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->write(I)V

    .line 113
    const/4 v2, 0x4

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {p1, v2, v3, v4}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->writePacket(I[BZ)V

    .line 114
    return-void
.end method

.method public getHashAlgorithm()I
    .locals 1

    .prologue
    .line 76
    iget v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/OnePassSignaturePacket;->hashAlgorithm:I

    return v0
.end method

.method public getKeyAlgorithm()I
    .locals 1

    .prologue
    .line 68
    iget v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/OnePassSignaturePacket;->keyAlgorithm:I

    return v0
.end method

.method public getKeyID()J
    .locals 2

    .prologue
    .line 84
    iget-wide v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/OnePassSignaturePacket;->keyID:J

    return-wide v0
.end method

.method public getSignatureType()I
    .locals 1

    .prologue
    .line 60
    iget v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/OnePassSignaturePacket;->sigType:I

    return v0
.end method
