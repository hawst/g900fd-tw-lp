.class public Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyRing;
.super Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyRing;
.source "PGPPublicKeyRing.java"


# instance fields
.field keys:Ljava/util/List;


# direct methods
.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 14
    .param p1, "in"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v13, 0xe

    .line 48
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyRing;-><init>()V

    .line 49
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyRing;->keys:Ljava/util/List;

    .line 51
    invoke-static {p1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyRing;->wrap(Ljava/io/InputStream;)Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;

    move-result-object v9

    .line 53
    .local v9, "pIn":Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;
    invoke-virtual {v9}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->nextPacketTag()I

    move-result v7

    .line 54
    .local v7, "initialTag":I
    const/4 v0, 0x6

    if-eq v7, v0, :cond_0

    if-eq v7, v13, :cond_0

    .line 56
    new-instance v0, Ljava/io/IOException;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "public key ring doesn\'t start with public key tag: tag 0x"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-static {v7}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v0, v12}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 61
    :cond_0
    invoke-virtual {v9}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->readPacket()Lcom/android/sec/org/bouncycastle/bcpg/Packet;

    move-result-object v1

    check-cast v1, Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;

    .line 62
    .local v1, "pubPk":Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;
    invoke-static {v9}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyRing;->readOptionalTrustPacket(Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;)Lcom/android/sec/org/bouncycastle/bcpg/TrustPacket;

    move-result-object v2

    .line 65
    .local v2, "trustPk":Lcom/android/sec/org/bouncycastle/bcpg/TrustPacket;
    invoke-static {v9}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyRing;->readSignaturesAndTrust(Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;)Ljava/util/List;

    move-result-object v3

    .line 67
    .local v3, "keySigs":Ljava/util/List;
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 68
    .local v4, "ids":Ljava/util/List;
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 69
    .local v5, "idTrusts":Ljava/util/List;
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 70
    .local v6, "idSigs":Ljava/util/List;
    invoke-static {v9, v4, v5, v6}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyRing;->readUserIDs(Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V

    .line 72
    iget-object v12, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyRing;->keys:Ljava/util/List;

    new-instance v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;

    invoke-direct/range {v0 .. v6}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;-><init>(Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;Lcom/android/sec/org/bouncycastle/bcpg/TrustPacket;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V

    invoke-interface {v12, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 76
    :goto_0
    invoke-virtual {v9}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->nextPacketTag()I

    move-result v0

    if-ne v0, v13, :cond_1

    .line 78
    invoke-virtual {v9}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->readPacket()Lcom/android/sec/org/bouncycastle/bcpg/Packet;

    move-result-object v10

    check-cast v10, Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;

    .line 79
    .local v10, "pk":Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;
    invoke-static {v9}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyRing;->readOptionalTrustPacket(Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;)Lcom/android/sec/org/bouncycastle/bcpg/TrustPacket;

    move-result-object v8

    .line 82
    .local v8, "kTrust":Lcom/android/sec/org/bouncycastle/bcpg/TrustPacket;
    invoke-static {v9}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyRing;->readSignaturesAndTrust(Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;)Ljava/util/List;

    move-result-object v11

    .line 84
    .local v11, "sigList":Ljava/util/List;
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyRing;->keys:Ljava/util/List;

    new-instance v12, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;

    invoke-direct {v12, v10, v8, v11}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;-><init>(Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;Lcom/android/sec/org/bouncycastle/bcpg/TrustPacket;Ljava/util/List;)V

    invoke-interface {v0, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 86
    .end local v8    # "kTrust":Lcom/android/sec/org/bouncycastle/bcpg/TrustPacket;
    .end local v10    # "pk":Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;
    .end local v11    # "sigList":Ljava/util/List;
    :cond_1
    return-void
.end method

.method public constructor <init>(Ljava/util/List;)V
    .locals 0
    .param p1, "pubKeys"    # Ljava/util/List;

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyRing;-><init>()V

    .line 42
    iput-object p1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyRing;->keys:Ljava/util/List;

    .line 43
    return-void
.end method

.method public constructor <init>([B)V
    .locals 1
    .param p1, "encoding"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 33
    new-instance v0, Ljava/io/ByteArrayInputStream;

    invoke-direct {v0, p1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {p0, v0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyRing;-><init>(Ljava/io/InputStream;)V

    .line 34
    return-void
.end method

.method public static insertPublicKey(Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyRing;Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;)Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyRing;
    .locals 10
    .param p0, "pubRing"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyRing;
    .param p1, "pubKey"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;

    .prologue
    .line 166
    new-instance v3, Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyRing;->keys:Ljava/util/List;

    invoke-direct {v3, v5}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 167
    .local v3, "keys":Ljava/util/List;
    const/4 v0, 0x0

    .line 168
    .local v0, "found":Z
    const/4 v4, 0x0

    .line 170
    .local v4, "masterFound":Z
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v5

    if-eq v1, v5, :cond_2

    .line 172
    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;

    .line 174
    .local v2, "key":Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;
    invoke-virtual {v2}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->getKeyID()J

    move-result-wide v6

    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->getKeyID()J

    move-result-wide v8

    cmp-long v5, v6, v8

    if-nez v5, :cond_0

    .line 176
    const/4 v0, 0x1

    .line 177
    invoke-interface {v3, v1, p1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 179
    :cond_0
    invoke-virtual {v2}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->isMasterKey()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 181
    const/4 v4, 0x1

    .line 170
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 185
    .end local v2    # "key":Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;
    :cond_2
    if-nez v0, :cond_4

    .line 187
    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->isMasterKey()Z

    move-result v5

    if-eqz v5, :cond_5

    .line 189
    if-eqz v4, :cond_3

    .line 191
    new-instance v5, Ljava/lang/IllegalArgumentException;

    const-string v6, "cannot add a master key to a ring that already has one"

    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 194
    :cond_3
    const/4 v5, 0x0

    invoke-interface {v3, v5, p1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 202
    :cond_4
    :goto_1
    new-instance v5, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyRing;

    invoke-direct {v5, v3}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyRing;-><init>(Ljava/util/List;)V

    return-object v5

    .line 198
    :cond_5
    invoke-interface {v3, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method public static removePublicKey(Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyRing;Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;)Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyRing;
    .locals 8
    .param p0, "pubRing"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyRing;
    .param p1, "pubKey"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;

    .prologue
    .line 217
    new-instance v3, Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyRing;->keys:Ljava/util/List;

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 218
    .local v3, "keys":Ljava/util/List;
    const/4 v0, 0x0

    .line 220
    .local v0, "found":Z
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    if-ge v1, v4, :cond_1

    .line 222
    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;

    .line 224
    .local v2, "key":Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;
    invoke-virtual {v2}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->getKeyID()J

    move-result-wide v4

    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->getKeyID()J

    move-result-wide v6

    cmp-long v4, v4, v6

    if-nez v4, :cond_0

    .line 226
    const/4 v0, 0x1

    .line 227
    invoke-interface {v3, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 220
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 231
    .end local v2    # "key":Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;
    :cond_1
    if-nez v0, :cond_2

    .line 233
    const/4 v4, 0x0

    .line 236
    :goto_1
    return-object v4

    :cond_2
    new-instance v4, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyRing;

    invoke-direct {v4, v3}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyRing;-><init>(Ljava/util/List;)V

    goto :goto_1
.end method


# virtual methods
.method public encode(Ljava/io/OutputStream;)V
    .locals 3
    .param p1, "outStream"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 146
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyRing;->keys:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-eq v0, v2, :cond_0

    .line 148
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyRing;->keys:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;

    .line 150
    .local v1, "k":Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;
    invoke-virtual {v1, p1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->encode(Ljava/io/OutputStream;)V

    .line 146
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 152
    .end local v1    # "k":Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;
    :cond_0
    return-void
.end method

.method public getEncoded()[B
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 135
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 137
    .local v0, "bOut":Ljava/io/ByteArrayOutputStream;
    invoke-virtual {p0, v0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyRing;->encode(Ljava/io/OutputStream;)V

    .line 139
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    return-object v1
.end method

.method public getPublicKey()Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;
    .locals 2

    .prologue
    .line 95
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyRing;->keys:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;

    return-object v0
.end method

.method public getPublicKey(J)Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;
    .locals 5
    .param p1, "keyID"    # J

    .prologue
    .line 109
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyRing;->keys:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-eq v0, v2, :cond_1

    .line 111
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyRing;->keys:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;

    .line 113
    .local v1, "k":Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;
    invoke-virtual {v1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->getKeyID()J

    move-result-wide v2

    cmp-long v2, p1, v2

    if-nez v2, :cond_0

    .line 119
    .end local v1    # "k":Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;
    :goto_1
    return-object v1

    .line 109
    .restart local v1    # "k":Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 119
    .end local v1    # "k":Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public getPublicKeys()Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyRing;->keys:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method
