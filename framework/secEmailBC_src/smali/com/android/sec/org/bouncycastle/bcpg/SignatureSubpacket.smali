.class public Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;
.super Ljava/lang/Object;
.source "SignatureSubpacket.java"


# instance fields
.field critical:Z

.field protected data:[B

.field type:I


# direct methods
.method protected constructor <init>(IZ[B)V
    .locals 0
    .param p1, "type"    # I
    .param p2, "critical"    # Z
    .param p3, "data"    # [B

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput p1, p0, Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;->type:I

    .line 21
    iput-boolean p2, p0, Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;->critical:Z

    .line 22
    iput-object p3, p0, Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;->data:[B

    .line 23
    return-void
.end method


# virtual methods
.method public encode(Ljava/io/OutputStream;)V
    .locals 2
    .param p1, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 47
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;->data:[B

    array-length v1, v1

    add-int/lit8 v0, v1, 0x1

    .line 49
    .local v0, "bodyLen":I
    const/16 v1, 0xc0

    if-ge v0, v1, :cond_0

    .line 51
    int-to-byte v1, v0

    invoke-virtual {p1, v1}, Ljava/io/OutputStream;->write(I)V

    .line 69
    :goto_0
    iget-boolean v1, p0, Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;->critical:Z

    if-eqz v1, :cond_2

    .line 71
    iget v1, p0, Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;->type:I

    or-int/lit16 v1, v1, 0x80

    invoke-virtual {p1, v1}, Ljava/io/OutputStream;->write(I)V

    .line 78
    :goto_1
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;->data:[B

    invoke-virtual {p1, v1}, Ljava/io/OutputStream;->write([B)V

    .line 79
    return-void

    .line 53
    :cond_0
    const/16 v1, 0x20bf

    if-gt v0, v1, :cond_1

    .line 55
    add-int/lit16 v0, v0, -0xc0

    .line 57
    shr-int/lit8 v1, v0, 0x8

    and-int/lit16 v1, v1, 0xff

    add-int/lit16 v1, v1, 0xc0

    int-to-byte v1, v1

    invoke-virtual {p1, v1}, Ljava/io/OutputStream;->write(I)V

    .line 58
    int-to-byte v1, v0

    invoke-virtual {p1, v1}, Ljava/io/OutputStream;->write(I)V

    goto :goto_0

    .line 62
    :cond_1
    const/16 v1, 0xff

    invoke-virtual {p1, v1}, Ljava/io/OutputStream;->write(I)V

    .line 63
    shr-int/lit8 v1, v0, 0x18

    int-to-byte v1, v1

    invoke-virtual {p1, v1}, Ljava/io/OutputStream;->write(I)V

    .line 64
    shr-int/lit8 v1, v0, 0x10

    int-to-byte v1, v1

    invoke-virtual {p1, v1}, Ljava/io/OutputStream;->write(I)V

    .line 65
    shr-int/lit8 v1, v0, 0x8

    int-to-byte v1, v1

    invoke-virtual {p1, v1}, Ljava/io/OutputStream;->write(I)V

    .line 66
    int-to-byte v1, v0

    invoke-virtual {p1, v1}, Ljava/io/OutputStream;->write(I)V

    goto :goto_0

    .line 75
    :cond_2
    iget v1, p0, Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;->type:I

    invoke-virtual {p1, v1}, Ljava/io/OutputStream;->write(I)V

    goto :goto_1
.end method

.method public getData()[B
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;->data:[B

    return-object v0
.end method

.method public getType()I
    .locals 1

    .prologue
    .line 27
    iget v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;->type:I

    return v0
.end method

.method public isCritical()Z
    .locals 1

    .prologue
    .line 32
    iget-boolean v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;->critical:Z

    return v0
.end method
