.class public Lcom/android/sec/org/bouncycastle/jce/spec/ECPublicKeySpec;
.super Lcom/android/sec/org/bouncycastle/jce/spec/ECKeySpec;
.source "ECPublicKeySpec.java"


# instance fields
.field private q:Lcom/android/sec/org/bouncycastle/math/ec/ECPoint;


# direct methods
.method public constructor <init>(Lcom/android/sec/org/bouncycastle/math/ec/ECPoint;Lcom/android/sec/org/bouncycastle/jce/spec/ECParameterSpec;)V
    .locals 0
    .param p1, "q"    # Lcom/android/sec/org/bouncycastle/math/ec/ECPoint;
    .param p2, "spec"    # Lcom/android/sec/org/bouncycastle/jce/spec/ECParameterSpec;

    .prologue
    .line 23
    invoke-direct {p0, p2}, Lcom/android/sec/org/bouncycastle/jce/spec/ECKeySpec;-><init>(Lcom/android/sec/org/bouncycastle/jce/spec/ECParameterSpec;)V

    .line 25
    iput-object p1, p0, Lcom/android/sec/org/bouncycastle/jce/spec/ECPublicKeySpec;->q:Lcom/android/sec/org/bouncycastle/math/ec/ECPoint;

    .line 26
    return-void
.end method


# virtual methods
.method public getQ()Lcom/android/sec/org/bouncycastle/math/ec/ECPoint;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/jce/spec/ECPublicKeySpec;->q:Lcom/android/sec/org/bouncycastle/math/ec/ECPoint;

    return-object v0
.end method
