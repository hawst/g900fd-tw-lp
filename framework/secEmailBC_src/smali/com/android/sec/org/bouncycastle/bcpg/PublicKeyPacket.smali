.class public Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;
.super Lcom/android/sec/org/bouncycastle/bcpg/ContainedPacket;
.source "PublicKeyPacket.java"

# interfaces
.implements Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyAlgorithmTags;


# instance fields
.field private algorithm:I

.field private key:Lcom/android/sec/org/bouncycastle/bcpg/BCPGKey;

.field private time:J

.field private validDays:I

.field private version:I


# direct methods
.method public constructor <init>(ILjava/util/Date;Lcom/android/sec/org/bouncycastle/bcpg/BCPGKey;)V
    .locals 4
    .param p1, "algorithm"    # I
    .param p2, "time"    # Ljava/util/Date;
    .param p3, "key"    # Lcom/android/sec/org/bouncycastle/bcpg/BCPGKey;

    .prologue
    .line 62
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/bcpg/ContainedPacket;-><init>()V

    .line 63
    const/4 v0, 0x4

    iput v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;->version:I

    .line 64
    invoke-virtual {p2}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    iput-wide v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;->time:J

    .line 65
    iput p1, p0, Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;->algorithm:I

    .line 66
    iput-object p3, p0, Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;->key:Lcom/android/sec/org/bouncycastle/bcpg/BCPGKey;

    .line 67
    return-void
.end method

.method constructor <init>(Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;)V
    .locals 4
    .param p1, "in"    # Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/bcpg/ContainedPacket;-><init>()V

    .line 22
    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->read()I

    move-result v0

    iput v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;->version:I

    .line 23
    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->read()I

    move-result v0

    int-to-long v0, v0

    const/16 v2, 0x18

    shl-long/2addr v0, v2

    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->read()I

    move-result v2

    shl-int/lit8 v2, v2, 0x10

    int-to-long v2, v2

    or-long/2addr v0, v2

    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->read()I

    move-result v2

    shl-int/lit8 v2, v2, 0x8

    int-to-long v2, v2

    or-long/2addr v0, v2

    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->read()I

    move-result v2

    int-to-long v2, v2

    or-long/2addr v0, v2

    iput-wide v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;->time:J

    .line 25
    iget v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;->version:I

    const/4 v1, 0x3

    if-gt v0, v1, :cond_0

    .line 27
    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->read()I

    move-result v0

    shl-int/lit8 v0, v0, 0x8

    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->read()I

    move-result v1

    or-int/2addr v0, v1

    iput v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;->validDays:I

    .line 30
    :cond_0
    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->read()I

    move-result v0

    int-to-byte v0, v0

    iput v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;->algorithm:I

    .line 32
    iget v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;->algorithm:I

    sparse-switch v0, :sswitch_data_0

    .line 47
    new-instance v0, Ljava/io/IOException;

    const-string v1, "unknown PGP public key algorithm encountered"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 37
    :sswitch_0
    new-instance v0, Lcom/android/sec/org/bouncycastle/bcpg/RSAPublicBCPGKey;

    invoke-direct {v0, p1}, Lcom/android/sec/org/bouncycastle/bcpg/RSAPublicBCPGKey;-><init>(Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;)V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;->key:Lcom/android/sec/org/bouncycastle/bcpg/BCPGKey;

    .line 49
    :goto_0
    return-void

    .line 40
    :sswitch_1
    new-instance v0, Lcom/android/sec/org/bouncycastle/bcpg/DSAPublicBCPGKey;

    invoke-direct {v0, p1}, Lcom/android/sec/org/bouncycastle/bcpg/DSAPublicBCPGKey;-><init>(Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;)V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;->key:Lcom/android/sec/org/bouncycastle/bcpg/BCPGKey;

    goto :goto_0

    .line 44
    :sswitch_2
    new-instance v0, Lcom/android/sec/org/bouncycastle/bcpg/ElGamalPublicBCPGKey;

    invoke-direct {v0, p1}, Lcom/android/sec/org/bouncycastle/bcpg/ElGamalPublicBCPGKey;-><init>(Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;)V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;->key:Lcom/android/sec/org/bouncycastle/bcpg/BCPGKey;

    goto :goto_0

    .line 32
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_0
        0x3 -> :sswitch_0
        0x10 -> :sswitch_2
        0x11 -> :sswitch_1
        0x14 -> :sswitch_2
    .end sparse-switch
.end method


# virtual methods
.method public encode(Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;)V
    .locals 3
    .param p1, "out"    # Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 124
    const/4 v0, 0x6

    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;->getEncodedContents()[B

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {p1, v0, v1, v2}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->writePacket(I[BZ)V

    .line 125
    return-void
.end method

.method public getAlgorithm()I
    .locals 1

    .prologue
    .line 76
    iget v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;->algorithm:I

    return v0
.end method

.method public getEncodedContents()[B
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 97
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 98
    .local v0, "bOut":Ljava/io/ByteArrayOutputStream;
    new-instance v1, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;

    invoke-direct {v1, v0}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 100
    .local v1, "pOut":Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;
    iget v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;->version:I

    invoke-virtual {v1, v2}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->write(I)V

    .line 102
    iget-wide v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;->time:J

    const/16 v4, 0x18

    shr-long/2addr v2, v4

    long-to-int v2, v2

    int-to-byte v2, v2

    invoke-virtual {v1, v2}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->write(I)V

    .line 103
    iget-wide v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;->time:J

    const/16 v4, 0x10

    shr-long/2addr v2, v4

    long-to-int v2, v2

    int-to-byte v2, v2

    invoke-virtual {v1, v2}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->write(I)V

    .line 104
    iget-wide v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;->time:J

    const/16 v4, 0x8

    shr-long/2addr v2, v4

    long-to-int v2, v2

    int-to-byte v2, v2

    invoke-virtual {v1, v2}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->write(I)V

    .line 105
    iget-wide v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;->time:J

    long-to-int v2, v2

    int-to-byte v2, v2

    invoke-virtual {v1, v2}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->write(I)V

    .line 107
    iget v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;->version:I

    const/4 v3, 0x3

    if-gt v2, v3, :cond_0

    .line 109
    iget v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;->validDays:I

    shr-int/lit8 v2, v2, 0x8

    int-to-byte v2, v2

    invoke-virtual {v1, v2}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->write(I)V

    .line 110
    iget v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;->validDays:I

    int-to-byte v2, v2

    invoke-virtual {v1, v2}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->write(I)V

    .line 113
    :cond_0
    iget v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;->algorithm:I

    invoke-virtual {v1, v2}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->write(I)V

    .line 115
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;->key:Lcom/android/sec/org/bouncycastle/bcpg/BCPGKey;

    check-cast v2, Lcom/android/sec/org/bouncycastle/bcpg/BCPGObject;

    invoke-virtual {v1, v2}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->writeObject(Lcom/android/sec/org/bouncycastle/bcpg/BCPGObject;)V

    .line 117
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v2

    return-object v2
.end method

.method public getKey()Lcom/android/sec/org/bouncycastle/bcpg/BCPGKey;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;->key:Lcom/android/sec/org/bouncycastle/bcpg/BCPGKey;

    return-object v0
.end method

.method public getTime()Ljava/util/Date;
    .locals 6

    .prologue
    .line 86
    new-instance v0, Ljava/util/Date;

    iget-wide v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;->time:J

    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    invoke-direct {v0, v2, v3}, Ljava/util/Date;-><init>(J)V

    return-object v0
.end method

.method public getValidDays()I
    .locals 1

    .prologue
    .line 81
    iget v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;->validDays:I

    return v0
.end method

.method public getVersion()I
    .locals 1

    .prologue
    .line 71
    iget v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;->version:I

    return v0
.end method
