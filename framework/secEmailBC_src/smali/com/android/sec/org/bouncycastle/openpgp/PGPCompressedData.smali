.class public Lcom/android/sec/org/bouncycastle/openpgp/PGPCompressedData;
.super Ljava/lang/Object;
.source "PGPCompressedData.java"

# interfaces
.implements Lcom/android/sec/org/bouncycastle/bcpg/CompressionAlgorithmTags;


# instance fields
.field data:Lcom/android/sec/org/bouncycastle/bcpg/CompressedDataPacket;


# direct methods
.method public constructor <init>(Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;)V
    .locals 1
    .param p1, "pIn"    # Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->readPacket()Lcom/android/sec/org/bouncycastle/bcpg/Packet;

    move-result-object v0

    check-cast v0, Lcom/android/sec/org/bouncycastle/bcpg/CompressedDataPacket;

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPCompressedData;->data:Lcom/android/sec/org/bouncycastle/bcpg/CompressedDataPacket;

    .line 27
    return-void
.end method


# virtual methods
.method public getAlgorithm()I
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPCompressedData;->data:Lcom/android/sec/org/bouncycastle/bcpg/CompressedDataPacket;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/bcpg/CompressedDataPacket;->getAlgorithm()I

    move-result v0

    return v0
.end method

.method public getDataStream()Ljava/io/InputStream;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 59
    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPCompressedData;->getAlgorithm()I

    move-result v1

    if-nez v1, :cond_0

    .line 61
    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPCompressedData;->getInputStream()Ljava/io/InputStream;

    move-result-object v1

    .line 133
    :goto_0
    return-object v1

    .line 63
    :cond_0
    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPCompressedData;->getAlgorithm()I

    move-result v1

    if-ne v1, v4, :cond_1

    .line 65
    new-instance v1, Lcom/android/sec/org/bouncycastle/openpgp/PGPCompressedData$1;

    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPCompressedData;->getInputStream()Ljava/io/InputStream;

    move-result-object v2

    new-instance v3, Ljava/util/zip/Inflater;

    invoke-direct {v3, v4}, Ljava/util/zip/Inflater;-><init>(Z)V

    invoke-direct {v1, p0, v2, v3}, Lcom/android/sec/org/bouncycastle/openpgp/PGPCompressedData$1;-><init>(Lcom/android/sec/org/bouncycastle/openpgp/PGPCompressedData;Ljava/io/InputStream;Ljava/util/zip/Inflater;)V

    goto :goto_0

    .line 96
    :cond_1
    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPCompressedData;->getAlgorithm()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_2

    .line 98
    new-instance v1, Lcom/android/sec/org/bouncycastle/openpgp/PGPCompressedData$2;

    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPCompressedData;->getInputStream()Ljava/io/InputStream;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/android/sec/org/bouncycastle/openpgp/PGPCompressedData$2;-><init>(Lcom/android/sec/org/bouncycastle/openpgp/PGPCompressedData;Ljava/io/InputStream;)V

    goto :goto_0

    .line 129
    :cond_2
    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPCompressedData;->getAlgorithm()I

    move-result v1

    const/4 v2, 0x3

    if-ne v1, v2, :cond_3

    .line 133
    :try_start_0
    new-instance v1, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;

    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPCompressedData;->getInputStream()Ljava/io/InputStream;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/android/sec/org/bouncycastle/apache/bzip2/CBZip2InputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 135
    :catch_0
    move-exception v0

    .line 137
    .local v0, "e":Ljava/io/IOException;
    new-instance v1, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "I/O problem with stream: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v1

    .line 141
    .end local v0    # "e":Ljava/io/IOException;
    :cond_3
    new-instance v1, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "can\'t recognise compression algorithm: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPCompressedData;->getAlgorithm()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public getInputStream()Ljava/io/InputStream;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPCompressedData;->data:Lcom/android/sec/org/bouncycastle/bcpg/CompressedDataPacket;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/bcpg/CompressedDataPacket;->getInputStream()Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;

    move-result-object v0

    return-object v0
.end method
