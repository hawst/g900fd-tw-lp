.class public Lcom/android/sec/org/bouncycastle/openpgp/PGPUserAttributeSubpacketVectorGenerator;
.super Ljava/lang/Object;
.source "PGPUserAttributeSubpacketVectorGenerator.java"


# instance fields
.field private list:Ljava/util/List;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPUserAttributeSubpacketVectorGenerator;->list:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public generate()Lcom/android/sec/org/bouncycastle/openpgp/PGPUserAttributeSubpacketVector;
    .locals 3

    .prologue
    .line 25
    new-instance v1, Lcom/android/sec/org/bouncycastle/openpgp/PGPUserAttributeSubpacketVector;

    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPUserAttributeSubpacketVectorGenerator;->list:Ljava/util/List;

    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPUserAttributeSubpacketVectorGenerator;->list:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    new-array v2, v2, [Lcom/android/sec/org/bouncycastle/bcpg/UserAttributeSubpacket;

    invoke-interface {v0, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/android/sec/org/bouncycastle/bcpg/UserAttributeSubpacket;

    check-cast v0, [Lcom/android/sec/org/bouncycastle/bcpg/UserAttributeSubpacket;

    invoke-direct {v1, v0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPUserAttributeSubpacketVector;-><init>([Lcom/android/sec/org/bouncycastle/bcpg/UserAttributeSubpacket;)V

    return-object v1
.end method

.method public setImageAttribute(I[B)V
    .locals 2
    .param p1, "imageType"    # I
    .param p2, "imageData"    # [B

    .prologue
    .line 15
    if-nez p2, :cond_0

    .line 17
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "attempt to set null image"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 20
    :cond_0
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPUserAttributeSubpacketVectorGenerator;->list:Ljava/util/List;

    new-instance v1, Lcom/android/sec/org/bouncycastle/bcpg/attr/ImageAttribute;

    invoke-direct {v1, p1, p2}, Lcom/android/sec/org/bouncycastle/bcpg/attr/ImageAttribute;-><init>(I[B)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 21
    return-void
.end method
