.class public Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
.super Ljava/lang/Exception;
.source "PGPException.java"


# instance fields
.field underlying:Ljava/lang/Exception;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 14
    invoke-direct {p0, p1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    .line 15
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 0
    .param p1, "message"    # Ljava/lang/String;
    .param p2, "underlying"    # Ljava/lang/Exception;

    .prologue
    .line 21
    invoke-direct {p0, p1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    .line 22
    iput-object p2, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;->underlying:Ljava/lang/Exception;

    .line 23
    return-void
.end method


# virtual methods
.method public getCause()Ljava/lang/Throwable;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;->underlying:Ljava/lang/Exception;

    return-object v0
.end method

.method public getUnderlyingException()Ljava/lang/Exception;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;->underlying:Ljava/lang/Exception;

    return-object v0
.end method
