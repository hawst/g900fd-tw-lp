.class public Lcom/android/sec/org/bouncycastle/openpgp/PGPOnePassSignature;
.super Ljava/lang/Object;
.source "PGPOnePassSignature.java"


# instance fields
.field private lastb:B

.field private sig:Ljava/security/Signature;

.field private sigPack:Lcom/android/sec/org/bouncycastle/bcpg/OnePassSignaturePacket;

.field private signatureType:I


# direct methods
.method constructor <init>(Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;)V
    .locals 1
    .param p1, "pIn"    # Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
        }
    .end annotation

    .prologue
    .line 32
    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->readPacket()Lcom/android/sec/org/bouncycastle/bcpg/Packet;

    move-result-object v0

    check-cast v0, Lcom/android/sec/org/bouncycastle/bcpg/OnePassSignaturePacket;

    invoke-direct {p0, v0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPOnePassSignature;-><init>(Lcom/android/sec/org/bouncycastle/bcpg/OnePassSignaturePacket;)V

    .line 33
    return-void
.end method

.method constructor <init>(Lcom/android/sec/org/bouncycastle/bcpg/OnePassSignaturePacket;)V
    .locals 1
    .param p1, "sigPack"    # Lcom/android/sec/org/bouncycastle/bcpg/OnePassSignaturePacket;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
        }
    .end annotation

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object p1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPOnePassSignature;->sigPack:Lcom/android/sec/org/bouncycastle/bcpg/OnePassSignaturePacket;

    .line 40
    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/bcpg/OnePassSignaturePacket;->getSignatureType()I

    move-result v0

    iput v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPOnePassSignature;->signatureType:I

    .line 41
    return-void
.end method


# virtual methods
.method public encode(Ljava/io/OutputStream;)V
    .locals 2
    .param p1, "outStream"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 217
    instance-of v1, p1, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 219
    check-cast v0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;

    .line 226
    .local v0, "out":Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;
    :goto_0
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPOnePassSignature;->sigPack:Lcom/android/sec/org/bouncycastle/bcpg/OnePassSignaturePacket;

    invoke-virtual {v0, v1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->writePacket(Lcom/android/sec/org/bouncycastle/bcpg/ContainedPacket;)V

    .line 227
    return-void

    .line 223
    .end local v0    # "out":Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;
    :cond_0
    new-instance v0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;

    invoke-direct {v0, p1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;-><init>(Ljava/io/OutputStream;)V

    .restart local v0    # "out":Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;
    goto :goto_0
.end method

.method public getEncoded()[B
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 204
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 206
    .local v0, "bOut":Ljava/io/ByteArrayOutputStream;
    invoke-virtual {p0, v0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPOnePassSignature;->encode(Ljava/io/OutputStream;)V

    .line 208
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    return-object v1
.end method

.method public getHashAlgorithm()I
    .locals 1

    .prologue
    .line 193
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPOnePassSignature;->sigPack:Lcom/android/sec/org/bouncycastle/bcpg/OnePassSignaturePacket;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/bcpg/OnePassSignaturePacket;->getHashAlgorithm()I

    move-result v0

    return v0
.end method

.method public getKeyAlgorithm()I
    .locals 1

    .prologue
    .line 198
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPOnePassSignature;->sigPack:Lcom/android/sec/org/bouncycastle/bcpg/OnePassSignaturePacket;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/bcpg/OnePassSignaturePacket;->getKeyAlgorithm()I

    move-result v0

    return v0
.end method

.method public getKeyID()J
    .locals 2

    .prologue
    .line 183
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPOnePassSignature;->sigPack:Lcom/android/sec/org/bouncycastle/bcpg/OnePassSignaturePacket;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/bcpg/OnePassSignaturePacket;->getKeyID()J

    move-result-wide v0

    return-wide v0
.end method

.method public getSignatureType()I
    .locals 1

    .prologue
    .line 188
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPOnePassSignature;->sigPack:Lcom/android/sec/org/bouncycastle/bcpg/OnePassSignaturePacket;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/bcpg/OnePassSignaturePacket;->getSignatureType()I

    move-result v0

    return v0
.end method

.method public initVerify(Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;Ljava/lang/String;)V
    .locals 1
    .param p1, "pubKey"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;
    .param p2, "provider"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/NoSuchProviderException;,
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
        }
    .end annotation

    .prologue
    .line 56
    invoke-static {p2}, Lcom/android/sec/org/bouncycastle/openpgp/PGPUtil;->getProvider(Ljava/lang/String;)Ljava/security/Provider;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPOnePassSignature;->initVerify(Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;Ljava/security/Provider;)V

    .line 57
    return-void
.end method

.method public initVerify(Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;Ljava/security/Provider;)V
    .locals 3
    .param p1, "pubKey"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;
    .param p2, "provider"    # Ljava/security/Provider;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
        }
    .end annotation

    .prologue
    .line 71
    const/4 v1, 0x0

    iput-byte v1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPOnePassSignature;->lastb:B

    .line 75
    :try_start_0
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPOnePassSignature;->sigPack:Lcom/android/sec/org/bouncycastle/bcpg/OnePassSignaturePacket;

    invoke-virtual {v1}, Lcom/android/sec/org/bouncycastle/bcpg/OnePassSignaturePacket;->getKeyAlgorithm()I

    move-result v1

    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPOnePassSignature;->sigPack:Lcom/android/sec/org/bouncycastle/bcpg/OnePassSignaturePacket;

    invoke-virtual {v2}, Lcom/android/sec/org/bouncycastle/bcpg/OnePassSignaturePacket;->getHashAlgorithm()I

    move-result v2

    invoke-static {v1, v2}, Lcom/android/sec/org/bouncycastle/openpgp/PGPUtil;->getSignatureName(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, p2}, Ljava/security/Signature;->getInstance(Ljava/lang/String;Ljava/security/Provider;)Ljava/security/Signature;

    move-result-object v1

    iput-object v1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPOnePassSignature;->sig:Ljava/security/Signature;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 86
    :try_start_1
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPOnePassSignature;->sig:Ljava/security/Signature;

    invoke-virtual {p1, p2}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->getKey(Ljava/security/Provider;)Ljava/security/PublicKey;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/security/Signature;->initVerify(Ljava/security/PublicKey;)V
    :try_end_1
    .catch Ljava/security/InvalidKeyException; {:try_start_1 .. :try_end_1} :catch_1

    .line 92
    return-void

    .line 79
    :catch_0
    move-exception v0

    .line 81
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;

    const-string v2, "can\'t set up signature object."

    invoke-direct {v1, v2, v0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v1

    .line 88
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v0

    .line 90
    .local v0, "e":Ljava/security/InvalidKeyException;
    new-instance v1, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;

    const-string v2, "invalid key."

    invoke-direct {v1, v2, v0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v1
.end method

.method public update(B)V
    .locals 4
    .param p1, "b"    # B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/SignatureException;
        }
    .end annotation

    .prologue
    const/16 v3, 0xa

    const/16 v2, 0xd

    .line 98
    iget v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPOnePassSignature;->signatureType:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    .line 100
    if-ne p1, v2, :cond_1

    .line 102
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPOnePassSignature;->sig:Ljava/security/Signature;

    invoke-virtual {v0, v2}, Ljava/security/Signature;->update(B)V

    .line 103
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPOnePassSignature;->sig:Ljava/security/Signature;

    invoke-virtual {v0, v3}, Ljava/security/Signature;->update(B)V

    .line 118
    :cond_0
    :goto_0
    iput-byte p1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPOnePassSignature;->lastb:B

    .line 124
    :goto_1
    return-void

    .line 105
    :cond_1
    if-ne p1, v3, :cond_2

    .line 107
    iget-byte v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPOnePassSignature;->lastb:B

    if-eq v0, v2, :cond_0

    .line 109
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPOnePassSignature;->sig:Ljava/security/Signature;

    invoke-virtual {v0, v2}, Ljava/security/Signature;->update(B)V

    .line 110
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPOnePassSignature;->sig:Ljava/security/Signature;

    invoke-virtual {v0, v3}, Ljava/security/Signature;->update(B)V

    goto :goto_0

    .line 115
    :cond_2
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPOnePassSignature;->sig:Ljava/security/Signature;

    invoke-virtual {v0, p1}, Ljava/security/Signature;->update(B)V

    goto :goto_0

    .line 122
    :cond_3
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPOnePassSignature;->sig:Ljava/security/Signature;

    invoke-virtual {v0, p1}, Ljava/security/Signature;->update(B)V

    goto :goto_1
.end method

.method public update([B)V
    .locals 3
    .param p1, "bytes"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/SignatureException;
        }
    .end annotation

    .prologue
    .line 130
    iget v1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPOnePassSignature;->signatureType:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 132
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v1, p1

    if-eq v0, v1, :cond_1

    .line 134
    aget-byte v1, p1, v0

    invoke-virtual {p0, v1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPOnePassSignature;->update(B)V

    .line 132
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 139
    .end local v0    # "i":I
    :cond_0
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPOnePassSignature;->sig:Ljava/security/Signature;

    invoke-virtual {v1, p1}, Ljava/security/Signature;->update([B)V

    .line 141
    :cond_1
    return-void
.end method

.method public update([BII)V
    .locals 4
    .param p1, "bytes"    # [B
    .param p2, "off"    # I
    .param p3, "length"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/SignatureException;
        }
    .end annotation

    .prologue
    .line 149
    iget v2, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPOnePassSignature;->signatureType:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 151
    add-int v0, p2, p3

    .line 153
    .local v0, "finish":I
    move v1, p2

    .local v1, "i":I
    :goto_0
    if-eq v1, v0, :cond_1

    .line 155
    aget-byte v2, p1, v1

    invoke-virtual {p0, v2}, Lcom/android/sec/org/bouncycastle/openpgp/PGPOnePassSignature;->update(B)V

    .line 153
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 160
    .end local v0    # "finish":I
    .end local v1    # "i":I
    :cond_0
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPOnePassSignature;->sig:Ljava/security/Signature;

    invoke-virtual {v2, p1, p2, p3}, Ljava/security/Signature;->update([BII)V

    .line 162
    :cond_1
    return-void
.end method

.method public verify(Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;)Z
    .locals 2
    .param p1, "pgpSig"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;,
            Ljava/security/SignatureException;
        }
    .end annotation

    .prologue
    .line 176
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPOnePassSignature;->sig:Ljava/security/Signature;

    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;->getSignatureTrailer()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/security/Signature;->update([B)V

    .line 178
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPOnePassSignature;->sig:Ljava/security/Signature;

    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;->getSignature()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/security/Signature;->verify([B)Z

    move-result v0

    return v0
.end method
