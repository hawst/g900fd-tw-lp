.class public Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;
.super Ljava/lang/Object;
.source "AttributeTable.java"


# instance fields
.field private attributes:Ljava/util/Hashtable;


# direct methods
.method public constructor <init>(Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;)V
    .locals 3
    .param p1, "v"    # Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    new-instance v2, Ljava/util/Hashtable;

    invoke-direct {v2}, Ljava/util/Hashtable;-><init>()V

    iput-object v2, p0, Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;->attributes:Ljava/util/Hashtable;

    .line 27
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;->size()I

    move-result v2

    if-eq v1, v2, :cond_0

    .line 29
    invoke-virtual {p1, v1}, Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;->get(I)Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;

    move-result-object v2

    invoke-static {v2}, Lcom/android/sec/org/bouncycastle/asn1/cms/Attribute;->getInstance(Ljava/lang/Object;)Lcom/android/sec/org/bouncycastle/asn1/cms/Attribute;

    move-result-object v0

    .line 31
    .local v0, "a":Lcom/android/sec/org/bouncycastle/asn1/cms/Attribute;
    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/cms/Attribute;->getAttrType()Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    move-result-object v2

    invoke-direct {p0, v2, v0}, Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;->addAttribute(Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;Lcom/android/sec/org/bouncycastle/asn1/cms/Attribute;)V

    .line 27
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 33
    .end local v0    # "a":Lcom/android/sec/org/bouncycastle/asn1/cms/Attribute;
    :cond_0
    return-void
.end method

.method public constructor <init>(Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;)V
    .locals 3
    .param p1, "s"    # Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    new-instance v2, Ljava/util/Hashtable;

    invoke-direct {v2}, Ljava/util/Hashtable;-><init>()V

    iput-object v2, p0, Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;->attributes:Ljava/util/Hashtable;

    .line 38
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;->size()I

    move-result v2

    if-eq v1, v2, :cond_0

    .line 40
    invoke-virtual {p1, v1}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;->getObjectAt(I)Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;

    move-result-object v2

    invoke-static {v2}, Lcom/android/sec/org/bouncycastle/asn1/cms/Attribute;->getInstance(Ljava/lang/Object;)Lcom/android/sec/org/bouncycastle/asn1/cms/Attribute;

    move-result-object v0

    .line 42
    .local v0, "a":Lcom/android/sec/org/bouncycastle/asn1/cms/Attribute;
    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/asn1/cms/Attribute;->getAttrType()Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    move-result-object v2

    invoke-direct {p0, v2, v0}, Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;->addAttribute(Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;Lcom/android/sec/org/bouncycastle/asn1/cms/Attribute;)V

    .line 38
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 44
    .end local v0    # "a":Lcom/android/sec/org/bouncycastle/asn1/cms/Attribute;
    :cond_0
    return-void
.end method

.method public constructor <init>(Lcom/android/sec/org/bouncycastle/asn1/cms/Attribute;)V
    .locals 1
    .param p1, "attr"    # Lcom/android/sec/org/bouncycastle/asn1/cms/Attribute;

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;->attributes:Ljava/util/Hashtable;

    .line 49
    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/asn1/cms/Attribute;->getAttrType()Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;->addAttribute(Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;Lcom/android/sec/org/bouncycastle/asn1/cms/Attribute;)V

    .line 50
    return-void
.end method

.method public constructor <init>(Lcom/android/sec/org/bouncycastle/asn1/cms/Attributes;)V
    .locals 1
    .param p1, "attrs"    # Lcom/android/sec/org/bouncycastle/asn1/cms/Attributes;

    .prologue
    .line 55
    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/asn1/cms/Attributes;->toASN1Primitive()Lcom/android/sec/org/bouncycastle/asn1/ASN1Primitive;

    move-result-object v0

    invoke-static {v0}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;->getInstance(Ljava/lang/Object;)Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;-><init>(Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;)V

    .line 56
    return-void
.end method

.method public constructor <init>(Ljava/util/Hashtable;)V
    .locals 1
    .param p1, "attrs"    # Ljava/util/Hashtable;

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;->attributes:Ljava/util/Hashtable;

    .line 21
    invoke-direct {p0, p1}, Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;->copyTable(Ljava/util/Hashtable;)Ljava/util/Hashtable;

    move-result-object v0

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;->attributes:Ljava/util/Hashtable;

    .line 22
    return-void
.end method

.method private addAttribute(Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;Lcom/android/sec/org/bouncycastle/asn1/cms/Attribute;)V
    .locals 3
    .param p1, "oid"    # Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;
    .param p2, "a"    # Lcom/android/sec/org/bouncycastle/asn1/cms/Attribute;

    .prologue
    .line 62
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;->attributes:Ljava/util/Hashtable;

    invoke-virtual {v2, p1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 64
    .local v1, "value":Ljava/lang/Object;
    if-nez v1, :cond_0

    .line 66
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;->attributes:Ljava/util/Hashtable;

    invoke-virtual {v2, p1, p2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 88
    :goto_0
    return-void

    .line 72
    :cond_0
    instance-of v2, v1, Lcom/android/sec/org/bouncycastle/asn1/cms/Attribute;

    if-eqz v2, :cond_1

    .line 74
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    .line 76
    .local v0, "v":Ljava/util/Vector;
    invoke-virtual {v0, v1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 77
    invoke-virtual {v0, p2}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 86
    :goto_1
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;->attributes:Ljava/util/Hashtable;

    invoke-virtual {v2, p1, v0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .end local v0    # "v":Ljava/util/Vector;
    :cond_1
    move-object v0, v1

    .line 81
    check-cast v0, Ljava/util/Vector;

    .line 83
    .restart local v0    # "v":Ljava/util/Vector;
    invoke-virtual {v0, p2}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    goto :goto_1
.end method

.method private copyTable(Ljava/util/Hashtable;)Ljava/util/Hashtable;
    .locals 4
    .param p1, "in"    # Ljava/util/Hashtable;

    .prologue
    .line 217
    new-instance v2, Ljava/util/Hashtable;

    invoke-direct {v2}, Ljava/util/Hashtable;-><init>()V

    .line 218
    .local v2, "out":Ljava/util/Hashtable;
    invoke-virtual {p1}, Ljava/util/Hashtable;->keys()Ljava/util/Enumeration;

    move-result-object v0

    .line 220
    .local v0, "e":Ljava/util/Enumeration;
    :goto_0
    invoke-interface {v0}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 222
    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v1

    .line 224
    .local v1, "key":Ljava/lang/Object;
    invoke-virtual {p1, v1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 227
    .end local v1    # "key":Ljava/lang/Object;
    :cond_0
    return-object v2
.end method


# virtual methods
.method public add(Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;)Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;
    .locals 3
    .param p1, "attrType"    # Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;
    .param p2, "attrValue"    # Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;

    .prologue
    .line 239
    new-instance v0, Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;

    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;->attributes:Ljava/util/Hashtable;

    invoke-direct {v0, v1}, Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;-><init>(Ljava/util/Hashtable;)V

    .line 241
    .local v0, "newTable":Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;
    new-instance v1, Lcom/android/sec/org/bouncycastle/asn1/cms/Attribute;

    new-instance v2, Lcom/android/sec/org/bouncycastle/asn1/DERSet;

    invoke-direct {v2, p2}, Lcom/android/sec/org/bouncycastle/asn1/DERSet;-><init>(Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;)V

    invoke-direct {v1, p1, v2}, Lcom/android/sec/org/bouncycastle/asn1/cms/Attribute;-><init>(Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;Lcom/android/sec/org/bouncycastle/asn1/ASN1Set;)V

    invoke-direct {v0, p1, v1}, Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;->addAttribute(Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;Lcom/android/sec/org/bouncycastle/asn1/cms/Attribute;)V

    .line 243
    return-object v0
.end method

.method public get(Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;)Lcom/android/sec/org/bouncycastle/asn1/cms/Attribute;
    .locals 2
    .param p1, "oid"    # Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    .prologue
    .line 107
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;->attributes:Ljava/util/Hashtable;

    invoke-virtual {v1, p1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 109
    .local v0, "value":Ljava/lang/Object;
    instance-of v1, v0, Ljava/util/Vector;

    if-eqz v1, :cond_0

    .line 111
    check-cast v0, Ljava/util/Vector;

    .end local v0    # "value":Ljava/lang/Object;
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/sec/org/bouncycastle/asn1/cms/Attribute;

    .line 114
    :goto_0
    return-object v1

    .restart local v0    # "value":Ljava/lang/Object;
    :cond_0
    check-cast v0, Lcom/android/sec/org/bouncycastle/asn1/cms/Attribute;

    .end local v0    # "value":Ljava/lang/Object;
    move-object v1, v0

    goto :goto_0
.end method

.method public get(Lcom/android/sec/org/bouncycastle/asn1/DERObjectIdentifier;)Lcom/android/sec/org/bouncycastle/asn1/cms/Attribute;
    .locals 2
    .param p1, "oid"    # Lcom/android/sec/org/bouncycastle/asn1/DERObjectIdentifier;

    .prologue
    .line 95
    new-instance v0, Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/asn1/DERObjectIdentifier;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;->get(Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;)Lcom/android/sec/org/bouncycastle/asn1/cms/Attribute;

    move-result-object v0

    return-object v0
.end method

.method public getAll(Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;)Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;
    .locals 4
    .param p1, "oid"    # Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    .prologue
    .line 135
    new-instance v1, Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;

    invoke-direct {v1}, Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;-><init>()V

    .line 137
    .local v1, "v":Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;
    iget-object v3, p0, Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;->attributes:Ljava/util/Hashtable;

    invoke-virtual {v3, p1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .line 139
    .local v2, "value":Ljava/lang/Object;
    instance-of v3, v2, Ljava/util/Vector;

    if-eqz v3, :cond_0

    .line 141
    check-cast v2, Ljava/util/Vector;

    .end local v2    # "value":Ljava/lang/Object;
    invoke-virtual {v2}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object v0

    .line 143
    .local v0, "e":Ljava/util/Enumeration;
    :goto_0
    invoke-interface {v0}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 145
    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/sec/org/bouncycastle/asn1/cms/Attribute;

    invoke-virtual {v1, v3}, Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;->add(Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;)V

    goto :goto_0

    .line 148
    .end local v0    # "e":Ljava/util/Enumeration;
    .restart local v2    # "value":Ljava/lang/Object;
    :cond_0
    if-eqz v2, :cond_1

    .line 150
    check-cast v2, Lcom/android/sec/org/bouncycastle/asn1/cms/Attribute;

    .end local v2    # "value":Ljava/lang/Object;
    invoke-virtual {v1, v2}, Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;->add(Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;)V

    .line 153
    :cond_1
    return-object v1
.end method

.method public getAll(Lcom/android/sec/org/bouncycastle/asn1/DERObjectIdentifier;)Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;
    .locals 2
    .param p1, "oid"    # Lcom/android/sec/org/bouncycastle/asn1/DERObjectIdentifier;

    .prologue
    .line 122
    new-instance v0, Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/asn1/DERObjectIdentifier;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;->getAll(Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;)Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;

    move-result-object v0

    return-object v0
.end method

.method public remove(Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;)Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;
    .locals 2
    .param p1, "attrType"    # Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    .prologue
    .line 248
    new-instance v0, Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;

    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;->attributes:Ljava/util/Hashtable;

    invoke-direct {v0, v1}, Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;-><init>(Ljava/util/Hashtable;)V

    .line 250
    .local v0, "newTable":Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;
    iget-object v1, v0, Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;->attributes:Ljava/util/Hashtable;

    invoke-virtual {v1, p1}, Ljava/util/Hashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 252
    return-object v0
.end method

.method public size()I
    .locals 4

    .prologue
    .line 158
    const/4 v2, 0x0

    .line 160
    .local v2, "size":I
    iget-object v3, p0, Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;->attributes:Ljava/util/Hashtable;

    invoke-virtual {v3}, Ljava/util/Hashtable;->elements()Ljava/util/Enumeration;

    move-result-object v0

    .local v0, "en":Ljava/util/Enumeration;
    :goto_0
    invoke-interface {v0}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 162
    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v1

    .line 164
    .local v1, "o":Ljava/lang/Object;
    instance-of v3, v1, Ljava/util/Vector;

    if-eqz v3, :cond_0

    .line 166
    check-cast v1, Ljava/util/Vector;

    .end local v1    # "o":Ljava/lang/Object;
    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_0

    .line 170
    .restart local v1    # "o":Ljava/lang/Object;
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 174
    .end local v1    # "o":Ljava/lang/Object;
    :cond_1
    return v2
.end method

.method public toASN1EncodableVector()Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;
    .locals 5

    .prologue
    .line 184
    new-instance v2, Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;

    invoke-direct {v2}, Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;-><init>()V

    .line 185
    .local v2, "v":Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;
    iget-object v4, p0, Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;->attributes:Ljava/util/Hashtable;

    invoke-virtual {v4}, Ljava/util/Hashtable;->elements()Ljava/util/Enumeration;

    move-result-object v0

    .line 187
    .local v0, "e":Ljava/util/Enumeration;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 189
    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v3

    .line 191
    .local v3, "value":Ljava/lang/Object;
    instance-of v4, v3, Ljava/util/Vector;

    if-eqz v4, :cond_1

    .line 193
    check-cast v3, Ljava/util/Vector;

    .end local v3    # "value":Ljava/lang/Object;
    invoke-virtual {v3}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object v1

    .line 195
    .local v1, "en":Ljava/util/Enumeration;
    :goto_1
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 197
    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v4

    invoke-static {v4}, Lcom/android/sec/org/bouncycastle/asn1/cms/Attribute;->getInstance(Ljava/lang/Object;)Lcom/android/sec/org/bouncycastle/asn1/cms/Attribute;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;->add(Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;)V

    goto :goto_1

    .line 202
    .end local v1    # "en":Ljava/util/Enumeration;
    .restart local v3    # "value":Ljava/lang/Object;
    :cond_1
    invoke-static {v3}, Lcom/android/sec/org/bouncycastle/asn1/cms/Attribute;->getInstance(Ljava/lang/Object;)Lcom/android/sec/org/bouncycastle/asn1/cms/Attribute;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;->add(Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;)V

    goto :goto_0

    .line 206
    .end local v3    # "value":Ljava/lang/Object;
    :cond_2
    return-object v2
.end method

.method public toASN1Structure()Lcom/android/sec/org/bouncycastle/asn1/cms/Attributes;
    .locals 2

    .prologue
    .line 211
    new-instance v0, Lcom/android/sec/org/bouncycastle/asn1/cms/Attributes;

    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;->toASN1EncodableVector()Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/sec/org/bouncycastle/asn1/cms/Attributes;-><init>(Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;)V

    return-object v0
.end method

.method public toHashtable()Ljava/util/Hashtable;
    .locals 1

    .prologue
    .line 179
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;->attributes:Ljava/util/Hashtable;

    invoke-direct {p0, v0}, Lcom/android/sec/org/bouncycastle/asn1/cms/AttributeTable;->copyTable(Ljava/util/Hashtable;)Ljava/util/Hashtable;

    move-result-object v0

    return-object v0
.end method
