.class public Lcom/android/sec/org/bouncycastle/openpgp/PGPObjectFactory;
.super Ljava/lang/Object;
.source "PGPObjectFactory.java"


# instance fields
.field in:Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;


# direct methods
.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 1
    .param p1, "in"    # Ljava/io/InputStream;

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    new-instance v0, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;

    invoke-direct {v0, p1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;-><init>(Ljava/io/InputStream;)V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPObjectFactory;->in:Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;

    .line 28
    return-void
.end method

.method public constructor <init>([B)V
    .locals 1
    .param p1, "bytes"    # [B

    .prologue
    .line 33
    new-instance v0, Ljava/io/ByteArrayInputStream;

    invoke-direct {v0, p1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {p0, v0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPObjectFactory;-><init>(Ljava/io/InputStream;)V

    .line 34
    return-void
.end method


# virtual methods
.method public nextObject()Ljava/lang/Object;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 47
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPObjectFactory;->in:Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;

    invoke-virtual {v2}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->nextPacketTag()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    .line 110
    new-instance v2, Ljava/io/IOException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "unknown object in stream: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPObjectFactory;->in:Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;

    invoke-virtual {v4}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->nextPacketTag()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 50
    :sswitch_0
    const/4 v2, 0x0

    .line 107
    :goto_0
    return-object v2

    .line 52
    :sswitch_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 54
    .local v1, "l":Ljava/util/List;
    :goto_1
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPObjectFactory;->in:Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;

    invoke-virtual {v2}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->nextPacketTag()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    .line 58
    :try_start_0
    new-instance v2, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;

    iget-object v3, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPObjectFactory;->in:Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;

    invoke-direct {v2, v3}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;-><init>(Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lcom/android/sec/org/bouncycastle/openpgp/PGPException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 60
    :catch_0
    move-exception v0

    .line 62
    .local v0, "e":Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
    new-instance v2, Ljava/io/IOException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "can\'t create signature object: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 66
    .end local v0    # "e":Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
    :cond_0
    new-instance v3, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureList;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    new-array v2, v2, [Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;

    invoke-interface {v1, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;

    check-cast v2, [Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;

    invoke-direct {v3, v2}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureList;-><init>([Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;)V

    move-object v2, v3

    goto :goto_0

    .line 70
    .end local v1    # "l":Ljava/util/List;
    :sswitch_2
    :try_start_1
    new-instance v2, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;

    iget-object v3, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPObjectFactory;->in:Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;

    invoke-direct {v2, v3}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;-><init>(Ljava/io/InputStream;)V
    :try_end_1
    .catch Lcom/android/sec/org/bouncycastle/openpgp/PGPException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 72
    :catch_1
    move-exception v0

    .line 74
    .restart local v0    # "e":Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
    new-instance v2, Ljava/io/IOException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "can\'t create secret key object: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 77
    .end local v0    # "e":Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
    :sswitch_3
    new-instance v2, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyRing;

    iget-object v3, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPObjectFactory;->in:Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;

    invoke-direct {v2, v3}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyRing;-><init>(Ljava/io/InputStream;)V

    goto :goto_0

    .line 79
    :sswitch_4
    new-instance v2, Lcom/android/sec/org/bouncycastle/openpgp/PGPCompressedData;

    iget-object v3, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPObjectFactory;->in:Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;

    invoke-direct {v2, v3}, Lcom/android/sec/org/bouncycastle/openpgp/PGPCompressedData;-><init>(Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;)V

    goto :goto_0

    .line 81
    :sswitch_5
    new-instance v2, Lcom/android/sec/org/bouncycastle/openpgp/PGPLiteralData;

    iget-object v3, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPObjectFactory;->in:Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;

    invoke-direct {v2, v3}, Lcom/android/sec/org/bouncycastle/openpgp/PGPLiteralData;-><init>(Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;)V

    goto/16 :goto_0

    .line 84
    :sswitch_6
    new-instance v2, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataList;

    iget-object v3, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPObjectFactory;->in:Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;

    invoke-direct {v2, v3}, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataList;-><init>(Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;)V

    goto/16 :goto_0

    .line 86
    :sswitch_7
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 88
    .restart local v1    # "l":Ljava/util/List;
    :goto_2
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPObjectFactory;->in:Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;

    invoke-virtual {v2}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->nextPacketTag()I

    move-result v2

    const/4 v3, 0x4

    if-ne v2, v3, :cond_1

    .line 92
    :try_start_2
    new-instance v2, Lcom/android/sec/org/bouncycastle/openpgp/PGPOnePassSignature;

    iget-object v3, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPObjectFactory;->in:Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;

    invoke-direct {v2, v3}, Lcom/android/sec/org/bouncycastle/openpgp/PGPOnePassSignature;-><init>(Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Lcom/android/sec/org/bouncycastle/openpgp/PGPException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_2

    .line 94
    :catch_2
    move-exception v0

    .line 96
    .restart local v0    # "e":Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
    new-instance v2, Ljava/io/IOException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "can\'t create one pass signature object: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 100
    .end local v0    # "e":Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
    :cond_1
    new-instance v3, Lcom/android/sec/org/bouncycastle/openpgp/PGPOnePassSignatureList;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    new-array v2, v2, [Lcom/android/sec/org/bouncycastle/openpgp/PGPOnePassSignature;

    invoke-interface {v1, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Lcom/android/sec/org/bouncycastle/openpgp/PGPOnePassSignature;

    check-cast v2, [Lcom/android/sec/org/bouncycastle/openpgp/PGPOnePassSignature;

    invoke-direct {v3, v2}, Lcom/android/sec/org/bouncycastle/openpgp/PGPOnePassSignatureList;-><init>([Lcom/android/sec/org/bouncycastle/openpgp/PGPOnePassSignature;)V

    move-object v2, v3

    goto/16 :goto_0

    .line 102
    .end local v1    # "l":Ljava/util/List;
    :sswitch_8
    new-instance v2, Lcom/android/sec/org/bouncycastle/openpgp/PGPMarker;

    iget-object v3, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPObjectFactory;->in:Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;

    invoke-direct {v2, v3}, Lcom/android/sec/org/bouncycastle/openpgp/PGPMarker;-><init>(Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;)V

    goto/16 :goto_0

    .line 107
    :sswitch_9
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPObjectFactory;->in:Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;

    invoke-virtual {v2}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->readPacket()Lcom/android/sec/org/bouncycastle/bcpg/Packet;

    move-result-object v2

    goto/16 :goto_0

    .line 47
    :sswitch_data_0
    .sparse-switch
        -0x1 -> :sswitch_0
        0x1 -> :sswitch_6
        0x2 -> :sswitch_1
        0x3 -> :sswitch_6
        0x4 -> :sswitch_7
        0x5 -> :sswitch_2
        0x6 -> :sswitch_3
        0x8 -> :sswitch_4
        0xa -> :sswitch_8
        0xb -> :sswitch_5
        0x3c -> :sswitch_9
        0x3d -> :sswitch_9
        0x3e -> :sswitch_9
        0x3f -> :sswitch_9
    .end sparse-switch
.end method
