.class public Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketGenerator;
.super Ljava/lang/Object;
.source "PGPSignatureSubpacketGenerator.java"


# instance fields
.field list:Ljava/util/List;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketGenerator;->list:Ljava/util/List;

    .line 32
    return-void
.end method


# virtual methods
.method public generate()Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;
    .locals 3

    .prologue
    .line 186
    new-instance v1, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;

    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketGenerator;->list:Ljava/util/List;

    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketGenerator;->list:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    new-array v2, v2, [Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;

    invoke-interface {v0, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;

    check-cast v0, [Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;

    invoke-direct {v1, v0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;-><init>([Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;)V

    return-object v1
.end method

.method public setEmbeddedSignature(ZLcom/android/sec/org/bouncycastle/openpgp/PGPSignature;)V
    .locals 5
    .param p1, "isCritical"    # Z
    .param p2, "pgpSignature"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 151
    invoke-virtual {p2}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;->getEncoded()[B

    move-result-object v1

    .line 154
    .local v1, "sig":[B
    array-length v2, v1

    add-int/lit8 v2, v2, -0x1

    const/16 v3, 0x100

    if-le v2, v3, :cond_0

    .line 156
    array-length v2, v1

    add-int/lit8 v2, v2, -0x3

    new-array v0, v2, [B

    .line 163
    .local v0, "data":[B
    :goto_0
    array-length v2, v1

    array-length v3, v0

    sub-int/2addr v2, v3

    const/4 v3, 0x0

    array-length v4, v0

    invoke-static {v1, v2, v0, v3, v4}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 165
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketGenerator;->list:Ljava/util/List;

    new-instance v3, Lcom/android/sec/org/bouncycastle/bcpg/sig/EmbeddedSignature;

    invoke-direct {v3, p1, v0}, Lcom/android/sec/org/bouncycastle/bcpg/sig/EmbeddedSignature;-><init>(Z[B)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 166
    return-void

    .line 160
    .end local v0    # "data":[B
    :cond_0
    array-length v2, v1

    add-int/lit8 v2, v2, -0x2

    new-array v0, v2, [B

    .restart local v0    # "data":[B
    goto :goto_0
.end method

.method public setExportable(ZZ)V
    .locals 2
    .param p1, "isCritical"    # Z
    .param p2, "isExportable"    # Z

    .prologue
    .line 45
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketGenerator;->list:Ljava/util/List;

    new-instance v1, Lcom/android/sec/org/bouncycastle/bcpg/sig/Exportable;

    invoke-direct {v1, p1, p2}, Lcom/android/sec/org/bouncycastle/bcpg/sig/Exportable;-><init>(ZZ)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 46
    return-void
.end method

.method public setKeyExpirationTime(ZJ)V
    .locals 2
    .param p1, "isCritical"    # Z
    .param p2, "seconds"    # J

    .prologue
    .line 76
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketGenerator;->list:Ljava/util/List;

    new-instance v1, Lcom/android/sec/org/bouncycastle/bcpg/sig/KeyExpirationTime;

    invoke-direct {v1, p1, p2, p3}, Lcom/android/sec/org/bouncycastle/bcpg/sig/KeyExpirationTime;-><init>(ZJ)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 77
    return-void
.end method

.method public setKeyFlags(ZI)V
    .locals 2
    .param p1, "isCritical"    # Z
    .param p2, "flags"    # I

    .prologue
    .line 131
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketGenerator;->list:Ljava/util/List;

    new-instance v1, Lcom/android/sec/org/bouncycastle/bcpg/sig/KeyFlags;

    invoke-direct {v1, p1, p2}, Lcom/android/sec/org/bouncycastle/bcpg/sig/KeyFlags;-><init>(ZI)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 132
    return-void
.end method

.method public setNotationData(ZZLjava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "isCritical"    # Z
    .param p2, "isHumanReadable"    # Z
    .param p3, "notationName"    # Ljava/lang/String;
    .param p4, "notationValue"    # Ljava/lang/String;

    .prologue
    .line 181
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketGenerator;->list:Ljava/util/List;

    new-instance v1, Lcom/android/sec/org/bouncycastle/bcpg/sig/NotationData;

    invoke-direct {v1, p1, p2, p3, p4}, Lcom/android/sec/org/bouncycastle/bcpg/sig/NotationData;-><init>(ZZLjava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 182
    return-void
.end method

.method public setPreferredCompressionAlgorithms(Z[I)V
    .locals 3
    .param p1, "isCritical"    # Z
    .param p2, "algorithms"    # [I

    .prologue
    .line 124
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketGenerator;->list:Ljava/util/List;

    new-instance v1, Lcom/android/sec/org/bouncycastle/bcpg/sig/PreferredAlgorithms;

    const/16 v2, 0x16

    invoke-direct {v1, v2, p1, p2}, Lcom/android/sec/org/bouncycastle/bcpg/sig/PreferredAlgorithms;-><init>(IZ[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 125
    return-void
.end method

.method public setPreferredHashAlgorithms(Z[I)V
    .locals 3
    .param p1, "isCritical"    # Z
    .param p2, "algorithms"    # [I

    .prologue
    .line 110
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketGenerator;->list:Ljava/util/List;

    new-instance v1, Lcom/android/sec/org/bouncycastle/bcpg/sig/PreferredAlgorithms;

    const/16 v2, 0x15

    invoke-direct {v1, v2, p1, p2}, Lcom/android/sec/org/bouncycastle/bcpg/sig/PreferredAlgorithms;-><init>(IZ[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 111
    return-void
.end method

.method public setPreferredSymmetricAlgorithms(Z[I)V
    .locals 3
    .param p1, "isCritical"    # Z
    .param p2, "algorithms"    # [I

    .prologue
    .line 117
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketGenerator;->list:Ljava/util/List;

    new-instance v1, Lcom/android/sec/org/bouncycastle/bcpg/sig/PreferredAlgorithms;

    const/16 v2, 0xb

    invoke-direct {v1, v2, p1, p2}, Lcom/android/sec/org/bouncycastle/bcpg/sig/PreferredAlgorithms;-><init>(IZ[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 118
    return-void
.end method

.method public setPrimaryUserID(ZZ)V
    .locals 2
    .param p1, "isCritical"    # Z
    .param p2, "isPrimaryUserID"    # Z

    .prologue
    .line 172
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketGenerator;->list:Ljava/util/List;

    new-instance v1, Lcom/android/sec/org/bouncycastle/bcpg/sig/PrimaryUserID;

    invoke-direct {v1, p1, p2}, Lcom/android/sec/org/bouncycastle/bcpg/sig/PrimaryUserID;-><init>(ZZ)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 173
    return-void
.end method

.method public setRevocable(ZZ)V
    .locals 2
    .param p1, "isCritical"    # Z
    .param p2, "isRevocable"    # Z

    .prologue
    .line 38
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketGenerator;->list:Ljava/util/List;

    new-instance v1, Lcom/android/sec/org/bouncycastle/bcpg/sig/Revocable;

    invoke-direct {v1, p1, p2}, Lcom/android/sec/org/bouncycastle/bcpg/sig/Revocable;-><init>(ZZ)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 39
    return-void
.end method

.method public setSignatureCreationTime(ZLjava/util/Date;)V
    .locals 2
    .param p1, "isCritical"    # Z
    .param p2, "date"    # Ljava/util/Date;

    .prologue
    .line 103
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketGenerator;->list:Ljava/util/List;

    new-instance v1, Lcom/android/sec/org/bouncycastle/bcpg/sig/SignatureCreationTime;

    invoke-direct {v1, p1, p2}, Lcom/android/sec/org/bouncycastle/bcpg/sig/SignatureCreationTime;-><init>(ZLjava/util/Date;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 104
    return-void
.end method

.method public setSignatureExpirationTime(ZJ)V
    .locals 2
    .param p1, "isCritical"    # Z
    .param p2, "seconds"    # J

    .prologue
    .line 90
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketGenerator;->list:Ljava/util/List;

    new-instance v1, Lcom/android/sec/org/bouncycastle/bcpg/sig/SignatureExpirationTime;

    invoke-direct {v1, p1, p2, p3}, Lcom/android/sec/org/bouncycastle/bcpg/sig/SignatureExpirationTime;-><init>(ZJ)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 91
    return-void
.end method

.method public setSignerUserID(ZLjava/lang/String;)V
    .locals 2
    .param p1, "isCritical"    # Z
    .param p2, "userID"    # Ljava/lang/String;

    .prologue
    .line 138
    if-nez p2, :cond_0

    .line 140
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "attempt to set null SignerUserID"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 143
    :cond_0
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketGenerator;->list:Ljava/util/List;

    new-instance v1, Lcom/android/sec/org/bouncycastle/bcpg/sig/SignerUserID;

    invoke-direct {v1, p1, p2}, Lcom/android/sec/org/bouncycastle/bcpg/sig/SignerUserID;-><init>(ZLjava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 144
    return-void
.end method

.method public setTrust(ZII)V
    .locals 2
    .param p1, "isCritical"    # Z
    .param p2, "depth"    # I
    .param p3, "trustAmount"    # I

    .prologue
    .line 62
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketGenerator;->list:Ljava/util/List;

    new-instance v1, Lcom/android/sec/org/bouncycastle/bcpg/sig/TrustSignature;

    invoke-direct {v1, p1, p2, p3}, Lcom/android/sec/org/bouncycastle/bcpg/sig/TrustSignature;-><init>(ZII)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 63
    return-void
.end method
