.class public Lcom/android/sec/org/bouncycastle/jcajce/provider/digest/SHA1$SHA1Mac;
.super Lcom/android/sec/org/bouncycastle/jcajce/provider/symmetric/util/BaseMac;
.source "SHA1.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/sec/org/bouncycastle/jcajce/provider/digest/SHA1;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SHA1Mac"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 79
    new-instance v0, Lcom/android/sec/org/bouncycastle/crypto/macs/HMac;

    new-instance v1, Lcom/android/sec/org/bouncycastle/crypto/digests/SHA1Digest;

    invoke-direct {v1}, Lcom/android/sec/org/bouncycastle/crypto/digests/SHA1Digest;-><init>()V

    invoke-direct {v0, v1}, Lcom/android/sec/org/bouncycastle/crypto/macs/HMac;-><init>(Lcom/android/sec/org/bouncycastle/crypto/Digest;)V

    invoke-direct {p0, v0}, Lcom/android/sec/org/bouncycastle/jcajce/provider/symmetric/util/BaseMac;-><init>(Lcom/android/sec/org/bouncycastle/crypto/Mac;)V

    .line 80
    return-void
.end method
