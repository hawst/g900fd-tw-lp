.class public Lcom/android/sec/org/bouncycastle/asn1/cms/IssuerAndSerialNumber;
.super Lcom/android/sec/org/bouncycastle/asn1/ASN1Object;
.source "IssuerAndSerialNumber.java"


# instance fields
.field private name:Lcom/android/sec/org/bouncycastle/asn1/x500/X500Name;

.field private serialNumber:Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;


# direct methods
.method public constructor <init>(Lcom/android/sec/org/bouncycastle/asn1/ASN1Sequence;)V
    .locals 1
    .param p1, "seq"    # Lcom/android/sec/org/bouncycastle/asn1/ASN1Sequence;

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Object;-><init>()V

    .line 44
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Sequence;->getObjectAt(I)Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;

    move-result-object v0

    invoke-static {v0}, Lcom/android/sec/org/bouncycastle/asn1/x500/X500Name;->getInstance(Ljava/lang/Object;)Lcom/android/sec/org/bouncycastle/asn1/x500/X500Name;

    move-result-object v0

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/asn1/cms/IssuerAndSerialNumber;->name:Lcom/android/sec/org/bouncycastle/asn1/x500/X500Name;

    .line 45
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Sequence;->getObjectAt(I)Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;

    move-result-object v0

    check-cast v0, Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/asn1/cms/IssuerAndSerialNumber;->serialNumber:Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;

    .line 46
    return-void
.end method

.method public constructor <init>(Lcom/android/sec/org/bouncycastle/asn1/x500/X500Name;Ljava/math/BigInteger;)V
    .locals 1
    .param p1, "name"    # Lcom/android/sec/org/bouncycastle/asn1/x500/X500Name;
    .param p2, "serialNumber"    # Ljava/math/BigInteger;

    .prologue
    .line 65
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Object;-><init>()V

    .line 66
    iput-object p1, p0, Lcom/android/sec/org/bouncycastle/asn1/cms/IssuerAndSerialNumber;->name:Lcom/android/sec/org/bouncycastle/asn1/x500/X500Name;

    .line 67
    new-instance v0, Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;

    invoke-direct {v0, p2}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;-><init>(Ljava/math/BigInteger;)V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/asn1/cms/IssuerAndSerialNumber;->serialNumber:Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;

    .line 68
    return-void
.end method

.method public constructor <init>(Lcom/android/sec/org/bouncycastle/asn1/x509/Certificate;)V
    .locals 1
    .param p1, "certificate"    # Lcom/android/sec/org/bouncycastle/asn1/x509/Certificate;

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Object;-><init>()V

    .line 51
    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/asn1/x509/Certificate;->getIssuer()Lcom/android/sec/org/bouncycastle/asn1/x500/X500Name;

    move-result-object v0

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/asn1/cms/IssuerAndSerialNumber;->name:Lcom/android/sec/org/bouncycastle/asn1/x500/X500Name;

    .line 52
    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/asn1/x509/Certificate;->getSerialNumber()Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/asn1/cms/IssuerAndSerialNumber;->serialNumber:Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;

    .line 53
    return-void
.end method

.method public constructor <init>(Lcom/android/sec/org/bouncycastle/asn1/x509/X509CertificateStructure;)V
    .locals 1
    .param p1, "certificate"    # Lcom/android/sec/org/bouncycastle/asn1/x509/X509CertificateStructure;

    .prologue
    .line 57
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Object;-><init>()V

    .line 58
    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/asn1/x509/X509CertificateStructure;->getIssuer()Lcom/android/sec/org/bouncycastle/asn1/x500/X500Name;

    move-result-object v0

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/asn1/cms/IssuerAndSerialNumber;->name:Lcom/android/sec/org/bouncycastle/asn1/x500/X500Name;

    .line 59
    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/asn1/x509/X509CertificateStructure;->getSerialNumber()Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/asn1/cms/IssuerAndSerialNumber;->serialNumber:Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;

    .line 60
    return-void
.end method

.method public constructor <init>(Lcom/android/sec/org/bouncycastle/asn1/x509/X509Name;Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;)V
    .locals 1
    .param p1, "name"    # Lcom/android/sec/org/bouncycastle/asn1/x509/X509Name;
    .param p2, "serialNumber"    # Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;

    .prologue
    .line 87
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Object;-><init>()V

    .line 88
    invoke-static {p1}, Lcom/android/sec/org/bouncycastle/asn1/x500/X500Name;->getInstance(Ljava/lang/Object;)Lcom/android/sec/org/bouncycastle/asn1/x500/X500Name;

    move-result-object v0

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/asn1/cms/IssuerAndSerialNumber;->name:Lcom/android/sec/org/bouncycastle/asn1/x500/X500Name;

    .line 89
    iput-object p2, p0, Lcom/android/sec/org/bouncycastle/asn1/cms/IssuerAndSerialNumber;->serialNumber:Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;

    .line 90
    return-void
.end method

.method public constructor <init>(Lcom/android/sec/org/bouncycastle/asn1/x509/X509Name;Ljava/math/BigInteger;)V
    .locals 1
    .param p1, "name"    # Lcom/android/sec/org/bouncycastle/asn1/x509/X509Name;
    .param p2, "serialNumber"    # Ljava/math/BigInteger;

    .prologue
    .line 76
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Object;-><init>()V

    .line 77
    invoke-static {p1}, Lcom/android/sec/org/bouncycastle/asn1/x500/X500Name;->getInstance(Ljava/lang/Object;)Lcom/android/sec/org/bouncycastle/asn1/x500/X500Name;

    move-result-object v0

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/asn1/cms/IssuerAndSerialNumber;->name:Lcom/android/sec/org/bouncycastle/asn1/x500/X500Name;

    .line 78
    new-instance v0, Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;

    invoke-direct {v0, p2}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;-><init>(Ljava/math/BigInteger;)V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/asn1/cms/IssuerAndSerialNumber;->serialNumber:Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;

    .line 79
    return-void
.end method

.method public static getInstance(Ljava/lang/Object;)Lcom/android/sec/org/bouncycastle/asn1/cms/IssuerAndSerialNumber;
    .locals 2
    .param p0, "obj"    # Ljava/lang/Object;

    .prologue
    .line 25
    instance-of v0, p0, Lcom/android/sec/org/bouncycastle/asn1/cms/IssuerAndSerialNumber;

    if-eqz v0, :cond_0

    .line 27
    check-cast p0, Lcom/android/sec/org/bouncycastle/asn1/cms/IssuerAndSerialNumber;

    .line 34
    .end local p0    # "obj":Ljava/lang/Object;
    :goto_0
    return-object p0

    .line 29
    .restart local p0    # "obj":Ljava/lang/Object;
    :cond_0
    if-eqz p0, :cond_1

    .line 31
    new-instance v0, Lcom/android/sec/org/bouncycastle/asn1/cms/IssuerAndSerialNumber;

    invoke-static {p0}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Sequence;->getInstance(Ljava/lang/Object;)Lcom/android/sec/org/bouncycastle/asn1/ASN1Sequence;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/sec/org/bouncycastle/asn1/cms/IssuerAndSerialNumber;-><init>(Lcom/android/sec/org/bouncycastle/asn1/ASN1Sequence;)V

    move-object p0, v0

    goto :goto_0

    .line 34
    :cond_1
    const/4 p0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getName()Lcom/android/sec/org/bouncycastle/asn1/x500/X500Name;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/asn1/cms/IssuerAndSerialNumber;->name:Lcom/android/sec/org/bouncycastle/asn1/x500/X500Name;

    return-object v0
.end method

.method public getSerialNumber()Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/asn1/cms/IssuerAndSerialNumber;->serialNumber:Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;

    return-object v0
.end method

.method public toASN1Primitive()Lcom/android/sec/org/bouncycastle/asn1/ASN1Primitive;
    .locals 2

    .prologue
    .line 104
    new-instance v0, Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;

    invoke-direct {v0}, Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;-><init>()V

    .line 106
    .local v0, "v":Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/asn1/cms/IssuerAndSerialNumber;->name:Lcom/android/sec/org/bouncycastle/asn1/x500/X500Name;

    invoke-virtual {v0, v1}, Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;->add(Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;)V

    .line 107
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/asn1/cms/IssuerAndSerialNumber;->serialNumber:Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;

    invoke-virtual {v0, v1}, Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;->add(Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;)V

    .line 109
    new-instance v1, Lcom/android/sec/org/bouncycastle/asn1/DERSequence;

    invoke-direct {v1, v0}, Lcom/android/sec/org/bouncycastle/asn1/DERSequence;-><init>(Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;)V

    return-object v1
.end method
