.class public Lcom/android/sec/org/bouncycastle/util/io/pem/PEMKeyUtilities$PEMUtilException;
.super Ljava/io/IOException;
.source "PEMKeyUtilities.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/sec/org/bouncycastle/util/io/pem/PEMKeyUtilities;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PEMUtilException"
.end annotation


# instance fields
.field private cause:Ljava/lang/Throwable;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 136
    invoke-direct {p0, p1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    .line 137
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 0
    .param p1, "msg"    # Ljava/lang/String;
    .param p2, "ex"    # Ljava/lang/Throwable;

    .prologue
    .line 140
    invoke-direct {p0, p1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    .line 141
    iput-object p2, p0, Lcom/android/sec/org/bouncycastle/util/io/pem/PEMKeyUtilities$PEMUtilException;->cause:Ljava/lang/Throwable;

    .line 142
    return-void
.end method


# virtual methods
.method public getCause()Ljava/lang/Throwable;
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/util/io/pem/PEMKeyUtilities$PEMUtilException;->cause:Ljava/lang/Throwable;

    return-object v0
.end method
