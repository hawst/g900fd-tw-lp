.class public Lcom/android/sec/org/bouncycastle/bcpg/SymmetricEncIntegrityPacket;
.super Lcom/android/sec/org/bouncycastle/bcpg/InputStreamPacket;
.source "SymmetricEncIntegrityPacket.java"


# instance fields
.field version:I


# direct methods
.method constructor <init>(Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;)V
    .locals 1
    .param p1, "in"    # Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 16
    invoke-direct {p0, p1}, Lcom/android/sec/org/bouncycastle/bcpg/InputStreamPacket;-><init>(Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;)V

    .line 18
    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;->read()I

    move-result v0

    iput v0, p0, Lcom/android/sec/org/bouncycastle/bcpg/SymmetricEncIntegrityPacket;->version:I

    .line 19
    return-void
.end method
