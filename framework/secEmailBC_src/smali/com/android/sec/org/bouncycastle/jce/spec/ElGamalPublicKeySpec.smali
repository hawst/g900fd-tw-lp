.class public Lcom/android/sec/org/bouncycastle/jce/spec/ElGamalPublicKeySpec;
.super Lcom/android/sec/org/bouncycastle/jce/spec/ElGamalKeySpec;
.source "ElGamalPublicKeySpec.java"


# instance fields
.field private y:Ljava/math/BigInteger;


# direct methods
.method public constructor <init>(Ljava/math/BigInteger;Lcom/android/sec/org/bouncycastle/jce/spec/ElGamalParameterSpec;)V
    .locals 0
    .param p1, "y"    # Ljava/math/BigInteger;
    .param p2, "spec"    # Lcom/android/sec/org/bouncycastle/jce/spec/ElGamalParameterSpec;

    .prologue
    .line 15
    invoke-direct {p0, p2}, Lcom/android/sec/org/bouncycastle/jce/spec/ElGamalKeySpec;-><init>(Lcom/android/sec/org/bouncycastle/jce/spec/ElGamalParameterSpec;)V

    .line 17
    iput-object p1, p0, Lcom/android/sec/org/bouncycastle/jce/spec/ElGamalPublicKeySpec;->y:Ljava/math/BigInteger;

    .line 18
    return-void
.end method


# virtual methods
.method public getY()Ljava/math/BigInteger;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/jce/spec/ElGamalPublicKeySpec;->y:Ljava/math/BigInteger;

    return-object v0
.end method
