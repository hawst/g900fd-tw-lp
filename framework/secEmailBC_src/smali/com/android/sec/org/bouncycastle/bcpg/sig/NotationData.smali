.class public Lcom/android/sec/org/bouncycastle/bcpg/sig/NotationData;
.super Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;
.source "NotationData.java"


# static fields
.field public static final HEADER_FLAG_LENGTH:I = 0x4

.field public static final HEADER_NAME_LENGTH:I = 0x2

.field public static final HEADER_VALUE_LENGTH:I = 0x2


# direct methods
.method public constructor <init>(ZZLjava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "critical"    # Z
    .param p2, "humanReadable"    # Z
    .param p3, "notationName"    # Ljava/lang/String;
    .param p4, "notationValue"    # Ljava/lang/String;

    .prologue
    .line 31
    const/16 v0, 0x14

    invoke-static {p2, p3, p4}, Lcom/android/sec/org/bouncycastle/bcpg/sig/NotationData;->createData(ZLjava/lang/String;Ljava/lang/String;)[B

    move-result-object v1

    invoke-direct {p0, v0, p1, v1}, Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;-><init>(IZ[B)V

    .line 32
    return-void
.end method

.method public constructor <init>(Z[B)V
    .locals 1
    .param p1, "critical"    # Z
    .param p2, "data"    # [B

    .prologue
    .line 22
    const/16 v0, 0x14

    invoke-direct {p0, v0, p1, p2}, Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;-><init>(IZ[B)V

    .line 23
    return-void
.end method

.method private static createData(ZLjava/lang/String;Ljava/lang/String;)[B
    .locals 8
    .param p0, "humanReadable"    # Z
    .param p1, "notationName"    # Ljava/lang/String;
    .param p2, "notationValue"    # Ljava/lang/String;

    .prologue
    const/16 v7, 0xff

    const/4 v6, 0x0

    .line 36
    new-instance v2, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v2}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 44
    .local v2, "out":Ljava/io/ByteArrayOutputStream;
    if-eqz p0, :cond_0

    const/16 v5, 0x80

    :goto_0
    invoke-virtual {v2, v5}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 45
    invoke-virtual {v2, v6}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 46
    invoke-virtual {v2, v6}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 47
    invoke-virtual {v2, v6}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 49
    const/4 v3, 0x0

    .line 52
    .local v3, "valueData":[B
    invoke-static {p1}, Lcom/android/sec/org/bouncycastle/util/Strings;->toUTF8ByteArray(Ljava/lang/String;)[B

    move-result-object v0

    .line 53
    .local v0, "nameData":[B
    array-length v5, v0

    invoke-static {v5, v7}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 55
    .local v1, "nameLength":I
    invoke-static {p2}, Lcom/android/sec/org/bouncycastle/util/Strings;->toUTF8ByteArray(Ljava/lang/String;)[B

    move-result-object v3

    .line 56
    array-length v5, v3

    invoke-static {v5, v7}, Ljava/lang/Math;->min(II)I

    move-result v4

    .line 59
    .local v4, "valueLength":I
    ushr-int/lit8 v5, v1, 0x8

    and-int/lit16 v5, v5, 0xff

    invoke-virtual {v2, v5}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 60
    ushr-int/lit8 v5, v1, 0x0

    and-int/lit16 v5, v5, 0xff

    invoke-virtual {v2, v5}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 63
    ushr-int/lit8 v5, v4, 0x8

    and-int/lit16 v5, v5, 0xff

    invoke-virtual {v2, v5}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 64
    ushr-int/lit8 v5, v4, 0x0

    and-int/lit16 v5, v5, 0xff

    invoke-virtual {v2, v5}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 67
    invoke-virtual {v2, v0, v6, v1}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    .line 70
    invoke-virtual {v2, v3, v6, v4}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    .line 72
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v5

    return-object v5

    .end local v0    # "nameData":[B
    .end local v1    # "nameLength":I
    .end local v3    # "valueData":[B
    .end local v4    # "valueLength":I
    :cond_0
    move v5, v6

    .line 44
    goto :goto_0
.end method


# virtual methods
.method public getNotationName()Ljava/lang/String;
    .locals 5

    .prologue
    .line 82
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/sig/NotationData;->data:[B

    const/4 v3, 0x4

    aget-byte v2, v2, v3

    shl-int/lit8 v2, v2, 0x8

    iget-object v3, p0, Lcom/android/sec/org/bouncycastle/bcpg/sig/NotationData;->data:[B

    const/4 v4, 0x5

    aget-byte v3, v3, v4

    shl-int/lit8 v3, v3, 0x0

    add-int v1, v2, v3

    .line 84
    .local v1, "nameLength":I
    new-array v0, v1, [B

    .line 85
    .local v0, "bName":[B
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/sig/NotationData;->data:[B

    const/16 v3, 0x8

    const/4 v4, 0x0

    invoke-static {v2, v3, v0, v4, v1}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 87
    invoke-static {v0}, Lcom/android/sec/org/bouncycastle/util/Strings;->fromUTF8ByteArray([B)Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method public getNotationValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 92
    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/bcpg/sig/NotationData;->getNotationValueBytes()[B

    move-result-object v0

    invoke-static {v0}, Lcom/android/sec/org/bouncycastle/util/Strings;->fromUTF8ByteArray([B)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getNotationValueBytes()[B
    .locals 6

    .prologue
    .line 97
    iget-object v3, p0, Lcom/android/sec/org/bouncycastle/bcpg/sig/NotationData;->data:[B

    const/4 v4, 0x4

    aget-byte v3, v3, v4

    shl-int/lit8 v3, v3, 0x8

    iget-object v4, p0, Lcom/android/sec/org/bouncycastle/bcpg/sig/NotationData;->data:[B

    const/4 v5, 0x5

    aget-byte v4, v4, v5

    shl-int/lit8 v4, v4, 0x0

    add-int v1, v3, v4

    .line 98
    .local v1, "nameLength":I
    iget-object v3, p0, Lcom/android/sec/org/bouncycastle/bcpg/sig/NotationData;->data:[B

    const/4 v4, 0x6

    aget-byte v3, v3, v4

    shl-int/lit8 v3, v3, 0x8

    iget-object v4, p0, Lcom/android/sec/org/bouncycastle/bcpg/sig/NotationData;->data:[B

    const/4 v5, 0x7

    aget-byte v4, v4, v5

    shl-int/lit8 v4, v4, 0x0

    add-int v2, v3, v4

    .line 100
    .local v2, "valueLength":I
    new-array v0, v2, [B

    .line 101
    .local v0, "bValue":[B
    iget-object v3, p0, Lcom/android/sec/org/bouncycastle/bcpg/sig/NotationData;->data:[B

    add-int/lit8 v4, v1, 0x8

    const/4 v5, 0x0

    invoke-static {v3, v4, v0, v5, v2}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 102
    return-object v0
.end method

.method public isHumanReadable()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 77
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/bcpg/sig/NotationData;->data:[B

    aget-byte v1, v1, v0

    const/16 v2, -0x80

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method
