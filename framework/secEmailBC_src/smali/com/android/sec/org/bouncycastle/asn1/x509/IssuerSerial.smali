.class public Lcom/android/sec/org/bouncycastle/asn1/x509/IssuerSerial;
.super Lcom/android/sec/org/bouncycastle/asn1/ASN1Object;
.source "IssuerSerial.java"


# instance fields
.field issuer:Lcom/android/sec/org/bouncycastle/asn1/x509/GeneralNames;

.field issuerUID:Lcom/android/sec/org/bouncycastle/asn1/DERBitString;

.field serial:Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;


# direct methods
.method private constructor <init>(Lcom/android/sec/org/bouncycastle/asn1/ASN1Sequence;)V
    .locals 3
    .param p1, "seq"    # Lcom/android/sec/org/bouncycastle/asn1/ASN1Sequence;

    .prologue
    const/4 v2, 0x3

    const/4 v1, 0x2

    .line 46
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Object;-><init>()V

    .line 47
    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Sequence;->size()I

    move-result v0

    if-eq v0, v1, :cond_0

    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Sequence;->size()I

    move-result v0

    if-eq v0, v2, :cond_0

    .line 49
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Bad sequence size: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Sequence;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 52
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Sequence;->getObjectAt(I)Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;

    move-result-object v0

    invoke-static {v0}, Lcom/android/sec/org/bouncycastle/asn1/x509/GeneralNames;->getInstance(Ljava/lang/Object;)Lcom/android/sec/org/bouncycastle/asn1/x509/GeneralNames;

    move-result-object v0

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/asn1/x509/IssuerSerial;->issuer:Lcom/android/sec/org/bouncycastle/asn1/x509/GeneralNames;

    .line 53
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Sequence;->getObjectAt(I)Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;

    move-result-object v0

    invoke-static {v0}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;->getInstance(Ljava/lang/Object;)Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/asn1/x509/IssuerSerial;->serial:Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;

    .line 55
    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Sequence;->size()I

    move-result v0

    if-ne v0, v2, :cond_1

    .line 57
    invoke-virtual {p1, v1}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Sequence;->getObjectAt(I)Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;

    move-result-object v0

    invoke-static {v0}, Lcom/android/sec/org/bouncycastle/asn1/DERBitString;->getInstance(Ljava/lang/Object;)Lcom/android/sec/org/bouncycastle/asn1/DERBitString;

    move-result-object v0

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/asn1/x509/IssuerSerial;->issuerUID:Lcom/android/sec/org/bouncycastle/asn1/DERBitString;

    .line 59
    :cond_1
    return-void
.end method

.method public constructor <init>(Lcom/android/sec/org/bouncycastle/asn1/x509/GeneralNames;Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;)V
    .locals 0
    .param p1, "issuer"    # Lcom/android/sec/org/bouncycastle/asn1/x509/GeneralNames;
    .param p2, "serial"    # Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;

    .prologue
    .line 71
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Object;-><init>()V

    .line 72
    iput-object p1, p0, Lcom/android/sec/org/bouncycastle/asn1/x509/IssuerSerial;->issuer:Lcom/android/sec/org/bouncycastle/asn1/x509/GeneralNames;

    .line 73
    iput-object p2, p0, Lcom/android/sec/org/bouncycastle/asn1/x509/IssuerSerial;->serial:Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;

    .line 74
    return-void
.end method

.method public constructor <init>(Lcom/android/sec/org/bouncycastle/asn1/x509/GeneralNames;Ljava/math/BigInteger;)V
    .locals 1
    .param p1, "issuer"    # Lcom/android/sec/org/bouncycastle/asn1/x509/GeneralNames;
    .param p2, "serial"    # Ljava/math/BigInteger;

    .prologue
    .line 65
    new-instance v0, Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;

    invoke-direct {v0, p2}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;-><init>(Ljava/math/BigInteger;)V

    invoke-direct {p0, p1, v0}, Lcom/android/sec/org/bouncycastle/asn1/x509/IssuerSerial;-><init>(Lcom/android/sec/org/bouncycastle/asn1/x509/GeneralNames;Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;)V

    .line 66
    return-void
.end method

.method public static getInstance(Lcom/android/sec/org/bouncycastle/asn1/ASN1TaggedObject;Z)Lcom/android/sec/org/bouncycastle/asn1/x509/IssuerSerial;
    .locals 1
    .param p0, "obj"    # Lcom/android/sec/org/bouncycastle/asn1/ASN1TaggedObject;
    .param p1, "explicit"    # Z

    .prologue
    .line 41
    invoke-static {p0, p1}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Sequence;->getInstance(Lcom/android/sec/org/bouncycastle/asn1/ASN1TaggedObject;Z)Lcom/android/sec/org/bouncycastle/asn1/ASN1Sequence;

    move-result-object v0

    invoke-static {v0}, Lcom/android/sec/org/bouncycastle/asn1/x509/IssuerSerial;->getInstance(Ljava/lang/Object;)Lcom/android/sec/org/bouncycastle/asn1/x509/IssuerSerial;

    move-result-object v0

    return-object v0
.end method

.method public static getInstance(Ljava/lang/Object;)Lcom/android/sec/org/bouncycastle/asn1/x509/IssuerSerial;
    .locals 2
    .param p0, "obj"    # Ljava/lang/Object;

    .prologue
    .line 24
    instance-of v0, p0, Lcom/android/sec/org/bouncycastle/asn1/x509/IssuerSerial;

    if-eqz v0, :cond_0

    .line 26
    check-cast p0, Lcom/android/sec/org/bouncycastle/asn1/x509/IssuerSerial;

    .line 34
    .end local p0    # "obj":Ljava/lang/Object;
    :goto_0
    return-object p0

    .line 29
    .restart local p0    # "obj":Ljava/lang/Object;
    :cond_0
    if-eqz p0, :cond_1

    .line 31
    new-instance v0, Lcom/android/sec/org/bouncycastle/asn1/x509/IssuerSerial;

    invoke-static {p0}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Sequence;->getInstance(Ljava/lang/Object;)Lcom/android/sec/org/bouncycastle/asn1/ASN1Sequence;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/sec/org/bouncycastle/asn1/x509/IssuerSerial;-><init>(Lcom/android/sec/org/bouncycastle/asn1/ASN1Sequence;)V

    move-object p0, v0

    goto :goto_0

    .line 34
    :cond_1
    const/4 p0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getIssuer()Lcom/android/sec/org/bouncycastle/asn1/x509/GeneralNames;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/asn1/x509/IssuerSerial;->issuer:Lcom/android/sec/org/bouncycastle/asn1/x509/GeneralNames;

    return-object v0
.end method

.method public getIssuerUID()Lcom/android/sec/org/bouncycastle/asn1/DERBitString;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/asn1/x509/IssuerSerial;->issuerUID:Lcom/android/sec/org/bouncycastle/asn1/DERBitString;

    return-object v0
.end method

.method public getSerial()Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/asn1/x509/IssuerSerial;->serial:Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;

    return-object v0
.end method

.method public toASN1Primitive()Lcom/android/sec/org/bouncycastle/asn1/ASN1Primitive;
    .locals 2

    .prologue
    .line 103
    new-instance v0, Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;

    invoke-direct {v0}, Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;-><init>()V

    .line 105
    .local v0, "v":Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/asn1/x509/IssuerSerial;->issuer:Lcom/android/sec/org/bouncycastle/asn1/x509/GeneralNames;

    invoke-virtual {v0, v1}, Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;->add(Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;)V

    .line 106
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/asn1/x509/IssuerSerial;->serial:Lcom/android/sec/org/bouncycastle/asn1/ASN1Integer;

    invoke-virtual {v0, v1}, Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;->add(Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;)V

    .line 108
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/asn1/x509/IssuerSerial;->issuerUID:Lcom/android/sec/org/bouncycastle/asn1/DERBitString;

    if-eqz v1, :cond_0

    .line 110
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/asn1/x509/IssuerSerial;->issuerUID:Lcom/android/sec/org/bouncycastle/asn1/DERBitString;

    invoke-virtual {v0, v1}, Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;->add(Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;)V

    .line 113
    :cond_0
    new-instance v1, Lcom/android/sec/org/bouncycastle/asn1/DERSequence;

    invoke-direct {v1, v0}, Lcom/android/sec/org/bouncycastle/asn1/DERSequence;-><init>(Lcom/android/sec/org/bouncycastle/asn1/ASN1EncodableVector;)V

    return-object v1
.end method
