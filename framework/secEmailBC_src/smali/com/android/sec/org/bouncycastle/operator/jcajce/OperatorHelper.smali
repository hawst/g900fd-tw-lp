.class Lcom/android/sec/org/bouncycastle/operator/jcajce/OperatorHelper;
.super Ljava/lang/Object;
.source "OperatorHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/sec/org/bouncycastle/operator/jcajce/OperatorHelper$OpCertificateException;
    }
.end annotation


# static fields
.field private static final asymmetricWrapperAlgNames:Ljava/util/Map;

.field private static final oids:Ljava/util/Map;

.field private static final symmetricKeyAlgNames:Ljava/util/Map;

.field private static final symmetricWrapperAlgNames:Ljava/util/Map;


# instance fields
.field private helper:Lcom/android/sec/org/bouncycastle/jcajce/JcaJceHelper;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 41
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/android/sec/org/bouncycastle/operator/jcajce/OperatorHelper;->oids:Ljava/util/Map;

    .line 42
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/android/sec/org/bouncycastle/operator/jcajce/OperatorHelper;->asymmetricWrapperAlgNames:Ljava/util/Map;

    .line 43
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/android/sec/org/bouncycastle/operator/jcajce/OperatorHelper;->symmetricWrapperAlgNames:Ljava/util/Map;

    .line 44
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/android/sec/org/bouncycastle/operator/jcajce/OperatorHelper;->symmetricKeyAlgNames:Ljava/util/Map;

    .line 51
    sget-object v0, Lcom/android/sec/org/bouncycastle/operator/jcajce/OperatorHelper;->oids:Ljava/util/Map;

    new-instance v1, Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    const-string v2, "1.2.840.113549.1.1.5"

    invoke-direct {v1, v2}, Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;-><init>(Ljava/lang/String;)V

    const-string v2, "SHA1WITHRSA"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 55
    sget-object v0, Lcom/android/sec/org/bouncycastle/operator/jcajce/OperatorHelper;->oids:Ljava/util/Map;

    sget-object v1, Lcom/android/sec/org/bouncycastle/asn1/pkcs/PKCSObjectIdentifiers;->sha256WithRSAEncryption:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    const-string v2, "SHA256WITHRSA"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 56
    sget-object v0, Lcom/android/sec/org/bouncycastle/operator/jcajce/OperatorHelper;->oids:Ljava/util/Map;

    sget-object v1, Lcom/android/sec/org/bouncycastle/asn1/pkcs/PKCSObjectIdentifiers;->sha384WithRSAEncryption:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    const-string v2, "SHA384WITHRSA"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 57
    sget-object v0, Lcom/android/sec/org/bouncycastle/operator/jcajce/OperatorHelper;->oids:Ljava/util/Map;

    sget-object v1, Lcom/android/sec/org/bouncycastle/asn1/pkcs/PKCSObjectIdentifiers;->sha512WithRSAEncryption:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    const-string v2, "SHA512WITHRSA"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 63
    sget-object v0, Lcom/android/sec/org/bouncycastle/operator/jcajce/OperatorHelper;->oids:Ljava/util/Map;

    new-instance v1, Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    const-string v2, "1.2.840.113549.1.1.4"

    invoke-direct {v1, v2}, Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;-><init>(Ljava/lang/String;)V

    const-string v2, "MD5WITHRSA"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 67
    sget-object v0, Lcom/android/sec/org/bouncycastle/operator/jcajce/OperatorHelper;->oids:Ljava/util/Map;

    new-instance v1, Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    const-string v2, "1.2.840.10040.4.3"

    invoke-direct {v1, v2}, Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;-><init>(Ljava/lang/String;)V

    const-string v2, "SHA1WITHDSA"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 68
    sget-object v0, Lcom/android/sec/org/bouncycastle/operator/jcajce/OperatorHelper;->oids:Ljava/util/Map;

    sget-object v1, Lcom/android/sec/org/bouncycastle/asn1/x9/X9ObjectIdentifiers;->ecdsa_with_SHA1:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    const-string v2, "SHA1WITHECDSA"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 72
    sget-object v0, Lcom/android/sec/org/bouncycastle/operator/jcajce/OperatorHelper;->oids:Ljava/util/Map;

    sget-object v1, Lcom/android/sec/org/bouncycastle/asn1/x9/X9ObjectIdentifiers;->ecdsa_with_SHA256:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    const-string v2, "SHA256WITHECDSA"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 73
    sget-object v0, Lcom/android/sec/org/bouncycastle/operator/jcajce/OperatorHelper;->oids:Ljava/util/Map;

    sget-object v1, Lcom/android/sec/org/bouncycastle/asn1/x9/X9ObjectIdentifiers;->ecdsa_with_SHA384:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    const-string v2, "SHA384WITHECDSA"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 74
    sget-object v0, Lcom/android/sec/org/bouncycastle/operator/jcajce/OperatorHelper;->oids:Ljava/util/Map;

    sget-object v1, Lcom/android/sec/org/bouncycastle/asn1/x9/X9ObjectIdentifiers;->ecdsa_with_SHA512:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    const-string v2, "SHA512WITHECDSA"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 75
    sget-object v0, Lcom/android/sec/org/bouncycastle/operator/jcajce/OperatorHelper;->oids:Ljava/util/Map;

    sget-object v1, Lcom/android/sec/org/bouncycastle/asn1/oiw/OIWObjectIdentifiers;->sha1WithRSA:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    const-string v2, "SHA1WITHRSA"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 76
    sget-object v0, Lcom/android/sec/org/bouncycastle/operator/jcajce/OperatorHelper;->oids:Ljava/util/Map;

    sget-object v1, Lcom/android/sec/org/bouncycastle/asn1/oiw/OIWObjectIdentifiers;->dsaWithSHA1:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    const-string v2, "SHA1WITHDSA"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 80
    sget-object v0, Lcom/android/sec/org/bouncycastle/operator/jcajce/OperatorHelper;->oids:Ljava/util/Map;

    sget-object v1, Lcom/android/sec/org/bouncycastle/asn1/nist/NISTObjectIdentifiers;->dsa_with_sha256:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    const-string v2, "SHA256WITHDSA"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 82
    sget-object v0, Lcom/android/sec/org/bouncycastle/operator/jcajce/OperatorHelper;->oids:Ljava/util/Map;

    sget-object v1, Lcom/android/sec/org/bouncycastle/asn1/oiw/OIWObjectIdentifiers;->idSHA1:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    const-string v2, "SHA-1"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 83
    sget-object v0, Lcom/android/sec/org/bouncycastle/operator/jcajce/OperatorHelper;->oids:Ljava/util/Map;

    sget-object v1, Lcom/android/sec/org/bouncycastle/asn1/nist/NISTObjectIdentifiers;->id_sha224:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    const-string v2, "SHA-224"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 84
    sget-object v0, Lcom/android/sec/org/bouncycastle/operator/jcajce/OperatorHelper;->oids:Ljava/util/Map;

    sget-object v1, Lcom/android/sec/org/bouncycastle/asn1/nist/NISTObjectIdentifiers;->id_sha256:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    const-string v2, "SHA-256"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 85
    sget-object v0, Lcom/android/sec/org/bouncycastle/operator/jcajce/OperatorHelper;->oids:Ljava/util/Map;

    sget-object v1, Lcom/android/sec/org/bouncycastle/asn1/nist/NISTObjectIdentifiers;->id_sha384:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    const-string v2, "SHA-384"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 86
    sget-object v0, Lcom/android/sec/org/bouncycastle/operator/jcajce/OperatorHelper;->oids:Ljava/util/Map;

    sget-object v1, Lcom/android/sec/org/bouncycastle/asn1/nist/NISTObjectIdentifiers;->id_sha512:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    const-string v2, "SHA-512"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 87
    sget-object v0, Lcom/android/sec/org/bouncycastle/operator/jcajce/OperatorHelper;->oids:Ljava/util/Map;

    sget-object v1, Lcom/android/sec/org/bouncycastle/asn1/teletrust/TeleTrusTObjectIdentifiers;->ripemd128:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    const-string v2, "RIPEMD-128"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 88
    sget-object v0, Lcom/android/sec/org/bouncycastle/operator/jcajce/OperatorHelper;->oids:Ljava/util/Map;

    sget-object v1, Lcom/android/sec/org/bouncycastle/asn1/teletrust/TeleTrusTObjectIdentifiers;->ripemd160:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    const-string v2, "RIPEMD-160"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 89
    sget-object v0, Lcom/android/sec/org/bouncycastle/operator/jcajce/OperatorHelper;->oids:Ljava/util/Map;

    sget-object v1, Lcom/android/sec/org/bouncycastle/asn1/teletrust/TeleTrusTObjectIdentifiers;->ripemd256:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    const-string v2, "RIPEMD-256"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 91
    sget-object v0, Lcom/android/sec/org/bouncycastle/operator/jcajce/OperatorHelper;->asymmetricWrapperAlgNames:Ljava/util/Map;

    sget-object v1, Lcom/android/sec/org/bouncycastle/asn1/pkcs/PKCSObjectIdentifiers;->rsaEncryption:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    const-string v2, "RSA/ECB/PKCS1Padding"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 93
    sget-object v0, Lcom/android/sec/org/bouncycastle/operator/jcajce/OperatorHelper;->symmetricWrapperAlgNames:Ljava/util/Map;

    sget-object v1, Lcom/android/sec/org/bouncycastle/asn1/pkcs/PKCSObjectIdentifiers;->id_alg_CMS3DESwrap:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    const-string v2, "DESEDEWrap"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 94
    sget-object v0, Lcom/android/sec/org/bouncycastle/operator/jcajce/OperatorHelper;->symmetricWrapperAlgNames:Ljava/util/Map;

    sget-object v1, Lcom/android/sec/org/bouncycastle/asn1/pkcs/PKCSObjectIdentifiers;->id_alg_CMSRC2wrap:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    const-string v2, "RC2Wrap"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 95
    sget-object v0, Lcom/android/sec/org/bouncycastle/operator/jcajce/OperatorHelper;->symmetricWrapperAlgNames:Ljava/util/Map;

    sget-object v1, Lcom/android/sec/org/bouncycastle/asn1/nist/NISTObjectIdentifiers;->id_aes128_wrap:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    const-string v2, "AESWrap"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 96
    sget-object v0, Lcom/android/sec/org/bouncycastle/operator/jcajce/OperatorHelper;->symmetricWrapperAlgNames:Ljava/util/Map;

    sget-object v1, Lcom/android/sec/org/bouncycastle/asn1/nist/NISTObjectIdentifiers;->id_aes192_wrap:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    const-string v2, "AESWrap"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 97
    sget-object v0, Lcom/android/sec/org/bouncycastle/operator/jcajce/OperatorHelper;->symmetricWrapperAlgNames:Ljava/util/Map;

    sget-object v1, Lcom/android/sec/org/bouncycastle/asn1/nist/NISTObjectIdentifiers;->id_aes256_wrap:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    const-string v2, "AESWrap"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 98
    sget-object v0, Lcom/android/sec/org/bouncycastle/operator/jcajce/OperatorHelper;->symmetricWrapperAlgNames:Ljava/util/Map;

    sget-object v1, Lcom/android/sec/org/bouncycastle/asn1/ntt/NTTObjectIdentifiers;->id_camellia128_wrap:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    const-string v2, "CamelliaWrap"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 99
    sget-object v0, Lcom/android/sec/org/bouncycastle/operator/jcajce/OperatorHelper;->symmetricWrapperAlgNames:Ljava/util/Map;

    sget-object v1, Lcom/android/sec/org/bouncycastle/asn1/ntt/NTTObjectIdentifiers;->id_camellia192_wrap:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    const-string v2, "CamelliaWrap"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 100
    sget-object v0, Lcom/android/sec/org/bouncycastle/operator/jcajce/OperatorHelper;->symmetricWrapperAlgNames:Ljava/util/Map;

    sget-object v1, Lcom/android/sec/org/bouncycastle/asn1/ntt/NTTObjectIdentifiers;->id_camellia256_wrap:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    const-string v2, "CamelliaWrap"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 101
    sget-object v0, Lcom/android/sec/org/bouncycastle/operator/jcajce/OperatorHelper;->symmetricWrapperAlgNames:Ljava/util/Map;

    sget-object v1, Lcom/android/sec/org/bouncycastle/asn1/kisa/KISAObjectIdentifiers;->id_npki_app_cmsSeed_wrap:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    const-string v2, "SEEDWrap"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 102
    sget-object v0, Lcom/android/sec/org/bouncycastle/operator/jcajce/OperatorHelper;->symmetricWrapperAlgNames:Ljava/util/Map;

    sget-object v1, Lcom/android/sec/org/bouncycastle/asn1/pkcs/PKCSObjectIdentifiers;->des_EDE3_CBC:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    const-string v2, "DESede"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 104
    sget-object v0, Lcom/android/sec/org/bouncycastle/operator/jcajce/OperatorHelper;->symmetricKeyAlgNames:Ljava/util/Map;

    sget-object v1, Lcom/android/sec/org/bouncycastle/asn1/nist/NISTObjectIdentifiers;->aes:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    const-string v2, "AES"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 105
    sget-object v0, Lcom/android/sec/org/bouncycastle/operator/jcajce/OperatorHelper;->symmetricKeyAlgNames:Ljava/util/Map;

    sget-object v1, Lcom/android/sec/org/bouncycastle/asn1/nist/NISTObjectIdentifiers;->id_aes128_CBC:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    const-string v2, "AES"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 106
    sget-object v0, Lcom/android/sec/org/bouncycastle/operator/jcajce/OperatorHelper;->symmetricKeyAlgNames:Ljava/util/Map;

    sget-object v1, Lcom/android/sec/org/bouncycastle/asn1/nist/NISTObjectIdentifiers;->id_aes192_CBC:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    const-string v2, "AES"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 107
    sget-object v0, Lcom/android/sec/org/bouncycastle/operator/jcajce/OperatorHelper;->symmetricKeyAlgNames:Ljava/util/Map;

    sget-object v1, Lcom/android/sec/org/bouncycastle/asn1/nist/NISTObjectIdentifiers;->id_aes256_CBC:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    const-string v2, "AES"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 108
    sget-object v0, Lcom/android/sec/org/bouncycastle/operator/jcajce/OperatorHelper;->symmetricKeyAlgNames:Ljava/util/Map;

    sget-object v1, Lcom/android/sec/org/bouncycastle/asn1/pkcs/PKCSObjectIdentifiers;->des_EDE3_CBC:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    const-string v2, "DESede"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 109
    sget-object v0, Lcom/android/sec/org/bouncycastle/operator/jcajce/OperatorHelper;->symmetricKeyAlgNames:Ljava/util/Map;

    sget-object v1, Lcom/android/sec/org/bouncycastle/asn1/pkcs/PKCSObjectIdentifiers;->RC2_CBC:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    const-string v2, "RC2"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 110
    return-void
.end method

.method constructor <init>(Lcom/android/sec/org/bouncycastle/jcajce/JcaJceHelper;)V
    .locals 0
    .param p1, "helper"    # Lcom/android/sec/org/bouncycastle/jcajce/JcaJceHelper;

    .prologue
    .line 115
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 116
    iput-object p1, p0, Lcom/android/sec/org/bouncycastle/operator/jcajce/OperatorHelper;->helper:Lcom/android/sec/org/bouncycastle/jcajce/JcaJceHelper;

    .line 117
    return-void
.end method

.method private static getDigestAlgName(Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;)Ljava/lang/String;
    .locals 1
    .param p0, "digestAlgOID"    # Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    .prologue
    .line 312
    sget-object v0, Lcom/android/sec/org/bouncycastle/asn1/pkcs/PKCSObjectIdentifiers;->md5:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    invoke-virtual {v0, p0}, Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 314
    const-string v0, "MD5"

    .line 358
    :goto_0
    return-object v0

    .line 316
    :cond_0
    sget-object v0, Lcom/android/sec/org/bouncycastle/asn1/oiw/OIWObjectIdentifiers;->idSHA1:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    invoke-virtual {v0, p0}, Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 318
    const-string v0, "SHA1"

    goto :goto_0

    .line 326
    :cond_1
    sget-object v0, Lcom/android/sec/org/bouncycastle/asn1/nist/NISTObjectIdentifiers;->id_sha256:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    invoke-virtual {v0, p0}, Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 328
    const-string v0, "SHA256"

    goto :goto_0

    .line 330
    :cond_2
    sget-object v0, Lcom/android/sec/org/bouncycastle/asn1/nist/NISTObjectIdentifiers;->id_sha384:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    invoke-virtual {v0, p0}, Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 332
    const-string v0, "SHA384"

    goto :goto_0

    .line 334
    :cond_3
    sget-object v0, Lcom/android/sec/org/bouncycastle/asn1/nist/NISTObjectIdentifiers;->id_sha512:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    invoke-virtual {v0, p0}, Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 336
    const-string v0, "SHA512"

    goto :goto_0

    .line 358
    :cond_4
    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;->getId()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static getSignatureName(Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;)Ljava/lang/String;
    .locals 4
    .param p0, "sigAlgId"    # Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;

    .prologue
    .line 290
    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;->getParameters()Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;

    move-result-object v0

    .line 292
    .local v0, "params":Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;
    if-eqz v0, :cond_0

    sget-object v2, Lcom/android/sec/org/bouncycastle/asn1/DERNull;->INSTANCE:Lcom/android/sec/org/bouncycastle/asn1/DERNull;

    invoke-virtual {v2, v0}, Lcom/android/sec/org/bouncycastle/asn1/DERNull;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 294
    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;->getAlgorithm()Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    move-result-object v2

    sget-object v3, Lcom/android/sec/org/bouncycastle/asn1/pkcs/PKCSObjectIdentifiers;->id_RSASSA_PSS:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    invoke-virtual {v2, v3}, Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 296
    invoke-static {v0}, Lcom/android/sec/org/bouncycastle/asn1/pkcs/RSASSAPSSparams;->getInstance(Ljava/lang/Object;)Lcom/android/sec/org/bouncycastle/asn1/pkcs/RSASSAPSSparams;

    move-result-object v1

    .line 297
    .local v1, "rsaParams":Lcom/android/sec/org/bouncycastle/asn1/pkcs/RSASSAPSSparams;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Lcom/android/sec/org/bouncycastle/asn1/pkcs/RSASSAPSSparams;->getHashAlgorithm()Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;->getAlgorithm()Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    move-result-object v3

    invoke-static {v3}, Lcom/android/sec/org/bouncycastle/operator/jcajce/OperatorHelper;->getDigestAlgName(Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "WITHRSAANDMGF1"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 306
    .end local v1    # "rsaParams":Lcom/android/sec/org/bouncycastle/asn1/pkcs/RSASSAPSSparams;
    :goto_0
    return-object v2

    .line 301
    :cond_0
    sget-object v2, Lcom/android/sec/org/bouncycastle/operator/jcajce/OperatorHelper;->oids:Ljava/util/Map;

    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;->getAlgorithm()Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 303
    sget-object v2, Lcom/android/sec/org/bouncycastle/operator/jcajce/OperatorHelper;->oids:Ljava/util/Map;

    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;->getAlgorithm()Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    goto :goto_0

    .line 306
    :cond_1
    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;->getAlgorithm()Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;->getId()Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method


# virtual methods
.method public convertCertificate(Lcom/android/sec/org/bouncycastle/cert/X509CertificateHolder;)Ljava/security/cert/X509Certificate;
    .locals 5
    .param p1, "certHolder"    # Lcom/android/sec/org/bouncycastle/cert/X509CertificateHolder;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/cert/CertificateException;
        }
    .end annotation

    .prologue
    .line 368
    :try_start_0
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/operator/jcajce/OperatorHelper;->helper:Lcom/android/sec/org/bouncycastle/jcajce/JcaJceHelper;

    const-string v3, "X.509"

    invoke-interface {v2, v3}, Lcom/android/sec/org/bouncycastle/jcajce/JcaJceHelper;->createCertificateFactory(Ljava/lang/String;)Ljava/security/cert/CertificateFactory;

    move-result-object v0

    .line 370
    .local v0, "certFact":Ljava/security/cert/CertificateFactory;
    new-instance v2, Ljava/io/ByteArrayInputStream;

    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/cert/X509CertificateHolder;->getEncoded()[B

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-virtual {v0, v2}, Ljava/security/cert/CertificateFactory;->generateCertificate(Ljava/io/InputStream;)Ljava/security/cert/Certificate;

    move-result-object v2

    check-cast v2, Ljava/security/cert/X509Certificate;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/security/NoSuchProviderException; {:try_start_0 .. :try_end_0} :catch_2

    return-object v2

    .line 372
    .end local v0    # "certFact":Ljava/security/cert/CertificateFactory;
    :catch_0
    move-exception v1

    .line 374
    .local v1, "e":Ljava/io/IOException;
    new-instance v2, Lcom/android/sec/org/bouncycastle/operator/jcajce/OperatorHelper$OpCertificateException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "cannot get encoded form of certificate: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, v1}, Lcom/android/sec/org/bouncycastle/operator/jcajce/OperatorHelper$OpCertificateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2

    .line 376
    .end local v1    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v1

    .line 378
    .local v1, "e":Ljava/security/NoSuchAlgorithmException;
    new-instance v2, Lcom/android/sec/org/bouncycastle/operator/jcajce/OperatorHelper$OpCertificateException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "cannot create certificate factory: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/security/NoSuchAlgorithmException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, v1}, Lcom/android/sec/org/bouncycastle/operator/jcajce/OperatorHelper$OpCertificateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2

    .line 380
    .end local v1    # "e":Ljava/security/NoSuchAlgorithmException;
    :catch_2
    move-exception v1

    .line 382
    .local v1, "e":Ljava/security/NoSuchProviderException;
    new-instance v2, Lcom/android/sec/org/bouncycastle/operator/jcajce/OperatorHelper$OpCertificateException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "cannot find factory provider: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/security/NoSuchProviderException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, v1}, Lcom/android/sec/org/bouncycastle/operator/jcajce/OperatorHelper$OpCertificateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
.end method

.method createAsymmetricWrapper(Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;Ljava/util/Map;)Ljavax/crypto/Cipher;
    .locals 6
    .param p1, "algorithm"    # Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;
    .param p2, "extraAlgNames"    # Ljava/util/Map;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/operator/OperatorCreationException;
        }
    .end annotation

    .prologue
    .line 124
    const/4 v1, 0x0

    .line 126
    .local v1, "cipherName":Ljava/lang/String;
    :try_start_0
    invoke-interface {p2}, Ljava/util/Map;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 128
    invoke-interface {p2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Ljava/lang/String;

    move-object v1, v0

    .line 131
    :cond_0
    if-nez v1, :cond_1

    .line 133
    sget-object v3, Lcom/android/sec/org/bouncycastle/operator/jcajce/OperatorHelper;->asymmetricWrapperAlgNames:Ljava/util/Map;

    invoke-interface {v3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Ljava/lang/String;

    move-object v1, v0
    :try_end_0
    .catch Ljava/security/GeneralSecurityException; {:try_start_0 .. :try_end_0} :catch_2

    .line 136
    :cond_1
    if-eqz v1, :cond_2

    .line 141
    :try_start_1
    iget-object v3, p0, Lcom/android/sec/org/bouncycastle/operator/jcajce/OperatorHelper;->helper:Lcom/android/sec/org/bouncycastle/jcajce/JcaJceHelper;

    invoke-interface {v3, v1}, Lcom/android/sec/org/bouncycastle/jcajce/JcaJceHelper;->createCipher(Ljava/lang/String;)Ljavax/crypto/Cipher;
    :try_end_1
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/security/GeneralSecurityException; {:try_start_1 .. :try_end_1} :catch_2

    move-result-object v3

    .line 161
    :goto_0
    return-object v3

    .line 143
    :catch_0
    move-exception v2

    .line 146
    .local v2, "e":Ljava/security/NoSuchAlgorithmException;
    :try_start_2
    const-string v3, "RSA/ECB/PKCS1Padding"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_2
    .catch Ljava/security/GeneralSecurityException; {:try_start_2 .. :try_end_2} :catch_2

    move-result v3

    if-eqz v3, :cond_2

    .line 150
    :try_start_3
    iget-object v3, p0, Lcom/android/sec/org/bouncycastle/operator/jcajce/OperatorHelper;->helper:Lcom/android/sec/org/bouncycastle/jcajce/JcaJceHelper;

    const-string v4, "RSA/NONE/PKCS1Padding"

    invoke-interface {v3, v4}, Lcom/android/sec/org/bouncycastle/jcajce/JcaJceHelper;->createCipher(Ljava/lang/String;)Ljavax/crypto/Cipher;
    :try_end_3
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/security/GeneralSecurityException; {:try_start_3 .. :try_end_3} :catch_2

    move-result-object v3

    goto :goto_0

    .line 152
    :catch_1
    move-exception v3

    .line 161
    .end local v2    # "e":Ljava/security/NoSuchAlgorithmException;
    :cond_2
    :try_start_4
    iget-object v3, p0, Lcom/android/sec/org/bouncycastle/operator/jcajce/OperatorHelper;->helper:Lcom/android/sec/org/bouncycastle/jcajce/JcaJceHelper;

    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Lcom/android/sec/org/bouncycastle/jcajce/JcaJceHelper;->createCipher(Ljava/lang/String;)Ljavax/crypto/Cipher;
    :try_end_4
    .catch Ljava/security/GeneralSecurityException; {:try_start_4 .. :try_end_4} :catch_2

    move-result-object v3

    goto :goto_0

    .line 163
    :catch_2
    move-exception v2

    .line 165
    .local v2, "e":Ljava/security/GeneralSecurityException;
    new-instance v3, Lcom/android/sec/org/bouncycastle/operator/OperatorCreationException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "cannot create cipher: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Ljava/security/GeneralSecurityException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4, v2}, Lcom/android/sec/org/bouncycastle/operator/OperatorCreationException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3
.end method

.method createDigest(Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;)Ljava/security/MessageDigest;
    .locals 5
    .param p1, "digAlgId"    # Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/GeneralSecurityException;
        }
    .end annotation

    .prologue
    .line 203
    :try_start_0
    iget-object v3, p0, Lcom/android/sec/org/bouncycastle/operator/jcajce/OperatorHelper;->helper:Lcom/android/sec/org/bouncycastle/jcajce/JcaJceHelper;

    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;->getAlgorithm()Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    move-result-object v4

    invoke-static {v4}, Lcom/android/sec/org/bouncycastle/operator/jcajce/OperatorHelper;->getDigestAlgName(Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Lcom/android/sec/org/bouncycastle/jcajce/JcaJceHelper;->createDigest(Ljava/lang/String;)Ljava/security/MessageDigest;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 222
    .local v0, "dig":Ljava/security/MessageDigest;
    :goto_0
    return-object v0

    .line 205
    .end local v0    # "dig":Ljava/security/MessageDigest;
    :catch_0
    move-exception v2

    .line 210
    .local v2, "e":Ljava/security/NoSuchAlgorithmException;
    sget-object v3, Lcom/android/sec/org/bouncycastle/operator/jcajce/OperatorHelper;->oids:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;->getAlgorithm()Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 212
    sget-object v3, Lcom/android/sec/org/bouncycastle/operator/jcajce/OperatorHelper;->oids:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;->getAlgorithm()Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 214
    .local v1, "digestAlgorithm":Ljava/lang/String;
    iget-object v3, p0, Lcom/android/sec/org/bouncycastle/operator/jcajce/OperatorHelper;->helper:Lcom/android/sec/org/bouncycastle/jcajce/JcaJceHelper;

    invoke-interface {v3, v1}, Lcom/android/sec/org/bouncycastle/jcajce/JcaJceHelper;->createDigest(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v0

    .line 215
    .restart local v0    # "dig":Ljava/security/MessageDigest;
    goto :goto_0

    .line 218
    .end local v0    # "dig":Ljava/security/MessageDigest;
    .end local v1    # "digestAlgorithm":Ljava/lang/String;
    :cond_0
    throw v2
.end method

.method public createRawSignature(Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;)Ljava/security/Signature;
    .locals 7
    .param p1, "algorithm"    # Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;

    .prologue
    .line 260
    :try_start_0
    invoke-static {p1}, Lcom/android/sec/org/bouncycastle/operator/jcajce/OperatorHelper;->getSignatureName(Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;)Ljava/lang/String;

    move-result-object v0

    .line 262
    .local v0, "algName":Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "NONE"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "WITH"

    invoke-virtual {v0, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v0, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 264
    iget-object v5, p0, Lcom/android/sec/org/bouncycastle/operator/jcajce/OperatorHelper;->helper:Lcom/android/sec/org/bouncycastle/jcajce/JcaJceHelper;

    invoke-interface {v5, v0}, Lcom/android/sec/org/bouncycastle/jcajce/JcaJceHelper;->createSignature(Ljava/lang/String;)Ljava/security/Signature;

    move-result-object v3

    .line 269
    .local v3, "sig":Ljava/security/Signature;
    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;->getAlgorithm()Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    move-result-object v5

    sget-object v6, Lcom/android/sec/org/bouncycastle/asn1/pkcs/PKCSObjectIdentifiers;->id_RSASSA_PSS:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    invoke-virtual {v5, v6}, Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 271
    iget-object v5, p0, Lcom/android/sec/org/bouncycastle/operator/jcajce/OperatorHelper;->helper:Lcom/android/sec/org/bouncycastle/jcajce/JcaJceHelper;

    invoke-interface {v5, v0}, Lcom/android/sec/org/bouncycastle/jcajce/JcaJceHelper;->createAlgorithmParameters(Ljava/lang/String;)Ljava/security/AlgorithmParameters;

    move-result-object v2

    .line 273
    .local v2, "params":Ljava/security/AlgorithmParameters;
    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;->getParameters()Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;

    move-result-object v5

    invoke-interface {v5}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Encodable;->toASN1Primitive()Lcom/android/sec/org/bouncycastle/asn1/ASN1Primitive;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/sec/org/bouncycastle/asn1/ASN1Primitive;->getEncoded()[B

    move-result-object v5

    const-string v6, "ASN.1"

    invoke-virtual {v2, v5, v6}, Ljava/security/AlgorithmParameters;->init([BLjava/lang/String;)V

    .line 275
    const-class v5, Ljava/security/spec/PSSParameterSpec;

    invoke-virtual {v2, v5}, Ljava/security/AlgorithmParameters;->getParameterSpec(Ljava/lang/Class;)Ljava/security/spec/AlgorithmParameterSpec;

    move-result-object v4

    check-cast v4, Ljava/security/spec/PSSParameterSpec;

    .line 276
    .local v4, "spec":Ljava/security/spec/PSSParameterSpec;
    invoke-virtual {v3, v4}, Ljava/security/Signature;->setParameter(Ljava/security/spec/AlgorithmParameterSpec;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 284
    .end local v0    # "algName":Ljava/lang/String;
    .end local v2    # "params":Ljava/security/AlgorithmParameters;
    .end local v3    # "sig":Ljava/security/Signature;
    .end local v4    # "spec":Ljava/security/spec/PSSParameterSpec;
    :cond_0
    :goto_0
    return-object v3

    .line 279
    :catch_0
    move-exception v1

    .line 281
    .local v1, "e":Ljava/lang/Exception;
    const/4 v3, 0x0

    goto :goto_0
.end method

.method createSignature(Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;)Ljava/security/Signature;
    .locals 5
    .param p1, "sigAlgId"    # Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/GeneralSecurityException;
        }
    .end annotation

    .prologue
    .line 232
    :try_start_0
    iget-object v3, p0, Lcom/android/sec/org/bouncycastle/operator/jcajce/OperatorHelper;->helper:Lcom/android/sec/org/bouncycastle/jcajce/JcaJceHelper;

    invoke-static {p1}, Lcom/android/sec/org/bouncycastle/operator/jcajce/OperatorHelper;->getSignatureName(Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Lcom/android/sec/org/bouncycastle/jcajce/JcaJceHelper;->createSignature(Ljava/lang/String;)Ljava/security/Signature;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 251
    .local v1, "sig":Ljava/security/Signature;
    :goto_0
    return-object v1

    .line 234
    .end local v1    # "sig":Ljava/security/Signature;
    :catch_0
    move-exception v0

    .line 239
    .local v0, "e":Ljava/security/NoSuchAlgorithmException;
    sget-object v3, Lcom/android/sec/org/bouncycastle/operator/jcajce/OperatorHelper;->oids:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;->getAlgorithm()Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 241
    sget-object v3, Lcom/android/sec/org/bouncycastle/operator/jcajce/OperatorHelper;->oids:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;->getAlgorithm()Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 243
    .local v2, "signatureAlgorithm":Ljava/lang/String;
    iget-object v3, p0, Lcom/android/sec/org/bouncycastle/operator/jcajce/OperatorHelper;->helper:Lcom/android/sec/org/bouncycastle/jcajce/JcaJceHelper;

    invoke-interface {v3, v2}, Lcom/android/sec/org/bouncycastle/jcajce/JcaJceHelper;->createSignature(Ljava/lang/String;)Ljava/security/Signature;

    move-result-object v1

    .line 244
    .restart local v1    # "sig":Ljava/security/Signature;
    goto :goto_0

    .line 247
    .end local v1    # "sig":Ljava/security/Signature;
    .end local v2    # "signatureAlgorithm":Ljava/lang/String;
    :cond_0
    throw v0
.end method

.method createSymmetricWrapper(Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;)Ljavax/crypto/Cipher;
    .locals 5
    .param p1, "algorithm"    # Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/operator/OperatorCreationException;
        }
    .end annotation

    .prologue
    .line 174
    :try_start_0
    sget-object v2, Lcom/android/sec/org/bouncycastle/operator/jcajce/OperatorHelper;->symmetricWrapperAlgNames:Ljava/util/Map;

    invoke-interface {v2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;
    :try_end_0
    .catch Ljava/security/GeneralSecurityException; {:try_start_0 .. :try_end_0} :catch_1

    .line 176
    .local v0, "cipherName":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 181
    :try_start_1
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/operator/jcajce/OperatorHelper;->helper:Lcom/android/sec/org/bouncycastle/jcajce/JcaJceHelper;

    invoke-interface {v2, v0}, Lcom/android/sec/org/bouncycastle/jcajce/JcaJceHelper;->createCipher(Ljava/lang/String;)Ljavax/crypto/Cipher;
    :try_end_1
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/security/GeneralSecurityException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v2

    .line 188
    :goto_0
    return-object v2

    .line 183
    :catch_0
    move-exception v2

    .line 188
    :cond_0
    :try_start_2
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/operator/jcajce/OperatorHelper;->helper:Lcom/android/sec/org/bouncycastle/jcajce/JcaJceHelper;

    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/android/sec/org/bouncycastle/jcajce/JcaJceHelper;->createCipher(Ljava/lang/String;)Ljavax/crypto/Cipher;
    :try_end_2
    .catch Ljava/security/GeneralSecurityException; {:try_start_2 .. :try_end_2} :catch_1

    move-result-object v2

    goto :goto_0

    .line 190
    .end local v0    # "cipherName":Ljava/lang/String;
    :catch_1
    move-exception v1

    .line 192
    .local v1, "e":Ljava/security/GeneralSecurityException;
    new-instance v2, Lcom/android/sec/org/bouncycastle/operator/OperatorCreationException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "cannot create cipher: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/security/GeneralSecurityException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, v1}, Lcom/android/sec/org/bouncycastle/operator/OperatorCreationException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
.end method

.method getKeyAlgorithmName(Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;)Ljava/lang/String;
    .locals 2
    .param p1, "oid"    # Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    .prologue
    .line 408
    sget-object v1, Lcom/android/sec/org/bouncycastle/operator/jcajce/OperatorHelper;->symmetricKeyAlgNames:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 410
    .local v0, "name":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 415
    .end local v0    # "name":Ljava/lang/String;
    :goto_0
    return-object v0

    .restart local v0    # "name":Ljava/lang/String;
    :cond_0
    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;->getId()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
