.class public Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/DSA$Mappings;
.super Lcom/android/sec/org/bouncycastle/jcajce/provider/util/AsymmetricAlgorithmProvider;
.source "DSA.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/DSA;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Mappings"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/jcajce/provider/util/AsymmetricAlgorithmProvider;-><init>()V

    .line 19
    return-void
.end method


# virtual methods
.method public configure(Lcom/android/sec/org/bouncycastle/jcajce/provider/config/ConfigurableProvider;)V
    .locals 4
    .param p1, "provider"    # Lcom/android/sec/org/bouncycastle/jcajce/provider/config/ConfigurableProvider;

    .prologue
    .line 23
    const-string v2, "AlgorithmParameters.DSA"

    const-string v3, "com.android.sec.org.bouncycastle.jcajce.provider.asymmetric.dsa.AlgorithmParametersSpi"

    invoke-interface {p1, v2, v3}, Lcom/android/sec/org/bouncycastle/jcajce/provider/config/ConfigurableProvider;->addAlgorithm(Ljava/lang/String;Ljava/lang/String;)V

    .line 25
    const-string v2, "AlgorithmParameterGenerator.DSA"

    const-string v3, "com.android.sec.org.bouncycastle.jcajce.provider.asymmetric.dsa.AlgorithmParameterGeneratorSpi"

    invoke-interface {p1, v2, v3}, Lcom/android/sec/org/bouncycastle/jcajce/provider/config/ConfigurableProvider;->addAlgorithm(Ljava/lang/String;Ljava/lang/String;)V

    .line 27
    const-string v2, "KeyPairGenerator.DSA"

    const-string v3, "com.android.sec.org.bouncycastle.jcajce.provider.asymmetric.dsa.KeyPairGeneratorSpi"

    invoke-interface {p1, v2, v3}, Lcom/android/sec/org/bouncycastle/jcajce/provider/config/ConfigurableProvider;->addAlgorithm(Ljava/lang/String;Ljava/lang/String;)V

    .line 28
    const-string v2, "KeyFactory.DSA"

    const-string v3, "com.android.sec.org.bouncycastle.jcajce.provider.asymmetric.dsa.KeyFactorySpi"

    invoke-interface {p1, v2, v3}, Lcom/android/sec/org/bouncycastle/jcajce/provider/config/ConfigurableProvider;->addAlgorithm(Ljava/lang/String;Ljava/lang/String;)V

    .line 31
    const-string v2, "Signature.SHA1withDSA"

    const-string v3, "com.android.sec.org.bouncycastle.jcajce.provider.asymmetric.dsa.DSASigner$stdDSA"

    invoke-interface {p1, v2, v3}, Lcom/android/sec/org/bouncycastle/jcajce/provider/config/ConfigurableProvider;->addAlgorithm(Ljava/lang/String;Ljava/lang/String;)V

    .line 33
    const-string v2, "Signature.NONEWITHDSA"

    const-string v3, "com.android.sec.org.bouncycastle.jcajce.provider.asymmetric.dsa.DSASigner$noneDSA"

    invoke-interface {p1, v2, v3}, Lcom/android/sec/org/bouncycastle/jcajce/provider/config/ConfigurableProvider;->addAlgorithm(Ljava/lang/String;Ljava/lang/String;)V

    .line 35
    const-string v2, "Alg.Alias.Signature.RAWDSA"

    const-string v3, "NONEWITHDSA"

    invoke-interface {p1, v2, v3}, Lcom/android/sec/org/bouncycastle/jcajce/provider/config/ConfigurableProvider;->addAlgorithm(Ljava/lang/String;Ljava/lang/String;)V

    .line 45
    const-string v2, "Alg.Alias.Signature.DSA"

    const-string v3, "SHA1withDSA"

    invoke-interface {p1, v2, v3}, Lcom/android/sec/org/bouncycastle/jcajce/provider/config/ConfigurableProvider;->addAlgorithm(Ljava/lang/String;Ljava/lang/String;)V

    .line 48
    const-string v2, "Alg.Alias.Signature.SHA/DSA"

    const-string v3, "SHA1withDSA"

    invoke-interface {p1, v2, v3}, Lcom/android/sec/org/bouncycastle/jcajce/provider/config/ConfigurableProvider;->addAlgorithm(Ljava/lang/String;Ljava/lang/String;)V

    .line 49
    const-string v2, "Alg.Alias.Signature.SHA1WITHDSA"

    const-string v3, "SHA1withDSA"

    invoke-interface {p1, v2, v3}, Lcom/android/sec/org/bouncycastle/jcajce/provider/config/ConfigurableProvider;->addAlgorithm(Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    const-string v2, "Alg.Alias.Signature.1.3.14.3.2.26with1.2.840.10040.4.1"

    const-string v3, "SHA1withDSA"

    invoke-interface {p1, v2, v3}, Lcom/android/sec/org/bouncycastle/jcajce/provider/config/ConfigurableProvider;->addAlgorithm(Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    const-string v2, "Alg.Alias.Signature.1.3.14.3.2.26with1.2.840.10040.4.3"

    const-string v3, "SHA1withDSA"

    invoke-interface {p1, v2, v3}, Lcom/android/sec/org/bouncycastle/jcajce/provider/config/ConfigurableProvider;->addAlgorithm(Ljava/lang/String;Ljava/lang/String;)V

    .line 52
    const-string v2, "Alg.Alias.Signature.DSAwithSHA1"

    const-string v3, "SHA1withDSA"

    invoke-interface {p1, v2, v3}, Lcom/android/sec/org/bouncycastle/jcajce/provider/config/ConfigurableProvider;->addAlgorithm(Ljava/lang/String;Ljava/lang/String;)V

    .line 53
    const-string v2, "Alg.Alias.Signature.DSAWITHSHA1"

    const-string v3, "SHA1withDSA"

    invoke-interface {p1, v2, v3}, Lcom/android/sec/org/bouncycastle/jcajce/provider/config/ConfigurableProvider;->addAlgorithm(Ljava/lang/String;Ljava/lang/String;)V

    .line 54
    const-string v2, "Alg.Alias.Signature.SHA1WithDSA"

    const-string v3, "SHA1withDSA"

    invoke-interface {p1, v2, v3}, Lcom/android/sec/org/bouncycastle/jcajce/provider/config/ConfigurableProvider;->addAlgorithm(Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    const-string v2, "Alg.Alias.Signature.DSAWithSHA1"

    const-string v3, "SHA1withDSA"

    invoke-interface {p1, v2, v3}, Lcom/android/sec/org/bouncycastle/jcajce/provider/config/ConfigurableProvider;->addAlgorithm(Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    const-string v2, "Alg.Alias.Signature.1.2.840.10040.4.3"

    const-string v3, "SHA1withDSA"

    invoke-interface {p1, v2, v3}, Lcom/android/sec/org/bouncycastle/jcajce/provider/config/ConfigurableProvider;->addAlgorithm(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    new-instance v1, Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/dsa/KeyFactorySpi;

    invoke-direct {v1}, Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/dsa/KeyFactorySpi;-><init>()V

    .line 62
    .local v1, "keyFact":Lcom/android/sec/org/bouncycastle/jcajce/provider/util/AsymmetricKeyInfoConverter;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    sget-object v2, Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/dsa/DSAUtil;->dsaOids:[Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    array-length v2, v2

    if-eq v0, v2, :cond_0

    .line 65
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Alg.Alias.Signature."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/dsa/DSAUtil;->dsaOids:[Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    aget-object v3, v3, v0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "SHA1withDSA"

    invoke-interface {p1, v2, v3}, Lcom/android/sec/org/bouncycastle/jcajce/provider/config/ConfigurableProvider;->addAlgorithm(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    sget-object v2, Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/dsa/DSAUtil;->dsaOids:[Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    aget-object v2, v2, v0

    const-string v3, "DSA"

    invoke-virtual {p0, p1, v2, v3, v1}, Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/DSA$Mappings;->registerOid(Lcom/android/sec/org/bouncycastle/jcajce/provider/config/ConfigurableProvider;Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;Ljava/lang/String;Lcom/android/sec/org/bouncycastle/jcajce/provider/util/AsymmetricKeyInfoConverter;)V

    .line 69
    sget-object v2, Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/dsa/DSAUtil;->dsaOids:[Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    aget-object v2, v2, v0

    const-string v3, "DSA"

    invoke-virtual {p0, p1, v2, v3}, Lcom/android/sec/org/bouncycastle/jcajce/provider/asymmetric/DSA$Mappings;->registerOidAlgorithmParameters(Lcom/android/sec/org/bouncycastle/jcajce/provider/config/ConfigurableProvider;Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;Ljava/lang/String;)V

    .line 62
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 71
    :cond_0
    return-void
.end method
