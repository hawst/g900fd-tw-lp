.class public Lcom/android/sec/org/bouncycastle/cms/SignerInformationVerifier;
.super Ljava/lang/Object;
.source "SignerInformationVerifier.java"


# instance fields
.field private digestProvider:Lcom/android/sec/org/bouncycastle/operator/DigestCalculatorProvider;

.field private sigAlgorithmFinder:Lcom/android/sec/org/bouncycastle/operator/SignatureAlgorithmIdentifierFinder;

.field private sigNameGenerator:Lcom/android/sec/org/bouncycastle/cms/CMSSignatureAlgorithmNameGenerator;

.field private verifierProvider:Lcom/android/sec/org/bouncycastle/operator/ContentVerifierProvider;


# direct methods
.method public constructor <init>(Lcom/android/sec/org/bouncycastle/cms/CMSSignatureAlgorithmNameGenerator;Lcom/android/sec/org/bouncycastle/operator/SignatureAlgorithmIdentifierFinder;Lcom/android/sec/org/bouncycastle/operator/ContentVerifierProvider;Lcom/android/sec/org/bouncycastle/operator/DigestCalculatorProvider;)V
    .locals 0
    .param p1, "sigNameGenerator"    # Lcom/android/sec/org/bouncycastle/cms/CMSSignatureAlgorithmNameGenerator;
    .param p2, "sigAlgorithmFinder"    # Lcom/android/sec/org/bouncycastle/operator/SignatureAlgorithmIdentifierFinder;
    .param p3, "verifierProvider"    # Lcom/android/sec/org/bouncycastle/operator/ContentVerifierProvider;
    .param p4, "digestProvider"    # Lcom/android/sec/org/bouncycastle/operator/DigestCalculatorProvider;

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInformationVerifier;->sigNameGenerator:Lcom/android/sec/org/bouncycastle/cms/CMSSignatureAlgorithmNameGenerator;

    .line 22
    iput-object p2, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInformationVerifier;->sigAlgorithmFinder:Lcom/android/sec/org/bouncycastle/operator/SignatureAlgorithmIdentifierFinder;

    .line 23
    iput-object p3, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInformationVerifier;->verifierProvider:Lcom/android/sec/org/bouncycastle/operator/ContentVerifierProvider;

    .line 24
    iput-object p4, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInformationVerifier;->digestProvider:Lcom/android/sec/org/bouncycastle/operator/DigestCalculatorProvider;

    .line 25
    return-void
.end method


# virtual methods
.method public getAssociatedCertificate()Lcom/android/sec/org/bouncycastle/cert/X509CertificateHolder;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInformationVerifier;->verifierProvider:Lcom/android/sec/org/bouncycastle/operator/ContentVerifierProvider;

    invoke-interface {v0}, Lcom/android/sec/org/bouncycastle/operator/ContentVerifierProvider;->getAssociatedCertificate()Lcom/android/sec/org/bouncycastle/cert/X509CertificateHolder;

    move-result-object v0

    return-object v0
.end method

.method public getContentVerifier(Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;)Lcom/android/sec/org/bouncycastle/operator/ContentVerifier;
    .locals 3
    .param p1, "signingAlgorithm"    # Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;
    .param p2, "digestAlgorithm"    # Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/operator/OperatorCreationException;
        }
    .end annotation

    .prologue
    .line 40
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInformationVerifier;->sigNameGenerator:Lcom/android/sec/org/bouncycastle/cms/CMSSignatureAlgorithmNameGenerator;

    invoke-interface {v1, p2, p1}, Lcom/android/sec/org/bouncycastle/cms/CMSSignatureAlgorithmNameGenerator;->getSignatureName(Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;)Ljava/lang/String;

    move-result-object v0

    .line 42
    .local v0, "signatureName":Ljava/lang/String;
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInformationVerifier;->verifierProvider:Lcom/android/sec/org/bouncycastle/operator/ContentVerifierProvider;

    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInformationVerifier;->sigAlgorithmFinder:Lcom/android/sec/org/bouncycastle/operator/SignatureAlgorithmIdentifierFinder;

    invoke-interface {v2, v0}, Lcom/android/sec/org/bouncycastle/operator/SignatureAlgorithmIdentifierFinder;->find(Ljava/lang/String;)Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/android/sec/org/bouncycastle/operator/ContentVerifierProvider;->get(Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;)Lcom/android/sec/org/bouncycastle/operator/ContentVerifier;

    move-result-object v1

    return-object v1
.end method

.method public getDigestCalculator(Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;)Lcom/android/sec/org/bouncycastle/operator/DigestCalculator;
    .locals 1
    .param p1, "algorithmIdentifier"    # Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/operator/OperatorCreationException;
        }
    .end annotation

    .prologue
    .line 48
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInformationVerifier;->digestProvider:Lcom/android/sec/org/bouncycastle/operator/DigestCalculatorProvider;

    invoke-interface {v0, p1}, Lcom/android/sec/org/bouncycastle/operator/DigestCalculatorProvider;->get(Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;)Lcom/android/sec/org/bouncycastle/operator/DigestCalculator;

    move-result-object v0

    return-object v0
.end method

.method public hasAssociatedCertificate()Z
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/SignerInformationVerifier;->verifierProvider:Lcom/android/sec/org/bouncycastle/operator/ContentVerifierProvider;

    invoke-interface {v0}, Lcom/android/sec/org/bouncycastle/operator/ContentVerifierProvider;->hasAssociatedCertificate()Z

    move-result v0

    return v0
.end method
