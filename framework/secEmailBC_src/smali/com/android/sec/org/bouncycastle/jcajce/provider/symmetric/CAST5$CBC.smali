.class public Lcom/android/sec/org/bouncycastle/jcajce/provider/symmetric/CAST5$CBC;
.super Lcom/android/sec/org/bouncycastle/jcajce/provider/symmetric/util/BaseBlockCipher;
.source "CAST5.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/sec/org/bouncycastle/jcajce/provider/symmetric/CAST5;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "CBC"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 45
    new-instance v0, Lcom/android/sec/org/bouncycastle/crypto/modes/CBCBlockCipher;

    new-instance v1, Lcom/android/sec/org/bouncycastle/crypto/engines/CAST5Engine;

    invoke-direct {v1}, Lcom/android/sec/org/bouncycastle/crypto/engines/CAST5Engine;-><init>()V

    invoke-direct {v0, v1}, Lcom/android/sec/org/bouncycastle/crypto/modes/CBCBlockCipher;-><init>(Lcom/android/sec/org/bouncycastle/crypto/BlockCipher;)V

    const/16 v1, 0x40

    invoke-direct {p0, v0, v1}, Lcom/android/sec/org/bouncycastle/jcajce/provider/symmetric/util/BaseBlockCipher;-><init>(Lcom/android/sec/org/bouncycastle/crypto/BlockCipher;I)V

    .line 46
    return-void
.end method
