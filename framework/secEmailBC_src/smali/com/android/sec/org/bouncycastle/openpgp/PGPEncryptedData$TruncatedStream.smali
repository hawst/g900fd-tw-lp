.class public Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedData$TruncatedStream;
.super Ljava/io/InputStream;
.source "PGPEncryptedData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "TruncatedStream"
.end annotation


# instance fields
.field bufPtr:I

.field in:Ljava/io/InputStream;

.field lookAhead:[I

.field final synthetic this$0:Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedData;


# direct methods
.method constructor <init>(Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedData;Ljava/io/InputStream;)V
    .locals 3
    .param p2, "in"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 26
    iput-object p1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedData$TruncatedStream;->this$0:Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedData;

    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    .line 19
    const/16 v1, 0x16

    new-array v1, v1, [I

    iput-object v1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedData$TruncatedStream;->lookAhead:[I

    .line 27
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedData$TruncatedStream;->lookAhead:[I

    array-length v1, v1

    if-eq v0, v1, :cond_1

    .line 29
    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedData$TruncatedStream;->lookAhead:[I

    invoke-virtual {p2}, Ljava/io/InputStream;->read()I

    move-result v2

    aput v2, v1, v0

    if-gez v2, :cond_0

    .line 31
    new-instance v1, Ljava/io/EOFException;

    invoke-direct {v1}, Ljava/io/EOFException;-><init>()V

    throw v1

    .line 27
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 35
    :cond_1
    const/4 v1, 0x0

    iput v1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedData$TruncatedStream;->bufPtr:I

    .line 36
    iput-object p2, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedData$TruncatedStream;->in:Ljava/io/InputStream;

    .line 37
    return-void
.end method


# virtual methods
.method getLookAhead()[I
    .locals 5

    .prologue
    .line 59
    iget-object v4, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedData$TruncatedStream;->lookAhead:[I

    array-length v4, v4

    new-array v3, v4, [I

    .line 60
    .local v3, "tmp":[I
    const/4 v0, 0x0

    .line 62
    .local v0, "count":I
    iget v2, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedData$TruncatedStream;->bufPtr:I

    .local v2, "i":I
    :goto_0
    iget-object v4, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedData$TruncatedStream;->lookAhead:[I

    array-length v4, v4

    if-eq v2, v4, :cond_0

    .line 64
    add-int/lit8 v1, v0, 0x1

    .end local v0    # "count":I
    .local v1, "count":I
    iget-object v4, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedData$TruncatedStream;->lookAhead:[I

    aget v4, v4, v2

    aput v4, v3, v0

    .line 62
    add-int/lit8 v2, v2, 0x1

    move v0, v1

    .end local v1    # "count":I
    .restart local v0    # "count":I
    goto :goto_0

    .line 66
    :cond_0
    const/4 v2, 0x0

    :goto_1
    iget v4, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedData$TruncatedStream;->bufPtr:I

    if-eq v2, v4, :cond_1

    .line 68
    add-int/lit8 v1, v0, 0x1

    .end local v0    # "count":I
    .restart local v1    # "count":I
    iget-object v4, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedData$TruncatedStream;->lookAhead:[I

    aget v4, v4, v2

    aput v4, v3, v0

    .line 66
    add-int/lit8 v2, v2, 0x1

    move v0, v1

    .end local v1    # "count":I
    .restart local v0    # "count":I
    goto :goto_1

    .line 71
    :cond_1
    return-object v3
.end method

.method public read()I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 42
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedData$TruncatedStream;->in:Ljava/io/InputStream;

    invoke-virtual {v2}, Ljava/io/InputStream;->read()I

    move-result v1

    .line 44
    .local v1, "ch":I
    if-ltz v1, :cond_0

    .line 46
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedData$TruncatedStream;->lookAhead:[I

    iget v3, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedData$TruncatedStream;->bufPtr:I

    aget v0, v2, v3

    .line 48
    .local v0, "c":I
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedData$TruncatedStream;->lookAhead:[I

    iget v3, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedData$TruncatedStream;->bufPtr:I

    aput v1, v2, v3

    .line 49
    iget v2, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedData$TruncatedStream;->bufPtr:I

    add-int/lit8 v2, v2, 0x1

    iget-object v3, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedData$TruncatedStream;->lookAhead:[I

    array-length v3, v3

    rem-int/2addr v2, v3

    iput v2, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedData$TruncatedStream;->bufPtr:I

    .line 54
    .end local v0    # "c":I
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method
