.class public Lcom/android/sec/org/bouncycastle/bcpg/sig/SignerUserID;
.super Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;
.source "SignerUserID.java"


# direct methods
.method public constructor <init>(ZLjava/lang/String;)V
    .locals 2
    .param p1, "critical"    # Z
    .param p2, "userID"    # Ljava/lang/String;

    .prologue
    .line 36
    const/16 v0, 0x1c

    invoke-static {p2}, Lcom/android/sec/org/bouncycastle/bcpg/sig/SignerUserID;->userIDToBytes(Ljava/lang/String;)[B

    move-result-object v1

    invoke-direct {p0, v0, p1, v1}, Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;-><init>(IZ[B)V

    .line 37
    return-void
.end method

.method public constructor <init>(Z[B)V
    .locals 1
    .param p1, "critical"    # Z
    .param p2, "data"    # [B

    .prologue
    .line 29
    const/16 v0, 0x1c

    invoke-direct {p0, v0, p1, p2}, Lcom/android/sec/org/bouncycastle/bcpg/SignatureSubpacket;-><init>(IZ[B)V

    .line 30
    return-void
.end method

.method private static userIDToBytes(Ljava/lang/String;)[B
    .locals 3
    .param p0, "id"    # Ljava/lang/String;

    .prologue
    .line 15
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    new-array v1, v2, [B

    .line 17
    .local v1, "idData":[B
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    if-eq v0, v2, :cond_0

    .line 19
    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    int-to-byte v2, v2

    aput-byte v2, v1, v0

    .line 17
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 22
    :cond_0
    return-object v1
.end method


# virtual methods
.method public getID()Ljava/lang/String;
    .locals 3

    .prologue
    .line 41
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/sig/SignerUserID;->data:[B

    array-length v2, v2

    new-array v0, v2, [C

    .line 43
    .local v0, "chars":[C
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v2, v0

    if-eq v1, v2, :cond_0

    .line 45
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/bcpg/sig/SignerUserID;->data:[B

    aget-byte v2, v2, v1

    and-int/lit16 v2, v2, 0xff

    int-to-char v2, v2

    aput-char v2, v0, v1

    .line 43
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 48
    :cond_0
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v0}, Ljava/lang/String;-><init>([C)V

    return-object v2
.end method
