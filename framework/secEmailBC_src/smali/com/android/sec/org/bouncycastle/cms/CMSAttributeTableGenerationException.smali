.class public Lcom/android/sec/org/bouncycastle/cms/CMSAttributeTableGenerationException;
.super Lcom/android/sec/org/bouncycastle/cms/CMSRuntimeException;
.source "CMSAttributeTableGenerationException.java"


# instance fields
.field e:Ljava/lang/Exception;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 11
    invoke-direct {p0, p1}, Lcom/android/sec/org/bouncycastle/cms/CMSRuntimeException;-><init>(Ljava/lang/String;)V

    .line 12
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "e"    # Ljava/lang/Exception;

    .prologue
    .line 18
    invoke-direct {p0, p1}, Lcom/android/sec/org/bouncycastle/cms/CMSRuntimeException;-><init>(Ljava/lang/String;)V

    .line 20
    iput-object p2, p0, Lcom/android/sec/org/bouncycastle/cms/CMSAttributeTableGenerationException;->e:Ljava/lang/Exception;

    .line 21
    return-void
.end method


# virtual methods
.method public getCause()Ljava/lang/Throwable;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/CMSAttributeTableGenerationException;->e:Ljava/lang/Exception;

    return-object v0
.end method

.method public getUnderlyingException()Ljava/lang/Exception;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/CMSAttributeTableGenerationException;->e:Ljava/lang/Exception;

    return-object v0
.end method
