.class Lcom/android/sec/org/bouncycastle/cms/CMSSignedDataGenerator$1;
.super Ljava/lang/Object;
.source "CMSSignedDataGenerator.java"

# interfaces
.implements Lcom/android/sec/org/bouncycastle/cms/CMSTypedData;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/sec/org/bouncycastle/cms/CMSSignedDataGenerator;->generate(Ljava/lang/String;Lcom/android/sec/org/bouncycastle/cms/CMSProcessable;ZLjava/security/Provider;Z)Lcom/android/sec/org/bouncycastle/cms/CMSSignedData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/sec/org/bouncycastle/cms/CMSSignedDataGenerator;

.field final synthetic val$content:Lcom/android/sec/org/bouncycastle/cms/CMSProcessable;

.field final synthetic val$contentTypeOID:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;


# direct methods
.method constructor <init>(Lcom/android/sec/org/bouncycastle/cms/CMSSignedDataGenerator;Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;Lcom/android/sec/org/bouncycastle/cms/CMSProcessable;)V
    .locals 0

    .prologue
    .line 527
    iput-object p1, p0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedDataGenerator$1;->this$0:Lcom/android/sec/org/bouncycastle/cms/CMSSignedDataGenerator;

    iput-object p2, p0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedDataGenerator$1;->val$contentTypeOID:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    iput-object p3, p0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedDataGenerator$1;->val$content:Lcom/android/sec/org/bouncycastle/cms/CMSProcessable;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getContent()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 541
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedDataGenerator$1;->val$content:Lcom/android/sec/org/bouncycastle/cms/CMSProcessable;

    invoke-interface {v0}, Lcom/android/sec/org/bouncycastle/cms/CMSProcessable;->getContent()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getContentType()Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;
    .locals 1

    .prologue
    .line 530
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedDataGenerator$1;->val$contentTypeOID:Lcom/android/sec/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    return-object v0
.end method

.method public write(Ljava/io/OutputStream;)V
    .locals 1
    .param p1, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/android/sec/org/bouncycastle/cms/CMSException;
        }
    .end annotation

    .prologue
    .line 536
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/CMSSignedDataGenerator$1;->val$content:Lcom/android/sec/org/bouncycastle/cms/CMSProcessable;

    invoke-interface {v0, p1}, Lcom/android/sec/org/bouncycastle/cms/CMSProcessable;->write(Ljava/io/OutputStream;)V

    .line 537
    return-void
.end method
