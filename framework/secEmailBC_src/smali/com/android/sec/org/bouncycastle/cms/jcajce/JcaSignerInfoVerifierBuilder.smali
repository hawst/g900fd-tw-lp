.class public Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSignerInfoVerifierBuilder;
.super Ljava/lang/Object;
.source "JcaSignerInfoVerifierBuilder.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSignerInfoVerifierBuilder$1;,
        Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSignerInfoVerifierBuilder$ProviderHelper;,
        Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSignerInfoVerifierBuilder$NamedHelper;,
        Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSignerInfoVerifierBuilder$Helper;
    }
.end annotation


# instance fields
.field private digestProvider:Lcom/android/sec/org/bouncycastle/operator/DigestCalculatorProvider;

.field private helper:Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSignerInfoVerifierBuilder$Helper;

.field private sigAlgIDFinder:Lcom/android/sec/org/bouncycastle/operator/SignatureAlgorithmIdentifierFinder;

.field private sigAlgNameGen:Lcom/android/sec/org/bouncycastle/cms/CMSSignatureAlgorithmNameGenerator;


# direct methods
.method public constructor <init>(Lcom/android/sec/org/bouncycastle/operator/DigestCalculatorProvider;)V
    .locals 2
    .param p1, "digestProvider"    # Lcom/android/sec/org/bouncycastle/operator/DigestCalculatorProvider;

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    new-instance v0, Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSignerInfoVerifierBuilder$Helper;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSignerInfoVerifierBuilder$Helper;-><init>(Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSignerInfoVerifierBuilder;Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSignerInfoVerifierBuilder$1;)V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSignerInfoVerifierBuilder;->helper:Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSignerInfoVerifierBuilder$Helper;

    .line 24
    new-instance v0, Lcom/android/sec/org/bouncycastle/cms/DefaultCMSSignatureAlgorithmNameGenerator;

    invoke-direct {v0}, Lcom/android/sec/org/bouncycastle/cms/DefaultCMSSignatureAlgorithmNameGenerator;-><init>()V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSignerInfoVerifierBuilder;->sigAlgNameGen:Lcom/android/sec/org/bouncycastle/cms/CMSSignatureAlgorithmNameGenerator;

    .line 25
    new-instance v0, Lcom/android/sec/org/bouncycastle/operator/DefaultSignatureAlgorithmIdentifierFinder;

    invoke-direct {v0}, Lcom/android/sec/org/bouncycastle/operator/DefaultSignatureAlgorithmIdentifierFinder;-><init>()V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSignerInfoVerifierBuilder;->sigAlgIDFinder:Lcom/android/sec/org/bouncycastle/operator/SignatureAlgorithmIdentifierFinder;

    .line 29
    iput-object p1, p0, Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSignerInfoVerifierBuilder;->digestProvider:Lcom/android/sec/org/bouncycastle/operator/DigestCalculatorProvider;

    .line 30
    return-void
.end method


# virtual methods
.method public build(Lcom/android/sec/org/bouncycastle/cert/X509CertificateHolder;)Lcom/android/sec/org/bouncycastle/cms/SignerInformationVerifier;
    .locals 5
    .param p1, "certHolder"    # Lcom/android/sec/org/bouncycastle/cert/X509CertificateHolder;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/operator/OperatorCreationException;,
            Ljava/security/cert/CertificateException;
        }
    .end annotation

    .prologue
    .line 69
    new-instance v0, Lcom/android/sec/org/bouncycastle/cms/SignerInformationVerifier;

    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSignerInfoVerifierBuilder;->sigAlgNameGen:Lcom/android/sec/org/bouncycastle/cms/CMSSignatureAlgorithmNameGenerator;

    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSignerInfoVerifierBuilder;->sigAlgIDFinder:Lcom/android/sec/org/bouncycastle/operator/SignatureAlgorithmIdentifierFinder;

    iget-object v3, p0, Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSignerInfoVerifierBuilder;->helper:Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSignerInfoVerifierBuilder$Helper;

    invoke-virtual {v3, p1}, Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSignerInfoVerifierBuilder$Helper;->createContentVerifierProvider(Lcom/android/sec/org/bouncycastle/cert/X509CertificateHolder;)Lcom/android/sec/org/bouncycastle/operator/ContentVerifierProvider;

    move-result-object v3

    iget-object v4, p0, Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSignerInfoVerifierBuilder;->digestProvider:Lcom/android/sec/org/bouncycastle/operator/DigestCalculatorProvider;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/sec/org/bouncycastle/cms/SignerInformationVerifier;-><init>(Lcom/android/sec/org/bouncycastle/cms/CMSSignatureAlgorithmNameGenerator;Lcom/android/sec/org/bouncycastle/operator/SignatureAlgorithmIdentifierFinder;Lcom/android/sec/org/bouncycastle/operator/ContentVerifierProvider;Lcom/android/sec/org/bouncycastle/operator/DigestCalculatorProvider;)V

    return-object v0
.end method

.method public build(Ljava/security/PublicKey;)Lcom/android/sec/org/bouncycastle/cms/SignerInformationVerifier;
    .locals 5
    .param p1, "pubKey"    # Ljava/security/PublicKey;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/operator/OperatorCreationException;
        }
    .end annotation

    .prologue
    .line 81
    new-instance v0, Lcom/android/sec/org/bouncycastle/cms/SignerInformationVerifier;

    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSignerInfoVerifierBuilder;->sigAlgNameGen:Lcom/android/sec/org/bouncycastle/cms/CMSSignatureAlgorithmNameGenerator;

    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSignerInfoVerifierBuilder;->sigAlgIDFinder:Lcom/android/sec/org/bouncycastle/operator/SignatureAlgorithmIdentifierFinder;

    iget-object v3, p0, Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSignerInfoVerifierBuilder;->helper:Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSignerInfoVerifierBuilder$Helper;

    invoke-virtual {v3, p1}, Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSignerInfoVerifierBuilder$Helper;->createContentVerifierProvider(Ljava/security/PublicKey;)Lcom/android/sec/org/bouncycastle/operator/ContentVerifierProvider;

    move-result-object v3

    iget-object v4, p0, Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSignerInfoVerifierBuilder;->digestProvider:Lcom/android/sec/org/bouncycastle/operator/DigestCalculatorProvider;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/sec/org/bouncycastle/cms/SignerInformationVerifier;-><init>(Lcom/android/sec/org/bouncycastle/cms/CMSSignatureAlgorithmNameGenerator;Lcom/android/sec/org/bouncycastle/operator/SignatureAlgorithmIdentifierFinder;Lcom/android/sec/org/bouncycastle/operator/ContentVerifierProvider;Lcom/android/sec/org/bouncycastle/operator/DigestCalculatorProvider;)V

    return-object v0
.end method

.method public build(Ljava/security/cert/X509Certificate;)Lcom/android/sec/org/bouncycastle/cms/SignerInformationVerifier;
    .locals 5
    .param p1, "certificate"    # Ljava/security/cert/X509Certificate;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/operator/OperatorCreationException;
        }
    .end annotation

    .prologue
    .line 75
    new-instance v0, Lcom/android/sec/org/bouncycastle/cms/SignerInformationVerifier;

    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSignerInfoVerifierBuilder;->sigAlgNameGen:Lcom/android/sec/org/bouncycastle/cms/CMSSignatureAlgorithmNameGenerator;

    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSignerInfoVerifierBuilder;->sigAlgIDFinder:Lcom/android/sec/org/bouncycastle/operator/SignatureAlgorithmIdentifierFinder;

    iget-object v3, p0, Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSignerInfoVerifierBuilder;->helper:Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSignerInfoVerifierBuilder$Helper;

    invoke-virtual {v3, p1}, Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSignerInfoVerifierBuilder$Helper;->createContentVerifierProvider(Ljava/security/cert/X509Certificate;)Lcom/android/sec/org/bouncycastle/operator/ContentVerifierProvider;

    move-result-object v3

    iget-object v4, p0, Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSignerInfoVerifierBuilder;->digestProvider:Lcom/android/sec/org/bouncycastle/operator/DigestCalculatorProvider;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/sec/org/bouncycastle/cms/SignerInformationVerifier;-><init>(Lcom/android/sec/org/bouncycastle/cms/CMSSignatureAlgorithmNameGenerator;Lcom/android/sec/org/bouncycastle/operator/SignatureAlgorithmIdentifierFinder;Lcom/android/sec/org/bouncycastle/operator/ContentVerifierProvider;Lcom/android/sec/org/bouncycastle/operator/DigestCalculatorProvider;)V

    return-object v0
.end method

.method public setProvider(Ljava/lang/String;)Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSignerInfoVerifierBuilder;
    .locals 1
    .param p1, "providerName"    # Ljava/lang/String;

    .prologue
    .line 41
    new-instance v0, Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSignerInfoVerifierBuilder$NamedHelper;

    invoke-direct {v0, p0, p1}, Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSignerInfoVerifierBuilder$NamedHelper;-><init>(Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSignerInfoVerifierBuilder;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSignerInfoVerifierBuilder;->helper:Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSignerInfoVerifierBuilder$Helper;

    .line 43
    return-object p0
.end method

.method public setProvider(Ljava/security/Provider;)Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSignerInfoVerifierBuilder;
    .locals 1
    .param p1, "provider"    # Ljava/security/Provider;

    .prologue
    .line 34
    new-instance v0, Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSignerInfoVerifierBuilder$ProviderHelper;

    invoke-direct {v0, p0, p1}, Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSignerInfoVerifierBuilder$ProviderHelper;-><init>(Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSignerInfoVerifierBuilder;Ljava/security/Provider;)V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSignerInfoVerifierBuilder;->helper:Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSignerInfoVerifierBuilder$Helper;

    .line 36
    return-object p0
.end method

.method public setSignatureAlgorithmFinder(Lcom/android/sec/org/bouncycastle/operator/SignatureAlgorithmIdentifierFinder;)Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSignerInfoVerifierBuilder;
    .locals 0
    .param p1, "sigAlgIDFinder"    # Lcom/android/sec/org/bouncycastle/operator/SignatureAlgorithmIdentifierFinder;

    .prologue
    .line 61
    iput-object p1, p0, Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSignerInfoVerifierBuilder;->sigAlgIDFinder:Lcom/android/sec/org/bouncycastle/operator/SignatureAlgorithmIdentifierFinder;

    .line 63
    return-object p0
.end method

.method public setSignatureAlgorithmNameGenerator(Lcom/android/sec/org/bouncycastle/cms/CMSSignatureAlgorithmNameGenerator;)Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSignerInfoVerifierBuilder;
    .locals 0
    .param p1, "sigAlgNameGen"    # Lcom/android/sec/org/bouncycastle/cms/CMSSignatureAlgorithmNameGenerator;

    .prologue
    .line 54
    iput-object p1, p0, Lcom/android/sec/org/bouncycastle/cms/jcajce/JcaSignerInfoVerifierBuilder;->sigAlgNameGen:Lcom/android/sec/org/bouncycastle/cms/CMSSignatureAlgorithmNameGenerator;

    .line 56
    return-object p0
.end method
