.class abstract Lcom/android/sec/org/bouncycastle/crypto/modes/gcm/GCMUtil;
.super Ljava/lang/Object;
.source "GCMUtil.java"


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static asBytes([I)[B
    .locals 2
    .param p0, "ns"    # [I

    .prologue
    .line 24
    const/16 v1, 0x10

    new-array v0, v1, [B

    .line 25
    .local v0, "output":[B
    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lcom/android/sec/org/bouncycastle/crypto/util/Pack;->intToBigEndian([I[BI)V

    .line 26
    return-object v0
.end method

.method static asInts([B[I)V
    .locals 1
    .param p0, "bs"    # [B
    .param p1, "output"    # [I

    .prologue
    .line 38
    const/4 v0, 0x0

    invoke-static {p0, v0, p1}, Lcom/android/sec/org/bouncycastle/crypto/util/Pack;->bigEndianToInt([BI[I)V

    .line 39
    return-void
.end method

.method static asInts([B)[I
    .locals 2
    .param p0, "bs"    # [B

    .prologue
    .line 31
    const/4 v1, 0x4

    new-array v0, v1, [I

    .line 32
    .local v0, "output":[I
    const/4 v1, 0x0

    invoke-static {p0, v1, v0}, Lcom/android/sec/org/bouncycastle/crypto/util/Pack;->bigEndianToInt([BI[I)V

    .line 33
    return-object v0
.end method

.method static multiply([B[B)V
    .locals 10
    .param p0, "block"    # [B
    .param p1, "val"    # [B

    .prologue
    const/4 v6, 0x1

    const/16 v9, 0x10

    const/4 v7, 0x0

    .line 43
    invoke-static {p0}, Lcom/android/sec/org/bouncycastle/util/Arrays;->clone([B)[B

    move-result-object v5

    .line 44
    .local v5, "tmp":[B
    new-array v1, v9, [B

    .line 46
    .local v1, "c":[B
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v9, :cond_4

    .line 48
    aget-byte v0, p1, v2

    .line 49
    .local v0, "bits":B
    const/4 v3, 0x7

    .local v3, "j":I
    :goto_1
    if-ltz v3, :cond_3

    .line 51
    shl-int v8, v6, v3

    and-int/2addr v8, v0

    if-eqz v8, :cond_0

    .line 53
    invoke-static {v1, v5}, Lcom/android/sec/org/bouncycastle/crypto/modes/gcm/GCMUtil;->xor([B[B)V

    .line 56
    :cond_0
    const/16 v8, 0xf

    aget-byte v8, v5, v8

    and-int/lit8 v8, v8, 0x1

    if-eqz v8, :cond_2

    move v4, v6

    .line 57
    .local v4, "lsb":Z
    :goto_2
    invoke-static {v5}, Lcom/android/sec/org/bouncycastle/crypto/modes/gcm/GCMUtil;->shiftRight([B)V

    .line 58
    if-eqz v4, :cond_1

    .line 62
    aget-byte v8, v5, v7

    xor-int/lit8 v8, v8, -0x1f

    int-to-byte v8, v8

    aput-byte v8, v5, v7

    .line 49
    :cond_1
    add-int/lit8 v3, v3, -0x1

    goto :goto_1

    .end local v4    # "lsb":Z
    :cond_2
    move v4, v7

    .line 56
    goto :goto_2

    .line 46
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 67
    .end local v0    # "bits":B
    .end local v3    # "j":I
    :cond_4
    invoke-static {v1, v7, p0, v7, v9}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 68
    return-void
.end method

.method static multiplyP([I)V
    .locals 4
    .param p0, "x"    # [I

    .prologue
    const/4 v1, 0x0

    .line 73
    const/4 v2, 0x3

    aget v2, p0, v2

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_1

    const/4 v0, 0x1

    .line 74
    .local v0, "lsb":Z
    :goto_0
    invoke-static {p0}, Lcom/android/sec/org/bouncycastle/crypto/modes/gcm/GCMUtil;->shiftRight([I)V

    .line 75
    if-eqz v0, :cond_0

    .line 79
    aget v2, p0, v1

    const/high16 v3, -0x1f000000

    xor-int/2addr v2, v3

    aput v2, p0, v1

    .line 81
    :cond_0
    return-void

    .end local v0    # "lsb":Z
    :cond_1
    move v0, v1

    .line 73
    goto :goto_0
.end method

.method static multiplyP([I[I)V
    .locals 4
    .param p0, "x"    # [I
    .param p1, "output"    # [I

    .prologue
    const/4 v1, 0x0

    .line 85
    const/4 v2, 0x3

    aget v2, p0, v2

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_1

    const/4 v0, 0x1

    .line 86
    .local v0, "lsb":Z
    :goto_0
    invoke-static {p0, p1}, Lcom/android/sec/org/bouncycastle/crypto/modes/gcm/GCMUtil;->shiftRight([I[I)V

    .line 87
    if-eqz v0, :cond_0

    .line 89
    aget v2, p1, v1

    const/high16 v3, -0x1f000000

    xor-int/2addr v2, v3

    aput v2, p1, v1

    .line 91
    :cond_0
    return-void

    .end local v0    # "lsb":Z
    :cond_1
    move v0, v1

    .line 85
    goto :goto_0
.end method

.method static multiplyP8([I)V
    .locals 6
    .param p0, "x"    # [I

    .prologue
    .line 101
    const/4 v2, 0x3

    aget v1, p0, v2

    .line 102
    .local v1, "lsw":I
    const/16 v2, 0x8

    invoke-static {p0, v2}, Lcom/android/sec/org/bouncycastle/crypto/modes/gcm/GCMUtil;->shiftRightN([II)V

    .line 103
    const/4 v0, 0x7

    .local v0, "i":I
    :goto_0
    if-ltz v0, :cond_1

    .line 105
    const/4 v2, 0x1

    shl-int/2addr v2, v0

    and-int/2addr v2, v1

    if-eqz v2, :cond_0

    .line 107
    const/4 v2, 0x0

    aget v3, p0, v2

    const/high16 v4, -0x1f000000

    rsub-int/lit8 v5, v0, 0x7

    ushr-int/2addr v4, v5

    xor-int/2addr v3, v4

    aput v3, p0, v2

    .line 103
    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 110
    :cond_1
    return-void
.end method

.method static multiplyP8([I[I)V
    .locals 6
    .param p0, "x"    # [I
    .param p1, "output"    # [I

    .prologue
    .line 114
    const/4 v2, 0x3

    aget v1, p0, v2

    .line 115
    .local v1, "lsw":I
    const/16 v2, 0x8

    invoke-static {p0, v2, p1}, Lcom/android/sec/org/bouncycastle/crypto/modes/gcm/GCMUtil;->shiftRightN([II[I)V

    .line 116
    const/4 v0, 0x7

    .local v0, "i":I
    :goto_0
    if-ltz v0, :cond_1

    .line 118
    const/4 v2, 0x1

    shl-int/2addr v2, v0

    and-int/2addr v2, v1

    if-eqz v2, :cond_0

    .line 120
    const/4 v2, 0x0

    aget v3, p1, v2

    const/high16 v4, -0x1f000000

    rsub-int/lit8 v5, v0, 0x7

    ushr-int/2addr v4, v5

    xor-int/2addr v3, v4

    aput v3, p1, v2

    .line 116
    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 123
    :cond_1
    return-void
.end method

.method static oneAsBytes()[B
    .locals 3

    .prologue
    .line 10
    const/16 v1, 0x10

    new-array v0, v1, [B

    .line 11
    .local v0, "tmp":[B
    const/4 v1, 0x0

    const/16 v2, -0x80

    aput-byte v2, v0, v1

    .line 12
    return-object v0
.end method

.method static oneAsInts()[I
    .locals 3

    .prologue
    .line 17
    const/4 v1, 0x4

    new-array v0, v1, [I

    .line 18
    .local v0, "tmp":[I
    const/4 v1, 0x0

    const/high16 v2, -0x80000000

    aput v2, v0, v1

    .line 19
    return-object v0
.end method

.method static shiftRight([B)V
    .locals 4
    .param p0, "block"    # [B

    .prologue
    .line 127
    const/4 v2, 0x0

    .line 128
    .local v2, "i":I
    const/4 v1, 0x0

    .line 131
    .local v1, "bit":I
    :goto_0
    aget-byte v3, p0, v2

    and-int/lit16 v0, v3, 0xff

    .line 132
    .local v0, "b":I
    ushr-int/lit8 v3, v0, 0x1

    or-int/2addr v3, v1

    int-to-byte v3, v3

    aput-byte v3, p0, v2

    .line 133
    add-int/lit8 v2, v2, 0x1

    const/16 v3, 0x10

    if-ne v2, v3, :cond_0

    .line 139
    return-void

    .line 137
    :cond_0
    and-int/lit8 v3, v0, 0x1

    shl-int/lit8 v1, v3, 0x7

    .line 138
    goto :goto_0
.end method

.method static shiftRight([B[B)V
    .locals 4
    .param p0, "block"    # [B
    .param p1, "output"    # [B

    .prologue
    .line 143
    const/4 v2, 0x0

    .line 144
    .local v2, "i":I
    const/4 v1, 0x0

    .line 147
    .local v1, "bit":I
    :goto_0
    aget-byte v3, p0, v2

    and-int/lit16 v0, v3, 0xff

    .line 148
    .local v0, "b":I
    ushr-int/lit8 v3, v0, 0x1

    or-int/2addr v3, v1

    int-to-byte v3, v3

    aput-byte v3, p1, v2

    .line 149
    add-int/lit8 v2, v2, 0x1

    const/16 v3, 0x10

    if-ne v2, v3, :cond_0

    .line 155
    return-void

    .line 153
    :cond_0
    and-int/lit8 v3, v0, 0x1

    shl-int/lit8 v1, v3, 0x7

    .line 154
    goto :goto_0
.end method

.method static shiftRight([I)V
    .locals 4
    .param p0, "block"    # [I

    .prologue
    .line 159
    const/4 v2, 0x0

    .line 160
    .local v2, "i":I
    const/4 v1, 0x0

    .line 163
    .local v1, "bit":I
    :goto_0
    aget v0, p0, v2

    .line 164
    .local v0, "b":I
    ushr-int/lit8 v3, v0, 0x1

    or-int/2addr v3, v1

    aput v3, p0, v2

    .line 165
    add-int/lit8 v2, v2, 0x1

    const/4 v3, 0x4

    if-ne v2, v3, :cond_0

    .line 171
    return-void

    .line 169
    :cond_0
    shl-int/lit8 v1, v0, 0x1f

    .line 170
    goto :goto_0
.end method

.method static shiftRight([I[I)V
    .locals 4
    .param p0, "block"    # [I
    .param p1, "output"    # [I

    .prologue
    .line 175
    const/4 v2, 0x0

    .line 176
    .local v2, "i":I
    const/4 v1, 0x0

    .line 179
    .local v1, "bit":I
    :goto_0
    aget v0, p0, v2

    .line 180
    .local v0, "b":I
    ushr-int/lit8 v3, v0, 0x1

    or-int/2addr v3, v1

    aput v3, p1, v2

    .line 181
    add-int/lit8 v2, v2, 0x1

    const/4 v3, 0x4

    if-ne v2, v3, :cond_0

    .line 187
    return-void

    .line 185
    :cond_0
    shl-int/lit8 v1, v0, 0x1f

    .line 186
    goto :goto_0
.end method

.method static shiftRightN([II)V
    .locals 4
    .param p0, "block"    # [I
    .param p1, "n"    # I

    .prologue
    .line 191
    const/4 v2, 0x0

    .line 192
    .local v2, "i":I
    const/4 v1, 0x0

    .line 195
    .local v1, "bits":I
    :goto_0
    aget v0, p0, v2

    .line 196
    .local v0, "b":I
    ushr-int v3, v0, p1

    or-int/2addr v3, v1

    aput v3, p0, v2

    .line 197
    add-int/lit8 v2, v2, 0x1

    const/4 v3, 0x4

    if-ne v2, v3, :cond_0

    .line 203
    return-void

    .line 201
    :cond_0
    rsub-int/lit8 v3, p1, 0x20

    shl-int v1, v0, v3

    .line 202
    goto :goto_0
.end method

.method static shiftRightN([II[I)V
    .locals 4
    .param p0, "block"    # [I
    .param p1, "n"    # I
    .param p2, "output"    # [I

    .prologue
    .line 207
    const/4 v2, 0x0

    .line 208
    .local v2, "i":I
    const/4 v1, 0x0

    .line 211
    .local v1, "bits":I
    :goto_0
    aget v0, p0, v2

    .line 212
    .local v0, "b":I
    ushr-int v3, v0, p1

    or-int/2addr v3, v1

    aput v3, p2, v2

    .line 213
    add-int/lit8 v2, v2, 0x1

    const/4 v3, 0x4

    if-ne v2, v3, :cond_0

    .line 219
    return-void

    .line 217
    :cond_0
    rsub-int/lit8 v3, p1, 0x20

    shl-int v1, v0, v3

    .line 218
    goto :goto_0
.end method

.method static xor([B[B)V
    .locals 3
    .param p0, "block"    # [B
    .param p1, "val"    # [B

    .prologue
    .line 223
    const/16 v0, 0xf

    .local v0, "i":I
    :goto_0
    if-ltz v0, :cond_0

    .line 225
    aget-byte v1, p0, v0

    aget-byte v2, p1, v0

    xor-int/2addr v1, v2

    int-to-byte v1, v1

    aput-byte v1, p0, v0

    .line 223
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 227
    :cond_0
    return-void
.end method

.method static xor([B[BII)V
    .locals 3
    .param p0, "block"    # [B
    .param p1, "val"    # [B
    .param p2, "off"    # I
    .param p3, "len"    # I

    .prologue
    .line 231
    move v0, p3

    .end local p3    # "len":I
    .local v0, "len":I
    :goto_0
    add-int/lit8 p3, v0, -0x1

    .end local v0    # "len":I
    .restart local p3    # "len":I
    if-lez v0, :cond_0

    .line 233
    aget-byte v1, p0, p3

    add-int v2, p2, p3

    aget-byte v2, p1, v2

    xor-int/2addr v1, v2

    int-to-byte v1, v1

    aput-byte v1, p0, p3

    move v0, p3

    .end local p3    # "len":I
    .restart local v0    # "len":I
    goto :goto_0

    .line 235
    .end local v0    # "len":I
    .restart local p3    # "len":I
    :cond_0
    return-void
.end method

.method static xor([B[B[B)V
    .locals 3
    .param p0, "block"    # [B
    .param p1, "val"    # [B
    .param p2, "output"    # [B

    .prologue
    .line 239
    const/16 v0, 0xf

    .local v0, "i":I
    :goto_0
    if-ltz v0, :cond_0

    .line 241
    aget-byte v1, p0, v0

    aget-byte v2, p1, v0

    xor-int/2addr v1, v2

    int-to-byte v1, v1

    aput-byte v1, p2, v0

    .line 239
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 243
    :cond_0
    return-void
.end method

.method static xor([I[I)V
    .locals 3
    .param p0, "block"    # [I
    .param p1, "val"    # [I

    .prologue
    .line 247
    const/4 v0, 0x3

    .local v0, "i":I
    :goto_0
    if-ltz v0, :cond_0

    .line 249
    aget v1, p0, v0

    aget v2, p1, v0

    xor-int/2addr v1, v2

    aput v1, p0, v0

    .line 247
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 251
    :cond_0
    return-void
.end method

.method static xor([I[I[I)V
    .locals 3
    .param p0, "block"    # [I
    .param p1, "val"    # [I
    .param p2, "output"    # [I

    .prologue
    .line 255
    const/4 v0, 0x3

    .local v0, "i":I
    :goto_0
    if-ltz v0, :cond_0

    .line 257
    aget v1, p0, v0

    aget v2, p1, v0

    xor-int/2addr v1, v2

    aput v1, p2, v0

    .line 255
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 259
    :cond_0
    return-void
.end method
