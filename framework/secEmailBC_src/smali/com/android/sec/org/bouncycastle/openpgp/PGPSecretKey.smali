.class public Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;
.super Ljava/lang/Object;
.source "PGPSecretKey.java"


# instance fields
.field final pub:Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;

.field final secret:Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;


# direct methods
.method public constructor <init>(IILjava/security/PublicKey;Ljava/security/PrivateKey;Ljava/util/Date;Ljava/lang/String;I[CLcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;Ljava/security/SecureRandom;Ljava/lang/String;)V
    .locals 10
    .param p1, "certificationLevel"    # I
    .param p2, "algorithm"    # I
    .param p3, "pubKey"    # Ljava/security/PublicKey;
    .param p4, "privKey"    # Ljava/security/PrivateKey;
    .param p5, "time"    # Ljava/util/Date;
    .param p6, "id"    # Ljava/lang/String;
    .param p7, "encAlgorithm"    # I
    .param p8, "passPhrase"    # [C
    .param p9, "hashedPcks"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;
    .param p10, "unhashedPcks"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;
    .param p11, "rand"    # Ljava/security/SecureRandom;
    .param p12, "provider"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;,
            Ljava/security/NoSuchProviderException;
        }
    .end annotation

    .prologue
    .line 304
    new-instance v2, Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyPair;

    invoke-direct {v2, p2, p3, p4, p5}, Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyPair;-><init>(ILjava/security/PublicKey;Ljava/security/PrivateKey;Ljava/util/Date;)V

    move-object v0, p0

    move v1, p1

    move-object/from16 v3, p6

    move/from16 v4, p7

    move-object/from16 v5, p8

    move-object/from16 v6, p9

    move-object/from16 v7, p10

    move-object/from16 v8, p11

    move-object/from16 v9, p12

    invoke-direct/range {v0 .. v9}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;-><init>(ILcom/android/sec/org/bouncycastle/openpgp/PGPKeyPair;Ljava/lang/String;I[CLcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;Ljava/security/SecureRandom;Ljava/lang/String;)V

    .line 305
    return-void
.end method

.method public constructor <init>(IILjava/security/PublicKey;Ljava/security/PrivateKey;Ljava/util/Date;Ljava/lang/String;I[CZLcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;Ljava/security/SecureRandom;Ljava/lang/String;)V
    .locals 15
    .param p1, "certificationLevel"    # I
    .param p2, "algorithm"    # I
    .param p3, "pubKey"    # Ljava/security/PublicKey;
    .param p4, "privKey"    # Ljava/security/PrivateKey;
    .param p5, "time"    # Ljava/util/Date;
    .param p6, "id"    # Ljava/lang/String;
    .param p7, "encAlgorithm"    # I
    .param p8, "passPhrase"    # [C
    .param p9, "useSHA1"    # Z
    .param p10, "hashedPcks"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;
    .param p11, "unhashedPcks"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;
    .param p12, "rand"    # Ljava/security/SecureRandom;
    .param p13, "provider"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;,
            Ljava/security/NoSuchProviderException;
        }
    .end annotation

    .prologue
    .line 323
    new-instance v6, Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyPair;

    move/from16 v0, p2

    move-object/from16 v1, p3

    move-object/from16 v2, p4

    move-object/from16 v3, p5

    invoke-direct {v6, v0, v1, v2, v3}, Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyPair;-><init>(ILjava/security/PublicKey;Ljava/security/PrivateKey;Ljava/util/Date;)V

    move-object v4, p0

    move/from16 v5, p1

    move-object/from16 v7, p6

    move/from16 v8, p7

    move-object/from16 v9, p8

    move/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    invoke-direct/range {v4 .. v14}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;-><init>(ILcom/android/sec/org/bouncycastle/openpgp/PGPKeyPair;Ljava/lang/String;I[CZLcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;Ljava/security/SecureRandom;Ljava/lang/String;)V

    .line 324
    return-void
.end method

.method public constructor <init>(ILcom/android/sec/org/bouncycastle/openpgp/PGPKeyPair;Ljava/lang/String;I[CLcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;Ljava/security/SecureRandom;Ljava/lang/String;)V
    .locals 11
    .param p1, "certificationLevel"    # I
    .param p2, "keyPair"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyPair;
    .param p3, "id"    # Ljava/lang/String;
    .param p4, "encAlgorithm"    # I
    .param p5, "passPhrase"    # [C
    .param p6, "hashedPcks"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;
    .param p7, "unhashedPcks"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;
    .param p8, "rand"    # Ljava/security/SecureRandom;
    .param p9, "provider"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;,
            Ljava/security/NoSuchProviderException;
        }
    .end annotation

    .prologue
    .line 214
    const/4 v6, 0x0

    move-object v0, p0

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move-object/from16 v5, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    invoke-direct/range {v0 .. v10}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;-><init>(ILcom/android/sec/org/bouncycastle/openpgp/PGPKeyPair;Ljava/lang/String;I[CZLcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;Ljava/security/SecureRandom;Ljava/lang/String;)V

    .line 215
    return-void
.end method

.method public constructor <init>(ILcom/android/sec/org/bouncycastle/openpgp/PGPKeyPair;Ljava/lang/String;I[CZLcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;Ljava/security/SecureRandom;Ljava/lang/String;)V
    .locals 11
    .param p1, "certificationLevel"    # I
    .param p2, "keyPair"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyPair;
    .param p3, "id"    # Ljava/lang/String;
    .param p4, "encAlgorithm"    # I
    .param p5, "passPhrase"    # [C
    .param p6, "useSHA1"    # Z
    .param p7, "hashedPcks"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;
    .param p8, "unhashedPcks"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;
    .param p9, "rand"    # Ljava/security/SecureRandom;
    .param p10, "provider"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;,
            Ljava/security/NoSuchProviderException;
        }
    .end annotation

    .prologue
    .line 230
    invoke-static/range {p10 .. p10}, Lcom/android/sec/org/bouncycastle/openpgp/PGPUtil;->getProvider(Ljava/lang/String;)Ljava/security/Provider;

    move-result-object v10

    move-object v0, p0

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move-object/from16 v5, p5

    move/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    invoke-direct/range {v0 .. v10}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;-><init>(ILcom/android/sec/org/bouncycastle/openpgp/PGPKeyPair;Ljava/lang/String;I[CZLcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;Ljava/security/SecureRandom;Ljava/security/Provider;)V

    .line 231
    return-void
.end method

.method public constructor <init>(ILcom/android/sec/org/bouncycastle/openpgp/PGPKeyPair;Ljava/lang/String;I[CZLcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;Ljava/security/SecureRandom;Ljava/security/Provider;)V
    .locals 9
    .param p1, "certificationLevel"    # I
    .param p2, "keyPair"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyPair;
    .param p3, "id"    # Ljava/lang/String;
    .param p4, "encAlgorithm"    # I
    .param p5, "passPhrase"    # [C
    .param p6, "useSHA1"    # Z
    .param p7, "hashedPcks"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;
    .param p8, "unhashedPcks"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;
    .param p9, "rand"    # Ljava/security/SecureRandom;
    .param p10, "provider"    # Ljava/security/Provider;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
        }
    .end annotation

    .prologue
    .line 246
    invoke-virtual {p2}, Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyPair;->getPrivateKey()Lcom/android/sec/org/bouncycastle/openpgp/PGPPrivateKey;

    move-result-object v6

    move v0, p1

    move-object v1, p2

    move-object v2, p3

    move-object/from16 v3, p7

    move-object/from16 v4, p8

    move-object/from16 v5, p10

    invoke-static/range {v0 .. v5}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;->certifiedPublicKey(ILcom/android/sec/org/bouncycastle/openpgp/PGPKeyPair;Ljava/lang/String;Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;Ljava/security/Provider;)Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;

    move-result-object v2

    const/4 v7, 0x1

    move-object v0, p0

    move-object v1, v6

    move v3, p4

    move-object v4, p5

    move v5, p6

    move-object/from16 v6, p9

    move-object/from16 v8, p10

    invoke-direct/range {v0 .. v8}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;-><init>(Lcom/android/sec/org/bouncycastle/openpgp/PGPPrivateKey;Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;I[CZLjava/security/SecureRandom;ZLjava/security/Provider;)V

    .line 247
    return-void
.end method

.method constructor <init>(Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;)V
    .locals 0
    .param p1, "secret"    # Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;
    .param p2, "pub"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;

    .prologue
    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    iput-object p1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;->secret:Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;

    .line 64
    iput-object p2, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;->pub:Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;

    .line 65
    return-void
.end method

.method constructor <init>(Lcom/android/sec/org/bouncycastle/openpgp/PGPPrivateKey;Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;I[CZLjava/security/SecureRandom;Ljava/security/Provider;)V
    .locals 9
    .param p1, "privKey"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPPrivateKey;
    .param p2, "pubKey"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;
    .param p3, "encAlgorithm"    # I
    .param p4, "passPhrase"    # [C
    .param p5, "useSHA1"    # Z
    .param p6, "rand"    # Ljava/security/SecureRandom;
    .param p7, "provider"    # Ljava/security/Provider;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
        }
    .end annotation

    .prologue
    .line 77
    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    move v5, p5

    move-object v6, p6

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;-><init>(Lcom/android/sec/org/bouncycastle/openpgp/PGPPrivateKey;Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;I[CZLjava/security/SecureRandom;ZLjava/security/Provider;)V

    .line 78
    return-void
.end method

.method constructor <init>(Lcom/android/sec/org/bouncycastle/openpgp/PGPPrivateKey;Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;I[CZLjava/security/SecureRandom;ZLjava/security/Provider;)V
    .locals 27
    .param p1, "privKey"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPPrivateKey;
    .param p2, "pubKey"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;
    .param p3, "encAlgorithm"    # I
    .param p4, "passPhrase"    # [C
    .param p5, "useSHA1"    # Z
    .param p6, "rand"    # Ljava/security/SecureRandom;
    .param p7, "isMasterKey"    # Z
    .param p8, "provider"    # Ljava/security/Provider;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
        }
    .end annotation

    .prologue
    .line 90
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 93
    move-object/from16 v0, p2

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;->pub:Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;

    .line 95
    invoke-virtual/range {p2 .. p2}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->getAlgorithm()I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    .line 116
    new-instance v3, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;

    const-string v4, "unknown key class"

    invoke-direct {v3, v4}, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 100
    :sswitch_0
    invoke-virtual/range {p1 .. p1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPrivateKey;->getKey()Ljava/security/PrivateKey;

    move-result-object v25

    check-cast v25, Ljava/security/interfaces/RSAPrivateCrtKey;

    .line 102
    .local v25, "rsK":Ljava/security/interfaces/RSAPrivateCrtKey;
    new-instance v26, Lcom/android/sec/org/bouncycastle/bcpg/RSASecretBCPGKey;

    invoke-interface/range {v25 .. v25}, Ljava/security/interfaces/RSAPrivateCrtKey;->getPrivateExponent()Ljava/math/BigInteger;

    move-result-object v3

    invoke-interface/range {v25 .. v25}, Ljava/security/interfaces/RSAPrivateCrtKey;->getPrimeP()Ljava/math/BigInteger;

    move-result-object v4

    invoke-interface/range {v25 .. v25}, Ljava/security/interfaces/RSAPrivateCrtKey;->getPrimeQ()Ljava/math/BigInteger;

    move-result-object v5

    move-object/from16 v0, v26

    invoke-direct {v0, v3, v4, v5}, Lcom/android/sec/org/bouncycastle/bcpg/RSASecretBCPGKey;-><init>(Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/math/BigInteger;)V

    .line 119
    .end local v25    # "rsK":Ljava/security/interfaces/RSAPrivateCrtKey;
    .local v26, "secKey":Lcom/android/sec/org/bouncycastle/bcpg/BCPGObject;
    :goto_0
    invoke-static/range {p3 .. p3}, Lcom/android/sec/org/bouncycastle/openpgp/PGPUtil;->getSymmetricCipherName(I)Ljava/lang/String;

    move-result-object v18

    .line 120
    .local v18, "cName":Ljava/lang/String;
    const/16 v17, 0x0

    .line 122
    .local v17, "c":Ljavax/crypto/Cipher;
    if-eqz v18, :cond_0

    .line 126
    :try_start_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/CFB/NoPadding"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p8

    invoke-static {v3, v0}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;Ljava/security/Provider;)Ljavax/crypto/Cipher;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v17

    .line 136
    :cond_0
    :try_start_1
    new-instance v16, Ljava/io/ByteArrayOutputStream;

    invoke-direct/range {v16 .. v16}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 137
    .local v16, "bOut":Ljava/io/ByteArrayOutputStream;
    new-instance v24, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;

    move-object/from16 v0, v24

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 139
    .local v24, "pOut":Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;
    move-object/from16 v0, v24

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->writeObject(Lcom/android/sec/org/bouncycastle/bcpg/BCPGObject;)V

    .line 141
    invoke-virtual/range {v16 .. v16}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v23

    .line 143
    .local v23, "keyData":[B
    move-object/from16 v0, v23

    array-length v3, v0

    move/from16 v0, p5

    move-object/from16 v1, v23

    invoke-static {v0, v1, v3}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;->checksum(Z[BI)[B

    move-result-object v3

    move-object/from16 v0, v24

    invoke-virtual {v0, v3}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->write([B)V

    .line 145
    if-eqz v17, :cond_3

    .line 147
    const/16 v3, 0x8

    new-array v8, v3, [B

    .line 149
    .local v8, "iv":[B
    move-object/from16 v0, p6

    invoke-virtual {v0, v8}, Ljava/security/SecureRandom;->nextBytes([B)V

    .line 151
    new-instance v7, Lcom/android/sec/org/bouncycastle/bcpg/S2K;

    const/4 v3, 0x2

    const/16 v4, 0x60

    invoke-direct {v7, v3, v8, v4}, Lcom/android/sec/org/bouncycastle/bcpg/S2K;-><init>(I[BI)V

    .line 152
    .local v7, "s2k":Lcom/android/sec/org/bouncycastle/bcpg/S2K;
    move/from16 v0, p3

    move-object/from16 v1, p4

    move-object/from16 v2, p8

    invoke-static {v0, v7, v1, v2}, Lcom/android/sec/org/bouncycastle/openpgp/PGPUtil;->makeKeyFromPassPhrase(ILcom/android/sec/org/bouncycastle/bcpg/S2K;[CLjava/security/Provider;)Ljavax/crypto/SecretKey;

    move-result-object v22

    .line 154
    .local v22, "key":Ljavax/crypto/SecretKey;
    const/4 v3, 0x1

    move-object/from16 v0, v17

    move-object/from16 v1, v22

    move-object/from16 v2, p6

    invoke-virtual {v0, v3, v1, v2}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;Ljava/security/SecureRandom;)V

    .line 156
    invoke-virtual/range {v17 .. v17}, Ljavax/crypto/Cipher;->getIV()[B

    move-result-object v8

    .line 158
    invoke-virtual/range {v16 .. v16}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v3

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Ljavax/crypto/Cipher;->doFinal([B)[B

    move-result-object v9

    .line 162
    .local v9, "encData":[B
    if-eqz p5, :cond_1

    .line 164
    const/16 v6, 0xfe

    .line 171
    .local v6, "s2kUsage":I
    :goto_1
    if-eqz p7, :cond_2

    .line 173
    new-instance v3, Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;->pub:Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;

    iget-object v4, v4, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->publicPk:Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;

    move/from16 v5, p3

    invoke-direct/range {v3 .. v9}, Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;-><init>(Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;IILcom/android/sec/org/bouncycastle/bcpg/S2K;[B[B)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;->secret:Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;
    :try_end_1
    .catch Lcom/android/sec/org/bouncycastle/openpgp/PGPException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    .line 200
    .end local v6    # "s2kUsage":I
    .end local v7    # "s2k":Lcom/android/sec/org/bouncycastle/bcpg/S2K;
    .end local v8    # "iv":[B
    .end local v9    # "encData":[B
    .end local v22    # "key":Ljavax/crypto/SecretKey;
    :goto_2
    return-void

    .line 105
    .end local v16    # "bOut":Ljava/io/ByteArrayOutputStream;
    .end local v17    # "c":Ljavax/crypto/Cipher;
    .end local v18    # "cName":Ljava/lang/String;
    .end local v23    # "keyData":[B
    .end local v24    # "pOut":Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;
    .end local v26    # "secKey":Lcom/android/sec/org/bouncycastle/bcpg/BCPGObject;
    :sswitch_1
    invoke-virtual/range {p1 .. p1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPrivateKey;->getKey()Ljava/security/PrivateKey;

    move-result-object v19

    check-cast v19, Ljava/security/interfaces/DSAPrivateKey;

    .line 107
    .local v19, "dsK":Ljava/security/interfaces/DSAPrivateKey;
    new-instance v26, Lcom/android/sec/org/bouncycastle/bcpg/DSASecretBCPGKey;

    invoke-interface/range {v19 .. v19}, Ljava/security/interfaces/DSAPrivateKey;->getX()Ljava/math/BigInteger;

    move-result-object v3

    move-object/from16 v0, v26

    invoke-direct {v0, v3}, Lcom/android/sec/org/bouncycastle/bcpg/DSASecretBCPGKey;-><init>(Ljava/math/BigInteger;)V

    .line 108
    .restart local v26    # "secKey":Lcom/android/sec/org/bouncycastle/bcpg/BCPGObject;
    goto/16 :goto_0

    .line 111
    .end local v19    # "dsK":Ljava/security/interfaces/DSAPrivateKey;
    .end local v26    # "secKey":Lcom/android/sec/org/bouncycastle/bcpg/BCPGObject;
    :sswitch_2
    invoke-virtual/range {p1 .. p1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPrivateKey;->getKey()Ljava/security/PrivateKey;

    move-result-object v21

    check-cast v21, Lcom/android/sec/org/bouncycastle/jce/interfaces/ElGamalPrivateKey;

    .line 113
    .local v21, "esK":Lcom/android/sec/org/bouncycastle/jce/interfaces/ElGamalPrivateKey;
    new-instance v26, Lcom/android/sec/org/bouncycastle/bcpg/ElGamalSecretBCPGKey;

    invoke-interface/range {v21 .. v21}, Lcom/android/sec/org/bouncycastle/jce/interfaces/ElGamalPrivateKey;->getX()Ljava/math/BigInteger;

    move-result-object v3

    move-object/from16 v0, v26

    invoke-direct {v0, v3}, Lcom/android/sec/org/bouncycastle/bcpg/ElGamalSecretBCPGKey;-><init>(Ljava/math/BigInteger;)V

    .line 114
    .restart local v26    # "secKey":Lcom/android/sec/org/bouncycastle/bcpg/BCPGObject;
    goto/16 :goto_0

    .line 128
    .end local v21    # "esK":Lcom/android/sec/org/bouncycastle/jce/interfaces/ElGamalPrivateKey;
    .restart local v17    # "c":Ljavax/crypto/Cipher;
    .restart local v18    # "cName":Ljava/lang/String;
    :catch_0
    move-exception v20

    .line 130
    .local v20, "e":Ljava/lang/Exception;
    new-instance v3, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;

    const-string v4, "Exception creating cipher"

    move-object/from16 v0, v20

    invoke-direct {v3, v4, v0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v3

    .line 168
    .end local v20    # "e":Ljava/lang/Exception;
    .restart local v7    # "s2k":Lcom/android/sec/org/bouncycastle/bcpg/S2K;
    .restart local v8    # "iv":[B
    .restart local v9    # "encData":[B
    .restart local v16    # "bOut":Ljava/io/ByteArrayOutputStream;
    .restart local v22    # "key":Ljavax/crypto/SecretKey;
    .restart local v23    # "keyData":[B
    .restart local v24    # "pOut":Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;
    :cond_1
    const/16 v6, 0xff

    .restart local v6    # "s2kUsage":I
    goto :goto_1

    .line 177
    :cond_2
    :try_start_2
    new-instance v3, Lcom/android/sec/org/bouncycastle/bcpg/SecretSubkeyPacket;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;->pub:Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;

    iget-object v4, v4, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->publicPk:Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;

    move/from16 v5, p3

    invoke-direct/range {v3 .. v9}, Lcom/android/sec/org/bouncycastle/bcpg/SecretSubkeyPacket;-><init>(Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;IILcom/android/sec/org/bouncycastle/bcpg/S2K;[B[B)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;->secret:Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;
    :try_end_2
    .catch Lcom/android/sec/org/bouncycastle/openpgp/PGPException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_2

    .line 192
    .end local v6    # "s2kUsage":I
    .end local v7    # "s2k":Lcom/android/sec/org/bouncycastle/bcpg/S2K;
    .end local v8    # "iv":[B
    .end local v9    # "encData":[B
    .end local v16    # "bOut":Ljava/io/ByteArrayOutputStream;
    .end local v22    # "key":Ljavax/crypto/SecretKey;
    .end local v23    # "keyData":[B
    .end local v24    # "pOut":Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;
    :catch_1
    move-exception v20

    .line 194
    .local v20, "e":Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
    throw v20

    .line 182
    .end local v20    # "e":Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
    .restart local v16    # "bOut":Ljava/io/ByteArrayOutputStream;
    .restart local v23    # "keyData":[B
    .restart local v24    # "pOut":Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;
    :cond_3
    if-eqz p7, :cond_4

    .line 184
    :try_start_3
    new-instance v10, Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;->pub:Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;

    iget-object v11, v3, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->publicPk:Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;

    const/4 v13, 0x0

    const/4 v14, 0x0

    invoke-virtual/range {v16 .. v16}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v15

    move/from16 v12, p3

    invoke-direct/range {v10 .. v15}, Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;-><init>(Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;ILcom/android/sec/org/bouncycastle/bcpg/S2K;[B[B)V

    move-object/from16 v0, p0

    iput-object v10, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;->secret:Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;
    :try_end_3
    .catch Lcom/android/sec/org/bouncycastle/openpgp/PGPException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_2

    .line 196
    .end local v16    # "bOut":Ljava/io/ByteArrayOutputStream;
    .end local v23    # "keyData":[B
    .end local v24    # "pOut":Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;
    :catch_2
    move-exception v20

    .line 198
    .local v20, "e":Ljava/lang/Exception;
    new-instance v3, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;

    const-string v4, "Exception encrypting key"

    move-object/from16 v0, v20

    invoke-direct {v3, v4, v0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v3

    .line 188
    .end local v20    # "e":Ljava/lang/Exception;
    .restart local v16    # "bOut":Ljava/io/ByteArrayOutputStream;
    .restart local v23    # "keyData":[B
    .restart local v24    # "pOut":Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;
    :cond_4
    :try_start_4
    new-instance v10, Lcom/android/sec/org/bouncycastle/bcpg/SecretSubkeyPacket;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;->pub:Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;

    iget-object v11, v3, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->publicPk:Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;

    const/4 v13, 0x0

    const/4 v14, 0x0

    invoke-virtual/range {v16 .. v16}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v15

    move/from16 v12, p3

    invoke-direct/range {v10 .. v15}, Lcom/android/sec/org/bouncycastle/bcpg/SecretSubkeyPacket;-><init>(Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;ILcom/android/sec/org/bouncycastle/bcpg/S2K;[B[B)V

    move-object/from16 v0, p0

    iput-object v10, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;->secret:Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;
    :try_end_4
    .catch Lcom/android/sec/org/bouncycastle/openpgp/PGPException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    goto/16 :goto_2

    .line 95
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_0
        0x3 -> :sswitch_0
        0x10 -> :sswitch_2
        0x11 -> :sswitch_1
        0x14 -> :sswitch_2
    .end sparse-switch
.end method

.method private static certifiedPublicKey(ILcom/android/sec/org/bouncycastle/openpgp/PGPKeyPair;Ljava/lang/String;Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;Ljava/security/Provider;)Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;
    .locals 6
    .param p0, "certificationLevel"    # I
    .param p1, "keyPair"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyPair;
    .param p2, "id"    # Ljava/lang/String;
    .param p3, "hashedPcks"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;
    .param p4, "unhashedPcks"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;
    .param p5, "provider"    # Ljava/security/Provider;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
        }
    .end annotation

    .prologue
    .line 262
    :try_start_0
    new-instance v2, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;

    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyPair;->getPublicKey()Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->getAlgorithm()I

    move-result v3

    const/4 v4, 0x2

    invoke-direct {v2, v3, v4, p5}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;-><init>(IILjava/security/Provider;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 272
    .local v2, "sGen":Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;
    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyPair;->getPrivateKey()Lcom/android/sec/org/bouncycastle/openpgp/PGPPrivateKey;

    move-result-object v3

    invoke-virtual {v2, p0, v3}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;->initSign(ILcom/android/sec/org/bouncycastle/openpgp/PGPPrivateKey;)V

    .line 274
    invoke-virtual {v2, p3}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;->setHashedSubpackets(Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;)V

    .line 275
    invoke-virtual {v2, p4}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;->setUnhashedSubpackets(Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;)V

    .line 279
    :try_start_1
    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyPair;->getPublicKey()Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;

    move-result-object v3

    invoke-virtual {v2, p2, v3}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;->generateCertification(Ljava/lang/String;Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;)Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;

    move-result-object v0

    .line 281
    .local v0, "certification":Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;
    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyPair;->getPublicKey()Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;

    move-result-object v3

    invoke-static {v3, p2, v0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->addCertification(Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;Ljava/lang/String;Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;)Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v3

    return-object v3

    .line 264
    .end local v0    # "certification":Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;
    .end local v2    # "sGen":Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;
    :catch_0
    move-exception v1

    .line 266
    .local v1, "e":Ljava/lang/Exception;
    new-instance v3, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "creating signature generator: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4, v1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v3

    .line 283
    .end local v1    # "e":Ljava/lang/Exception;
    .restart local v2    # "sGen":Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;
    :catch_1
    move-exception v1

    .line 285
    .restart local v1    # "e":Ljava/lang/Exception;
    new-instance v3, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "exception doing certification: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4, v1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v3
.end method

.method private static checksum(Z[BI)[B
    .locals 7
    .param p0, "useSHA1"    # Z
    .param p1, "bytes"    # [B
    .param p2, "length"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 641
    if-eqz p0, :cond_0

    .line 645
    :try_start_0
    const-string v5, "SHA1"

    invoke-static {v5}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v2

    .line 647
    .local v2, "dig":Ljava/security/MessageDigest;
    const/4 v5, 0x0

    invoke-virtual {v2, p1, v5, p2}, Ljava/security/MessageDigest;->update([BII)V

    .line 649
    invoke-virtual {v2}, Ljava/security/MessageDigest;->digest()[B
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 670
    .end local v2    # "dig":Ljava/security/MessageDigest;
    :goto_0
    return-object v0

    .line 651
    :catch_0
    move-exception v3

    .line 653
    .local v3, "e":Ljava/security/NoSuchAlgorithmException;
    new-instance v5, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;

    const-string v6, "Can\'t find SHA-1"

    invoke-direct {v5, v6, v3}, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v5

    .line 658
    .end local v3    # "e":Ljava/security/NoSuchAlgorithmException;
    :cond_0
    const/4 v1, 0x0

    .line 660
    .local v1, "checksum":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_1
    if-eq v4, p2, :cond_1

    .line 662
    aget-byte v5, p1, v4

    and-int/lit16 v5, v5, 0xff

    add-int/2addr v1, v5

    .line 660
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 665
    :cond_1
    const/4 v5, 0x2

    new-array v0, v5, [B

    .line 667
    .local v0, "check":[B
    shr-int/lit8 v5, v1, 0x8

    int-to-byte v5, v5

    aput-byte v5, v0, v6

    .line 668
    const/4 v5, 0x1

    int-to-byte v6, v1

    aput-byte v6, v0, v5

    goto :goto_0
.end method

.method public static copyWithNewPassword(Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;[C[CILjava/security/SecureRandom;Ljava/lang/String;)Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;
    .locals 6
    .param p0, "key"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;
    .param p1, "oldPassPhrase"    # [C
    .param p2, "newPassPhrase"    # [C
    .param p3, "newEncAlgorithm"    # I
    .param p4, "rand"    # Ljava/security/SecureRandom;
    .param p5, "provider"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;,
            Ljava/security/NoSuchProviderException;
        }
    .end annotation

    .prologue
    .line 769
    invoke-static {p5}, Lcom/android/sec/org/bouncycastle/openpgp/PGPUtil;->getProvider(Ljava/lang/String;)Ljava/security/Provider;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    invoke-static/range {v0 .. v5}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;->copyWithNewPassword(Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;[C[CILjava/security/SecureRandom;Ljava/security/Provider;)Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;

    move-result-object v0

    return-object v0
.end method

.method public static copyWithNewPassword(Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;[C[CILjava/security/SecureRandom;Ljava/security/Provider;)Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;
    .locals 17
    .param p0, "key"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;
    .param p1, "oldPassPhrase"    # [C
    .param p2, "newPassPhrase"    # [C
    .param p3, "newEncAlgorithm"    # I
    .param p4, "rand"    # Ljava/security/SecureRandom;
    .param p5, "provider"    # Ljava/security/Provider;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
        }
    .end annotation

    .prologue
    .line 792
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p5

    invoke-direct {v0, v1, v2}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;->extractKeyData([CLjava/security/Provider;)[B

    move-result-object v14

    .line 793
    .local v14, "rawKeyData":[B
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;->secret:Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;

    invoke-virtual {v4}, Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;->getS2KUsage()I

    move-result v6

    .line 794
    .local v6, "s2kUsage":I
    const/4 v8, 0x0

    .line 795
    .local v8, "iv":[B
    const/4 v7, 0x0

    .line 798
    .local v7, "s2k":Lcom/android/sec/org/bouncycastle/bcpg/S2K;
    if-nez p3, :cond_1

    .line 800
    const/4 v6, 0x0

    .line 801
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;->secret:Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;

    invoke-virtual {v4}, Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;->getS2KUsage()I

    move-result v4

    const/16 v5, 0xfe

    if-ne v4, v5, :cond_0

    .line 803
    array-length v4, v14

    add-int/lit8 v4, v4, -0x12

    new-array v9, v4, [B

    .line 805
    .local v9, "keyData":[B
    const/4 v4, 0x0

    const/4 v5, 0x0

    array-length v0, v9

    move/from16 v16, v0

    add-int/lit8 v16, v16, -0x2

    move/from16 v0, v16

    invoke-static {v14, v4, v9, v5, v0}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 807
    const/4 v4, 0x0

    array-length v5, v9

    add-int/lit8 v5, v5, -0x2

    invoke-static {v4, v9, v5}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;->checksum(Z[BI)[B

    move-result-object v12

    .line 809
    .local v12, "check":[B
    array-length v4, v9

    add-int/lit8 v4, v4, -0x2

    const/4 v5, 0x0

    aget-byte v5, v12, v5

    aput-byte v5, v9, v4

    .line 810
    array-length v4, v9

    add-int/lit8 v4, v4, -0x1

    const/4 v5, 0x1

    aget-byte v5, v12, v5

    aput-byte v5, v9, v4

    .line 857
    .end local v12    # "check":[B
    :goto_0
    const/4 v3, 0x0

    .line 858
    .local v3, "secret":Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;->secret:Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;

    instance-of v4, v4, Lcom/android/sec/org/bouncycastle/bcpg/SecretSubkeyPacket;

    if-eqz v4, :cond_2

    .line 860
    new-instance v3, Lcom/android/sec/org/bouncycastle/bcpg/SecretSubkeyPacket;

    .end local v3    # "secret":Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;->secret:Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;

    invoke-virtual {v4}, Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;->getPublicKeyPacket()Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;

    move-result-object v4

    move/from16 v5, p3

    invoke-direct/range {v3 .. v9}, Lcom/android/sec/org/bouncycastle/bcpg/SecretSubkeyPacket;-><init>(Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;IILcom/android/sec/org/bouncycastle/bcpg/S2K;[B[B)V

    .line 869
    .restart local v3    # "secret":Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;
    :goto_1
    new-instance v4, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;->pub:Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;

    invoke-direct {v4, v3, v5}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;-><init>(Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;)V

    return-object v4

    .line 814
    .end local v3    # "secret":Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;
    .end local v9    # "keyData":[B
    :cond_0
    move-object v9, v14

    .restart local v9    # "keyData":[B
    goto :goto_0

    .line 819
    .end local v9    # "keyData":[B
    :cond_1
    const/4 v10, 0x0

    .line 820
    .local v10, "c":Ljavax/crypto/Cipher;
    invoke-static/range {p3 .. p3}, Lcom/android/sec/org/bouncycastle/openpgp/PGPUtil;->getSymmetricCipherName(I)Ljava/lang/String;

    move-result-object v11

    .line 824
    .local v11, "cName":Ljava/lang/String;
    :try_start_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/CFB/NoPadding"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p5

    invoke-static {v4, v0}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;Ljava/security/Provider;)Ljavax/crypto/Cipher;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v10

    .line 831
    const/16 v4, 0x8

    new-array v8, v4, [B

    .line 833
    move-object/from16 v0, p4

    invoke-virtual {v0, v8}, Ljava/security/SecureRandom;->nextBytes([B)V

    .line 835
    new-instance v7, Lcom/android/sec/org/bouncycastle/bcpg/S2K;

    .end local v7    # "s2k":Lcom/android/sec/org/bouncycastle/bcpg/S2K;
    const/4 v4, 0x2

    const/16 v5, 0x60

    invoke-direct {v7, v4, v8, v5}, Lcom/android/sec/org/bouncycastle/bcpg/S2K;-><init>(I[BI)V

    .line 839
    .restart local v7    # "s2k":Lcom/android/sec/org/bouncycastle/bcpg/S2K;
    :try_start_1
    move/from16 v0, p3

    move-object/from16 v1, p2

    move-object/from16 v2, p5

    invoke-static {v0, v7, v1, v2}, Lcom/android/sec/org/bouncycastle/openpgp/PGPUtil;->makeKeyFromPassPhrase(ILcom/android/sec/org/bouncycastle/bcpg/S2K;[CLjava/security/Provider;)Ljavax/crypto/SecretKey;

    move-result-object v15

    .line 841
    .local v15, "sKey":Ljavax/crypto/SecretKey;
    const/4 v4, 0x1

    move-object/from16 v0, p4

    invoke-virtual {v10, v4, v15, v0}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;Ljava/security/SecureRandom;)V

    .line 843
    invoke-virtual {v10}, Ljavax/crypto/Cipher;->getIV()[B

    move-result-object v8

    .line 845
    invoke-virtual {v10, v14}, Ljavax/crypto/Cipher;->doFinal([B)[B
    :try_end_1
    .catch Lcom/android/sec/org/bouncycastle/openpgp/PGPException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    move-result-object v9

    .restart local v9    # "keyData":[B
    goto :goto_0

    .line 826
    .end local v9    # "keyData":[B
    .end local v15    # "sKey":Ljavax/crypto/SecretKey;
    :catch_0
    move-exception v13

    .line 828
    .local v13, "e":Ljava/lang/Exception;
    new-instance v4, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;

    const-string v5, "Exception creating cipher"

    invoke-direct {v4, v5, v13}, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v4

    .line 847
    .end local v13    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v13

    .line 849
    .local v13, "e":Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
    throw v13

    .line 851
    .end local v13    # "e":Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
    :catch_2
    move-exception v13

    .line 853
    .local v13, "e":Ljava/lang/Exception;
    new-instance v4, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;

    const-string v5, "Exception encrypting key"

    invoke-direct {v4, v5, v13}, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v4

    .line 865
    .end local v10    # "c":Ljavax/crypto/Cipher;
    .end local v11    # "cName":Ljava/lang/String;
    .end local v13    # "e":Ljava/lang/Exception;
    .restart local v3    # "secret":Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;
    .restart local v9    # "keyData":[B
    :cond_2
    new-instance v3, Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;

    .end local v3    # "secret":Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;->secret:Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;

    invoke-virtual {v4}, Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;->getPublicKeyPacket()Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;

    move-result-object v4

    move/from16 v5, p3

    invoke-direct/range {v3 .. v9}, Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;-><init>(Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;IILcom/android/sec/org/bouncycastle/bcpg/S2K;[B[B)V

    .restart local v3    # "secret":Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;
    goto :goto_1
.end method

.method private extractKeyData([CLjava/security/Provider;)[B
    .locals 22
    .param p1, "passPhrase"    # [C
    .param p2, "provider"    # Ljava/security/Provider;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
        }
    .end annotation

    .prologue
    .line 418
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;->secret:Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;

    invoke-virtual {v4}, Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;->getEncAlgorithm()I

    move-result v4

    invoke-static {v4}, Lcom/android/sec/org/bouncycastle/openpgp/PGPUtil;->getSymmetricCipherName(I)Ljava/lang/String;

    move-result-object v8

    .line 419
    .local v8, "cName":Ljava/lang/String;
    const/4 v2, 0x0

    .line 421
    .local v2, "c":Ljavax/crypto/Cipher;
    if-eqz v8, :cond_0

    .line 425
    :try_start_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v7, "/CFB/NoPadding"

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p2

    invoke-static {v4, v0}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;Ljava/security/Provider;)Ljavax/crypto/Cipher;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v2

    .line 433
    :cond_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;->secret:Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;

    invoke-virtual {v4}, Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;->getSecretKeyData()[B

    move-result-object v3

    .line 434
    .local v3, "encData":[B
    const/4 v6, 0x0

    .line 438
    .local v6, "data":[B
    if-eqz v2, :cond_8

    .line 442
    :try_start_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;->secret:Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;

    invoke-virtual {v4}, Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;->getPublicKeyPacket()Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;->getVersion()I

    move-result v4

    const/4 v7, 0x4

    if-ne v4, v7, :cond_4

    .line 444
    new-instance v15, Ljavax/crypto/spec/IvParameterSpec;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;->secret:Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;

    invoke-virtual {v4}, Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;->getIV()[B

    move-result-object v4

    invoke-direct {v15, v4}, Ljavax/crypto/spec/IvParameterSpec;-><init>([B)V

    .line 446
    .local v15, "ivSpec":Ljavax/crypto/spec/IvParameterSpec;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;->secret:Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;

    invoke-virtual {v4}, Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;->getEncAlgorithm()I

    move-result v4

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;->secret:Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;

    invoke-virtual {v7}, Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;->getS2K()Lcom/android/sec/org/bouncycastle/bcpg/S2K;

    move-result-object v7

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-static {v4, v7, v0, v1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPUtil;->makeKeyFromPassPhrase(ILcom/android/sec/org/bouncycastle/bcpg/S2K;[CLjava/security/Provider;)Ljavax/crypto/SecretKey;

    move-result-object v17

    .line 448
    .local v17, "key":Ljavax/crypto/SecretKey;
    const/4 v4, 0x2

    move-object/from16 v0, v17

    invoke-virtual {v2, v4, v0, v15}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;)V

    .line 450
    const/4 v4, 0x0

    array-length v7, v3

    invoke-virtual {v2, v3, v4, v7}, Ljavax/crypto/Cipher;->doFinal([BII)[B

    move-result-object v6

    .line 452
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;->secret:Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;

    invoke-virtual {v4}, Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;->getS2KUsage()I

    move-result v4

    const/16 v7, 0xfe

    if-ne v4, v7, :cond_1

    const/16 v19, 0x1

    .line 453
    .local v19, "useSHA1":Z
    :goto_0
    if-eqz v19, :cond_2

    array-length v4, v6

    add-int/lit8 v4, v4, -0x14

    :goto_1
    move/from16 v0, v19

    invoke-static {v0, v6, v4}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;->checksum(Z[BI)[B

    move-result-object v10

    .line 455
    .local v10, "check":[B
    const/4 v13, 0x0

    .local v13, "i":I
    :goto_2
    array-length v4, v10

    if-eq v13, v4, :cond_9

    .line 457
    aget-byte v4, v10, v13

    array-length v7, v6

    array-length v0, v10

    move/from16 v20, v0

    sub-int v7, v7, v20

    add-int/2addr v7, v13

    aget-byte v7, v6, v7

    if-eq v4, v7, :cond_3

    .line 459
    new-instance v4, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "checksum mismatch at "

    move-object/from16 v0, v20

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v20, " of "

    move-object/from16 v0, v20

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    array-length v0, v10

    move/from16 v20, v0

    move/from16 v0, v20

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v4, v7}, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_1
    .catch Lcom/android/sec/org/bouncycastle/openpgp/PGPException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3

    .line 516
    .end local v10    # "check":[B
    .end local v13    # "i":I
    .end local v15    # "ivSpec":Ljavax/crypto/spec/IvParameterSpec;
    .end local v17    # "key":Ljavax/crypto/SecretKey;
    .end local v19    # "useSHA1":Z
    :catch_0
    move-exception v12

    .line 518
    .local v12, "e":Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
    :try_start_2
    throw v12
    :try_end_2
    .catch Lcom/android/sec/org/bouncycastle/openpgp/PGPException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_4

    .line 532
    .end local v12    # "e":Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
    :catch_1
    move-exception v12

    .line 534
    .restart local v12    # "e":Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
    throw v12

    .line 427
    .end local v3    # "encData":[B
    .end local v6    # "data":[B
    .end local v12    # "e":Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
    :catch_2
    move-exception v12

    .line 429
    .local v12, "e":Ljava/lang/Exception;
    new-instance v4, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;

    const-string v7, "Exception creating cipher"

    invoke-direct {v4, v7, v12}, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v4

    .line 452
    .end local v12    # "e":Ljava/lang/Exception;
    .restart local v3    # "encData":[B
    .restart local v6    # "data":[B
    .restart local v15    # "ivSpec":Ljavax/crypto/spec/IvParameterSpec;
    .restart local v17    # "key":Ljavax/crypto/SecretKey;
    :cond_1
    const/16 v19, 0x0

    goto :goto_0

    .line 453
    .restart local v19    # "useSHA1":Z
    :cond_2
    :try_start_3
    array-length v4, v6

    add-int/lit8 v4, v4, -0x2

    goto :goto_1

    .line 455
    .restart local v10    # "check":[B
    .restart local v13    # "i":I
    :cond_3
    add-int/lit8 v13, v13, 0x1

    goto :goto_2

    .line 465
    .end local v10    # "check":[B
    .end local v13    # "i":I
    .end local v15    # "ivSpec":Ljavax/crypto/spec/IvParameterSpec;
    .end local v17    # "key":Ljavax/crypto/SecretKey;
    .end local v19    # "useSHA1":Z
    :cond_4
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;->secret:Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;

    invoke-virtual {v4}, Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;->getEncAlgorithm()I

    move-result v4

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;->secret:Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;

    invoke-virtual {v7}, Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;->getS2K()Lcom/android/sec/org/bouncycastle/bcpg/S2K;

    move-result-object v7

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-static {v4, v7, v0, v1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPUtil;->makeKeyFromPassPhrase(ILcom/android/sec/org/bouncycastle/bcpg/S2K;[CLjava/security/Provider;)Ljavax/crypto/SecretKey;

    move-result-object v17

    .line 467
    .restart local v17    # "key":Ljavax/crypto/SecretKey;
    array-length v4, v3

    new-array v6, v4, [B

    .line 469
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;->secret:Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;

    invoke-virtual {v4}, Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;->getIV()[B

    move-result-object v4

    array-length v4, v4

    new-array v14, v4, [B

    .line 471
    .local v14, "iv":[B
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;->secret:Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;

    invoke-virtual {v4}, Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;->getIV()[B

    move-result-object v4

    const/4 v7, 0x0

    const/16 v20, 0x0

    array-length v0, v14

    move/from16 v21, v0

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-static {v4, v7, v14, v0, v1}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 476
    const/16 v18, 0x0

    .line 478
    .local v18, "pos":I
    const/4 v13, 0x0

    .restart local v13    # "i":I
    :goto_3
    const/4 v4, 0x4

    if-eq v13, v4, :cond_6

    .line 480
    const/4 v4, 0x2

    new-instance v7, Ljavax/crypto/spec/IvParameterSpec;

    invoke-direct {v7, v14}, Ljavax/crypto/spec/IvParameterSpec;-><init>([B)V

    move-object/from16 v0, v17

    invoke-virtual {v2, v4, v0, v7}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;)V

    .line 482
    aget-byte v4, v3, v18

    shl-int/lit8 v4, v4, 0x8

    add-int/lit8 v7, v18, 0x1

    aget-byte v7, v3, v7

    and-int/lit16 v7, v7, 0xff

    or-int/2addr v4, v7

    add-int/lit8 v4, v4, 0x7

    div-int/lit8 v5, v4, 0x8

    .line 484
    .local v5, "encLen":I
    aget-byte v4, v3, v18

    aput-byte v4, v6, v18

    .line 485
    add-int/lit8 v4, v18, 0x1

    add-int/lit8 v7, v18, 0x1

    aget-byte v7, v3, v7

    aput-byte v7, v6, v4

    .line 487
    add-int/lit8 v4, v18, 0x2

    add-int/lit8 v7, v18, 0x2

    invoke-virtual/range {v2 .. v7}, Ljavax/crypto/Cipher;->doFinal([BII[BI)I

    .line 488
    add-int/lit8 v4, v5, 0x2

    add-int v18, v18, v4

    .line 490
    const/4 v4, 0x3

    if-eq v13, v4, :cond_5

    .line 492
    array-length v4, v14

    sub-int v4, v18, v4

    const/4 v7, 0x0

    array-length v0, v14

    move/from16 v20, v0

    move/from16 v0, v20

    invoke-static {v3, v4, v14, v7, v0}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 478
    :cond_5
    add-int/lit8 v13, v13, 0x1

    goto :goto_3

    .line 500
    .end local v5    # "encLen":I
    :cond_6
    aget-byte v4, v3, v18

    shl-int/lit8 v4, v4, 0x8

    const v7, 0xff00

    and-int/2addr v4, v7

    add-int/lit8 v7, v18, 0x1

    aget-byte v7, v3, v7

    and-int/lit16 v7, v7, 0xff

    or-int v11, v4, v7

    .line 501
    .local v11, "cs":I
    const/4 v9, 0x0

    .line 502
    .local v9, "calcCs":I
    const/16 v16, 0x0

    .local v16, "j":I
    :goto_4
    array-length v4, v6

    add-int/lit8 v4, v4, -0x2

    move/from16 v0, v16

    if-ge v0, v4, :cond_7

    .line 504
    aget-byte v4, v6, v16

    and-int/lit16 v4, v4, 0xff

    add-int/2addr v9, v4

    .line 502
    add-int/lit8 v16, v16, 0x1

    goto :goto_4

    .line 507
    :cond_7
    const v4, 0xffff

    and-int/2addr v9, v4

    .line 508
    if-eq v9, v11, :cond_9

    .line 510
    new-instance v4, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "checksum mismatch: passphrase wrong, expected "

    move-object/from16 v0, v20

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {v11}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v20, " found "

    move-object/from16 v0, v20

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {v9}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v4, v7}, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_3
    .catch Lcom/android/sec/org/bouncycastle/openpgp/PGPException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3

    .line 520
    .end local v9    # "calcCs":I
    .end local v11    # "cs":I
    .end local v13    # "i":I
    .end local v14    # "iv":[B
    .end local v16    # "j":I
    .end local v17    # "key":Ljavax/crypto/SecretKey;
    .end local v18    # "pos":I
    :catch_3
    move-exception v12

    .line 522
    .restart local v12    # "e":Ljava/lang/Exception;
    :try_start_4
    new-instance v4, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;

    const-string v7, "Exception decrypting key"

    invoke-direct {v4, v7, v12}, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v4
    :try_end_4
    .catch Lcom/android/sec/org/bouncycastle/openpgp/PGPException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_4

    .line 536
    .end local v12    # "e":Ljava/lang/Exception;
    :catch_4
    move-exception v12

    .line 538
    .restart local v12    # "e":Ljava/lang/Exception;
    new-instance v4, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;

    const-string v7, "Exception constructing key"

    invoke-direct {v4, v7, v12}, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v4

    .line 527
    .end local v12    # "e":Ljava/lang/Exception;
    :cond_8
    move-object v6, v3

    .line 530
    :cond_9
    return-object v6
.end method

.method public static replacePublicKey(Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;)Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;
    .locals 4
    .param p0, "secretKey"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;
    .param p1, "publicKey"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;

    .prologue
    .line 882
    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->getKeyID()J

    move-result-wide v0

    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;->getKeyID()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 884
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "keyIDs do not match"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 887
    :cond_0
    new-instance v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;

    iget-object v1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;->secret:Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;

    invoke-direct {v0, v1, p1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;-><init>(Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;)V

    return-object v0
.end method


# virtual methods
.method public encode(Ljava/io/OutputStream;)V
    .locals 8
    .param p1, "outStream"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 690
    instance-of v6, p1, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;

    if-eqz v6, :cond_1

    move-object v3, p1

    .line 692
    check-cast v3, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;

    .line 699
    .local v3, "out":Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;
    :goto_0
    iget-object v6, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;->secret:Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;

    invoke-virtual {v3, v6}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->writePacket(Lcom/android/sec/org/bouncycastle/bcpg/ContainedPacket;)V

    .line 700
    iget-object v6, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;->pub:Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;

    iget-object v6, v6, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->trustPk:Lcom/android/sec/org/bouncycastle/bcpg/TrustPacket;

    if-eqz v6, :cond_0

    .line 702
    iget-object v6, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;->pub:Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;

    iget-object v6, v6, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->trustPk:Lcom/android/sec/org/bouncycastle/bcpg/TrustPacket;

    invoke-virtual {v3, v6}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->writePacket(Lcom/android/sec/org/bouncycastle/bcpg/ContainedPacket;)V

    .line 705
    :cond_0
    iget-object v6, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;->pub:Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;

    iget-object v6, v6, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->subSigs:Ljava/util/List;

    if-nez v6, :cond_6

    .line 707
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v6, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;->pub:Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;

    iget-object v6, v6, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->keySigs:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    if-eq v0, v6, :cond_2

    .line 709
    iget-object v6, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;->pub:Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;

    iget-object v6, v6, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->keySigs:Ljava/util/List;

    invoke-interface {v6, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;

    invoke-virtual {v6, v3}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;->encode(Ljava/io/OutputStream;)V

    .line 707
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 696
    .end local v0    # "i":I
    .end local v3    # "out":Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;
    :cond_1
    new-instance v3, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;

    invoke-direct {v3, p1}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;-><init>(Ljava/io/OutputStream;)V

    .restart local v3    # "out":Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;
    goto :goto_0

    .line 712
    .restart local v0    # "i":I
    :cond_2
    const/4 v0, 0x0

    :goto_2
    iget-object v6, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;->pub:Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;

    iget-object v6, v6, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->ids:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    if-eq v0, v6, :cond_7

    .line 714
    iget-object v6, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;->pub:Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;

    iget-object v6, v6, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->ids:Ljava/util/List;

    invoke-interface {v6, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    instance-of v6, v6, Ljava/lang/String;

    if-eqz v6, :cond_4

    .line 716
    iget-object v6, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;->pub:Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;

    iget-object v6, v6, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->ids:Ljava/util/List;

    invoke-interface {v6, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 718
    .local v1, "id":Ljava/lang/String;
    new-instance v6, Lcom/android/sec/org/bouncycastle/bcpg/UserIDPacket;

    invoke-direct {v6, v1}, Lcom/android/sec/org/bouncycastle/bcpg/UserIDPacket;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v6}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->writePacket(Lcom/android/sec/org/bouncycastle/bcpg/ContainedPacket;)V

    .line 727
    .end local v1    # "id":Ljava/lang/String;
    :goto_3
    iget-object v6, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;->pub:Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;

    iget-object v6, v6, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->idTrusts:Ljava/util/List;

    invoke-interface {v6, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    if-eqz v6, :cond_3

    .line 729
    iget-object v6, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;->pub:Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;

    iget-object v6, v6, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->idTrusts:Ljava/util/List;

    invoke-interface {v6, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/android/sec/org/bouncycastle/bcpg/ContainedPacket;

    invoke-virtual {v3, v6}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->writePacket(Lcom/android/sec/org/bouncycastle/bcpg/ContainedPacket;)V

    .line 732
    :cond_3
    iget-object v6, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;->pub:Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;

    iget-object v6, v6, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->idSigs:Ljava/util/List;

    invoke-interface {v6, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/ArrayList;

    .line 734
    .local v4, "sigs":Ljava/util/List;
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_4
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v6

    if-eq v2, v6, :cond_5

    .line 736
    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;

    invoke-virtual {v6, v3}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;->encode(Ljava/io/OutputStream;)V

    .line 734
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 722
    .end local v2    # "j":I
    .end local v4    # "sigs":Ljava/util/List;
    :cond_4
    iget-object v6, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;->pub:Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;

    iget-object v6, v6, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->ids:Ljava/util/List;

    invoke-interface {v6, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/sec/org/bouncycastle/openpgp/PGPUserAttributeSubpacketVector;

    .line 724
    .local v5, "v":Lcom/android/sec/org/bouncycastle/openpgp/PGPUserAttributeSubpacketVector;
    new-instance v6, Lcom/android/sec/org/bouncycastle/bcpg/UserAttributePacket;

    invoke-virtual {v5}, Lcom/android/sec/org/bouncycastle/openpgp/PGPUserAttributeSubpacketVector;->toSubpacketArray()[Lcom/android/sec/org/bouncycastle/bcpg/UserAttributeSubpacket;

    move-result-object v7

    invoke-direct {v6, v7}, Lcom/android/sec/org/bouncycastle/bcpg/UserAttributePacket;-><init>([Lcom/android/sec/org/bouncycastle/bcpg/UserAttributeSubpacket;)V

    invoke-virtual {v3, v6}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;->writePacket(Lcom/android/sec/org/bouncycastle/bcpg/ContainedPacket;)V

    goto :goto_3

    .line 712
    .end local v5    # "v":Lcom/android/sec/org/bouncycastle/openpgp/PGPUserAttributeSubpacketVector;
    .restart local v2    # "j":I
    .restart local v4    # "sigs":Ljava/util/List;
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 742
    .end local v0    # "i":I
    .end local v2    # "j":I
    .end local v4    # "sigs":Ljava/util/List;
    :cond_6
    const/4 v2, 0x0

    .restart local v2    # "j":I
    :goto_5
    iget-object v6, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;->pub:Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;

    iget-object v6, v6, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->subSigs:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    if-eq v2, v6, :cond_7

    .line 744
    iget-object v6, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;->pub:Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;

    iget-object v6, v6, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->subSigs:Ljava/util/List;

    invoke-interface {v6, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;

    invoke-virtual {v6, v3}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;->encode(Ljava/io/OutputStream;)V

    .line 742
    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    .line 747
    .end local v2    # "j":I
    :cond_7
    return-void
.end method

.method public extractPrivateKey([CLjava/lang/String;)Lcom/android/sec/org/bouncycastle/openpgp/PGPPrivateKey;
    .locals 1
    .param p1, "passPhrase"    # [C
    .param p2, "provider"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;,
            Ljava/security/NoSuchProviderException;
        }
    .end annotation

    .prologue
    .line 556
    invoke-static {p2}, Lcom/android/sec/org/bouncycastle/openpgp/PGPUtil;->getProvider(Ljava/lang/String;)Ljava/security/Provider;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;->extractPrivateKey([CLjava/security/Provider;)Lcom/android/sec/org/bouncycastle/openpgp/PGPPrivateKey;

    move-result-object v0

    return-object v0
.end method

.method public extractPrivateKey([CLjava/security/Provider;)Lcom/android/sec/org/bouncycastle/openpgp/PGPPrivateKey;
    .locals 25
    .param p1, "passPhrase"    # [C
    .param p2, "provider"    # Ljava/security/Provider;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
        }
    .end annotation

    .prologue
    .line 572
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;->secret:Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;

    invoke-virtual {v3}, Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;->getSecretKeyData()[B

    move-result-object v24

    .line 573
    .local v24, "secKeyData":[B
    if-eqz v24, :cond_0

    move-object/from16 v0, v24

    array-length v3, v0

    const/4 v4, 0x1

    if-ge v3, v4, :cond_1

    .line 575
    :cond_0
    const/4 v3, 0x0

    .line 623
    :goto_0
    return-object v3

    .line 578
    :cond_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;->secret:Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;

    invoke-virtual {v3}, Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;->getPublicKeyPacket()Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;

    move-result-object v21

    .line 583
    .local v21, "pubPk":Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;
    :try_start_0
    invoke-direct/range {p0 .. p2}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;->extractKeyData([CLjava/security/Provider;)[B

    move-result-object v11

    .line 584
    .local v11, "data":[B
    new-instance v20, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;

    new-instance v3, Ljava/io/ByteArrayInputStream;

    invoke-direct {v3, v11}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    move-object/from16 v0, v20

    invoke-direct {v0, v3}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;-><init>(Ljava/io/InputStream;)V

    .line 586
    .local v20, "in":Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;
    invoke-virtual/range {v21 .. v21}, Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;->getAlgorithm()I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    .line 625
    new-instance v3, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;

    const-string v4, "unknown public key algorithm encountered"

    invoke-direct {v3, v4}, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_0
    .catch Lcom/android/sec/org/bouncycastle/openpgp/PGPException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 628
    .end local v11    # "data":[B
    .end local v20    # "in":Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;
    :catch_0
    move-exception v15

    .line 630
    .local v15, "e":Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
    throw v15

    .line 591
    .end local v15    # "e":Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
    .restart local v11    # "data":[B
    .restart local v20    # "in":Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;
    :sswitch_0
    :try_start_1
    invoke-virtual/range {v21 .. v21}, Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;->getKey()Lcom/android/sec/org/bouncycastle/bcpg/BCPGKey;

    move-result-object v23

    check-cast v23, Lcom/android/sec/org/bouncycastle/bcpg/RSAPublicBCPGKey;

    .line 592
    .local v23, "rsaPub":Lcom/android/sec/org/bouncycastle/bcpg/RSAPublicBCPGKey;
    new-instance v22, Lcom/android/sec/org/bouncycastle/bcpg/RSASecretBCPGKey;

    move-object/from16 v0, v22

    move-object/from16 v1, v20

    invoke-direct {v0, v1}, Lcom/android/sec/org/bouncycastle/bcpg/RSASecretBCPGKey;-><init>(Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;)V

    .line 593
    .local v22, "rsaPriv":Lcom/android/sec/org/bouncycastle/bcpg/RSASecretBCPGKey;
    new-instance v2, Ljava/security/spec/RSAPrivateCrtKeySpec;

    invoke-virtual/range {v22 .. v22}, Lcom/android/sec/org/bouncycastle/bcpg/RSASecretBCPGKey;->getModulus()Ljava/math/BigInteger;

    move-result-object v3

    invoke-virtual/range {v23 .. v23}, Lcom/android/sec/org/bouncycastle/bcpg/RSAPublicBCPGKey;->getPublicExponent()Ljava/math/BigInteger;

    move-result-object v4

    invoke-virtual/range {v22 .. v22}, Lcom/android/sec/org/bouncycastle/bcpg/RSASecretBCPGKey;->getPrivateExponent()Ljava/math/BigInteger;

    move-result-object v5

    invoke-virtual/range {v22 .. v22}, Lcom/android/sec/org/bouncycastle/bcpg/RSASecretBCPGKey;->getPrimeP()Ljava/math/BigInteger;

    move-result-object v6

    invoke-virtual/range {v22 .. v22}, Lcom/android/sec/org/bouncycastle/bcpg/RSASecretBCPGKey;->getPrimeQ()Ljava/math/BigInteger;

    move-result-object v7

    invoke-virtual/range {v22 .. v22}, Lcom/android/sec/org/bouncycastle/bcpg/RSASecretBCPGKey;->getPrimeExponentP()Ljava/math/BigInteger;

    move-result-object v8

    invoke-virtual/range {v22 .. v22}, Lcom/android/sec/org/bouncycastle/bcpg/RSASecretBCPGKey;->getPrimeExponentQ()Ljava/math/BigInteger;

    move-result-object v9

    invoke-virtual/range {v22 .. v22}, Lcom/android/sec/org/bouncycastle/bcpg/RSASecretBCPGKey;->getCrtCoefficient()Ljava/math/BigInteger;

    move-result-object v10

    invoke-direct/range {v2 .. v10}, Ljava/security/spec/RSAPrivateCrtKeySpec;-><init>(Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/math/BigInteger;)V

    .line 603
    .local v2, "rsaPrivSpec":Ljava/security/spec/RSAPrivateCrtKeySpec;
    const-string v3, "RSA"

    move-object/from16 v0, p2

    invoke-static {v3, v0}, Ljava/security/KeyFactory;->getInstance(Ljava/lang/String;Ljava/security/Provider;)Ljava/security/KeyFactory;

    move-result-object v19

    .line 605
    .local v19, "fact":Ljava/security/KeyFactory;
    new-instance v3, Lcom/android/sec/org/bouncycastle/openpgp/PGPPrivateKey;

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Ljava/security/KeyFactory;->generatePrivate(Ljava/security/spec/KeySpec;)Ljava/security/PrivateKey;

    move-result-object v4

    invoke-virtual/range {p0 .. p0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;->getKeyID()J

    move-result-wide v6

    invoke-direct {v3, v4, v6, v7}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPrivateKey;-><init>(Ljava/security/PrivateKey;J)V
    :try_end_1
    .catch Lcom/android/sec/org/bouncycastle/openpgp/PGPException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 632
    .end local v2    # "rsaPrivSpec":Ljava/security/spec/RSAPrivateCrtKeySpec;
    .end local v11    # "data":[B
    .end local v19    # "fact":Ljava/security/KeyFactory;
    .end local v20    # "in":Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;
    .end local v22    # "rsaPriv":Lcom/android/sec/org/bouncycastle/bcpg/RSASecretBCPGKey;
    .end local v23    # "rsaPub":Lcom/android/sec/org/bouncycastle/bcpg/RSAPublicBCPGKey;
    :catch_1
    move-exception v15

    .line 634
    .local v15, "e":Ljava/lang/Exception;
    new-instance v3, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;

    const-string v4, "Exception constructing key"

    invoke-direct {v3, v4, v15}, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v3

    .line 607
    .end local v15    # "e":Ljava/lang/Exception;
    .restart local v11    # "data":[B
    .restart local v20    # "in":Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;
    :sswitch_1
    :try_start_2
    invoke-virtual/range {v21 .. v21}, Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;->getKey()Lcom/android/sec/org/bouncycastle/bcpg/BCPGKey;

    move-result-object v14

    check-cast v14, Lcom/android/sec/org/bouncycastle/bcpg/DSAPublicBCPGKey;

    .line 608
    .local v14, "dsaPub":Lcom/android/sec/org/bouncycastle/bcpg/DSAPublicBCPGKey;
    new-instance v12, Lcom/android/sec/org/bouncycastle/bcpg/DSASecretBCPGKey;

    move-object/from16 v0, v20

    invoke-direct {v12, v0}, Lcom/android/sec/org/bouncycastle/bcpg/DSASecretBCPGKey;-><init>(Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;)V

    .line 609
    .local v12, "dsaPriv":Lcom/android/sec/org/bouncycastle/bcpg/DSASecretBCPGKey;
    new-instance v13, Ljava/security/spec/DSAPrivateKeySpec;

    invoke-virtual {v12}, Lcom/android/sec/org/bouncycastle/bcpg/DSASecretBCPGKey;->getX()Ljava/math/BigInteger;

    move-result-object v3

    invoke-virtual {v14}, Lcom/android/sec/org/bouncycastle/bcpg/DSAPublicBCPGKey;->getP()Ljava/math/BigInteger;

    move-result-object v4

    invoke-virtual {v14}, Lcom/android/sec/org/bouncycastle/bcpg/DSAPublicBCPGKey;->getQ()Ljava/math/BigInteger;

    move-result-object v5

    invoke-virtual {v14}, Lcom/android/sec/org/bouncycastle/bcpg/DSAPublicBCPGKey;->getG()Ljava/math/BigInteger;

    move-result-object v6

    invoke-direct {v13, v3, v4, v5, v6}, Ljava/security/spec/DSAPrivateKeySpec;-><init>(Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/math/BigInteger;)V

    .line 612
    .local v13, "dsaPrivSpec":Ljava/security/spec/DSAPrivateKeySpec;
    const-string v3, "DSA"

    move-object/from16 v0, p2

    invoke-static {v3, v0}, Ljava/security/KeyFactory;->getInstance(Ljava/lang/String;Ljava/security/Provider;)Ljava/security/KeyFactory;

    move-result-object v19

    .line 614
    .restart local v19    # "fact":Ljava/security/KeyFactory;
    new-instance v3, Lcom/android/sec/org/bouncycastle/openpgp/PGPPrivateKey;

    move-object/from16 v0, v19

    invoke-virtual {v0, v13}, Ljava/security/KeyFactory;->generatePrivate(Ljava/security/spec/KeySpec;)Ljava/security/PrivateKey;

    move-result-object v4

    invoke-virtual/range {p0 .. p0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;->getKeyID()J

    move-result-wide v6

    invoke-direct {v3, v4, v6, v7}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPrivateKey;-><init>(Ljava/security/PrivateKey;J)V

    goto/16 :goto_0

    .line 617
    .end local v12    # "dsaPriv":Lcom/android/sec/org/bouncycastle/bcpg/DSASecretBCPGKey;
    .end local v13    # "dsaPrivSpec":Ljava/security/spec/DSAPrivateKeySpec;
    .end local v14    # "dsaPub":Lcom/android/sec/org/bouncycastle/bcpg/DSAPublicBCPGKey;
    .end local v19    # "fact":Ljava/security/KeyFactory;
    :sswitch_2
    invoke-virtual/range {v21 .. v21}, Lcom/android/sec/org/bouncycastle/bcpg/PublicKeyPacket;->getKey()Lcom/android/sec/org/bouncycastle/bcpg/BCPGKey;

    move-result-object v17

    check-cast v17, Lcom/android/sec/org/bouncycastle/bcpg/ElGamalPublicBCPGKey;

    .line 618
    .local v17, "elPub":Lcom/android/sec/org/bouncycastle/bcpg/ElGamalPublicBCPGKey;
    new-instance v16, Lcom/android/sec/org/bouncycastle/bcpg/ElGamalSecretBCPGKey;

    move-object/from16 v0, v16

    move-object/from16 v1, v20

    invoke-direct {v0, v1}, Lcom/android/sec/org/bouncycastle/bcpg/ElGamalSecretBCPGKey;-><init>(Lcom/android/sec/org/bouncycastle/bcpg/BCPGInputStream;)V

    .line 619
    .local v16, "elPriv":Lcom/android/sec/org/bouncycastle/bcpg/ElGamalSecretBCPGKey;
    new-instance v18, Lcom/android/sec/org/bouncycastle/jce/spec/ElGamalPrivateKeySpec;

    invoke-virtual/range {v16 .. v16}, Lcom/android/sec/org/bouncycastle/bcpg/ElGamalSecretBCPGKey;->getX()Ljava/math/BigInteger;

    move-result-object v3

    new-instance v4, Lcom/android/sec/org/bouncycastle/jce/spec/ElGamalParameterSpec;

    invoke-virtual/range {v17 .. v17}, Lcom/android/sec/org/bouncycastle/bcpg/ElGamalPublicBCPGKey;->getP()Ljava/math/BigInteger;

    move-result-object v5

    invoke-virtual/range {v17 .. v17}, Lcom/android/sec/org/bouncycastle/bcpg/ElGamalPublicBCPGKey;->getG()Ljava/math/BigInteger;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/android/sec/org/bouncycastle/jce/spec/ElGamalParameterSpec;-><init>(Ljava/math/BigInteger;Ljava/math/BigInteger;)V

    move-object/from16 v0, v18

    invoke-direct {v0, v3, v4}, Lcom/android/sec/org/bouncycastle/jce/spec/ElGamalPrivateKeySpec;-><init>(Ljava/math/BigInteger;Lcom/android/sec/org/bouncycastle/jce/spec/ElGamalParameterSpec;)V

    .line 621
    .local v18, "elSpec":Lcom/android/sec/org/bouncycastle/jce/spec/ElGamalPrivateKeySpec;
    const-string v3, "ElGamal"

    move-object/from16 v0, p2

    invoke-static {v3, v0}, Ljava/security/KeyFactory;->getInstance(Ljava/lang/String;Ljava/security/Provider;)Ljava/security/KeyFactory;

    move-result-object v19

    .line 623
    .restart local v19    # "fact":Ljava/security/KeyFactory;
    new-instance v3, Lcom/android/sec/org/bouncycastle/openpgp/PGPPrivateKey;

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/security/KeyFactory;->generatePrivate(Ljava/security/spec/KeySpec;)Ljava/security/PrivateKey;

    move-result-object v4

    invoke-virtual/range {p0 .. p0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;->getKeyID()J

    move-result-wide v6

    invoke-direct {v3, v4, v6, v7}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPrivateKey;-><init>(Ljava/security/PrivateKey;J)V
    :try_end_2
    .catch Lcom/android/sec/org/bouncycastle/openpgp/PGPException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto/16 :goto_0

    .line 586
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_0
        0x3 -> :sswitch_0
        0x10 -> :sswitch_2
        0x11 -> :sswitch_1
        0x14 -> :sswitch_2
    .end sparse-switch
.end method

.method public getEncoded()[B
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 677
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 679
    .local v0, "bOut":Ljava/io/ByteArrayOutputStream;
    invoke-virtual {p0, v0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;->encode(Ljava/io/OutputStream;)V

    .line 681
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    return-object v1
.end method

.method public getKeyEncryptionAlgorithm()I
    .locals 1

    .prologue
    .line 370
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;->secret:Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/bcpg/SecretKeyPacket;->getEncAlgorithm()I

    move-result v0

    return v0
.end method

.method public getKeyID()J
    .locals 2

    .prologue
    .line 380
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;->pub:Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->getKeyID()J

    move-result-wide v0

    return-wide v0
.end method

.method public getPublicKey()Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;
    .locals 1

    .prologue
    .line 390
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;->pub:Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;

    return-object v0
.end method

.method public getUserAttributes()Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 410
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;->pub:Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->getUserAttributes()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public getUserIDs()Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 400
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;->pub:Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->getUserIDs()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public isMasterKey()Z
    .locals 1

    .prologue
    .line 360
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;->pub:Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->isMasterKey()Z

    move-result v0

    return v0
.end method

.method public isSigningKey()Z
    .locals 8

    .prologue
    const/16 v7, 0x13

    const/16 v6, 0x11

    const/4 v2, 0x0

    const/4 v5, 0x3

    const/4 v3, 0x1

    .line 336
    iget-object v4, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;->pub:Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;

    invoke-virtual {v4}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->getAlgorithm()I

    move-result v0

    .line 338
    .local v0, "algorithm":I
    iget-object v4, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;->pub:Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;

    invoke-virtual {v4}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->getVersion()I

    move-result v4

    if-le v4, v5, :cond_2

    .line 340
    iget-object v4, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;->pub:Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;

    invoke-virtual {v4}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->getKeyFlags()I

    move-result v1

    .line 342
    .local v1, "keyFlags":I
    if-eq v0, v3, :cond_0

    if-eq v0, v5, :cond_0

    if-eq v0, v6, :cond_0

    if-eq v0, v7, :cond_0

    const/16 v4, 0x14

    if-ne v0, v4, :cond_1

    :cond_0
    and-int/lit8 v4, v1, 0x2

    if-eqz v4, :cond_1

    move v2, v3

    .line 349
    .end local v1    # "keyFlags":I
    :cond_1
    :goto_0
    return v2

    :cond_2
    if-eq v0, v3, :cond_3

    if-eq v0, v5, :cond_3

    if-eq v0, v6, :cond_3

    if-eq v0, v7, :cond_3

    const/16 v4, 0x14

    if-ne v0, v4, :cond_1

    :cond_3
    move v2, v3

    goto :goto_0
.end method
