.class public Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaContentVerifierProviderBuilder;
.super Ljava/lang/Object;
.source "JcaContentVerifierProviderBuilder.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaContentVerifierProviderBuilder$SignatureOutputStream;,
        Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaContentVerifierProviderBuilder$RawSigVerifier;,
        Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaContentVerifierProviderBuilder$SigVerifier;
    }
.end annotation


# instance fields
.field private helper:Lcom/android/sec/org/bouncycastle/operator/jcajce/OperatorHelper;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    new-instance v0, Lcom/android/sec/org/bouncycastle/operator/jcajce/OperatorHelper;

    new-instance v1, Lcom/android/sec/org/bouncycastle/jcajce/DefaultJcaJceHelper;

    invoke-direct {v1}, Lcom/android/sec/org/bouncycastle/jcajce/DefaultJcaJceHelper;-><init>()V

    invoke-direct {v0, v1}, Lcom/android/sec/org/bouncycastle/operator/jcajce/OperatorHelper;-><init>(Lcom/android/sec/org/bouncycastle/jcajce/JcaJceHelper;)V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaContentVerifierProviderBuilder;->helper:Lcom/android/sec/org/bouncycastle/operator/jcajce/OperatorHelper;

    .line 33
    return-void
.end method

.method static synthetic access$000(Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaContentVerifierProviderBuilder;)Lcom/android/sec/org/bouncycastle/operator/jcajce/OperatorHelper;
    .locals 1
    .param p0, "x0"    # Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaContentVerifierProviderBuilder;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaContentVerifierProviderBuilder;->helper:Lcom/android/sec/org/bouncycastle/operator/jcajce/OperatorHelper;

    return-object v0
.end method

.method static synthetic access$100(Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaContentVerifierProviderBuilder;Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;Ljava/security/PublicKey;)Ljava/security/Signature;
    .locals 1
    .param p0, "x0"    # Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaContentVerifierProviderBuilder;
    .param p1, "x1"    # Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;
    .param p2, "x2"    # Ljava/security/PublicKey;

    .prologue
    .line 27
    invoke-direct {p0, p1, p2}, Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaContentVerifierProviderBuilder;->createRawSig(Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;Ljava/security/PublicKey;)Ljava/security/Signature;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaContentVerifierProviderBuilder;Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;Ljava/security/PublicKey;)Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaContentVerifierProviderBuilder$SignatureOutputStream;
    .locals 1
    .param p0, "x0"    # Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaContentVerifierProviderBuilder;
    .param p1, "x1"    # Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;
    .param p2, "x2"    # Ljava/security/PublicKey;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/operator/OperatorCreationException;
        }
    .end annotation

    .prologue
    .line 27
    invoke-direct {p0, p1, p2}, Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaContentVerifierProviderBuilder;->createSignatureStream(Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;Ljava/security/PublicKey;)Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaContentVerifierProviderBuilder$SignatureOutputStream;

    move-result-object v0

    return-object v0
.end method

.method private createRawSig(Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;Ljava/security/PublicKey;)Ljava/security/Signature;
    .locals 3
    .param p1, "algorithm"    # Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;
    .param p2, "publicKey"    # Ljava/security/PublicKey;

    .prologue
    .line 169
    :try_start_0
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaContentVerifierProviderBuilder;->helper:Lcom/android/sec/org/bouncycastle/operator/jcajce/OperatorHelper;

    invoke-virtual {v2, p1}, Lcom/android/sec/org/bouncycastle/operator/jcajce/OperatorHelper;->createRawSignature(Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;)Ljava/security/Signature;

    move-result-object v1

    .line 171
    .local v1, "rawSig":Ljava/security/Signature;
    if-eqz v1, :cond_0

    .line 173
    invoke-virtual {v1, p2}, Ljava/security/Signature;->initVerify(Ljava/security/PublicKey;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 180
    :cond_0
    :goto_0
    return-object v1

    .line 176
    .end local v1    # "rawSig":Ljava/security/Signature;
    :catch_0
    move-exception v0

    .line 178
    .local v0, "e":Ljava/lang/Exception;
    const/4 v1, 0x0

    .restart local v1    # "rawSig":Ljava/security/Signature;
    goto :goto_0
.end method

.method private createSignatureStream(Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;Ljava/security/PublicKey;)Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaContentVerifierProviderBuilder$SignatureOutputStream;
    .locals 5
    .param p1, "algorithm"    # Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;
    .param p2, "publicKey"    # Ljava/security/PublicKey;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/operator/OperatorCreationException;
        }
    .end annotation

    .prologue
    .line 152
    :try_start_0
    iget-object v2, p0, Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaContentVerifierProviderBuilder;->helper:Lcom/android/sec/org/bouncycastle/operator/jcajce/OperatorHelper;

    invoke-virtual {v2, p1}, Lcom/android/sec/org/bouncycastle/operator/jcajce/OperatorHelper;->createSignature(Lcom/android/sec/org/bouncycastle/asn1/x509/AlgorithmIdentifier;)Ljava/security/Signature;

    move-result-object v1

    .line 154
    .local v1, "sig":Ljava/security/Signature;
    invoke-virtual {v1, p2}, Ljava/security/Signature;->initVerify(Ljava/security/PublicKey;)V

    .line 156
    new-instance v2, Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaContentVerifierProviderBuilder$SignatureOutputStream;

    invoke-direct {v2, p0, v1}, Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaContentVerifierProviderBuilder$SignatureOutputStream;-><init>(Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaContentVerifierProviderBuilder;Ljava/security/Signature;)V
    :try_end_0
    .catch Ljava/security/GeneralSecurityException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v2

    .line 158
    .end local v1    # "sig":Ljava/security/Signature;
    :catch_0
    move-exception v0

    .line 160
    .local v0, "e":Ljava/security/GeneralSecurityException;
    new-instance v2, Lcom/android/sec/org/bouncycastle/operator/OperatorCreationException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "exception on setup: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, v0}, Lcom/android/sec/org/bouncycastle/operator/OperatorCreationException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
.end method


# virtual methods
.method public build(Lcom/android/sec/org/bouncycastle/cert/X509CertificateHolder;)Lcom/android/sec/org/bouncycastle/operator/ContentVerifierProvider;
    .locals 1
    .param p1, "certHolder"    # Lcom/android/sec/org/bouncycastle/cert/X509CertificateHolder;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/operator/OperatorCreationException;,
            Ljava/security/cert/CertificateException;
        }
    .end annotation

    .prologue
    .line 52
    iget-object v0, p0, Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaContentVerifierProviderBuilder;->helper:Lcom/android/sec/org/bouncycastle/operator/jcajce/OperatorHelper;

    invoke-virtual {v0, p1}, Lcom/android/sec/org/bouncycastle/operator/jcajce/OperatorHelper;->convertCertificate(Lcom/android/sec/org/bouncycastle/cert/X509CertificateHolder;)Ljava/security/cert/X509Certificate;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaContentVerifierProviderBuilder;->build(Ljava/security/cert/X509Certificate;)Lcom/android/sec/org/bouncycastle/operator/ContentVerifierProvider;

    move-result-object v0

    return-object v0
.end method

.method public build(Ljava/security/PublicKey;)Lcom/android/sec/org/bouncycastle/operator/ContentVerifierProvider;
    .locals 1
    .param p1, "publicKey"    # Ljava/security/PublicKey;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/operator/OperatorCreationException;
        }
    .end annotation

    .prologue
    .line 116
    new-instance v0, Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaContentVerifierProviderBuilder$2;

    invoke-direct {v0, p0, p1}, Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaContentVerifierProviderBuilder$2;-><init>(Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaContentVerifierProviderBuilder;Ljava/security/PublicKey;)V

    return-object v0
.end method

.method public build(Ljava/security/cert/X509Certificate;)Lcom/android/sec/org/bouncycastle/operator/ContentVerifierProvider;
    .locals 5
    .param p1, "certificate"    # Ljava/security/cert/X509Certificate;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/operator/OperatorCreationException;
        }
    .end annotation

    .prologue
    .line 62
    :try_start_0
    new-instance v0, Lcom/android/sec/org/bouncycastle/cert/jcajce/JcaX509CertificateHolder;

    invoke-direct {v0, p1}, Lcom/android/sec/org/bouncycastle/cert/jcajce/JcaX509CertificateHolder;-><init>(Ljava/security/cert/X509Certificate;)V
    :try_end_0
    .catch Ljava/security/cert/CertificateEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 69
    .local v0, "certHolder":Lcom/android/sec/org/bouncycastle/cert/X509CertificateHolder;
    new-instance v2, Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaContentVerifierProviderBuilder$1;

    invoke-direct {v2, p0, v0, p1}, Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaContentVerifierProviderBuilder$1;-><init>(Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaContentVerifierProviderBuilder;Lcom/android/sec/org/bouncycastle/cert/X509CertificateHolder;Ljava/security/cert/X509Certificate;)V

    return-object v2

    .line 64
    .end local v0    # "certHolder":Lcom/android/sec/org/bouncycastle/cert/X509CertificateHolder;
    :catch_0
    move-exception v1

    .line 66
    .local v1, "e":Ljava/security/cert/CertificateEncodingException;
    new-instance v2, Lcom/android/sec/org/bouncycastle/operator/OperatorCreationException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "cannot process certificate: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/security/cert/CertificateEncodingException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, v1}, Lcom/android/sec/org/bouncycastle/operator/OperatorCreationException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
.end method

.method public setProvider(Ljava/lang/String;)Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaContentVerifierProviderBuilder;
    .locals 2
    .param p1, "providerName"    # Ljava/lang/String;

    .prologue
    .line 44
    new-instance v0, Lcom/android/sec/org/bouncycastle/operator/jcajce/OperatorHelper;

    new-instance v1, Lcom/android/sec/org/bouncycastle/jcajce/NamedJcaJceHelper;

    invoke-direct {v1, p1}, Lcom/android/sec/org/bouncycastle/jcajce/NamedJcaJceHelper;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v1}, Lcom/android/sec/org/bouncycastle/operator/jcajce/OperatorHelper;-><init>(Lcom/android/sec/org/bouncycastle/jcajce/JcaJceHelper;)V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaContentVerifierProviderBuilder;->helper:Lcom/android/sec/org/bouncycastle/operator/jcajce/OperatorHelper;

    .line 46
    return-object p0
.end method

.method public setProvider(Ljava/security/Provider;)Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaContentVerifierProviderBuilder;
    .locals 2
    .param p1, "provider"    # Ljava/security/Provider;

    .prologue
    .line 37
    new-instance v0, Lcom/android/sec/org/bouncycastle/operator/jcajce/OperatorHelper;

    new-instance v1, Lcom/android/sec/org/bouncycastle/jcajce/ProviderJcaJceHelper;

    invoke-direct {v1, p1}, Lcom/android/sec/org/bouncycastle/jcajce/ProviderJcaJceHelper;-><init>(Ljava/security/Provider;)V

    invoke-direct {v0, v1}, Lcom/android/sec/org/bouncycastle/operator/jcajce/OperatorHelper;-><init>(Lcom/android/sec/org/bouncycastle/jcajce/JcaJceHelper;)V

    iput-object v0, p0, Lcom/android/sec/org/bouncycastle/operator/jcajce/JcaContentVerifierProviderBuilder;->helper:Lcom/android/sec/org/bouncycastle/operator/jcajce/OperatorHelper;

    .line 39
    return-object p0
.end method
