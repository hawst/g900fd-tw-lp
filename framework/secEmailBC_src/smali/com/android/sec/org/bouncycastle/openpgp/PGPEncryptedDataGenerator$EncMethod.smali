.class abstract Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator$EncMethod;
.super Lcom/android/sec/org/bouncycastle/bcpg/ContainedPacket;
.source "PGPEncryptedDataGenerator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x402
    name = "EncMethod"
.end annotation


# instance fields
.field protected encAlgorithm:I

.field protected key:Ljava/security/Key;

.field protected sessionInfo:[B

.field final synthetic this$0:Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;


# direct methods
.method private constructor <init>(Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;)V
    .locals 0

    .prologue
    .line 41
    iput-object p1, p0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator$EncMethod;->this$0:Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;

    invoke-direct {p0}, Lcom/android/sec/org/bouncycastle/bcpg/ContainedPacket;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;
    .param p2, "x1"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator$1;

    .prologue
    .line 41
    invoke-direct {p0, p1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator$EncMethod;-><init>(Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;)V

    return-void
.end method


# virtual methods
.method public abstract addSessionInfo([B)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation
.end method
