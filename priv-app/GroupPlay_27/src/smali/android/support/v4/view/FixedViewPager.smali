.class public Landroid/support/v4/view/FixedViewPager;
.super Landroid/view/ViewGroup;
.source "FixedViewPager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/support/v4/view/FixedViewPager$LayoutParams;,
        Landroid/support/v4/view/FixedViewPager$PagerObserver;,
        Landroid/support/v4/view/FixedViewPager$MyAccessibilityDelegate;,
        Landroid/support/v4/view/FixedViewPager$SavedState;,
        Landroid/support/v4/view/FixedViewPager$MoveInfo;,
        Landroid/support/v4/view/FixedViewPager$Decor;,
        Landroid/support/v4/view/FixedViewPager$OnAdapterChangeListener;,
        Landroid/support/v4/view/FixedViewPager$SimpleOnPageChangeListener;,
        Landroid/support/v4/view/FixedViewPager$OnPageChangeListener;,
        Landroid/support/v4/view/FixedViewPager$ItemInfo;
    }
.end annotation


# static fields
.field private static final CLOSE_ENOUGH:I = 0x2

.field private static final COMPARATOR:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Landroid/support/v4/view/FixedViewPager$ItemInfo;",
            ">;"
        }
    .end annotation
.end field

.field private static final DEBUG:Z = false

.field private static final DEFAULT_GUTTER_SIZE:I = 0x10

.field private static final DEFAULT_OFFSCREEN_PAGES:I = 0x1

.field private static final INVALID_POINTER:I = -0x1

.field private static final LAYOUT_ATTRS:[I

.field private static final MAX_SETTLE_DURATION:I = 0x258

.field private static final MIN_DISTANCE_FOR_FLING:I = 0x19

.field private static final NO_BOUNCE_BACK_BY_APPLE_PATENT:Z = true

.field public static final SCROLL_STATE_DRAGGING:I = 0x1

.field public static final SCROLL_STATE_IDLE:I = 0x0

.field public static final SCROLL_STATE_SETTLING:I = 0x2

.field private static final TAG:Ljava/lang/String; = "ViewPager"

.field private static final USE_CACHE:Z

.field private static final sInterpolator:Landroid/view/animation/Interpolator;


# instance fields
.field private mActivePointerId:I

.field private mAdapter:Landroid/support/v4/view/FixedPagerAdapter;

.field private mAdapterChangeListener:Landroid/support/v4/view/FixedViewPager$OnAdapterChangeListener;

.field private mBottomPageBounds:I

.field private mCalledSuper:Z

.field private mChildHeightMeasureSpec:I

.field private mChildWidthMeasureSpec:I

.field private mCloseEnough:I

.field private mCurItem:I

.field private mDecorChildCount:I

.field private mDefaultGutterSize:I

.field private mFakeDragBeginTime:J

.field private mFakeDragging:Z

.field private mFirstLayout:Z

.field private mFirstOffset:F

.field private mFlingDistance:I

.field private mGutterSize:I

.field private mIgnoreGutter:Z

.field private mInLayout:Z

.field private mInitialMotionX:F

.field private mInternalPageChangeListener:Landroid/support/v4/view/FixedViewPager$OnPageChangeListener;

.field private mIsBeingDragged:Z

.field private mIsUnableToDrag:Z

.field private final mItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/support/v4/view/FixedViewPager$ItemInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mLastMotionX:F

.field private mLastMotionY:F

.field private mLastOffset:F

.field private mLeftEdge:Landroid/support/v4/widget/EdgeEffectCompat;

.field private mMarginDrawable:Landroid/graphics/drawable/Drawable;

.field private mMaximumVelocity:I

.field private mMinimumVelocity:I

.field private mNeedCalculatePageOffsets:Z

.field private mObserver:Landroid/support/v4/view/FixedViewPager$PagerObserver;

.field private mOffscreenPageLimit:I

.field private mOnPageChangeListener:Landroid/support/v4/view/FixedViewPager$OnPageChangeListener;

.field private mPageMargin:I

.field private mPopulatePending:Z

.field private mPreviouseDataCount:I

.field private mRestoredAdapterState:Landroid/os/Parcelable;

.field private mRestoredClassLoader:Ljava/lang/ClassLoader;

.field private mRestoredCurItem:I

.field private mRightEdge:Landroid/support/v4/widget/EdgeEffectCompat;

.field private mScrollState:I

.field private mScroller:Landroid/widget/Scroller;

.field private mScrollingCacheEnabled:Z

.field private final mTempItem:Landroid/support/v4/view/FixedViewPager$ItemInfo;

.field private final mTempRect:Landroid/graphics/Rect;

.field private mTopPageBounds:I

.field private mTouchSlop:I

.field private mVelocityTracker:Landroid/view/VelocityTracker;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 97
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x10100b3

    aput v2, v0, v1

    sput-object v0, Landroid/support/v4/view/FixedViewPager;->LAYOUT_ATTRS:[I

    .line 109
    new-instance v0, Landroid/support/v4/view/FixedViewPager$1;

    invoke-direct {v0}, Landroid/support/v4/view/FixedViewPager$1;-><init>()V

    sput-object v0, Landroid/support/v4/view/FixedViewPager;->COMPARATOR:Ljava/util/Comparator;

    .line 116
    new-instance v0, Landroid/support/v4/view/FixedViewPager$2;

    invoke-direct {v0}, Landroid/support/v4/view/FixedViewPager$2;-><init>()V

    sput-object v0, Landroid/support/v4/view/FixedViewPager;->sInterpolator:Landroid/view/animation/Interpolator;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 302
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    .line 124
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v4/view/FixedViewPager;->mItems:Ljava/util/ArrayList;

    .line 125
    new-instance v0, Landroid/support/v4/view/FixedViewPager$ItemInfo;

    invoke-direct {v0}, Landroid/support/v4/view/FixedViewPager$ItemInfo;-><init>()V

    iput-object v0, p0, Landroid/support/v4/view/FixedViewPager;->mTempItem:Landroid/support/v4/view/FixedViewPager$ItemInfo;

    .line 127
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Landroid/support/v4/view/FixedViewPager;->mTempRect:Landroid/graphics/Rect;

    .line 131
    iput v2, p0, Landroid/support/v4/view/FixedViewPager;->mRestoredCurItem:I

    .line 132
    iput-object v4, p0, Landroid/support/v4/view/FixedViewPager;->mRestoredAdapterState:Landroid/os/Parcelable;

    .line 133
    iput-object v4, p0, Landroid/support/v4/view/FixedViewPager;->mRestoredClassLoader:Ljava/lang/ClassLoader;

    .line 145
    const v0, -0x800001

    iput v0, p0, Landroid/support/v4/view/FixedViewPager;->mFirstOffset:F

    .line 146
    const v0, 0x7f7fffff    # Float.MAX_VALUE

    iput v0, p0, Landroid/support/v4/view/FixedViewPager;->mLastOffset:F

    .line 155
    iput v3, p0, Landroid/support/v4/view/FixedViewPager;->mOffscreenPageLimit:I

    .line 174
    iput v2, p0, Landroid/support/v4/view/FixedViewPager;->mActivePointerId:I

    .line 201
    iput-boolean v3, p0, Landroid/support/v4/view/FixedViewPager;->mFirstLayout:Z

    .line 202
    iput-boolean v1, p0, Landroid/support/v4/view/FixedViewPager;->mNeedCalculatePageOffsets:Z

    .line 227
    iput v1, p0, Landroid/support/v4/view/FixedViewPager;->mScrollState:I

    .line 733
    iput v1, p0, Landroid/support/v4/view/FixedViewPager;->mPreviouseDataCount:I

    .line 303
    invoke-virtual {p0}, Landroid/support/v4/view/FixedViewPager;->initViewPager()V

    .line 304
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 307
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 124
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v4/view/FixedViewPager;->mItems:Ljava/util/ArrayList;

    .line 125
    new-instance v0, Landroid/support/v4/view/FixedViewPager$ItemInfo;

    invoke-direct {v0}, Landroid/support/v4/view/FixedViewPager$ItemInfo;-><init>()V

    iput-object v0, p0, Landroid/support/v4/view/FixedViewPager;->mTempItem:Landroid/support/v4/view/FixedViewPager$ItemInfo;

    .line 127
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Landroid/support/v4/view/FixedViewPager;->mTempRect:Landroid/graphics/Rect;

    .line 131
    iput v2, p0, Landroid/support/v4/view/FixedViewPager;->mRestoredCurItem:I

    .line 132
    iput-object v4, p0, Landroid/support/v4/view/FixedViewPager;->mRestoredAdapterState:Landroid/os/Parcelable;

    .line 133
    iput-object v4, p0, Landroid/support/v4/view/FixedViewPager;->mRestoredClassLoader:Ljava/lang/ClassLoader;

    .line 145
    const v0, -0x800001

    iput v0, p0, Landroid/support/v4/view/FixedViewPager;->mFirstOffset:F

    .line 146
    const v0, 0x7f7fffff    # Float.MAX_VALUE

    iput v0, p0, Landroid/support/v4/view/FixedViewPager;->mLastOffset:F

    .line 155
    iput v3, p0, Landroid/support/v4/view/FixedViewPager;->mOffscreenPageLimit:I

    .line 174
    iput v2, p0, Landroid/support/v4/view/FixedViewPager;->mActivePointerId:I

    .line 201
    iput-boolean v3, p0, Landroid/support/v4/view/FixedViewPager;->mFirstLayout:Z

    .line 202
    iput-boolean v1, p0, Landroid/support/v4/view/FixedViewPager;->mNeedCalculatePageOffsets:Z

    .line 227
    iput v1, p0, Landroid/support/v4/view/FixedViewPager;->mScrollState:I

    .line 733
    iput v1, p0, Landroid/support/v4/view/FixedViewPager;->mPreviouseDataCount:I

    .line 308
    invoke-virtual {p0}, Landroid/support/v4/view/FixedViewPager;->initViewPager()V

    .line 309
    return-void
.end method

.method static synthetic access$100(Landroid/support/v4/view/FixedViewPager;)Landroid/support/v4/view/FixedPagerAdapter;
    .locals 1
    .param p0, "x0"    # Landroid/support/v4/view/FixedViewPager;

    .prologue
    .line 85
    iget-object v0, p0, Landroid/support/v4/view/FixedViewPager;->mAdapter:Landroid/support/v4/view/FixedPagerAdapter;

    return-object v0
.end method

.method static synthetic access$200(Landroid/support/v4/view/FixedViewPager;)I
    .locals 1
    .param p0, "x0"    # Landroid/support/v4/view/FixedViewPager;

    .prologue
    .line 85
    iget v0, p0, Landroid/support/v4/view/FixedViewPager;->mCurItem:I

    return v0
.end method

.method static synthetic access$300()[I
    .locals 1

    .prologue
    .line 85
    sget-object v0, Landroid/support/v4/view/FixedViewPager;->LAYOUT_ATTRS:[I

    return-object v0
.end method

.method private calculatePageOffsets(Landroid/support/v4/view/FixedViewPager$ItemInfo;ILandroid/support/v4/view/FixedViewPager$ItemInfo;)V
    .locals 14
    .param p1, "curItem"    # Landroid/support/v4/view/FixedViewPager$ItemInfo;
    .param p2, "curIndex"    # I
    .param p3, "oldCurInfo"    # Landroid/support/v4/view/FixedViewPager$ItemInfo;

    .prologue
    .line 1033
    iget-object v12, p0, Landroid/support/v4/view/FixedViewPager;->mAdapter:Landroid/support/v4/view/FixedPagerAdapter;

    invoke-virtual {v12}, Landroid/support/v4/view/FixedPagerAdapter;->getCount()I

    move-result v1

    .line 1034
    .local v1, "N":I
    invoke-virtual {p0}, Landroid/support/v4/view/FixedViewPager;->getWidth()I

    move-result v11

    .line 1035
    .local v11, "width":I
    if-lez v11, :cond_0

    iget v12, p0, Landroid/support/v4/view/FixedViewPager;->mPageMargin:I

    int-to-float v12, v12

    int-to-float v13, v11

    div-float v6, v12, v13

    .line 1037
    .local v6, "marginOffset":F
    :goto_0
    if-eqz p3, :cond_6

    .line 1038
    move-object/from16 v0, p3

    iget v8, v0, Landroid/support/v4/view/FixedViewPager$ItemInfo;->position:I

    .line 1040
    .local v8, "oldCurPosition":I
    iget v12, p1, Landroid/support/v4/view/FixedViewPager$ItemInfo;->position:I

    if-ge v8, v12, :cond_3

    .line 1041
    const/4 v5, 0x0

    .line 1042
    .local v5, "itemIndex":I
    const/4 v3, 0x0

    .line 1043
    .local v3, "ii":Landroid/support/v4/view/FixedViewPager$ItemInfo;
    move-object/from16 v0, p3

    iget v12, v0, Landroid/support/v4/view/FixedViewPager$ItemInfo;->offset:F

    move-object/from16 v0, p3

    iget v13, v0, Landroid/support/v4/view/FixedViewPager$ItemInfo;->widthFactor:F

    add-float/2addr v12, v13

    add-float v7, v12, v6

    .line 1044
    .local v7, "offset":F
    add-int/lit8 v9, v8, 0x1

    .line 1045
    .local v9, "pos":I
    :goto_1
    iget v12, p1, Landroid/support/v4/view/FixedViewPager$ItemInfo;->position:I

    if-gt v9, v12, :cond_6

    iget-object v12, p0, Landroid/support/v4/view/FixedViewPager;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v12}, Ljava/util/ArrayList;->size()I

    move-result v12

    if-ge v5, v12, :cond_6

    .line 1046
    iget-object v12, p0, Landroid/support/v4/view/FixedViewPager;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v12, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "ii":Landroid/support/v4/view/FixedViewPager$ItemInfo;
    check-cast v3, Landroid/support/v4/view/FixedViewPager$ItemInfo;

    .line 1047
    .restart local v3    # "ii":Landroid/support/v4/view/FixedViewPager$ItemInfo;
    :goto_2
    iget v12, v3, Landroid/support/v4/view/FixedViewPager$ItemInfo;->position:I

    if-le v9, v12, :cond_1

    iget-object v12, p0, Landroid/support/v4/view/FixedViewPager;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v12}, Ljava/util/ArrayList;->size()I

    move-result v12

    add-int/lit8 v12, v12, -0x1

    if-ge v5, v12, :cond_1

    .line 1048
    add-int/lit8 v5, v5, 0x1

    .line 1049
    iget-object v12, p0, Landroid/support/v4/view/FixedViewPager;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v12, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "ii":Landroid/support/v4/view/FixedViewPager$ItemInfo;
    check-cast v3, Landroid/support/v4/view/FixedViewPager$ItemInfo;

    .restart local v3    # "ii":Landroid/support/v4/view/FixedViewPager$ItemInfo;
    goto :goto_2

    .line 1035
    .end local v3    # "ii":Landroid/support/v4/view/FixedViewPager$ItemInfo;
    .end local v5    # "itemIndex":I
    .end local v6    # "marginOffset":F
    .end local v7    # "offset":F
    .end local v8    # "oldCurPosition":I
    .end local v9    # "pos":I
    :cond_0
    const/4 v6, 0x0

    goto :goto_0

    .line 1051
    .restart local v3    # "ii":Landroid/support/v4/view/FixedViewPager$ItemInfo;
    .restart local v5    # "itemIndex":I
    .restart local v6    # "marginOffset":F
    .restart local v7    # "offset":F
    .restart local v8    # "oldCurPosition":I
    .restart local v9    # "pos":I
    :cond_1
    :goto_3
    iget v12, v3, Landroid/support/v4/view/FixedViewPager$ItemInfo;->position:I

    if-ge v9, v12, :cond_2

    .line 1054
    iget-object v12, p0, Landroid/support/v4/view/FixedViewPager;->mAdapter:Landroid/support/v4/view/FixedPagerAdapter;

    invoke-virtual {v12, v9}, Landroid/support/v4/view/FixedPagerAdapter;->getPageWidth(I)F

    move-result v12

    add-float/2addr v12, v6

    add-float/2addr v7, v12

    .line 1055
    add-int/lit8 v9, v9, 0x1

    goto :goto_3

    .line 1057
    :cond_2
    iput v7, v3, Landroid/support/v4/view/FixedViewPager$ItemInfo;->offset:F

    .line 1058
    iget v12, v3, Landroid/support/v4/view/FixedViewPager$ItemInfo;->widthFactor:F

    add-float/2addr v12, v6

    add-float/2addr v7, v12

    .line 1045
    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    .line 1060
    .end local v3    # "ii":Landroid/support/v4/view/FixedViewPager$ItemInfo;
    .end local v5    # "itemIndex":I
    .end local v7    # "offset":F
    .end local v9    # "pos":I
    :cond_3
    iget v12, p1, Landroid/support/v4/view/FixedViewPager$ItemInfo;->position:I

    if-le v8, v12, :cond_6

    .line 1061
    iget-object v12, p0, Landroid/support/v4/view/FixedViewPager;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v12}, Ljava/util/ArrayList;->size()I

    move-result v12

    add-int/lit8 v5, v12, -0x1

    .line 1062
    .restart local v5    # "itemIndex":I
    const/4 v3, 0x0

    .line 1063
    .restart local v3    # "ii":Landroid/support/v4/view/FixedViewPager$ItemInfo;
    move-object/from16 v0, p3

    iget v7, v0, Landroid/support/v4/view/FixedViewPager$ItemInfo;->offset:F

    .line 1064
    .restart local v7    # "offset":F
    add-int/lit8 v9, v8, -0x1

    .line 1065
    .restart local v9    # "pos":I
    :goto_4
    iget v12, p1, Landroid/support/v4/view/FixedViewPager$ItemInfo;->position:I

    if-lt v9, v12, :cond_6

    if-ltz v5, :cond_6

    .line 1066
    iget-object v12, p0, Landroid/support/v4/view/FixedViewPager;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v12, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "ii":Landroid/support/v4/view/FixedViewPager$ItemInfo;
    check-cast v3, Landroid/support/v4/view/FixedViewPager$ItemInfo;

    .line 1067
    .restart local v3    # "ii":Landroid/support/v4/view/FixedViewPager$ItemInfo;
    :goto_5
    iget v12, v3, Landroid/support/v4/view/FixedViewPager$ItemInfo;->position:I

    if-ge v9, v12, :cond_4

    if-lez v5, :cond_4

    .line 1068
    add-int/lit8 v5, v5, -0x1

    .line 1069
    iget-object v12, p0, Landroid/support/v4/view/FixedViewPager;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v12, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "ii":Landroid/support/v4/view/FixedViewPager$ItemInfo;
    check-cast v3, Landroid/support/v4/view/FixedViewPager$ItemInfo;

    .restart local v3    # "ii":Landroid/support/v4/view/FixedViewPager$ItemInfo;
    goto :goto_5

    .line 1071
    :cond_4
    :goto_6
    iget v12, v3, Landroid/support/v4/view/FixedViewPager$ItemInfo;->position:I

    if-le v9, v12, :cond_5

    .line 1074
    iget-object v12, p0, Landroid/support/v4/view/FixedViewPager;->mAdapter:Landroid/support/v4/view/FixedPagerAdapter;

    invoke-virtual {v12, v9}, Landroid/support/v4/view/FixedPagerAdapter;->getPageWidth(I)F

    move-result v12

    add-float/2addr v12, v6

    sub-float/2addr v7, v12

    .line 1075
    add-int/lit8 v9, v9, -0x1

    goto :goto_6

    .line 1077
    :cond_5
    iget v12, v3, Landroid/support/v4/view/FixedViewPager$ItemInfo;->widthFactor:F

    add-float/2addr v12, v6

    sub-float/2addr v7, v12

    .line 1078
    iput v7, v3, Landroid/support/v4/view/FixedViewPager$ItemInfo;->offset:F

    .line 1065
    add-int/lit8 v9, v9, -0x1

    goto :goto_4

    .line 1084
    .end local v3    # "ii":Landroid/support/v4/view/FixedViewPager$ItemInfo;
    .end local v5    # "itemIndex":I
    .end local v7    # "offset":F
    .end local v8    # "oldCurPosition":I
    .end local v9    # "pos":I
    :cond_6
    iget-object v12, p0, Landroid/support/v4/view/FixedViewPager;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v12}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 1085
    .local v4, "itemCount":I
    iget v7, p1, Landroid/support/v4/view/FixedViewPager$ItemInfo;->offset:F

    .line 1086
    .restart local v7    # "offset":F
    iget v12, p1, Landroid/support/v4/view/FixedViewPager$ItemInfo;->position:I

    add-int/lit8 v9, v12, -0x1

    .line 1087
    .restart local v9    # "pos":I
    iget v12, p1, Landroid/support/v4/view/FixedViewPager$ItemInfo;->position:I

    if-nez v12, :cond_7

    iget v12, p1, Landroid/support/v4/view/FixedViewPager$ItemInfo;->offset:F

    :goto_7
    iput v12, p0, Landroid/support/v4/view/FixedViewPager;->mFirstOffset:F

    .line 1088
    iget v12, p1, Landroid/support/v4/view/FixedViewPager$ItemInfo;->position:I

    add-int/lit8 v13, v1, -0x1

    if-ne v12, v13, :cond_8

    iget v12, p1, Landroid/support/v4/view/FixedViewPager$ItemInfo;->offset:F

    iget v13, p1, Landroid/support/v4/view/FixedViewPager$ItemInfo;->widthFactor:F

    add-float/2addr v12, v13

    const/high16 v13, 0x3f800000    # 1.0f

    sub-float/2addr v12, v13

    :goto_8
    iput v12, p0, Landroid/support/v4/view/FixedViewPager;->mLastOffset:F

    .line 1091
    add-int/lit8 v2, p2, -0x1

    .local v2, "i":I
    :goto_9
    if-ltz v2, :cond_b

    .line 1092
    iget-object v12, p0, Landroid/support/v4/view/FixedViewPager;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v12, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/support/v4/view/FixedViewPager$ItemInfo;

    .line 1093
    .restart local v3    # "ii":Landroid/support/v4/view/FixedViewPager$ItemInfo;
    :goto_a
    iget v12, v3, Landroid/support/v4/view/FixedViewPager$ItemInfo;->position:I

    if-le v9, v12, :cond_9

    .line 1094
    iget-object v12, p0, Landroid/support/v4/view/FixedViewPager;->mAdapter:Landroid/support/v4/view/FixedPagerAdapter;

    add-int/lit8 v10, v9, -0x1

    .end local v9    # "pos":I
    .local v10, "pos":I
    invoke-virtual {v12, v9}, Landroid/support/v4/view/FixedPagerAdapter;->getPageWidth(I)F

    move-result v12

    add-float/2addr v12, v6

    sub-float/2addr v7, v12

    move v9, v10

    .end local v10    # "pos":I
    .restart local v9    # "pos":I
    goto :goto_a

    .line 1087
    .end local v2    # "i":I
    .end local v3    # "ii":Landroid/support/v4/view/FixedViewPager$ItemInfo;
    :cond_7
    const v12, -0x800001

    goto :goto_7

    .line 1088
    :cond_8
    const v12, 0x7f7fffff    # Float.MAX_VALUE

    goto :goto_8

    .line 1096
    .restart local v2    # "i":I
    .restart local v3    # "ii":Landroid/support/v4/view/FixedViewPager$ItemInfo;
    :cond_9
    iget v12, v3, Landroid/support/v4/view/FixedViewPager$ItemInfo;->widthFactor:F

    add-float/2addr v12, v6

    sub-float/2addr v7, v12

    .line 1097
    iput v7, v3, Landroid/support/v4/view/FixedViewPager$ItemInfo;->offset:F

    .line 1098
    iget v12, v3, Landroid/support/v4/view/FixedViewPager$ItemInfo;->position:I

    if-nez v12, :cond_a

    iput v7, p0, Landroid/support/v4/view/FixedViewPager;->mFirstOffset:F

    .line 1091
    :cond_a
    add-int/lit8 v2, v2, -0x1

    add-int/lit8 v9, v9, -0x1

    goto :goto_9

    .line 1100
    .end local v3    # "ii":Landroid/support/v4/view/FixedViewPager$ItemInfo;
    :cond_b
    iget v12, p1, Landroid/support/v4/view/FixedViewPager$ItemInfo;->offset:F

    iget v13, p1, Landroid/support/v4/view/FixedViewPager$ItemInfo;->widthFactor:F

    add-float/2addr v12, v13

    add-float v7, v12, v6

    .line 1101
    iget v12, p1, Landroid/support/v4/view/FixedViewPager$ItemInfo;->position:I

    add-int/lit8 v9, v12, 0x1

    .line 1103
    add-int/lit8 v2, p2, 0x1

    :goto_b
    if-ge v2, v4, :cond_e

    .line 1104
    iget-object v12, p0, Landroid/support/v4/view/FixedViewPager;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v12, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/support/v4/view/FixedViewPager$ItemInfo;

    .line 1105
    .restart local v3    # "ii":Landroid/support/v4/view/FixedViewPager$ItemInfo;
    :goto_c
    iget v12, v3, Landroid/support/v4/view/FixedViewPager$ItemInfo;->position:I

    if-ge v9, v12, :cond_c

    .line 1106
    iget-object v12, p0, Landroid/support/v4/view/FixedViewPager;->mAdapter:Landroid/support/v4/view/FixedPagerAdapter;

    add-int/lit8 v10, v9, 0x1

    .end local v9    # "pos":I
    .restart local v10    # "pos":I
    invoke-virtual {v12, v9}, Landroid/support/v4/view/FixedPagerAdapter;->getPageWidth(I)F

    move-result v12

    add-float/2addr v12, v6

    add-float/2addr v7, v12

    move v9, v10

    .end local v10    # "pos":I
    .restart local v9    # "pos":I
    goto :goto_c

    .line 1108
    :cond_c
    iget v12, v3, Landroid/support/v4/view/FixedViewPager$ItemInfo;->position:I

    add-int/lit8 v13, v1, -0x1

    if-ne v12, v13, :cond_d

    .line 1109
    iget v12, v3, Landroid/support/v4/view/FixedViewPager$ItemInfo;->widthFactor:F

    add-float/2addr v12, v7

    const/high16 v13, 0x3f800000    # 1.0f

    sub-float/2addr v12, v13

    iput v12, p0, Landroid/support/v4/view/FixedViewPager;->mLastOffset:F

    .line 1111
    :cond_d
    iput v7, v3, Landroid/support/v4/view/FixedViewPager$ItemInfo;->offset:F

    .line 1112
    iget v12, v3, Landroid/support/v4/view/FixedViewPager$ItemInfo;->widthFactor:F

    add-float/2addr v12, v6

    add-float/2addr v7, v12

    .line 1103
    add-int/lit8 v2, v2, 0x1

    add-int/lit8 v9, v9, 0x1

    goto :goto_b

    .line 1115
    .end local v3    # "ii":Landroid/support/v4/view/FixedViewPager$ItemInfo;
    :cond_e
    const/4 v12, 0x0

    iput-boolean v12, p0, Landroid/support/v4/view/FixedViewPager;->mNeedCalculatePageOffsets:Z

    .line 1116
    return-void
.end method

.method private completeScroll()V
    .locals 10

    .prologue
    const/4 v7, 0x0

    .line 1620
    iget v8, p0, Landroid/support/v4/view/FixedViewPager;->mScrollState:I

    const/4 v9, 0x2

    if-ne v8, v9, :cond_4

    const/4 v2, 0x1

    .line 1621
    .local v2, "needPopulate":Z
    :goto_0
    if-eqz v2, :cond_2

    .line 1623
    invoke-direct {p0, v7}, Landroid/support/v4/view/FixedViewPager;->setScrollingCacheEnabled(Z)V

    .line 1624
    iget-object v8, p0, Landroid/support/v4/view/FixedViewPager;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v8}, Landroid/widget/Scroller;->abortAnimation()V

    .line 1625
    invoke-virtual {p0}, Landroid/support/v4/view/FixedViewPager;->getScrollX()I

    move-result v3

    .line 1626
    .local v3, "oldX":I
    invoke-virtual {p0}, Landroid/support/v4/view/FixedViewPager;->getScrollY()I

    move-result v4

    .line 1627
    .local v4, "oldY":I
    iget-object v8, p0, Landroid/support/v4/view/FixedViewPager;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v8}, Landroid/widget/Scroller;->getCurrX()I

    move-result v5

    .line 1628
    .local v5, "x":I
    iget-object v8, p0, Landroid/support/v4/view/FixedViewPager;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v8}, Landroid/widget/Scroller;->getCurrY()I

    move-result v6

    .line 1629
    .local v6, "y":I
    if-ne v3, v5, :cond_0

    if-eq v4, v6, :cond_1

    .line 1630
    :cond_0
    invoke-virtual {p0, v5, v6}, Landroid/support/v4/view/FixedViewPager;->scrollTo(II)V

    .line 1632
    :cond_1
    invoke-direct {p0, v7}, Landroid/support/v4/view/FixedViewPager;->setScrollState(I)V

    .line 1634
    .end local v3    # "oldX":I
    .end local v4    # "oldY":I
    .end local v5    # "x":I
    .end local v6    # "y":I
    :cond_2
    iput-boolean v7, p0, Landroid/support/v4/view/FixedViewPager;->mPopulatePending:Z

    .line 1635
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v8, p0, Landroid/support/v4/view/FixedViewPager;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-ge v0, v8, :cond_5

    .line 1636
    iget-object v8, p0, Landroid/support/v4/view/FixedViewPager;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v4/view/FixedViewPager$ItemInfo;

    .line 1637
    .local v1, "ii":Landroid/support/v4/view/FixedViewPager$ItemInfo;
    iget-boolean v8, v1, Landroid/support/v4/view/FixedViewPager$ItemInfo;->scrolling:Z

    if-eqz v8, :cond_3

    .line 1638
    const/4 v2, 0x1

    .line 1639
    iput-boolean v7, v1, Landroid/support/v4/view/FixedViewPager$ItemInfo;->scrolling:Z

    .line 1635
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .end local v0    # "i":I
    .end local v1    # "ii":Landroid/support/v4/view/FixedViewPager$ItemInfo;
    .end local v2    # "needPopulate":Z
    :cond_4
    move v2, v7

    .line 1620
    goto :goto_0

    .line 1642
    .restart local v0    # "i":I
    .restart local v2    # "needPopulate":Z
    :cond_5
    if-eqz v2, :cond_6

    .line 1643
    invoke-virtual {p0}, Landroid/support/v4/view/FixedViewPager;->populate()V

    .line 1645
    :cond_6
    return-void
.end method

.method private determineTargetPage(IFII)I
    .locals 5
    .param p1, "currentPage"    # I
    .param p2, "pageOffset"    # F
    .param p3, "velocity"    # I
    .param p4, "deltaX"    # I

    .prologue
    .line 2004
    const/4 v2, 0x0

    .line 2005
    .local v2, "targetPage":I
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "currentPage:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ",pageOffset:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ",velocity:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ",deltaX:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 2009
    if-lez p4, :cond_2

    .line 2011
    move v2, p1

    .line 2031
    :cond_0
    :goto_0
    iget-object v3, p0, Landroid/support/v4/view/FixedViewPager;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_1

    .line 2032
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "targetPage:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 2033
    iget-object v3, p0, Landroid/support/v4/view/FixedViewPager;->mItems:Ljava/util/ArrayList;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/FixedViewPager$ItemInfo;

    .line 2034
    .local v0, "firstItem":Landroid/support/v4/view/FixedViewPager$ItemInfo;
    iget-object v3, p0, Landroid/support/v4/view/FixedViewPager;->mItems:Ljava/util/ArrayList;

    iget-object v4, p0, Landroid/support/v4/view/FixedViewPager;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v4/view/FixedViewPager$ItemInfo;

    .line 2037
    .local v1, "lastItem":Landroid/support/v4/view/FixedViewPager$ItemInfo;
    iget v3, v0, Landroid/support/v4/view/FixedViewPager$ItemInfo;->position:I

    iget v4, v1, Landroid/support/v4/view/FixedViewPager$ItemInfo;->position:I

    invoke-static {v2, v4}, Ljava/lang/Math;->min(II)I

    move-result v4

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 2040
    .end local v0    # "firstItem":Landroid/support/v4/view/FixedViewPager$ItemInfo;
    .end local v1    # "lastItem":Landroid/support/v4/view/FixedViewPager$ItemInfo;
    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "targetPage:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 2042
    return v2

    .line 2013
    :cond_2
    if-gez p4, :cond_0

    .line 2015
    add-int/lit8 v2, p1, 0x1

    goto :goto_0
.end method

.method private endDrag()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2275
    iput-boolean v0, p0, Landroid/support/v4/view/FixedViewPager;->mIsBeingDragged:Z

    .line 2276
    iput-boolean v0, p0, Landroid/support/v4/view/FixedViewPager;->mIsUnableToDrag:Z

    .line 2278
    iget-object v0, p0, Landroid/support/v4/view/FixedViewPager;->mVelocityTracker:Landroid/view/VelocityTracker;

    if-eqz v0, :cond_0

    .line 2279
    iget-object v0, p0, Landroid/support/v4/view/FixedViewPager;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->recycle()V

    .line 2280
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v4/view/FixedViewPager;->mVelocityTracker:Landroid/view/VelocityTracker;

    .line 2282
    :cond_0
    return-void
.end method

.method private getChildRectInPagerCoordinates(Landroid/graphics/Rect;Landroid/view/View;)Landroid/graphics/Rect;
    .locals 4
    .param p1, "outRect"    # Landroid/graphics/Rect;
    .param p2, "child"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x0

    .line 2417
    if-nez p1, :cond_0

    .line 2418
    new-instance p1, Landroid/graphics/Rect;

    .end local p1    # "outRect":Landroid/graphics/Rect;
    invoke-direct {p1}, Landroid/graphics/Rect;-><init>()V

    .line 2420
    .restart local p1    # "outRect":Landroid/graphics/Rect;
    :cond_0
    if-nez p2, :cond_2

    .line 2421
    invoke-virtual {p1, v2, v2, v2, v2}, Landroid/graphics/Rect;->set(IIII)V

    .line 2439
    :cond_1
    return-object p1

    .line 2424
    :cond_2
    invoke-virtual {p2}, Landroid/view/View;->getLeft()I

    move-result v2

    iput v2, p1, Landroid/graphics/Rect;->left:I

    .line 2425
    invoke-virtual {p2}, Landroid/view/View;->getRight()I

    move-result v2

    iput v2, p1, Landroid/graphics/Rect;->right:I

    .line 2426
    invoke-virtual {p2}, Landroid/view/View;->getTop()I

    move-result v2

    iput v2, p1, Landroid/graphics/Rect;->top:I

    .line 2427
    invoke-virtual {p2}, Landroid/view/View;->getBottom()I

    move-result v2

    iput v2, p1, Landroid/graphics/Rect;->bottom:I

    .line 2429
    invoke-virtual {p2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    .line 2430
    .local v1, "parent":Landroid/view/ViewParent;
    :goto_0
    instance-of v2, v1, Landroid/view/ViewGroup;

    if-eqz v2, :cond_1

    if-eq v1, p0, :cond_1

    move-object v0, v1

    .line 2431
    check-cast v0, Landroid/view/ViewGroup;

    .line 2432
    .local v0, "group":Landroid/view/ViewGroup;
    iget v2, p1, Landroid/graphics/Rect;->left:I

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getLeft()I

    move-result v3

    add-int/2addr v2, v3

    iput v2, p1, Landroid/graphics/Rect;->left:I

    .line 2433
    iget v2, p1, Landroid/graphics/Rect;->right:I

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getRight()I

    move-result v3

    add-int/2addr v2, v3

    iput v2, p1, Landroid/graphics/Rect;->right:I

    .line 2434
    iget v2, p1, Landroid/graphics/Rect;->top:I

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getTop()I

    move-result v3

    add-int/2addr v2, v3

    iput v2, p1, Landroid/graphics/Rect;->top:I

    .line 2435
    iget v2, p1, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getBottom()I

    move-result v3

    add-int/2addr v2, v3

    iput v2, p1, Landroid/graphics/Rect;->bottom:I

    .line 2437
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    .line 2438
    goto :goto_0
.end method

.method private infoForCurrentScrollPosition()Landroid/support/v4/view/FixedViewPager$ItemInfo;
    .locals 15

    .prologue
    const/4 v8, 0x0

    .line 1962
    invoke-virtual {p0}, Landroid/support/v4/view/FixedViewPager;->getWidth()I

    move-result v12

    .line 1963
    .local v12, "width":I
    if-lez v12, :cond_5

    invoke-virtual {p0}, Landroid/support/v4/view/FixedViewPager;->getScrollX()I

    move-result v13

    int-to-float v13, v13

    int-to-float v14, v12

    div-float v11, v13, v14

    .line 1964
    .local v11, "scrollOffset":F
    :goto_0
    if-lez v12, :cond_0

    iget v13, p0, Landroid/support/v4/view/FixedViewPager;->mPageMargin:I

    int-to-float v13, v13

    int-to-float v14, v12

    div-float v8, v13, v14

    .line 1965
    .local v8, "marginOffset":F
    :cond_0
    const/4 v5, -0x1

    .line 1966
    .local v5, "lastPos":I
    const/4 v4, 0x0

    .line 1967
    .local v4, "lastOffset":F
    const/4 v6, 0x0

    .line 1968
    .local v6, "lastWidth":F
    const/4 v0, 0x1

    .line 1970
    .local v0, "first":Z
    const/4 v3, 0x0

    .line 1971
    .local v3, "lastItem":Landroid/support/v4/view/FixedViewPager$ItemInfo;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    iget-object v13, p0, Landroid/support/v4/view/FixedViewPager;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v13

    if-ge v1, v13, :cond_4

    .line 1972
    iget-object v13, p0, Landroid/support/v4/view/FixedViewPager;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v13, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/support/v4/view/FixedViewPager$ItemInfo;

    .line 1974
    .local v2, "ii":Landroid/support/v4/view/FixedViewPager$ItemInfo;
    if-nez v0, :cond_1

    iget v13, v2, Landroid/support/v4/view/FixedViewPager$ItemInfo;->position:I

    add-int/lit8 v14, v5, 0x1

    if-eq v13, v14, :cond_1

    .line 1976
    iget-object v2, p0, Landroid/support/v4/view/FixedViewPager;->mTempItem:Landroid/support/v4/view/FixedViewPager$ItemInfo;

    .line 1977
    add-float v13, v4, v6

    add-float/2addr v13, v8

    iput v13, v2, Landroid/support/v4/view/FixedViewPager$ItemInfo;->offset:F

    .line 1978
    add-int/lit8 v13, v5, 0x1

    iput v13, v2, Landroid/support/v4/view/FixedViewPager$ItemInfo;->position:I

    .line 1979
    iget-object v13, p0, Landroid/support/v4/view/FixedViewPager;->mAdapter:Landroid/support/v4/view/FixedPagerAdapter;

    iget v14, v2, Landroid/support/v4/view/FixedViewPager$ItemInfo;->position:I

    invoke-virtual {v13, v14}, Landroid/support/v4/view/FixedPagerAdapter;->getPageWidth(I)F

    move-result v13

    iput v13, v2, Landroid/support/v4/view/FixedViewPager$ItemInfo;->widthFactor:F

    .line 1980
    add-int/lit8 v1, v1, -0x1

    .line 1982
    :cond_1
    iget v9, v2, Landroid/support/v4/view/FixedViewPager$ItemInfo;->offset:F

    .line 1984
    .local v9, "offset":F
    move v7, v9

    .line 1985
    .local v7, "leftBound":F
    iget v13, v2, Landroid/support/v4/view/FixedViewPager$ItemInfo;->widthFactor:F

    add-float/2addr v13, v9

    add-float v10, v13, v8

    .line 1986
    .local v10, "rightBound":F
    if-nez v0, :cond_2

    cmpl-float v13, v11, v7

    if-ltz v13, :cond_4

    .line 1987
    :cond_2
    cmpg-float v13, v11, v10

    if-ltz v13, :cond_3

    iget-object v13, p0, Landroid/support/v4/view/FixedViewPager;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v13

    add-int/lit8 v13, v13, -0x1

    if-ne v1, v13, :cond_6

    :cond_3
    move-object v3, v2

    .line 2000
    .end local v2    # "ii":Landroid/support/v4/view/FixedViewPager$ItemInfo;
    .end local v3    # "lastItem":Landroid/support/v4/view/FixedViewPager$ItemInfo;
    .end local v7    # "leftBound":F
    .end local v9    # "offset":F
    .end local v10    # "rightBound":F
    :cond_4
    return-object v3

    .end local v0    # "first":Z
    .end local v1    # "i":I
    .end local v4    # "lastOffset":F
    .end local v5    # "lastPos":I
    .end local v6    # "lastWidth":F
    .end local v8    # "marginOffset":F
    .end local v11    # "scrollOffset":F
    :cond_5
    move v11, v8

    .line 1963
    goto :goto_0

    .line 1993
    .restart local v0    # "first":Z
    .restart local v1    # "i":I
    .restart local v2    # "ii":Landroid/support/v4/view/FixedViewPager$ItemInfo;
    .restart local v3    # "lastItem":Landroid/support/v4/view/FixedViewPager$ItemInfo;
    .restart local v4    # "lastOffset":F
    .restart local v5    # "lastPos":I
    .restart local v6    # "lastWidth":F
    .restart local v7    # "leftBound":F
    .restart local v8    # "marginOffset":F
    .restart local v9    # "offset":F
    .restart local v10    # "rightBound":F
    .restart local v11    # "scrollOffset":F
    :cond_6
    const/4 v0, 0x0

    .line 1994
    iget v5, v2, Landroid/support/v4/view/FixedViewPager$ItemInfo;->position:I

    .line 1995
    move v4, v9

    .line 1996
    iget v6, v2, Landroid/support/v4/view/FixedViewPager$ItemInfo;->widthFactor:F

    .line 1997
    move-object v3, v2

    .line 1971
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method private isGutterDrag(FF)Z
    .locals 3
    .param p1, "x"    # F
    .param p2, "dx"    # F

    .prologue
    const/4 v2, 0x0

    .line 1648
    iget v0, p0, Landroid/support/v4/view/FixedViewPager;->mGutterSize:I

    int-to-float v0, v0

    cmpg-float v0, p1, v0

    if-gez v0, :cond_0

    cmpl-float v0, p2, v2

    if-gtz v0, :cond_1

    :cond_0
    invoke-virtual {p0}, Landroid/support/v4/view/FixedViewPager;->getWidth()I

    move-result v0

    iget v1, p0, Landroid/support/v4/view/FixedViewPager;->mGutterSize:I

    sub-int/2addr v0, v1

    int-to-float v0, v0

    cmpl-float v0, p1, v0

    if-lez v0, :cond_2

    cmpg-float v0, p2, v2

    if-gez v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private onSecondaryPointerUp(Landroid/view/MotionEvent;)V
    .locals 4
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 2260
    invoke-static {p1}, Landroid/support/v4/view/MotionEventCompat;->getActionIndex(Landroid/view/MotionEvent;)I

    move-result v2

    .line 2261
    .local v2, "pointerIndex":I
    invoke-static {p1, v2}, Landroid/support/v4/view/MotionEventCompat;->getPointerId(Landroid/view/MotionEvent;I)I

    move-result v1

    .line 2262
    .local v1, "pointerId":I
    iget v3, p0, Landroid/support/v4/view/FixedViewPager;->mActivePointerId:I

    if-ne v1, v3, :cond_0

    .line 2265
    if-nez v2, :cond_1

    const/4 v0, 0x1

    .line 2266
    .local v0, "newPointerIndex":I
    :goto_0
    invoke-static {p1, v0}, Landroid/support/v4/view/MotionEventCompat;->getX(Landroid/view/MotionEvent;I)F

    move-result v3

    iput v3, p0, Landroid/support/v4/view/FixedViewPager;->mLastMotionX:F

    .line 2267
    invoke-static {p1, v0}, Landroid/support/v4/view/MotionEventCompat;->getPointerId(Landroid/view/MotionEvent;I)I

    move-result v3

    iput v3, p0, Landroid/support/v4/view/FixedViewPager;->mActivePointerId:I

    .line 2268
    iget-object v3, p0, Landroid/support/v4/view/FixedViewPager;->mVelocityTracker:Landroid/view/VelocityTracker;

    if-eqz v3, :cond_0

    .line 2269
    iget-object v3, p0, Landroid/support/v4/view/FixedViewPager;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v3}, Landroid/view/VelocityTracker;->clear()V

    .line 2272
    .end local v0    # "newPointerIndex":I
    :cond_0
    return-void

    .line 2265
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private pageScrolled(I)Z
    .locals 10
    .param p1, "xpos"    # I

    .prologue
    const/4 v7, 0x0

    .line 1530
    iget-object v8, p0, Landroid/support/v4/view/FixedViewPager;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-nez v8, :cond_0

    .line 1531
    iput-boolean v7, p0, Landroid/support/v4/view/FixedViewPager;->mCalledSuper:Z

    .line 1532
    const/4 v8, 0x0

    invoke-virtual {p0, v7, v8, v7}, Landroid/support/v4/view/FixedViewPager;->onPageScrolled(IFI)V

    .line 1533
    iget-boolean v8, p0, Landroid/support/v4/view/FixedViewPager;->mCalledSuper:Z

    if-nez v8, :cond_2

    .line 1534
    new-instance v7, Ljava/lang/IllegalStateException;

    const-string v8, "onPageScrolled did not call superclass implementation"

    invoke-direct {v7, v8}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 1539
    :cond_0
    invoke-direct {p0}, Landroid/support/v4/view/FixedViewPager;->infoForCurrentScrollPosition()Landroid/support/v4/view/FixedViewPager$ItemInfo;

    move-result-object v1

    .line 1540
    .local v1, "ii":Landroid/support/v4/view/FixedViewPager$ItemInfo;
    invoke-virtual {p0}, Landroid/support/v4/view/FixedViewPager;->getWidth()I

    move-result v5

    .line 1541
    .local v5, "width":I
    iget v8, p0, Landroid/support/v4/view/FixedViewPager;->mPageMargin:I

    add-int v6, v5, v8

    .line 1542
    .local v6, "widthWithMargin":I
    iget v8, p0, Landroid/support/v4/view/FixedViewPager;->mPageMargin:I

    int-to-float v8, v8

    int-to-float v9, v5

    div-float v2, v8, v9

    .line 1543
    .local v2, "marginOffset":F
    iget v0, v1, Landroid/support/v4/view/FixedViewPager$ItemInfo;->position:I

    .line 1544
    .local v0, "currentPage":I
    int-to-float v8, p1

    int-to-float v9, v5

    div-float/2addr v8, v9

    iget v9, v1, Landroid/support/v4/view/FixedViewPager$ItemInfo;->offset:F

    sub-float/2addr v8, v9

    iget v9, v1, Landroid/support/v4/view/FixedViewPager$ItemInfo;->widthFactor:F

    add-float/2addr v9, v2

    div-float v4, v8, v9

    .line 1546
    .local v4, "pageOffset":F
    int-to-float v8, v6

    mul-float/2addr v8, v4

    float-to-int v3, v8

    .line 1548
    .local v3, "offsetPixels":I
    iput-boolean v7, p0, Landroid/support/v4/view/FixedViewPager;->mCalledSuper:Z

    .line 1549
    invoke-virtual {p0, v0, v4, v3}, Landroid/support/v4/view/FixedViewPager;->onPageScrolled(IFI)V

    .line 1550
    iget-boolean v7, p0, Landroid/support/v4/view/FixedViewPager;->mCalledSuper:Z

    if-nez v7, :cond_1

    .line 1551
    new-instance v7, Ljava/lang/IllegalStateException;

    const-string v8, "onPageScrolled did not call superclass implementation"

    invoke-direct {v7, v8}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 1554
    :cond_1
    const/4 v7, 0x1

    .end local v0    # "currentPage":I
    .end local v1    # "ii":Landroid/support/v4/view/FixedViewPager$ItemInfo;
    .end local v2    # "marginOffset":F
    .end local v3    # "offsetPixels":I
    .end local v4    # "pageOffset":F
    .end local v5    # "width":I
    .end local v6    # "widthWithMargin":I
    :cond_2
    return v7
.end method

.method private performDrag(F)Z
    .locals 17
    .param p1, "x"    # F

    .prologue
    .line 1911
    const/4 v7, 0x0

    .line 1913
    .local v7, "needsInvalidate":Z
    move-object/from16 v0, p0

    iget v14, v0, Landroid/support/v4/view/FixedViewPager;->mLastMotionX:F

    sub-float v2, v14, p1

    .line 1914
    .local v2, "deltaX":F
    move/from16 v0, p1

    move-object/from16 v1, p0

    iput v0, v1, Landroid/support/v4/view/FixedViewPager;->mLastMotionX:F

    .line 1916
    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/view/FixedViewPager;->getScrollX()I

    move-result v14

    int-to-float v8, v14

    .line 1917
    .local v8, "oldScrollX":F
    add-float v12, v8, v2

    .line 1918
    .local v12, "scrollX":F
    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/view/FixedViewPager;->getWidth()I

    move-result v13

    .line 1920
    .local v13, "width":I
    int-to-float v14, v13

    move-object/from16 v0, p0

    iget v15, v0, Landroid/support/v4/view/FixedViewPager;->mFirstOffset:F

    mul-float v6, v14, v15

    .line 1921
    .local v6, "leftBound":F
    int-to-float v14, v13

    move-object/from16 v0, p0

    iget v15, v0, Landroid/support/v4/view/FixedViewPager;->mLastOffset:F

    mul-float v11, v14, v15

    .line 1922
    .local v11, "rightBound":F
    const/4 v5, 0x1

    .line 1923
    .local v5, "leftAbsolute":Z
    const/4 v10, 0x1

    .line 1925
    .local v10, "rightAbsolute":Z
    move-object/from16 v0, p0

    iget-object v14, v0, Landroid/support/v4/view/FixedViewPager;->mItems:Ljava/util/ArrayList;

    const/4 v15, 0x0

    invoke-virtual {v14, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/support/v4/view/FixedViewPager$ItemInfo;

    .line 1926
    .local v3, "firstItem":Landroid/support/v4/view/FixedViewPager$ItemInfo;
    move-object/from16 v0, p0

    iget-object v14, v0, Landroid/support/v4/view/FixedViewPager;->mItems:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v15, v0, Landroid/support/v4/view/FixedViewPager;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v15}, Ljava/util/ArrayList;->size()I

    move-result v15

    add-int/lit8 v15, v15, -0x1

    invoke-virtual {v14, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/support/v4/view/FixedViewPager$ItemInfo;

    .line 1927
    .local v4, "lastItem":Landroid/support/v4/view/FixedViewPager$ItemInfo;
    iget v14, v3, Landroid/support/v4/view/FixedViewPager$ItemInfo;->position:I

    if-eqz v14, :cond_0

    .line 1928
    const/4 v5, 0x0

    .line 1929
    iget v14, v3, Landroid/support/v4/view/FixedViewPager$ItemInfo;->offset:F

    int-to-float v15, v13

    mul-float v6, v14, v15

    .line 1931
    :cond_0
    iget v14, v4, Landroid/support/v4/view/FixedViewPager$ItemInfo;->position:I

    move-object/from16 v0, p0

    iget-object v15, v0, Landroid/support/v4/view/FixedViewPager;->mAdapter:Landroid/support/v4/view/FixedPagerAdapter;

    invoke-virtual {v15}, Landroid/support/v4/view/FixedPagerAdapter;->getCount()I

    move-result v15

    add-int/lit8 v15, v15, -0x1

    if-eq v14, v15, :cond_1

    .line 1932
    const/4 v10, 0x0

    .line 1933
    iget v14, v4, Landroid/support/v4/view/FixedViewPager$ItemInfo;->offset:F

    int-to-float v15, v13

    mul-float v11, v14, v15

    .line 1936
    :cond_1
    cmpg-float v14, v12, v6

    if-gez v14, :cond_4

    .line 1937
    if-eqz v5, :cond_2

    .line 1938
    sub-float v9, v6, v12

    .line 1939
    .local v9, "over":F
    move-object/from16 v0, p0

    iget-object v14, v0, Landroid/support/v4/view/FixedViewPager;->mLeftEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-static {v9}, Ljava/lang/Math;->abs(F)F

    move-result v15

    int-to-float v0, v13

    move/from16 v16, v0

    div-float v15, v15, v16

    invoke-virtual {v14, v15}, Landroid/support/v4/widget/EdgeEffectCompat;->onPull(F)Z

    move-result v7

    .line 1941
    .end local v9    # "over":F
    :cond_2
    move v12, v6

    .line 1950
    :cond_3
    :goto_0
    move-object/from16 v0, p0

    iget v14, v0, Landroid/support/v4/view/FixedViewPager;->mLastMotionX:F

    float-to-int v15, v12

    int-to-float v15, v15

    sub-float v15, v12, v15

    add-float/2addr v14, v15

    move-object/from16 v0, p0

    iput v14, v0, Landroid/support/v4/view/FixedViewPager;->mLastMotionX:F

    .line 1951
    float-to-int v14, v12

    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/view/FixedViewPager;->getScrollY()I

    move-result v15

    move-object/from16 v0, p0

    invoke-virtual {v0, v14, v15}, Landroid/support/v4/view/FixedViewPager;->scrollTo(II)V

    .line 1952
    float-to-int v14, v12

    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Landroid/support/v4/view/FixedViewPager;->pageScrolled(I)Z

    .line 1954
    return v7

    .line 1942
    :cond_4
    cmpl-float v14, v12, v11

    if-lez v14, :cond_3

    .line 1943
    if-eqz v10, :cond_5

    .line 1944
    sub-float v9, v12, v11

    .line 1945
    .restart local v9    # "over":F
    move-object/from16 v0, p0

    iget-object v14, v0, Landroid/support/v4/view/FixedViewPager;->mRightEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-static {v9}, Ljava/lang/Math;->abs(F)F

    move-result v15

    int-to-float v0, v13

    move/from16 v16, v0

    div-float v15, v15, v16

    invoke-virtual {v14, v15}, Landroid/support/v4/widget/EdgeEffectCompat;->onPull(F)Z

    move-result v7

    .line 1947
    .end local v9    # "over":F
    :cond_5
    move v12, v11

    goto :goto_0
.end method

.method private recomputeScrollPosition(IIII)V
    .locals 14
    .param p1, "width"    # I
    .param p2, "oldWidth"    # I
    .param p3, "margin"    # I
    .param p4, "oldMargin"    # I

    .prologue
    .line 1370
    if-lez p2, :cond_1

    iget-object v0, p0, Landroid/support/v4/view/FixedViewPager;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1371
    add-int v12, p1, p3

    .line 1372
    .local v12, "widthWithMargin":I
    add-int v7, p2, p4

    .line 1373
    .local v7, "oldWidthWithMargin":I
    invoke-virtual {p0}, Landroid/support/v4/view/FixedViewPager;->getScrollX()I

    move-result v13

    .line 1374
    .local v13, "xpos":I
    int-to-float v0, v13

    int-to-float v2, v7

    div-float v8, v0, v2

    .line 1375
    .local v8, "pageOffset":F
    int-to-float v0, v12

    mul-float/2addr v0, v8

    float-to-int v1, v0

    .line 1377
    .local v1, "newOffsetPixels":I
    invoke-virtual {p0}, Landroid/support/v4/view/FixedViewPager;->getScrollY()I

    move-result v0

    invoke-virtual {p0, v1, v0}, Landroid/support/v4/view/FixedViewPager;->scrollTo(II)V

    .line 1378
    iget-object v0, p0, Landroid/support/v4/view/FixedViewPager;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->isFinished()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1380
    iget-object v0, p0, Landroid/support/v4/view/FixedViewPager;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->getDuration()I

    move-result v0

    iget-object v2, p0, Landroid/support/v4/view/FixedViewPager;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v2}, Landroid/widget/Scroller;->timePassed()I

    move-result v2

    sub-int v5, v0, v2

    .line 1381
    .local v5, "newDuration":I
    iget v0, p0, Landroid/support/v4/view/FixedViewPager;->mCurItem:I

    invoke-virtual {p0, v0}, Landroid/support/v4/view/FixedViewPager;->infoForPosition(I)Landroid/support/v4/view/FixedViewPager$ItemInfo;

    move-result-object v11

    .line 1382
    .local v11, "targetInfo":Landroid/support/v4/view/FixedViewPager$ItemInfo;
    iget-object v0, p0, Landroid/support/v4/view/FixedViewPager;->mScroller:Landroid/widget/Scroller;

    const/4 v2, 0x0

    iget v3, v11, Landroid/support/v4/view/FixedViewPager$ItemInfo;->offset:F

    int-to-float v4, p1

    mul-float/2addr v3, v4

    float-to-int v3, v3

    const/4 v4, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/widget/Scroller;->startScroll(IIIII)V

    .line 1394
    .end local v1    # "newOffsetPixels":I
    .end local v5    # "newDuration":I
    .end local v7    # "oldWidthWithMargin":I
    .end local v8    # "pageOffset":F
    .end local v11    # "targetInfo":Landroid/support/v4/view/FixedViewPager$ItemInfo;
    .end local v12    # "widthWithMargin":I
    .end local v13    # "xpos":I
    :cond_0
    :goto_0
    return-void

    .line 1386
    :cond_1
    iget v0, p0, Landroid/support/v4/view/FixedViewPager;->mCurItem:I

    invoke-virtual {p0, v0}, Landroid/support/v4/view/FixedViewPager;->infoForPosition(I)Landroid/support/v4/view/FixedViewPager$ItemInfo;

    move-result-object v6

    .line 1387
    .local v6, "ii":Landroid/support/v4/view/FixedViewPager$ItemInfo;
    if-eqz v6, :cond_2

    iget v0, v6, Landroid/support/v4/view/FixedViewPager$ItemInfo;->offset:F

    iget v2, p0, Landroid/support/v4/view/FixedViewPager;->mLastOffset:F

    invoke-static {v0, v2}, Ljava/lang/Math;->min(FF)F

    move-result v9

    .line 1388
    .local v9, "scrollOffset":F
    :goto_1
    int-to-float v0, p1

    mul-float/2addr v0, v9

    float-to-int v10, v0

    .line 1389
    .local v10, "scrollPos":I
    invoke-virtual {p0}, Landroid/support/v4/view/FixedViewPager;->getScrollX()I

    move-result v0

    if-eq v10, v0, :cond_0

    .line 1390
    invoke-direct {p0}, Landroid/support/v4/view/FixedViewPager;->completeScroll()V

    .line 1391
    invoke-virtual {p0}, Landroid/support/v4/view/FixedViewPager;->getScrollY()I

    move-result v0

    invoke-virtual {p0, v10, v0}, Landroid/support/v4/view/FixedViewPager;->scrollTo(II)V

    goto :goto_0

    .line 1387
    .end local v9    # "scrollOffset":F
    .end local v10    # "scrollPos":I
    :cond_2
    const/4 v9, 0x0

    goto :goto_1
.end method

.method private removeNonDecorViews()V
    .locals 4

    .prologue
    .line 397
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p0}, Landroid/support/v4/view/FixedViewPager;->getChildCount()I

    move-result v3

    if-ge v1, v3, :cond_1

    .line 398
    invoke-virtual {p0, v1}, Landroid/support/v4/view/FixedViewPager;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 399
    .local v0, "child":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/support/v4/view/FixedViewPager$LayoutParams;

    .line 400
    .local v2, "lp":Landroid/support/v4/view/FixedViewPager$LayoutParams;
    iget-boolean v3, v2, Landroid/support/v4/view/FixedViewPager$LayoutParams;->isDecor:Z

    if-nez v3, :cond_0

    .line 401
    invoke-virtual {p0, v1}, Landroid/support/v4/view/FixedViewPager;->removeViewAt(I)V

    .line 402
    add-int/lit8 v1, v1, -0x1

    .line 397
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 405
    .end local v0    # "child":Landroid/view/View;
    .end local v2    # "lp":Landroid/support/v4/view/FixedViewPager$LayoutParams;
    :cond_1
    return-void
.end method

.method private setScrollState(I)V
    .locals 1
    .param p1, "newState"    # I

    .prologue
    .line 339
    iget v0, p0, Landroid/support/v4/view/FixedViewPager;->mScrollState:I

    if-ne v0, p1, :cond_1

    .line 347
    :cond_0
    :goto_0
    return-void

    .line 343
    :cond_1
    iput p1, p0, Landroid/support/v4/view/FixedViewPager;->mScrollState:I

    .line 344
    iget-object v0, p0, Landroid/support/v4/view/FixedViewPager;->mOnPageChangeListener:Landroid/support/v4/view/FixedViewPager$OnPageChangeListener;

    if-eqz v0, :cond_0

    .line 345
    iget-object v0, p0, Landroid/support/v4/view/FixedViewPager;->mOnPageChangeListener:Landroid/support/v4/view/FixedViewPager$OnPageChangeListener;

    invoke-interface {v0, p1}, Landroid/support/v4/view/FixedViewPager$OnPageChangeListener;->onPageScrollStateChanged(I)V

    goto :goto_0
.end method

.method private setScrollingCacheEnabled(Z)V
    .locals 1
    .param p1, "enabled"    # Z

    .prologue
    .line 2285
    iget-boolean v0, p0, Landroid/support/v4/view/FixedViewPager;->mScrollingCacheEnabled:Z

    if-eq v0, p1, :cond_0

    .line 2286
    iput-boolean p1, p0, Landroid/support/v4/view/FixedViewPager;->mScrollingCacheEnabled:Z

    .line 2297
    :cond_0
    return-void
.end method


# virtual methods
.method public addFocusables(Ljava/util/ArrayList;II)V
    .locals 7
    .param p2, "direction"    # I
    .param p3, "focusableMode"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;II)V"
        }
    .end annotation

    .prologue
    .line 2463
    .local p1, "views":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/View;>;"
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 2465
    .local v2, "focusableCount":I
    invoke-virtual {p0}, Landroid/support/v4/view/FixedViewPager;->getDescendantFocusability()I

    move-result v1

    .line 2467
    .local v1, "descendantFocusability":I
    const/high16 v5, 0x60000

    if-eq v1, v5, :cond_1

    .line 2468
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-virtual {p0}, Landroid/support/v4/view/FixedViewPager;->getChildCount()I

    move-result v5

    if-ge v3, v5, :cond_1

    .line 2469
    invoke-virtual {p0, v3}, Landroid/support/v4/view/FixedViewPager;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 2470
    .local v0, "child":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v5

    if-nez v5, :cond_0

    .line 2471
    invoke-virtual {p0, v0}, Landroid/support/v4/view/FixedViewPager;->infoForChild(Landroid/view/View;)Landroid/support/v4/view/FixedViewPager$ItemInfo;

    move-result-object v4

    .line 2472
    .local v4, "ii":Landroid/support/v4/view/FixedViewPager$ItemInfo;
    if-eqz v4, :cond_0

    iget v5, v4, Landroid/support/v4/view/FixedViewPager$ItemInfo;->position:I

    iget v6, p0, Landroid/support/v4/view/FixedViewPager;->mCurItem:I

    if-ne v5, v6, :cond_0

    .line 2473
    invoke-virtual {v0, p1, p2, p3}, Landroid/view/View;->addFocusables(Ljava/util/ArrayList;II)V

    .line 2468
    .end local v4    # "ii":Landroid/support/v4/view/FixedViewPager$ItemInfo;
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 2483
    .end local v0    # "child":Landroid/view/View;
    .end local v3    # "i":I
    :cond_1
    const/high16 v5, 0x40000

    if-ne v1, v5, :cond_2

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ne v2, v5, :cond_3

    .line 2489
    :cond_2
    invoke-virtual {p0}, Landroid/support/v4/view/FixedViewPager;->isFocusable()Z

    move-result v5

    if-nez v5, :cond_4

    .line 2500
    :cond_3
    :goto_1
    return-void

    .line 2492
    :cond_4
    and-int/lit8 v5, p3, 0x1

    const/4 v6, 0x1

    if-ne v5, v6, :cond_5

    invoke-virtual {p0}, Landroid/support/v4/view/FixedViewPager;->isInTouchMode()Z

    move-result v5

    if-eqz v5, :cond_5

    invoke-virtual {p0}, Landroid/support/v4/view/FixedViewPager;->isFocusableInTouchMode()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 2496
    :cond_5
    if-eqz p1, :cond_3

    .line 2497
    invoke-virtual {p1, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method addNewItem(II)Landroid/support/v4/view/FixedViewPager$ItemInfo;
    .locals 2
    .param p1, "position"    # I
    .param p2, "index"    # I

    .prologue
    .line 697
    new-instance v0, Landroid/support/v4/view/FixedViewPager$ItemInfo;

    invoke-direct {v0}, Landroid/support/v4/view/FixedViewPager$ItemInfo;-><init>()V

    .line 698
    .local v0, "ii":Landroid/support/v4/view/FixedViewPager$ItemInfo;
    iput p1, v0, Landroid/support/v4/view/FixedViewPager$ItemInfo;->position:I

    .line 699
    iget-object v1, p0, Landroid/support/v4/view/FixedViewPager;->mAdapter:Landroid/support/v4/view/FixedPagerAdapter;

    invoke-virtual {v1, p0, p1}, Landroid/support/v4/view/FixedPagerAdapter;->instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;

    move-result-object v1

    iput-object v1, v0, Landroid/support/v4/view/FixedViewPager$ItemInfo;->object:Ljava/lang/Object;

    .line 700
    iget-object v1, p0, Landroid/support/v4/view/FixedViewPager;->mAdapter:Landroid/support/v4/view/FixedPagerAdapter;

    invoke-virtual {v1, p1}, Landroid/support/v4/view/FixedPagerAdapter;->getPageWidth(I)F

    move-result v1

    iput v1, v0, Landroid/support/v4/view/FixedViewPager$ItemInfo;->widthFactor:F

    .line 701
    if-ltz p2, :cond_0

    iget-object v1, p0, Landroid/support/v4/view/FixedViewPager;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lt p2, v1, :cond_1

    .line 702
    :cond_0
    iget-object v1, p0, Landroid/support/v4/view/FixedViewPager;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 706
    :goto_0
    return-object v0

    .line 704
    :cond_1
    iget-object v1, p0, Landroid/support/v4/view/FixedViewPager;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v1, p2, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    goto :goto_0
.end method

.method public addTouchables(Ljava/util/ArrayList;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2510
    .local p1, "views":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/View;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p0}, Landroid/support/v4/view/FixedViewPager;->getChildCount()I

    move-result v3

    if-ge v1, v3, :cond_1

    .line 2511
    invoke-virtual {p0, v1}, Landroid/support/v4/view/FixedViewPager;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 2512
    .local v0, "child":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v3

    if-nez v3, :cond_0

    .line 2513
    invoke-virtual {p0, v0}, Landroid/support/v4/view/FixedViewPager;->infoForChild(Landroid/view/View;)Landroid/support/v4/view/FixedViewPager$ItemInfo;

    move-result-object v2

    .line 2514
    .local v2, "ii":Landroid/support/v4/view/FixedViewPager$ItemInfo;
    if-eqz v2, :cond_0

    iget v3, v2, Landroid/support/v4/view/FixedViewPager$ItemInfo;->position:I

    iget v4, p0, Landroid/support/v4/view/FixedViewPager;->mCurItem:I

    if-ne v3, v4, :cond_0

    .line 2515
    invoke-virtual {v0, p1}, Landroid/view/View;->addTouchables(Ljava/util/ArrayList;)V

    .line 2510
    .end local v2    # "ii":Landroid/support/v4/view/FixedViewPager$ItemInfo;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2519
    .end local v0    # "child":Landroid/view/View;
    :cond_1
    return-void
.end method

.method public addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V
    .locals 3
    .param p1, "child"    # Landroid/view/View;
    .param p2, "index"    # I
    .param p3, "params"    # Landroid/view/ViewGroup$LayoutParams;

    .prologue
    .line 1203
    invoke-virtual {p0, p3}, Landroid/support/v4/view/FixedViewPager;->checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1204
    invoke-virtual {p0, p3}, Landroid/support/v4/view/FixedViewPager;->generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;

    move-result-object p3

    :cond_0
    move-object v0, p3

    .line 1206
    check-cast v0, Landroid/support/v4/view/FixedViewPager$LayoutParams;

    .line 1207
    .local v0, "lp":Landroid/support/v4/view/FixedViewPager$LayoutParams;
    iget-boolean v1, v0, Landroid/support/v4/view/FixedViewPager$LayoutParams;->isDecor:Z

    instance-of v2, p1, Landroid/support/v4/view/FixedViewPager$Decor;

    or-int/2addr v1, v2

    iput-boolean v1, v0, Landroid/support/v4/view/FixedViewPager$LayoutParams;->isDecor:Z

    .line 1208
    iget-boolean v1, p0, Landroid/support/v4/view/FixedViewPager;->mInLayout:Z

    if-eqz v1, :cond_2

    .line 1209
    if-eqz v0, :cond_1

    iget-boolean v1, v0, Landroid/support/v4/view/FixedViewPager$LayoutParams;->isDecor:Z

    if-eqz v1, :cond_1

    .line 1210
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Cannot add pager decor view during layout"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1212
    :cond_1
    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/support/v4/view/FixedViewPager$LayoutParams;->needsMeasure:Z

    .line 1213
    invoke-virtual {p0, p1, p2, p3}, Landroid/support/v4/view/FixedViewPager;->addViewInLayout(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)Z

    .line 1225
    :goto_0
    return-void

    .line 1215
    :cond_2
    invoke-super {p0, p1, p2, p3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method public arrowScroll(I)Z
    .locals 8
    .param p1, "direction"    # I

    .prologue
    const/16 v7, 0x42

    const/16 v6, 0x11

    .line 2374
    invoke-virtual {p0}, Landroid/support/v4/view/FixedViewPager;->findFocus()Landroid/view/View;

    move-result-object v1

    .line 2375
    .local v1, "currentFocused":Landroid/view/View;
    if-ne v1, p0, :cond_0

    const/4 v1, 0x0

    .line 2377
    :cond_0
    const/4 v2, 0x0

    .line 2379
    .local v2, "handled":Z
    invoke-static {}, Landroid/view/FocusFinder;->getInstance()Landroid/view/FocusFinder;

    move-result-object v5

    invoke-virtual {v5, p0, v1, p1}, Landroid/view/FocusFinder;->findNextFocus(Landroid/view/ViewGroup;Landroid/view/View;I)Landroid/view/View;

    move-result-object v3

    .line 2381
    .local v3, "nextFocused":Landroid/view/View;
    if-eqz v3, :cond_6

    if-eq v3, v1, :cond_6

    .line 2382
    if-ne p1, v6, :cond_4

    .line 2385
    iget-object v5, p0, Landroid/support/v4/view/FixedViewPager;->mTempRect:Landroid/graphics/Rect;

    invoke-direct {p0, v5, v3}, Landroid/support/v4/view/FixedViewPager;->getChildRectInPagerCoordinates(Landroid/graphics/Rect;Landroid/view/View;)Landroid/graphics/Rect;

    move-result-object v5

    iget v4, v5, Landroid/graphics/Rect;->left:I

    .line 2386
    .local v4, "nextLeft":I
    iget-object v5, p0, Landroid/support/v4/view/FixedViewPager;->mTempRect:Landroid/graphics/Rect;

    invoke-direct {p0, v5, v1}, Landroid/support/v4/view/FixedViewPager;->getChildRectInPagerCoordinates(Landroid/graphics/Rect;Landroid/view/View;)Landroid/graphics/Rect;

    move-result-object v5

    iget v0, v5, Landroid/graphics/Rect;->left:I

    .line 2387
    .local v0, "currLeft":I
    if-eqz v1, :cond_3

    if-lt v4, v0, :cond_3

    .line 2388
    invoke-virtual {p0}, Landroid/support/v4/view/FixedViewPager;->pageLeft()Z

    move-result v2

    .line 2410
    .end local v0    # "currLeft":I
    .end local v4    # "nextLeft":I
    :cond_1
    :goto_0
    if-eqz v2, :cond_2

    .line 2411
    invoke-static {p1}, Landroid/view/SoundEffectConstants;->getContantForFocusDirection(I)I

    move-result v5

    invoke-virtual {p0, v5}, Landroid/support/v4/view/FixedViewPager;->playSoundEffect(I)V

    .line 2413
    :cond_2
    return v2

    .line 2390
    .restart local v0    # "currLeft":I
    .restart local v4    # "nextLeft":I
    :cond_3
    invoke-virtual {v3}, Landroid/view/View;->requestFocus()Z

    move-result v2

    goto :goto_0

    .line 2392
    .end local v0    # "currLeft":I
    .end local v4    # "nextLeft":I
    :cond_4
    if-ne p1, v7, :cond_1

    .line 2395
    iget-object v5, p0, Landroid/support/v4/view/FixedViewPager;->mTempRect:Landroid/graphics/Rect;

    invoke-direct {p0, v5, v3}, Landroid/support/v4/view/FixedViewPager;->getChildRectInPagerCoordinates(Landroid/graphics/Rect;Landroid/view/View;)Landroid/graphics/Rect;

    move-result-object v5

    iget v4, v5, Landroid/graphics/Rect;->left:I

    .line 2396
    .restart local v4    # "nextLeft":I
    iget-object v5, p0, Landroid/support/v4/view/FixedViewPager;->mTempRect:Landroid/graphics/Rect;

    invoke-direct {p0, v5, v1}, Landroid/support/v4/view/FixedViewPager;->getChildRectInPagerCoordinates(Landroid/graphics/Rect;Landroid/view/View;)Landroid/graphics/Rect;

    move-result-object v5

    iget v0, v5, Landroid/graphics/Rect;->left:I

    .line 2397
    .restart local v0    # "currLeft":I
    if-eqz v1, :cond_5

    if-gt v4, v0, :cond_5

    .line 2398
    invoke-virtual {p0}, Landroid/support/v4/view/FixedViewPager;->pageRight()Z

    move-result v2

    goto :goto_0

    .line 2400
    :cond_5
    invoke-virtual {v3}, Landroid/view/View;->requestFocus()Z

    move-result v2

    goto :goto_0

    .line 2403
    .end local v0    # "currLeft":I
    .end local v4    # "nextLeft":I
    :cond_6
    if-eq p1, v6, :cond_7

    const/4 v5, 0x1

    if-ne p1, v5, :cond_8

    .line 2405
    :cond_7
    invoke-virtual {p0}, Landroid/support/v4/view/FixedViewPager;->pageLeft()Z

    move-result v2

    goto :goto_0

    .line 2406
    :cond_8
    if-eq p1, v7, :cond_9

    const/4 v5, 0x2

    if-ne p1, v5, :cond_1

    .line 2408
    :cond_9
    invoke-virtual {p0}, Landroid/support/v4/view/FixedViewPager;->pageRight()Z

    move-result v2

    goto :goto_0
.end method

.method public beginFakeDrag()Z
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 2149
    iget-boolean v2, p0, Landroid/support/v4/view/FixedViewPager;->mIsBeingDragged:Z

    if-eqz v2, :cond_0

    .line 2165
    :goto_0
    return v4

    .line 2152
    :cond_0
    iput-boolean v9, p0, Landroid/support/v4/view/FixedViewPager;->mFakeDragging:Z

    .line 2153
    invoke-direct {p0, v9}, Landroid/support/v4/view/FixedViewPager;->setScrollState(I)V

    .line 2154
    iput v5, p0, Landroid/support/v4/view/FixedViewPager;->mLastMotionX:F

    iput v5, p0, Landroid/support/v4/view/FixedViewPager;->mInitialMotionX:F

    .line 2155
    iget-object v2, p0, Landroid/support/v4/view/FixedViewPager;->mVelocityTracker:Landroid/view/VelocityTracker;

    if-nez v2, :cond_1

    .line 2156
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v2

    iput-object v2, p0, Landroid/support/v4/view/FixedViewPager;->mVelocityTracker:Landroid/view/VelocityTracker;

    .line 2160
    :goto_1
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .local v0, "time":J
    move-wide v2, v0

    move v6, v5

    move v7, v4

    .line 2161
    invoke-static/range {v0 .. v7}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v8

    .line 2162
    .local v8, "ev":Landroid/view/MotionEvent;
    iget-object v2, p0, Landroid/support/v4/view/FixedViewPager;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v2, v8}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 2163
    invoke-virtual {v8}, Landroid/view/MotionEvent;->recycle()V

    .line 2164
    iput-wide v0, p0, Landroid/support/v4/view/FixedViewPager;->mFakeDragBeginTime:J

    move v4, v9

    .line 2165
    goto :goto_0

    .line 2158
    .end local v0    # "time":J
    .end local v8    # "ev":Landroid/view/MotionEvent;
    :cond_1
    iget-object v2, p0, Landroid/support/v4/view/FixedViewPager;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v2}, Landroid/view/VelocityTracker;->clear()V

    goto :goto_1
.end method

.method protected canScroll(Landroid/view/View;ZIII)Z
    .locals 11
    .param p1, "v"    # Landroid/view/View;
    .param p2, "checkV"    # Z
    .param p3, "dx"    # I
    .param p4, "x"    # I
    .param p5, "y"    # I

    .prologue
    .line 2311
    instance-of v0, p1, Landroid/view/ViewGroup;

    if-eqz v0, :cond_1

    move-object v7, p1

    .line 2312
    check-cast v7, Landroid/view/ViewGroup;

    .line 2313
    .local v7, "group":Landroid/view/ViewGroup;
    invoke-virtual {p1}, Landroid/view/View;->getScrollX()I

    move-result v9

    .line 2314
    .local v9, "scrollX":I
    invoke-virtual {p1}, Landroid/view/View;->getScrollY()I

    move-result v10

    .line 2315
    .local v10, "scrollY":I
    invoke-virtual {v7}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v6

    .line 2317
    .local v6, "count":I
    add-int/lit8 v8, v6, -0x1

    .local v8, "i":I
    :goto_0
    if-ltz v8, :cond_1

    .line 2320
    invoke-virtual {v7, v8}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 2321
    .local v1, "child":Landroid/view/View;
    add-int v0, p4, v9

    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v2

    if-lt v0, v2, :cond_0

    add-int v0, p4, v9

    invoke-virtual {v1}, Landroid/view/View;->getRight()I

    move-result v2

    if-ge v0, v2, :cond_0

    add-int v0, p5, v10

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v2

    if-lt v0, v2, :cond_0

    add-int v0, p5, v10

    invoke-virtual {v1}, Landroid/view/View;->getBottom()I

    move-result v2

    if-ge v0, v2, :cond_0

    const/4 v2, 0x1

    add-int v0, p4, v9

    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v3

    sub-int v4, v0, v3

    add-int v0, p5, v10

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v3

    sub-int v5, v0, v3

    move-object v0, p0

    move v3, p3

    invoke-virtual/range {v0 .. v5}, Landroid/support/v4/view/FixedViewPager;->canScroll(Landroid/view/View;ZIII)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2325
    const/4 v0, 0x1

    .line 2330
    .end local v1    # "child":Landroid/view/View;
    .end local v6    # "count":I
    .end local v7    # "group":Landroid/view/ViewGroup;
    .end local v8    # "i":I
    .end local v9    # "scrollX":I
    .end local v10    # "scrollY":I
    :goto_1
    return v0

    .line 2317
    .restart local v1    # "child":Landroid/view/View;
    .restart local v6    # "count":I
    .restart local v7    # "group":Landroid/view/ViewGroup;
    .restart local v8    # "i":I
    .restart local v9    # "scrollX":I
    .restart local v10    # "scrollY":I
    :cond_0
    add-int/lit8 v8, v8, -0x1

    goto :goto_0

    .line 2330
    .end local v1    # "child":Landroid/view/View;
    .end local v6    # "count":I
    .end local v7    # "group":Landroid/view/ViewGroup;
    .end local v8    # "i":I
    .end local v9    # "scrollX":I
    .end local v10    # "scrollY":I
    :cond_1
    if-eqz p2, :cond_2

    neg-int v0, p3

    invoke-static {p1, v0}, Landroid/support/v4/view/ViewCompat;->canScrollHorizontally(Landroid/view/View;I)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method protected checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .locals 1
    .param p1, "p"    # Landroid/view/ViewGroup$LayoutParams;

    .prologue
    .line 2588
    instance-of v0, p1, Landroid/support/v4/view/FixedViewPager$LayoutParams;

    if-eqz v0, :cond_0

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public computeScroll()V
    .locals 5

    .prologue
    .line 1506
    iget-object v4, p0, Landroid/support/v4/view/FixedViewPager;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v4}, Landroid/widget/Scroller;->isFinished()Z

    move-result v4

    if-nez v4, :cond_2

    iget-object v4, p0, Landroid/support/v4/view/FixedViewPager;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v4}, Landroid/widget/Scroller;->computeScrollOffset()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1507
    invoke-virtual {p0}, Landroid/support/v4/view/FixedViewPager;->getScrollX()I

    move-result v0

    .line 1508
    .local v0, "oldX":I
    invoke-virtual {p0}, Landroid/support/v4/view/FixedViewPager;->getScrollY()I

    move-result v1

    .line 1509
    .local v1, "oldY":I
    iget-object v4, p0, Landroid/support/v4/view/FixedViewPager;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v4}, Landroid/widget/Scroller;->getCurrX()I

    move-result v2

    .line 1510
    .local v2, "x":I
    iget-object v4, p0, Landroid/support/v4/view/FixedViewPager;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v4}, Landroid/widget/Scroller;->getCurrY()I

    move-result v3

    .line 1512
    .local v3, "y":I
    if-ne v0, v2, :cond_0

    if-eq v1, v3, :cond_1

    .line 1513
    :cond_0
    invoke-virtual {p0, v2, v3}, Landroid/support/v4/view/FixedViewPager;->scrollTo(II)V

    .line 1514
    invoke-direct {p0, v2}, Landroid/support/v4/view/FixedViewPager;->pageScrolled(I)Z

    move-result v4

    if-nez v4, :cond_1

    .line 1515
    iget-object v4, p0, Landroid/support/v4/view/FixedViewPager;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v4}, Landroid/widget/Scroller;->abortAnimation()V

    .line 1516
    const/4 v4, 0x0

    invoke-virtual {p0, v4, v3}, Landroid/support/v4/view/FixedViewPager;->scrollTo(II)V

    .line 1521
    :cond_1
    invoke-static {p0}, Landroid/support/v4/view/ViewCompat;->postInvalidateOnAnimation(Landroid/view/View;)V

    .line 1527
    .end local v0    # "oldX":I
    .end local v1    # "oldY":I
    .end local v2    # "x":I
    .end local v3    # "y":I
    :goto_0
    return-void

    .line 1526
    :cond_2
    invoke-direct {p0}, Landroid/support/v4/view/FixedViewPager;->completeScroll()V

    goto :goto_0
.end method

.method dataSetChanged()V
    .locals 30

    .prologue
    .line 737
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/support/v4/view/FixedViewPager;->mAdapter:Landroid/support/v4/view/FixedPagerAdapter;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Landroid/support/v4/view/FixedPagerAdapter;->getCount()I

    move-result v5

    .line 738
    .local v5, "adapterCount":I
    move-object/from16 v0, p0

    iget v0, v0, Landroid/support/v4/view/FixedViewPager;->mPreviouseDataCount:I

    move/from16 v25, v0

    .line 739
    .local v25, "prevAdapterCount":I
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/support/v4/view/FixedViewPager;->mAdapter:Landroid/support/v4/view/FixedPagerAdapter;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Landroid/support/v4/view/FixedPagerAdapter;->getCount()I

    move-result v27

    move/from16 v0, v27

    move-object/from16 v1, p0

    iput v0, v1, Landroid/support/v4/view/FixedViewPager;->mPreviouseDataCount:I

    .line 741
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/support/v4/view/FixedViewPager;->mItems:Ljava/util/ArrayList;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Ljava/util/ArrayList;->size()I

    move-result v27

    move-object/from16 v0, p0

    iget v0, v0, Landroid/support/v4/view/FixedViewPager;->mOffscreenPageLimit:I

    move/from16 v28, v0

    mul-int/lit8 v28, v28, 0x2

    add-int/lit8 v28, v28, 0x1

    move/from16 v0, v27

    move/from16 v1, v28

    if-ge v0, v1, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/support/v4/view/FixedViewPager;->mItems:Ljava/util/ArrayList;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Ljava/util/ArrayList;->size()I

    move-result v27

    move/from16 v0, v27

    if-ge v0, v5, :cond_1

    const/16 v16, 0x1

    .line 743
    .local v16, "needPopulate":Z
    :goto_0
    move-object/from16 v0, p0

    iget v0, v0, Landroid/support/v4/view/FixedViewPager;->mCurItem:I

    move/from16 v17, v0

    .line 744
    .local v17, "newCurrItem":I
    const/4 v13, 0x0

    .line 746
    .local v13, "isUpdating":Z
    new-instance v18, Ljava/util/ArrayList;

    invoke-direct/range {v18 .. v18}, Ljava/util/ArrayList;-><init>()V

    .line 747
    .local v18, "newItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/support/v4/view/FixedViewPager$ItemInfo;>;"
    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    .line 748
    .local v15, "moveInfos":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/support/v4/view/FixedViewPager$MoveInfo;>;"
    new-instance v20, Ljava/util/HashSet;

    invoke-direct/range {v20 .. v20}, Ljava/util/HashSet;-><init>()V

    .line 749
    .local v20, "newPositions":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/Integer;>;"
    new-instance v24, Ljava/util/HashSet;

    invoke-direct/range {v24 .. v24}, Ljava/util/HashSet;-><init>()V

    .line 751
    .local v24, "positionsToAdd":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/Integer;>;"
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/support/v4/view/FixedViewPager;->mItems:Ljava/util/ArrayList;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Ljava/util/ArrayList;->size()I

    move-result v27

    move/from16 v0, v27

    if-ge v9, v0, :cond_9

    .line 752
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/support/v4/view/FixedViewPager;->mItems:Ljava/util/ArrayList;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/support/v4/view/FixedViewPager$ItemInfo;

    .line 753
    .local v11, "ii":Landroid/support/v4/view/FixedViewPager$ItemInfo;
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/support/v4/view/FixedViewPager;->mAdapter:Landroid/support/v4/view/FixedPagerAdapter;

    move-object/from16 v27, v0

    iget-object v0, v11, Landroid/support/v4/view/FixedViewPager$ItemInfo;->object:Ljava/lang/Object;

    move-object/from16 v28, v0

    invoke-virtual/range {v27 .. v28}, Landroid/support/v4/view/FixedPagerAdapter;->getItemPosition(Ljava/lang/Object;)I

    move-result v19

    .line 756
    .local v19, "newPosition":I
    const/16 v27, -0x1

    move/from16 v0, v19

    move/from16 v1, v27

    if-ne v0, v1, :cond_2

    .line 757
    move-object/from16 v0, v18

    invoke-virtual {v0, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 751
    :cond_0
    :goto_2
    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    .line 741
    .end local v9    # "i":I
    .end local v11    # "ii":Landroid/support/v4/view/FixedViewPager$ItemInfo;
    .end local v13    # "isUpdating":Z
    .end local v15    # "moveInfos":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/support/v4/view/FixedViewPager$MoveInfo;>;"
    .end local v16    # "needPopulate":Z
    .end local v17    # "newCurrItem":I
    .end local v18    # "newItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/support/v4/view/FixedViewPager$ItemInfo;>;"
    .end local v19    # "newPosition":I
    .end local v20    # "newPositions":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/Integer;>;"
    .end local v24    # "positionsToAdd":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/Integer;>;"
    :cond_1
    const/16 v16, 0x0

    goto :goto_0

    .line 762
    .restart local v9    # "i":I
    .restart local v11    # "ii":Landroid/support/v4/view/FixedViewPager$ItemInfo;
    .restart local v13    # "isUpdating":Z
    .restart local v15    # "moveInfos":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/support/v4/view/FixedViewPager$MoveInfo;>;"
    .restart local v16    # "needPopulate":Z
    .restart local v17    # "newCurrItem":I
    .restart local v18    # "newItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/support/v4/view/FixedViewPager$ItemInfo;>;"
    .restart local v19    # "newPosition":I
    .restart local v20    # "newPositions":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/Integer;>;"
    .restart local v24    # "positionsToAdd":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/Integer;>;"
    :cond_2
    const/16 v27, -0x2

    move/from16 v0, v19

    move/from16 v1, v27

    if-ne v0, v1, :cond_4

    .line 763
    if-nez v13, :cond_3

    .line 764
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/support/v4/view/FixedViewPager;->mAdapter:Landroid/support/v4/view/FixedPagerAdapter;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Landroid/support/v4/view/FixedPagerAdapter;->startUpdate(Landroid/view/ViewGroup;)V

    .line 765
    const/4 v13, 0x1

    .line 768
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/support/v4/view/FixedViewPager;->mAdapter:Landroid/support/v4/view/FixedPagerAdapter;

    move-object/from16 v27, v0

    iget v0, v11, Landroid/support/v4/view/FixedViewPager$ItemInfo;->position:I

    move/from16 v28, v0

    iget-object v0, v11, Landroid/support/v4/view/FixedViewPager$ItemInfo;->object:Ljava/lang/Object;

    move-object/from16 v29, v0

    move-object/from16 v0, v27

    move-object/from16 v1, p0

    move/from16 v2, v28

    move-object/from16 v3, v29

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v4/view/FixedPagerAdapter;->destroyItem(Landroid/view/ViewGroup;ILjava/lang/Object;)V

    .line 769
    const/16 v16, 0x1

    .line 771
    move-object/from16 v0, p0

    iget v0, v0, Landroid/support/v4/view/FixedViewPager;->mCurItem:I

    move/from16 v27, v0

    iget v0, v11, Landroid/support/v4/view/FixedViewPager$ItemInfo;->position:I

    move/from16 v28, v0

    move/from16 v0, v27

    move/from16 v1, v28

    if-ne v0, v1, :cond_0

    .line 773
    const/16 v27, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Landroid/support/v4/view/FixedViewPager;->mCurItem:I

    move/from16 v28, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/support/v4/view/FixedViewPager;->mAdapter:Landroid/support/v4/view/FixedPagerAdapter;

    move-object/from16 v29, v0

    invoke-virtual/range {v29 .. v29}, Landroid/support/v4/view/FixedPagerAdapter;->getCount()I

    move-result v29

    add-int/lit8 v29, v29, -0x1

    invoke-static/range {v28 .. v29}, Ljava/lang/Math;->min(II)I

    move-result v28

    invoke-static/range {v27 .. v28}, Ljava/lang/Math;->max(II)I

    move-result v17

    .line 774
    const/16 v16, 0x1

    goto :goto_2

    .line 780
    :cond_4
    iget v0, v11, Landroid/support/v4/view/FixedViewPager$ItemInfo;->position:I

    move/from16 v27, v0

    move/from16 v0, v27

    move/from16 v1, v19

    if-ne v0, v1, :cond_5

    .line 781
    move-object/from16 v0, v18

    invoke-virtual {v0, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 785
    :cond_5
    iget v0, v11, Landroid/support/v4/view/FixedViewPager$ItemInfo;->position:I

    move/from16 v27, v0

    move-object/from16 v0, p0

    iget v0, v0, Landroid/support/v4/view/FixedViewPager;->mCurItem:I

    move/from16 v28, v0

    move/from16 v0, v27

    move/from16 v1, v28

    if-ne v0, v1, :cond_6

    .line 787
    move/from16 v17, v19

    .line 790
    :cond_6
    iget v0, v11, Landroid/support/v4/view/FixedViewPager$ItemInfo;->position:I

    move/from16 v21, v0

    .line 791
    .local v21, "oldPosition":I
    move-object/from16 v0, v18

    invoke-virtual {v0, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 794
    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v27

    move-object/from16 v0, v20

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v27

    if-nez v27, :cond_7

    .line 795
    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    const-string v28, "ADD:"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, v27

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    invoke-static/range {v27 .. v27}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 797
    move/from16 v0, v25

    if-le v0, v5, :cond_8

    .line 799
    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    const-string v28, "ADD:"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, v27

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v27

    const-string v28, " is ignored this is deletion!!"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    invoke-static/range {v27 .. v27}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 808
    :cond_7
    :goto_3
    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v27

    move-object/from16 v0, v24

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 809
    move/from16 v0, v19

    iput v0, v11, Landroid/support/v4/view/FixedViewPager$ItemInfo;->position:I

    .line 810
    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v27

    move-object/from16 v0, v20

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 811
    const/16 v16, 0x1

    .line 813
    new-instance v12, Landroid/support/v4/view/FixedViewPager$MoveInfo;

    iget-object v0, v11, Landroid/support/v4/view/FixedViewPager$ItemInfo;->object:Ljava/lang/Object;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    move/from16 v1, v21

    move/from16 v2, v19

    invoke-direct {v12, v0, v1, v2}, Landroid/support/v4/view/FixedViewPager$MoveInfo;-><init>(Ljava/lang/Object;II)V

    .line 814
    .local v12, "info":Landroid/support/v4/view/FixedViewPager$MoveInfo;
    invoke-virtual {v15, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 803
    .end local v12    # "info":Landroid/support/v4/view/FixedViewPager$MoveInfo;
    :cond_8
    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v27

    move-object/from16 v0, v24

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 818
    .end local v11    # "ii":Landroid/support/v4/view/FixedViewPager$ItemInfo;
    .end local v19    # "newPosition":I
    .end local v21    # "oldPosition":I
    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/support/v4/view/FixedViewPager;->mAdapter:Landroid/support/v4/view/FixedPagerAdapter;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-virtual {v0, v15}, Landroid/support/v4/view/FixedPagerAdapter;->moveItems(Ljava/util/Collection;)V

    .line 821
    move-object/from16 v0, p0

    iget v0, v0, Landroid/support/v4/view/FixedViewPager;->mOffscreenPageLimit:I

    move/from16 v22, v0

    .line 822
    .local v22, "pageLimit":I
    const/16 v27, 0x0

    sub-int v28, v17, v22

    invoke-static/range {v27 .. v28}, Ljava/lang/Math;->max(II)I

    move-result v26

    .line 823
    .local v26, "startPos":I
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/support/v4/view/FixedViewPager;->mAdapter:Landroid/support/v4/view/FixedPagerAdapter;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Landroid/support/v4/view/FixedPagerAdapter;->getCount()I

    move-result v4

    .line 824
    .local v4, "N":I
    add-int/lit8 v27, v4, -0x1

    add-int v28, v17, v22

    invoke-static/range {v27 .. v28}, Ljava/lang/Math;->min(II)I

    move-result v8

    .line 826
    .local v8, "endPos":I
    const/4 v9, 0x0

    :goto_4
    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->size()I

    move-result v27

    move/from16 v0, v27

    if-ge v9, v0, :cond_d

    .line 827
    move-object/from16 v0, v18

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/support/v4/view/FixedViewPager$ItemInfo;

    .line 828
    .restart local v11    # "ii":Landroid/support/v4/view/FixedViewPager$ItemInfo;
    iget v0, v11, Landroid/support/v4/view/FixedViewPager$ItemInfo;->position:I

    move/from16 v23, v0

    .line 830
    .local v23, "position":I
    move/from16 v0, v23

    move/from16 v1, v26

    if-lt v0, v1, :cond_a

    move/from16 v0, v23

    if-le v0, v8, :cond_c

    .line 831
    :cond_a
    if-nez v13, :cond_b

    .line 832
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/support/v4/view/FixedViewPager;->mAdapter:Landroid/support/v4/view/FixedPagerAdapter;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Landroid/support/v4/view/FixedPagerAdapter;->startUpdate(Landroid/view/ViewGroup;)V

    .line 833
    const/4 v13, 0x1

    .line 836
    :cond_b
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/support/v4/view/FixedViewPager;->mAdapter:Landroid/support/v4/view/FixedPagerAdapter;

    move-object/from16 v27, v0

    iget v0, v11, Landroid/support/v4/view/FixedViewPager$ItemInfo;->position:I

    move/from16 v28, v0

    iget-object v0, v11, Landroid/support/v4/view/FixedViewPager$ItemInfo;->object:Ljava/lang/Object;

    move-object/from16 v29, v0

    move-object/from16 v0, v27

    move-object/from16 v1, p0

    move/from16 v2, v28

    move-object/from16 v3, v29

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v4/view/FixedPagerAdapter;->destroyItem(Landroid/view/ViewGroup;ILjava/lang/Object;)V

    .line 837
    move-object/from16 v0, v18

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 838
    add-int/lit8 v9, v9, -0x1

    .line 826
    :cond_c
    add-int/lit8 v9, v9, 0x1

    goto :goto_4

    .line 842
    .end local v11    # "ii":Landroid/support/v4/view/FixedViewPager$ItemInfo;
    .end local v23    # "position":I
    :cond_d
    if-eqz v13, :cond_e

    .line 843
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/support/v4/view/FixedViewPager;->mAdapter:Landroid/support/v4/view/FixedPagerAdapter;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Landroid/support/v4/view/FixedPagerAdapter;->finishUpdate(Landroid/view/ViewGroup;)V

    .line 847
    :cond_e
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/support/v4/view/FixedViewPager;->mItems:Ljava/util/ArrayList;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Ljava/util/ArrayList;->clear()V

    .line 848
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/support/v4/view/FixedViewPager;->mItems:Ljava/util/ArrayList;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 851
    invoke-virtual/range {v24 .. v24}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v10

    .local v10, "i$":Ljava/util/Iterator;
    :goto_5
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v27

    if-eqz v27, :cond_f

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v27

    check-cast v27, Ljava/lang/Integer;

    invoke-virtual/range {v27 .. v27}, Ljava/lang/Integer;->intValue()I

    move-result v23

    .line 852
    .restart local v23    # "position":I
    const/16 v27, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v23

    move/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/view/FixedViewPager;->addNewItem(II)Landroid/support/v4/view/FixedViewPager$ItemInfo;

    .line 853
    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    const-string v28, "addNewItem itemIndex:"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, v27

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    invoke-static/range {v27 .. v27}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    goto :goto_5

    .line 857
    .end local v23    # "position":I
    :cond_f
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/support/v4/view/FixedViewPager;->mItems:Ljava/util/ArrayList;

    move-object/from16 v27, v0

    sget-object v28, Landroid/support/v4/view/FixedViewPager;->COMPARATOR:Ljava/util/Comparator;

    invoke-static/range {v27 .. v28}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 859
    if-eqz v16, :cond_12

    .line 861
    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/view/FixedViewPager;->getChildCount()I

    move-result v7

    .line 862
    .local v7, "childCount":I
    const/4 v9, 0x0

    :goto_6
    if-ge v9, v7, :cond_11

    .line 863
    move-object/from16 v0, p0

    invoke-virtual {v0, v9}, Landroid/support/v4/view/FixedViewPager;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    .line 864
    .local v6, "child":Landroid/view/View;
    invoke-virtual {v6}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v14

    check-cast v14, Landroid/support/v4/view/FixedViewPager$LayoutParams;

    .line 865
    .local v14, "lp":Landroid/support/v4/view/FixedViewPager$LayoutParams;
    iget-boolean v0, v14, Landroid/support/v4/view/FixedViewPager$LayoutParams;->isDecor:Z

    move/from16 v27, v0

    if-nez v27, :cond_10

    .line 866
    const/16 v27, 0x0

    move/from16 v0, v27

    iput v0, v14, Landroid/support/v4/view/FixedViewPager$LayoutParams;->widthFactor:F

    .line 862
    :cond_10
    add-int/lit8 v9, v9, 0x1

    goto :goto_6

    .line 870
    .end local v6    # "child":Landroid/view/View;
    .end local v14    # "lp":Landroid/support/v4/view/FixedViewPager$LayoutParams;
    :cond_11
    const/16 v27, 0x0

    const/16 v28, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v17

    move/from16 v2, v27

    move/from16 v3, v28

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v4/view/FixedViewPager;->setCurrentItemInternal(IZZ)V

    .line 871
    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/view/FixedViewPager;->requestLayout()V

    .line 873
    .end local v7    # "childCount":I
    :cond_12
    return-void
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 2336
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0, p1}, Landroid/support/v4/view/FixedViewPager;->executeKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 6
    .param p1, "event"    # Landroid/view/accessibility/AccessibilityEvent;

    .prologue
    .line 2561
    invoke-virtual {p0}, Landroid/support/v4/view/FixedViewPager;->getChildCount()I

    move-result v1

    .line 2562
    .local v1, "childCount":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v1, :cond_1

    .line 2563
    invoke-virtual {p0, v2}, Landroid/support/v4/view/FixedViewPager;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 2564
    .local v0, "child":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v4

    if-nez v4, :cond_0

    .line 2565
    invoke-virtual {p0, v0}, Landroid/support/v4/view/FixedViewPager;->infoForChild(Landroid/view/View;)Landroid/support/v4/view/FixedViewPager$ItemInfo;

    move-result-object v3

    .line 2566
    .local v3, "ii":Landroid/support/v4/view/FixedViewPager$ItemInfo;
    if-eqz v3, :cond_0

    iget v4, v3, Landroid/support/v4/view/FixedViewPager$ItemInfo;->position:I

    iget v5, p0, Landroid/support/v4/view/FixedViewPager;->mCurItem:I

    if-ne v4, v5, :cond_0

    invoke-virtual {v0, p1}, Landroid/view/View;->dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2568
    const/4 v4, 0x1

    .line 2573
    .end local v0    # "child":Landroid/view/View;
    .end local v3    # "ii":Landroid/support/v4/view/FixedViewPager$ItemInfo;
    :goto_1
    return v4

    .line 2562
    .restart local v0    # "child":Landroid/view/View;
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 2573
    .end local v0    # "child":Landroid/view/View;
    :cond_1
    const/4 v4, 0x0

    goto :goto_1
.end method

.method distanceInfluenceForSnapDuration(F)F
    .locals 4
    .param p1, "f"    # F

    .prologue
    .line 633
    const/high16 v0, 0x3f000000    # 0.5f

    sub-float/2addr p1, v0

    .line 634
    float-to-double v0, p1

    const-wide v2, 0x3fde28c7460698c7L    # 0.4712389167638204

    mul-double/2addr v0, v2

    double-to-float p1, v0

    .line 635
    float-to-double v0, p1

    invoke-static {v0, v1}, Ljava/lang/Math;->sin(D)D

    move-result-wide v0

    double-to-float v0, v0

    return v0
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 8
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v6, 0x1

    .line 2047
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->draw(Landroid/graphics/Canvas;)V

    .line 2048
    const/4 v1, 0x0

    .line 2050
    .local v1, "needsInvalidate":Z
    invoke-static {p0}, Landroid/support/v4/view/ViewCompat;->getOverScrollMode(Landroid/view/View;)I

    move-result v2

    .line 2051
    .local v2, "overScrollMode":I
    if-eqz v2, :cond_0

    if-ne v2, v6, :cond_4

    iget-object v5, p0, Landroid/support/v4/view/FixedViewPager;->mAdapter:Landroid/support/v4/view/FixedPagerAdapter;

    if-eqz v5, :cond_4

    iget-object v5, p0, Landroid/support/v4/view/FixedViewPager;->mAdapter:Landroid/support/v4/view/FixedPagerAdapter;

    invoke-virtual {v5}, Landroid/support/v4/view/FixedPagerAdapter;->getCount()I

    move-result v5

    if-le v5, v6, :cond_4

    .line 2054
    :cond_0
    iget-object v5, p0, Landroid/support/v4/view/FixedViewPager;->mLeftEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-virtual {v5}, Landroid/support/v4/widget/EdgeEffectCompat;->isFinished()Z

    move-result v5

    if-nez v5, :cond_1

    .line 2055
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v3

    .line 2056
    .local v3, "restoreCount":I
    invoke-virtual {p0}, Landroid/support/v4/view/FixedViewPager;->getHeight()I

    move-result v5

    invoke-virtual {p0}, Landroid/support/v4/view/FixedViewPager;->getPaddingTop()I

    move-result v6

    sub-int/2addr v5, v6

    invoke-virtual {p0}, Landroid/support/v4/view/FixedViewPager;->getPaddingBottom()I

    move-result v6

    sub-int v0, v5, v6

    .line 2057
    .local v0, "height":I
    invoke-virtual {p0}, Landroid/support/v4/view/FixedViewPager;->getWidth()I

    move-result v4

    .line 2059
    .local v4, "width":I
    const/high16 v5, 0x43870000    # 270.0f

    invoke-virtual {p1, v5}, Landroid/graphics/Canvas;->rotate(F)V

    .line 2060
    neg-int v5, v0

    invoke-virtual {p0}, Landroid/support/v4/view/FixedViewPager;->getPaddingTop()I

    move-result v6

    add-int/2addr v5, v6

    int-to-float v5, v5

    iget v6, p0, Landroid/support/v4/view/FixedViewPager;->mFirstOffset:F

    int-to-float v7, v4

    mul-float/2addr v6, v7

    invoke-virtual {p1, v5, v6}, Landroid/graphics/Canvas;->translate(FF)V

    .line 2061
    iget-object v5, p0, Landroid/support/v4/view/FixedViewPager;->mLeftEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-virtual {v5, v0, v4}, Landroid/support/v4/widget/EdgeEffectCompat;->setSize(II)V

    .line 2062
    iget-object v5, p0, Landroid/support/v4/view/FixedViewPager;->mLeftEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-virtual {v5, p1}, Landroid/support/v4/widget/EdgeEffectCompat;->draw(Landroid/graphics/Canvas;)Z

    move-result v5

    or-int/2addr v1, v5

    .line 2063
    invoke-virtual {p1, v3}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 2065
    .end local v0    # "height":I
    .end local v3    # "restoreCount":I
    .end local v4    # "width":I
    :cond_1
    iget-object v5, p0, Landroid/support/v4/view/FixedViewPager;->mRightEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-virtual {v5}, Landroid/support/v4/widget/EdgeEffectCompat;->isFinished()Z

    move-result v5

    if-nez v5, :cond_2

    .line 2066
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v3

    .line 2067
    .restart local v3    # "restoreCount":I
    invoke-virtual {p0}, Landroid/support/v4/view/FixedViewPager;->getWidth()I

    move-result v4

    .line 2068
    .restart local v4    # "width":I
    invoke-virtual {p0}, Landroid/support/v4/view/FixedViewPager;->getHeight()I

    move-result v5

    invoke-virtual {p0}, Landroid/support/v4/view/FixedViewPager;->getPaddingTop()I

    move-result v6

    sub-int/2addr v5, v6

    invoke-virtual {p0}, Landroid/support/v4/view/FixedViewPager;->getPaddingBottom()I

    move-result v6

    sub-int v0, v5, v6

    .line 2070
    .restart local v0    # "height":I
    const/high16 v5, 0x42b40000    # 90.0f

    invoke-virtual {p1, v5}, Landroid/graphics/Canvas;->rotate(F)V

    .line 2071
    invoke-virtual {p0}, Landroid/support/v4/view/FixedViewPager;->getPaddingTop()I

    move-result v5

    neg-int v5, v5

    int-to-float v5, v5

    iget v6, p0, Landroid/support/v4/view/FixedViewPager;->mLastOffset:F

    const/high16 v7, 0x3f800000    # 1.0f

    add-float/2addr v6, v7

    neg-float v6, v6

    int-to-float v7, v4

    mul-float/2addr v6, v7

    invoke-virtual {p1, v5, v6}, Landroid/graphics/Canvas;->translate(FF)V

    .line 2072
    iget-object v5, p0, Landroid/support/v4/view/FixedViewPager;->mRightEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-virtual {v5, v0, v4}, Landroid/support/v4/widget/EdgeEffectCompat;->setSize(II)V

    .line 2073
    iget-object v5, p0, Landroid/support/v4/view/FixedViewPager;->mRightEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-virtual {v5, p1}, Landroid/support/v4/widget/EdgeEffectCompat;->draw(Landroid/graphics/Canvas;)Z

    move-result v5

    or-int/2addr v1, v5

    .line 2074
    invoke-virtual {p1, v3}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 2081
    .end local v0    # "height":I
    .end local v3    # "restoreCount":I
    .end local v4    # "width":I
    :cond_2
    :goto_0
    if-eqz v1, :cond_3

    .line 2083
    invoke-static {p0}, Landroid/support/v4/view/ViewCompat;->postInvalidateOnAnimation(Landroid/view/View;)V

    .line 2085
    :cond_3
    return-void

    .line 2077
    :cond_4
    iget-object v5, p0, Landroid/support/v4/view/FixedViewPager;->mLeftEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-virtual {v5}, Landroid/support/v4/widget/EdgeEffectCompat;->finish()V

    .line 2078
    iget-object v5, p0, Landroid/support/v4/view/FixedViewPager;->mRightEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-virtual {v5}, Landroid/support/v4/widget/EdgeEffectCompat;->finish()V

    goto :goto_0
.end method

.method protected drawableStateChanged()V
    .locals 2

    .prologue
    .line 621
    invoke-super {p0}, Landroid/view/ViewGroup;->drawableStateChanged()V

    .line 622
    iget-object v0, p0, Landroid/support/v4/view/FixedViewPager;->mMarginDrawable:Landroid/graphics/drawable/Drawable;

    .line 623
    .local v0, "d":Landroid/graphics/drawable/Drawable;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 624
    invoke-virtual {p0}, Landroid/support/v4/view/FixedViewPager;->getDrawableState()[I

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    .line 626
    :cond_0
    return-void
.end method

.method public endFakeDrag()V
    .locals 12

    .prologue
    const/4 v11, 0x1

    .line 2175
    iget-boolean v9, p0, Landroid/support/v4/view/FixedViewPager;->mFakeDragging:Z

    if-nez v9, :cond_0

    .line 2176
    new-instance v9, Ljava/lang/IllegalStateException;

    const-string v10, "No fake drag in progress. Call beginFakeDrag first."

    invoke-direct {v9, v10}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v9

    .line 2179
    :cond_0
    iget-object v7, p0, Landroid/support/v4/view/FixedViewPager;->mVelocityTracker:Landroid/view/VelocityTracker;

    .line 2180
    .local v7, "velocityTracker":Landroid/view/VelocityTracker;
    const/16 v9, 0x3e8

    iget v10, p0, Landroid/support/v4/view/FixedViewPager;->mMaximumVelocity:I

    int-to-float v10, v10

    invoke-virtual {v7, v9, v10}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    .line 2181
    iget v9, p0, Landroid/support/v4/view/FixedViewPager;->mActivePointerId:I

    invoke-static {v7, v9}, Landroid/support/v4/view/VelocityTrackerCompat;->getXVelocity(Landroid/view/VelocityTracker;I)F

    move-result v9

    float-to-int v2, v9

    .line 2183
    .local v2, "initialVelocity":I
    iput-boolean v11, p0, Landroid/support/v4/view/FixedViewPager;->mPopulatePending:Z

    .line 2184
    invoke-virtual {p0}, Landroid/support/v4/view/FixedViewPager;->getWidth()I

    move-result v8

    .line 2185
    .local v8, "width":I
    invoke-virtual {p0}, Landroid/support/v4/view/FixedViewPager;->getScrollX()I

    move-result v5

    .line 2186
    .local v5, "scrollX":I
    invoke-direct {p0}, Landroid/support/v4/view/FixedViewPager;->infoForCurrentScrollPosition()Landroid/support/v4/view/FixedViewPager$ItemInfo;

    move-result-object v1

    .line 2187
    .local v1, "ii":Landroid/support/v4/view/FixedViewPager$ItemInfo;
    iget v0, v1, Landroid/support/v4/view/FixedViewPager$ItemInfo;->position:I

    .line 2188
    .local v0, "currentPage":I
    int-to-float v9, v5

    int-to-float v10, v8

    div-float/2addr v9, v10

    iget v10, v1, Landroid/support/v4/view/FixedViewPager$ItemInfo;->offset:F

    sub-float/2addr v9, v10

    iget v10, v1, Landroid/support/v4/view/FixedViewPager$ItemInfo;->widthFactor:F

    div-float v4, v9, v10

    .line 2189
    .local v4, "pageOffset":F
    iget v9, p0, Landroid/support/v4/view/FixedViewPager;->mLastMotionX:F

    iget v10, p0, Landroid/support/v4/view/FixedViewPager;->mInitialMotionX:F

    sub-float/2addr v9, v10

    float-to-int v6, v9

    .line 2190
    .local v6, "totalDelta":I
    invoke-direct {p0, v0, v4, v2, v6}, Landroid/support/v4/view/FixedViewPager;->determineTargetPage(IFII)I

    move-result v3

    .line 2192
    .local v3, "nextPage":I
    invoke-virtual {p0, v3, v11, v11, v2}, Landroid/support/v4/view/FixedViewPager;->setCurrentItemInternal(IZZI)V

    .line 2193
    invoke-direct {p0}, Landroid/support/v4/view/FixedViewPager;->endDrag()V

    .line 2195
    const/4 v9, 0x0

    iput-boolean v9, p0, Landroid/support/v4/view/FixedViewPager;->mFakeDragging:Z

    .line 2196
    return-void
.end method

.method public executeKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 4
    .param p1, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v3, 0x1

    .line 2348
    const/4 v0, 0x0

    .line 2349
    .local v0, "handled":Z
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_0

    .line 2350
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 2370
    :cond_0
    :goto_0
    return v0

    .line 2352
    :sswitch_0
    const/16 v1, 0x11

    invoke-virtual {p0, v1}, Landroid/support/v4/view/FixedViewPager;->arrowScroll(I)Z

    move-result v0

    .line 2353
    goto :goto_0

    .line 2355
    :sswitch_1
    const/16 v1, 0x42

    invoke-virtual {p0, v1}, Landroid/support/v4/view/FixedViewPager;->arrowScroll(I)Z

    move-result v0

    .line 2356
    goto :goto_0

    .line 2358
    :sswitch_2
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-lt v1, v2, :cond_0

    .line 2361
    invoke-static {p1}, Landroid/support/v4/view/KeyEventCompat;->hasNoModifiers(Landroid/view/KeyEvent;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2362
    const/4 v1, 0x2

    invoke-virtual {p0, v1}, Landroid/support/v4/view/FixedViewPager;->arrowScroll(I)Z

    move-result v0

    goto :goto_0

    .line 2363
    :cond_1
    invoke-static {p1, v3}, Landroid/support/v4/view/KeyEventCompat;->hasModifiers(Landroid/view/KeyEvent;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2364
    invoke-virtual {p0, v3}, Landroid/support/v4/view/FixedViewPager;->arrowScroll(I)Z

    move-result v0

    goto :goto_0

    .line 2350
    nop

    :sswitch_data_0
    .sparse-switch
        0x15 -> :sswitch_0
        0x16 -> :sswitch_1
        0x3d -> :sswitch_2
    .end sparse-switch
.end method

.method public fakeDragBy(F)V
    .locals 17
    .param p1, "xOffset"    # F

    .prologue
    .line 2206
    move-object/from16 v0, p0

    iget-boolean v1, v0, Landroid/support/v4/view/FixedViewPager;->mFakeDragging:Z

    if-nez v1, :cond_0

    .line 2207
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "No fake drag in progress. Call beginFakeDrag first."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2210
    :cond_0
    move-object/from16 v0, p0

    iget v1, v0, Landroid/support/v4/view/FixedViewPager;->mLastMotionX:F

    add-float v1, v1, p1

    move-object/from16 v0, p0

    iput v1, v0, Landroid/support/v4/view/FixedViewPager;->mLastMotionX:F

    .line 2212
    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/view/FixedViewPager;->getScrollX()I

    move-result v1

    int-to-float v13, v1

    .line 2213
    .local v13, "oldScrollX":F
    sub-float v15, v13, p1

    .line 2214
    .local v15, "scrollX":F
    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/view/FixedViewPager;->getWidth()I

    move-result v16

    .line 2216
    .local v16, "width":I
    move/from16 v0, v16

    int-to-float v1, v0

    move-object/from16 v0, p0

    iget v2, v0, Landroid/support/v4/view/FixedViewPager;->mFirstOffset:F

    mul-float v12, v1, v2

    .line 2217
    .local v12, "leftBound":F
    move/from16 v0, v16

    int-to-float v1, v0

    move-object/from16 v0, p0

    iget v2, v0, Landroid/support/v4/view/FixedViewPager;->mLastOffset:F

    mul-float v14, v1, v2

    .line 2219
    .local v14, "rightBound":F
    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/v4/view/FixedViewPager;->mItems:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/support/v4/view/FixedViewPager$ItemInfo;

    .line 2220
    .local v10, "firstItem":Landroid/support/v4/view/FixedViewPager$ItemInfo;
    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/v4/view/FixedViewPager;->mItems:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v4/view/FixedViewPager;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/support/v4/view/FixedViewPager$ItemInfo;

    .line 2221
    .local v11, "lastItem":Landroid/support/v4/view/FixedViewPager$ItemInfo;
    iget v1, v10, Landroid/support/v4/view/FixedViewPager$ItemInfo;->position:I

    if-eqz v1, :cond_1

    .line 2222
    iget v1, v10, Landroid/support/v4/view/FixedViewPager$ItemInfo;->offset:F

    move/from16 v0, v16

    int-to-float v2, v0

    mul-float v12, v1, v2

    .line 2224
    :cond_1
    iget v1, v11, Landroid/support/v4/view/FixedViewPager$ItemInfo;->position:I

    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v4/view/FixedViewPager;->mAdapter:Landroid/support/v4/view/FixedPagerAdapter;

    invoke-virtual {v2}, Landroid/support/v4/view/FixedPagerAdapter;->getCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-eq v1, v2, :cond_2

    .line 2225
    iget v1, v11, Landroid/support/v4/view/FixedViewPager$ItemInfo;->offset:F

    move/from16 v0, v16

    int-to-float v2, v0

    mul-float v14, v1, v2

    .line 2228
    :cond_2
    cmpg-float v1, v15, v12

    if-gez v1, :cond_4

    .line 2229
    move v15, v12

    .line 2234
    :cond_3
    :goto_0
    move-object/from16 v0, p0

    iget v1, v0, Landroid/support/v4/view/FixedViewPager;->mLastMotionX:F

    float-to-int v2, v15

    int-to-float v2, v2

    sub-float v2, v15, v2

    add-float/2addr v1, v2

    move-object/from16 v0, p0

    iput v1, v0, Landroid/support/v4/view/FixedViewPager;->mLastMotionX:F

    .line 2235
    float-to-int v1, v15

    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/view/FixedViewPager;->getScrollY()I

    move-result v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/view/FixedViewPager;->scrollTo(II)V

    .line 2236
    float-to-int v1, v15

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Landroid/support/v4/view/FixedViewPager;->pageScrolled(I)Z

    .line 2239
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v3

    .line 2240
    .local v3, "time":J
    move-object/from16 v0, p0

    iget-wide v1, v0, Landroid/support/v4/view/FixedViewPager;->mFakeDragBeginTime:J

    const/4 v5, 0x2

    move-object/from16 v0, p0

    iget v6, v0, Landroid/support/v4/view/FixedViewPager;->mLastMotionX:F

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-static/range {v1 .. v8}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v9

    .line 2242
    .local v9, "ev":Landroid/view/MotionEvent;
    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/v4/view/FixedViewPager;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v1, v9}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 2243
    invoke-virtual {v9}, Landroid/view/MotionEvent;->recycle()V

    .line 2244
    return-void

    .line 2230
    .end local v3    # "time":J
    .end local v9    # "ev":Landroid/view/MotionEvent;
    :cond_4
    cmpl-float v1, v15, v14

    if-lez v1, :cond_3

    .line 2231
    move v15, v14

    goto :goto_0
.end method

.method protected generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 2578
    new-instance v0, Landroid/support/v4/view/FixedViewPager$LayoutParams;

    invoke-direct {v0}, Landroid/support/v4/view/FixedViewPager$LayoutParams;-><init>()V

    return-object v0
.end method

.method public generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .locals 2
    .param p1, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 2593
    new-instance v0, Landroid/support/v4/view/FixedViewPager$LayoutParams;

    invoke-virtual {p0}, Landroid/support/v4/view/FixedViewPager;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Landroid/support/v4/view/FixedViewPager$LayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method protected generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1
    .param p1, "p"    # Landroid/view/ViewGroup$LayoutParams;

    .prologue
    .line 2583
    invoke-virtual {p0}, Landroid/support/v4/view/FixedViewPager;->generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    return-object v0
.end method

.method public getAdapter()Landroid/support/v4/view/PagerAdapter;
    .locals 1

    .prologue
    .line 413
    iget-object v0, p0, Landroid/support/v4/view/FixedViewPager;->mAdapter:Landroid/support/v4/view/FixedPagerAdapter;

    return-object v0
.end method

.method public getCurrentItem()I
    .locals 1

    .prologue
    .line 444
    iget v0, p0, Landroid/support/v4/view/FixedViewPager;->mCurItem:I

    return v0
.end method

.method public getOffscreenPageLimit()I
    .locals 1

    .prologue
    .line 534
    iget v0, p0, Landroid/support/v4/view/FixedViewPager;->mOffscreenPageLimit:I

    return v0
.end method

.method public getPageMargin()I
    .locals 1

    .prologue
    .line 590
    iget v0, p0, Landroid/support/v4/view/FixedViewPager;->mPageMargin:I

    return v0
.end method

.method public getSize()I
    .locals 1

    .prologue
    .line 2695
    iget-object v0, p0, Landroid/support/v4/view/FixedViewPager;->mItems:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v4/view/FixedViewPager;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method infoForAnyChild(Landroid/view/View;)Landroid/support/v4/view/FixedViewPager$ItemInfo;
    .locals 2
    .param p1, "child"    # Landroid/view/View;

    .prologue
    .line 1239
    :goto_0
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .local v0, "parent":Landroid/view/ViewParent;
    if-eq v0, p0, :cond_2

    .line 1240
    if-eqz v0, :cond_0

    instance-of v1, v0, Landroid/view/View;

    if-nez v1, :cond_1

    .line 1241
    :cond_0
    const/4 v1, 0x0

    .line 1245
    :goto_1
    return-object v1

    :cond_1
    move-object p1, v0

    .line 1243
    check-cast p1, Landroid/view/View;

    goto :goto_0

    .line 1245
    :cond_2
    invoke-virtual {p0, p1}, Landroid/support/v4/view/FixedViewPager;->infoForChild(Landroid/view/View;)Landroid/support/v4/view/FixedViewPager$ItemInfo;

    move-result-object v1

    goto :goto_1
.end method

.method infoForChild(Landroid/view/View;)Landroid/support/v4/view/FixedViewPager$ItemInfo;
    .locals 4
    .param p1, "child"    # Landroid/view/View;

    .prologue
    .line 1228
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Landroid/support/v4/view/FixedViewPager;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 1229
    iget-object v2, p0, Landroid/support/v4/view/FixedViewPager;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v4/view/FixedViewPager$ItemInfo;

    .line 1230
    .local v1, "ii":Landroid/support/v4/view/FixedViewPager$ItemInfo;
    iget-object v2, p0, Landroid/support/v4/view/FixedViewPager;->mAdapter:Landroid/support/v4/view/FixedPagerAdapter;

    iget-object v3, v1, Landroid/support/v4/view/FixedViewPager$ItemInfo;->object:Ljava/lang/Object;

    invoke-virtual {v2, p1, v3}, Landroid/support/v4/view/FixedPagerAdapter;->isViewFromObject(Landroid/view/View;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1234
    .end local v1    # "ii":Landroid/support/v4/view/FixedViewPager$ItemInfo;
    :goto_1
    return-object v1

    .line 1228
    .restart local v1    # "ii":Landroid/support/v4/view/FixedViewPager$ItemInfo;
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1234
    .end local v1    # "ii":Landroid/support/v4/view/FixedViewPager$ItemInfo;
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method infoForPosition(I)Landroid/support/v4/view/FixedViewPager$ItemInfo;
    .locals 3
    .param p1, "position"    # I

    .prologue
    .line 1249
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Landroid/support/v4/view/FixedViewPager;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 1250
    iget-object v2, p0, Landroid/support/v4/view/FixedViewPager;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v4/view/FixedViewPager$ItemInfo;

    .line 1251
    .local v1, "ii":Landroid/support/v4/view/FixedViewPager$ItemInfo;
    iget v2, v1, Landroid/support/v4/view/FixedViewPager$ItemInfo;->position:I

    if-ne v2, p1, :cond_0

    .line 1255
    .end local v1    # "ii":Landroid/support/v4/view/FixedViewPager$ItemInfo;
    :goto_1
    return-object v1

    .line 1249
    .restart local v1    # "ii":Landroid/support/v4/view/FixedViewPager$ItemInfo;
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1255
    .end local v1    # "ii":Landroid/support/v4/view/FixedViewPager$ItemInfo;
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method initViewPager()V
    .locals 5

    .prologue
    .line 312
    const/4 v3, 0x0

    invoke-virtual {p0, v3}, Landroid/support/v4/view/FixedViewPager;->setWillNotDraw(Z)V

    .line 313
    const/high16 v3, 0x40000

    invoke-virtual {p0, v3}, Landroid/support/v4/view/FixedViewPager;->setDescendantFocusability(I)V

    .line 315
    invoke-virtual {p0}, Landroid/support/v4/view/FixedViewPager;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 316
    .local v1, "context":Landroid/content/Context;
    new-instance v3, Landroid/widget/Scroller;

    sget-object v4, Landroid/support/v4/view/FixedViewPager;->sInterpolator:Landroid/view/animation/Interpolator;

    invoke-direct {v3, v1, v4}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;Landroid/view/animation/Interpolator;)V

    iput-object v3, p0, Landroid/support/v4/view/FixedViewPager;->mScroller:Landroid/widget/Scroller;

    .line 317
    invoke-static {v1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    .line 318
    .local v0, "configuration":Landroid/view/ViewConfiguration;
    invoke-static {v0}, Landroid/support/v4/view/ViewConfigurationCompat;->getScaledPagingTouchSlop(Landroid/view/ViewConfiguration;)I

    move-result v3

    iput v3, p0, Landroid/support/v4/view/FixedViewPager;->mTouchSlop:I

    .line 319
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMinimumFlingVelocity()I

    move-result v3

    iput v3, p0, Landroid/support/v4/view/FixedViewPager;->mMinimumVelocity:I

    .line 320
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMaximumFlingVelocity()I

    move-result v3

    iput v3, p0, Landroid/support/v4/view/FixedViewPager;->mMaximumVelocity:I

    .line 321
    new-instance v3, Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-direct {v3, v1}, Landroid/support/v4/widget/EdgeEffectCompat;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Landroid/support/v4/view/FixedViewPager;->mLeftEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    .line 322
    new-instance v3, Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-direct {v3, v1}, Landroid/support/v4/widget/EdgeEffectCompat;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Landroid/support/v4/view/FixedViewPager;->mRightEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    .line 324
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v2, v3, Landroid/util/DisplayMetrics;->density:F

    .line 325
    .local v2, "density":F
    const/high16 v3, 0x41c80000    # 25.0f

    mul-float/2addr v3, v2

    float-to-int v3, v3

    iput v3, p0, Landroid/support/v4/view/FixedViewPager;->mFlingDistance:I

    .line 326
    const/high16 v3, 0x40000000    # 2.0f

    mul-float/2addr v3, v2

    float-to-int v3, v3

    iput v3, p0, Landroid/support/v4/view/FixedViewPager;->mCloseEnough:I

    .line 327
    const/high16 v3, 0x41800000    # 16.0f

    mul-float/2addr v3, v2

    float-to-int v3, v3

    iput v3, p0, Landroid/support/v4/view/FixedViewPager;->mDefaultGutterSize:I

    .line 329
    new-instance v3, Landroid/support/v4/view/FixedViewPager$MyAccessibilityDelegate;

    invoke-direct {v3, p0}, Landroid/support/v4/view/FixedViewPager$MyAccessibilityDelegate;-><init>(Landroid/support/v4/view/FixedViewPager;)V

    invoke-static {p0, v3}, Landroid/support/v4/view/ViewCompat;->setAccessibilityDelegate(Landroid/view/View;Landroid/support/v4/view/AccessibilityDelegateCompat;)V

    .line 331
    invoke-static {p0}, Landroid/support/v4/view/ViewCompat;->getImportantForAccessibility(Landroid/view/View;)I

    move-result v3

    if-nez v3, :cond_0

    .line 333
    const/4 v3, 0x1

    invoke-static {p0, v3}, Landroid/support/v4/view/ViewCompat;->setImportantForAccessibility(Landroid/view/View;I)V

    .line 336
    :cond_0
    return-void
.end method

.method public isFakeDragging()Z
    .locals 1

    .prologue
    .line 2256
    iget-boolean v0, p0, Landroid/support/v4/view/FixedViewPager;->mFakeDragging:Z

    return v0
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .prologue
    .line 1260
    invoke-super {p0}, Landroid/view/ViewGroup;->onAttachedToWindow()V

    .line 1261
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/view/FixedViewPager;->mFirstLayout:Z

    .line 1262
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 18
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 2089
    invoke-super/range {p0 .. p1}, Landroid/view/ViewGroup;->onDraw(Landroid/graphics/Canvas;)V

    .line 2092
    move-object/from16 v0, p0

    iget v13, v0, Landroid/support/v4/view/FixedViewPager;->mPageMargin:I

    if-lez v13, :cond_2

    move-object/from16 v0, p0

    iget-object v13, v0, Landroid/support/v4/view/FixedViewPager;->mMarginDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v13, :cond_2

    move-object/from16 v0, p0

    iget-object v13, v0, Landroid/support/v4/view/FixedViewPager;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v13

    if-lez v13, :cond_2

    move-object/from16 v0, p0

    iget-object v13, v0, Landroid/support/v4/view/FixedViewPager;->mAdapter:Landroid/support/v4/view/FixedPagerAdapter;

    if-eqz v13, :cond_2

    .line 2093
    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/view/FixedViewPager;->getScrollX()I

    move-result v10

    .line 2094
    .local v10, "scrollX":I
    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/view/FixedViewPager;->getWidth()I

    move-result v11

    .line 2096
    .local v11, "width":I
    move-object/from16 v0, p0

    iget v13, v0, Landroid/support/v4/view/FixedViewPager;->mPageMargin:I

    int-to-float v13, v13

    int-to-float v14, v11

    div-float v7, v13, v14

    .line 2097
    .local v7, "marginOffset":F
    const/4 v5, 0x0

    .line 2098
    .local v5, "itemIndex":I
    move-object/from16 v0, p0

    iget-object v13, v0, Landroid/support/v4/view/FixedViewPager;->mItems:Ljava/util/ArrayList;

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/support/v4/view/FixedViewPager$ItemInfo;

    .line 2099
    .local v3, "ii":Landroid/support/v4/view/FixedViewPager$ItemInfo;
    iget v8, v3, Landroid/support/v4/view/FixedViewPager$ItemInfo;->offset:F

    .line 2100
    .local v8, "offset":F
    move-object/from16 v0, p0

    iget-object v13, v0, Landroid/support/v4/view/FixedViewPager;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 2101
    .local v4, "itemCount":I
    iget v2, v3, Landroid/support/v4/view/FixedViewPager$ItemInfo;->position:I

    .line 2102
    .local v2, "firstPos":I
    move-object/from16 v0, p0

    iget-object v13, v0, Landroid/support/v4/view/FixedViewPager;->mItems:Ljava/util/ArrayList;

    add-int/lit8 v14, v4, -0x1

    invoke-virtual {v13, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Landroid/support/v4/view/FixedViewPager$ItemInfo;

    iget v6, v13, Landroid/support/v4/view/FixedViewPager$ItemInfo;->position:I

    .line 2103
    .local v6, "lastPos":I
    move v9, v2

    .local v9, "pos":I
    :goto_0
    if-ge v9, v6, :cond_2

    .line 2104
    :goto_1
    iget v13, v3, Landroid/support/v4/view/FixedViewPager$ItemInfo;->position:I

    if-le v9, v13, :cond_0

    if-ge v5, v4, :cond_0

    .line 2105
    move-object/from16 v0, p0

    iget-object v13, v0, Landroid/support/v4/view/FixedViewPager;->mItems:Ljava/util/ArrayList;

    add-int/lit8 v5, v5, 0x1

    invoke-virtual {v13, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "ii":Landroid/support/v4/view/FixedViewPager$ItemInfo;
    check-cast v3, Landroid/support/v4/view/FixedViewPager$ItemInfo;

    .restart local v3    # "ii":Landroid/support/v4/view/FixedViewPager$ItemInfo;
    goto :goto_1

    .line 2109
    :cond_0
    iget v13, v3, Landroid/support/v4/view/FixedViewPager$ItemInfo;->position:I

    if-ne v9, v13, :cond_3

    .line 2110
    iget v13, v3, Landroid/support/v4/view/FixedViewPager$ItemInfo;->offset:F

    iget v14, v3, Landroid/support/v4/view/FixedViewPager$ItemInfo;->widthFactor:F

    add-float/2addr v13, v14

    int-to-float v14, v11

    mul-float v1, v13, v14

    .line 2111
    .local v1, "drawAt":F
    iget v13, v3, Landroid/support/v4/view/FixedViewPager$ItemInfo;->offset:F

    iget v14, v3, Landroid/support/v4/view/FixedViewPager$ItemInfo;->widthFactor:F

    add-float/2addr v13, v14

    add-float v8, v13, v7

    .line 2118
    :goto_2
    move-object/from16 v0, p0

    iget v13, v0, Landroid/support/v4/view/FixedViewPager;->mPageMargin:I

    int-to-float v13, v13

    add-float/2addr v13, v1

    int-to-float v14, v10

    cmpl-float v13, v13, v14

    if-lez v13, :cond_1

    .line 2119
    move-object/from16 v0, p0

    iget-object v13, v0, Landroid/support/v4/view/FixedViewPager;->mMarginDrawable:Landroid/graphics/drawable/Drawable;

    float-to-int v14, v1

    move-object/from16 v0, p0

    iget v15, v0, Landroid/support/v4/view/FixedViewPager;->mTopPageBounds:I

    move-object/from16 v0, p0

    iget v0, v0, Landroid/support/v4/view/FixedViewPager;->mPageMargin:I

    move/from16 v16, v0

    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v16, v0

    add-float v16, v16, v1

    const/high16 v17, 0x3f000000    # 0.5f

    add-float v16, v16, v17

    move/from16 v0, v16

    float-to-int v0, v0

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget v0, v0, Landroid/support/v4/view/FixedViewPager;->mBottomPageBounds:I

    move/from16 v17, v0

    invoke-virtual/range {v13 .. v17}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 2121
    move-object/from16 v0, p0

    iget-object v13, v0, Landroid/support/v4/view/FixedViewPager;->mMarginDrawable:Landroid/graphics/drawable/Drawable;

    move-object/from16 v0, p1

    invoke-virtual {v13, v0}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 2124
    :cond_1
    add-int v13, v10, v11

    int-to-float v13, v13

    cmpl-float v13, v1, v13

    if-lez v13, :cond_4

    .line 2129
    .end local v1    # "drawAt":F
    .end local v2    # "firstPos":I
    .end local v3    # "ii":Landroid/support/v4/view/FixedViewPager$ItemInfo;
    .end local v4    # "itemCount":I
    .end local v5    # "itemIndex":I
    .end local v6    # "lastPos":I
    .end local v7    # "marginOffset":F
    .end local v8    # "offset":F
    .end local v9    # "pos":I
    .end local v10    # "scrollX":I
    .end local v11    # "width":I
    :cond_2
    return-void

    .line 2113
    .restart local v2    # "firstPos":I
    .restart local v3    # "ii":Landroid/support/v4/view/FixedViewPager$ItemInfo;
    .restart local v4    # "itemCount":I
    .restart local v5    # "itemIndex":I
    .restart local v6    # "lastPos":I
    .restart local v7    # "marginOffset":F
    .restart local v8    # "offset":F
    .restart local v9    # "pos":I
    .restart local v10    # "scrollX":I
    .restart local v11    # "width":I
    :cond_3
    move-object/from16 v0, p0

    iget-object v13, v0, Landroid/support/v4/view/FixedViewPager;->mAdapter:Landroid/support/v4/view/FixedPagerAdapter;

    invoke-virtual {v13, v9}, Landroid/support/v4/view/FixedPagerAdapter;->getPageWidth(I)F

    move-result v12

    .line 2114
    .local v12, "widthFactor":F
    add-float v13, v8, v12

    int-to-float v14, v11

    mul-float v1, v13, v14

    .line 2115
    .restart local v1    # "drawAt":F
    add-float v13, v12, v7

    add-float/2addr v8, v13

    goto :goto_2

    .line 2103
    .end local v12    # "widthFactor":F
    :cond_4
    add-int/lit8 v9, v9, 0x1

    goto :goto_0
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 14
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 1659
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    and-int/lit16 v6, v0, 0xff

    .line 1662
    .local v6, "action":I
    const/4 v0, 0x3

    if-eq v6, v0, :cond_0

    const/4 v0, 0x1

    if-ne v6, v0, :cond_2

    .line 1665
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v4/view/FixedViewPager;->mIsBeingDragged:Z

    .line 1666
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v4/view/FixedViewPager;->mIsUnableToDrag:Z

    .line 1667
    const/4 v0, -0x1

    iput v0, p0, Landroid/support/v4/view/FixedViewPager;->mActivePointerId:I

    .line 1668
    iget-object v0, p0, Landroid/support/v4/view/FixedViewPager;->mVelocityTracker:Landroid/view/VelocityTracker;

    if-eqz v0, :cond_1

    .line 1669
    iget-object v0, p0, Landroid/support/v4/view/FixedViewPager;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->recycle()V

    .line 1670
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v4/view/FixedViewPager;->mVelocityTracker:Landroid/view/VelocityTracker;

    .line 1672
    :cond_1
    const/4 v0, 0x0

    .line 1789
    :goto_0
    return v0

    .line 1677
    :cond_2
    if-eqz v6, :cond_4

    .line 1678
    iget-boolean v0, p0, Landroid/support/v4/view/FixedViewPager;->mIsBeingDragged:Z

    if-eqz v0, :cond_3

    .line 1680
    const/4 v0, 0x1

    goto :goto_0

    .line 1682
    :cond_3
    iget-boolean v0, p0, Landroid/support/v4/view/FixedViewPager;->mIsUnableToDrag:Z

    if-eqz v0, :cond_4

    .line 1684
    const/4 v0, 0x0

    goto :goto_0

    .line 1688
    :cond_4
    sparse-switch v6, :sswitch_data_0

    .line 1780
    :cond_5
    :goto_1
    iget-object v0, p0, Landroid/support/v4/view/FixedViewPager;->mVelocityTracker:Landroid/view/VelocityTracker;

    if-nez v0, :cond_6

    .line 1781
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/view/FixedViewPager;->mVelocityTracker:Landroid/view/VelocityTracker;

    .line 1783
    :cond_6
    iget-object v0, p0, Landroid/support/v4/view/FixedViewPager;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v0, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 1789
    iget-boolean v0, p0, Landroid/support/v4/view/FixedViewPager;->mIsBeingDragged:Z

    goto :goto_0

    .line 1699
    :sswitch_0
    iget v7, p0, Landroid/support/v4/view/FixedViewPager;->mActivePointerId:I

    .line 1700
    .local v7, "activePointerId":I
    const/4 v0, -0x1

    if-eq v7, v0, :cond_5

    .line 1705
    invoke-static {p1, v7}, Landroid/support/v4/view/MotionEventCompat;->findPointerIndex(Landroid/view/MotionEvent;I)I

    move-result v9

    .line 1706
    .local v9, "pointerIndex":I
    invoke-static {p1, v9}, Landroid/support/v4/view/MotionEventCompat;->getX(Landroid/view/MotionEvent;I)F

    move-result v10

    .line 1707
    .local v10, "x":F
    iget v0, p0, Landroid/support/v4/view/FixedViewPager;->mLastMotionX:F

    sub-float v8, v10, v0

    .line 1708
    .local v8, "dx":F
    invoke-static {v8}, Ljava/lang/Math;->abs(F)F

    move-result v11

    .line 1709
    .local v11, "xDiff":F
    invoke-static {p1, v9}, Landroid/support/v4/view/MotionEventCompat;->getY(Landroid/view/MotionEvent;I)F

    move-result v12

    .line 1710
    .local v12, "y":F
    iget v0, p0, Landroid/support/v4/view/FixedViewPager;->mLastMotionY:F

    sub-float v0, v12, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v13

    .line 1713
    .local v13, "yDiff":F
    const/4 v0, 0x0

    cmpl-float v0, v8, v0

    if-eqz v0, :cond_7

    iget v0, p0, Landroid/support/v4/view/FixedViewPager;->mLastMotionX:F

    invoke-direct {p0, v0, v8}, Landroid/support/v4/view/FixedViewPager;->isGutterDrag(FF)Z

    move-result v0

    if-nez v0, :cond_7

    const/4 v2, 0x0

    float-to-int v3, v8

    float-to-int v4, v10

    float-to-int v5, v12

    move-object v0, p0

    move-object v1, p0

    invoke-virtual/range {v0 .. v5}, Landroid/support/v4/view/FixedViewPager;->canScroll(Landroid/view/View;ZIII)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1716
    iput v10, p0, Landroid/support/v4/view/FixedViewPager;->mLastMotionX:F

    iput v10, p0, Landroid/support/v4/view/FixedViewPager;->mInitialMotionX:F

    .line 1717
    iput v12, p0, Landroid/support/v4/view/FixedViewPager;->mLastMotionY:F

    .line 1718
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/view/FixedViewPager;->mIsUnableToDrag:Z

    .line 1719
    const/4 v0, 0x0

    goto :goto_0

    .line 1721
    :cond_7
    iget v0, p0, Landroid/support/v4/view/FixedViewPager;->mTouchSlop:I

    int-to-float v0, v0

    cmpl-float v0, v11, v0

    if-lez v0, :cond_a

    cmpl-float v0, v11, v13

    if-lez v0, :cond_a

    .line 1723
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/view/FixedViewPager;->mIsBeingDragged:Z

    .line 1724
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Landroid/support/v4/view/FixedViewPager;->setScrollState(I)V

    .line 1725
    const/4 v0, 0x0

    cmpl-float v0, v8, v0

    if-lez v0, :cond_9

    iget v0, p0, Landroid/support/v4/view/FixedViewPager;->mInitialMotionX:F

    iget v1, p0, Landroid/support/v4/view/FixedViewPager;->mTouchSlop:I

    int-to-float v1, v1

    add-float/2addr v0, v1

    :goto_2
    iput v0, p0, Landroid/support/v4/view/FixedViewPager;->mLastMotionX:F

    .line 1727
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Landroid/support/v4/view/FixedViewPager;->setScrollingCacheEnabled(Z)V

    .line 1738
    :cond_8
    :goto_3
    iget-boolean v0, p0, Landroid/support/v4/view/FixedViewPager;->mIsBeingDragged:Z

    if-eqz v0, :cond_5

    .line 1740
    invoke-direct {p0, v10}, Landroid/support/v4/view/FixedViewPager;->performDrag(F)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1741
    invoke-static {p0}, Landroid/support/v4/view/ViewCompat;->postInvalidateOnAnimation(Landroid/view/View;)V

    goto/16 :goto_1

    .line 1725
    :cond_9
    iget v0, p0, Landroid/support/v4/view/FixedViewPager;->mInitialMotionX:F

    iget v1, p0, Landroid/support/v4/view/FixedViewPager;->mTouchSlop:I

    int-to-float v1, v1

    sub-float/2addr v0, v1

    goto :goto_2

    .line 1729
    :cond_a
    iget v0, p0, Landroid/support/v4/view/FixedViewPager;->mTouchSlop:I

    int-to-float v0, v0

    cmpl-float v0, v13, v0

    if-lez v0, :cond_8

    .line 1735
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/view/FixedViewPager;->mIsUnableToDrag:Z

    goto :goto_3

    .line 1752
    .end local v7    # "activePointerId":I
    .end local v8    # "dx":F
    .end local v9    # "pointerIndex":I
    .end local v10    # "x":F
    .end local v11    # "xDiff":F
    .end local v12    # "y":F
    .end local v13    # "yDiff":F
    :sswitch_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Landroid/support/v4/view/FixedViewPager;->mInitialMotionX:F

    iput v0, p0, Landroid/support/v4/view/FixedViewPager;->mLastMotionX:F

    .line 1753
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, Landroid/support/v4/view/FixedViewPager;->mLastMotionY:F

    .line 1754
    const/4 v0, 0x0

    invoke-static {p1, v0}, Landroid/support/v4/view/MotionEventCompat;->getPointerId(Landroid/view/MotionEvent;I)I

    move-result v0

    iput v0, p0, Landroid/support/v4/view/FixedViewPager;->mActivePointerId:I

    .line 1755
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v4/view/FixedViewPager;->mIsUnableToDrag:Z

    .line 1757
    iget-object v0, p0, Landroid/support/v4/view/FixedViewPager;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->computeScrollOffset()Z

    .line 1758
    iget v0, p0, Landroid/support/v4/view/FixedViewPager;->mScrollState:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_b

    iget-object v0, p0, Landroid/support/v4/view/FixedViewPager;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->getFinalX()I

    move-result v0

    iget-object v1, p0, Landroid/support/v4/view/FixedViewPager;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v1}, Landroid/widget/Scroller;->getCurrX()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    iget v1, p0, Landroid/support/v4/view/FixedViewPager;->mCloseEnough:I

    if-le v0, v1, :cond_b

    .line 1760
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v4/view/FixedViewPager;->mPopulatePending:Z

    .line 1761
    invoke-virtual {p0}, Landroid/support/v4/view/FixedViewPager;->populate()V

    .line 1762
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v4/view/FixedViewPager;->mIsBeingDragged:Z

    .line 1763
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Landroid/support/v4/view/FixedViewPager;->setScrollState(I)V

    goto/16 :goto_1

    .line 1765
    :cond_b
    invoke-direct {p0}, Landroid/support/v4/view/FixedViewPager;->completeScroll()V

    .line 1766
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v4/view/FixedViewPager;->mIsBeingDragged:Z

    goto/16 :goto_1

    .line 1776
    :sswitch_2
    invoke-direct {p0, p1}, Landroid/support/v4/view/FixedViewPager;->onSecondaryPointerUp(Landroid/view/MotionEvent;)V

    goto/16 :goto_1

    .line 1688
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_1
        0x2 -> :sswitch_0
        0x6 -> :sswitch_2
    .end sparse-switch
.end method

.method protected onLayout(ZIIII)V
    .locals 24
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 1398
    const/16 v22, 0x1

    move/from16 v0, v22

    move-object/from16 v1, p0

    iput-boolean v0, v1, Landroid/support/v4/view/FixedViewPager;->mInLayout:Z

    .line 1399
    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/view/FixedViewPager;->populate()V

    .line 1400
    const/16 v22, 0x0

    move/from16 v0, v22

    move-object/from16 v1, p0

    iput-boolean v0, v1, Landroid/support/v4/view/FixedViewPager;->mInLayout:Z

    .line 1402
    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/view/FixedViewPager;->getChildCount()I

    move-result v5

    .line 1403
    .local v5, "count":I
    sub-int v20, p4, p2

    .line 1404
    .local v20, "width":I
    sub-int v7, p5, p3

    .line 1405
    .local v7, "height":I
    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/view/FixedViewPager;->getPaddingLeft()I

    move-result v15

    .line 1406
    .local v15, "paddingLeft":I
    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/view/FixedViewPager;->getPaddingTop()I

    move-result v17

    .line 1407
    .local v17, "paddingTop":I
    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/view/FixedViewPager;->getPaddingRight()I

    move-result v16

    .line 1408
    .local v16, "paddingRight":I
    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/view/FixedViewPager;->getPaddingBottom()I

    move-result v14

    .line 1409
    .local v14, "paddingBottom":I
    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/view/FixedViewPager;->getScrollX()I

    move-result v18

    .line 1411
    .local v18, "scrollX":I
    const/4 v6, 0x0

    .line 1415
    .local v6, "decorCount":I
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_0
    if-ge v10, v5, :cond_1

    .line 1416
    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Landroid/support/v4/view/FixedViewPager;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 1417
    .local v2, "child":Landroid/view/View;
    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v22

    const/16 v23, 0x8

    move/from16 v0, v22

    move/from16 v1, v23

    if-eq v0, v1, :cond_0

    .line 1418
    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v13

    check-cast v13, Landroid/support/v4/view/FixedViewPager$LayoutParams;

    .line 1419
    .local v13, "lp":Landroid/support/v4/view/FixedViewPager$LayoutParams;
    const/4 v3, 0x0

    .line 1420
    .local v3, "childLeft":I
    const/4 v4, 0x0

    .line 1421
    .local v4, "childTop":I
    iget-boolean v0, v13, Landroid/support/v4/view/FixedViewPager$LayoutParams;->isDecor:Z

    move/from16 v22, v0

    if-eqz v22, :cond_0

    .line 1422
    iget v0, v13, Landroid/support/v4/view/FixedViewPager$LayoutParams;->gravity:I

    move/from16 v22, v0

    and-int/lit8 v9, v22, 0x7

    .line 1423
    .local v9, "hgrav":I
    iget v0, v13, Landroid/support/v4/view/FixedViewPager$LayoutParams;->gravity:I

    move/from16 v22, v0

    and-int/lit8 v19, v22, 0x70

    .line 1424
    .local v19, "vgrav":I
    packed-switch v9, :pswitch_data_0

    .line 1426
    :pswitch_0
    move v3, v15

    .line 1441
    :goto_1
    sparse-switch v19, :sswitch_data_0

    .line 1443
    move/from16 v4, v17

    .line 1458
    :goto_2
    add-int v3, v3, v18

    .line 1459
    invoke-virtual {v2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v22

    add-int v22, v22, v3

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v23

    add-int v23, v23, v4

    move/from16 v0, v22

    move/from16 v1, v23

    invoke-virtual {v2, v3, v4, v0, v1}, Landroid/view/View;->layout(IIII)V

    .line 1462
    add-int/lit8 v6, v6, 0x1

    .line 1415
    .end local v3    # "childLeft":I
    .end local v4    # "childTop":I
    .end local v9    # "hgrav":I
    .end local v13    # "lp":Landroid/support/v4/view/FixedViewPager$LayoutParams;
    .end local v19    # "vgrav":I
    :cond_0
    add-int/lit8 v10, v10, 0x1

    goto :goto_0

    .line 1429
    .restart local v3    # "childLeft":I
    .restart local v4    # "childTop":I
    .restart local v9    # "hgrav":I
    .restart local v13    # "lp":Landroid/support/v4/view/FixedViewPager$LayoutParams;
    .restart local v19    # "vgrav":I
    :pswitch_1
    move v3, v15

    .line 1430
    invoke-virtual {v2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v22

    add-int v15, v15, v22

    .line 1431
    goto :goto_1

    .line 1433
    :pswitch_2
    invoke-virtual {v2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v22

    sub-int v22, v20, v22

    div-int/lit8 v22, v22, 0x2

    move/from16 v0, v22

    invoke-static {v0, v15}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 1435
    goto :goto_1

    .line 1437
    :pswitch_3
    sub-int v22, v20, v16

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v23

    sub-int v3, v22, v23

    .line 1438
    invoke-virtual {v2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v22

    add-int v16, v16, v22

    goto :goto_1

    .line 1446
    :sswitch_0
    move/from16 v4, v17

    .line 1447
    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v22

    add-int v17, v17, v22

    .line 1448
    goto :goto_2

    .line 1450
    :sswitch_1
    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v22

    sub-int v22, v7, v22

    div-int/lit8 v22, v22, 0x2

    move/from16 v0, v22

    move/from16 v1, v17

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v4

    .line 1452
    goto :goto_2

    .line 1454
    :sswitch_2
    sub-int v22, v7, v14

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v23

    sub-int v4, v22, v23

    .line 1455
    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v22

    add-int v14, v14, v22

    goto :goto_2

    .line 1468
    .end local v2    # "child":Landroid/view/View;
    .end local v3    # "childLeft":I
    .end local v4    # "childTop":I
    .end local v9    # "hgrav":I
    .end local v13    # "lp":Landroid/support/v4/view/FixedViewPager$LayoutParams;
    .end local v19    # "vgrav":I
    :cond_1
    const/4 v10, 0x0

    :goto_3
    if-ge v10, v5, :cond_4

    .line 1469
    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Landroid/support/v4/view/FixedViewPager;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 1470
    .restart local v2    # "child":Landroid/view/View;
    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v22

    const/16 v23, 0x8

    move/from16 v0, v22

    move/from16 v1, v23

    if-eq v0, v1, :cond_3

    .line 1471
    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v13

    check-cast v13, Landroid/support/v4/view/FixedViewPager$LayoutParams;

    .line 1473
    .restart local v13    # "lp":Landroid/support/v4/view/FixedViewPager$LayoutParams;
    iget-boolean v0, v13, Landroid/support/v4/view/FixedViewPager$LayoutParams;->isDecor:Z

    move/from16 v22, v0

    if-nez v22, :cond_3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Landroid/support/v4/view/FixedViewPager;->infoForChild(Landroid/view/View;)Landroid/support/v4/view/FixedViewPager$ItemInfo;

    move-result-object v11

    .local v11, "ii":Landroid/support/v4/view/FixedViewPager$ItemInfo;
    if-eqz v11, :cond_3

    .line 1474
    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v22, v0

    iget v0, v11, Landroid/support/v4/view/FixedViewPager$ItemInfo;->offset:F

    move/from16 v23, v0

    mul-float v22, v22, v23

    move/from16 v0, v22

    float-to-int v12, v0

    .line 1475
    .local v12, "loff":I
    add-int v3, v15, v12

    .line 1476
    .restart local v3    # "childLeft":I
    move/from16 v4, v17

    .line 1477
    .restart local v4    # "childTop":I
    iget-boolean v0, v13, Landroid/support/v4/view/FixedViewPager$LayoutParams;->needsMeasure:Z

    move/from16 v22, v0

    if-eqz v22, :cond_2

    .line 1480
    const/16 v22, 0x0

    move/from16 v0, v22

    iput-boolean v0, v13, Landroid/support/v4/view/FixedViewPager$LayoutParams;->needsMeasure:Z

    .line 1481
    sub-int v22, v20, v15

    sub-int v22, v22, v16

    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v22, v0

    iget v0, v13, Landroid/support/v4/view/FixedViewPager$LayoutParams;->widthFactor:F

    move/from16 v23, v0

    mul-float v22, v22, v23

    move/from16 v0, v22

    float-to-int v0, v0

    move/from16 v22, v0

    const/high16 v23, 0x40000000    # 2.0f

    invoke-static/range {v22 .. v23}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v21

    .line 1484
    .local v21, "widthSpec":I
    sub-int v22, v7, v17

    sub-int v22, v22, v14

    const/high16 v23, 0x40000000    # 2.0f

    invoke-static/range {v22 .. v23}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    .line 1487
    .local v8, "heightSpec":I
    move/from16 v0, v21

    invoke-virtual {v2, v0, v8}, Landroid/view/View;->measure(II)V

    .line 1492
    .end local v8    # "heightSpec":I
    .end local v21    # "widthSpec":I
    :cond_2
    invoke-virtual {v2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v22

    add-int v22, v22, v3

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v23

    add-int v23, v23, v4

    move/from16 v0, v22

    move/from16 v1, v23

    invoke-virtual {v2, v3, v4, v0, v1}, Landroid/view/View;->layout(IIII)V

    .line 1468
    .end local v3    # "childLeft":I
    .end local v4    # "childTop":I
    .end local v11    # "ii":Landroid/support/v4/view/FixedViewPager$ItemInfo;
    .end local v12    # "loff":I
    .end local v13    # "lp":Landroid/support/v4/view/FixedViewPager$LayoutParams;
    :cond_3
    add-int/lit8 v10, v10, 0x1

    goto/16 :goto_3

    .line 1498
    .end local v2    # "child":Landroid/view/View;
    :cond_4
    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Landroid/support/v4/view/FixedViewPager;->mTopPageBounds:I

    .line 1499
    sub-int v22, v7, v14

    move/from16 v0, v22

    move-object/from16 v1, p0

    iput v0, v1, Landroid/support/v4/view/FixedViewPager;->mBottomPageBounds:I

    .line 1500
    move-object/from16 v0, p0

    iput v6, v0, Landroid/support/v4/view/FixedViewPager;->mDecorChildCount:I

    .line 1501
    const/16 v22, 0x0

    move/from16 v0, v22

    move-object/from16 v1, p0

    iput-boolean v0, v1, Landroid/support/v4/view/FixedViewPager;->mFirstLayout:Z

    .line 1502
    return-void

    .line 1424
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_3
    .end packed-switch

    .line 1441
    :sswitch_data_0
    .sparse-switch
        0x10 -> :sswitch_1
        0x30 -> :sswitch_0
        0x50 -> :sswitch_2
    .end sparse-switch
.end method

.method protected onMeasure(II)V
    .locals 23
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 1271
    const/16 v21, 0x0

    move/from16 v0, v21

    move/from16 v1, p1

    invoke-static {v0, v1}, Landroid/support/v4/view/FixedViewPager;->getDefaultSize(II)I

    move-result v21

    const/16 v22, 0x0

    move/from16 v0, v22

    move/from16 v1, p2

    invoke-static {v0, v1}, Landroid/support/v4/view/FixedViewPager;->getDefaultSize(II)I

    move-result v22

    move-object/from16 v0, p0

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/view/FixedViewPager;->setMeasuredDimension(II)V

    .line 1274
    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/view/FixedViewPager;->getMeasuredWidth()I

    move-result v15

    .line 1275
    .local v15, "measuredWidth":I
    div-int/lit8 v14, v15, 0xa

    .line 1276
    .local v14, "maxGutterSize":I
    move-object/from16 v0, p0

    iget v0, v0, Landroid/support/v4/view/FixedViewPager;->mDefaultGutterSize:I

    move/from16 v21, v0

    move/from16 v0, v21

    invoke-static {v14, v0}, Ljava/lang/Math;->min(II)I

    move-result v21

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput v0, v1, Landroid/support/v4/view/FixedViewPager;->mGutterSize:I

    .line 1279
    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/view/FixedViewPager;->getPaddingLeft()I

    move-result v21

    sub-int v21, v15, v21

    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/view/FixedViewPager;->getPaddingRight()I

    move-result v22

    sub-int v5, v21, v22

    .line 1280
    .local v5, "childWidthSize":I
    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/view/FixedViewPager;->getMeasuredHeight()I

    move-result v21

    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/view/FixedViewPager;->getPaddingTop()I

    move-result v22

    sub-int v21, v21, v22

    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/view/FixedViewPager;->getPaddingBottom()I

    move-result v22

    sub-int v4, v21, v22

    .line 1287
    .local v4, "childHeightSize":I
    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/view/FixedViewPager;->getChildCount()I

    move-result v16

    .line 1288
    .local v16, "size":I
    const/4 v12, 0x0

    .local v12, "i":I
    :goto_0
    move/from16 v0, v16

    if-ge v12, v0, :cond_a

    .line 1289
    move-object/from16 v0, p0

    invoke-virtual {v0, v12}, Landroid/support/v4/view/FixedViewPager;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 1290
    .local v3, "child":Landroid/view/View;
    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    move-result v21

    const/16 v22, 0x8

    move/from16 v0, v21

    move/from16 v1, v22

    if-eq v0, v1, :cond_5

    .line 1291
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v13

    check-cast v13, Landroid/support/v4/view/FixedViewPager$LayoutParams;

    .line 1292
    .local v13, "lp":Landroid/support/v4/view/FixedViewPager$LayoutParams;
    if-eqz v13, :cond_5

    iget-boolean v0, v13, Landroid/support/v4/view/FixedViewPager$LayoutParams;->isDecor:Z

    move/from16 v21, v0

    if-eqz v21, :cond_5

    .line 1293
    iget v0, v13, Landroid/support/v4/view/FixedViewPager$LayoutParams;->gravity:I

    move/from16 v21, v0

    and-int/lit8 v11, v21, 0x7

    .line 1294
    .local v11, "hgrav":I
    iget v0, v13, Landroid/support/v4/view/FixedViewPager$LayoutParams;->gravity:I

    move/from16 v21, v0

    and-int/lit8 v17, v21, 0x70

    .line 1295
    .local v17, "vgrav":I
    const/high16 v18, -0x80000000

    .line 1296
    .local v18, "widthMode":I
    const/high16 v8, -0x80000000

    .line 1297
    .local v8, "heightMode":I
    const/16 v21, 0x30

    move/from16 v0, v17

    move/from16 v1, v21

    if-eq v0, v1, :cond_0

    const/16 v21, 0x50

    move/from16 v0, v17

    move/from16 v1, v21

    if-ne v0, v1, :cond_6

    :cond_0
    const/4 v7, 0x1

    .line 1298
    .local v7, "consumeVertical":Z
    :goto_1
    const/16 v21, 0x3

    move/from16 v0, v21

    if-eq v11, v0, :cond_1

    const/16 v21, 0x5

    move/from16 v0, v21

    if-ne v11, v0, :cond_7

    :cond_1
    const/4 v6, 0x1

    .line 1300
    .local v6, "consumeHorizontal":Z
    :goto_2
    if-eqz v7, :cond_8

    .line 1301
    const/high16 v18, 0x40000000    # 2.0f

    .line 1306
    :cond_2
    :goto_3
    move/from16 v19, v5

    .line 1307
    .local v19, "widthSize":I
    move v9, v4

    .line 1308
    .local v9, "heightSize":I
    iget v0, v13, Landroid/support/v4/view/FixedViewPager$LayoutParams;->width:I

    move/from16 v21, v0

    const/16 v22, -0x2

    move/from16 v0, v21

    move/from16 v1, v22

    if-eq v0, v1, :cond_3

    .line 1309
    const/high16 v18, 0x40000000    # 2.0f

    .line 1310
    iget v0, v13, Landroid/support/v4/view/FixedViewPager$LayoutParams;->width:I

    move/from16 v21, v0

    const/16 v22, -0x1

    move/from16 v0, v21

    move/from16 v1, v22

    if-eq v0, v1, :cond_3

    .line 1311
    iget v0, v13, Landroid/support/v4/view/FixedViewPager$LayoutParams;->width:I

    move/from16 v19, v0

    .line 1314
    :cond_3
    iget v0, v13, Landroid/support/v4/view/FixedViewPager$LayoutParams;->height:I

    move/from16 v21, v0

    const/16 v22, -0x2

    move/from16 v0, v21

    move/from16 v1, v22

    if-eq v0, v1, :cond_4

    .line 1315
    const/high16 v8, 0x40000000    # 2.0f

    .line 1316
    iget v0, v13, Landroid/support/v4/view/FixedViewPager$LayoutParams;->height:I

    move/from16 v21, v0

    const/16 v22, -0x1

    move/from16 v0, v21

    move/from16 v1, v22

    if-eq v0, v1, :cond_4

    .line 1317
    iget v9, v13, Landroid/support/v4/view/FixedViewPager$LayoutParams;->height:I

    .line 1320
    :cond_4
    move/from16 v0, v19

    move/from16 v1, v18

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v20

    .line 1321
    .local v20, "widthSpec":I
    invoke-static {v9, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v10

    .line 1322
    .local v10, "heightSpec":I
    move/from16 v0, v20

    invoke-virtual {v3, v0, v10}, Landroid/view/View;->measure(II)V

    .line 1324
    if-eqz v7, :cond_9

    .line 1325
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v21

    sub-int v4, v4, v21

    .line 1288
    .end local v6    # "consumeHorizontal":Z
    .end local v7    # "consumeVertical":Z
    .end local v8    # "heightMode":I
    .end local v9    # "heightSize":I
    .end local v10    # "heightSpec":I
    .end local v11    # "hgrav":I
    .end local v13    # "lp":Landroid/support/v4/view/FixedViewPager$LayoutParams;
    .end local v17    # "vgrav":I
    .end local v18    # "widthMode":I
    .end local v19    # "widthSize":I
    .end local v20    # "widthSpec":I
    :cond_5
    :goto_4
    add-int/lit8 v12, v12, 0x1

    goto/16 :goto_0

    .line 1297
    .restart local v8    # "heightMode":I
    .restart local v11    # "hgrav":I
    .restart local v13    # "lp":Landroid/support/v4/view/FixedViewPager$LayoutParams;
    .restart local v17    # "vgrav":I
    .restart local v18    # "widthMode":I
    :cond_6
    const/4 v7, 0x0

    goto :goto_1

    .line 1298
    .restart local v7    # "consumeVertical":Z
    :cond_7
    const/4 v6, 0x0

    goto :goto_2

    .line 1302
    .restart local v6    # "consumeHorizontal":Z
    :cond_8
    if-eqz v6, :cond_2

    .line 1303
    const/high16 v8, 0x40000000    # 2.0f

    goto :goto_3

    .line 1326
    .restart local v9    # "heightSize":I
    .restart local v10    # "heightSpec":I
    .restart local v19    # "widthSize":I
    .restart local v20    # "widthSpec":I
    :cond_9
    if-eqz v6, :cond_5

    .line 1327
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredWidth()I

    move-result v21

    sub-int v5, v5, v21

    goto :goto_4

    .line 1333
    .end local v3    # "child":Landroid/view/View;
    .end local v6    # "consumeHorizontal":Z
    .end local v7    # "consumeVertical":Z
    .end local v8    # "heightMode":I
    .end local v9    # "heightSize":I
    .end local v10    # "heightSpec":I
    .end local v11    # "hgrav":I
    .end local v13    # "lp":Landroid/support/v4/view/FixedViewPager$LayoutParams;
    .end local v17    # "vgrav":I
    .end local v18    # "widthMode":I
    .end local v19    # "widthSize":I
    .end local v20    # "widthSpec":I
    :cond_a
    const/high16 v21, 0x40000000    # 2.0f

    move/from16 v0, v21

    invoke-static {v5, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v21

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput v0, v1, Landroid/support/v4/view/FixedViewPager;->mChildWidthMeasureSpec:I

    .line 1334
    const/high16 v21, 0x40000000    # 2.0f

    move/from16 v0, v21

    invoke-static {v4, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v21

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput v0, v1, Landroid/support/v4/view/FixedViewPager;->mChildHeightMeasureSpec:I

    .line 1337
    const/16 v21, 0x1

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput-boolean v0, v1, Landroid/support/v4/view/FixedViewPager;->mInLayout:Z

    .line 1338
    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/view/FixedViewPager;->populate()V

    .line 1339
    const/16 v21, 0x0

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput-boolean v0, v1, Landroid/support/v4/view/FixedViewPager;->mInLayout:Z

    .line 1342
    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/view/FixedViewPager;->getChildCount()I

    move-result v16

    .line 1343
    const/4 v12, 0x0

    :goto_5
    move/from16 v0, v16

    if-ge v12, v0, :cond_d

    .line 1344
    move-object/from16 v0, p0

    invoke-virtual {v0, v12}, Landroid/support/v4/view/FixedViewPager;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 1345
    .restart local v3    # "child":Landroid/view/View;
    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    move-result v21

    const/16 v22, 0x8

    move/from16 v0, v21

    move/from16 v1, v22

    if-eq v0, v1, :cond_c

    .line 1349
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v13

    check-cast v13, Landroid/support/v4/view/FixedViewPager$LayoutParams;

    .line 1350
    .restart local v13    # "lp":Landroid/support/v4/view/FixedViewPager$LayoutParams;
    if-eqz v13, :cond_b

    iget-boolean v0, v13, Landroid/support/v4/view/FixedViewPager$LayoutParams;->isDecor:Z

    move/from16 v21, v0

    if-nez v21, :cond_c

    .line 1351
    :cond_b
    int-to-float v0, v5

    move/from16 v21, v0

    iget v0, v13, Landroid/support/v4/view/FixedViewPager$LayoutParams;->widthFactor:F

    move/from16 v22, v0

    mul-float v21, v21, v22

    move/from16 v0, v21

    float-to-int v0, v0

    move/from16 v21, v0

    const/high16 v22, 0x40000000    # 2.0f

    invoke-static/range {v21 .. v22}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v20

    .line 1353
    .restart local v20    # "widthSpec":I
    move-object/from16 v0, p0

    iget v0, v0, Landroid/support/v4/view/FixedViewPager;->mChildHeightMeasureSpec:I

    move/from16 v21, v0

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v3, v0, v1}, Landroid/view/View;->measure(II)V

    .line 1343
    .end local v13    # "lp":Landroid/support/v4/view/FixedViewPager$LayoutParams;
    .end local v20    # "widthSpec":I
    :cond_c
    add-int/lit8 v12, v12, 0x1

    goto :goto_5

    .line 1357
    .end local v3    # "child":Landroid/view/View;
    :cond_d
    return-void
.end method

.method protected onPageScrolled(IFI)V
    .locals 16
    .param p1, "position"    # I
    .param p2, "offset"    # F
    .param p3, "offsetPixels"    # I

    .prologue
    .line 1571
    move-object/from16 v0, p0

    iget v14, v0, Landroid/support/v4/view/FixedViewPager;->mDecorChildCount:I

    if-lez v14, :cond_2

    .line 1572
    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/view/FixedViewPager;->getScrollX()I

    move-result v12

    .line 1573
    .local v12, "scrollX":I
    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/view/FixedViewPager;->getPaddingLeft()I

    move-result v10

    .line 1574
    .local v10, "paddingLeft":I
    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/view/FixedViewPager;->getPaddingRight()I

    move-result v11

    .line 1575
    .local v11, "paddingRight":I
    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/view/FixedViewPager;->getWidth()I

    move-result v13

    .line 1576
    .local v13, "width":I
    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/view/FixedViewPager;->getChildCount()I

    move-result v4

    .line 1577
    .local v4, "childCount":I
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_0
    if-ge v8, v4, :cond_2

    .line 1578
    move-object/from16 v0, p0

    invoke-virtual {v0, v8}, Landroid/support/v4/view/FixedViewPager;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 1579
    .local v3, "child":Landroid/view/View;
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v9

    check-cast v9, Landroid/support/v4/view/FixedViewPager$LayoutParams;

    .line 1580
    .local v9, "lp":Landroid/support/v4/view/FixedViewPager$LayoutParams;
    iget-boolean v14, v9, Landroid/support/v4/view/FixedViewPager$LayoutParams;->isDecor:Z

    if-nez v14, :cond_1

    .line 1577
    :cond_0
    :goto_1
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 1582
    :cond_1
    iget v14, v9, Landroid/support/v4/view/FixedViewPager$LayoutParams;->gravity:I

    and-int/lit8 v7, v14, 0x7

    .line 1583
    .local v7, "hgrav":I
    const/4 v5, 0x0

    .line 1584
    .local v5, "childLeft":I
    packed-switch v7, :pswitch_data_0

    .line 1586
    :pswitch_0
    move v5, v10

    .line 1601
    :goto_2
    add-int/2addr v5, v12

    .line 1603
    invoke-virtual {v3}, Landroid/view/View;->getLeft()I

    move-result v14

    sub-int v6, v5, v14

    .line 1604
    .local v6, "childOffset":I
    if-eqz v6, :cond_0

    .line 1605
    invoke-virtual {v3, v6}, Landroid/view/View;->offsetLeftAndRight(I)V

    goto :goto_1

    .line 1589
    .end local v6    # "childOffset":I
    :pswitch_1
    move v5, v10

    .line 1590
    invoke-virtual {v3}, Landroid/view/View;->getWidth()I

    move-result v14

    add-int/2addr v10, v14

    .line 1591
    goto :goto_2

    .line 1593
    :pswitch_2
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredWidth()I

    move-result v14

    sub-int v14, v13, v14

    div-int/lit8 v14, v14, 0x2

    invoke-static {v14, v10}, Ljava/lang/Math;->max(II)I

    move-result v5

    .line 1595
    goto :goto_2

    .line 1597
    :pswitch_3
    sub-int v14, v13, v11

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredWidth()I

    move-result v15

    sub-int v5, v14, v15

    .line 1598
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredWidth()I

    move-result v14

    add-int/2addr v11, v14

    goto :goto_2

    .line 1610
    .end local v3    # "child":Landroid/view/View;
    .end local v4    # "childCount":I
    .end local v5    # "childLeft":I
    .end local v7    # "hgrav":I
    .end local v8    # "i":I
    .end local v9    # "lp":Landroid/support/v4/view/FixedViewPager$LayoutParams;
    .end local v10    # "paddingLeft":I
    .end local v11    # "paddingRight":I
    .end local v12    # "scrollX":I
    .end local v13    # "width":I
    :cond_2
    move-object/from16 v0, p0

    iget-object v14, v0, Landroid/support/v4/view/FixedViewPager;->mOnPageChangeListener:Landroid/support/v4/view/FixedViewPager$OnPageChangeListener;

    if-eqz v14, :cond_3

    .line 1611
    move-object/from16 v0, p0

    iget-object v14, v0, Landroid/support/v4/view/FixedViewPager;->mOnPageChangeListener:Landroid/support/v4/view/FixedViewPager$OnPageChangeListener;

    move/from16 v0, p1

    move/from16 v1, p2

    move/from16 v2, p3

    invoke-interface {v14, v0, v1, v2}, Landroid/support/v4/view/FixedViewPager$OnPageChangeListener;->onPageScrolled(IFI)V

    .line 1613
    :cond_3
    move-object/from16 v0, p0

    iget-object v14, v0, Landroid/support/v4/view/FixedViewPager;->mInternalPageChangeListener:Landroid/support/v4/view/FixedViewPager$OnPageChangeListener;

    if-eqz v14, :cond_4

    .line 1614
    move-object/from16 v0, p0

    iget-object v14, v0, Landroid/support/v4/view/FixedViewPager;->mInternalPageChangeListener:Landroid/support/v4/view/FixedViewPager$OnPageChangeListener;

    move/from16 v0, p1

    move/from16 v1, p2

    move/from16 v2, p3

    invoke-interface {v14, v0, v1, v2}, Landroid/support/v4/view/FixedViewPager$OnPageChangeListener;->onPageScrolled(IFI)V

    .line 1616
    :cond_4
    const/4 v14, 0x1

    move-object/from16 v0, p0

    iput-boolean v14, v0, Landroid/support/v4/view/FixedViewPager;->mCalledSuper:Z

    .line 1617
    return-void

    .line 1584
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method protected onRequestFocusInDescendants(ILandroid/graphics/Rect;)Z
    .locals 9
    .param p1, "direction"    # I
    .param p2, "previouslyFocusedRect"    # Landroid/graphics/Rect;

    .prologue
    .line 2530
    invoke-virtual {p0}, Landroid/support/v4/view/FixedViewPager;->getChildCount()I

    move-result v1

    .line 2531
    .local v1, "count":I
    and-int/lit8 v7, p1, 0x2

    if-eqz v7, :cond_0

    .line 2532
    const/4 v6, 0x0

    .line 2533
    .local v6, "index":I
    const/4 v5, 0x1

    .line 2534
    .local v5, "increment":I
    move v2, v1

    .line 2540
    .local v2, "end":I
    :goto_0
    move v3, v6

    .local v3, "i":I
    :goto_1
    if-eq v3, v2, :cond_2

    .line 2541
    invoke-virtual {p0, v3}, Landroid/support/v4/view/FixedViewPager;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 2542
    .local v0, "child":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v7

    if-nez v7, :cond_1

    .line 2543
    invoke-virtual {p0, v0}, Landroid/support/v4/view/FixedViewPager;->infoForChild(Landroid/view/View;)Landroid/support/v4/view/FixedViewPager$ItemInfo;

    move-result-object v4

    .line 2544
    .local v4, "ii":Landroid/support/v4/view/FixedViewPager$ItemInfo;
    if-eqz v4, :cond_1

    iget v7, v4, Landroid/support/v4/view/FixedViewPager$ItemInfo;->position:I

    iget v8, p0, Landroid/support/v4/view/FixedViewPager;->mCurItem:I

    if-ne v7, v8, :cond_1

    .line 2545
    invoke-virtual {v0, p1, p2}, Landroid/view/View;->requestFocus(ILandroid/graphics/Rect;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 2546
    const/4 v7, 0x1

    .line 2551
    .end local v0    # "child":Landroid/view/View;
    .end local v4    # "ii":Landroid/support/v4/view/FixedViewPager$ItemInfo;
    :goto_2
    return v7

    .line 2536
    .end local v2    # "end":I
    .end local v3    # "i":I
    .end local v5    # "increment":I
    .end local v6    # "index":I
    :cond_0
    add-int/lit8 v6, v1, -0x1

    .line 2537
    .restart local v6    # "index":I
    const/4 v5, -0x1

    .line 2538
    .restart local v5    # "increment":I
    const/4 v2, -0x1

    .restart local v2    # "end":I
    goto :goto_0

    .line 2540
    .restart local v0    # "child":Landroid/view/View;
    .restart local v3    # "i":I
    :cond_1
    add-int/2addr v3, v5

    goto :goto_1

    .line 2551
    .end local v0    # "child":Landroid/view/View;
    :cond_2
    const/4 v7, 0x0

    goto :goto_2
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 4
    .param p1, "state"    # Landroid/os/Parcelable;

    .prologue
    .line 1183
    instance-of v1, p1, Landroid/support/v4/view/FixedViewPager$SavedState;

    if-nez v1, :cond_0

    .line 1184
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 1199
    :goto_0
    return-void

    :cond_0
    move-object v0, p1

    .line 1188
    check-cast v0, Landroid/support/v4/view/FixedViewPager$SavedState;

    .line 1189
    .local v0, "ss":Landroid/support/v4/view/FixedViewPager$SavedState;
    invoke-virtual {v0}, Landroid/support/v4/view/FixedViewPager$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v1

    invoke-super {p0, v1}, Landroid/view/ViewGroup;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 1191
    iget-object v1, p0, Landroid/support/v4/view/FixedViewPager;->mAdapter:Landroid/support/v4/view/FixedPagerAdapter;

    if-eqz v1, :cond_1

    .line 1192
    iget-object v1, p0, Landroid/support/v4/view/FixedViewPager;->mAdapter:Landroid/support/v4/view/FixedPagerAdapter;

    iget-object v2, v0, Landroid/support/v4/view/FixedViewPager$SavedState;->adapterState:Landroid/os/Parcelable;

    iget-object v3, v0, Landroid/support/v4/view/FixedViewPager$SavedState;->loader:Ljava/lang/ClassLoader;

    invoke-virtual {v1, v2, v3}, Landroid/support/v4/view/FixedPagerAdapter;->restoreState(Landroid/os/Parcelable;Ljava/lang/ClassLoader;)V

    .line 1193
    iget v1, v0, Landroid/support/v4/view/FixedViewPager$SavedState;->position:I

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-virtual {p0, v1, v2, v3}, Landroid/support/v4/view/FixedViewPager;->setCurrentItemInternal(IZZ)V

    goto :goto_0

    .line 1195
    :cond_1
    iget v1, v0, Landroid/support/v4/view/FixedViewPager$SavedState;->position:I

    iput v1, p0, Landroid/support/v4/view/FixedViewPager;->mRestoredCurItem:I

    .line 1196
    iget-object v1, v0, Landroid/support/v4/view/FixedViewPager$SavedState;->adapterState:Landroid/os/Parcelable;

    iput-object v1, p0, Landroid/support/v4/view/FixedViewPager;->mRestoredAdapterState:Landroid/os/Parcelable;

    .line 1197
    iget-object v1, v0, Landroid/support/v4/view/FixedViewPager$SavedState;->loader:Ljava/lang/ClassLoader;

    iput-object v1, p0, Landroid/support/v4/view/FixedViewPager;->mRestoredClassLoader:Ljava/lang/ClassLoader;

    goto :goto_0
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .locals 3

    .prologue
    .line 1172
    invoke-super {p0}, Landroid/view/ViewGroup;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v1

    .line 1173
    .local v1, "superState":Landroid/os/Parcelable;
    new-instance v0, Landroid/support/v4/view/FixedViewPager$SavedState;

    invoke-direct {v0, v1}, Landroid/support/v4/view/FixedViewPager$SavedState;-><init>(Landroid/os/Parcelable;)V

    .line 1174
    .local v0, "ss":Landroid/support/v4/view/FixedViewPager$SavedState;
    iget v2, p0, Landroid/support/v4/view/FixedViewPager;->mCurItem:I

    iput v2, v0, Landroid/support/v4/view/FixedViewPager$SavedState;->position:I

    .line 1175
    iget-object v2, p0, Landroid/support/v4/view/FixedViewPager;->mAdapter:Landroid/support/v4/view/FixedPagerAdapter;

    if-eqz v2, :cond_0

    .line 1176
    iget-object v2, p0, Landroid/support/v4/view/FixedViewPager;->mAdapter:Landroid/support/v4/view/FixedPagerAdapter;

    invoke-virtual {v2}, Landroid/support/v4/view/FixedPagerAdapter;->saveState()Landroid/os/Parcelable;

    move-result-object v2

    iput-object v2, v0, Landroid/support/v4/view/FixedViewPager$SavedState;->adapterState:Landroid/os/Parcelable;

    .line 1178
    :cond_0
    return-object v0
.end method

.method protected onSizeChanged(IIII)V
    .locals 2
    .param p1, "w"    # I
    .param p2, "h"    # I
    .param p3, "oldw"    # I
    .param p4, "oldh"    # I

    .prologue
    .line 1361
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/ViewGroup;->onSizeChanged(IIII)V

    .line 1364
    if-eq p1, p3, :cond_0

    .line 1365
    iget v0, p0, Landroid/support/v4/view/FixedViewPager;->mPageMargin:I

    iget v1, p0, Landroid/support/v4/view/FixedViewPager;->mPageMargin:I

    invoke-direct {p0, p1, p3, v0, v1}, Landroid/support/v4/view/FixedViewPager;->recomputeScrollPosition(IIII)V

    .line 1367
    :cond_0
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 25
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 1794
    move-object/from16 v0, p0

    iget-boolean v0, v0, Landroid/support/v4/view/FixedViewPager;->mFakeDragging:Z

    move/from16 v22, v0

    if-eqz v22, :cond_0

    .line 1798
    const/16 v22, 0x1

    .line 1907
    :goto_0
    return v22

    .line 1801
    :cond_0
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v22

    if-nez v22, :cond_1

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getEdgeFlags()I

    move-result v22

    if-eqz v22, :cond_1

    .line 1804
    const/16 v22, 0x0

    goto :goto_0

    .line 1807
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/support/v4/view/FixedViewPager;->mAdapter:Landroid/support/v4/view/FixedPagerAdapter;

    move-object/from16 v22, v0

    if-eqz v22, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/support/v4/view/FixedViewPager;->mAdapter:Landroid/support/v4/view/FixedPagerAdapter;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/support/v4/view/FixedPagerAdapter;->getCount()I

    move-result v22

    if-nez v22, :cond_3

    .line 1809
    :cond_2
    const/16 v22, 0x0

    goto :goto_0

    .line 1812
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/support/v4/view/FixedViewPager;->mVelocityTracker:Landroid/view/VelocityTracker;

    move-object/from16 v22, v0

    if-nez v22, :cond_4

    .line 1813
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Landroid/support/v4/view/FixedViewPager;->mVelocityTracker:Landroid/view/VelocityTracker;

    .line 1815
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/support/v4/view/FixedViewPager;->mVelocityTracker:Landroid/view/VelocityTracker;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 1817
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v4

    .line 1818
    .local v4, "action":I
    const/4 v10, 0x0

    .line 1820
    .local v10, "needsInvalidate":Z
    and-int/lit16 v0, v4, 0xff

    move/from16 v22, v0

    packed-switch v22, :pswitch_data_0

    .line 1904
    :cond_5
    :goto_1
    :pswitch_0
    if-eqz v10, :cond_6

    .line 1905
    invoke-static/range {p0 .. p0}, Landroid/support/v4/view/ViewCompat;->postInvalidateOnAnimation(Landroid/view/View;)V

    .line 1907
    :cond_6
    const/16 v22, 0x1

    goto :goto_0

    .line 1822
    :pswitch_1
    const/16 v22, 0x0

    move/from16 v0, v22

    move-object/from16 v1, p0

    iput-boolean v0, v1, Landroid/support/v4/view/FixedViewPager;->mPopulatePending:Z

    .line 1823
    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/view/FixedViewPager;->populate()V

    .line 1824
    const/16 v22, 0x0

    move/from16 v0, v22

    move-object/from16 v1, p0

    iput-boolean v0, v1, Landroid/support/v4/view/FixedViewPager;->mIsBeingDragged:Z

    .line 1825
    const/16 v22, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-direct {v0, v1}, Landroid/support/v4/view/FixedViewPager;->setScrollState(I)V

    .line 1828
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v22

    move/from16 v0, v22

    move-object/from16 v1, p0

    iput v0, v1, Landroid/support/v4/view/FixedViewPager;->mInitialMotionX:F

    move/from16 v0, v22

    move-object/from16 v1, p0

    iput v0, v1, Landroid/support/v4/view/FixedViewPager;->mLastMotionX:F

    .line 1829
    const/16 v22, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-static {v0, v1}, Landroid/support/v4/view/MotionEventCompat;->getPointerId(Landroid/view/MotionEvent;I)I

    move-result v22

    move/from16 v0, v22

    move-object/from16 v1, p0

    iput v0, v1, Landroid/support/v4/view/FixedViewPager;->mActivePointerId:I

    goto :goto_1

    .line 1833
    :pswitch_2
    move-object/from16 v0, p0

    iget-boolean v0, v0, Landroid/support/v4/view/FixedViewPager;->mIsBeingDragged:Z

    move/from16 v22, v0

    if-nez v22, :cond_7

    .line 1834
    move-object/from16 v0, p0

    iget v0, v0, Landroid/support/v4/view/FixedViewPager;->mActivePointerId:I

    move/from16 v22, v0

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-static {v0, v1}, Landroid/support/v4/view/MotionEventCompat;->findPointerIndex(Landroid/view/MotionEvent;I)I

    move-result v13

    .line 1835
    .local v13, "pointerIndex":I
    move-object/from16 v0, p1

    invoke-static {v0, v13}, Landroid/support/v4/view/MotionEventCompat;->getX(Landroid/view/MotionEvent;I)F

    move-result v18

    .line 1836
    .local v18, "x":F
    move-object/from16 v0, p0

    iget v0, v0, Landroid/support/v4/view/FixedViewPager;->mLastMotionX:F

    move/from16 v22, v0

    sub-float v22, v18, v22

    invoke-static/range {v22 .. v22}, Ljava/lang/Math;->abs(F)F

    move-result v19

    .line 1837
    .local v19, "xDiff":F
    move-object/from16 v0, p1

    invoke-static {v0, v13}, Landroid/support/v4/view/MotionEventCompat;->getY(Landroid/view/MotionEvent;I)F

    move-result v20

    .line 1838
    .local v20, "y":F
    move-object/from16 v0, p0

    iget v0, v0, Landroid/support/v4/view/FixedViewPager;->mLastMotionY:F

    move/from16 v22, v0

    sub-float v22, v20, v22

    invoke-static/range {v22 .. v22}, Ljava/lang/Math;->abs(F)F

    move-result v21

    .line 1840
    .local v21, "yDiff":F
    move-object/from16 v0, p0

    iget v0, v0, Landroid/support/v4/view/FixedViewPager;->mTouchSlop:I

    move/from16 v22, v0

    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v22, v0

    cmpl-float v22, v19, v22

    if-lez v22, :cond_7

    cmpl-float v22, v19, v21

    if-lez v22, :cond_7

    .line 1842
    const/16 v22, 0x1

    move/from16 v0, v22

    move-object/from16 v1, p0

    iput-boolean v0, v1, Landroid/support/v4/view/FixedViewPager;->mIsBeingDragged:Z

    .line 1843
    move-object/from16 v0, p0

    iget v0, v0, Landroid/support/v4/view/FixedViewPager;->mInitialMotionX:F

    move/from16 v22, v0

    sub-float v22, v18, v22

    const/16 v23, 0x0

    cmpl-float v22, v22, v23

    if-lez v22, :cond_8

    move-object/from16 v0, p0

    iget v0, v0, Landroid/support/v4/view/FixedViewPager;->mInitialMotionX:F

    move/from16 v22, v0

    move-object/from16 v0, p0

    iget v0, v0, Landroid/support/v4/view/FixedViewPager;->mTouchSlop:I

    move/from16 v23, v0

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    add-float v22, v22, v23

    :goto_2
    move/from16 v0, v22

    move-object/from16 v1, p0

    iput v0, v1, Landroid/support/v4/view/FixedViewPager;->mLastMotionX:F

    .line 1845
    const/16 v22, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-direct {v0, v1}, Landroid/support/v4/view/FixedViewPager;->setScrollState(I)V

    .line 1846
    const/16 v22, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-direct {v0, v1}, Landroid/support/v4/view/FixedViewPager;->setScrollingCacheEnabled(Z)V

    .line 1850
    .end local v13    # "pointerIndex":I
    .end local v18    # "x":F
    .end local v19    # "xDiff":F
    .end local v20    # "y":F
    .end local v21    # "yDiff":F
    :cond_7
    move-object/from16 v0, p0

    iget-boolean v0, v0, Landroid/support/v4/view/FixedViewPager;->mIsBeingDragged:Z

    move/from16 v22, v0

    if-eqz v22, :cond_5

    .line 1852
    move-object/from16 v0, p0

    iget v0, v0, Landroid/support/v4/view/FixedViewPager;->mActivePointerId:I

    move/from16 v22, v0

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-static {v0, v1}, Landroid/support/v4/view/MotionEventCompat;->findPointerIndex(Landroid/view/MotionEvent;I)I

    move-result v5

    .line 1854
    .local v5, "activePointerIndex":I
    move-object/from16 v0, p1

    invoke-static {v0, v5}, Landroid/support/v4/view/MotionEventCompat;->getX(Landroid/view/MotionEvent;I)F

    move-result v18

    .line 1855
    .restart local v18    # "x":F
    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-direct {v0, v1}, Landroid/support/v4/view/FixedViewPager;->performDrag(F)Z

    move-result v22

    or-int v10, v10, v22

    .line 1856
    goto/16 :goto_1

    .line 1843
    .end local v5    # "activePointerIndex":I
    .restart local v13    # "pointerIndex":I
    .restart local v19    # "xDiff":F
    .restart local v20    # "y":F
    .restart local v21    # "yDiff":F
    :cond_8
    move-object/from16 v0, p0

    iget v0, v0, Landroid/support/v4/view/FixedViewPager;->mInitialMotionX:F

    move/from16 v22, v0

    move-object/from16 v0, p0

    iget v0, v0, Landroid/support/v4/view/FixedViewPager;->mTouchSlop:I

    move/from16 v23, v0

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    sub-float v22, v22, v23

    goto :goto_2

    .line 1859
    .end local v13    # "pointerIndex":I
    .end local v18    # "x":F
    .end local v19    # "xDiff":F
    .end local v20    # "y":F
    .end local v21    # "yDiff":F
    :pswitch_3
    move-object/from16 v0, p0

    iget-boolean v0, v0, Landroid/support/v4/view/FixedViewPager;->mIsBeingDragged:Z

    move/from16 v22, v0

    if-eqz v22, :cond_5

    .line 1860
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/support/v4/view/FixedViewPager;->mVelocityTracker:Landroid/view/VelocityTracker;

    move-object/from16 v16, v0

    .line 1861
    .local v16, "velocityTracker":Landroid/view/VelocityTracker;
    const/16 v22, 0x3e8

    move-object/from16 v0, p0

    iget v0, v0, Landroid/support/v4/view/FixedViewPager;->mMaximumVelocity:I

    move/from16 v23, v0

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    move-object/from16 v0, v16

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    .line 1862
    move-object/from16 v0, p0

    iget v0, v0, Landroid/support/v4/view/FixedViewPager;->mActivePointerId:I

    move/from16 v22, v0

    move-object/from16 v0, v16

    move/from16 v1, v22

    invoke-static {v0, v1}, Landroid/support/v4/view/VelocityTrackerCompat;->getXVelocity(Landroid/view/VelocityTracker;I)F

    move-result v22

    move/from16 v0, v22

    float-to-int v9, v0

    .line 1864
    .local v9, "initialVelocity":I
    const/16 v22, 0x1

    move/from16 v0, v22

    move-object/from16 v1, p0

    iput-boolean v0, v1, Landroid/support/v4/view/FixedViewPager;->mPopulatePending:Z

    .line 1865
    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/view/FixedViewPager;->getWidth()I

    move-result v17

    .line 1866
    .local v17, "width":I
    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/view/FixedViewPager;->getScrollX()I

    move-result v14

    .line 1867
    .local v14, "scrollX":I
    invoke-direct/range {p0 .. p0}, Landroid/support/v4/view/FixedViewPager;->infoForCurrentScrollPosition()Landroid/support/v4/view/FixedViewPager$ItemInfo;

    move-result-object v7

    .line 1868
    .local v7, "ii":Landroid/support/v4/view/FixedViewPager$ItemInfo;
    iget v6, v7, Landroid/support/v4/view/FixedViewPager$ItemInfo;->position:I

    .line 1869
    .local v6, "currentPage":I
    int-to-float v0, v14

    move/from16 v22, v0

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v23, v0

    div-float v22, v22, v23

    iget v0, v7, Landroid/support/v4/view/FixedViewPager$ItemInfo;->offset:F

    move/from16 v23, v0

    sub-float v22, v22, v23

    iget v0, v7, Landroid/support/v4/view/FixedViewPager$ItemInfo;->widthFactor:F

    move/from16 v23, v0

    div-float v12, v22, v23

    .line 1870
    .local v12, "pageOffset":F
    move-object/from16 v0, p0

    iget v0, v0, Landroid/support/v4/view/FixedViewPager;->mActivePointerId:I

    move/from16 v22, v0

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-static {v0, v1}, Landroid/support/v4/view/MotionEventCompat;->findPointerIndex(Landroid/view/MotionEvent;I)I

    move-result v5

    .line 1872
    .restart local v5    # "activePointerIndex":I
    move-object/from16 v0, p1

    invoke-static {v0, v5}, Landroid/support/v4/view/MotionEventCompat;->getX(Landroid/view/MotionEvent;I)F

    move-result v18

    .line 1873
    .restart local v18    # "x":F
    move-object/from16 v0, p0

    iget v0, v0, Landroid/support/v4/view/FixedViewPager;->mInitialMotionX:F

    move/from16 v22, v0

    sub-float v22, v18, v22

    move/from16 v0, v22

    float-to-int v15, v0

    .line 1874
    .local v15, "totalDelta":I
    move-object/from16 v0, p0

    invoke-direct {v0, v6, v12, v9, v15}, Landroid/support/v4/view/FixedViewPager;->determineTargetPage(IFII)I

    move-result v11

    .line 1876
    .local v11, "nextPage":I
    const/16 v22, 0x1

    const/16 v23, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v0, v11, v1, v2, v9}, Landroid/support/v4/view/FixedViewPager;->setCurrentItemInternal(IZZI)V

    .line 1878
    const/16 v22, -0x1

    move/from16 v0, v22

    move-object/from16 v1, p0

    iput v0, v1, Landroid/support/v4/view/FixedViewPager;->mActivePointerId:I

    .line 1879
    invoke-direct/range {p0 .. p0}, Landroid/support/v4/view/FixedViewPager;->endDrag()V

    .line 1880
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/support/v4/view/FixedViewPager;->mLeftEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/support/v4/widget/EdgeEffectCompat;->onRelease()Z

    move-result v22

    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/support/v4/view/FixedViewPager;->mRightEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/support/v4/widget/EdgeEffectCompat;->onRelease()Z

    move-result v23

    or-int v10, v22, v23

    .line 1881
    goto/16 :goto_1

    .line 1884
    .end local v5    # "activePointerIndex":I
    .end local v6    # "currentPage":I
    .end local v7    # "ii":Landroid/support/v4/view/FixedViewPager$ItemInfo;
    .end local v9    # "initialVelocity":I
    .end local v11    # "nextPage":I
    .end local v12    # "pageOffset":F
    .end local v14    # "scrollX":I
    .end local v15    # "totalDelta":I
    .end local v16    # "velocityTracker":Landroid/view/VelocityTracker;
    .end local v17    # "width":I
    .end local v18    # "x":F
    :pswitch_4
    move-object/from16 v0, p0

    iget-boolean v0, v0, Landroid/support/v4/view/FixedViewPager;->mIsBeingDragged:Z

    move/from16 v22, v0

    if-eqz v22, :cond_5

    .line 1885
    move-object/from16 v0, p0

    iget v0, v0, Landroid/support/v4/view/FixedViewPager;->mCurItem:I

    move/from16 v22, v0

    const/16 v23, 0x1

    const/16 v24, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v22

    move/from16 v2, v23

    move/from16 v3, v24

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v4/view/FixedViewPager;->setCurrentItemInternal(IZZ)V

    .line 1886
    const/16 v22, -0x1

    move/from16 v0, v22

    move-object/from16 v1, p0

    iput v0, v1, Landroid/support/v4/view/FixedViewPager;->mActivePointerId:I

    .line 1887
    invoke-direct/range {p0 .. p0}, Landroid/support/v4/view/FixedViewPager;->endDrag()V

    .line 1888
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/support/v4/view/FixedViewPager;->mLeftEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/support/v4/widget/EdgeEffectCompat;->onRelease()Z

    move-result v22

    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/support/v4/view/FixedViewPager;->mRightEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/support/v4/widget/EdgeEffectCompat;->onRelease()Z

    move-result v23

    or-int v10, v22, v23

    goto/16 :goto_1

    .line 1892
    :pswitch_5
    invoke-static/range {p1 .. p1}, Landroid/support/v4/view/MotionEventCompat;->getActionIndex(Landroid/view/MotionEvent;)I

    move-result v8

    .line 1893
    .local v8, "index":I
    move-object/from16 v0, p1

    invoke-static {v0, v8}, Landroid/support/v4/view/MotionEventCompat;->getX(Landroid/view/MotionEvent;I)F

    move-result v18

    .line 1894
    .restart local v18    # "x":F
    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Landroid/support/v4/view/FixedViewPager;->mLastMotionX:F

    .line 1895
    move-object/from16 v0, p1

    invoke-static {v0, v8}, Landroid/support/v4/view/MotionEventCompat;->getPointerId(Landroid/view/MotionEvent;I)I

    move-result v22

    move/from16 v0, v22

    move-object/from16 v1, p0

    iput v0, v1, Landroid/support/v4/view/FixedViewPager;->mActivePointerId:I

    goto/16 :goto_1

    .line 1899
    .end local v8    # "index":I
    .end local v18    # "x":F
    :pswitch_6
    invoke-direct/range {p0 .. p1}, Landroid/support/v4/view/FixedViewPager;->onSecondaryPointerUp(Landroid/view/MotionEvent;)V

    .line 1900
    move-object/from16 v0, p0

    iget v0, v0, Landroid/support/v4/view/FixedViewPager;->mActivePointerId:I

    move/from16 v22, v0

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-static {v0, v1}, Landroid/support/v4/view/MotionEventCompat;->findPointerIndex(Landroid/view/MotionEvent;I)I

    move-result v22

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-static {v0, v1}, Landroid/support/v4/view/MotionEventCompat;->getX(Landroid/view/MotionEvent;I)F

    move-result v22

    move/from16 v0, v22

    move-object/from16 v1, p0

    iput v0, v1, Landroid/support/v4/view/FixedViewPager;->mLastMotionX:F

    goto/16 :goto_1

    .line 1820
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_4
        :pswitch_0
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method pageLeft()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 2443
    iget v1, p0, Landroid/support/v4/view/FixedViewPager;->mCurItem:I

    if-lez v1, :cond_0

    .line 2444
    iget v1, p0, Landroid/support/v4/view/FixedViewPager;->mCurItem:I

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p0, v1, v0}, Landroid/support/v4/view/FixedViewPager;->setCurrentItem(IZ)V

    .line 2447
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method pageRight()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 2451
    iget-object v1, p0, Landroid/support/v4/view/FixedViewPager;->mAdapter:Landroid/support/v4/view/FixedPagerAdapter;

    if-eqz v1, :cond_0

    iget v1, p0, Landroid/support/v4/view/FixedViewPager;->mCurItem:I

    iget-object v2, p0, Landroid/support/v4/view/FixedViewPager;->mAdapter:Landroid/support/v4/view/FixedPagerAdapter;

    invoke-virtual {v2}, Landroid/support/v4/view/FixedPagerAdapter;->getCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ge v1, v2, :cond_0

    .line 2452
    iget v1, p0, Landroid/support/v4/view/FixedViewPager;->mCurItem:I

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {p0, v1, v0}, Landroid/support/v4/view/FixedViewPager;->setCurrentItem(IZ)V

    .line 2455
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method populate()V
    .locals 1

    .prologue
    .line 876
    iget v0, p0, Landroid/support/v4/view/FixedViewPager;->mCurItem:I

    invoke-virtual {p0, v0}, Landroid/support/v4/view/FixedViewPager;->populate(I)V

    .line 877
    return-void
.end method

.method populate(I)V
    .locals 25
    .param p1, "newCurrentItem"    # I

    .prologue
    .line 880
    const/16 v18, 0x0

    .line 881
    .local v18, "oldCurInfo":Landroid/support/v4/view/FixedViewPager$ItemInfo;
    move-object/from16 v0, p0

    iget v0, v0, Landroid/support/v4/view/FixedViewPager;->mCurItem:I

    move/from16 v22, v0

    move/from16 v0, v22

    move/from16 v1, p1

    if-eq v0, v1, :cond_0

    .line 882
    move-object/from16 v0, p0

    iget v0, v0, Landroid/support/v4/view/FixedViewPager;->mCurItem:I

    move/from16 v22, v0

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/support/v4/view/FixedViewPager;->infoForPosition(I)Landroid/support/v4/view/FixedViewPager$ItemInfo;

    move-result-object v18

    .line 883
    move/from16 v0, p1

    move-object/from16 v1, p0

    iput v0, v1, Landroid/support/v4/view/FixedViewPager;->mCurItem:I

    .line 886
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/support/v4/view/FixedViewPager;->mAdapter:Landroid/support/v4/view/FixedPagerAdapter;

    move-object/from16 v22, v0

    if-nez v22, :cond_2

    .line 1030
    :cond_1
    return-void

    .line 894
    :cond_2
    move-object/from16 v0, p0

    iget-boolean v0, v0, Landroid/support/v4/view/FixedViewPager;->mPopulatePending:Z

    move/from16 v22, v0

    if-nez v22, :cond_1

    .line 902
    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/view/FixedViewPager;->getWindowToken()Landroid/os/IBinder;

    move-result-object v22

    if-eqz v22, :cond_1

    .line 906
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/support/v4/view/FixedViewPager;->mAdapter:Landroid/support/v4/view/FixedPagerAdapter;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Landroid/support/v4/view/FixedPagerAdapter;->startUpdate(Landroid/view/ViewGroup;)V

    .line 908
    move-object/from16 v0, p0

    iget v0, v0, Landroid/support/v4/view/FixedViewPager;->mOffscreenPageLimit:I

    move/from16 v19, v0

    .line 909
    .local v19, "pageLimit":I
    const/16 v22, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Landroid/support/v4/view/FixedViewPager;->mCurItem:I

    move/from16 v23, v0

    sub-int v23, v23, v19

    invoke-static/range {v22 .. v23}, Ljava/lang/Math;->max(II)I

    move-result v21

    .line 910
    .local v21, "startPos":I
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/support/v4/view/FixedViewPager;->mAdapter:Landroid/support/v4/view/FixedPagerAdapter;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/support/v4/view/FixedPagerAdapter;->getCount()I

    move-result v4

    .line 911
    .local v4, "N":I
    add-int/lit8 v22, v4, -0x1

    move-object/from16 v0, p0

    iget v0, v0, Landroid/support/v4/view/FixedViewPager;->mCurItem:I

    move/from16 v23, v0

    add-int v23, v23, v19

    invoke-static/range {v22 .. v23}, Ljava/lang/Math;->min(II)I

    move-result v10

    .line 914
    .local v10, "endPos":I
    const/4 v7, -0x1

    .line 915
    .local v7, "curIndex":I
    const/4 v8, 0x0

    .line 916
    .local v8, "curItem":Landroid/support/v4/view/FixedViewPager$ItemInfo;
    const/4 v7, 0x0

    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/support/v4/view/FixedViewPager;->mItems:Ljava/util/ArrayList;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Ljava/util/ArrayList;->size()I

    move-result v22

    move/from16 v0, v22

    if-ge v7, v0, :cond_3

    .line 917
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/support/v4/view/FixedViewPager;->mItems:Ljava/util/ArrayList;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Landroid/support/v4/view/FixedViewPager$ItemInfo;

    .line 918
    .local v14, "ii":Landroid/support/v4/view/FixedViewPager$ItemInfo;
    iget v0, v14, Landroid/support/v4/view/FixedViewPager$ItemInfo;->position:I

    move/from16 v22, v0

    move-object/from16 v0, p0

    iget v0, v0, Landroid/support/v4/view/FixedViewPager;->mCurItem:I

    move/from16 v23, v0

    move/from16 v0, v22

    move/from16 v1, v23

    if-lt v0, v1, :cond_9

    .line 919
    iget v0, v14, Landroid/support/v4/view/FixedViewPager$ItemInfo;->position:I

    move/from16 v22, v0

    move-object/from16 v0, p0

    iget v0, v0, Landroid/support/v4/view/FixedViewPager;->mCurItem:I

    move/from16 v23, v0

    move/from16 v0, v22

    move/from16 v1, v23

    if-ne v0, v1, :cond_3

    move-object v8, v14

    .line 924
    .end local v14    # "ii":Landroid/support/v4/view/FixedViewPager$ItemInfo;
    :cond_3
    if-nez v8, :cond_4

    if-lez v4, :cond_4

    .line 925
    move-object/from16 v0, p0

    iget v0, v0, Landroid/support/v4/view/FixedViewPager;->mCurItem:I

    move/from16 v22, v0

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-virtual {v0, v1, v7}, Landroid/support/v4/view/FixedViewPager;->addNewItem(II)Landroid/support/v4/view/FixedViewPager$ItemInfo;

    move-result-object v8

    .line 931
    :cond_4
    if-eqz v8, :cond_7

    .line 932
    const/4 v11, 0x0

    .line 933
    .local v11, "extraWidthLeft":F
    add-int/lit8 v15, v7, -0x1

    .line 934
    .local v15, "itemIndex":I
    if-ltz v15, :cond_a

    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/support/v4/view/FixedViewPager;->mItems:Ljava/util/ArrayList;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-virtual {v0, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Landroid/support/v4/view/FixedViewPager$ItemInfo;

    move-object/from16 v14, v22

    .line 935
    .restart local v14    # "ii":Landroid/support/v4/view/FixedViewPager$ItemInfo;
    :goto_1
    const/high16 v22, 0x40000000    # 2.0f

    iget v0, v8, Landroid/support/v4/view/FixedViewPager$ItemInfo;->widthFactor:F

    move/from16 v23, v0

    sub-float v16, v22, v23

    .line 936
    .local v16, "leftWidthNeeded":F
    move-object/from16 v0, p0

    iget v0, v0, Landroid/support/v4/view/FixedViewPager;->mCurItem:I

    move/from16 v22, v0

    add-int/lit8 v20, v22, -0x1

    .local v20, "pos":I
    :goto_2
    if-ltz v20, :cond_5

    .line 937
    cmpl-float v22, v11, v16

    if-ltz v22, :cond_e

    move/from16 v0, v20

    move/from16 v1, v21

    if-ge v0, v1, :cond_e

    .line 938
    if-nez v14, :cond_b

    .line 960
    :cond_5
    iget v12, v8, Landroid/support/v4/view/FixedViewPager$ItemInfo;->widthFactor:F

    .line 961
    .local v12, "extraWidthRight":F
    add-int/lit8 v15, v7, 0x1

    .line 962
    const/high16 v22, 0x40000000    # 2.0f

    cmpg-float v22, v12, v22

    if-gez v22, :cond_6

    .line 963
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/support/v4/view/FixedViewPager;->mItems:Ljava/util/ArrayList;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Ljava/util/ArrayList;->size()I

    move-result v22

    move/from16 v0, v22

    if-ge v15, v0, :cond_12

    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/support/v4/view/FixedViewPager;->mItems:Ljava/util/ArrayList;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-virtual {v0, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Landroid/support/v4/view/FixedViewPager$ItemInfo;

    move-object/from16 v14, v22

    .line 964
    :goto_3
    move-object/from16 v0, p0

    iget v0, v0, Landroid/support/v4/view/FixedViewPager;->mCurItem:I

    move/from16 v22, v0

    add-int/lit8 v20, v22, 0x1

    :goto_4
    move/from16 v0, v20

    if-ge v0, v4, :cond_6

    .line 965
    const/high16 v22, 0x40000000    # 2.0f

    cmpl-float v22, v12, v22

    if-ltz v22, :cond_16

    move/from16 v0, v20

    if-le v0, v10, :cond_16

    .line 966
    if-nez v14, :cond_13

    .line 987
    :cond_6
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v8, v7, v1}, Landroid/support/v4/view/FixedViewPager;->calculatePageOffsets(Landroid/support/v4/view/FixedViewPager$ItemInfo;ILandroid/support/v4/view/FixedViewPager$ItemInfo;)V

    .line 997
    .end local v11    # "extraWidthLeft":F
    .end local v12    # "extraWidthRight":F
    .end local v14    # "ii":Landroid/support/v4/view/FixedViewPager$ItemInfo;
    .end local v15    # "itemIndex":I
    .end local v16    # "leftWidthNeeded":F
    .end local v20    # "pos":I
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/support/v4/view/FixedViewPager;->mAdapter:Landroid/support/v4/view/FixedPagerAdapter;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget v0, v0, Landroid/support/v4/view/FixedViewPager;->mCurItem:I

    move/from16 v24, v0

    if-eqz v8, :cond_1a

    iget-object v0, v8, Landroid/support/v4/view/FixedViewPager$ItemInfo;->object:Ljava/lang/Object;

    move-object/from16 v22, v0

    :goto_5
    move-object/from16 v0, v23

    move-object/from16 v1, p0

    move/from16 v2, v24

    move-object/from16 v3, v22

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v4/view/FixedPagerAdapter;->setPrimaryItem(Landroid/view/ViewGroup;ILjava/lang/Object;)V

    .line 999
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/support/v4/view/FixedViewPager;->mAdapter:Landroid/support/v4/view/FixedPagerAdapter;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Landroid/support/v4/view/FixedPagerAdapter;->finishUpdate(Landroid/view/ViewGroup;)V

    .line 1002
    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/view/FixedViewPager;->getChildCount()I

    move-result v6

    .line 1003
    .local v6, "childCount":I
    const/4 v13, 0x0

    .local v13, "i":I
    :goto_6
    if-ge v13, v6, :cond_1b

    .line 1004
    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Landroid/support/v4/view/FixedViewPager;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    .line 1005
    .local v5, "child":Landroid/view/View;
    invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v17

    check-cast v17, Landroid/support/v4/view/FixedViewPager$LayoutParams;

    .line 1006
    .local v17, "lp":Landroid/support/v4/view/FixedViewPager$LayoutParams;
    move-object/from16 v0, v17

    iget-boolean v0, v0, Landroid/support/v4/view/FixedViewPager$LayoutParams;->isDecor:Z

    move/from16 v22, v0

    if-nez v22, :cond_8

    move-object/from16 v0, v17

    iget v0, v0, Landroid/support/v4/view/FixedViewPager$LayoutParams;->widthFactor:F

    move/from16 v22, v0

    const/16 v23, 0x0

    cmpl-float v22, v22, v23

    if-nez v22, :cond_8

    .line 1008
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Landroid/support/v4/view/FixedViewPager;->infoForChild(Landroid/view/View;)Landroid/support/v4/view/FixedViewPager$ItemInfo;

    move-result-object v14

    .line 1009
    .restart local v14    # "ii":Landroid/support/v4/view/FixedViewPager$ItemInfo;
    if-eqz v14, :cond_8

    .line 1010
    iget v0, v14, Landroid/support/v4/view/FixedViewPager$ItemInfo;->widthFactor:F

    move/from16 v22, v0

    move/from16 v0, v22

    move-object/from16 v1, v17

    iput v0, v1, Landroid/support/v4/view/FixedViewPager$LayoutParams;->widthFactor:F

    .line 1003
    .end local v14    # "ii":Landroid/support/v4/view/FixedViewPager$ItemInfo;
    :cond_8
    add-int/lit8 v13, v13, 0x1

    goto :goto_6

    .line 916
    .end local v5    # "child":Landroid/view/View;
    .end local v6    # "childCount":I
    .end local v13    # "i":I
    .end local v17    # "lp":Landroid/support/v4/view/FixedViewPager$LayoutParams;
    .restart local v14    # "ii":Landroid/support/v4/view/FixedViewPager$ItemInfo;
    :cond_9
    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_0

    .line 934
    .end local v14    # "ii":Landroid/support/v4/view/FixedViewPager$ItemInfo;
    .restart local v11    # "extraWidthLeft":F
    .restart local v15    # "itemIndex":I
    :cond_a
    const/4 v14, 0x0

    goto/16 :goto_1

    .line 941
    .restart local v14    # "ii":Landroid/support/v4/view/FixedViewPager$ItemInfo;
    .restart local v16    # "leftWidthNeeded":F
    .restart local v20    # "pos":I
    :cond_b
    iget v0, v14, Landroid/support/v4/view/FixedViewPager$ItemInfo;->position:I

    move/from16 v22, v0

    move/from16 v0, v20

    move/from16 v1, v22

    if-ne v0, v1, :cond_c

    iget-boolean v0, v14, Landroid/support/v4/view/FixedViewPager$ItemInfo;->scrolling:Z

    move/from16 v22, v0

    if-nez v22, :cond_c

    .line 942
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/support/v4/view/FixedViewPager;->mItems:Ljava/util/ArrayList;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-virtual {v0, v15}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 943
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/support/v4/view/FixedViewPager;->mAdapter:Landroid/support/v4/view/FixedPagerAdapter;

    move-object/from16 v22, v0

    iget-object v0, v14, Landroid/support/v4/view/FixedViewPager$ItemInfo;->object:Ljava/lang/Object;

    move-object/from16 v23, v0

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    move/from16 v2, v20

    move-object/from16 v3, v23

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v4/view/FixedPagerAdapter;->destroyItem(Landroid/view/ViewGroup;ILjava/lang/Object;)V

    .line 944
    add-int/lit8 v15, v15, -0x1

    .line 945
    add-int/lit8 v7, v7, -0x1

    .line 946
    if-ltz v15, :cond_d

    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/support/v4/view/FixedViewPager;->mItems:Ljava/util/ArrayList;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-virtual {v0, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Landroid/support/v4/view/FixedViewPager$ItemInfo;

    move-object/from16 v14, v22

    .line 936
    :cond_c
    :goto_7
    add-int/lit8 v20, v20, -0x1

    goto/16 :goto_2

    .line 946
    :cond_d
    const/4 v14, 0x0

    goto :goto_7

    .line 948
    :cond_e
    if-eqz v14, :cond_10

    iget v0, v14, Landroid/support/v4/view/FixedViewPager$ItemInfo;->position:I

    move/from16 v22, v0

    move/from16 v0, v20

    move/from16 v1, v22

    if-ne v0, v1, :cond_10

    .line 949
    iget v0, v14, Landroid/support/v4/view/FixedViewPager$ItemInfo;->widthFactor:F

    move/from16 v22, v0

    add-float v11, v11, v22

    .line 950
    add-int/lit8 v15, v15, -0x1

    .line 951
    if-ltz v15, :cond_f

    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/support/v4/view/FixedViewPager;->mItems:Ljava/util/ArrayList;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-virtual {v0, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Landroid/support/v4/view/FixedViewPager$ItemInfo;

    move-object/from16 v14, v22

    :goto_8
    goto :goto_7

    :cond_f
    const/4 v14, 0x0

    goto :goto_8

    .line 953
    :cond_10
    add-int/lit8 v22, v15, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v20

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/view/FixedViewPager;->addNewItem(II)Landroid/support/v4/view/FixedViewPager$ItemInfo;

    move-result-object v14

    .line 954
    iget v0, v14, Landroid/support/v4/view/FixedViewPager$ItemInfo;->widthFactor:F

    move/from16 v22, v0

    add-float v11, v11, v22

    .line 955
    add-int/lit8 v7, v7, 0x1

    .line 956
    if-ltz v15, :cond_11

    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/support/v4/view/FixedViewPager;->mItems:Ljava/util/ArrayList;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-virtual {v0, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Landroid/support/v4/view/FixedViewPager$ItemInfo;

    move-object/from16 v14, v22

    :goto_9
    goto :goto_7

    :cond_11
    const/4 v14, 0x0

    goto :goto_9

    .line 963
    .restart local v12    # "extraWidthRight":F
    :cond_12
    const/4 v14, 0x0

    goto/16 :goto_3

    .line 969
    :cond_13
    iget v0, v14, Landroid/support/v4/view/FixedViewPager$ItemInfo;->position:I

    move/from16 v22, v0

    move/from16 v0, v20

    move/from16 v1, v22

    if-ne v0, v1, :cond_14

    iget-boolean v0, v14, Landroid/support/v4/view/FixedViewPager$ItemInfo;->scrolling:Z

    move/from16 v22, v0

    if-nez v22, :cond_14

    .line 970
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/support/v4/view/FixedViewPager;->mItems:Ljava/util/ArrayList;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-virtual {v0, v15}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 971
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/support/v4/view/FixedViewPager;->mAdapter:Landroid/support/v4/view/FixedPagerAdapter;

    move-object/from16 v22, v0

    iget-object v0, v14, Landroid/support/v4/view/FixedViewPager$ItemInfo;->object:Ljava/lang/Object;

    move-object/from16 v23, v0

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    move/from16 v2, v20

    move-object/from16 v3, v23

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v4/view/FixedPagerAdapter;->destroyItem(Landroid/view/ViewGroup;ILjava/lang/Object;)V

    .line 972
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/support/v4/view/FixedViewPager;->mItems:Ljava/util/ArrayList;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Ljava/util/ArrayList;->size()I

    move-result v22

    move/from16 v0, v22

    if-ge v15, v0, :cond_15

    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/support/v4/view/FixedViewPager;->mItems:Ljava/util/ArrayList;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-virtual {v0, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Landroid/support/v4/view/FixedViewPager$ItemInfo;

    move-object/from16 v14, v22

    .line 964
    :cond_14
    :goto_a
    add-int/lit8 v20, v20, 0x1

    goto/16 :goto_4

    .line 972
    :cond_15
    const/4 v14, 0x0

    goto :goto_a

    .line 974
    :cond_16
    if-eqz v14, :cond_18

    iget v0, v14, Landroid/support/v4/view/FixedViewPager$ItemInfo;->position:I

    move/from16 v22, v0

    move/from16 v0, v20

    move/from16 v1, v22

    if-ne v0, v1, :cond_18

    .line 975
    iget v0, v14, Landroid/support/v4/view/FixedViewPager$ItemInfo;->widthFactor:F

    move/from16 v22, v0

    add-float v12, v12, v22

    .line 976
    add-int/lit8 v15, v15, 0x1

    .line 977
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/support/v4/view/FixedViewPager;->mItems:Ljava/util/ArrayList;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Ljava/util/ArrayList;->size()I

    move-result v22

    move/from16 v0, v22

    if-ge v15, v0, :cond_17

    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/support/v4/view/FixedViewPager;->mItems:Ljava/util/ArrayList;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-virtual {v0, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Landroid/support/v4/view/FixedViewPager$ItemInfo;

    move-object/from16 v14, v22

    :goto_b
    goto :goto_a

    :cond_17
    const/4 v14, 0x0

    goto :goto_b

    .line 979
    :cond_18
    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v1, v15}, Landroid/support/v4/view/FixedViewPager;->addNewItem(II)Landroid/support/v4/view/FixedViewPager$ItemInfo;

    move-result-object v14

    .line 980
    add-int/lit8 v15, v15, 0x1

    .line 981
    iget v0, v14, Landroid/support/v4/view/FixedViewPager$ItemInfo;->widthFactor:F

    move/from16 v22, v0

    add-float v12, v12, v22

    .line 982
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/support/v4/view/FixedViewPager;->mItems:Ljava/util/ArrayList;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Ljava/util/ArrayList;->size()I

    move-result v22

    move/from16 v0, v22

    if-ge v15, v0, :cond_19

    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/support/v4/view/FixedViewPager;->mItems:Ljava/util/ArrayList;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-virtual {v0, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Landroid/support/v4/view/FixedViewPager$ItemInfo;

    move-object/from16 v14, v22

    :goto_c
    goto :goto_a

    :cond_19
    const/4 v14, 0x0

    goto :goto_c

    .line 997
    .end local v11    # "extraWidthLeft":F
    .end local v12    # "extraWidthRight":F
    .end local v14    # "ii":Landroid/support/v4/view/FixedViewPager$ItemInfo;
    .end local v15    # "itemIndex":I
    .end local v16    # "leftWidthNeeded":F
    .end local v20    # "pos":I
    :cond_1a
    const/16 v22, 0x0

    goto/16 :goto_5

    .line 1015
    .restart local v6    # "childCount":I
    .restart local v13    # "i":I
    :cond_1b
    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/view/FixedViewPager;->hasFocus()Z

    move-result v22

    if-eqz v22, :cond_1

    .line 1016
    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/view/FixedViewPager;->findFocus()Landroid/view/View;

    move-result-object v9

    .line 1017
    .local v9, "currentFocused":Landroid/view/View;
    if-eqz v9, :cond_1e

    move-object/from16 v0, p0

    invoke-virtual {v0, v9}, Landroid/support/v4/view/FixedViewPager;->infoForAnyChild(Landroid/view/View;)Landroid/support/v4/view/FixedViewPager$ItemInfo;

    move-result-object v14

    .line 1018
    .restart local v14    # "ii":Landroid/support/v4/view/FixedViewPager$ItemInfo;
    :goto_d
    if-eqz v14, :cond_1c

    iget v0, v14, Landroid/support/v4/view/FixedViewPager$ItemInfo;->position:I

    move/from16 v22, v0

    move-object/from16 v0, p0

    iget v0, v0, Landroid/support/v4/view/FixedViewPager;->mCurItem:I

    move/from16 v23, v0

    move/from16 v0, v22

    move/from16 v1, v23

    if-eq v0, v1, :cond_1

    .line 1019
    :cond_1c
    const/4 v13, 0x0

    :goto_e
    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/view/FixedViewPager;->getChildCount()I

    move-result v22

    move/from16 v0, v22

    if-ge v13, v0, :cond_1

    .line 1020
    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Landroid/support/v4/view/FixedViewPager;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    .line 1021
    .restart local v5    # "child":Landroid/view/View;
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Landroid/support/v4/view/FixedViewPager;->infoForChild(Landroid/view/View;)Landroid/support/v4/view/FixedViewPager$ItemInfo;

    move-result-object v14

    .line 1022
    if-eqz v14, :cond_1d

    iget v0, v14, Landroid/support/v4/view/FixedViewPager$ItemInfo;->position:I

    move/from16 v22, v0

    move-object/from16 v0, p0

    iget v0, v0, Landroid/support/v4/view/FixedViewPager;->mCurItem:I

    move/from16 v23, v0

    move/from16 v0, v22

    move/from16 v1, v23

    if-ne v0, v1, :cond_1d

    .line 1023
    const/16 v22, 0x2

    move/from16 v0, v22

    invoke-virtual {v5, v0}, Landroid/view/View;->requestFocus(I)Z

    move-result v22

    if-nez v22, :cond_1

    .line 1019
    :cond_1d
    add-int/lit8 v13, v13, 0x1

    goto :goto_e

    .line 1017
    .end local v5    # "child":Landroid/view/View;
    .end local v14    # "ii":Landroid/support/v4/view/FixedViewPager$ItemInfo;
    :cond_1e
    const/4 v14, 0x0

    goto :goto_d
.end method

.method public setAdapter(Landroid/support/v4/view/FixedPagerAdapter;)V
    .locals 9
    .param p1, "adapter"    # Landroid/support/v4/view/FixedPagerAdapter;

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 355
    iget-object v3, p0, Landroid/support/v4/view/FixedViewPager;->mAdapter:Landroid/support/v4/view/FixedPagerAdapter;

    if-eqz v3, :cond_1

    .line 356
    iget-object v3, p0, Landroid/support/v4/view/FixedViewPager;->mAdapter:Landroid/support/v4/view/FixedPagerAdapter;

    iget-object v4, p0, Landroid/support/v4/view/FixedViewPager;->mObserver:Landroid/support/v4/view/FixedViewPager$PagerObserver;

    invoke-virtual {v3, v4}, Landroid/support/v4/view/FixedPagerAdapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 357
    iget-object v3, p0, Landroid/support/v4/view/FixedViewPager;->mAdapter:Landroid/support/v4/view/FixedPagerAdapter;

    invoke-virtual {v3, p0}, Landroid/support/v4/view/FixedPagerAdapter;->startUpdate(Landroid/view/ViewGroup;)V

    .line 358
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v3, p0, Landroid/support/v4/view/FixedViewPager;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v0, v3, :cond_0

    .line 359
    iget-object v3, p0, Landroid/support/v4/view/FixedViewPager;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v4/view/FixedViewPager$ItemInfo;

    .line 360
    .local v1, "ii":Landroid/support/v4/view/FixedViewPager$ItemInfo;
    iget-object v3, p0, Landroid/support/v4/view/FixedViewPager;->mAdapter:Landroid/support/v4/view/FixedPagerAdapter;

    iget v4, v1, Landroid/support/v4/view/FixedViewPager$ItemInfo;->position:I

    iget-object v5, v1, Landroid/support/v4/view/FixedViewPager$ItemInfo;->object:Ljava/lang/Object;

    invoke-virtual {v3, p0, v4, v5}, Landroid/support/v4/view/FixedPagerAdapter;->destroyItem(Landroid/view/ViewGroup;ILjava/lang/Object;)V

    .line 358
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 362
    .end local v1    # "ii":Landroid/support/v4/view/FixedViewPager$ItemInfo;
    :cond_0
    iget-object v3, p0, Landroid/support/v4/view/FixedViewPager;->mAdapter:Landroid/support/v4/view/FixedPagerAdapter;

    invoke-virtual {v3, p0}, Landroid/support/v4/view/FixedPagerAdapter;->finishUpdate(Landroid/view/ViewGroup;)V

    .line 363
    iget-object v3, p0, Landroid/support/v4/view/FixedViewPager;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 364
    invoke-direct {p0}, Landroid/support/v4/view/FixedViewPager;->removeNonDecorViews()V

    .line 365
    iput v6, p0, Landroid/support/v4/view/FixedViewPager;->mCurItem:I

    .line 366
    invoke-virtual {p0, v6, v6}, Landroid/support/v4/view/FixedViewPager;->scrollTo(II)V

    .line 369
    .end local v0    # "i":I
    :cond_1
    iget-object v2, p0, Landroid/support/v4/view/FixedViewPager;->mAdapter:Landroid/support/v4/view/FixedPagerAdapter;

    .line 370
    .local v2, "oldAdapter":Landroid/support/v4/view/PagerAdapter;
    iput-object p1, p0, Landroid/support/v4/view/FixedViewPager;->mAdapter:Landroid/support/v4/view/FixedPagerAdapter;

    .line 372
    iget-object v3, p0, Landroid/support/v4/view/FixedViewPager;->mAdapter:Landroid/support/v4/view/FixedPagerAdapter;

    if-eqz v3, :cond_3

    .line 373
    iget-object v3, p0, Landroid/support/v4/view/FixedViewPager;->mObserver:Landroid/support/v4/view/FixedViewPager$PagerObserver;

    if-nez v3, :cond_2

    .line 374
    new-instance v3, Landroid/support/v4/view/FixedViewPager$PagerObserver;

    invoke-direct {v3, p0, v7}, Landroid/support/v4/view/FixedViewPager$PagerObserver;-><init>(Landroid/support/v4/view/FixedViewPager;Landroid/support/v4/view/FixedViewPager$1;)V

    iput-object v3, p0, Landroid/support/v4/view/FixedViewPager;->mObserver:Landroid/support/v4/view/FixedViewPager$PagerObserver;

    .line 376
    :cond_2
    iget-object v3, p0, Landroid/support/v4/view/FixedViewPager;->mAdapter:Landroid/support/v4/view/FixedPagerAdapter;

    invoke-virtual {v3}, Landroid/support/v4/view/FixedPagerAdapter;->getCount()I

    move-result v3

    iput v3, p0, Landroid/support/v4/view/FixedViewPager;->mPreviouseDataCount:I

    .line 377
    iget-object v3, p0, Landroid/support/v4/view/FixedViewPager;->mAdapter:Landroid/support/v4/view/FixedPagerAdapter;

    iget-object v4, p0, Landroid/support/v4/view/FixedViewPager;->mObserver:Landroid/support/v4/view/FixedViewPager$PagerObserver;

    invoke-virtual {v3, v4}, Landroid/support/v4/view/FixedPagerAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 378
    iput-boolean v6, p0, Landroid/support/v4/view/FixedViewPager;->mPopulatePending:Z

    .line 379
    iput-boolean v8, p0, Landroid/support/v4/view/FixedViewPager;->mFirstLayout:Z

    .line 380
    iget v3, p0, Landroid/support/v4/view/FixedViewPager;->mRestoredCurItem:I

    if-ltz v3, :cond_5

    .line 381
    iget-object v3, p0, Landroid/support/v4/view/FixedViewPager;->mAdapter:Landroid/support/v4/view/FixedPagerAdapter;

    iget-object v4, p0, Landroid/support/v4/view/FixedViewPager;->mRestoredAdapterState:Landroid/os/Parcelable;

    iget-object v5, p0, Landroid/support/v4/view/FixedViewPager;->mRestoredClassLoader:Ljava/lang/ClassLoader;

    invoke-virtual {v3, v4, v5}, Landroid/support/v4/view/FixedPagerAdapter;->restoreState(Landroid/os/Parcelable;Ljava/lang/ClassLoader;)V

    .line 382
    iget v3, p0, Landroid/support/v4/view/FixedViewPager;->mRestoredCurItem:I

    invoke-virtual {p0, v3, v6, v8}, Landroid/support/v4/view/FixedViewPager;->setCurrentItemInternal(IZZ)V

    .line 383
    const/4 v3, -0x1

    iput v3, p0, Landroid/support/v4/view/FixedViewPager;->mRestoredCurItem:I

    .line 384
    iput-object v7, p0, Landroid/support/v4/view/FixedViewPager;->mRestoredAdapterState:Landroid/os/Parcelable;

    .line 385
    iput-object v7, p0, Landroid/support/v4/view/FixedViewPager;->mRestoredClassLoader:Ljava/lang/ClassLoader;

    .line 391
    :cond_3
    :goto_1
    iget-object v3, p0, Landroid/support/v4/view/FixedViewPager;->mAdapterChangeListener:Landroid/support/v4/view/FixedViewPager$OnAdapterChangeListener;

    if-eqz v3, :cond_4

    if-eq v2, p1, :cond_4

    .line 392
    iget-object v3, p0, Landroid/support/v4/view/FixedViewPager;->mAdapterChangeListener:Landroid/support/v4/view/FixedViewPager$OnAdapterChangeListener;

    invoke-interface {v3, v2, p1}, Landroid/support/v4/view/FixedViewPager$OnAdapterChangeListener;->onAdapterChanged(Landroid/support/v4/view/PagerAdapter;Landroid/support/v4/view/PagerAdapter;)V

    .line 394
    :cond_4
    return-void

    .line 387
    :cond_5
    invoke-virtual {p0}, Landroid/support/v4/view/FixedViewPager;->populate()V

    goto :goto_1
.end method

.method public setCurrentItem(I)V
    .locals 2
    .param p1, "item"    # I

    .prologue
    const/4 v1, 0x0

    .line 428
    iput-boolean v1, p0, Landroid/support/v4/view/FixedViewPager;->mPopulatePending:Z

    .line 429
    iget-boolean v0, p0, Landroid/support/v4/view/FixedViewPager;->mFirstLayout:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, p1, v0, v1}, Landroid/support/v4/view/FixedViewPager;->setCurrentItemInternal(IZZ)V

    .line 430
    return-void

    :cond_0
    move v0, v1

    .line 429
    goto :goto_0
.end method

.method public setCurrentItem(IZ)V
    .locals 1
    .param p1, "item"    # I
    .param p2, "smoothScroll"    # Z

    .prologue
    const/4 v0, 0x0

    .line 439
    iput-boolean v0, p0, Landroid/support/v4/view/FixedViewPager;->mPopulatePending:Z

    .line 440
    invoke-virtual {p0, p1, p2, v0}, Landroid/support/v4/view/FixedViewPager;->setCurrentItemInternal(IZZ)V

    .line 441
    return-void
.end method

.method setCurrentItemInternal(IZZ)V
    .locals 1
    .param p1, "item"    # I
    .param p2, "smoothScroll"    # Z
    .param p3, "always"    # Z

    .prologue
    .line 448
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, p3, v0}, Landroid/support/v4/view/FixedViewPager;->setCurrentItemInternal(IZZI)V

    .line 449
    return-void
.end method

.method setCurrentItemInternal(IZZI)V
    .locals 11
    .param p1, "item"    # I
    .param p2, "smoothScroll"    # Z
    .param p3, "always"    # Z
    .param p4, "velocity"    # I

    .prologue
    const/4 v2, 0x1

    const/4 v7, 0x0

    .line 452
    iget-object v6, p0, Landroid/support/v4/view/FixedViewPager;->mAdapter:Landroid/support/v4/view/FixedPagerAdapter;

    if-eqz v6, :cond_0

    iget-object v6, p0, Landroid/support/v4/view/FixedViewPager;->mAdapter:Landroid/support/v4/view/FixedPagerAdapter;

    invoke-virtual {v6}, Landroid/support/v4/view/FixedPagerAdapter;->getCount()I

    move-result v6

    if-gtz v6, :cond_2

    .line 453
    :cond_0
    invoke-direct {p0, v7}, Landroid/support/v4/view/FixedViewPager;->setScrollingCacheEnabled(Z)V

    .line 502
    :cond_1
    :goto_0
    return-void

    .line 456
    :cond_2
    if-nez p3, :cond_3

    iget v6, p0, Landroid/support/v4/view/FixedViewPager;->mCurItem:I

    if-ne v6, p1, :cond_3

    iget-object v6, p0, Landroid/support/v4/view/FixedViewPager;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-eqz v6, :cond_3

    .line 457
    invoke-direct {p0, v7}, Landroid/support/v4/view/FixedViewPager;->setScrollingCacheEnabled(Z)V

    goto :goto_0

    .line 461
    :cond_3
    if-gez p1, :cond_6

    .line 462
    const/4 p1, 0x0

    .line 466
    :cond_4
    :goto_1
    iget v4, p0, Landroid/support/v4/view/FixedViewPager;->mOffscreenPageLimit:I

    .line 467
    .local v4, "pageLimit":I
    iget v6, p0, Landroid/support/v4/view/FixedViewPager;->mCurItem:I

    add-int/2addr v6, v4

    if-gt p1, v6, :cond_5

    iget v6, p0, Landroid/support/v4/view/FixedViewPager;->mCurItem:I

    sub-int/2addr v6, v4

    if-ge p1, v6, :cond_7

    .line 471
    :cond_5
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_2
    iget-object v6, p0, Landroid/support/v4/view/FixedViewPager;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-ge v3, v6, :cond_7

    .line 472
    iget-object v6, p0, Landroid/support/v4/view/FixedViewPager;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/support/v4/view/FixedViewPager$ItemInfo;

    iput-boolean v2, v6, Landroid/support/v4/view/FixedViewPager$ItemInfo;->scrolling:Z

    .line 471
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 463
    .end local v3    # "i":I
    .end local v4    # "pageLimit":I
    :cond_6
    iget-object v6, p0, Landroid/support/v4/view/FixedViewPager;->mAdapter:Landroid/support/v4/view/FixedPagerAdapter;

    invoke-virtual {v6}, Landroid/support/v4/view/FixedPagerAdapter;->getCount()I

    move-result v6

    if-lt p1, v6, :cond_4

    .line 464
    iget-object v6, p0, Landroid/support/v4/view/FixedViewPager;->mAdapter:Landroid/support/v4/view/FixedPagerAdapter;

    invoke-virtual {v6}, Landroid/support/v4/view/FixedPagerAdapter;->getCount()I

    move-result v6

    add-int/lit8 p1, v6, -0x1

    goto :goto_1

    .line 475
    .restart local v4    # "pageLimit":I
    :cond_7
    iget v6, p0, Landroid/support/v4/view/FixedViewPager;->mCurItem:I

    if-eq v6, p1, :cond_a

    .line 476
    .local v2, "dispatchSelected":Z
    :goto_3
    invoke-virtual {p0, p1}, Landroid/support/v4/view/FixedViewPager;->populate(I)V

    .line 477
    invoke-virtual {p0, p1}, Landroid/support/v4/view/FixedViewPager;->infoForPosition(I)Landroid/support/v4/view/FixedViewPager$ItemInfo;

    move-result-object v0

    .line 478
    .local v0, "curInfo":Landroid/support/v4/view/FixedViewPager$ItemInfo;
    const/4 v1, 0x0

    .line 479
    .local v1, "destX":I
    if-eqz v0, :cond_8

    .line 480
    invoke-virtual {p0}, Landroid/support/v4/view/FixedViewPager;->getWidth()I

    move-result v5

    .line 481
    .local v5, "width":I
    int-to-float v6, v5

    iget v8, p0, Landroid/support/v4/view/FixedViewPager;->mFirstOffset:F

    iget v9, v0, Landroid/support/v4/view/FixedViewPager$ItemInfo;->offset:F

    iget v10, p0, Landroid/support/v4/view/FixedViewPager;->mLastOffset:F

    invoke-static {v9, v10}, Ljava/lang/Math;->min(FF)F

    move-result v9

    invoke-static {v8, v9}, Ljava/lang/Math;->max(FF)F

    move-result v8

    mul-float/2addr v6, v8

    float-to-int v1, v6

    .line 484
    .end local v5    # "width":I
    :cond_8
    if-eqz p2, :cond_b

    .line 485
    invoke-virtual {p0, v1, v7, p4}, Landroid/support/v4/view/FixedViewPager;->smoothScrollTo(III)V

    .line 486
    if-eqz v2, :cond_9

    iget-object v6, p0, Landroid/support/v4/view/FixedViewPager;->mOnPageChangeListener:Landroid/support/v4/view/FixedViewPager$OnPageChangeListener;

    if-eqz v6, :cond_9

    .line 487
    iget-object v6, p0, Landroid/support/v4/view/FixedViewPager;->mOnPageChangeListener:Landroid/support/v4/view/FixedViewPager$OnPageChangeListener;

    invoke-interface {v6, p1}, Landroid/support/v4/view/FixedViewPager$OnPageChangeListener;->onPageSelected(I)V

    .line 489
    :cond_9
    if-eqz v2, :cond_1

    iget-object v6, p0, Landroid/support/v4/view/FixedViewPager;->mInternalPageChangeListener:Landroid/support/v4/view/FixedViewPager$OnPageChangeListener;

    if-eqz v6, :cond_1

    .line 490
    iget-object v6, p0, Landroid/support/v4/view/FixedViewPager;->mInternalPageChangeListener:Landroid/support/v4/view/FixedViewPager$OnPageChangeListener;

    invoke-interface {v6, p1}, Landroid/support/v4/view/FixedViewPager$OnPageChangeListener;->onPageSelected(I)V

    goto/16 :goto_0

    .end local v0    # "curInfo":Landroid/support/v4/view/FixedViewPager$ItemInfo;
    .end local v1    # "destX":I
    .end local v2    # "dispatchSelected":Z
    :cond_a
    move v2, v7

    .line 475
    goto :goto_3

    .line 493
    .restart local v0    # "curInfo":Landroid/support/v4/view/FixedViewPager$ItemInfo;
    .restart local v1    # "destX":I
    .restart local v2    # "dispatchSelected":Z
    :cond_b
    if-eqz v2, :cond_c

    iget-object v6, p0, Landroid/support/v4/view/FixedViewPager;->mOnPageChangeListener:Landroid/support/v4/view/FixedViewPager$OnPageChangeListener;

    if-eqz v6, :cond_c

    .line 494
    iget-object v6, p0, Landroid/support/v4/view/FixedViewPager;->mOnPageChangeListener:Landroid/support/v4/view/FixedViewPager$OnPageChangeListener;

    invoke-interface {v6, p1}, Landroid/support/v4/view/FixedViewPager$OnPageChangeListener;->onPageSelected(I)V

    .line 496
    :cond_c
    if-eqz v2, :cond_d

    iget-object v6, p0, Landroid/support/v4/view/FixedViewPager;->mInternalPageChangeListener:Landroid/support/v4/view/FixedViewPager$OnPageChangeListener;

    if-eqz v6, :cond_d

    .line 497
    iget-object v6, p0, Landroid/support/v4/view/FixedViewPager;->mInternalPageChangeListener:Landroid/support/v4/view/FixedViewPager$OnPageChangeListener;

    invoke-interface {v6, p1}, Landroid/support/v4/view/FixedViewPager$OnPageChangeListener;->onPageSelected(I)V

    .line 499
    :cond_d
    invoke-direct {p0}, Landroid/support/v4/view/FixedViewPager;->completeScroll()V

    .line 500
    invoke-virtual {p0, v1, v7}, Landroid/support/v4/view/FixedViewPager;->scrollTo(II)V

    goto/16 :goto_0
.end method

.method setInternalPageChangeListener(Landroid/support/v4/view/FixedViewPager$OnPageChangeListener;)Landroid/support/v4/view/FixedViewPager$OnPageChangeListener;
    .locals 1
    .param p1, "listener"    # Landroid/support/v4/view/FixedViewPager$OnPageChangeListener;

    .prologue
    .line 521
    iget-object v0, p0, Landroid/support/v4/view/FixedViewPager;->mInternalPageChangeListener:Landroid/support/v4/view/FixedViewPager$OnPageChangeListener;

    .line 522
    .local v0, "oldListener":Landroid/support/v4/view/FixedViewPager$OnPageChangeListener;
    iput-object p1, p0, Landroid/support/v4/view/FixedViewPager;->mInternalPageChangeListener:Landroid/support/v4/view/FixedViewPager$OnPageChangeListener;

    .line 523
    return-object v0
.end method

.method public setOffscreenPageLimit(I)V
    .locals 4
    .param p1, "limit"    # I

    .prologue
    const/4 v3, 0x1

    .line 555
    if-ge p1, v3, :cond_0

    .line 556
    const-string v0, "ViewPager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Requested offscreen page limit "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " too small; defaulting to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/groupcast/application/Logger;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 558
    const/4 p1, 0x1

    .line 560
    :cond_0
    iget v0, p0, Landroid/support/v4/view/FixedViewPager;->mOffscreenPageLimit:I

    if-eq p1, v0, :cond_1

    .line 561
    iput p1, p0, Landroid/support/v4/view/FixedViewPager;->mOffscreenPageLimit:I

    .line 562
    invoke-virtual {p0}, Landroid/support/v4/view/FixedViewPager;->populate()V

    .line 564
    :cond_1
    return-void
.end method

.method setOnAdapterChangeListener(Landroid/support/v4/view/FixedViewPager$OnAdapterChangeListener;)V
    .locals 0
    .param p1, "listener"    # Landroid/support/v4/view/FixedViewPager$OnAdapterChangeListener;

    .prologue
    .line 417
    iput-object p1, p0, Landroid/support/v4/view/FixedViewPager;->mAdapterChangeListener:Landroid/support/v4/view/FixedViewPager$OnAdapterChangeListener;

    .line 418
    return-void
.end method

.method public setOnPageChangeListener(Landroid/support/v4/view/FixedViewPager$OnPageChangeListener;)V
    .locals 0
    .param p1, "listener"    # Landroid/support/v4/view/FixedViewPager$OnPageChangeListener;

    .prologue
    .line 511
    iput-object p1, p0, Landroid/support/v4/view/FixedViewPager;->mOnPageChangeListener:Landroid/support/v4/view/FixedViewPager$OnPageChangeListener;

    .line 512
    return-void
.end method

.method public setPageMargin(I)V
    .locals 2
    .param p1, "marginPixels"    # I

    .prologue
    .line 575
    iget v0, p0, Landroid/support/v4/view/FixedViewPager;->mPageMargin:I

    .line 576
    .local v0, "oldMargin":I
    iput p1, p0, Landroid/support/v4/view/FixedViewPager;->mPageMargin:I

    .line 578
    invoke-virtual {p0}, Landroid/support/v4/view/FixedViewPager;->getWidth()I

    move-result v1

    .line 579
    .local v1, "width":I
    invoke-direct {p0, v1, v1, p1, v0}, Landroid/support/v4/view/FixedViewPager;->recomputeScrollPosition(IIII)V

    .line 581
    invoke-virtual {p0}, Landroid/support/v4/view/FixedViewPager;->requestLayout()V

    .line 582
    return-void
.end method

.method public setPageMarginDrawable(I)V
    .locals 1
    .param p1, "resId"    # I

    .prologue
    .line 611
    invoke-virtual {p0}, Landroid/support/v4/view/FixedViewPager;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/support/v4/view/FixedViewPager;->setPageMarginDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 612
    return-void
.end method

.method public setPageMarginDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 1
    .param p1, "d"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 599
    iput-object p1, p0, Landroid/support/v4/view/FixedViewPager;->mMarginDrawable:Landroid/graphics/drawable/Drawable;

    .line 600
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Landroid/support/v4/view/FixedViewPager;->refreshDrawableState()V

    .line 601
    :cond_0
    if-nez p1, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Landroid/support/v4/view/FixedViewPager;->setWillNotDraw(Z)V

    .line 602
    invoke-virtual {p0}, Landroid/support/v4/view/FixedViewPager;->invalidate()V

    .line 603
    return-void

    .line 601
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method smoothScrollTo(II)V
    .locals 1
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 645
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Landroid/support/v4/view/FixedViewPager;->smoothScrollTo(III)V

    .line 646
    return-void
.end method

.method smoothScrollTo(III)V
    .locals 15
    .param p1, "x"    # I
    .param p2, "y"    # I
    .param p3, "velocity"    # I

    .prologue
    .line 656
    invoke-virtual {p0}, Landroid/support/v4/view/FixedViewPager;->getChildCount()I

    move-result v1

    if-nez v1, :cond_0

    .line 658
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Landroid/support/v4/view/FixedViewPager;->setScrollingCacheEnabled(Z)V

    .line 694
    :goto_0
    return-void

    .line 661
    :cond_0
    invoke-virtual {p0}, Landroid/support/v4/view/FixedViewPager;->getScrollX()I

    move-result v2

    .line 662
    .local v2, "sx":I
    invoke-virtual {p0}, Landroid/support/v4/view/FixedViewPager;->getScrollY()I

    move-result v3

    .line 663
    .local v3, "sy":I
    sub-int v4, p1, v2

    .line 664
    .local v4, "dx":I
    sub-int v5, p2, v3

    .line 665
    .local v5, "dy":I
    if-nez v4, :cond_1

    if-nez v5, :cond_1

    .line 666
    invoke-direct {p0}, Landroid/support/v4/view/FixedViewPager;->completeScroll()V

    .line 667
    invoke-virtual {p0}, Landroid/support/v4/view/FixedViewPager;->populate()V

    .line 668
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Landroid/support/v4/view/FixedViewPager;->setScrollState(I)V

    goto :goto_0

    .line 672
    :cond_1
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Landroid/support/v4/view/FixedViewPager;->setScrollingCacheEnabled(Z)V

    .line 673
    const/4 v1, 0x2

    invoke-direct {p0, v1}, Landroid/support/v4/view/FixedViewPager;->setScrollState(I)V

    .line 675
    invoke-virtual {p0}, Landroid/support/v4/view/FixedViewPager;->getWidth()I

    move-result v12

    .line 676
    .local v12, "width":I
    div-int/lit8 v9, v12, 0x2

    .line 677
    .local v9, "halfWidth":I
    const/high16 v1, 0x3f800000    # 1.0f

    const/high16 v13, 0x3f800000    # 1.0f

    invoke-static {v4}, Ljava/lang/Math;->abs(I)I

    move-result v14

    int-to-float v14, v14

    mul-float/2addr v13, v14

    int-to-float v14, v12

    div-float/2addr v13, v14

    invoke-static {v1, v13}, Ljava/lang/Math;->min(FF)F

    move-result v8

    .line 678
    .local v8, "distanceRatio":F
    int-to-float v1, v9

    int-to-float v13, v9

    invoke-virtual {p0, v8}, Landroid/support/v4/view/FixedViewPager;->distanceInfluenceForSnapDuration(F)F

    move-result v14

    mul-float/2addr v13, v14

    add-float v7, v1, v13

    .line 681
    .local v7, "distance":F
    const/4 v6, 0x0

    .line 682
    .local v6, "duration":I
    invoke-static/range {p3 .. p3}, Ljava/lang/Math;->abs(I)I

    move-result p3

    .line 683
    if-lez p3, :cond_2

    .line 684
    const/high16 v1, 0x447a0000    # 1000.0f

    move/from16 v0, p3

    int-to-float v13, v0

    div-float v13, v7, v13

    invoke-static {v13}, Ljava/lang/Math;->abs(F)F

    move-result v13

    mul-float/2addr v1, v13

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    mul-int/lit8 v6, v1, 0x4

    .line 690
    :goto_1
    const/16 v1, 0x258

    invoke-static {v6, v1}, Ljava/lang/Math;->min(II)I

    move-result v6

    .line 692
    iget-object v1, p0, Landroid/support/v4/view/FixedViewPager;->mScroller:Landroid/widget/Scroller;

    invoke-virtual/range {v1 .. v6}, Landroid/widget/Scroller;->startScroll(IIIII)V

    .line 693
    invoke-static {p0}, Landroid/support/v4/view/ViewCompat;->postInvalidateOnAnimation(Landroid/view/View;)V

    goto :goto_0

    .line 686
    :cond_2
    int-to-float v1, v12

    iget-object v13, p0, Landroid/support/v4/view/FixedViewPager;->mAdapter:Landroid/support/v4/view/FixedPagerAdapter;

    iget v14, p0, Landroid/support/v4/view/FixedViewPager;->mCurItem:I

    invoke-virtual {v13, v14}, Landroid/support/v4/view/FixedPagerAdapter;->getPageWidth(I)F

    move-result v13

    mul-float v11, v1, v13

    .line 687
    .local v11, "pageWidth":F
    invoke-static {v4}, Ljava/lang/Math;->abs(I)I

    move-result v1

    int-to-float v1, v1

    iget v13, p0, Landroid/support/v4/view/FixedViewPager;->mPageMargin:I

    int-to-float v13, v13

    add-float/2addr v13, v11

    div-float v10, v1, v13

    .line 688
    .local v10, "pageDelta":F
    const/high16 v1, 0x3f800000    # 1.0f

    add-float/2addr v1, v10

    const/high16 v13, 0x42c80000    # 100.0f

    mul-float/2addr v1, v13

    float-to-int v6, v1

    goto :goto_1
.end method

.method protected verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .locals 1
    .param p1, "who"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 616
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Landroid/support/v4/view/FixedViewPager;->mMarginDrawable:Landroid/graphics/drawable/Drawable;

    if-ne p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
