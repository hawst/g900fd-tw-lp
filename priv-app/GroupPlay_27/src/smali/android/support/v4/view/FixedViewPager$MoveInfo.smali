.class public Landroid/support/v4/view/FixedViewPager$MoveInfo;
.super Ljava/lang/Object;
.source "FixedViewPager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/v4/view/FixedViewPager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "MoveInfo"
.end annotation


# instance fields
.field private final mItem:Ljava/lang/Object;

.field private final mNewPosition:I

.field private final mOldPosition:I


# direct methods
.method constructor <init>(Ljava/lang/Object;II)V
    .locals 0
    .param p1, "item"    # Ljava/lang/Object;
    .param p2, "oldPosition"    # I
    .param p3, "newPosition"    # I

    .prologue
    .line 714
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 715
    iput-object p1, p0, Landroid/support/v4/view/FixedViewPager$MoveInfo;->mItem:Ljava/lang/Object;

    .line 716
    iput p2, p0, Landroid/support/v4/view/FixedViewPager$MoveInfo;->mOldPosition:I

    .line 717
    iput p3, p0, Landroid/support/v4/view/FixedViewPager$MoveInfo;->mNewPosition:I

    .line 718
    return-void
.end method


# virtual methods
.method public getItem()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 721
    iget-object v0, p0, Landroid/support/v4/view/FixedViewPager$MoveInfo;->mItem:Ljava/lang/Object;

    return-object v0
.end method

.method public getNewPosition()I
    .locals 1

    .prologue
    .line 729
    iget v0, p0, Landroid/support/v4/view/FixedViewPager$MoveInfo;->mNewPosition:I

    return v0
.end method

.method public getOldPosition()I
    .locals 1

    .prologue
    .line 725
    iget v0, p0, Landroid/support/v4/view/FixedViewPager$MoveInfo;->mOldPosition:I

    return v0
.end method
