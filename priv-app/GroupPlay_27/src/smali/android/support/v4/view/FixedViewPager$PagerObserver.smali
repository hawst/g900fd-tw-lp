.class Landroid/support/v4/view/FixedViewPager$PagerObserver;
.super Landroid/database/DataSetObserver;
.source "FixedViewPager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/v4/view/FixedViewPager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PagerObserver"
.end annotation


# instance fields
.field final synthetic this$0:Landroid/support/v4/view/FixedViewPager;


# direct methods
.method private constructor <init>(Landroid/support/v4/view/FixedViewPager;)V
    .locals 0

    .prologue
    .line 2640
    iput-object p1, p0, Landroid/support/v4/view/FixedViewPager$PagerObserver;->this$0:Landroid/support/v4/view/FixedViewPager;

    invoke-direct {p0}, Landroid/database/DataSetObserver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Landroid/support/v4/view/FixedViewPager;Landroid/support/v4/view/FixedViewPager$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/support/v4/view/FixedViewPager;
    .param p2, "x1"    # Landroid/support/v4/view/FixedViewPager$1;

    .prologue
    .line 2640
    invoke-direct {p0, p1}, Landroid/support/v4/view/FixedViewPager$PagerObserver;-><init>(Landroid/support/v4/view/FixedViewPager;)V

    return-void
.end method


# virtual methods
.method public onChanged()V
    .locals 1

    .prologue
    .line 2643
    iget-object v0, p0, Landroid/support/v4/view/FixedViewPager$PagerObserver;->this$0:Landroid/support/v4/view/FixedViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/FixedViewPager;->dataSetChanged()V

    .line 2644
    return-void
.end method

.method public onInvalidated()V
    .locals 1

    .prologue
    .line 2647
    iget-object v0, p0, Landroid/support/v4/view/FixedViewPager$PagerObserver;->this$0:Landroid/support/v4/view/FixedViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/FixedViewPager;->dataSetChanged()V

    .line 2648
    return-void
.end method
