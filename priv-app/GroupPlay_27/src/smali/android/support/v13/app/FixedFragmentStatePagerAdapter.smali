.class public abstract Landroid/support/v13/app/FixedFragmentStatePagerAdapter;
.super Landroid/support/v4/view/FixedPagerAdapter;
.source "FixedFragmentStatePagerAdapter.java"


# static fields
.field private static final DEBUG:Z = false

.field private static final TAG:Ljava/lang/String; = "FragmentStatePagerAdapter"


# instance fields
.field private mCurTransaction:Landroid/app/FragmentTransaction;

.field private mCurrentPrimaryItem:Landroid/app/Fragment;

.field private final mFragmentManager:Landroid/app/FragmentManager;

.field private final mFragments:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Landroid/app/Fragment;",
            ">;"
        }
    .end annotation
.end field

.field private final mSavedState:Landroid/os/Bundle;


# direct methods
.method public constructor <init>(Landroid/app/FragmentManager;)V
    .locals 2
    .param p1, "fm"    # Landroid/app/FragmentManager;

    .prologue
    const/4 v1, 0x0

    .line 86
    invoke-direct {p0}, Landroid/support/v4/view/FixedPagerAdapter;-><init>()V

    .line 80
    iput-object v1, p0, Landroid/support/v13/app/FixedFragmentStatePagerAdapter;->mCurTransaction:Landroid/app/FragmentTransaction;

    .line 82
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Landroid/support/v13/app/FixedFragmentStatePagerAdapter;->mSavedState:Landroid/os/Bundle;

    .line 83
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Landroid/support/v13/app/FixedFragmentStatePagerAdapter;->mFragments:Ljava/util/HashMap;

    .line 84
    iput-object v1, p0, Landroid/support/v13/app/FixedFragmentStatePagerAdapter;->mCurrentPrimaryItem:Landroid/app/Fragment;

    .line 87
    iput-object p1, p0, Landroid/support/v13/app/FixedFragmentStatePagerAdapter;->mFragmentManager:Landroid/app/FragmentManager;

    .line 88
    return-void
.end method


# virtual methods
.method public destroyItem(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 5
    .param p1, "container"    # Landroid/view/ViewGroup;
    .param p2, "position"    # I
    .param p3, "object"    # Ljava/lang/Object;

    .prologue
    .line 176
    move-object v1, p3

    check-cast v1, Landroid/app/Fragment;

    .line 178
    .local v1, "fragment":Landroid/app/Fragment;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "position:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 182
    invoke-virtual {p0, p1}, Landroid/support/v13/app/FixedFragmentStatePagerAdapter;->finishUpdate(Landroid/view/ViewGroup;)V

    .line 184
    iget-object v3, p0, Landroid/support/v13/app/FixedFragmentStatePagerAdapter;->mCurTransaction:Landroid/app/FragmentTransaction;

    if-nez v3, :cond_0

    .line 185
    iget-object v3, p0, Landroid/support/v13/app/FixedFragmentStatePagerAdapter;->mFragmentManager:Landroid/app/FragmentManager;

    invoke-virtual {v3}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v3

    iput-object v3, p0, Landroid/support/v13/app/FixedFragmentStatePagerAdapter;->mCurTransaction:Landroid/app/FragmentTransaction;

    .line 190
    :cond_0
    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    .line 193
    .local v2, "key":Ljava/lang/String;
    :try_start_0
    iget-object v3, p0, Landroid/support/v13/app/FixedFragmentStatePagerAdapter;->mSavedState:Landroid/os/Bundle;

    iget-object v4, p0, Landroid/support/v13/app/FixedFragmentStatePagerAdapter;->mFragmentManager:Landroid/app/FragmentManager;

    invoke-virtual {v4, v1}, Landroid/app/FragmentManager;->saveFragmentInstanceState(Landroid/app/Fragment;)Landroid/app/Fragment$SavedState;

    move-result-object v4

    invoke-virtual {v3, v2, v4}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 199
    :goto_0
    iget-object v3, p0, Landroid/support/v13/app/FixedFragmentStatePagerAdapter;->mFragments:Ljava/util/HashMap;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 201
    iget-object v3, p0, Landroid/support/v13/app/FixedFragmentStatePagerAdapter;->mCurTransaction:Landroid/app/FragmentTransaction;

    invoke-virtual {v3, v1}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 202
    return-void

    .line 195
    :catch_0
    move-exception v0

    .line 197
    .local v0, "e":Ljava/lang/Exception;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "does not exist:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public finishUpdate(Landroid/view/ViewGroup;)V
    .locals 2
    .param p1, "container"    # Landroid/view/ViewGroup;

    .prologue
    .line 220
    iget-object v1, p0, Landroid/support/v13/app/FixedFragmentStatePagerAdapter;->mCurTransaction:Landroid/app/FragmentTransaction;

    if-eqz v1, :cond_0

    .line 221
    iget-object v1, p0, Landroid/support/v13/app/FixedFragmentStatePagerAdapter;->mCurTransaction:Landroid/app/FragmentTransaction;

    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 222
    const/4 v1, 0x0

    iput-object v1, p0, Landroid/support/v13/app/FixedFragmentStatePagerAdapter;->mCurTransaction:Landroid/app/FragmentTransaction;

    .line 224
    :try_start_0
    iget-object v1, p0, Landroid/support/v13/app/FixedFragmentStatePagerAdapter;->mFragmentManager:Landroid/app/FragmentManager;

    invoke-virtual {v1}, Landroid/app/FragmentManager;->executePendingTransactions()Z
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 229
    :cond_0
    :goto_0
    return-void

    .line 225
    :catch_0
    move-exception v0

    .line 226
    .local v0, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0
.end method

.method public abstract getItem(I)Landroid/app/Fragment;
.end method

.method public instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 6
    .param p1, "container"    # Landroid/view/ViewGroup;
    .param p2, "position"    # I

    .prologue
    .line 105
    iget-object v4, p0, Landroid/support/v13/app/FixedFragmentStatePagerAdapter;->mFragments:Ljava/util/HashMap;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Fragment;

    .line 107
    .local v0, "f":Landroid/app/Fragment;
    if-eqz v0, :cond_0

    .line 129
    .end local v0    # "f":Landroid/app/Fragment;
    :goto_0
    return-object v0

    .line 111
    .restart local v0    # "f":Landroid/app/Fragment;
    :cond_0
    iget-object v4, p0, Landroid/support/v13/app/FixedFragmentStatePagerAdapter;->mCurTransaction:Landroid/app/FragmentTransaction;

    if-nez v4, :cond_1

    .line 112
    iget-object v4, p0, Landroid/support/v13/app/FixedFragmentStatePagerAdapter;->mFragmentManager:Landroid/app/FragmentManager;

    invoke-virtual {v4}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v4

    iput-object v4, p0, Landroid/support/v13/app/FixedFragmentStatePagerAdapter;->mCurTransaction:Landroid/app/FragmentTransaction;

    .line 115
    :cond_1
    invoke-virtual {p0, p2}, Landroid/support/v13/app/FixedFragmentStatePagerAdapter;->getItem(I)Landroid/app/Fragment;

    move-result-object v1

    .line 118
    .local v1, "fragment":Landroid/app/Fragment;
    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    .line 119
    .local v2, "key":Ljava/lang/String;
    iget-object v4, p0, Landroid/support/v13/app/FixedFragmentStatePagerAdapter;->mSavedState:Landroid/os/Bundle;

    invoke-virtual {v4, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Landroid/app/Fragment$SavedState;

    .line 121
    .local v3, "savedState":Landroid/app/Fragment$SavedState;
    if-eqz v3, :cond_2

    .line 122
    invoke-virtual {v1, v3}, Landroid/app/Fragment;->setInitialSavedState(Landroid/app/Fragment$SavedState;)V

    .line 125
    :cond_2
    const/4 v4, 0x0

    invoke-static {v1, v4}, Landroid/support/v13/app/FragmentCompat;->setMenuVisibility(Landroid/app/Fragment;Z)V

    .line 126
    iget-object v4, p0, Landroid/support/v13/app/FixedFragmentStatePagerAdapter;->mFragments:Ljava/util/HashMap;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 127
    iget-object v4, p0, Landroid/support/v13/app/FixedFragmentStatePagerAdapter;->mCurTransaction:Landroid/app/FragmentTransaction;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getId()I

    move-result v5

    invoke-virtual {v4, v5, v1}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-object v0, v1

    .line 129
    goto :goto_0
.end method

.method public isViewFromObject(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1
    .param p1, "view"    # Landroid/view/View;
    .param p2, "object"    # Ljava/lang/Object;

    .prologue
    .line 233
    check-cast p2, Landroid/app/Fragment;

    .end local p2    # "object":Ljava/lang/Object;
    invoke-virtual {p2}, Landroid/app/Fragment;->getView()Landroid/view/View;

    move-result-object v0

    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public moveItems(Ljava/util/Collection;)V
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Landroid/support/v4/view/FixedViewPager$MoveInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 134
    .local p1, "items":Ljava/util/Collection;, "Ljava/util/Collection<Landroid/support/v4/view/FixedViewPager$MoveInfo;>;"
    new-instance v7, Ljava/util/HashMap;

    invoke-direct {v7}, Ljava/util/HashMap;-><init>()V

    .line 135
    .local v7, "newFragments":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;Landroid/app/Fragment;>;"
    new-instance v6, Ljava/util/HashSet;

    invoke-direct {v6}, Ljava/util/HashSet;-><init>()V

    .line 136
    .local v6, "movedIndices":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/Integer;>;"
    new-instance v10, Landroid/os/Bundle;

    invoke-direct {v10}, Landroid/os/Bundle;-><init>()V

    .line 138
    .local v10, "newSavedState":Landroid/os/Bundle;
    invoke-interface/range {p1 .. p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v15

    if-eqz v15, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/support/v4/view/FixedViewPager$MoveInfo;

    .line 139
    .local v4, "info":Landroid/support/v4/view/FixedViewPager$MoveInfo;
    invoke-virtual {v4}, Landroid/support/v4/view/FixedViewPager$MoveInfo;->getOldPosition()I

    move-result v11

    .line 140
    .local v11, "oldPosition":I
    invoke-virtual {v4}, Landroid/support/v4/view/FixedViewPager$MoveInfo;->getNewPosition()I

    move-result v8

    .line 141
    .local v8, "newPosition":I
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    invoke-virtual {v4}, Landroid/support/v4/view/FixedViewPager$MoveInfo;->getItem()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Landroid/app/Fragment;

    move-object/from16 v0, v16

    invoke-virtual {v7, v0, v15}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 142
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    invoke-virtual {v6, v15}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 143
    invoke-static {v11}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v12

    .line 144
    .local v12, "oldPositionKey":Ljava/lang/String;
    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    .line 145
    .local v9, "newPositionKey":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v15, v0, Landroid/support/v13/app/FixedFragmentStatePagerAdapter;->mSavedState:Landroid/os/Bundle;

    invoke-virtual {v15, v12}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v13

    check-cast v13, Landroid/app/Fragment$SavedState;

    .line 147
    .local v13, "state":Landroid/app/Fragment$SavedState;
    if-eqz v13, :cond_0

    .line 148
    invoke-virtual {v10, v9, v13}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    goto :goto_0

    .line 153
    .end local v4    # "info":Landroid/support/v4/view/FixedViewPager$MoveInfo;
    .end local v8    # "newPosition":I
    .end local v9    # "newPositionKey":Ljava/lang/String;
    .end local v11    # "oldPosition":I
    .end local v12    # "oldPositionKey":Ljava/lang/String;
    .end local v13    # "state":Landroid/app/Fragment$SavedState;
    :cond_1
    new-instance v14, Ljava/util/HashSet;

    move-object/from16 v0, p0

    iget-object v15, v0, Landroid/support/v13/app/FixedFragmentStatePagerAdapter;->mFragments:Ljava/util/HashMap;

    invoke-virtual {v15}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v15

    invoke-direct {v14, v15}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 154
    .local v14, "unaddedIndices":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    invoke-interface {v14, v6}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    .line 156
    invoke-interface {v14}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v15

    if-eqz v15, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/Integer;

    invoke-virtual {v15}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 157
    .local v3, "index":I
    move-object/from16 v0, p0

    iget-object v15, v0, Landroid/support/v13/app/FixedFragmentStatePagerAdapter;->mFragments:Ljava/util/HashMap;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/Fragment;

    .line 158
    .local v1, "fragment":Landroid/app/Fragment;
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    invoke-virtual {v7, v15, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 159
    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    .line 160
    .local v5, "key":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v15, v0, Landroid/support/v13/app/FixedFragmentStatePagerAdapter;->mSavedState:Landroid/os/Bundle;

    invoke-virtual {v15, v5}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v13

    check-cast v13, Landroid/app/Fragment$SavedState;

    .line 162
    .restart local v13    # "state":Landroid/app/Fragment$SavedState;
    if-eqz v13, :cond_2

    .line 163
    invoke-virtual {v10, v5, v13}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    goto :goto_1

    .line 168
    .end local v1    # "fragment":Landroid/app/Fragment;
    .end local v3    # "index":I
    .end local v5    # "key":Ljava/lang/String;
    .end local v13    # "state":Landroid/app/Fragment$SavedState;
    :cond_3
    move-object/from16 v0, p0

    iget-object v15, v0, Landroid/support/v13/app/FixedFragmentStatePagerAdapter;->mFragments:Ljava/util/HashMap;

    invoke-virtual {v15}, Ljava/util/HashMap;->clear()V

    .line 169
    move-object/from16 v0, p0

    iget-object v15, v0, Landroid/support/v13/app/FixedFragmentStatePagerAdapter;->mSavedState:Landroid/os/Bundle;

    invoke-virtual {v15}, Landroid/os/Bundle;->clear()V

    .line 170
    move-object/from16 v0, p0

    iget-object v15, v0, Landroid/support/v13/app/FixedFragmentStatePagerAdapter;->mFragments:Ljava/util/HashMap;

    invoke-virtual {v15, v7}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    .line 171
    move-object/from16 v0, p0

    iget-object v15, v0, Landroid/support/v13/app/FixedFragmentStatePagerAdapter;->mSavedState:Landroid/os/Bundle;

    invoke-virtual {v15, v10}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 172
    return-void
.end method

.method public restoreState(Landroid/os/Parcelable;Ljava/lang/ClassLoader;)V
    .locals 10
    .param p1, "state"    # Landroid/os/Parcelable;
    .param p2, "loader"    # Ljava/lang/ClassLoader;

    .prologue
    .line 264
    if-eqz p1, :cond_3

    move-object v0, p1

    .line 265
    check-cast v0, Landroid/os/Bundle;

    .line 266
    .local v0, "bundle":Landroid/os/Bundle;
    invoke-virtual {v0, p2}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 267
    iget-object v7, p0, Landroid/support/v13/app/FixedFragmentStatePagerAdapter;->mSavedState:Landroid/os/Bundle;

    invoke-virtual {v7}, Landroid/os/Bundle;->clear()V

    .line 268
    iget-object v7, p0, Landroid/support/v13/app/FixedFragmentStatePagerAdapter;->mFragments:Ljava/util/HashMap;

    invoke-virtual {v7}, Ljava/util/HashMap;->clear()V

    .line 269
    const-string v7, "states"

    invoke-virtual {v0, v7}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v6

    check-cast v6, Landroid/os/Bundle;

    .line 270
    .local v6, "previousState":Landroid/os/Bundle;
    if-eqz v6, :cond_0

    .line 271
    iget-object v7, p0, Landroid/support/v13/app/FixedFragmentStatePagerAdapter;->mSavedState:Landroid/os/Bundle;

    invoke-virtual {v7, v6}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 273
    :cond_0
    invoke-virtual {v0}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v5

    .line 274
    .local v5, "keys":Ljava/lang/Iterable;, "Ljava/lang/Iterable<Ljava/lang/String;>;"
    invoke-interface {v5}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 275
    .local v4, "key":Ljava/lang/String;
    const-string v7, "f"

    invoke-virtual {v4, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 276
    const/4 v7, 0x1

    invoke-virtual {v4, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    .line 277
    .local v3, "index":I
    iget-object v7, p0, Landroid/support/v13/app/FixedFragmentStatePagerAdapter;->mFragmentManager:Landroid/app/FragmentManager;

    invoke-virtual {v7, v0, v4}, Landroid/app/FragmentManager;->getFragment(Landroid/os/Bundle;Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    .line 278
    .local v1, "f":Landroid/app/Fragment;
    if-eqz v1, :cond_2

    .line 279
    const/4 v7, 0x0

    invoke-static {v1, v7}, Landroid/support/v13/app/FragmentCompat;->setMenuVisibility(Landroid/app/Fragment;Z)V

    .line 280
    iget-object v7, p0, Landroid/support/v13/app/FixedFragmentStatePagerAdapter;->mFragments:Ljava/util/HashMap;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v7, v8, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 282
    :cond_2
    const-string v7, "FragmentStatePagerAdapter"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Bad fragment at key "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/samsung/groupcast/application/Logger;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 287
    .end local v0    # "bundle":Landroid/os/Bundle;
    .end local v1    # "f":Landroid/app/Fragment;
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "index":I
    .end local v4    # "key":Ljava/lang/String;
    .end local v5    # "keys":Ljava/lang/Iterable;, "Ljava/lang/Iterable<Ljava/lang/String;>;"
    .end local v6    # "previousState":Landroid/os/Bundle;
    :cond_3
    return-void
.end method

.method public saveState()Landroid/os/Parcelable;
    .locals 8

    .prologue
    .line 238
    const/4 v5, 0x0

    .line 240
    .local v5, "state":Landroid/os/Bundle;
    iget-object v6, p0, Landroid/support/v13/app/FixedFragmentStatePagerAdapter;->mSavedState:Landroid/os/Bundle;

    invoke-virtual {v6}, Landroid/os/Bundle;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_0

    .line 241
    new-instance v5, Landroid/os/Bundle;

    .end local v5    # "state":Landroid/os/Bundle;
    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 242
    .restart local v5    # "state":Landroid/os/Bundle;
    const-string v6, "states"

    iget-object v7, p0, Landroid/support/v13/app/FixedFragmentStatePagerAdapter;->mSavedState:Landroid/os/Bundle;

    invoke-virtual {v5, v6, v7}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 245
    :cond_0
    iget-object v6, p0, Landroid/support/v13/app/FixedFragmentStatePagerAdapter;->mFragments:Ljava/util/HashMap;

    invoke-virtual {v6}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 246
    .local v0, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Landroid/app/Fragment;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 247
    .local v3, "index":I
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/Fragment;

    .line 249
    .local v1, "f":Landroid/app/Fragment;
    if-eqz v1, :cond_1

    .line 250
    if-nez v5, :cond_2

    .line 251
    new-instance v5, Landroid/os/Bundle;

    .end local v5    # "state":Landroid/os/Bundle;
    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 254
    .restart local v5    # "state":Landroid/os/Bundle;
    :cond_2
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "f"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 255
    .local v4, "key":Ljava/lang/String;
    iget-object v6, p0, Landroid/support/v13/app/FixedFragmentStatePagerAdapter;->mFragmentManager:Landroid/app/FragmentManager;

    invoke-virtual {v6, v5, v4, v1}, Landroid/app/FragmentManager;->putFragment(Landroid/os/Bundle;Ljava/lang/String;Landroid/app/Fragment;)V

    goto :goto_0

    .line 259
    .end local v0    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Landroid/app/Fragment;>;"
    .end local v1    # "f":Landroid/app/Fragment;
    .end local v3    # "index":I
    .end local v4    # "key":Ljava/lang/String;
    :cond_3
    return-object v5
.end method

.method public setPrimaryItem(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 3
    .param p1, "container"    # Landroid/view/ViewGroup;
    .param p2, "position"    # I
    .param p3, "object"    # Ljava/lang/Object;

    .prologue
    .line 206
    move-object v0, p3

    check-cast v0, Landroid/app/Fragment;

    .line 207
    .local v0, "fragment":Landroid/app/Fragment;
    iget-object v1, p0, Landroid/support/v13/app/FixedFragmentStatePagerAdapter;->mCurrentPrimaryItem:Landroid/app/Fragment;

    if-eq v0, v1, :cond_2

    .line 208
    iget-object v1, p0, Landroid/support/v13/app/FixedFragmentStatePagerAdapter;->mCurrentPrimaryItem:Landroid/app/Fragment;

    if-eqz v1, :cond_0

    .line 209
    iget-object v1, p0, Landroid/support/v13/app/FixedFragmentStatePagerAdapter;->mCurrentPrimaryItem:Landroid/app/Fragment;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Landroid/support/v13/app/FragmentCompat;->setMenuVisibility(Landroid/app/Fragment;Z)V

    .line 211
    :cond_0
    if-eqz v0, :cond_1

    .line 212
    const/4 v1, 0x1

    invoke-static {v0, v1}, Landroid/support/v13/app/FragmentCompat;->setMenuVisibility(Landroid/app/Fragment;Z)V

    .line 214
    :cond_1
    iput-object v0, p0, Landroid/support/v13/app/FixedFragmentStatePagerAdapter;->mCurrentPrimaryItem:Landroid/app/Fragment;

    .line 216
    :cond_2
    return-void
.end method

.method public startUpdate(Landroid/view/ViewGroup;)V
    .locals 0
    .param p1, "container"    # Landroid/view/ViewGroup;

    .prologue
    .line 97
    return-void
.end method
