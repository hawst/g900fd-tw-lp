.class public final Lcom/sec/android/band/BandInterface$BAND_EXTRA;
.super Ljava/lang/Object;
.source "BandInterface.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/band/BandInterface;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "BAND_EXTRA"
.end annotation


# static fields
.field public static final AUTHENTICATION_TYPE:Ljava/lang/String; = "authentication"

.field public static final BSSID:Ljava/lang/String; = "bssid"

.field public static final CAPABILITY_INFO:Ljava/lang/String; = "CapabilityInfo"

.field public static final DEFAULT_BSSID:Ljava/lang/String; = "default_bssid"

.field public static final DEFAULT_SSID:Ljava/lang/String; = "default_ssid"

.field public static final ENCRYPTION_TYPE:Ljava/lang/String; = "encryption"

.field public static final IS_ENC_PASSPHRASE:Ljava/lang/String; = "is_encrypted_passphrase"

.field public static final NDEFMSG:Ljava/lang/String; = "ndef"

.field public static final NEW_CONNECTION:Ljava/lang/String; = "new_connection"

.field public static final PACKAGE_NAME:Ljava/lang/String; = "packageName"

.field public static final PASSPHRASE:Ljava/lang/String; = "passphrase"

.field public static final RESULT:Ljava/lang/String; = "result"

.field public static final SERVICE_CHUNK:Ljava/lang/String; = "serviceChunk"

.field public static final SSID:Ljava/lang/String; = "ssid"

.field public static final STATUS_INFO:Ljava/lang/String; = "statusInfo"

.field public static final TYPE:Ljava/lang/String; = "type"


# instance fields
.field final synthetic this$0:Lcom/sec/android/band/BandInterface;


# direct methods
.method public constructor <init>(Lcom/sec/android/band/BandInterface;)V
    .locals 0

    .prologue
    .line 156
    iput-object p1, p0, Lcom/sec/android/band/BandInterface$BAND_EXTRA;->this$0:Lcom/sec/android/band/BandInterface;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
