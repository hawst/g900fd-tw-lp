.class Lcom/sec/android/band/BandInterface$1;
.super Landroid/content/BroadcastReceiver;
.source "BandInterface.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/band/BandInterface;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/band/BandInterface;


# direct methods
.method constructor <init>(Lcom/sec/android/band/BandInterface;)V
    .locals 0

    .prologue
    .line 266
    iput-object p1, p0, Lcom/sec/android/band/BandInterface$1;->this$0:Lcom/sec/android/band/BandInterface;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 15
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 269
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v11

    .line 271
    .local v11, "action":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/band/BandInterface$1;->this$0:Lcom/sec/android/band/BandInterface;

    # getter for: Lcom/sec/android/band/BandInterface;->mBandEventHandler:Lcom/sec/android/band/BandInterface$BandEventHandler;
    invoke-static {v1}, Lcom/sec/android/band/BandInterface;->access$000(Lcom/sec/android/band/BandInterface;)Lcom/sec/android/band/BandInterface$BandEventHandler;

    move-result-object v1

    if-nez v1, :cond_1

    .line 273
    const-string v1, "BAND_LOG_PROTO_GP"

    const-string v2, "[GP]Callback is not registered!"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 410
    :cond_0
    :goto_0
    return-void

    .line 277
    :cond_1
    const-string v1, "com.sec.android.band.CONFIGURAITON"

    invoke-virtual {v11, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 278
    iget-object v1, p0, Lcom/sec/android/band/BandInterface$1;->this$0:Lcom/sec/android/band/BandInterface;

    const-string v2, "ssid"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/sec/android/band/BandInterface;->ssid:Ljava/lang/String;

    .line 279
    iget-object v1, p0, Lcom/sec/android/band/BandInterface$1;->this$0:Lcom/sec/android/band/BandInterface;

    const-string v2, "bssid"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/sec/android/band/BandInterface;->bssid:Ljava/lang/String;

    .line 281
    iget-object v1, p0, Lcom/sec/android/band/BandInterface$1;->this$0:Lcom/sec/android/band/BandInterface;

    const-string v2, "default_ssid"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/sec/android/band/BandInterface;->defSsid:Ljava/lang/String;

    .line 282
    iget-object v1, p0, Lcom/sec/android/band/BandInterface$1;->this$0:Lcom/sec/android/band/BandInterface;

    const-string v2, "default_bssid"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/sec/android/band/BandInterface;->defBssid:Ljava/lang/String;

    .line 284
    iget-object v1, p0, Lcom/sec/android/band/BandInterface$1;->this$0:Lcom/sec/android/band/BandInterface;

    const-string v2, "packageName"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/sec/android/band/BandInterface;->pkgName:Ljava/lang/String;

    .line 286
    iget-object v1, p0, Lcom/sec/android/band/BandInterface$1;->this$0:Lcom/sec/android/band/BandInterface;

    const-string v2, "passphrase"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/sec/android/band/BandInterface;->passphrase:Ljava/lang/String;

    .line 288
    iget-object v1, p0, Lcom/sec/android/band/BandInterface$1;->this$0:Lcom/sec/android/band/BandInterface;

    const-string v2, "encryption"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/sec/android/band/BandInterface;->encryption:Ljava/lang/String;

    .line 289
    iget-object v1, p0, Lcom/sec/android/band/BandInterface$1;->this$0:Lcom/sec/android/band/BandInterface;

    const-string v2, "authentication"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/sec/android/band/BandInterface;->authentication:Ljava/lang/String;

    .line 291
    iget-object v1, p0, Lcom/sec/android/band/BandInterface$1;->this$0:Lcom/sec/android/band/BandInterface;

    const-string v2, "new_connection"

    const/4 v3, 0x1

    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, v1, Lcom/sec/android/band/BandInterface;->newConnection:Z

    .line 294
    const-string v1, "BAND_LOG_PROTO_GP"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[GP]I/Intent : BAND_NETWORK_CONFIGURATION - SSID["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/band/BandInterface$1;->this$0:Lcom/sec/android/band/BandInterface;

    iget-object v3, v3, Lcom/sec/android/band/BandInterface;->ssid:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "bssid["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/band/BandInterface$1;->this$0:Lcom/sec/android/band/BandInterface;

    iget-object v3, v3, Lcom/sec/android/band/BandInterface;->bssid:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "pkgName["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/band/BandInterface$1;->this$0:Lcom/sec/android/band/BandInterface;

    iget-object v3, v3, Lcom/sec/android/band/BandInterface;->pkgName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "defSsid["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/band/BandInterface$1;->this$0:Lcom/sec/android/band/BandInterface;

    iget-object v3, v3, Lcom/sec/android/band/BandInterface;->defSsid:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "defBssid["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/band/BandInterface$1;->this$0:Lcom/sec/android/band/BandInterface;

    iget-object v3, v3, Lcom/sec/android/band/BandInterface;->defBssid:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "newConnection["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/band/BandInterface$1;->this$0:Lcom/sec/android/band/BandInterface;

    iget-boolean v3, v3, Lcom/sec/android/band/BandInterface;->newConnection:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 300
    iget-object v1, p0, Lcom/sec/android/band/BandInterface$1;->this$0:Lcom/sec/android/band/BandInterface;

    # getter for: Lcom/sec/android/band/BandInterface;->mBandEventHandler:Lcom/sec/android/band/BandInterface$BandEventHandler;
    invoke-static {v1}, Lcom/sec/android/band/BandInterface;->access$000(Lcom/sec/android/band/BandInterface;)Lcom/sec/android/band/BandInterface$BandEventHandler;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/band/BandInterface$1;->this$0:Lcom/sec/android/band/BandInterface;

    iget-object v2, v2, Lcom/sec/android/band/BandInterface;->ssid:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/android/band/BandInterface$1;->this$0:Lcom/sec/android/band/BandInterface;

    iget-object v3, v3, Lcom/sec/android/band/BandInterface;->bssid:Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/android/band/BandInterface$1;->this$0:Lcom/sec/android/band/BandInterface;

    iget-object v4, v4, Lcom/sec/android/band/BandInterface;->pkgName:Ljava/lang/String;

    iget-object v5, p0, Lcom/sec/android/band/BandInterface$1;->this$0:Lcom/sec/android/band/BandInterface;

    iget-object v5, v5, Lcom/sec/android/band/BandInterface;->defSsid:Ljava/lang/String;

    iget-object v6, p0, Lcom/sec/android/band/BandInterface$1;->this$0:Lcom/sec/android/band/BandInterface;

    iget-object v6, v6, Lcom/sec/android/band/BandInterface;->defBssid:Ljava/lang/String;

    iget-object v7, p0, Lcom/sec/android/band/BandInterface$1;->this$0:Lcom/sec/android/band/BandInterface;

    iget-object v7, v7, Lcom/sec/android/band/BandInterface;->passphrase:Ljava/lang/String;

    iget-object v8, p0, Lcom/sec/android/band/BandInterface$1;->this$0:Lcom/sec/android/band/BandInterface;

    iget-object v8, v8, Lcom/sec/android/band/BandInterface;->encryption:Ljava/lang/String;

    iget-object v9, p0, Lcom/sec/android/band/BandInterface$1;->this$0:Lcom/sec/android/band/BandInterface;

    iget-object v9, v9, Lcom/sec/android/band/BandInterface;->authentication:Ljava/lang/String;

    iget-object v10, p0, Lcom/sec/android/band/BandInterface$1;->this$0:Lcom/sec/android/band/BandInterface;

    iget-boolean v10, v10, Lcom/sec/android/band/BandInterface;->newConnection:Z

    invoke-interface/range {v1 .. v10}, Lcom/sec/android/band/BandInterface$BandEventHandler;->onConfiguration(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)I

    goto/16 :goto_0

    .line 303
    :cond_2
    const-string v1, "com.sec.android.band.NETWORK_STATUS"

    invoke-virtual {v11, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 304
    iget-object v1, p0, Lcom/sec/android/band/BandInterface$1;->this$0:Lcom/sec/android/band/BandInterface;

    const-string v2, "statusInfo"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/sec/android/band/BandInterface;->status:Ljava/lang/String;

    .line 307
    const-string v1, "BAND_LOG_PROTO_GP"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[GP]I/Intent : BAND_NETWORK_STATUS - status ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/band/BandInterface$1;->this$0:Lcom/sec/android/band/BandInterface;

    iget-object v3, v3, Lcom/sec/android/band/BandInterface;->status:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 310
    iget-object v1, p0, Lcom/sec/android/band/BandInterface$1;->this$0:Lcom/sec/android/band/BandInterface;

    # getter for: Lcom/sec/android/band/BandInterface;->mBandEventHandler:Lcom/sec/android/band/BandInterface$BandEventHandler;
    invoke-static {v1}, Lcom/sec/android/band/BandInterface;->access$000(Lcom/sec/android/band/BandInterface;)Lcom/sec/android/band/BandInterface$BandEventHandler;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/band/BandInterface$1;->this$0:Lcom/sec/android/band/BandInterface;

    iget-object v2, v2, Lcom/sec/android/band/BandInterface;->status:Ljava/lang/String;

    invoke-interface {v1, v2}, Lcom/sec/android/band/BandInterface$BandEventHandler;->onNetworkStatus(Ljava/lang/String;)I

    goto/16 :goto_0

    .line 312
    :cond_3
    const-string v1, "com.sec.android.band.NDEF_DISCOVERED"

    invoke-virtual {v11, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 313
    iget-object v1, p0, Lcom/sec/android/band/BandInterface$1;->this$0:Lcom/sec/android/band/BandInterface;

    const-string v2, "ssid"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/sec/android/band/BandInterface;->ssid:Ljava/lang/String;

    .line 314
    iget-object v1, p0, Lcom/sec/android/band/BandInterface$1;->this$0:Lcom/sec/android/band/BandInterface;

    const-string v2, "bssid"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/sec/android/band/BandInterface;->bssid:Ljava/lang/String;

    .line 315
    iget-object v1, p0, Lcom/sec/android/band/BandInterface$1;->this$0:Lcom/sec/android/band/BandInterface;

    const-string v2, "passphrase"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/sec/android/band/BandInterface;->passphrase:Ljava/lang/String;

    .line 317
    const-string v1, "serviceChunk"

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 318
    iget-object v1, p0, Lcom/sec/android/band/BandInterface$1;->this$0:Lcom/sec/android/band/BandInterface;

    const-string v2, "serviceChunk"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v2

    iput-object v2, v1, Lcom/sec/android/band/BandInterface;->serviceChunk:[B

    .line 329
    :cond_4
    const-string v1, "BAND_LOG_PROTO_GP"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[GP]I/Intent : BAND_NDEF_DISCOVERED - SSID["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/band/BandInterface$1;->this$0:Lcom/sec/android/band/BandInterface;

    iget-object v3, v3, Lcom/sec/android/band/BandInterface;->ssid:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "BSSID["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/band/BandInterface$1;->this$0:Lcom/sec/android/band/BandInterface;

    iget-object v3, v3, Lcom/sec/android/band/BandInterface;->bssid:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "Passphrase["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/band/BandInterface$1;->this$0:Lcom/sec/android/band/BandInterface;

    iget-object v3, v3, Lcom/sec/android/band/BandInterface;->passphrase:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 334
    const-string v1, "BandInterface"

    const-string v2, "Bringing registerd Activity to front"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 335
    iget-object v1, p0, Lcom/sec/android/band/BandInterface$1;->this$0:Lcom/sec/android/band/BandInterface;

    # getter for: Lcom/sec/android/band/BandInterface;->mBandEventHandler:Lcom/sec/android/band/BandInterface$BandEventHandler;
    invoke-static {v1}, Lcom/sec/android/band/BandInterface;->access$000(Lcom/sec/android/band/BandInterface;)Lcom/sec/android/band/BandInterface$BandEventHandler;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/android/band/BandInterface$BandEventHandler;->onPreNdefDiscovered()I

    move-result v1

    if-ltz v1, :cond_0

    .line 338
    iget-object v1, p0, Lcom/sec/android/band/BandInterface$1;->this$0:Lcom/sec/android/band/BandInterface;

    new-instance v2, Landroid/os/Handler;

    iget-object v3, p0, Lcom/sec/android/band/BandInterface$1;->this$0:Lcom/sec/android/band/BandInterface;

    # getter for: Lcom/sec/android/band/BandInterface;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/band/BandInterface;->access$200(Lcom/sec/android/band/BandInterface;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    # setter for: Lcom/sec/android/band/BandInterface;->mHandler:Landroid/os/Handler;
    invoke-static {v1, v2}, Lcom/sec/android/band/BandInterface;->access$102(Lcom/sec/android/band/BandInterface;Landroid/os/Handler;)Landroid/os/Handler;

    .line 339
    new-instance v14, Lcom/sec/android/band/BandInterface$1$1;

    invoke-direct {v14, p0}, Lcom/sec/android/band/BandInterface$1$1;-><init>(Lcom/sec/android/band/BandInterface$1;)V

    .line 385
    .local v14, "runnable":Ljava/lang/Runnable;
    new-instance v1, Ljava/lang/Thread;

    invoke-direct {v1, v14}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    goto/16 :goto_0

    .line 387
    .end local v14    # "runnable":Ljava/lang/Runnable;
    :cond_5
    const-string v1, "com.sec.android.band.SERVICE_RESULT"

    invoke-virtual {v11, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 389
    iget-object v1, p0, Lcom/sec/android/band/BandInterface$1;->this$0:Lcom/sec/android/band/BandInterface;

    const-string v2, "result"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/sec/android/band/BandInterface;->result:Ljava/lang/String;

    .line 391
    iget-object v1, p0, Lcom/sec/android/band/BandInterface$1;->this$0:Lcom/sec/android/band/BandInterface;

    iget-object v1, v1, Lcom/sec/android/band/BandInterface;->result:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/band/BandInterface$1;->this$0:Lcom/sec/android/band/BandInterface;

    # getter for: Lcom/sec/android/band/BandInterface;->mBandActionListener:Lcom/sec/android/band/BandInterface$BandActionListener;
    invoke-static {v1}, Lcom/sec/android/band/BandInterface;->access$400(Lcom/sec/android/band/BandInterface;)Lcom/sec/android/band/BandInterface$BandActionListener;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 392
    iget-object v1, p0, Lcom/sec/android/band/BandInterface$1;->this$0:Lcom/sec/android/band/BandInterface;

    iget-object v1, v1, Lcom/sec/android/band/BandInterface;->result:Ljava/lang/String;

    const-string v2, "Success"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 393
    iget-object v1, p0, Lcom/sec/android/band/BandInterface$1;->this$0:Lcom/sec/android/band/BandInterface;

    # getter for: Lcom/sec/android/band/BandInterface;->mBandActionListener:Lcom/sec/android/band/BandInterface$BandActionListener;
    invoke-static {v1}, Lcom/sec/android/band/BandInterface;->access$400(Lcom/sec/android/band/BandInterface;)Lcom/sec/android/band/BandInterface$BandActionListener;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/android/band/BandInterface$BandActionListener;->onSuccess()I

    .line 397
    :cond_6
    :goto_1
    iget-object v1, p0, Lcom/sec/android/band/BandInterface$1;->this$0:Lcom/sec/android/band/BandInterface;

    const/4 v2, 0x0

    # setter for: Lcom/sec/android/band/BandInterface;->mBandActionListener:Lcom/sec/android/band/BandInterface$BandActionListener;
    invoke-static {v1, v2}, Lcom/sec/android/band/BandInterface;->access$402(Lcom/sec/android/band/BandInterface;Lcom/sec/android/band/BandInterface$BandActionListener;)Lcom/sec/android/band/BandInterface$BandActionListener;

    goto/16 :goto_0

    .line 394
    :cond_7
    iget-object v1, p0, Lcom/sec/android/band/BandInterface$1;->this$0:Lcom/sec/android/band/BandInterface;

    iget-object v1, v1, Lcom/sec/android/band/BandInterface;->result:Ljava/lang/String;

    const-string v2, "Fail"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 395
    iget-object v1, p0, Lcom/sec/android/band/BandInterface$1;->this$0:Lcom/sec/android/band/BandInterface;

    # getter for: Lcom/sec/android/band/BandInterface;->mBandActionListener:Lcom/sec/android/band/BandInterface$BandActionListener;
    invoke-static {v1}, Lcom/sec/android/band/BandInterface;->access$400(Lcom/sec/android/band/BandInterface;)Lcom/sec/android/band/BandInterface$BandActionListener;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/android/band/BandInterface$BandActionListener;->onFail()I

    goto :goto_1

    .line 399
    :cond_8
    const-string v1, "com.sec.android.band.REQUEST_NDEFMESSAGE"

    invoke-virtual {v11, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 401
    iget-object v1, p0, Lcom/sec/android/band/BandInterface$1;->this$0:Lcom/sec/android/band/BandInterface;

    # getter for: Lcom/sec/android/band/BandInterface;->mBandEventHandler:Lcom/sec/android/band/BandInterface$BandEventHandler;
    invoke-static {v1}, Lcom/sec/android/band/BandInterface;->access$000(Lcom/sec/android/band/BandInterface;)Lcom/sec/android/band/BandInterface$BandEventHandler;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/android/band/BandInterface$BandEventHandler;->onRequestNdefMessage()Landroid/nfc/NdefMessage;

    move-result-object v12

    .line 402
    .local v12, "msg":Landroid/nfc/NdefMessage;
    new-instance v13, Landroid/content/Intent;

    const-string v1, "com.sec.android.band.RESPONSE_NDEFMESSAGE"

    invoke-direct {v13, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 403
    .local v13, "newIntent":Landroid/content/Intent;
    const-string v1, "ndef"

    invoke-virtual {v13, v1, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 404
    const-string v1, "com.sec.android.band"

    invoke-virtual {v13, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 405
    iget-object v1, p0, Lcom/sec/android/band/BandInterface$1;->this$0:Lcom/sec/android/band/BandInterface;

    # getter for: Lcom/sec/android/band/BandInterface;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/band/BandInterface;->access$200(Lcom/sec/android/band/BandInterface;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v13}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 408
    const-string v1, "BAND_LOG_PROTO_GP"

    const-string v2, "[GP]I/Intent : BAND_RESPONSE_NDEFMESSAGE"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method
