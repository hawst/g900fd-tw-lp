.class public Lcom/sec/android/band/BandActivity;
.super Lcom/samsung/groupcast/application/GPActivity;
.source "BandActivity.java"


# static fields
.field public static final EXTERNAL_APP:Ljava/lang/String; = "External_App"

.field public static final GP_BAND_LOG:Ljava/lang/String; = "GroupPlayBand"

.field public static final PARAMETER_DIVIDER:Ljava/lang/String; = ":"

.field public static final SESSIONSUMM_DIVIDER:Ljava/lang/String; = ";"


# instance fields
.field private mBandInterface:Lcom/sec/android/band/BandInterface;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/samsung/groupcast/application/GPActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public getBandInterface()Lcom/sec/android/band/BandInterface;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/band/BandActivity;->mBandInterface:Lcom/sec/android/band/BandInterface;

    return-object v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 27
    invoke-super {p0, p1}, Lcom/samsung/groupcast/application/GPActivity;->onCreate(Landroid/os/Bundle;)V

    .line 28
    new-instance v0, Lcom/sec/android/band/BandInterface;

    invoke-direct {v0, p0}, Lcom/sec/android/band/BandInterface;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/band/BandActivity;->mBandInterface:Lcom/sec/android/band/BandInterface;

    .line 29
    iget-object v0, p0, Lcom/sec/android/band/BandActivity;->mBandInterface:Lcom/sec/android/band/BandInterface;

    invoke-virtual {v0}, Lcom/sec/android/band/BandInterface;->registerBroacastReceiver()Z

    .line 31
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 63
    invoke-super {p0}, Lcom/samsung/groupcast/application/GPActivity;->onDestroy()V

    .line 64
    iget-object v0, p0, Lcom/sec/android/band/BandActivity;->mBandInterface:Lcom/sec/android/band/BandInterface;

    invoke-virtual {v0}, Lcom/sec/android/band/BandInterface;->unregisterBroadcastReceiver()Z

    .line 66
    return-void
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 56
    invoke-super {p0}, Lcom/samsung/groupcast/application/GPActivity;->onPause()V

    .line 57
    iget-object v0, p0, Lcom/sec/android/band/BandActivity;->mBandInterface:Lcom/sec/android/band/BandInterface;

    invoke-virtual {v0}, Lcom/sec/android/band/BandInterface;->pause()V

    .line 58
    return-void
.end method

.method protected onResume()V
    .locals 4

    .prologue
    .line 37
    invoke-super {p0}, Lcom/samsung/groupcast/application/GPActivity;->onResume()V

    .line 40
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    .line 42
    .local v1, "status_info":Ljava/lang/String;
    instance-of v2, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;

    if-eqz v2, :cond_0

    move-object v2, p0

    .line 44
    check-cast v2, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;

    invoke-virtual {v2}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->getCurrentActivityName()Ljava/lang/String;

    move-result-object v0

    .line 45
    .local v0, "currentName":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 46
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 50
    .end local v0    # "currentName":Ljava/lang/String;
    :cond_0
    iget-object v2, p0, Lcom/sec/android/band/BandActivity;->mBandInterface:Lcom/sec/android/band/BandInterface;

    invoke-virtual {v2, v1}, Lcom/sec/android/band/BandInterface;->update(Ljava/lang/String;)Z

    .line 51
    return-void
.end method
