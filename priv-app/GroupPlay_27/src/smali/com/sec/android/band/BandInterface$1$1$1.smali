.class Lcom/sec/android/band/BandInterface$1$1$1;
.super Ljava/lang/Object;
.source "BandInterface.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/band/BandInterface$1$1;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$2:Lcom/sec/android/band/BandInterface$1$1;


# direct methods
.method constructor <init>(Lcom/sec/android/band/BandInterface$1$1;)V
    .locals 0

    .prologue
    .line 363
    iput-object p1, p0, Lcom/sec/android/band/BandInterface$1$1$1;->this$2:Lcom/sec/android/band/BandInterface$1$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 367
    iget-object v0, p0, Lcom/sec/android/band/BandInterface$1$1$1;->this$2:Lcom/sec/android/band/BandInterface$1$1;

    iget-object v0, v0, Lcom/sec/android/band/BandInterface$1$1;->this$1:Lcom/sec/android/band/BandInterface$1;

    iget-object v0, v0, Lcom/sec/android/band/BandInterface$1;->this$0:Lcom/sec/android/band/BandInterface;

    iget-object v0, v0, Lcom/sec/android/band/BandInterface;->serviceChunk:[B

    if-eqz v0, :cond_0

    .line 368
    iget-object v0, p0, Lcom/sec/android/band/BandInterface$1$1$1;->this$2:Lcom/sec/android/band/BandInterface$1$1;

    iget-object v0, v0, Lcom/sec/android/band/BandInterface$1$1;->this$1:Lcom/sec/android/band/BandInterface$1;

    iget-object v0, v0, Lcom/sec/android/band/BandInterface$1;->this$0:Lcom/sec/android/band/BandInterface;

    # getter for: Lcom/sec/android/band/BandInterface;->mBandEventHandler:Lcom/sec/android/band/BandInterface$BandEventHandler;
    invoke-static {v0}, Lcom/sec/android/band/BandInterface;->access$000(Lcom/sec/android/band/BandInterface;)Lcom/sec/android/band/BandInterface$BandEventHandler;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/band/BandInterface$1$1$1;->this$2:Lcom/sec/android/band/BandInterface$1$1;

    iget-object v1, v1, Lcom/sec/android/band/BandInterface$1$1;->this$1:Lcom/sec/android/band/BandInterface$1;

    iget-object v1, v1, Lcom/sec/android/band/BandInterface$1;->this$0:Lcom/sec/android/band/BandInterface;

    iget-object v1, v1, Lcom/sec/android/band/BandInterface;->ssid:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/android/band/BandInterface$1$1$1;->this$2:Lcom/sec/android/band/BandInterface$1$1;

    iget-object v2, v2, Lcom/sec/android/band/BandInterface$1$1;->this$1:Lcom/sec/android/band/BandInterface$1;

    iget-object v2, v2, Lcom/sec/android/band/BandInterface$1;->this$0:Lcom/sec/android/band/BandInterface;

    iget-object v2, v2, Lcom/sec/android/band/BandInterface;->bssid:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/android/band/BandInterface$1$1$1;->this$2:Lcom/sec/android/band/BandInterface$1$1;

    iget-object v3, v3, Lcom/sec/android/band/BandInterface$1$1;->this$1:Lcom/sec/android/band/BandInterface$1;

    iget-object v3, v3, Lcom/sec/android/band/BandInterface$1;->this$0:Lcom/sec/android/band/BandInterface;

    iget-object v3, v3, Lcom/sec/android/band/BandInterface;->passphrase:Ljava/lang/String;

    new-instance v4, Ljava/lang/String;

    iget-object v5, p0, Lcom/sec/android/band/BandInterface$1$1$1;->this$2:Lcom/sec/android/band/BandInterface$1$1;

    iget-object v5, v5, Lcom/sec/android/band/BandInterface$1$1;->this$1:Lcom/sec/android/band/BandInterface$1;

    iget-object v5, v5, Lcom/sec/android/band/BandInterface$1;->this$0:Lcom/sec/android/band/BandInterface;

    iget-object v5, v5, Lcom/sec/android/band/BandInterface;->serviceChunk:[B

    invoke-direct {v4, v5}, Ljava/lang/String;-><init>([B)V

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/sec/android/band/BandInterface$BandEventHandler;->onNdefDiscovered(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    .line 375
    :goto_0
    return-void

    .line 371
    :cond_0
    iget-object v0, p0, Lcom/sec/android/band/BandInterface$1$1$1;->this$2:Lcom/sec/android/band/BandInterface$1$1;

    iget-object v0, v0, Lcom/sec/android/band/BandInterface$1$1;->this$1:Lcom/sec/android/band/BandInterface$1;

    iget-object v0, v0, Lcom/sec/android/band/BandInterface$1;->this$0:Lcom/sec/android/band/BandInterface;

    # getter for: Lcom/sec/android/band/BandInterface;->mBandEventHandler:Lcom/sec/android/band/BandInterface$BandEventHandler;
    invoke-static {v0}, Lcom/sec/android/band/BandInterface;->access$000(Lcom/sec/android/band/BandInterface;)Lcom/sec/android/band/BandInterface$BandEventHandler;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/band/BandInterface$1$1$1;->this$2:Lcom/sec/android/band/BandInterface$1$1;

    iget-object v1, v1, Lcom/sec/android/band/BandInterface$1$1;->this$1:Lcom/sec/android/band/BandInterface$1;

    iget-object v1, v1, Lcom/sec/android/band/BandInterface$1;->this$0:Lcom/sec/android/band/BandInterface;

    iget-object v1, v1, Lcom/sec/android/band/BandInterface;->ssid:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/android/band/BandInterface$1$1$1;->this$2:Lcom/sec/android/band/BandInterface$1$1;

    iget-object v2, v2, Lcom/sec/android/band/BandInterface$1$1;->this$1:Lcom/sec/android/band/BandInterface$1;

    iget-object v2, v2, Lcom/sec/android/band/BandInterface$1;->this$0:Lcom/sec/android/band/BandInterface;

    iget-object v2, v2, Lcom/sec/android/band/BandInterface;->bssid:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/android/band/BandInterface$1$1$1;->this$2:Lcom/sec/android/band/BandInterface$1$1;

    iget-object v3, v3, Lcom/sec/android/band/BandInterface$1$1;->this$1:Lcom/sec/android/band/BandInterface$1;

    iget-object v3, v3, Lcom/sec/android/band/BandInterface$1;->this$0:Lcom/sec/android/band/BandInterface;

    iget-object v3, v3, Lcom/sec/android/band/BandInterface;->passphrase:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/sec/android/band/BandInterface$BandEventHandler;->onNdefDiscovered(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
