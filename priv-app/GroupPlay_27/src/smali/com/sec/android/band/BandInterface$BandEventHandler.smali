.class public interface abstract Lcom/sec/android/band/BandInterface$BandEventHandler;
.super Ljava/lang/Object;
.source "BandInterface.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/band/BandInterface;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "BandEventHandler"
.end annotation


# virtual methods
.method public abstract onConfiguration(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)I
.end method

.method public abstract onNdefDiscovered(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
.end method

.method public abstract onNetworkStatus(Ljava/lang/String;)I
.end method

.method public abstract onPreNdefDiscovered()I
.end method

.method public abstract onRequestNdefMessage()Landroid/nfc/NdefMessage;
.end method
