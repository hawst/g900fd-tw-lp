.class Lcom/sec/android/band/BandInterface$1$1;
.super Ljava/lang/Object;
.source "BandInterface.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/band/BandInterface$1;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final RETRY_COUNT:I

.field final WAIT_TIME:I

.field count:I

.field final synthetic this$1:Lcom/sec/android/band/BandInterface$1;


# direct methods
.method constructor <init>(Lcom/sec/android/band/BandInterface$1;)V
    .locals 1

    .prologue
    .line 339
    iput-object p1, p0, Lcom/sec/android/band/BandInterface$1$1;->this$1:Lcom/sec/android/band/BandInterface$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 340
    const/16 v0, 0x12c

    iput v0, p0, Lcom/sec/android/band/BandInterface$1$1;->RETRY_COUNT:I

    .line 341
    const/16 v0, 0xa

    iput v0, p0, Lcom/sec/android/band/BandInterface$1$1;->WAIT_TIME:I

    .line 342
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/band/BandInterface$1$1;->count:I

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 346
    invoke-static {}, Landroid/os/Looper;->prepare()V

    .line 347
    :goto_0
    iget-object v1, p0, Lcom/sec/android/band/BandInterface$1$1;->this$1:Lcom/sec/android/band/BandInterface$1;

    iget-object v1, v1, Lcom/sec/android/band/BandInterface$1;->this$0:Lcom/sec/android/band/BandInterface;

    # getter for: Lcom/sec/android/band/BandInterface;->mUpdate:Z
    invoke-static {v1}, Lcom/sec/android/band/BandInterface;->access$300(Lcom/sec/android/band/BandInterface;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 348
    iget v1, p0, Lcom/sec/android/band/BandInterface$1$1;->count:I

    const/16 v2, 0x12c

    if-le v1, v2, :cond_2

    .line 350
    const-string v1, "BandInterface"

    const-string v2, "Failed to send NDEF Discovered Message"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 362
    :cond_0
    :goto_1
    iget-object v1, p0, Lcom/sec/android/band/BandInterface$1$1;->this$1:Lcom/sec/android/band/BandInterface$1;

    iget-object v1, v1, Lcom/sec/android/band/BandInterface$1;->this$0:Lcom/sec/android/band/BandInterface;

    # getter for: Lcom/sec/android/band/BandInterface;->mUpdate:Z
    invoke-static {v1}, Lcom/sec/android/band/BandInterface;->access$300(Lcom/sec/android/band/BandInterface;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 363
    iget-object v1, p0, Lcom/sec/android/band/BandInterface$1$1;->this$1:Lcom/sec/android/band/BandInterface$1;

    iget-object v1, v1, Lcom/sec/android/band/BandInterface$1;->this$0:Lcom/sec/android/band/BandInterface;

    # getter for: Lcom/sec/android/band/BandInterface;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/band/BandInterface;->access$100(Lcom/sec/android/band/BandInterface;)Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Lcom/sec/android/band/BandInterface$1$1$1;

    invoke-direct {v2, p0}, Lcom/sec/android/band/BandInterface$1$1$1;-><init>(Lcom/sec/android/band/BandInterface$1$1;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 380
    :cond_1
    const-string v1, "BandInterface"

    const-string v2, "onNdefDiscovered sent!"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 381
    invoke-static {}, Landroid/os/Looper;->loop()V

    .line 382
    return-void

    .line 355
    :cond_2
    const-wide/16 v1, 0xa

    :try_start_0
    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 359
    iget v1, p0, Lcom/sec/android/band/BandInterface$1$1;->count:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/sec/android/band/BandInterface$1$1;->count:I

    goto :goto_0

    .line 356
    :catch_0
    move-exception v0

    .line 357
    .local v0, "e":Ljava/lang/InterruptedException;
    goto :goto_1
.end method
