.class public final Lcom/sec/android/band/BandInterface$BAND_ACTION;
.super Ljava/lang/Object;
.source "BandInterface.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/band/BandInterface;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "BAND_ACTION"
.end annotation


# static fields
.field public static final CONFIGURATION:Ljava/lang/String; = "com.sec.android.band.CONFIGURAITON"

.field public static final NDEF_DISCOVERED:Ljava/lang/String; = "com.sec.android.band.NDEF_DISCOVERED"

.field public static final NETWORK_RELEASE:Ljava/lang/String; = "com.sec.android.band.NETWORK_RELEASE"

.field public static final NETWORK_SETUP:Ljava/lang/String; = "com.sec.android.band.NETWORK_SETUP"

.field public static final NETWORK_STATUS:Ljava/lang/String; = "com.sec.android.band.NETWORK_STATUS"

.field public static final REGISTER:Ljava/lang/String; = "com.sec.android.band.REGISTER"

.field public static final REQUEST_NDEFMESSAGE:Ljava/lang/String; = "com.sec.android.band.REQUEST_NDEFMESSAGE"

.field public static final RESPONSE_NDEFMESSAGE:Ljava/lang/String; = "com.sec.android.band.RESPONSE_NDEFMESSAGE"

.field public static final SERVICE_INFO:Ljava/lang/String; = "com.sec.android.band.SERVICE_INFO"

.field public static final SERVICE_RESULT:Ljava/lang/String; = "com.sec.android.band.SERVICE_RESULT"

.field public static final UNREGISTER:Ljava/lang/String; = "com.sec.android.band.UNREGISTER"

.field public static final UPDATE:Ljava/lang/String; = "com.sec.android.band.UPDATE"


# instance fields
.field final synthetic this$0:Lcom/sec/android/band/BandInterface;


# direct methods
.method public constructor <init>(Lcom/sec/android/band/BandInterface;)V
    .locals 0

    .prologue
    .line 101
    iput-object p1, p0, Lcom/sec/android/band/BandInterface$BAND_ACTION;->this$0:Lcom/sec/android/band/BandInterface;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
