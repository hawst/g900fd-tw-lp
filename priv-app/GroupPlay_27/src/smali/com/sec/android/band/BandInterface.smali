.class public final Lcom/sec/android/band/BandInterface;
.super Ljava/lang/Object;
.source "BandInterface.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/band/BandInterface$BandActionListener;,
        Lcom/sec/android/band/BandInterface$BandEventHandler;,
        Lcom/sec/android/band/BandInterface$BAND_NETWORK_STATUS;,
        Lcom/sec/android/band/BandInterface$BAND_ACTION_RESULT;,
        Lcom/sec/android/band/BandInterface$BAND_ENCRYPTION_TYPE;,
        Lcom/sec/android/band/BandInterface$BAND_AUTH_TYPE;,
        Lcom/sec/android/band/BandInterface$BAND_NETWORK_SETUP_TYPE;,
        Lcom/sec/android/band/BandInterface$BAND_EXTRA;,
        Lcom/sec/android/band/BandInterface$BAND_ACTION;
    }
.end annotation


# static fields
.field public static final BAND_CAPABILITY_INFO:Ljava/lang/String; = "Undefined"

.field public static final BAND_PACKAGE_NAME:Ljava/lang/String; = "com.sec.android.band"

.field public static final BAND_TARGET_VERSION:Ljava/lang/String; = "1.0"

.field private static final DBG:Z = true

.field private static final LOG_BAND_PROTOCOL:Ljava/lang/String; = "BAND_LOG_PROTO_GP"

.field private static final LOG_PREFIX:Ljava/lang/String; = "[GP]"

.field private static final LOG_TAG:Ljava/lang/String; = "BandInterface"

.field public static final LOG_TYPE:Ljava/lang/String; = "GP"

.field public static final TIME_DBG:Z = true

.field public static final TIME_DBG_LOGTAG:Ljava/lang/String; = "Band_Time"

.field public static final VER:Ljava/lang/String; = "1.03"

.field private static sBandInterface:Lcom/sec/android/band/BandInterface;


# instance fields
.field authentication:Ljava/lang/String;

.field bssid:Ljava/lang/String;

.field defBssid:Ljava/lang/String;

.field defSsid:Ljava/lang/String;

.field encryption:Ljava/lang/String;

.field private mBandActionFilter:Landroid/content/IntentFilter;

.field private mBandActionListener:Lcom/sec/android/band/BandInterface$BandActionListener;

.field private final mBandActionReceiver:Landroid/content/BroadcastReceiver;

.field private mBandEventHandler:Lcom/sec/android/band/BandInterface$BandEventHandler;

.field private final mContext:Landroid/content/Context;

.field private mHandler:Landroid/os/Handler;

.field private mIsReceiverRegistered:Z

.field private mIsRegistered:Z

.field private mNdefMessageCallback:Landroid/nfc/NfcAdapter$CreateNdefMessageCallback;

.field private mUpdate:Z

.field newConnection:Z

.field passphrase:Ljava/lang/String;

.field pkgName:Ljava/lang/String;

.field result:Ljava/lang/String;

.field serviceChunk:[B

.field ssid:Ljava/lang/String;

.field status:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 46
    new-instance v0, Lcom/sec/android/band/BandInterface;

    invoke-direct {v0}, Lcom/sec/android/band/BandInterface;-><init>()V

    sput-object v0, Lcom/sec/android/band/BandInterface;->sBandInterface:Lcom/sec/android/band/BandInterface;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 426
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 241
    iput-object v0, p0, Lcom/sec/android/band/BandInterface;->mBandActionFilter:Landroid/content/IntentFilter;

    .line 243
    iput-object v0, p0, Lcom/sec/android/band/BandInterface;->mBandActionListener:Lcom/sec/android/band/BandInterface$BandActionListener;

    .line 245
    iput-object v0, p0, Lcom/sec/android/band/BandInterface;->mBandEventHandler:Lcom/sec/android/band/BandInterface$BandEventHandler;

    .line 247
    iput-object v0, p0, Lcom/sec/android/band/BandInterface;->mNdefMessageCallback:Landroid/nfc/NfcAdapter$CreateNdefMessageCallback;

    .line 249
    iput-boolean v1, p0, Lcom/sec/android/band/BandInterface;->mUpdate:Z

    .line 251
    iput-object v0, p0, Lcom/sec/android/band/BandInterface;->mHandler:Landroid/os/Handler;

    .line 254
    iput-boolean v1, p0, Lcom/sec/android/band/BandInterface;->mIsRegistered:Z

    .line 256
    iput-boolean v1, p0, Lcom/sec/android/band/BandInterface;->mIsReceiverRegistered:Z

    .line 262
    iput-boolean v1, p0, Lcom/sec/android/band/BandInterface;->newConnection:Z

    .line 266
    new-instance v0, Lcom/sec/android/band/BandInterface$1;

    invoke-direct {v0, p0}, Lcom/sec/android/band/BandInterface$1;-><init>(Lcom/sec/android/band/BandInterface;)V

    iput-object v0, p0, Lcom/sec/android/band/BandInterface;->mBandActionReceiver:Landroid/content/BroadcastReceiver;

    .line 427
    invoke-static {}, Lcom/samsung/groupcast/application/App;->getInstance()Lcom/samsung/groupcast/application/App;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/groupcast/application/App;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/band/BandInterface;->mContext:Landroid/content/Context;

    .line 428
    invoke-direct {p0}, Lcom/sec/android/band/BandInterface;->onCreate()V

    .line 429
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 431
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 241
    iput-object v0, p0, Lcom/sec/android/band/BandInterface;->mBandActionFilter:Landroid/content/IntentFilter;

    .line 243
    iput-object v0, p0, Lcom/sec/android/band/BandInterface;->mBandActionListener:Lcom/sec/android/band/BandInterface$BandActionListener;

    .line 245
    iput-object v0, p0, Lcom/sec/android/band/BandInterface;->mBandEventHandler:Lcom/sec/android/band/BandInterface$BandEventHandler;

    .line 247
    iput-object v0, p0, Lcom/sec/android/band/BandInterface;->mNdefMessageCallback:Landroid/nfc/NfcAdapter$CreateNdefMessageCallback;

    .line 249
    iput-boolean v1, p0, Lcom/sec/android/band/BandInterface;->mUpdate:Z

    .line 251
    iput-object v0, p0, Lcom/sec/android/band/BandInterface;->mHandler:Landroid/os/Handler;

    .line 254
    iput-boolean v1, p0, Lcom/sec/android/band/BandInterface;->mIsRegistered:Z

    .line 256
    iput-boolean v1, p0, Lcom/sec/android/band/BandInterface;->mIsReceiverRegistered:Z

    .line 262
    iput-boolean v1, p0, Lcom/sec/android/band/BandInterface;->newConnection:Z

    .line 266
    new-instance v0, Lcom/sec/android/band/BandInterface$1;

    invoke-direct {v0, p0}, Lcom/sec/android/band/BandInterface$1;-><init>(Lcom/sec/android/band/BandInterface;)V

    iput-object v0, p0, Lcom/sec/android/band/BandInterface;->mBandActionReceiver:Landroid/content/BroadcastReceiver;

    .line 432
    iput-object p1, p0, Lcom/sec/android/band/BandInterface;->mContext:Landroid/content/Context;

    .line 433
    invoke-direct {p0}, Lcom/sec/android/band/BandInterface;->onCreate()V

    .line 434
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/band/BandInterface;)Lcom/sec/android/band/BandInterface$BandEventHandler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/band/BandInterface;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/android/band/BandInterface;->mBandEventHandler:Lcom/sec/android/band/BandInterface$BandEventHandler;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/band/BandInterface;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/band/BandInterface;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/android/band/BandInterface;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$102(Lcom/sec/android/band/BandInterface;Landroid/os/Handler;)Landroid/os/Handler;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/band/BandInterface;
    .param p1, "x1"    # Landroid/os/Handler;

    .prologue
    .line 40
    iput-object p1, p0, Lcom/sec/android/band/BandInterface;->mHandler:Landroid/os/Handler;

    return-object p1
.end method

.method static synthetic access$200(Lcom/sec/android/band/BandInterface;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/band/BandInterface;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/android/band/BandInterface;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/band/BandInterface;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/band/BandInterface;

    .prologue
    .line 40
    iget-boolean v0, p0, Lcom/sec/android/band/BandInterface;->mUpdate:Z

    return v0
.end method

.method static synthetic access$400(Lcom/sec/android/band/BandInterface;)Lcom/sec/android/band/BandInterface$BandActionListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/band/BandInterface;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/android/band/BandInterface;->mBandActionListener:Lcom/sec/android/band/BandInterface$BandActionListener;

    return-object v0
.end method

.method static synthetic access$402(Lcom/sec/android/band/BandInterface;Lcom/sec/android/band/BandInterface$BandActionListener;)Lcom/sec/android/band/BandInterface$BandActionListener;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/band/BandInterface;
    .param p1, "x1"    # Lcom/sec/android/band/BandInterface$BandActionListener;

    .prologue
    .line 40
    iput-object p1, p0, Lcom/sec/android/band/BandInterface;->mBandActionListener:Lcom/sec/android/band/BandInterface$BandActionListener;

    return-object p1
.end method

.method static synthetic access$500(Lcom/sec/android/band/BandInterface;)Landroid/nfc/NdefMessage;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/band/BandInterface;

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/sec/android/band/BandInterface;->composeAndroidBeamNdef()Landroid/nfc/NdefMessage;

    move-result-object v0

    return-object v0
.end method

.method private composeAndroidBeamNdef()Landroid/nfc/NdefMessage;
    .locals 1

    .prologue
    .line 830
    const/4 v0, 0x0

    return-object v0
.end method

.method public static getInstance()Lcom/sec/android/band/BandInterface;
    .locals 1

    .prologue
    .line 415
    sget-object v0, Lcom/sec/android/band/BandInterface;->sBandInterface:Lcom/sec/android/band/BandInterface;

    return-object v0
.end method

.method private getNdefPushMessageCallback()Landroid/nfc/NfcAdapter$CreateNdefMessageCallback;
    .locals 1

    .prologue
    .line 835
    iget-object v0, p0, Lcom/sec/android/band/BandInterface;->mNdefMessageCallback:Landroid/nfc/NfcAdapter$CreateNdefMessageCallback;

    if-nez v0, :cond_0

    .line 836
    new-instance v0, Lcom/sec/android/band/BandInterface$2;

    invoke-direct {v0, p0}, Lcom/sec/android/band/BandInterface$2;-><init>(Lcom/sec/android/band/BandInterface;)V

    iput-object v0, p0, Lcom/sec/android/band/BandInterface;->mNdefMessageCallback:Landroid/nfc/NfcAdapter$CreateNdefMessageCallback;

    .line 843
    :cond_0
    iget-object v0, p0, Lcom/sec/android/band/BandInterface;->mNdefMessageCallback:Landroid/nfc/NfcAdapter$CreateNdefMessageCallback;

    return-object v0
.end method

.method private onCreate()V
    .locals 2

    .prologue
    .line 447
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iput-object v0, p0, Lcom/sec/android/band/BandInterface;->mBandActionFilter:Landroid/content/IntentFilter;

    .line 448
    iget-object v0, p0, Lcom/sec/android/band/BandInterface;->mBandActionFilter:Landroid/content/IntentFilter;

    const-string v1, "com.sec.android.band.CONFIGURAITON"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 449
    iget-object v0, p0, Lcom/sec/android/band/BandInterface;->mBandActionFilter:Landroid/content/IntentFilter;

    const-string v1, "com.sec.android.band.NETWORK_STATUS"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 450
    iget-object v0, p0, Lcom/sec/android/band/BandInterface;->mBandActionFilter:Landroid/content/IntentFilter;

    const-string v1, "com.sec.android.band.NDEF_DISCOVERED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 451
    iget-object v0, p0, Lcom/sec/android/band/BandInterface;->mBandActionFilter:Landroid/content/IntentFilter;

    const-string v1, "com.sec.android.band.SERVICE_RESULT"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 452
    iget-object v0, p0, Lcom/sec/android/band/BandInterface;->mBandActionFilter:Landroid/content/IntentFilter;

    const-string v1, "com.sec.android.band.REQUEST_NDEFMESSAGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 453
    return-void
.end method


# virtual methods
.method public doNetworkRelease(Ljava/lang/String;)Z
    .locals 5
    .param p1, "type"    # Ljava/lang/String;

    .prologue
    .line 490
    const-string v2, "BandInterface"

    const-string v3, "doNetworkRegister()"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 492
    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.sec.android.band.NETWORK_RELEASE"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 493
    .local v0, "intent":Landroid/content/Intent;
    iget-object v2, p0, Lcom/sec/android/band/BandInterface;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    .line 494
    .local v1, "pkgName":Ljava/lang/String;
    const-string v2, "packageName"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 496
    if-eqz p1, :cond_0

    .line 497
    const-string v2, "type"

    invoke-virtual {v0, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 499
    :cond_0
    const-string v2, "com.sec.android.band"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 500
    iget-object v2, p0, Lcom/sec/android/band/BandInterface;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 503
    const-string v2, "BAND_LOG_PROTO_GP"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[GP]O/Intent : BAND_NETWORK_RELEASE - PackageName["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 506
    const/4 v2, 0x1

    return v2
.end method

.method public isBandInstalled()Z
    .locals 11

    .prologue
    const/4 v10, 0x3

    const/4 v6, 0x0

    .line 726
    :try_start_0
    iget-object v7, p0, Lcom/sec/android/band/BandInterface;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v7

    const-string v8, "com.sec.android.band"

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v9}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v3

    .line 727
    .local v3, "i":Landroid/content/pm/PackageInfo;
    iget-object v2, v3, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    .line 729
    .local v2, "bandVersion":Ljava/lang/String;
    const-string v7, "BandInterface"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Band Version : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", Target Version : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "1.0"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 732
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v7

    if-lt v7, v10, :cond_0

    const-string v7, "1.0"

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    if-lt v7, v10, :cond_0

    .line 734
    const-string v7, "."

    invoke-virtual {v2, v7}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 735
    .local v0, "bandMajorIndex":I
    const-string v7, "1.0"

    const-string v8, "."

    invoke-virtual {v7, v8}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    .line 737
    .local v5, "targetVersionIndex":I
    if-lez v0, :cond_0

    if-lez v5, :cond_0

    .line 738
    const/4 v7, 0x0

    invoke-virtual {v2, v7, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 739
    .local v1, "bandMajorVersion":Ljava/lang/String;
    const-string v7, "1.0"

    const/4 v8, 0x0

    invoke-virtual {v7, v8, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 741
    .local v4, "targetVersion":Ljava/lang/String;
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    if-lt v7, v8, :cond_0

    .line 743
    const-string v7, "BandInterface"

    const-string v8, "Band is installed on this system"

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 744
    const/4 v6, 0x1

    .line 749
    .end local v0    # "bandMajorIndex":I
    .end local v1    # "bandMajorVersion":Ljava/lang/String;
    .end local v2    # "bandVersion":Ljava/lang/String;
    .end local v3    # "i":Landroid/content/pm/PackageInfo;
    .end local v4    # "targetVersion":Ljava/lang/String;
    .end local v5    # "targetVersionIndex":I
    :cond_0
    :goto_0
    return v6

    .line 748
    :catch_0
    move-exception v7

    goto :goto_0
.end method

.method public pause()V
    .locals 2

    .prologue
    .line 546
    const-string v0, "BandInterface"

    const-string v1, "pause()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 547
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/band/BandInterface;->mUpdate:Z

    .line 548
    return-void
.end method

.method public register()Z
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 463
    const-string v2, "BandInterface"

    const-string v3, "register()"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 465
    iput-boolean v5, p0, Lcom/sec/android/band/BandInterface;->mIsRegistered:Z

    .line 467
    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.sec.android.band.REGISTER"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 468
    .local v0, "intent":Landroid/content/Intent;
    iget-object v2, p0, Lcom/sec/android/band/BandInterface;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    .line 469
    .local v1, "pkgName":Ljava/lang/String;
    const/16 v2, 0x20

    invoke-virtual {v0, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 470
    const-string v2, "packageName"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 471
    const-string v2, "CapabilityInfo"

    const-string v3, "Undefined"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 473
    const-string v2, "com.sec.android.band"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 474
    iget-object v2, p0, Lcom/sec/android/band/BandInterface;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 477
    const-string v2, "BAND_LOG_PROTO_GP"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[GP]O/Intent : BAND_REGISTER - PackageName["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "CapabilityInfo["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "Undefined"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 480
    return v5
.end method

.method public registerBroacastReceiver()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 697
    const-string v0, "BandInterface"

    const-string v1, "registerBroacastReceiver()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 698
    iget-boolean v0, p0, Lcom/sec/android/band/BandInterface;->mIsReceiverRegistered:Z

    if-nez v0, :cond_0

    .line 699
    iget-object v0, p0, Lcom/sec/android/band/BandInterface;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/band/BandInterface;->mBandActionReceiver:Landroid/content/BroadcastReceiver;

    iget-object v2, p0, Lcom/sec/android/band/BandInterface;->mBandActionFilter:Landroid/content/IntentFilter;

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 700
    iput-boolean v3, p0, Lcom/sec/android/band/BandInterface;->mIsReceiverRegistered:Z

    .line 702
    :cond_0
    return v3
.end method

.method public setBandCallback(Lcom/sec/android/band/BandInterface$BandEventHandler;)V
    .locals 0
    .param p1, "bandHandler"    # Lcom/sec/android/band/BandInterface$BandEventHandler;

    .prologue
    .line 442
    iput-object p1, p0, Lcom/sec/android/band/BandInterface;->mBandEventHandler:Lcom/sec/android/band/BandInterface$BandEventHandler;

    .line 443
    return-void
.end method

.method public setNetworkSetup(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "type"    # Ljava/lang/String;
    .param p2, "ssid"    # Ljava/lang/String;
    .param p3, "passphrase"    # Ljava/lang/String;
    .param p4, "encryption"    # Ljava/lang/String;
    .param p5, "authentication"    # Ljava/lang/String;

    .prologue
    .line 589
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.band.NETWORK_SETUP"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 590
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "type"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 591
    const-string v1, "ssid"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 592
    const-string v1, "passphrase"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 594
    const-string v1, "authentication"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 595
    const-string v1, "encryption"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 596
    const-string v1, "com.sec.android.band"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 597
    iget-object v1, p0, Lcom/sec/android/band/BandInterface;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 600
    const-string v1, "BAND_LOG_PROTO_GP"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[GP]O/Intent : BAND_NETWORK_SETUP - TYPE["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "],"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "SSID["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "],"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "Passphrase["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "],"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "Security["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "],"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "Authentication["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 603
    return-void
.end method

.method public setServiceInfo(Ljava/lang/String;)V
    .locals 4
    .param p1, "chunk"    # Ljava/lang/String;

    .prologue
    .line 672
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.band.SERVICE_INFO"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 674
    .local v0, "intent":Landroid/content/Intent;
    if-eqz p1, :cond_0

    .line 680
    const-string v1, "serviceChunk"

    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 683
    :cond_0
    const-string v1, "com.sec.android.band"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 684
    iget-object v1, p0, Lcom/sec/android/band/BandInterface;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 688
    const-string v1, "BAND_LOG_PROTO_GP"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[GP]O/Intent : BAND_SERVICE_INFO - SSID["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/band/BandInterface;->ssid:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "],"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "Passphrase["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/band/BandInterface;->passphrase:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 690
    return-void
.end method

.method public setServiceInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p1, "ssid"    # Ljava/lang/String;
    .param p2, "bssid"    # Ljava/lang/String;
    .param p3, "passphrase"    # Ljava/lang/String;
    .param p4, "encryption"    # Ljava/lang/String;
    .param p5, "authentication"    # Ljava/lang/String;
    .param p6, "chunk"    # Ljava/lang/String;

    .prologue
    .line 639
    const-string v1, "Band_Time"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[S]setServiceInfo="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 641
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.band.SERVICE_INFO"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 642
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "ssid"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 643
    const-string v1, "bssid"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 644
    const-string v1, "passphrase"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 645
    const-string v1, "authentication"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 646
    const-string v1, "encryption"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 648
    if-eqz p6, :cond_0

    .line 655
    const-string v1, "serviceChunk"

    invoke-virtual {p6}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 658
    :cond_0
    const-string v1, "com.sec.android.band"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 659
    iget-object v1, p0, Lcom/sec/android/band/BandInterface;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 663
    const-string v1, "BAND_LOG_PROTO_GP"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[GP]O/Intent : BAND_SERVICE_INFO - SSID["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "],"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "Bssid["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "],"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "Passphrase["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "],"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "authentication["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "],"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "encryption["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "],"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "chunk["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 667
    return-void
.end method

.method public setServiceInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/band/BandInterface$BandActionListener;)V
    .locals 0
    .param p1, "ssid"    # Ljava/lang/String;
    .param p2, "bssid"    # Ljava/lang/String;
    .param p3, "passphrase"    # Ljava/lang/String;
    .param p4, "encryption"    # Ljava/lang/String;
    .param p5, "authentication"    # Ljava/lang/String;
    .param p6, "chunk"    # Ljava/lang/String;
    .param p7, "bandActionListener"    # Lcom/sec/android/band/BandInterface$BandActionListener;

    .prologue
    .line 620
    iput-object p7, p0, Lcom/sec/android/band/BandInterface;->mBandActionListener:Lcom/sec/android/band/BandInterface$BandActionListener;

    .line 621
    invoke-virtual/range {p0 .. p6}, Lcom/sec/android/band/BandInterface;->setServiceInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 622
    return-void
.end method

.method public unregister()Z
    .locals 5

    .prologue
    .line 558
    const-string v2, "BandInterface"

    const-string v3, "unregister()"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 560
    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.sec.android.band.UNREGISTER"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 561
    .local v0, "intent":Landroid/content/Intent;
    const-string v2, "packageName"

    iget-object v3, p0, Lcom/sec/android/band/BandInterface;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 562
    iget-object v2, p0, Lcom/sec/android/band/BandInterface;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    .line 564
    .local v1, "pkgName":Ljava/lang/String;
    const-string v2, "com.sec.android.band"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 565
    iget-object v2, p0, Lcom/sec/android/band/BandInterface;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 568
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/sec/android/band/BandInterface;->mIsRegistered:Z

    .line 571
    const-string v2, "BAND_LOG_PROTO_GP"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[GP]O/Intent : BAND_UNREGISTER - PkgName["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 574
    const/4 v2, 0x1

    return v2
.end method

.method public unregisterBroadcastReceiver()Z
    .locals 2

    .prologue
    .line 710
    const-string v0, "BandInterface"

    const-string v1, "unRegisterBroadcastReceiver()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 711
    iget-boolean v0, p0, Lcom/sec/android/band/BandInterface;->mIsReceiverRegistered:Z

    if-eqz v0, :cond_0

    .line 712
    iget-object v0, p0, Lcom/sec/android/band/BandInterface;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/band/BandInterface;->mBandActionReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 713
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/band/BandInterface;->mIsReceiverRegistered:Z

    .line 715
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public update(Ljava/lang/String;)Z
    .locals 6
    .param p1, "statusInfo"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x1

    .line 517
    iget-boolean v2, p0, Lcom/sec/android/band/BandInterface;->mIsRegistered:Z

    if-nez v2, :cond_0

    .line 518
    const-string v2, "register"

    invoke-static {v2}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 519
    invoke-virtual {p0}, Lcom/sec/android/band/BandInterface;->register()Z

    .line 522
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.sec.android.band.UPDATE"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 523
    .local v0, "intent":Landroid/content/Intent;
    iget-object v2, p0, Lcom/sec/android/band/BandInterface;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    .line 524
    .local v1, "pkgName":Ljava/lang/String;
    const-string v2, "packageName"

    iget-object v3, p0, Lcom/sec/android/band/BandInterface;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 525
    const-string v2, "statusInfo"

    invoke-virtual {v0, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 528
    const-string v2, "com.sec.android.band"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 529
    iget-object v2, p0, Lcom/sec/android/band/BandInterface;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 532
    const-string v2, "BAND_LOG_PROTO_GP"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[GP]O/Intent : BAND_UPDATE - PkgName["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "statusInfo["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "],"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 535
    iput-boolean v5, p0, Lcom/sec/android/band/BandInterface;->mUpdate:Z

    .line 537
    return v5
.end method
