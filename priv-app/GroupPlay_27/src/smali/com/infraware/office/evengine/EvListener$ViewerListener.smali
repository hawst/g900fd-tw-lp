.class public interface abstract Lcom/infraware/office/evengine/EvListener$ViewerListener;
.super Ljava/lang/Object;
.source "EvListener.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/evengine/EvListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ViewerListener"
.end annotation


# virtual methods
.method public abstract GetBitmap(II)Landroid/graphics/Bitmap;
.end method

.method public abstract OnCloseDoc()V
.end method

.method public abstract OnDrawBitmap(II)V
.end method

.method public abstract OnLoadComplete()V
.end method

.method public abstract OnLoadFail(I)V
.end method

.method public abstract OnPageMove(III)V
.end method

.method public abstract OnPrintMode(Ljava/lang/String;)V
.end method

.method public abstract OnPrintedCount(I)V
.end method

.method public abstract OnProgress(II)V
.end method

.method public abstract OnProgressStart(I)V
.end method

.method public abstract OnTerminate()V
.end method

.method public abstract OnTotalLoadComplete()V
.end method
