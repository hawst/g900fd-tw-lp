.class public abstract Lcom/infraware/office/evengine/EvInterface;
.super Ljava/lang/Object;
.source "EvInterface.java"

# interfaces
.implements Lcom/infraware/office/evengine/E$EV_DOCEXTENSION_TYPE;
.implements Lcom/infraware/office/evengine/E$EV_EDITMODETYPE;
.implements Lcom/infraware/office/evengine/E;


# static fields
.field protected static mInterface:Lcom/infraware/office/evengine/EvInterface;


# instance fields
.field protected Ev:Lcom/infraware/office/evengine/EV;

.field protected Native:Lcom/infraware/office/evengine/EvNative;

.field protected mEditorMode:I

.field protected mHeapSize:I

.field protected mbInit:Z

.field protected mbSuspend:I


# direct methods
.method protected constructor <init>(Ljava/lang/String;)V
    .locals 2
    .param p1, "AppDir"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    const/16 v0, 0x100

    iput v0, p0, Lcom/infraware/office/evengine/EvInterface;->mHeapSize:I

    .line 14
    iput-boolean v1, p0, Lcom/infraware/office/evengine/EvInterface;->mbInit:Z

    .line 15
    iput v1, p0, Lcom/infraware/office/evengine/EvInterface;->mbSuspend:I

    .line 16
    iput v1, p0, Lcom/infraware/office/evengine/EvInterface;->mEditorMode:I

    .line 20
    new-instance v0, Lcom/infraware/office/evengine/EV;

    invoke-direct {v0}, Lcom/infraware/office/evengine/EV;-><init>()V

    iput-object v0, p0, Lcom/infraware/office/evengine/EvInterface;->Ev:Lcom/infraware/office/evengine/EV;

    .line 21
    new-instance v0, Lcom/infraware/office/evengine/EvNative;

    invoke-direct {v0, p0, p1}, Lcom/infraware/office/evengine/EvNative;-><init>(Lcom/infraware/office/evengine/EvInterface;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/infraware/office/evengine/EvInterface;->Native:Lcom/infraware/office/evengine/EvNative;

    .line 22
    return-void
.end method

.method public static getInterface(Ljava/lang/String;)Lcom/infraware/office/evengine/EvInterface;
    .locals 1
    .param p0, "AppDir"    # Ljava/lang/String;

    .prologue
    .line 29
    sget-object v0, Lcom/infraware/office/evengine/EvInterface;->mInterface:Lcom/infraware/office/evengine/EvInterface;

    if-nez v0, :cond_0

    .line 30
    new-instance v0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;

    invoke-direct {v0, p0}, Lcom/infraware/office/evengine/EvCompInterfaceMsg;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/infraware/office/evengine/EvInterface;->mInterface:Lcom/infraware/office/evengine/EvInterface;

    .line 33
    :cond_0
    sget-object v0, Lcom/infraware/office/evengine/EvInterface;->mInterface:Lcom/infraware/office/evengine/EvInterface;

    return-object v0
.end method


# virtual methods
.method public EV()Lcom/infraware/office/evengine/EV;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/infraware/office/evengine/EvInterface;->Ev:Lcom/infraware/office/evengine/EV;

    return-object v0
.end method

.method public abstract ICancel()V
.end method

.method public IClose()V
    .locals 2

    .prologue
    .line 131
    iget-object v0, p0, Lcom/infraware/office/evengine/EvInterface;->Native:Lcom/infraware/office/evengine/EvNative;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/infraware/office/evengine/EvNative;->SetListener(Lcom/infraware/office/evengine/EvListener$ViewerListener;)V

    .line 132
    iget-object v0, p0, Lcom/infraware/office/evengine/EvInterface;->Ev:Lcom/infraware/office/evengine/EV;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV;->clear()V

    .line 133
    return-void
.end method

.method public abstract IFinalize()V
.end method

.method public abstract IGetConfig()Lcom/infraware/office/evengine/EV$CONFIG_INFO;
.end method

.method public IGetInitialHeapSize()I
    .locals 1

    .prologue
    .line 80
    iget v0, p0, Lcom/infraware/office/evengine/EvInterface;->mHeapSize:I

    return v0
.end method

.method public IInitialize(III)V
    .locals 2
    .param p1, "a_nWidth"    # I
    .param p2, "a_nHeight"    # I
    .param p3, "a_nDpi"    # I

    .prologue
    .line 108
    iget-object v0, p0, Lcom/infraware/office/evengine/EvInterface;->Native:Lcom/infraware/office/evengine/EvNative;

    iget v1, p0, Lcom/infraware/office/evengine/EvInterface;->mHeapSize:I

    invoke-virtual {v0, v1}, Lcom/infraware/office/evengine/EvNative;->ISetHeapSize(I)V

    .line 109
    return-void
.end method

.method public abstract IOpen(Ljava/lang/String;IIIIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract IOpenEx(Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public ISetHeapSize(I)V
    .locals 1
    .param p1, "a_nHeapSize"    # I

    .prologue
    .line 72
    iget-object v0, p0, Lcom/infraware/office/evengine/EvInterface;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1}, Lcom/infraware/office/evengine/EvNative;->ISetHeapSize(I)V

    .line 73
    return-void
.end method

.method public ISetInitialHeapSize(I)V
    .locals 0
    .param p1, "a_nHeapSize"    # I

    .prologue
    .line 84
    iput p1, p0, Lcom/infraware/office/evengine/EvInterface;->mHeapSize:I

    .line 85
    return-void
.end method

.method public abstract ISetListener(Lcom/infraware/office/evengine/EvListener$ViewerListener;)V
.end method

.method public ISetLocale(I)V
    .locals 1
    .param p1, "a_nLocale"    # I

    .prologue
    .line 76
    iget-object v0, p0, Lcom/infraware/office/evengine/EvInterface;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1}, Lcom/infraware/office/evengine/EvNative;->ISetLocale(I)V

    .line 77
    return-void
.end method

.method public abstract ISetPrint(IIILjava/lang/String;I)V
.end method

.method public abstract ISetPrintEx(IIILjava/lang/String;IILjava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract ISetWHByPrint(IILjava/lang/String;ILjava/lang/String;II)V
.end method

.method OnFinalizeComplete()V
    .locals 1

    .prologue
    .line 102
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/infraware/office/evengine/EvInterface;->mbInit:Z

    return-void
.end method

.method OnInitComplete(I)V
    .locals 1
    .param p1, "EEV_ERROR_CODE"    # I

    .prologue
    .line 101
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/infraware/office/evengine/EvInterface;->mbInit:Z

    return-void
.end method

.method abstract OnTimerStart()V
.end method

.method abstract OnTimerStop()V
.end method

.method public getDocFileExtentionType(Ljava/lang/String;)I
    .locals 4
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    .line 39
    const/16 v0, 0xff

    .line 40
    .local v0, "EV_DOCEXTENSION_TYPE":I
    const/16 v3, 0x2e

    invoke-virtual {p1, v3}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v1

    .line 41
    .local v1, "nIndex":I
    if-ltz v1, :cond_0

    .line 42
    invoke-virtual {p1, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    .line 43
    .local v2, "strExt":Ljava/lang/String;
    const-string v3, ".doc"

    invoke-virtual {v2, v3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_1

    .line 44
    const/4 v0, 0x2

    .line 63
    .end local v2    # "strExt":Ljava/lang/String;
    :cond_0
    :goto_0
    return v0

    .line 45
    .restart local v2    # "strExt":Ljava/lang/String;
    :cond_1
    const-string v3, ".docx"

    invoke-virtual {v2, v3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_2

    .line 46
    const/16 v0, 0x12

    goto :goto_0

    .line 47
    :cond_2
    const-string v3, ".ppt"

    invoke-virtual {v2, v3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_3

    .line 48
    const/4 v0, 0x1

    goto :goto_0

    .line 49
    :cond_3
    const-string v3, ".pptx"

    invoke-virtual {v2, v3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_4

    .line 50
    const/16 v0, 0x13

    goto :goto_0

    .line 57
    :cond_4
    const-string v3, ".pdf"

    invoke-virtual {v2, v3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_0

    .line 58
    const/4 v0, 0x6

    goto :goto_0
.end method

.method public getFontFileList()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/infraware/office/evengine/EvInterface;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvNative;->GetFontFileList()[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method isInit()Z
    .locals 1

    .prologue
    .line 100
    iget-boolean v0, p0, Lcom/infraware/office/evengine/EvInterface;->mbInit:Z

    return v0
.end method
