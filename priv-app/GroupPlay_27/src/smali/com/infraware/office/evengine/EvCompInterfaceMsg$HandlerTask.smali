.class public Lcom/infraware/office/evengine/EvCompInterfaceMsg$HandlerTask;
.super Landroid/os/Handler;
.source "EvCompInterfaceMsg.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/infraware/office/evengine/EvCompInterfaceMsg;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "HandlerTask"
.end annotation


# static fields
.field private static final PREVIEW_TIMER:I = 0x1

.field private static final RUNTIMER:I


# instance fields
.field private mbAlive:Z

.field private mbPreview:Z

.field final synthetic this$0:Lcom/infraware/office/evengine/EvCompInterfaceMsg;


# direct methods
.method protected constructor <init>(Lcom/infraware/office/evengine/EvCompInterfaceMsg;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 15
    iput-object p1, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg$HandlerTask;->this$0:Lcom/infraware/office/evengine/EvCompInterfaceMsg;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 19
    iput-boolean v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg$HandlerTask;->mbAlive:Z

    .line 20
    iput-boolean v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg$HandlerTask;->mbPreview:Z

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const-wide/16 v2, 0x1

    const/4 v1, 0x0

    .line 45
    invoke-virtual {p0, v1}, Lcom/infraware/office/evengine/EvCompInterfaceMsg$HandlerTask;->removeMessages(I)V

    .line 46
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 71
    :cond_0
    :goto_0
    return-void

    .line 49
    :pswitch_0
    iget-boolean v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg$HandlerTask;->mbAlive:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg$HandlerTask;->this$0:Lcom/infraware/office/evengine/EvCompInterfaceMsg;

    iget v0, v0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->mbSuspend:I

    if-nez v0, :cond_1

    .line 51
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg$HandlerTask;->this$0:Lcom/infraware/office/evengine/EvCompInterfaceMsg;

    iget-object v0, v0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvNative;->ITimer()V

    .line 54
    :cond_1
    iget-boolean v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg$HandlerTask;->mbAlive:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg$HandlerTask;->this$0:Lcom/infraware/office/evengine/EvCompInterfaceMsg;

    iget v0, v0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->mbSuspend:I

    if-nez v0, :cond_0

    .line 56
    invoke-virtual {p0, v1, v2, v3}, Lcom/infraware/office/evengine/EvCompInterfaceMsg$HandlerTask;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 60
    :pswitch_1
    iget-boolean v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg$HandlerTask;->mbPreview:Z

    if-eqz v0, :cond_2

    .line 65
    :cond_2
    iget-boolean v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg$HandlerTask;->mbPreview:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg$HandlerTask;->this$0:Lcom/infraware/office/evengine/EvCompInterfaceMsg;

    iget v0, v0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->mbSuspend:I

    if-nez v0, :cond_0

    .line 67
    const/4 v0, 0x1

    invoke-virtual {p0, v0, v2, v3}, Lcom/infraware/office/evengine/EvCompInterfaceMsg$HandlerTask;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 46
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method setOperationTimer(Z)V
    .locals 3
    .param p1, "bStart"    # Z

    .prologue
    const/4 v2, 0x0

    .line 23
    iput-boolean p1, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg$HandlerTask;->mbAlive:Z

    .line 24
    iget-boolean v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg$HandlerTask;->mbAlive:Z

    if-eqz v0, :cond_0

    .line 26
    invoke-virtual {p0, v2}, Lcom/infraware/office/evengine/EvCompInterfaceMsg$HandlerTask;->sendEmptyMessage(I)Z

    .line 31
    :goto_0
    return-void

    .line 28
    :cond_0
    const-string v0, "EvCompInterfaceMsg"

    const-string v1, "remove timer1"

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 29
    invoke-virtual {p0, v2}, Lcom/infraware/office/evengine/EvCompInterfaceMsg$HandlerTask;->removeMessages(I)V

    goto :goto_0
.end method

.method setPreviewTimer(Z)V
    .locals 3
    .param p1, "bStart"    # Z

    .prologue
    const/4 v2, 0x1

    .line 34
    iput-boolean p1, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg$HandlerTask;->mbPreview:Z

    .line 35
    iget-boolean v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg$HandlerTask;->mbPreview:Z

    if-eqz v0, :cond_0

    .line 37
    invoke-virtual {p0, v2}, Lcom/infraware/office/evengine/EvCompInterfaceMsg$HandlerTask;->sendEmptyMessage(I)Z

    .line 42
    :goto_0
    return-void

    .line 39
    :cond_0
    const-string v0, "EvCompInterfaceMsg"

    const-string v1, "remove timer1"

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 40
    invoke-virtual {p0, v2}, Lcom/infraware/office/evengine/EvCompInterfaceMsg$HandlerTask;->removeMessages(I)V

    goto :goto_0
.end method
