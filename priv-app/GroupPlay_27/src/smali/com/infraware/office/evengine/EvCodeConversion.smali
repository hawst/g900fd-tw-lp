.class public Lcom/infraware/office/evengine/EvCodeConversion;
.super Ljava/lang/Object;
.source "EvCodeConversion.java"


# static fields
.field static mEvCodeConversion:Lcom/infraware/office/evengine/EvCodeConversion;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const/4 v0, 0x0

    sput-object v0, Lcom/infraware/office/evengine/EvCodeConversion;->mEvCodeConversion:Lcom/infraware/office/evengine/EvCodeConversion;

    return-void
.end method

.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static getCodeConversion()Lcom/infraware/office/evengine/EvCodeConversion;
    .locals 1

    .prologue
    .line 14
    sget-object v0, Lcom/infraware/office/evengine/EvCodeConversion;->mEvCodeConversion:Lcom/infraware/office/evengine/EvCodeConversion;

    if-nez v0, :cond_0

    .line 15
    new-instance v0, Lcom/infraware/office/evengine/EvCodeConversion;

    invoke-direct {v0}, Lcom/infraware/office/evengine/EvCodeConversion;-><init>()V

    sput-object v0, Lcom/infraware/office/evengine/EvCodeConversion;->mEvCodeConversion:Lcom/infraware/office/evengine/EvCodeConversion;

    .line 17
    :cond_0
    sget-object v0, Lcom/infraware/office/evengine/EvCodeConversion;->mEvCodeConversion:Lcom/infraware/office/evengine/EvCodeConversion;

    return-object v0
.end method


# virtual methods
.method DecodeToUnicode(ILjava/nio/ByteBuffer;)Ljava/lang/String;
    .locals 4
    .param p1, "nCodePage"    # I
    .param p2, "byteBuffer"    # Ljava/nio/ByteBuffer;

    .prologue
    .line 23
    sparse-switch p1, :sswitch_data_0

    .line 37
    const-string v3, "windows-949"

    invoke-static {v3}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v2

    .line 40
    .local v2, "charset":Ljava/nio/charset/Charset;
    :goto_0
    invoke-virtual {v2, p2}, Ljava/nio/charset/Charset;->decode(Ljava/nio/ByteBuffer;)Ljava/nio/CharBuffer;

    move-result-object v1

    .line 42
    .local v1, "charBuffer":Ljava/nio/CharBuffer;
    invoke-virtual {v1}, Ljava/nio/CharBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    .line 43
    .local v0, "UCode":Ljava/lang/String;
    return-object v0

    .line 27
    .end local v0    # "UCode":Ljava/lang/String;
    .end local v1    # "charBuffer":Ljava/nio/CharBuffer;
    .end local v2    # "charset":Ljava/nio/charset/Charset;
    :sswitch_0
    const-string v3, "windows-949"

    invoke-static {v3}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v2

    .line 28
    .restart local v2    # "charset":Ljava/nio/charset/Charset;
    goto :goto_0

    .line 30
    .end local v2    # "charset":Ljava/nio/charset/Charset;
    :sswitch_1
    const-string v3, "windows-936"

    invoke-static {v3}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v2

    .line 31
    .restart local v2    # "charset":Ljava/nio/charset/Charset;
    goto :goto_0

    .line 33
    .end local v2    # "charset":Ljava/nio/charset/Charset;
    :sswitch_2
    const-string v3, "windows-949"

    invoke-static {v3}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v2

    .line 34
    .restart local v2    # "charset":Ljava/nio/charset/Charset;
    goto :goto_0

    .line 23
    nop

    :sswitch_data_0
    .sparse-switch
        0x3a8 -> :sswitch_1
        0x3b5 -> :sswitch_2
        0xfde9 -> :sswitch_0
    .end sparse-switch
.end method

.method EncodeToAscii(ILjava/lang/String;[B)V
    .locals 4
    .param p1, "nCodePage"    # I
    .param p2, "str"    # Ljava/lang/String;
    .param p3, "buffer"    # [B

    .prologue
    .line 49
    sparse-switch p1, :sswitch_data_0

    .line 59
    const-string v2, "windows-949"

    invoke-static {v2}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v1

    .line 62
    .local v1, "charset":Ljava/nio/charset/Charset;
    :goto_0
    invoke-virtual {v1, p2}, Ljava/nio/charset/Charset;->encode(Ljava/lang/String;)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 63
    .local v0, "byteBuffer":Ljava/nio/ByteBuffer;
    const/4 v2, 0x0

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->limit()I

    move-result v3

    invoke-virtual {v0, p3, v2, v3}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;

    .line 64
    return-void

    .line 52
    .end local v0    # "byteBuffer":Ljava/nio/ByteBuffer;
    .end local v1    # "charset":Ljava/nio/charset/Charset;
    :sswitch_0
    const-string v2, "windows-936"

    invoke-static {v2}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v1

    .line 53
    .restart local v1    # "charset":Ljava/nio/charset/Charset;
    goto :goto_0

    .line 55
    .end local v1    # "charset":Ljava/nio/charset/Charset;
    :sswitch_1
    const-string v2, "windows-949"

    invoke-static {v2}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v1

    .line 56
    .restart local v1    # "charset":Ljava/nio/charset/Charset;
    goto :goto_0

    .line 49
    :sswitch_data_0
    .sparse-switch
        0x3a8 -> :sswitch_0
        0x3b5 -> :sswitch_1
    .end sparse-switch
.end method
