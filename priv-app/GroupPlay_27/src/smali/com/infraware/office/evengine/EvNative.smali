.class Lcom/infraware/office/evengine/EvNative;
.super Ljava/lang/Object;
.source "EvNative.java"

# interfaces
.implements Lcom/infraware/office/evengine/E$EV_GUI_EVENT;


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private LOG_CAT:Ljava/lang/String;

.field private mDvListener:Lcom/infraware/office/evengine/EvListener$ViewerListener;

.field private mEvCodeConversion:Lcom/infraware/office/evengine/EvCodeConversion;

.field private mEvImageUtil:Lcom/infraware/office/evengine/EvImageUtil;

.field private mInterface:Lcom/infraware/office/evengine/EvInterface;

.field private mSystemFontFilePaths:[Ljava/lang/String;

.field private versionCode:Ljava/lang/String;

.field private versionTAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    const-class v0, Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/infraware/office/evengine/EvNative;->$assertionsDisabled:Z

    .line 512
    const-string v0, "EX_Engine5ex"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 513
    const-string v0, "polarisexternel"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 514
    const-string v0, "polarisexternelSDK"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 515
    return-void

    .line 14
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lcom/infraware/office/evengine/EvInterface;Ljava/lang/String;)V
    .locals 2
    .param p1, "a_interface"    # Lcom/infraware/office/evengine/EvInterface;
    .param p2, "appDir"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 99
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    const-string v0, "EvNative"

    iput-object v0, p0, Lcom/infraware/office/evengine/EvNative;->LOG_CAT:Ljava/lang/String;

    .line 18
    const-string v0, "OFFICESDK5VERSION_GroupPlay2.5"

    iput-object v0, p0, Lcom/infraware/office/evengine/EvNative;->versionTAG:Ljava/lang/String;

    .line 19
    const-string v0, "5.0.08.20.01R"

    iput-object v0, p0, Lcom/infraware/office/evengine/EvNative;->versionCode:Ljava/lang/String;

    .line 21
    iput-object v1, p0, Lcom/infraware/office/evengine/EvNative;->mInterface:Lcom/infraware/office/evengine/EvInterface;

    .line 22
    iput-object v1, p0, Lcom/infraware/office/evengine/EvNative;->mDvListener:Lcom/infraware/office/evengine/EvListener$ViewerListener;

    .line 28
    iput-object v1, p0, Lcom/infraware/office/evengine/EvNative;->mEvCodeConversion:Lcom/infraware/office/evengine/EvCodeConversion;

    .line 29
    iput-object v1, p0, Lcom/infraware/office/evengine/EvNative;->mEvImageUtil:Lcom/infraware/office/evengine/EvImageUtil;

    .line 30
    iput-object v1, p0, Lcom/infraware/office/evengine/EvNative;->mSystemFontFilePaths:[Ljava/lang/String;

    .line 101
    sget-boolean v0, Lcom/infraware/office/evengine/EvNative;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mInterface:Lcom/infraware/office/evengine/EvInterface;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 103
    :cond_0
    iput-object p1, p0, Lcom/infraware/office/evengine/EvNative;->mInterface:Lcom/infraware/office/evengine/EvInterface;

    .line 104
    invoke-static {}, Lcom/infraware/office/evengine/EvCodeConversion;->getCodeConversion()Lcom/infraware/office/evengine/EvCodeConversion;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvCodeConversion:Lcom/infraware/office/evengine/EvCodeConversion;

    .line 105
    invoke-static {}, Lcom/infraware/office/evengine/EvImageUtil;->getEvImageUtil()Lcom/infraware/office/evengine/EvImageUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvImageUtil:Lcom/infraware/office/evengine/EvImageUtil;

    .line 106
    invoke-direct {p0}, Lcom/infraware/office/evengine/EvNative;->MakeSystemFontFileNames()V

    .line 107
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mEvCodeConversion:Lcom/infraware/office/evengine/EvCodeConversion;

    iget-object v1, p0, Lcom/infraware/office/evengine/EvNative;->mEvImageUtil:Lcom/infraware/office/evengine/EvImageUtil;

    invoke-direct {p0, v0, v1, p2}, Lcom/infraware/office/evengine/EvNative;->IBeginNative(Lcom/infraware/office/evengine/EvCodeConversion;Lcom/infraware/office/evengine/EvImageUtil;Ljava/lang/String;)I

    .line 108
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->versionTAG:Ljava/lang/String;

    iget-object v1, p0, Lcom/infraware/office/evengine/EvNative;->versionCode:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 109
    return-void
.end method

.method private GetBitmap(IIZ)Landroid/graphics/Bitmap;
    .locals 1
    .param p1, "w"    # I
    .param p2, "h"    # I
    .param p3, "a_bTrue"    # Z

    .prologue
    .line 373
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mDvListener:Lcom/infraware/office/evengine/EvListener$ViewerListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mDvListener:Lcom/infraware/office/evengine/EvListener$ViewerListener;

    invoke-interface {v0, p1, p2}, Lcom/infraware/office/evengine/EvListener$ViewerListener;->GetBitmap(II)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 375
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private native IBeginNative(Lcom/infraware/office/evengine/EvCodeConversion;Lcom/infraware/office/evengine/EvImageUtil;Ljava/lang/String;)I
.end method

.method private MakeSystemFontFileNames()V
    .locals 21

    .prologue
    .line 34
    const/16 v19, 0x8

    move/from16 v0, v19

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    const-string v20, "nanum"

    aput-object v20, v18, v19

    const/16 v19, 0x1

    const-string v20, "droids"

    aput-object v20, v18, v19

    const/16 v19, 0x2

    const-string v20, "droidnask"

    aput-object v20, v18, v19

    const/16 v19, 0x3

    const-string v20, "samsung"

    aput-object v20, v18, v19

    const/16 v19, 0x4

    const-string v20, "lohit"

    aput-object v20, v18, v19

    const/16 v19, 0x5

    const-string v20, "thai"

    aput-object v20, v18, v19

    const/16 v19, 0x6

    const-string v20, "gp"

    aput-object v20, v18, v19

    const/16 v19, 0x7

    const-string v20, "arab"

    aput-object v20, v18, v19

    .line 35
    .local v18, "useString":[Ljava/lang/String;
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 36
    .local v10, "fontPaths":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v19, Ljava/io/File;

    const-string v20, "/system/fonts/"

    invoke-direct/range {v19 .. v20}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v19 .. v19}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v7

    .line 37
    .local v7, "fontFiles":[Ljava/io/File;
    array-length v6, v7

    .line 38
    .local v6, "fontCount":I
    const-wide/16 v16, 0x0

    .line 39
    .local v16, "mostBigFileSize":J
    const/4 v13, 0x0

    .local v13, "i":I
    :goto_0
    if-lt v13, v6, :cond_0

    .line 61
    const-string v15, "droidsansjapanese"

    .line 62
    .local v15, "japaneseString":Ljava/lang/String;
    const/4 v13, 0x0

    :goto_1
    if-lt v13, v6, :cond_5

    .line 72
    const-string v3, "droidsansthai"

    .line 73
    .local v3, "defaultThaiString":Ljava/lang/String;
    const-string v11, "gp_thai"

    .line 74
    .local v11, "gpThaiString":Ljava/lang/String;
    const-string v12, "gs_thai"

    .line 75
    .local v12, "gsThaiString":Ljava/lang/String;
    const/4 v13, 0x0

    :goto_2
    if-lt v13, v6, :cond_7

    .line 93
    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v9

    .line 94
    .local v9, "fontNameSize":I
    new-array v0, v9, [Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/infraware/office/evengine/EvNative;->mSystemFontFilePaths:[Ljava/lang/String;

    .line 95
    const/4 v13, 0x0

    :goto_3
    if-lt v13, v9, :cond_c

    .line 97
    return-void

    .line 40
    .end local v3    # "defaultThaiString":Ljava/lang/String;
    .end local v9    # "fontNameSize":I
    .end local v11    # "gpThaiString":Ljava/lang/String;
    .end local v12    # "gsThaiString":Ljava/lang/String;
    .end local v15    # "japaneseString":Ljava/lang/String;
    :cond_0
    aget-object v19, v7, v13

    invoke-virtual/range {v19 .. v19}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v8

    .line 43
    .local v8, "fontName":Ljava/lang/String;
    const-string v19, "bold"

    move-object/from16 v0, v19

    invoke-virtual {v8, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v19

    if-nez v19, :cond_1

    const-string v19, "lindseyforsamsung-regular"

    move-object/from16 v0, v19

    invoke-virtual {v8, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v19

    if-eqz v19, :cond_2

    .line 39
    :cond_1
    add-int/lit8 v13, v13, 0x1

    goto :goto_0

    .line 46
    :cond_2
    aget-object v19, v7, v13

    invoke-virtual/range {v19 .. v19}, Ljava/io/File;->length()J

    move-result-wide v4

    .line 48
    .local v4, "fileSize":J
    const/4 v14, 0x0

    .local v14, "j":I
    :goto_4
    move-object/from16 v0, v18

    array-length v0, v0

    move/from16 v19, v0

    move/from16 v0, v19

    if-ge v14, v0, :cond_1

    .line 49
    new-instance v19, Ljava/lang/StringBuilder;

    const-string v20, "(?i).*"

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-object v20, v18, v14

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, ".*"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v8, v0}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v19

    if-eqz v19, :cond_3

    .line 50
    aget-object v19, v7, v13

    invoke-virtual/range {v19 .. v19}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v19

    if-eqz v19, :cond_3

    .line 51
    cmp-long v19, v16, v4

    if-gez v19, :cond_4

    .line 52
    move-wide/from16 v16, v4

    .line 53
    const/16 v19, 0x0

    aget-object v20, v7, v13

    invoke-virtual/range {v20 .. v20}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v20

    move/from16 v0, v19

    move-object/from16 v1, v20

    invoke-interface {v10, v0, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 48
    :cond_3
    :goto_5
    add-int/lit8 v14, v14, 0x1

    goto :goto_4

    .line 56
    :cond_4
    aget-object v19, v7, v13

    invoke-virtual/range {v19 .. v19}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-interface {v10, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 63
    .end local v4    # "fileSize":J
    .end local v8    # "fontName":Ljava/lang/String;
    .end local v14    # "j":I
    .restart local v15    # "japaneseString":Ljava/lang/String;
    :cond_5
    aget-object v19, v7, v13

    invoke-virtual/range {v19 .. v19}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v8

    .line 65
    .restart local v8    # "fontName":Ljava/lang/String;
    new-instance v19, Ljava/lang/StringBuilder;

    const-string v20, "(?i).*"

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v19

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, ".*"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v8, v0}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v19

    if-eqz v19, :cond_6

    .line 66
    aget-object v19, v7, v13

    invoke-virtual/range {v19 .. v19}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v19

    if-eqz v19, :cond_6

    .line 67
    const/16 v19, 0x0

    aget-object v20, v7, v13

    invoke-virtual/range {v20 .. v20}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v20

    move/from16 v0, v19

    move-object/from16 v1, v20

    invoke-interface {v10, v0, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 62
    :cond_6
    add-int/lit8 v13, v13, 0x1

    goto/16 :goto_1

    .line 76
    .end local v8    # "fontName":Ljava/lang/String;
    .restart local v3    # "defaultThaiString":Ljava/lang/String;
    .restart local v11    # "gpThaiString":Ljava/lang/String;
    .restart local v12    # "gsThaiString":Ljava/lang/String;
    :cond_7
    aget-object v19, v7, v13

    invoke-virtual/range {v19 .. v19}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v8

    .line 78
    .restart local v8    # "fontName":Ljava/lang/String;
    new-instance v19, Ljava/lang/StringBuilder;

    const-string v20, "(?i).*"

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v19

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, ".*"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v8, v0}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v19

    if-nez v19, :cond_8

    new-instance v19, Ljava/lang/StringBuilder;

    const-string v20, "(?i).*"

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v19

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, ".*"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v8, v0}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v19

    if-eqz v19, :cond_9

    .line 79
    :cond_8
    aget-object v19, v7, v13

    invoke-virtual/range {v19 .. v19}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v19

    if-eqz v19, :cond_9

    .line 80
    const/4 v14, 0x0

    .restart local v14    # "j":I
    :goto_6
    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v19

    move/from16 v0, v19

    if-lt v14, v0, :cond_a

    .line 75
    .end local v14    # "j":I
    :cond_9
    :goto_7
    add-int/lit8 v13, v13, 0x1

    goto/16 :goto_2

    .line 81
    .restart local v14    # "j":I
    :cond_a
    invoke-interface {v10, v14}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/String;

    invoke-virtual/range {v19 .. v19}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    .line 83
    .local v2, "defaultFontName":Ljava/lang/String;
    new-instance v19, Ljava/lang/StringBuilder;

    const-string v20, "(?i).*"

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, ".*"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v2, v0}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v19

    if-eqz v19, :cond_b

    .line 84
    aget-object v19, v7, v13

    invoke-virtual/range {v19 .. v19}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-interface {v10, v14, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    goto :goto_7

    .line 80
    :cond_b
    add-int/lit8 v14, v14, 0x1

    goto :goto_6

    .line 96
    .end local v2    # "defaultFontName":Ljava/lang/String;
    .end local v8    # "fontName":Ljava/lang/String;
    .end local v14    # "j":I
    .restart local v9    # "fontNameSize":I
    :cond_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/infraware/office/evengine/EvNative;->mSystemFontFilePaths:[Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-interface {v10, v13}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/String;

    aput-object v19, v20, v13

    .line 95
    add-int/lit8 v13, v13, 0x1

    goto/16 :goto_3
.end method

.method private OnCloseDoc()V
    .locals 1

    .prologue
    .line 354
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mDvListener:Lcom/infraware/office/evengine/EvListener$ViewerListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mDvListener:Lcom/infraware/office/evengine/EvListener$ViewerListener;

    invoke-interface {v0}, Lcom/infraware/office/evengine/EvListener$ViewerListener;->OnCloseDoc()V

    :cond_0
    return-void
.end method

.method private OnDrawBitmap(III)V
    .locals 3
    .param p1, "nCallId"    # I
    .param p2, "bShowAutomap"    # I
    .param p3, "nCurrentMode"    # I

    .prologue
    const v2, 0xffff

    .line 379
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->EV()Lcom/infraware/office/evengine/EV;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV;->getConfigInfo()Lcom/infraware/office/evengine/EV$CONFIG_INFO;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/infraware/office/evengine/EvNative;->IGetConfig(Lcom/infraware/office/evengine/EV$CONFIG_INFO;)V

    .line 388
    if-ne p1, v2, :cond_0

    .line 389
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->LOG_CAT:Ljava/lang/String;

    const-string v1, "CallId == eEV_GUI_MAX_EVENT"

    invoke-static {v0, v1}, Lcom/infraware/common/util/CMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 391
    :cond_0
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mDvListener:Lcom/infraware/office/evengine/EvListener$ViewerListener;

    if-eqz v0, :cond_1

    if-eq p1, v2, :cond_1

    .line 392
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mDvListener:Lcom/infraware/office/evengine/EvListener$ViewerListener;

    invoke-interface {v0, p1, p2}, Lcom/infraware/office/evengine/EvListener$ViewerListener;->OnDrawBitmap(II)V

    .line 393
    :cond_1
    return-void
.end method

.method private OnFinalizeComplete()V
    .locals 1

    .prologue
    .line 350
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->OnFinalizeComplete()V

    return-void
.end method

.method private OnGetDeviceInfo()Ljava/lang/String;
    .locals 1

    .prologue
    .line 410
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    return-object v0
.end method

.method private OnGetResStringID(I)Ljava/lang/String;
    .locals 1
    .param p1, "nStrID"    # I

    .prologue
    .line 399
    const-string v0, ""

    return-object v0
.end method

.method private OnInitComplete(I)V
    .locals 1
    .param p1, "EEV_ERROR_CODE"    # I

    .prologue
    .line 349
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0, p1}, Lcom/infraware/office/evengine/EvInterface;->OnInitComplete(I)V

    return-void
.end method

.method private OnLoadComplete(I)V
    .locals 1
    .param p1, "bBookmarkExsit"    # I

    .prologue
    .line 351
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mDvListener:Lcom/infraware/office/evengine/EvListener$ViewerListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mDvListener:Lcom/infraware/office/evengine/EvListener$ViewerListener;

    invoke-interface {v0}, Lcom/infraware/office/evengine/EvListener$ViewerListener;->OnLoadComplete()V

    :cond_0
    return-void
.end method

.method private OnLoadFail(I)V
    .locals 1
    .param p1, "EEV_ERROR_CODE"    # I

    .prologue
    .line 353
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mDvListener:Lcom/infraware/office/evengine/EvListener$ViewerListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mDvListener:Lcom/infraware/office/evengine/EvListener$ViewerListener;

    invoke-interface {v0, p1}, Lcom/infraware/office/evengine/EvListener$ViewerListener;->OnLoadFail(I)V

    :cond_0
    return-void
.end method

.method private OnPageMove(III)V
    .locals 1
    .param p1, "nCurrentPage"    # I
    .param p2, "nTotalPage"    # I
    .param p3, "nErrorCode"    # I

    .prologue
    .line 356
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->EV()Lcom/infraware/office/evengine/EV;

    move-result-object v0

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV;->getConfigInfo()Lcom/infraware/office/evengine/EV$CONFIG_INFO;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/infraware/office/evengine/EvNative;->IGetConfig(Lcom/infraware/office/evengine/EV$CONFIG_INFO;)V

    .line 357
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mDvListener:Lcom/infraware/office/evengine/EvListener$ViewerListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mDvListener:Lcom/infraware/office/evengine/EvListener$ViewerListener;

    invoke-interface {v0, p1, p2, p3}, Lcom/infraware/office/evengine/EvListener$ViewerListener;->OnPageMove(III)V

    .line 358
    :cond_0
    return-void
.end method

.method private OnPrintMode(Ljava/lang/String;)V
    .locals 1
    .param p1, "strPrintFile"    # Ljava/lang/String;

    .prologue
    .line 366
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mDvListener:Lcom/infraware/office/evengine/EvListener$ViewerListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mDvListener:Lcom/infraware/office/evengine/EvListener$ViewerListener;

    invoke-interface {v0, p1}, Lcom/infraware/office/evengine/EvListener$ViewerListener;->OnPrintMode(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private OnPrintedCount(I)V
    .locals 1
    .param p1, "nTotalCount"    # I

    .prologue
    .line 367
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mDvListener:Lcom/infraware/office/evengine/EvListener$ViewerListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mDvListener:Lcom/infraware/office/evengine/EvListener$ViewerListener;

    invoke-interface {v0, p1}, Lcom/infraware/office/evengine/EvListener$ViewerListener;->OnPrintedCount(I)V

    :cond_0
    return-void
.end method

.method private OnProgress(II)V
    .locals 1
    .param p1, "EV_PROGRESS_TYPE"    # I
    .param p2, "nProgressValue"    # I

    .prologue
    .line 360
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mDvListener:Lcom/infraware/office/evengine/EvListener$ViewerListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mDvListener:Lcom/infraware/office/evengine/EvListener$ViewerListener;

    invoke-interface {v0, p1, p2}, Lcom/infraware/office/evengine/EvListener$ViewerListener;->OnProgress(II)V

    :cond_0
    return-void
.end method

.method private OnProgressStart(I)V
    .locals 1
    .param p1, "EV_PROGRESS_TYPE"    # I

    .prologue
    .line 359
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mDvListener:Lcom/infraware/office/evengine/EvListener$ViewerListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mDvListener:Lcom/infraware/office/evengine/EvListener$ViewerListener;

    invoke-interface {v0, p1}, Lcom/infraware/office/evengine/EvListener$ViewerListener;->OnProgressStart(I)V

    :cond_0
    return-void
.end method

.method private OnTerminate()V
    .locals 1

    .prologue
    .line 361
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mDvListener:Lcom/infraware/office/evengine/EvListener$ViewerListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mDvListener:Lcom/infraware/office/evengine/EvListener$ViewerListener;

    invoke-interface {v0}, Lcom/infraware/office/evengine/EvListener$ViewerListener;->OnTerminate()V

    :cond_0
    return-void
.end method

.method private OnTimerStart()V
    .locals 1

    .prologue
    .line 414
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mInterface:Lcom/infraware/office/evengine/EvInterface;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->OnTimerStart()V

    :cond_0
    return-void
.end method

.method private OnTimerStop()V
    .locals 1

    .prologue
    .line 415
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mInterface:Lcom/infraware/office/evengine/EvInterface;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mInterface:Lcom/infraware/office/evengine/EvInterface;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvInterface;->OnTimerStop()V

    :cond_0
    return-void
.end method

.method private OnTotalLoadComplete()V
    .locals 1

    .prologue
    .line 352
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mDvListener:Lcom/infraware/office/evengine/EvListener$ViewerListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mDvListener:Lcom/infraware/office/evengine/EvListener$ViewerListener;

    invoke-interface {v0}, Lcom/infraware/office/evengine/EvListener$ViewerListener;->OnTotalLoadComplete()V

    :cond_0
    return-void
.end method


# virtual methods
.method GetFontFileList()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 348
    iget-object v0, p0, Lcom/infraware/office/evengine/EvNative;->mSystemFontFilePaths:[Ljava/lang/String;

    return-object v0
.end method

.method native ICancel()V
.end method

.method native IClose()V
.end method

.method native IFinalize()V
.end method

.method native IGetConfig(Lcom/infraware/office/evengine/EV$CONFIG_INFO;)V
.end method

.method native IInitialize(IIIIIIII)V
.end method

.method native IOpen(Ljava/lang/String;IIIIIIIIIIIILjava/lang/String;Ljava/lang/String;)V
.end method

.method native IOpenEx(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method native ISetHeapSize(I)V
.end method

.method native ISetLocale(I)V
.end method

.method native ISetPrint(IIILjava/lang/String;I)V
.end method

.method native ISetPrintEx(IIILjava/lang/String;ILjava/lang/String;Ljava/lang/String;IIII)V
.end method

.method native ISetThumbnailByPrint(IILjava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V
.end method

.method native ITimer()V
.end method

.method SetListener(Lcom/infraware/office/evengine/EvListener$ViewerListener;)V
    .locals 0
    .param p1, "DvL"    # Lcom/infraware/office/evengine/EvListener$ViewerListener;

    .prologue
    .line 113
    iput-object p1, p0, Lcom/infraware/office/evengine/EvNative;->mDvListener:Lcom/infraware/office/evengine/EvListener$ViewerListener;

    .line 119
    return-void
.end method
