.class Lcom/infraware/office/evengine/EvCompInterfaceMsg;
.super Lcom/infraware/office/evengine/EvInterface;
.source "EvCompInterfaceMsg.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/infraware/office/evengine/EvCompInterfaceMsg$HandlerTask;
    }
.end annotation


# instance fields
.field protected final mHandler:Lcom/infraware/office/evengine/EvCompInterfaceMsg$HandlerTask;

.field private versionCode:Ljava/lang/String;

.field private versionTAG:Ljava/lang/String;


# direct methods
.method protected constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "AppDir"    # Ljava/lang/String;

    .prologue
    .line 93
    invoke-direct {p0, p1}, Lcom/infraware/office/evengine/EvInterface;-><init>(Ljava/lang/String;)V

    .line 12
    const-string v0, "OFFICESDK5VERSION_GroupPlay2.5"

    iput-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->versionTAG:Ljava/lang/String;

    .line 13
    const-string v0, "5.0.08.20.01R"

    iput-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->versionCode:Ljava/lang/String;

    .line 74
    new-instance v0, Lcom/infraware/office/evengine/EvCompInterfaceMsg$HandlerTask;

    invoke-direct {v0, p0}, Lcom/infraware/office/evengine/EvCompInterfaceMsg$HandlerTask;-><init>(Lcom/infraware/office/evengine/EvCompInterfaceMsg;)V

    iput-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->mHandler:Lcom/infraware/office/evengine/EvCompInterfaceMsg$HandlerTask;

    .line 94
    return-void
.end method


# virtual methods
.method public ICancel()V
    .locals 1

    .prologue
    .line 412
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvNative;->ICancel()V

    .line 413
    return-void
.end method

.method public IClose()V
    .locals 2

    .prologue
    .line 166
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvNative;->IClose()V

    .line 168
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->mHandler:Lcom/infraware/office/evengine/EvCompInterfaceMsg$HandlerTask;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/infraware/office/evengine/EvCompInterfaceMsg$HandlerTask;->setOperationTimer(Z)V

    .line 170
    invoke-super {p0}, Lcom/infraware/office/evengine/EvInterface;->IClose()V

    .line 171
    return-void
.end method

.method public IFinalize()V
    .locals 1

    .prologue
    .line 135
    invoke-virtual {p0}, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->isInit()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 136
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EvNative;->IFinalize()V

    .line 138
    :cond_0
    return-void
.end method

.method public IGetConfig()Lcom/infraware/office/evengine/EV$CONFIG_INFO;
    .locals 1

    .prologue
    .line 419
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Ev:Lcom/infraware/office/evengine/EV;

    invoke-virtual {v0}, Lcom/infraware/office/evengine/EV;->getConfigInfo()Lcom/infraware/office/evengine/EV$CONFIG_INFO;

    move-result-object v0

    return-object v0
.end method

.method public IInitialize(III)V
    .locals 9
    .param p1, "a_nWidth"    # I
    .param p2, "a_nHeight"    # I
    .param p3, "a_nDpi"    # I

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 118
    invoke-super {p0, p1, p2, p3}, Lcom/infraware/office/evengine/EvInterface;->IInitialize(III)V

    .line 120
    invoke-virtual {p0}, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->isInit()Z

    move-result v0

    if-nez v0, :cond_0

    .line 122
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    .line 127
    const/16 v6, 0x10

    move v1, p1

    move v2, p2

    move v5, v4

    move v7, v3

    move v8, p3

    .line 122
    invoke-virtual/range {v0 .. v8}, Lcom/infraware/office/evengine/EvNative;->IInitialize(IIIIIIII)V

    .line 131
    :cond_0
    return-void
.end method

.method public IOpen(Ljava/lang/String;IIIIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 17
    .param p1, "a_sFilePath"    # Ljava/lang/String;
    .param p2, "a_nWidth"    # I
    .param p3, "a_nHeight"    # I
    .param p4, "EEV_FILE_LOAD_TYPE"    # I
    .param p5, "a_nLocale"    # I
    .param p6, "bLandScape"    # I
    .param p7, "a_sTempPath"    # Ljava/lang/String;
    .param p8, "a_sBookMarkPath"    # Ljava/lang/String;
    .param p9, "a_sBookClipPath"    # Ljava/lang/String;

    .prologue
    .line 152
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->versionTAG:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->versionCode:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 153
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    .line 157
    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    move-object/from16 v2, p1

    move/from16 v3, p2

    move/from16 v4, p3

    move/from16 v5, p4

    move/from16 v13, p5

    move/from16 v14, p6

    move-object/from16 v15, p7

    move-object/from16 v16, p8

    .line 153
    invoke-virtual/range {v1 .. v16}, Lcom/infraware/office/evengine/EvNative;->IOpen(Ljava/lang/String;IIIIIIIIIIIILjava/lang/String;Ljava/lang/String;)V

    .line 158
    return-void
.end method

.method public IOpenEx(Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "a_sFilePath"    # Ljava/lang/String;
    .param p2, "a_sPassword"    # Ljava/lang/String;
    .param p3, "a_nLocale"    # I
    .param p4, "a_bLandScape"    # I
    .param p5, "a_sTempPath"    # Ljava/lang/String;
    .param p6, "a_sBookMarkPath"    # Ljava/lang/String;
    .param p7, "a_sBookClipPath"    # Ljava/lang/String;

    .prologue
    .line 161
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1, p2}, Lcom/infraware/office/evengine/EvNative;->IOpenEx(Ljava/lang/String;Ljava/lang/String;)V

    .line 162
    return-void
.end method

.method public ISetHeapSize(I)V
    .locals 1
    .param p1, "a_nHeapSize"    # I

    .prologue
    .line 98
    invoke-super {p0, p1}, Lcom/infraware/office/evengine/EvInterface;->ISetHeapSize(I)V

    .line 99
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1}, Lcom/infraware/office/evengine/EvNative;->ISetHeapSize(I)V

    .line 100
    return-void
.end method

.method public ISetListener(Lcom/infraware/office/evengine/EvListener$ViewerListener;)V
    .locals 1
    .param p1, "DvL"    # Lcom/infraware/office/evengine/EvListener$ViewerListener;

    .prologue
    .line 109
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    invoke-virtual {v0, p1}, Lcom/infraware/office/evengine/EvNative;->SetListener(Lcom/infraware/office/evengine/EvListener$ViewerListener;)V

    .line 110
    return-void
.end method

.method public ISetPrint(IIILjava/lang/String;I)V
    .locals 6
    .param p1, "a_PaperSize"    # I
    .param p2, "a_StartPage"    # I
    .param p3, "a_EndPage"    # I
    .param p4, "a_szFilePath"    # Ljava/lang/String;
    .param p5, "a_ReturnType"    # I

    .prologue
    .line 1031
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    move v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/infraware/office/evengine/EvNative;->ISetPrint(IIILjava/lang/String;I)V

    .line 1032
    return-void
.end method

.method public ISetPrintEx(IIILjava/lang/String;IILjava/lang/String;Ljava/lang/String;)V
    .locals 12
    .param p1, "a_PaperSize"    # I
    .param p2, "a_StartPage"    # I
    .param p3, "a_EndPage"    # I
    .param p4, "a_szFilePath"    # Ljava/lang/String;
    .param p5, "a_ReturnType"    # I
    .param p6, "a_margin"    # I
    .param p7, "a_szPageBoundary"    # Ljava/lang/String;
    .param p8, "a_szOutputPath"    # Ljava/lang/String;

    .prologue
    .line 1035
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    move v1, p1

    move v2, p2

    move v3, p3

    move-object/from16 v4, p4

    move/from16 v5, p5

    move-object/from16 v6, p7

    move-object/from16 v7, p8

    move/from16 v8, p6

    move/from16 v9, p6

    move/from16 v10, p6

    move/from16 v11, p6

    invoke-virtual/range {v0 .. v11}, Lcom/infraware/office/evengine/EvNative;->ISetPrintEx(IIILjava/lang/String;ILjava/lang/String;Ljava/lang/String;IIII)V

    .line 1036
    return-void
.end method

.method public ISetWHByPrint(IILjava/lang/String;ILjava/lang/String;II)V
    .locals 9
    .param p1, "a_StartPage"    # I
    .param p2, "a_EndPage"    # I
    .param p3, "a_szFilePath"    # Ljava/lang/String;
    .param p4, "a_RetrunType"    # I
    .param p5, "a_szOutputPath"    # Ljava/lang/String;
    .param p6, "a_nWidth"    # I
    .param p7, "a_nHeight"    # I

    .prologue
    .line 1039
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->Native:Lcom/infraware/office/evengine/EvNative;

    const/4 v5, 0x0

    move v1, p1

    move v2, p2

    move-object v3, p3

    move v4, p4

    move-object v6, p5

    move v7, p6

    move/from16 v8, p7

    invoke-virtual/range {v0 .. v8}, Lcom/infraware/office/evengine/EvNative;->ISetThumbnailByPrint(IILjava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 1040
    return-void
.end method

.method protected OnInitComplete(I)V
    .locals 0
    .param p1, "EEV_ERROR_CODE"    # I

    .prologue
    .line 113
    invoke-super {p0, p1}, Lcom/infraware/office/evengine/EvInterface;->OnInitComplete(I)V

    .line 114
    return-void
.end method

.method OnTimerStart()V
    .locals 2

    .prologue
    .line 77
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->mHandler:Lcom/infraware/office/evengine/EvCompInterfaceMsg$HandlerTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/infraware/office/evengine/EvCompInterfaceMsg$HandlerTask;->setOperationTimer(Z)V

    .line 78
    return-void
.end method

.method OnTimerStop()V
    .locals 2

    .prologue
    .line 81
    iget-object v0, p0, Lcom/infraware/office/evengine/EvCompInterfaceMsg;->mHandler:Lcom/infraware/office/evengine/EvCompInterfaceMsg$HandlerTask;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/infraware/office/evengine/EvCompInterfaceMsg$HandlerTask;->setOperationTimer(Z)V

    .line 82
    return-void
.end method
