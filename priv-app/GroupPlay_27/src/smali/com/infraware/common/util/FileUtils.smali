.class public abstract Lcom/infraware/common/util/FileUtils;
.super Ljava/lang/Object;
.source "FileUtils.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static makeDirectory(Ljava/lang/String;ZLandroid/content/Context;)Ljava/lang/String;
    .locals 4
    .param p0, "path"    # Ljava/lang/String;
    .param p1, "isAbsolutePath"    # Z
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 41
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 42
    .local v1, "f":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    .line 44
    .local v0, "dirName":Ljava/lang/String;
    const/4 v2, 0x0

    .line 45
    .local v2, "new_dir":Ljava/io/File;
    if-nez p1, :cond_1

    if-eqz p2, :cond_1

    .line 46
    const/4 v3, 0x3

    invoke-virtual {p2, v0, v3}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v2

    .line 52
    :cond_0
    :goto_0
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 53
    invoke-virtual {v2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v3

    .line 55
    :goto_1
    return-object v3

    .line 48
    :cond_1
    new-instance v2, Ljava/io/File;

    .end local v2    # "new_dir":Ljava/io/File;
    invoke-direct {v2, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 49
    .restart local v2    # "new_dir":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_0

    .line 50
    invoke-virtual {v2}, Ljava/io/File;->mkdir()Z

    goto :goto_0

    .line 55
    :cond_2
    const/4 v3, 0x0

    goto :goto_1
.end method
