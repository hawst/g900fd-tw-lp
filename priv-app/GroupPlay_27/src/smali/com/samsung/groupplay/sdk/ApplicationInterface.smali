.class public Lcom/samsung/groupplay/sdk/ApplicationInterface;
.super Ljava/lang/Object;
.source "ApplicationInterface.java"

# interfaces
.implements Lcom/samsung/groupplay/sdk/SDKCommon;


# static fields
.field private static final GP_PACKAGE_NAME:Ljava/lang/String; = "com.samsung.groupcast"

.field private static final GP_STATE_SERVICE:Ljava/lang/String; = "com.samsung.groupcast.service.GroupPlayState"

.field private static final INTENT_GP_FOR_EXTERNAL:Ljava/lang/String; = "com.samsung.groupcast.action.SEND_GPSDK"

.field public static final KEY_BACKGROUND_BITMAP:Ljava/lang/String; = "APPLICATION_INTERFACE_BACKGROUND_BITMAP"

.field public static final KEY_IS_START_FROM_GPSDK:Ljava/lang/String; = "APPLICATION_INTERFACE_IS_START_FROM_GPSDK"

.field public static final KEY_NEXT_ACTION:Ljava/lang/String; = "APPLICATION_INTERFACE_NEXT_ACTION"

.field public static final KEY_TYPE:Ljava/lang/String; = "APPLICATION_INTERFACE_TYPE"

.field private static final TAG:Ljava/lang/String;

.field public static final TYPE_CREATE:I = 0x1

.field public static final TYPE_DEFAULT:I = 0x0

.field public static final TYPE_JOIN:I = 0x2

.field public static final WIFI_AP_HOST_IP:Ljava/lang/String; = "192.168.43.1"


# instance fields
.field private bInit:Z

.field private final mContext:Landroid/content/Context;

.field private mGroupPlayTask:Landroid/app/ActivityManager$RunningTaskInfo;

.field private mState:Lcom/samsung/groupcast/service/IGroupPlayState;

.field private final servConn:Landroid/content/ServiceConnection;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const-class v0, Lcom/samsung/groupplay/sdk/ApplicationInterface;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/groupplay/sdk/ApplicationInterface;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 89
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/groupplay/sdk/ApplicationInterface;->bInit:Z

    .line 70
    new-instance v0, Lcom/samsung/groupplay/sdk/ApplicationInterface$1;

    invoke-direct {v0, p0}, Lcom/samsung/groupplay/sdk/ApplicationInterface$1;-><init>(Lcom/samsung/groupplay/sdk/ApplicationInterface;)V

    iput-object v0, p0, Lcom/samsung/groupplay/sdk/ApplicationInterface;->servConn:Landroid/content/ServiceConnection;

    .line 90
    sget-object v0, Lcom/samsung/groupplay/sdk/ApplicationInterface;->TAG:Ljava/lang/String;

    const-string v1, "[ApplicationInterface]"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 91
    iput-object p1, p0, Lcom/samsung/groupplay/sdk/ApplicationInterface;->mContext:Landroid/content/Context;

    .line 92
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 27
    sget-object v0, Lcom/samsung/groupplay/sdk/ApplicationInterface;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$102(Lcom/samsung/groupplay/sdk/ApplicationInterface;Lcom/samsung/groupcast/service/IGroupPlayState;)Lcom/samsung/groupcast/service/IGroupPlayState;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/groupplay/sdk/ApplicationInterface;
    .param p1, "x1"    # Lcom/samsung/groupcast/service/IGroupPlayState;

    .prologue
    .line 27
    iput-object p1, p0, Lcom/samsung/groupplay/sdk/ApplicationInterface;->mState:Lcom/samsung/groupcast/service/IGroupPlayState;

    return-object p1
.end method

.method private addExtraIntent(Landroid/content/Intent;ILjava/lang/String;)V
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "type"    # I
    .param p3, "action"    # Ljava/lang/String;

    .prologue
    .line 297
    sget-object v0, Lcom/samsung/groupplay/sdk/ApplicationInterface;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[addExtraIntent] - Type : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", NextAction : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 299
    const-string v0, "APPLICATION_INTERFACE_IS_START_FROM_GPSDK"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 300
    const-string v0, "APPLICATION_INTERFACE_TYPE"

    invoke-virtual {p1, v0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 301
    const-string v0, "APPLICATION_INTERFACE_NEXT_ACTION"

    invoke-virtual {p1, v0, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 302
    const/high16 v0, 0x10000000

    invoke-virtual {p1, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 303
    return-void
.end method

.method private bind()V
    .locals 4

    .prologue
    .line 155
    sget-object v1, Lcom/samsung/groupplay/sdk/ApplicationInterface;->TAG:Ljava/lang/String;

    const-string v2, "[bind]"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 157
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.samsung.groupcast.service.GroupPlayState"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 158
    .local v0, "intent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/samsung/groupplay/sdk/ApplicationInterface;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/groupplay/sdk/ApplicationInterface;->servConn:Landroid/content/ServiceConnection;

    const/4 v3, 0x1

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 159
    return-void
.end method

.method private getWifiIP()Ljava/lang/String;
    .locals 4

    .prologue
    .line 254
    sget-object v2, Lcom/samsung/groupplay/sdk/ApplicationInterface;->TAG:Ljava/lang/String;

    const-string v3, "[getWifiIP]"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 256
    invoke-virtual {p0}, Lcom/samsung/groupplay/sdk/ApplicationInterface;->hasSession()Z

    move-result v2

    if-nez v2, :cond_0

    .line 257
    const/4 v2, 0x0

    .line 267
    :goto_0
    return-object v2

    .line 260
    :cond_0
    iget-object v2, p0, Lcom/samsung/groupplay/sdk/ApplicationInterface;->mContext:Landroid/content/Context;

    const-string v3, "wifi"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/wifi/WifiManager;

    .line 261
    .local v1, "wifimanager":Landroid/net/wifi/WifiManager;
    invoke-virtual {v1}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/wifi/WifiInfo;->getIpAddress()I

    move-result v0

    .line 262
    .local v0, "myIPint":I
    if-nez v0, :cond_1

    .line 263
    const-string v2, "192.168.43.1"

    goto :goto_0

    .line 267
    :cond_1
    invoke-direct {p0, v0}, Lcom/samsung/groupplay/sdk/ApplicationInterface;->intToIp(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method private intToIp(I)Ljava/lang/String;
    .locals 2
    .param p1, "i"    # I

    .prologue
    .line 272
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    and-int/lit16 v1, p1, 0xff

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    shr-int/lit8 v1, p1, 0x8

    and-int/lit16 v1, v1, 0xff

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    shr-int/lit8 v1, p1, 0x10

    and-int/lit16 v1, v1, 0xff

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    shr-int/lit8 v1, p1, 0x18

    and-int/lit16 v1, v1, 0xff

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private unbind()V
    .locals 2

    .prologue
    .line 168
    sget-object v0, Lcom/samsung/groupplay/sdk/ApplicationInterface;->TAG:Ljava/lang/String;

    const-string v1, "[unbind]"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 170
    iget-object v0, p0, Lcom/samsung/groupplay/sdk/ApplicationInterface;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/groupplay/sdk/ApplicationInterface;->servConn:Landroid/content/ServiceConnection;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 171
    return-void
.end method


# virtual methods
.method public getContactPoint()[Ljava/lang/String;
    .locals 3

    .prologue
    .line 325
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "ssin.kwak@samsung.com"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "histroy.kim@samsung.com"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "js271.kim@samsung.com"

    aput-object v2, v0, v1

    return-object v0
.end method

.method public getGroupPlayTask()Landroid/app/ActivityManager$RunningTaskInfo;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NullPointerException;
        }
    .end annotation

    .prologue
    .line 311
    iget-object v0, p0, Lcom/samsung/groupplay/sdk/ApplicationInterface;->mGroupPlayTask:Landroid/app/ActivityManager$RunningTaskInfo;

    if-nez v0, :cond_0

    .line 312
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 314
    :cond_0
    iget-object v0, p0, Lcom/samsung/groupplay/sdk/ApplicationInterface;->mGroupPlayTask:Landroid/app/ActivityManager$RunningTaskInfo;

    return-object v0
.end method

.method public getVersion()I
    .locals 1

    .prologue
    .line 320
    const/16 v0, 0x4ee8

    return v0
.end method

.method public hasSession()Z
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 179
    sget-object v3, Lcom/samsung/groupplay/sdk/ApplicationInterface;->TAG:Ljava/lang/String;

    const-string v4, "[hasSession]"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 181
    invoke-virtual {p0}, Lcom/samsung/groupplay/sdk/ApplicationInterface;->isRunning()Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v2

    .line 203
    :goto_0
    return v0

    .line 187
    :cond_0
    :try_start_0
    iget-boolean v3, p0, Lcom/samsung/groupplay/sdk/ApplicationInterface;->bInit:Z

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/samsung/groupplay/sdk/ApplicationInterface;->mState:Lcom/samsung/groupcast/service/IGroupPlayState;

    if-nez v3, :cond_2

    .line 188
    :cond_1
    sget-object v3, Lcom/samsung/groupplay/sdk/ApplicationInterface;->TAG:Ljava/lang/String;

    const-string v4, "[hasSession] initialization is not compledted"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v2

    .line 191
    goto :goto_0

    .line 194
    :cond_2
    iget-object v3, p0, Lcom/samsung/groupplay/sdk/ApplicationInterface;->mState:Lcom/samsung/groupcast/service/IGroupPlayState;

    invoke-interface {v3}, Lcom/samsung/groupcast/service/IGroupPlayState;->hasSession()Z

    move-result v0

    .line 195
    .local v0, "bHasSession":Z
    if-eqz v0, :cond_3

    .line 196
    sget-object v3, Lcom/samsung/groupplay/sdk/ApplicationInterface;->TAG:Ljava/lang/String;

    const-string v4, "[hasSession] has session"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 201
    .end local v0    # "bHasSession":Z
    :catch_0
    move-exception v1

    .line 202
    .local v1, "e":Landroid/os/RemoteException;
    sget-object v3, Lcom/samsung/groupplay/sdk/ApplicationInterface;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[hasSession] error : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v2

    .line 203
    goto :goto_0

    .line 198
    .end local v1    # "e":Landroid/os/RemoteException;
    .restart local v0    # "bHasSession":Z
    :cond_3
    :try_start_1
    sget-object v3, Lcom/samsung/groupplay/sdk/ApplicationInterface;->TAG:Ljava/lang/String;

    const-string v4, "[hasSession] not has session"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method public initialize()V
    .locals 2

    .prologue
    .line 128
    sget-object v0, Lcom/samsung/groupplay/sdk/ApplicationInterface;->TAG:Ljava/lang/String;

    const-string v1, "[initialize]"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 130
    iget-boolean v0, p0, Lcom/samsung/groupplay/sdk/ApplicationInterface;->bInit:Z

    if-nez v0, :cond_0

    .line 131
    invoke-direct {p0}, Lcom/samsung/groupplay/sdk/ApplicationInterface;->bind()V

    .line 132
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/groupplay/sdk/ApplicationInterface;->bInit:Z

    .line 134
    :cond_0
    return-void
.end method

.method public isClient()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 236
    sget-object v2, Lcom/samsung/groupplay/sdk/ApplicationInterface;->TAG:Ljava/lang/String;

    const-string v3, "[isClient]"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 238
    invoke-direct {p0}, Lcom/samsung/groupplay/sdk/ApplicationInterface;->getWifiIP()Ljava/lang/String;

    move-result-object v0

    .line 239
    .local v0, "myIP":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 240
    sget-object v2, Lcom/samsung/groupplay/sdk/ApplicationInterface;->TAG:Ljava/lang/String;

    const-string v3, "[isClient] not client"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 249
    :goto_0
    return v1

    .line 244
    :cond_0
    const-string v2, "192.168.43.1"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 245
    sget-object v2, Lcom/samsung/groupplay/sdk/ApplicationInterface;->TAG:Ljava/lang/String;

    const-string v3, "[isClient] not client"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 248
    :cond_1
    sget-object v1, Lcom/samsung/groupplay/sdk/ApplicationInterface;->TAG:Ljava/lang/String;

    const-string v2, "[isClient] client"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 249
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isHost()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 213
    sget-object v2, Lcom/samsung/groupplay/sdk/ApplicationInterface;->TAG:Ljava/lang/String;

    const-string v3, "[isHost]"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 215
    invoke-direct {p0}, Lcom/samsung/groupplay/sdk/ApplicationInterface;->getWifiIP()Ljava/lang/String;

    move-result-object v0

    .line 216
    .local v0, "myIP":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 217
    sget-object v2, Lcom/samsung/groupplay/sdk/ApplicationInterface;->TAG:Ljava/lang/String;

    const-string v3, "[isHost] not host"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 226
    :goto_0
    return v1

    .line 221
    :cond_0
    const-string v2, "192.168.43.1"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 222
    sget-object v1, Lcom/samsung/groupplay/sdk/ApplicationInterface;->TAG:Ljava/lang/String;

    const-string v2, "[isHost] host"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 223
    const/4 v1, 0x1

    goto :goto_0

    .line 225
    :cond_1
    sget-object v2, Lcom/samsung/groupplay/sdk/ApplicationInterface;->TAG:Ljava/lang/String;

    const-string v3, "[isHost] not host"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public isRunning()Z
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 102
    sget-object v5, Lcom/samsung/groupplay/sdk/ApplicationInterface;->TAG:Ljava/lang/String;

    const-string v6, "[isRunning]"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 104
    iget-object v5, p0, Lcom/samsung/groupplay/sdk/ApplicationInterface;->mContext:Landroid/content/Context;

    if-nez v5, :cond_0

    .line 105
    sget-object v5, Lcom/samsung/groupplay/sdk/ApplicationInterface;->TAG:Ljava/lang/String;

    const-string v6, "[isRunning] Context is null"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 124
    :goto_0
    return v4

    .line 109
    :cond_0
    iget-object v5, p0, Lcom/samsung/groupplay/sdk/ApplicationInterface;->mContext:Landroid/content/Context;

    const-string v6, "activity"

    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 110
    .local v0, "am":Landroid/app/ActivityManager;
    const v5, 0x7fffffff

    invoke-virtual {v0, v5}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v3

    .line 111
    .local v3, "recentTasks":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 112
    .local v1, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 113
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/ActivityManager$RunningTaskInfo;

    .line 114
    .local v2, "rap":Landroid/app/ActivityManager$RunningTaskInfo;
    iget-object v5, v2, Landroid/app/ActivityManager$RunningTaskInfo;->baseActivity:Landroid/content/ComponentName;

    invoke-virtual {v5}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v5

    const-string v6, "com.samsung.groupcast"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 115
    sget-object v4, Lcom/samsung/groupplay/sdk/ApplicationInterface;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "[isRunning] Found GroupPlay task - id : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v2, Landroid/app/ActivityManager$RunningTaskInfo;->id:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", TopActivity : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, v2, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 117
    iput-object v2, p0, Lcom/samsung/groupplay/sdk/ApplicationInterface;->mGroupPlayTask:Landroid/app/ActivityManager$RunningTaskInfo;

    .line 119
    sget-object v4, Lcom/samsung/groupplay/sdk/ApplicationInterface;->TAG:Ljava/lang/String;

    const-string v5, "[isRunning] running"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 120
    const/4 v4, 0x1

    goto :goto_0

    .line 123
    .end local v2    # "rap":Landroid/app/ActivityManager$RunningTaskInfo;
    :cond_2
    sget-object v5, Lcom/samsung/groupplay/sdk/ApplicationInterface;->TAG:Ljava/lang/String;

    const-string v6, "[isRunning] not running"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public startGroupPlay()V
    .locals 5

    .prologue
    .line 284
    sget-object v2, Lcom/samsung/groupplay/sdk/ApplicationInterface;->TAG:Ljava/lang/String;

    const-string v3, "[startGroupPlay]"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 287
    :try_start_0
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.samsung.groupcast.action.SEND_GPSDK"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 288
    .local v1, "intent":Landroid/content/Intent;
    const/4 v2, 0x0

    iget-object v3, p0, Lcom/samsung/groupplay/sdk/ApplicationInterface;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v1, v2, v3}, Lcom/samsung/groupplay/sdk/ApplicationInterface;->addExtraIntent(Landroid/content/Intent;ILjava/lang/String;)V

    .line 289
    iget-object v2, p0, Lcom/samsung/groupplay/sdk/ApplicationInterface;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 293
    .end local v1    # "intent":Landroid/content/Intent;
    :goto_0
    return-void

    .line 290
    :catch_0
    move-exception v0

    .line 291
    .local v0, "e":Ljava/lang/Exception;
    sget-object v2, Lcom/samsung/groupplay/sdk/ApplicationInterface;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[startGroupPlay] : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public terminate()V
    .locals 2

    .prologue
    .line 137
    sget-object v0, Lcom/samsung/groupplay/sdk/ApplicationInterface;->TAG:Ljava/lang/String;

    const-string v1, "[terminate]"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 139
    iget-boolean v0, p0, Lcom/samsung/groupplay/sdk/ApplicationInterface;->bInit:Z

    if-eqz v0, :cond_0

    .line 140
    invoke-direct {p0}, Lcom/samsung/groupplay/sdk/ApplicationInterface;->unbind()V

    .line 141
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/groupplay/sdk/ApplicationInterface;->bInit:Z

    .line 143
    :cond_0
    return-void
.end method
