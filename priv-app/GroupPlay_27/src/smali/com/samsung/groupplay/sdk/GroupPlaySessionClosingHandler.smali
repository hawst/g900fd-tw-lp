.class public Lcom/samsung/groupplay/sdk/GroupPlaySessionClosingHandler;
.super Ljava/lang/Object;
.source "GroupPlaySessionClosingHandler.java"


# static fields
.field private static final ACTION_GROUP_PLAY_SESSION_CLOSED:Ljava/lang/String; = "com.samsung.groupcast.action.GROUP_PLAY_SESSION_CLOSED"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static notifyTermination()V
    .locals 2

    .prologue
    .line 12
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.samsung.groupcast.action.GROUP_PLAY_SESSION_CLOSED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 13
    .local v0, "gpSessionClosedIntent":Landroid/content/Intent;
    invoke-static {}, Lcom/samsung/groupcast/application/App;->getInstance()Lcom/samsung/groupcast/application/App;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/samsung/groupcast/application/App;->sendBroadcast(Landroid/content/Intent;)V

    .line 14
    return-void
.end method
