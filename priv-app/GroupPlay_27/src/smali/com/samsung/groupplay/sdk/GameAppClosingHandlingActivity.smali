.class public Lcom/samsung/groupplay/sdk/GameAppClosingHandlingActivity;
.super Landroid/app/Activity;
.source "GameAppClosingHandlingActivity.java"


# static fields
.field public static final ACTION_CLOSE:Ljava/lang/String; = "com.samsung.groupcast.action.APP_CLOSE_NOTIFICATION_TO_GP2"

.field public static final EXTRA_END_GROUP_PLAY:Ljava/lang/String; = "EXTRA_END_GROUP_PLAY"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "arg0"    # Landroid/os/Bundle;

    .prologue
    .line 18
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 21
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 22
    .local v0, "endMain":Landroid/content/Intent;
    const-string v1, "com.samsung.groupcast.action.APP_CLOSE_NOTIFICATION_TO_GP2"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 23
    invoke-virtual {p0, v0}, Lcom/samsung/groupplay/sdk/GameAppClosingHandlingActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 32
    invoke-virtual {p0}, Lcom/samsung/groupplay/sdk/GameAppClosingHandlingActivity;->finish()V

    .line 33
    return-void
.end method
