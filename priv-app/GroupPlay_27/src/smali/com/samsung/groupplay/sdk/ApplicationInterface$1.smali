.class Lcom/samsung/groupplay/sdk/ApplicationInterface$1;
.super Ljava/lang/Object;
.source "ApplicationInterface.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/groupplay/sdk/ApplicationInterface;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/groupplay/sdk/ApplicationInterface;


# direct methods
.method constructor <init>(Lcom/samsung/groupplay/sdk/ApplicationInterface;)V
    .locals 0

    .prologue
    .line 70
    iput-object p1, p0, Lcom/samsung/groupplay/sdk/ApplicationInterface$1;->this$0:Lcom/samsung/groupplay/sdk/ApplicationInterface;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2
    .param p1, "name"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 79
    # getter for: Lcom/samsung/groupplay/sdk/ApplicationInterface;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/groupplay/sdk/ApplicationInterface;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "[onServiceConnected]"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 80
    iget-object v0, p0, Lcom/samsung/groupplay/sdk/ApplicationInterface$1;->this$0:Lcom/samsung/groupplay/sdk/ApplicationInterface;

    invoke-static {p2}, Lcom/samsung/groupcast/service/IGroupPlayState$Stub;->asInterface(Landroid/os/IBinder;)Lcom/samsung/groupcast/service/IGroupPlayState;

    move-result-object v1

    # setter for: Lcom/samsung/groupplay/sdk/ApplicationInterface;->mState:Lcom/samsung/groupcast/service/IGroupPlayState;
    invoke-static {v0, v1}, Lcom/samsung/groupplay/sdk/ApplicationInterface;->access$102(Lcom/samsung/groupplay/sdk/ApplicationInterface;Lcom/samsung/groupcast/service/IGroupPlayState;)Lcom/samsung/groupcast/service/IGroupPlayState;

    .line 81
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1, "name"    # Landroid/content/ComponentName;

    .prologue
    .line 73
    # getter for: Lcom/samsung/groupplay/sdk/ApplicationInterface;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/groupplay/sdk/ApplicationInterface;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "[onServiceDisconnected]"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 74
    iget-object v0, p0, Lcom/samsung/groupplay/sdk/ApplicationInterface$1;->this$0:Lcom/samsung/groupplay/sdk/ApplicationInterface;

    const/4 v1, 0x0

    # setter for: Lcom/samsung/groupplay/sdk/ApplicationInterface;->mState:Lcom/samsung/groupcast/service/IGroupPlayState;
    invoke-static {v0, v1}, Lcom/samsung/groupplay/sdk/ApplicationInterface;->access$102(Lcom/samsung/groupplay/sdk/ApplicationInterface;Lcom/samsung/groupcast/service/IGroupPlayState;)Lcom/samsung/groupcast/service/IGroupPlayState;

    .line 75
    return-void
.end method
