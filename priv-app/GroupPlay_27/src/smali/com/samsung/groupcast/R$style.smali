.class public final Lcom/samsung/groupcast/R$style;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/groupcast/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "style"
.end annotation


# static fields
.field public static final AppBaseTheme:I = 0x7f090000

.field public static final AppTheme:I = 0x7f090001

.field public static final CustomRatingBar:I = 0x7f090002

.field public static final HelpListItem:I = 0x7f09000d

.field public static final HelpListItemButton:I = 0x7f090010

.field public static final HelpListItemImage:I = 0x7f090011

.field public static final HelpListItemNumber:I = 0x7f09000e

.field public static final HelpListItem_NoLeftMargin:I = 0x7f09000f

.field public static final HelpPageLayoutBase:I = 0x7f09000a

.field public static final HelpPageTitle:I = 0x7f09000c

.field public static final NumberedList:I = 0x7f09000b

.field public static final tag_DefaultListHeader:I = 0x7f090003

.field public static final tag_GroupCastBaseActionBar:I = 0x7f090022

.field public static final tag_GroupCastBaseActionBarLight:I = 0x7f090023

.field public static final tag_GroupCastBaseTheme:I = 0x7f090013

.field public static final tag_GroupCastBaseThemeLight:I = 0x7f090014

.field public static final tag_GroupCastBaseThemeLightLandscape:I = 0x7f090015

.field public static final tag_GroupCastHelpThemeLight:I = 0x7f09001d

.field public static final tag_GroupCastJoinThemeVariant:I = 0x7f090016

.field public static final tag_GroupCastLicenseThemeVariant:I = 0x7f09001a

.field public static final tag_GroupCastNoUITheme:I = 0x7f090012

.field public static final tag_GroupCastPlayGameTheme:I = 0x7f09001e

.field public static final tag_GroupCastPlayGameThemeLight:I = 0x7f09001f

.field public static final tag_GroupCastPlayGameThemeVariant:I = 0x7f090017

.field public static final tag_GroupCastStartScreenForExternalThemeVariant:I = 0x7f09001c

.field public static final tag_GroupCastStartScreenThemeVariant:I = 0x7f090019

.field public static final tag_GroupCastTransparentActionBar:I = 0x7f09001b

.field public static final tag_GroupCastViewerActionBar:I = 0x7f090024

.field public static final tag_GroupCastViewerActionBarLight:I = 0x7f090025

.field public static final tag_GroupCastViewerTheme:I = 0x7f090020

.field public static final tag_GroupCastViewerThemeLight:I = 0x7f090021

.field public static final tag_GroupCastViewerThemeVariant:I = 0x7f090018

.field public static final tag_MainListHeader:I = 0x7f090004

.field public static final tag_MainListSessionItem:I = 0x7f090006

.field public static final tag_MainListStartShareItem:I = 0x7f090005

.field public static final tag_PenSettingsDialogTheme:I = 0x7f090026

.field public static final tag_PenSettingsDialogTitle:I = 0x7f090027

.field public static final tag_PlaygameInstall1Item:I = 0x7f090008

.field public static final tag_PlaygameInstall2Item:I = 0x7f090009

.field public static final tag_PlaygameTitleItem:I = 0x7f090007


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11126
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
