.class public Lcom/samsung/groupcast/view/ViewPager;
.super Landroid/support/v4/view/FixedViewPager;
.source "ViewPager.java"


# instance fields
.field private mPagingEnabled:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 12
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/samsung/groupcast/view/ViewPager;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 13
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 16
    invoke-direct {p0, p1, p2}, Landroid/support/v4/view/FixedViewPager;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 9
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/groupcast/view/ViewPager;->mPagingEnabled:Z

    .line 17
    return-void
.end method


# virtual methods
.method public isPagingEnabled()Z
    .locals 1

    .prologue
    .line 20
    iget-boolean v0, p0, Lcom/samsung/groupcast/view/ViewPager;->mPagingEnabled:Z

    return v0
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v1, 0x0

    .line 29
    iget-boolean v2, p0, Lcom/samsung/groupcast/view/ViewPager;->mPagingEnabled:Z

    if-nez v2, :cond_1

    .line 43
    :cond_0
    :goto_0
    return v1

    .line 33
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/groupcast/view/ViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/samsung/groupcast/view/ViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/view/PagerAdapter;->getCount()I

    move-result v2

    const/4 v3, 0x1

    if-le v2, v3, :cond_0

    .line 40
    :try_start_0
    invoke-super {p0, p1}, Landroid/support/v4/view/FixedViewPager;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 41
    .local v1, "intercept":Z
    goto :goto_0

    .line 42
    .end local v1    # "intercept":Z
    :catch_0
    move-exception v0

    .line 43
    .local v0, "exception":Ljava/lang/Exception;
    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v1, 0x0

    .line 49
    iget-boolean v2, p0, Lcom/samsung/groupcast/view/ViewPager;->mPagingEnabled:Z

    if-nez v2, :cond_1

    .line 63
    :cond_0
    :goto_0
    return v1

    .line 53
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/groupcast/view/ViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/samsung/groupcast/view/ViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/view/PagerAdapter;->getCount()I

    move-result v2

    const/4 v3, 0x1

    if-le v2, v3, :cond_0

    .line 60
    :try_start_0
    invoke-super {p0, p1}, Landroid/support/v4/view/FixedViewPager;->onTouchEvent(Landroid/view/MotionEvent;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 61
    .local v1, "intercept":Z
    goto :goto_0

    .line 62
    .end local v1    # "intercept":Z
    :catch_0
    move-exception v0

    .line 63
    .local v0, "exception":Ljava/lang/Exception;
    goto :goto_0
.end method

.method public setPagingEnabled(Z)V
    .locals 0
    .param p1, "enabled"    # Z

    .prologue
    .line 24
    iput-boolean p1, p0, Lcom/samsung/groupcast/view/ViewPager;->mPagingEnabled:Z

    .line 25
    return-void
.end method
