.class public Lcom/samsung/groupcast/view/ViewTools;
.super Ljava/lang/Object;
.source "ViewTools.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static hideSoftKeyboard(Landroid/app/Activity;)V
    .locals 1
    .param p0, "activity"    # Landroid/app/Activity;

    .prologue
    .line 16
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/samsung/groupcast/view/ViewTools;->hideSoftKeyboard(Landroid/app/Activity;Landroid/view/View;)V

    .line 17
    return-void
.end method

.method public static hideSoftKeyboard(Landroid/app/Activity;Landroid/view/View;)V
    .locals 4
    .param p0, "activity"    # Landroid/app/Activity;
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 20
    if-nez p0, :cond_0

    .line 40
    :goto_0
    return-void

    .line 24
    :cond_0
    if-eqz p1, :cond_3

    move-object v0, p1

    .line 26
    .local v0, "currentFocus":Landroid/view/View;
    :goto_1
    if-eqz v0, :cond_2

    .line 27
    const-string v2, "input_method"

    invoke-virtual {p0, v2}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/inputmethod/InputMethodManager;

    .line 30
    .local v1, "imm":Landroid/view/inputmethod/InputMethodManager;
    invoke-virtual {v0}, Landroid/view/View;->isFocused()Z

    move-result v2

    if-nez v2, :cond_1

    .line 31
    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 34
    :cond_1
    invoke-virtual {v0}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 35
    invoke-virtual {v0}, Landroid/view/View;->clearFocus()V

    .line 38
    .end local v1    # "imm":Landroid/view/inputmethod/InputMethodManager;
    :cond_2
    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v2

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Landroid/view/Window;->setSoftInputMode(I)V

    goto :goto_0

    .line 24
    .end local v0    # "currentFocus":Landroid/view/View;
    :cond_3
    invoke-virtual {p0}, Landroid/app/Activity;->getCurrentFocus()Landroid/view/View;

    move-result-object v0

    goto :goto_1
.end method

.method public static isEditorActionForSubmitting(ILandroid/view/KeyEvent;)Z
    .locals 4
    .param p0, "actionId"    # I
    .param p1, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 65
    const/4 v2, 0x6

    if-eq p0, v2, :cond_0

    const/4 v2, 0x2

    if-ne p0, v2, :cond_2

    :cond_0
    move v0, v1

    .line 78
    :cond_1
    :goto_0
    return v0

    .line 69
    :cond_2
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    if-nez v2, :cond_1

    .line 73
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v2

    const/16 v3, 0x42

    if-eq v2, v3, :cond_3

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v2

    const/16 v3, 0x17

    if-ne v2, v3, :cond_1

    :cond_3
    move v0, v1

    .line 75
    goto :goto_0
.end method

.method public static showSowftKeyboard(Landroid/app/Activity;Landroid/view/View;)V
    .locals 3
    .param p0, "activity"    # Landroid/app/Activity;
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 43
    move-object v0, p1

    .line 45
    .local v0, "currentFocus":Landroid/view/View;
    if-eqz v0, :cond_1

    .line 46
    const-string v2, "input_method"

    invoke-virtual {p0, v2}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/inputmethod/InputMethodManager;

    .line 49
    .local v1, "imm":Landroid/view/inputmethod/InputMethodManager;
    invoke-virtual {v0}, Landroid/view/View;->isFocused()Z

    move-result v2

    if-nez v2, :cond_0

    .line 50
    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 53
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    .line 55
    .end local v1    # "imm":Landroid/view/inputmethod/InputMethodManager;
    :cond_1
    return-void
.end method
