.class public interface abstract Lcom/samsung/groupcast/view/ZoomDetector$ZoomerDelegate;
.super Ljava/lang/Object;
.source "ZoomDetector.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/groupcast/view/ZoomDetector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ZoomerDelegate"
.end annotation


# virtual methods
.method public abstract isPanable()Z
.end method

.method public abstract isTwoFingerPanMode()Z
.end method

.method public abstract onGetMatrix(Lcom/samsung/groupcast/view/ZoomDetector;)Landroid/graphics/Matrix;
.end method

.method public abstract onPan(Lcom/samsung/groupcast/view/ZoomDetector;FF)V
.end method

.method public abstract onScale(Lcom/samsung/groupcast/view/ZoomDetector;F)Landroid/graphics/Matrix;
.end method

.method public abstract onZoomGestureBegin(Lcom/samsung/groupcast/view/ZoomDetector;)V
.end method

.method public abstract onZoomGestureEnd(Lcom/samsung/groupcast/view/ZoomDetector;)V
.end method
