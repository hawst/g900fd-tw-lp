.class public Lcom/samsung/groupcast/view/ZoomDetector;
.super Ljava/lang/Object;
.source "ZoomDetector.java"

# interfaces
.implements Landroid/view/ScaleGestureDetector$OnScaleGestureListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/groupcast/view/ZoomDetector$ZoomerDelegate;
    }
.end annotation


# static fields
.field private static final DEFAULT_SCALE_LOCK_THRESHOLD:F = 80.0f

.field private static final DEFAULT_SCALE_UNLOCK_THRESHOLD:F = 0.2f


# instance fields
.field private mAccumFocusDx:F

.field private mAccumFocusDy:F

.field private final mDelegate:Lcom/samsung/groupcast/view/ZoomDetector$ZoomerDelegate;

.field private mEnabled:Z

.field private mGestureBegan:Z

.field private mLastScaleFocusX:F

.field private mLastScaleFocusY:F

.field private mLastTouchX:F

.field private mLastTouchY:F

.field private mReferenceScaleSpan:F

.field private final mScaleDetector:Landroid/view/ScaleGestureDetector;

.field private mScaleLockEnabled:Z

.field private mScaleLockThreshold:F

.field private mScaleLockThresholdSquared:F

.field private mScaleUnlockThreshold:F

.field private mTrackedPointerId:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/samsung/groupcast/view/ZoomDetector$ZoomerDelegate;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "delegate"    # Lcom/samsung/groupcast/view/ZoomDetector$ZoomerDelegate;

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    const v0, 0x3e4ccccd    # 0.2f

    iput v0, p0, Lcom/samsung/groupcast/view/ZoomDetector;->mScaleUnlockThreshold:F

    .line 36
    const/high16 v0, 0x42a00000    # 80.0f

    iput v0, p0, Lcom/samsung/groupcast/view/ZoomDetector;->mScaleLockThreshold:F

    .line 37
    const/high16 v0, 0x45c80000    # 6400.0f

    iput v0, p0, Lcom/samsung/groupcast/view/ZoomDetector;->mScaleLockThresholdSquared:F

    .line 41
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/groupcast/view/ZoomDetector;->mTrackedPointerId:I

    .line 52
    const-string v0, "context"

    invoke-static {v0, p1}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 53
    const-string v0, "delegate"

    invoke-static {v0, p2}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 54
    iput-object p2, p0, Lcom/samsung/groupcast/view/ZoomDetector;->mDelegate:Lcom/samsung/groupcast/view/ZoomDetector$ZoomerDelegate;

    .line 55
    new-instance v0, Landroid/view/ScaleGestureDetector;

    invoke-direct {v0, p1, p0}, Landroid/view/ScaleGestureDetector;-><init>(Landroid/content/Context;Landroid/view/ScaleGestureDetector$OnScaleGestureListener;)V

    iput-object v0, p0, Lcom/samsung/groupcast/view/ZoomDetector;->mScaleDetector:Landroid/view/ScaleGestureDetector;

    .line 56
    return-void
.end method

.method private disableScaleLock()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 261
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/groupcast/view/ZoomDetector;->mScaleLockEnabled:Z

    .line 262
    iput v1, p0, Lcom/samsung/groupcast/view/ZoomDetector;->mAccumFocusDx:F

    .line 263
    iput v1, p0, Lcom/samsung/groupcast/view/ZoomDetector;->mAccumFocusDx:F

    .line 264
    return-void
.end method

.method private enableScaleLock(F)V
    .locals 2
    .param p1, "referenceScaleSpan"    # F

    .prologue
    const/4 v1, 0x0

    .line 254
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/groupcast/view/ZoomDetector;->mScaleLockEnabled:Z

    .line 255
    iput p1, p0, Lcom/samsung/groupcast/view/ZoomDetector;->mReferenceScaleSpan:F

    .line 256
    iput v1, p0, Lcom/samsung/groupcast/view/ZoomDetector;->mAccumFocusDx:F

    .line 257
    iput v1, p0, Lcom/samsung/groupcast/view/ZoomDetector;->mAccumFocusDy:F

    .line 258
    return-void
.end method

.method private onCancelEvent(Landroid/view/MotionEvent;)V
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 248
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/groupcast/view/ZoomDetector;->mTrackedPointerId:I

    .line 249
    invoke-direct {p0}, Lcom/samsung/groupcast/view/ZoomDetector;->stopGestureIfNeeded()V

    .line 250
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/groupcast/view/ZoomDetector;->mReferenceScaleSpan:F

    .line 251
    return-void
.end method

.method private onDownEvent(Landroid/view/MotionEvent;)V
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 184
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Lcom/samsung/groupcast/view/ZoomDetector;->mLastTouchX:F

    .line 185
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, Lcom/samsung/groupcast/view/ZoomDetector;->mLastTouchY:F

    .line 186
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v0

    iput v0, p0, Lcom/samsung/groupcast/view/ZoomDetector;->mTrackedPointerId:I

    .line 187
    return-void
.end method

.method private onMoveEvent(Landroid/view/MotionEvent;)Z
    .locals 9
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v8, -0x1

    .line 190
    const/4 v0, 0x1

    .line 191
    .local v0, "bConsumed":Z
    iget v7, p0, Lcom/samsung/groupcast/view/ZoomDetector;->mTrackedPointerId:I

    invoke-virtual {p1, v7}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v4

    .line 192
    .local v4, "pointerIndex":I
    if-ne v4, v8, :cond_4

    iget v5, p0, Lcom/samsung/groupcast/view/ZoomDetector;->mLastTouchX:F

    .line 193
    .local v5, "x":F
    :goto_0
    if-ne v4, v8, :cond_5

    iget v6, p0, Lcom/samsung/groupcast/view/ZoomDetector;->mLastTouchY:F

    .line 195
    .local v6, "y":F
    :goto_1
    const/4 v1, 0x0

    .line 196
    .local v1, "bTwoFingerMode":Z
    iget-object v7, p0, Lcom/samsung/groupcast/view/ZoomDetector;->mDelegate:Lcom/samsung/groupcast/view/ZoomDetector$ZoomerDelegate;

    if-eqz v7, :cond_0

    .line 197
    iget-object v7, p0, Lcom/samsung/groupcast/view/ZoomDetector;->mDelegate:Lcom/samsung/groupcast/view/ZoomDetector$ZoomerDelegate;

    invoke-interface {v7}, Lcom/samsung/groupcast/view/ZoomDetector$ZoomerDelegate;->isTwoFingerPanMode()Z

    move-result v1

    .line 200
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v7

    const/4 v8, 0x1

    if-ne v7, v8, :cond_1

    if-eqz v1, :cond_2

    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v7

    const/4 v8, 0x2

    if-ne v7, v8, :cond_7

    if-eqz v1, :cond_7

    .line 203
    :cond_2
    iget v7, p0, Lcom/samsung/groupcast/view/ZoomDetector;->mLastTouchX:F

    sub-float v2, v5, v7

    .line 204
    .local v2, "dx":F
    iget v7, p0, Lcom/samsung/groupcast/view/ZoomDetector;->mLastTouchY:F

    sub-float v3, v6, v7

    .line 206
    .local v3, "dy":F
    invoke-direct {p0}, Lcom/samsung/groupcast/view/ZoomDetector;->startGestureIfNeeded()V

    .line 208
    iget-object v7, p0, Lcom/samsung/groupcast/view/ZoomDetector;->mDelegate:Lcom/samsung/groupcast/view/ZoomDetector$ZoomerDelegate;

    if-eqz v7, :cond_3

    .line 210
    iget-object v7, p0, Lcom/samsung/groupcast/view/ZoomDetector;->mDelegate:Lcom/samsung/groupcast/view/ZoomDetector$ZoomerDelegate;

    invoke-interface {v7}, Lcom/samsung/groupcast/view/ZoomDetector$ZoomerDelegate;->isPanable()Z

    move-result v7

    if-eqz v7, :cond_6

    .line 211
    iget-object v7, p0, Lcom/samsung/groupcast/view/ZoomDetector;->mDelegate:Lcom/samsung/groupcast/view/ZoomDetector$ZoomerDelegate;

    invoke-interface {v7, p0, v2, v3}, Lcom/samsung/groupcast/view/ZoomDetector$ZoomerDelegate;->onPan(Lcom/samsung/groupcast/view/ZoomDetector;FF)V

    .line 220
    .end local v2    # "dx":F
    .end local v3    # "dy":F
    :cond_3
    :goto_2
    iput v5, p0, Lcom/samsung/groupcast/view/ZoomDetector;->mLastTouchX:F

    .line 221
    iput v6, p0, Lcom/samsung/groupcast/view/ZoomDetector;->mLastTouchY:F

    .line 222
    return v0

    .line 192
    .end local v1    # "bTwoFingerMode":Z
    .end local v5    # "x":F
    .end local v6    # "y":F
    :cond_4
    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getX(I)F

    move-result v5

    goto :goto_0

    .line 193
    .restart local v5    # "x":F
    :cond_5
    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getY(I)F

    move-result v6

    goto :goto_1

    .line 213
    .restart local v1    # "bTwoFingerMode":Z
    .restart local v2    # "dx":F
    .restart local v3    # "dy":F
    .restart local v6    # "y":F
    :cond_6
    const/4 v0, 0x0

    goto :goto_2

    .line 217
    .end local v2    # "dx":F
    .end local v3    # "dy":F
    :cond_7
    const/4 v0, 0x0

    goto :goto_2
.end method

.method private onPointerDownEvent(Landroid/view/MotionEvent;)V
    .locals 0
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 239
    return-void
.end method

.method private onPointerUpEvent(Landroid/view/MotionEvent;)V
    .locals 4
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 226
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v0

    .line 227
    .local v0, "actionIndex":I
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v2

    .line 229
    .local v2, "pointerId":I
    iget v3, p0, Lcom/samsung/groupcast/view/ZoomDetector;->mTrackedPointerId:I

    if-ne v2, v3, :cond_0

    .line 230
    if-nez v0, :cond_1

    const/4 v1, 0x1

    .line 231
    .local v1, "newPointerIndex":I
    :goto_0
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getX(I)F

    move-result v3

    iput v3, p0, Lcom/samsung/groupcast/view/ZoomDetector;->mLastTouchX:F

    .line 232
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getY(I)F

    move-result v3

    iput v3, p0, Lcom/samsung/groupcast/view/ZoomDetector;->mLastTouchY:F

    .line 233
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v3

    iput v3, p0, Lcom/samsung/groupcast/view/ZoomDetector;->mTrackedPointerId:I

    .line 235
    .end local v1    # "newPointerIndex":I
    :cond_0
    return-void

    .line 230
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private onUpEvent(Landroid/view/MotionEvent;)V
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 242
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/groupcast/view/ZoomDetector;->mTrackedPointerId:I

    .line 243
    invoke-direct {p0}, Lcom/samsung/groupcast/view/ZoomDetector;->stopGestureIfNeeded()V

    .line 244
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/groupcast/view/ZoomDetector;->mReferenceScaleSpan:F

    .line 245
    return-void
.end method

.method private startGestureIfNeeded()V
    .locals 1

    .prologue
    .line 290
    iget-object v0, p0, Lcom/samsung/groupcast/view/ZoomDetector;->mDelegate:Lcom/samsung/groupcast/view/ZoomDetector$ZoomerDelegate;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/samsung/groupcast/view/ZoomDetector;->mGestureBegan:Z

    if-nez v0, :cond_0

    .line 291
    iget-object v0, p0, Lcom/samsung/groupcast/view/ZoomDetector;->mDelegate:Lcom/samsung/groupcast/view/ZoomDetector$ZoomerDelegate;

    invoke-interface {v0, p0}, Lcom/samsung/groupcast/view/ZoomDetector$ZoomerDelegate;->onZoomGestureBegin(Lcom/samsung/groupcast/view/ZoomDetector;)V

    .line 292
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/groupcast/view/ZoomDetector;->mGestureBegan:Z

    .line 294
    :cond_0
    return-void
.end method

.method private stopGestureIfNeeded()V
    .locals 1

    .prologue
    .line 297
    iget-object v0, p0, Lcom/samsung/groupcast/view/ZoomDetector;->mDelegate:Lcom/samsung/groupcast/view/ZoomDetector$ZoomerDelegate;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/samsung/groupcast/view/ZoomDetector;->mGestureBegan:Z

    if-eqz v0, :cond_0

    .line 298
    iget-object v0, p0, Lcom/samsung/groupcast/view/ZoomDetector;->mDelegate:Lcom/samsung/groupcast/view/ZoomDetector$ZoomerDelegate;

    invoke-interface {v0, p0}, Lcom/samsung/groupcast/view/ZoomDetector$ZoomerDelegate;->onZoomGestureEnd(Lcom/samsung/groupcast/view/ZoomDetector;)V

    .line 301
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/groupcast/view/ZoomDetector;->mGestureBegan:Z

    .line 302
    return-void
.end method

.method private updateScaleLock(FFF)V
    .locals 7
    .param p1, "focusDx"    # F
    .param p2, "focusDy"    # F
    .param p3, "currentSpan"    # F

    .prologue
    const/high16 v5, 0x3f800000    # 1.0f

    .line 267
    iget-boolean v4, p0, Lcom/samsung/groupcast/view/ZoomDetector;->mScaleLockEnabled:Z

    if-eqz v4, :cond_2

    .line 268
    const/4 v4, 0x0

    cmpl-float v4, p3, v4

    if-lez v4, :cond_1

    .line 269
    iget v4, p0, Lcom/samsung/groupcast/view/ZoomDetector;->mReferenceScaleSpan:F

    div-float v0, p3, v4

    .line 270
    .local v0, "absoluteScaleChange":F
    iget v4, p0, Lcom/samsung/groupcast/view/ZoomDetector;->mScaleUnlockThreshold:F

    add-float v3, v5, v4

    .line 271
    .local v3, "upperThreshold":F
    iget v4, p0, Lcom/samsung/groupcast/view/ZoomDetector;->mScaleUnlockThreshold:F

    sub-float v2, v5, v4

    .line 273
    .local v2, "lowerThreshold":F
    cmpl-float v4, v0, v3

    if-gtz v4, :cond_0

    cmpg-float v4, v0, v2

    if-gez v4, :cond_1

    .line 274
    :cond_0
    invoke-direct {p0}, Lcom/samsung/groupcast/view/ZoomDetector;->disableScaleLock()V

    .line 287
    .end local v0    # "absoluteScaleChange":F
    .end local v2    # "lowerThreshold":F
    .end local v3    # "upperThreshold":F
    :cond_1
    :goto_0
    return-void

    .line 278
    :cond_2
    iget v4, p0, Lcom/samsung/groupcast/view/ZoomDetector;->mAccumFocusDx:F

    add-float/2addr v4, p1

    iput v4, p0, Lcom/samsung/groupcast/view/ZoomDetector;->mAccumFocusDx:F

    .line 279
    iget v4, p0, Lcom/samsung/groupcast/view/ZoomDetector;->mAccumFocusDy:F

    add-float/2addr v4, p2

    iput v4, p0, Lcom/samsung/groupcast/view/ZoomDetector;->mAccumFocusDy:F

    .line 280
    iget v4, p0, Lcom/samsung/groupcast/view/ZoomDetector;->mAccumFocusDx:F

    iget v5, p0, Lcom/samsung/groupcast/view/ZoomDetector;->mAccumFocusDx:F

    mul-float/2addr v4, v5

    iget v5, p0, Lcom/samsung/groupcast/view/ZoomDetector;->mAccumFocusDy:F

    iget v6, p0, Lcom/samsung/groupcast/view/ZoomDetector;->mAccumFocusDy:F

    mul-float/2addr v5, v6

    add-float v1, v4, v5

    .line 283
    .local v1, "accumFocusChangeSquared":F
    iget v4, p0, Lcom/samsung/groupcast/view/ZoomDetector;->mScaleLockThresholdSquared:F

    cmpl-float v4, v1, v4

    if-lez v4, :cond_1

    .line 284
    invoke-direct {p0, p3}, Lcom/samsung/groupcast/view/ZoomDetector;->enableScaleLock(F)V

    goto :goto_0
.end method


# virtual methods
.method public getScaleLockThreshold()F
    .locals 1

    .prologue
    .line 75
    iget v0, p0, Lcom/samsung/groupcast/view/ZoomDetector;->mScaleLockThreshold:F

    return v0
.end method

.method public getScaleUnlockThreshold()F
    .locals 1

    .prologue
    .line 67
    iget v0, p0, Lcom/samsung/groupcast/view/ZoomDetector;->mScaleUnlockThreshold:F

    return v0
.end method

.method public isEnabled()Z
    .locals 1

    .prologue
    .line 59
    iget-boolean v0, p0, Lcom/samsung/groupcast/view/ZoomDetector;->mEnabled:Z

    return v0
.end method

.method public onScale(Landroid/view/ScaleGestureDetector;)Z
    .locals 13
    .param p1, "detector"    # Landroid/view/ScaleGestureDetector;

    .prologue
    const/4 v12, 0x0

    const/4 v11, 0x1

    .line 137
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusX()F

    move-result v4

    .line 138
    .local v4, "focusX":F
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusY()F

    move-result v5

    .line 139
    .local v5, "focusY":F
    iget v10, p0, Lcom/samsung/groupcast/view/ZoomDetector;->mLastScaleFocusX:F

    sub-float v2, v4, v10

    .line 140
    .local v2, "focusDx":F
    iget v10, p0, Lcom/samsung/groupcast/view/ZoomDetector;->mLastScaleFocusY:F

    sub-float v3, v5, v10

    .line 141
    .local v3, "focusDy":F
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getCurrentSpan()F

    move-result v0

    .line 143
    .local v0, "currentSpan":F
    invoke-direct {p0, v2, v3, v0}, Lcom/samsung/groupcast/view/ZoomDetector;->updateScaleLock(FFF)V

    .line 145
    iget-boolean v10, p0, Lcom/samsung/groupcast/view/ZoomDetector;->mScaleLockEnabled:Z

    if-nez v10, :cond_0

    .line 157
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getScaleFactor()F

    move-result v1

    .line 159
    .local v1, "factor":F
    iget-object v10, p0, Lcom/samsung/groupcast/view/ZoomDetector;->mDelegate:Lcom/samsung/groupcast/view/ZoomDetector$ZoomerDelegate;

    invoke-interface {v10, p0}, Lcom/samsung/groupcast/view/ZoomDetector$ZoomerDelegate;->onGetMatrix(Lcom/samsung/groupcast/view/ZoomDetector;)Landroid/graphics/Matrix;

    move-result-object v9

    .line 160
    .local v9, "preScaleMatrix":Landroid/graphics/Matrix;
    new-instance v6, Landroid/graphics/Matrix;

    invoke-direct {v6}, Landroid/graphics/Matrix;-><init>()V

    .line 161
    .local v6, "inversePreScaleMatrix":Landroid/graphics/Matrix;
    iget-object v10, p0, Lcom/samsung/groupcast/view/ZoomDetector;->mDelegate:Lcom/samsung/groupcast/view/ZoomDetector$ZoomerDelegate;

    invoke-interface {v10, p0, v1}, Lcom/samsung/groupcast/view/ZoomDetector$ZoomerDelegate;->onScale(Lcom/samsung/groupcast/view/ZoomDetector;F)Landroid/graphics/Matrix;

    move-result-object v8

    .line 162
    .local v8, "postScaleMatrix":Landroid/graphics/Matrix;
    invoke-virtual {v9, v6}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    .line 163
    const/4 v10, 0x2

    new-array v7, v10, [F

    aput v4, v7, v12

    aput v5, v7, v11

    .line 164
    .local v7, "point":[F
    invoke-virtual {v6, v7}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 165
    invoke-virtual {v8, v7}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 166
    aget v10, v7, v12

    sub-float v10, v4, v10

    add-float/2addr v2, v10

    .line 167
    aget v10, v7, v11

    sub-float v10, v5, v10

    add-float/2addr v3, v10

    .line 171
    .end local v1    # "factor":F
    .end local v6    # "inversePreScaleMatrix":Landroid/graphics/Matrix;
    .end local v7    # "point":[F
    .end local v8    # "postScaleMatrix":Landroid/graphics/Matrix;
    .end local v9    # "preScaleMatrix":Landroid/graphics/Matrix;
    :cond_0
    iget-object v10, p0, Lcom/samsung/groupcast/view/ZoomDetector;->mDelegate:Lcom/samsung/groupcast/view/ZoomDetector$ZoomerDelegate;

    invoke-interface {v10, p0, v2, v3}, Lcom/samsung/groupcast/view/ZoomDetector$ZoomerDelegate;->onPan(Lcom/samsung/groupcast/view/ZoomDetector;FF)V

    .line 173
    iput v4, p0, Lcom/samsung/groupcast/view/ZoomDetector;->mLastScaleFocusX:F

    .line 174
    iput v5, p0, Lcom/samsung/groupcast/view/ZoomDetector;->mLastScaleFocusY:F

    .line 175
    return v11
.end method

.method public onScaleBegin(Landroid/view/ScaleGestureDetector;)Z
    .locals 1
    .param p1, "detector"    # Landroid/view/ScaleGestureDetector;

    .prologue
    .line 128
    invoke-direct {p0}, Lcom/samsung/groupcast/view/ZoomDetector;->startGestureIfNeeded()V

    .line 129
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusX()F

    move-result v0

    iput v0, p0, Lcom/samsung/groupcast/view/ZoomDetector;->mLastScaleFocusX:F

    .line 130
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusY()F

    move-result v0

    iput v0, p0, Lcom/samsung/groupcast/view/ZoomDetector;->mLastScaleFocusY:F

    .line 131
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getCurrentSpan()F

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/groupcast/view/ZoomDetector;->enableScaleLock(F)V

    .line 132
    const/4 v0, 0x1

    return v0
.end method

.method public onScaleEnd(Landroid/view/ScaleGestureDetector;)V
    .locals 0
    .param p1, "detector"    # Landroid/view/ScaleGestureDetector;

    .prologue
    .line 181
    return-void
.end method

.method public onTouch(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 84
    iget-boolean v1, p0, Lcom/samsung/groupcast/view/ZoomDetector;->mEnabled:Z

    if-nez v1, :cond_0

    .line 85
    const/4 v0, 0x0

    .line 123
    :goto_0
    return v0

    .line 88
    :cond_0
    iget-object v1, p0, Lcom/samsung/groupcast/view/ZoomDetector;->mScaleDetector:Landroid/view/ScaleGestureDetector;

    invoke-virtual {v1, p1}, Landroid/view/ScaleGestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 90
    .local v0, "bConsumed":Z
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 92
    :pswitch_1
    invoke-direct {p0, p1}, Lcom/samsung/groupcast/view/ZoomDetector;->onDownEvent(Landroid/view/MotionEvent;)V

    goto :goto_0

    .line 96
    :pswitch_2
    invoke-direct {p0, p1}, Lcom/samsung/groupcast/view/ZoomDetector;->onUpEvent(Landroid/view/MotionEvent;)V

    goto :goto_0

    .line 102
    :pswitch_3
    invoke-direct {p0, p1}, Lcom/samsung/groupcast/view/ZoomDetector;->onMoveEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 103
    goto :goto_0

    .line 107
    :pswitch_4
    invoke-direct {p0, p1}, Lcom/samsung/groupcast/view/ZoomDetector;->onCancelEvent(Landroid/view/MotionEvent;)V

    goto :goto_0

    .line 112
    :pswitch_5
    invoke-direct {p0, p1}, Lcom/samsung/groupcast/view/ZoomDetector;->onPointerUpEvent(Landroid/view/MotionEvent;)V

    goto :goto_0

    .line 117
    :pswitch_6
    invoke-direct {p0, p1}, Lcom/samsung/groupcast/view/ZoomDetector;->onPointerDownEvent(Landroid/view/MotionEvent;)V

    goto :goto_0

    .line 90
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_6
        :pswitch_5
    .end packed-switch
.end method

.method public setEnabled(Z)V
    .locals 0
    .param p1, "enabled"    # Z

    .prologue
    .line 63
    iput-boolean p1, p0, Lcom/samsung/groupcast/view/ZoomDetector;->mEnabled:Z

    .line 64
    return-void
.end method

.method public setScaleLockThresholdSquared(F)V
    .locals 1
    .param p1, "threshold"    # F

    .prologue
    .line 79
    iput p1, p0, Lcom/samsung/groupcast/view/ZoomDetector;->mScaleLockThreshold:F

    .line 80
    mul-float v0, p1, p1

    iput v0, p0, Lcom/samsung/groupcast/view/ZoomDetector;->mScaleLockThresholdSquared:F

    .line 81
    return-void
.end method

.method public setScaleUnlockThreshold(F)V
    .locals 1
    .param p1, "scaleUnlockThreshold"    # F

    .prologue
    .line 71
    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iput v0, p0, Lcom/samsung/groupcast/view/ZoomDetector;->mScaleUnlockThreshold:F

    .line 72
    return-void
.end method
