.class public final Lcom/samsung/groupcast/R$dimen;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/groupcast/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "dimen"
.end annotation


# static fields
.field public static final action_bar_height:I = 0x7f0d0038

.field public static final action_bar_item_pen_settings_color_margin:I = 0x7f0d0046

.field public static final button_width:I = 0x7f0d003b

.field public static final eraser_preview_size:I = 0x7f0d0033

.field public static final eraser_settings_width:I = 0x7f0d0039

.field public static final help_popup_text_margin_bottom:I = 0x7f0d003d

.field public static final help_share_content_pop_up_top:I = 0x7f0d003c

.field public static final help_tap_margin_top:I = 0x7f0d0040

.field public static final helppage_title_margin_bottom:I = 0x7f0d003f

.field public static final helppage_title_margin_top:I = 0x7f0d003e

.field public static final main_game_item_padding:I = 0x7f0d0045

.field public static final max_height:I = 0x7f0d0044

.field public static final participants_position_x:I = 0x7f0d0041

.field public static final pen_settings_width:I = 0x7f0d003a

.field public static final progress_popup_margin:I = 0x7f0d0036

.field public static final progress_popup_minheight:I = 0x7f0d0035

.field public static final progress_popup_padding:I = 0x7f0d0037

.field public static final progress_popup_width:I = 0x7f0d0034

.field public static final resolve_text_margin_multi:I = 0x7f0d0042

.field public static final resolve_text_margin_single:I = 0x7f0d0043

.field public static final tag_brushsize_preview_base_height:I = 0x7f0d0019

.field public static final tag_brushsize_preview_circle_bottom:I = 0x7f0d001d

.field public static final tag_brushsize_preview_circle_bottom_dark_shadow:I = 0x7f0d0025

.field public static final tag_brushsize_preview_circle_bottom_light_shadow:I = 0x7f0d0021

.field public static final tag_brushsize_preview_circle_left:I = 0x7f0d001a

.field public static final tag_brushsize_preview_circle_left_dark_shadow:I = 0x7f0d0022

.field public static final tag_brushsize_preview_circle_left_light_shadow:I = 0x7f0d001e

.field public static final tag_brushsize_preview_circle_right:I = 0x7f0d001b

.field public static final tag_brushsize_preview_circle_right_dark_shadow:I = 0x7f0d0023

.field public static final tag_brushsize_preview_circle_right_light_shadow:I = 0x7f0d001f

.field public static final tag_brushsize_preview_circle_top:I = 0x7f0d001c

.field public static final tag_brushsize_preview_circle_top_dark_shadow:I = 0x7f0d0024

.field public static final tag_brushsize_preview_circle_top_light_shadow:I = 0x7f0d0020

.field public static final tag_brushsize_seekbar_slot_bottom:I = 0x7f0d0014

.field public static final tag_brushsize_seekbar_slot_bottom_dark_shadow:I = 0x7f0d0018

.field public static final tag_brushsize_seekbar_slot_bottom_light_shadow:I = 0x7f0d0016

.field public static final tag_brushsize_seekbar_slot_left:I = 0x7f0d0011

.field public static final tag_brushsize_seekbar_slot_right:I = 0x7f0d0012

.field public static final tag_brushsize_seekbar_slot_top:I = 0x7f0d0013

.field public static final tag_brushsize_seekbar_slot_top_dark_shadow:I = 0x7f0d0017

.field public static final tag_brushsize_seekbar_slot_top_light_shadow:I = 0x7f0d0015

.field public static final tag_colorpicker_gap:I = 0x7f0d0027

.field public static final tag_colorpicker_margin:I = 0x7f0d0026

.field public static final tag_pen_dialog_allwidget_base_width:I = 0x7f0d0002

.field public static final tag_pen_dialog_brushsize_base_height:I = 0x7f0d0003

.field public static final tag_pen_dialog_brushsize_padding_left:I = 0x7f0d0007

.field public static final tag_pen_dialog_brushsize_padding_right:I = 0x7f0d0008

.field public static final tag_pen_dialog_brushsize_padding_top:I = 0x7f0d0009

.field public static final tag_pen_dialog_brushsize_seekbar_circle_radius:I = 0x7f0d000e

.field public static final tag_pen_dialog_brushsize_seekbar_circle_x:I = 0x7f0d000f

.field public static final tag_pen_dialog_brushsize_seekbar_circle_y:I = 0x7f0d0010

.field public static final tag_pen_dialog_brushsize_seekbar_height:I = 0x7f0d000b

.field public static final tag_pen_dialog_brushsize_seekbar_width:I = 0x7f0d000a

.field public static final tag_pen_dialog_brushsize_seekindicator_left:I = 0x7f0d000c

.field public static final tag_pen_dialog_brushsize_seekindicator_top:I = 0x7f0d000d

.field public static final tag_pen_dialog_colorpicker_base_height:I = 0x7f0d0004

.field public static final tag_pen_dialog_gradient_base_height:I = 0x7f0d0006

.field public static final tag_pen_dialog_gradient_base_width:I = 0x7f0d0005

.field public static final tag_pen_dialog_title_text_size:I = 0x7f0d0001

.field public static final tag_pen_settings_close_icon_size:I = 0x7f0d0000

.field public static final thumbnail_edge_padding:I = 0x7f0d0032

.field public static final thumbnail_first_blank:I = 0x7f0d0031

.field public static final thumbnail_fragment_height:I = 0x7f0d002b

.field public static final thumbnail_fragment_width:I = 0x7f0d002a

.field public static final thumbnail_height:I = 0x7f0d0029

.field public static final thumbnail_margin:I = 0x7f0d002d

.field public static final thumbnail_margin_bottom:I = 0x7f0d002e

.field public static final thumbnail_padding:I = 0x7f0d002f

.field public static final thumbnail_padding_bottom:I = 0x7f0d0030

.field public static final thumbnail_text_size:I = 0x7f0d002c

.field public static final thumbnail_width:I = 0x7f0d0028


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 125
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
