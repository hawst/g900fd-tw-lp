.class public Lcom/samsung/groupcast/GroupPlayManager$EXTRA;
.super Ljava/lang/Object;
.source "GroupPlayManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/groupcast/GroupPlayManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "EXTRA"
.end annotation


# static fields
.field public static final CAPABILITY_INFO:Ljava/lang/String; = "com.samsung.groupcast.extra.CAPABILITY_INFO"

.field public static final MSG_BUNDEL:Ljava/lang/String; = "com.samsung.groupcast.extra.MSG_BUNDEL"

.field public static final PACKAGE_NAME:Ljava/lang/String; = "com.samsung.groupcast.extra.PACKAGE_NAME"

.field public static final SCREEN_ID:Ljava/lang/String; = "com.samsung.groupcast.extra.SCREEN_ID"

.field public static final SESSION_PIN:Ljava/lang/String; = "com.samsung.groupcast.extra.SESSION_PIN"

.field public static final SESSION_SUMMARY:Ljava/lang/String; = "com.samsung.groupcast.extra.SESSION_SUMMARY"

.field public static final STATUS_INFO:Ljava/lang/String; = "com.samsung.groupcast.extra.STATUS_INFO"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
