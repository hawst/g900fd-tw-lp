.class public final enum Lcom/samsung/groupcast/GroupPlayManager$MessageType;
.super Ljava/lang/Enum;
.source "GroupPlayManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/groupcast/GroupPlayManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "MessageType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/groupcast/GroupPlayManager$MessageType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/groupcast/GroupPlayManager$MessageType;

.field public static final enum NETWORK_STATUS:Lcom/samsung/groupcast/GroupPlayManager$MessageType;

.field public static final enum REGISTER:Lcom/samsung/groupcast/GroupPlayManager$MessageType;

.field public static final enum SERVICE:Lcom/samsung/groupcast/GroupPlayManager$MessageType;

.field public static final enum UNREGISTER:Lcom/samsung/groupcast/GroupPlayManager$MessageType;

.field public static final enum UPDATE:Lcom/samsung/groupcast/GroupPlayManager$MessageType;


# instance fields
.field intent_string:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 57
    new-instance v0, Lcom/samsung/groupcast/GroupPlayManager$MessageType;

    const-string v1, "REGISTER"

    const-string v2, "com.samsung.groupcast.action.GP_REGISTER"

    invoke-direct {v0, v1, v3, v2}, Lcom/samsung/groupcast/GroupPlayManager$MessageType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/groupcast/GroupPlayManager$MessageType;->REGISTER:Lcom/samsung/groupcast/GroupPlayManager$MessageType;

    .line 58
    new-instance v0, Lcom/samsung/groupcast/GroupPlayManager$MessageType;

    const-string v1, "UPDATE"

    const-string v2, "com.samsung.groupcast.action.GP_UPDATE"

    invoke-direct {v0, v1, v4, v2}, Lcom/samsung/groupcast/GroupPlayManager$MessageType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/groupcast/GroupPlayManager$MessageType;->UPDATE:Lcom/samsung/groupcast/GroupPlayManager$MessageType;

    .line 59
    new-instance v0, Lcom/samsung/groupcast/GroupPlayManager$MessageType;

    const-string v1, "SERVICE"

    const-string v2, "com.samsung.groupcast.action.GP_SERVICE"

    invoke-direct {v0, v1, v5, v2}, Lcom/samsung/groupcast/GroupPlayManager$MessageType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/groupcast/GroupPlayManager$MessageType;->SERVICE:Lcom/samsung/groupcast/GroupPlayManager$MessageType;

    .line 60
    new-instance v0, Lcom/samsung/groupcast/GroupPlayManager$MessageType;

    const-string v1, "NETWORK_STATUS"

    const-string v2, "com.samsung.groupcast.action.GP_NETWORK_STATUS"

    invoke-direct {v0, v1, v6, v2}, Lcom/samsung/groupcast/GroupPlayManager$MessageType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/groupcast/GroupPlayManager$MessageType;->NETWORK_STATUS:Lcom/samsung/groupcast/GroupPlayManager$MessageType;

    .line 61
    new-instance v0, Lcom/samsung/groupcast/GroupPlayManager$MessageType;

    const-string v1, "UNREGISTER"

    const-string v2, "com.samsung.groupcast.action.GP_UNREGISTER"

    invoke-direct {v0, v1, v7, v2}, Lcom/samsung/groupcast/GroupPlayManager$MessageType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/groupcast/GroupPlayManager$MessageType;->UNREGISTER:Lcom/samsung/groupcast/GroupPlayManager$MessageType;

    .line 56
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/samsung/groupcast/GroupPlayManager$MessageType;

    sget-object v1, Lcom/samsung/groupcast/GroupPlayManager$MessageType;->REGISTER:Lcom/samsung/groupcast/GroupPlayManager$MessageType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/groupcast/GroupPlayManager$MessageType;->UPDATE:Lcom/samsung/groupcast/GroupPlayManager$MessageType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/groupcast/GroupPlayManager$MessageType;->SERVICE:Lcom/samsung/groupcast/GroupPlayManager$MessageType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/groupcast/GroupPlayManager$MessageType;->NETWORK_STATUS:Lcom/samsung/groupcast/GroupPlayManager$MessageType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/samsung/groupcast/GroupPlayManager$MessageType;->UNREGISTER:Lcom/samsung/groupcast/GroupPlayManager$MessageType;

    aput-object v1, v0, v7

    sput-object v0, Lcom/samsung/groupcast/GroupPlayManager$MessageType;->$VALUES:[Lcom/samsung/groupcast/GroupPlayManager$MessageType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 65
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 66
    iput-object p3, p0, Lcom/samsung/groupcast/GroupPlayManager$MessageType;->intent_string:Ljava/lang/String;

    .line 67
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/groupcast/GroupPlayManager$MessageType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 56
    const-class v0, Lcom/samsung/groupcast/GroupPlayManager$MessageType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/groupcast/GroupPlayManager$MessageType;

    return-object v0
.end method

.method public static values()[Lcom/samsung/groupcast/GroupPlayManager$MessageType;
    .locals 1

    .prologue
    .line 56
    sget-object v0, Lcom/samsung/groupcast/GroupPlayManager$MessageType;->$VALUES:[Lcom/samsung/groupcast/GroupPlayManager$MessageType;

    invoke-virtual {v0}, [Lcom/samsung/groupcast/GroupPlayManager$MessageType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/groupcast/GroupPlayManager$MessageType;

    return-object v0
.end method


# virtual methods
.method getString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/samsung/groupcast/GroupPlayManager$MessageType;->intent_string:Ljava/lang/String;

    return-object v0
.end method
