.class public Lcom/samsung/groupcast/GroupPlayManager;
.super Ljava/lang/Object;
.source "GroupPlayManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/groupcast/GroupPlayManager$MessageType;,
        Lcom/samsung/groupcast/GroupPlayManager$ExternalEventHandler;,
        Lcom/samsung/groupcast/GroupPlayManager$GroupPlayEventHandler;,
        Lcom/samsung/groupcast/GroupPlayManager$EXTRA;
    }
.end annotation


# static fields
.field public static final GP_NETWORK_STATUS:Ljava/lang/String; = "com.samsung.groupcast.action.GP_NETWORK_STATUS"

.field public static final GP_REGISTER:Ljava/lang/String; = "com.samsung.groupcast.action.GP_REGISTER"

.field public static final GP_SERVICE:Ljava/lang/String; = "com.samsung.groupcast.action.GP_SERVICE"

.field public static final GP_UNREGISTER:Ljava/lang/String; = "com.samsung.groupcast.action.GP_UNREGISTER"

.field public static final GP_UPDATE:Ljava/lang/String; = "com.samsung.groupcast.action.GP_UPDATE"

.field public static TAG_LOG:Ljava/lang/String;

.field public static TAG_PACKET:Ljava/lang/String;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mExternalActionFilter:Landroid/content/IntentFilter;

.field private mExternalActionReceiver:Landroid/content/BroadcastReceiver;

.field private mExternalEventHandler:Lcom/samsung/groupcast/GroupPlayManager$ExternalEventHandler;

.field private mGroupPlayActionFilter:Landroid/content/IntentFilter;

.field private mGroupPlayActionReceiver:Landroid/content/BroadcastReceiver;

.field private mGroupPlayEventHandler:Lcom/samsung/groupcast/GroupPlayManager$GroupPlayEventHandler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const-string v0, "GroupPlayBandpkt"

    sput-object v0, Lcom/samsung/groupcast/GroupPlayManager;->TAG_PACKET:Ljava/lang/String;

    .line 22
    const-string v0, "GroupPlayBandLog"

    sput-object v0, Lcom/samsung/groupcast/GroupPlayManager;->TAG_LOG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "c"    # Landroid/content/Context;

    .prologue
    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 77
    iput-object p1, p0, Lcom/samsung/groupcast/GroupPlayManager;->mContext:Landroid/content/Context;

    .line 79
    new-instance v0, Lcom/samsung/groupcast/GroupPlayManager$1;

    invoke-direct {v0, p0}, Lcom/samsung/groupcast/GroupPlayManager$1;-><init>(Lcom/samsung/groupcast/GroupPlayManager;)V

    iput-object v0, p0, Lcom/samsung/groupcast/GroupPlayManager;->mGroupPlayActionReceiver:Landroid/content/BroadcastReceiver;

    .line 98
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iput-object v0, p0, Lcom/samsung/groupcast/GroupPlayManager;->mGroupPlayActionFilter:Landroid/content/IntentFilter;

    .line 99
    iget-object v0, p0, Lcom/samsung/groupcast/GroupPlayManager;->mGroupPlayActionFilter:Landroid/content/IntentFilter;

    const-string v1, "com.samsung.groupcast.action.GP_NETWORK_STATUS"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 100
    iget-object v0, p0, Lcom/samsung/groupcast/GroupPlayManager;->mGroupPlayActionFilter:Landroid/content/IntentFilter;

    const-string v1, "com.samsung.groupcast.action.GP_SERVICE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 103
    new-instance v0, Lcom/samsung/groupcast/GroupPlayManager$2;

    invoke-direct {v0, p0}, Lcom/samsung/groupcast/GroupPlayManager$2;-><init>(Lcom/samsung/groupcast/GroupPlayManager;)V

    iput-object v0, p0, Lcom/samsung/groupcast/GroupPlayManager;->mExternalActionReceiver:Landroid/content/BroadcastReceiver;

    .line 135
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iput-object v0, p0, Lcom/samsung/groupcast/GroupPlayManager;->mExternalActionFilter:Landroid/content/IntentFilter;

    .line 136
    iget-object v0, p0, Lcom/samsung/groupcast/GroupPlayManager;->mExternalActionFilter:Landroid/content/IntentFilter;

    const-string v1, "com.samsung.groupcast.action.GP_REGISTER"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 137
    iget-object v0, p0, Lcom/samsung/groupcast/GroupPlayManager;->mExternalActionFilter:Landroid/content/IntentFilter;

    const-string v1, "com.samsung.groupcast.action.GP_UPDATE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 138
    iget-object v0, p0, Lcom/samsung/groupcast/GroupPlayManager;->mExternalActionFilter:Landroid/content/IntentFilter;

    const-string v1, "com.samsung.groupcast.action.GP_UNREGISTER"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 140
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/groupcast/GroupPlayManager;)Lcom/samsung/groupcast/GroupPlayManager$GroupPlayEventHandler;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/GroupPlayManager;

    .prologue
    .line 13
    iget-object v0, p0, Lcom/samsung/groupcast/GroupPlayManager;->mGroupPlayEventHandler:Lcom/samsung/groupcast/GroupPlayManager$GroupPlayEventHandler;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/groupcast/GroupPlayManager;)Lcom/samsung/groupcast/GroupPlayManager$ExternalEventHandler;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/GroupPlayManager;

    .prologue
    .line 13
    iget-object v0, p0, Lcom/samsung/groupcast/GroupPlayManager;->mExternalEventHandler:Lcom/samsung/groupcast/GroupPlayManager$ExternalEventHandler;

    return-object v0
.end method

.method private sendMessage(Lcom/samsung/groupcast/GroupPlayManager$MessageType;Landroid/os/Bundle;)V
    .locals 2
    .param p1, "type"    # Lcom/samsung/groupcast/GroupPlayManager$MessageType;
    .param p2, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 143
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p1}, Lcom/samsung/groupcast/GroupPlayManager$MessageType;->getString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 144
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.samsung.groupcast.extra.MSG_BUNDEL"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 145
    iget-object v1, p0, Lcom/samsung/groupcast/GroupPlayManager;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 146
    return-void
.end method


# virtual methods
.method public extRegisterListener(Lcom/samsung/groupcast/GroupPlayManager$ExternalEventHandler;)V
    .locals 3
    .param p1, "listen"    # Lcom/samsung/groupcast/GroupPlayManager$ExternalEventHandler;

    .prologue
    .line 190
    iput-object p1, p0, Lcom/samsung/groupcast/GroupPlayManager;->mExternalEventHandler:Lcom/samsung/groupcast/GroupPlayManager$ExternalEventHandler;

    .line 191
    iget-object v0, p0, Lcom/samsung/groupcast/GroupPlayManager;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/groupcast/GroupPlayManager;->mExternalActionReceiver:Landroid/content/BroadcastReceiver;

    iget-object v2, p0, Lcom/samsung/groupcast/GroupPlayManager;->mExternalActionFilter:Landroid/content/IntentFilter;

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 192
    return-void
.end method

.method public gpRegisterListener(Lcom/samsung/groupcast/GroupPlayManager$GroupPlayEventHandler;)V
    .locals 3
    .param p1, "listen"    # Lcom/samsung/groupcast/GroupPlayManager$GroupPlayEventHandler;

    .prologue
    .line 178
    iput-object p1, p0, Lcom/samsung/groupcast/GroupPlayManager;->mGroupPlayEventHandler:Lcom/samsung/groupcast/GroupPlayManager$GroupPlayEventHandler;

    .line 179
    iget-object v0, p0, Lcom/samsung/groupcast/GroupPlayManager;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/groupcast/GroupPlayManager;->mGroupPlayActionReceiver:Landroid/content/BroadcastReceiver;

    iget-object v2, p0, Lcom/samsung/groupcast/GroupPlayManager;->mGroupPlayActionFilter:Landroid/content/IntentFilter;

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 180
    return-void
.end method

.method public sendRegister(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "capability_info"    # Ljava/lang/String;

    .prologue
    .line 154
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 155
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v1, "com.samsung.groupcast.extra.PACKAGE_NAME"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 156
    const-string v1, "com.samsung.groupcast.extra.CAPABILITY_INFO"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 157
    sget-object v1, Lcom/samsung/groupcast/GroupPlayManager$MessageType;->REGISTER:Lcom/samsung/groupcast/GroupPlayManager$MessageType;

    invoke-direct {p0, v1, v0}, Lcom/samsung/groupcast/GroupPlayManager;->sendMessage(Lcom/samsung/groupcast/GroupPlayManager$MessageType;Landroid/os/Bundle;)V

    .line 159
    sget-object v1, Lcom/samsung/groupcast/GroupPlayManager;->TAG_LOG:Ljava/lang/String;

    const-string v2, "[EXT_APP]send : REGISTER"

    invoke-static {v1, v2}, Lcom/samsung/groupcast/application/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 160
    sget-object v1, Lcom/samsung/groupcast/GroupPlayManager;->TAG_PACKET:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "     PACKAGE_NAME :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/groupcast/application/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 161
    sget-object v1, Lcom/samsung/groupcast/GroupPlayManager;->TAG_PACKET:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "     CAPABILITY_INFO :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/groupcast/application/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 162
    return-void
.end method

.method public sendUnRegister()V
    .locals 2

    .prologue
    .line 149
    sget-object v0, Lcom/samsung/groupcast/GroupPlayManager;->TAG_LOG:Ljava/lang/String;

    const-string v1, "[EXT_APP]send : UNREGISTER"

    invoke-static {v0, v1}, Lcom/samsung/groupcast/application/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 150
    sget-object v0, Lcom/samsung/groupcast/GroupPlayManager$MessageType;->UNREGISTER:Lcom/samsung/groupcast/GroupPlayManager$MessageType;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/samsung/groupcast/GroupPlayManager;->sendMessage(Lcom/samsung/groupcast/GroupPlayManager$MessageType;Landroid/os/Bundle;)V

    .line 151
    return-void
.end method

.method public sendUpdate(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "status_info"    # Ljava/lang/String;
    .param p3, "screenId"    # Ljava/lang/String;

    .prologue
    .line 165
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 166
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v1, "com.samsung.groupcast.extra.PACKAGE_NAME"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 167
    const-string v1, "com.samsung.groupcast.extra.STATUS_INFO"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 168
    const-string v1, "com.samsung.groupcast.extra.SCREEN_ID"

    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 170
    sget-object v1, Lcom/samsung/groupcast/GroupPlayManager$MessageType;->UPDATE:Lcom/samsung/groupcast/GroupPlayManager$MessageType;

    invoke-direct {p0, v1, v0}, Lcom/samsung/groupcast/GroupPlayManager;->sendMessage(Lcom/samsung/groupcast/GroupPlayManager$MessageType;Landroid/os/Bundle;)V

    .line 171
    sget-object v1, Lcom/samsung/groupcast/GroupPlayManager;->TAG_LOG:Ljava/lang/String;

    const-string v2, "[EXT_APP]send : UPDATE"

    invoke-static {v1, v2}, Lcom/samsung/groupcast/application/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 172
    sget-object v1, Lcom/samsung/groupcast/GroupPlayManager;->TAG_PACKET:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "     PACKAGE_NAME :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/groupcast/application/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 173
    sget-object v1, Lcom/samsung/groupcast/GroupPlayManager;->TAG_PACKET:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "     STATUS_INFO :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/groupcast/application/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 174
    sget-object v1, Lcom/samsung/groupcast/GroupPlayManager;->TAG_PACKET:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "     SCREEN_ID :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/groupcast/application/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 175
    return-void
.end method

.method public unExtRegisterListener()V
    .locals 2

    .prologue
    .line 195
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/groupcast/GroupPlayManager;->mExternalEventHandler:Lcom/samsung/groupcast/GroupPlayManager$ExternalEventHandler;

    .line 197
    :try_start_0
    iget-object v0, p0, Lcom/samsung/groupcast/GroupPlayManager;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/groupcast/GroupPlayManager;->mExternalActionReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 199
    :goto_0
    return-void

    .line 198
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public unGpRegisterListener()V
    .locals 2

    .prologue
    .line 183
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/groupcast/GroupPlayManager;->mGroupPlayEventHandler:Lcom/samsung/groupcast/GroupPlayManager$GroupPlayEventHandler;

    .line 185
    :try_start_0
    iget-object v0, p0, Lcom/samsung/groupcast/GroupPlayManager;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/groupcast/GroupPlayManager;->mGroupPlayActionReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 187
    :goto_0
    return-void

    .line 186
    :catch_0
    move-exception v0

    goto :goto_0
.end method
