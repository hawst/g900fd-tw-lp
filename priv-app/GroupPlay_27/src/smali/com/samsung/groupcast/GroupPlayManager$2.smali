.class Lcom/samsung/groupcast/GroupPlayManager$2;
.super Landroid/content/BroadcastReceiver;
.source "GroupPlayManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/groupcast/GroupPlayManager;-><init>(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/groupcast/GroupPlayManager;


# direct methods
.method constructor <init>(Lcom/samsung/groupcast/GroupPlayManager;)V
    .locals 0

    .prologue
    .line 103
    iput-object p1, p0, Lcom/samsung/groupcast/GroupPlayManager$2;->this$0:Lcom/samsung/groupcast/GroupPlayManager;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7
    .param p1, "arg0"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 107
    iget-object v5, p0, Lcom/samsung/groupcast/GroupPlayManager$2;->this$0:Lcom/samsung/groupcast/GroupPlayManager;

    # getter for: Lcom/samsung/groupcast/GroupPlayManager;->mExternalEventHandler:Lcom/samsung/groupcast/GroupPlayManager$ExternalEventHandler;
    invoke-static {v5}, Lcom/samsung/groupcast/GroupPlayManager;->access$100(Lcom/samsung/groupcast/GroupPlayManager;)Lcom/samsung/groupcast/GroupPlayManager$ExternalEventHandler;

    move-result-object v5

    if-nez v5, :cond_1

    .line 108
    sget-object v5, Lcom/samsung/groupcast/GroupPlayManager;->TAG_LOG:Ljava/lang/String;

    const-string v6, "[GP]Error : ExtMessageManager.mExtListener is NULL"

    invoke-static {v5, v6}, Lcom/samsung/groupcast/application/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    :cond_0
    :goto_0
    return-void

    .line 113
    :cond_1
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    const-string v6, "com.samsung.groupcast.action.GP_REGISTER"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 114
    const-string v5, "com.samsung.groupcast.extra.MSG_BUNDEL"

    invoke-virtual {p2, v5}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    .line 115
    .local v1, "b":Landroid/os/Bundle;
    const-string v5, "com.samsung.groupcast.extra.PACKAGE_NAME"

    invoke-virtual {v1, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 116
    .local v2, "packageName":Ljava/lang/String;
    const-string v5, "com.samsung.groupcast.extra.CAPABILITY_INFO"

    invoke-virtual {v1, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 117
    .local v0, "Capability_info":Ljava/lang/String;
    iget-object v5, p0, Lcom/samsung/groupcast/GroupPlayManager$2;->this$0:Lcom/samsung/groupcast/GroupPlayManager;

    # getter for: Lcom/samsung/groupcast/GroupPlayManager;->mExternalEventHandler:Lcom/samsung/groupcast/GroupPlayManager$ExternalEventHandler;
    invoke-static {v5}, Lcom/samsung/groupcast/GroupPlayManager;->access$100(Lcom/samsung/groupcast/GroupPlayManager;)Lcom/samsung/groupcast/GroupPlayManager$ExternalEventHandler;

    move-result-object v5

    invoke-interface {v5, v2, v0}, Lcom/samsung/groupcast/GroupPlayManager$ExternalEventHandler;->onGpRegister(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 119
    .end local v0    # "Capability_info":Ljava/lang/String;
    .end local v1    # "b":Landroid/os/Bundle;
    .end local v2    # "packageName":Ljava/lang/String;
    :cond_2
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    const-string v6, "com.samsung.groupcast.action.GP_UPDATE"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 120
    const-string v5, "com.samsung.groupcast.extra.MSG_BUNDEL"

    invoke-virtual {p2, v5}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    .line 121
    .restart local v1    # "b":Landroid/os/Bundle;
    const-string v5, "com.samsung.groupcast.extra.PACKAGE_NAME"

    invoke-virtual {v1, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 122
    .restart local v2    # "packageName":Ljava/lang/String;
    const-string v5, "com.samsung.groupcast.extra.STATUS_INFO"

    invoke-virtual {v1, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 123
    .local v4, "status_info":Ljava/lang/String;
    const-string v5, "com.samsung.groupcast.extra.SCREEN_ID"

    invoke-virtual {v1, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 125
    .local v3, "screen_id":Ljava/lang/String;
    const-string v5, "GroupPlayBandLog"

    const-string v6, "[GP]receive : GP_UPDATE"

    invoke-static {v5, v6}, Lcom/samsung/groupcast/application/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 126
    iget-object v5, p0, Lcom/samsung/groupcast/GroupPlayManager$2;->this$0:Lcom/samsung/groupcast/GroupPlayManager;

    # getter for: Lcom/samsung/groupcast/GroupPlayManager;->mExternalEventHandler:Lcom/samsung/groupcast/GroupPlayManager$ExternalEventHandler;
    invoke-static {v5}, Lcom/samsung/groupcast/GroupPlayManager;->access$100(Lcom/samsung/groupcast/GroupPlayManager;)Lcom/samsung/groupcast/GroupPlayManager$ExternalEventHandler;

    move-result-object v5

    invoke-interface {v5, v2, v4, v3}, Lcom/samsung/groupcast/GroupPlayManager$ExternalEventHandler;->onGpUpdate(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 128
    .end local v1    # "b":Landroid/os/Bundle;
    .end local v2    # "packageName":Ljava/lang/String;
    .end local v3    # "screen_id":Ljava/lang/String;
    .end local v4    # "status_info":Ljava/lang/String;
    :cond_3
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    const-string v6, "com.samsung.groupcast.action.GP_UNREGISTER"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 129
    const-string v5, "GroupPlayBandLog"

    const-string v6, "[GP]receive : GP_UNREGISTER"

    invoke-static {v5, v6}, Lcom/samsung/groupcast/application/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 130
    iget-object v5, p0, Lcom/samsung/groupcast/GroupPlayManager$2;->this$0:Lcom/samsung/groupcast/GroupPlayManager;

    # getter for: Lcom/samsung/groupcast/GroupPlayManager;->mExternalEventHandler:Lcom/samsung/groupcast/GroupPlayManager$ExternalEventHandler;
    invoke-static {v5}, Lcom/samsung/groupcast/GroupPlayManager;->access$100(Lcom/samsung/groupcast/GroupPlayManager;)Lcom/samsung/groupcast/GroupPlayManager$ExternalEventHandler;

    move-result-object v5

    invoke-interface {v5}, Lcom/samsung/groupcast/GroupPlayManager$ExternalEventHandler;->onGpunRegister()V

    goto :goto_0
.end method
