.class Lcom/samsung/groupcast/ssp/SSPInterface;
.super Ljava/lang/Object;
.source "SSPInterface.java"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mService:Lcom/sec/spp/push/IPushClientService;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput-object v0, p0, Lcom/samsung/groupcast/ssp/SSPInterface;->mService:Lcom/sec/spp/push/IPushClientService;

    .line 16
    iput-object v0, p0, Lcom/samsung/groupcast/ssp/SSPInterface;->mContext:Landroid/content/Context;

    .line 26
    iput-object p1, p0, Lcom/samsung/groupcast/ssp/SSPInterface;->mContext:Landroid/content/Context;

    .line 27
    return-void
.end method

.method private Log(Ljava/lang/Object;Ljava/lang/String;)V
    .locals 3
    .param p1, "cls"    # Ljava/lang/Object;
    .param p2, "msg"    # Ljava/lang/String;

    .prologue
    .line 157
    const-string v0, "SPPApp"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 158
    return-void
.end method


# virtual methods
.method public bindService(Landroid/content/ServiceConnection;)V
    .locals 6
    .param p1, "svcConnection"    # Landroid/content/ServiceConnection;

    .prologue
    .line 32
    const-string v3, "onResume"

    invoke-direct {p0, p0, v3}, Lcom/samsung/groupcast/ssp/SSPInterface;->Log(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    iget-object v3, p0, Lcom/samsung/groupcast/ssp/SSPInterface;->mContext:Landroid/content/Context;

    new-instance v4, Landroid/content/Intent;

    const-string v5, "com.sec.spp.push.PUSH_CLIENT_SERVICE_ACTION"

    invoke-direct {v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    move-result-object v1

    .line 38
    .local v1, "cn":Landroid/content/ComponentName;
    if-eqz v1, :cond_0

    .line 39
    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.sec.spp.push.PUSH_CLIENT_SERVICE_ACTION"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 40
    .local v2, "intent":Landroid/content/Intent;
    iget-object v3, p0, Lcom/samsung/groupcast/ssp/SSPInterface;->mContext:Landroid/content/Context;

    const/4 v4, 0x1

    invoke-virtual {v3, v2, p1, v4}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v0

    .line 41
    .local v0, "bResult":Z
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "bindService(mPushClientConnection) requested. bResult="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, p0, v3}, Lcom/samsung/groupcast/ssp/SSPInterface;->Log(Ljava/lang/Object;Ljava/lang/String;)V

    .line 43
    .end local v0    # "bResult":Z
    .end local v2    # "intent":Landroid/content/Intent;
    :cond_0
    return-void
.end method

.method public deregistration(Ljava/lang/String;)V
    .locals 3
    .param p1, "appId"    # Ljava/lang/String;

    .prologue
    .line 72
    const-string v1, "deregistration()"

    invoke-direct {p0, p0, v1}, Lcom/samsung/groupcast/ssp/SSPInterface;->Log(Ljava/lang/Object;Ljava/lang/String;)V

    .line 74
    iget-object v1, p0, Lcom/samsung/groupcast/ssp/SSPInterface;->mService:Lcom/sec/spp/push/IPushClientService;

    if-eqz v1, :cond_0

    .line 76
    :try_start_0
    iget-object v1, p0, Lcom/samsung/groupcast/ssp/SSPInterface;->mService:Lcom/sec/spp/push/IPushClientService;

    invoke-interface {v1, p1}, Lcom/sec/spp/push/IPushClientService;->deregistration(Ljava/lang/String;)V

    .line 77
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mService.deregistration("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") requested"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, p0, v1}, Lcom/samsung/groupcast/ssp/SSPInterface;->Log(Ljava/lang/Object;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 85
    :goto_0
    return-void

    .line 78
    :catch_0
    move-exception v0

    .line 79
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    .line 80
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mService.deregistration() failed - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Landroid/os/RemoteException;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, p0, v1}, Lcom/samsung/groupcast/ssp/SSPInterface;->Log(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 83
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const-string v1, "mService is null"

    invoke-direct {p0, p0, v1}, Lcom/samsung/groupcast/ssp/SSPInterface;->Log(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getRegId(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "appId"    # Ljava/lang/String;

    .prologue
    .line 88
    const-string v2, "getRegId()"

    invoke-direct {p0, p0, v2}, Lcom/samsung/groupcast/ssp/SSPInterface;->Log(Ljava/lang/Object;Ljava/lang/String;)V

    .line 90
    iget-object v2, p0, Lcom/samsung/groupcast/ssp/SSPInterface;->mService:Lcom/sec/spp/push/IPushClientService;

    if-eqz v2, :cond_0

    .line 92
    :try_start_0
    iget-object v2, p0, Lcom/samsung/groupcast/ssp/SSPInterface;->mService:Lcom/sec/spp/push/IPushClientService;

    invoke-interface {v2, p1}, Lcom/sec/spp/push/IPushClientService;->getRegId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 93
    .local v1, "regId":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mService.getRegId("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") requested. regid : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, p0, v2}, Lcom/samsung/groupcast/ssp/SSPInterface;->Log(Ljava/lang/Object;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 103
    .end local v1    # "regId":Ljava/lang/String;
    :goto_0
    return-object v1

    .line 95
    :catch_0
    move-exception v0

    .line 96
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    .line 97
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mService.getRegId() failed - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/os/RemoteException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, p0, v2}, Lcom/samsung/groupcast/ssp/SSPInterface;->Log(Ljava/lang/Object;Ljava/lang/String;)V

    .line 103
    .end local v0    # "e":Landroid/os/RemoteException;
    :goto_1
    const/4 v1, 0x0

    goto :goto_0

    .line 100
    :cond_0
    const-string v2, "mService is null"

    invoke-direct {p0, p0, v2}, Lcom/samsung/groupcast/ssp/SSPInterface;->Log(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public getRegisteredAppIDs()[Ljava/lang/String;
    .locals 8

    .prologue
    .line 128
    const-string v6, "getRegisteredAppIDs()"

    invoke-direct {p0, p0, v6}, Lcom/samsung/groupcast/ssp/SSPInterface;->Log(Ljava/lang/Object;Ljava/lang/String;)V

    .line 129
    const/4 v5, 0x0

    .line 131
    .local v5, "mAppLists":[Ljava/lang/String;
    iget-object v6, p0, Lcom/samsung/groupcast/ssp/SSPInterface;->mService:Lcom/sec/spp/push/IPushClientService;

    if-eqz v6, :cond_3

    .line 133
    :try_start_0
    iget-object v6, p0, Lcom/samsung/groupcast/ssp/SSPInterface;->mService:Lcom/sec/spp/push/IPushClientService;

    invoke-interface {v6}, Lcom/sec/spp/push/IPushClientService;->getRegisteredAppIDs()[Ljava/lang/String;

    move-result-object v5

    .line 135
    if-eqz v5, :cond_0

    array-length v6, v5

    if-lez v6, :cond_0

    .line 136
    move-object v0, v5

    .local v0, "arr$":[Ljava/lang/String;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v4, v0, v2

    .line 137
    .local v4, "mApp":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Registered Application ID : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, p0, v6}, Lcom/samsung/groupcast/ssp/SSPInterface;->Log(Ljava/lang/Object;Ljava/lang/String;)V

    .line 136
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 139
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    .end local v4    # "mApp":Ljava/lang/String;
    :cond_0
    array-length v6, v5

    if-gtz v6, :cond_2

    .line 140
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Registered Application is empty - mAppLists.length="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    array-length v7, v5

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, p0, v6}, Lcom/samsung/groupcast/ssp/SSPInterface;->Log(Ljava/lang/Object;Ljava/lang/String;)V

    .line 153
    :cond_1
    :goto_1
    return-object v5

    .line 143
    :cond_2
    const-string v6, "Registered Application is empty - mAppLists is null "

    invoke-direct {p0, p0, v6}, Lcom/samsung/groupcast/ssp/SSPInterface;->Log(Ljava/lang/Object;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 145
    :catch_0
    move-exception v1

    .line 146
    .local v1, "e":Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    .line 147
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "getRegisteredAppIDs() failed - "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Landroid/os/RemoteException;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, p0, v6}, Lcom/samsung/groupcast/ssp/SSPInterface;->Log(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_1

    .line 150
    .end local v1    # "e":Landroid/os/RemoteException;
    :cond_3
    const-string v6, "mService is null"

    invoke-direct {p0, p0, v6}, Lcom/samsung/groupcast/ssp/SSPInterface;->Log(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public isPushAvailable()Z
    .locals 4

    .prologue
    .line 107
    const-string v2, "isPushAvailable()"

    invoke-direct {p0, p0, v2}, Lcom/samsung/groupcast/ssp/SSPInterface;->Log(Ljava/lang/Object;Ljava/lang/String;)V

    .line 109
    iget-object v2, p0, Lcom/samsung/groupcast/ssp/SSPInterface;->mService:Lcom/sec/spp/push/IPushClientService;

    if-eqz v2, :cond_0

    .line 111
    :try_start_0
    iget-object v2, p0, Lcom/samsung/groupcast/ssp/SSPInterface;->mService:Lcom/sec/spp/push/IPushClientService;

    invoke-interface {v2}, Lcom/sec/spp/push/IPushClientService;->isPushAvailable()Z

    move-result v0

    .line 112
    .local v0, "bPushAvailable":Z
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mService.isPushAvailable() requested. bPushAvailable : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, p0, v2}, Lcom/samsung/groupcast/ssp/SSPInterface;->Log(Ljava/lang/Object;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 123
    .end local v0    # "bPushAvailable":Z
    :goto_0
    return v0

    .line 115
    :catch_0
    move-exception v1

    .line 116
    .local v1, "e":Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    .line 117
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mService.isPushAvailable() failed - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Landroid/os/RemoteException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, p0, v2}, Lcom/samsung/groupcast/ssp/SSPInterface;->Log(Ljava/lang/Object;Ljava/lang/String;)V

    .line 123
    .end local v1    # "e":Landroid/os/RemoteException;
    :goto_1
    const/4 v0, 0x0

    goto :goto_0

    .line 120
    :cond_0
    const-string v2, "mService is null"

    invoke-direct {p0, p0, v2}, Lcom/samsung/groupcast/ssp/SSPInterface;->Log(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public registration(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "appId"    # Ljava/lang/String;
    .param p2, "userData"    # Ljava/lang/String;

    .prologue
    .line 55
    const-string v1, "registration()"

    invoke-direct {p0, p0, v1}, Lcom/samsung/groupcast/ssp/SSPInterface;->Log(Ljava/lang/Object;Ljava/lang/String;)V

    .line 57
    iget-object v1, p0, Lcom/samsung/groupcast/ssp/SSPInterface;->mService:Lcom/sec/spp/push/IPushClientService;

    if-eqz v1, :cond_0

    .line 59
    :try_start_0
    iget-object v1, p0, Lcom/samsung/groupcast/ssp/SSPInterface;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object p2

    .line 60
    iget-object v1, p0, Lcom/samsung/groupcast/ssp/SSPInterface;->mService:Lcom/sec/spp/push/IPushClientService;

    invoke-interface {v1, p1, p2}, Lcom/sec/spp/push/IPushClientService;->registration(Ljava/lang/String;Ljava/lang/String;)V

    .line 61
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mService.registration("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") requested"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, p0, v1}, Lcom/samsung/groupcast/ssp/SSPInterface;->Log(Ljava/lang/Object;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 69
    :goto_0
    return-void

    .line 62
    :catch_0
    move-exception v0

    .line 63
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    .line 64
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mService.registration() failed - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Landroid/os/RemoteException;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, p0, v1}, Lcom/samsung/groupcast/ssp/SSPInterface;->Log(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 67
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const-string v1, "mService is null"

    invoke-direct {p0, p0, v1}, Lcom/samsung/groupcast/ssp/SSPInterface;->Log(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setService(Lcom/sec/spp/push/IPushClientService;)V
    .locals 0
    .param p1, "svc"    # Lcom/sec/spp/push/IPushClientService;

    .prologue
    .line 20
    iput-object p1, p0, Lcom/samsung/groupcast/ssp/SSPInterface;->mService:Lcom/sec/spp/push/IPushClientService;

    .line 21
    return-void
.end method

.method public unbindService(Landroid/content/ServiceConnection;)V
    .locals 1
    .param p1, "svcConnection"    # Landroid/content/ServiceConnection;

    .prologue
    .line 46
    const-string v0, "onPause"

    invoke-direct {p0, p0, v0}, Lcom/samsung/groupcast/ssp/SSPInterface;->Log(Ljava/lang/Object;Ljava/lang/String;)V

    .line 47
    iget-object v0, p0, Lcom/samsung/groupcast/ssp/SSPInterface;->mService:Lcom/sec/spp/push/IPushClientService;

    if-eqz v0, :cond_0

    .line 48
    iget-object v0, p0, Lcom/samsung/groupcast/ssp/SSPInterface;->mContext:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 49
    const-string v0, "unbindService(mPushClientConnection) requested."

    invoke-direct {p0, p0, v0}, Lcom/samsung/groupcast/ssp/SSPInterface;->Log(Ljava/lang/Object;Ljava/lang/String;)V

    .line 51
    :cond_0
    return-void
.end method
