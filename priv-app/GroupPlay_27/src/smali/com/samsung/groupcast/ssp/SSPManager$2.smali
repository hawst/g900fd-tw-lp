.class Lcom/samsung/groupcast/ssp/SSPManager$2;
.super Landroid/content/BroadcastReceiver;
.source "SSPManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/groupcast/ssp/SSPManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/groupcast/ssp/SSPManager;


# direct methods
.method constructor <init>(Lcom/samsung/groupcast/ssp/SSPManager;)V
    .locals 0

    .prologue
    .line 56
    iput-object p1, p0, Lcom/samsung/groupcast/ssp/SSPManager$2;->this$0:Lcom/samsung/groupcast/ssp/SSPManager;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/16 v8, 0x3e8

    .line 59
    iget-object v5, p0, Lcom/samsung/groupcast/ssp/SSPManager$2;->this$0:Lcom/samsung/groupcast/ssp/SSPManager;

    const-string v6, "[BroadcastReceiver] onReceive"

    # invokes: Lcom/samsung/groupcast/ssp/SSPManager;->Log(Ljava/lang/Object;Ljava/lang/String;)V
    invoke-static {v5, p0, v6}, Lcom/samsung/groupcast/ssp/SSPManager;->access$100(Lcom/samsung/groupcast/ssp/SSPManager;Ljava/lang/Object;Ljava/lang/String;)V

    .line 61
    const/16 v2, 0x3e8

    .line 62
    .local v2, "errorCode":I
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 63
    .local v0, "action":Ljava/lang/String;
    iget-object v5, p0, Lcom/samsung/groupcast/ssp/SSPManager$2;->this$0:Lcom/samsung/groupcast/ssp/SSPManager;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "[BroadcastReceiver] action:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    # invokes: Lcom/samsung/groupcast/ssp/SSPManager;->Log(Ljava/lang/Object;Ljava/lang/String;)V
    invoke-static {v5, p0, v6}, Lcom/samsung/groupcast/ssp/SSPManager;->access$100(Lcom/samsung/groupcast/ssp/SSPManager;Ljava/lang/Object;Ljava/lang/String;)V

    .line 77
    const-string v5, "com.sec.spp.RegistrationChangedAction"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 79
    const-string v5, "appId"

    invoke-virtual {p2, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 80
    .local v1, "appId":Ljava/lang/String;
    iget-object v5, p0, Lcom/samsung/groupcast/ssp/SSPManager$2;->this$0:Lcom/samsung/groupcast/ssp/SSPManager;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "[BroadcastReceiver] appId:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    # invokes: Lcom/samsung/groupcast/ssp/SSPManager;->Log(Ljava/lang/Object;Ljava/lang/String;)V
    invoke-static {v5, p0, v6}, Lcom/samsung/groupcast/ssp/SSPManager;->access$100(Lcom/samsung/groupcast/ssp/SSPManager;Ljava/lang/Object;Ljava/lang/String;)V

    .line 83
    if-nez v1, :cond_1

    .line 84
    iget-object v5, p0, Lcom/samsung/groupcast/ssp/SSPManager$2;->this$0:Lcom/samsung/groupcast/ssp/SSPManager;

    const-string v6, "[BroadcastReceiver] appId==null"

    # invokes: Lcom/samsung/groupcast/ssp/SSPManager;->Log(Ljava/lang/Object;Ljava/lang/String;)V
    invoke-static {v5, p0, v6}, Lcom/samsung/groupcast/ssp/SSPManager;->access$100(Lcom/samsung/groupcast/ssp/SSPManager;Ljava/lang/Object;Ljava/lang/String;)V

    .line 131
    .end local v1    # "appId":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 87
    .restart local v1    # "appId":Ljava/lang/String;
    :cond_1
    const-string v5, "com.sec.spp.RegistrationFail"

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 88
    const-string v5, "Error"

    invoke-virtual {p2, v5, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 89
    iget-object v5, p0, Lcom/samsung/groupcast/ssp/SSPManager$2;->this$0:Lcom/samsung/groupcast/ssp/SSPManager;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "[BroadcastReceiver] PUSH_REGISTRATION_FAIL. appid is default :"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    # invokes: Lcom/samsung/groupcast/ssp/SSPManager;->Log(Ljava/lang/Object;Ljava/lang/String;)V
    invoke-static {v5, p0, v6}, Lcom/samsung/groupcast/ssp/SSPManager;->access$100(Lcom/samsung/groupcast/ssp/SSPManager;Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 93
    :cond_2
    const-string v5, "com.sec.spp.DeRegistrationFail"

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 94
    const-string v5, "Error"

    invoke-virtual {p2, v5, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 95
    iget-object v5, p0, Lcom/samsung/groupcast/ssp/SSPManager$2;->this$0:Lcom/samsung/groupcast/ssp/SSPManager;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "[BroadcastReceiver] PUSH_DEREGISTRATION_FAIL. appid is default :"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    # invokes: Lcom/samsung/groupcast/ssp/SSPManager;->Log(Ljava/lang/Object;Ljava/lang/String;)V
    invoke-static {v5, p0, v6}, Lcom/samsung/groupcast/ssp/SSPManager;->access$100(Lcom/samsung/groupcast/ssp/SSPManager;Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 99
    :cond_3
    const-string v5, "b6453db0bf7a2b3b"

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 100
    const-string v5, "com.sec.spp.Status"

    const/4 v6, 0x1

    invoke-virtual {p2, v5, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    .line 103
    .local v4, "registrationState":I
    iget-object v5, p0, Lcom/samsung/groupcast/ssp/SSPManager$2;->this$0:Lcom/samsung/groupcast/ssp/SSPManager;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "[BroadcastReceiver] registrationState:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    # invokes: Lcom/samsung/groupcast/ssp/SSPManager;->Log(Ljava/lang/Object;Ljava/lang/String;)V
    invoke-static {v5, p0, v6}, Lcom/samsung/groupcast/ssp/SSPManager;->access$100(Lcom/samsung/groupcast/ssp/SSPManager;Ljava/lang/Object;Ljava/lang/String;)V

    .line 106
    packed-switch v4, :pswitch_data_0

    goto :goto_0

    .line 108
    :pswitch_0
    const-string v5, "RegistrationID"

    invoke-virtual {p2, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 109
    .local v3, "intentRegId":Ljava/lang/String;
    iget-object v5, p0, Lcom/samsung/groupcast/ssp/SSPManager$2;->this$0:Lcom/samsung/groupcast/ssp/SSPManager;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "[BroadcastReceiver] PUSH_REGISTRATION_SUCCESS:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    # invokes: Lcom/samsung/groupcast/ssp/SSPManager;->Log(Ljava/lang/Object;Ljava/lang/String;)V
    invoke-static {v5, p0, v6}, Lcom/samsung/groupcast/ssp/SSPManager;->access$100(Lcom/samsung/groupcast/ssp/SSPManager;Ljava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 113
    .end local v3    # "intentRegId":Ljava/lang/String;
    :pswitch_1
    const-string v5, "Error"

    invoke-virtual {p2, v5, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 114
    iget-object v5, p0, Lcom/samsung/groupcast/ssp/SSPManager$2;->this$0:Lcom/samsung/groupcast/ssp/SSPManager;

    const-string v6, "[BroadcastReceiver] PUSH_REGISTRATION_FAIL"

    # invokes: Lcom/samsung/groupcast/ssp/SSPManager;->Log(Ljava/lang/Object;Ljava/lang/String;)V
    invoke-static {v5, p0, v6}, Lcom/samsung/groupcast/ssp/SSPManager;->access$100(Lcom/samsung/groupcast/ssp/SSPManager;Ljava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 117
    :pswitch_2
    iget-object v5, p0, Lcom/samsung/groupcast/ssp/SSPManager$2;->this$0:Lcom/samsung/groupcast/ssp/SSPManager;

    const-string v6, "[BroadcastReceiver] PUSH_DEREGISTRATION_SUCCESS"

    # invokes: Lcom/samsung/groupcast/ssp/SSPManager;->Log(Ljava/lang/Object;Ljava/lang/String;)V
    invoke-static {v5, p0, v6}, Lcom/samsung/groupcast/ssp/SSPManager;->access$100(Lcom/samsung/groupcast/ssp/SSPManager;Ljava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 120
    :pswitch_3
    const-string v5, "Error"

    invoke-virtual {p2, v5, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 121
    iget-object v5, p0, Lcom/samsung/groupcast/ssp/SSPManager$2;->this$0:Lcom/samsung/groupcast/ssp/SSPManager;

    const-string v6, "[BroadcastReceiver] PUSH_DEREGISTRATION_FAIL"

    # invokes: Lcom/samsung/groupcast/ssp/SSPManager;->Log(Ljava/lang/Object;Ljava/lang/String;)V
    invoke-static {v5, p0, v6}, Lcom/samsung/groupcast/ssp/SSPManager;->access$100(Lcom/samsung/groupcast/ssp/SSPManager;Ljava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 125
    .end local v1    # "appId":Ljava/lang/String;
    .end local v4    # "registrationState":I
    :cond_4
    const-string v5, "com.sec.spp.ServiceAbnormallyStoppedAction"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 126
    iget-object v5, p0, Lcom/samsung/groupcast/ssp/SSPManager$2;->this$0:Lcom/samsung/groupcast/ssp/SSPManager;

    const-string v6, "[BroadcastReceiver] received : SERVICE_ABNORMALLY_STOPPED_ACTION"

    # invokes: Lcom/samsung/groupcast/ssp/SSPManager;->Log(Ljava/lang/Object;Ljava/lang/String;)V
    invoke-static {v5, p0, v6}, Lcom/samsung/groupcast/ssp/SSPManager;->access$100(Lcom/samsung/groupcast/ssp/SSPManager;Ljava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 130
    :cond_5
    iget-object v5, p0, Lcom/samsung/groupcast/ssp/SSPManager$2;->this$0:Lcom/samsung/groupcast/ssp/SSPManager;

    const-string v6, "[BroadcastReceiver] action!=com.sec.spp.RegistrationChangedAction"

    # invokes: Lcom/samsung/groupcast/ssp/SSPManager;->Log(Ljava/lang/Object;Ljava/lang/String;)V
    invoke-static {v5, p0, v6}, Lcom/samsung/groupcast/ssp/SSPManager;->access$100(Lcom/samsung/groupcast/ssp/SSPManager;Ljava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 106
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
