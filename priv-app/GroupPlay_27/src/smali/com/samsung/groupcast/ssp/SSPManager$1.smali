.class Lcom/samsung/groupcast/ssp/SSPManager$1;
.super Ljava/lang/Object;
.source "SSPManager.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/groupcast/ssp/SSPManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/groupcast/ssp/SSPManager;


# direct methods
.method constructor <init>(Lcom/samsung/groupcast/ssp/SSPManager;)V
    .locals 0

    .prologue
    .line 33
    iput-object p1, p0, Lcom/samsung/groupcast/ssp/SSPManager$1;->this$0:Lcom/samsung/groupcast/ssp/SSPManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 4
    .param p1, "name"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 43
    const-string v2, "SPPM"

    const-string v3, "service connected"

    invoke-static {v2, v3}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 44
    invoke-static {p2}, Lcom/sec/spp/push/IPushClientService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/spp/push/IPushClientService;

    move-result-object v1

    .line 45
    .local v1, "serviceInstance":Lcom/sec/spp/push/IPushClientService;
    iget-object v2, p0, Lcom/samsung/groupcast/ssp/SSPManager$1;->this$0:Lcom/samsung/groupcast/ssp/SSPManager;

    # getter for: Lcom/samsung/groupcast/ssp/SSPManager;->mSSPInterface:Lcom/samsung/groupcast/ssp/SSPInterface;
    invoke-static {v2}, Lcom/samsung/groupcast/ssp/SSPManager;->access$000(Lcom/samsung/groupcast/ssp/SSPManager;)Lcom/samsung/groupcast/ssp/SSPInterface;

    move-result-object v2

    if-eqz v2, :cond_1

    if-eqz v1, :cond_1

    .line 46
    iget-object v2, p0, Lcom/samsung/groupcast/ssp/SSPManager$1;->this$0:Lcom/samsung/groupcast/ssp/SSPManager;

    # getter for: Lcom/samsung/groupcast/ssp/SSPManager;->mSSPInterface:Lcom/samsung/groupcast/ssp/SSPInterface;
    invoke-static {v2}, Lcom/samsung/groupcast/ssp/SSPManager;->access$000(Lcom/samsung/groupcast/ssp/SSPManager;)Lcom/samsung/groupcast/ssp/SSPInterface;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/samsung/groupcast/ssp/SSPInterface;->setService(Lcom/sec/spp/push/IPushClientService;)V

    .line 47
    iget-object v2, p0, Lcom/samsung/groupcast/ssp/SSPManager$1;->this$0:Lcom/samsung/groupcast/ssp/SSPManager;

    invoke-virtual {v2}, Lcom/samsung/groupcast/ssp/SSPManager;->getRegId()Ljava/lang/String;

    move-result-object v0

    .line 48
    .local v0, "regId":Ljava/lang/String;
    if-eqz v0, :cond_0

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 49
    :cond_0
    iget-object v2, p0, Lcom/samsung/groupcast/ssp/SSPManager$1;->this$0:Lcom/samsung/groupcast/ssp/SSPManager;

    invoke-virtual {v2}, Lcom/samsung/groupcast/ssp/SSPManager;->registration()V

    .line 52
    .end local v0    # "regId":Ljava/lang/String;
    :cond_1
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1, "name"    # Landroid/content/ComponentName;

    .prologue
    .line 37
    const-string v0, "SPPM"

    const-string v1, "service disconnected"

    invoke-static {v0, v1}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 39
    return-void
.end method
