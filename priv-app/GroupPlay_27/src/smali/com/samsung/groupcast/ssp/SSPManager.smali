.class public Lcom/samsung/groupcast/ssp/SSPManager;
.super Ljava/lang/Object;
.source "SSPManager.java"


# static fields
.field public static final SSP_APP_ID:Ljava/lang/String; = "b6453db0bf7a2b3b"

.field public static final SSP_APP_SCRTKEY:Ljava/lang/String; = "77eQo8kpNb+eah+DtsCbJpkaROs="

.field public static final SSP_PKG_NAME:Ljava/lang/String; = "com.sec.spp.push"

.field private static final TAG:Ljava/lang/String; = "SPPM"

.field private static sInstance:Lcom/samsung/groupcast/ssp/SSPManager;


# instance fields
.field private mConn:Landroid/content/ServiceConnection;

.field private mRegisterReceiver:Landroid/content/BroadcastReceiver;

.field private final mSSPInterface:Lcom/samsung/groupcast/ssp/SSPInterface;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    new-instance v0, Lcom/samsung/groupcast/ssp/SSPManager;

    invoke-direct {v0}, Lcom/samsung/groupcast/ssp/SSPManager;-><init>()V

    sput-object v0, Lcom/samsung/groupcast/ssp/SSPManager;->sInstance:Lcom/samsung/groupcast/ssp/SSPManager;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    .line 140
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    new-instance v0, Lcom/samsung/groupcast/ssp/SSPManager$1;

    invoke-direct {v0, p0}, Lcom/samsung/groupcast/ssp/SSPManager$1;-><init>(Lcom/samsung/groupcast/ssp/SSPManager;)V

    iput-object v0, p0, Lcom/samsung/groupcast/ssp/SSPManager;->mConn:Landroid/content/ServiceConnection;

    .line 56
    new-instance v0, Lcom/samsung/groupcast/ssp/SSPManager$2;

    invoke-direct {v0, p0}, Lcom/samsung/groupcast/ssp/SSPManager$2;-><init>(Lcom/samsung/groupcast/ssp/SSPManager;)V

    iput-object v0, p0, Lcom/samsung/groupcast/ssp/SSPManager;->mRegisterReceiver:Landroid/content/BroadcastReceiver;

    .line 141
    const-string v0, "SPPM"

    const-string v1, "SSPManager"

    invoke-static {v0, v1}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 142
    new-instance v0, Lcom/samsung/groupcast/ssp/SSPInterface;

    invoke-static {}, Lcom/samsung/groupcast/application/App;->getInstance()Lcom/samsung/groupcast/application/App;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/groupcast/ssp/SSPInterface;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/groupcast/ssp/SSPManager;->mSSPInterface:Lcom/samsung/groupcast/ssp/SSPInterface;

    .line 143
    return-void
.end method

.method private Log(Ljava/lang/Object;Ljava/lang/String;)V
    .locals 3
    .param p1, "cls"    # Ljava/lang/Object;
    .param p2, "msg"    # Ljava/lang/String;

    .prologue
    .line 183
    const-string v0, "SPPM"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 184
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/groupcast/ssp/SSPManager;)Lcom/samsung/groupcast/ssp/SSPInterface;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/ssp/SSPManager;

    .prologue
    .line 16
    iget-object v0, p0, Lcom/samsung/groupcast/ssp/SSPManager;->mSSPInterface:Lcom/samsung/groupcast/ssp/SSPInterface;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/groupcast/ssp/SSPManager;Ljava/lang/Object;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/groupcast/ssp/SSPManager;
    .param p1, "x1"    # Ljava/lang/Object;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 16
    invoke-direct {p0, p1, p2}, Lcom/samsung/groupcast/ssp/SSPManager;->Log(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public static getInstance()Lcom/samsung/groupcast/ssp/SSPManager;
    .locals 1

    .prologue
    .line 137
    sget-object v0, Lcom/samsung/groupcast/ssp/SSPManager;->sInstance:Lcom/samsung/groupcast/ssp/SSPManager;

    return-object v0
.end method


# virtual methods
.method public deregistration()V
    .locals 2

    .prologue
    .line 167
    const-string v0, "SPPM"

    const-string v1, "UNREG"

    invoke-static {v0, v1}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 168
    iget-object v0, p0, Lcom/samsung/groupcast/ssp/SSPManager;->mSSPInterface:Lcom/samsung/groupcast/ssp/SSPInterface;

    const-string v1, "b6453db0bf7a2b3b"

    invoke-virtual {v0, v1}, Lcom/samsung/groupcast/ssp/SSPInterface;->deregistration(Ljava/lang/String;)V

    .line 169
    return-void
.end method

.method public endService()V
    .locals 2

    .prologue
    .line 156
    const-string v0, "SPPM"

    const-string v1, "endSvc"

    invoke-static {v0, v1}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 157
    iget-object v0, p0, Lcom/samsung/groupcast/ssp/SSPManager;->mSSPInterface:Lcom/samsung/groupcast/ssp/SSPInterface;

    iget-object v1, p0, Lcom/samsung/groupcast/ssp/SSPManager;->mConn:Landroid/content/ServiceConnection;

    invoke-virtual {v0, v1}, Lcom/samsung/groupcast/ssp/SSPInterface;->unbindService(Landroid/content/ServiceConnection;)V

    .line 158
    return-void
.end method

.method public getRegAppIDs()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 178
    iget-object v0, p0, Lcom/samsung/groupcast/ssp/SSPManager;->mSSPInterface:Lcom/samsung/groupcast/ssp/SSPInterface;

    invoke-virtual {v0}, Lcom/samsung/groupcast/ssp/SSPInterface;->getRegisteredAppIDs()[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getRegId()Ljava/lang/String;
    .locals 4

    .prologue
    .line 172
    iget-object v1, p0, Lcom/samsung/groupcast/ssp/SSPManager;->mSSPInterface:Lcom/samsung/groupcast/ssp/SSPInterface;

    const-string v2, "b6453db0bf7a2b3b"

    invoke-virtual {v1, v2}, Lcom/samsung/groupcast/ssp/SSPInterface;->getRegId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 173
    .local v0, "regId":Ljava/lang/String;
    const-string v1, "SPPM"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "GETID:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 174
    return-object v0
.end method

.method public registration()V
    .locals 3

    .prologue
    .line 162
    const-string v0, "SPPM"

    const-string v1, "REG"

    invoke-static {v0, v1}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 163
    iget-object v0, p0, Lcom/samsung/groupcast/ssp/SSPManager;->mSSPInterface:Lcom/samsung/groupcast/ssp/SSPInterface;

    const-string v1, "b6453db0bf7a2b3b"

    const-string v2, "77eQo8kpNb+eah+DtsCbJpkaROs="

    invoke-virtual {v0, v1, v2}, Lcom/samsung/groupcast/ssp/SSPInterface;->registration(Ljava/lang/String;Ljava/lang/String;)V

    .line 164
    return-void
.end method

.method public startService()V
    .locals 2

    .prologue
    .line 148
    const-string v0, "SPPM"

    const-string v1, "startSVC"

    invoke-static {v0, v1}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 150
    const-string v0, "com.sec.spp.push"

    invoke-static {v0}, Lcom/samsung/groupcast/misc/utility/FileTools;->isInstalledApplication(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 151
    iget-object v0, p0, Lcom/samsung/groupcast/ssp/SSPManager;->mSSPInterface:Lcom/samsung/groupcast/ssp/SSPInterface;

    iget-object v1, p0, Lcom/samsung/groupcast/ssp/SSPManager;->mConn:Landroid/content/ServiceConnection;

    invoke-virtual {v0, v1}, Lcom/samsung/groupcast/ssp/SSPInterface;->bindService(Landroid/content/ServiceConnection;)V

    .line 153
    :cond_0
    return-void
.end method
