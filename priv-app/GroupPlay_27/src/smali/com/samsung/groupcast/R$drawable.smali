.class public final Lcom/samsung/groupcast/R$drawable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/groupcast/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "drawable"
.end annotation


# static fields
.field public static final action_bar_tab_button_selector_dark:I = 0x7f020000

.field public static final action_bar_tab_button_selector_light:I = 0x7f020001

.field public static final button_install:I = 0x7f020002

.field public static final create_group_button_selector_light:I = 0x7f020003

.field public static final custom_ratingbar:I = 0x7f020004

.field public static final custom_ratingbar_empty:I = 0x7f020005

.field public static final custom_ratingbar_filled:I = 0x7f020006

.field public static final group_play_start:I = 0x7f020007

.field public static final group_play_stub_left:I = 0x7f020008

.field public static final group_play_stub_right:I = 0x7f020009

.field public static final group_play_stub_right_no_cam:I = 0x7f02000a

.field public static final groupcast_action_more:I = 0x7f02000b

.field public static final groupcast_actionbar_icon_document:I = 0x7f02000c

.field public static final groupcast_arrow_left:I = 0x7f02000d

.field public static final groupcast_arrow_right:I = 0x7f02000e

.field public static final groupcast_bg_h:I = 0x7f02000f

.field public static final groupcast_bottom_divider_line:I = 0x7f020010

.field public static final groupcast_bottom_next_icon:I = 0x7f020011

.field public static final groupcast_category_line:I = 0x7f020012

.field public static final groupcast_main_apps:I = 0x7f020013

.field public static final groupcast_main_default:I = 0x7f020014

.field public static final groupcast_main_group:I = 0x7f020015

.field public static final groupcast_main_icon_star_off:I = 0x7f020016

.field public static final groupcast_main_icon_star_on:I = 0x7f020017

.field public static final groupcast_main_title_arrow:I = 0x7f020018

.field public static final groupcast_mainmenu_icon:I = 0x7f020019

.field public static final groupcast_menu_badge:I = 0x7f02001a

.field public static final groupcast_photo_bg:I = 0x7f02001b

.field public static final groupcast_photo_bg_selected:I = 0x7f02001c

.field public static final groupcast_refresh_icon:I = 0x7f02001d

.field public static final groupcast_setup_action_background:I = 0x7f02001e

.field public static final groupcast_welcome_page_lock_icon:I = 0x7f02001f

.field public static final groupplay_btn_01_renameicon:I = 0x7f020020

.field public static final groupplay_btn_02_disabled_focused_holo_light:I = 0x7f020021

.field public static final groupplay_btn_02_disabled_holo_light:I = 0x7f020022

.field public static final groupplay_btn_02_focused_holo_light:I = 0x7f020023

.field public static final groupplay_btn_02_normal_holo_light:I = 0x7f020024

.field public static final groupplay_btn_02_pressed_holo_light:I = 0x7f020025

.field public static final groupplay_btn_02_selected_holo_light:I = 0x7f020026

.field public static final groupplay_collage_disable:I = 0x7f020027

.field public static final groupplay_collage_normal:I = 0x7f020028

.field public static final groupplay_group_mode_disable:I = 0x7f020029

.field public static final groupplay_group_mode_normal:I = 0x7f02002a

.field public static final groupplay_icon:I = 0x7f02002b

.field public static final groupplay_info_disable:I = 0x7f02002c

.field public static final groupplay_info_normal:I = 0x7f02002d

.field public static final groupplay_members_disable:I = 0x7f02002e

.field public static final groupplay_members_normal:I = 0x7f02002f

.field public static final groupplay_noitem_apps:I = 0x7f020030

.field public static final groupplay_popup_bg:I = 0x7f020031

.field public static final groupplay_popup_in_bg:I = 0x7f020032

.field public static final home_menu_page_navi_focus:I = 0x7f020033

.field public static final home_menu_page_navi_normal:I = 0x7f020034

.field public static final home_menu_sum_page_navi_focus:I = 0x7f020035

.field public static final home_menu_sum_page_navi_normal:I = 0x7f020036

.field public static final main_sub_tab_selected_focused:I = 0x7f020037

.field public static final main_sub_tab_selected_focused_pressed:I = 0x7f020038

.field public static final option_popup_icon_help:I = 0x7f020039

.field public static final option_popup_icon_help_dim:I = 0x7f02003a

.field public static final option_popup_icon_help_disable:I = 0x7f02003b

.field public static final pen_color_focus:I = 0x7f02003c

.field public static final pen_color_focus_02:I = 0x7f02003d

.field public static final pen_color_shadow:I = 0x7f02003e

.field public static final popup_exit:I = 0x7f02003f

.field public static final popup_exit_press:I = 0x7f020040

.field public static final progress_bar_light:I = 0x7f020041

.field public static final progress_bg:I = 0x7f020042

.field public static final progress_handle:I = 0x7f020043

.field public static final progress_handle_press:I = 0x7f020044

.field public static final progress_interminate:I = 0x7f020045

.field public static final radio_collage_button_selector:I = 0x7f020046

.field public static final radio_collage_sum_button_selector:I = 0x7f020047

.field public static final snote_dialog_background:I = 0x7f020048

.field public static final snote_palette_background:I = 0x7f020049

.field public static final snote_preview_background:I = 0x7f02004a

.field public static final spen_brushsize_slider:I = 0x7f02004b

.field public static final stat_sys_group_play_ap:I = 0x7f02004c

.field public static final stub_cancel_btn_disabled:I = 0x7f02004d

.field public static final stub_cancel_btn_normal:I = 0x7f02004e

.field public static final stub_install_button_focused:I = 0x7f02004f

.field public static final stub_install_button_normal:I = 0x7f020050

.field public static final stub_install_button_pressed:I = 0x7f020051

.field public static final tablet_grid_view_item_selector_light:I = 0x7f020052

.field public static final tag_action_bar_icon_background_selector_dark:I = 0x7f020053

.field public static final tag_action_bar_icon_background_selector_light:I = 0x7f020054

.field public static final tag_action_bar_icon_group_mode_selector:I = 0x7f020055

.field public static final tag_grid_view_item_selector:I = 0x7f020056

.field public static final tag_menu_icon_collage_selector:I = 0x7f020057

.field public static final tag_menu_icon_group_mode_selector:I = 0x7f020058

.field public static final tag_menu_icon_help_selector:I = 0x7f020059

.field public static final tag_menu_icon_info_selector:I = 0x7f02005a

.field public static final tag_menu_icon_members_selector:I = 0x7f02005b

.field public static final tag_pen_dialog_exit:I = 0x7f02005c

.field public static final tag_thumbnail_loading:I = 0x7f02005d

.field public static final thumbnail_background:I = 0x7f02005e

.field public static final thumbnail_loading:I = 0x7f02005f

.field public static final tw_ab_transparent_dark_holo:I = 0x7f020060

.field public static final tw_ab_transparent_light_holo:I = 0x7f020061

.field public static final tw_action_bar_sub_tab_bg_01_holo_dark:I = 0x7f020062

.field public static final tw_action_bar_sub_tab_bg_01_holo_light:I = 0x7f020063

.field public static final tw_action_bar_sub_tab_bg_holo_dark:I = 0x7f020064

.field public static final tw_action_bar_sub_tab_bg_holo_light:I = 0x7f020065

.field public static final tw_action_bar_tab_selected_bg_holo_dark:I = 0x7f020066

.field public static final tw_action_bar_tab_selected_bg_holo_light:I = 0x7f020067

.field public static final tw_action_item_background_focused_holo_dark:I = 0x7f020068

.field public static final tw_action_item_background_pressed_holo_dark:I = 0x7f020069

.field public static final tw_action_item_background_selected_holo_dark:I = 0x7f02006a

.field public static final tw_dialog_full_holo_dark:I = 0x7f02006b

.field public static final tw_dialog_full_holo_light:I = 0x7f02006c

.field public static final tw_divider_ab_holo_dark:I = 0x7f02006d

.field public static final tw_divider_ab_holo_light:I = 0x7f02006e

.field public static final tw_divider_popup_horizontal_holo_dark:I = 0x7f02006f

.field public static final tw_divider_vertical_holo_light:I = 0x7f020070

.field public static final tw_ic_ab_back_holo_dark:I = 0x7f020071

.field public static final tw_list_disabled_focused_holo_dark:I = 0x7f020072

.field public static final tw_list_disabled_focused_holo_light:I = 0x7f020073

.field public static final tw_list_disabled_holo_dark:I = 0x7f020074

.field public static final tw_list_disabled_holo_light:I = 0x7f020075

.field public static final tw_list_divider_holo_light:I = 0x7f020076

.field public static final tw_list_focused_holo_dark:I = 0x7f020077

.field public static final tw_list_focused_holo_light:I = 0x7f020078

.field public static final tw_list_normal_holo_light:I = 0x7f020079

.field public static final tw_list_pressed_holo_dark:I = 0x7f02007a

.field public static final tw_list_pressed_holo_light:I = 0x7f02007b

.field public static final tw_list_scroll_holo_dark:I = 0x7f02007c

.field public static final tw_list_section_divider_holo_light:I = 0x7f02007d

.field public static final tw_list_selected_holo_dark:I = 0x7f02007e

.field public static final tw_list_selected_holo_light:I = 0x7f02007f

.field public static final tw_list_selector_disabled_holo_light:I = 0x7f020080

.field public static final tw_menu_dropdown_panel_holo_dark:I = 0x7f020081

.field public static final tw_menu_hardkey_panel_holo_dark:I = 0x7f020082

.field public static final tw_progress_primary_holo_light:I = 0x7f020083

.field public static final tw_progress_secondary_holo_light:I = 0x7f020084

.field public static final tw_progressbar_indeterminate1_holo_light:I = 0x7f020085

.field public static final tw_progressbar_indeterminate2_holo_light:I = 0x7f020086

.field public static final tw_progressbar_indeterminate3_holo_light:I = 0x7f020087

.field public static final tw_progressbar_indeterminate4_holo_light:I = 0x7f020088

.field public static final tw_tab_selected_focused_holo_dark:I = 0x7f020089

.field public static final tw_tab_selected_focused_holo_light:I = 0x7f02008a

.field public static final tw_tab_selected_focused_pressed_holo_dark:I = 0x7f02008b

.field public static final tw_tab_selected_focused_pressed_holo_light:I = 0x7f02008c

.field public static final tw_tab_selected_pressed_holo_dark:I = 0x7f02008d

.field public static final tw_tab_selected_pressed_holo_light:I = 0x7f02008e


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 220
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
