.class public interface abstract Lcom/samsung/groupcast/core/messaging/Channel;
.super Ljava/lang/Object;
.source "Channel.java"


# virtual methods
.method public abstract acceptFileOffer(Lcom/samsung/groupcast/core/messaging/FileOffer;)V
.end method

.method public abstract addListener(Lcom/samsung/groupcast/core/messaging/ChannelListener;)V
.end method

.method public abstract cancelAcceptedFile(Lcom/samsung/groupcast/core/messaging/DataKey;)V
.end method

.method public abstract getChannelId()Ljava/lang/String;
.end method

.method public abstract getListeners()Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/groupcast/core/messaging/ChannelListener;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getParticipantInfo(Ljava/lang/String;)Lcom/samsung/groupcast/legacy/gp2/session/model/ParticipantInfo;
.end method

.method public abstract getParticipantsIpInfo()Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getPeerCount()I
.end method

.method public abstract getPeers()Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getUserInfo()Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;
.end method

.method public abstract getUsersName()Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract hasFileOffer(Lcom/samsung/groupcast/core/messaging/DataKey;)I
.end method

.method public abstract leave()V
.end method

.method public abstract removeListener(Lcom/samsung/groupcast/core/messaging/ChannelListener;)V
.end method

.method public abstract sendFileIssueToNode(Ljava/lang/String;Lcom/samsung/groupcast/core/messaging/FileIssue;J)V
.end method

.method public abstract sendMessageToAll(Lcom/samsung/groupcast/core/messaging/Message;)V
.end method

.method public abstract sendMessageToAll(Lcom/samsung/groupcast/core/messaging/Message;Z)V
.end method

.method public abstract sendMessageToNode(Ljava/lang/String;Lcom/samsung/groupcast/core/messaging/Message;)V
.end method

.method public abstract sendMessageToNode(Ljava/lang/String;Lcom/samsung/groupcast/core/messaging/Message;Z)V
.end method

.method public abstract updateParticipants(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;[Ljava/lang/String;Z[Ljava/lang/String;)V
.end method
