.class public Lcom/samsung/groupcast/core/messaging/MessagingClient;
.super Ljava/lang/Object;
.source "MessagingClient.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/groupcast/core/messaging/MessagingClient$ChannelRecord;,
        Lcom/samsung/groupcast/core/messaging/MessagingClient$MAGNET_CONNECTION_TYPE;
    }
.end annotation


# static fields
.field private static final CHORD_CONNECTING_TIMEOUT:I = 0x7d0

.field public static final ChordIfIntentName:Ljava/lang/String; = "chordIfType"

.field public static DefaultInfaceName:Ljava/lang/String; = null

.field public static DefaultInterface:Lcom/samsung/groupcast/core/messaging/MessagingClient$MAGNET_CONNECTION_TYPE; = null

.field public static final INT_MAGNET_CONNECTION_TYPE_WIFI:I = 0x0

.field public static final INT_MAGNET_CONNECTION_TYPE_WIFIP2P:I = 0x1

.field public static final INT_MAGNET_CONNECTION_TYPE_WIFI_MF_OXYGEN:I = 0x2

.field private static final MAGNET_SHUTDOWN_WINDOW_MILLISECONDS:J = 0xbb8L

.field private static final TAG:Ljava/lang/String; = "MessagingClient"

.field public static final chordFilePath:Ljava/lang/String;

.field private static final sChord:Lcom/samsung/android/sdk/chord/Schord;

.field private static sChordMgr:Lcom/samsung/android/sdk/chord/SchordManager;

.field private static sMessagingClient:Lcom/samsung/groupcast/core/messaging/MessagingClient;


# instance fields
.field private final mChordHandler:Landroid/os/HandlerThread;

.field private mChordInterface:I

.field private mChordStarted:Z

.field private final mConnectionBlockQ:Ljava/util/concurrent/SynchronousQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/SynchronousQueue",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mConnections:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Lcom/samsung/groupcast/core/messaging/ConnectionHandle;",
            ">;"
        }
    .end annotation
.end field

.field private mDelayedShutdownRunnable:Ljava/lang/Runnable;

.field private final mHybridChannels:Ljava/util/Hashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/groupcast/core/messaging/HybridChannel;",
            ">;"
        }
    .end annotation
.end field

.field private final mIChordManagerListener:Lcom/samsung/android/sdk/chord/SchordManager$StatusListener;

.field private final mLocalChannels:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/groupcast/core/messaging/MessagingClient$ChannelRecord;",
            ">;"
        }
    .end annotation
.end field

.field private mWifiConnectivity:Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 34
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/Chord_GP"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/samsung/groupcast/core/messaging/MessagingClient;->chordFilePath:Ljava/lang/String;

    .line 64
    sget-object v1, Lcom/samsung/groupcast/core/messaging/MessagingClient$MAGNET_CONNECTION_TYPE;->WIFI:Lcom/samsung/groupcast/core/messaging/MessagingClient$MAGNET_CONNECTION_TYPE;

    sput-object v1, Lcom/samsung/groupcast/core/messaging/MessagingClient;->DefaultInterface:Lcom/samsung/groupcast/core/messaging/MessagingClient$MAGNET_CONNECTION_TYPE;

    .line 66
    const-string v1, "wlan0"

    sput-object v1, Lcom/samsung/groupcast/core/messaging/MessagingClient;->DefaultInfaceName:Ljava/lang/String;

    .line 71
    sput-object v3, Lcom/samsung/groupcast/core/messaging/MessagingClient;->sMessagingClient:Lcom/samsung/groupcast/core/messaging/MessagingClient;

    .line 81
    sput-object v3, Lcom/samsung/groupcast/core/messaging/MessagingClient;->sChordMgr:Lcom/samsung/android/sdk/chord/SchordManager;

    .line 83
    new-instance v1, Lcom/samsung/android/sdk/chord/Schord;

    invoke-direct {v1}, Lcom/samsung/android/sdk/chord/Schord;-><init>()V

    sput-object v1, Lcom/samsung/groupcast/core/messaging/MessagingClient;->sChord:Lcom/samsung/android/sdk/chord/Schord;

    .line 86
    :try_start_0
    sget-object v1, Lcom/samsung/groupcast/core/messaging/MessagingClient;->sChord:Lcom/samsung/android/sdk/chord/Schord;

    invoke-static {}, Lcom/samsung/groupcast/application/App;->getInstance()Lcom/samsung/groupcast/application/App;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/chord/Schord;->initialize(Landroid/content/Context;)V
    :try_end_0
    .catch Lcom/samsung/android/sdk/SsdkUnsupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 90
    .local v0, "e":Lcom/samsung/android/sdk/SsdkUnsupportedException;
    :goto_0
    return-void

    .line 87
    .end local v0    # "e":Lcom/samsung/android/sdk/SsdkUnsupportedException;
    :catch_0
    move-exception v0

    .line 88
    .restart local v0    # "e":Lcom/samsung/android/sdk/SsdkUnsupportedException;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/SsdkUnsupportedException;->printStackTrace()V

    goto :goto_0
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 218
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    new-instance v0, Ljava/util/concurrent/SynchronousQueue;

    invoke-direct {v0}, Ljava/util/concurrent/SynchronousQueue;-><init>()V

    iput-object v0, p0, Lcom/samsung/groupcast/core/messaging/MessagingClient;->mConnectionBlockQ:Ljava/util/concurrent/SynchronousQueue;

    .line 75
    iput v1, p0, Lcom/samsung/groupcast/core/messaging/MessagingClient;->mChordInterface:I

    .line 79
    iput-boolean v1, p0, Lcom/samsung/groupcast/core/messaging/MessagingClient;->mChordStarted:Z

    .line 82
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "GP_CHORD_T"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/samsung/groupcast/core/messaging/MessagingClient;->mChordHandler:Landroid/os/HandlerThread;

    .line 92
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/groupcast/core/messaging/MessagingClient;->mLocalChannels:Ljava/util/HashMap;

    .line 94
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/samsung/groupcast/core/messaging/MessagingClient;->mConnections:Ljava/util/HashSet;

    .line 95
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lcom/samsung/groupcast/core/messaging/MessagingClient;->mHybridChannels:Ljava/util/Hashtable;

    .line 110
    new-instance v0, Lcom/samsung/groupcast/core/messaging/MessagingClient$1;

    invoke-direct {v0, p0}, Lcom/samsung/groupcast/core/messaging/MessagingClient$1;-><init>(Lcom/samsung/groupcast/core/messaging/MessagingClient;)V

    iput-object v0, p0, Lcom/samsung/groupcast/core/messaging/MessagingClient;->mIChordManagerListener:Lcom/samsung/android/sdk/chord/SchordManager$StatusListener;

    .line 222
    iget-object v0, p0, Lcom/samsung/groupcast/core/messaging/MessagingClient;->mChordHandler:Landroid/os/HandlerThread;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/HandlerThread;->setDaemon(Z)V

    .line 223
    iget-object v0, p0, Lcom/samsung/groupcast/core/messaging/MessagingClient;->mChordHandler:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 225
    const-string v0, ""

    invoke-static {v0}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 226
    return-void
.end method

.method public static declared-synchronized GetInstance()Lcom/samsung/groupcast/core/messaging/MessagingClient;
    .locals 4

    .prologue
    .line 203
    const-class v1, Lcom/samsung/groupcast/core/messaging/MessagingClient;

    monitor-enter v1

    :try_start_0
    const-string v0, "MessagingClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Getinstance:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/samsung/groupcast/core/messaging/MessagingClient;->sMessagingClient:Lcom/samsung/groupcast/core/messaging/MessagingClient;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 204
    sget-object v0, Lcom/samsung/groupcast/core/messaging/MessagingClient;->sMessagingClient:Lcom/samsung/groupcast/core/messaging/MessagingClient;

    if-nez v0, :cond_0

    .line 205
    new-instance v0, Lcom/samsung/groupcast/core/messaging/MessagingClient;

    invoke-direct {v0}, Lcom/samsung/groupcast/core/messaging/MessagingClient;-><init>()V

    sput-object v0, Lcom/samsung/groupcast/core/messaging/MessagingClient;->sMessagingClient:Lcom/samsung/groupcast/core/messaging/MessagingClient;

    .line 207
    :cond_0
    sget-object v0, Lcom/samsung/groupcast/core/messaging/MessagingClient;->sMessagingClient:Lcom/samsung/groupcast/core/messaging/MessagingClient;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 203
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic access$000(Lcom/samsung/groupcast/core/messaging/MessagingClient;)Ljava/util/concurrent/SynchronousQueue;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/core/messaging/MessagingClient;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/samsung/groupcast/core/messaging/MessagingClient;->mConnectionBlockQ:Ljava/util/concurrent/SynchronousQueue;

    return-object v0
.end method

.method static synthetic access$102(Lcom/samsung/groupcast/core/messaging/MessagingClient;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/groupcast/core/messaging/MessagingClient;
    .param p1, "x1"    # Z

    .prologue
    .line 27
    iput-boolean p1, p0, Lcom/samsung/groupcast/core/messaging/MessagingClient;->mChordStarted:Z

    return p1
.end method

.method static synthetic access$200(Lcom/samsung/groupcast/core/messaging/MessagingClient;)Lcom/samsung/android/sdk/chord/SchordManager;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/core/messaging/MessagingClient;

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/samsung/groupcast/core/messaging/MessagingClient;->getChordMgrInternal()Lcom/samsung/android/sdk/chord/SchordManager;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/groupcast/core/messaging/MessagingClient;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/groupcast/core/messaging/MessagingClient;

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/samsung/groupcast/core/messaging/MessagingClient;->endChordInternal()V

    return-void
.end method

.method static synthetic access$400(Lcom/samsung/groupcast/core/messaging/MessagingClient;)Ljava/util/HashMap;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/core/messaging/MessagingClient;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/samsung/groupcast/core/messaging/MessagingClient;->mLocalChannels:Ljava/util/HashMap;

    return-object v0
.end method

.method public static checkChordDir()V
    .locals 3

    .prologue
    .line 345
    invoke-static {}, Lcom/samsung/groupcast/core/messaging/MessagingClient;->getChordMgr()Lcom/samsung/android/sdk/chord/SchordManager;

    move-result-object v1

    sget-object v2, Lcom/samsung/groupcast/core/messaging/MessagingClient;->chordFilePath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/chord/SchordManager;->setTempDirectory(Ljava/lang/String;)V

    .line 346
    new-instance v0, Ljava/io/File;

    sget-object v1, Lcom/samsung/groupcast/core/messaging/MessagingClient;->chordFilePath:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 347
    .local v0, "f":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    .line 348
    const-string v1, "MessagingClient"

    const-string v2, "chord dir doesn\'t exist"

    invoke-static {v1, v2}, Lcom/samsung/groupcast/application/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 349
    invoke-virtual {v0}, Ljava/io/File;->mkdir()Z

    .line 351
    :cond_0
    return-void
.end method

.method private endChordInternal()V
    .locals 2

    .prologue
    .line 192
    monitor-enter p0

    .line 193
    :try_start_0
    sget-object v0, Lcom/samsung/groupcast/core/messaging/MessagingClient;->sChordMgr:Lcom/samsung/android/sdk/chord/SchordManager;

    if-nez v0, :cond_0

    .line 194
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "NULL chordMgr:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/samsung/groupcast/core/messaging/MessagingClient;->sChordMgr:Lcom/samsung/android/sdk/chord/SchordManager;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 199
    :goto_0
    monitor-exit p0

    .line 200
    return-void

    .line 196
    :cond_0
    sget-object v0, Lcom/samsung/groupcast/core/messaging/MessagingClient;->sChordMgr:Lcom/samsung/android/sdk/chord/SchordManager;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/chord/SchordManager;->close()V

    .line 197
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/groupcast/core/messaging/MessagingClient;->sChordMgr:Lcom/samsung/android/sdk/chord/SchordManager;

    goto :goto_0

    .line 199
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static getChordMgr()Lcom/samsung/android/sdk/chord/SchordManager;
    .locals 1

    .prologue
    .line 159
    invoke-static {}, Lcom/samsung/groupcast/core/messaging/MessagingClient;->GetInstance()Lcom/samsung/groupcast/core/messaging/MessagingClient;

    move-result-object v0

    invoke-direct {v0}, Lcom/samsung/groupcast/core/messaging/MessagingClient;->getChordMgrInternal()Lcom/samsung/android/sdk/chord/SchordManager;

    move-result-object v0

    return-object v0
.end method

.method private getChordMgrInternal()Lcom/samsung/android/sdk/chord/SchordManager;
    .locals 3

    .prologue
    .line 163
    monitor-enter p0

    .line 164
    :try_start_0
    sget-object v1, Lcom/samsung/groupcast/core/messaging/MessagingClient;->sChordMgr:Lcom/samsung/android/sdk/chord/SchordManager;

    if-nez v1, :cond_0

    .line 165
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "init chordMgr:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/samsung/groupcast/core/messaging/MessagingClient;->sChordMgr:Lcom/samsung/android/sdk/chord/SchordManager;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 168
    :try_start_1
    new-instance v1, Lcom/samsung/android/sdk/chord/SchordManager;

    invoke-static {}, Lcom/samsung/groupcast/application/App;->getInstance()Lcom/samsung/groupcast/application/App;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/android/sdk/chord/SchordManager;-><init>(Landroid/content/Context;)V

    sput-object v1, Lcom/samsung/groupcast/core/messaging/MessagingClient;->sChordMgr:Lcom/samsung/android/sdk/chord/SchordManager;

    .line 170
    sget-object v1, Lcom/samsung/groupcast/core/messaging/MessagingClient;->sChordMgr:Lcom/samsung/android/sdk/chord/SchordManager;

    sget-object v2, Lcom/samsung/groupcast/core/messaging/MessagingClient;->chordFilePath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/chord/SchordManager;->setTempDirectory(Ljava/lang/String;)V

    .line 171
    sget-object v1, Lcom/samsung/groupcast/core/messaging/MessagingClient;->sChordMgr:Lcom/samsung/android/sdk/chord/SchordManager;

    iget-object v2, p0, Lcom/samsung/groupcast/core/messaging/MessagingClient;->mChordHandler:Landroid/os/HandlerThread;

    invoke-virtual {v2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/chord/SchordManager;->setLooper(Landroid/os/Looper;)V

    .line 174
    sget-object v1, Lcom/samsung/groupcast/core/messaging/MessagingClient;->sChordMgr:Lcom/samsung/android/sdk/chord/SchordManager;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/chord/SchordManager;->setSecureModeEnabled(Z)V

    .line 178
    sget-object v1, Lcom/samsung/groupcast/core/messaging/MessagingClient;->sChordMgr:Lcom/samsung/android/sdk/chord/SchordManager;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/chord/SchordManager;->setSmartDiscoveryEnabled(Z)V

    .line 179
    sget-object v1, Lcom/samsung/groupcast/core/messaging/MessagingClient;->sChordMgr:Lcom/samsung/android/sdk/chord/SchordManager;

    const v2, 0x84d0

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/chord/SchordManager;->setNodeKeepAliveTimeout(I)V

    .line 181
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "init chordMgr:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/samsung/groupcast/core/messaging/MessagingClient;->sChordMgr:Lcom/samsung/android/sdk/chord/SchordManager;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 186
    :cond_0
    :goto_0
    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 187
    sget-object v1, Lcom/samsung/groupcast/core/messaging/MessagingClient;->sChordMgr:Lcom/samsung/android/sdk/chord/SchordManager;

    return-object v1

    .line 182
    :catch_0
    move-exception v0

    .line 183
    .local v0, "e":Ljava/lang/Exception;
    :try_start_3
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 186
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v1
.end method

.method public static releaseChordDir()V
    .locals 2

    .prologue
    .line 211
    new-instance v0, Ljava/io/File;

    sget-object v1, Lcom/samsung/groupcast/core/messaging/MessagingClient;->chordFilePath:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 212
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 213
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 215
    :cond_0
    return-void
.end method


# virtual methods
.method public acquireConnection(Ljava/lang/String;)Lcom/samsung/groupcast/core/messaging/ConnectionHandle;
    .locals 1
    .param p1, "requesterId"    # Ljava/lang/String;

    .prologue
    .line 337
    sget-object v0, Lcom/samsung/groupcast/core/messaging/MessagingClient;->DefaultInterface:Lcom/samsung/groupcast/core/messaging/MessagingClient$MAGNET_CONNECTION_TYPE;

    invoke-virtual {p0, p1, v0}, Lcom/samsung/groupcast/core/messaging/MessagingClient;->acquireConnection(Ljava/lang/String;Lcom/samsung/groupcast/core/messaging/MessagingClient$MAGNET_CONNECTION_TYPE;)Lcom/samsung/groupcast/core/messaging/ConnectionHandle;

    move-result-object v0

    return-object v0
.end method

.method public declared-synchronized acquireConnection(Ljava/lang/String;Lcom/samsung/groupcast/core/messaging/MessagingClient$MAGNET_CONNECTION_TYPE;)Lcom/samsung/groupcast/core/messaging/ConnectionHandle;
    .locals 13
    .param p1, "requesterId"    # Ljava/lang/String;
    .param p2, "interfaceType"    # Lcom/samsung/groupcast/core/messaging/MessagingClient$MAGNET_CONNECTION_TYPE;

    .prologue
    const/4 v12, 0x1

    .line 357
    monitor-enter p0

    :try_start_0
    new-instance v0, Lcom/samsung/groupcast/core/messaging/ConnectionHandle;

    invoke-direct {v0, p1}, Lcom/samsung/groupcast/core/messaging/ConnectionHandle;-><init>(Ljava/lang/String;)V

    .line 358
    .local v0, "connection":Lcom/samsung/groupcast/core/messaging/ConnectionHandle;
    iget-object v6, p0, Lcom/samsung/groupcast/core/messaging/MessagingClient;->mConnections:Ljava/util/HashSet;

    invoke-virtual {v6, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 359
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    const-string v7, "acquireConnection"

    const/4 v8, 0x0

    const/4 v9, 0x4

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    const-string v11, "requesterId"

    aput-object v11, v9, v10

    const/4 v10, 0x1

    aput-object p1, v9, v10

    const/4 v10, 0x2

    const-string v11, "connections"

    aput-object v11, v9, v10

    const/4 v10, 0x3

    iget-object v11, p0, Lcom/samsung/groupcast/core/messaging/MessagingClient;->mConnections:Ljava/util/HashSet;

    aput-object v11, v9, v10

    invoke-static {v6, v7, v8, v9}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 361
    const-string v6, "MessagingClient"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "acquireConnection req  isChordStarted:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p0}, Lcom/samsung/groupcast/core/messaging/MessagingClient;->isChordStarted()Z

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 362
    invoke-virtual {p0}, Lcom/samsung/groupcast/core/messaging/MessagingClient;->isChordStarted()Z

    move-result v6

    if-nez v6, :cond_1

    .line 364
    const/4 v5, 0x0

    .line 366
    .local v5, "isExceptionOccurred":Z
    const/4 v6, 0x0

    iput v6, p0, Lcom/samsung/groupcast/core/messaging/MessagingClient;->mChordInterface:I

    .line 369
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    const-string v7, "acquireConnection"

    const-string v8, "starting magnet"

    invoke-static {v6, v7, v8}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;)V

    .line 371
    sget-object v6, Lcom/samsung/groupcast/core/messaging/MessagingClient$MAGNET_CONNECTION_TYPE;->WIFIP2P:Lcom/samsung/groupcast/core/messaging/MessagingClient$MAGNET_CONNECTION_TYPE;

    invoke-virtual {v6, p2}, Lcom/samsung/groupcast/core/messaging/MessagingClient$MAGNET_CONNECTION_TYPE;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 373
    const-string v6, "p2p-wlan0-0"

    sput-object v6, Lcom/samsung/groupcast/core/messaging/MessagingClient;->DefaultInfaceName:Ljava/lang/String;

    .line 375
    const/4 v6, 0x1

    iput v6, p0, Lcom/samsung/groupcast/core/messaging/MessagingClient;->mChordInterface:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 407
    :cond_0
    :goto_0
    const/4 v5, 0x0

    .line 410
    :try_start_1
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "real mChordInterface!!! >"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p0, Lcom/samsung/groupcast/core/messaging/MessagingClient;->mChordInterface:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 411
    invoke-static {}, Lcom/samsung/groupcast/core/messaging/MessagingClient;->checkChordDir()V

    .line 413
    iget-object v6, p0, Lcom/samsung/groupcast/core/messaging/MessagingClient;->mConnectionBlockQ:Ljava/util/concurrent/SynchronousQueue;

    invoke-virtual {v6}, Ljava/util/concurrent/SynchronousQueue;->clear()V

    .line 414
    invoke-direct {p0}, Lcom/samsung/groupcast/core/messaging/MessagingClient;->getChordMgrInternal()Lcom/samsung/android/sdk/chord/SchordManager;

    move-result-object v6

    iget v7, p0, Lcom/samsung/groupcast/core/messaging/MessagingClient;->mChordInterface:I

    iget-object v8, p0, Lcom/samsung/groupcast/core/messaging/MessagingClient;->mIChordManagerListener:Lcom/samsung/android/sdk/chord/SchordManager$StatusListener;

    invoke-virtual {v6, v7, v8}, Lcom/samsung/android/sdk/chord/SchordManager;->start(ILcom/samsung/android/sdk/chord/SchordManager$StatusListener;)V

    .line 415
    const-string v6, "chord start to wait"

    invoke-static {v6}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 416
    iget-object v6, p0, Lcom/samsung/groupcast/core/messaging/MessagingClient;->mConnectionBlockQ:Ljava/util/concurrent/SynchronousQueue;

    const-wide/16 v7, 0x7d0

    sget-object v9, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v6, v7, v8, v9}, Ljava/util/concurrent/SynchronousQueue;->poll(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object v6

    if-nez v6, :cond_8

    .line 417
    const-string v6, "chord waiting timed out"

    invoke-static {v6}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 420
    new-instance v6, Ljava/lang/Exception;

    invoke-direct {v6}, Ljava/lang/Exception;-><init>()V

    throw v6
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 423
    :catch_0
    move-exception v1

    .line 424
    .local v1, "e":Ljava/lang/Exception;
    const/4 v5, 0x1

    .line 425
    :try_start_2
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 428
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_1
    if-nez v5, :cond_9

    .line 429
    const-string v6, "connected no error!!"

    invoke-static {v6}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 440
    .end local v5    # "isExceptionOccurred":Z
    :cond_1
    :goto_2
    iget-object v6, p0, Lcom/samsung/groupcast/core/messaging/MessagingClient;->mDelayedShutdownRunnable:Ljava/lang/Runnable;

    if-eqz v6, :cond_2

    .line 441
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    const-string v7, "acquireConnection"

    const-string v8, "cancelling scheduled magnet shutdown"

    invoke-static {v6, v7, v8}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;)V

    .line 442
    iget-object v6, p0, Lcom/samsung/groupcast/core/messaging/MessagingClient;->mDelayedShutdownRunnable:Ljava/lang/Runnable;

    invoke-static {v6}, Lcom/samsung/groupcast/application/MainQueue;->cancel(Ljava/lang/Runnable;)V

    .line 443
    const/4 v6, 0x0

    iput-object v6, p0, Lcom/samsung/groupcast/core/messaging/MessagingClient;->mDelayedShutdownRunnable:Ljava/lang/Runnable;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 446
    :cond_2
    monitor-exit p0

    return-object v0

    .line 377
    .restart local v5    # "isExceptionOccurred":Z
    :cond_3
    :try_start_3
    sget-object v6, Lcom/samsung/groupcast/core/messaging/MessagingClient$MAGNET_CONNECTION_TYPE;->WIFI_MF_OXYZEN:Lcom/samsung/groupcast/core/messaging/MessagingClient$MAGNET_CONNECTION_TYPE;

    invoke-virtual {v6, p2}, Lcom/samsung/groupcast/core/messaging/MessagingClient$MAGNET_CONNECTION_TYPE;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 379
    const/4 v6, 0x0

    iput v6, p0, Lcom/samsung/groupcast/core/messaging/MessagingClient;->mChordInterface:I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 357
    .end local v0    # "connection":Lcom/samsung/groupcast/core/messaging/ConnectionHandle;
    .end local v5    # "isExceptionOccurred":Z
    :catchall_0
    move-exception v6

    monitor-exit p0

    throw v6

    .line 382
    .restart local v0    # "connection":Lcom/samsung/groupcast/core/messaging/ConnectionHandle;
    .restart local v5    # "isExceptionOccurred":Z
    :cond_4
    :try_start_4
    invoke-direct {p0}, Lcom/samsung/groupcast/core/messaging/MessagingClient;->getChordMgrInternal()Lcom/samsung/android/sdk/chord/SchordManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/android/sdk/chord/SchordManager;->getAvailableInterfaceTypes()Ljava/util/List;

    move-result-object v4

    .line 383
    .local v4, "ifList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    if-eqz v4, :cond_5

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v6

    if-nez v6, :cond_6

    .line 384
    :cond_5
    const-string v6, "start!!! >0"

    invoke-static {v6}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 385
    const/4 v6, 0x0

    iput v6, p0, Lcom/samsung/groupcast/core/messaging/MessagingClient;->mChordInterface:I

    goto/16 :goto_0

    .line 387
    :cond_6
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_7
    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    .line 388
    .local v2, "i":Ljava/lang/Integer;
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v6

    if-eq v12, v6, :cond_7

    .line 390
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v6

    packed-switch v6, :pswitch_data_0

    .line 396
    :pswitch_0
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "ERR!!! >"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    goto :goto_3

    .line 399
    :pswitch_1
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "start!!! >"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 400
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v6

    iput v6, p0, Lcom/samsung/groupcast/core/messaging/MessagingClient;->mChordInterface:I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 422
    .end local v2    # "i":Ljava/lang/Integer;
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v4    # "ifList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    :cond_8
    :try_start_5
    const-string v6, "chord waiting done"

    invoke-static {v6}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_1

    .line 435
    :cond_9
    :try_start_6
    const-string v6, "check error!!"

    invoke-static {v6}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto/16 :goto_2

    .line 390
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public acquireHybridChannel(Ljava/lang/String;)Lcom/samsung/groupcast/core/messaging/HybridChannel;
    .locals 4
    .param p1, "channelId"    # Ljava/lang/String;

    .prologue
    .line 311
    iget-object v2, p0, Lcom/samsung/groupcast/core/messaging/MessagingClient;->mHybridChannels:Ljava/util/Hashtable;

    monitor-enter v2

    .line 312
    :try_start_0
    iget-object v1, p0, Lcom/samsung/groupcast/core/messaging/MessagingClient;->mHybridChannels:Ljava/util/Hashtable;

    invoke-virtual {v1, p1}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 313
    iget-object v1, p0, Lcom/samsung/groupcast/core/messaging/MessagingClient;->mHybridChannels:Ljava/util/Hashtable;

    invoke-virtual {v1, p1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/groupcast/core/messaging/HybridChannel;

    invoke-virtual {v1}, Lcom/samsung/groupcast/core/messaging/HybridChannel;->getLocalChannel()Lcom/samsung/groupcast/core/messaging/LocalChannel;

    move-result-object v1

    if-nez v1, :cond_0

    .line 314
    iget-object v1, p0, Lcom/samsung/groupcast/core/messaging/MessagingClient;->mHybridChannels:Ljava/util/Hashtable;

    invoke-virtual {v1, p1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/groupcast/core/messaging/HybridChannel;

    invoke-virtual {p0, p1}, Lcom/samsung/groupcast/core/messaging/MessagingClient;->acquireLocalChannel(Ljava/lang/String;)Lcom/samsung/groupcast/core/messaging/LocalChannel;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/samsung/groupcast/core/messaging/HybridChannel;->setLocalChannel(Lcom/samsung/groupcast/core/messaging/LocalChannel;)V

    .line 316
    :cond_0
    iget-object v1, p0, Lcom/samsung/groupcast/core/messaging/MessagingClient;->mHybridChannels:Ljava/util/Hashtable;

    invoke-virtual {v1, p1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/groupcast/core/messaging/HybridChannel;

    monitor-exit v2

    .line 321
    :goto_0
    return-object v1

    .line 318
    :cond_1
    new-instance v0, Lcom/samsung/groupcast/core/messaging/HybridChannel;

    invoke-direct {v0, p1}, Lcom/samsung/groupcast/core/messaging/HybridChannel;-><init>(Ljava/lang/String;)V

    .line 319
    .local v0, "hc":Lcom/samsung/groupcast/core/messaging/HybridChannel;
    invoke-virtual {p0, p1}, Lcom/samsung/groupcast/core/messaging/MessagingClient;->acquireLocalChannel(Ljava/lang/String;)Lcom/samsung/groupcast/core/messaging/LocalChannel;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/groupcast/core/messaging/HybridChannel;->setLocalChannel(Lcom/samsung/groupcast/core/messaging/LocalChannel;)V

    .line 320
    iget-object v1, p0, Lcom/samsung/groupcast/core/messaging/MessagingClient;->mHybridChannels:Ljava/util/Hashtable;

    invoke-virtual {v1, p1, v0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 321
    monitor-exit v2

    move-object v1, v0

    goto :goto_0

    .line 323
    .end local v0    # "hc":Lcom/samsung/groupcast/core/messaging/HybridChannel;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public acquireLocalChannel(Ljava/lang/String;)Lcom/samsung/groupcast/core/messaging/LocalChannel;
    .locals 12
    .param p1, "channelId"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x4

    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 252
    const-string v3, "channelId"

    invoke-static {v3, p1}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 254
    iget-object v3, p0, Lcom/samsung/groupcast/core/messaging/MessagingClient;->mLocalChannels:Ljava/util/HashMap;

    invoke-virtual {v3, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/groupcast/core/messaging/MessagingClient$ChannelRecord;

    .line 256
    .local v2, "record":Lcom/samsung/groupcast/core/messaging/MessagingClient$ChannelRecord;
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/samsung/groupcast/core/messaging/MessagingClient$ChannelRecord;->getChannel()Lcom/samsung/groupcast/core/messaging/LocalChannel;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v2}, Lcom/samsung/groupcast/core/messaging/MessagingClient$ChannelRecord;->getChannel()Lcom/samsung/groupcast/core/messaging/LocalChannel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/groupcast/core/messaging/LocalChannel;->getChordChannel()Lcom/samsung/android/sdk/chord/SchordChannel;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 258
    invoke-virtual {v2}, Lcom/samsung/groupcast/core/messaging/MessagingClient$ChannelRecord;->incrementRefCount()I

    .line 259
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    const-string v4, "acquireChannel"

    const/4 v5, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    const-string v7, "channelId"

    aput-object v7, v6, v8

    aput-object p1, v6, v9

    const-string v7, "refCount"

    aput-object v7, v6, v10

    invoke-virtual {v2}, Lcom/samsung/groupcast/core/messaging/MessagingClient$ChannelRecord;->getRefCount()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v11

    invoke-static {v3, v4, v5, v6}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 261
    invoke-virtual {v2}, Lcom/samsung/groupcast/core/messaging/MessagingClient$ChannelRecord;->getChannel()Lcom/samsung/groupcast/core/messaging/LocalChannel;

    move-result-object v0

    .line 284
    :goto_0
    return-object v0

    .line 266
    :cond_0
    new-instance v0, Lcom/samsung/groupcast/core/messaging/LocalChannel;

    invoke-direct {p0}, Lcom/samsung/groupcast/core/messaging/MessagingClient;->getChordMgrInternal()Lcom/samsung/android/sdk/chord/SchordManager;

    move-result-object v3

    invoke-direct {v0, v3, p1}, Lcom/samsung/groupcast/core/messaging/LocalChannel;-><init>(Lcom/samsung/android/sdk/chord/SchordManager;Ljava/lang/String;)V

    .line 268
    .local v0, "channel":Lcom/samsung/groupcast/core/messaging/LocalChannel;
    :try_start_0
    invoke-direct {p0}, Lcom/samsung/groupcast/core/messaging/MessagingClient;->getChordMgrInternal()Lcom/samsung/android/sdk/chord/SchordManager;

    move-result-object v3

    invoke-virtual {v3, p1, v0}, Lcom/samsung/android/sdk/chord/SchordManager;->joinChannel(Ljava/lang/String;Lcom/samsung/android/sdk/chord/SchordChannel$StatusListener;)Lcom/samsung/android/sdk/chord/SchordChannel;

    move-result-object v3

    if-nez v3, :cond_1

    .line 269
    invoke-static {}, Lcom/samsung/groupcast/application/App;->getInstance()Lcom/samsung/groupcast/application/App;

    move-result-object v3

    const-string v4, "faile to joinchannel"

    const/4 v5, 0x0

    invoke-static {v3, v4, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    .line 270
    const-string v3, "failed"

    invoke-static {v3}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 280
    :cond_1
    :goto_1
    new-instance v2, Lcom/samsung/groupcast/core/messaging/MessagingClient$ChannelRecord;

    .end local v2    # "record":Lcom/samsung/groupcast/core/messaging/MessagingClient$ChannelRecord;
    invoke-direct {v2, v0}, Lcom/samsung/groupcast/core/messaging/MessagingClient$ChannelRecord;-><init>(Lcom/samsung/groupcast/core/messaging/LocalChannel;)V

    .line 281
    .restart local v2    # "record":Lcom/samsung/groupcast/core/messaging/MessagingClient$ChannelRecord;
    iget-object v3, p0, Lcom/samsung/groupcast/core/messaging/MessagingClient;->mLocalChannels:Ljava/util/HashMap;

    invoke-virtual {v3, p1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 282
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    const-string v4, "acquireChannel"

    const/4 v5, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    const-string v7, "channelId"

    aput-object v7, v6, v8

    aput-object p1, v6, v9

    const-string v7, "refCount"

    aput-object v7, v6, v10

    invoke-virtual {v2}, Lcom/samsung/groupcast/core/messaging/MessagingClient$ChannelRecord;->getRefCount()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v11

    invoke-static {v3, v4, v5, v6}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_0

    .line 272
    :catch_0
    move-exception v1

    .line 274
    .local v1, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v1}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_1

    .line 275
    .end local v1    # "e":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v1

    .line 277
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method protected finalize()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 232
    const-string v0, ""

    invoke-static {v0}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 235
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 236
    return-void
.end method

.method public getChordInterface()I
    .locals 2

    .prologue
    .line 245
    iget v0, p0, Lcom/samsung/groupcast/core/messaging/MessagingClient;->mChordInterface:I

    if-nez v0, :cond_0

    .line 246
    const-string v0, "MessagingClient"

    const-string v1, "mChordInterface was 0!!!"

    invoke-static {v0, v1}, Lcom/samsung/groupcast/application/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 248
    :cond_0
    iget v0, p0, Lcom/samsung/groupcast/core/messaging/MessagingClient;->mChordInterface:I

    return v0
.end method

.method public getLocalChannel(Ljava/lang/String;)Lcom/samsung/groupcast/core/messaging/LocalChannel;
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 104
    iget-object v0, p0, Lcom/samsung/groupcast/core/messaging/MessagingClient;->mLocalChannels:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 105
    iget-object v0, p0, Lcom/samsung/groupcast/core/messaging/MessagingClient;->mLocalChannels:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/groupcast/core/messaging/MessagingClient$ChannelRecord;

    invoke-virtual {v0}, Lcom/samsung/groupcast/core/messaging/MessagingClient$ChannelRecord;->getChannel()Lcom/samsung/groupcast/core/messaging/LocalChannel;

    move-result-object v0

    .line 107
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getLocalChannels()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/groupcast/core/messaging/MessagingClient$ChannelRecord;",
            ">;"
        }
    .end annotation

    .prologue
    .line 100
    iget-object v0, p0, Lcom/samsung/groupcast/core/messaging/MessagingClient;->mLocalChannels:Ljava/util/HashMap;

    return-object v0
.end method

.method public hasWifiConnectivity()Z
    .locals 1

    .prologue
    .line 241
    iget-boolean v0, p0, Lcom/samsung/groupcast/core/messaging/MessagingClient;->mWifiConnectivity:Z

    return v0
.end method

.method public isChordStarted()Z
    .locals 1

    .prologue
    .line 341
    iget-boolean v0, p0, Lcom/samsung/groupcast/core/messaging/MessagingClient;->mChordStarted:Z

    return v0
.end method

.method public releaseConnection(Lcom/samsung/groupcast/core/messaging/ConnectionHandle;)V
    .locals 6
    .param p1, "connection"    # Lcom/samsung/groupcast/core/messaging/ConnectionHandle;

    .prologue
    .line 450
    const-string v0, "connection"

    invoke-static {v0, p1}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 452
    iget-object v0, p0, Lcom/samsung/groupcast/core/messaging/MessagingClient;->mConnections:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/groupcast/core/messaging/MessagingClient;->mConnections:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 454
    const-string v0, "MessagingClient"

    const-string v1, "connection was not previously acquired or is already released"

    invoke-static {v0, v1}, Lcom/samsung/groupcast/application/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 457
    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "releaseConnection"

    const/4 v2, 0x0

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string v5, "requesterId"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-virtual {p1}, Lcom/samsung/groupcast/core/messaging/ConnectionHandle;->getRequesterId()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const-string v5, "connections"

    aput-object v5, v3, v4

    const/4 v4, 0x3

    iget-object v5, p0, Lcom/samsung/groupcast/core/messaging/MessagingClient;->mConnections:Ljava/util/HashSet;

    aput-object v5, v3, v4

    invoke-static {v0, v1, v2, v3}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 460
    iget-object v0, p0, Lcom/samsung/groupcast/core/messaging/MessagingClient;->mConnections:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 461
    const-string v0, "mConnection is not empty, return!!"

    invoke-static {v0}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 484
    :goto_0
    return-void

    .line 466
    :cond_1
    new-instance v0, Lcom/samsung/groupcast/core/messaging/MessagingClient$2;

    invoke-direct {v0, p0}, Lcom/samsung/groupcast/core/messaging/MessagingClient$2;-><init>(Lcom/samsung/groupcast/core/messaging/MessagingClient;)V

    iput-object v0, p0, Lcom/samsung/groupcast/core/messaging/MessagingClient;->mDelayedShutdownRunnable:Ljava/lang/Runnable;

    .line 481
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "releaseConnection"

    const-string v2, "scheduling magnet shutdown in 3000ms"

    invoke-static {v0, v1, v2}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;)V

    .line 483
    iget-object v0, p0, Lcom/samsung/groupcast/core/messaging/MessagingClient;->mDelayedShutdownRunnable:Ljava/lang/Runnable;

    const-wide/16 v1, 0xbb8

    invoke-static {v0, v1, v2}, Lcom/samsung/groupcast/application/MainQueue;->postDelayed(Ljava/lang/Runnable;J)V

    goto :goto_0
.end method

.method public releaseHybridChannel(Lcom/samsung/groupcast/core/messaging/HybridChannel;)V
    .locals 1
    .param p1, "channel"    # Lcom/samsung/groupcast/core/messaging/HybridChannel;

    .prologue
    .line 327
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/samsung/groupcast/core/messaging/HybridChannel;->getLocalChannel()Lcom/samsung/groupcast/core/messaging/LocalChannel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 328
    invoke-virtual {p1}, Lcom/samsung/groupcast/core/messaging/HybridChannel;->getLocalChannel()Lcom/samsung/groupcast/core/messaging/LocalChannel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 329
    invoke-virtual {p1}, Lcom/samsung/groupcast/core/messaging/HybridChannel;->getLocalChannel()Lcom/samsung/groupcast/core/messaging/LocalChannel;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/groupcast/core/messaging/MessagingClient;->releaseLocalChannel(Lcom/samsung/groupcast/core/messaging/LocalChannel;)V

    .line 330
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/samsung/groupcast/core/messaging/HybridChannel;->setLocalChannel(Lcom/samsung/groupcast/core/messaging/LocalChannel;)V

    .line 333
    :cond_0
    return-void
.end method

.method public releaseLocalChannel(Lcom/samsung/groupcast/core/messaging/LocalChannel;)V
    .locals 8
    .param p1, "channel"    # Lcom/samsung/groupcast/core/messaging/LocalChannel;

    .prologue
    .line 288
    const-string v2, "channel"

    invoke-static {v2, p1}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 289
    invoke-virtual {p1}, Lcom/samsung/groupcast/core/messaging/LocalChannel;->getChannelId()Ljava/lang/String;

    move-result-object v0

    .line 290
    .local v0, "channelId":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/groupcast/core/messaging/MessagingClient;->mLocalChannels:Ljava/util/HashMap;

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/groupcast/core/messaging/MessagingClient$ChannelRecord;

    .line 294
    .local v1, "record":Lcom/samsung/groupcast/core/messaging/MessagingClient$ChannelRecord;
    if-eqz v1, :cond_1

    .line 295
    invoke-virtual {v1}, Lcom/samsung/groupcast/core/messaging/MessagingClient$ChannelRecord;->decrementRefCount()I

    .line 296
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const-string v3, "releaseChannel"

    const/4 v4, 0x0

    const/4 v5, 0x4

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    const-string v7, "channelId"

    aput-object v7, v5, v6

    const/4 v6, 0x1

    aput-object v0, v5, v6

    const/4 v6, 0x2

    const-string v7, "refCount"

    aput-object v7, v5, v6

    const/4 v6, 0x3

    invoke-virtual {v1}, Lcom/samsung/groupcast/core/messaging/MessagingClient$ChannelRecord;->getRefCount()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v2, v3, v4, v5}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 300
    invoke-virtual {v1}, Lcom/samsung/groupcast/core/messaging/MessagingClient$ChannelRecord;->hasReferences()Z

    move-result v2

    if-nez v2, :cond_0

    .line 301
    iget-object v2, p0, Lcom/samsung/groupcast/core/messaging/MessagingClient;->mLocalChannels:Ljava/util/HashMap;

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 302
    invoke-virtual {v1}, Lcom/samsung/groupcast/core/messaging/MessagingClient$ChannelRecord;->getChannel()Lcom/samsung/groupcast/core/messaging/LocalChannel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/groupcast/core/messaging/LocalChannel;->leave()V

    .line 307
    :cond_0
    :goto_0
    return-void

    .line 305
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "channelId:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " is not in "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/groupcast/core/messaging/MessagingClient;->mLocalChannels:Ljava/util/HashMap;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    goto :goto_0
.end method
