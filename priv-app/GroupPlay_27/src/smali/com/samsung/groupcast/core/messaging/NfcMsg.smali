.class public Lcom/samsung/groupcast/core/messaging/NfcMsg;
.super Ljava/lang/Object;
.source "NfcMsg.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public inviter_screen_id:Ljava/lang/String;

.field public music_live_channel_id:Ljava/lang/String;

.field public session_pin:Ljava/lang/String;

.field public session_summary:Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;

.field public wifi_bssid:Ljava/lang/String;

.field public wifi_passwd:Ljava/lang/String;

.field public wifi_ssid:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/nfc/NdefMessage;)V
    .locals 7
    .param p1, "msg"    # Landroid/nfc/NdefMessage;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    new-instance v0, Ljava/io/ByteArrayInputStream;

    invoke-virtual {p1}, Landroid/nfc/NdefMessage;->getRecords()[Landroid/nfc/NdefRecord;

    move-result-object v5

    const/4 v6, 0x0

    aget-object v5, v5, v6

    invoke-virtual {v5}, Landroid/nfc/NdefRecord;->getPayload()[B

    move-result-object v5

    invoke-direct {v0, v5}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 38
    .local v0, "bis":Ljava/io/ByteArrayInputStream;
    const/4 v2, 0x0

    .line 41
    .local v2, "in":Ljava/io/ObjectInput;
    :try_start_0
    new-instance v3, Ljava/io/ObjectInputStream;

    invoke-direct {v3, v0}, Ljava/io/ObjectInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/io/StreamCorruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_2

    .line 42
    .end local v2    # "in":Ljava/io/ObjectInput;
    .local v3, "in":Ljava/io/ObjectInput;
    :try_start_1
    invoke-interface {v3}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/groupcast/core/messaging/NfcMsg;

    .line 43
    .local v4, "nfcMsg":Lcom/samsung/groupcast/core/messaging/NfcMsg;
    iget-object v5, v4, Lcom/samsung/groupcast/core/messaging/NfcMsg;->wifi_bssid:Ljava/lang/String;

    iput-object v5, p0, Lcom/samsung/groupcast/core/messaging/NfcMsg;->wifi_bssid:Ljava/lang/String;

    .line 44
    iget-object v5, v4, Lcom/samsung/groupcast/core/messaging/NfcMsg;->wifi_passwd:Ljava/lang/String;

    iput-object v5, p0, Lcom/samsung/groupcast/core/messaging/NfcMsg;->wifi_passwd:Ljava/lang/String;

    .line 45
    iget-object v5, v4, Lcom/samsung/groupcast/core/messaging/NfcMsg;->wifi_ssid:Ljava/lang/String;

    iput-object v5, p0, Lcom/samsung/groupcast/core/messaging/NfcMsg;->wifi_ssid:Ljava/lang/String;

    .line 46
    iget-object v5, v4, Lcom/samsung/groupcast/core/messaging/NfcMsg;->session_summary:Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;

    iput-object v5, p0, Lcom/samsung/groupcast/core/messaging/NfcMsg;->session_summary:Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;

    .line 47
    iget-object v5, v4, Lcom/samsung/groupcast/core/messaging/NfcMsg;->session_pin:Ljava/lang/String;

    iput-object v5, p0, Lcom/samsung/groupcast/core/messaging/NfcMsg;->session_pin:Ljava/lang/String;

    .line 48
    iget-object v5, v4, Lcom/samsung/groupcast/core/messaging/NfcMsg;->inviter_screen_id:Ljava/lang/String;

    iput-object v5, p0, Lcom/samsung/groupcast/core/messaging/NfcMsg;->inviter_screen_id:Ljava/lang/String;

    .line 49
    iget-object v5, v4, Lcom/samsung/groupcast/core/messaging/NfcMsg;->music_live_channel_id:Ljava/lang/String;

    iput-object v5, p0, Lcom/samsung/groupcast/core/messaging/NfcMsg;->music_live_channel_id:Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/StreamCorruptedException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_6
    .catch Ljava/lang/ClassNotFoundException; {:try_start_1 .. :try_end_1} :catch_5

    .line 62
    :try_start_2
    invoke-interface {v3}, Ljava/io/ObjectInput;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3

    .line 65
    :goto_0
    :try_start_3
    invoke-virtual {v0}, Ljava/io/ByteArrayInputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_4

    .line 67
    :goto_1
    return-void

    .line 50
    .end local v3    # "in":Ljava/io/ObjectInput;
    .end local v4    # "nfcMsg":Lcom/samsung/groupcast/core/messaging/NfcMsg;
    .restart local v2    # "in":Ljava/io/ObjectInput;
    :catch_0
    move-exception v1

    .line 51
    .local v1, "e":Ljava/io/StreamCorruptedException;
    :goto_2
    invoke-virtual {v1}, Ljava/io/StreamCorruptedException;->printStackTrace()V

    .line 52
    new-instance v5, Ljava/lang/Exception;

    invoke-virtual {v1}, Ljava/io/StreamCorruptedException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v5

    .line 53
    .end local v1    # "e":Ljava/io/StreamCorruptedException;
    :catch_1
    move-exception v1

    .line 54
    .local v1, "e":Ljava/io/IOException;
    :goto_3
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .line 55
    new-instance v5, Ljava/lang/Exception;

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v5

    .line 56
    .end local v1    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v1

    .line 57
    .local v1, "e":Ljava/lang/ClassNotFoundException;
    :goto_4
    invoke-virtual {v1}, Ljava/lang/ClassNotFoundException;->printStackTrace()V

    .line 58
    new-instance v5, Ljava/lang/Exception;

    invoke-virtual {v1}, Ljava/lang/ClassNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v5

    .line 63
    .end local v1    # "e":Ljava/lang/ClassNotFoundException;
    .end local v2    # "in":Ljava/io/ObjectInput;
    .restart local v3    # "in":Ljava/io/ObjectInput;
    .restart local v4    # "nfcMsg":Lcom/samsung/groupcast/core/messaging/NfcMsg;
    :catch_3
    move-exception v5

    goto :goto_0

    .line 66
    :catch_4
    move-exception v5

    goto :goto_1

    .line 56
    .end local v4    # "nfcMsg":Lcom/samsung/groupcast/core/messaging/NfcMsg;
    :catch_5
    move-exception v1

    move-object v2, v3

    .end local v3    # "in":Ljava/io/ObjectInput;
    .restart local v2    # "in":Ljava/io/ObjectInput;
    goto :goto_4

    .line 53
    .end local v2    # "in":Ljava/io/ObjectInput;
    .restart local v3    # "in":Ljava/io/ObjectInput;
    :catch_6
    move-exception v1

    move-object v2, v3

    .end local v3    # "in":Ljava/io/ObjectInput;
    .restart local v2    # "in":Ljava/io/ObjectInput;
    goto :goto_3

    .line 50
    .end local v2    # "in":Ljava/io/ObjectInput;
    .restart local v3    # "in":Ljava/io/ObjectInput;
    :catch_7
    move-exception v1

    move-object v2, v3

    .end local v3    # "in":Ljava/io/ObjectInput;
    .restart local v2    # "in":Ljava/io/ObjectInput;
    goto :goto_2
.end method

.method protected constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Parcelable;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "wifi_bssid"    # Ljava/lang/String;
    .param p2, "wifi_passwd"    # Ljava/lang/String;
    .param p3, "wifi_ssid"    # Ljava/lang/String;
    .param p4, "session_summary"    # Landroid/os/Parcelable;
    .param p5, "session_pin"    # Ljava/lang/String;
    .param p6, "inviter_screen_id"    # Ljava/lang/String;

    .prologue
    .line 98
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 99
    iput-object p1, p0, Lcom/samsung/groupcast/core/messaging/NfcMsg;->wifi_bssid:Ljava/lang/String;

    .line 100
    iput-object p2, p0, Lcom/samsung/groupcast/core/messaging/NfcMsg;->wifi_passwd:Ljava/lang/String;

    .line 101
    iput-object p3, p0, Lcom/samsung/groupcast/core/messaging/NfcMsg;->wifi_ssid:Ljava/lang/String;

    .line 102
    check-cast p4, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;

    .end local p4    # "session_summary":Landroid/os/Parcelable;
    iput-object p4, p0, Lcom/samsung/groupcast/core/messaging/NfcMsg;->session_summary:Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;

    .line 103
    iput-object p5, p0, Lcom/samsung/groupcast/core/messaging/NfcMsg;->session_pin:Ljava/lang/String;

    .line 104
    iput-object p6, p0, Lcom/samsung/groupcast/core/messaging/NfcMsg;->inviter_screen_id:Ljava/lang/String;

    .line 105
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Parcelable;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "wifi_bssid"    # Ljava/lang/String;
    .param p2, "wifi_passwd"    # Ljava/lang/String;
    .param p3, "wifi_ssid"    # Ljava/lang/String;
    .param p4, "session_summary"    # Landroid/os/Parcelable;
    .param p5, "session_pin"    # Ljava/lang/String;
    .param p6, "inviter_screen_id"    # Ljava/lang/String;
    .param p7, "music_live_channel_id"    # Ljava/lang/String;

    .prologue
    .line 89
    invoke-direct/range {p0 .. p6}, Lcom/samsung/groupcast/core/messaging/NfcMsg;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Parcelable;Ljava/lang/String;Ljava/lang/String;)V

    .line 91
    iput-object p7, p0, Lcom/samsung/groupcast/core/messaging/NfcMsg;->music_live_channel_id:Ljava/lang/String;

    .line 92
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "wifi_bssid"    # Ljava/lang/String;
    .param p2, "wifi_passwd"    # Ljava/lang/String;
    .param p3, "wifi_ssid"    # Ljava/lang/String;
    .param p4, "session_summary"    # Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;
    .param p5, "session_pin"    # Ljava/lang/String;
    .param p6, "inviter_screen_id"    # Ljava/lang/String;

    .prologue
    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    iput-object p1, p0, Lcom/samsung/groupcast/core/messaging/NfcMsg;->wifi_bssid:Ljava/lang/String;

    .line 75
    iput-object p2, p0, Lcom/samsung/groupcast/core/messaging/NfcMsg;->wifi_passwd:Ljava/lang/String;

    .line 76
    iput-object p3, p0, Lcom/samsung/groupcast/core/messaging/NfcMsg;->wifi_ssid:Ljava/lang/String;

    .line 77
    iput-object p4, p0, Lcom/samsung/groupcast/core/messaging/NfcMsg;->session_summary:Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;

    .line 78
    iput-object p5, p0, Lcom/samsung/groupcast/core/messaging/NfcMsg;->session_pin:Ljava/lang/String;

    .line 79
    iput-object p6, p0, Lcom/samsung/groupcast/core/messaging/NfcMsg;->inviter_screen_id:Ljava/lang/String;

    .line 80
    return-void
.end method

.method private createMimeRecord(Ljava/lang/String;[B)Landroid/nfc/NdefRecord;
    .locals 4
    .param p1, "mimeType"    # Ljava/lang/String;
    .param p2, "payload"    # [B

    .prologue
    .line 140
    const-string v2, "US-ASCII"

    invoke-static {v2}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v0

    .line 142
    .local v0, "mimeBytes":[B
    new-instance v1, Landroid/nfc/NdefRecord;

    const/4 v2, 0x2

    const/4 v3, 0x0

    new-array v3, v3, [B

    invoke-direct {v1, v2, v0, v3, p2}, Landroid/nfc/NdefRecord;-><init>(S[B[B[B)V

    .line 145
    .local v1, "mimeRecord":Landroid/nfc/NdefRecord;
    return-object v1
.end method


# virtual methods
.method public getBytes()[B
    .locals 6

    .prologue
    .line 108
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 109
    .local v0, "bos":Ljava/io/ByteArrayOutputStream;
    const/4 v3, 0x0

    .line 110
    .local v3, "out":Ljava/io/ObjectOutput;
    const/4 v1, 0x0

    .line 113
    .local v1, "bytes":[B
    :try_start_0
    new-instance v4, Ljava/io/ObjectOutputStream;

    invoke-direct {v4, v0}, Ljava/io/ObjectOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 114
    .end local v3    # "out":Ljava/io/ObjectOutput;
    .local v4, "out":Ljava/io/ObjectOutput;
    :try_start_1
    invoke-interface {v4, p0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    .line 115
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3

    move-result-object v1

    move-object v3, v4

    .line 121
    .end local v4    # "out":Ljava/io/ObjectOutput;
    .restart local v3    # "out":Ljava/io/ObjectOutput;
    :goto_0
    :try_start_2
    invoke-interface {v3}, Ljava/io/ObjectOutput;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 124
    :goto_1
    :try_start_3
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    .line 127
    :goto_2
    return-object v1

    .line 116
    :catch_0
    move-exception v2

    .line 117
    .local v2, "e":Ljava/io/IOException;
    :goto_3
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 122
    .end local v2    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v5

    goto :goto_1

    .line 125
    :catch_2
    move-exception v5

    goto :goto_2

    .line 116
    .end local v3    # "out":Ljava/io/ObjectOutput;
    .restart local v4    # "out":Ljava/io/ObjectOutput;
    :catch_3
    move-exception v2

    move-object v3, v4

    .end local v4    # "out":Ljava/io/ObjectOutput;
    .restart local v3    # "out":Ljava/io/ObjectOutput;
    goto :goto_3
.end method

.method public getNdefMessage()Landroid/nfc/NdefMessage;
    .locals 5

    .prologue
    .line 131
    new-instance v0, Landroid/nfc/NdefMessage;

    const/4 v1, 0x2

    new-array v1, v1, [Landroid/nfc/NdefRecord;

    const/4 v2, 0x0

    const-string v3, "application/com.samsung.groupcast"

    invoke-virtual {p0}, Lcom/samsung/groupcast/core/messaging/NfcMsg;->getBytes()[B

    move-result-object v4

    invoke-direct {p0, v3, v4}, Lcom/samsung/groupcast/core/messaging/NfcMsg;->createMimeRecord(Ljava/lang/String;[B)Landroid/nfc/NdefRecord;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "com.samsung.groupcast"

    invoke-static {v3}, Landroid/nfc/NdefRecord;->createApplicationRecord(Ljava/lang/String;)Landroid/nfc/NdefRecord;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-direct {v0, v1}, Landroid/nfc/NdefMessage;-><init>([Landroid/nfc/NdefRecord;)V

    .line 136
    .local v0, "msg":Landroid/nfc/NdefMessage;
    return-object v0
.end method
