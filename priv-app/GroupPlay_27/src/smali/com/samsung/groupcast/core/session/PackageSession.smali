.class public Lcom/samsung/groupcast/core/session/PackageSession;
.super Ljava/lang/Object;
.source "PackageSession.java"

# interfaces
.implements Lcom/samsung/android/sdk/chord/SchordChannel$StatusListener;


# static fields
.field private static final TAG:Ljava/lang/String; = "GPSDKv2_PS"


# instance fields
.field private mChannelMap:Ljava/util/Hashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/sdk/chord/SchordChannel;",
            ">;"
        }
    .end annotation
.end field

.field private mChannelNameList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mPkgName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "pkgName"    # Ljava/lang/String;

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/groupcast/core/session/PackageSession;->mChannelNameList:Ljava/util/List;

    .line 23
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lcom/samsung/groupcast/core/session/PackageSession;->mChannelMap:Ljava/util/Hashtable;

    .line 32
    iput-object p1, p0, Lcom/samsung/groupcast/core/session/PackageSession;->mPkgName:Ljava/lang/String;

    .line 33
    return-void
.end method


# virtual methods
.method public addChannel(Lcom/samsung/android/sdk/chord/SchordChannel;)V
    .locals 4
    .param p1, "schordChannel"    # Lcom/samsung/android/sdk/chord/SchordChannel;

    .prologue
    .line 41
    sget-boolean v1, Lcom/samsung/groupcast/debug/DebugFlags;->DEBUG_FLAG_BOOLEAN_PKGSESSION_LOG:Z

    if-eqz v1, :cond_0

    .line 42
    const-string v1, "GPSDKv2_PS"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "+add size:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/groupcast/core/session/PackageSession;->mChannelMap:Ljava/util/Hashtable;

    invoke-virtual {v3}, Ljava/util/Hashtable;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/groupcast/core/session/PackageSession;->mChannelNameList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 45
    :cond_0
    if-eqz p1, :cond_2

    .line 46
    invoke-interface {p1}, Lcom/samsung/android/sdk/chord/SchordChannel;->getName()Ljava/lang/String;

    move-result-object v0

    .line 47
    .local v0, "name":Ljava/lang/String;
    sget-boolean v1, Lcom/samsung/groupcast/debug/DebugFlags;->DEBUG_FLAG_BOOLEAN_PKGSESSION_LOG:Z

    if-eqz v1, :cond_1

    .line 48
    const-string v1, "GPSDKv2_PS"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "name:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    :cond_1
    iget-object v1, p0, Lcom/samsung/groupcast/core/session/PackageSession;->mChannelNameList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 52
    iget-object v1, p0, Lcom/samsung/groupcast/core/session/PackageSession;->mChannelMap:Ljava/util/Hashtable;

    invoke-virtual {v1, v0, p1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 55
    .end local v0    # "name":Ljava/lang/String;
    :cond_2
    sget-boolean v1, Lcom/samsung/groupcast/debug/DebugFlags;->DEBUG_FLAG_BOOLEAN_PKGSESSION_LOG:Z

    if-eqz v1, :cond_3

    .line 56
    const-string v1, "GPSDKv2_PS"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "-add size:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/groupcast/core/session/PackageSession;->mChannelMap:Ljava/util/Hashtable;

    invoke-virtual {v3}, Ljava/util/Hashtable;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/groupcast/core/session/PackageSession;->mChannelNameList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    :cond_3
    return-void
.end method

.method public dumpChannels()V
    .locals 5

    .prologue
    .line 88
    sget-boolean v2, Lcom/samsung/groupcast/debug/DebugFlags;->DEBUG_FLAG_BOOLEAN_PKGSESSION_LOG:Z

    if-eqz v2, :cond_0

    .line 89
    const-string v2, "GPSDKv2_PS"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "dumpCh mChList:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/groupcast/core/session/PackageSession;->mChannelNameList:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ",mChMap:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/groupcast/core/session/PackageSession;->mChannelMap:Ljava/util/Hashtable;

    invoke-virtual {v4}, Ljava/util/Hashtable;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/groupcast/application/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 91
    iget-object v2, p0, Lcom/samsung/groupcast/core/session/PackageSession;->mChannelNameList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 92
    .local v1, "s":Ljava/lang/String;
    const-string v2, "GPSDKv2_PS"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "-joinch:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ",:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/groupcast/core/session/PackageSession;->mChannelMap:Ljava/util/Hashtable;

    invoke-virtual {v4, v1}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/groupcast/application/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 95
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "s":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method public getChannel(Ljava/lang/String;)Lcom/samsung/android/sdk/chord/SchordChannel;
    .locals 1
    .param p1, "channelName"    # Ljava/lang/String;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/samsung/groupcast/core/session/PackageSession;->mChannelMap:Ljava/util/Hashtable;

    invoke-virtual {v0, p1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/chord/SchordChannel;

    return-object v0
.end method

.method public getChannelNameList()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 61
    const-string v0, "GPSDKv2_PS"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getChannelNameList size:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/groupcast/core/session/PackageSession;->mChannelNameList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    iget-object v0, p0, Lcom/samsung/groupcast/core/session/PackageSession;->mChannelNameList:Ljava/util/List;

    return-object v0
.end method

.method public getPkgName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/samsung/groupcast/core/session/PackageSession;->mPkgName:Ljava/lang/String;

    return-object v0
.end method

.method public hasChannel(Ljava/lang/String;)Z
    .locals 1
    .param p1, "channelName"    # Ljava/lang/String;

    .prologue
    .line 75
    iget-object v0, p0, Lcom/samsung/groupcast/core/session/PackageSession;->mChannelMap:Ljava/util/Hashtable;

    invoke-virtual {v0, p1}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public joinChannel(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "channel"    # Ljava/lang/String;

    .prologue
    .line 98
    const-string v1, "GPSDKv2_PS"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "joinCh:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/groupcast/application/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 99
    invoke-static {}, Lcom/samsung/groupcast/core/messaging/MessagingClient;->getChordMgr()Lcom/samsung/android/sdk/chord/SchordManager;

    move-result-object v1

    invoke-virtual {v1, p1, p0}, Lcom/samsung/android/sdk/chord/SchordManager;->joinChannel(Ljava/lang/String;Lcom/samsung/android/sdk/chord/SchordChannel$StatusListener;)Lcom/samsung/android/sdk/chord/SchordChannel;

    move-result-object v0

    .line 100
    .local v0, "ch":Lcom/samsung/android/sdk/chord/SchordChannel;
    if-eqz v0, :cond_1

    .line 101
    invoke-virtual {p0, v0}, Lcom/samsung/groupcast/core/session/PackageSession;->addChannel(Lcom/samsung/android/sdk/chord/SchordChannel;)V

    .line 102
    sget-boolean v1, Lcom/samsung/groupcast/debug/DebugFlags;->DEBUG_FLAG_BOOLEAN_PKGSESSION_LOG:Z

    if-eqz v1, :cond_0

    .line 103
    invoke-virtual {p0}, Lcom/samsung/groupcast/core/session/PackageSession;->dumpChannels()V

    .line 105
    :cond_0
    invoke-interface {v0}, Lcom/samsung/android/sdk/chord/SchordChannel;->getName()Ljava/lang/String;

    move-result-object v1

    .line 107
    :goto_0
    return-object v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public leaveChannel(Ljava/lang/String;)V
    .locals 3
    .param p1, "channel"    # Ljava/lang/String;

    .prologue
    .line 111
    const-string v0, "GPSDKv2_PS"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "leaveCh:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/groupcast/application/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    invoke-virtual {p0, p1}, Lcom/samsung/groupcast/core/session/PackageSession;->removeChannel(Ljava/lang/String;)V

    .line 113
    invoke-static {}, Lcom/samsung/groupcast/core/messaging/MessagingClient;->getChordMgr()Lcom/samsung/android/sdk/chord/SchordManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/chord/SchordManager;->leaveChannel(Ljava/lang/String;)V

    .line 115
    sget-boolean v0, Lcom/samsung/groupcast/debug/DebugFlags;->DEBUG_FLAG_BOOLEAN_PKGSESSION_LOG:Z

    if-eqz v0, :cond_0

    .line 116
    invoke-virtual {p0}, Lcom/samsung/groupcast/core/session/PackageSession;->dumpChannels()V

    .line 118
    :cond_0
    return-void
.end method

.method public onDataReceived(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[[B)V
    .locals 11
    .param p1, "fromNode"    # Ljava/lang/String;
    .param p2, "fromChannel"    # Ljava/lang/String;
    .param p3, "payloadType"    # Ljava/lang/String;
    .param p4, "payload"    # [[B

    .prologue
    .line 151
    invoke-static {}, Lcom/samsung/groupcast/service/GroupPlayStateService;->getLastestInstance()Lcom/samsung/groupcast/service/GroupPlayStateService;

    move-result-object v9

    .line 152
    .local v9, "svc":Lcom/samsung/groupcast/service/GroupPlayStateService;
    if-eqz v9, :cond_0

    .line 153
    invoke-virtual {v9}, Lcom/samsung/groupcast/service/GroupPlayStateService;->getOutgoingMsgHandler()Landroid/os/Handler;

    move-result-object v8

    .line 154
    .local v8, "outgoingMsgHandler":Landroid/os/Handler;
    if-eqz v8, :cond_0

    .line 155
    const v10, 0x10002

    new-instance v0, Lcom/samsung/groupcast/service/GroupPlayPacket;

    iget-object v1, p0, Lcom/samsung/groupcast/core/session/PackageSession;->mPkgName:Ljava/lang/String;

    const-string v4, ""

    const/4 v5, 0x0

    move-object v2, p1

    move-object v3, p2

    move-object v6, p3

    move-object v7, p4

    invoke-direct/range {v0 .. v7}, Lcom/samsung/groupcast/service/GroupPlayPacket;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;[[B)V

    invoke-virtual {v8, v10, v0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v8, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 160
    .end local v8    # "outgoingMsgHandler":Landroid/os/Handler;
    :cond_0
    return-void
.end method

.method public onFileChunkReceived(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJ)V
    .locals 0
    .param p1, "arg0"    # Ljava/lang/String;
    .param p2, "arg1"    # Ljava/lang/String;
    .param p3, "arg2"    # Ljava/lang/String;
    .param p4, "arg3"    # Ljava/lang/String;
    .param p5, "arg4"    # Ljava/lang/String;
    .param p6, "arg5"    # Ljava/lang/String;
    .param p7, "arg6"    # J
    .param p9, "arg7"    # J

    .prologue
    .line 193
    return-void
.end method

.method public onFileChunkSent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJJ)V
    .locals 0
    .param p1, "arg0"    # Ljava/lang/String;
    .param p2, "arg1"    # Ljava/lang/String;
    .param p3, "arg2"    # Ljava/lang/String;
    .param p4, "arg3"    # Ljava/lang/String;
    .param p5, "arg4"    # Ljava/lang/String;
    .param p6, "arg5"    # Ljava/lang/String;
    .param p7, "arg6"    # J
    .param p9, "arg7"    # J
    .param p11, "arg8"    # J

    .prologue
    .line 197
    return-void
.end method

.method public onFileFailed(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 0
    .param p1, "arg0"    # Ljava/lang/String;
    .param p2, "arg1"    # Ljava/lang/String;
    .param p3, "arg2"    # Ljava/lang/String;
    .param p4, "arg3"    # Ljava/lang/String;
    .param p5, "arg4"    # Ljava/lang/String;
    .param p6, "arg5"    # I

    .prologue
    .line 201
    return-void
.end method

.method public onFileReceived(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;)V
    .locals 0
    .param p1, "arg0"    # Ljava/lang/String;
    .param p2, "arg1"    # Ljava/lang/String;
    .param p3, "arg2"    # Ljava/lang/String;
    .param p4, "arg3"    # Ljava/lang/String;
    .param p5, "arg4"    # Ljava/lang/String;
    .param p6, "arg5"    # Ljava/lang/String;
    .param p7, "arg6"    # J
    .param p9, "arg7"    # Ljava/lang/String;

    .prologue
    .line 205
    return-void
.end method

.method public onFileSent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "arg0"    # Ljava/lang/String;
    .param p2, "arg1"    # Ljava/lang/String;
    .param p3, "arg2"    # Ljava/lang/String;
    .param p4, "arg3"    # Ljava/lang/String;
    .param p5, "arg4"    # Ljava/lang/String;
    .param p6, "arg5"    # Ljava/lang/String;

    .prologue
    .line 209
    return-void
.end method

.method public onFileWillReceive(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V
    .locals 0
    .param p1, "arg0"    # Ljava/lang/String;
    .param p2, "arg1"    # Ljava/lang/String;
    .param p3, "arg2"    # Ljava/lang/String;
    .param p4, "arg3"    # Ljava/lang/String;
    .param p5, "arg4"    # Ljava/lang/String;
    .param p6, "arg5"    # Ljava/lang/String;
    .param p7, "arg6"    # J

    .prologue
    .line 213
    return-void
.end method

.method public onMultiFilesChunkReceived(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;JJ)V
    .locals 0
    .param p1, "arg0"    # Ljava/lang/String;
    .param p2, "arg1"    # Ljava/lang/String;
    .param p3, "arg2"    # Ljava/lang/String;
    .param p4, "arg3"    # Ljava/lang/String;
    .param p5, "arg4"    # I
    .param p6, "arg5"    # Ljava/lang/String;
    .param p7, "arg6"    # J
    .param p9, "arg7"    # J

    .prologue
    .line 217
    return-void
.end method

.method public onMultiFilesChunkSent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;JJJ)V
    .locals 0
    .param p1, "arg0"    # Ljava/lang/String;
    .param p2, "arg1"    # Ljava/lang/String;
    .param p3, "arg2"    # Ljava/lang/String;
    .param p4, "arg3"    # Ljava/lang/String;
    .param p5, "arg4"    # I
    .param p6, "arg5"    # Ljava/lang/String;
    .param p7, "arg6"    # J
    .param p9, "arg7"    # J
    .param p11, "arg8"    # J

    .prologue
    .line 221
    return-void
.end method

.method public onMultiFilesFailed(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V
    .locals 0
    .param p1, "arg0"    # Ljava/lang/String;
    .param p2, "arg1"    # Ljava/lang/String;
    .param p3, "arg2"    # Ljava/lang/String;
    .param p4, "arg3"    # Ljava/lang/String;
    .param p5, "arg4"    # I
    .param p6, "arg5"    # I

    .prologue
    .line 225
    return-void
.end method

.method public onMultiFilesFinished(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 0
    .param p1, "arg0"    # Ljava/lang/String;
    .param p2, "arg1"    # Ljava/lang/String;
    .param p3, "arg2"    # Ljava/lang/String;
    .param p4, "arg3"    # I

    .prologue
    .line 228
    return-void
.end method

.method public onMultiFilesReceived(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;JLjava/lang/String;)V
    .locals 0
    .param p1, "arg0"    # Ljava/lang/String;
    .param p2, "arg1"    # Ljava/lang/String;
    .param p3, "arg2"    # Ljava/lang/String;
    .param p4, "arg3"    # Ljava/lang/String;
    .param p5, "arg4"    # I
    .param p6, "arg5"    # Ljava/lang/String;
    .param p7, "arg6"    # J
    .param p9, "arg7"    # Ljava/lang/String;

    .prologue
    .line 232
    return-void
.end method

.method public onMultiFilesSent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p1, "arg0"    # Ljava/lang/String;
    .param p2, "arg1"    # Ljava/lang/String;
    .param p3, "arg2"    # Ljava/lang/String;
    .param p4, "arg3"    # Ljava/lang/String;
    .param p5, "arg4"    # I
    .param p6, "arg5"    # Ljava/lang/String;

    .prologue
    .line 236
    return-void
.end method

.method public onMultiFilesWillReceive(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;J)V
    .locals 0
    .param p1, "arg0"    # Ljava/lang/String;
    .param p2, "arg1"    # Ljava/lang/String;
    .param p3, "arg2"    # Ljava/lang/String;
    .param p4, "arg3"    # Ljava/lang/String;
    .param p5, "arg4"    # I
    .param p6, "arg5"    # Ljava/lang/String;
    .param p7, "arg6"    # J

    .prologue
    .line 240
    return-void
.end method

.method public onNodeChanged(ILjava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p1, "event"    # I
    .param p2, "fromNode"    # Ljava/lang/String;
    .param p3, "fromChannel"    # Ljava/lang/String;

    .prologue
    .line 164
    invoke-static {}, Lcom/samsung/groupcast/service/GroupPlayStateService;->getLastestInstance()Lcom/samsung/groupcast/service/GroupPlayStateService;

    move-result-object v2

    .line 165
    .local v2, "svc":Lcom/samsung/groupcast/service/GroupPlayStateService;
    if-eqz v2, :cond_0

    .line 166
    invoke-virtual {v2}, Lcom/samsung/groupcast/service/GroupPlayStateService;->getOutgoingMsgHandler()Landroid/os/Handler;

    move-result-object v1

    .line 167
    .local v1, "outgoingMsgHandler":Landroid/os/Handler;
    if-eqz v1, :cond_0

    .line 169
    :try_start_0
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3}, Lorg/json/JSONObject;-><init>()V

    const-string v4, "CH"

    invoke-virtual {v3, v4, p3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v3

    const-string v4, "NODE"

    invoke-virtual {v3, v4, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v3

    invoke-virtual {v3}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, p1, v3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 179
    .end local v1    # "outgoingMsgHandler":Landroid/os/Handler;
    :cond_0
    :goto_0
    return-void

    .line 173
    .restart local v1    # "outgoingMsgHandler":Landroid/os/Handler;
    :catch_0
    move-exception v0

    .line 175
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public onNodeJoined(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "fromNode"    # Ljava/lang/String;
    .param p2, "fromChannel"    # Ljava/lang/String;

    .prologue
    .line 183
    const v0, 0x10003

    invoke-virtual {p0, v0, p1, p2}, Lcom/samsung/groupcast/core/session/PackageSession;->onNodeChanged(ILjava/lang/String;Ljava/lang/String;)V

    .line 184
    return-void
.end method

.method public onNodeLeft(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "fromNode"    # Ljava/lang/String;
    .param p2, "fromChannel"    # Ljava/lang/String;

    .prologue
    .line 188
    const v0, 0x10004

    invoke-virtual {p0, v0, p1, p2}, Lcom/samsung/groupcast/core/session/PackageSession;->onNodeChanged(ILjava/lang/String;Ljava/lang/String;)V

    .line 189
    return-void
.end method

.method public removeChannel(Ljava/lang/String;)V
    .locals 1
    .param p1, "channelName"    # Ljava/lang/String;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/samsung/groupcast/core/session/PackageSession;->mChannelNameList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 67
    iget-object v0, p0, Lcom/samsung/groupcast/core/session/PackageSession;->mChannelMap:Ljava/util/Hashtable;

    invoke-virtual {v0, p1}, Ljava/util/Hashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 68
    return-void
.end method

.method public sendData(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[[B)V
    .locals 7
    .param p1, "channel"    # Ljava/lang/String;
    .param p2, "toNode"    # Ljava/lang/String;
    .param p3, "payloadType"    # Ljava/lang/String;
    .param p4, "payload"    # [[B

    .prologue
    .line 122
    iget-object v4, p0, Lcom/samsung/groupcast/core/session/PackageSession;->mChannelMap:Ljava/util/Hashtable;

    invoke-virtual {v4, p1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/chord/SchordChannel;

    .line 123
    .local v0, "c":Lcom/samsung/android/sdk/chord/SchordChannel;
    if-eqz v0, :cond_1

    .line 124
    const-string v4, "GPSDKv2_PS"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "sendData ch:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ",n:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    invoke-interface {v0}, Lcom/samsung/android/sdk/chord/SchordChannel;->getJoinedNodeList()Ljava/util/List;

    move-result-object v2

    .line 126
    .local v2, "l":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 127
    .local v3, "s":Ljava/lang/String;
    const-string v4, "GPSDKv2_PS"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "ch-:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 129
    .end local v3    # "s":Ljava/lang/String;
    :cond_0
    invoke-interface {v0, p2, p3, p4}, Lcom/samsung/android/sdk/chord/SchordChannel;->sendData(Ljava/lang/String;Ljava/lang/String;[[B)V

    .line 134
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "l":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :goto_1
    return-void

    .line 131
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/groupcast/core/session/PackageSession;->dumpChannels()V

    .line 132
    const-string v4, "GPSDKv2_PS"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "sendData no channel:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/groupcast/application/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public sendDataToAll(Ljava/lang/String;Ljava/lang/String;[[B)V
    .locals 4
    .param p1, "channel"    # Ljava/lang/String;
    .param p2, "payloadType"    # Ljava/lang/String;
    .param p3, "payload"    # [[B

    .prologue
    .line 137
    iget-object v1, p0, Lcom/samsung/groupcast/core/session/PackageSession;->mChannelMap:Ljava/util/Hashtable;

    invoke-virtual {v1, p1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/chord/SchordChannel;

    .line 138
    .local v0, "c":Lcom/samsung/android/sdk/chord/SchordChannel;
    if-eqz v0, :cond_0

    .line 139
    invoke-interface {v0, p2, p3}, Lcom/samsung/android/sdk/chord/SchordChannel;->sendDataToAll(Ljava/lang/String;[[B)V

    .line 144
    :goto_0
    return-void

    .line 141
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/groupcast/core/session/PackageSession;->dumpChannels()V

    .line 142
    const-string v1, "GPSDKv2_PS"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "sendDataToAll no channel:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/groupcast/application/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public verifyChannels()V
    .locals 3

    .prologue
    .line 79
    iget-object v2, p0, Lcom/samsung/groupcast/core/session/PackageSession;->mChannelNameList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 80
    .local v1, "s":Ljava/lang/String;
    invoke-static {}, Lcom/samsung/groupcast/core/messaging/MessagingClient;->getChordMgr()Lcom/samsung/android/sdk/chord/SchordManager;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/samsung/android/sdk/chord/SchordManager;->getJoinedChannel(Ljava/lang/String;)Lcom/samsung/android/sdk/chord/SchordChannel;

    move-result-object v2

    if-nez v2, :cond_0

    .line 81
    iget-object v2, p0, Lcom/samsung/groupcast/core/session/PackageSession;->mChannelMap:Ljava/util/Hashtable;

    invoke-virtual {v2, v1}, Ljava/util/Hashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 82
    iget-object v2, p0, Lcom/samsung/groupcast/core/session/PackageSession;->mChannelNameList:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    .line 85
    .end local v1    # "s":Ljava/lang/String;
    :cond_1
    return-void
.end method
