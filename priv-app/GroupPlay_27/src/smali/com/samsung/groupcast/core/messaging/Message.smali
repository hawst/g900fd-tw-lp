.class public interface abstract Lcom/samsung/groupcast/core/messaging/Message;
.super Ljava/lang/Object;
.source "Message.java"


# virtual methods
.method public abstract getPayload()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<[B>;"
        }
    .end annotation
.end method

.method public abstract getType()Ljava/lang/String;
.end method

.method public abstract notifyListener(Lcom/samsung/groupcast/core/messaging/ChannelListener;Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;)V
.end method
