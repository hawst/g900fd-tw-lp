.class Lcom/samsung/groupcast/core/messaging/MessagingClient$ChannelRecord;
.super Ljava/lang/Object;
.source "MessagingClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/groupcast/core/messaging/MessagingClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ChannelRecord"
.end annotation


# instance fields
.field private final mChannel:Lcom/samsung/groupcast/core/messaging/LocalChannel;

.field private mRefCount:I


# direct methods
.method public constructor <init>(Lcom/samsung/groupcast/core/messaging/LocalChannel;)V
    .locals 1
    .param p1, "channel"    # Lcom/samsung/groupcast/core/messaging/LocalChannel;

    .prologue
    .line 525
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 523
    const/4 v0, 0x1

    iput v0, p0, Lcom/samsung/groupcast/core/messaging/MessagingClient$ChannelRecord;->mRefCount:I

    .line 526
    iput-object p1, p0, Lcom/samsung/groupcast/core/messaging/MessagingClient$ChannelRecord;->mChannel:Lcom/samsung/groupcast/core/messaging/LocalChannel;

    .line 527
    return-void
.end method


# virtual methods
.method public decrementRefCount()I
    .locals 1

    .prologue
    .line 538
    iget v0, p0, Lcom/samsung/groupcast/core/messaging/MessagingClient$ChannelRecord;->mRefCount:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/samsung/groupcast/core/messaging/MessagingClient$ChannelRecord;->mRefCount:I

    return v0
.end method

.method public getChannel()Lcom/samsung/groupcast/core/messaging/LocalChannel;
    .locals 1

    .prologue
    .line 546
    iget-object v0, p0, Lcom/samsung/groupcast/core/messaging/MessagingClient$ChannelRecord;->mChannel:Lcom/samsung/groupcast/core/messaging/LocalChannel;

    return-object v0
.end method

.method public getRefCount()I
    .locals 1

    .prologue
    .line 530
    iget v0, p0, Lcom/samsung/groupcast/core/messaging/MessagingClient$ChannelRecord;->mRefCount:I

    return v0
.end method

.method public hasReferences()Z
    .locals 1

    .prologue
    .line 542
    iget v0, p0, Lcom/samsung/groupcast/core/messaging/MessagingClient$ChannelRecord;->mRefCount:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public incrementRefCount()I
    .locals 1

    .prologue
    .line 534
    iget v0, p0, Lcom/samsung/groupcast/core/messaging/MessagingClient$ChannelRecord;->mRefCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/samsung/groupcast/core/messaging/MessagingClient$ChannelRecord;->mRefCount:I

    return v0
.end method
