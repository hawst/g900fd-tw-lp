.class Lcom/samsung/groupcast/core/messaging/LocalChannel$1;
.super Ljava/lang/Object;
.source "LocalChannel.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/groupcast/core/messaging/LocalChannel;->updateParticipants(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;[Ljava/lang/String;Z[Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/groupcast/core/messaging/LocalChannel;

.field final synthetic val$channel:Lcom/samsung/groupcast/core/messaging/LocalChannel;

.field final synthetic val$key:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/samsung/groupcast/core/messaging/LocalChannel;Ljava/lang/String;Lcom/samsung/groupcast/core/messaging/LocalChannel;)V
    .locals 0

    .prologue
    .line 404
    iput-object p1, p0, Lcom/samsung/groupcast/core/messaging/LocalChannel$1;->this$0:Lcom/samsung/groupcast/core/messaging/LocalChannel;

    iput-object p2, p0, Lcom/samsung/groupcast/core/messaging/LocalChannel$1;->val$key:Ljava/lang/String;

    iput-object p3, p0, Lcom/samsung/groupcast/core/messaging/LocalChannel$1;->val$channel:Lcom/samsung/groupcast/core/messaging/LocalChannel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 407
    iget-object v2, p0, Lcom/samsung/groupcast/core/messaging/LocalChannel$1;->this$0:Lcom/samsung/groupcast/core/messaging/LocalChannel;

    # getter for: Lcom/samsung/groupcast/core/messaging/LocalChannel;->mParticipants:Ljava/util/concurrent/ConcurrentHashMap;
    invoke-static {v2}, Lcom/samsung/groupcast/core/messaging/LocalChannel;->access$000(Lcom/samsung/groupcast/core/messaging/LocalChannel;)Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/groupcast/core/messaging/LocalChannel$1;->val$key:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 408
    iget-object v2, p0, Lcom/samsung/groupcast/core/messaging/LocalChannel$1;->this$0:Lcom/samsung/groupcast/core/messaging/LocalChannel;

    # getter for: Lcom/samsung/groupcast/core/messaging/LocalChannel;->mParticipantLeaveEvents:Ljava/util/concurrent/ConcurrentHashMap;
    invoke-static {v2}, Lcom/samsung/groupcast/core/messaging/LocalChannel;->access$100(Lcom/samsung/groupcast/core/messaging/LocalChannel;)Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/groupcast/core/messaging/LocalChannel$1;->val$key:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 409
    iget-object v2, p0, Lcom/samsung/groupcast/core/messaging/LocalChannel$1;->this$0:Lcom/samsung/groupcast/core/messaging/LocalChannel;

    # getter for: Lcom/samsung/groupcast/core/messaging/LocalChannel;->mListeners:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/samsung/groupcast/core/messaging/LocalChannel;->access$200(Lcom/samsung/groupcast/core/messaging/LocalChannel;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/groupcast/core/messaging/ChannelListener;

    .line 410
    .local v1, "listener":Lcom/samsung/groupcast/core/messaging/ChannelListener;
    iget-object v2, p0, Lcom/samsung/groupcast/core/messaging/LocalChannel$1;->val$channel:Lcom/samsung/groupcast/core/messaging/LocalChannel;

    iget-object v3, p0, Lcom/samsung/groupcast/core/messaging/LocalChannel$1;->val$key:Ljava/lang/String;

    invoke-interface {v1, v2, v3}, Lcom/samsung/groupcast/core/messaging/ChannelListener;->onParticipantInfoChanged(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;)V

    goto :goto_0

    .line 412
    .end local v1    # "listener":Lcom/samsung/groupcast/core/messaging/ChannelListener;
    :cond_0
    return-void
.end method
