.class public abstract Lcom/samsung/groupcast/core/messaging/MessageCreator;
.super Ljava/lang/Object;
.source "MessageCreator.java"


# instance fields
.field private final mType:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "type"    # Ljava/lang/String;

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    iput-object p1, p0, Lcom/samsung/groupcast/core/messaging/MessageCreator;->mType:Ljava/lang/String;

    .line 10
    return-void
.end method


# virtual methods
.method public abstract createMessage(Ljava/util/List;)Lcom/samsung/groupcast/core/messaging/Message;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<[B>;)",
            "Lcom/samsung/groupcast/core/messaging/Message;"
        }
    .end annotation
.end method

.method public getType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 13
    iget-object v0, p0, Lcom/samsung/groupcast/core/messaging/MessageCreator;->mType:Ljava/lang/String;

    return-object v0
.end method
