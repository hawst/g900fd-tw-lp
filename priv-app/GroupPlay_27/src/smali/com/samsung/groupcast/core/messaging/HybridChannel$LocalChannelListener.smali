.class Lcom/samsung/groupcast/core/messaging/HybridChannel$LocalChannelListener;
.super Ljava/lang/Object;
.source "HybridChannel.java"

# interfaces
.implements Lcom/samsung/groupcast/core/messaging/ChannelListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/groupcast/core/messaging/HybridChannel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LocalChannelListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/groupcast/core/messaging/HybridChannel;


# direct methods
.method private constructor <init>(Lcom/samsung/groupcast/core/messaging/HybridChannel;)V
    .locals 0

    .prologue
    .line 429
    iput-object p1, p0, Lcom/samsung/groupcast/core/messaging/HybridChannel$LocalChannelListener;->this$0:Lcom/samsung/groupcast/core/messaging/HybridChannel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/groupcast/core/messaging/HybridChannel;Lcom/samsung/groupcast/core/messaging/HybridChannel$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/groupcast/core/messaging/HybridChannel;
    .param p2, "x1"    # Lcom/samsung/groupcast/core/messaging/HybridChannel$1;

    .prologue
    .line 429
    invoke-direct {p0, p1}, Lcom/samsung/groupcast/core/messaging/HybridChannel$LocalChannelListener;-><init>(Lcom/samsung/groupcast/core/messaging/HybridChannel;)V

    return-void
.end method


# virtual methods
.method public onCurrentPageInfoV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/CurrentPageInfoV1;)V
    .locals 5
    .param p1, "channel"    # Lcom/samsung/groupcast/core/messaging/Channel;
    .param p2, "node"    # Ljava/lang/String;
    .param p3, "message"    # Lcom/samsung/groupcast/legacy/gp2/messaging/v1/CurrentPageInfoV1;

    .prologue
    .line 502
    const-string v2, "HBC_L"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "on:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p3}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/CurrentPageInfoV1;->getType()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 503
    iget-object v2, p0, Lcom/samsung/groupcast/core/messaging/HybridChannel$LocalChannelListener;->this$0:Lcom/samsung/groupcast/core/messaging/HybridChannel;

    # getter for: Lcom/samsung/groupcast/core/messaging/HybridChannel;->mListeners:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/samsung/groupcast/core/messaging/HybridChannel;->access$100(Lcom/samsung/groupcast/core/messaging/HybridChannel;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/groupcast/core/messaging/ChannelListener;

    .line 504
    .local v1, "listener":Lcom/samsung/groupcast/core/messaging/ChannelListener;
    invoke-interface {v1, p1, p2, p3}, Lcom/samsung/groupcast/core/messaging/ChannelListener;->onCurrentPageInfoV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/CurrentPageInfoV1;)V

    goto :goto_0

    .line 506
    .end local v1    # "listener":Lcom/samsung/groupcast/core/messaging/ChannelListener;
    :cond_0
    return-void
.end method

.method public onCurrentPageRequestV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/CurrentPageRequestV1;)V
    .locals 5
    .param p1, "channel"    # Lcom/samsung/groupcast/core/messaging/Channel;
    .param p2, "node"    # Ljava/lang/String;
    .param p3, "message"    # Lcom/samsung/groupcast/legacy/gp2/messaging/v1/CurrentPageRequestV1;

    .prologue
    .line 493
    const-string v2, "HBC_L"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "on:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p3}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/CurrentPageRequestV1;->getType()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 494
    iget-object v2, p0, Lcom/samsung/groupcast/core/messaging/HybridChannel$LocalChannelListener;->this$0:Lcom/samsung/groupcast/core/messaging/HybridChannel;

    # getter for: Lcom/samsung/groupcast/core/messaging/HybridChannel;->mListeners:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/samsung/groupcast/core/messaging/HybridChannel;->access$100(Lcom/samsung/groupcast/core/messaging/HybridChannel;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/groupcast/core/messaging/ChannelListener;

    .line 495
    .local v1, "listener":Lcom/samsung/groupcast/core/messaging/ChannelListener;
    invoke-interface {v1, p1, p2, p3}, Lcom/samsung/groupcast/core/messaging/ChannelListener;->onCurrentPageRequestV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/CurrentPageRequestV1;)V

    goto :goto_0

    .line 498
    .end local v1    # "listener":Lcom/samsung/groupcast/core/messaging/ChannelListener;
    :cond_0
    return-void
.end method

.method public onDrawingSyncJSONResponseV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/DrawingSyncJsonResponseV1;)V
    .locals 5
    .param p1, "channel"    # Lcom/samsung/groupcast/core/messaging/Channel;
    .param p2, "node"    # Ljava/lang/String;
    .param p3, "message"    # Lcom/samsung/groupcast/legacy/gp2/messaging/v1/DrawingSyncJsonResponseV1;

    .prologue
    .line 611
    const-string v2, "HBC_L"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "on:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p3}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/DrawingSyncJsonResponseV1;->getType()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 612
    iget-object v2, p0, Lcom/samsung/groupcast/core/messaging/HybridChannel$LocalChannelListener;->this$0:Lcom/samsung/groupcast/core/messaging/HybridChannel;

    # getter for: Lcom/samsung/groupcast/core/messaging/HybridChannel;->mListeners:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/samsung/groupcast/core/messaging/HybridChannel;->access$100(Lcom/samsung/groupcast/core/messaging/HybridChannel;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/groupcast/core/messaging/ChannelListener;

    .line 613
    .local v1, "listener":Lcom/samsung/groupcast/core/messaging/ChannelListener;
    invoke-interface {v1, p1, p2, p3}, Lcom/samsung/groupcast/core/messaging/ChannelListener;->onDrawingSyncJSONResponseV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/DrawingSyncJsonResponseV1;)V

    goto :goto_0

    .line 615
    .end local v1    # "listener":Lcom/samsung/groupcast/core/messaging/ChannelListener;
    :cond_0
    return-void
.end method

.method public onDrawingSyncJsonRequest(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/DrawingSyncJsonRequestV1;)V
    .locals 5
    .param p1, "channel"    # Lcom/samsung/groupcast/core/messaging/Channel;
    .param p2, "node"    # Ljava/lang/String;
    .param p3, "message"    # Lcom/samsung/groupcast/legacy/gp2/messaging/v1/DrawingSyncJsonRequestV1;

    .prologue
    .line 602
    const-string v2, "HBC_L"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "on:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p3}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/DrawingSyncJsonRequestV1;->getType()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 603
    iget-object v2, p0, Lcom/samsung/groupcast/core/messaging/HybridChannel$LocalChannelListener;->this$0:Lcom/samsung/groupcast/core/messaging/HybridChannel;

    # getter for: Lcom/samsung/groupcast/core/messaging/HybridChannel;->mListeners:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/samsung/groupcast/core/messaging/HybridChannel;->access$100(Lcom/samsung/groupcast/core/messaging/HybridChannel;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/groupcast/core/messaging/ChannelListener;

    .line 604
    .local v1, "listener":Lcom/samsung/groupcast/core/messaging/ChannelListener;
    invoke-interface {v1, p1, p2, p3}, Lcom/samsung/groupcast/core/messaging/ChannelListener;->onDrawingSyncJsonRequest(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/DrawingSyncJsonRequestV1;)V

    goto :goto_0

    .line 606
    .end local v1    # "listener":Lcom/samsung/groupcast/core/messaging/ChannelListener;
    :cond_0
    return-void
.end method

.method public onDrawingSyncResponseV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/DrawingSyncNotificationV1;)V
    .locals 5
    .param p1, "channel"    # Lcom/samsung/groupcast/core/messaging/Channel;
    .param p2, "node"    # Ljava/lang/String;
    .param p3, "message"    # Lcom/samsung/groupcast/legacy/gp2/messaging/v1/DrawingSyncNotificationV1;

    .prologue
    .line 593
    const-string v2, "HBC_L"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "on:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p3}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/DrawingSyncNotificationV1;->getType()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 594
    iget-object v2, p0, Lcom/samsung/groupcast/core/messaging/HybridChannel$LocalChannelListener;->this$0:Lcom/samsung/groupcast/core/messaging/HybridChannel;

    # getter for: Lcom/samsung/groupcast/core/messaging/HybridChannel;->mListeners:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/samsung/groupcast/core/messaging/HybridChannel;->access$100(Lcom/samsung/groupcast/core/messaging/HybridChannel;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/groupcast/core/messaging/ChannelListener;

    .line 595
    .local v1, "listener":Lcom/samsung/groupcast/core/messaging/ChannelListener;
    invoke-interface {v1, p1, p2, p3}, Lcom/samsung/groupcast/core/messaging/ChannelListener;->onDrawingSyncResponseV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/DrawingSyncNotificationV1;)V

    goto :goto_0

    .line 597
    .end local v1    # "listener":Lcom/samsung/groupcast/core/messaging/ChannelListener;
    :cond_0
    return-void
.end method

.method public onFileAvailabilityInfoV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileAvailabilityInfoV1;)V
    .locals 5
    .param p1, "channel"    # Lcom/samsung/groupcast/core/messaging/Channel;
    .param p2, "node"    # Ljava/lang/String;
    .param p3, "message"    # Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileAvailabilityInfoV1;

    .prologue
    .line 529
    const-string v2, "HBC_L"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "on:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p3}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileAvailabilityInfoV1;->getType()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 530
    iget-object v2, p0, Lcom/samsung/groupcast/core/messaging/HybridChannel$LocalChannelListener;->this$0:Lcom/samsung/groupcast/core/messaging/HybridChannel;

    # getter for: Lcom/samsung/groupcast/core/messaging/HybridChannel;->mListeners:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/samsung/groupcast/core/messaging/HybridChannel;->access$100(Lcom/samsung/groupcast/core/messaging/HybridChannel;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/groupcast/core/messaging/ChannelListener;

    .line 531
    .local v1, "listener":Lcom/samsung/groupcast/core/messaging/ChannelListener;
    invoke-interface {v1, p1, p2, p3}, Lcom/samsung/groupcast/core/messaging/ChannelListener;->onFileAvailabilityInfoV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileAvailabilityInfoV1;)V

    goto :goto_0

    .line 534
    .end local v1    # "listener":Lcom/samsung/groupcast/core/messaging/ChannelListener;
    :cond_0
    return-void
.end method

.method public onFileOfferV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileOfferV1;)V
    .locals 5
    .param p1, "channel"    # Lcom/samsung/groupcast/core/messaging/Channel;
    .param p2, "node"    # Ljava/lang/String;
    .param p3, "message"    # Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileOfferV1;

    .prologue
    .line 556
    const-string v2, "HBC_L"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "on:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p3}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileOfferV1;->getType()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 557
    iget-object v2, p0, Lcom/samsung/groupcast/core/messaging/HybridChannel$LocalChannelListener;->this$0:Lcom/samsung/groupcast/core/messaging/HybridChannel;

    # getter for: Lcom/samsung/groupcast/core/messaging/HybridChannel;->mListeners:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/samsung/groupcast/core/messaging/HybridChannel;->access$100(Lcom/samsung/groupcast/core/messaging/HybridChannel;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/groupcast/core/messaging/ChannelListener;

    .line 558
    .local v1, "listener":Lcom/samsung/groupcast/core/messaging/ChannelListener;
    invoke-interface {v1, p1, p2, p3}, Lcom/samsung/groupcast/core/messaging/ChannelListener;->onFileOfferV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileOfferV1;)V

    goto :goto_0

    .line 561
    .end local v1    # "listener":Lcom/samsung/groupcast/core/messaging/ChannelListener;
    :cond_0
    return-void
.end method

.method public onFileReceiveFailureV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileOfferV1;)V
    .locals 4
    .param p1, "channel"    # Lcom/samsung/groupcast/core/messaging/Channel;
    .param p2, "node"    # Ljava/lang/String;
    .param p3, "offer"    # Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileOfferV1;

    .prologue
    .line 583
    const-string v2, "HBC_L"

    const-string v3, "on:FRF"

    invoke-static {v2, v3}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 584
    iget-object v2, p0, Lcom/samsung/groupcast/core/messaging/HybridChannel$LocalChannelListener;->this$0:Lcom/samsung/groupcast/core/messaging/HybridChannel;

    # getter for: Lcom/samsung/groupcast/core/messaging/HybridChannel;->mListeners:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/samsung/groupcast/core/messaging/HybridChannel;->access$100(Lcom/samsung/groupcast/core/messaging/HybridChannel;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/groupcast/core/messaging/ChannelListener;

    .line 585
    .local v1, "listener":Lcom/samsung/groupcast/core/messaging/ChannelListener;
    invoke-interface {v1, p1, p2, p3}, Lcom/samsung/groupcast/core/messaging/ChannelListener;->onFileReceiveFailureV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileOfferV1;)V

    goto :goto_0

    .line 588
    .end local v1    # "listener":Lcom/samsung/groupcast/core/messaging/ChannelListener;
    :cond_0
    return-void
.end method

.method public onFileReceiveProgressV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileOfferV1;JJ)V
    .locals 9
    .param p1, "channel"    # Lcom/samsung/groupcast/core/messaging/Channel;
    .param p2, "node"    # Ljava/lang/String;
    .param p3, "offer"    # Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileOfferV1;
    .param p4, "offset"    # J
    .param p6, "size"    # J

    .prologue
    .line 566
    const-string v1, "HBC_L"

    const-string v2, "on:FRP"

    invoke-static {v1, v2}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 567
    iget-object v1, p0, Lcom/samsung/groupcast/core/messaging/HybridChannel$LocalChannelListener;->this$0:Lcom/samsung/groupcast/core/messaging/HybridChannel;

    # getter for: Lcom/samsung/groupcast/core/messaging/HybridChannel;->mListeners:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/samsung/groupcast/core/messaging/HybridChannel;->access$100(Lcom/samsung/groupcast/core/messaging/HybridChannel;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .local v8, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/groupcast/core/messaging/ChannelListener;

    .local v0, "listener":Lcom/samsung/groupcast/core/messaging/ChannelListener;
    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-wide v4, p4

    move-wide v6, p6

    .line 568
    invoke-interface/range {v0 .. v7}, Lcom/samsung/groupcast/core/messaging/ChannelListener;->onFileReceiveProgressV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileOfferV1;JJ)V

    goto :goto_0

    .line 570
    .end local v0    # "listener":Lcom/samsung/groupcast/core/messaging/ChannelListener;
    :cond_0
    return-void
.end method

.method public onFileReceiveSuccessV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileOfferV1;Ljava/lang/String;)V
    .locals 4
    .param p1, "channel"    # Lcom/samsung/groupcast/core/messaging/Channel;
    .param p2, "node"    # Ljava/lang/String;
    .param p3, "offer"    # Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileOfferV1;
    .param p4, "tempPath"    # Ljava/lang/String;

    .prologue
    .line 575
    const-string v2, "HBC_L"

    const-string v3, "on:FRS"

    invoke-static {v2, v3}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 576
    iget-object v2, p0, Lcom/samsung/groupcast/core/messaging/HybridChannel$LocalChannelListener;->this$0:Lcom/samsung/groupcast/core/messaging/HybridChannel;

    # getter for: Lcom/samsung/groupcast/core/messaging/HybridChannel;->mListeners:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/samsung/groupcast/core/messaging/HybridChannel;->access$100(Lcom/samsung/groupcast/core/messaging/HybridChannel;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/groupcast/core/messaging/ChannelListener;

    .line 577
    .local v1, "listener":Lcom/samsung/groupcast/core/messaging/ChannelListener;
    invoke-interface {v1, p1, p2, p3, p4}, Lcom/samsung/groupcast/core/messaging/ChannelListener;->onFileReceiveSuccessV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileOfferV1;Ljava/lang/String;)V

    goto :goto_0

    .line 579
    .end local v1    # "listener":Lcom/samsung/groupcast/core/messaging/ChannelListener;
    :cond_0
    return-void
.end method

.method public onFileRequestV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileRequestV1;)V
    .locals 5
    .param p1, "channel"    # Lcom/samsung/groupcast/core/messaging/Channel;
    .param p2, "node"    # Ljava/lang/String;
    .param p3, "message"    # Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileRequestV1;

    .prologue
    .line 547
    const-string v2, "HBC_L"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "on:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p3}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileRequestV1;->getType()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 548
    iget-object v2, p0, Lcom/samsung/groupcast/core/messaging/HybridChannel$LocalChannelListener;->this$0:Lcom/samsung/groupcast/core/messaging/HybridChannel;

    # getter for: Lcom/samsung/groupcast/core/messaging/HybridChannel;->mListeners:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/samsung/groupcast/core/messaging/HybridChannel;->access$100(Lcom/samsung/groupcast/core/messaging/HybridChannel;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/groupcast/core/messaging/ChannelListener;

    .line 549
    .local v1, "listener":Lcom/samsung/groupcast/core/messaging/ChannelListener;
    invoke-interface {v1, p1, p2, p3}, Lcom/samsung/groupcast/core/messaging/ChannelListener;->onFileRequestV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileRequestV1;)V

    goto :goto_0

    .line 552
    .end local v1    # "listener":Lcom/samsung/groupcast/core/messaging/ChannelListener;
    :cond_0
    return-void
.end method

.method public onManifestInfoV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/ManifestInfoV1;)V
    .locals 5
    .param p1, "channel"    # Lcom/samsung/groupcast/core/messaging/Channel;
    .param p2, "node"    # Ljava/lang/String;
    .param p3, "message"    # Lcom/samsung/groupcast/legacy/gp2/messaging/v1/ManifestInfoV1;

    .prologue
    .line 519
    const-string v2, "HBC_L"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "on:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p3}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/ManifestInfoV1;->getType()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 520
    iget-object v2, p0, Lcom/samsung/groupcast/core/messaging/HybridChannel$LocalChannelListener;->this$0:Lcom/samsung/groupcast/core/messaging/HybridChannel;

    # getter for: Lcom/samsung/groupcast/core/messaging/HybridChannel;->mListeners:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/samsung/groupcast/core/messaging/HybridChannel;->access$100(Lcom/samsung/groupcast/core/messaging/HybridChannel;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/groupcast/core/messaging/ChannelListener;

    .line 521
    .local v1, "listener":Lcom/samsung/groupcast/core/messaging/ChannelListener;
    invoke-interface {v1, p1, p2, p3}, Lcom/samsung/groupcast/core/messaging/ChannelListener;->onManifestInfoV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/ManifestInfoV1;)V

    goto :goto_0

    .line 524
    .end local v1    # "listener":Lcom/samsung/groupcast/core/messaging/ChannelListener;
    :cond_0
    return-void
.end method

.method public onPageCommandV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageCommandV1;)V
    .locals 5
    .param p1, "channel"    # Lcom/samsung/groupcast/core/messaging/Channel;
    .param p2, "node"    # Ljava/lang/String;
    .param p3, "message"    # Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageCommandV1;

    .prologue
    .line 510
    const-string v2, "HBC_L"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "do:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p3}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageCommandV1;->getType()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 511
    iget-object v2, p0, Lcom/samsung/groupcast/core/messaging/HybridChannel$LocalChannelListener;->this$0:Lcom/samsung/groupcast/core/messaging/HybridChannel;

    # getter for: Lcom/samsung/groupcast/core/messaging/HybridChannel;->mListeners:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/samsung/groupcast/core/messaging/HybridChannel;->access$100(Lcom/samsung/groupcast/core/messaging/HybridChannel;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/groupcast/core/messaging/ChannelListener;

    .line 512
    .local v1, "listener":Lcom/samsung/groupcast/core/messaging/ChannelListener;
    invoke-interface {v1, p1, p2, p3}, Lcom/samsung/groupcast/core/messaging/ChannelListener;->onPageCommandV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageCommandV1;)V

    goto :goto_0

    .line 514
    .end local v1    # "listener":Lcom/samsung/groupcast/core/messaging/ChannelListener;
    :cond_0
    iget-object v2, p0, Lcom/samsung/groupcast/core/messaging/HybridChannel$LocalChannelListener;->this$0:Lcom/samsung/groupcast/core/messaging/HybridChannel;

    invoke-virtual {v2, p3}, Lcom/samsung/groupcast/core/messaging/HybridChannel;->repeatMsgToRemote(Lcom/samsung/groupcast/core/messaging/Message;)V

    .line 515
    return-void
.end method

.method public onPageEditV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageEditV1;)V
    .locals 5
    .param p1, "channel"    # Lcom/samsung/groupcast/core/messaging/Channel;
    .param p2, "node"    # Ljava/lang/String;
    .param p3, "message"    # Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageEditV1;

    .prologue
    .line 538
    const-string v2, "HBC_L"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "do:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p3}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageEditV1;->getType()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 539
    iget-object v2, p0, Lcom/samsung/groupcast/core/messaging/HybridChannel$LocalChannelListener;->this$0:Lcom/samsung/groupcast/core/messaging/HybridChannel;

    # getter for: Lcom/samsung/groupcast/core/messaging/HybridChannel;->mListeners:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/samsung/groupcast/core/messaging/HybridChannel;->access$100(Lcom/samsung/groupcast/core/messaging/HybridChannel;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/groupcast/core/messaging/ChannelListener;

    .line 540
    .local v1, "listener":Lcom/samsung/groupcast/core/messaging/ChannelListener;
    invoke-interface {v1, p1, p2, p3}, Lcom/samsung/groupcast/core/messaging/ChannelListener;->onPageEditV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageEditV1;)V

    goto :goto_0

    .line 542
    .end local v1    # "listener":Lcom/samsung/groupcast/core/messaging/ChannelListener;
    :cond_0
    iget-object v2, p0, Lcom/samsung/groupcast/core/messaging/HybridChannel$LocalChannelListener;->this$0:Lcom/samsung/groupcast/core/messaging/HybridChannel;

    invoke-virtual {v2, p3}, Lcom/samsung/groupcast/core/messaging/HybridChannel;->repeatMsgToRemote(Lcom/samsung/groupcast/core/messaging/Message;)V

    .line 543
    return-void
.end method

.method public onParticipantInfoChanged(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;)V
    .locals 4
    .param p1, "channel"    # Lcom/samsung/groupcast/core/messaging/Channel;
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    .line 448
    const-string v2, "HBC_L"

    const-string v3, "on:onParticipantInfoChanged"

    invoke-static {v2, v3}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 449
    iget-object v2, p0, Lcom/samsung/groupcast/core/messaging/HybridChannel$LocalChannelListener;->this$0:Lcom/samsung/groupcast/core/messaging/HybridChannel;

    # getter for: Lcom/samsung/groupcast/core/messaging/HybridChannel;->mListeners:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/samsung/groupcast/core/messaging/HybridChannel;->access$100(Lcom/samsung/groupcast/core/messaging/HybridChannel;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/groupcast/core/messaging/ChannelListener;

    .line 450
    .local v1, "listener":Lcom/samsung/groupcast/core/messaging/ChannelListener;
    invoke-interface {v1, p1, p2}, Lcom/samsung/groupcast/core/messaging/ChannelListener;->onParticipantInfoChanged(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;)V

    goto :goto_0

    .line 452
    .end local v1    # "listener":Lcom/samsung/groupcast/core/messaging/ChannelListener;
    :cond_0
    return-void
.end method

.method public onPeerJoined(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;)V
    .locals 4
    .param p1, "channel"    # Lcom/samsung/groupcast/core/messaging/Channel;
    .param p2, "node"    # Ljava/lang/String;

    .prologue
    .line 432
    const-string v2, "HBC_L"

    const-string v3, "on:peer joined"

    invoke-static {v2, v3}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 433
    iget-object v2, p0, Lcom/samsung/groupcast/core/messaging/HybridChannel$LocalChannelListener;->this$0:Lcom/samsung/groupcast/core/messaging/HybridChannel;

    # getter for: Lcom/samsung/groupcast/core/messaging/HybridChannel;->mListeners:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/samsung/groupcast/core/messaging/HybridChannel;->access$100(Lcom/samsung/groupcast/core/messaging/HybridChannel;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/groupcast/core/messaging/ChannelListener;

    .line 434
    .local v1, "listener":Lcom/samsung/groupcast/core/messaging/ChannelListener;
    invoke-interface {v1, p1, p2}, Lcom/samsung/groupcast/core/messaging/ChannelListener;->onPeerJoined(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;)V

    goto :goto_0

    .line 436
    .end local v1    # "listener":Lcom/samsung/groupcast/core/messaging/ChannelListener;
    :cond_0
    return-void
.end method

.method public onPeerLeft(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;)V
    .locals 4
    .param p1, "channel"    # Lcom/samsung/groupcast/core/messaging/Channel;
    .param p2, "node"    # Ljava/lang/String;

    .prologue
    .line 440
    const-string v2, "HBC_L"

    const-string v3, "on:peer left"

    invoke-static {v2, v3}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 441
    iget-object v2, p0, Lcom/samsung/groupcast/core/messaging/HybridChannel$LocalChannelListener;->this$0:Lcom/samsung/groupcast/core/messaging/HybridChannel;

    # getter for: Lcom/samsung/groupcast/core/messaging/HybridChannel;->mListeners:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/samsung/groupcast/core/messaging/HybridChannel;->access$100(Lcom/samsung/groupcast/core/messaging/HybridChannel;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/groupcast/core/messaging/ChannelListener;

    .line 442
    .local v1, "listener":Lcom/samsung/groupcast/core/messaging/ChannelListener;
    invoke-interface {v1, p1, p2}, Lcom/samsung/groupcast/core/messaging/ChannelListener;->onPeerLeft(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;)V

    goto :goto_0

    .line 444
    .end local v1    # "listener":Lcom/samsung/groupcast/core/messaging/ChannelListener;
    :cond_0
    return-void
.end method

.method public onPingV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PingV1;)V
    .locals 5
    .param p1, "channel"    # Lcom/samsung/groupcast/core/messaging/Channel;
    .param p2, "node"    # Ljava/lang/String;
    .param p3, "message"    # Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PingV1;

    .prologue
    .line 456
    const-string v2, "HBC_L"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "on:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p3}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PingV1;->getType()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 457
    iget-object v2, p0, Lcom/samsung/groupcast/core/messaging/HybridChannel$LocalChannelListener;->this$0:Lcom/samsung/groupcast/core/messaging/HybridChannel;

    # getter for: Lcom/samsung/groupcast/core/messaging/HybridChannel;->mListeners:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/samsung/groupcast/core/messaging/HybridChannel;->access$100(Lcom/samsung/groupcast/core/messaging/HybridChannel;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/groupcast/core/messaging/ChannelListener;

    .line 458
    .local v1, "listener":Lcom/samsung/groupcast/core/messaging/ChannelListener;
    invoke-interface {v1, p1, p2, p3}, Lcom/samsung/groupcast/core/messaging/ChannelListener;->onPingV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PingV1;)V

    goto :goto_0

    .line 460
    .end local v1    # "listener":Lcom/samsung/groupcast/core/messaging/ChannelListener;
    :cond_0
    iget-object v2, p0, Lcom/samsung/groupcast/core/messaging/HybridChannel$LocalChannelListener;->this$0:Lcom/samsung/groupcast/core/messaging/HybridChannel;

    invoke-virtual {v2, p3}, Lcom/samsung/groupcast/core/messaging/HybridChannel;->repeatMsgToRemote(Lcom/samsung/groupcast/core/messaging/Message;)V

    .line 461
    return-void
.end method

.method public onPongV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PongV1;)V
    .locals 5
    .param p1, "channel"    # Lcom/samsung/groupcast/core/messaging/Channel;
    .param p2, "node"    # Ljava/lang/String;
    .param p3, "message"    # Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PongV1;

    .prologue
    .line 465
    const-string v2, "HBC_L"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "on:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p3}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PongV1;->getType()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 466
    iget-object v2, p0, Lcom/samsung/groupcast/core/messaging/HybridChannel$LocalChannelListener;->this$0:Lcom/samsung/groupcast/core/messaging/HybridChannel;

    # getter for: Lcom/samsung/groupcast/core/messaging/HybridChannel;->mListeners:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/samsung/groupcast/core/messaging/HybridChannel;->access$100(Lcom/samsung/groupcast/core/messaging/HybridChannel;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/groupcast/core/messaging/ChannelListener;

    .line 467
    .local v1, "listener":Lcom/samsung/groupcast/core/messaging/ChannelListener;
    invoke-interface {v1, p1, p2, p3}, Lcom/samsung/groupcast/core/messaging/ChannelListener;->onPongV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PongV1;)V

    goto :goto_0

    .line 469
    .end local v1    # "listener":Lcom/samsung/groupcast/core/messaging/ChannelListener;
    :cond_0
    iget-object v2, p0, Lcom/samsung/groupcast/core/messaging/HybridChannel$LocalChannelListener;->this$0:Lcom/samsung/groupcast/core/messaging/HybridChannel;

    invoke-virtual {v2, p3}, Lcom/samsung/groupcast/core/messaging/HybridChannel;->repeatMsgToRemote(Lcom/samsung/groupcast/core/messaging/Message;)V

    .line 470
    return-void
.end method

.method public onSessionSummaryInfoV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/SessionSummaryInfoV1;)V
    .locals 5
    .param p1, "channel"    # Lcom/samsung/groupcast/core/messaging/Channel;
    .param p2, "node"    # Ljava/lang/String;
    .param p3, "message"    # Lcom/samsung/groupcast/legacy/gp2/messaging/v1/SessionSummaryInfoV1;

    .prologue
    .line 484
    const-string v2, "HBC_L"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "on:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p3}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/SessionSummaryInfoV1;->getType()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 485
    iget-object v2, p0, Lcom/samsung/groupcast/core/messaging/HybridChannel$LocalChannelListener;->this$0:Lcom/samsung/groupcast/core/messaging/HybridChannel;

    # getter for: Lcom/samsung/groupcast/core/messaging/HybridChannel;->mListeners:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/samsung/groupcast/core/messaging/HybridChannel;->access$100(Lcom/samsung/groupcast/core/messaging/HybridChannel;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/groupcast/core/messaging/ChannelListener;

    .line 486
    .local v1, "listener":Lcom/samsung/groupcast/core/messaging/ChannelListener;
    invoke-interface {v1, p1, p2, p3}, Lcom/samsung/groupcast/core/messaging/ChannelListener;->onSessionSummaryInfoV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/SessionSummaryInfoV1;)V

    goto :goto_0

    .line 488
    .end local v1    # "listener":Lcom/samsung/groupcast/core/messaging/ChannelListener;
    :cond_0
    return-void
.end method

.method public onSessionSummaryRequestV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/SessionSummaryRequestV1;)V
    .locals 5
    .param p1, "channel"    # Lcom/samsung/groupcast/core/messaging/Channel;
    .param p2, "node"    # Ljava/lang/String;
    .param p3, "message"    # Lcom/samsung/groupcast/legacy/gp2/messaging/v1/SessionSummaryRequestV1;

    .prologue
    .line 475
    const-string v2, "HBC_L"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "on:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p3}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/SessionSummaryRequestV1;->getType()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 476
    iget-object v2, p0, Lcom/samsung/groupcast/core/messaging/HybridChannel$LocalChannelListener;->this$0:Lcom/samsung/groupcast/core/messaging/HybridChannel;

    # getter for: Lcom/samsung/groupcast/core/messaging/HybridChannel;->mListeners:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/samsung/groupcast/core/messaging/HybridChannel;->access$100(Lcom/samsung/groupcast/core/messaging/HybridChannel;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/groupcast/core/messaging/ChannelListener;

    .line 477
    .local v1, "listener":Lcom/samsung/groupcast/core/messaging/ChannelListener;
    invoke-interface {v1, p1, p2, p3}, Lcom/samsung/groupcast/core/messaging/ChannelListener;->onSessionSummaryRequestV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/SessionSummaryRequestV1;)V

    goto :goto_0

    .line 479
    .end local v1    # "listener":Lcom/samsung/groupcast/core/messaging/ChannelListener;
    :cond_0
    return-void
.end method
