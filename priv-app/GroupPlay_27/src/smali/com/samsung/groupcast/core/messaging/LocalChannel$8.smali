.class Lcom/samsung/groupcast/core/messaging/LocalChannel$8;
.super Ljava/lang/Object;
.source "LocalChannel.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/groupcast/core/messaging/LocalChannel;->onFileReceived(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/groupcast/core/messaging/LocalChannel;

.field final synthetic val$fromNodeF:Ljava/lang/String;

.field final synthetic val$offer:Lcom/samsung/groupcast/core/messaging/FileOffer;

.field final synthetic val$tempPathF:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/samsung/groupcast/core/messaging/LocalChannel;Lcom/samsung/groupcast/core/messaging/FileOffer;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 709
    iput-object p1, p0, Lcom/samsung/groupcast/core/messaging/LocalChannel$8;->this$0:Lcom/samsung/groupcast/core/messaging/LocalChannel;

    iput-object p2, p0, Lcom/samsung/groupcast/core/messaging/LocalChannel$8;->val$offer:Lcom/samsung/groupcast/core/messaging/FileOffer;

    iput-object p3, p0, Lcom/samsung/groupcast/core/messaging/LocalChannel$8;->val$fromNodeF:Ljava/lang/String;

    iput-object p4, p0, Lcom/samsung/groupcast/core/messaging/LocalChannel$8;->val$tempPathF:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 712
    iget-object v2, p0, Lcom/samsung/groupcast/core/messaging/LocalChannel$8;->this$0:Lcom/samsung/groupcast/core/messaging/LocalChannel;

    # getter for: Lcom/samsung/groupcast/core/messaging/LocalChannel;->mListeners:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/samsung/groupcast/core/messaging/LocalChannel;->access$200(Lcom/samsung/groupcast/core/messaging/LocalChannel;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/groupcast/core/messaging/ChannelListener;

    .line 713
    .local v1, "listener":Lcom/samsung/groupcast/core/messaging/ChannelListener;
    iget-object v2, p0, Lcom/samsung/groupcast/core/messaging/LocalChannel$8;->val$offer:Lcom/samsung/groupcast/core/messaging/FileOffer;

    iget-object v3, p0, Lcom/samsung/groupcast/core/messaging/LocalChannel$8;->this$0:Lcom/samsung/groupcast/core/messaging/LocalChannel;

    iget-object v4, p0, Lcom/samsung/groupcast/core/messaging/LocalChannel$8;->val$fromNodeF:Ljava/lang/String;

    iget-object v5, p0, Lcom/samsung/groupcast/core/messaging/LocalChannel$8;->val$tempPathF:Ljava/lang/String;

    invoke-interface {v2, v1, v3, v4, v5}, Lcom/samsung/groupcast/core/messaging/FileOffer;->notifyListenerOfSuccess(Lcom/samsung/groupcast/core/messaging/ChannelListener;Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 715
    .end local v1    # "listener":Lcom/samsung/groupcast/core/messaging/ChannelListener;
    :cond_0
    return-void
.end method
