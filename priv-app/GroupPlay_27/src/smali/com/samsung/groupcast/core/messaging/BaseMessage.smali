.class public abstract Lcom/samsung/groupcast/core/messaging/BaseMessage;
.super Ljava/lang/Object;
.source "BaseMessage.java"

# interfaces
.implements Lcom/samsung/groupcast/core/messaging/Message;


# instance fields
.field private final mPayload:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<[B>;"
        }
    .end annotation
.end field

.field private mReadIndex:I

.field private final mType:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "type"    # Ljava/lang/String;

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/groupcast/core/messaging/BaseMessage;->mPayload:Ljava/util/ArrayList;

    .line 22
    iput-object p1, p0, Lcom/samsung/groupcast/core/messaging/BaseMessage;->mType:Ljava/lang/String;

    .line 23
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/util/List;)V
    .locals 1
    .param p1, "type"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<[B>;)V"
        }
    .end annotation

    .prologue
    .line 25
    .local p2, "payload":Ljava/util/List;, "Ljava/util/List<[B>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/groupcast/core/messaging/BaseMessage;->mPayload:Ljava/util/ArrayList;

    .line 26
    iput-object p1, p0, Lcom/samsung/groupcast/core/messaging/BaseMessage;->mType:Ljava/lang/String;

    .line 27
    iget-object v0, p0, Lcom/samsung/groupcast/core/messaging/BaseMessage;->mPayload:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 28
    return-void
.end method


# virtual methods
.method public getPayload()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<[B>;"
        }
    .end annotation

    .prologue
    .line 37
    iget-object v0, p0, Lcom/samsung/groupcast/core/messaging/BaseMessage;->mPayload:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/samsung/groupcast/core/messaging/BaseMessage;->mType:Ljava/lang/String;

    return-object v0
.end method

.method protected readByteArray()[B
    .locals 6

    .prologue
    .line 93
    const/4 v1, 0x0

    .line 95
    .local v1, "data":[B
    :try_start_0
    iget-object v3, p0, Lcom/samsung/groupcast/core/messaging/BaseMessage;->mPayload:Ljava/util/ArrayList;

    iget v4, p0, Lcom/samsung/groupcast/core/messaging/BaseMessage;->mReadIndex:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, Lcom/samsung/groupcast/core/messaging/BaseMessage;->mReadIndex:I

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, [B

    move-object v1, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 99
    :goto_0
    return-object v1

    .line 96
    :catch_0
    move-exception v2

    .line 97
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method protected readFloat()F
    .locals 4

    .prologue
    .line 148
    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/samsung/groupcast/core/messaging/BaseMessage;->mPayload:Ljava/util/ArrayList;

    iget v2, p0, Lcom/samsung/groupcast/core/messaging/BaseMessage;->mReadIndex:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/samsung/groupcast/core/messaging/BaseMessage;->mReadIndex:I

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [B

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([B)V

    .line 149
    .local v0, "string":Ljava/lang/String;
    invoke-static {v0}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    return v1
.end method

.method protected readInt()I
    .locals 4

    .prologue
    .line 138
    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/samsung/groupcast/core/messaging/BaseMessage;->mPayload:Ljava/util/ArrayList;

    iget v2, p0, Lcom/samsung/groupcast/core/messaging/BaseMessage;->mReadIndex:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/samsung/groupcast/core/messaging/BaseMessage;->mReadIndex:I

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [B

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([B)V

    .line 139
    .local v0, "string":Ljava/lang/String;
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    return v1
.end method

.method protected readLong()J
    .locals 4

    .prologue
    .line 143
    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/samsung/groupcast/core/messaging/BaseMessage;->mPayload:Ljava/util/ArrayList;

    iget v2, p0, Lcom/samsung/groupcast/core/messaging/BaseMessage;->mReadIndex:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/samsung/groupcast/core/messaging/BaseMessage;->mReadIndex:I

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [B

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([B)V

    .line 144
    .local v0, "string":Ljava/lang/String;
    invoke-static {v0}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    return-wide v1
.end method

.method protected readObject()Ljava/lang/Object;
    .locals 9

    .prologue
    .line 103
    const/4 v5, 0x0

    .line 104
    .local v5, "o":Ljava/lang/Object;
    iget-object v6, p0, Lcom/samsung/groupcast/core/messaging/BaseMessage;->mPayload:Ljava/util/ArrayList;

    iget v7, p0, Lcom/samsung/groupcast/core/messaging/BaseMessage;->mReadIndex:I

    add-int/lit8 v8, v7, 0x1

    iput v8, p0, Lcom/samsung/groupcast/core/messaging/BaseMessage;->mReadIndex:I

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [B

    .line 106
    .local v1, "bytes":[B
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "size:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    array-length v8, v1

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    new-instance v0, Ljava/io/ByteArrayInputStream;

    invoke-direct {v0, v1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 109
    .local v0, "bis":Ljava/io/ByteArrayInputStream;
    const/4 v3, 0x0

    .line 111
    .local v3, "in":Ljava/io/ObjectInput;
    :try_start_0
    new-instance v4, Ljava/io/ObjectInputStream;

    invoke-direct {v4, v0}, Ljava/io/ObjectInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 112
    .end local v3    # "in":Ljava/io/ObjectInput;
    .local v4, "in":Ljava/io/ObjectInput;
    :try_start_1
    invoke-interface {v4}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/ClassNotFoundException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v5

    .line 122
    :try_start_2
    invoke-virtual {v0}, Ljava/io/ByteArrayInputStream;->close()V

    .line 123
    invoke-interface {v4}, Ljava/io/ObjectInput;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-object v3, v4

    .line 129
    .end local v4    # "in":Ljava/io/ObjectInput;
    .end local v5    # "o":Ljava/lang/Object;
    .restart local v3    # "in":Ljava/io/ObjectInput;
    :goto_0
    return-object v5

    .line 124
    .end local v3    # "in":Ljava/io/ObjectInput;
    .restart local v4    # "in":Ljava/io/ObjectInput;
    .restart local v5    # "o":Ljava/lang/Object;
    :catch_0
    move-exception v2

    .line 126
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    move-object v3, v4

    .line 128
    .end local v4    # "in":Ljava/io/ObjectInput;
    .restart local v3    # "in":Ljava/io/ObjectInput;
    goto :goto_0

    .line 114
    .end local v2    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v2

    .line 116
    .local v2, "e":Ljava/lang/ClassNotFoundException;
    :goto_1
    :try_start_3
    invoke-virtual {v2}, Ljava/lang/ClassNotFoundException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 122
    :try_start_4
    invoke-virtual {v0}, Ljava/io/ByteArrayInputStream;->close()V

    .line 123
    invoke-interface {v3}, Ljava/io/ObjectInput;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 124
    :catch_2
    move-exception v2

    .line 126
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 117
    .end local v2    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v2

    .line 119
    .restart local v2    # "e":Ljava/io/IOException;
    :goto_2
    :try_start_5
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 122
    :try_start_6
    invoke-virtual {v0}, Ljava/io/ByteArrayInputStream;->close()V

    .line 123
    invoke-interface {v3}, Ljava/io/ObjectInput;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    goto :goto_0

    .line 124
    :catch_4
    move-exception v2

    .line 126
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 121
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v6

    .line 122
    :goto_3
    :try_start_7
    invoke-virtual {v0}, Ljava/io/ByteArrayInputStream;->close()V

    .line 123
    invoke-interface {v3}, Ljava/io/ObjectInput;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_5

    .line 127
    :goto_4
    throw v6

    .line 124
    :catch_5
    move-exception v2

    .line 126
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 121
    .end local v2    # "e":Ljava/io/IOException;
    .end local v3    # "in":Ljava/io/ObjectInput;
    .restart local v4    # "in":Ljava/io/ObjectInput;
    :catchall_1
    move-exception v6

    move-object v3, v4

    .end local v4    # "in":Ljava/io/ObjectInput;
    .restart local v3    # "in":Ljava/io/ObjectInput;
    goto :goto_3

    .line 117
    .end local v3    # "in":Ljava/io/ObjectInput;
    .restart local v4    # "in":Ljava/io/ObjectInput;
    :catch_6
    move-exception v2

    move-object v3, v4

    .end local v4    # "in":Ljava/io/ObjectInput;
    .restart local v3    # "in":Ljava/io/ObjectInput;
    goto :goto_2

    .line 114
    .end local v3    # "in":Ljava/io/ObjectInput;
    .restart local v4    # "in":Ljava/io/ObjectInput;
    :catch_7
    move-exception v2

    move-object v3, v4

    .end local v4    # "in":Ljava/io/ObjectInput;
    .restart local v3    # "in":Ljava/io/ObjectInput;
    goto :goto_1
.end method

.method protected readString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 133
    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/samsung/groupcast/core/messaging/BaseMessage;->mPayload:Ljava/util/ArrayList;

    iget v2, p0, Lcom/samsung/groupcast/core/messaging/BaseMessage;->mReadIndex:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/samsung/groupcast/core/messaging/BaseMessage;->mReadIndex:I

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [B

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([B)V

    .line 134
    .local v0, "string":Ljava/lang/String;
    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 154
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "type"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/samsung/groupcast/core/messaging/BaseMessage;->mType:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "payloadCount"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget-object v3, p0, Lcom/samsung/groupcast/core/messaging/BaseMessage;->mPayload:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/samsung/groupcast/misc/utility/StringTools;->getDebugString(Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected writeByteArray([B)V
    .locals 1
    .param p1, "data"    # [B

    .prologue
    .line 87
    if-eqz p1, :cond_0

    .line 88
    iget-object v0, p0, Lcom/samsung/groupcast/core/messaging/BaseMessage;->mPayload:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 90
    :cond_0
    return-void
.end method

.method protected writeFloat(F)V
    .locals 3
    .param p1, "value"    # F

    .prologue
    .line 59
    invoke-static {p1}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v0

    .line 60
    .local v0, "string":Ljava/lang/String;
    iget-object v1, p0, Lcom/samsung/groupcast/core/messaging/BaseMessage;->mPayload:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 61
    return-void
.end method

.method protected writeInt(I)V
    .locals 3
    .param p1, "value"    # I

    .prologue
    .line 49
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    .line 50
    .local v0, "string":Ljava/lang/String;
    iget-object v1, p0, Lcom/samsung/groupcast/core/messaging/BaseMessage;->mPayload:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 51
    return-void
.end method

.method protected writeLong(J)V
    .locals 3
    .param p1, "value"    # J

    .prologue
    .line 54
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    .line 55
    .local v0, "string":Ljava/lang/String;
    iget-object v1, p0, Lcom/samsung/groupcast/core/messaging/BaseMessage;->mPayload:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 56
    return-void
.end method

.method protected writeObject(Ljava/lang/Object;)V
    .locals 8
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 64
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 65
    .local v0, "bos":Ljava/io/ByteArrayOutputStream;
    const/4 v3, 0x0

    .line 67
    .local v3, "out":Ljava/io/ObjectOutput;
    :try_start_0
    new-instance v4, Ljava/io/ObjectOutputStream;

    invoke-direct {v4, v0}, Ljava/io/ObjectOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 68
    .end local v3    # "out":Ljava/io/ObjectOutput;
    .local v4, "out":Ljava/io/ObjectOutput;
    :try_start_1
    invoke-interface {v4, p1}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    .line 69
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    .line 70
    .local v1, "bytes":[B
    iget-object v5, p0, Lcom/samsung/groupcast/core/messaging/BaseMessage;->mPayload:Ljava/util/ArrayList;

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 71
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "size:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    array-length v7, v1

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 77
    :try_start_2
    invoke-interface {v4}, Ljava/io/ObjectOutput;->close()V

    .line 78
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-object v3, v4

    .line 84
    .end local v1    # "bytes":[B
    .end local v4    # "out":Ljava/io/ObjectOutput;
    .restart local v3    # "out":Ljava/io/ObjectOutput;
    :goto_0
    return-void

    .line 79
    .end local v3    # "out":Ljava/io/ObjectOutput;
    .restart local v1    # "bytes":[B
    .restart local v4    # "out":Ljava/io/ObjectOutput;
    :catch_0
    move-exception v2

    .line 81
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    move-object v3, v4

    .line 83
    .end local v4    # "out":Ljava/io/ObjectOutput;
    .restart local v3    # "out":Ljava/io/ObjectOutput;
    goto :goto_0

    .line 72
    .end local v1    # "bytes":[B
    .end local v2    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v2

    .line 74
    .restart local v2    # "e":Ljava/io/IOException;
    :goto_1
    :try_start_3
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 77
    :try_start_4
    invoke-interface {v3}, Ljava/io/ObjectOutput;->close()V

    .line 78
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 79
    :catch_2
    move-exception v2

    .line 81
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 76
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v5

    .line 77
    :goto_2
    :try_start_5
    invoke-interface {v3}, Ljava/io/ObjectOutput;->close()V

    .line 78
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 82
    :goto_3
    throw v5

    .line 79
    :catch_3
    move-exception v2

    .line 81
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 76
    .end local v2    # "e":Ljava/io/IOException;
    .end local v3    # "out":Ljava/io/ObjectOutput;
    .restart local v4    # "out":Ljava/io/ObjectOutput;
    :catchall_1
    move-exception v5

    move-object v3, v4

    .end local v4    # "out":Ljava/io/ObjectOutput;
    .restart local v3    # "out":Ljava/io/ObjectOutput;
    goto :goto_2

    .line 72
    .end local v3    # "out":Ljava/io/ObjectOutput;
    .restart local v4    # "out":Ljava/io/ObjectOutput;
    :catch_4
    move-exception v2

    move-object v3, v4

    .end local v4    # "out":Ljava/io/ObjectOutput;
    .restart local v3    # "out":Ljava/io/ObjectOutput;
    goto :goto_1
.end method

.method protected writeString(Ljava/lang/String;)V
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 41
    if-nez p1, :cond_0

    .line 42
    const-string p1, ""

    .line 45
    :cond_0
    iget-object v0, p0, Lcom/samsung/groupcast/core/messaging/BaseMessage;->mPayload:Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 46
    return-void
.end method
