.class public Lcom/samsung/groupcast/core/messaging/ConnectionHandle;
.super Ljava/lang/Object;
.source "ConnectionHandle.java"


# instance fields
.field private final mId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "requesterId"    # Ljava/lang/String;

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    const-string v0, "requesterId"

    invoke-static {v0, p1}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 10
    iput-object p1, p0, Lcom/samsung/groupcast/core/messaging/ConnectionHandle;->mId:Ljava/lang/String;

    .line 11
    return-void
.end method


# virtual methods
.method public getRequesterId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 14
    iget-object v0, p0, Lcom/samsung/groupcast/core/messaging/ConnectionHandle;->mId:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/samsung/groupcast/core/messaging/ConnectionHandle;->mId:Ljava/lang/String;

    return-object v0
.end method
