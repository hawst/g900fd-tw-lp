.class public Lcom/samsung/groupcast/core/session/PackageSessionManager;
.super Ljava/lang/Object;
.source "PackageSessionManager.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "GPSDKv2_PSM"

.field private static sInstance:Lcom/samsung/groupcast/core/session/PackageSessionManager;


# instance fields
.field private mChordConnection:Lcom/samsung/groupcast/core/messaging/ConnectionHandle;

.field private mSessionTable:Ljava/util/Hashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/groupcast/core/session/PackageSession;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    new-instance v0, Lcom/samsung/groupcast/core/session/PackageSessionManager;

    invoke-direct {v0}, Lcom/samsung/groupcast/core/session/PackageSessionManager;-><init>()V

    sput-object v0, Lcom/samsung/groupcast/core/session/PackageSessionManager;->sInstance:Lcom/samsung/groupcast/core/session/PackageSessionManager;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lcom/samsung/groupcast/core/session/PackageSessionManager;->mSessionTable:Ljava/util/Hashtable;

    .line 30
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/groupcast/core/session/PackageSessionManager;->mChordConnection:Lcom/samsung/groupcast/core/messaging/ConnectionHandle;

    .line 23
    return-void
.end method

.method private checkChordStatus()V
    .locals 2

    .prologue
    .line 40
    monitor-enter p0

    .line 41
    :try_start_0
    iget-object v0, p0, Lcom/samsung/groupcast/core/session/PackageSessionManager;->mChordConnection:Lcom/samsung/groupcast/core/messaging/ConnectionHandle;

    if-nez v0, :cond_0

    .line 42
    const-string v0, "GPSDKv2_PSM"

    const-string v1, "starting...connection"

    invoke-static {v0, v1}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 43
    invoke-static {}, Lcom/samsung/groupcast/core/messaging/MessagingClient;->GetInstance()Lcom/samsung/groupcast/core/messaging/MessagingClient;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/groupcast/core/messaging/MessagingClient;->acquireConnection(Ljava/lang/String;)Lcom/samsung/groupcast/core/messaging/ConnectionHandle;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/groupcast/core/session/PackageSessionManager;->mChordConnection:Lcom/samsung/groupcast/core/messaging/ConnectionHandle;

    .line 46
    :cond_0
    monitor-exit p0

    .line 47
    return-void

    .line 46
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private static debugLog(Ljava/lang/String;)V
    .locals 1
    .param p0, "msg"    # Ljava/lang/String;

    .prologue
    .line 34
    sget-boolean v0, Lcom/samsung/groupcast/debug/DebugFlags;->DEBUG_FLAG_BOOLEAN_PKGSESSIONMGR_LOG:Z

    if-eqz v0, :cond_0

    .line 35
    const-string v0, "GPSDKv2_PSM"

    invoke-static {v0, p0}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 37
    :cond_0
    return-void
.end method

.method public static getInstance()Lcom/samsung/groupcast/core/session/PackageSessionManager;
    .locals 1

    .prologue
    .line 26
    sget-object v0, Lcom/samsung/groupcast/core/session/PackageSessionManager;->sInstance:Lcom/samsung/groupcast/core/session/PackageSessionManager;

    return-object v0
.end method


# virtual methods
.method public addPackageSession(Ljava/lang/String;Lcom/samsung/groupcast/core/session/PackageSession;)V
    .locals 2
    .param p1, "pkgName"    # Ljava/lang/String;
    .param p2, "ps"    # Lcom/samsung/groupcast/core/session/PackageSession;

    .prologue
    .line 77
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "addPS:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",t:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/groupcast/core/session/PackageSessionManager;->mSessionTable:Ljava/util/Hashtable;

    invoke-virtual {v1}, Ljava/util/Hashtable;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/groupcast/core/session/PackageSessionManager;->debugLog(Ljava/lang/String;)V

    .line 78
    iget-object v0, p0, Lcom/samsung/groupcast/core/session/PackageSessionManager;->mSessionTable:Ljava/util/Hashtable;

    invoke-virtual {v0, p1, p2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 79
    return-void
.end method

.method public getPackageSession(Ljava/lang/String;)Lcom/samsung/groupcast/core/session/PackageSession;
    .locals 3
    .param p1, "pkgName"    # Ljava/lang/String;

    .prologue
    .line 65
    invoke-direct {p0}, Lcom/samsung/groupcast/core/session/PackageSessionManager;->checkChordStatus()V

    .line 66
    iget-object v1, p0, Lcom/samsung/groupcast/core/session/PackageSessionManager;->mSessionTable:Ljava/util/Hashtable;

    invoke-virtual {v1, p1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/groupcast/core/session/PackageSession;

    .line 67
    .local v0, "ps":Lcom/samsung/groupcast/core/session/PackageSession;
    if-nez v0, :cond_0

    .line 68
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ps is null for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/groupcast/core/session/PackageSessionManager;->debugLog(Ljava/lang/String;)V

    .line 69
    new-instance v0, Lcom/samsung/groupcast/core/session/PackageSession;

    .end local v0    # "ps":Lcom/samsung/groupcast/core/session/PackageSession;
    invoke-direct {v0, p1}, Lcom/samsung/groupcast/core/session/PackageSession;-><init>(Ljava/lang/String;)V

    .line 71
    .restart local v0    # "ps":Lcom/samsung/groupcast/core/session/PackageSession;
    invoke-virtual {p0, p1, v0}, Lcom/samsung/groupcast/core/session/PackageSessionManager;->addPackageSession(Ljava/lang/String;Lcom/samsung/groupcast/core/session/PackageSession;)V

    .line 73
    :cond_0
    return-object v0
.end method

.method public lostConnection()V
    .locals 3

    .prologue
    .line 52
    :try_start_0
    new-instance v1, Ljava/lang/Exception;

    const-string v2, "get callstack"

    invoke-direct {v1, v2}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 53
    :catch_0
    move-exception v0

    .line 54
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 56
    const-string v1, "GPSDKv2_PSM"

    const-string v2, "connection lost"

    invoke-static {v1, v2}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    monitor-enter p0

    .line 60
    const/4 v1, 0x0

    :try_start_1
    iput-object v1, p0, Lcom/samsung/groupcast/core/session/PackageSessionManager;->mChordConnection:Lcom/samsung/groupcast/core/messaging/ConnectionHandle;

    .line 61
    monitor-exit p0

    .line 62
    return-void

    .line 61
    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public removePackageSession(Ljava/lang/String;)V
    .locals 2
    .param p1, "pkgName"    # Ljava/lang/String;

    .prologue
    .line 82
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "rmPS:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",t:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/groupcast/core/session/PackageSessionManager;->mSessionTable:Ljava/util/Hashtable;

    invoke-virtual {v1}, Ljava/util/Hashtable;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/groupcast/core/session/PackageSessionManager;->debugLog(Ljava/lang/String;)V

    .line 83
    iget-object v0, p0, Lcom/samsung/groupcast/core/session/PackageSessionManager;->mSessionTable:Ljava/util/Hashtable;

    invoke-virtual {v0, p1}, Ljava/util/Hashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 84
    return-void
.end method
