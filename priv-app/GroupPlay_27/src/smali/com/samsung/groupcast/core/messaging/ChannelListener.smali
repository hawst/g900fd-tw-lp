.class public interface abstract Lcom/samsung/groupcast/core/messaging/ChannelListener;
.super Ljava/lang/Object;
.source "ChannelListener.java"


# virtual methods
.method public abstract onCurrentPageInfoV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/CurrentPageInfoV1;)V
.end method

.method public abstract onCurrentPageRequestV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/CurrentPageRequestV1;)V
.end method

.method public abstract onDrawingSyncJSONResponseV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/DrawingSyncJsonResponseV1;)V
.end method

.method public abstract onDrawingSyncJsonRequest(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/DrawingSyncJsonRequestV1;)V
.end method

.method public abstract onDrawingSyncResponseV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/DrawingSyncNotificationV1;)V
.end method

.method public abstract onFileAvailabilityInfoV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileAvailabilityInfoV1;)V
.end method

.method public abstract onFileOfferV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileOfferV1;)V
.end method

.method public abstract onFileReceiveFailureV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileOfferV1;)V
.end method

.method public abstract onFileReceiveProgressV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileOfferV1;JJ)V
.end method

.method public abstract onFileReceiveSuccessV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileOfferV1;Ljava/lang/String;)V
.end method

.method public abstract onFileRequestV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileRequestV1;)V
.end method

.method public abstract onManifestInfoV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/ManifestInfoV1;)V
.end method

.method public abstract onPageCommandV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageCommandV1;)V
.end method

.method public abstract onPageEditV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageEditV1;)V
.end method

.method public abstract onParticipantInfoChanged(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;)V
.end method

.method public abstract onPeerJoined(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;)V
.end method

.method public abstract onPeerLeft(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;)V
.end method

.method public abstract onPingV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PingV1;)V
.end method

.method public abstract onPongV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PongV1;)V
.end method

.method public abstract onSessionSummaryInfoV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/SessionSummaryInfoV1;)V
.end method

.method public abstract onSessionSummaryRequestV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/SessionSummaryRequestV1;)V
.end method
