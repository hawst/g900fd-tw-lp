.class public Lcom/samsung/groupcast/core/messaging/HybridChannel;
.super Ljava/lang/Object;
.source "HybridChannel.java"

# interfaces
.implements Lcom/samsung/groupcast/core/messaging/Channel;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/groupcast/core/messaging/HybridChannel$1;,
        Lcom/samsung/groupcast/core/messaging/HybridChannel$LocalChannelListener;
    }
.end annotation


# static fields
.field public static final HAS_FILEOFFER_ON_LOCAL:I = 0x1

.field public static final HAS_FILEOFFER_ON_REMOTE:I = 0x2

.field public static final NODE_TYPE_ALL:I = 0x300

.field public static final NODE_TYPE_LOCAL:I = 0x100

.field public static final NODE_TYPE_REMOTE:I = 0x200

.field private static final TAG:Ljava/lang/String; = "HBC"

.field private static final TAG_HBC_L:Ljava/lang/String; = "HBC_L"

.field private static final TAG_HBC_R:Ljava/lang/String; = "HBC_R"


# instance fields
.field private final mChannelId:Ljava/lang/String;

.field private mListeners:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/groupcast/core/messaging/ChannelListener;",
            ">;"
        }
    .end annotation
.end field

.field private mLocalChannel:Lcom/samsung/groupcast/core/messaging/LocalChannel;

.field private mLocalChannelListener:Lcom/samsung/groupcast/core/messaging/HybridChannel$LocalChannelListener;

.field private mLocalNodesByRemoteNodes:Ljava/util/concurrent/ConcurrentMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mRemoteChannel:Lcom/samsung/groupcast/core/messaging/Channel;

.field private mRemoteNodesByLocalNodes:Ljava/util/concurrent/ConcurrentMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 2
    .param p1, "channelId"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/groupcast/core/messaging/HybridChannel;->mRemoteNodesByLocalNodes:Ljava/util/concurrent/ConcurrentMap;

    .line 41
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/groupcast/core/messaging/HybridChannel;->mLocalNodesByRemoteNodes:Ljava/util/concurrent/ConcurrentMap;

    .line 44
    new-instance v0, Lcom/samsung/groupcast/core/messaging/HybridChannel$LocalChannelListener;

    invoke-direct {v0, p0, v1}, Lcom/samsung/groupcast/core/messaging/HybridChannel$LocalChannelListener;-><init>(Lcom/samsung/groupcast/core/messaging/HybridChannel;Lcom/samsung/groupcast/core/messaging/HybridChannel$1;)V

    iput-object v0, p0, Lcom/samsung/groupcast/core/messaging/HybridChannel;->mLocalChannelListener:Lcom/samsung/groupcast/core/messaging/HybridChannel$LocalChannelListener;

    .line 46
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/groupcast/core/messaging/HybridChannel;->mListeners:Ljava/util/ArrayList;

    .line 49
    iput-object v1, p0, Lcom/samsung/groupcast/core/messaging/HybridChannel;->mLocalChannel:Lcom/samsung/groupcast/core/messaging/LocalChannel;

    .line 50
    iput-object v1, p0, Lcom/samsung/groupcast/core/messaging/HybridChannel;->mRemoteChannel:Lcom/samsung/groupcast/core/messaging/Channel;

    .line 53
    iput-object p1, p0, Lcom/samsung/groupcast/core/messaging/HybridChannel;->mChannelId:Ljava/lang/String;

    .line 54
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mChannelId:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/groupcast/core/messaging/HybridChannel;->mChannelId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 56
    return-void
.end method

.method static synthetic access$100(Lcom/samsung/groupcast/core/messaging/HybridChannel;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/core/messaging/HybridChannel;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/samsung/groupcast/core/messaging/HybridChannel;->mListeners:Ljava/util/ArrayList;

    return-object v0
.end method


# virtual methods
.method public acceptFileOffer(Lcom/samsung/groupcast/core/messaging/FileOffer;)V
    .locals 3
    .param p1, "offer"    # Lcom/samsung/groupcast/core/messaging/FileOffer;

    .prologue
    .line 331
    const-string v0, "HBC"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "acceptFileOffer:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 332
    invoke-interface {p1}, Lcom/samsung/groupcast/core/messaging/FileOffer;->getFileInfo()Lcom/samsung/groupcast/core/messaging/FileInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/groupcast/core/messaging/FileInfo;->getDataKey()Lcom/samsung/groupcast/core/messaging/DataKey;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/groupcast/core/messaging/HybridChannel;->hasFileOffer(Lcom/samsung/groupcast/core/messaging/DataKey;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 343
    :goto_0
    return-void

    .line 334
    :pswitch_0
    const-string v0, "HBC"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "acceptFileOffer local:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 335
    iget-object v0, p0, Lcom/samsung/groupcast/core/messaging/HybridChannel;->mLocalChannel:Lcom/samsung/groupcast/core/messaging/LocalChannel;

    invoke-virtual {v0, p1}, Lcom/samsung/groupcast/core/messaging/LocalChannel;->acceptFileOffer(Lcom/samsung/groupcast/core/messaging/FileOffer;)V

    goto :goto_0

    .line 339
    :pswitch_1
    const-string v0, "HBC"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "acceptFileOffer remote:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 340
    iget-object v0, p0, Lcom/samsung/groupcast/core/messaging/HybridChannel;->mRemoteChannel:Lcom/samsung/groupcast/core/messaging/Channel;

    invoke-interface {v0, p1}, Lcom/samsung/groupcast/core/messaging/Channel;->acceptFileOffer(Lcom/samsung/groupcast/core/messaging/FileOffer;)V

    goto :goto_0

    .line 332
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public addListener(Lcom/samsung/groupcast/core/messaging/ChannelListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/samsung/groupcast/core/messaging/ChannelListener;

    .prologue
    .line 148
    if-nez p1, :cond_1

    .line 156
    :cond_0
    :goto_0
    return-void

    .line 152
    :cond_1
    iget-object v0, p0, Lcom/samsung/groupcast/core/messaging/HybridChannel;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 153
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/samsung/groupcast/core/messaging/HybridChannel;->mListeners:Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/samsung/groupcast/core/messaging/HybridChannel;->mListeners:Ljava/util/ArrayList;

    .line 154
    iget-object v0, p0, Lcom/samsung/groupcast/core/messaging/HybridChannel;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public cancelAcceptedFile(Lcom/samsung/groupcast/core/messaging/DataKey;)V
    .locals 3
    .param p1, "dataKey"    # Lcom/samsung/groupcast/core/messaging/DataKey;

    .prologue
    .line 347
    const-string v0, "HBC"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "cancelAcceptedFile:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 348
    invoke-virtual {p0, p1}, Lcom/samsung/groupcast/core/messaging/HybridChannel;->hasFileOffer(Lcom/samsung/groupcast/core/messaging/DataKey;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 360
    :goto_0
    return-void

    .line 350
    :pswitch_0
    const-string v0, "HBC"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "cancelAcceptedFile local:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 351
    iget-object v0, p0, Lcom/samsung/groupcast/core/messaging/HybridChannel;->mLocalChannel:Lcom/samsung/groupcast/core/messaging/LocalChannel;

    invoke-virtual {v0, p1}, Lcom/samsung/groupcast/core/messaging/LocalChannel;->cancelAcceptedFile(Lcom/samsung/groupcast/core/messaging/DataKey;)V

    goto :goto_0

    .line 355
    :pswitch_1
    const-string v0, "HBC"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "cancelAcceptedFile remote:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 356
    iget-object v0, p0, Lcom/samsung/groupcast/core/messaging/HybridChannel;->mRemoteChannel:Lcom/samsung/groupcast/core/messaging/Channel;

    invoke-interface {v0, p1}, Lcom/samsung/groupcast/core/messaging/Channel;->cancelAcceptedFile(Lcom/samsung/groupcast/core/messaging/DataKey;)V

    goto :goto_0

    .line 348
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public dumpUsers()V
    .locals 3

    .prologue
    .line 207
    :try_start_0
    const-string v0, "HBC"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mLocalNodesByRemoteNodes:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/groupcast/core/messaging/HybridChannel;->mLocalNodesByRemoteNodes:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v2}, Ljava/util/concurrent/ConcurrentMap;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/groupcast/core/messaging/HybridChannel;->mLocalNodesByRemoteNodes:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v2}, Ljava/util/concurrent/ConcurrentMap;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 209
    const-string v0, "HBC"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mRemoteNodesByLocalNodes:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/groupcast/core/messaging/HybridChannel;->mRemoteNodesByLocalNodes:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v2}, Ljava/util/concurrent/ConcurrentMap;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/groupcast/core/messaging/HybridChannel;->mRemoteNodesByLocalNodes:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v2}, Ljava/util/concurrent/ConcurrentMap;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 211
    const-string v0, "HBC"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "LocalPeer:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/samsung/groupcast/core/messaging/HybridChannel;->getPeers()Ljava/util/Collection;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 216
    :goto_0
    return-void

    .line 212
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public getChannelId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 131
    const-string v0, ""

    invoke-static {v0}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 132
    iget-object v0, p0, Lcom/samsung/groupcast/core/messaging/HybridChannel;->mLocalChannel:Lcom/samsung/groupcast/core/messaging/LocalChannel;

    if-eqz v0, :cond_0

    .line 133
    iget-object v0, p0, Lcom/samsung/groupcast/core/messaging/HybridChannel;->mLocalChannel:Lcom/samsung/groupcast/core/messaging/LocalChannel;

    invoke-virtual {v0}, Lcom/samsung/groupcast/core/messaging/LocalChannel;->getChannelId()Ljava/lang/String;

    move-result-object v0

    .line 135
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getListeners()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/groupcast/core/messaging/ChannelListener;",
            ">;"
        }
    .end annotation

    .prologue
    .line 169
    iget-object v0, p0, Lcom/samsung/groupcast/core/messaging/HybridChannel;->mListeners:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getLocalChannel()Lcom/samsung/groupcast/core/messaging/LocalChannel;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/samsung/groupcast/core/messaging/HybridChannel;->mLocalChannel:Lcom/samsung/groupcast/core/messaging/LocalChannel;

    return-object v0
.end method

.method public getNodeType(Ljava/lang/String;)I
    .locals 1
    .param p1, "node"    # Ljava/lang/String;

    .prologue
    .line 179
    const/16 v0, 0x100

    return v0
.end method

.method public getParticipantInfo(Ljava/lang/String;)Lcom/samsung/groupcast/legacy/gp2/session/model/ParticipantInfo;
    .locals 3
    .param p1, "participantName"    # Ljava/lang/String;

    .prologue
    .line 398
    const-string v1, ""

    invoke-static {v1}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 399
    const/4 v0, 0x0

    .line 401
    .local v0, "p":Lcom/samsung/groupcast/legacy/gp2/session/model/ParticipantInfo;
    iget-object v1, p0, Lcom/samsung/groupcast/core/messaging/HybridChannel;->mLocalChannel:Lcom/samsung/groupcast/core/messaging/LocalChannel;

    if-eqz v1, :cond_0

    .line 402
    iget-object v1, p0, Lcom/samsung/groupcast/core/messaging/HybridChannel;->mLocalChannel:Lcom/samsung/groupcast/core/messaging/LocalChannel;

    invoke-virtual {v1, p1}, Lcom/samsung/groupcast/core/messaging/LocalChannel;->getParticipantInfo(Ljava/lang/String;)Lcom/samsung/groupcast/legacy/gp2/session/model/ParticipantInfo;

    move-result-object v0

    .line 405
    :cond_0
    if-nez v0, :cond_1

    iget-object v1, p0, Lcom/samsung/groupcast/core/messaging/HybridChannel;->mRemoteChannel:Lcom/samsung/groupcast/core/messaging/Channel;

    if-eqz v1, :cond_1

    .line 406
    iget-object v1, p0, Lcom/samsung/groupcast/core/messaging/HybridChannel;->mRemoteChannel:Lcom/samsung/groupcast/core/messaging/Channel;

    invoke-interface {v1, p1}, Lcom/samsung/groupcast/core/messaging/Channel;->getParticipantInfo(Ljava/lang/String;)Lcom/samsung/groupcast/legacy/gp2/session/model/ParticipantInfo;

    move-result-object v0

    .line 409
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "p:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 411
    return-object v0
.end method

.method public getParticipantsIpInfo()Ljava/util/ArrayList;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 416
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 417
    .local v0, "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v1, p0, Lcom/samsung/groupcast/core/messaging/HybridChannel;->mLocalChannel:Lcom/samsung/groupcast/core/messaging/LocalChannel;

    if-eqz v1, :cond_0

    .line 418
    iget-object v1, p0, Lcom/samsung/groupcast/core/messaging/HybridChannel;->mLocalChannel:Lcom/samsung/groupcast/core/messaging/LocalChannel;

    invoke-virtual {v1}, Lcom/samsung/groupcast/core/messaging/LocalChannel;->getParticipantsIpInfo()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 420
    :cond_0
    iget-object v1, p0, Lcom/samsung/groupcast/core/messaging/HybridChannel;->mRemoteChannel:Lcom/samsung/groupcast/core/messaging/Channel;

    if-eqz v1, :cond_1

    .line 421
    iget-object v1, p0, Lcom/samsung/groupcast/core/messaging/HybridChannel;->mRemoteChannel:Lcom/samsung/groupcast/core/messaging/Channel;

    invoke-interface {v1}, Lcom/samsung/groupcast/core/messaging/Channel;->getParticipantsIpInfo()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 423
    :cond_1
    const-string v1, "HBC"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getParticipantsIpInfo:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 424
    return-object v0
.end method

.method public getPeerCount()I
    .locals 4

    .prologue
    .line 90
    const/4 v0, 0x0

    .line 91
    .local v0, "result":I
    iget-object v1, p0, Lcom/samsung/groupcast/core/messaging/HybridChannel;->mLocalChannel:Lcom/samsung/groupcast/core/messaging/LocalChannel;

    if-eqz v1, :cond_0

    .line 92
    iget-object v1, p0, Lcom/samsung/groupcast/core/messaging/HybridChannel;->mLocalChannel:Lcom/samsung/groupcast/core/messaging/LocalChannel;

    invoke-virtual {v1}, Lcom/samsung/groupcast/core/messaging/LocalChannel;->getPeerCount()I

    move-result v1

    add-int/2addr v0, v1

    .line 94
    sget-boolean v1, Lcom/samsung/groupcast/debug/DebugFlags;->DEBUG_FLAG_BOOLEAN_REMOTE_DETAILLOG:Z

    if-eqz v1, :cond_0

    .line 95
    const-string v1, "HBC"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getPeerCount(L):"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 99
    :cond_0
    iget-object v1, p0, Lcom/samsung/groupcast/core/messaging/HybridChannel;->mRemoteChannel:Lcom/samsung/groupcast/core/messaging/Channel;

    if-eqz v1, :cond_1

    .line 100
    iget-object v1, p0, Lcom/samsung/groupcast/core/messaging/HybridChannel;->mRemoteChannel:Lcom/samsung/groupcast/core/messaging/Channel;

    invoke-interface {v1}, Lcom/samsung/groupcast/core/messaging/Channel;->getPeerCount()I

    move-result v1

    add-int/2addr v0, v1

    .line 102
    sget-boolean v1, Lcom/samsung/groupcast/debug/DebugFlags;->DEBUG_FLAG_BOOLEAN_REMOTE_DETAILLOG:Z

    if-eqz v1, :cond_1

    .line 103
    const-string v1, "HBC"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getPeerCount(R):"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 107
    :cond_1
    sget-boolean v1, Lcom/samsung/groupcast/debug/DebugFlags;->DEBUG_FLAG_BOOLEAN_REMOTE_DETAILLOG:Z

    if-eqz v1, :cond_2

    .line 108
    const-string v1, "HBC"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getPeerCount(T):"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 110
    :cond_2
    return v0
.end method

.method public getPeers()Ljava/util/Collection;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 115
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 116
    .local v0, "result":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    iget-object v1, p0, Lcom/samsung/groupcast/core/messaging/HybridChannel;->mLocalChannel:Lcom/samsung/groupcast/core/messaging/LocalChannel;

    if-eqz v1, :cond_0

    .line 117
    iget-object v1, p0, Lcom/samsung/groupcast/core/messaging/HybridChannel;->mLocalChannel:Lcom/samsung/groupcast/core/messaging/LocalChannel;

    invoke-virtual {v1}, Lcom/samsung/groupcast/core/messaging/LocalChannel;->getPeers()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    .line 118
    const-string v1, "HBC"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getPeers(L):"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    :cond_0
    iget-object v1, p0, Lcom/samsung/groupcast/core/messaging/HybridChannel;->mRemoteChannel:Lcom/samsung/groupcast/core/messaging/Channel;

    if-eqz v1, :cond_1

    .line 122
    iget-object v1, p0, Lcom/samsung/groupcast/core/messaging/HybridChannel;->mRemoteChannel:Lcom/samsung/groupcast/core/messaging/Channel;

    invoke-interface {v1}, Lcom/samsung/groupcast/core/messaging/Channel;->getPeers()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    .line 123
    const-string v1, "HBC"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getPeers(R):"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    :cond_1
    const-string v1, "HBC"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getPeers(T):"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 126
    return-object v0
.end method

.method public getUserInfo()Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;
    .locals 2

    .prologue
    .line 75
    const-string v1, ""

    invoke-static {v1}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 83
    invoke-static {}, Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;->getInstance()Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;

    move-result-object v0

    .line 84
    .local v0, "ui":Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;
    invoke-virtual {v0}, Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;->updatePhoneNumberAtExtra()V

    .line 85
    return-object v0
.end method

.method public getUsersName()Ljava/util/Collection;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 381
    const-string v1, ""

    invoke-static {v1}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 382
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 383
    .local v0, "names":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v1, p0, Lcom/samsung/groupcast/core/messaging/HybridChannel;->mLocalChannel:Lcom/samsung/groupcast/core/messaging/LocalChannel;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/groupcast/core/messaging/HybridChannel;->mLocalChannel:Lcom/samsung/groupcast/core/messaging/LocalChannel;

    invoke-virtual {v1}, Lcom/samsung/groupcast/core/messaging/LocalChannel;->getUsersName()Ljava/util/Collection;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 384
    iget-object v1, p0, Lcom/samsung/groupcast/core/messaging/HybridChannel;->mLocalChannel:Lcom/samsung/groupcast/core/messaging/LocalChannel;

    invoke-virtual {v1}, Lcom/samsung/groupcast/core/messaging/LocalChannel;->getUsersName()Ljava/util/Collection;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 387
    :cond_0
    iget-object v1, p0, Lcom/samsung/groupcast/core/messaging/HybridChannel;->mRemoteChannel:Lcom/samsung/groupcast/core/messaging/Channel;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/samsung/groupcast/core/messaging/HybridChannel;->mRemoteChannel:Lcom/samsung/groupcast/core/messaging/Channel;

    invoke-interface {v1}, Lcom/samsung/groupcast/core/messaging/Channel;->getUsersName()Ljava/util/Collection;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 388
    iget-object v1, p0, Lcom/samsung/groupcast/core/messaging/HybridChannel;->mRemoteChannel:Lcom/samsung/groupcast/core/messaging/Channel;

    invoke-interface {v1}, Lcom/samsung/groupcast/core/messaging/Channel;->getUsersName()Ljava/util/Collection;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 391
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getUserName:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 393
    return-object v0
.end method

.method public hasFileOffer(Lcom/samsung/groupcast/core/messaging/DataKey;)I
    .locals 2
    .param p1, "dataKey"    # Lcom/samsung/groupcast/core/messaging/DataKey;

    .prologue
    .line 633
    const/4 v0, 0x0

    .line 634
    .local v0, "ret":I
    iget-object v1, p0, Lcom/samsung/groupcast/core/messaging/HybridChannel;->mLocalChannel:Lcom/samsung/groupcast/core/messaging/LocalChannel;

    if-eqz v1, :cond_0

    .line 635
    iget-object v1, p0, Lcom/samsung/groupcast/core/messaging/HybridChannel;->mLocalChannel:Lcom/samsung/groupcast/core/messaging/LocalChannel;

    invoke-virtual {v1, p1}, Lcom/samsung/groupcast/core/messaging/LocalChannel;->hasFileOffer(Lcom/samsung/groupcast/core/messaging/DataKey;)I

    move-result v0

    .line 638
    :cond_0
    if-nez v0, :cond_1

    iget-object v1, p0, Lcom/samsung/groupcast/core/messaging/HybridChannel;->mRemoteChannel:Lcom/samsung/groupcast/core/messaging/Channel;

    if-eqz v1, :cond_1

    .line 639
    iget-object v1, p0, Lcom/samsung/groupcast/core/messaging/HybridChannel;->mRemoteChannel:Lcom/samsung/groupcast/core/messaging/Channel;

    invoke-interface {v1, p1}, Lcom/samsung/groupcast/core/messaging/Channel;->hasFileOffer(Lcom/samsung/groupcast/core/messaging/DataKey;)I

    move-result v0

    .line 641
    :cond_1
    return v0
.end method

.method public isRemoteMessage(Lcom/samsung/groupcast/core/messaging/Message;)Z
    .locals 1
    .param p1, "message"    # Lcom/samsung/groupcast/core/messaging/Message;

    .prologue
    .line 264
    const/4 v0, 0x1

    .line 269
    .local v0, "result":Z
    return v0
.end method

.method public leave()V
    .locals 1

    .prologue
    .line 140
    const-string v0, ""

    invoke-static {v0}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 141
    iget-object v0, p0, Lcom/samsung/groupcast/core/messaging/HybridChannel;->mLocalChannel:Lcom/samsung/groupcast/core/messaging/LocalChannel;

    if-eqz v0, :cond_0

    .line 142
    iget-object v0, p0, Lcom/samsung/groupcast/core/messaging/HybridChannel;->mLocalChannel:Lcom/samsung/groupcast/core/messaging/LocalChannel;

    invoke-virtual {v0}, Lcom/samsung/groupcast/core/messaging/LocalChannel;->leave()V

    .line 144
    :cond_0
    return-void
.end method

.method public makePairWithLocalPeerToRemotePeer(Ljava/lang/String;Ljava/lang/String;)V
    .locals 10
    .param p1, "localNode"    # Ljava/lang/String;
    .param p2, "remotePeer"    # Ljava/lang/String;

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 183
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "LocalPeerToRemotePeer+"

    const-string v2, " "

    new-array v3, v9, [Ljava/lang/Object;

    const-string v4, "localNode"

    aput-object v4, v3, v5

    aput-object p1, v3, v6

    const-string v4, "remotePeer"

    aput-object v4, v3, v7

    aput-object p2, v3, v8

    invoke-static {v0, v1, v2, v3}, Lcom/samsung/groupcast/application/Logger;->wx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 185
    iget-object v0, p0, Lcom/samsung/groupcast/core/messaging/HybridChannel;->mRemoteNodesByLocalNodes:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0, p1, p2}, Ljava/util/concurrent/ConcurrentMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 186
    iget-object v0, p0, Lcom/samsung/groupcast/core/messaging/HybridChannel;->mLocalNodesByRemoteNodes:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0, p2, p1}, Ljava/util/concurrent/ConcurrentMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 187
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "LocalPeerToRemotePeer+"

    const-string v2, " "

    new-array v3, v9, [Ljava/lang/Object;

    const-string v4, "mRemoteNodesByLocalNodes"

    aput-object v4, v3, v5

    iget-object v4, p0, Lcom/samsung/groupcast/core/messaging/HybridChannel;->mRemoteNodesByLocalNodes:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v4}, Ljava/util/concurrent/ConcurrentMap;->size()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    const-string v4, "mLocalNodesByRemoteNodes"

    aput-object v4, v3, v7

    iget-object v4, p0, Lcom/samsung/groupcast/core/messaging/HybridChannel;->mLocalNodesByRemoteNodes:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v4}, Ljava/util/concurrent/ConcurrentMap;->size()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v8

    invoke-static {v0, v1, v2, v3}, Lcom/samsung/groupcast/application/Logger;->wx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 190
    return-void
.end method

.method public removeListener(Lcom/samsung/groupcast/core/messaging/ChannelListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/samsung/groupcast/core/messaging/ChannelListener;

    .prologue
    .line 160
    if-nez p1, :cond_0

    .line 165
    :goto_0
    return-void

    .line 163
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/samsung/groupcast/core/messaging/HybridChannel;->mListeners:Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/samsung/groupcast/core/messaging/HybridChannel;->mListeners:Ljava/util/ArrayList;

    .line 164
    iget-object v0, p0, Lcom/samsung/groupcast/core/messaging/HybridChannel;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public removeLocalPeer(Ljava/lang/String;)V
    .locals 9
    .param p1, "node"    # Ljava/lang/String;

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 193
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-string v2, "LocalPeerToRemotePeer- mRemoteNodesByLocalNodes"

    const-string v3, " "

    new-array v4, v8, [Ljava/lang/Object;

    const-string v5, "localNode"

    aput-object v5, v4, v6

    aput-object p1, v4, v7

    invoke-static {v1, v2, v3, v4}, Lcom/samsung/groupcast/application/Logger;->wx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 195
    iget-object v1, p0, Lcom/samsung/groupcast/core/messaging/HybridChannel;->mRemoteNodesByLocalNodes:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v1, p1}, Ljava/util/concurrent/ConcurrentMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 196
    .local v0, "remote":Ljava/lang/String;
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-string v2, "LocalPeerToRemotePeer- "

    const-string v3, " "

    new-array v4, v8, [Ljava/lang/Object;

    const-string v5, "remote"

    aput-object v5, v4, v6

    aput-object v0, v4, v7

    invoke-static {v1, v2, v3, v4}, Lcom/samsung/groupcast/application/Logger;->wx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 197
    if-eqz v0, :cond_0

    .line 198
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-string v2, "LocalPeerToRemotePeer- mLocalNodesByRemoteNodes"

    const-string v3, " "

    new-array v4, v8, [Ljava/lang/Object;

    const-string v5, "remote"

    aput-object v5, v4, v6

    aput-object v0, v4, v7

    invoke-static {v1, v2, v3, v4}, Lcom/samsung/groupcast/application/Logger;->wx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 200
    iget-object v1, p0, Lcom/samsung/groupcast/core/messaging/HybridChannel;->mLocalNodesByRemoteNodes:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v1, v0}, Ljava/util/concurrent/ConcurrentMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 202
    :cond_0
    return-void
.end method

.method public repeatMsgToLocal(Lcom/samsung/groupcast/core/messaging/Message;)V
    .locals 1
    .param p1, "message"    # Lcom/samsung/groupcast/core/messaging/Message;

    .prologue
    .line 226
    iget-object v0, p0, Lcom/samsung/groupcast/core/messaging/HybridChannel;->mLocalChannel:Lcom/samsung/groupcast/core/messaging/LocalChannel;

    if-eqz v0, :cond_0

    .line 227
    iget-object v0, p0, Lcom/samsung/groupcast/core/messaging/HybridChannel;->mLocalChannel:Lcom/samsung/groupcast/core/messaging/LocalChannel;

    invoke-virtual {v0, p1}, Lcom/samsung/groupcast/core/messaging/LocalChannel;->sendMessageToAll(Lcom/samsung/groupcast/core/messaging/Message;)V

    .line 229
    :cond_0
    return-void
.end method

.method public repeatMsgToRemote(Lcom/samsung/groupcast/core/messaging/Message;)V
    .locals 1
    .param p1, "message"    # Lcom/samsung/groupcast/core/messaging/Message;

    .prologue
    .line 219
    iget-object v0, p0, Lcom/samsung/groupcast/core/messaging/HybridChannel;->mRemoteChannel:Lcom/samsung/groupcast/core/messaging/Channel;

    if-eqz v0, :cond_0

    .line 220
    iget-object v0, p0, Lcom/samsung/groupcast/core/messaging/HybridChannel;->mRemoteChannel:Lcom/samsung/groupcast/core/messaging/Channel;

    invoke-interface {v0, p1}, Lcom/samsung/groupcast/core/messaging/Channel;->sendMessageToAll(Lcom/samsung/groupcast/core/messaging/Message;)V

    .line 223
    :cond_0
    return-void
.end method

.method public sendFileIssueToNode(Ljava/lang/String;Lcom/samsung/groupcast/core/messaging/FileIssue;J)V
    .locals 7
    .param p1, "node"    # Ljava/lang/String;
    .param p2, "issue"    # Lcom/samsung/groupcast/core/messaging/FileIssue;
    .param p3, "timeoutMilliseconds"    # J

    .prologue
    .line 302
    const-string v1, ""

    invoke-static {v1}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 303
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-string v2, "sendFileIssueToNode"

    const-string v3, "debug"

    const/4 v4, 0x4

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const-string v6, "node"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    aput-object p1, v4, v5

    const/4 v5, 0x2

    const-string v6, "File"

    aput-object v6, v4, v5

    const/4 v5, 0x3

    invoke-interface {p2}, Lcom/samsung/groupcast/core/messaging/FileIssue;->getFilePath()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v1, v2, v3, v4}, Lcom/samsung/groupcast/application/Logger;->wx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 307
    const/4 v0, 0x0

    .line 308
    .local v0, "c":Lcom/samsung/groupcast/core/messaging/Channel;
    invoke-virtual {p0, p1}, Lcom/samsung/groupcast/core/messaging/HybridChannel;->getNodeType(Ljava/lang/String;)I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 322
    :cond_0
    :goto_0
    const-string v1, "HBC"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "sendMsgToNode:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 324
    if-eqz v0, :cond_1

    .line 325
    invoke-interface {v0, p1, p2, p3, p4}, Lcom/samsung/groupcast/core/messaging/Channel;->sendFileIssueToNode(Ljava/lang/String;Lcom/samsung/groupcast/core/messaging/FileIssue;J)V

    .line 327
    :cond_1
    return-void

    .line 311
    :sswitch_0
    iget-object v1, p0, Lcom/samsung/groupcast/core/messaging/HybridChannel;->mLocalChannel:Lcom/samsung/groupcast/core/messaging/LocalChannel;

    if-eqz v1, :cond_0

    .line 312
    iget-object v0, p0, Lcom/samsung/groupcast/core/messaging/HybridChannel;->mLocalChannel:Lcom/samsung/groupcast/core/messaging/LocalChannel;

    goto :goto_0

    .line 317
    :sswitch_1
    iget-object v1, p0, Lcom/samsung/groupcast/core/messaging/HybridChannel;->mRemoteChannel:Lcom/samsung/groupcast/core/messaging/Channel;

    if-eqz v1, :cond_0

    .line 318
    iget-object v0, p0, Lcom/samsung/groupcast/core/messaging/HybridChannel;->mRemoteChannel:Lcom/samsung/groupcast/core/messaging/Channel;

    goto :goto_0

    .line 308
    nop

    :sswitch_data_0
    .sparse-switch
        0x100 -> :sswitch_0
        0x200 -> :sswitch_1
        0x300 -> :sswitch_0
    .end sparse-switch
.end method

.method public sendMessageToAll(Lcom/samsung/groupcast/core/messaging/Message;)V
    .locals 1
    .param p1, "message"    # Lcom/samsung/groupcast/core/messaging/Message;

    .prologue
    .line 276
    const-string v0, ""

    invoke-static {v0}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 277
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/samsung/groupcast/core/messaging/HybridChannel;->sendMessageToAll(Lcom/samsung/groupcast/core/messaging/Message;Z)V

    .line 278
    return-void
.end method

.method public sendMessageToAll(Lcom/samsung/groupcast/core/messaging/Message;Z)V
    .locals 6
    .param p1, "message"    # Lcom/samsung/groupcast/core/messaging/Message;
    .param p2, "log"    # Z

    .prologue
    .line 282
    invoke-virtual {p0}, Lcom/samsung/groupcast/core/messaging/HybridChannel;->dumpUsers()V

    .line 284
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "sendMessageToAll"

    const-string v2, "debug"

    const/4 v3, 0x6

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string v5, "this"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    aput-object p0, v3, v4

    const/4 v4, 0x2

    const-string v5, "local"

    aput-object v5, v3, v4

    const/4 v4, 0x3

    iget-object v5, p0, Lcom/samsung/groupcast/core/messaging/HybridChannel;->mLocalChannel:Lcom/samsung/groupcast/core/messaging/LocalChannel;

    aput-object v5, v3, v4

    const/4 v4, 0x4

    const-string v5, "remote"

    aput-object v5, v3, v4

    const/4 v4, 0x5

    iget-object v5, p0, Lcom/samsung/groupcast/core/messaging/HybridChannel;->mRemoteChannel:Lcom/samsung/groupcast/core/messaging/Channel;

    aput-object v5, v3, v4

    invoke-static {v0, v1, v2, v3}, Lcom/samsung/groupcast/application/Logger;->wx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 288
    iget-object v0, p0, Lcom/samsung/groupcast/core/messaging/HybridChannel;->mLocalChannel:Lcom/samsung/groupcast/core/messaging/LocalChannel;

    if-eqz v0, :cond_0

    .line 289
    iget-object v0, p0, Lcom/samsung/groupcast/core/messaging/HybridChannel;->mLocalChannel:Lcom/samsung/groupcast/core/messaging/LocalChannel;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/groupcast/core/messaging/LocalChannel;->sendMessageToAll(Lcom/samsung/groupcast/core/messaging/Message;Z)V

    .line 290
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Local sent"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-interface {p1}, Lcom/samsung/groupcast/core/messaging/Message;->getType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 293
    :cond_0
    iget-object v0, p0, Lcom/samsung/groupcast/core/messaging/HybridChannel;->mRemoteChannel:Lcom/samsung/groupcast/core/messaging/Channel;

    if-eqz v0, :cond_1

    .line 294
    iget-object v0, p0, Lcom/samsung/groupcast/core/messaging/HybridChannel;->mRemoteChannel:Lcom/samsung/groupcast/core/messaging/Channel;

    invoke-interface {v0, p1, p2}, Lcom/samsung/groupcast/core/messaging/Channel;->sendMessageToAll(Lcom/samsung/groupcast/core/messaging/Message;Z)V

    .line 295
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Remote sent:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-interface {p1}, Lcom/samsung/groupcast/core/messaging/Message;->getType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 298
    :cond_1
    return-void
.end method

.method public sendMessageToNode(Ljava/lang/String;Lcom/samsung/groupcast/core/messaging/Message;)V
    .locals 1
    .param p1, "node"    # Ljava/lang/String;
    .param p2, "message"    # Lcom/samsung/groupcast/core/messaging/Message;

    .prologue
    .line 233
    const-string v0, ""

    invoke-static {v0}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 234
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/samsung/groupcast/core/messaging/HybridChannel;->sendMessageToNode(Ljava/lang/String;Lcom/samsung/groupcast/core/messaging/Message;Z)V

    .line 235
    return-void
.end method

.method public sendMessageToNode(Ljava/lang/String;Lcom/samsung/groupcast/core/messaging/Message;Z)V
    .locals 4
    .param p1, "node"    # Ljava/lang/String;
    .param p2, "message"    # Lcom/samsung/groupcast/core/messaging/Message;
    .param p3, "log"    # Z

    .prologue
    .line 239
    const-string v1, ""

    invoke-static {v1}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 241
    const/4 v0, 0x0

    .line 242
    .local v0, "c":Lcom/samsung/groupcast/core/messaging/Channel;
    invoke-virtual {p0, p1}, Lcom/samsung/groupcast/core/messaging/HybridChannel;->getNodeType(Ljava/lang/String;)I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 256
    :cond_0
    :goto_0
    const-string v1, "HBC"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "sendMsgToNode:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 258
    if-eqz v0, :cond_1

    .line 259
    invoke-interface {v0, p1, p2, p3}, Lcom/samsung/groupcast/core/messaging/Channel;->sendMessageToNode(Ljava/lang/String;Lcom/samsung/groupcast/core/messaging/Message;Z)V

    .line 261
    :cond_1
    return-void

    .line 245
    :sswitch_0
    iget-object v1, p0, Lcom/samsung/groupcast/core/messaging/HybridChannel;->mLocalChannel:Lcom/samsung/groupcast/core/messaging/LocalChannel;

    if-eqz v1, :cond_0

    .line 246
    iget-object v0, p0, Lcom/samsung/groupcast/core/messaging/HybridChannel;->mLocalChannel:Lcom/samsung/groupcast/core/messaging/LocalChannel;

    goto :goto_0

    .line 251
    :sswitch_1
    iget-object v1, p0, Lcom/samsung/groupcast/core/messaging/HybridChannel;->mRemoteChannel:Lcom/samsung/groupcast/core/messaging/Channel;

    if-eqz v1, :cond_0

    .line 252
    iget-object v0, p0, Lcom/samsung/groupcast/core/messaging/HybridChannel;->mRemoteChannel:Lcom/samsung/groupcast/core/messaging/Channel;

    goto :goto_0

    .line 242
    nop

    :sswitch_data_0
    .sparse-switch
        0x100 -> :sswitch_0
        0x200 -> :sswitch_1
        0x300 -> :sswitch_0
    .end sparse-switch
.end method

.method public setLocalChannel(Lcom/samsung/groupcast/core/messaging/LocalChannel;)V
    .locals 3
    .param p1, "localChannel"    # Lcom/samsung/groupcast/core/messaging/LocalChannel;

    .prologue
    .line 64
    iput-object p1, p0, Lcom/samsung/groupcast/core/messaging/HybridChannel;->mLocalChannel:Lcom/samsung/groupcast/core/messaging/LocalChannel;

    .line 65
    iget-object v0, p0, Lcom/samsung/groupcast/core/messaging/HybridChannel;->mLocalChannel:Lcom/samsung/groupcast/core/messaging/LocalChannel;

    if-eqz v0, :cond_0

    .line 66
    iget-object v0, p0, Lcom/samsung/groupcast/core/messaging/HybridChannel;->mLocalChannel:Lcom/samsung/groupcast/core/messaging/LocalChannel;

    iget-object v1, p0, Lcom/samsung/groupcast/core/messaging/HybridChannel;->mLocalChannelListener:Lcom/samsung/groupcast/core/messaging/HybridChannel$LocalChannelListener;

    invoke-virtual {v0, v1}, Lcom/samsung/groupcast/core/messaging/LocalChannel;->addListener(Lcom/samsung/groupcast/core/messaging/ChannelListener;)V

    .line 68
    :cond_0
    const-string v0, "HBC"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setLocalChannelAs:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    return-void
.end method

.method public updateParticipants(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;[Ljava/lang/String;Z[Ljava/lang/String;)V
    .locals 8
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "pingId"    # Ljava/lang/Integer;
    .param p4, "wifiIp"    # Ljava/lang/String;
    .param p5, "joinedActivities"    # [Ljava/lang/String;
    .param p6, "contentsReset"    # Z
    .param p7, "joinedGames"    # [Ljava/lang/String;

    .prologue
    .line 365
    const-string v0, ""

    invoke-static {v0}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 366
    iget-object v0, p0, Lcom/samsung/groupcast/core/messaging/HybridChannel;->mLocalChannel:Lcom/samsung/groupcast/core/messaging/LocalChannel;

    if-eqz v0, :cond_0

    .line 367
    iget-object v0, p0, Lcom/samsung/groupcast/core/messaging/HybridChannel;->mLocalChannel:Lcom/samsung/groupcast/core/messaging/LocalChannel;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move v6, p6

    move-object v7, p7

    invoke-virtual/range {v0 .. v7}, Lcom/samsung/groupcast/core/messaging/LocalChannel;->updateParticipants(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;[Ljava/lang/String;Z[Ljava/lang/String;)V

    .line 371
    :cond_0
    iget-object v0, p0, Lcom/samsung/groupcast/core/messaging/HybridChannel;->mRemoteChannel:Lcom/samsung/groupcast/core/messaging/Channel;

    if-eqz v0, :cond_1

    .line 372
    iget-object v0, p0, Lcom/samsung/groupcast/core/messaging/HybridChannel;->mRemoteChannel:Lcom/samsung/groupcast/core/messaging/Channel;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move v6, p6

    move-object v7, p7

    invoke-interface/range {v0 .. v7}, Lcom/samsung/groupcast/core/messaging/Channel;->updateParticipants(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;[Ljava/lang/String;Z[Ljava/lang/String;)V

    .line 377
    :cond_1
    return-void
.end method
