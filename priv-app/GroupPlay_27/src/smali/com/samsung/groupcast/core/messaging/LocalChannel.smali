.class public Lcom/samsung/groupcast/core/messaging/LocalChannel;
.super Ljava/lang/Object;
.source "LocalChannel.java"

# interfaces
.implements Lcom/samsung/android/sdk/chord/SchordChannel$StatusListener;
.implements Lcom/samsung/groupcast/core/messaging/Channel;


# static fields
.field public static final PARTICIPANT_LEAVE_TIMEOUT:I = 0x2710


# instance fields
.field private final mChannelId:Ljava/lang/String;

.field private final mChord:Lcom/samsung/android/sdk/chord/SchordManager;

.field private final mExchangeIdFromFileInfo:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mFileOfferByDataKey:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/groupcast/core/messaging/FileOffer;",
            ">;"
        }
    .end annotation
.end field

.field private mListeners:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/groupcast/core/messaging/ChannelListener;",
            ">;"
        }
    .end annotation
.end field

.field private final mParticipantLeaveEvents:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private final mParticipants:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/groupcast/legacy/gp2/session/model/ParticipantInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final mPeers:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mUserInfo:Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;

.field private final mWifiUtils:Lcom/samsung/groupcast/net/wifi/WifiUtils;


# direct methods
.method public constructor <init>(Lcom/samsung/android/sdk/chord/SchordManager;Ljava/lang/String;)V
    .locals 1
    .param p1, "chord"    # Lcom/samsung/android/sdk/chord/SchordManager;
    .param p2, "channelID"    # Ljava/lang/String;

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/groupcast/core/messaging/LocalChannel;->mListeners:Ljava/util/ArrayList;

    .line 48
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/samsung/groupcast/core/messaging/LocalChannel;->mPeers:Ljava/util/HashSet;

    .line 49
    invoke-static {}, Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;->getInstance()Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/groupcast/core/messaging/LocalChannel;->mUserInfo:Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;

    .line 50
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/groupcast/core/messaging/LocalChannel;->mParticipants:Ljava/util/concurrent/ConcurrentHashMap;

    .line 52
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/groupcast/core/messaging/LocalChannel;->mParticipantLeaveEvents:Ljava/util/concurrent/ConcurrentHashMap;

    .line 54
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/groupcast/core/messaging/LocalChannel;->mExchangeIdFromFileInfo:Ljava/util/concurrent/ConcurrentHashMap;

    .line 56
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/groupcast/core/messaging/LocalChannel;->mFileOfferByDataKey:Ljava/util/concurrent/ConcurrentHashMap;

    .line 58
    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiUtils;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/groupcast/core/messaging/LocalChannel;->mWifiUtils:Lcom/samsung/groupcast/net/wifi/WifiUtils;

    .line 67
    const-string v0, "chord"

    invoke-static {v0, p1}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 68
    const-string v0, "channelID"

    invoke-static {v0, p2}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 69
    iput-object p1, p0, Lcom/samsung/groupcast/core/messaging/LocalChannel;->mChord:Lcom/samsung/android/sdk/chord/SchordManager;

    .line 70
    iput-object p2, p0, Lcom/samsung/groupcast/core/messaging/LocalChannel;->mChannelId:Ljava/lang/String;

    .line 74
    return-void
.end method

.method public static GetBytesArray([[B)Ljava/util/List;
    .locals 3
    .param p0, "data"    # [[B
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([[B)",
            "Ljava/util/List",
            "<[B>;"
        }
    .end annotation

    .prologue
    .line 244
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 245
    .local v1, "result":Ljava/util/List;, "Ljava/util/List<[B>;"
    if-eqz p0, :cond_0

    .line 246
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, p0

    if-ge v0, v2, :cond_0

    .line 247
    aget-object v2, p0, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 246
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 250
    .end local v0    # "i":I
    :cond_0
    return-object v1
.end method

.method public static GetBytesArray(Ljava/util/List;)[[B
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<[B>;)[[B"
        }
    .end annotation

    .prologue
    .local p0, "data":Ljava/util/List;, "Ljava/util/List<[B>;"
    const/4 v5, 0x1

    .line 225
    const/4 v1, 0x0

    check-cast v1, [[B

    .line 227
    .local v1, "result":[[B
    if-eqz p0, :cond_0

    .line 228
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v3

    if-nez v3, :cond_1

    .line 229
    const-string v3, "GetBytesArray"

    const-string v4, "data was empty!"

    invoke-static {v3, v4}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 230
    new-array v1, v5, [[B

    .line 231
    const/4 v3, 0x0

    new-array v4, v5, [B

    aput-object v4, v1, v3

    .line 240
    :cond_0
    return-object v1

    .line 233
    :cond_1
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v2

    .line 234
    .local v2, "rows":I
    new-array v1, v2, [[B

    .line 235
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v2, :cond_0

    .line 236
    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [B

    invoke-virtual {v3}, [B->clone()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [B

    aput-object v3, v1, v0

    .line 235
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/samsung/groupcast/core/messaging/LocalChannel;)Ljava/util/concurrent/ConcurrentHashMap;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/core/messaging/LocalChannel;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/samsung/groupcast/core/messaging/LocalChannel;->mParticipants:Ljava/util/concurrent/ConcurrentHashMap;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/groupcast/core/messaging/LocalChannel;)Ljava/util/concurrent/ConcurrentHashMap;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/core/messaging/LocalChannel;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/samsung/groupcast/core/messaging/LocalChannel;->mParticipantLeaveEvents:Ljava/util/concurrent/ConcurrentHashMap;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/groupcast/core/messaging/LocalChannel;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/core/messaging/LocalChannel;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/samsung/groupcast/core/messaging/LocalChannel;->mListeners:Ljava/util/ArrayList;

    return-object v0
.end method

.method public static getChannelIdForSession(Lcom/samsung/groupcast/legacy/gp2/session/model/Session;)Ljava/lang/String;
    .locals 2
    .param p0, "session"    # Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    .prologue
    .line 61
    const-string v0, "session"

    invoke-static {v0, p0}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 62
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "com.samsung.groupcast.Session_V"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->getVersion()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->getSessionId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getFileInfoFromTypeString(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p0, "type"    # Ljava/lang/String;

    .prologue
    .line 481
    const/4 v2, 0x0

    .line 482
    .local v2, "fileInfo":Ljava/lang/String;
    invoke-static {p0}, Lcom/samsung/groupcast/core/messaging/LocalChannel;->getProtocolFromTypeString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 483
    .local v0, "cmd":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v4

    if-ge v3, v4, :cond_0

    .line 484
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "&"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, ""

    invoke-virtual {p0, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    .line 486
    :try_start_0
    new-instance v3, Lcom/samsung/groupcast/core/messaging/FileInfo;

    invoke-direct {v3, v2}, Lcom/samsung/groupcast/core/messaging/FileInfo;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Lcom/samsung/groupcast/core/messaging/FileInfo;->getStringRepresentation()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 492
    :cond_0
    :goto_0
    return-object v2

    .line 487
    :catch_0
    move-exception v1

    .line 488
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static getProtocolFromTypeString(Ljava/lang/String;)Ljava/lang/String;
    .locals 9
    .param p0, "type"    # Ljava/lang/String;

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 469
    move-object v0, p0

    .line 470
    .local v0, "protocol":Ljava/lang/String;
    const-string v2, "&"

    invoke-virtual {p0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 471
    .local v1, "splitTypeData":[Ljava/lang/String;
    if-eqz v1, :cond_0

    array-length v2, v1

    if-lt v2, v8, :cond_0

    const-string v2, "&"

    invoke-virtual {p0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 473
    const-class v2, Lcom/samsung/groupcast/core/messaging/LocalChannel;

    const-string v3, "onDataReceived"

    const-string v4, "typeParsing"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const-string v6, "type"

    aput-object v6, v5, v7

    aget-object v6, v1, v7

    aput-object v6, v5, v8

    invoke-static {v2, v3, v4, v5}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 474
    aget-object v0, v1, v7

    .line 476
    :cond_0
    return-object v0
.end method

.method public static makeTypeStringWithFileInfo(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "cmd"    # Ljava/lang/String;
    .param p1, "data"    # Ljava/lang/String;

    .prologue
    .line 463
    const-string v0, ""

    .line 464
    .local v0, "result":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "&"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 465
    return-object v0
.end method


# virtual methods
.method public acceptFileOffer(Lcom/samsung/groupcast/core/messaging/FileOffer;)V
    .locals 14
    .param p1, "offer"    # Lcom/samsung/groupcast/core/messaging/FileOffer;

    .prologue
    .line 313
    invoke-interface {p1}, Lcom/samsung/groupcast/core/messaging/FileOffer;->getFileInfo()Lcom/samsung/groupcast/core/messaging/FileInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/groupcast/core/messaging/FileInfo;->getDataKey()Lcom/samsung/groupcast/core/messaging/DataKey;

    move-result-object v8

    .line 315
    .local v8, "dataKey":Lcom/samsung/groupcast/core/messaging/DataKey;
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v2, "acceptFileOffer"

    const-string v10, "check "

    const/4 v11, 0x6

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    const-string v13, "dataKey"

    aput-object v13, v11, v12

    const/4 v12, 0x1

    aput-object v8, v11, v12

    const/4 v12, 0x2

    const-string v13, "offer"

    aput-object v13, v11, v12

    const/4 v12, 0x3

    aput-object p1, v11, v12

    const/4 v12, 0x4

    const-string v13, "mFileOfferByDataKey size:"

    aput-object v13, v11, v12

    const/4 v12, 0x5

    iget-object v13, p0, Lcom/samsung/groupcast/core/messaging/LocalChannel;->mFileOfferByDataKey:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v13}, Ljava/util/concurrent/ConcurrentHashMap;->size()I

    move-result v13

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-static {v0, v2, v10, v11}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 318
    iget-object v0, p0, Lcom/samsung/groupcast/core/messaging/LocalChannel;->mFileOfferByDataKey:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v8}, Lcom/samsung/groupcast/core/messaging/DataKey;->getDataId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/concurrent/ConcurrentHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 319
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v2, "acceptFileOffer"

    const-string v10, "offer already accepted"

    const/4 v11, 0x4

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    const-string v13, "offer"

    aput-object v13, v11, v12

    const/4 v12, 0x1

    aput-object p1, v11, v12

    const/4 v12, 0x2

    const-string v13, "datakey"

    aput-object v13, v11, v12

    const/4 v12, 0x3

    aput-object v8, v11, v12

    invoke-static {v0, v2, v10, v11}, Lcom/samsung/groupcast/application/Logger;->wx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 347
    :goto_0
    return-void

    .line 324
    :cond_0
    iget-object v0, p0, Lcom/samsung/groupcast/core/messaging/LocalChannel;->mChord:Lcom/samsung/android/sdk/chord/SchordManager;

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/samsung/groupcast/core/messaging/LocalChannel;->getChordChannel()Lcom/samsung/android/sdk/chord/SchordChannel;

    move-result-object v0

    if-nez v0, :cond_2

    .line 325
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "there is null "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/samsung/groupcast/core/messaging/LocalChannel;->mChord:Lcom/samsung/android/sdk/chord/SchordManager;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/samsung/groupcast/core/messaging/LocalChannel;->getChordChannel()Lcom/samsung/android/sdk/chord/SchordChannel;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 328
    :cond_2
    invoke-interface {p1}, Lcom/samsung/groupcast/core/messaging/FileOffer;->getToken()Lcom/samsung/groupcast/core/messaging/FileOfferToken;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/groupcast/core/messaging/FileOfferToken;->getValue()Ljava/lang/String;

    move-result-object v1

    .line 329
    .local v1, "coreTranscationId":Ljava/lang/String;
    const-wide/16 v6, -0x1

    .line 330
    .local v6, "chunkTimeoutMsec":J
    const/4 v3, -0x1

    .line 331
    .local v3, "chunkRetries":I
    const-wide/32 v4, 0x20000

    .line 332
    .local v4, "chunkSize":J
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v2, "acceptFileOffer"

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    const-string v12, "offer"

    aput-object v12, v10, v11

    const/4 v11, 0x1

    aput-object p1, v10, v11

    invoke-static {v0, v2, p0, v10}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 335
    :try_start_0
    iget-object v0, p0, Lcom/samsung/groupcast/core/messaging/LocalChannel;->mFileOfferByDataKey:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v8}, Lcom/samsung/groupcast/core/messaging/DataKey;->getDataId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2, p1}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 336
    invoke-virtual {p0}, Lcom/samsung/groupcast/core/messaging/LocalChannel;->getChordChannel()Lcom/samsung/android/sdk/chord/SchordChannel;

    move-result-object v0

    long-to-int v2, v6

    invoke-interface/range {v0 .. v5}, Lcom/samsung/android/sdk/chord/SchordChannel;->acceptFile(Ljava/lang/String;IIJ)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 342
    :goto_1
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v2, "acceptFileOffer"

    const-string v10, "check "

    const/4 v11, 0x2

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    const-string v13, "dataKey.getDataId()"

    aput-object v13, v11, v12

    const/4 v12, 0x1

    invoke-virtual {v8}, Lcom/samsung/groupcast/core/messaging/DataKey;->getDataId()Ljava/lang/String;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-static {v0, v2, v10, v11}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 344
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v2, "acceptFileOffer"

    const-string v10, "check "

    const/4 v11, 0x2

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    const-string v13, "offer"

    aput-object v13, v11, v12

    const/4 v12, 0x1

    aput-object p1, v11, v12

    invoke-static {v0, v2, v10, v11}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 345
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v2, "acceptFileOffer"

    const-string v10, "check "

    const/4 v11, 0x2

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    const-string v13, "mFileOfferByDataKey size:"

    aput-object v13, v11, v12

    const/4 v12, 0x1

    iget-object v13, p0, Lcom/samsung/groupcast/core/messaging/LocalChannel;->mFileOfferByDataKey:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v13}, Ljava/util/concurrent/ConcurrentHashMap;->size()I

    move-result v13

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-static {v0, v2, v10, v11}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 338
    :catch_0
    move-exception v9

    .line 339
    .local v9, "e":Ljava/lang/Exception;
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v2, "acceptFileOffer"

    const-string v10, "exception"

    const/4 v11, 0x2

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    const-string v13, "e"

    aput-object v13, v11, v12

    const/4 v12, 0x1

    invoke-virtual {v9}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-static {v0, v2, v10, v11}, Lcom/samsung/groupcast/application/Logger;->wx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 340
    invoke-virtual {v9}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method public addListener(Lcom/samsung/groupcast/core/messaging/ChannelListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/samsung/groupcast/core/messaging/ChannelListener;

    .prologue
    .line 174
    if-nez p1, :cond_1

    .line 182
    :cond_0
    :goto_0
    return-void

    .line 178
    :cond_1
    iget-object v0, p0, Lcom/samsung/groupcast/core/messaging/LocalChannel;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 179
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/samsung/groupcast/core/messaging/LocalChannel;->mListeners:Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/samsung/groupcast/core/messaging/LocalChannel;->mListeners:Ljava/util/ArrayList;

    .line 180
    iget-object v0, p0, Lcom/samsung/groupcast/core/messaging/LocalChannel;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public cancelAcceptedFile(Lcom/samsung/groupcast/core/messaging/DataKey;)V
    .locals 12
    .param p1, "dataKey"    # Lcom/samsung/groupcast/core/messaging/DataKey;

    .prologue
    const/4 v11, 0x2

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 350
    iget-object v3, p0, Lcom/samsung/groupcast/core/messaging/LocalChannel;->mFileOfferByDataKey:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {p1}, Lcom/samsung/groupcast/core/messaging/DataKey;->getDataId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/groupcast/core/messaging/FileOffer;

    .line 352
    .local v2, "fileOffer":Lcom/samsung/groupcast/core/messaging/FileOffer;
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    const-string v4, "cancelAcceptedFile"

    const-string v5, "check "

    const/4 v6, 0x4

    new-array v6, v6, [Ljava/lang/Object;

    const-string v7, "dataKey"

    aput-object v7, v6, v9

    aput-object p1, v6, v10

    const-string v7, "mFileOfferByDataKey size:"

    aput-object v7, v6, v11

    const/4 v7, 0x3

    iget-object v8, p0, Lcom/samsung/groupcast/core/messaging/LocalChannel;->mFileOfferByDataKey:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v8}, Ljava/util/concurrent/ConcurrentHashMap;->size()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v3, v4, v5, v6}, Lcom/samsung/groupcast/application/Logger;->wx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 355
    if-nez v2, :cond_0

    .line 373
    :goto_0
    return-void

    .line 359
    :cond_0
    iget-object v3, p0, Lcom/samsung/groupcast/core/messaging/LocalChannel;->mChord:Lcom/samsung/android/sdk/chord/SchordManager;

    if-eqz v3, :cond_1

    invoke-virtual {p0}, Lcom/samsung/groupcast/core/messaging/LocalChannel;->getChordChannel()Lcom/samsung/android/sdk/chord/SchordChannel;

    move-result-object v3

    if-nez v3, :cond_2

    .line 360
    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "there is null "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/groupcast/core/messaging/LocalChannel;->mChord:Lcom/samsung/android/sdk/chord/SchordManager;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/samsung/groupcast/core/messaging/LocalChannel;->getChordChannel()Lcom/samsung/android/sdk/chord/SchordChannel;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 363
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    const-string v4, "cancelAcceptedFile"

    new-array v5, v11, [Ljava/lang/Object;

    const-string v6, "dataKey"

    aput-object v6, v5, v9

    aput-object p1, v5, v10

    invoke-static {v3, v4, p0, v5}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 364
    iget-object v3, p0, Lcom/samsung/groupcast/core/messaging/LocalChannel;->mExchangeIdFromFileInfo:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-interface {v2}, Lcom/samsung/groupcast/core/messaging/FileOffer;->getFileInfo()Lcom/samsung/groupcast/core/messaging/FileInfo;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/groupcast/core/messaging/FileInfo;->getStringRepresentation()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 366
    .local v1, "exchangeId":Ljava/lang/String;
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    const-string v4, "cancelAcceptedFile"

    const-string v5, "remove"

    new-array v6, v11, [Ljava/lang/Object;

    const-string v7, "mExchangeIdFromFileInfo"

    aput-object v7, v6, v9

    iget-object v7, p0, Lcom/samsung/groupcast/core/messaging/LocalChannel;->mExchangeIdFromFileInfo:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v7}, Ljava/util/concurrent/ConcurrentHashMap;->size()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v10

    invoke-static {v3, v4, v5, v6}, Lcom/samsung/groupcast/application/Logger;->wx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 369
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/groupcast/core/messaging/LocalChannel;->getChordChannel()Lcom/samsung/android/sdk/chord/SchordChannel;

    move-result-object v3

    invoke-interface {v3, v1}, Lcom/samsung/android/sdk/chord/SchordChannel;->cancelFile(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 370
    :catch_0
    move-exception v0

    .line 371
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public getChannelId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lcom/samsung/groupcast/core/messaging/LocalChannel;->mChannelId:Ljava/lang/String;

    return-object v0
.end method

.method public getChordChannel()Lcom/samsung/android/sdk/chord/SchordChannel;
    .locals 5

    .prologue
    .line 79
    iget-object v3, p0, Lcom/samsung/groupcast/core/messaging/LocalChannel;->mChord:Lcom/samsung/android/sdk/chord/SchordManager;

    if-eqz v3, :cond_1

    .line 80
    iget-object v3, p0, Lcom/samsung/groupcast/core/messaging/LocalChannel;->mChord:Lcom/samsung/android/sdk/chord/SchordManager;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/chord/SchordManager;->getJoinedChannelList()Ljava/util/List;

    move-result-object v2

    .line 81
    .local v2, "l":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/chord/SchordChannel;>;"
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/chord/SchordChannel;

    .line 82
    .local v0, "c":Lcom/samsung/android/sdk/chord/SchordChannel;
    invoke-interface {v0}, Lcom/samsung/android/sdk/chord/SchordChannel;->getName()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/groupcast/core/messaging/LocalChannel;->mChannelId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 86
    .end local v0    # "c":Lcom/samsung/android/sdk/chord/SchordChannel;
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "l":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/chord/SchordChannel;>;"
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getListeners()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/groupcast/core/messaging/ChannelListener;",
            ">;"
        }
    .end annotation

    .prologue
    .line 196
    iget-object v0, p0, Lcom/samsung/groupcast/core/messaging/LocalChannel;->mListeners:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getParticipantInfo(Ljava/lang/String;)Lcom/samsung/groupcast/legacy/gp2/session/model/ParticipantInfo;
    .locals 1
    .param p1, "participantName"    # Ljava/lang/String;

    .prologue
    .line 441
    if-nez p1, :cond_0

    .line 442
    const/4 v0, 0x0

    .line 443
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/groupcast/core/messaging/LocalChannel;->mParticipants:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/groupcast/legacy/gp2/session/model/ParticipantInfo;

    goto :goto_0
.end method

.method public getParticipantsIpInfo()Ljava/util/ArrayList;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 449
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 451
    .local v0, "ParticipantsWifiInfos":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v3, p0, Lcom/samsung/groupcast/core/messaging/LocalChannel;->mParticipants:Ljava/util/concurrent/ConcurrentHashMap;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/samsung/groupcast/core/messaging/LocalChannel;->mParticipants:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v3}, Ljava/util/concurrent/ConcurrentHashMap;->size()I

    move-result v3

    if-gtz v3, :cond_2

    .line 452
    :cond_0
    const/4 v0, 0x0

    .line 459
    .end local v0    # "ParticipantsWifiInfos":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_1
    return-object v0

    .line 454
    .restart local v0    # "ParticipantsWifiInfos":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_2
    iget-object v3, p0, Lcom/samsung/groupcast/core/messaging/LocalChannel;->mParticipants:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v3}, Ljava/util/concurrent/ConcurrentHashMap;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/session/model/ParticipantInfo;>;>;"
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 455
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 456
    .local v1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/session/model/ParticipantInfo;>;"
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/groupcast/legacy/gp2/session/model/ParticipantInfo;

    invoke-virtual {v3}, Lcom/samsung/groupcast/legacy/gp2/session/model/ParticipantInfo;->getWifiIp()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public getPeerCount()I
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lcom/samsung/groupcast/core/messaging/LocalChannel;->mPeers:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public getPeers()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 131
    iget-object v0, p0, Lcom/samsung/groupcast/core/messaging/LocalChannel;->mPeers:Ljava/util/HashSet;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableCollection(Ljava/util/Collection;)Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method public getUserInfo()Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/samsung/groupcast/core/messaging/LocalChannel;->mUserInfo:Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;

    return-object v0
.end method

.method public getUsersName()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 436
    iget-object v0, p0, Lcom/samsung/groupcast/core/messaging/LocalChannel;->mParticipants:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public hasFileOffer(Lcom/samsung/groupcast/core/messaging/DataKey;)I
    .locals 9
    .param p1, "dataKey"    # Lcom/samsung/groupcast/core/messaging/DataKey;

    .prologue
    const/4 v8, 0x2

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 850
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const-string v3, "hasFileOffer"

    const-string v4, "check "

    const/4 v5, 0x4

    new-array v5, v5, [Ljava/lang/Object;

    const-string v6, "dataKey"

    aput-object v6, v5, v1

    aput-object p1, v5, v0

    const-string v6, "mFileOfferByDataKey size:"

    aput-object v6, v5, v8

    const/4 v6, 0x3

    iget-object v7, p0, Lcom/samsung/groupcast/core/messaging/LocalChannel;->mFileOfferByDataKey:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v7}, Ljava/util/concurrent/ConcurrentHashMap;->size()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v2, v3, v4, v5}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 853
    iget-object v2, p0, Lcom/samsung/groupcast/core/messaging/LocalChannel;->mFileOfferByDataKey:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {p1}, Lcom/samsung/groupcast/core/messaging/DataKey;->getDataId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/concurrent/ConcurrentHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 854
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const-string v3, "hasFileOffer"

    const-string v4, "offer exists"

    new-array v5, v8, [Ljava/lang/Object;

    const-string v6, "datakey"

    aput-object v6, v5, v1

    aput-object p1, v5, v0

    invoke-static {v2, v3, v4, v5}, Lcom/samsung/groupcast/application/Logger;->wx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 857
    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public leave()V
    .locals 14

    .prologue
    const/4 v13, 0x4

    const/4 v12, 0x3

    const/4 v11, 0x2

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 142
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    const-string v5, "leave"

    invoke-static {v4, v5, p0}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;)V

    .line 144
    iget-object v4, p0, Lcom/samsung/groupcast/core/messaging/LocalChannel;->mChord:Lcom/samsung/android/sdk/chord/SchordManager;

    if-eqz v4, :cond_0

    invoke-virtual {p0}, Lcom/samsung/groupcast/core/messaging/LocalChannel;->getChordChannel()Lcom/samsung/android/sdk/chord/SchordChannel;

    move-result-object v4

    if-nez v4, :cond_1

    .line 145
    :cond_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "there is null "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/groupcast/core/messaging/LocalChannel;->mChord:Lcom/samsung/android/sdk/chord/SchordManager;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Lcom/samsung/groupcast/core/messaging/LocalChannel;->getChordChannel()Lcom/samsung/android/sdk/chord/SchordChannel;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 170
    :goto_0
    return-void

    .line 149
    :cond_1
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    const-string v5, "leave"

    const-string v6, "check "

    new-array v7, v13, [Ljava/lang/Object;

    const-string v8, "mFileOfferByDataKey"

    aput-object v8, v7, v9

    iget-object v8, p0, Lcom/samsung/groupcast/core/messaging/LocalChannel;->mFileOfferByDataKey:Ljava/util/concurrent/ConcurrentHashMap;

    aput-object v8, v7, v10

    const-string v8, "mFileOfferByDataKey size:"

    aput-object v8, v7, v11

    iget-object v8, p0, Lcom/samsung/groupcast/core/messaging/LocalChannel;->mFileOfferByDataKey:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v8}, Ljava/util/concurrent/ConcurrentHashMap;->size()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v12

    invoke-static {v4, v5, v6, v7}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 153
    iget-object v4, p0, Lcom/samsung/groupcast/core/messaging/LocalChannel;->mFileOfferByDataKey:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v4}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/groupcast/core/messaging/FileOffer;

    .line 154
    .local v3, "offer":Lcom/samsung/groupcast/core/messaging/FileOffer;
    iget-object v4, p0, Lcom/samsung/groupcast/core/messaging/LocalChannel;->mExchangeIdFromFileInfo:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-interface {v3}, Lcom/samsung/groupcast/core/messaging/FileOffer;->getFileInfo()Lcom/samsung/groupcast/core/messaging/FileInfo;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/groupcast/core/messaging/FileInfo;->getStringRepresentation()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 156
    .local v1, "exchangeId":Ljava/lang/String;
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    const-string v5, "leave"

    const-string v6, "remove"

    new-array v7, v13, [Ljava/lang/Object;

    const-string v8, "mExchangeIdFromFileInfo"

    aput-object v8, v7, v9

    iget-object v8, p0, Lcom/samsung/groupcast/core/messaging/LocalChannel;->mExchangeIdFromFileInfo:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v8}, Ljava/util/concurrent/ConcurrentHashMap;->size()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v10

    const-string v8, "exchangeId"

    aput-object v8, v7, v11

    aput-object v1, v7, v12

    invoke-static {v4, v5, v6, v7}, Lcom/samsung/groupcast/application/Logger;->wx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 158
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_2

    .line 160
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/groupcast/core/messaging/LocalChannel;->getChordChannel()Lcom/samsung/android/sdk/chord/SchordChannel;

    move-result-object v4

    invoke-interface {v4, v1}, Lcom/samsung/android/sdk/chord/SchordChannel;->cancelFile(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 161
    :catch_0
    move-exception v0

    .line 162
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 167
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v1    # "exchangeId":Ljava/lang/String;
    .end local v3    # "offer":Lcom/samsung/groupcast/core/messaging/FileOffer;
    :cond_3
    iget-object v4, p0, Lcom/samsung/groupcast/core/messaging/LocalChannel;->mChord:Lcom/samsung/android/sdk/chord/SchordManager;

    iget-object v5, p0, Lcom/samsung/groupcast/core/messaging/LocalChannel;->mChannelId:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/chord/SchordManager;->leaveChannel(Ljava/lang/String;)V

    .line 168
    iget-object v4, p0, Lcom/samsung/groupcast/core/messaging/LocalChannel;->mPeers:Ljava/util/HashSet;

    invoke-virtual {v4}, Ljava/util/HashSet;->clear()V

    .line 169
    iget-object v4, p0, Lcom/samsung/groupcast/core/messaging/LocalChannel;->mFileOfferByDataKey:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v4}, Ljava/util/concurrent/ConcurrentHashMap;->clear()V

    goto/16 :goto_0
.end method

.method public onDataReceived(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[[B)V
    .locals 4
    .param p1, "fromNode"    # Ljava/lang/String;
    .param p2, "fromChannel"    # Ljava/lang/String;
    .param p3, "type"    # Ljava/lang/String;
    .param p4, "payload"    # [[B

    .prologue
    .line 533
    invoke-static {p3}, Lcom/samsung/groupcast/core/messaging/LocalChannel;->getProtocolFromTypeString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 534
    .local v0, "cmd":Ljava/lang/String;
    invoke-static {p4}, Lcom/samsung/groupcast/core/messaging/LocalChannel;->GetBytesArray([[B)Ljava/util/List;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/samsung/groupcast/core/messaging/MessageFactory;->createMessage(Ljava/lang/String;Ljava/util/List;)Lcom/samsung/groupcast/core/messaging/Message;

    move-result-object v2

    .line 536
    .local v2, "message":Lcom/samsung/groupcast/core/messaging/Message;
    if-nez v2, :cond_0

    .line 549
    :goto_0
    return-void

    .line 540
    :cond_0
    move-object v1, p1

    .line 541
    .local v1, "fromNodeF":Ljava/lang/String;
    new-instance v3, Lcom/samsung/groupcast/core/messaging/LocalChannel$5;

    invoke-direct {v3, p0, v2, v1}, Lcom/samsung/groupcast/core/messaging/LocalChannel$5;-><init>(Lcom/samsung/groupcast/core/messaging/LocalChannel;Lcom/samsung/groupcast/core/messaging/Message;Ljava/lang/String;)V

    invoke-static {v3}, Lcom/samsung/groupcast/application/MainQueue;->post(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public onFileChunkReceived(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJ)V
    .locals 18
    .param p1, "fromNode"    # Ljava/lang/String;
    .param p2, "fromChannel"    # Ljava/lang/String;
    .param p3, "originalName"    # Ljava/lang/String;
    .param p4, "hash"    # Ljava/lang/String;
    .param p5, "type"    # Ljava/lang/String;
    .param p6, "exchangeId"    # Ljava/lang/String;
    .param p7, "fileSize"    # J
    .param p9, "offset"    # J

    .prologue
    .line 606
    const/4 v12, 0x0

    .line 608
    .local v12, "info":Lcom/samsung/groupcast/core/messaging/FileInfo;
    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-string v2, "onChunkReceived"

    const-string v14, "exchangeId parsing "

    const/4 v15, 0x4

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    const-string v17, "type"

    aput-object v17, v15, v16

    const/16 v16, 0x1

    aput-object p5, v15, v16

    const/16 v16, 0x2

    const-string v17, "exchangeId"

    aput-object v17, v15, v16

    const/16 v16, 0x3

    aput-object p6, v15, v16

    invoke-static {v1, v2, v14, v15}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 612
    invoke-static/range {p5 .. p5}, Lcom/samsung/groupcast/core/messaging/LocalChannel;->getFileInfoFromTypeString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 613
    .local v9, "data":Ljava/lang/String;
    if-nez v9, :cond_0

    .line 615
    move-object/from16 v9, p6

    .line 619
    :cond_0
    :try_start_0
    new-instance v13, Lcom/samsung/groupcast/core/messaging/FileInfo;

    invoke-direct {v13, v9}, Lcom/samsung/groupcast/core/messaging/FileInfo;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .end local v12    # "info":Lcom/samsung/groupcast/core/messaging/FileInfo;
    .local v13, "info":Lcom/samsung/groupcast/core/messaging/FileInfo;
    move-object v12, v13

    .line 626
    .end local v13    # "info":Lcom/samsung/groupcast/core/messaging/FileInfo;
    .restart local v12    # "info":Lcom/samsung/groupcast/core/messaging/FileInfo;
    :goto_0
    invoke-virtual {v12}, Lcom/samsung/groupcast/core/messaging/FileInfo;->getDataKey()Lcom/samsung/groupcast/core/messaging/DataKey;

    move-result-object v10

    .line 627
    .local v10, "dataKey":Lcom/samsung/groupcast/core/messaging/DataKey;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/groupcast/core/messaging/LocalChannel;->mFileOfferByDataKey:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v10}, Lcom/samsung/groupcast/core/messaging/DataKey;->getDataId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/groupcast/core/messaging/FileOffer;

    .line 629
    .local v3, "offer":Lcom/samsung/groupcast/core/messaging/FileOffer;
    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-string v2, "onChunkReceived"

    const-string v14, "check "

    const/4 v15, 0x6

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    const-string v17, "dataKey"

    aput-object v17, v15, v16

    const/16 v16, 0x1

    aput-object v10, v15, v16

    const/16 v16, 0x2

    const-string v17, "offer"

    aput-object v17, v15, v16

    const/16 v16, 0x3

    aput-object v3, v15, v16

    const/16 v16, 0x4

    const-string v17, "mFileOfferByDataKey size:"

    aput-object v17, v15, v16

    const/16 v16, 0x5

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/groupcast/core/messaging/LocalChannel;->mFileOfferByDataKey:Ljava/util/concurrent/ConcurrentHashMap;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/util/concurrent/ConcurrentHashMap;->size()I

    move-result v17

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    aput-object v17, v15, v16

    invoke-static {v1, v2, v14, v15}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 632
    if-nez v3, :cond_1

    .line 650
    :goto_1
    return-void

    .line 620
    .end local v3    # "offer":Lcom/samsung/groupcast/core/messaging/FileOffer;
    .end local v10    # "dataKey":Lcom/samsung/groupcast/core/messaging/DataKey;
    :catch_0
    move-exception v11

    .line 621
    .local v11, "e":Ljava/lang/Exception;
    invoke-virtual {v11}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 636
    .end local v11    # "e":Ljava/lang/Exception;
    .restart local v3    # "offer":Lcom/samsung/groupcast/core/messaging/FileOffer;
    .restart local v10    # "dataKey":Lcom/samsung/groupcast/core/messaging/DataKey;
    :cond_1
    move-object/from16 v4, p1

    .line 637
    .local v4, "fromNodeF":Ljava/lang/String;
    move-wide/from16 v7, p7

    .line 638
    .local v7, "fileSizeF":J
    move-wide/from16 v5, p9

    .line 640
    .local v5, "offsetF":J
    new-instance v1, Lcom/samsung/groupcast/core/messaging/LocalChannel$7;

    move-object/from16 v2, p0

    invoke-direct/range {v1 .. v8}, Lcom/samsung/groupcast/core/messaging/LocalChannel$7;-><init>(Lcom/samsung/groupcast/core/messaging/LocalChannel;Lcom/samsung/groupcast/core/messaging/FileOffer;Ljava/lang/String;JJ)V

    invoke-static {v1}, Lcom/samsung/groupcast/application/MainQueue;->post(Ljava/lang/Runnable;)V

    goto :goto_1
.end method

.method public onFileChunkSent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJJ)V
    .locals 0
    .param p1, "destination"    # Ljava/lang/String;
    .param p2, "channel"    # Ljava/lang/String;
    .param p3, "filename"    # Ljava/lang/String;
    .param p4, "hash"    # Ljava/lang/String;
    .param p5, "type"    # Ljava/lang/String;
    .param p6, "exchangeId"    # Ljava/lang/String;
    .param p7, "fileSize"    # J
    .param p9, "offset"    # J
    .param p11, "chunkSize"    # J

    .prologue
    .line 785
    return-void
.end method

.method public onFileFailed(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 17
    .param p1, "fromNode"    # Ljava/lang/String;
    .param p2, "fromChannel"    # Ljava/lang/String;
    .param p3, "originalName"    # Ljava/lang/String;
    .param p4, "hash"    # Ljava/lang/String;
    .param p5, "exchangeId"    # Ljava/lang/String;
    .param p6, "reason"    # I

    .prologue
    .line 723
    const/4 v7, 0x0

    .line 725
    .local v7, "info":Lcom/samsung/groupcast/core/messaging/FileInfo;
    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v11

    const-string v12, "onFileFailed"

    const-string v13, "exchangeId parsing "

    const/4 v14, 0x2

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    const-string v16, "exchangeId"

    aput-object v16, v14, v15

    const/4 v15, 0x1

    aput-object p5, v14, v15

    invoke-static {v11, v12, v13, v14}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 727
    move-object/from16 v4, p5

    .line 728
    .local v4, "fileInfo":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/groupcast/core/messaging/LocalChannel;->mExchangeIdFromFileInfo:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v11}, Ljava/util/concurrent/ConcurrentHashMap;->keySet()Ljava/util/Set;

    move-result-object v11

    invoke-interface {v11}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .local v6, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    .line 729
    .local v10, "s":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/groupcast/core/messaging/LocalChannel;->mExchangeIdFromFileInfo:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v11, v10}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    move-object/from16 v0, p5

    invoke-virtual {v11, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 730
    move-object v4, v10

    .line 731
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/groupcast/core/messaging/LocalChannel;->mExchangeIdFromFileInfo:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v11, v10}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 732
    .local v3, "eid":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v11

    const-string v12, "onFileFailed"

    const-string v13, "remove"

    const/4 v14, 0x4

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    const-string v16, "mExchangeIdFromFileInfo"

    aput-object v16, v14, v15

    const/4 v15, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/groupcast/core/messaging/LocalChannel;->mExchangeIdFromFileInfo:Ljava/util/concurrent/ConcurrentHashMap;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Ljava/util/concurrent/ConcurrentHashMap;->size()I

    move-result v16

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    aput-object v16, v14, v15

    const/4 v15, 0x2

    const-string v16, "eid:"

    aput-object v16, v14, v15

    const/4 v15, 0x3

    aput-object v3, v14, v15

    invoke-static {v11, v12, v13, v14}, Lcom/samsung/groupcast/application/Logger;->wx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 738
    .end local v3    # "eid":Ljava/lang/String;
    .end local v10    # "s":Ljava/lang/String;
    :cond_1
    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v11

    const-string v12, "onFileFailed"

    const-string v13, "exchangeId parsing "

    const/4 v14, 0x2

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    const-string v16, "fileInfo"

    aput-object v16, v14, v15

    const/4 v15, 0x1

    aput-object v4, v14, v15

    invoke-static {v11, v12, v13, v14}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 741
    :try_start_0
    new-instance v8, Lcom/samsung/groupcast/core/messaging/FileInfo;

    invoke-direct {v8, v4}, Lcom/samsung/groupcast/core/messaging/FileInfo;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .end local v7    # "info":Lcom/samsung/groupcast/core/messaging/FileInfo;
    .local v8, "info":Lcom/samsung/groupcast/core/messaging/FileInfo;
    move-object v7, v8

    .line 752
    .end local v8    # "info":Lcom/samsung/groupcast/core/messaging/FileInfo;
    .restart local v7    # "info":Lcom/samsung/groupcast/core/messaging/FileInfo;
    :goto_0
    if-nez v7, :cond_2

    .line 753
    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v11

    const-string v12, "info is null"

    invoke-static {v11, v12}, Lcom/samsung/groupcast/application/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 781
    :goto_1
    return-void

    .line 742
    :catch_0
    move-exception v2

    .line 748
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v11

    const-string v12, "onFileFailed"

    const-string v13, "exchangeId parsing failed"

    const/4 v14, 0x2

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    const-string v16, "exchangeId"

    aput-object v16, v14, v15

    const/4 v15, 0x1

    aput-object v4, v14, v15

    invoke-static {v11, v12, v13, v14}, Lcom/samsung/groupcast/application/Logger;->wx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_0

    .line 757
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_2
    invoke-virtual {v7}, Lcom/samsung/groupcast/core/messaging/FileInfo;->getDataKey()Lcom/samsung/groupcast/core/messaging/DataKey;

    move-result-object v1

    .line 758
    .local v1, "dataKey":Lcom/samsung/groupcast/core/messaging/DataKey;
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/groupcast/core/messaging/LocalChannel;->mFileOfferByDataKey:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1}, Lcom/samsung/groupcast/core/messaging/DataKey;->getDataId()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/samsung/groupcast/core/messaging/FileOffer;

    .line 759
    .local v9, "offer":Lcom/samsung/groupcast/core/messaging/FileOffer;
    move-object/from16 v5, p1

    .line 761
    .local v5, "fromNodeF":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v11

    const-string v12, "onFileFailed"

    const-string v13, "check "

    const/4 v14, 0x6

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    const-string v16, "dataKey"

    aput-object v16, v14, v15

    const/4 v15, 0x1

    aput-object v1, v14, v15

    const/4 v15, 0x2

    const-string v16, "offer"

    aput-object v16, v14, v15

    const/4 v15, 0x3

    aput-object v9, v14, v15

    const/4 v15, 0x4

    const-string v16, "mFileOfferByDataKey size:"

    aput-object v16, v14, v15

    const/4 v15, 0x5

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/groupcast/core/messaging/LocalChannel;->mFileOfferByDataKey:Ljava/util/concurrent/ConcurrentHashMap;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Ljava/util/concurrent/ConcurrentHashMap;->size()I

    move-result v16

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    aput-object v16, v14, v15

    invoke-static {v11, v12, v13, v14}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 764
    if-nez v9, :cond_3

    .line 765
    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v11

    const-string v12, "onFileFailed"

    const-string v13, "no accepted offer for received file"

    const/4 v14, 0x2

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    const-string v16, "info"

    aput-object v16, v14, v15

    const/4 v15, 0x1

    aput-object v7, v14, v15

    invoke-static {v11, v12, v13, v14}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_1

    .line 770
    :cond_3
    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v11

    const-string v12, "onFileFailed"

    const/4 v13, 0x0

    const/4 v14, 0x2

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    const-string v16, "offer"

    aput-object v16, v14, v15

    const/4 v15, 0x1

    aput-object v9, v14, v15

    invoke-static {v11, v12, v13, v14}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 772
    new-instance v11, Lcom/samsung/groupcast/core/messaging/LocalChannel$9;

    move-object/from16 v0, p0

    invoke-direct {v11, v0, v9, v5}, Lcom/samsung/groupcast/core/messaging/LocalChannel$9;-><init>(Lcom/samsung/groupcast/core/messaging/LocalChannel;Lcom/samsung/groupcast/core/messaging/FileOffer;Ljava/lang/String;)V

    invoke-static {v11}, Lcom/samsung/groupcast/application/MainQueue;->post(Ljava/lang/Runnable;)V

    goto/16 :goto_1
.end method

.method public onFileReceived(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;)V
    .locals 19
    .param p1, "fromNode"    # Ljava/lang/String;
    .param p2, "fromChannel"    # Ljava/lang/String;
    .param p3, "originalName"    # Ljava/lang/String;
    .param p4, "hash"    # Ljava/lang/String;
    .param p5, "type"    # Ljava/lang/String;
    .param p6, "exchangeId"    # Ljava/lang/String;
    .param p7, "fileSize"    # J
    .param p9, "tempPath"    # Ljava/lang/String;

    .prologue
    .line 655
    const/4 v9, 0x0

    .line 657
    .local v9, "info":Lcom/samsung/groupcast/core/messaging/FileInfo;
    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v13

    const-string v14, "onFileReceived"

    const-string v15, "exchangeId parsing "

    const/16 v16, 0x4

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    const-string v18, "type"

    aput-object v18, v16, v17

    const/16 v17, 0x1

    aput-object p5, v16, v17

    const/16 v17, 0x2

    const-string v18, "exchangeId"

    aput-object v18, v16, v17

    const/16 v17, 0x3

    aput-object p6, v16, v17

    invoke-static/range {v13 .. v16}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 659
    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v13

    const-string v14, "onFileReceived"

    const-string v15, "check before remove"

    const/16 v16, 0x2

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    const-string v18, "mFileOfferByDataKey size:"

    aput-object v18, v16, v17

    const/16 v17, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/groupcast/core/messaging/LocalChannel;->mFileOfferByDataKey:Ljava/util/concurrent/ConcurrentHashMap;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Ljava/util/concurrent/ConcurrentHashMap;->size()I

    move-result v18

    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    aput-object v18, v16, v17

    invoke-static/range {v13 .. v16}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 663
    invoke-static/range {p5 .. p5}, Lcom/samsung/groupcast/core/messaging/LocalChannel;->getProtocolFromTypeString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 664
    .local v1, "cmd":Ljava/lang/String;
    invoke-static/range {p5 .. p5}, Lcom/samsung/groupcast/core/messaging/LocalChannel;->getFileInfoFromTypeString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 665
    .local v2, "data":Ljava/lang/String;
    if-nez v2, :cond_0

    .line 667
    move-object/from16 v2, p6

    .line 669
    :cond_0
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/groupcast/core/messaging/LocalChannel;->mExchangeIdFromFileInfo:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v13, v2}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 670
    .local v5, "eid":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v13

    const-string v14, "onFileReceived"

    const-string v15, "exchangeId parsing "

    const/16 v16, 0x2

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    const-string v18, "eid"

    aput-object v18, v16, v17

    const/16 v17, 0x1

    aput-object v5, v16, v17

    invoke-static/range {v13 .. v16}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 671
    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v13

    const-string v14, "onFileReceived"

    const-string v15, "remove"

    const/16 v16, 0x2

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    const-string v18, "mExchangeIdFromFileInfo"

    aput-object v18, v16, v17

    const/16 v17, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/groupcast/core/messaging/LocalChannel;->mExchangeIdFromFileInfo:Ljava/util/concurrent/ConcurrentHashMap;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Ljava/util/concurrent/ConcurrentHashMap;->size()I

    move-result v18

    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    aput-object v18, v16, v17

    invoke-static/range {v13 .. v16}, Lcom/samsung/groupcast/application/Logger;->wx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 675
    :try_start_0
    new-instance v10, Lcom/samsung/groupcast/core/messaging/FileInfo;

    invoke-direct {v10, v2}, Lcom/samsung/groupcast/core/messaging/FileInfo;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .end local v9    # "info":Lcom/samsung/groupcast/core/messaging/FileInfo;
    .local v10, "info":Lcom/samsung/groupcast/core/messaging/FileInfo;
    move-object v9, v10

    .line 686
    .end local v10    # "info":Lcom/samsung/groupcast/core/messaging/FileInfo;
    .restart local v9    # "info":Lcom/samsung/groupcast/core/messaging/FileInfo;
    :goto_0
    invoke-virtual {v9}, Lcom/samsung/groupcast/core/messaging/FileInfo;->getDataKey()Lcom/samsung/groupcast/core/messaging/DataKey;

    move-result-object v3

    .line 687
    .local v3, "dataKey":Lcom/samsung/groupcast/core/messaging/DataKey;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/groupcast/core/messaging/LocalChannel;->mFileOfferByDataKey:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v3}, Lcom/samsung/groupcast/core/messaging/DataKey;->getDataId()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/samsung/groupcast/core/messaging/FileOffer;

    .line 689
    .local v11, "offer":Lcom/samsung/groupcast/core/messaging/FileOffer;
    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v13

    const-string v14, "onFileReceived"

    const-string v15, "check "

    const/16 v16, 0x2

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    const-string v18, "dataKey"

    aput-object v18, v16, v17

    const/16 v17, 0x1

    aput-object v3, v16, v17

    invoke-static/range {v13 .. v16}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 690
    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v13

    const-string v14, "onFileReceived"

    const-string v15, "check "

    const/16 v16, 0x2

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    const-string v18, "offer"

    aput-object v18, v16, v17

    const/16 v17, 0x1

    aput-object v11, v16, v17

    invoke-static/range {v13 .. v16}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 691
    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v13

    const-string v14, "onFileReceived"

    const-string v15, "check "

    const/16 v16, 0x2

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    const-string v18, "mFileOfferByDataKey size:"

    aput-object v18, v16, v17

    const/16 v17, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/groupcast/core/messaging/LocalChannel;->mFileOfferByDataKey:Ljava/util/concurrent/ConcurrentHashMap;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Ljava/util/concurrent/ConcurrentHashMap;->size()I

    move-result v18

    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    aput-object v18, v16, v17

    invoke-static/range {v13 .. v16}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 694
    if-nez v11, :cond_1

    .line 696
    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v13

    const-string v14, "onFileReceived"

    const-string v15, "no accepted offer for received file"

    const/16 v16, 0x6

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    const-string v18, "type"

    aput-object v18, v16, v17

    const/16 v17, 0x1

    aput-object v1, v16, v17

    const/16 v17, 0x2

    const-string v18, "info"

    aput-object v18, v16, v17

    const/16 v17, 0x3

    aput-object v9, v16, v17

    const/16 v17, 0x4

    const-string v18, "tempPath"

    aput-object v18, v16, v17

    const/16 v17, 0x5

    aput-object p9, v16, v17

    invoke-static/range {v13 .. v16}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 698
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/groupcast/core/messaging/LocalChannel;->mFileOfferByDataKey:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v13}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    move-result-object v13

    invoke-interface {v13}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .local v8, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_2

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/groupcast/core/messaging/FileOffer;

    .line 699
    .local v6, "f":Lcom/samsung/groupcast/core/messaging/FileOffer;
    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v13

    const-string v14, "onFileReceived"

    const-string v15, "check "

    const/16 v16, 0x2

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    const-string v18, "f"

    aput-object v18, v16, v17

    const/16 v17, 0x1

    aput-object v6, v16, v17

    invoke-static/range {v13 .. v16}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_1

    .line 676
    .end local v3    # "dataKey":Lcom/samsung/groupcast/core/messaging/DataKey;
    .end local v6    # "f":Lcom/samsung/groupcast/core/messaging/FileOffer;
    .end local v8    # "i$":Ljava/util/Iterator;
    .end local v11    # "offer":Lcom/samsung/groupcast/core/messaging/FileOffer;
    :catch_0
    move-exception v4

    .line 682
    .local v4, "e":Ljava/lang/Exception;
    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v13

    const-string v14, "onFileReceived"

    const-string v15, "exchangeId parsing failed"

    const/16 v16, 0x6

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    const-string v18, "type"

    aput-object v18, v16, v17

    const/16 v17, 0x1

    aput-object v1, v16, v17

    const/16 v17, 0x2

    const-string v18, "exchangeId"

    aput-object v18, v16, v17

    const/16 v17, 0x3

    aput-object v2, v16, v17

    const/16 v17, 0x4

    const-string v18, "tempPath"

    aput-object v18, v16, v17

    const/16 v17, 0x5

    aput-object p9, v16, v17

    invoke-static/range {v13 .. v16}, Lcom/samsung/groupcast/application/Logger;->wx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 704
    .end local v4    # "e":Ljava/lang/Exception;
    .restart local v3    # "dataKey":Lcom/samsung/groupcast/core/messaging/DataKey;
    .restart local v11    # "offer":Lcom/samsung/groupcast/core/messaging/FileOffer;
    :cond_1
    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v13

    const-string v14, "onFileReceived"

    const/4 v15, 0x0

    const/16 v16, 0x4

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    const-string v18, "offer"

    aput-object v18, v16, v17

    const/16 v17, 0x1

    aput-object v11, v16, v17

    const/16 v17, 0x2

    const-string v18, "tempPath"

    aput-object v18, v16, v17

    const/16 v17, 0x3

    aput-object p9, v16, v17

    invoke-static/range {v13 .. v16}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 706
    move-object/from16 v7, p1

    .line 707
    .local v7, "fromNodeF":Ljava/lang/String;
    move-object/from16 v12, p9

    .line 709
    .local v12, "tempPathF":Ljava/lang/String;
    new-instance v13, Lcom/samsung/groupcast/core/messaging/LocalChannel$8;

    move-object/from16 v0, p0

    invoke-direct {v13, v0, v11, v7, v12}, Lcom/samsung/groupcast/core/messaging/LocalChannel$8;-><init>(Lcom/samsung/groupcast/core/messaging/LocalChannel;Lcom/samsung/groupcast/core/messaging/FileOffer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v13}, Lcom/samsung/groupcast/application/MainQueue;->post(Ljava/lang/Runnable;)V

    .line 718
    .end local v7    # "fromNodeF":Ljava/lang/String;
    .end local v12    # "tempPathF":Ljava/lang/String;
    :cond_2
    return-void
.end method

.method public onFileSent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p1, "destination"    # Ljava/lang/String;
    .param p2, "channel"    # Ljava/lang/String;
    .param p3, "filename"    # Ljava/lang/String;
    .param p4, "hash"    # Ljava/lang/String;
    .param p5, "type"    # Ljava/lang/String;
    .param p6, "exchangeId"    # Ljava/lang/String;

    .prologue
    .line 790
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "onFileSent"

    const/4 v2, 0x0

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string v5, "type"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    aput-object p5, v3, v4

    const/4 v4, 0x2

    const-string v5, "exchangeId"

    aput-object v5, v3, v4

    const/4 v4, 0x3

    aput-object p6, v3, v4

    invoke-static {v0, v1, v2, v3}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 791
    return-void
.end method

.method public onFileWillReceive(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V
    .locals 15
    .param p1, "fromNode"    # Ljava/lang/String;
    .param p2, "fromChannel"    # Ljava/lang/String;
    .param p3, "originalName"    # Ljava/lang/String;
    .param p4, "hash"    # Ljava/lang/String;
    .param p5, "type"    # Ljava/lang/String;
    .param p6, "exchangeId"    # Ljava/lang/String;
    .param p7, "fileSize"    # J

    .prologue
    .line 555
    const/4 v5, 0x0

    .line 557
    .local v5, "info":Lcom/samsung/groupcast/core/messaging/FileInfo;
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v9

    const-string v10, "onFileNotified"

    const-string v11, "exchangeId parsing "

    const/4 v12, 0x4

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    const-string v14, "type"

    aput-object v14, v12, v13

    const/4 v13, 0x1

    aput-object p5, v12, v13

    const/4 v13, 0x2

    const-string v14, "exchangeId"

    aput-object v14, v12, v13

    const/4 v13, 0x3

    aput-object p6, v12, v13

    invoke-static {v9, v10, v11, v12}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 561
    invoke-static/range {p5 .. p5}, Lcom/samsung/groupcast/core/messaging/LocalChannel;->getProtocolFromTypeString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 562
    .local v1, "cmd":Ljava/lang/String;
    invoke-static/range {p5 .. p5}, Lcom/samsung/groupcast/core/messaging/LocalChannel;->getFileInfoFromTypeString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 563
    .local v2, "data":Ljava/lang/String;
    if-nez v2, :cond_0

    .line 565
    move-object/from16 v2, p6

    .line 567
    :cond_0
    iget-object v9, p0, Lcom/samsung/groupcast/core/messaging/LocalChannel;->mExchangeIdFromFileInfo:Ljava/util/concurrent/ConcurrentHashMap;

    move-object/from16 v0, p6

    invoke-virtual {v9, v2, v0}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 568
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v9

    const-string v10, "onFileNotified"

    const-string v11, "put"

    const/4 v12, 0x2

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    const-string v14, "mExchangeIdFromFileInfo"

    aput-object v14, v12, v13

    const/4 v13, 0x1

    iget-object v14, p0, Lcom/samsung/groupcast/core/messaging/LocalChannel;->mExchangeIdFromFileInfo:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v14}, Ljava/util/concurrent/ConcurrentHashMap;->size()I

    move-result v14

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    aput-object v14, v12, v13

    invoke-static {v9, v10, v11, v12}, Lcom/samsung/groupcast/application/Logger;->wx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 572
    :try_start_0
    new-instance v6, Lcom/samsung/groupcast/core/messaging/FileInfo;

    invoke-direct {v6, v2}, Lcom/samsung/groupcast/core/messaging/FileInfo;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 580
    .end local v5    # "info":Lcom/samsung/groupcast/core/messaging/FileInfo;
    .local v6, "info":Lcom/samsung/groupcast/core/messaging/FileInfo;
    new-instance v8, Lcom/samsung/groupcast/core/messaging/FileOfferToken;

    move-object/from16 v0, p6

    invoke-direct {v8, v0}, Lcom/samsung/groupcast/core/messaging/FileOfferToken;-><init>(Ljava/lang/String;)V

    .line 581
    .local v8, "token":Lcom/samsung/groupcast/core/messaging/FileOfferToken;
    invoke-static {v1, v6, v8}, Lcom/samsung/groupcast/core/messaging/MessageFactory;->createFileOffer(Ljava/lang/String;Lcom/samsung/groupcast/core/messaging/FileInfo;Lcom/samsung/groupcast/core/messaging/FileOfferToken;)Lcom/samsung/groupcast/core/messaging/FileOffer;

    move-result-object v7

    .line 583
    .local v7, "offer":Lcom/samsung/groupcast/core/messaging/FileOffer;
    if-nez v7, :cond_1

    .line 584
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v9

    const-string v10, "onFileNotified"

    const-string v11, "file offer creation failed"

    const/4 v12, 0x6

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    const-string v14, "type"

    aput-object v14, v12, v13

    const/4 v13, 0x1

    aput-object v1, v12, v13

    const/4 v13, 0x2

    const-string v14, "info"

    aput-object v14, v12, v13

    const/4 v13, 0x3

    aput-object v6, v12, v13

    const/4 v13, 0x4

    const-string v14, "token"

    aput-object v14, v12, v13

    const/4 v13, 0x5

    aput-object v8, v12, v13

    invoke-static {v9, v10, v11, v12}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    move-object v5, v6

    .line 601
    .end local v6    # "info":Lcom/samsung/groupcast/core/messaging/FileInfo;
    .end local v7    # "offer":Lcom/samsung/groupcast/core/messaging/FileOffer;
    .end local v8    # "token":Lcom/samsung/groupcast/core/messaging/FileOfferToken;
    .restart local v5    # "info":Lcom/samsung/groupcast/core/messaging/FileInfo;
    :goto_0
    return-void

    .line 573
    :catch_0
    move-exception v3

    .line 574
    .local v3, "e":Ljava/lang/Exception;
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v9

    const-string v10, "onFileNotified"

    const-string v11, "exchangeId parsing failed"

    const/4 v12, 0x4

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    const-string v14, "type"

    aput-object v14, v12, v13

    const/4 v13, 0x1

    aput-object v1, v12, v13

    const/4 v13, 0x2

    const-string v14, "exchangeId"

    aput-object v14, v12, v13

    const/4 v13, 0x3

    aput-object v2, v12, v13

    invoke-static {v9, v10, v11, v12}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_0

    .line 589
    .end local v3    # "e":Ljava/lang/Exception;
    .end local v5    # "info":Lcom/samsung/groupcast/core/messaging/FileInfo;
    .restart local v6    # "info":Lcom/samsung/groupcast/core/messaging/FileInfo;
    .restart local v7    # "offer":Lcom/samsung/groupcast/core/messaging/FileOffer;
    .restart local v8    # "token":Lcom/samsung/groupcast/core/messaging/FileOfferToken;
    :cond_1
    invoke-static {}, Lcom/samsung/groupcast/core/messaging/MessagingClient;->checkChordDir()V

    .line 591
    move-object/from16 v4, p1

    .line 592
    .local v4, "fromNodeF":Ljava/lang/String;
    new-instance v9, Lcom/samsung/groupcast/core/messaging/LocalChannel$6;

    invoke-direct {v9, p0, v7, v4}, Lcom/samsung/groupcast/core/messaging/LocalChannel$6;-><init>(Lcom/samsung/groupcast/core/messaging/LocalChannel;Lcom/samsung/groupcast/core/messaging/FileOffer;Ljava/lang/String;)V

    invoke-static {v9}, Lcom/samsung/groupcast/application/MainQueue;->post(Ljava/lang/Runnable;)V

    move-object v5, v6

    .line 601
    .end local v6    # "info":Lcom/samsung/groupcast/core/messaging/FileInfo;
    .restart local v5    # "info":Lcom/samsung/groupcast/core/messaging/FileInfo;
    goto :goto_0
.end method

.method public onMultiFilesChunkReceived(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;JJ)V
    .locals 0
    .param p1, "arg0"    # Ljava/lang/String;
    .param p2, "arg1"    # Ljava/lang/String;
    .param p3, "arg2"    # Ljava/lang/String;
    .param p4, "arg3"    # Ljava/lang/String;
    .param p5, "arg4"    # I
    .param p6, "arg5"    # Ljava/lang/String;
    .param p7, "arg6"    # J
    .param p9, "arg7"    # J

    .prologue
    .line 805
    return-void
.end method

.method public onMultiFilesChunkSent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;JJJ)V
    .locals 0
    .param p1, "arg0"    # Ljava/lang/String;
    .param p2, "arg1"    # Ljava/lang/String;
    .param p3, "arg2"    # Ljava/lang/String;
    .param p4, "arg3"    # Ljava/lang/String;
    .param p5, "arg4"    # I
    .param p6, "arg5"    # Ljava/lang/String;
    .param p7, "arg6"    # J
    .param p9, "arg7"    # J
    .param p11, "arg8"    # J

    .prologue
    .line 812
    return-void
.end method

.method public onMultiFilesFailed(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V
    .locals 0
    .param p1, "arg0"    # Ljava/lang/String;
    .param p2, "arg1"    # Ljava/lang/String;
    .param p3, "arg2"    # Ljava/lang/String;
    .param p4, "arg3"    # Ljava/lang/String;
    .param p5, "arg4"    # I
    .param p6, "arg5"    # I

    .prologue
    .line 819
    return-void
.end method

.method public onMultiFilesFinished(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 0
    .param p1, "arg0"    # Ljava/lang/String;
    .param p2, "arg1"    # Ljava/lang/String;
    .param p3, "arg2"    # Ljava/lang/String;
    .param p4, "arg3"    # I

    .prologue
    .line 825
    return-void
.end method

.method public onMultiFilesReceived(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;JLjava/lang/String;)V
    .locals 0
    .param p1, "arg0"    # Ljava/lang/String;
    .param p2, "arg1"    # Ljava/lang/String;
    .param p3, "arg2"    # Ljava/lang/String;
    .param p4, "arg3"    # Ljava/lang/String;
    .param p5, "arg4"    # I
    .param p6, "arg5"    # Ljava/lang/String;
    .param p7, "arg6"    # J
    .param p9, "arg7"    # Ljava/lang/String;

    .prologue
    .line 832
    return-void
.end method

.method public onMultiFilesSent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p1, "arg0"    # Ljava/lang/String;
    .param p2, "arg1"    # Ljava/lang/String;
    .param p3, "arg2"    # Ljava/lang/String;
    .param p4, "arg3"    # Ljava/lang/String;
    .param p5, "arg4"    # I
    .param p6, "arg5"    # Ljava/lang/String;

    .prologue
    .line 839
    return-void
.end method

.method public onMultiFilesWillReceive(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;J)V
    .locals 0
    .param p1, "arg0"    # Ljava/lang/String;
    .param p2, "arg1"    # Ljava/lang/String;
    .param p3, "arg2"    # Ljava/lang/String;
    .param p4, "arg3"    # Ljava/lang/String;
    .param p5, "arg4"    # I
    .param p6, "arg5"    # Ljava/lang/String;
    .param p7, "arg6"    # J

    .prologue
    .line 846
    return-void
.end method

.method public onNodeJoined(Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p1, "fromNode"    # Ljava/lang/String;
    .param p2, "fromChannel"    # Ljava/lang/String;

    .prologue
    .line 498
    iget-object v1, p0, Lcom/samsung/groupcast/core/messaging/LocalChannel;->mPeers:Ljava/util/HashSet;

    invoke-virtual {v1, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 499
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-string v2, "onNodeJoinEvent"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string v5, "node"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    aput-object p1, v3, v4

    invoke-static {v1, v2, p0, v3}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 501
    move-object v0, p1

    .line 502
    .local v0, "fromNodeF":Ljava/lang/String;
    new-instance v1, Lcom/samsung/groupcast/core/messaging/LocalChannel$3;

    invoke-direct {v1, p0, v0}, Lcom/samsung/groupcast/core/messaging/LocalChannel$3;-><init>(Lcom/samsung/groupcast/core/messaging/LocalChannel;Ljava/lang/String;)V

    invoke-static {v1}, Lcom/samsung/groupcast/application/MainQueue;->post(Ljava/lang/Runnable;)V

    .line 510
    return-void
.end method

.method public onNodeLeft(Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p1, "fromNode"    # Ljava/lang/String;
    .param p2, "fromChannel"    # Ljava/lang/String;

    .prologue
    .line 514
    iget-object v1, p0, Lcom/samsung/groupcast/core/messaging/LocalChannel;->mPeers:Ljava/util/HashSet;

    invoke-virtual {v1, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 515
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-string v2, "onNodeLeaveEvent"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string v5, "node"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    aput-object p1, v3, v4

    invoke-static {v1, v2, p0, v3}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 517
    move-object v0, p1

    .line 518
    .local v0, "fromNodeF":Ljava/lang/String;
    new-instance v1, Lcom/samsung/groupcast/core/messaging/LocalChannel$4;

    invoke-direct {v1, p0, v0}, Lcom/samsung/groupcast/core/messaging/LocalChannel$4;-><init>(Lcom/samsung/groupcast/core/messaging/LocalChannel;Ljava/lang/String;)V

    invoke-static {v1}, Lcom/samsung/groupcast/application/MainQueue;->post(Ljava/lang/Runnable;)V

    .line 527
    return-void
.end method

.method public removeListener(Lcom/samsung/groupcast/core/messaging/ChannelListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/samsung/groupcast/core/messaging/ChannelListener;

    .prologue
    .line 186
    if-nez p1, :cond_0

    .line 192
    :goto_0
    return-void

    .line 190
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/samsung/groupcast/core/messaging/LocalChannel;->mListeners:Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/samsung/groupcast/core/messaging/LocalChannel;->mListeners:Ljava/util/ArrayList;

    .line 191
    iget-object v0, p0, Lcom/samsung/groupcast/core/messaging/LocalChannel;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public sendFileIssueToNode(Ljava/lang/String;Lcom/samsung/groupcast/core/messaging/FileIssue;J)V
    .locals 7
    .param p1, "node"    # Ljava/lang/String;
    .param p2, "issue"    # Lcom/samsung/groupcast/core/messaging/FileIssue;
    .param p3, "timeoutMilliseconds"    # J

    .prologue
    .line 286
    const-string v2, "node"

    invoke-static {v2, p1}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 287
    const-string v2, "issue"

    invoke-static {v2, p2}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 288
    invoke-interface {p2}, Lcom/samsung/groupcast/core/messaging/FileIssue;->getFileInfo()Lcom/samsung/groupcast/core/messaging/FileInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/groupcast/core/messaging/FileInfo;->getStringRepresentation()Ljava/lang/String;

    move-result-object v1

    .line 289
    .local v1, "fileInfo":Ljava/lang/String;
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const-string v3, "sendFileIssueToNode"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const-string v6, "issue"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    aput-object p2, v4, v5

    invoke-static {v2, v3, p0, v4}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 291
    iget-object v2, p0, Lcom/samsung/groupcast/core/messaging/LocalChannel;->mChord:Lcom/samsung/android/sdk/chord/SchordManager;

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/samsung/groupcast/core/messaging/LocalChannel;->getChordChannel()Lcom/samsung/android/sdk/chord/SchordChannel;

    move-result-object v2

    if-nez v2, :cond_1

    .line 292
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "there is null "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/groupcast/core/messaging/LocalChannel;->mChord:Lcom/samsung/android/sdk/chord/SchordManager;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/samsung/groupcast/core/messaging/LocalChannel;->getChordChannel()Lcom/samsung/android/sdk/chord/SchordChannel;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 309
    :goto_0
    return-void

    .line 304
    :cond_1
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/groupcast/core/messaging/LocalChannel;->getChordChannel()Lcom/samsung/android/sdk/chord/SchordChannel;

    move-result-object v2

    invoke-interface {p2}, Lcom/samsung/groupcast/core/messaging/FileIssue;->getType()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v1}, Lcom/samsung/groupcast/core/messaging/LocalChannel;->makeTypeStringWithFileInfo(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {p2}, Lcom/samsung/groupcast/core/messaging/FileIssue;->getFilePath()Ljava/lang/String;

    move-result-object v4

    long-to-int v5, p3

    invoke-interface {v2, p1, v3, v4, v5}, Lcom/samsung/android/sdk/chord/SchordChannel;->sendFile(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 306
    :catch_0
    move-exception v0

    .line 307
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public sendMessageToAll(Lcom/samsung/groupcast/core/messaging/Message;)V
    .locals 1
    .param p1, "message"    # Lcom/samsung/groupcast/core/messaging/Message;

    .prologue
    .line 257
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/samsung/groupcast/core/messaging/LocalChannel;->sendMessageToAll(Lcom/samsung/groupcast/core/messaging/Message;Z)V

    .line 258
    return-void
.end method

.method public sendMessageToAll(Lcom/samsung/groupcast/core/messaging/Message;Z)V
    .locals 6
    .param p1, "message"    # Lcom/samsung/groupcast/core/messaging/Message;
    .param p2, "log"    # Z

    .prologue
    .line 261
    const-string v1, "message"

    invoke-static {v1, p1}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 270
    iget-object v1, p0, Lcom/samsung/groupcast/core/messaging/LocalChannel;->mChord:Lcom/samsung/android/sdk/chord/SchordManager;

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/samsung/groupcast/core/messaging/LocalChannel;->getChordChannel()Lcom/samsung/android/sdk/chord/SchordChannel;

    move-result-object v1

    if-nez v1, :cond_1

    .line 271
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "there is null "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/groupcast/core/messaging/LocalChannel;->mChord:Lcom/samsung/android/sdk/chord/SchordManager;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/samsung/groupcast/core/messaging/LocalChannel;->getChordChannel()Lcom/samsung/android/sdk/chord/SchordChannel;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 283
    :goto_0
    return-void

    .line 274
    :cond_1
    if-eqz p2, :cond_2

    .line 275
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-string v2, "sendToAll"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string v5, "message"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    aput-object p1, v3, v4

    invoke-static {v1, v2, p0, v3}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 279
    :cond_2
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/groupcast/core/messaging/LocalChannel;->getChordChannel()Lcom/samsung/android/sdk/chord/SchordChannel;

    move-result-object v1

    invoke-interface {p1}, Lcom/samsung/groupcast/core/messaging/Message;->getType()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1}, Lcom/samsung/groupcast/core/messaging/Message;->getPayload()Ljava/util/List;

    move-result-object v3

    invoke-static {v3}, Lcom/samsung/groupcast/core/messaging/LocalChannel;->GetBytesArray(Ljava/util/List;)[[B

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcom/samsung/android/sdk/chord/SchordChannel;->sendDataToAll(Ljava/lang/String;[[B)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 280
    :catch_0
    move-exception v0

    .line 281
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public sendMessageToNode(Ljava/lang/String;Lcom/samsung/groupcast/core/messaging/Message;)V
    .locals 1
    .param p1, "node"    # Ljava/lang/String;
    .param p2, "message"    # Lcom/samsung/groupcast/core/messaging/Message;

    .prologue
    .line 202
    const/4 v0, 0x1

    invoke-virtual {p0, p1, p2, v0}, Lcom/samsung/groupcast/core/messaging/LocalChannel;->sendMessageToNode(Ljava/lang/String;Lcom/samsung/groupcast/core/messaging/Message;Z)V

    .line 203
    return-void
.end method

.method public sendMessageToNode(Ljava/lang/String;Lcom/samsung/groupcast/core/messaging/Message;Z)V
    .locals 6
    .param p1, "node"    # Ljava/lang/String;
    .param p2, "message"    # Lcom/samsung/groupcast/core/messaging/Message;
    .param p3, "log"    # Z

    .prologue
    .line 206
    const-string v1, "node"

    invoke-static {v1, p1}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 207
    const-string v1, "message"

    invoke-static {v1, p2}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 209
    iget-object v1, p0, Lcom/samsung/groupcast/core/messaging/LocalChannel;->mChord:Lcom/samsung/android/sdk/chord/SchordManager;

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/samsung/groupcast/core/messaging/LocalChannel;->getChordChannel()Lcom/samsung/android/sdk/chord/SchordChannel;

    move-result-object v1

    if-nez v1, :cond_1

    .line 210
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "there is null "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/groupcast/core/messaging/LocalChannel;->mChord:Lcom/samsung/android/sdk/chord/SchordManager;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/samsung/groupcast/core/messaging/LocalChannel;->getChordChannel()Lcom/samsung/android/sdk/chord/SchordChannel;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 222
    :goto_0
    return-void

    .line 213
    :cond_1
    if-eqz p3, :cond_2

    .line 214
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-string v2, "sendToNode"

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string v5, "node"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    aput-object p1, v3, v4

    const/4 v4, 0x2

    const-string v5, "message"

    aput-object v5, v3, v4

    const/4 v4, 0x3

    aput-object p2, v3, v4

    invoke-static {v1, v2, p0, v3}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 218
    :cond_2
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/groupcast/core/messaging/LocalChannel;->getChordChannel()Lcom/samsung/android/sdk/chord/SchordChannel;

    move-result-object v1

    invoke-interface {p2}, Lcom/samsung/groupcast/core/messaging/Message;->getType()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p2}, Lcom/samsung/groupcast/core/messaging/Message;->getPayload()Ljava/util/List;

    move-result-object v3

    invoke-static {v3}, Lcom/samsung/groupcast/core/messaging/LocalChannel;->GetBytesArray(Ljava/util/List;)[[B

    move-result-object v3

    invoke-interface {v1, p1, v2, v3}, Lcom/samsung/android/sdk/chord/SchordChannel;->sendData(Ljava/lang/String;Ljava/lang/String;[[B)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 219
    :catch_0
    move-exception v0

    .line 220
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 796
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "channelId"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/samsung/groupcast/core/messaging/LocalChannel;->mChannelId:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "peers"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget-object v3, p0, Lcom/samsung/groupcast/core/messaging/LocalChannel;->mPeers:Ljava/util/HashSet;

    invoke-virtual {v3}, Ljava/util/HashSet;->size()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/samsung/groupcast/misc/utility/StringTools;->getDebugString(Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public updateParticipants(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;[Ljava/lang/String;Z[Ljava/lang/String;)V
    .locals 12
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "pingId"    # Ljava/lang/Integer;
    .param p4, "wifiIp"    # Ljava/lang/String;
    .param p5, "joinedActivities"    # [Ljava/lang/String;
    .param p6, "contentsResetEnabled"    # Z
    .param p7, "joinedGames"    # [Ljava/lang/String;

    .prologue
    .line 379
    new-instance v11, Ljava/util/HashSet;

    invoke-direct {v11}, Ljava/util/HashSet;-><init>()V

    .line 381
    .local v11, "updateNames":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    iget-object v1, p0, Lcom/samsung/groupcast/core/messaging/LocalChannel;->mParticipants:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1, p1}, Ljava/util/concurrent/ConcurrentHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 382
    invoke-static {}, Lcom/samsung/groupcast/application/App;->getInstance()Lcom/samsung/groupcast/application/App;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/groupcast/application/App;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    .line 383
    .local v8, "context":Landroid/content/Context;
    const v1, 0x7f08002d

    invoke-virtual {v8, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p2, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v8, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 386
    invoke-virtual {v11, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 389
    .end local v8    # "context":Landroid/content/Context;
    :cond_0
    iget-object v1, p0, Lcom/samsung/groupcast/core/messaging/LocalChannel;->mParticipants:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/groupcast/legacy/gp2/session/model/ParticipantInfo;

    .line 390
    .local v0, "participantInfo":Lcom/samsung/groupcast/legacy/gp2/session/model/ParticipantInfo;
    if-nez v0, :cond_2

    .line 391
    new-instance v0, Lcom/samsung/groupcast/legacy/gp2/session/model/ParticipantInfo;

    .end local v0    # "participantInfo":Lcom/samsung/groupcast/legacy/gp2/session/model/ParticipantInfo;
    move-object v1, p2

    move-object v2, p3

    move-object/from16 v3, p4

    move-object/from16 v4, p5

    move/from16 v5, p6

    move-object/from16 v6, p7

    invoke-direct/range {v0 .. v6}, Lcom/samsung/groupcast/legacy/gp2/session/model/ParticipantInfo;-><init>(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;[Ljava/lang/String;Z[Ljava/lang/String;)V

    .line 394
    .restart local v0    # "participantInfo":Lcom/samsung/groupcast/legacy/gp2/session/model/ParticipantInfo;
    iget-object v1, p0, Lcom/samsung/groupcast/core/messaging/LocalChannel;->mParticipants:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 395
    invoke-virtual {v11, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 402
    :cond_1
    :goto_0
    iget-object v1, p0, Lcom/samsung/groupcast/core/messaging/LocalChannel;->mParticipantLeaveEvents:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_3

    .line 403
    move-object v7, p0

    .line 404
    .local v7, "channel":Lcom/samsung/groupcast/core/messaging/LocalChannel;
    iget-object v1, p0, Lcom/samsung/groupcast/core/messaging/LocalChannel;->mParticipantLeaveEvents:Ljava/util/concurrent/ConcurrentHashMap;

    new-instance v2, Lcom/samsung/groupcast/core/messaging/LocalChannel$1;

    invoke-direct {v2, p0, p1, v7}, Lcom/samsung/groupcast/core/messaging/LocalChannel$1;-><init>(Lcom/samsung/groupcast/core/messaging/LocalChannel;Ljava/lang/String;Lcom/samsung/groupcast/core/messaging/LocalChannel;)V

    invoke-virtual {v1, p1, v2}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 417
    .end local v7    # "channel":Lcom/samsung/groupcast/core/messaging/LocalChannel;
    :goto_1
    iget-object v1, p0, Lcom/samsung/groupcast/core/messaging/LocalChannel;->mParticipantLeaveEvents:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Runnable;

    const-wide/16 v2, 0x2710

    invoke-static {v1, v2, v3}, Lcom/samsung/groupcast/application/MainQueue;->postDelayed(Ljava/lang/Runnable;J)V

    .line 419
    invoke-virtual {v11}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v10

    .line 420
    .local v10, "updateName":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :goto_2
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 421
    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    .line 422
    .local v9, "peerName":Ljava/lang/String;
    new-instance v1, Lcom/samsung/groupcast/core/messaging/LocalChannel$2;

    invoke-direct {v1, p0, v9}, Lcom/samsung/groupcast/core/messaging/LocalChannel$2;-><init>(Lcom/samsung/groupcast/core/messaging/LocalChannel;Ljava/lang/String;)V

    invoke-static {v1}, Lcom/samsung/groupcast/application/MainQueue;->post(Ljava/lang/Runnable;)V

    goto :goto_2

    .end local v9    # "peerName":Ljava/lang/String;
    .end local v10    # "updateName":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :cond_2
    move-object v1, p2

    move-object v2, p3

    move-object/from16 v3, p4

    move-object/from16 v4, p5

    move/from16 v5, p6

    move-object/from16 v6, p7

    .line 397
    invoke-virtual/range {v0 .. v6}, Lcom/samsung/groupcast/legacy/gp2/session/model/ParticipantInfo;->update(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;[Ljava/lang/String;Z[Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 399
    invoke-virtual {v11, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 415
    :cond_3
    iget-object v1, p0, Lcom/samsung/groupcast/core/messaging/LocalChannel;->mParticipantLeaveEvents:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Runnable;

    invoke-static {v1}, Lcom/samsung/groupcast/application/MainQueue;->cancel(Ljava/lang/Runnable;)V

    goto :goto_1

    .line 432
    .restart local v10    # "updateName":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :cond_4
    return-void
.end method
