.class public Lcom/samsung/groupcast/core/messaging/MessageFactory;
.super Ljava/lang/Object;
.source "MessageFactory.java"


# static fields
.field private static final mMessageCreators:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/groupcast/core/messaging/MessageCreator;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/samsung/groupcast/core/messaging/MessageFactory;->mMessageCreators:Ljava/util/HashMap;

    .line 31
    sget-object v0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PingV1;->MESSAGE_CREATOR:Lcom/samsung/groupcast/core/messaging/MessageCreator;

    invoke-static {v0}, Lcom/samsung/groupcast/core/messaging/MessageFactory;->registerMessageCreator(Lcom/samsung/groupcast/core/messaging/MessageCreator;)V

    .line 32
    sget-object v0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PongV1;->MESSAGE_CREATOR:Lcom/samsung/groupcast/core/messaging/MessageCreator;

    invoke-static {v0}, Lcom/samsung/groupcast/core/messaging/MessageFactory;->registerMessageCreator(Lcom/samsung/groupcast/core/messaging/MessageCreator;)V

    .line 33
    sget-object v0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/SessionSummaryRequestV1;->MESSAGE_CREATOR:Lcom/samsung/groupcast/core/messaging/MessageCreator;

    invoke-static {v0}, Lcom/samsung/groupcast/core/messaging/MessageFactory;->registerMessageCreator(Lcom/samsung/groupcast/core/messaging/MessageCreator;)V

    .line 34
    sget-object v0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/SessionSummaryInfoV1;->MESSAGE_CREATOR:Lcom/samsung/groupcast/core/messaging/MessageCreator;

    invoke-static {v0}, Lcom/samsung/groupcast/core/messaging/MessageFactory;->registerMessageCreator(Lcom/samsung/groupcast/core/messaging/MessageCreator;)V

    .line 35
    sget-object v0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/CurrentPageRequestV1;->MESSAGE_CREATOR:Lcom/samsung/groupcast/core/messaging/MessageCreator;

    invoke-static {v0}, Lcom/samsung/groupcast/core/messaging/MessageFactory;->registerMessageCreator(Lcom/samsung/groupcast/core/messaging/MessageCreator;)V

    .line 36
    sget-object v0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/CurrentPageInfoV1;->MESSAGE_CREATOR:Lcom/samsung/groupcast/core/messaging/MessageCreator;

    invoke-static {v0}, Lcom/samsung/groupcast/core/messaging/MessageFactory;->registerMessageCreator(Lcom/samsung/groupcast/core/messaging/MessageCreator;)V

    .line 37
    sget-object v0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/ManifestInfoV1;->MESSAGE_CREATOR:Lcom/samsung/groupcast/core/messaging/MessageCreator;

    invoke-static {v0}, Lcom/samsung/groupcast/core/messaging/MessageFactory;->registerMessageCreator(Lcom/samsung/groupcast/core/messaging/MessageCreator;)V

    .line 38
    sget-object v0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileAvailabilityInfoV1;->MESSAGE_CREATOR:Lcom/samsung/groupcast/core/messaging/MessageCreator;

    invoke-static {v0}, Lcom/samsung/groupcast/core/messaging/MessageFactory;->registerMessageCreator(Lcom/samsung/groupcast/core/messaging/MessageCreator;)V

    .line 39
    sget-object v0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageEditV1;->MESSAGE_CREATOR:Lcom/samsung/groupcast/core/messaging/MessageCreator;

    invoke-static {v0}, Lcom/samsung/groupcast/core/messaging/MessageFactory;->registerMessageCreator(Lcom/samsung/groupcast/core/messaging/MessageCreator;)V

    .line 40
    sget-object v0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileRequestV1;->MESSAGE_CREATOR:Lcom/samsung/groupcast/core/messaging/MessageCreator;

    invoke-static {v0}, Lcom/samsung/groupcast/core/messaging/MessageFactory;->registerMessageCreator(Lcom/samsung/groupcast/core/messaging/MessageCreator;)V

    .line 41
    sget-object v0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageCommandV1;->MESSAGE_CREATOR:Lcom/samsung/groupcast/core/messaging/MessageCreator;

    invoke-static {v0}, Lcom/samsung/groupcast/core/messaging/MessageFactory;->registerMessageCreator(Lcom/samsung/groupcast/core/messaging/MessageCreator;)V

    .line 42
    sget-object v0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/DrawingSyncNotificationV1;->MESSAGE_CREATOR:Lcom/samsung/groupcast/core/messaging/MessageCreator;

    invoke-static {v0}, Lcom/samsung/groupcast/core/messaging/MessageFactory;->registerMessageCreator(Lcom/samsung/groupcast/core/messaging/MessageCreator;)V

    .line 43
    sget-object v0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/DrawingSyncJsonRequestV1;->MESSAGE_CREATOR:Lcom/samsung/groupcast/core/messaging/MessageCreator;

    invoke-static {v0}, Lcom/samsung/groupcast/core/messaging/MessageFactory;->registerMessageCreator(Lcom/samsung/groupcast/core/messaging/MessageCreator;)V

    .line 44
    sget-object v0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/DrawingSyncJsonResponseV1;->MESSAGE_CREATOR:Lcom/samsung/groupcast/core/messaging/MessageCreator;

    invoke-static {v0}, Lcom/samsung/groupcast/core/messaging/MessageFactory;->registerMessageCreator(Lcom/samsung/groupcast/core/messaging/MessageCreator;)V

    .line 45
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static createFileOffer(Ljava/lang/String;Lcom/samsung/groupcast/core/messaging/FileInfo;Lcom/samsung/groupcast/core/messaging/FileOfferToken;)Lcom/samsung/groupcast/core/messaging/FileOffer;
    .locals 1
    .param p0, "type"    # Ljava/lang/String;
    .param p1, "info"    # Lcom/samsung/groupcast/core/messaging/FileInfo;
    .param p2, "token"    # Lcom/samsung/groupcast/core/messaging/FileOfferToken;

    .prologue
    .line 71
    new-instance v0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileOfferV1;

    invoke-direct {v0, p1, p2}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileOfferV1;-><init>(Lcom/samsung/groupcast/core/messaging/FileInfo;Lcom/samsung/groupcast/core/messaging/FileOfferToken;)V

    return-object v0
.end method

.method public static createMessage(Ljava/lang/String;Ljava/util/List;)Lcom/samsung/groupcast/core/messaging/Message;
    .locals 13
    .param p0, "type"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<[B>;)",
            "Lcom/samsung/groupcast/core/messaging/Message;"
        }
    .end annotation

    .prologue
    .local p1, "payload":Ljava/util/List;, "Ljava/util/List<[B>;"
    const/4 v12, 0x4

    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 48
    sget-object v3, Lcom/samsung/groupcast/core/messaging/MessageFactory;->mMessageCreators:Ljava/util/HashMap;

    invoke-virtual {v3, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/groupcast/core/messaging/MessageCreator;

    .line 50
    .local v0, "creator":Lcom/samsung/groupcast/core/messaging/MessageCreator;
    if-nez v0, :cond_0

    .line 51
    const-class v3, Lcom/samsung/groupcast/core/messaging/MessageFactory;

    const-string v4, "createMessage"

    const-string v5, "no creator found for type"

    new-array v6, v12, [Ljava/lang/Object;

    const-string v7, "type"

    aput-object v7, v6, v8

    aput-object p0, v6, v9

    const-string v7, "payloadCount"

    aput-object v7, v6, v10

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v11

    invoke-static {v3, v4, v5, v6}, Lcom/samsung/groupcast/application/Logger;->wx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 53
    const/4 v2, 0x0

    .line 65
    :goto_0
    return-object v2

    .line 56
    :cond_0
    const/4 v2, 0x0

    .line 59
    .local v2, "message":Lcom/samsung/groupcast/core/messaging/Message;
    :try_start_0
    invoke-virtual {v0, p1}, Lcom/samsung/groupcast/core/messaging/MessageCreator;->createMessage(Ljava/util/List;)Lcom/samsung/groupcast/core/messaging/Message;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    goto :goto_0

    .line 60
    :catch_0
    move-exception v1

    .line 61
    .local v1, "e":Ljava/lang/Exception;
    const-class v3, Lcom/samsung/groupcast/core/messaging/MessageFactory;

    const-string v4, "createMessage"

    const-string v5, "exception while creating message"

    const/4 v6, 0x6

    new-array v6, v6, [Ljava/lang/Object;

    const-string v7, "type"

    aput-object v7, v6, v8

    aput-object p0, v6, v9

    const-string v7, "payloadCount"

    aput-object v7, v6, v10

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v11

    const-string v7, "exception"

    aput-object v7, v6, v12

    const/4 v7, 0x5

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v3, v4, v5, v6}, Lcom/samsung/groupcast/application/Logger;->wx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private static registerMessageCreator(Lcom/samsung/groupcast/core/messaging/MessageCreator;)V
    .locals 2
    .param p0, "creator"    # Lcom/samsung/groupcast/core/messaging/MessageCreator;

    .prologue
    .line 75
    invoke-virtual {p0}, Lcom/samsung/groupcast/core/messaging/MessageCreator;->getType()Ljava/lang/String;

    move-result-object v0

    .line 76
    .local v0, "type":Ljava/lang/String;
    sget-object v1, Lcom/samsung/groupcast/core/messaging/MessageFactory;->mMessageCreators:Ljava/util/HashMap;

    invoke-virtual {v1, v0, p0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 77
    return-void
.end method
