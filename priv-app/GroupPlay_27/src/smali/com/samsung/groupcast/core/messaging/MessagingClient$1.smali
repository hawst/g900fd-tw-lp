.class Lcom/samsung/groupcast/core/messaging/MessagingClient$1;
.super Ljava/lang/Object;
.source "MessagingClient.java"

# interfaces
.implements Lcom/samsung/android/sdk/chord/SchordManager$StatusListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/groupcast/core/messaging/MessagingClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/groupcast/core/messaging/MessagingClient;


# direct methods
.method constructor <init>(Lcom/samsung/groupcast/core/messaging/MessagingClient;)V
    .locals 0

    .prologue
    .line 111
    iput-object p1, p0, Lcom/samsung/groupcast/core/messaging/MessagingClient$1;->this$0:Lcom/samsung/groupcast/core/messaging/MessagingClient;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onStarted(Ljava/lang/String;I)V
    .locals 5
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "reason"    # I

    .prologue
    const/4 v4, 0x1

    .line 115
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onStarted chord:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 116
    iget-object v1, p0, Lcom/samsung/groupcast/core/messaging/MessagingClient$1;->this$0:Lcom/samsung/groupcast/core/messaging/MessagingClient;

    # getter for: Lcom/samsung/groupcast/core/messaging/MessagingClient;->mConnectionBlockQ:Ljava/util/concurrent/SynchronousQueue;
    invoke-static {v1}, Lcom/samsung/groupcast/core/messaging/MessagingClient;->access$000(Lcom/samsung/groupcast/core/messaging/MessagingClient;)Ljava/util/concurrent/SynchronousQueue;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 118
    :try_start_0
    iget-object v1, p0, Lcom/samsung/groupcast/core/messaging/MessagingClient$1;->this$0:Lcom/samsung/groupcast/core/messaging/MessagingClient;

    # getter for: Lcom/samsung/groupcast/core/messaging/MessagingClient;->mConnectionBlockQ:Ljava/util/concurrent/SynchronousQueue;
    invoke-static {v1}, Lcom/samsung/groupcast/core/messaging/MessagingClient;->access$000(Lcom/samsung/groupcast/core/messaging/MessagingClient;)Ljava/util/concurrent/SynchronousQueue;

    move-result-object v1

    const-string v2, "started"

    invoke-virtual {v1, v2}, Ljava/util/concurrent/SynchronousQueue;->put(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 127
    :goto_0
    iget-object v1, p0, Lcom/samsung/groupcast/core/messaging/MessagingClient$1;->this$0:Lcom/samsung/groupcast/core/messaging/MessagingClient;

    # setter for: Lcom/samsung/groupcast/core/messaging/MessagingClient;->mChordStarted:Z
    invoke-static {v1, v4}, Lcom/samsung/groupcast/core/messaging/MessagingClient;->access$102(Lcom/samsung/groupcast/core/messaging/MessagingClient;Z)Z

    .line 129
    if-ne v4, p2, :cond_0

    .line 130
    const-string v1, "STARTED_BY_RECONNECTION"

    invoke-static {v1}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 134
    :cond_0
    return-void

    .line 119
    :catch_0
    move-exception v0

    .line 120
    .local v0, "e":Ljava/lang/InterruptedException;
    const-string v1, "MessagingClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mConnectionBlockQ e:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/groupcast/application/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0

    .line 124
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :cond_1
    const-string v1, "MessagingClient"

    const-string v2, "mConnectionBlockQ is null"

    invoke-static {v1, v2}, Lcom/samsung/groupcast/application/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onStopped(I)V
    .locals 3
    .param p1, "err"    # I

    .prologue
    .line 138
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "err:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 139
    iget-object v0, p0, Lcom/samsung/groupcast/core/messaging/MessagingClient$1;->this$0:Lcom/samsung/groupcast/core/messaging/MessagingClient;

    const/4 v1, 0x0

    # setter for: Lcom/samsung/groupcast/core/messaging/MessagingClient;->mChordStarted:Z
    invoke-static {v0, v1}, Lcom/samsung/groupcast/core/messaging/MessagingClient;->access$102(Lcom/samsung/groupcast/core/messaging/MessagingClient;Z)Z

    .line 140
    const/16 v0, 0x3eb

    if-ne v0, p1, :cond_0

    .line 141
    iget-object v0, p0, Lcom/samsung/groupcast/core/messaging/MessagingClient$1;->this$0:Lcom/samsung/groupcast/core/messaging/MessagingClient;

    # invokes: Lcom/samsung/groupcast/core/messaging/MessagingClient;->getChordMgrInternal()Lcom/samsung/android/sdk/chord/SchordManager;
    invoke-static {v0}, Lcom/samsung/groupcast/core/messaging/MessagingClient;->access$200(Lcom/samsung/groupcast/core/messaging/MessagingClient;)Lcom/samsung/android/sdk/chord/SchordManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/chord/SchordManager;->stop()V

    .line 144
    :cond_0
    iget-object v0, p0, Lcom/samsung/groupcast/core/messaging/MessagingClient$1;->this$0:Lcom/samsung/groupcast/core/messaging/MessagingClient;

    # invokes: Lcom/samsung/groupcast/core/messaging/MessagingClient;->endChordInternal()V
    invoke-static {v0}, Lcom/samsung/groupcast/core/messaging/MessagingClient;->access$300(Lcom/samsung/groupcast/core/messaging/MessagingClient;)V

    .line 145
    iget-object v0, p0, Lcom/samsung/groupcast/core/messaging/MessagingClient$1;->this$0:Lcom/samsung/groupcast/core/messaging/MessagingClient;

    # getter for: Lcom/samsung/groupcast/core/messaging/MessagingClient;->mLocalChannels:Ljava/util/HashMap;
    invoke-static {v0}, Lcom/samsung/groupcast/core/messaging/MessagingClient;->access$400(Lcom/samsung/groupcast/core/messaging/MessagingClient;)Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 148
    invoke-static {}, Lcom/samsung/groupcast/core/session/PackageSessionManager;->getInstance()Lcom/samsung/groupcast/core/session/PackageSessionManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/groupcast/core/session/PackageSessionManager;->lostConnection()V

    .line 149
    invoke-static {}, Lcom/samsung/groupcast/application/App;->getInstance()Lcom/samsung/groupcast/application/App;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.samsung.groupplay.intent.action.connection.wifi.lost"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/samsung/groupcast/application/App;->sendBroadcast(Landroid/content/Intent;)V

    .line 151
    const-string v0, "MessagingClient"

    const-string v1, "broadcast:com.samsung.groupplay.intent.action.connection.wifi.lost"

    invoke-static {v0, v1}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 153
    return-void
.end method
