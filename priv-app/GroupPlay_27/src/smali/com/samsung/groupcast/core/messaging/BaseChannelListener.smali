.class public Lcom/samsung/groupcast/core/messaging/BaseChannelListener;
.super Ljava/lang/Object;
.source "BaseChannelListener.java"

# interfaces
.implements Lcom/samsung/groupcast/core/messaging/ChannelListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method protected onContentRequestV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileRequestV1;Ljava/lang/String;)V
    .locals 0
    .param p1, "channel"    # Lcom/samsung/groupcast/core/messaging/Channel;
    .param p2, "node"    # Ljava/lang/String;
    .param p3, "message"    # Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileRequestV1;
    .param p4, "contentId"    # Ljava/lang/String;

    .prologue
    .line 83
    return-void
.end method

.method public onCurrentPageInfoV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/CurrentPageInfoV1;)V
    .locals 0
    .param p1, "channel"    # Lcom/samsung/groupcast/core/messaging/Channel;
    .param p2, "node"    # Ljava/lang/String;
    .param p3, "message"    # Lcom/samsung/groupcast/legacy/gp2/messaging/v1/CurrentPageInfoV1;

    .prologue
    .line 67
    return-void
.end method

.method public onCurrentPageRequestV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/CurrentPageRequestV1;)V
    .locals 0
    .param p1, "channel"    # Lcom/samsung/groupcast/core/messaging/Channel;
    .param p2, "node"    # Ljava/lang/String;
    .param p3, "message"    # Lcom/samsung/groupcast/legacy/gp2/messaging/v1/CurrentPageRequestV1;

    .prologue
    .line 64
    return-void
.end method

.method public onDrawingSyncJSONResponseV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/DrawingSyncJsonResponseV1;)V
    .locals 0
    .param p1, "channel"    # Lcom/samsung/groupcast/core/messaging/Channel;
    .param p2, "node"    # Ljava/lang/String;
    .param p3, "message"    # Lcom/samsung/groupcast/legacy/gp2/messaging/v1/DrawingSyncJsonResponseV1;

    .prologue
    .line 108
    return-void
.end method

.method public onDrawingSyncJsonRequest(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/DrawingSyncJsonRequestV1;)V
    .locals 0
    .param p1, "channel"    # Lcom/samsung/groupcast/core/messaging/Channel;
    .param p2, "node"    # Ljava/lang/String;
    .param p3, "message"    # Lcom/samsung/groupcast/legacy/gp2/messaging/v1/DrawingSyncJsonRequestV1;

    .prologue
    .line 112
    return-void
.end method

.method public onDrawingSyncResponseV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/DrawingSyncNotificationV1;)V
    .locals 0
    .param p1, "channel"    # Lcom/samsung/groupcast/core/messaging/Channel;
    .param p2, "node"    # Ljava/lang/String;
    .param p3, "message"    # Lcom/samsung/groupcast/legacy/gp2/messaging/v1/DrawingSyncNotificationV1;

    .prologue
    .line 104
    return-void
.end method

.method public onFileAvailabilityInfoV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileAvailabilityInfoV1;)V
    .locals 0
    .param p1, "channel"    # Lcom/samsung/groupcast/core/messaging/Channel;
    .param p2, "node"    # Ljava/lang/String;
    .param p3, "message"    # Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileAvailabilityInfoV1;

    .prologue
    .line 74
    return-void
.end method

.method public onFileOfferV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileOfferV1;)V
    .locals 0
    .param p1, "channel"    # Lcom/samsung/groupcast/core/messaging/Channel;
    .param p2, "node"    # Ljava/lang/String;
    .param p3, "message"    # Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileOfferV1;

    .prologue
    .line 86
    return-void
.end method

.method public onFileReceiveFailureV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileOfferV1;)V
    .locals 0
    .param p1, "channel"    # Lcom/samsung/groupcast/core/messaging/Channel;
    .param p2, "node"    # Ljava/lang/String;
    .param p3, "offer"    # Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileOfferV1;

    .prologue
    .line 97
    return-void
.end method

.method public onFileReceiveProgressV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileOfferV1;JJ)V
    .locals 0
    .param p1, "channel"    # Lcom/samsung/groupcast/core/messaging/Channel;
    .param p2, "node"    # Ljava/lang/String;
    .param p3, "offer"    # Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileOfferV1;
    .param p4, "offset"    # J
    .param p6, "size"    # J

    .prologue
    .line 90
    return-void
.end method

.method public onFileReceiveSuccessV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileOfferV1;Ljava/lang/String;)V
    .locals 0
    .param p1, "channel"    # Lcom/samsung/groupcast/core/messaging/Channel;
    .param p2, "node"    # Ljava/lang/String;
    .param p3, "offer"    # Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileOfferV1;
    .param p4, "tempPath"    # Ljava/lang/String;

    .prologue
    .line 94
    return-void
.end method

.method public onFileRequestV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileRequestV1;)V
    .locals 3
    .param p1, "channel"    # Lcom/samsung/groupcast/core/messaging/Channel;
    .param p2, "node"    # Ljava/lang/String;
    .param p3, "message"    # Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileRequestV1;

    .prologue
    .line 53
    invoke-virtual {p3}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileRequestV1;->getDataKey()Lcom/samsung/groupcast/core/messaging/DataKey;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/groupcast/core/messaging/DataKey;->getDataType()Ljava/lang/String;

    move-result-object v1

    .line 54
    .local v1, "dataType":Ljava/lang/String;
    invoke-virtual {p3}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileRequestV1;->getDataKey()Lcom/samsung/groupcast/core/messaging/DataKey;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/groupcast/core/messaging/DataKey;->getDataId()Ljava/lang/String;

    move-result-object v0

    .line 56
    .local v0, "dataId":Ljava/lang/String;
    const-string v2, "MANIFEST"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 57
    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/samsung/groupcast/core/messaging/BaseChannelListener;->onManifestRequestV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileRequestV1;Ljava/lang/String;)V

    .line 61
    :cond_0
    :goto_0
    return-void

    .line 58
    :cond_1
    const-string v2, "CONTENT"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 59
    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/samsung/groupcast/core/messaging/BaseChannelListener;->onContentRequestV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileRequestV1;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onManifestInfoV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/ManifestInfoV1;)V
    .locals 0
    .param p1, "channel"    # Lcom/samsung/groupcast/core/messaging/Channel;
    .param p2, "node"    # Ljava/lang/String;
    .param p3, "message"    # Lcom/samsung/groupcast/legacy/gp2/messaging/v1/ManifestInfoV1;

    .prologue
    .line 70
    return-void
.end method

.method protected onManifestRequestV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileRequestV1;Ljava/lang/String;)V
    .locals 0
    .param p1, "channel"    # Lcom/samsung/groupcast/core/messaging/Channel;
    .param p2, "node"    # Ljava/lang/String;
    .param p3, "message"    # Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileRequestV1;
    .param p4, "manifestId"    # Ljava/lang/String;

    .prologue
    .line 80
    return-void
.end method

.method public onPageCommandV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageCommandV1;)V
    .locals 0
    .param p1, "channel"    # Lcom/samsung/groupcast/core/messaging/Channel;
    .param p2, "node"    # Ljava/lang/String;
    .param p3, "message"    # Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageCommandV1;

    .prologue
    .line 100
    return-void
.end method

.method public onPageEditV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageEditV1;)V
    .locals 0
    .param p1, "channel"    # Lcom/samsung/groupcast/core/messaging/Channel;
    .param p2, "node"    # Ljava/lang/String;
    .param p3, "message"    # Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageEditV1;

    .prologue
    .line 77
    return-void
.end method

.method public onParticipantInfoChanged(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;)V
    .locals 0
    .param p1, "channel"    # Lcom/samsung/groupcast/core/messaging/Channel;
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    .line 28
    return-void
.end method

.method public onPeerJoined(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;)V
    .locals 0
    .param p1, "channel"    # Lcom/samsung/groupcast/core/messaging/Channel;
    .param p2, "node"    # Ljava/lang/String;

    .prologue
    .line 22
    return-void
.end method

.method public onPeerLeft(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;)V
    .locals 0
    .param p1, "channel"    # Lcom/samsung/groupcast/core/messaging/Channel;
    .param p2, "node"    # Ljava/lang/String;

    .prologue
    .line 25
    return-void
.end method

.method public onPingV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PingV1;)V
    .locals 3
    .param p1, "channel"    # Lcom/samsung/groupcast/core/messaging/Channel;
    .param p2, "node"    # Ljava/lang/String;
    .param p3, "message"    # Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PingV1;

    .prologue
    .line 32
    invoke-virtual {p3}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PingV1;->getId()Ljava/lang/Integer;

    move-result-object v0

    .line 33
    .local v0, "id":Ljava/lang/Integer;
    new-instance v1, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PongV1;

    invoke-interface {p1}, Lcom/samsung/groupcast/core/messaging/Channel;->getUserInfo()Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PongV1;-><init>(Ljava/lang/Integer;Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;)V

    .line 34
    .local v1, "reply":Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PongV1;
    const/4 v2, 0x0

    invoke-interface {p1, p2, v1, v2}, Lcom/samsung/groupcast/core/messaging/Channel;->sendMessageToNode(Ljava/lang/String;Lcom/samsung/groupcast/core/messaging/Message;Z)V

    .line 35
    return-void
.end method

.method public onPongV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PongV1;)V
    .locals 8
    .param p1, "channel"    # Lcom/samsung/groupcast/core/messaging/Channel;
    .param p2, "node"    # Ljava/lang/String;
    .param p3, "message"    # Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PongV1;

    .prologue
    .line 39
    invoke-virtual {p3}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PongV1;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p3}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PongV1;->getId()Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {p3}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PongV1;->getWifi()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p3}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PongV1;->getJoinedActivities()[Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p3}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PongV1;->getContentsResetEnabled()Z

    move-result v6

    invoke-virtual {p3}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PongV1;->getJoinedGames()[Ljava/lang/String;

    move-result-object v7

    move-object v0, p1

    move-object v1, p2

    invoke-interface/range {v0 .. v7}, Lcom/samsung/groupcast/core/messaging/Channel;->updateParticipants(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;[Ljava/lang/String;Z[Ljava/lang/String;)V

    .line 42
    return-void
.end method

.method public onSessionSummaryInfoV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/SessionSummaryInfoV1;)V
    .locals 0
    .param p1, "channel"    # Lcom/samsung/groupcast/core/messaging/Channel;
    .param p2, "node"    # Ljava/lang/String;
    .param p3, "message"    # Lcom/samsung/groupcast/legacy/gp2/messaging/v1/SessionSummaryInfoV1;

    .prologue
    .line 49
    return-void
.end method

.method public onSessionSummaryRequestV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/SessionSummaryRequestV1;)V
    .locals 0
    .param p1, "channel"    # Lcom/samsung/groupcast/core/messaging/Channel;
    .param p2, "node"    # Ljava/lang/String;
    .param p3, "message"    # Lcom/samsung/groupcast/legacy/gp2/messaging/v1/SessionSummaryRequestV1;

    .prologue
    .line 46
    return-void
.end method
