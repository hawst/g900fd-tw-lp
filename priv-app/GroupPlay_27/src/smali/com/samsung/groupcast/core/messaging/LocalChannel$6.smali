.class Lcom/samsung/groupcast/core/messaging/LocalChannel$6;
.super Ljava/lang/Object;
.source "LocalChannel.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/groupcast/core/messaging/LocalChannel;->onFileWillReceive(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/groupcast/core/messaging/LocalChannel;

.field final synthetic val$fromNodeF:Ljava/lang/String;

.field final synthetic val$offer:Lcom/samsung/groupcast/core/messaging/FileOffer;


# direct methods
.method constructor <init>(Lcom/samsung/groupcast/core/messaging/LocalChannel;Lcom/samsung/groupcast/core/messaging/FileOffer;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 592
    iput-object p1, p0, Lcom/samsung/groupcast/core/messaging/LocalChannel$6;->this$0:Lcom/samsung/groupcast/core/messaging/LocalChannel;

    iput-object p2, p0, Lcom/samsung/groupcast/core/messaging/LocalChannel$6;->val$offer:Lcom/samsung/groupcast/core/messaging/FileOffer;

    iput-object p3, p0, Lcom/samsung/groupcast/core/messaging/LocalChannel$6;->val$fromNodeF:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 595
    iget-object v2, p0, Lcom/samsung/groupcast/core/messaging/LocalChannel$6;->this$0:Lcom/samsung/groupcast/core/messaging/LocalChannel;

    # getter for: Lcom/samsung/groupcast/core/messaging/LocalChannel;->mListeners:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/samsung/groupcast/core/messaging/LocalChannel;->access$200(Lcom/samsung/groupcast/core/messaging/LocalChannel;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/groupcast/core/messaging/ChannelListener;

    .line 596
    .local v1, "listener":Lcom/samsung/groupcast/core/messaging/ChannelListener;
    iget-object v2, p0, Lcom/samsung/groupcast/core/messaging/LocalChannel$6;->val$offer:Lcom/samsung/groupcast/core/messaging/FileOffer;

    iget-object v3, p0, Lcom/samsung/groupcast/core/messaging/LocalChannel$6;->this$0:Lcom/samsung/groupcast/core/messaging/LocalChannel;

    iget-object v4, p0, Lcom/samsung/groupcast/core/messaging/LocalChannel$6;->val$fromNodeF:Ljava/lang/String;

    invoke-interface {v2, v1, v3, v4}, Lcom/samsung/groupcast/core/messaging/FileOffer;->notifyListenerOfOffer(Lcom/samsung/groupcast/core/messaging/ChannelListener;Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;)V

    goto :goto_0

    .line 598
    .end local v1    # "listener":Lcom/samsung/groupcast/core/messaging/ChannelListener;
    :cond_0
    return-void
.end method
