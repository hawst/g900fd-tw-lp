.class Lcom/samsung/groupcast/core/messaging/MessagingClient$2;
.super Ljava/lang/Object;
.source "MessagingClient.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/groupcast/core/messaging/MessagingClient;->releaseConnection(Lcom/samsung/groupcast/core/messaging/ConnectionHandle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/groupcast/core/messaging/MessagingClient;


# direct methods
.method constructor <init>(Lcom/samsung/groupcast/core/messaging/MessagingClient;)V
    .locals 0

    .prologue
    .line 466
    iput-object p1, p0, Lcom/samsung/groupcast/core/messaging/MessagingClient$2;->this$0:Lcom/samsung/groupcast/core/messaging/MessagingClient;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 469
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "releaseConnection"

    const-string v2, "start stopping magnet"

    invoke-static {v0, v1, v2}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;)V

    .line 470
    iget-object v0, p0, Lcom/samsung/groupcast/core/messaging/MessagingClient$2;->this$0:Lcom/samsung/groupcast/core/messaging/MessagingClient;

    invoke-virtual {v0}, Lcom/samsung/groupcast/core/messaging/MessagingClient;->isChordStarted()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 471
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "releaseConnection"

    const-string v2, "stopping magnet"

    invoke-static {v0, v1, v2}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;)V

    .line 473
    iget-object v0, p0, Lcom/samsung/groupcast/core/messaging/MessagingClient$2;->this$0:Lcom/samsung/groupcast/core/messaging/MessagingClient;

    # invokes: Lcom/samsung/groupcast/core/messaging/MessagingClient;->getChordMgrInternal()Lcom/samsung/android/sdk/chord/SchordManager;
    invoke-static {v0}, Lcom/samsung/groupcast/core/messaging/MessagingClient;->access$200(Lcom/samsung/groupcast/core/messaging/MessagingClient;)Lcom/samsung/android/sdk/chord/SchordManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/chord/SchordManager;->stop()V

    .line 478
    :cond_0
    return-void
.end method
