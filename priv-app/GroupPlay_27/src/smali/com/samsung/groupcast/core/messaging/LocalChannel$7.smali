.class Lcom/samsung/groupcast/core/messaging/LocalChannel$7;
.super Ljava/lang/Object;
.source "LocalChannel.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/groupcast/core/messaging/LocalChannel;->onFileChunkReceived(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/groupcast/core/messaging/LocalChannel;

.field final synthetic val$fileSizeF:J

.field final synthetic val$fromNodeF:Ljava/lang/String;

.field final synthetic val$offer:Lcom/samsung/groupcast/core/messaging/FileOffer;

.field final synthetic val$offsetF:J


# direct methods
.method constructor <init>(Lcom/samsung/groupcast/core/messaging/LocalChannel;Lcom/samsung/groupcast/core/messaging/FileOffer;Ljava/lang/String;JJ)V
    .locals 0

    .prologue
    .line 640
    iput-object p1, p0, Lcom/samsung/groupcast/core/messaging/LocalChannel$7;->this$0:Lcom/samsung/groupcast/core/messaging/LocalChannel;

    iput-object p2, p0, Lcom/samsung/groupcast/core/messaging/LocalChannel$7;->val$offer:Lcom/samsung/groupcast/core/messaging/FileOffer;

    iput-object p3, p0, Lcom/samsung/groupcast/core/messaging/LocalChannel$7;->val$fromNodeF:Ljava/lang/String;

    iput-wide p4, p0, Lcom/samsung/groupcast/core/messaging/LocalChannel$7;->val$offsetF:J

    iput-wide p6, p0, Lcom/samsung/groupcast/core/messaging/LocalChannel$7;->val$fileSizeF:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    .line 643
    iget-object v0, p0, Lcom/samsung/groupcast/core/messaging/LocalChannel$7;->this$0:Lcom/samsung/groupcast/core/messaging/LocalChannel;

    # getter for: Lcom/samsung/groupcast/core/messaging/LocalChannel;->mListeners:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/groupcast/core/messaging/LocalChannel;->access$200(Lcom/samsung/groupcast/core/messaging/LocalChannel;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .local v8, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/groupcast/core/messaging/ChannelListener;

    .line 644
    .local v1, "listener":Lcom/samsung/groupcast/core/messaging/ChannelListener;
    iget-object v0, p0, Lcom/samsung/groupcast/core/messaging/LocalChannel$7;->val$offer:Lcom/samsung/groupcast/core/messaging/FileOffer;

    iget-object v2, p0, Lcom/samsung/groupcast/core/messaging/LocalChannel$7;->this$0:Lcom/samsung/groupcast/core/messaging/LocalChannel;

    iget-object v3, p0, Lcom/samsung/groupcast/core/messaging/LocalChannel$7;->val$fromNodeF:Ljava/lang/String;

    iget-wide v4, p0, Lcom/samsung/groupcast/core/messaging/LocalChannel$7;->val$offsetF:J

    iget-wide v6, p0, Lcom/samsung/groupcast/core/messaging/LocalChannel$7;->val$fileSizeF:J

    invoke-interface/range {v0 .. v7}, Lcom/samsung/groupcast/core/messaging/FileOffer;->notifyListenerOfProgress(Lcom/samsung/groupcast/core/messaging/ChannelListener;Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;JJ)V

    goto :goto_0

    .line 647
    .end local v1    # "listener":Lcom/samsung/groupcast/core/messaging/ChannelListener;
    :cond_0
    return-void
.end method
