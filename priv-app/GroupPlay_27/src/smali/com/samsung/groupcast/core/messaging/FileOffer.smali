.class public interface abstract Lcom/samsung/groupcast/core/messaging/FileOffer;
.super Ljava/lang/Object;
.source "FileOffer.java"

# interfaces
.implements Lcom/samsung/groupcast/core/messaging/FileExchangeMessage;


# virtual methods
.method public abstract getToken()Lcom/samsung/groupcast/core/messaging/FileOfferToken;
.end method

.method public abstract notifyListenerOfFailure(Lcom/samsung/groupcast/core/messaging/ChannelListener;Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;)V
.end method

.method public abstract notifyListenerOfOffer(Lcom/samsung/groupcast/core/messaging/ChannelListener;Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;)V
.end method

.method public abstract notifyListenerOfProgress(Lcom/samsung/groupcast/core/messaging/ChannelListener;Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;JJ)V
.end method

.method public abstract notifyListenerOfSuccess(Lcom/samsung/groupcast/core/messaging/ChannelListener;Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Ljava/lang/String;)V
.end method
