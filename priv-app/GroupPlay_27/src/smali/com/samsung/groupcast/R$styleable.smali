.class public final Lcom/samsung/groupcast/R$styleable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/groupcast/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "styleable"
.end annotation


# static fields
.field public static final HorizontalListView:[I

.field public static final HorizontalListView_android_scrollX:I = 0x9

.field public static final HorizontalListView_android_scrollY:I = 0xa

.field public static final HorizontalListView_android_scrollbarAlwaysDrawHorizontalTrack:I = 0x5

.field public static final HorizontalListView_android_scrollbarAlwaysDrawVerticalTrack:I = 0x6

.field public static final HorizontalListView_android_scrollbarDefaultDelayBeforeFade:I = 0xf

.field public static final HorizontalListView_android_scrollbarFadeDuration:I = 0xe

.field public static final HorizontalListView_android_scrollbarSize:I = 0x0

.field public static final HorizontalListView_android_scrollbarStyle:I = 0x7

.field public static final HorizontalListView_android_scrollbarThumbHorizontal:I = 0x1

.field public static final HorizontalListView_android_scrollbarThumbVertical:I = 0x2

.field public static final HorizontalListView_android_scrollbarTrackHorizontal:I = 0x3

.field public static final HorizontalListView_android_scrollbarTrackVertical:I = 0x4

.field public static final HorizontalListView_android_scrollbars:I = 0xc

.field public static final HorizontalListView_android_soundEffectsEnabled:I = 0xd

.field public static final HorizontalListView_android_tag:I = 0x8

.field public static final HorizontalListView_android_visibility:I = 0xb

.field public static final SplitView:[I

.field public static final SplitView_handle:I = 0x0

.field public static final SplitView_primaryContent:I = 0x1

.field public static final SplitView_secondaryContent:I = 0x2

.field public static final volume:[I

.field public static final volume_state_volume_on:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 11238
    const/16 v0, 0x10

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/samsung/groupcast/R$styleable;->HorizontalListView:[I

    .line 11354
    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/samsung/groupcast/R$styleable;->SplitView:[I

    .line 11397
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const/high16 v2, 0x7f010000

    aput v2, v0, v1

    sput-object v0, Lcom/samsung/groupcast/R$styleable;->volume:[I

    return-void

    .line 11238
    :array_0
    .array-data 4
        0x1010063
        0x1010064
        0x1010065
        0x1010066
        0x1010067
        0x1010068
        0x1010069
        0x101007f
        0x10100d1
        0x10100d2
        0x10100d3
        0x10100dc
        0x10100de
        0x1010215
        0x10102a8
        0x10102a9
    .end array-data

    .line 11354
    :array_1
    .array-data 4
        0x7f010001
        0x7f010002
        0x7f010003
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11197
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
