.class public Lcom/samsung/groupcast/service/GroupPlayPacket;
.super Ljava/lang/Object;
.source "GroupPlayPacket.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/samsung/groupcast/service/GroupPlayPacket;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final channel:Ljava/lang/String;

.field public final fromNode:Ljava/lang/String;

.field public final isBroadcast:I

.field mByteArray:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<[B>;"
        }
    .end annotation
.end field

.field public final payloadType:Ljava/lang/String;

.field public final pkgName:Ljava/lang/String;

.field public final toNode:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 101
    new-instance v0, Lcom/samsung/groupcast/service/GroupPlayPacket$1;

    invoke-direct {v0}, Lcom/samsung/groupcast/service/GroupPlayPacket$1;-><init>()V

    sput-object v0, Lcom/samsung/groupcast/service/GroupPlayPacket;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 5
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 82
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lcom/samsung/groupcast/service/GroupPlayPacket;->mByteArray:Ljava/util/ArrayList;

    .line 83
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/groupcast/service/GroupPlayPacket;->pkgName:Ljava/lang/String;

    .line 84
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/groupcast/service/GroupPlayPacket;->fromNode:Ljava/lang/String;

    .line 85
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/groupcast/service/GroupPlayPacket;->channel:Ljava/lang/String;

    .line 86
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/groupcast/service/GroupPlayPacket;->toNode:Ljava/lang/String;

    .line 87
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/groupcast/service/GroupPlayPacket;->payloadType:Ljava/lang/String;

    .line 88
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    iput v3, p0, Lcom/samsung/groupcast/service/GroupPlayPacket;->isBroadcast:I

    .line 92
    :try_start_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 93
    .local v2, "total":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_0

    .line 94
    iget-object v3, p0, Lcom/samsung/groupcast/service/GroupPlayPacket;->mByteArray:Ljava/util/ArrayList;

    invoke-virtual {p1}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 93
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 96
    .end local v1    # "i":I
    .end local v2    # "total":I
    :catch_0
    move-exception v0

    .line 97
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 99
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_0
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/samsung/groupcast/service/GroupPlayPacket$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/samsung/groupcast/service/GroupPlayPacket$1;

    .prologue
    .line 9
    invoke-direct {p0, p1}, Lcom/samsung/groupcast/service/GroupPlayPacket;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;[[B)V
    .locals 1
    .param p1, "pkgName"    # Ljava/lang/String;
    .param p2, "fromNode"    # Ljava/lang/String;
    .param p3, "channel"    # Ljava/lang/String;
    .param p4, "toNode"    # Ljava/lang/String;
    .param p5, "isToAll"    # I
    .param p6, "payloadType"    # Ljava/lang/String;
    .param p7, "payload"    # [[B

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/groupcast/service/GroupPlayPacket;->mByteArray:Ljava/util/ArrayList;

    .line 23
    iput-object p1, p0, Lcom/samsung/groupcast/service/GroupPlayPacket;->pkgName:Ljava/lang/String;

    .line 24
    iput-object p2, p0, Lcom/samsung/groupcast/service/GroupPlayPacket;->fromNode:Ljava/lang/String;

    .line 25
    iput-object p3, p0, Lcom/samsung/groupcast/service/GroupPlayPacket;->channel:Ljava/lang/String;

    .line 26
    iput-object p4, p0, Lcom/samsung/groupcast/service/GroupPlayPacket;->toNode:Ljava/lang/String;

    .line 27
    iput-object p6, p0, Lcom/samsung/groupcast/service/GroupPlayPacket;->payloadType:Ljava/lang/String;

    .line 28
    iput p5, p0, Lcom/samsung/groupcast/service/GroupPlayPacket;->isBroadcast:I

    .line 30
    invoke-virtual {p0, p7}, Lcom/samsung/groupcast/service/GroupPlayPacket;->setData([[B)V

    .line 31
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 63
    const/4 v0, 0x0

    return v0
.end method

.method public getData()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<[B>;"
        }
    .end annotation

    .prologue
    .line 49
    iget-object v0, p0, Lcom/samsung/groupcast/service/GroupPlayPacket;->mByteArray:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getDataInArray()[[B
    .locals 3

    .prologue
    .line 53
    iget-object v2, p0, Lcom/samsung/groupcast/service/GroupPlayPacket;->mByteArray:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    new-array v0, v2, [[B

    .line 54
    .local v0, "data":[[B
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/samsung/groupcast/service/GroupPlayPacket;->mByteArray:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 55
    iget-object v2, p0, Lcom/samsung/groupcast/service/GroupPlayPacket;->mByteArray:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [B

    invoke-virtual {v2}, [B->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [B

    aput-object v2, v0, v1

    .line 54
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 57
    :cond_0
    return-object v0
.end method

.method public setData([[B)V
    .locals 5
    .param p1, "data"    # [[B

    .prologue
    .line 39
    iget-object v4, p0, Lcom/samsung/groupcast/service/GroupPlayPacket;->mByteArray:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    .line 40
    iget-object v4, p0, Lcom/samsung/groupcast/service/GroupPlayPacket;->mByteArray:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    .line 43
    :cond_0
    move-object v0, p1

    .local v0, "arr$":[[B
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v1, v0, v2

    .line 44
    .local v1, "bs":[B
    iget-object v4, p0, Lcom/samsung/groupcast/service/GroupPlayPacket;->mByteArray:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 43
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 46
    .end local v1    # "bs":[B
    :cond_1
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 35
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "GP:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/groupcast/service/GroupPlayPacket;->pkgName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/groupcast/service/GroupPlayPacket;->fromNode:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/groupcast/service/GroupPlayPacket;->channel:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/groupcast/service/GroupPlayPacket;->toNode:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/groupcast/service/GroupPlayPacket;->payloadType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3
    .param p1, "arg0"    # Landroid/os/Parcel;
    .param p2, "arg1"    # I

    .prologue
    .line 68
    iget-object v2, p0, Lcom/samsung/groupcast/service/GroupPlayPacket;->pkgName:Ljava/lang/String;

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 69
    iget-object v2, p0, Lcom/samsung/groupcast/service/GroupPlayPacket;->fromNode:Ljava/lang/String;

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 70
    iget-object v2, p0, Lcom/samsung/groupcast/service/GroupPlayPacket;->channel:Ljava/lang/String;

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 71
    iget-object v2, p0, Lcom/samsung/groupcast/service/GroupPlayPacket;->toNode:Ljava/lang/String;

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 72
    iget-object v2, p0, Lcom/samsung/groupcast/service/GroupPlayPacket;->payloadType:Ljava/lang/String;

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 73
    iget v2, p0, Lcom/samsung/groupcast/service/GroupPlayPacket;->isBroadcast:I

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 76
    iget-object v2, p0, Lcom/samsung/groupcast/service/GroupPlayPacket;->mByteArray:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 77
    iget-object v2, p0, Lcom/samsung/groupcast/service/GroupPlayPacket;->mByteArray:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    .line 78
    .local v0, "bs":[B
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    goto :goto_0

    .line 80
    .end local v0    # "bs":[B
    :cond_0
    return-void
.end method
