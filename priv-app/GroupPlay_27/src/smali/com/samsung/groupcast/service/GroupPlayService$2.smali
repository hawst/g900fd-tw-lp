.class Lcom/samsung/groupcast/service/GroupPlayService$2;
.super Landroid/content/BroadcastReceiver;
.source "GroupPlayService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/groupcast/service/GroupPlayService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/groupcast/service/GroupPlayService;


# direct methods
.method constructor <init>(Lcom/samsung/groupcast/service/GroupPlayService;)V
    .locals 0

    .prologue
    .line 83
    iput-object p1, p0, Lcom/samsung/groupcast/service/GroupPlayService$2;->this$0:Lcom/samsung/groupcast/service/GroupPlayService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v5, 0x1

    .line 87
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 89
    .local v0, "action":Ljava/lang/String;
    const-string v3, "android.net.wifi.WIFI_STATE_CHANGED"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 90
    iget-object v3, p0, Lcom/samsung/groupcast/service/GroupPlayService$2;->this$0:Lcom/samsung/groupcast/service/GroupPlayService;

    # getter for: Lcom/samsung/groupcast/service/GroupPlayService;->TAG:Ljava/lang/String;
    invoke-static {v3}, Lcom/samsung/groupcast/service/GroupPlayService;->access$200(Lcom/samsung/groupcast/service/GroupPlayService;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "WiFi Receiver - WIFI_STATE_CHANGED_ACTION"

    invoke-static {v3, v4}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 91
    const-string v3, "wifi_state"

    invoke-virtual {p2, v3, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 95
    .local v2, "wifiState":I
    packed-switch v2, :pswitch_data_0

    .line 157
    .end local v2    # "wifiState":I
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 97
    .restart local v2    # "wifiState":I
    :pswitch_1
    iget-object v3, p0, Lcom/samsung/groupcast/service/GroupPlayService$2;->this$0:Lcom/samsung/groupcast/service/GroupPlayService;

    # getter for: Lcom/samsung/groupcast/service/GroupPlayService;->TAG:Ljava/lang/String;
    invoke-static {v3}, Lcom/samsung/groupcast/service/GroupPlayService;->access$200(Lcom/samsung/groupcast/service/GroupPlayService;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "WiFi Receiver - WIFI_STATE_DISABLED"

    invoke-static {v3, v4}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 101
    :pswitch_2
    iget-object v3, p0, Lcom/samsung/groupcast/service/GroupPlayService$2;->this$0:Lcom/samsung/groupcast/service/GroupPlayService;

    # getter for: Lcom/samsung/groupcast/service/GroupPlayService;->TAG:Ljava/lang/String;
    invoke-static {v3}, Lcom/samsung/groupcast/service/GroupPlayService;->access$200(Lcom/samsung/groupcast/service/GroupPlayService;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "WiFi Receiver - WIFI_STATE_DISABLING"

    invoke-static {v3, v4}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 105
    :pswitch_3
    iget-object v3, p0, Lcom/samsung/groupcast/service/GroupPlayService$2;->this$0:Lcom/samsung/groupcast/service/GroupPlayService;

    # getter for: Lcom/samsung/groupcast/service/GroupPlayService;->TAG:Ljava/lang/String;
    invoke-static {v3}, Lcom/samsung/groupcast/service/GroupPlayService;->access$200(Lcom/samsung/groupcast/service/GroupPlayService;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "WiFi Receiver - WIFI_STATE_UNKNOWN"

    invoke-static {v3, v4}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 110
    :pswitch_4
    iget-object v3, p0, Lcom/samsung/groupcast/service/GroupPlayService$2;->this$0:Lcom/samsung/groupcast/service/GroupPlayService;

    # getter for: Lcom/samsung/groupcast/service/GroupPlayService;->TAG:Ljava/lang/String;
    invoke-static {v3}, Lcom/samsung/groupcast/service/GroupPlayService;->access$200(Lcom/samsung/groupcast/service/GroupPlayService;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "WiFi Receiver - WIFI_STATE_ENABLED"

    invoke-static {v3, v4}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 113
    .end local v2    # "wifiState":I
    :cond_1
    const-string v3, "android.net.wifi.STATE_CHANGE"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 114
    iget-object v3, p0, Lcom/samsung/groupcast/service/GroupPlayService$2;->this$0:Lcom/samsung/groupcast/service/GroupPlayService;

    # getter for: Lcom/samsung/groupcast/service/GroupPlayService;->TAG:Ljava/lang/String;
    invoke-static {v3}, Lcom/samsung/groupcast/service/GroupPlayService;->access$200(Lcom/samsung/groupcast/service/GroupPlayService;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "WiFi Receiver - NETWORK_STATE_CHANGED_ACTION"

    invoke-static {v3, v4}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 115
    const-string v3, "networkInfo"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/net/NetworkInfo;

    .line 118
    .local v1, "info":Landroid/net/NetworkInfo;
    if-eqz v1, :cond_0

    .line 121
    sget-object v3, Lcom/samsung/groupcast/service/GroupPlayService$3;->$SwitchMap$android$net$NetworkInfo$State:[I

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/NetworkInfo$State;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_1

    goto :goto_0

    .line 123
    :pswitch_5
    iget-object v3, p0, Lcom/samsung/groupcast/service/GroupPlayService$2;->this$0:Lcom/samsung/groupcast/service/GroupPlayService;

    # getter for: Lcom/samsung/groupcast/service/GroupPlayService;->TAG:Ljava/lang/String;
    invoke-static {v3}, Lcom/samsung/groupcast/service/GroupPlayService;->access$200(Lcom/samsung/groupcast/service/GroupPlayService;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "WiFi Receiver - Connected"

    invoke-static {v3, v4}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 143
    :pswitch_6
    iget-object v3, p0, Lcom/samsung/groupcast/service/GroupPlayService$2;->this$0:Lcom/samsung/groupcast/service/GroupPlayService;

    # getter for: Lcom/samsung/groupcast/service/GroupPlayService;->TAG:Ljava/lang/String;
    invoke-static {v3}, Lcom/samsung/groupcast/service/GroupPlayService;->access$200(Lcom/samsung/groupcast/service/GroupPlayService;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "Wifi Receiver - Disconnected"

    invoke-static {v3, v4}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 144
    invoke-virtual {p0}, Lcom/samsung/groupcast/service/GroupPlayService$2;->isInitialStickyBroadcast()Z

    move-result v3

    if-nez v3, :cond_0

    .line 145
    iget-object v3, p0, Lcom/samsung/groupcast/service/GroupPlayService$2;->this$0:Lcom/samsung/groupcast/service/GroupPlayService;

    invoke-virtual {v3, v5}, Lcom/samsung/groupcast/service/GroupPlayService;->stopForeground(Z)V

    .line 146
    iget-object v3, p0, Lcom/samsung/groupcast/service/GroupPlayService$2;->this$0:Lcom/samsung/groupcast/service/GroupPlayService;

    # invokes: Lcom/samsung/groupcast/service/GroupPlayService;->callMainActivity()V
    invoke-static {v3}, Lcom/samsung/groupcast/service/GroupPlayService;->access$000(Lcom/samsung/groupcast/service/GroupPlayService;)V

    goto/16 :goto_0

    .line 150
    .end local v1    # "info":Landroid/net/NetworkInfo;
    :cond_2
    const-string v3, "android.net.wifi.supplicant.STATE_CHANGE"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 153
    const-string v3, "android.net.wifi.CONFIGURED_NETWORKS_CHANGE"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    goto/16 :goto_0

    .line 95
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_4
        :pswitch_3
    .end packed-switch

    .line 121
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method
