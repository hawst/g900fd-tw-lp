.class Lcom/samsung/groupcast/service/GroupPlayService$1;
.super Landroid/content/BroadcastReceiver;
.source "GroupPlayService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/groupcast/service/GroupPlayService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/groupcast/service/GroupPlayService;


# direct methods
.method constructor <init>(Lcom/samsung/groupcast/service/GroupPlayService;)V
    .locals 0

    .prologue
    .line 38
    iput-object p1, p0, Lcom/samsung/groupcast/service/GroupPlayService$1;->this$0:Lcom/samsung/groupcast/service/GroupPlayService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v4, 0x0

    .line 41
    const-string v2, "android.net.wifi.WIFI_AP_STATE_CHANGED"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 42
    const-string v2, "wifi_state"

    const/16 v3, 0xe

    invoke-virtual {p2, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 46
    .local v0, "apState":I
    packed-switch v0, :pswitch_data_0

    .line 67
    :pswitch_0
    const-string v2, "GP_SVC"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "UNKNOWN:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    .end local v0    # "apState":I
    :cond_0
    :goto_0
    return-void

    .line 48
    .restart local v0    # "apState":I
    :pswitch_1
    const-string v2, "GP_SVC"

    const-string v3, "WIFI_AP_STATE_ENABLED!"

    invoke-static {v2, v3}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 51
    :pswitch_2
    const-string v2, "GP_SVC"

    const-string v3, "WIFI_AP_STATE_DISABLED!"

    invoke-static {v2, v3}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    invoke-static {v4}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->BroadCastGroupPlayAPStartNStopForTEDC(Z)V

    .line 58
    invoke-virtual {p0}, Lcom/samsung/groupcast/service/GroupPlayService$1;->isInitialStickyBroadcast()Z

    move-result v2

    if-nez v2, :cond_0

    .line 59
    iget-object v2, p0, Lcom/samsung/groupcast/service/GroupPlayService$1;->this$0:Lcom/samsung/groupcast/service/GroupPlayService;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/samsung/groupcast/service/GroupPlayService;->stopForeground(Z)V

    .line 60
    iget-object v2, p0, Lcom/samsung/groupcast/service/GroupPlayService$1;->this$0:Lcom/samsung/groupcast/service/GroupPlayService;

    # invokes: Lcom/samsung/groupcast/service/GroupPlayService;->callMainActivity()V
    invoke-static {v2}, Lcom/samsung/groupcast/service/GroupPlayService;->access$000(Lcom/samsung/groupcast/service/GroupPlayService;)V

    goto :goto_0

    .line 64
    :pswitch_3
    const-string v2, "GP_SVC"

    const-string v3, "WIFI_AP_STATE_FAILED!"

    invoke-static {v2, v3}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 71
    .end local v0    # "apState":I
    :cond_1
    const-string v2, "android.net.wifi.WIFI_AP_STA_STATUS_CHANGED"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 72
    const-string v2, "NUM"

    invoke-virtual {p2, v2, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 73
    .local v1, "countOfClient":I
    const-string v2, "GP_SVC"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "count:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiAPUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->isRvfHotspotEnabled()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 75
    iget-object v2, p0, Lcom/samsung/groupcast/service/GroupPlayService$1;->this$0:Lcom/samsung/groupcast/service/GroupPlayService;

    # invokes: Lcom/samsung/groupcast/service/GroupPlayService;->showNotification(I)V
    invoke-static {v2, v1}, Lcom/samsung/groupcast/service/GroupPlayService;->access$100(Lcom/samsung/groupcast/service/GroupPlayService;I)V

    goto :goto_0

    .line 46
    nop

    :pswitch_data_0
    .packed-switch 0xb
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method
