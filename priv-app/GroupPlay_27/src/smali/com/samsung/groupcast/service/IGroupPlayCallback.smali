.class public interface abstract Lcom/samsung/groupcast/service/IGroupPlayCallback;
.super Ljava/lang/Object;
.source "IGroupPlayCallback.java"

# interfaces
.implements Landroid/os/IInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/groupcast/service/IGroupPlayCallback$Stub;
    }
.end annotation


# virtual methods
.method public abstract onMessageEvent(ILcom/samsung/groupcast/service/GroupPlayPacket;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract onStatusEvent(ILjava/lang/String;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method
