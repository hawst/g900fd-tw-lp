.class public Lcom/samsung/groupcast/service/GroupPlayService;
.super Landroid/app/Service;
.source "GroupPlayService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/groupcast/service/GroupPlayService$3;,
        Lcom/samsung/groupcast/service/GroupPlayService$GroupPlayBinder;
    }
.end annotation


# static fields
.field public static final EXTRA_STARTED_BY_EXTERNAL:Ljava/lang/String; = "EXTRA_STARTED_BY_EXTERNAL"

.field public static final EXTRA_STARTED_BY_NOTIFICATION:Ljava/lang/String; = "EXTRA_STARTED_BY_NOTIFICATION"

.field private static sConnection:Landroid/content/ServiceConnection;

.field private static sIsBound:Z

.field private static sServiceIntent:Landroid/content/Intent;


# instance fields
.field private final NOTIFICATION:I

.field private TAG:Ljava/lang/String;

.field private final mBinder:Landroid/os/IBinder;

.field private mTypeOfExternalApp:I

.field private mWifiApReceiver:Landroid/content/BroadcastReceiver;

.field private mWifiReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 335
    sput-object v1, Lcom/samsung/groupcast/service/GroupPlayService;->sServiceIntent:Landroid/content/Intent;

    .line 336
    const/4 v0, 0x0

    sput-boolean v0, Lcom/samsung/groupcast/service/GroupPlayService;->sIsBound:Z

    .line 337
    sput-object v1, Lcom/samsung/groupcast/service/GroupPlayService;->sConnection:Landroid/content/ServiceConnection;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 30
    const-string v0, "GroupPlayService"

    iput-object v0, p0, Lcom/samsung/groupcast/service/GroupPlayService;->TAG:Ljava/lang/String;

    .line 33
    const/16 v0, 0x7a69

    iput v0, p0, Lcom/samsung/groupcast/service/GroupPlayService;->NOTIFICATION:I

    .line 34
    new-instance v0, Lcom/samsung/groupcast/service/GroupPlayService$GroupPlayBinder;

    invoke-direct {v0, p0}, Lcom/samsung/groupcast/service/GroupPlayService$GroupPlayBinder;-><init>(Lcom/samsung/groupcast/service/GroupPlayService;)V

    iput-object v0, p0, Lcom/samsung/groupcast/service/GroupPlayService;->mBinder:Landroid/os/IBinder;

    .line 35
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/groupcast/service/GroupPlayService;->mTypeOfExternalApp:I

    .line 38
    new-instance v0, Lcom/samsung/groupcast/service/GroupPlayService$1;

    invoke-direct {v0, p0}, Lcom/samsung/groupcast/service/GroupPlayService$1;-><init>(Lcom/samsung/groupcast/service/GroupPlayService;)V

    iput-object v0, p0, Lcom/samsung/groupcast/service/GroupPlayService;->mWifiApReceiver:Landroid/content/BroadcastReceiver;

    .line 83
    new-instance v0, Lcom/samsung/groupcast/service/GroupPlayService$2;

    invoke-direct {v0, p0}, Lcom/samsung/groupcast/service/GroupPlayService$2;-><init>(Lcom/samsung/groupcast/service/GroupPlayService;)V

    iput-object v0, p0, Lcom/samsung/groupcast/service/GroupPlayService;->mWifiReceiver:Landroid/content/BroadcastReceiver;

    .line 213
    return-void
.end method

.method public static StartNBindGroupPlayService(ILandroid/content/ServiceConnection;)V
    .locals 4
    .param p0, "typeFromExternalApp"    # I
    .param p1, "connection"    # Landroid/content/ServiceConnection;

    .prologue
    .line 341
    const-string v0, "---"

    invoke-static {v0}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 342
    new-instance v0, Landroid/content/Intent;

    invoke-static {}, Lcom/samsung/groupcast/application/App;->getInstance()Lcom/samsung/groupcast/application/App;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/groupcast/application/App;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/samsung/groupcast/service/GroupPlayService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    sput-object v0, Lcom/samsung/groupcast/service/GroupPlayService;->sServiceIntent:Landroid/content/Intent;

    .line 345
    sget-object v0, Lcom/samsung/groupcast/service/GroupPlayService;->sServiceIntent:Landroid/content/Intent;

    const-string v1, "EXTRA_STARTED_BY_EXTERNAL"

    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 346
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "startService: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/samsung/groupcast/application/App;->getInstance()Lcom/samsung/groupcast/application/App;

    move-result-object v1

    sget-object v2, Lcom/samsung/groupcast/service/GroupPlayService;->sServiceIntent:Landroid/content/Intent;

    invoke-virtual {v1, v2}, Lcom/samsung/groupcast/application/App;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 347
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "bindService: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/samsung/groupcast/application/App;->getInstance()Lcom/samsung/groupcast/application/App;

    move-result-object v1

    sget-object v2, Lcom/samsung/groupcast/service/GroupPlayService;->sServiceIntent:Landroid/content/Intent;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p1, v3}, Lcom/samsung/groupcast/application/App;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 348
    sput-object p1, Lcom/samsung/groupcast/service/GroupPlayService;->sConnection:Landroid/content/ServiceConnection;

    .line 349
    const/4 v0, 0x1

    sput-boolean v0, Lcom/samsung/groupcast/service/GroupPlayService;->sIsBound:Z

    .line 350
    return-void
.end method

.method public static StopNUnbindGroupPlayService(Landroid/content/ServiceConnection;)V
    .locals 5
    .param p0, "connection"    # Landroid/content/ServiceConnection;

    .prologue
    const/4 v4, 0x0

    .line 354
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "stopsvc:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/samsung/groupcast/service/GroupPlayService;->sServiceIntent:Landroid/content/Intent;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 355
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "stopsvc:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/samsung/groupcast/service/GroupPlayService;->sConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 356
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "stopsvc:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-boolean v3, Lcom/samsung/groupcast/service/GroupPlayService;->sIsBound:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 357
    sget-boolean v2, Lcom/samsung/groupcast/service/GroupPlayService;->sIsBound:Z

    if-eqz v2, :cond_2

    .line 358
    if-nez p0, :cond_0

    .line 359
    sget-object p0, Lcom/samsung/groupcast/service/GroupPlayService;->sConnection:Landroid/content/ServiceConnection;

    .line 362
    :cond_0
    if-eqz p0, :cond_1

    .line 364
    :try_start_0
    invoke-static {}, Lcom/samsung/groupcast/application/App;->getInstance()Lcom/samsung/groupcast/application/App;

    move-result-object v2

    invoke-virtual {v2, p0}, Lcom/samsung/groupcast/application/App;->unbindService(Landroid/content/ServiceConnection;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 369
    :cond_1
    :goto_0
    const/4 v2, 0x0

    sput-boolean v2, Lcom/samsung/groupcast/service/GroupPlayService;->sIsBound:Z

    .line 370
    sput-object v4, Lcom/samsung/groupcast/service/GroupPlayService;->sConnection:Landroid/content/ServiceConnection;

    .line 373
    :cond_2
    sget-object v2, Lcom/samsung/groupcast/service/GroupPlayService;->sServiceIntent:Landroid/content/Intent;

    if-eqz v2, :cond_3

    .line 374
    invoke-static {}, Lcom/samsung/groupcast/application/App;->getInstance()Lcom/samsung/groupcast/application/App;

    move-result-object v2

    sget-object v3, Lcom/samsung/groupcast/service/GroupPlayService;->sServiceIntent:Landroid/content/Intent;

    invoke-virtual {v2, v3}, Lcom/samsung/groupcast/application/App;->stopService(Landroid/content/Intent;)Z

    move-result v1

    .line 375
    .local v1, "result":Z
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "stopsvc b:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 376
    sput-object v4, Lcom/samsung/groupcast/service/GroupPlayService;->sServiceIntent:Landroid/content/Intent;

    .line 381
    .end local v1    # "result":Z
    :cond_3
    return-void

    .line 365
    :catch_0
    move-exception v0

    .line 366
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/samsung/groupcast/service/GroupPlayService;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/groupcast/service/GroupPlayService;

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/samsung/groupcast/service/GroupPlayService;->callMainActivity()V

    return-void
.end method

.method static synthetic access$100(Lcom/samsung/groupcast/service/GroupPlayService;I)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/groupcast/service/GroupPlayService;
    .param p1, "x1"    # I

    .prologue
    .line 29
    invoke-direct {p0, p1}, Lcom/samsung/groupcast/service/GroupPlayService;->showNotification(I)V

    return-void
.end method

.method static synthetic access$200(Lcom/samsung/groupcast/service/GroupPlayService;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/service/GroupPlayService;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/samsung/groupcast/service/GroupPlayService;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method private callMainActivity()V
    .locals 2

    .prologue
    .line 201
    :try_start_0
    sget-boolean v1, Lcom/samsung/groupcast/service/GroupPlayService;->sIsBound:Z

    if-nez v1, :cond_0

    .line 202
    const-string v1, "sIsBound is false"

    invoke-static {v1}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 211
    :goto_0
    return-void

    .line 206
    :cond_0
    invoke-direct {p0}, Lcom/samsung/groupcast/service/GroupPlayService;->getCallMainActivityForClose()Landroid/app/PendingIntent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/PendingIntent;->send()V
    :try_end_0
    .catch Landroid/app/PendingIntent$CanceledException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 207
    :catch_0
    move-exception v0

    .line 209
    .local v0, "e":Landroid/app/PendingIntent$CanceledException;
    invoke-virtual {v0}, Landroid/app/PendingIntent$CanceledException;->printStackTrace()V

    goto :goto_0
.end method

.method private getCallMainActivityForClose()Landroid/app/PendingIntent;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 162
    const/4 v1, 0x0

    .line 163
    .local v1, "intent":Landroid/content/Intent;
    iget-object v2, p0, Lcom/samsung/groupcast/service/GroupPlayService;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mTypeOfExternalApp:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/samsung/groupcast/service/GroupPlayService;->mTypeOfExternalApp:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/groupcast/application/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 168
    new-instance v1, Landroid/content/Intent;

    .end local v1    # "intent":Landroid/content/Intent;
    const-class v2, Lcom/samsung/groupcast/application/main/MainActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 171
    .restart local v1    # "intent":Landroid/content/Intent;
    const-string v2, "android.intent.action.MAIN"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 172
    const-string v2, "android.intent.category.LAUNCHER"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 174
    const/high16 v2, 0x4000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 177
    invoke-static {p0, v5, v1, v5}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 178
    .local v0, "contentIntent":Landroid/app/PendingIntent;
    return-object v0
.end method

.method private getCallStartActivityForResume()Landroid/app/PendingIntent;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 183
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 184
    .local v1, "intent":Landroid/content/Intent;
    iget-object v2, p0, Lcom/samsung/groupcast/service/GroupPlayService;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mTypeOfExternalApp:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/samsung/groupcast/service/GroupPlayService;->mTypeOfExternalApp:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/groupcast/application/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 186
    const-string v2, "android.intent.action.MAIN"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 187
    const-string v2, "android.intent.category.LAUNCHER"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 188
    new-instance v2, Landroid/content/ComponentName;

    invoke-static {}, Lcom/samsung/groupcast/application/App;->getInstance()Lcom/samsung/groupcast/application/App;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/groupcast/application/App;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const-class v4, Lcom/samsung/groupcast/application/start/StartActivity;

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 192
    const/high16 v2, 0x10100000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 193
    const-string v2, "EXTRA_STARTED_BY_NOTIFICATION"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 195
    invoke-static {p0, v5, v1, v5}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 196
    .local v0, "contentIntent":Landroid/app/PendingIntent;
    return-object v0
.end method

.method private showJoinNotification()V
    .locals 7
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 317
    const-string v3, "---"

    invoke-static {v3}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 318
    const/4 v2, 0x0

    .line 320
    .local v2, "notification":Landroid/app/Notification;
    invoke-virtual {p0}, Lcom/samsung/groupcast/service/GroupPlayService;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f080001

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 321
    .local v1, "apTitle":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/groupcast/service/GroupPlayService;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f080006

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiUtils;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getWifiSSID()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 325
    .local v0, "apStringLine1":Ljava/lang/String;
    const-string v3, "GP_SVC"

    invoke-static {v3, v1}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 327
    new-instance v3, Landroid/app/Notification$BigTextStyle;

    new-instance v4, Landroid/app/Notification$Builder;

    invoke-direct {v4, p0}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v4, v1}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v4

    invoke-virtual {v4, v0}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v4

    const v5, 0x7f02004c

    invoke-virtual {v4, v5}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v4

    invoke-direct {p0}, Lcom/samsung/groupcast/service/GroupPlayService;->getCallStartActivityForResume()Landroid/app/PendingIntent;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/app/Notification$BigTextStyle;-><init>(Landroid/app/Notification$Builder;)V

    invoke-virtual {v3, v0}, Landroid/app/Notification$BigTextStyle;->bigText(Ljava/lang/CharSequence;)Landroid/app/Notification$BigTextStyle;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Notification$BigTextStyle;->build()Landroid/app/Notification;

    move-result-object v2

    .line 331
    if-eqz v2, :cond_0

    .line 332
    const/16 v3, 0x7a69

    invoke-virtual {p0, v3, v2}, Lcom/samsung/groupcast/service/GroupPlayService;->startForeground(ILandroid/app/Notification;)V

    .line 333
    :cond_0
    return-void
.end method

.method private showNotification(I)V
    .locals 7
    .param p1, "count"    # I
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 286
    const-string v3, "---"

    invoke-static {v3}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 287
    const/4 v2, 0x0

    .line 288
    .local v2, "notification":Landroid/app/Notification;
    const-string v1, ""

    .line 289
    .local v1, "apTitle":Ljava/lang/String;
    const-string v0, ""

    .line 291
    .local v0, "apStringLine1":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/groupcast/service/GroupPlayService;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f080001

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 292
    invoke-virtual {p0}, Lcom/samsung/groupcast/service/GroupPlayService;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f08007a

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiUtils;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getWifiSSID()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 296
    const-string v3, "GP_SVC"

    invoke-static {v3, v1}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 298
    new-instance v3, Landroid/app/Notification$BigTextStyle;

    new-instance v4, Landroid/app/Notification$Builder;

    invoke-direct {v4, p0}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v4, v1}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v4

    invoke-virtual {v4, v0}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v4

    const v5, 0x7f02004c

    invoke-virtual {v4, v5}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v4

    invoke-direct {p0}, Lcom/samsung/groupcast/service/GroupPlayService;->getCallStartActivityForResume()Landroid/app/PendingIntent;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/app/Notification$BigTextStyle;-><init>(Landroid/app/Notification$Builder;)V

    invoke-virtual {v3, v0}, Landroid/app/Notification$BigTextStyle;->bigText(Ljava/lang/CharSequence;)Landroid/app/Notification$BigTextStyle;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Notification$BigTextStyle;->build()Landroid/app/Notification;

    move-result-object v2

    .line 310
    if-eqz v2, :cond_0

    .line 311
    const/16 v3, 0x7a69

    invoke-virtual {p0, v3, v2}, Lcom/samsung/groupcast/service/GroupPlayService;->startForeground(ILandroid/app/Notification;)V

    .line 312
    :cond_0
    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 236
    const-string v0, "---"

    invoke-static {v0}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 237
    iget-object v0, p0, Lcom/samsung/groupcast/service/GroupPlayService;->mBinder:Landroid/os/IBinder;

    return-object v0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 221
    const-string v0, "---"

    invoke-static {v0}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 222
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 223
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/samsung/groupcast/service/GroupPlayService;->stopForeground(Z)V

    .line 225
    sget-object v0, Lcom/samsung/groupcast/core/messaging/MessagingClient;->DefaultInterface:Lcom/samsung/groupcast/core/messaging/MessagingClient$MAGNET_CONNECTION_TYPE;

    invoke-virtual {v0}, Lcom/samsung/groupcast/core/messaging/MessagingClient$MAGNET_CONNECTION_TYPE;->value()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 232
    :goto_0
    return-void

    .line 227
    :pswitch_0
    invoke-virtual {p0}, Lcom/samsung/groupcast/service/GroupPlayService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/groupcast/service/GroupPlayService;->mWifiApReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 228
    invoke-virtual {p0}, Lcom/samsung/groupcast/service/GroupPlayService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/groupcast/service/GroupPlayService;->mWifiReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    goto :goto_0

    .line 225
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 5
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    const/4 v4, 0x0

    .line 242
    const-string v2, "---"

    invoke-static {v2}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 243
    invoke-super {p0, p1, p2, p3}, Landroid/app/Service;->onStartCommand(Landroid/content/Intent;II)I

    .line 245
    if-eqz p1, :cond_0

    .line 246
    const-string v2, "EXTRA_STARTED_BY_EXTERNAL"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 247
    const-string v2, "EXTRA_STARTED_BY_EXTERNAL"

    invoke-virtual {p1, v2, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/samsung/groupcast/service/GroupPlayService;->mTypeOfExternalApp:I

    .line 251
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isExtra:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/samsung/groupcast/service/GroupPlayService;->mTypeOfExternalApp:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 255
    sget-object v2, Lcom/samsung/groupcast/core/messaging/MessagingClient;->DefaultInterface:Lcom/samsung/groupcast/core/messaging/MessagingClient$MAGNET_CONNECTION_TYPE;

    invoke-virtual {v2}, Lcom/samsung/groupcast/core/messaging/MessagingClient$MAGNET_CONNECTION_TYPE;->value()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 281
    :goto_0
    const/4 v2, 0x2

    return v2

    .line 257
    :pswitch_0
    new-instance v0, Landroid/content/IntentFilter;

    const-string v2, "android.net.wifi.WIFI_AP_STATE_CHANGED"

    invoke-direct {v0, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 258
    .local v0, "filter4AP":Landroid/content/IntentFilter;
    const-string v2, "android.net.wifi.WIFI_AP_STA_STATUS_CHANGED"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 259
    invoke-virtual {p0}, Lcom/samsung/groupcast/service/GroupPlayService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/groupcast/service/GroupPlayService;->mWifiApReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v2, v3, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 262
    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "android.net.wifi.WIFI_STATE_CHANGED"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 263
    .local v1, "filter4Wifi":Landroid/content/IntentFilter;
    const-string v2, "android.net.wifi.STATE_CHANGE"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 264
    const-string v2, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 265
    const-string v2, "android.net.wifi.supplicant.CONNECTION_CHANGE"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 266
    const-string v2, "android.net.wifi.supplicant.STATE_CHANGE"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 267
    const-string v2, "android.net.wifi.CONFIGURED_NETWORKS_CHANGE"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 268
    invoke-virtual {p0}, Lcom/samsung/groupcast/service/GroupPlayService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/groupcast/service/GroupPlayService;->mWifiReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v2, v3, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 271
    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getWifiManager()Landroid/net/wifi/WifiManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/wifi/WifiManager;->isWifiApEnabled()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 272
    invoke-direct {p0, v4}, Lcom/samsung/groupcast/service/GroupPlayService;->showNotification(I)V

    goto :goto_0

    .line 274
    :cond_1
    invoke-direct {p0}, Lcom/samsung/groupcast/service/GroupPlayService;->showJoinNotification()V

    goto :goto_0

    .line 255
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method
