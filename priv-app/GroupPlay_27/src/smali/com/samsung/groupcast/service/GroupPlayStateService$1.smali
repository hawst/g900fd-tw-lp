.class Lcom/samsung/groupcast/service/GroupPlayStateService$1;
.super Landroid/os/Handler;
.source "GroupPlayStateService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/groupcast/service/GroupPlayStateService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/groupcast/service/GroupPlayStateService;


# direct methods
.method constructor <init>(Lcom/samsung/groupcast/service/GroupPlayStateService;)V
    .locals 0

    .prologue
    .line 60
    iput-object p1, p0, Lcom/samsung/groupcast/service/GroupPlayStateService$1;->this$0:Lcom/samsung/groupcast/service/GroupPlayStateService;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 10
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 63
    monitor-enter p0

    .line 64
    const/4 v1, 0x0

    .line 66
    .local v1, "N":I
    :try_start_0
    iget v6, p1, Landroid/os/Message;->what:I

    packed-switch v6, :pswitch_data_0

    .line 142
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 145
    :goto_0
    monitor-exit p0

    .line 147
    return-void

    .line 68
    :pswitch_0
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "incomingMsg:MSG_EVENT_START cnt:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/samsung/groupcast/service/GroupPlayStateService$1;->this$0:Lcom/samsung/groupcast/service/GroupPlayStateService;

    # getter for: Lcom/samsung/groupcast/service/GroupPlayStateService;->mGPCallbacks:Landroid/os/RemoteCallbackList;
    invoke-static {v7}, Lcom/samsung/groupcast/service/GroupPlayStateService;->access$000(Lcom/samsung/groupcast/service/GroupPlayStateService;)Landroid/os/RemoteCallbackList;

    move-result-object v7

    invoke-virtual {v7}, Landroid/os/RemoteCallbackList;->getRegisteredCallbackCount()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    # invokes: Lcom/samsung/groupcast/service/GroupPlayStateService;->debugLog(Ljava/lang/String;)V
    invoke-static {v6}, Lcom/samsung/groupcast/service/GroupPlayStateService;->access$100(Ljava/lang/String;)V

    .line 72
    iget-object v6, p0, Lcom/samsung/groupcast/service/GroupPlayStateService$1;->this$0:Lcom/samsung/groupcast/service/GroupPlayStateService;

    # getter for: Lcom/samsung/groupcast/service/GroupPlayStateService;->mGPCallbacks:Landroid/os/RemoteCallbackList;
    invoke-static {v6}, Lcom/samsung/groupcast/service/GroupPlayStateService;->access$000(Lcom/samsung/groupcast/service/GroupPlayStateService;)Landroid/os/RemoteCallbackList;

    move-result-object v6

    invoke-virtual {v6}, Landroid/os/RemoteCallbackList;->beginBroadcast()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    .line 73
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    if-ge v3, v1, :cond_1

    .line 75
    :try_start_1
    const-string v4, ""

    .line 77
    .local v4, "msgString":Ljava/lang/String;
    iget-object v6, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    instance-of v6, v6, Ljava/lang/String;

    if-eqz v6, :cond_0

    .line 78
    iget-object v4, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .end local v4    # "msgString":Ljava/lang/String;
    check-cast v4, Ljava/lang/String;

    .line 81
    .restart local v4    # "msgString":Ljava/lang/String;
    :cond_0
    iget-object v6, p0, Lcom/samsung/groupcast/service/GroupPlayStateService$1;->this$0:Lcom/samsung/groupcast/service/GroupPlayStateService;

    # getter for: Lcom/samsung/groupcast/service/GroupPlayStateService;->mGPCallbacks:Landroid/os/RemoteCallbackList;
    invoke-static {v6}, Lcom/samsung/groupcast/service/GroupPlayStateService;->access$000(Lcom/samsung/groupcast/service/GroupPlayStateService;)Landroid/os/RemoteCallbackList;

    move-result-object v6

    invoke-virtual {v6, v3}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v6

    check-cast v6, Lcom/samsung/groupcast/service/IGroupPlayCallback;

    iget v7, p1, Landroid/os/Message;->what:I

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "test:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/samsung/groupcast/service/GroupPlayStateService$1;->this$0:Lcom/samsung/groupcast/service/GroupPlayStateService;

    # getter for: Lcom/samsung/groupcast/service/GroupPlayStateService;->mGPCallbacks:Landroid/os/RemoteCallbackList;
    invoke-static {v9}, Lcom/samsung/groupcast/service/GroupPlayStateService;->access$000(Lcom/samsung/groupcast/service/GroupPlayStateService;)Landroid/os/RemoteCallbackList;

    move-result-object v9

    invoke-virtual {v9, v3}, Landroid/os/RemoteCallbackList;->getBroadcastCookie(I)Ljava/lang/Object;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ":"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v6, v7, v8}, Lcom/samsung/groupcast/service/IGroupPlayCallback;->onStatusEvent(ILjava/lang/String;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 73
    .end local v4    # "msgString":Ljava/lang/String;
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 90
    :cond_1
    :try_start_2
    iget-object v6, p0, Lcom/samsung/groupcast/service/GroupPlayStateService$1;->this$0:Lcom/samsung/groupcast/service/GroupPlayStateService;

    # getter for: Lcom/samsung/groupcast/service/GroupPlayStateService;->mGPCallbacks:Landroid/os/RemoteCallbackList;
    invoke-static {v6}, Lcom/samsung/groupcast/service/GroupPlayStateService;->access$000(Lcom/samsung/groupcast/service/GroupPlayStateService;)Landroid/os/RemoteCallbackList;

    move-result-object v6

    invoke-virtual {v6}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    goto :goto_0

    .line 145
    .end local v3    # "i":I
    :catchall_0
    move-exception v6

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v6

    .line 94
    :pswitch_1
    :try_start_3
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "incomingMsg:MSG_EVENT_RECV cnt:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/samsung/groupcast/service/GroupPlayStateService$1;->this$0:Lcom/samsung/groupcast/service/GroupPlayStateService;

    # getter for: Lcom/samsung/groupcast/service/GroupPlayStateService;->mGPCallbacks:Landroid/os/RemoteCallbackList;
    invoke-static {v7}, Lcom/samsung/groupcast/service/GroupPlayStateService;->access$000(Lcom/samsung/groupcast/service/GroupPlayStateService;)Landroid/os/RemoteCallbackList;

    move-result-object v7

    invoke-virtual {v7}, Landroid/os/RemoteCallbackList;->getRegisteredCallbackCount()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    # invokes: Lcom/samsung/groupcast/service/GroupPlayStateService;->debugLog(Ljava/lang/String;)V
    invoke-static {v6}, Lcom/samsung/groupcast/service/GroupPlayStateService;->access$100(Ljava/lang/String;)V

    .line 98
    iget-object v6, p0, Lcom/samsung/groupcast/service/GroupPlayStateService$1;->this$0:Lcom/samsung/groupcast/service/GroupPlayStateService;

    # getter for: Lcom/samsung/groupcast/service/GroupPlayStateService;->mGPCallbacks:Landroid/os/RemoteCallbackList;
    invoke-static {v6}, Lcom/samsung/groupcast/service/GroupPlayStateService;->access$000(Lcom/samsung/groupcast/service/GroupPlayStateService;)Landroid/os/RemoteCallbackList;

    move-result-object v6

    invoke-virtual {v6}, Landroid/os/RemoteCallbackList;->beginBroadcast()I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result v1

    .line 99
    const/4 v3, 0x0

    .restart local v3    # "i":I
    :goto_3
    if-ge v3, v1, :cond_3

    .line 100
    const/4 v2, 0x0

    .line 102
    .local v2, "data":Lcom/samsung/groupcast/service/GroupPlayPacket;
    :try_start_4
    iget-object v6, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    instance-of v6, v6, Lcom/samsung/groupcast/service/GroupPlayPacket;

    if-eqz v6, :cond_2

    .line 103
    iget-object v6, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object v0, v6

    check-cast v0, Lcom/samsung/groupcast/service/GroupPlayPacket;

    move-object v2, v0

    .line 104
    iget-object v6, p0, Lcom/samsung/groupcast/service/GroupPlayStateService$1;->this$0:Lcom/samsung/groupcast/service/GroupPlayStateService;

    # getter for: Lcom/samsung/groupcast/service/GroupPlayStateService;->mGPCallbacks:Landroid/os/RemoteCallbackList;
    invoke-static {v6}, Lcom/samsung/groupcast/service/GroupPlayStateService;->access$000(Lcom/samsung/groupcast/service/GroupPlayStateService;)Landroid/os/RemoteCallbackList;

    move-result-object v6

    invoke-virtual {v6, v3}, Landroid/os/RemoteCallbackList;->getBroadcastCookie(I)Ljava/lang/Object;

    move-result-object v5

    .line 105
    .local v5, "obj":Ljava/lang/Object;
    if-eqz v5, :cond_2

    instance-of v6, v5, Ljava/lang/String;

    if-eqz v6, :cond_2

    iget-object v6, v2, Lcom/samsung/groupcast/service/GroupPlayPacket;->pkgName:Ljava/lang/String;

    check-cast v5, Ljava/lang/String;

    .end local v5    # "obj":Ljava/lang/Object;
    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 107
    iget-object v6, p0, Lcom/samsung/groupcast/service/GroupPlayStateService$1;->this$0:Lcom/samsung/groupcast/service/GroupPlayStateService;

    # getter for: Lcom/samsung/groupcast/service/GroupPlayStateService;->mGPCallbacks:Landroid/os/RemoteCallbackList;
    invoke-static {v6}, Lcom/samsung/groupcast/service/GroupPlayStateService;->access$000(Lcom/samsung/groupcast/service/GroupPlayStateService;)Landroid/os/RemoteCallbackList;

    move-result-object v6

    invoke-virtual {v6, v3}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v6

    check-cast v6, Lcom/samsung/groupcast/service/IGroupPlayCallback;

    iget v7, p1, Landroid/os/Message;->what:I

    invoke-interface {v6, v7, v2}, Lcom/samsung/groupcast/service/IGroupPlayCallback;->onMessageEvent(ILcom/samsung/groupcast/service/GroupPlayPacket;)V
    :try_end_4
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 99
    :cond_2
    :goto_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 116
    .end local v2    # "data":Lcom/samsung/groupcast/service/GroupPlayPacket;
    :cond_3
    :try_start_5
    iget-object v6, p0, Lcom/samsung/groupcast/service/GroupPlayStateService$1;->this$0:Lcom/samsung/groupcast/service/GroupPlayStateService;

    # getter for: Lcom/samsung/groupcast/service/GroupPlayStateService;->mGPCallbacks:Landroid/os/RemoteCallbackList;
    invoke-static {v6}, Lcom/samsung/groupcast/service/GroupPlayStateService;->access$000(Lcom/samsung/groupcast/service/GroupPlayStateService;)Landroid/os/RemoteCallbackList;

    move-result-object v6

    invoke-virtual {v6}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    goto/16 :goto_0

    .line 121
    .end local v3    # "i":I
    :pswitch_2
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "incomingMsg:MSG_EVENT_NODE_CHANGED "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p1, Landroid/os/Message;->what:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "cnt:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/samsung/groupcast/service/GroupPlayStateService$1;->this$0:Lcom/samsung/groupcast/service/GroupPlayStateService;

    # getter for: Lcom/samsung/groupcast/service/GroupPlayStateService;->mGPCallbacks:Landroid/os/RemoteCallbackList;
    invoke-static {v7}, Lcom/samsung/groupcast/service/GroupPlayStateService;->access$000(Lcom/samsung/groupcast/service/GroupPlayStateService;)Landroid/os/RemoteCallbackList;

    move-result-object v7

    invoke-virtual {v7}, Landroid/os/RemoteCallbackList;->getRegisteredCallbackCount()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    # invokes: Lcom/samsung/groupcast/service/GroupPlayStateService;->debugLog(Ljava/lang/String;)V
    invoke-static {v6}, Lcom/samsung/groupcast/service/GroupPlayStateService;->access$100(Ljava/lang/String;)V

    .line 125
    iget-object v6, p0, Lcom/samsung/groupcast/service/GroupPlayStateService$1;->this$0:Lcom/samsung/groupcast/service/GroupPlayStateService;

    # getter for: Lcom/samsung/groupcast/service/GroupPlayStateService;->mGPCallbacks:Landroid/os/RemoteCallbackList;
    invoke-static {v6}, Lcom/samsung/groupcast/service/GroupPlayStateService;->access$000(Lcom/samsung/groupcast/service/GroupPlayStateService;)Landroid/os/RemoteCallbackList;

    move-result-object v6

    invoke-virtual {v6}, Landroid/os/RemoteCallbackList;->beginBroadcast()I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-result v1

    .line 126
    const/4 v3, 0x0

    .restart local v3    # "i":I
    :goto_5
    if-ge v3, v1, :cond_5

    .line 128
    :try_start_6
    iget-object v6, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    instance-of v6, v6, Ljava/lang/String;

    if-eqz v6, :cond_4

    .line 129
    iget-object v6, p0, Lcom/samsung/groupcast/service/GroupPlayStateService$1;->this$0:Lcom/samsung/groupcast/service/GroupPlayStateService;

    # getter for: Lcom/samsung/groupcast/service/GroupPlayStateService;->mGPCallbacks:Landroid/os/RemoteCallbackList;
    invoke-static {v6}, Lcom/samsung/groupcast/service/GroupPlayStateService;->access$000(Lcom/samsung/groupcast/service/GroupPlayStateService;)Landroid/os/RemoteCallbackList;

    move-result-object v6

    invoke-virtual {v6, v3}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v6

    check-cast v6, Lcom/samsung/groupcast/service/IGroupPlayCallback;

    iget v8, p1, Landroid/os/Message;->what:I

    iget-object v7, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v7, Ljava/lang/String;

    invoke-interface {v6, v8, v7}, Lcom/samsung/groupcast/service/IGroupPlayCallback;->onStatusEvent(ILjava/lang/String;)V
    :try_end_6
    .catch Landroid/os/RemoteException; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 126
    :cond_4
    :goto_6
    add-int/lit8 v3, v3, 0x1

    goto :goto_5

    .line 137
    :cond_5
    :try_start_7
    iget-object v6, p0, Lcom/samsung/groupcast/service/GroupPlayStateService$1;->this$0:Lcom/samsung/groupcast/service/GroupPlayStateService;

    # getter for: Lcom/samsung/groupcast/service/GroupPlayStateService;->mGPCallbacks:Landroid/os/RemoteCallbackList;
    invoke-static {v6}, Lcom/samsung/groupcast/service/GroupPlayStateService;->access$000(Lcom/samsung/groupcast/service/GroupPlayStateService;)Landroid/os/RemoteCallbackList;

    move-result-object v6

    invoke-virtual {v6}, Landroid/os/RemoteCallbackList;->finishBroadcast()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto/16 :goto_0

    .line 132
    :catch_0
    move-exception v6

    goto :goto_6

    .line 111
    .restart local v2    # "data":Lcom/samsung/groupcast/service/GroupPlayPacket;
    :catch_1
    move-exception v6

    goto :goto_4

    .line 85
    .end local v2    # "data":Lcom/samsung/groupcast/service/GroupPlayPacket;
    :catch_2
    move-exception v6

    goto/16 :goto_2

    .line 66
    nop

    :pswitch_data_0
    .packed-switch 0x10001
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method
