.class public Lcom/samsung/groupcast/service/GroupPlayStateService;
.super Landroid/app/Service;
.source "GroupPlayStateService.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "GPSDKv2_SVC"

.field private static sLastestInstance:Lcom/samsung/groupcast/service/GroupPlayStateService;


# instance fields
.field mBinder:Lcom/samsung/groupcast/service/IGroupPlayState$Stub;

.field private final mGPCallbacks:Landroid/os/RemoteCallbackList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/RemoteCallbackList",
            "<",
            "Lcom/samsung/groupcast/service/IGroupPlayCallback;",
            ">;"
        }
    .end annotation
.end field

.field private mIncomingMsgHandler:Lcom/samsung/groupcast/service/IncomingMsgHandler;

.field private final mOutgoingMsgHandler:Landroid/os/Handler;

.field private mServiceLooper:Landroid/os/Looper;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 46
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/groupcast/service/GroupPlayStateService;->sLastestInstance:Lcom/samsung/groupcast/service/GroupPlayStateService;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 43
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 55
    new-instance v0, Landroid/os/RemoteCallbackList;

    invoke-direct {v0}, Landroid/os/RemoteCallbackList;-><init>()V

    iput-object v0, p0, Lcom/samsung/groupcast/service/GroupPlayStateService;->mGPCallbacks:Landroid/os/RemoteCallbackList;

    .line 60
    new-instance v0, Lcom/samsung/groupcast/service/GroupPlayStateService$1;

    invoke-direct {v0, p0}, Lcom/samsung/groupcast/service/GroupPlayStateService$1;-><init>(Lcom/samsung/groupcast/service/GroupPlayStateService;)V

    iput-object v0, p0, Lcom/samsung/groupcast/service/GroupPlayStateService;->mOutgoingMsgHandler:Landroid/os/Handler;

    .line 229
    new-instance v0, Lcom/samsung/groupcast/service/GroupPlayStateService$2;

    invoke-direct {v0, p0}, Lcom/samsung/groupcast/service/GroupPlayStateService$2;-><init>(Lcom/samsung/groupcast/service/GroupPlayStateService;)V

    iput-object v0, p0, Lcom/samsung/groupcast/service/GroupPlayStateService;->mBinder:Lcom/samsung/groupcast/service/IGroupPlayState$Stub;

    return-void
.end method

.method static synthetic access$000(Lcom/samsung/groupcast/service/GroupPlayStateService;)Landroid/os/RemoteCallbackList;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/service/GroupPlayStateService;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/samsung/groupcast/service/GroupPlayStateService;->mGPCallbacks:Landroid/os/RemoteCallbackList;

    return-object v0
.end method

.method static synthetic access$100(Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Ljava/lang/String;

    .prologue
    .line 43
    invoke-static {p0}, Lcom/samsung/groupcast/service/GroupPlayStateService;->debugLog(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$200(Lcom/samsung/groupcast/service/GroupPlayStateService;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/service/GroupPlayStateService;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/samsung/groupcast/service/GroupPlayStateService;->mOutgoingMsgHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private static debugLog(Ljava/lang/String;)V
    .locals 1
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    .line 49
    sget-boolean v0, Lcom/samsung/groupcast/debug/DebugFlags;->DEBUG_FLAG_BOOLEAN_STATESVC_LOG:Z

    if-eqz v0, :cond_0

    .line 50
    const-string v0, "GPSDKv2_SVC"

    invoke-static {v0, p0}, Lcom/samsung/groupcast/application/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 52
    :cond_0
    return-void
.end method

.method public static getLastestInstance()Lcom/samsung/groupcast/service/GroupPlayStateService;
    .locals 1

    .prologue
    .line 225
    sget-object v0, Lcom/samsung/groupcast/service/GroupPlayStateService;->sLastestInstance:Lcom/samsung/groupcast/service/GroupPlayStateService;

    return-object v0
.end method


# virtual methods
.method public getIncomingMsgHandler()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lcom/samsung/groupcast/service/GroupPlayStateService;->mIncomingMsgHandler:Lcom/samsung/groupcast/service/IncomingMsgHandler;

    return-object v0
.end method

.method public getOutgoingMsgHandler()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 156
    iget-object v0, p0, Lcom/samsung/groupcast/service/GroupPlayStateService;->mOutgoingMsgHandler:Landroid/os/Handler;

    return-object v0
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 219
    const-string v0, "GPSDKv2_SVC"

    const-string v1, "[onBind]"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 220
    iget-object v0, p0, Lcom/samsung/groupcast/service/GroupPlayStateService;->mBinder:Lcom/samsung/groupcast/service/IGroupPlayState$Stub;

    return-object v0
.end method

.method public onCreate()V
    .locals 3

    .prologue
    .line 193
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "ServiceStartArguments"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    .line 195
    .local v0, "thread":Landroid/os/HandlerThread;
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 199
    const-string v1, "onCreate.."

    invoke-static {v1}, Lcom/samsung/groupcast/service/GroupPlayStateService;->debugLog(Ljava/lang/String;)V

    .line 203
    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/groupcast/service/GroupPlayStateService;->mServiceLooper:Landroid/os/Looper;

    .line 204
    new-instance v1, Lcom/samsung/groupcast/service/IncomingMsgHandler;

    iget-object v2, p0, Lcom/samsung/groupcast/service/GroupPlayStateService;->mServiceLooper:Landroid/os/Looper;

    invoke-direct {v1, v2, p0}, Lcom/samsung/groupcast/service/IncomingMsgHandler;-><init>(Landroid/os/Looper;Lcom/samsung/groupcast/service/GroupPlayStateService;)V

    iput-object v1, p0, Lcom/samsung/groupcast/service/GroupPlayStateService;->mIncomingMsgHandler:Lcom/samsung/groupcast/service/IncomingMsgHandler;

    .line 206
    sput-object p0, Lcom/samsung/groupcast/service/GroupPlayStateService;->sLastestInstance:Lcom/samsung/groupcast/service/GroupPlayStateService;

    .line 207
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 212
    const-string v0, "GPSDKv2_SVC"

    const-string v1, "[onDestroy]"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 213
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/groupcast/service/GroupPlayStateService;->sLastestInstance:Lcom/samsung/groupcast/service/GroupPlayStateService;

    .line 214
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 215
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    .line 169
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onStartCommand:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/groupcast/service/GroupPlayStateService;->debugLog(Ljava/lang/String;)V

    .line 171
    iget-object v1, p0, Lcom/samsung/groupcast/service/GroupPlayStateService;->mIncomingMsgHandler:Lcom/samsung/groupcast/service/IncomingMsgHandler;

    if-eqz v1, :cond_0

    .line 172
    iget-object v1, p0, Lcom/samsung/groupcast/service/GroupPlayStateService;->mIncomingMsgHandler:Lcom/samsung/groupcast/service/IncomingMsgHandler;

    invoke-virtual {v1}, Lcom/samsung/groupcast/service/IncomingMsgHandler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 173
    .local v0, "msg":Landroid/os/Message;
    iput p3, v0, Landroid/os/Message;->arg1:I

    .line 174
    iget-object v1, p0, Lcom/samsung/groupcast/service/GroupPlayStateService;->mIncomingMsgHandler:Lcom/samsung/groupcast/service/IncomingMsgHandler;

    invoke-virtual {v1, v0}, Lcom/samsung/groupcast/service/IncomingMsgHandler;->sendMessage(Landroid/os/Message;)Z

    .line 182
    .end local v0    # "msg":Landroid/os/Message;
    :goto_0
    const/4 v1, 0x1

    return v1

    .line 178
    :cond_0
    const-string v1, "GPSDKv2_SVC"

    const-string v2, "NoSvcHandler!!!"

    invoke-static {v1, v2}, Lcom/samsung/groupcast/application/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public stopMe(I)V
    .locals 0
    .param p1, "arg"    # I

    .prologue
    .line 161
    invoke-virtual {p0, p1}, Lcom/samsung/groupcast/service/GroupPlayStateService;->stopSelf(I)V

    .line 162
    return-void
.end method
