.class public Lcom/samsung/groupcast/service/GroupPlayV2Message;
.super Ljava/lang/Object;
.source "GroupPlayV2Message.java"


# static fields
.field public static final INTENT_GROUPPLAY_WIFI_CONNECTION_LOST:Ljava/lang/String; = "com.samsung.groupplay.intent.action.connection.wifi.lost"

.field public static final KEY_JSON_NODE_CHANNEL:Ljava/lang/String; = "CH"

.field public static final KEY_JSON_NODE_NODE:Ljava/lang/String; = "NODE"

.field public static final MSG_EVENT_NODE_JOINED:I = 0x10003

.field public static final MSG_EVENT_NODE_LEFT:I = 0x10004

.field public static final MSG_EVENT_RECV:I = 0x10002

.field public static final MSG_EVENT_START:I = 0x10001

.field public static final MSG_EVENT_START_ERR:I = -0xfff0000

.field public static final MSG_FILE_START:I = 0x20001

.field public static final MSG_FILE_START_ERR:I = -0xffdffff

.field public static final MSG_MASK_CLASS_EVENT:I = 0x10000

.field public static final MSG_MASK_CLASS_FILE:I = 0x20000

.field public static final MSG_MASK_ERR:I = -0x10000000

.field public static final MSG_MASK_STATUS_MSG:I = 0x0

.field public static final MSG_STATUS_ERR:I = -0xfffffff

.field public static final MSG_STATUS_OK:I = 0x1


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 4
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
