.class Lcom/samsung/groupcast/service/GroupPlayStateService$2;
.super Lcom/samsung/groupcast/service/IGroupPlayState$Stub;
.source "GroupPlayStateService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/groupcast/service/GroupPlayStateService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/groupcast/service/GroupPlayStateService;


# direct methods
.method constructor <init>(Lcom/samsung/groupcast/service/GroupPlayStateService;)V
    .locals 0

    .prologue
    .line 229
    iput-object p1, p0, Lcom/samsung/groupcast/service/GroupPlayStateService$2;->this$0:Lcom/samsung/groupcast/service/GroupPlayStateService;

    invoke-direct {p0}, Lcom/samsung/groupcast/service/IGroupPlayState$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public getChordManagerName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 304
    invoke-static {}, Lcom/samsung/groupcast/core/messaging/MessagingClient;->getChordMgr()Lcom/samsung/android/sdk/chord/SchordManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/chord/SchordManager;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getJoinedChannelList(Ljava/lang/String;)Ljava/util/List;
    .locals 7
    .param p1, "pkg"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 266
    const-string v4, "GPSDKv2_SVC"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "getJoinedChannelList pkg:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 267
    invoke-static {}, Lcom/samsung/groupcast/core/session/PackageSessionManager;->getInstance()Lcom/samsung/groupcast/core/session/PackageSessionManager;

    move-result-object v4

    invoke-virtual {v4, p1}, Lcom/samsung/groupcast/core/session/PackageSessionManager;->getPackageSession(Ljava/lang/String;)Lcom/samsung/groupcast/core/session/PackageSession;

    move-result-object v2

    .line 268
    .local v2, "ps":Lcom/samsung/groupcast/core/session/PackageSession;
    if-eqz v2, :cond_0

    .line 269
    invoke-virtual {v2}, Lcom/samsung/groupcast/core/session/PackageSession;->getChannelNameList()Ljava/util/List;

    move-result-object v1

    .line 270
    .local v1, "l":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    sget-boolean v4, Lcom/samsung/groupcast/debug/DebugFlags;->DEBUG_FLAG_BOOLEAN_STATESVC_LOG:Z

    if-eqz v4, :cond_1

    .line 271
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 272
    .local v3, "s":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "-"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    # invokes: Lcom/samsung/groupcast/service/GroupPlayStateService;->debugLog(Ljava/lang/String;)V
    invoke-static {v4}, Lcom/samsung/groupcast/service/GroupPlayStateService;->access$100(Ljava/lang/String;)V

    goto :goto_0

    .line 278
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "l":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v3    # "s":Ljava/lang/String;
    :cond_0
    const-string v4, "GPSDKv2_SVC"

    const-string v5, "getJoinedChannel returns null"

    invoke-static {v4, v5}, Lcom/samsung/groupcast/application/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 279
    const/4 v1, 0x0

    :cond_1
    return-object v1
.end method

.method public getJoinedNodeList(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;
    .locals 1
    .param p1, "pkg"    # Ljava/lang/String;
    .param p2, "channel"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 340
    const/4 v0, 0x0

    return-object v0
.end method

.method public getName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "pkg"    # Ljava/lang/String;
    .param p2, "channel"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 327
    const/4 v0, 0x0

    return-object v0
.end method

.method public getNodeIpAddress(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "pkg"    # Ljava/lang/String;
    .param p2, "channel"    # Ljava/lang/String;
    .param p3, "node"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 334
    const/4 v0, 0x0

    return-object v0
.end method

.method public hasSession()Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 232
    const-string v0, "GPSDKv2_SVC"

    const-string v1, "[hasSession]"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 233
    invoke-static {}, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->HasSession()Z

    move-result v0

    return v0
.end method

.method public isSecureChannel(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p1, "pkg"    # Ljava/lang/String;
    .param p2, "channel"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 346
    const/4 v0, 0x0

    return v0
.end method

.method public joinChannel(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "pkg"    # Ljava/lang/String;
    .param p2, "channel"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 284
    const-string v1, "GPSDKv2_SVC"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "joinChannel pkg:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ",ch:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 285
    invoke-static {}, Lcom/samsung/groupcast/core/session/PackageSessionManager;->getInstance()Lcom/samsung/groupcast/core/session/PackageSessionManager;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/samsung/groupcast/core/session/PackageSessionManager;->getPackageSession(Ljava/lang/String;)Lcom/samsung/groupcast/core/session/PackageSession;

    move-result-object v0

    .line 286
    .local v0, "ps":Lcom/samsung/groupcast/core/session/PackageSession;
    if-eqz v0, :cond_0

    .line 287
    invoke-virtual {v0, p2}, Lcom/samsung/groupcast/core/session/PackageSession;->joinChannel(Ljava/lang/String;)Ljava/lang/String;

    .line 289
    :cond_0
    return-object p2
.end method

.method public leaveChannel(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "pkg"    # Ljava/lang/String;
    .param p2, "channel"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 294
    const-string v1, "GPSDKv2_SVC"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "leaveChannel pkg:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ",ch:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 295
    invoke-static {}, Lcom/samsung/groupcast/core/session/PackageSessionManager;->getInstance()Lcom/samsung/groupcast/core/session/PackageSessionManager;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/samsung/groupcast/core/session/PackageSessionManager;->getPackageSession(Ljava/lang/String;)Lcom/samsung/groupcast/core/session/PackageSession;

    move-result-object v0

    .line 296
    .local v0, "ps":Lcom/samsung/groupcast/core/session/PackageSession;
    if-eqz v0, :cond_0

    .line 297
    invoke-virtual {v0, p2}, Lcom/samsung/groupcast/core/session/PackageSession;->leaveChannel(Ljava/lang/String;)V

    .line 299
    :cond_0
    return-void
.end method

.method public registerCallback(Ljava/lang/String;Lcom/samsung/groupcast/service/IGroupPlayCallback;)V
    .locals 2
    .param p1, "pkgName"    # Ljava/lang/String;
    .param p2, "cb"    # Lcom/samsung/groupcast/service/IGroupPlayCallback;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 239
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "registerCallback:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    # invokes: Lcom/samsung/groupcast/service/GroupPlayStateService;->debugLog(Ljava/lang/String;)V
    invoke-static {v0}, Lcom/samsung/groupcast/service/GroupPlayStateService;->access$100(Ljava/lang/String;)V

    .line 241
    if-eqz p2, :cond_0

    .line 242
    iget-object v0, p0, Lcom/samsung/groupcast/service/GroupPlayStateService$2;->this$0:Lcom/samsung/groupcast/service/GroupPlayStateService;

    # getter for: Lcom/samsung/groupcast/service/GroupPlayStateService;->mGPCallbacks:Landroid/os/RemoteCallbackList;
    invoke-static {v0}, Lcom/samsung/groupcast/service/GroupPlayStateService;->access$000(Lcom/samsung/groupcast/service/GroupPlayStateService;)Landroid/os/RemoteCallbackList;

    move-result-object v0

    invoke-virtual {v0, p2, p1}, Landroid/os/RemoteCallbackList;->register(Landroid/os/IInterface;Ljava/lang/Object;)Z

    .line 244
    new-instance v0, Lcom/samsung/groupcast/service/GroupPlayStateService$2$1;

    invoke-direct {v0, p0, p1}, Lcom/samsung/groupcast/service/GroupPlayStateService$2$1;-><init>(Lcom/samsung/groupcast/service/GroupPlayStateService$2;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/samsung/groupcast/application/SideWorkingQueue;->post(Ljava/lang/Runnable;)V

    .line 252
    :cond_0
    return-void
.end method

.method public sendData(Lcom/samsung/groupcast/service/GroupPlayPacket;)V
    .locals 5
    .param p1, "pkt"    # Lcom/samsung/groupcast/service/GroupPlayPacket;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 310
    invoke-static {}, Lcom/samsung/groupcast/core/session/PackageSessionManager;->getInstance()Lcom/samsung/groupcast/core/session/PackageSessionManager;

    move-result-object v1

    iget-object v2, p1, Lcom/samsung/groupcast/service/GroupPlayPacket;->pkgName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/samsung/groupcast/core/session/PackageSessionManager;->getPackageSession(Ljava/lang/String;)Lcom/samsung/groupcast/core/session/PackageSession;

    move-result-object v0

    .line 311
    .local v0, "ps":Lcom/samsung/groupcast/core/session/PackageSession;
    if-eqz v0, :cond_0

    .line 312
    iget-object v1, p1, Lcom/samsung/groupcast/service/GroupPlayPacket;->channel:Ljava/lang/String;

    iget-object v2, p1, Lcom/samsung/groupcast/service/GroupPlayPacket;->toNode:Ljava/lang/String;

    iget-object v3, p1, Lcom/samsung/groupcast/service/GroupPlayPacket;->payloadType:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/samsung/groupcast/service/GroupPlayPacket;->getDataInArray()[[B

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/samsung/groupcast/core/session/PackageSession;->sendData(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[[B)V

    .line 314
    :cond_0
    return-void
.end method

.method public sendMessageToAll(Lcom/samsung/groupcast/service/GroupPlayPacket;)V
    .locals 4
    .param p1, "pkt"    # Lcom/samsung/groupcast/service/GroupPlayPacket;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 318
    invoke-static {}, Lcom/samsung/groupcast/core/session/PackageSessionManager;->getInstance()Lcom/samsung/groupcast/core/session/PackageSessionManager;

    move-result-object v1

    iget-object v2, p1, Lcom/samsung/groupcast/service/GroupPlayPacket;->pkgName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/samsung/groupcast/core/session/PackageSessionManager;->getPackageSession(Ljava/lang/String;)Lcom/samsung/groupcast/core/session/PackageSession;

    move-result-object v0

    .line 319
    .local v0, "ps":Lcom/samsung/groupcast/core/session/PackageSession;
    if-eqz v0, :cond_0

    .line 320
    iget-object v1, p1, Lcom/samsung/groupcast/service/GroupPlayPacket;->channel:Ljava/lang/String;

    iget-object v2, p1, Lcom/samsung/groupcast/service/GroupPlayPacket;->payloadType:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/samsung/groupcast/service/GroupPlayPacket;->getDataInArray()[[B

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/groupcast/core/session/PackageSession;->sendDataToAll(Ljava/lang/String;Ljava/lang/String;[[B)V

    .line 322
    :cond_0
    return-void
.end method

.method public unregisterCallback(Lcom/samsung/groupcast/service/IGroupPlayCallback;)V
    .locals 2
    .param p1, "cb"    # Lcom/samsung/groupcast/service/IGroupPlayCallback;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 256
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "unregisterCallback:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    # invokes: Lcom/samsung/groupcast/service/GroupPlayStateService;->debugLog(Ljava/lang/String;)V
    invoke-static {v0}, Lcom/samsung/groupcast/service/GroupPlayStateService;->access$100(Ljava/lang/String;)V

    .line 257
    if-eqz p1, :cond_0

    .line 258
    iget-object v0, p0, Lcom/samsung/groupcast/service/GroupPlayStateService$2;->this$0:Lcom/samsung/groupcast/service/GroupPlayStateService;

    # getter for: Lcom/samsung/groupcast/service/GroupPlayStateService;->mGPCallbacks:Landroid/os/RemoteCallbackList;
    invoke-static {v0}, Lcom/samsung/groupcast/service/GroupPlayStateService;->access$000(Lcom/samsung/groupcast/service/GroupPlayStateService;)Landroid/os/RemoteCallbackList;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/os/RemoteCallbackList;->unregister(Landroid/os/IInterface;)Z

    .line 260
    :cond_0
    return-void
.end method
