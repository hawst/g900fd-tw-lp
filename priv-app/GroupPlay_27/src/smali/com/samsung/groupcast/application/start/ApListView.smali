.class public Lcom/samsung/groupcast/application/start/ApListView;
.super Landroid/widget/AdapterView;
.source "ApListView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/groupcast/application/start/ApListView$Delegate;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/AdapterView",
        "<",
        "Landroid/widget/ListAdapter;",
        ">;"
    }
.end annotation


# instance fields
.field protected mAdapter:Landroid/widget/ListAdapter;

.field public mAlwaysOverrideTouch:Z

.field protected mCurrentX:I

.field private mDataChanged:Z

.field private mDataObserver:Landroid/database/DataSetObserver;

.field private mDelegate:Lcom/samsung/groupcast/application/start/ApListView$Delegate;

.field private mDisplayOffset:I

.field private mGesture:Landroid/view/GestureDetector;

.field private mLeftViewIndex:I

.field private mMaxX:I

.field protected mNextX:I

.field private mOnGesture:Landroid/view/GestureDetector$OnGestureListener;

.field private mOnItemClicked:Landroid/widget/AdapterView$OnItemClickListener;

.field private mOnItemLongClicked:Landroid/widget/AdapterView$OnItemLongClickListener;

.field private mOnItemSelected:Landroid/widget/AdapterView$OnItemSelectedListener;

.field private mRemovedViewQueue:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private mRightViewIndex:I

.field protected mScroller:Landroid/widget/Scroller;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v1, 0x0

    .line 44
    invoke-direct {p0, p1, p2}, Landroid/widget/AdapterView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 24
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/groupcast/application/start/ApListView;->mDelegate:Lcom/samsung/groupcast/application/start/ApListView$Delegate;

    .line 26
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/groupcast/application/start/ApListView;->mAlwaysOverrideTouch:Z

    .line 28
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/groupcast/application/start/ApListView;->mLeftViewIndex:I

    .line 29
    iput v1, p0, Lcom/samsung/groupcast/application/start/ApListView;->mRightViewIndex:I

    .line 32
    const v0, 0x7fffffff

    iput v0, p0, Lcom/samsung/groupcast/application/start/ApListView;->mMaxX:I

    .line 33
    iput v1, p0, Lcom/samsung/groupcast/application/start/ApListView;->mDisplayOffset:I

    .line 36
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/samsung/groupcast/application/start/ApListView;->mRemovedViewQueue:Ljava/util/Queue;

    .line 40
    iput-boolean v1, p0, Lcom/samsung/groupcast/application/start/ApListView;->mDataChanged:Z

    .line 78
    new-instance v0, Lcom/samsung/groupcast/application/start/ApListView$1;

    invoke-direct {v0, p0}, Lcom/samsung/groupcast/application/start/ApListView$1;-><init>(Lcom/samsung/groupcast/application/start/ApListView;)V

    iput-object v0, p0, Lcom/samsung/groupcast/application/start/ApListView;->mDataObserver:Landroid/database/DataSetObserver;

    .line 320
    new-instance v0, Lcom/samsung/groupcast/application/start/ApListView$3;

    invoke-direct {v0, p0}, Lcom/samsung/groupcast/application/start/ApListView$3;-><init>(Lcom/samsung/groupcast/application/start/ApListView;)V

    iput-object v0, p0, Lcom/samsung/groupcast/application/start/ApListView;->mOnGesture:Landroid/view/GestureDetector$OnGestureListener;

    .line 45
    invoke-direct {p0}, Lcom/samsung/groupcast/application/start/ApListView;->initView()V

    .line 46
    return-void
.end method

.method static synthetic access$002(Lcom/samsung/groupcast/application/start/ApListView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/groupcast/application/start/ApListView;
    .param p1, "x1"    # Z

    .prologue
    .line 20
    iput-boolean p1, p0, Lcom/samsung/groupcast/application/start/ApListView;->mDataChanged:Z

    return p1
.end method

.method static synthetic access$100(Lcom/samsung/groupcast/application/start/ApListView;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/groupcast/application/start/ApListView;

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/samsung/groupcast/application/start/ApListView;->reset()V

    return-void
.end method

.method static synthetic access$200(Lcom/samsung/groupcast/application/start/ApListView;)Landroid/widget/AdapterView$OnItemClickListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/application/start/ApListView;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/ApListView;->mOnItemClicked:Landroid/widget/AdapterView$OnItemClickListener;

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/groupcast/application/start/ApListView;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/application/start/ApListView;

    .prologue
    .line 20
    iget v0, p0, Lcom/samsung/groupcast/application/start/ApListView;->mLeftViewIndex:I

    return v0
.end method

.method static synthetic access$400(Lcom/samsung/groupcast/application/start/ApListView;)Landroid/widget/AdapterView$OnItemSelectedListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/application/start/ApListView;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/ApListView;->mOnItemSelected:Landroid/widget/AdapterView$OnItemSelectedListener;

    return-object v0
.end method

.method static synthetic access$500(Lcom/samsung/groupcast/application/start/ApListView;)Landroid/widget/AdapterView$OnItemLongClickListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/application/start/ApListView;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/ApListView;->mOnItemLongClicked:Landroid/widget/AdapterView$OnItemLongClickListener;

    return-object v0
.end method

.method private addAndMeasureChild(Landroid/view/View;I)V
    .locals 4
    .param p1, "child"    # Landroid/view/View;
    .param p2, "viewPos"    # I

    .prologue
    const/4 v1, -0x1

    const/high16 v3, -0x80000000

    .line 131
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 132
    .local v0, "params":Landroid/view/ViewGroup$LayoutParams;
    if-nez v0, :cond_0

    .line 133
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    .end local v0    # "params":Landroid/view/ViewGroup$LayoutParams;
    invoke-direct {v0, v1, v1}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 136
    .restart local v0    # "params":Landroid/view/ViewGroup$LayoutParams;
    :cond_0
    const/4 v1, 0x1

    invoke-virtual {p0, p1, p2, v0, v1}, Lcom/samsung/groupcast/application/start/ApListView;->addViewInLayout(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;Z)Z

    .line 137
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/ApListView;->getWidth()I

    move-result v1

    invoke-static {v1, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/ApListView;->getHeight()I

    move-result v2

    invoke-static {v2, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-virtual {p1, v1, v2}, Landroid/view/View;->measure(II)V

    .line 139
    return-void
.end method

.method private fillList(I)V
    .locals 3
    .param p1, "dx"    # I

    .prologue
    .line 216
    const/4 v1, 0x0

    .line 217
    .local v1, "edge":I
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/ApListView;->getChildCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {p0, v2}, Lcom/samsung/groupcast/application/start/ApListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 218
    .local v0, "child":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 219
    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v1

    .line 221
    :cond_0
    invoke-direct {p0, v1, p1}, Lcom/samsung/groupcast/application/start/ApListView;->fillListRight(II)V

    .line 223
    const/4 v1, 0x0

    .line 224
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/samsung/groupcast/application/start/ApListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 225
    if-eqz v0, :cond_1

    .line 226
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v1

    .line 228
    :cond_1
    invoke-direct {p0, v1, p1}, Lcom/samsung/groupcast/application/start/ApListView;->fillListLeft(II)V

    .line 229
    return-void
.end method

.method private fillListLeft(II)V
    .locals 4
    .param p1, "leftEdge"    # I
    .param p2, "dx"    # I

    .prologue
    .line 251
    :goto_0
    add-int v1, p1, p2

    if-lez v1, :cond_0

    iget v1, p0, Lcom/samsung/groupcast/application/start/ApListView;->mLeftViewIndex:I

    if-ltz v1, :cond_0

    .line 252
    iget-object v2, p0, Lcom/samsung/groupcast/application/start/ApListView;->mAdapter:Landroid/widget/ListAdapter;

    iget v3, p0, Lcom/samsung/groupcast/application/start/ApListView;->mLeftViewIndex:I

    iget-object v1, p0, Lcom/samsung/groupcast/application/start/ApListView;->mRemovedViewQueue:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    invoke-interface {v2, v3, v1, p0}, Landroid/widget/ListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 253
    .local v0, "child":Landroid/view/View;
    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/samsung/groupcast/application/start/ApListView;->addAndMeasureChild(Landroid/view/View;I)V

    .line 254
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    sub-int/2addr p1, v1

    .line 255
    iget v1, p0, Lcom/samsung/groupcast/application/start/ApListView;->mLeftViewIndex:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/samsung/groupcast/application/start/ApListView;->mLeftViewIndex:I

    .line 256
    iget v1, p0, Lcom/samsung/groupcast/application/start/ApListView;->mDisplayOffset:I

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    sub-int/2addr v1, v2

    iput v1, p0, Lcom/samsung/groupcast/application/start/ApListView;->mDisplayOffset:I

    goto :goto_0

    .line 258
    .end local v0    # "child":Landroid/view/View;
    :cond_0
    return-void
.end method

.method private fillListRight(II)V
    .locals 4
    .param p1, "rightEdge"    # I
    .param p2, "dx"    # I

    .prologue
    .line 232
    :goto_0
    add-int v1, p1, p2

    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/ApListView;->getWidth()I

    move-result v2

    if-ge v1, v2, :cond_2

    iget v1, p0, Lcom/samsung/groupcast/application/start/ApListView;->mRightViewIndex:I

    iget-object v2, p0, Lcom/samsung/groupcast/application/start/ApListView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v2}, Landroid/widget/ListAdapter;->getCount()I

    move-result v2

    if-ge v1, v2, :cond_2

    .line 234
    iget-object v2, p0, Lcom/samsung/groupcast/application/start/ApListView;->mAdapter:Landroid/widget/ListAdapter;

    iget v3, p0, Lcom/samsung/groupcast/application/start/ApListView;->mRightViewIndex:I

    iget-object v1, p0, Lcom/samsung/groupcast/application/start/ApListView;->mRemovedViewQueue:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    invoke-interface {v2, v3, v1, p0}, Landroid/widget/ListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 235
    .local v0, "child":Landroid/view/View;
    const/4 v1, -0x1

    invoke-direct {p0, v0, v1}, Lcom/samsung/groupcast/application/start/ApListView;->addAndMeasureChild(Landroid/view/View;I)V

    .line 236
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    add-int/2addr p1, v1

    .line 238
    iget v1, p0, Lcom/samsung/groupcast/application/start/ApListView;->mRightViewIndex:I

    iget-object v2, p0, Lcom/samsung/groupcast/application/start/ApListView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v2}, Landroid/widget/ListAdapter;->getCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ne v1, v2, :cond_0

    .line 239
    iget v1, p0, Lcom/samsung/groupcast/application/start/ApListView;->mCurrentX:I

    add-int/2addr v1, p1

    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/ApListView;->getWidth()I

    move-result v2

    sub-int/2addr v1, v2

    iput v1, p0, Lcom/samsung/groupcast/application/start/ApListView;->mMaxX:I

    .line 242
    :cond_0
    iget v1, p0, Lcom/samsung/groupcast/application/start/ApListView;->mMaxX:I

    if-gez v1, :cond_1

    .line 243
    const/4 v1, 0x0

    iput v1, p0, Lcom/samsung/groupcast/application/start/ApListView;->mMaxX:I

    .line 245
    :cond_1
    iget v1, p0, Lcom/samsung/groupcast/application/start/ApListView;->mRightViewIndex:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/samsung/groupcast/application/start/ApListView;->mRightViewIndex:I

    goto :goto_0

    .line 248
    .end local v0    # "child":Landroid/view/View;
    :cond_2
    return-void
.end method

.method private declared-synchronized initView()V
    .locals 3

    .prologue
    .line 53
    monitor-enter p0

    const/4 v0, -0x1

    :try_start_0
    iput v0, p0, Lcom/samsung/groupcast/application/start/ApListView;->mLeftViewIndex:I

    .line 54
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/groupcast/application/start/ApListView;->mRightViewIndex:I

    .line 55
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/groupcast/application/start/ApListView;->mDisplayOffset:I

    .line 56
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/groupcast/application/start/ApListView;->mCurrentX:I

    .line 57
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/groupcast/application/start/ApListView;->mNextX:I

    .line 58
    const v0, 0x7fffffff

    iput v0, p0, Lcom/samsung/groupcast/application/start/ApListView;->mMaxX:I

    .line 59
    new-instance v0, Landroid/widget/Scroller;

    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/ApListView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/groupcast/application/start/ApListView;->mScroller:Landroid/widget/Scroller;

    .line 60
    new-instance v0, Landroid/view/GestureDetector;

    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/ApListView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/groupcast/application/start/ApListView;->mOnGesture:Landroid/view/GestureDetector$OnGestureListener;

    invoke-direct {v0, v1, v2}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/samsung/groupcast/application/start/ApListView;->mGesture:Landroid/view/GestureDetector;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 61
    monitor-exit p0

    return-void

    .line 53
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private positionItems(I)V
    .locals 7
    .param p1, "dx"    # I

    .prologue
    .line 281
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/ApListView;->getChildCount()I

    move-result v4

    if-lez v4, :cond_0

    .line 282
    iget v4, p0, Lcom/samsung/groupcast/application/start/ApListView;->mDisplayOffset:I

    add-int/2addr v4, p1

    iput v4, p0, Lcom/samsung/groupcast/application/start/ApListView;->mDisplayOffset:I

    .line 283
    iget v3, p0, Lcom/samsung/groupcast/application/start/ApListView;->mDisplayOffset:I

    .line 284
    .local v3, "left":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/ApListView;->getChildCount()I

    move-result v4

    if-ge v2, v4, :cond_0

    .line 285
    invoke-virtual {p0, v2}, Lcom/samsung/groupcast/application/start/ApListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 286
    .local v0, "child":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    .line 287
    .local v1, "childWidth":I
    const/4 v4, 0x0

    add-int v5, v3, v1

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v6

    invoke-virtual {v0, v3, v4, v5, v6}, Landroid/view/View;->layout(IIII)V

    .line 288
    invoke-virtual {v0}, Landroid/view/View;->getPaddingRight()I

    move-result v4

    add-int/2addr v4, v1

    add-int/2addr v3, v4

    .line 284
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 291
    .end local v0    # "child":Landroid/view/View;
    .end local v1    # "childWidth":I
    .end local v2    # "i":I
    .end local v3    # "left":I
    :cond_0
    return-void
.end method

.method private removeNonVisibleItems(I)V
    .locals 4
    .param p1, "dx"    # I

    .prologue
    const/4 v3, 0x0

    .line 261
    invoke-virtual {p0, v3}, Lcom/samsung/groupcast/application/start/ApListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 262
    .local v0, "child":Landroid/view/View;
    :goto_0
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v1

    add-int/2addr v1, p1

    if-gtz v1, :cond_0

    .line 263
    iget v1, p0, Lcom/samsung/groupcast/application/start/ApListView;->mDisplayOffset:I

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    add-int/2addr v1, v2

    iput v1, p0, Lcom/samsung/groupcast/application/start/ApListView;->mDisplayOffset:I

    .line 264
    iget-object v1, p0, Lcom/samsung/groupcast/application/start/ApListView;->mRemovedViewQueue:Ljava/util/Queue;

    invoke-interface {v1, v0}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    .line 265
    invoke-virtual {p0, v0}, Lcom/samsung/groupcast/application/start/ApListView;->removeViewInLayout(Landroid/view/View;)V

    .line 266
    iget v1, p0, Lcom/samsung/groupcast/application/start/ApListView;->mLeftViewIndex:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/samsung/groupcast/application/start/ApListView;->mLeftViewIndex:I

    .line 267
    invoke-virtual {p0, v3}, Lcom/samsung/groupcast/application/start/ApListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 271
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/ApListView;->getChildCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p0, v1}, Lcom/samsung/groupcast/application/start/ApListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 272
    :goto_1
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v1

    add-int/2addr v1, p1

    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/ApListView;->getWidth()I

    move-result v2

    if-lt v1, v2, :cond_1

    .line 273
    iget-object v1, p0, Lcom/samsung/groupcast/application/start/ApListView;->mRemovedViewQueue:Ljava/util/Queue;

    invoke-interface {v1, v0}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    .line 274
    invoke-virtual {p0, v0}, Lcom/samsung/groupcast/application/start/ApListView;->removeViewInLayout(Landroid/view/View;)V

    .line 275
    iget v1, p0, Lcom/samsung/groupcast/application/start/ApListView;->mRightViewIndex:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/samsung/groupcast/application/start/ApListView;->mRightViewIndex:I

    .line 276
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/ApListView;->getChildCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p0, v1}, Lcom/samsung/groupcast/application/start/ApListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    goto :goto_1

    .line 278
    :cond_1
    return-void
.end method

.method private declared-synchronized reset()V
    .locals 1

    .prologue
    .line 120
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/samsung/groupcast/application/start/ApListView;->initView()V

    .line 121
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/ApListView;->removeAllViewsInLayout()V

    .line 122
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/ApListView;->requestLayout()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 123
    monitor-exit p0

    return-void

    .line 120
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private setArrow()V
    .locals 9

    .prologue
    .line 193
    const/4 v4, 0x0

    .line 194
    .local v4, "right":Z
    const/4 v2, 0x0

    .line 195
    .local v2, "left":Z
    const/16 v3, 0xa

    .line 196
    .local v3, "margin":I
    const/4 v7, 0x0

    invoke-virtual {p0, v7}, Lcom/samsung/groupcast/application/start/ApListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 197
    .local v0, "child":Landroid/view/View;
    if-eqz v0, :cond_1

    .line 198
    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v1

    .line 199
    .local v1, "childWidth":I
    iget-object v7, p0, Lcom/samsung/groupcast/application/start/ApListView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v7}, Landroid/widget/ListAdapter;->getCount()I

    move-result v7

    mul-int v5, v7, v1

    .line 200
    .local v5, "totalWidth":I
    mul-int/lit8 v6, v1, 0x4

    .line 202
    .local v6, "visibleWidth":I
    iget-object v7, p0, Lcom/samsung/groupcast/application/start/ApListView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v7}, Landroid/widget/ListAdapter;->getCount()I

    move-result v7

    const/4 v8, 0x4

    if-le v7, v8, :cond_1

    .line 203
    const/4 v4, 0x1

    .line 204
    iget v7, p0, Lcom/samsung/groupcast/application/start/ApListView;->mCurrentX:I

    sub-int v8, v5, v6

    sub-int/2addr v8, v3

    if-lt v7, v8, :cond_0

    .line 205
    const/4 v4, 0x0

    .line 207
    :cond_0
    iget v7, p0, Lcom/samsung/groupcast/application/start/ApListView;->mCurrentX:I

    if-le v7, v3, :cond_1

    .line 208
    const/4 v2, 0x1

    .line 212
    .end local v1    # "childWidth":I
    .end local v5    # "totalWidth":I
    .end local v6    # "visibleWidth":I
    :cond_1
    iget-object v7, p0, Lcom/samsung/groupcast/application/start/ApListView;->mDelegate:Lcom/samsung/groupcast/application/start/ApListView$Delegate;

    iget-object v8, p0, Lcom/samsung/groupcast/application/start/ApListView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v8}, Landroid/widget/ListAdapter;->getCount()I

    move-result v8

    invoke-interface {v7, v2, v4, v8}, Lcom/samsung/groupcast/application/start/ApListView$Delegate;->showArrow(ZZI)V

    .line 213
    return-void
.end method


# virtual methods
.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 300
    invoke-super {p0, p1}, Landroid/widget/AdapterView;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 301
    .local v0, "handled":Z
    iget-object v1, p0, Lcom/samsung/groupcast/application/start/ApListView;->mGesture:Landroid/view/GestureDetector;

    invoke-virtual {v1, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    or-int/2addr v0, v1

    .line 302
    return v0
.end method

.method public bridge synthetic getAdapter()Landroid/widget/Adapter;
    .locals 1

    .prologue
    .line 20
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/ApListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    return-object v0
.end method

.method public getAdapter()Landroid/widget/ListAdapter;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/ApListView;->mAdapter:Landroid/widget/ListAdapter;

    return-object v0
.end method

.method public getSelectedView()Landroid/view/View;
    .locals 1

    .prologue
    .line 106
    const/4 v0, 0x0

    return-object v0
.end method

.method protected onDown(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v1, 0x1

    .line 316
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/ApListView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v0, v1}, Landroid/widget/Scroller;->forceFinished(Z)V

    .line 317
    return v1
.end method

.method protected onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 9
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "velocityX"    # F
    .param p4, "velocityY"    # F

    .prologue
    .line 307
    monitor-enter p0

    .line 308
    :try_start_0
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/ApListView;->mScroller:Landroid/widget/Scroller;

    iget v1, p0, Lcom/samsung/groupcast/application/start/ApListView;->mNextX:I

    const/4 v2, 0x0

    neg-float v3, p3

    float-to-int v3, v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    iget v6, p0, Lcom/samsung/groupcast/application/start/ApListView;->mMaxX:I

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v0 .. v8}, Landroid/widget/Scroller;->fling(IIIIIIII)V

    .line 309
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 310
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/ApListView;->requestLayout()V

    .line 312
    const/4 v0, 0x1

    return v0

    .line 309
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method protected declared-synchronized onLayout(ZIIII)V
    .locals 5
    .param p1, "changed"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I

    .prologue
    .line 145
    monitor-enter p0

    :try_start_0
    invoke-super/range {p0 .. p5}, Landroid/widget/AdapterView;->onLayout(ZIIII)V

    .line 147
    iget-object v3, p0, Lcom/samsung/groupcast/application/start/ApListView;->mAdapter:Landroid/widget/ListAdapter;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v3, :cond_0

    .line 190
    :goto_0
    monitor-exit p0

    return-void

    .line 151
    :cond_0
    :try_start_1
    iget-boolean v3, p0, Lcom/samsung/groupcast/application/start/ApListView;->mDataChanged:Z

    if-eqz v3, :cond_1

    .line 152
    iget v1, p0, Lcom/samsung/groupcast/application/start/ApListView;->mCurrentX:I

    .line 153
    .local v1, "oldCurrentX":I
    invoke-direct {p0}, Lcom/samsung/groupcast/application/start/ApListView;->initView()V

    .line 154
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/ApListView;->removeAllViewsInLayout()V

    .line 155
    iput v1, p0, Lcom/samsung/groupcast/application/start/ApListView;->mNextX:I

    .line 156
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/samsung/groupcast/application/start/ApListView;->mDataChanged:Z

    .line 159
    .end local v1    # "oldCurrentX":I
    :cond_1
    iget-object v3, p0, Lcom/samsung/groupcast/application/start/ApListView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v3}, Landroid/widget/Scroller;->computeScrollOffset()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 160
    iget-object v3, p0, Lcom/samsung/groupcast/application/start/ApListView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v3}, Landroid/widget/Scroller;->getCurrX()I

    move-result v2

    .line 161
    .local v2, "scrollx":I
    iput v2, p0, Lcom/samsung/groupcast/application/start/ApListView;->mNextX:I

    .line 164
    .end local v2    # "scrollx":I
    :cond_2
    iget v3, p0, Lcom/samsung/groupcast/application/start/ApListView;->mNextX:I

    if-gtz v3, :cond_3

    .line 165
    const/4 v3, 0x0

    iput v3, p0, Lcom/samsung/groupcast/application/start/ApListView;->mNextX:I

    .line 166
    iget-object v3, p0, Lcom/samsung/groupcast/application/start/ApListView;->mScroller:Landroid/widget/Scroller;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/widget/Scroller;->forceFinished(Z)V

    .line 168
    :cond_3
    iget v3, p0, Lcom/samsung/groupcast/application/start/ApListView;->mNextX:I

    iget v4, p0, Lcom/samsung/groupcast/application/start/ApListView;->mMaxX:I

    if-lt v3, v4, :cond_4

    .line 169
    iget v3, p0, Lcom/samsung/groupcast/application/start/ApListView;->mMaxX:I

    iput v3, p0, Lcom/samsung/groupcast/application/start/ApListView;->mNextX:I

    .line 170
    iget-object v3, p0, Lcom/samsung/groupcast/application/start/ApListView;->mScroller:Landroid/widget/Scroller;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/widget/Scroller;->forceFinished(Z)V

    .line 173
    :cond_4
    iget v3, p0, Lcom/samsung/groupcast/application/start/ApListView;->mCurrentX:I

    iget v4, p0, Lcom/samsung/groupcast/application/start/ApListView;->mNextX:I

    sub-int v0, v3, v4

    .line 175
    .local v0, "dx":I
    invoke-direct {p0, v0}, Lcom/samsung/groupcast/application/start/ApListView;->removeNonVisibleItems(I)V

    .line 176
    invoke-direct {p0, v0}, Lcom/samsung/groupcast/application/start/ApListView;->fillList(I)V

    .line 177
    invoke-direct {p0, v0}, Lcom/samsung/groupcast/application/start/ApListView;->positionItems(I)V

    .line 179
    iget v3, p0, Lcom/samsung/groupcast/application/start/ApListView;->mNextX:I

    iput v3, p0, Lcom/samsung/groupcast/application/start/ApListView;->mCurrentX:I

    .line 181
    iget-object v3, p0, Lcom/samsung/groupcast/application/start/ApListView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v3}, Landroid/widget/Scroller;->isFinished()Z

    move-result v3

    if-nez v3, :cond_5

    .line 182
    new-instance v3, Lcom/samsung/groupcast/application/start/ApListView$2;

    invoke-direct {v3, p0}, Lcom/samsung/groupcast/application/start/ApListView$2;-><init>(Lcom/samsung/groupcast/application/start/ApListView;)V

    invoke-virtual {p0, v3}, Lcom/samsung/groupcast/application/start/ApListView;->post(Ljava/lang/Runnable;)Z

    .line 189
    :cond_5
    invoke-direct {p0}, Lcom/samsung/groupcast/application/start/ApListView;->setArrow()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 145
    .end local v0    # "dx":I
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3
.end method

.method public declared-synchronized scrollTo(I)V
    .locals 5
    .param p1, "x"    # I

    .prologue
    .line 294
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/ApListView;->mScroller:Landroid/widget/Scroller;

    iget v1, p0, Lcom/samsung/groupcast/application/start/ApListView;->mNextX:I

    const/4 v2, 0x0

    iget v3, p0, Lcom/samsung/groupcast/application/start/ApListView;->mNextX:I

    sub-int v3, p1, v3

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/Scroller;->startScroll(IIII)V

    .line 295
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/ApListView;->requestLayout()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 296
    monitor-exit p0

    return-void

    .line 294
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public bridge synthetic setAdapter(Landroid/widget/Adapter;)V
    .locals 0
    .param p1, "x0"    # Landroid/widget/Adapter;

    .prologue
    .line 20
    check-cast p1, Landroid/widget/ListAdapter;

    .end local p1    # "x0":Landroid/widget/Adapter;
    invoke-virtual {p0, p1}, Lcom/samsung/groupcast/application/start/ApListView;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method

.method public setAdapter(Landroid/widget/ListAdapter;)V
    .locals 2
    .param p1, "adapter"    # Landroid/widget/ListAdapter;

    .prologue
    .line 111
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/ApListView;->mAdapter:Landroid/widget/ListAdapter;

    if-eqz v0, :cond_0

    .line 112
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/ApListView;->mAdapter:Landroid/widget/ListAdapter;

    iget-object v1, p0, Lcom/samsung/groupcast/application/start/ApListView;->mDataObserver:Landroid/database/DataSetObserver;

    invoke-interface {v0, v1}, Landroid/widget/ListAdapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 114
    :cond_0
    iput-object p1, p0, Lcom/samsung/groupcast/application/start/ApListView;->mAdapter:Landroid/widget/ListAdapter;

    .line 115
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/ApListView;->mAdapter:Landroid/widget/ListAdapter;

    iget-object v1, p0, Lcom/samsung/groupcast/application/start/ApListView;->mDataObserver:Landroid/database/DataSetObserver;

    invoke-interface {v0, v1}, Landroid/widget/ListAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 116
    invoke-direct {p0}, Lcom/samsung/groupcast/application/start/ApListView;->reset()V

    .line 117
    return-void
.end method

.method public setDelegate(Lcom/samsung/groupcast/application/start/ApListView$Delegate;)V
    .locals 0
    .param p1, "delegate"    # Lcom/samsung/groupcast/application/start/ApListView$Delegate;

    .prologue
    .line 49
    iput-object p1, p0, Lcom/samsung/groupcast/application/start/ApListView;->mDelegate:Lcom/samsung/groupcast/application/start/ApListView$Delegate;

    .line 50
    return-void
.end method

.method public setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V
    .locals 0
    .param p1, "listener"    # Landroid/widget/AdapterView$OnItemClickListener;

    .prologue
    .line 70
    iput-object p1, p0, Lcom/samsung/groupcast/application/start/ApListView;->mOnItemClicked:Landroid/widget/AdapterView$OnItemClickListener;

    .line 71
    return-void
.end method

.method public setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V
    .locals 0
    .param p1, "listener"    # Landroid/widget/AdapterView$OnItemLongClickListener;

    .prologue
    .line 75
    iput-object p1, p0, Lcom/samsung/groupcast/application/start/ApListView;->mOnItemLongClicked:Landroid/widget/AdapterView$OnItemLongClickListener;

    .line 76
    return-void
.end method

.method public setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V
    .locals 0
    .param p1, "listener"    # Landroid/widget/AdapterView$OnItemSelectedListener;

    .prologue
    .line 65
    iput-object p1, p0, Lcom/samsung/groupcast/application/start/ApListView;->mOnItemSelected:Landroid/widget/AdapterView$OnItemSelectedListener;

    .line 66
    return-void
.end method

.method public setSelection(I)V
    .locals 0
    .param p1, "position"    # I

    .prologue
    .line 128
    return-void
.end method
