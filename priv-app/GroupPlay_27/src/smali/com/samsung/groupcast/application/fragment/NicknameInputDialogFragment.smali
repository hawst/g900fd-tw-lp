.class public Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;
.super Landroid/app/DialogFragment;
.source "NicknameInputDialogFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment$NicknameInputDialogFragmentDelegate;
    }
.end annotation


# static fields
.field private static mInstance:Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;


# instance fields
.field private bundle:Landroid/os/Bundle;

.field private mContentView:Landroid/view/View;

.field private mDelegate:Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment$NicknameInputDialogFragmentDelegate;

.field private mNickEditText:Landroid/widget/EditText;

.field private mOkClickListener:Landroid/view/View$OnClickListener;

.field private mOnEditorActionListener:Landroid/widget/TextView$OnEditorActionListener;

.field private mSummittingNickname:Ljava/lang/String;

.field private mTextWatcher:Landroid/text/TextWatcher;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 93
    new-instance v0, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment$1;

    invoke-direct {v0, p0}, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment$1;-><init>(Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;)V

    iput-object v0, p0, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;->mOnEditorActionListener:Landroid/widget/TextView$OnEditorActionListener;

    .line 119
    new-instance v0, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment$2;

    invoke-direct {v0, p0}, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment$2;-><init>(Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;)V

    iput-object v0, p0, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;->mTextWatcher:Landroid/text/TextWatcher;

    .line 204
    new-instance v0, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment$6;

    invoke-direct {v0, p0}, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment$6;-><init>(Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;)V

    iput-object v0, p0, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;->mOkClickListener:Landroid/view/View$OnClickListener;

    return-void
.end method

.method static synthetic access$000(Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;->updateOkButton()V

    return-void
.end method

.method static synthetic access$100(Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;->isNicknameEntered()Z

    move-result v0

    return v0
.end method

.method static synthetic access$200(Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;)Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment$NicknameInputDialogFragmentDelegate;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;->mDelegate:Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment$NicknameInputDialogFragmentDelegate;

    return-object v0
.end method

.method static synthetic access$300()Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;->mInstance:Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;

    return-object v0
.end method

.method static synthetic access$400(Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;)Landroid/widget/EditText;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;->mNickEditText:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$500(Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;->onDialogCancel()V

    return-void
.end method

.method private isNicknameEntered()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 226
    const-string v0, ""

    .line 228
    .local v0, "nickname":Ljava/lang/String;
    iget-object v1, p0, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;->mNickEditText:Landroid/widget/EditText;

    if-nez v1, :cond_1

    .line 229
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v1

    if-nez v1, :cond_0

    move v1, v2

    .line 240
    :goto_0
    return v1

    .line 232
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v1

    const v3, 0x7f07001e

    invoke-virtual {v1, v3}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;->mNickEditText:Landroid/widget/EditText;

    .line 235
    :cond_1
    iget-object v1, p0, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;->mNickEditText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 237
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_3

    :cond_2
    move v1, v2

    .line 238
    goto :goto_0

    .line 240
    :cond_3
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private onDialogCancel()V
    .locals 4

    .prologue
    .line 199
    iget-object v0, p0, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;->mDelegate:Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment$NicknameInputDialogFragmentDelegate;

    if-eqz v0, :cond_0

    .line 200
    iget-object v0, p0, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;->mDelegate:Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment$NicknameInputDialogFragmentDelegate;

    sget-object v1, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;->mInstance:Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-interface {v0, v1, v2, v3}, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment$NicknameInputDialogFragmentDelegate;->onNicknameSubmitted(Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;Ljava/lang/String;Z)V

    .line 201
    :cond_0
    return-void
.end method

.method private updateOkButton()V
    .locals 3

    .prologue
    .line 215
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v1

    check-cast v1, Landroid/app/AlertDialog;

    .line 216
    .local v1, "dialog":Landroid/app/AlertDialog;
    if-nez v1, :cond_0

    .line 223
    :goto_0
    return-void

    .line 218
    :cond_0
    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    .line 219
    .local v0, "button":Landroid/widget/Button;
    invoke-direct {p0}, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;->isNicknameEntered()Z

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 221
    iget-object v2, p0, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;->mOkClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method


# virtual methods
.method public getSummittingNickname()Ljava/lang/String;
    .locals 2

    .prologue
    .line 275
    iget-object v0, p0, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;->mNickEditText:Landroid/widget/EditText;

    if-nez v0, :cond_1

    .line 276
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    if-nez v0, :cond_0

    .line 277
    const-string v0, ""

    .line 287
    :goto_0
    return-object v0

    .line 279
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    const v1, 0x7f07001e

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;->mNickEditText:Landroid/widget/EditText;

    .line 283
    :cond_1
    iget-object v0, p0, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;->mNickEditText:Landroid/widget/EditText;

    if-nez v0, :cond_2

    .line 284
    const-string v0, ""

    goto :goto_0

    .line 287
    :cond_2
    iget-object v0, p0, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;->mNickEditText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 53
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onAttach(Landroid/app/Activity;)V

    .line 54
    const-class v0, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment$NicknameInputDialogFragmentDelegate;

    invoke-static {p1, v0}, Lcom/samsung/groupcast/misc/utility/Verify;->instanceOf(Ljava/lang/Object;Ljava/lang/Class;)V

    .line 55
    check-cast p1, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment$NicknameInputDialogFragmentDelegate;

    .end local p1    # "activity":Landroid/app/Activity;
    invoke-virtual {p0, p1}, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;->setDelegate(Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment$NicknameInputDialogFragmentDelegate;)V

    .line 56
    return-void
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 0
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    .line 194
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onCancel(Landroid/content/DialogInterface;)V

    .line 195
    invoke-direct {p0}, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;->onDialogCancel()V

    .line 196
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v3, 0x0

    const v4, 0x7f08000e

    .line 135
    const/4 v0, 0x0

    .line 137
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    new-instance v0, Landroid/app/AlertDialog$Builder;

    .end local v0    # "builder":Landroid/app/AlertDialog$Builder;
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 140
    .restart local v0    # "builder":Landroid/app/AlertDialog$Builder;
    iput-object v3, p0, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;->mContentView:Landroid/view/View;

    .line 143
    const v1, 0x7f080093

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 144
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f04000a

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;->mContentView:Landroid/view/View;

    .line 146
    iget-object v1, p0, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;->mContentView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 149
    iget-object v1, p0, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;->mContentView:Landroid/view/View;

    if-nez v1, :cond_0

    .line 150
    const v1, 0x7f08003b

    invoke-virtual {p0, v1}, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 151
    new-instance v1, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment$3;

    invoke-direct {v1, p0}, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment$3;-><init>(Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;)V

    invoke-virtual {v0, v4, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 158
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    .line 185
    :goto_0
    return-object v1

    .line 161
    :cond_0
    iget-object v1, p0, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;->mContentView:Landroid/view/View;

    const v2, 0x7f07001e

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;->mNickEditText:Landroid/widget/EditText;

    .line 162
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;->bundle:Landroid/os/Bundle;

    .line 163
    iget-object v1, p0, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;->bundle:Landroid/os/Bundle;

    if-eqz v1, :cond_1

    .line 164
    iget-object v1, p0, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;->mNickEditText:Landroid/widget/EditText;

    iget-object v2, p0, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;->bundle:Landroid/os/Bundle;

    const-string v3, "Nickname"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    .line 166
    :cond_1
    iget-object v1, p0, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;->mNickEditText:Landroid/widget/EditText;

    iget-object v2, p0, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;->mOnEditorActionListener:Landroid/widget/TextView$OnEditorActionListener;

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 167
    iget-object v1, p0, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;->mNickEditText:Landroid/widget/EditText;

    iget-object v2, p0, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;->mTextWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 171
    new-instance v1, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment$4;

    invoke-direct {v1, p0}, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment$4;-><init>(Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;)V

    invoke-virtual {v0, v4, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 177
    const v1, 0x7f08000f

    new-instance v2, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment$5;

    invoke-direct {v2, p0}, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment$5;-><init>(Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 185
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 264
    invoke-super {p0}, Landroid/app/DialogFragment;->onDestroy()V

    .line 265
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;->mNickEditText:Landroid/widget/EditText;

    invoke-static {v0, v1}, Lcom/samsung/groupcast/view/ViewTools;->hideSoftKeyboard(Landroid/app/Activity;Landroid/view/View;)V

    .line 266
    invoke-static {}, Lcom/samsung/groupcast/application/Environment;->getUserName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;->setNewUserName(Ljava/lang/String;)V

    .line 267
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 255
    iget-object v0, p0, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;->mNickEditText:Landroid/widget/EditText;

    if-eqz v0, :cond_0

    .line 256
    iget-object v0, p0, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;->mNickEditText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->clearFocus()V

    .line 259
    :cond_0
    invoke-super {p0}, Landroid/app/DialogFragment;->onPause()V

    .line 260
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 60
    invoke-super {p0}, Landroid/app/DialogFragment;->onResume()V

    .line 61
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    if-nez v0, :cond_1

    .line 91
    :cond_0
    :goto_0
    return-void

    .line 65
    :cond_1
    iget-object v0, p0, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;->mNickEditText:Landroid/widget/EditText;

    if-nez v0, :cond_2

    .line 66
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    const v1, 0x7f07001e

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;->mNickEditText:Landroid/widget/EditText;

    .line 68
    :cond_2
    iget-object v0, p0, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;->mNickEditText:Landroid/widget/EditText;

    if-eqz v0, :cond_0

    .line 71
    iget-object v0, p0, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;->mNickEditText:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;->mOnEditorActionListener:Landroid/widget/TextView$OnEditorActionListener;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 72
    iget-object v0, p0, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;->mNickEditText:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;->mTextWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 73
    iget-object v0, p0, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;->mNickEditText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 75
    iget-object v0, p0, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;->mSummittingNickname:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;->mSummittingNickname:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 76
    iget-object v0, p0, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;->mNickEditText:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;->mSummittingNickname:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 77
    iget-object v0, p0, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;->mNickEditText:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;->mSummittingNickname:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    .line 81
    :cond_3
    iget-object v0, p0, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;->mDelegate:Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment$NicknameInputDialogFragmentDelegate;

    if-nez v0, :cond_4

    invoke-virtual {p0}, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    instance-of v0, v0, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment$NicknameInputDialogFragmentDelegate;

    if-eqz v0, :cond_4

    .line 82
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment$NicknameInputDialogFragmentDelegate;

    invoke-virtual {p0, v0}, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;->setDelegate(Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment$NicknameInputDialogFragmentDelegate;)V

    .line 85
    :cond_4
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;->mNickEditText:Landroid/widget/EditText;

    invoke-static {v0, v1}, Lcom/samsung/groupcast/view/ViewTools;->showSowftKeyboard(Landroid/app/Activity;Landroid/view/View;)V

    .line 86
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 89
    invoke-direct {p0}, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;->updateOkButton()V

    .line 90
    invoke-static {}, Lcom/samsung/groupcast/application/Environment;->getUserName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;->setNewUserName(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 271
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 272
    return-void
.end method

.method public onStop()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 246
    invoke-super {p0}, Landroid/app/DialogFragment;->onStop()V

    .line 247
    iput-object v0, p0, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;->mDelegate:Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment$NicknameInputDialogFragmentDelegate;

    .line 248
    sput-object v0, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;->mInstance:Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;

    .line 249
    iput-object v0, p0, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;->mNickEditText:Landroid/widget/EditText;

    .line 250
    return-void
.end method

.method public setDelegate(Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment$NicknameInputDialogFragmentDelegate;)V
    .locals 0
    .param p1, "mDelegate"    # Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment$NicknameInputDialogFragmentDelegate;

    .prologue
    .line 189
    iput-object p1, p0, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;->mDelegate:Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment$NicknameInputDialogFragmentDelegate;

    .line 190
    return-void
.end method

.method public setSummittingNickname(Ljava/lang/String;)V
    .locals 0
    .param p1, "summittingNickname"    # Ljava/lang/String;

    .prologue
    .line 291
    iput-object p1, p0, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;->mSummittingNickname:Ljava/lang/String;

    .line 292
    return-void
.end method
