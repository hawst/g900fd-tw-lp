.class public Lcom/samsung/groupcast/application/start/StartFragment;
.super Landroid/app/Fragment;
.source "StartFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/groupcast/application/start/StartFragment$DisclaimerFragmentCheckedListener;
    }
.end annotation


# static fields
.field private static mNfcConnectOngoing:Z


# instance fields
.field disclaimerCheckedListener:Lcom/samsung/groupcast/application/start/StartFragment$DisclaimerFragmentCheckedListener;

.field private mBandPassphrase:Ljava/lang/String;

.field private mBandTargetSSID:Ljava/lang/String;

.field private mButton:Landroid/widget/Button;

.field private mCheckBox:Landroid/widget/CheckBox;

.field private mDeviceName:Landroid/widget/EditText;

.field private mHowManyDisclaimerShowing:I

.field mPosition:I

.field private mTextView:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 44
    const/4 v0, 0x0

    sput-boolean v0, Lcom/samsung/groupcast/application/start/StartFragment;->mNfcConnectOngoing:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 32
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/groupcast/application/start/StartFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/groupcast/application/start/StartFragment;

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/samsung/groupcast/application/start/StartFragment;->onCheckBoxCheckChanged()V

    return-void
.end method

.method static newInstance(I)Lcom/samsung/groupcast/application/start/StartFragment;
    .locals 3
    .param p0, "position"    # I

    .prologue
    .line 56
    new-instance v1, Lcom/samsung/groupcast/application/start/StartFragment;

    invoke-direct {v1}, Lcom/samsung/groupcast/application/start/StartFragment;-><init>()V

    .line 58
    .local v1, "f":Lcom/samsung/groupcast/application/start/StartFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 59
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "item_position"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 60
    invoke-virtual {v1, v0}, Lcom/samsung/groupcast/application/start/StartFragment;->setArguments(Landroid/os/Bundle;)V

    .line 62
    return-object v1
.end method

.method private onCheckBoxCheckChanged()V
    .locals 2

    .prologue
    .line 123
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/StartFragment;->mCheckBox:Landroid/widget/CheckBox;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->playSoundEffect(I)V

    .line 124
    return-void
.end method


# virtual methods
.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 117
    invoke-super {p0, p1}, Landroid/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 119
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 36
    invoke-super {p0, p1}, Landroid/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 38
    :try_start_0
    check-cast p1, Lcom/samsung/groupcast/application/start/StartFragment$DisclaimerFragmentCheckedListener;

    .end local p1    # "activity":Landroid/app/Activity;
    iput-object p1, p0, Lcom/samsung/groupcast/application/start/StartFragment;->disclaimerCheckedListener:Lcom/samsung/groupcast/application/start/StartFragment$DisclaimerFragmentCheckedListener;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 42
    :goto_0
    return-void

    .line 39
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 67
    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 68
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "item_position"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    :goto_0
    iput v0, p0, Lcom/samsung/groupcast/application/start/StartFragment;->mPosition:I

    .line 69
    return-void

    .line 68
    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const v3, 0x7f040016

    const/4 v2, 0x0

    .line 74
    iget v1, p0, Lcom/samsung/groupcast/application/start/StartFragment;->mPosition:I

    packed-switch v1, :pswitch_data_0

    .line 109
    invoke-virtual {p1, v3, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 112
    .local v0, "v":Landroid/view/View;
    :goto_0
    return-object v0

    .line 76
    .end local v0    # "v":Landroid/view/View;
    :pswitch_0
    invoke-virtual {p1, v3, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 77
    .restart local v0    # "v":Landroid/view/View;
    goto :goto_0

    .line 79
    .end local v0    # "v":Landroid/view/View;
    :pswitch_1
    const v1, 0x7f040015

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 80
    .restart local v0    # "v":Landroid/view/View;
    const v1, 0x7f07000b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/samsung/groupcast/application/start/StartFragment;->mTextView:Landroid/widget/TextView;

    .line 81
    iget-object v1, p0, Lcom/samsung/groupcast/application/start/StartFragment;->mTextView:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const v3, 0x7f08007b

    invoke-static {v3}, Lcom/samsung/groupcast/application/App;->getSanitizedString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const v3, 0x7f08007c

    invoke-static {v3}, Lcom/samsung/groupcast/application/App;->getSanitizedString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const v3, 0x7f08007d

    invoke-static {v3}, Lcom/samsung/groupcast/application/App;->getSanitizedString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const v3, 0x7f08007e

    invoke-static {v3}, Lcom/samsung/groupcast/application/App;->getSanitizedString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const v3, 0x7f08007f

    invoke-static {v3}, Lcom/samsung/groupcast/application/App;->getSanitizedString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const v3, 0x7f080080

    invoke-static {v3}, Lcom/samsung/groupcast/application/App;->getSanitizedString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const v3, 0x7f080081

    invoke-static {v3}, Lcom/samsung/groupcast/application/App;->getSanitizedString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const v3, 0x7f080082

    invoke-static {v3}, Lcom/samsung/groupcast/application/App;->getSanitizedString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const v3, 0x7f080083

    invoke-static {v3}, Lcom/samsung/groupcast/application/App;->getSanitizedString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const v3, 0x7f080084

    invoke-static {v3}, Lcom/samsung/groupcast/application/App;->getSanitizedString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const v3, 0x7f080085

    invoke-static {v3}, Lcom/samsung/groupcast/application/App;->getSanitizedString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const v3, 0x7f080086

    invoke-static {v3}, Lcom/samsung/groupcast/application/App;->getSanitizedString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const v3, 0x7f080087

    invoke-static {v3}, Lcom/samsung/groupcast/application/App;->getSanitizedString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const v3, 0x7f080088

    invoke-static {v3}, Lcom/samsung/groupcast/application/App;->getSanitizedString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const v3, 0x7f080089

    invoke-static {v3}, Lcom/samsung/groupcast/application/App;->getSanitizedString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 97
    const v1, 0x7f070032

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    iput-object v1, p0, Lcom/samsung/groupcast/application/start/StartFragment;->mCheckBox:Landroid/widget/CheckBox;

    .line 98
    iget-object v1, p0, Lcom/samsung/groupcast/application/start/StartFragment;->mCheckBox:Landroid/widget/CheckBox;

    new-instance v2, Lcom/samsung/groupcast/application/start/StartFragment$1;

    invoke-direct {v2, p0}, Lcom/samsung/groupcast/application/start/StartFragment$1;-><init>(Lcom/samsung/groupcast/application/start/StartFragment;)V

    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    goto/16 :goto_0

    .line 74
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
