.class public Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$AboutItem;
.super Ljava/lang/Object;
.source "SettingActivityListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "AboutItem"
.end annotation


# instance fields
.field private final mItemId:Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$AboutEnum;

.field private final mTitle:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$AboutEnum;Ljava/lang/String;)V
    .locals 0
    .param p1, "itemId"    # Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$AboutEnum;
    .param p2, "title"    # Ljava/lang/String;

    .prologue
    .line 92
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 93
    iput-object p1, p0, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$AboutItem;->mItemId:Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$AboutEnum;

    .line 94
    iput-object p2, p0, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$AboutItem;->mTitle:Ljava/lang/String;

    .line 95
    return-void
.end method


# virtual methods
.method public getItemId()Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$AboutEnum;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$AboutItem;->mItemId:Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$AboutEnum;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$AboutItem;->mTitle:Ljava/lang/String;

    return-object v0
.end method
