.class public Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;
.super Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;
.source "JoinSessionDialogFragment.java"

# interfaces
.implements Lcom/samsung/groupcast/application/fragment/MessageDialogFragment$MessageDialogFragmentDelegate;
.implements Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner$SessionScannerDelegate;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment$JoinSessionDialogFragmentDelegate;,
        Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment$VERSION_COLLISION_STATUS;
    }
.end annotation


# static fields
.field private static final SEARCH_TIMEOUT:I = 0x3a98

.field private static final TAG_JOINING_SESSION_SUMMARY:Ljava/lang/String; = "TAG_JOINING_SESSION_SUMMARY"

.field private static final TAG_JOIN_TIMEOUT:Ljava/lang/String; = "TAG_JOIN_TIMEOUT"

.field public static final TAG_PIN_INPUT_DIALOG:Ljava/lang/String; = "TAG_PIN_INPUT_DIALOG"

.field private static final TAG_VERSION_NOT_COMPATIBLE:Ljava/lang/String; = "TAG_VERSION_NOT_COMPATIBLE"


# instance fields
.field private mFetchManifestRequest:Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchManifestRequest;

.field private mFinished:Z

.field private mIsCheckedShowPasswordCheckBox:Z

.field private mJoiningSessionSummary:Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;

.field private final mManifestFetcher:Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestFetcher;

.field private mNfcAutoConnectionGoingOn:Z

.field private mPinFromBand:Ljava/lang/String;

.field private mReadManifestRequest:Lcom/samsung/groupcast/legacy/gp2/session/storage/ReadManifestRequest;

.field private mScanner:Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner;

.field private mSummittingPincode:Ljava/lang/String;

.field private final searchTimeout:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 75
    invoke-direct {p0}, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;-><init>()V

    .line 46
    iput-boolean v1, p0, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->mFinished:Z

    .line 47
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->mScanner:Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner;

    .line 48
    iput-boolean v1, p0, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->mNfcAutoConnectionGoingOn:Z

    .line 136
    new-instance v0, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment$1;

    invoke-direct {v0, p0}, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment$1;-><init>(Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;)V

    iput-object v0, p0, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->searchTimeout:Ljava/lang/Runnable;

    .line 76
    const-string v0, "---"

    invoke-static {v0}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 77
    new-instance v0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner;

    invoke-static {}, Lcom/samsung/groupcast/application/App;->getMessagingClient()Lcom/samsung/groupcast/core/messaging/MessagingClient;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner;-><init>(Lcom/samsung/groupcast/core/messaging/MessagingClient;)V

    iput-object v0, p0, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->mScanner:Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner;

    .line 78
    new-instance v0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestFetcher;

    invoke-static {}, Lcom/samsung/groupcast/application/App;->getMessagingClient()Lcom/samsung/groupcast/core/messaging/MessagingClient;

    move-result-object v1

    const-string v2, "com.samsung.groupcast.Lobby_V1"

    invoke-direct {v0, v1, v2}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestFetcher;-><init>(Lcom/samsung/groupcast/core/messaging/MessagingClient;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->mManifestFetcher:Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestFetcher;

    .line 80
    const-string v0, "==="

    invoke-static {v0}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 81
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;)Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment$JoinSessionDialogFragmentDelegate;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->getDelegate()Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment$JoinSessionDialogFragmentDelegate;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;

    .prologue
    .line 36
    iget-boolean v0, p0, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->mNfcAutoConnectionGoingOn:Z

    return v0
.end method

.method static synthetic access$200(Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchFileRequest;Lcom/samsung/groupcast/misc/requests/Result;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;
    .param p1, "x1"    # Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchFileRequest;
    .param p4, "x4"    # Lcom/samsung/groupcast/misc/requests/Result;
    .param p5, "x5"    # Ljava/lang/String;

    .prologue
    .line 36
    invoke-direct/range {p0 .. p5}, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->onFetchManifestRequestComplete(Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchFileRequest;Lcom/samsung/groupcast/misc/requests/Result;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$300(Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;Lcom/samsung/groupcast/legacy/gp2/session/storage/ReadManifestRequest;Lcom/samsung/groupcast/misc/requests/Result;Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;
    .param p1, "x1"    # Lcom/samsung/groupcast/legacy/gp2/session/storage/ReadManifestRequest;
    .param p2, "x2"    # Lcom/samsung/groupcast/misc/requests/Result;
    .param p3, "x3"    # Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;
    .param p4, "x4"    # Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;
    .param p5, "x5"    # Ljava/lang/String;

    .prologue
    .line 36
    invoke-direct/range {p0 .. p5}, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->onReadManifestRequestComplete(Lcom/samsung/groupcast/legacy/gp2/session/storage/ReadManifestRequest;Lcom/samsung/groupcast/misc/requests/Result;Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;Ljava/lang/String;)V

    return-void
.end method

.method private getDelegate()Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment$JoinSessionDialogFragmentDelegate;
    .locals 2

    .prologue
    .line 158
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-class v1, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment$JoinSessionDialogFragmentDelegate;

    invoke-static {v0, v1}, Lcom/samsung/groupcast/misc/utility/Verify;->instanceOf(Ljava/lang/Object;Ljava/lang/Class;)V

    .line 159
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment$JoinSessionDialogFragmentDelegate;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 162
    :goto_0
    return-object v0

    .line 160
    :catch_0
    move-exception v0

    .line 162
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private hidePinCodeInputDialog()V
    .locals 4

    .prologue
    .line 215
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->isFinishing()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 230
    :cond_0
    :goto_0
    return-void

    .line 219
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "TAG_PIN_INPUT_DIALOG"

    invoke-virtual {v1, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 222
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-string v3, "TAG_PIN_INPUT_DIALOG"

    invoke-virtual {v2, v3}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 225
    :catch_0
    move-exception v0

    .line 226
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private onFetchManifestRequestComplete(Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchFileRequest;Lcom/samsung/groupcast/misc/requests/Result;Ljava/lang/String;)V
    .locals 8
    .param p1, "summary"    # Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;
    .param p2, "pin"    # Ljava/lang/String;
    .param p3, "request"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchFileRequest;
    .param p4, "result"    # Lcom/samsung/groupcast/misc/requests/Result;
    .param p5, "path"    # Ljava/lang/String;

    .prologue
    .line 310
    const-string v5, "---"

    invoke-static {v5}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 312
    iget-object v5, p0, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->mFetchManifestRequest:Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchManifestRequest;

    if-nez v5, :cond_0

    .line 313
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    const-string v6, "onFetchManifestRequestComplete"

    const-string v7, "mFetchManifestRequest is null"

    invoke-static {v5, v6, v7}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;)V

    .line 349
    :goto_0
    return-void

    .line 317
    :cond_0
    iget-object v5, p0, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->mFetchManifestRequest:Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchManifestRequest;

    invoke-virtual {v5}, Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchManifestRequest;->getSessionId()Ljava/lang/String;

    move-result-object v3

    .line 318
    .local v3, "sessionId":Ljava/lang/String;
    iget-object v5, p0, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->mFetchManifestRequest:Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchManifestRequest;

    invoke-virtual {v5}, Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchManifestRequest;->getManifestId()Ljava/lang/String;

    move-result-object v0

    .line 319
    .local v0, "manifestId":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;->getManifestTimestamp()J

    move-result-wide v1

    .line 320
    .local v1, "manifestTimestamp":J
    const/4 v5, 0x0

    iput-object v5, p0, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->mFetchManifestRequest:Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchManifestRequest;

    .line 322
    invoke-virtual {p4}, Lcom/samsung/groupcast/misc/requests/Result;->isTimeout()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 323
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    const-string v6, "onFetchManifestRequestComplete"

    const-string v7, "timeout while fetching manifest"

    invoke-static {v5, v6, v7}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;)V

    .line 325
    invoke-direct {p0}, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->showJoinTimeoutDialog()V

    .line 326
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->dismiss()V

    goto :goto_0

    .line 330
    :cond_1
    invoke-virtual {p4}, Lcom/samsung/groupcast/misc/requests/Result;->isError()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 331
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    const-string v6, "onFetchManifestRequestComplete"

    const-string v7, "failure (not timeout) while fetching manifest"

    invoke-static {v5, v6, v7}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;)V

    .line 333
    invoke-direct {p0}, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->showJoinTimeoutDialog()V

    .line 334
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->dismiss()V

    goto :goto_0

    .line 338
    :cond_2
    new-instance v4, Lcom/samsung/groupcast/legacy/gp2/session/storage/StorageAgent;

    invoke-direct {v4, v3}, Lcom/samsung/groupcast/legacy/gp2/session/storage/StorageAgent;-><init>(Ljava/lang/String;)V

    .line 339
    .local v4, "storageAgent":Lcom/samsung/groupcast/legacy/gp2/session/storage/StorageAgent;
    invoke-virtual {v4, v0, v1, v2}, Lcom/samsung/groupcast/legacy/gp2/session/storage/StorageAgent;->createReadManifestRequest(Ljava/lang/String;J)Lcom/samsung/groupcast/legacy/gp2/session/storage/ReadManifestRequest;

    move-result-object v5

    iput-object v5, p0, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->mReadManifestRequest:Lcom/samsung/groupcast/legacy/gp2/session/storage/ReadManifestRequest;

    .line 341
    iget-object v5, p0, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->mReadManifestRequest:Lcom/samsung/groupcast/legacy/gp2/session/storage/ReadManifestRequest;

    new-instance v6, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment$3;

    invoke-direct {v6, p0, p1, p2}, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment$3;-><init>(Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;Ljava/lang/String;)V

    invoke-virtual {v5, v6}, Lcom/samsung/groupcast/legacy/gp2/session/storage/ReadManifestRequest;->addListener(Lcom/samsung/groupcast/misc/requests/RequestListener;)V

    .line 348
    iget-object v5, p0, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->mReadManifestRequest:Lcom/samsung/groupcast/legacy/gp2/session/storage/ReadManifestRequest;

    invoke-virtual {v5}, Lcom/samsung/groupcast/legacy/gp2/session/storage/ReadManifestRequest;->start()V

    goto :goto_0
.end method

.method private onReadManifestRequestComplete(Lcom/samsung/groupcast/legacy/gp2/session/storage/ReadManifestRequest;Lcom/samsung/groupcast/misc/requests/Result;Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;Ljava/lang/String;)V
    .locals 5
    .param p1, "request"    # Lcom/samsung/groupcast/legacy/gp2/session/storage/ReadManifestRequest;
    .param p2, "result"    # Lcom/samsung/groupcast/misc/requests/Result;
    .param p3, "manifest"    # Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;
    .param p4, "summary"    # Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;
    .param p5, "pin"    # Ljava/lang/String;

    .prologue
    .line 353
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->mReadManifestRequest:Lcom/samsung/groupcast/legacy/gp2/session/storage/ReadManifestRequest;

    .line 354
    const-string v1, "---"

    invoke-static {v1}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 356
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->mFinished:Z

    .line 358
    invoke-virtual {p2}, Lcom/samsung/groupcast/misc/requests/Result;->isError()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 359
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-string v2, "onReadManifestRequestComplete"

    const-string v3, "failure while reading manifest"

    invoke-static {v1, v2, v3}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;)V

    .line 360
    invoke-direct {p0}, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->showJoinTimeoutDialog()V

    .line 361
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->dismiss()V

    .line 379
    :goto_0
    return-void

    .line 366
    :cond_0
    invoke-static {p4, p5, p3}, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->createOngoingSession(Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;)Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    move-result-object v0

    .line 367
    .local v0, "session":Lcom/samsung/groupcast/legacy/gp2/session/model/Session;
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-string v2, "onReadManifestComplete"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "manifest retrieved session="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;)V

    .line 369
    invoke-direct {p0}, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->getDelegate()Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment$JoinSessionDialogFragmentDelegate;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 370
    invoke-direct {p0}, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->getDelegate()Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment$JoinSessionDialogFragmentDelegate;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment$JoinSessionDialogFragmentDelegate;->onJoinSessionFinished(Lcom/samsung/groupcast/legacy/gp2/session/model/Session;)V

    .line 374
    :goto_1
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 375
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->dismiss()V

    .line 378
    :cond_1
    iget-object v1, p0, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->searchTimeout:Ljava/lang/Runnable;

    invoke-static {v1}, Lcom/samsung/groupcast/application/MainQueue;->cancel(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 372
    :cond_2
    const-string v1, "No activity waiting for response?! WTF?"

    invoke-static {v1}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    goto :goto_1
.end method

.method private safe_dismiss()V
    .locals 1

    .prologue
    .line 434
    const-string v0, "---"

    invoke-static {v0}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 435
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 436
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 437
    :cond_0
    return-void
.end method

.method private showJoinTimeoutDialog()V
    .locals 4

    .prologue
    .line 388
    const-string v2, "---"

    invoke-static {v2}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 390
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-string v3, "TAG_JOIN_TIMEOUT"

    invoke-virtual {v2, v3}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 405
    :cond_0
    :goto_0
    return-void

    .line 395
    :cond_1
    const v2, 0x7f080015

    invoke-static {v2}, Lcom/samsung/groupcast/application/fragment/MessageDialogFragment;->newInstance(I)Lcom/samsung/groupcast/application/fragment/MessageDialogFragment;

    move-result-object v1

    .line 399
    .local v1, "fragment":Lcom/samsung/groupcast/application/fragment/MessageDialogFragment;
    const/4 v2, 0x0

    :try_start_0
    invoke-virtual {v1, v2}, Lcom/samsung/groupcast/application/fragment/MessageDialogFragment;->setDelegate(Lcom/samsung/groupcast/application/fragment/MessageDialogFragment$MessageDialogFragmentDelegate;)V

    .line 400
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-string v3, "TAG_JOIN_TIMEOUT"

    invoke-virtual {v1, v2, v3}, Lcom/samsung/groupcast/application/fragment/MessageDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 401
    :catch_0
    move-exception v0

    .line 402
    .local v0, "e":Ljava/lang/IllegalStateException;
    const-string v2, "Unable to show timeout dialog :-)"

    invoke-static {v2}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 403
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0
.end method

.method private showPinCodeInputDialog()V
    .locals 3

    .prologue
    .line 194
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->isFinishing()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 212
    :cond_0
    :goto_0
    return-void

    .line 198
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "TAG_PIN_INPUT_DIALOG"

    invoke-virtual {v1, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    if-nez v1, :cond_0

    .line 202
    :cond_2
    const/4 v1, 0x1

    invoke-static {v1}, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->newInstance(I)Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;

    move-result-object v0

    .line 204
    .local v0, "fragment":Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    check-cast v1, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment$PinCodeInputDialogFragmentDelegate;

    invoke-virtual {v0, v1}, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->setDelegate(Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment$PinCodeInputDialogFragmentDelegate;)V

    .line 205
    iget-object v1, p0, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->mSummittingPincode:Ljava/lang/String;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->mSummittingPincode:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 206
    iget-object v1, p0, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->mSummittingPincode:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->setSummittingPincode(Ljava/lang/String;)V

    .line 208
    :cond_3
    iget-boolean v1, p0, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->mIsCheckedShowPasswordCheckBox:Z

    invoke-virtual {v0, v1}, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->setCheckedShowPasswordCheckBox(Z)V

    .line 210
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "TAG_PIN_INPUT_DIALOG"

    invoke-virtual {v0, v1, v2}, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 211
    const/4 v0, 0x0

    .line 212
    goto :goto_0
.end method

.method private showVersionIncompatibleDialog(Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment$VERSION_COLLISION_STATUS;)V
    .locals 5
    .param p1, "versionCollison"    # Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment$VERSION_COLLISION_STATUS;

    .prologue
    .line 408
    const-string v3, "---"

    invoke-static {v3}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 410
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    const-string v4, "TAG_VERSION_NOT_COMPATIBLE"

    invoke-virtual {v3, v4}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 431
    :cond_0
    :goto_0
    return-void

    .line 415
    :cond_1
    const v2, 0x7f080005

    .line 416
    .local v2, "msgId":I
    sget-object v3, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment$VERSION_COLLISION_STATUS;->JOIN_TO_HIGHER_VERSION:Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment$VERSION_COLLISION_STATUS;

    invoke-virtual {p1, v3}, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment$VERSION_COLLISION_STATUS;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 417
    const v2, 0x7f080005

    .line 422
    :cond_2
    :goto_1
    invoke-static {v2}, Lcom/samsung/groupcast/application/fragment/MessageDialogFragment;->newInstance(I)Lcom/samsung/groupcast/application/fragment/MessageDialogFragment;

    move-result-object v1

    .line 425
    .local v1, "fragment":Lcom/samsung/groupcast/application/fragment/MessageDialogFragment;
    const/4 v3, 0x0

    :try_start_0
    invoke-virtual {v1, v3}, Lcom/samsung/groupcast/application/fragment/MessageDialogFragment;->setDelegate(Lcom/samsung/groupcast/application/fragment/MessageDialogFragment$MessageDialogFragmentDelegate;)V

    .line 426
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    const-string v4, "TAG_VERSION_NOT_COMPATIBLE"

    invoke-virtual {v1, v3, v4}, Lcom/samsung/groupcast/application/fragment/MessageDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 427
    :catch_0
    move-exception v0

    .line 428
    .local v0, "e":Ljava/lang/IllegalStateException;
    const-string v3, "Unable to show version incompatible dialog :-)"

    invoke-static {v3}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 429
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0

    .line 418
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    .end local v1    # "fragment":Lcom/samsung/groupcast/application/fragment/MessageDialogFragment;
    :cond_3
    sget-object v3, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment$VERSION_COLLISION_STATUS;->JOIN_TO_LOWER_VERSION:Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment$VERSION_COLLISION_STATUS;

    invoke-virtual {p1, v3}, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment$VERSION_COLLISION_STATUS;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_1
.end method

.method private stopSearch()V
    .locals 2

    .prologue
    .line 233
    const-string v0, "---"

    invoke-static {v0}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 235
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->mScanner:Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner;

    if-eqz v0, :cond_0

    .line 236
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->mScanner:Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner;

    invoke-virtual {v0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner;->disable()V

    .line 237
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->mScanner:Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner;->setDelegate(Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner$SessionScannerDelegate;)V

    .line 239
    :cond_0
    return-void
.end method


# virtual methods
.method public dismiss()V
    .locals 1

    .prologue
    .line 383
    const-string v0, "---"

    invoke-static {v0}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 384
    invoke-direct {p0}, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->safe_dismiss()V

    .line 385
    return-void
.end method

.method public doJoin(Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;Ljava/lang/String;)V
    .locals 2
    .param p1, "summary"    # Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;
    .param p2, "pin"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x1

    .line 242
    const-string v0, "doJoin"

    invoke-static {v0}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 244
    invoke-virtual {p1}, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;->getVersion()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 246
    sget-object v0, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment$VERSION_COLLISION_STATUS;->JOIN_TO_HIGHER_VERSION:Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment$VERSION_COLLISION_STATUS;

    invoke-direct {p0, v0}, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->showVersionIncompatibleDialog(Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment$VERSION_COLLISION_STATUS;)V

    .line 247
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->dismiss()V

    .line 262
    :goto_0
    return-void

    .line 249
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;->getVersion()I

    move-result v0

    if-le v1, v0, :cond_1

    .line 258
    :cond_1
    iput-object p1, p0, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->mJoiningSessionSummary:Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;

    .line 260
    invoke-direct {p0}, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->stopSearch()V

    .line 261
    invoke-virtual {p0, p1, p2}, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->fetchManifestForSession(Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public doJoinAfterPasswordInput(Ljava/lang/String;)V
    .locals 3
    .param p1, "pin"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x1

    .line 444
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->mJoiningSessionSummary:Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;

    if-nez v0, :cond_1

    .line 470
    :cond_0
    :goto_0
    return-void

    .line 447
    :cond_1
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->mJoiningSessionSummary:Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;

    invoke-virtual {v0, p1}, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;->isPinValid(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 448
    invoke-direct {p0}, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->getDelegate()Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment$JoinSessionDialogFragmentDelegate;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 449
    invoke-direct {p0}, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->getDelegate()Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment$JoinSessionDialogFragmentDelegate;

    move-result-object v0

    invoke-interface {v0, v2}, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment$JoinSessionDialogFragmentDelegate;->setPinOK(Z)V

    .line 452
    :cond_2
    invoke-direct {p0}, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->hidePinCodeInputDialog()V

    .line 453
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->mJoiningSessionSummary:Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;

    invoke-virtual {p0, v0, p1}, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->doJoin(Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;Ljava/lang/String;)V

    goto :goto_0

    .line 455
    :cond_3
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f080045

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 456
    invoke-direct {p0}, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->getDelegate()Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment$JoinSessionDialogFragmentDelegate;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 457
    invoke-direct {p0}, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->getDelegate()Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment$JoinSessionDialogFragmentDelegate;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment$JoinSessionDialogFragmentDelegate;->setPinOK(Z)V

    goto :goto_0
.end method

.method public fetchManifestForSession(Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;Ljava/lang/String;)V
    .locals 3
    .param p1, "summary"    # Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;
    .param p2, "pin"    # Ljava/lang/String;

    .prologue
    .line 268
    const-string v0, "---"

    invoke-static {v0}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 270
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 271
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f08001b

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->setText(Ljava/lang/String;)V

    .line 275
    :cond_0
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->searchTimeout:Ljava/lang/Runnable;

    const-wide/16 v1, 0x3a98

    invoke-static {v0, v1, v2}, Lcom/samsung/groupcast/application/MainQueue;->postDelayed(Ljava/lang/Runnable;J)V

    .line 277
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->mManifestFetcher:Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestFetcher;

    invoke-virtual {v0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestFetcher;->enable()V

    .line 279
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->mManifestFetcher:Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestFetcher;

    invoke-virtual {p1}, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;->getSessionId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;->getManifestId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestFetcher;->createFetchManifestRequest(Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchManifestRequest;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->mFetchManifestRequest:Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchManifestRequest;

    .line 282
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->mFetchManifestRequest:Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchManifestRequest;

    new-instance v1, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment$2;

    invoke-direct {v1, p0, p1, p2}, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment$2;-><init>(Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchManifestRequest;->addListener(Lcom/samsung/groupcast/misc/requests/RequestListener;)V

    .line 305
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->mFetchManifestRequest:Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchManifestRequest;

    invoke-virtual {v0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchManifestRequest;->start()V

    .line 306
    return-void
.end method

.method public isNfcAutoConnectionGoingOn()Z
    .locals 1

    .prologue
    .line 473
    iget-boolean v0, p0, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->mNfcAutoConnectionGoingOn:Z

    return v0
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 2
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 109
    invoke-super {p0, p1}, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;->onAttach(Landroid/app/Activity;)V

    .line 111
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f080033

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->setText(Ljava/lang/String;)V

    .line 112
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 85
    invoke-super {p0, p1}, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 86
    const-string v0, "---"

    invoke-static {v0}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 88
    if-eqz p1, :cond_0

    .line 89
    const-string v0, "TAG_JOINING_SESSION_SUMMARY"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;

    iput-object v0, p0, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->mJoiningSessionSummary:Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;

    .line 91
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->mJoiningSessionSummary:Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;

    if-eqz v0, :cond_0

    .line 92
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->mJoiningSessionSummary:Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->doJoin(Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;Ljava/lang/String;)V

    .line 94
    :cond_0
    return-void
.end method

.method public onDiscoveredSessionsChanged(Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner;)V
    .locals 4
    .param p1, "scanner"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner;

    .prologue
    .line 167
    const-string v2, "---"

    invoke-static {v2}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 168
    invoke-virtual {p1}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner;->getDiscoveredSessions()Ljava/util/ArrayList;

    move-result-object v0

    .line 170
    .local v0, "sessions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;>;"
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "sessions size = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 172
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v3, 0x1

    if-ge v2, v3, :cond_0

    .line 191
    :goto_0
    return-void

    .line 175
    :cond_0
    iget-object v2, p0, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->searchTimeout:Ljava/lang/Runnable;

    invoke-static {v2}, Lcom/samsung/groupcast/application/MainQueue;->cancel(Ljava/lang/Runnable;)V

    .line 177
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;

    .line 179
    .local v1, "summary":Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "PIN:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;->isPinProtected()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ",NFC:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->isNfcAutoConnectionGoingOn()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 181
    invoke-virtual {v1}, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;->isPinProtected()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->isNfcAutoConnectionGoingOn()Z

    move-result v2

    if-nez v2, :cond_1

    .line 184
    invoke-direct {p0}, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->showPinCodeInputDialog()V

    .line 185
    iput-object v1, p0, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->mJoiningSessionSummary:Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;

    .line 186
    invoke-direct {p0}, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->stopSearch()V

    goto :goto_0

    .line 189
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f080034

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->setText(Ljava/lang/String;)V

    .line 190
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->isNfcAutoConnectionGoingOn()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->mPinFromBand:Ljava/lang/String;

    :goto_1
    invoke-virtual {p0, v1, v2}, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->doJoin(Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    .line 148
    const-string v0, "---"

    invoke-static {v0}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 150
    iget-boolean v0, p0, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->mFinished:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->getDelegate()Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment$JoinSessionDialogFragmentDelegate;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 151
    invoke-direct {p0}, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->getDelegate()Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment$JoinSessionDialogFragmentDelegate;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment$JoinSessionDialogFragmentDelegate;->onJoinSessionFinished(Lcom/samsung/groupcast/legacy/gp2/session/model/Session;)V

    .line 153
    :cond_0
    invoke-direct {p0}, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->safe_dismiss()V

    .line 154
    return-void
.end method

.method public onMessageDialogDismissed(Lcom/samsung/groupcast/application/fragment/MessageDialogFragment;Z)V
    .locals 0
    .param p1, "fragment"    # Lcom/samsung/groupcast/application/fragment/MessageDialogFragment;
    .param p2, "cancelled"    # Z

    .prologue
    .line 440
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 116
    invoke-super {p0}, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;->onPause()V

    .line 118
    invoke-direct {p0}, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->hidePinCodeInputDialog()V

    .line 120
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->mManifestFetcher:Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestFetcher;

    invoke-virtual {v0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestFetcher;->disable()V

    .line 121
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->mScanner:Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner;

    invoke-virtual {v0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner;->disable()V

    .line 122
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->mScanner:Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner;->setDelegate(Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner$SessionScannerDelegate;)V

    .line 123
    return-void
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 127
    invoke-super {p0}, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;->onResume()V

    .line 129
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->mManifestFetcher:Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestFetcher;

    invoke-virtual {v0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestFetcher;->enable()V

    .line 130
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->mScanner:Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner;

    invoke-virtual {v0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner;->enable()V

    .line 131
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->mScanner:Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner;

    invoke-virtual {v0, p0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner;->setDelegate(Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner$SessionScannerDelegate;)V

    .line 133
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->searchTimeout:Ljava/lang/Runnable;

    const-wide/16 v1, 0x3a98

    invoke-static {v0, v1, v2}, Lcom/samsung/groupcast/application/MainQueue;->postDelayed(Ljava/lang/Runnable;J)V

    .line 134
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 100
    invoke-super {p0, p1}, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 102
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->mJoiningSessionSummary:Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->isChangingConfigurations()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 103
    const-string v0, "TAG_JOINING_SESSION_SUMMARY"

    iget-object v1, p0, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->mJoiningSessionSummary:Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 105
    :cond_0
    return-void
.end method

.method public setCheckedShowPasswordCheckBox(Z)V
    .locals 0
    .param p1, "isCheckedShowPasswordCheckBox"    # Z

    .prologue
    .line 486
    iput-boolean p1, p0, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->mIsCheckedShowPasswordCheckBox:Z

    .line 487
    return-void
.end method

.method public setNfcAutoConnectionInfo(ZLjava/lang/String;)V
    .locals 0
    .param p1, "mNfcAutoConnectionGoingOn"    # Z
    .param p2, "pinFromBand"    # Ljava/lang/String;

    .prologue
    .line 477
    iput-boolean p1, p0, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->mNfcAutoConnectionGoingOn:Z

    .line 478
    iput-object p2, p0, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->mPinFromBand:Ljava/lang/String;

    .line 479
    return-void
.end method

.method public setSummittingPincode(Ljava/lang/String;)V
    .locals 0
    .param p1, "summittingPincode"    # Ljava/lang/String;

    .prologue
    .line 482
    iput-object p1, p0, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->mSummittingPincode:Ljava/lang/String;

    .line 483
    return-void
.end method
