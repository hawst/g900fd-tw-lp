.class public Lcom/samsung/groupcast/application/viewer/ViewerActivity;
.super Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;
.source "ViewerActivity.java"


# static fields
.field private static final ACTION_BAR_STATE_DEFAULT:I = 0x0

.field private static final ACTION_BAR_STATE_DRAWMODE_ENABLE:I = 0x1

.field private static final ACTION_BAR_STATE_ERASER_ENABLE:I = 0x3

.field private static final ACTION_BAR_THUMNAIL_STRIP_AUTO_HIDE_DELAYED_DURATION:I = 0xfa0

.field public static final BRUSH_SIZE:Ljava/lang/String; = "BRUSH_SIZE"

.field private static final CAMERA_PHOTO_PATH:Ljava/lang/String; = "CameraPhotoPath"

.field private static final CONFIRMATION_DIALOG:Ljava/lang/String; = "RemoveConfirmationDialog"

.field public static final CONTENTS_RESET_CONFIRM_FRAGMENT:Ljava/lang/String; = "CONTENTS_RESET_CONFIRM_FRAGMENT"

.field public static final CUSTOM_COLOR:Ljava/lang/String; = "CUSTOM_COLOR"

.field public static final CUSTOM_COLOR_VALID:Ljava/lang/String; = "CUSTOM_COLOR_VALID"

.field private static final DELETE_ITEM_ID:Ljava/lang/String; = "DeleteItemId"

.field public static final DOCUMENTS_COLLECTION_NAME:Ljava/lang/String; = "MyDocumentPages"

.field private static final DOC_TOO_LARGE_VIEWER_TAG:Ljava/lang/String; = "DOC_TOO_LARGE_VIEWER_TAG"

.field public static final ERASER_MODE:Ljava/lang/String; = "ERASER_MODE"

.field public static final ERASER_SIZE:Ljava/lang/String; = "ERASER_SIZE"

.field private static final EXTRA_COLLECTION_NAME_ID:Ljava/lang/String; = "COLLECTION_NAME_ID"

.field private static final EXTRA_CONTENT_BROKER_ID:Ljava/lang/String; = "CONTENT_BROKER_ID"

.field private static final EXTRA_CONTENT_MAP_STORE_ID:Ljava/lang/String; = "CONTENT_MAP_STORE_ID"

.field private static final EXTRA_FORCING_TASK_REMOVAL:Ljava/lang/String; = "FORCING_TASK_REMOVAL"

.field private static final EXTRA_SESSION_STORE_ID:Ljava/lang/String; = "SESSION_STORE_ID"

.field private static final EXTRA_SHARED_EXPERIENCE_MANAGER_ID:Ljava/lang/String; = "SHARED_EXPERIENCE_MANAGER_ID"

.field public static final FROM_EXCTERNAL_APP:Ljava/lang/String; = "FROM_EXCTERNAL_APP"

.field public static final IMAGE_COLLECTION_NAME:Ljava/lang/String; = "MyImages"

.field public static JPEG_SAVE_QUALITY:I = 0x0

.field private static final KEY_COLLECTION_NAME_ID:Ljava/lang/String; = "COLLECTION_NAME_STORE_ID"

.field private static final KEY_DRAWING_MODE:Ljava/lang/String; = "DRAWING_MODE_KEY"

.field private static final KEY_ERASER_SETTINGS:Ljava/lang/String; = "ERASER_SETTINGS_KEY"

.field private static final KEY_PEN_SETTINGS:Ljava/lang/String; = "PEN_SETTINGS_KEY"

.field private static final KEY_SAVING_LIST_STORE_ID:Ljava/lang/String; = "SAVING_LIST_STORE_ID"

.field private static final KEY_THUMBNAILS_STATE:Ljava/lang/String; = "THUMBNAILS_STATE_KEY"

.field public static final MAIN_COLLECTION_NAME:Ljava/lang/String; = "MyAllFiles"

.field public static final PEN_COLOR:Ljava/lang/String; = "PEN_COLOR"

.field public static final PHOTO_COLLECTION_NAME:Ljava/lang/String; = "MyPhoto"

.field private static final POPUP_X_POSITION:I = 0x0

.field private static final POPUP_Y_POSITION:I = 0x0

.field public static final PREFS_NAME:Ljava/lang/String; = "MyPrefs"

.field private static final TAG:Ljava/lang/String; = "Viewer"

.field private static final TAG_ADD_FILES_FRAGMENTDIALOG:Ljava/lang/String; = "ADD_FILES_FRAGMENTDIALOG"

.field private static final TAG_PROGRESS_DIALOG_FRAGMENT:Ljava/lang/String; = "PROGRESS_DIALOG_FRAGMENT"

.field protected static final TAG_SESSION_CRASH_DIALOG_FRAGMENT:Ljava/lang/String; = "SESSION_CRASH_DIALOG_FRAGMENT"

.field private static final TAG_SESSION_NO_LONGER_AVAILABLE:Ljava/lang/String; = "SESSION_NO_LONGER_AVAILABLE"

.field private static final TAG_THUMBNAILS_STRIP:Ljava/lang/String; = "THUMBNAILS_STRIP"

.field public static final UNKNOWN:I = 0x0

.field private static final VIEW_PAGER_MARGIN_DIPS:F = 50.0f

.field private static final VIEW_PAGER_OFFSCREEN_PAGE_LIMIT:I = 0x2


# instance fields
.field public isConfirmedContentsReset:Z

.field private mExistMoreMenu:Z

.field public mPageIndexShowing:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const/16 v0, 0x64

    sput v0, Lcom/samsung/groupcast/application/viewer/ViewerActivity;->JPEG_SAVE_QUALITY:I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 4
    invoke-direct {p0}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;-><init>()V

    .line 41
    iput-boolean v1, p0, Lcom/samsung/groupcast/application/viewer/ViewerActivity;->isConfirmedContentsReset:Z

    .line 42
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/groupcast/application/viewer/ViewerActivity;->mPageIndexShowing:Z

    .line 43
    iput-boolean v1, p0, Lcom/samsung/groupcast/application/viewer/ViewerActivity;->mExistMoreMenu:Z

    return-void
.end method


# virtual methods
.method public onPeerChanged()V
    .locals 0

    .prologue
    .line 67
    return-void
.end method

.method public onViewerPeerChanged()V
    .locals 0

    .prologue
    .line 73
    return-void
.end method
