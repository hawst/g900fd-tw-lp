.class public Lcom/samsung/groupcast/application/start/StartActivityForExternalGame;
.super Landroid/app/Activity;
.source "StartActivityForExternalGame.java"


# static fields
.field public static final IS_START_FROM_EXTERNAL:Ljava/lang/String; = "IsStartFromExternal"

.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    const-class v0, Lcom/samsung/groupcast/application/start/StartActivityForExternalGame;

    invoke-static {v0}, Lcom/samsung/groupcast/application/Logger;->getTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/groupcast/application/start/StartActivityForExternalGame;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method private startStartActivityForGame()V
    .locals 4

    .prologue
    .line 32
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivityForExternalGame;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 33
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivityForExternalGame;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/samsung/groupcast/application/start/StartActivityForGame;

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 34
    invoke-static {}, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->HasSession()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 35
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivityForExternalGame;->finish()V

    .line 51
    :goto_0
    return-void

    .line 39
    :cond_0
    sget-object v1, Lcom/samsung/groupcast/application/start/StartActivity;->start_Activity:Landroid/app/Activity;

    check-cast v1, Lcom/samsung/groupcast/application/start/StartActivity;

    .line 40
    .local v1, "startToFinish":Lcom/samsung/groupcast/application/start/StartActivity;
    if-eqz v1, :cond_1

    .line 41
    invoke-virtual {v1}, Lcom/samsung/groupcast/application/start/StartActivity;->finish()V

    .line 44
    :cond_1
    const/high16 v2, 0x4000000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 45
    const/high16 v2, 0x40000000    # 2.0f

    invoke-virtual {v0, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 46
    const v2, 0x8000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 48
    const-string v2, "IsStartFromExternal"

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 49
    invoke-virtual {p0, v0}, Lcom/samsung/groupcast/application/start/StartActivityForExternalGame;->startActivity(Landroid/content/Intent;)V

    .line 50
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivityForExternalGame;->finish()V

    goto :goto_0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 19
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 20
    sget-object v0, Lcom/samsung/groupcast/application/start/StartActivityForExternalGame;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[onCreate] version:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/samsung/groupcast/application/App;->getVersionName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/groupcast/application/Logger;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 25
    invoke-direct {p0}, Lcom/samsung/groupcast/application/start/StartActivityForExternalGame;->startStartActivityForGame()V

    .line 29
    return-void
.end method
