.class Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment$2;
.super Ljava/lang/Object;
.source "DisclaimerDialogFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;


# direct methods
.method constructor <init>(Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;)V
    .locals 0

    .prologue
    .line 295
    iput-object p1, p0, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment$2;->this$0:Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 298
    iget-object v0, p0, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment$2;->this$0:Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;

    # getter for: Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->mCheckBox:Landroid/widget/CheckBox;
    invoke-static {v0}, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->access$100(Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;)Landroid/widget/CheckBox;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 299
    iget-object v0, p0, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment$2;->this$0:Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;

    # getter for: Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->mCheckBox:Landroid/widget/CheckBox;
    invoke-static {v0}, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->access$100(Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;)Landroid/widget/CheckBox;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 301
    :cond_0
    iget-object v0, p0, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment$2;->this$0:Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;

    # invokes: Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->getDelegate()Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment$Delegate;
    invoke-static {v0}, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->access$200(Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;)Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment$Delegate;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 302
    iget-object v0, p0, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment$2;->this$0:Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;

    # invokes: Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->getDelegate()Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment$Delegate;
    invoke-static {v0}, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->access$200(Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;)Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment$Delegate;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment$2;->this$0:Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment$Delegate;->onDisclaimerDialogDismissed(Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;Z)V

    .line 305
    :cond_1
    return-void
.end method
