.class public Lcom/samsung/groupcast/application/fragment/FlightModeDialogFragment;
.super Landroid/app/DialogFragment;
.source "FlightModeDialogFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/groupcast/application/fragment/FlightModeDialogFragment$FlightModeDialogFragmentDelegate;
    }
.end annotation


# static fields
.field private static final KEY_TEXT_RESOURCE_ID:Ljava/lang/String; = "TEXT_RESOURCE_ID"

.field private static final KEY_TYPE_OF_BTN:Ljava/lang/String; = "BTN_TYPE"

.field public static final TYPE_CREATE_GROUP:I = 0x0

.field public static final TYPE_JOIN_GROUP:I = 0x1

.field public static final TYPE_PINCODE_DIALOG_CREATE:I = 0x2


# instance fields
.field private mFlightModeDialogFragmentDelegate:Lcom/samsung/groupcast/application/fragment/FlightModeDialogFragment$FlightModeDialogFragmentDelegate;

.field private mTextResourceId:I

.field private mType:I

.field private mWasAgreed:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 16
    return-void
.end method

.method static synthetic access$002(Lcom/samsung/groupcast/application/fragment/FlightModeDialogFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/groupcast/application/fragment/FlightModeDialogFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 14
    iput-boolean p1, p0, Lcom/samsung/groupcast/application/fragment/FlightModeDialogFragment;->mWasAgreed:Z

    return p1
.end method

.method private getDelegate()Lcom/samsung/groupcast/application/fragment/FlightModeDialogFragment$FlightModeDialogFragmentDelegate;
    .locals 2

    .prologue
    .line 109
    iget-object v0, p0, Lcom/samsung/groupcast/application/fragment/FlightModeDialogFragment;->mFlightModeDialogFragmentDelegate:Lcom/samsung/groupcast/application/fragment/FlightModeDialogFragment$FlightModeDialogFragmentDelegate;

    if-eqz v0, :cond_0

    .line 110
    iget-object v0, p0, Lcom/samsung/groupcast/application/fragment/FlightModeDialogFragment;->mFlightModeDialogFragmentDelegate:Lcom/samsung/groupcast/application/fragment/FlightModeDialogFragment$FlightModeDialogFragmentDelegate;

    .line 117
    :goto_0
    return-object v0

    .line 113
    :cond_0
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/fragment/FlightModeDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-class v1, Lcom/samsung/groupcast/application/fragment/FlightModeDialogFragment$FlightModeDialogFragmentDelegate;

    invoke-static {v0, v1}, Lcom/samsung/groupcast/misc/utility/Verify;->instanceOf(Ljava/lang/Object;Ljava/lang/Class;)V

    .line 114
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/fragment/FlightModeDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/samsung/groupcast/application/fragment/FlightModeDialogFragment$FlightModeDialogFragmentDelegate;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 115
    :catch_0
    move-exception v0

    .line 117
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static newInstance(II)Lcom/samsung/groupcast/application/fragment/FlightModeDialogFragment;
    .locals 3
    .param p0, "textResourceId"    # I
    .param p1, "type"    # I

    .prologue
    .line 34
    new-instance v1, Lcom/samsung/groupcast/application/fragment/FlightModeDialogFragment;

    invoke-direct {v1}, Lcom/samsung/groupcast/application/fragment/FlightModeDialogFragment;-><init>()V

    .line 35
    .local v1, "fragment":Lcom/samsung/groupcast/application/fragment/FlightModeDialogFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 36
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "TEXT_RESOURCE_ID"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 37
    const-string v2, "BTN_TYPE"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 38
    invoke-virtual {v1, v0}, Lcom/samsung/groupcast/application/fragment/FlightModeDialogFragment;->setArguments(Landroid/os/Bundle;)V

    .line 39
    return-object v1
.end method


# virtual methods
.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 3
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    const/4 v2, 0x0

    .line 81
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onCancel(Landroid/content/DialogInterface;)V

    .line 82
    iput-boolean v2, p0, Lcom/samsung/groupcast/application/fragment/FlightModeDialogFragment;->mWasAgreed:Z

    .line 83
    invoke-direct {p0}, Lcom/samsung/groupcast/application/fragment/FlightModeDialogFragment;->getDelegate()Lcom/samsung/groupcast/application/fragment/FlightModeDialogFragment$FlightModeDialogFragmentDelegate;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 84
    invoke-direct {p0}, Lcom/samsung/groupcast/application/fragment/FlightModeDialogFragment;->getDelegate()Lcom/samsung/groupcast/application/fragment/FlightModeDialogFragment$FlightModeDialogFragmentDelegate;

    move-result-object v0

    iget v1, p0, Lcom/samsung/groupcast/application/fragment/FlightModeDialogFragment;->mType:I

    invoke-interface {v0, p0, v2, v1}, Lcom/samsung/groupcast/application/fragment/FlightModeDialogFragment$FlightModeDialogFragmentDelegate;->onFlightModeDialogDismissed(Lcom/samsung/groupcast/application/fragment/FlightModeDialogFragment;ZI)V

    .line 85
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 44
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 45
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/fragment/FlightModeDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "TEXT_RESOURCE_ID"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/samsung/groupcast/application/fragment/FlightModeDialogFragment;->mTextResourceId:I

    .line 46
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/fragment/FlightModeDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "BTN_TYPE"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/samsung/groupcast/application/fragment/FlightModeDialogFragment;->mType:I

    .line 47
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 51
    const/4 v0, 0x0

    .line 52
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    new-instance v0, Landroid/app/AlertDialog$Builder;

    .end local v0    # "builder":Landroid/app/AlertDialog$Builder;
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/fragment/FlightModeDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 54
    .restart local v0    # "builder":Landroid/app/AlertDialog$Builder;
    iget v2, p0, Lcom/samsung/groupcast/application/fragment/FlightModeDialogFragment;->mType:I

    if-nez v2, :cond_1

    .line 55
    const v2, 0x7f080029

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 59
    :cond_0
    :goto_0
    iget v2, p0, Lcom/samsung/groupcast/application/fragment/FlightModeDialogFragment;->mTextResourceId:I

    invoke-static {v2}, Lcom/samsung/groupcast/application/App;->getSanitizedString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 60
    const v2, 0x7f080010

    new-instance v3, Lcom/samsung/groupcast/application/fragment/FlightModeDialogFragment$1;

    invoke-direct {v3, p0}, Lcom/samsung/groupcast/application/fragment/FlightModeDialogFragment$1;-><init>(Lcom/samsung/groupcast/application/fragment/FlightModeDialogFragment;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 67
    const v2, 0x7f080011

    new-instance v3, Lcom/samsung/groupcast/application/fragment/FlightModeDialogFragment$2;

    invoke-direct {v3, p0}, Lcom/samsung/groupcast/application/fragment/FlightModeDialogFragment$2;-><init>(Lcom/samsung/groupcast/application/fragment/FlightModeDialogFragment;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 74
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    .line 75
    .local v1, "dialog":Landroid/app/AlertDialog;
    return-object v1

    .line 56
    .end local v1    # "dialog":Landroid/app/AlertDialog;
    :cond_1
    iget v2, p0, Lcom/samsung/groupcast/application/fragment/FlightModeDialogFragment;->mType:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 57
    const v2, 0x7f08002a

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    goto :goto_0
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 3
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    .line 90
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onDismiss(Landroid/content/DialogInterface;)V

    .line 92
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/fragment/FlightModeDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_1

    .line 102
    :cond_0
    :goto_0
    return-void

    .line 96
    :cond_1
    iget-boolean v0, p0, Lcom/samsung/groupcast/application/fragment/FlightModeDialogFragment;->mWasAgreed:Z

    if-nez v0, :cond_2

    invoke-direct {p0}, Lcom/samsung/groupcast/application/fragment/FlightModeDialogFragment;->getDelegate()Lcom/samsung/groupcast/application/fragment/FlightModeDialogFragment$FlightModeDialogFragmentDelegate;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 97
    invoke-direct {p0}, Lcom/samsung/groupcast/application/fragment/FlightModeDialogFragment;->getDelegate()Lcom/samsung/groupcast/application/fragment/FlightModeDialogFragment$FlightModeDialogFragmentDelegate;

    move-result-object v0

    const/4 v1, 0x0

    iget v2, p0, Lcom/samsung/groupcast/application/fragment/FlightModeDialogFragment;->mType:I

    invoke-interface {v0, p0, v1, v2}, Lcom/samsung/groupcast/application/fragment/FlightModeDialogFragment$FlightModeDialogFragmentDelegate;->onFlightModeDialogDismissed(Lcom/samsung/groupcast/application/fragment/FlightModeDialogFragment;ZI)V

    goto :goto_0

    .line 99
    :cond_2
    iget-boolean v0, p0, Lcom/samsung/groupcast/application/fragment/FlightModeDialogFragment;->mWasAgreed:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/samsung/groupcast/application/fragment/FlightModeDialogFragment;->getDelegate()Lcom/samsung/groupcast/application/fragment/FlightModeDialogFragment$FlightModeDialogFragmentDelegate;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 100
    invoke-direct {p0}, Lcom/samsung/groupcast/application/fragment/FlightModeDialogFragment;->getDelegate()Lcom/samsung/groupcast/application/fragment/FlightModeDialogFragment$FlightModeDialogFragmentDelegate;

    move-result-object v0

    const/4 v1, 0x1

    iget v2, p0, Lcom/samsung/groupcast/application/fragment/FlightModeDialogFragment;->mType:I

    invoke-interface {v0, p0, v1, v2}, Lcom/samsung/groupcast/application/fragment/FlightModeDialogFragment$FlightModeDialogFragmentDelegate;->onFlightModeDialogDismissed(Lcom/samsung/groupcast/application/fragment/FlightModeDialogFragment;ZI)V

    goto :goto_0
.end method

.method public setDelegate(Lcom/samsung/groupcast/application/fragment/FlightModeDialogFragment$FlightModeDialogFragmentDelegate;)V
    .locals 0
    .param p1, "delegate"    # Lcom/samsung/groupcast/application/fragment/FlightModeDialogFragment$FlightModeDialogFragmentDelegate;

    .prologue
    .line 105
    iput-object p1, p0, Lcom/samsung/groupcast/application/fragment/FlightModeDialogFragment;->mFlightModeDialogFragmentDelegate:Lcom/samsung/groupcast/application/fragment/FlightModeDialogFragment$FlightModeDialogFragmentDelegate;

    .line 106
    return-void
.end method
