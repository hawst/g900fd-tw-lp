.class public Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;
.super Landroid/app/DialogFragment;
.source "DisclaimerDialogFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment$Delegate;
    }
.end annotation


# static fields
.field private static final DISCLAIMER_DIALOG_FRAGMENT_TAG:Ljava/lang/String; = "DisclaimerDialogFragment"

.field private static final KEY_DISCLAIMER_MESSAGE:Ljava/lang/String; = "DISCLAIMER_MESSAGE"

.field private static final KEY_DISCLAIMER_TITLE:Ljava/lang/String; = "DISCLAIMER_TITLE"

.field private static final KEY_DISCLAIMER_TYPE:Ljava/lang/String; = "DISCLAIMER_TYPE"

.field private static final KEY_DISCLAIMER_VIEW:Ljava/lang/String; = "DISCLAIMER_VIEW"

.field public static final KEY_SKIP_TYPE:[Ljava/lang/String;

.field public static final TYPE_CANT_ACCESS_INTERNET:I = 0x5

.field public static final TYPE_CHN_CONNECT_TO_WLAN:I = 0x7

.field public static final TYPE_CHN_CONNECT_VIA_MOBILE_NETWORKS:I = 0x8

.field public static final TYPE_DATA_IS_COLLECTED_BY_SERVER:I = 0x6

.field public static final TYPE_DEVICE_DOESNT_SUPPORT_NFC:I = 0x4

.field public static final TYPE_HOTSPOT_EXPENDS_MORE_BATTERY:I = 0x2

.field public static final TYPE_HOW_TO_CONNECT_USING_NFC:I = 0x3

.field public static final TYPE_LEGAL_RESONSIBILITY:I = 0x1

.field public static final TYPE_SCURITY_ADVICE:I


# instance fields
.field private mCheckBox:Landroid/widget/CheckBox;

.field private mCheckBoxText:Ljava/lang/String;

.field private mMessage:Ljava/lang/String;

.field private mSeveralDays:I

.field private mTitle:Ljava/lang/String;

.field private mType:I

.field private mView:Landroid/view/View;

.field private mViewID:I

.field private mWasCancelled:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 57
    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "SKIP_SCURITY_ADVICE_DISCLAIMER"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "SKIP_LEGAL_RESONSIBILITY_DISCLAIMER"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "SKIP_HOTSPOT_EXPENDS_MORE_BATTERY_DISCLAIMER"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "SKIP_HOW_TO_CONNECT_USING_NFC_DISCLAIMER"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "SKIP_DEVICE_DOESNT_SUPPORT_NFC"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "SKIP_CANT_ACCESS_INTERNET"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "SKIP_DATA_IS_COLLECTED_BY_SERVER"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "SKIP__CHN_CONNECT_TO_WLAN"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "SKIP_CHN_CONNECT_VIA_MOBILE_NETWORKS"

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->KEY_SKIP_TYPE:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 156
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->mViewID:I

    .line 157
    const/16 v0, 0x3e7

    iput v0, p0, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->mType:I

    .line 158
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->mSeveralDays:I

    return-void
.end method

.method static synthetic access$000(Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->onCheckBoxCheckChanged()V

    return-void
.end method

.method static synthetic access$100(Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;)Landroid/widget/CheckBox;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->mCheckBox:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;)Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment$Delegate;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->getDelegate()Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment$Delegate;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;

    .prologue
    .line 29
    iget v0, p0, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->mType:I

    return v0
.end method

.method static synthetic access$402(Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;
    .param p1, "x1"    # I

    .prologue
    .line 29
    iput p1, p0, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->mSeveralDays:I

    return p1
.end method

.method public static clearConfirms()V
    .locals 1

    .prologue
    .line 147
    const/4 v0, -0x1

    invoke-static {v0}, Lcom/samsung/groupcast/application/Preferences;->setDisclaimerConfirmed(I)V

    .line 148
    return-void
.end method

.method private getDelegate()Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment$Delegate;
    .locals 1

    .prologue
    .line 465
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment$Delegate;

    return-object v0
.end method

.method private getKey(I)Ljava/lang/String;
    .locals 1
    .param p1, "type"    # I

    .prologue
    .line 511
    sget-object v0, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->KEY_SKIP_TYPE:[Ljava/lang/String;

    aget-object v0, v0, p1

    return-object v0
.end method

.method private isMandatoryInformation()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 526
    iget v1, p0, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->mType:I

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->mType:I

    if-ne v1, v0, :cond_1

    .line 529
    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static newInstance()Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;
    .locals 2

    .prologue
    .line 164
    new-instance v1, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;

    invoke-direct {v1}, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;-><init>()V

    .line 165
    .local v1, "dialog":Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 166
    .local v0, "args":Landroid/os/Bundle;
    invoke-virtual {v1, v0}, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->setArguments(Landroid/os/Bundle;)V

    .line 167
    return-object v1
.end method

.method private onCheckBoxCheckChanged()V
    .locals 2

    .prologue
    .line 469
    iget-object v0, p0, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->mCheckBox:Landroid/widget/CheckBox;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->playSoundEffect(I)V

    .line 470
    return-void
.end method

.method private sanitizeViews()V
    .locals 3

    .prologue
    .line 473
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v1

    if-nez v1, :cond_1

    .line 483
    :cond_0
    :goto_0
    return-void

    .line 477
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v1

    const v2, 0x7f07000b

    invoke-virtual {v1, v2}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 479
    .local v0, "textView":Landroid/widget/TextView;
    if-eqz v0, :cond_0

    .line 480
    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/groupcast/application/App;->getSanitizedString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 481
    const-string v1, "ROBOTO_LIGHT"

    invoke-static {v0, v1}, Lcom/samsung/groupcast/misc/utility/StringTools;->setTypeFace(Landroid/widget/TextView;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static showIfNeeded(Landroid/app/Activity;I)Z
    .locals 7
    .param p0, "activity"    # Landroid/app/Activity;
    .param p1, "type"    # I

    .prologue
    const v6, 0x7f080028

    const v5, 0x7f080027

    const/4 v1, 0x1

    const v4, 0x7f08000c

    .line 69
    sget-object v2, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->KEY_SKIP_TYPE:[Ljava/lang/String;

    aget-object v2, v2, p1

    invoke-static {v2}, Lcom/samsung/groupcast/application/Preferences;->getSkipDisclaimerNotificationPreferenceDynamically(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {p1}, Lcom/samsung/groupcast/application/Preferences;->getDisclaimerConfirmed(I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 71
    :cond_0
    const/4 v1, 0x0

    .line 143
    :cond_1
    :goto_0
    return v1

    .line 75
    :cond_2
    invoke-virtual {p0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-string v3, "DisclaimerDialogFragment"

    invoke-virtual {v2, v3}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v2

    if-nez v2, :cond_1

    .line 77
    invoke-static {}, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->newInstance()Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;

    move-result-object v0

    .line 80
    .local v0, "fragment":Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;
    packed-switch p1, :pswitch_data_0

    .line 139
    :goto_1
    invoke-virtual {v0, p1}, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->setType(I)V

    .line 140
    invoke-virtual {p0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-string v3, "DisclaimerDialogFragment"

    invoke-virtual {v0, v2, v3}, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    .line 82
    :pswitch_0
    const v2, 0x7f08000b

    invoke-static {v2}, Lcom/samsung/groupcast/application/App;->getSanitizedString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->setMessage(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 85
    :pswitch_1
    invoke-virtual {p0, v5}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->setTitle(Ljava/lang/CharSequence;)V

    .line 87
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const v3, 0x7f08007b

    invoke-static {v3}, Lcom/samsung/groupcast/application/App;->getSanitizedString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const v3, 0x7f08007c

    invoke-static {v3}, Lcom/samsung/groupcast/application/App;->getSanitizedString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const v3, 0x7f08007d

    invoke-static {v3}, Lcom/samsung/groupcast/application/App;->getSanitizedString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const v3, 0x7f08007e

    invoke-static {v3}, Lcom/samsung/groupcast/application/App;->getSanitizedString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const v3, 0x7f08007f

    invoke-static {v3}, Lcom/samsung/groupcast/application/App;->getSanitizedString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const v3, 0x7f080080

    invoke-static {v3}, Lcom/samsung/groupcast/application/App;->getSanitizedString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const v3, 0x7f080081

    invoke-static {v3}, Lcom/samsung/groupcast/application/App;->getSanitizedString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const v3, 0x7f080082

    invoke-static {v3}, Lcom/samsung/groupcast/application/App;->getSanitizedString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const v3, 0x7f080083

    invoke-static {v3}, Lcom/samsung/groupcast/application/App;->getSanitizedString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const v3, 0x7f080084

    invoke-static {v3}, Lcom/samsung/groupcast/application/App;->getSanitizedString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const v3, 0x7f080085

    invoke-static {v3}, Lcom/samsung/groupcast/application/App;->getSanitizedString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const v3, 0x7f080086

    invoke-static {v3}, Lcom/samsung/groupcast/application/App;->getSanitizedString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const v3, 0x7f080087

    invoke-static {v3}, Lcom/samsung/groupcast/application/App;->getSanitizedString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const v3, 0x7f080088

    invoke-static {v3}, Lcom/samsung/groupcast/application/App;->getSanitizedString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const v3, 0x7f080089

    invoke-static {v3}, Lcom/samsung/groupcast/application/App;->getSanitizedString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->setMessage(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 108
    :pswitch_2
    invoke-static {v6}, Lcom/samsung/groupcast/application/App;->getSanitizedString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->setTitle(Ljava/lang/CharSequence;)V

    .line 109
    const v2, 0x7f080030

    invoke-virtual {p0, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->setMessage(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 112
    :pswitch_3
    const v2, 0x7f08002b

    invoke-static {v2}, Lcom/samsung/groupcast/application/App;->getSanitizedString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->setMessage(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 115
    :pswitch_4
    const v2, 0x7f080036

    invoke-virtual {p0, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->setMessage(Ljava/lang/CharSequence;)V

    .line 116
    invoke-virtual {p0, v4}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->setCheckBoxText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 119
    :pswitch_5
    invoke-static {v6}, Lcom/samsung/groupcast/application/App;->getSanitizedString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->setTitle(Ljava/lang/CharSequence;)V

    .line 120
    const v2, 0x7f040004

    invoke-virtual {v0, v2}, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->setCustomViewId(I)V

    goto/16 :goto_1

    .line 123
    :pswitch_6
    invoke-virtual {p0, v5}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->setTitle(Ljava/lang/CharSequence;)V

    .line 124
    const v2, 0x7f0800b9

    invoke-static {v2}, Lcom/samsung/groupcast/application/App;->getSanitizedString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->setMessage(Ljava/lang/CharSequence;)V

    .line 125
    invoke-virtual {p0, v4}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->setCheckBoxText(Ljava/lang/CharSequence;)V

    .line 127
    :pswitch_7
    const v2, 0x7f080068

    invoke-virtual {p0, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->setTitle(Ljava/lang/CharSequence;)V

    .line 128
    const v2, 0x7f080069

    invoke-static {v2}, Lcom/samsung/groupcast/application/App;->getSanitizedString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->setMessage(Ljava/lang/CharSequence;)V

    .line 129
    invoke-virtual {p0, v4}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->setCheckBoxText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 132
    :pswitch_8
    const v2, 0x7f080065

    invoke-virtual {p0, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->setTitle(Ljava/lang/CharSequence;)V

    .line 133
    const v2, 0x7f080066

    invoke-static {v2}, Lcom/samsung/groupcast/application/App;->getSanitizedString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->setMessage(Ljava/lang/CharSequence;)V

    .line 134
    invoke-virtual {p0, v4}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->setCheckBoxText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 80
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_5
        :pswitch_2
        :pswitch_4
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method


# virtual methods
.method public addMessage(Ljava/lang/CharSequence;)V
    .locals 2
    .param p1, "text"    # Ljava/lang/CharSequence;

    .prologue
    .line 515
    iget-object v0, p0, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->mMessage:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 519
    .end local p1    # "text":Ljava/lang/CharSequence;
    :goto_0
    return-void

    .line 518
    .restart local p1    # "text":Ljava/lang/CharSequence;
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->mMessage:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\r\n\r\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    check-cast p1, Ljava/lang/String;

    .end local p1    # "text":Ljava/lang/CharSequence;
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->mMessage:Ljava/lang/String;

    goto :goto_0
.end method

.method public getType()I
    .locals 1

    .prologue
    .line 533
    iget v0, p0, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->mType:I

    return v0
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    const/4 v1, 0x1

    .line 434
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onCancel(Landroid/content/DialogInterface;)V

    .line 435
    iput-boolean v1, p0, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->mWasCancelled:Z

    .line 440
    invoke-direct {p0}, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->getDelegate()Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment$Delegate;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 441
    invoke-direct {p0}, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->isMandatoryInformation()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 442
    invoke-direct {p0}, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->getDelegate()Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment$Delegate;

    move-result-object v0

    invoke-interface {v0, p0, v1}, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment$Delegate;->onDisclaimerDialogDismissed(Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;Z)V

    .line 446
    :cond_0
    :goto_0
    return-void

    .line 444
    :cond_1
    invoke-direct {p0}, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->getDelegate()Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment$Delegate;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, p0, v1}, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment$Delegate;->onDisclaimerDialogDismissed(Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;Z)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 172
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "[onCreate]"

    invoke-static {v0, v1}, Lcom/samsung/groupcast/application/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 173
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 175
    if-nez p1, :cond_1

    .line 195
    :cond_0
    :goto_0
    return-void

    .line 179
    :cond_1
    const-string v0, "DISCLAIMER_TITLE"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 180
    const-string v0, "DISCLAIMER_TITLE"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->mTitle:Ljava/lang/String;

    .line 183
    :cond_2
    const-string v0, "DISCLAIMER_MESSAGE"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 184
    const-string v0, "DISCLAIMER_MESSAGE"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->mMessage:Ljava/lang/String;

    .line 187
    :cond_3
    const-string v0, "DISCLAIMER_VIEW"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 188
    const-string v0, "DISCLAIMER_VIEW"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->mViewID:I

    .line 191
    :cond_4
    const-string v0, "DISCLAIMER_TYPE"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 192
    const-string v0, "DISCLAIMER_TYPE"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->mType:I

    goto :goto_0
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 13
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const v12, 0x7f080028

    const v8, 0x7f080027

    const v11, 0x7f08000e

    const v10, 0x7f07000b

    const/16 v9, 0x8

    .line 199
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v6

    const-string v7, "[onCreateDialog]"

    invoke-static {v6, v7}, Lcom/samsung/groupcast/application/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 202
    iget v6, p0, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->mType:I

    packed-switch v6, :pswitch_data_0

    .line 245
    :goto_0
    const/4 v0, 0x0

    .line 250
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    new-instance v0, Landroid/app/AlertDialog$Builder;

    .end local v0    # "builder":Landroid/app/AlertDialog$Builder;
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    invoke-direct {v0, v6}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 253
    .restart local v0    # "builder":Landroid/app/AlertDialog$Builder;
    iget-object v6, p0, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->mTitle:Ljava/lang/String;

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->mTitle:Ljava/lang/String;

    const-string v7, ""

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 254
    :cond_0
    const v6, 0x7f08000a

    invoke-virtual {p0, v6}, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 259
    :goto_1
    iget v6, p0, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->mViewID:I

    if-eqz v6, :cond_1

    .line 260
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v6

    iget v7, p0, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->mViewID:I

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v6

    iput-object v6, p0, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->mView:Landroid/view/View;

    .line 263
    :cond_1
    iget-object v6, p0, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->mView:Landroid/view/View;

    if-nez v6, :cond_6

    .line 264
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v6

    const v7, 0x7f04001c

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 267
    .local v1, "customView":Landroid/view/View;
    iget-object v6, p0, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->mMessage:Ljava/lang/String;

    if-eqz v6, :cond_2

    iget-object v6, p0, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->mMessage:Ljava/lang/String;

    const-string v7, ""

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 268
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    const-string v7, "onCreateDialog"

    const-string v8, "There isn\'t any message"

    invoke-static {v6, v7, v8}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;)V

    .line 270
    :cond_3
    invoke-virtual {v1, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 271
    .local v5, "textView":Landroid/widget/TextView;
    iget-object v6, p0, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->mMessage:Ljava/lang/String;

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 276
    .end local v5    # "textView":Landroid/widget/TextView;
    :goto_2
    const v6, 0x7f07000d

    invoke-virtual {v1, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/CheckBox;

    iput-object v6, p0, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->mCheckBox:Landroid/widget/CheckBox;

    .line 278
    iget v6, p0, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->mType:I

    const/4 v7, 0x1

    if-eq v6, v7, :cond_9

    .line 279
    iget-object v6, p0, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->mCheckBoxText:Ljava/lang/String;

    if-eqz v6, :cond_7

    .line 280
    iget-object v6, p0, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->mCheckBox:Landroid/widget/CheckBox;

    iget-object v7, p0, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->mCheckBoxText:Ljava/lang/String;

    invoke-virtual {v6, v7}, Landroid/widget/CheckBox;->setText(Ljava/lang/CharSequence;)V

    .line 285
    :goto_3
    iget-object v6, p0, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->mCheckBox:Landroid/widget/CheckBox;

    const-string v7, "ROBOTO_LIGHT"

    invoke-static {v6, v7}, Lcom/samsung/groupcast/misc/utility/StringTools;->setTypeFace(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 286
    iget-object v6, p0, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->mCheckBox:Landroid/widget/CheckBox;

    new-instance v7, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment$1;

    invoke-direct {v7, p0}, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment$1;-><init>(Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;)V

    invoke-virtual {v6, v7}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 293
    iget v6, p0, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->mType:I

    const/4 v7, 0x7

    if-eq v6, v7, :cond_4

    iget v6, p0, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->mType:I

    if-ne v6, v9, :cond_8

    .line 294
    :cond_4
    const v6, 0x7f08000f

    invoke-static {v6}, Lcom/samsung/groupcast/application/App;->getSanitizedString(I)Ljava/lang/String;

    move-result-object v6

    new-instance v7, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment$2;

    invoke-direct {v7, p0}, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment$2;-><init>(Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;)V

    invoke-virtual {v0, v6, v7}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 307
    invoke-static {v11}, Lcom/samsung/groupcast/application/App;->getSanitizedString(I)Ljava/lang/String;

    move-result-object v6

    new-instance v7, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment$3;

    invoke-direct {v7, p0}, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment$3;-><init>(Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;)V

    invoke-virtual {v0, v6, v7}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 369
    :goto_4
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 370
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    .line 371
    .local v2, "dialog":Landroid/app/AlertDialog;
    return-object v2

    .line 204
    .end local v0    # "builder":Landroid/app/AlertDialog$Builder;
    .end local v1    # "customView":Landroid/view/View;
    .end local v2    # "dialog":Landroid/app/AlertDialog;
    :pswitch_0
    const v6, 0x7f08000b

    invoke-static {v6}, Lcom/samsung/groupcast/application/App;->getSanitizedString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->setMessage(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 207
    :pswitch_1
    invoke-virtual {p0, v8}, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->setTitle(Ljava/lang/CharSequence;)V

    .line 208
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const v7, 0x7f08007b

    invoke-static {v7}, Lcom/samsung/groupcast/application/App;->getSanitizedString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const v7, 0x7f08007c

    invoke-static {v7}, Lcom/samsung/groupcast/application/App;->getSanitizedString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const v7, 0x7f08007d

    invoke-static {v7}, Lcom/samsung/groupcast/application/App;->getSanitizedString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const v7, 0x7f08007e

    invoke-static {v7}, Lcom/samsung/groupcast/application/App;->getSanitizedString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const v7, 0x7f08007f

    invoke-static {v7}, Lcom/samsung/groupcast/application/App;->getSanitizedString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const v7, 0x7f080080

    invoke-static {v7}, Lcom/samsung/groupcast/application/App;->getSanitizedString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const v7, 0x7f080081

    invoke-static {v7}, Lcom/samsung/groupcast/application/App;->getSanitizedString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const v7, 0x7f080082

    invoke-static {v7}, Lcom/samsung/groupcast/application/App;->getSanitizedString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const v7, 0x7f080083

    invoke-static {v7}, Lcom/samsung/groupcast/application/App;->getSanitizedString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const v7, 0x7f080084

    invoke-static {v7}, Lcom/samsung/groupcast/application/App;->getSanitizedString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\n\n"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const v7, 0x7f080085

    invoke-static {v7}, Lcom/samsung/groupcast/application/App;->getSanitizedString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const v7, 0x7f080086

    invoke-static {v7}, Lcom/samsung/groupcast/application/App;->getSanitizedString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const v7, 0x7f080087

    invoke-static {v7}, Lcom/samsung/groupcast/application/App;->getSanitizedString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const v7, 0x7f080088

    invoke-static {v7}, Lcom/samsung/groupcast/application/App;->getSanitizedString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const v7, 0x7f080089

    invoke-static {v7}, Lcom/samsung/groupcast/application/App;->getSanitizedString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->setMessage(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 225
    :pswitch_2
    invoke-static {v12}, Lcom/samsung/groupcast/application/App;->getSanitizedString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->setTitle(Ljava/lang/CharSequence;)V

    .line 226
    const v6, 0x7f080030

    invoke-virtual {p0, v6}, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->setMessage(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 229
    :pswitch_3
    const v6, 0x7f08002b

    invoke-static {v6}, Lcom/samsung/groupcast/application/App;->getSanitizedString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->setMessage(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 232
    :pswitch_4
    const v6, 0x7f080036

    invoke-virtual {p0, v6}, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->setMessage(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 235
    :pswitch_5
    invoke-static {v12}, Lcom/samsung/groupcast/application/App;->getSanitizedString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->setTitle(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 238
    :pswitch_6
    invoke-virtual {p0, v8}, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->setTitle(Ljava/lang/CharSequence;)V

    .line 239
    const v6, 0x7f0800b9

    invoke-static {v6}, Lcom/samsung/groupcast/application/App;->getSanitizedString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->setMessage(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 256
    .restart local v0    # "builder":Landroid/app/AlertDialog$Builder;
    :cond_5
    iget-object v6, p0, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->mTitle:Ljava/lang/String;

    invoke-virtual {v0, v6}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    goto/16 :goto_1

    .line 273
    :cond_6
    iget-object v1, p0, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->mView:Landroid/view/View;

    .restart local v1    # "customView":Landroid/view/View;
    goto/16 :goto_2

    .line 282
    :cond_7
    iget-object v6, p0, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->mCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {p0}, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v7

    const v8, 0x7f08000c

    invoke-virtual {v7, v8}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/CheckBox;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    .line 324
    :cond_8
    invoke-static {v11}, Lcom/samsung/groupcast/application/App;->getSanitizedString(I)Ljava/lang/String;

    move-result-object v6

    new-instance v7, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment$4;

    invoke-direct {v7, p0}, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment$4;-><init>(Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;)V

    invoke-virtual {v0, v6, v7}, Landroid/app/AlertDialog$Builder;->setNeutralButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto/16 :goto_4

    .line 334
    :cond_9
    iget-object v6, p0, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->mCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v6, v9}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 335
    const v6, 0x7f080024

    invoke-static {v6}, Lcom/samsung/groupcast/application/App;->getSanitizedString(I)Ljava/lang/String;

    move-result-object v6

    new-instance v7, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment$5;

    invoke-direct {v7, p0}, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment$5;-><init>(Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;)V

    invoke-virtual {v0, v6, v7}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 344
    const v6, 0x7f080025

    invoke-static {v6}, Lcom/samsung/groupcast/application/App;->getSanitizedString(I)Ljava/lang/String;

    move-result-object v6

    new-instance v7, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment$6;

    invoke-direct {v7, p0}, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment$6;-><init>(Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;)V

    invoke-virtual {v0, v6, v7}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 359
    invoke-virtual {v1, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 360
    .restart local v5    # "textView":Landroid/widget/TextView;
    iget-object v6, p0, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->mCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v6}, Landroid/widget/CheckBox;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout$LayoutParams;

    .line 361
    .local v3, "l":Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual {v5}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout$LayoutParams;

    .line 362
    .local v4, "li":Landroid/widget/LinearLayout$LayoutParams;
    iget v6, v3, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    iput v6, v4, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    .line 363
    invoke-virtual {v5, v4}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_4

    .line 202
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_5
        :pswitch_2
        :pswitch_4
        :pswitch_6
    .end packed-switch
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    .line 450
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onDismiss(Landroid/content/DialogInterface;)V

    .line 452
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_1

    .line 462
    :cond_0
    :goto_0
    return-void

    .line 456
    :cond_1
    iget-boolean v0, p0, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->mWasCancelled:Z

    if-nez v0, :cond_2

    iget v0, p0, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->mType:I

    invoke-direct {p0, v0}, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->getKey(I)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->mCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 457
    iget v0, p0, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->mSeveralDays:I

    iget v1, p0, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->mType:I

    invoke-direct {p0, v1}, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->getKey(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/groupcast/application/Preferences;->setSkipDisclaimerNotificationPreference(ILjava/lang/String;)V

    .line 460
    :cond_2
    invoke-direct {p0}, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->getDelegate()Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment$Delegate;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 461
    invoke-direct {p0}, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->getDelegate()Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment$Delegate;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, p0, v1}, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment$Delegate;->onDisclaimerDialogDismissed(Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;Z)V

    goto :goto_0
.end method

.method public onResume()V
    .locals 2

    .prologue
    const v1, 0x7f08000c

    .line 383
    invoke-super {p0}, Landroid/app/DialogFragment;->onResume()V

    .line 385
    iget v0, p0, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->mType:I

    packed-switch v0, :pswitch_data_0

    .line 412
    :goto_0
    :pswitch_0
    return-void

    .line 389
    :pswitch_1
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    check-cast v0, Landroid/app/AlertDialog;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Button;->requestFocus()Z

    goto :goto_0

    .line 396
    :pswitch_2
    iget-object v0, p0, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->mCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setText(I)V

    goto :goto_0

    .line 401
    :pswitch_3
    iget-object v0, p0, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->mCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setText(I)V

    goto :goto_0

    .line 404
    :pswitch_4
    iget-object v0, p0, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->mCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setText(I)V

    goto :goto_0

    .line 407
    :pswitch_5
    iget-object v0, p0, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->mCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setText(I)V

    goto :goto_0

    .line 385
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_4
        :pswitch_2
        :pswitch_3
        :pswitch_5
    .end packed-switch
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 416
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 418
    iget-object v0, p0, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->mTitle:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->mTitle:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 419
    const-string v0, "DISCLAIMER_TITLE"

    iget-object v1, p0, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->mTitle:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 421
    :cond_0
    iget-object v0, p0, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->mMessage:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->mMessage:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 422
    const-string v0, "DISCLAIMER_MESSAGE"

    iget-object v1, p0, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->mMessage:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 424
    :cond_1
    iget v0, p0, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->mViewID:I

    if-eqz v0, :cond_2

    .line 425
    const-string v0, "DISCLAIMER_VIEW"

    iget v1, p0, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->mViewID:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 427
    :cond_2
    iget v0, p0, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->mType:I

    const/16 v1, 0x3e7

    if-eq v0, v1, :cond_3

    .line 428
    const-string v0, "DISCLAIMER_TYPE"

    iget v1, p0, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->mType:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 430
    :cond_3
    return-void
.end method

.method public onStart()V
    .locals 1

    .prologue
    .line 376
    const-string v0, "---"

    invoke-static {v0}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 377
    invoke-super {p0}, Landroid/app/DialogFragment;->onStart()V

    .line 378
    invoke-direct {p0}, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->sanitizeViews()V

    .line 379
    return-void
.end method

.method public setCheckBoxText(Ljava/lang/CharSequence;)V
    .locals 0
    .param p1, "title"    # Ljava/lang/CharSequence;

    .prologue
    .line 490
    check-cast p1, Ljava/lang/String;

    .end local p1    # "title":Ljava/lang/CharSequence;
    iput-object p1, p0, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->mCheckBoxText:Ljava/lang/String;

    .line 491
    return-void
.end method

.method public setCustomView(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 506
    iput-object p1, p0, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->mView:Landroid/view/View;

    .line 507
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    iput v0, p0, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->mViewID:I

    .line 508
    return-void
.end method

.method public setCustomViewId(I)V
    .locals 0
    .param p1, "mId"    # I

    .prologue
    .line 502
    iput p1, p0, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->mViewID:I

    .line 503
    return-void
.end method

.method public setDuration(I)V
    .locals 0
    .param p1, "days"    # I

    .prologue
    .line 522
    iput p1, p0, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->mSeveralDays:I

    .line 523
    return-void
.end method

.method public setMessage(Ljava/lang/CharSequence;)V
    .locals 0
    .param p1, "text"    # Ljava/lang/CharSequence;

    .prologue
    .line 494
    check-cast p1, Ljava/lang/String;

    .end local p1    # "text":Ljava/lang/CharSequence;
    iput-object p1, p0, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->mMessage:Ljava/lang/String;

    .line 495
    return-void
.end method

.method public setTitle(Ljava/lang/CharSequence;)V
    .locals 0
    .param p1, "title"    # Ljava/lang/CharSequence;

    .prologue
    .line 486
    check-cast p1, Ljava/lang/String;

    .end local p1    # "title":Ljava/lang/CharSequence;
    iput-object p1, p0, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->mTitle:Ljava/lang/String;

    .line 487
    return-void
.end method

.method public setType(I)V
    .locals 0
    .param p1, "index"    # I

    .prologue
    .line 498
    iput p1, p0, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->mType:I

    .line 499
    return-void
.end method
