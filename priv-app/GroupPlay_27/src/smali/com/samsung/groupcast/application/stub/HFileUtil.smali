.class public Lcom/samsung/groupcast/application/stub/HFileUtil;
.super Ljava/lang/Object;
.source "HFileUtil.java"


# instance fields
.field private _AbsolutePath:Ljava/lang/String;

.field private _Context:Landroid/content/Context;

.field private _File:Ljava/io/File;

.field private _FileName:Ljava/lang/String;

.field private _bPrepared:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/groupcast/application/stub/HFileUtil;->_bPrepared:Z

    .line 14
    iput-object p1, p0, Lcom/samsung/groupcast/application/stub/HFileUtil;->_Context:Landroid/content/Context;

    .line 15
    iput-object p2, p0, Lcom/samsung/groupcast/application/stub/HFileUtil;->_FileName:Ljava/lang/String;

    .line 16
    return-void
.end method

.method private createDir(Ljava/lang/String;)Z
    .locals 3
    .param p1, "dir"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x1

    .line 36
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 38
    .local v0, "root":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 44
    :cond_0
    :goto_0
    return v1

    .line 41
    :cond_1
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    move-result v2

    if-nez v2, :cond_0

    .line 44
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private createInternalStorageFile(Ljava/lang/String;)Ljava/io/File;
    .locals 3
    .param p1, "filename"    # Ljava/lang/String;

    .prologue
    .line 48
    iget-object v2, p0, Lcom/samsung/groupcast/application/stub/HFileUtil;->_Context:Landroid/content/Context;

    invoke-virtual {v2, p1}, Landroid/content/Context;->getFileStreamPath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    .line 49
    .local v0, "absolutePath":Ljava/lang/String;
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 50
    .local v1, "ret":Ljava/io/File;
    return-object v1
.end method


# virtual methods
.method public delete()Z
    .locals 1

    .prologue
    .line 29
    iget-boolean v0, p0, Lcom/samsung/groupcast/application/stub/HFileUtil;->_bPrepared:Z

    if-eqz v0, :cond_0

    .line 30
    iget-object v0, p0, Lcom/samsung/groupcast/application/stub/HFileUtil;->_File:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v0

    .line 32
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public exists()Z
    .locals 1

    .prologue
    .line 73
    iget-boolean v0, p0, Lcom/samsung/groupcast/application/stub/HFileUtil;->_bPrepared:Z

    if-eqz v0, :cond_0

    .line 74
    iget-object v0, p0, Lcom/samsung/groupcast/application/stub/HFileUtil;->_File:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    .line 76
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getAbsolutePath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/samsung/groupcast/application/stub/HFileUtil;->_AbsolutePath:Ljava/lang/String;

    return-object v0
.end method

.method public getFile()Ljava/io/File;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/samsung/groupcast/application/stub/HFileUtil;->_File:Ljava/io/File;

    return-object v0
.end method

.method public getFileName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/samsung/groupcast/application/stub/HFileUtil;->_FileName:Ljava/lang/String;

    return-object v0
.end method

.method public length()J
    .locals 2

    .prologue
    .line 80
    iget-boolean v0, p0, Lcom/samsung/groupcast/application/stub/HFileUtil;->_bPrepared:Z

    if-eqz v0, :cond_0

    .line 81
    iget-object v0, p0, Lcom/samsung/groupcast/application/stub/HFileUtil;->_File:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v0

    .line 83
    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public prepare()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 54
    iget-boolean v1, p0, Lcom/samsung/groupcast/application/stub/HFileUtil;->_bPrepared:Z

    if-eqz v1, :cond_0

    .line 65
    :goto_0
    return v0

    .line 58
    :cond_0
    iget-object v1, p0, Lcom/samsung/groupcast/application/stub/HFileUtil;->_Context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/samsung/groupcast/application/stub/HFileUtil;->createDir(Ljava/lang/String;)Z

    .line 59
    iget-object v1, p0, Lcom/samsung/groupcast/application/stub/HFileUtil;->_FileName:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/samsung/groupcast/application/stub/HFileUtil;->createInternalStorageFile(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/groupcast/application/stub/HFileUtil;->_File:Ljava/io/File;

    .line 60
    iget-object v1, p0, Lcom/samsung/groupcast/application/stub/HFileUtil;->_File:Ljava/io/File;

    if-nez v1, :cond_1

    .line 61
    const/4 v0, 0x0

    goto :goto_0

    .line 63
    :cond_1
    iget-object v1, p0, Lcom/samsung/groupcast/application/stub/HFileUtil;->_File:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/groupcast/application/stub/HFileUtil;->_AbsolutePath:Ljava/lang/String;

    .line 64
    iput-boolean v0, p0, Lcom/samsung/groupcast/application/stub/HFileUtil;->_bPrepared:Z

    goto :goto_0
.end method

.method public release()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 23
    iput-object v0, p0, Lcom/samsung/groupcast/application/stub/HFileUtil;->_Context:Landroid/content/Context;

    .line 24
    iput-object v0, p0, Lcom/samsung/groupcast/application/stub/HFileUtil;->_File:Ljava/io/File;

    .line 25
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/groupcast/application/stub/HFileUtil;->_bPrepared:Z

    .line 26
    return-void
.end method
