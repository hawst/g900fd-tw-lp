.class Lcom/samsung/groupcast/application/start/StartActivity$9;
.super Ljava/lang/Object;
.source "StartActivity.java"

# interfaces
.implements Landroid/support/v4/view/FixedViewPager$OnPageChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/groupcast/application/start/StartActivity;->initSetupLayout()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/groupcast/application/start/StartActivity;


# direct methods
.method constructor <init>(Lcom/samsung/groupcast/application/start/StartActivity;)V
    .locals 0

    .prologue
    .line 1664
    iput-object p1, p0, Lcom/samsung/groupcast/application/start/StartActivity$9;->this$0:Lcom/samsung/groupcast/application/start/StartActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPageScrollStateChanged(I)V
    .locals 0
    .param p1, "state"    # I

    .prologue
    .line 1697
    return-void
.end method

.method public onPageScrolled(IFI)V
    .locals 0
    .param p1, "position"    # I
    .param p2, "positionOffset"    # F
    .param p3, "positionOffsetPixels"    # I

    .prologue
    .line 1692
    return-void
.end method

.method public onPageSelected(I)V
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 1668
    packed-switch p1, :pswitch_data_0

    .line 1686
    :goto_0
    :pswitch_0
    return-void

    .line 1672
    :pswitch_1
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/StartActivity$9;->this$0:Lcom/samsung/groupcast/application/start/StartActivity;

    # getter for: Lcom/samsung/groupcast/application/start/StartActivity;->mStartNextBtn:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/samsung/groupcast/application/start/StartActivity;->access$700(Lcom/samsung/groupcast/application/start/StartActivity;)Landroid/widget/LinearLayout;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/groupcast/application/start/StartActivity$9;->this$0:Lcom/samsung/groupcast/application/start/StartActivity;

    # getter for: Lcom/samsung/groupcast/application/start/StartActivity;->mDisclaimerChecked:Z
    invoke-static {v1}, Lcom/samsung/groupcast/application/start/StartActivity;->access$600(Lcom/samsung/groupcast/application/start/StartActivity;)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 1673
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/StartActivity$9;->this$0:Lcom/samsung/groupcast/application/start/StartActivity;

    # getter for: Lcom/samsung/groupcast/application/start/StartActivity;->mDisclaimerChecked:Z
    invoke-static {v0}, Lcom/samsung/groupcast/application/start/StartActivity;->access$600(Lcom/samsung/groupcast/application/start/StartActivity;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1674
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/StartActivity$9;->this$0:Lcom/samsung/groupcast/application/start/StartActivity;

    # getter for: Lcom/samsung/groupcast/application/start/StartActivity;->mStartNextBtn:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/samsung/groupcast/application/start/StartActivity;->access$700(Lcom/samsung/groupcast/application/start/StartActivity;)Landroid/widget/LinearLayout;

    move-result-object v0

    const v1, 0x3ecccccd    # 0.4f

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setAlpha(F)V

    .line 1676
    :cond_0
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/StartActivity$9;->this$0:Lcom/samsung/groupcast/application/start/StartActivity;

    iget-object v0, v0, Lcom/samsung/groupcast/application/start/StartActivity;->contentView:Landroid/view/View;

    const v1, 0x7f070038

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 1668
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
