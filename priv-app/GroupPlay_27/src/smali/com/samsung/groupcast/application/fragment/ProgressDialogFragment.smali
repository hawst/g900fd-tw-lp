.class public Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;
.super Landroid/app/DialogFragment;
.source "ProgressDialogFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment$ProgressDelegate;
    }
.end annotation


# static fields
.field private static final KEY_PROGRESS_PERCENTAGE:Ljava/lang/String; = "PROGRESS_PERCENTAGE"

.field private static final KEY_TEXT:Ljava/lang/String; = "TEXT"

.field private static final KEY_TITLE:Ljava/lang/String; = "TITLE"

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mCancelled:Z

.field private mProgressDelegate:Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment$ProgressDelegate;

.field private mProgressPercentage:I

.field private mProgressTextView:Landroid/widget/TextView;

.field private mText:Ljava/lang/String;

.field private mTimeoutDuration:I

.field private mTimeoutRunnable:Ljava/lang/Runnable;

.field private mTitle:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const-class v0, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;

    invoke-static {v0}, Lcom/samsung/groupcast/application/Logger;->getTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 26
    const/16 v0, 0x3a98

    iput v0, p0, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;->mTimeoutDuration:I

    .line 30
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;

    .prologue
    .line 22
    iget v0, p0, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;->mTimeoutDuration:I

    return v0
.end method

.method private getProgressDelegate()Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment$ProgressDelegate;
    .locals 2

    .prologue
    .line 146
    iget-object v0, p0, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;->mProgressDelegate:Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment$ProgressDelegate;

    if-eqz v0, :cond_0

    .line 147
    iget-object v0, p0, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;->mProgressDelegate:Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment$ProgressDelegate;

    .line 154
    :goto_0
    return-object v0

    .line 150
    :cond_0
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-class v1, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment$ProgressDelegate;

    invoke-static {v0, v1}, Lcom/samsung/groupcast/misc/utility/Verify;->instanceOf(Ljava/lang/Object;Ljava/lang/Class;)V

    .line 151
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment$ProgressDelegate;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 152
    :catch_0
    move-exception v0

    .line 154
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private updateViews()V
    .locals 5

    .prologue
    .line 183
    iget-object v1, p0, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;->mProgressTextView:Landroid/widget/TextView;

    if-nez v1, :cond_0

    .line 191
    :goto_0
    return-void

    .line 187
    :cond_0
    const v1, 0x7f080007

    invoke-virtual {p0, v1}, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;->mProgressPercentage:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 190
    .local v0, "text":Ljava/lang/String;
    iget-object v1, p0, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;->mProgressTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method


# virtual methods
.method public forceDismiss()V
    .locals 2

    .prologue
    .line 194
    iget-object v0, p0, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;->mTimeoutRunnable:Ljava/lang/Runnable;

    if-nez v0, :cond_0

    .line 195
    sget-object v0, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;->TAG:Ljava/lang/String;

    const-string v1, "[forceDismiss] Timeout threa"

    invoke-static {v0, v1}, Lcom/samsung/groupcast/application/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 199
    :goto_0
    return-void

    .line 198
    :cond_0
    iget-object v0, p0, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;->mTimeoutRunnable:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    goto :goto_0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 91
    sget-object v0, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;->TAG:Ljava/lang/String;

    const-string v1, "[onActivityCreated]"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 92
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 93
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;->getView()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f070055

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;->mProgressTextView:Landroid/widget/TextView;

    .line 94
    return-void
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    .line 107
    sget-object v0, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;->TAG:Ljava/lang/String;

    const-string v1, "[onCancel]"

    invoke-static {v0, v1}, Lcom/samsung/groupcast/application/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onCancel(Landroid/content/DialogInterface;)V

    .line 109
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;->mCancelled:Z

    .line 110
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 46
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 48
    iget-object v0, p0, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;->mTimeoutRunnable:Ljava/lang/Runnable;

    if-nez v0, :cond_0

    .line 49
    new-instance v0, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment$1;

    invoke-direct {v0, p0}, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment$1;-><init>(Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;)V

    iput-object v0, p0, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;->mTimeoutRunnable:Ljava/lang/Runnable;

    .line 63
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v3, 0x1

    .line 67
    sget-object v1, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;->TAG:Ljava/lang/String;

    const-string v2, "[onCreateView]"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 69
    if-eqz p3, :cond_0

    .line 70
    const-string v1, "PROGRESS_PERCENTAGE"

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;->mProgressPercentage:I

    .line 71
    const-string v1, "TITLE"

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;->mTitle:Ljava/lang/String;

    .line 72
    const-string v1, "TEXT"

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;->mText:Ljava/lang/String;

    .line 75
    :cond_0
    iget-object v1, p0, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;->mTitle:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 76
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;->mTitle:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/app/Dialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 80
    :goto_0
    const v1, 0x7f04001e

    invoke-virtual {p1, v1, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 82
    .local v0, "v":Landroid/view/View;
    const v1, 0x7f070055

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;->mProgressTextView:Landroid/widget/TextView;

    .line 84
    iget-object v1, p0, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;->mText:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 85
    iget-object v1, p0, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;->mProgressTextView:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;->mText:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 86
    :cond_1
    return-object v0

    .line 78
    .end local v0    # "v":Landroid/view/View;
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/app/Dialog;->requestWindowFeature(I)Z

    goto :goto_0
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    .line 114
    sget-object v0, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;->TAG:Ljava/lang/String;

    const-string v1, "[onDismiss]"

    invoke-static {v0, v1}, Lcom/samsung/groupcast/application/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 115
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onDismiss(Landroid/content/DialogInterface;)V

    .line 117
    iget-object v0, p0, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;->mTimeoutRunnable:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 118
    iget-object v0, p0, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;->mTimeoutRunnable:Ljava/lang/Runnable;

    invoke-static {v0}, Lcom/samsung/groupcast/application/MainQueue;->cancel(Ljava/lang/Runnable;)V

    .line 121
    :cond_0
    invoke-direct {p0}, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;->getProgressDelegate()Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment$ProgressDelegate;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 122
    invoke-direct {p0}, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;->getProgressDelegate()Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment$ProgressDelegate;

    move-result-object v0

    iget-boolean v1, p0, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;->mCancelled:Z

    invoke-interface {v0, p0, v1}, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment$ProgressDelegate;->onProgressDialogDismissed(Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;Z)V

    .line 124
    :cond_1
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 98
    sget-object v0, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;->TAG:Ljava/lang/String;

    const-string v1, "[onSaveInstanceState]"

    invoke-static {v0, v1}, Lcom/samsung/groupcast/application/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 99
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 100
    const-string v0, "PROGRESS_PERCENTAGE"

    iget v1, p0, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;->mProgressPercentage:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 101
    const-string v0, "TITLE"

    iget-object v1, p0, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;->mTitle:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 102
    const-string v0, "TEXT"

    iget-object v1, p0, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;->mText:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 103
    return-void
.end method

.method public onStart()V
    .locals 3

    .prologue
    .line 128
    invoke-super {p0}, Landroid/app/DialogFragment;->onStart()V

    .line 129
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;->isCancelable()Z

    move-result v0

    if-nez v0, :cond_0

    .line 130
    iget-object v0, p0, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;->mTimeoutRunnable:Ljava/lang/Runnable;

    invoke-static {v0}, Lcom/samsung/groupcast/application/MainQueue;->cancel(Ljava/lang/Runnable;)V

    .line 131
    iget-object v0, p0, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;->mTimeoutRunnable:Ljava/lang/Runnable;

    iget v1, p0, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;->mTimeoutDuration:I

    int-to-long v1, v1

    invoke-static {v0, v1, v2}, Lcom/samsung/groupcast/application/MainQueue;->postDelayed(Ljava/lang/Runnable;J)V

    .line 133
    :cond_0
    return-void
.end method

.method public setProgressDelegate(Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment$ProgressDelegate;)V
    .locals 0
    .param p1, "delegate"    # Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment$ProgressDelegate;

    .prologue
    .line 142
    iput-object p1, p0, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;->mProgressDelegate:Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment$ProgressDelegate;

    .line 143
    return-void
.end method

.method public setProgressPercentage(I)V
    .locals 0
    .param p1, "progressPercentage"    # I

    .prologue
    .line 158
    iput p1, p0, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;->mProgressPercentage:I

    .line 159
    invoke-direct {p0}, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;->updateViews()V

    .line 160
    return-void
.end method

.method public setText(Ljava/lang/String;)V
    .locals 2
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 171
    sget-object v0, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;->TAG:Ljava/lang/String;

    const-string v1, "[setText]"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 173
    iput-object p1, p0, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;->mText:Ljava/lang/String;

    .line 174
    iget-object v0, p0, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;->mProgressTextView:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 175
    iget-object v0, p0, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;->mProgressTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 176
    :cond_0
    return-void
.end method

.method public setTimeout(I)V
    .locals 0
    .param p1, "duration"    # I

    .prologue
    .line 179
    iput p1, p0, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;->mTimeoutDuration:I

    .line 180
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 2
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 163
    sget-object v0, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;->TAG:Ljava/lang/String;

    const-string v1, "[setTitle]"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 164
    iput-object p1, p0, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;->mTitle:Ljava/lang/String;

    .line 166
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 167
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/app/Dialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 168
    :cond_0
    return-void
.end method

.method public stopTimer()V
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;->mTimeoutRunnable:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 137
    iget-object v0, p0, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;->mTimeoutRunnable:Ljava/lang/Runnable;

    invoke-static {v0}, Lcom/samsung/groupcast/application/MainQueue;->cancel(Ljava/lang/Runnable;)V

    .line 139
    :cond_0
    return-void
.end method
