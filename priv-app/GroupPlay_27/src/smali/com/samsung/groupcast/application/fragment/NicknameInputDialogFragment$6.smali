.class Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment$6;
.super Ljava/lang/Object;
.source "NicknameInputDialogFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;


# direct methods
.method constructor <init>(Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;)V
    .locals 0

    .prologue
    .line 204
    iput-object p1, p0, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment$6;->this$0:Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 207
    iget-object v0, p0, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment$6;->this$0:Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;

    # getter for: Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;->mDelegate:Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment$NicknameInputDialogFragmentDelegate;
    invoke-static {v0}, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;->access$200(Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;)Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment$NicknameInputDialogFragmentDelegate;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 208
    iget-object v0, p0, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment$6;->this$0:Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;

    # getter for: Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;->mDelegate:Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment$NicknameInputDialogFragmentDelegate;
    invoke-static {v0}, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;->access$200(Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;)Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment$NicknameInputDialogFragmentDelegate;

    move-result-object v0

    # getter for: Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;->mInstance:Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;
    invoke-static {}, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;->access$300()Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment$6;->this$0:Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;

    # getter for: Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;->mNickEditText:Landroid/widget/EditText;
    invoke-static {v2}, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;->access$400(Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;)Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment$NicknameInputDialogFragmentDelegate;->onNicknameSubmitted(Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;Ljava/lang/String;Z)V

    .line 210
    :cond_0
    return-void
.end method
