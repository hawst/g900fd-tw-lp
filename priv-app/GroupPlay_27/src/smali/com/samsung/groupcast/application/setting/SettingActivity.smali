.class public Lcom/samsung/groupcast/application/setting/SettingActivity;
.super Landroid/app/Activity;
.source "SettingActivity.java"

# interfaces
.implements Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment$NicknameInputDialogFragmentDelegate;
.implements Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$Delegate;
.implements Lcom/samsung/groupcast/application/start/StartFragment$DisclaimerFragmentCheckedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/groupcast/application/setting/SettingActivity$1;
    }
.end annotation


# static fields
.field private static final NICKNAME_INPUT_DIALOG_FRAGMENT:Ljava/lang/String; = "TAG_NICKNAME_DIALOG"


# instance fields
.field private TAG:Ljava/lang/String;

.field private mListAdapter:Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter;

.field private mListView:Landroid/widget/ListView;

.field private mNickName:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 40
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/groupcast/application/setting/SettingActivity;->TAG:Ljava/lang/String;

    return-void
.end method

.method private hideNicknameInputDialog()V
    .locals 4

    .prologue
    .line 140
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/setting/SettingActivity;->isFinishing()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 154
    :cond_0
    :goto_0
    return-void

    .line 144
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/setting/SettingActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/samsung/groupcast/application/setting/SettingActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "TAG_NICKNAME_DIALOG"

    invoke-virtual {v1, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 147
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/setting/SettingActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    invoke-virtual {p0}, Lcom/samsung/groupcast/application/setting/SettingActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-string v3, "TAG_NICKNAME_DIALOG"

    invoke-virtual {v2, v3}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 150
    :catch_0
    move-exception v0

    .line 151
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private showNicknameInputDialog()V
    .locals 5

    .prologue
    .line 116
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/setting/SettingActivity;->isFinishing()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 137
    :cond_0
    :goto_0
    return-void

    .line 120
    :cond_1
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 121
    .local v0, "args":Landroid/os/Bundle;
    const-string v3, "Nickname"

    iget-object v4, p0, Lcom/samsung/groupcast/application/setting/SettingActivity;->mNickName:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/setting/SettingActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-virtual {p0}, Lcom/samsung/groupcast/application/setting/SettingActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    const-string v4, "TAG_NICKNAME_DIALOG"

    invoke-virtual {v3, v4}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v3

    if-nez v3, :cond_0

    .line 128
    :cond_2
    :try_start_0
    new-instance v2, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;

    invoke-direct {v2}, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;-><init>()V

    .line 129
    .local v2, "fragment":Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;
    invoke-virtual {v2, v0}, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;->setArguments(Landroid/os/Bundle;)V

    .line 131
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/setting/SettingActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    const-string v4, "TAG_NICKNAME_DIALOG"

    invoke-virtual {v2, v3, v4}, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 133
    .end local v2    # "fragment":Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;
    :catch_0
    move-exception v1

    .line 134
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static startActivity(Landroid/app/Activity;)V
    .locals 2
    .param p0, "caller"    # Landroid/app/Activity;

    .prologue
    .line 267
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/samsung/groupcast/application/setting/SettingActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 268
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 269
    const v1, 0x1000c

    invoke-virtual {p0, v0, v1}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 270
    return-void
.end method

.method private updateAboutAdapter()V
    .locals 9

    .prologue
    .line 220
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/setting/SettingActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    .line 222
    .local v3, "packageManager":Landroid/content/pm/PackageManager;
    const-string v5, ""

    .line 224
    .local v5, "versionName":Ljava/lang/String;
    :try_start_0
    const-string v6, "com.samsung.groupcast"

    const/4 v7, 0x0

    invoke-virtual {v3, v6, v7}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v2

    .line 225
    .local v2, "packageInfo":Landroid/content/pm/PackageInfo;
    iget-object v5, v2, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 231
    .end local v2    # "packageInfo":Landroid/content/pm/PackageInfo;
    :goto_0
    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    .line 232
    .local v4, "sbVersion":Ljava/lang/StringBuffer;
    const v6, 0x7f0800e8

    invoke-static {v6}, Lcom/samsung/groupcast/application/App;->getSanitizedString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 233
    const-string v6, " "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 235
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 236
    .local v0, "aboutItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$AboutItem;>;"
    new-instance v6, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$AboutItem;

    sget-object v7, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$AboutEnum;->ABOUT_FAQ:Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$AboutEnum;

    const v8, 0x7f0800e6

    invoke-static {v8}, Lcom/samsung/groupcast/application/App;->getSanitizedString(I)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v6, v7, v8}, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$AboutItem;-><init>(Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$AboutEnum;Ljava/lang/String;)V

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 240
    new-instance v6, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$AboutItem;

    sget-object v7, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$AboutEnum;->ABOUT_VERSION:Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$AboutEnum;

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v6, v7, v8}, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$AboutItem;-><init>(Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$AboutEnum;Ljava/lang/String;)V

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 242
    iget-object v6, p0, Lcom/samsung/groupcast/application/setting/SettingActivity;->mListAdapter:Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter;

    invoke-virtual {v6, v0}, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter;->setAboutItems(Ljava/util/List;)V

    .line 244
    iget-object v6, p0, Lcom/samsung/groupcast/application/setting/SettingActivity;->mListAdapter:Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter;

    if-eqz v6, :cond_0

    .line 245
    iget-object v6, p0, Lcom/samsung/groupcast/application/setting/SettingActivity;->mListAdapter:Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter;

    invoke-virtual {v6, v0}, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter;->setAboutItems(Ljava/util/List;)V

    .line 246
    :cond_0
    return-void

    .line 226
    .end local v0    # "aboutItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$AboutItem;>;"
    .end local v4    # "sbVersion":Ljava/lang/StringBuffer;
    :catch_0
    move-exception v1

    .line 228
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v1}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0
.end method

.method private updateInvitationAdapter()V
    .locals 6

    .prologue
    .line 206
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 208
    .local v0, "invitationItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$InvitationItem;>;"
    new-instance v1, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$InvitationItem;

    sget-object v2, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$InvitationEnum;->CHECK_PASSWORD:Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$InvitationEnum;

    const v3, 0x7f080040

    invoke-static {v3}, Lcom/samsung/groupcast/application/App;->getSanitizedString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {}, Lcom/samsung/groupcast/application/Preferences;->getCheckPasswordPreference()Z

    move-result v5

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$InvitationItem;-><init>(Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$InvitationEnum;Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 212
    iget-object v1, p0, Lcom/samsung/groupcast/application/setting/SettingActivity;->mListAdapter:Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter;

    invoke-virtual {v1, v0}, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter;->setInvotationItems(Ljava/util/List;)V

    .line 214
    iget-object v1, p0, Lcom/samsung/groupcast/application/setting/SettingActivity;->mListAdapter:Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter;

    if-eqz v1, :cond_0

    .line 215
    iget-object v1, p0, Lcom/samsung/groupcast/application/setting/SettingActivity;->mListAdapter:Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter;

    invoke-virtual {v1, v0}, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter;->setInvotationItems(Ljava/util/List;)V

    .line 216
    :cond_0
    return-void
.end method


# virtual methods
.method public isDisclaimerFragmentChecked(Z)V
    .locals 0
    .param p1, "mChecked"    # Z

    .prologue
    .line 252
    return-void
.end method

.method public onAboutItemSelected(Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$AboutItem;)V
    .locals 3
    .param p1, "item"    # Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$AboutItem;

    .prologue
    .line 189
    sget-object v1, Lcom/samsung/groupcast/application/setting/SettingActivity$1;->$SwitchMap$com$samsung$groupcast$application$setting$SettingActivityListAdapter$AboutEnum:[I

    invoke-virtual {p1}, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$AboutItem;->getItemId()Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$AboutEnum;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$AboutEnum;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 202
    :goto_0
    return-void

    .line 191
    :pswitch_0
    invoke-static {}, Lcom/samsung/groupcast/application/StatLog;->getInstance()Lcom/samsung/groupcast/application/StatLog;

    move-result-object v1

    sget-object v2, Lcom/samsung/groupcast/application/StatLog$Menu;->HELP:Lcom/samsung/groupcast/application/StatLog$Menu;

    invoke-virtual {v1, v2}, Lcom/samsung/groupcast/application/StatLog;->countMenu(Lcom/samsung/groupcast/application/StatLog$Menu;)V

    .line 192
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    const/4 v2, 0x1

    invoke-static {v2}, Lcom/samsung/groupcast/misc/utility/HelpConnectionUtil;->createFaqUrl(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 196
    .local v0, "intent":Landroid/content/Intent;
    invoke-static {p0, v0}, Lcom/samsung/groupcast/misc/utility/HelpConnectionUtil;->startInSBrowser(Landroid/app/Activity;Landroid/content/Intent;)V

    goto :goto_0

    .line 199
    .end local v0    # "intent":Landroid/content/Intent;
    :pswitch_1
    const-string v1, "ABOUT_NOTICE FOR WEB"

    const/4 v2, 0x0

    invoke-static {p0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 189
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v2, 0x1

    .line 49
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 51
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/setting/SettingActivity;->getRequestedOrientation()I

    move-result v1

    if-eq v2, v1, :cond_0

    .line 52
    invoke-virtual {p0, v2}, Lcom/samsung/groupcast/application/setting/SettingActivity;->setRequestedOrientation(I)V

    .line 55
    :cond_0
    const v1, 0x7f040014

    invoke-virtual {p0, v1}, Lcom/samsung/groupcast/application/setting/SettingActivity;->setContentView(I)V

    .line 57
    const v1, 0x7f07002f

    invoke-virtual {p0, v1}, Lcom/samsung/groupcast/application/setting/SettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    iput-object v1, p0, Lcom/samsung/groupcast/application/setting/SettingActivity;->mListView:Landroid/widget/ListView;

    .line 59
    new-instance v1, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter;

    invoke-virtual {p0}, Lcom/samsung/groupcast/application/setting/SettingActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/samsung/groupcast/application/setting/SettingActivity;->mListAdapter:Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter;

    .line 60
    iget-object v1, p0, Lcom/samsung/groupcast/application/setting/SettingActivity;->mListAdapter:Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter;

    invoke-virtual {v1, p0}, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter;->setDelegate(Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$Delegate;)V

    .line 61
    iget-object v1, p0, Lcom/samsung/groupcast/application/setting/SettingActivity;->mListAdapter:Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter;

    iget-object v2, p0, Lcom/samsung/groupcast/application/setting/SettingActivity;->mListView:Landroid/widget/ListView;

    invoke-virtual {v1, v2}, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter;->attachListView(Landroid/widget/ListView;)V

    .line 62
    invoke-static {}, Lcom/samsung/groupcast/application/Preferences;->getNicknamePreference()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/groupcast/application/setting/SettingActivity;->mNickName:Ljava/lang/String;

    .line 65
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 66
    .local v0, "nicknameItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$NicknameItem;>;"
    new-instance v1, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$NicknameItem;

    invoke-static {}, Lcom/samsung/groupcast/application/Preferences;->getNicknamePreference()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$NicknameItem;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 67
    iget-object v1, p0, Lcom/samsung/groupcast/application/setting/SettingActivity;->mListAdapter:Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter;

    invoke-virtual {v1, v0}, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter;->setNickNameItems(Ljava/util/List;)V

    .line 70
    invoke-direct {p0}, Lcom/samsung/groupcast/application/setting/SettingActivity;->updateInvitationAdapter()V

    .line 72
    invoke-direct {p0}, Lcom/samsung/groupcast/application/setting/SettingActivity;->updateAboutAdapter()V

    .line 73
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 95
    iget-object v0, p0, Lcom/samsung/groupcast/application/setting/SettingActivity;->TAG:Ljava/lang/String;

    const-string v1, "setting > onDestroy()"

    invoke-static {v0, v1}, Lcom/samsung/groupcast/application/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 97
    return-void
.end method

.method public onInvotationItemSelected(Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$InvitationItem;)V
    .locals 4
    .param p1, "item"    # Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$InvitationItem;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 167
    sget-object v2, Lcom/samsung/groupcast/application/setting/SettingActivity$1;->$SwitchMap$com$samsung$groupcast$application$setting$SettingActivityListAdapter$InvitationEnum:[I

    invoke-virtual {p1}, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$InvitationItem;->getItemId()Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$InvitationEnum;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$InvitationEnum;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 180
    :goto_0
    invoke-direct {p0}, Lcom/samsung/groupcast/application/setting/SettingActivity;->updateInvitationAdapter()V

    .line 181
    return-void

    .line 170
    :pswitch_0
    invoke-virtual {p1}, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$InvitationItem;->getChecked()Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-ne v2, v0, :cond_0

    move v0, v1

    .line 171
    .local v0, "forAction":Z
    :cond_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/groupcast/application/Preferences;->setCheckPasswordPreference(Ljava/lang/Boolean;)V

    .line 172
    if-eqz v0, :cond_1

    .line 173
    const v2, 0x7f080042

    invoke-static {p0, v2, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 176
    :cond_1
    const v2, 0x7f080056

    invoke-static {p0, v2, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 167
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onNicknameItemSelected(Ljava/lang/String;)V
    .locals 2
    .param p1, "nickName"    # Ljava/lang/String;

    .prologue
    .line 158
    iput-object p1, p0, Lcom/samsung/groupcast/application/setting/SettingActivity;->mNickName:Ljava/lang/String;

    .line 159
    invoke-static {}, Lcom/samsung/groupcast/application/StatLog;->getInstance()Lcom/samsung/groupcast/application/StatLog;

    move-result-object v0

    sget-object v1, Lcom/samsung/groupcast/application/StatLog$Menu;->NICK_NAME_SETTING:Lcom/samsung/groupcast/application/StatLog$Menu;

    invoke-virtual {v0, v1}, Lcom/samsung/groupcast/application/StatLog;->countMenu(Lcom/samsung/groupcast/application/StatLog$Menu;)V

    .line 160
    invoke-direct {p0}, Lcom/samsung/groupcast/application/setting/SettingActivity;->showNicknameInputDialog()V

    .line 161
    return-void
.end method

.method public onNicknameSubmitted(Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;Ljava/lang/String;Z)V
    .locals 2
    .param p1, "dialogFragment"    # Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;
    .param p2, "nickName"    # Ljava/lang/String;
    .param p3, "dialogCancelled"    # Z

    .prologue
    .line 102
    if-eqz p3, :cond_0

    .line 112
    :goto_0
    return-void

    .line 106
    :cond_0
    iput-object p2, p0, Lcom/samsung/groupcast/application/setting/SettingActivity;->mNickName:Ljava/lang/String;

    .line 107
    invoke-direct {p0}, Lcom/samsung/groupcast/application/setting/SettingActivity;->hideNicknameInputDialog()V

    .line 108
    iget-object v1, p0, Lcom/samsung/groupcast/application/setting/SettingActivity;->mNickName:Ljava/lang/String;

    invoke-static {v1}, Lcom/samsung/groupcast/application/Preferences;->setNicknamePreference(Ljava/lang/String;)V

    .line 109
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 110
    .local v0, "nicknameItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$NicknameItem;>;"
    new-instance v1, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$NicknameItem;

    invoke-direct {v1, p2}, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$NicknameItem;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 111
    iget-object v1, p0, Lcom/samsung/groupcast/application/setting/SettingActivity;->mListAdapter:Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter;

    invoke-virtual {v1, v0}, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter;->setNickNameItems(Ljava/util/List;)V

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 257
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 263
    :goto_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 259
    :pswitch_0
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/setting/SettingActivity;->onBackPressed()V

    goto :goto_0

    .line 257
    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 89
    iget-object v0, p0, Lcom/samsung/groupcast/application/setting/SettingActivity;->TAG:Ljava/lang/String;

    const-string v1, "setting > onPause()"

    invoke-static {v0, v1}, Lcom/samsung/groupcast/application/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 91
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 83
    iget-object v0, p0, Lcom/samsung/groupcast/application/setting/SettingActivity;->TAG:Ljava/lang/String;

    const-string v1, "setting > onResume()"

    invoke-static {v0, v1}, Lcom/samsung/groupcast/application/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 85
    return-void
.end method

.method protected onStart()V
    .locals 2

    .prologue
    .line 77
    iget-object v0, p0, Lcom/samsung/groupcast/application/setting/SettingActivity;->TAG:Ljava/lang/String;

    const-string v1, "setting > onStart()"

    invoke-static {v0, v1}, Lcom/samsung/groupcast/application/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    .line 79
    return-void
.end method
