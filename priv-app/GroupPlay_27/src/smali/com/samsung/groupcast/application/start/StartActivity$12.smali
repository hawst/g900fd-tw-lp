.class Lcom/samsung/groupcast/application/start/StartActivity$12;
.super Ljava/lang/Object;
.source "StartActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/groupcast/application/start/StartActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/groupcast/application/start/StartActivity;


# direct methods
.method constructor <init>(Lcom/samsung/groupcast/application/start/StartActivity;)V
    .locals 0

    .prologue
    .line 2310
    iput-object p1, p0, Lcom/samsung/groupcast/application/start/StartActivity$12;->this$0:Lcom/samsung/groupcast/application/start/StartActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 2314
    iget-object v3, p0, Lcom/samsung/groupcast/application/start/StartActivity$12;->this$0:Lcom/samsung/groupcast/application/start/StartActivity;

    invoke-virtual {v3}, Lcom/samsung/groupcast/application/start/StartActivity;->isFinishing()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2336
    :cond_0
    :goto_0
    return-void

    .line 2317
    :cond_1
    invoke-static {}, Lcom/samsung/groupcast/application/StatLog;->getInstance()Lcom/samsung/groupcast/application/StatLog;

    move-result-object v3

    sget-object v4, Lcom/samsung/groupcast/application/StatLog$Menu;->NICK_NAME_SETTING:Lcom/samsung/groupcast/application/StatLog$Menu;

    invoke-virtual {v3, v4}, Lcom/samsung/groupcast/application/StatLog;->countMenu(Lcom/samsung/groupcast/application/StatLog$Menu;)V

    .line 2318
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2319
    .local v0, "args":Landroid/os/Bundle;
    const-string v4, "Nickname"

    iget-object v3, p0, Lcom/samsung/groupcast/application/start/StartActivity$12;->this$0:Lcom/samsung/groupcast/application/start/StartActivity;

    # getter for: Lcom/samsung/groupcast/application/start/StartActivity;->mNickName:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/samsung/groupcast/application/start/StartActivity;->access$2000(Lcom/samsung/groupcast/application/start/StartActivity;)Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v4, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2321
    iget-object v3, p0, Lcom/samsung/groupcast/application/start/StartActivity$12;->this$0:Lcom/samsung/groupcast/application/start/StartActivity;

    invoke-virtual {v3}, Lcom/samsung/groupcast/application/start/StartActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/samsung/groupcast/application/start/StartActivity$12;->this$0:Lcom/samsung/groupcast/application/start/StartActivity;

    invoke-virtual {v3}, Lcom/samsung/groupcast/application/start/StartActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    const-string v4, "TAG_NICKNAME_DIALOG"

    invoke-virtual {v3, v4}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v3

    if-nez v3, :cond_0

    .line 2326
    :cond_2
    :try_start_0
    new-instance v2, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;

    invoke-direct {v2}, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;-><init>()V

    .line 2327
    .local v2, "fragment":Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;
    invoke-virtual {v2, v0}, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;->setArguments(Landroid/os/Bundle;)V

    .line 2329
    iget-object v3, p0, Lcom/samsung/groupcast/application/start/StartActivity$12;->this$0:Lcom/samsung/groupcast/application/start/StartActivity;

    invoke-virtual {v3}, Lcom/samsung/groupcast/application/start/StartActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    const-string v4, "TAG_NICKNAME_DIALOG"

    invoke-virtual {v2, v3, v4}, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2331
    .end local v2    # "fragment":Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;
    :catch_0
    move-exception v1

    .line 2332
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
