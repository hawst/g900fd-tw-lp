.class Lcom/samsung/groupcast/application/main/MainActivity$3;
.super Ljava/lang/Object;
.source "MainActivity.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/groupcast/application/main/MainActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/groupcast/application/main/MainActivity;


# direct methods
.method constructor <init>(Lcom/samsung/groupcast/application/main/MainActivity;)V
    .locals 0

    .prologue
    .line 727
    iput-object p1, p0, Lcom/samsung/groupcast/application/main/MainActivity$3;->this$0:Lcom/samsung/groupcast/application/main/MainActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2
    .param p1, "name"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 736
    const-string v0, "---"

    invoke-static {v0}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 737
    iget-object v0, p0, Lcom/samsung/groupcast/application/main/MainActivity$3;->this$0:Lcom/samsung/groupcast/application/main/MainActivity;

    check-cast p2, Lcom/samsung/groupcast/service/GroupPlayService$GroupPlayBinder;

    .end local p2    # "service":Landroid/os/IBinder;
    invoke-virtual {p2}, Lcom/samsung/groupcast/service/GroupPlayService$GroupPlayBinder;->getService()Lcom/samsung/groupcast/service/GroupPlayService;

    move-result-object v1

    # setter for: Lcom/samsung/groupcast/application/main/MainActivity;->mService:Lcom/samsung/groupcast/service/GroupPlayService;
    invoke-static {v0, v1}, Lcom/samsung/groupcast/application/main/MainActivity;->access$302(Lcom/samsung/groupcast/application/main/MainActivity;Lcom/samsung/groupcast/service/GroupPlayService;)Lcom/samsung/groupcast/service/GroupPlayService;

    .line 738
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1, "name"    # Landroid/content/ComponentName;

    .prologue
    .line 730
    const-string v0, "---"

    invoke-static {v0}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 731
    iget-object v0, p0, Lcom/samsung/groupcast/application/main/MainActivity$3;->this$0:Lcom/samsung/groupcast/application/main/MainActivity;

    const/4 v1, 0x0

    # setter for: Lcom/samsung/groupcast/application/main/MainActivity;->mService:Lcom/samsung/groupcast/service/GroupPlayService;
    invoke-static {v0, v1}, Lcom/samsung/groupcast/application/main/MainActivity;->access$302(Lcom/samsung/groupcast/application/main/MainActivity;Lcom/samsung/groupcast/service/GroupPlayService;)Lcom/samsung/groupcast/service/GroupPlayService;

    .line 732
    return-void
.end method
