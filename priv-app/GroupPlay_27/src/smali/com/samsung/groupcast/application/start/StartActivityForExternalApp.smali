.class public Lcom/samsung/groupcast/application/start/StartActivityForExternalApp;
.super Landroid/app/Activity;
.source "StartActivityForExternalApp.java"


# static fields
.field public static final EXTERNAL_APP_TYPE:Ljava/lang/String; = "ExternalAppContentType"

.field public static final IS_START_FROM_EXTERNAL:Ljava/lang/String; = "IsStartFromExternal"

.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    const-class v0, Lcom/samsung/groupcast/application/start/StartActivityForExternalApp;

    invoke-static {v0}, Lcom/samsung/groupcast/application/Logger;->getTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/groupcast/application/start/StartActivityForExternalApp;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method private startStartActivity()V
    .locals 3

    .prologue
    .line 26
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivityForExternalApp;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 27
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivityForExternalApp;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/samsung/groupcast/application/start/StartActivity;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 28
    invoke-static {}, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->HasSession()Z

    move-result v1

    if-nez v1, :cond_0

    .line 29
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 31
    :cond_0
    const-string v1, "IsStartFromExternal"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 32
    invoke-virtual {p0, v0}, Lcom/samsung/groupcast/application/start/StartActivityForExternalApp;->startActivity(Landroid/content/Intent;)V

    .line 33
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivityForExternalApp;->finish()V

    .line 34
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 20
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 21
    sget-object v0, Lcom/samsung/groupcast/application/start/StartActivityForExternalApp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[onCreate] version:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/samsung/groupcast/application/App;->getVersionName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/groupcast/application/Logger;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 22
    invoke-direct {p0}, Lcom/samsung/groupcast/application/start/StartActivityForExternalApp;->startStartActivity()V

    .line 23
    return-void
.end method
