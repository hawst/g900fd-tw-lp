.class Lcom/samsung/groupcast/application/stub/ApplicationManager$PackageInstallObserver;
.super Landroid/content/pm/IPackageInstallObserver$Stub;
.source "ApplicationManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/groupcast/application/stub/ApplicationManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "PackageInstallObserver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/groupcast/application/stub/ApplicationManager;


# direct methods
.method constructor <init>(Lcom/samsung/groupcast/application/stub/ApplicationManager;)V
    .locals 0

    .prologue
    .line 301
    iput-object p1, p0, Lcom/samsung/groupcast/application/stub/ApplicationManager$PackageInstallObserver;->this$0:Lcom/samsung/groupcast/application/stub/ApplicationManager;

    invoke-direct {p0}, Landroid/content/pm/IPackageInstallObserver$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public packageInstalled(Ljava/lang/String;I)V
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "returnCode"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 304
    iget-object v1, p0, Lcom/samsung/groupcast/application/stub/ApplicationManager$PackageInstallObserver;->this$0:Lcom/samsung/groupcast/application/stub/ApplicationManager;

    # getter for: Lcom/samsung/groupcast/application/stub/ApplicationManager;->onInstalledPackaged:Lcom/samsung/groupcast/application/stub/OnInstalledPackaged;
    invoke-static {v1}, Lcom/samsung/groupcast/application/stub/ApplicationManager;->access$000(Lcom/samsung/groupcast/application/stub/ApplicationManager;)Lcom/samsung/groupcast/application/stub/OnInstalledPackaged;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 306
    const/4 v1, 0x1

    if-eq p2, v1, :cond_0

    .line 311
    :cond_0
    iget-object v1, p0, Lcom/samsung/groupcast/application/stub/ApplicationManager$PackageInstallObserver;->this$0:Lcom/samsung/groupcast/application/stub/ApplicationManager;

    iput-object p1, v1, Lcom/samsung/groupcast/application/stub/ApplicationManager;->pkgname:Ljava/lang/String;

    .line 312
    iget-object v1, p0, Lcom/samsung/groupcast/application/stub/ApplicationManager$PackageInstallObserver;->this$0:Lcom/samsung/groupcast/application/stub/ApplicationManager;

    iput p2, v1, Lcom/samsung/groupcast/application/stub/ApplicationManager;->returncode:I

    .line 313
    iget-object v1, p0, Lcom/samsung/groupcast/application/stub/ApplicationManager$PackageInstallObserver;->this$0:Lcom/samsung/groupcast/application/stub/ApplicationManager;

    # getter for: Lcom/samsung/groupcast/application/stub/ApplicationManager;->handler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/samsung/groupcast/application/stub/ApplicationManager;->access$100(Lcom/samsung/groupcast/application/stub/ApplicationManager;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 314
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/samsung/groupcast/application/stub/ApplicationManager$PackageInstallObserver;->this$0:Lcom/samsung/groupcast/application/stub/ApplicationManager;

    # getter for: Lcom/samsung/groupcast/application/stub/ApplicationManager;->handler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/samsung/groupcast/application/stub/ApplicationManager;->access$100(Lcom/samsung/groupcast/application/stub/ApplicationManager;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 316
    .end local v0    # "msg":Landroid/os/Message;
    :cond_1
    return-void
.end method
