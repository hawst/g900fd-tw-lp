.class Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity$1;
.super Ljava/lang/Object;
.source "SharedExperienceActivity.java"

# interfaces
.implements Lcom/sec/android/band/BandInterface$BandEventHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;


# direct methods
.method constructor <init>(Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;)V
    .locals 0

    .prologue
    .line 160
    iput-object p1, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity$1;->this$0:Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onConfiguration(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)I
    .locals 16
    .param p1, "ssid"    # Ljava/lang/String;
    .param p2, "bssid"    # Ljava/lang/String;
    .param p3, "PeerScreenId"    # Ljava/lang/String;
    .param p4, "defSsid"    # Ljava/lang/String;
    .param p5, "defBssid"    # Ljava/lang/String;
    .param p6, "defPassphrase"    # Ljava/lang/String;
    .param p7, "defEncryption"    # Ljava/lang/String;
    .param p8, "defAuthentication"    # Ljava/lang/String;
    .param p9, "newConnection"    # Z

    .prologue
    .line 320
    const/4 v1, 0x0

    invoke-static {v1}, Lcom/samsung/groupcast/application/App;->SET_EXTERNAL_APP(Z)V

    .line 322
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity$1;->this$0:Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;

    # getter for: Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mCurrentActivityName:Ljava/lang/String;
    invoke-static {v1}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->access$000(Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/groupcast/application/App;->isTopActivity(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 323
    const/4 v1, 0x0

    .line 572
    :goto_0
    return v1

    .line 328
    :cond_0
    const-string v1, "GroupPlayBand"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onConfiguration ["

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "/"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiUtils;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getWifiBSSID()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "]"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 330
    if-eqz p2, :cond_6

    const-string v1, "00:00:00:00:00:00"

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getWifiBSSID()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_6

    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getWifiBSSID()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 333
    const-string v1, "GroupPlayBand"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "other Group ["

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "/"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiUtils;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getWifiBSSID()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "]"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 335
    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiAPUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->isRvfHotspotEnabled()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 336
    if-eqz p3, :cond_2

    const-string v1, ":"

    move-object/from16 v0, p3

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    array-length v1, v1

    const/4 v2, 0x1

    if-le v1, v2, :cond_1

    const-string v1, ":"

    move-object/from16 v0, p3

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    aget-object v1, v1, v2

    const-class v2, Lcom/samsung/groupcast/application/start/StartActivity;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 337
    :cond_1
    const-string v1, "GroupPlayBand"

    const-string v2, "return~!"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 338
    const/4 v1, 0x0

    goto/16 :goto_0

    .line 340
    :cond_2
    const-string v1, "GroupPlayBand"

    const-string v2, "maybe peer is connected Other Lagacy Moible Ap. we will command peer disconnect"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 355
    :cond_3
    invoke-static {}, Lcom/samsung/groupcast/application/App;->isRegister_Extra_App()Z

    move-result v1

    if-nez v1, :cond_9

    .line 356
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity$1;->this$0:Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;

    invoke-virtual {v1}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->getCurrentActivityName()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_8

    .line 357
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity$1;->this$0:Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;

    # getter for: Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->access$100(Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity$1;->this$0:Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;

    # getter for: Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mCurrentActivityName:Ljava/lang/String;
    invoke-static {v2}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->access$000(Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity$1;->this$0:Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;

    invoke-virtual {v2}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->getCurrentActivityName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 365
    .local v9, "MyScreenId":Ljava/lang/String;
    :goto_1
    const-string v1, ":"

    invoke-virtual {v9, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v10

    .line 367
    .local v10, "MyScreenId_splited":[Ljava/lang/String;
    if-eqz v10, :cond_4

    const/4 v1, 0x0

    aget-object v1, v10, v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity$1;->this$0:Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;

    invoke-virtual {v2}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 368
    const/4 v1, 0x1

    invoke-static {v1}, Lcom/samsung/groupcast/application/App;->SET_EXTERNAL_APP(Z)V

    .line 370
    :cond_4
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity$1;->this$0:Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;

    invoke-virtual {v1}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->getSession()Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    move-result-object v1

    if-nez v1, :cond_a

    .line 371
    const-string v1, "GroupPlayBand"

    const-string v2, "Session is null, return!"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 372
    const/4 v1, 0x0

    goto/16 :goto_0

    .line 344
    .end local v9    # "MyScreenId":Ljava/lang/String;
    .end local v10    # "MyScreenId_splited":[Ljava/lang/String;
    :cond_5
    const-string v1, "GroupPlayBand"

    const-string v2, "return : I\'m STA"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 345
    const/4 v1, 0x0

    goto/16 :goto_0

    .line 347
    :cond_6
    if-eqz p2, :cond_7

    const-string v1, "00:00:00:00:00:00"

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 348
    :cond_7
    if-eqz p3, :cond_3

    const-string v1, ":"

    move-object/from16 v0, p3

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    array-length v1, v1

    const/4 v2, 0x1

    if-le v1, v2, :cond_3

    const-string v1, ":"

    move-object/from16 v0, p3

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    aget-object v1, v1, v2

    const-class v2, Lcom/samsung/groupcast/application/start/StartActivity;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 350
    const-string v1, "GroupPlayBand"

    const-string v2, "Peer is disconnected! return~!"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 351
    const/4 v1, 0x0

    goto/16 :goto_0

    .line 361
    :cond_8
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity$1;->this$0:Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;

    # getter for: Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->access$100(Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity$1;->this$0:Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;

    # getter for: Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mCurrentActivityName:Ljava/lang/String;
    invoke-static {v2}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->access$000(Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .restart local v9    # "MyScreenId":Ljava/lang/String;
    goto/16 :goto_1

    .line 363
    .end local v9    # "MyScreenId":Ljava/lang/String;
    :cond_9
    invoke-static {}, Lcom/samsung/groupcast/application/App;->getScreenId()Ljava/lang/String;

    move-result-object v9

    .restart local v9    # "MyScreenId":Ljava/lang/String;
    goto/16 :goto_1

    .line 375
    .restart local v10    # "MyScreenId_splited":[Ljava/lang/String;
    :cond_a
    new-instance v15, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity$1;->this$0:Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;

    invoke-virtual {v1}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->getSession()Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    move-result-object v1

    invoke-direct {v15, v1}, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;-><init>(Lcom/samsung/groupcast/legacy/gp2/session/model/Session;)V

    .line 379
    .local v15, "ss":Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity$1;->this$0:Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;

    invoke-virtual {v1}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->getSession()Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->getPin()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_c

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity$1;->this$0:Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;

    invoke-virtual {v1}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->getSession()Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->getPin()Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_c

    .line 380
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v15}, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;->toBandString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ";"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity$1;->this$0:Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;

    invoke-virtual {v2}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->getSession()Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->getPin()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ";"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 384
    .local v7, "chunk":Ljava/lang/String;
    :goto_2
    const-string v1, "GroupPlayBand"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "chunk["

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "]"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 385
    const-string v1, "GroupPlayBand"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "chunk length:"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 387
    const-string v1, "GroupPlayBand"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "CONFIGURATION RECEIVE SERVICE_INFO:"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p3

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/groupcast/application/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 392
    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getWifiBSSID()Ljava/lang/String;

    move-result-object v3

    .line 393
    .local v3, "Bssid":Ljava/lang/String;
    if-nez p3, :cond_d

    .line 395
    const-string v1, "GroupPlayBand"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "peer is idle(GroupPlay is not launched), and isHotspot enabled:"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiAPUtils;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->isRvfHotspotEnabled()Z

    move-result v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 399
    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiAPUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->isRvfHotspotEnabled()Z

    move-result v1

    if-eqz v1, :cond_b

    .line 400
    const-string v1, "GroupPlayBand"

    const-string v2, "Peer is idle and I\'m not Station."

    invoke-static {v1, v2}, Lcom/samsung/groupcast/application/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 401
    const-string v1, "GroupPlayBand"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SEND SCREENID:"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/groupcast/application/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 403
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity$1;->this$0:Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;

    iget-object v1, v1, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mBandInterface:Lcom/sec/android/band/BandInterface;

    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getWifiSSID()Ljava/lang/String;

    move-result-object v2

    const-string v4, "GroupCast"

    const-string v5, "WPA2_PSK"

    const-string v6, "TKIP"

    new-instance v8, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity$1$1;

    move-object/from16 v0, p0

    invoke-direct {v8, v0}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity$1$1;-><init>(Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity$1;)V

    invoke-virtual/range {v1 .. v8}, Lcom/sec/android/band/BandInterface;->setServiceInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/band/BandInterface$BandActionListener;)V

    .line 572
    :cond_b
    :goto_3
    const/4 v1, 0x0

    goto/16 :goto_0

    .line 382
    .end local v3    # "Bssid":Ljava/lang/String;
    .end local v7    # "chunk":Ljava/lang/String;
    :cond_c
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v15}, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;->toBandString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ";"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .restart local v7    # "chunk":Ljava/lang/String;
    goto/16 :goto_2

    .line 420
    .restart local v3    # "Bssid":Ljava/lang/String;
    :cond_d
    const-string v1, "GroupPlayBand"

    const-string v2, "peer is launched(GP or with external app)"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 421
    const-string v1, ":"

    move-object/from16 v0, p3

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v11

    .line 423
    .local v11, "PeerScreenIdSplitted":[Ljava/lang/String;
    const/4 v1, 0x0

    aget-object v1, v11, v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity$1;->this$0:Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;

    # getter for: Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->access$100(Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_e

    .line 426
    const-string v1, "GroupPlayBand"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "other nfc app (like 3d Gallary) isHotspot:"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiAPUtils;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->isRvfHotspotEnabled()Z

    move-result v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 430
    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiAPUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->isRvfHotspotEnabled()Z

    move-result v1

    if-eqz v1, :cond_b

    .line 431
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity$1;->this$0:Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;

    iget-object v1, v1, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mBandInterface:Lcom/sec/android/band/BandInterface;

    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getWifiSSID()Ljava/lang/String;

    move-result-object v2

    const-string v4, "GroupCast"

    const-string v5, "WPA2_PSK"

    const-string v6, "TKIP"

    new-instance v8, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity$1$2;

    move-object/from16 v0, p0

    invoke-direct {v8, v0}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity$1$2;-><init>(Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity$1;)V

    invoke-virtual/range {v1 .. v8}, Lcom/sec/android/band/BandInterface;->setServiceInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/band/BandInterface$BandActionListener;)V

    goto/16 :goto_3

    .line 446
    :cond_e
    array-length v1, v11

    const/4 v2, 0x3

    if-ne v1, v2, :cond_11

    .line 448
    invoke-static {}, Lcom/samsung/groupcast/application/App;->getScreenId()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_f

    .line 449
    const-string v1, "GroupPlayBand"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "peer have local app servicetype(shared photo, doc...) or External App("

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Lcom/samsung/groupcast/application/App;->getScreenId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "/"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/4 v4, 0x1

    aget-object v4, v11, v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ":"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Lcom/samsung/groupcast/application/App;->getScreenId()Ljava/lang/String;

    move-result-object v4

    const-string v5, ":"

    invoke-virtual {v4, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    aget-object v4, v4, v5

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ")"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 461
    :goto_4
    invoke-static {}, Lcom/samsung/groupcast/application/App;->getScreenId()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_10

    const/4 v1, 0x1

    aget-object v1, v11, v1

    invoke-static {}, Lcom/samsung/groupcast/application/App;->getScreenId()Ljava/lang/String;

    move-result-object v2

    const-string v4, ":"

    invoke-virtual {v2, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x0

    aget-object v2, v2, v4

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_10

    .line 464
    invoke-static {}, Lcom/samsung/groupcast/application/App;->isRegister_Extra_App()Z

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_b

    .line 465
    const-string v1, "GroupPlayBand"

    const-string v2, "I have External App and peer has External App too!"

    invoke-static {v1, v2}, Lcom/samsung/groupcast/application/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 467
    const-string v1, "GroupPlayBand"

    const-string v2, "We will do not send NDEF_DISCOVERD!"

    invoke-static {v1, v2}, Lcom/samsung/groupcast/application/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 469
    new-instance v13, Landroid/os/Bundle;

    invoke-direct {v13}, Landroid/os/Bundle;-><init>()V

    .line 472
    .local v13, "bundle":Landroid/os/Bundle;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity$1;->this$0:Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;

    # getter for: Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->access$100(Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    move-object/from16 v0, p3

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v12

    .line 475
    .local v12, "PurePeerScreenId":Ljava/lang/String;
    const-string v1, "GroupPlayBand"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "removed screenid:"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/groupcast/application/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 478
    const-string v1, "com.samsung.groupcast.extra.SCREEN_ID"

    invoke-virtual {v13, v1, v12}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 479
    new-instance v14, Landroid/content/Intent;

    const-string v1, "com.samsung.groupcast.action.GP_SERVICE"

    invoke-direct {v14, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 480
    .local v14, "in":Landroid/content/Intent;
    invoke-virtual {v14, v13}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 481
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity$1;->this$0:Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;

    invoke-virtual {v1, v14}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 482
    const-string v1, "GroupPlayBand"

    const-string v2, "[GP] send GroupPlayManager.GP_SERVICE"

    invoke-static {v1, v2}, Lcom/samsung/groupcast/application/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 484
    const-string v1, "GroupPlayBand"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "     SCREENID : "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p3

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/groupcast/application/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 455
    .end local v12    # "PurePeerScreenId":Ljava/lang/String;
    .end local v13    # "bundle":Landroid/os/Bundle;
    .end local v14    # "in":Landroid/content/Intent;
    :cond_f
    const-string v1, "GroupPlayBand"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "peer have local app servicetype(shared photo, doc...) or External App("

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Lcom/samsung/groupcast/application/App;->getScreenId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "/"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/4 v4, 0x1

    aget-object v4, v11, v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ")"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_4

    .line 504
    :cond_10
    const-string v1, "GroupPlayBand"

    const-string v2, "peer have different local app or External App"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    .line 507
    :cond_11
    array-length v1, v11

    const/4 v2, 0x2

    if-ne v1, v2, :cond_b

    .line 509
    const-string v1, "GroupPlayBand"

    const-string v2, "peer is only GroupPlay launched"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 510
    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiAPUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->isRvfHotspotEnabled()Z

    move-result v1

    if-eqz v1, :cond_13

    const/4 v1, 0x1

    aget-object v1, v11, v1

    const-class v2, Lcom/samsung/groupcast/application/main/MainActivity;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_12

    invoke-static {}, Lcom/samsung/groupcast/application/App;->getScreenId()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_13

    .line 512
    :cond_12
    const-string v1, "GroupPlayBand"

    const-string v2, "I am host(Mobile AP Enabled) and peer is not MainActivity"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 515
    const-string v1, "GroupPlayBand"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SEND SERVICE_INFO SCREENID:"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/groupcast/application/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 517
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity$1;->this$0:Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;

    iget-object v1, v1, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mBandInterface:Lcom/sec/android/band/BandInterface;

    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getWifiSSID()Ljava/lang/String;

    move-result-object v2

    const-string v4, "GroupCast"

    const-string v5, "WPA2_PSK"

    const-string v6, "TKIP"

    new-instance v8, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity$1$3;

    move-object/from16 v0, p0

    invoke-direct {v8, v0}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity$1$3;-><init>(Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity$1;)V

    invoke-virtual/range {v1 .. v8}, Lcom/sec/android/band/BandInterface;->setServiceInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/band/BandInterface$BandActionListener;)V

    goto/16 :goto_3

    .line 532
    :cond_13
    const-string v1, "GroupPlayBand"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "My screenId is :"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 533
    if-eqz v10, :cond_b

    const/4 v1, 0x0

    aget-object v1, v10, v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity$1;->this$0:Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;

    invoke-virtual {v2}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_14

    array-length v1, v10

    const/4 v2, 0x2

    if-le v1, v2, :cond_b

    .line 539
    :cond_14
    const-string v1, "GroupPlayBand"

    const-string v2, "I have External App, peer is GP (Start or Main)"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 541
    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiAPUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->isRvfHotspotEnabled()Z

    move-result v1

    if-nez v1, :cond_15

    const-string v1, ":"

    move-object/from16 v0, p3

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    aget-object v1, v1, v2

    const-class v2, Lcom/samsung/groupcast/application/start/StartActivity;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_16

    :cond_15
    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiAPUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->isRvfHotspotEnabled()Z

    move-result v1

    if-eqz v1, :cond_b

    .line 545
    :cond_16
    const-string v1, "GroupPlayBand"

    const-string v2, "I have External App or local app, peer is (GP Main(only) And I\'m not Host) or I\'m Host"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 547
    const-string v1, "GroupPlayBand"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SEND SERVICE_INFO SCREENID:"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/groupcast/application/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 549
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity$1;->this$0:Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;

    iget-object v1, v1, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mBandInterface:Lcom/sec/android/band/BandInterface;

    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getWifiSSID()Ljava/lang/String;

    move-result-object v2

    const-string v4, "GroupCast"

    const-string v5, "WPA2_PSK"

    const-string v6, "TKIP"

    new-instance v8, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity$1$4;

    move-object/from16 v0, p0

    invoke-direct {v8, v0}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity$1$4;-><init>(Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity$1;)V

    invoke-virtual/range {v1 .. v8}, Lcom/sec/android/band/BandInterface;->setServiceInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/band/BandInterface$BandActionListener;)V

    goto/16 :goto_3
.end method

.method public onNdefDiscovered(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    .locals 13
    .param p1, "ssid"    # Ljava/lang/String;
    .param p2, "bssid"    # Ljava/lang/String;
    .param p3, "passphrase"    # Ljava/lang/String;
    .param p4, "chunk"    # Ljava/lang/String;

    .prologue
    .line 169
    const-string v10, "GroupPlayBand"

    const-string v11, "onNdefDiscovered"

    invoke-static {v10, v11}, Lcom/samsung/groupcast/application/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 170
    const/4 v10, 0x0

    invoke-static {v10}, Lcom/samsung/groupcast/application/App;->SET_EXTERNAL_APP(Z)V

    .line 171
    iget-object v10, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity$1;->this$0:Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;

    # getter for: Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mCurrentActivityName:Ljava/lang/String;
    invoke-static {v10}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->access$000(Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;)Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/samsung/groupcast/application/App;->isTopActivity(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_0

    .line 172
    const/4 v10, 0x0

    .line 309
    :goto_0
    return v10

    .line 174
    :cond_0
    if-eqz p2, :cond_1

    const-string v10, "00:00:00:00:00:00"

    invoke-virtual {p2, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_1

    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiUtils;

    move-result-object v10

    invoke-virtual {v10}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getWifiBSSID()Ljava/lang/String;

    move-result-object v10

    if-eqz v10, :cond_1

    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiUtils;

    move-result-object v10

    invoke-virtual {v10}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getWifiBSSID()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p2, v10}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_1

    .line 177
    const-string v10, "GroupPlayBand"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "other Group return ["

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "/"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiUtils;

    move-result-object v12

    invoke-virtual {v12}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getWifiBSSID()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "]"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 179
    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiAPUtils;

    move-result-object v10

    invoke-virtual {v10}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->isRvfHotspotEnabled()Z

    move-result v10

    if-nez v10, :cond_1

    .line 180
    const/4 v10, 0x0

    goto :goto_0

    .line 183
    :cond_1
    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiAPUtils;

    move-result-object v10

    invoke-virtual {v10}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->isRvfHotspotEnabled()Z

    move-result v10

    if-nez v10, :cond_2

    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiUtils;

    move-result-object v10

    invoke-virtual {v10}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->isWifiEnabled()Z

    move-result v10

    if-nez v10, :cond_2

    .line 185
    const-string v10, "GroupPlayBand"

    const-string v11, "My network is disconnected by user! return~!"

    invoke-static {v10, v11}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 186
    const/4 v10, 0x0

    goto :goto_0

    .line 190
    :cond_2
    invoke-static {}, Lcom/samsung/groupcast/application/App;->isRegister_Extra_App()Z

    move-result v10

    if-nez v10, :cond_6

    .line 191
    iget-object v10, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity$1;->this$0:Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;

    invoke-virtual {v10}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->getCurrentActivityName()Ljava/lang/String;

    move-result-object v10

    if-eqz v10, :cond_5

    .line 192
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v11, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity$1;->this$0:Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;

    # getter for: Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mContext:Landroid/content/Context;
    invoke-static {v11}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->access$100(Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;)Landroid/content/Context;

    move-result-object v11

    invoke-virtual {v11}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ":"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity$1;->this$0:Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;

    # getter for: Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mCurrentActivityName:Ljava/lang/String;
    invoke-static {v11}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->access$000(Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ":"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity$1;->this$0:Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;

    invoke-virtual {v11}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->getCurrentActivityName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 200
    .local v1, "MyScreenId":Ljava/lang/String;
    :goto_1
    const-string v10, ":"

    invoke-virtual {v1, v10}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 202
    .local v2, "MyScreenId_splited":[Ljava/lang/String;
    const/4 v3, 0x0

    .line 204
    .local v3, "PeerScreenId":Ljava/lang/String;
    if-eqz p4, :cond_3

    .line 205
    const-string v10, ";"

    move-object/from16 v0, p4

    invoke-virtual {v0, v10}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    .line 206
    .local v7, "chunk_splitted":[Ljava/lang/String;
    array-length v10, v7

    add-int/lit8 v10, v10, -0x1

    aget-object v3, v7, v10

    .line 207
    invoke-static {v3}, Lcom/samsung/groupcast/application/App;->setPeerScreenId(Ljava/lang/String;)V

    .line 210
    .end local v7    # "chunk_splitted":[Ljava/lang/String;
    :cond_3
    const-string v10, "GroupPlayBand"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "NDEF_DISCOVERD RECEIVED SCREENID:"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/samsung/groupcast/application/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 213
    const-string v10, "GroupPlayBand"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "[GP] receive onNdefDiscovered : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-object v12, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity$1;->this$0:Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;

    # getter for: Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mContext:Landroid/content/Context;
    invoke-static {v12}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->access$100(Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;)Landroid/content/Context;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/samsung/groupcast/application/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 215
    const-string v10, "GroupPlayBand"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "         isRegister_Extra_App:"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-static {}, Lcom/samsung/groupcast/application/App;->isRegister_Extra_App()Z

    move-result v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/samsung/groupcast/application/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 218
    invoke-static {}, Lcom/samsung/groupcast/application/App;->isRegister_Extra_App()Z

    move-result v10

    const/4 v11, 0x1

    if-ne v10, v11, :cond_7

    .line 219
    const-string v10, "GroupPlayBand"

    const-string v11, "I have External App "

    invoke-static {v10, v11}, Lcom/samsung/groupcast/application/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 220
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 221
    .local v6, "bundle":Landroid/os/Bundle;
    const-string v10, "com.samsung.groupcast.extra.SCREEN_ID"

    invoke-virtual {v6, v10, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 222
    new-instance v9, Landroid/content/Intent;

    const-string v10, "com.samsung.groupcast.action.GP_SERVICE"

    invoke-direct {v9, v10}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 223
    .local v9, "in":Landroid/content/Intent;
    invoke-virtual {v9, v6}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 224
    iget-object v10, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity$1;->this$0:Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;

    invoke-virtual {v10, v9}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 225
    const-string v10, "GroupPlayBand"

    const-string v11, "[GP] send GroupPlayManager.GP_SERVICE"

    invoke-static {v10, v11}, Lcom/samsung/groupcast/application/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 226
    const-string v10, "GroupPlayBand"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "     SCREENID : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/samsung/groupcast/application/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 309
    .end local v6    # "bundle":Landroid/os/Bundle;
    .end local v9    # "in":Landroid/content/Intent;
    :cond_4
    :goto_2
    const/4 v10, 0x0

    goto/16 :goto_0

    .line 196
    .end local v1    # "MyScreenId":Ljava/lang/String;
    .end local v2    # "MyScreenId_splited":[Ljava/lang/String;
    .end local v3    # "PeerScreenId":Ljava/lang/String;
    :cond_5
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v11, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity$1;->this$0:Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;

    # getter for: Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mContext:Landroid/content/Context;
    invoke-static {v11}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->access$100(Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;)Landroid/content/Context;

    move-result-object v11

    invoke-virtual {v11}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ":"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity$1;->this$0:Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;

    # getter for: Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mCurrentActivityName:Ljava/lang/String;
    invoke-static {v11}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->access$000(Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .restart local v1    # "MyScreenId":Ljava/lang/String;
    goto/16 :goto_1

    .line 198
    .end local v1    # "MyScreenId":Ljava/lang/String;
    :cond_6
    invoke-static {}, Lcom/samsung/groupcast/application/App;->getScreenId()Ljava/lang/String;

    move-result-object v1

    .restart local v1    # "MyScreenId":Ljava/lang/String;
    goto/16 :goto_1

    .line 228
    .restart local v2    # "MyScreenId_splited":[Ljava/lang/String;
    .restart local v3    # "PeerScreenId":Ljava/lang/String;
    :cond_7
    const-string v10, "GroupPlayBand"

    const-string v11, "I don\'t have External App"

    invoke-static {v10, v11}, Lcom/samsung/groupcast/application/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 229
    invoke-static {}, Lcom/samsung/groupcast/application/App;->getPeerScreenId()Ljava/lang/String;

    move-result-object v10

    if-nez v10, :cond_8

    .line 230
    const-string v10, "GroupPlayBand"

    const-string v11, "SCREENID is null return"

    invoke-static {v10, v11}, Lcom/samsung/groupcast/application/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 231
    const/4 v10, 0x0

    goto/16 :goto_0

    .line 234
    :cond_8
    invoke-static {}, Lcom/samsung/groupcast/application/App;->getPeerScreenId()Ljava/lang/String;

    move-result-object v10

    const-string v11, ":"

    invoke-virtual {v10, v11}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 235
    .local v4, "PeerScreenId_splited":[Ljava/lang/String;
    const-string v10, "GroupPlayBand"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "["

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-object v12, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity$1;->this$0:Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;

    # getter for: Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mContext:Landroid/content/Context;
    invoke-static {v12}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->access$100(Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;)Landroid/content/Context;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "] split_data[0] :"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const/4 v12, 0x0

    aget-object v12, v4, v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/samsung/groupcast/application/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 237
    const-string v10, "GroupPlayBand"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "["

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-object v12, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity$1;->this$0:Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;

    # getter for: Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mContext:Landroid/content/Context;
    invoke-static {v12}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->access$100(Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;)Landroid/content/Context;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "] split_data[1] :"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const/4 v12, 0x1

    aget-object v12, v4, v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/samsung/groupcast/application/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 239
    const/4 v10, 0x0

    aget-object v10, v4, v10

    iget-object v11, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity$1;->this$0:Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;

    invoke-virtual {v11}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->getPackageName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_f

    .line 241
    const-string v10, "GroupPlayBand"

    const-string v11, "peer have external App."

    invoke-static {v10, v11}, Lcom/samsung/groupcast/application/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 243
    const/4 v10, 0x0

    aget-object v10, v2, v10

    iget-object v11, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity$1;->this$0:Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;

    invoke-virtual {v11}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->getPackageName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_9

    .line 244
    const-string v10, "GroupPlayBand"

    const-string v11, "I\'m Main or local service."

    invoke-static {v10, v11}, Lcom/samsung/groupcast/application/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 245
    array-length v10, v2

    const/4 v11, 0x3

    if-lt v10, v11, :cond_9

    .line 246
    const-string v10, "GroupPlayBand"

    const-string v11, "I\'m local service. return"

    invoke-static {v10, v11}, Lcom/samsung/groupcast/application/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 247
    const/4 v10, 0x0

    goto/16 :goto_0

    .line 251
    :cond_9
    const-string v10, "com.sec.android.app.mediasync"

    const/4 v11, 0x0

    aget-object v11, v4, v11

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_c

    .line 252
    const-string v10, "GroupPlayBand"

    const-string v11, "peer is MLS."

    invoke-static {v10, v11}, Lcom/samsung/groupcast/application/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 254
    iget-object v10, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity$1;->this$0:Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;

    # getter for: Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mContext:Landroid/content/Context;
    invoke-static {v10}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->access$100(Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;)Landroid/content/Context;

    move-result-object v10

    invoke-virtual {v10}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    check-cast v5, Lcom/samsung/groupcast/application/App;

    .line 260
    .local v5, "app":Lcom/samsung/groupcast/application/App;
    const-string v10, "com.sec.android.app.mediasync"

    invoke-virtual {v5, v10}, Lcom/samsung/groupcast/application/App;->isRunning(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_b

    .line 263
    iget-object v10, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity$1;->this$0:Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;

    const-string v11, "MUSIC_LIVE_SHARE_SCREEN"

    iput-object v11, v10, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mServiceType:Ljava/lang/String;

    .line 264
    array-length v10, v4

    const/4 v11, 0x2

    if-le v10, v11, :cond_a

    .line 265
    iget-object v10, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity$1;->this$0:Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;

    const/4 v11, 0x2

    aget-object v11, v4, v11

    iput-object v11, v10, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mMenuType:Ljava/lang/String;

    .line 267
    :cond_a
    iget-object v10, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity$1;->this$0:Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;

    const-string v11, "com.samsung.groupcast"

    const/4 v12, 0x0

    invoke-virtual {v10, v11, v12}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->IntentMusicLiveShareReceiver(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 269
    :cond_b
    const/4 v10, 0x0

    invoke-static {v10}, Lcom/samsung/groupcast/application/App;->setPeerScreenId(Ljava/lang/String;)V

    .line 270
    iget-object v10, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity$1;->this$0:Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;

    const/4 v11, 0x0

    iput-object v11, v10, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mServiceType:Ljava/lang/String;

    .line 271
    iget-object v10, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity$1;->this$0:Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;

    const/4 v11, 0x0

    iput-object v11, v10, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mMenuType:Ljava/lang/String;

    goto/16 :goto_2

    .line 272
    .end local v5    # "app":Lcom/samsung/groupcast/application/App;
    :cond_c
    const-string v10, "com.sec.android.app.mv.player"

    const/4 v11, 0x0

    aget-object v11, v4, v11

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_e

    .line 274
    const-string v10, "GroupPlayBand"

    const-string v11, "peer is ShareVideo."

    invoke-static {v10, v11}, Lcom/samsung/groupcast/application/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 275
    iget-object v10, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity$1;->this$0:Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;

    # getter for: Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mContext:Landroid/content/Context;
    invoke-static {v10}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->access$100(Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;)Landroid/content/Context;

    move-result-object v10

    invoke-virtual {v10}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    check-cast v5, Lcom/samsung/groupcast/application/App;

    .line 277
    .restart local v5    # "app":Lcom/samsung/groupcast/application/App;
    const-string v10, "com.sec.android.app.mv.player"

    invoke-virtual {v5, v10}, Lcom/samsung/groupcast/application/App;->isRunning(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_4

    .line 278
    iget-object v10, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity$1;->this$0:Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;

    const-string v11, "com.sec.android.app.mv.player"

    iput-object v11, v10, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mServiceType:Ljava/lang/String;

    .line 279
    array-length v10, v4

    const/4 v11, 0x2

    if-le v10, v11, :cond_d

    .line 280
    iget-object v10, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity$1;->this$0:Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;

    const/4 v11, 0x2

    aget-object v11, v4, v11

    iput-object v11, v10, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mMenuType:Ljava/lang/String;

    .line 282
    :cond_d
    iget-object v10, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity$1;->this$0:Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;

    const-string v11, ""

    const-string v12, ""

    invoke-virtual {v10, v11, v12}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->IntentShareVideo(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 285
    .end local v5    # "app":Lcom/samsung/groupcast/application/App;
    :cond_e
    const-string v10, "GroupPlayBand"

    const-string v11, "peer is other externl app"

    invoke-static {v10, v11}, Lcom/samsung/groupcast/application/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 290
    :try_start_0
    iget-object v10, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity$1;->this$0:Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;

    invoke-virtual {v10}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->getApplication()Landroid/app/Application;

    move-result-object v5

    check-cast v5, Lcom/samsung/groupcast/application/App;

    .line 291
    .restart local v5    # "app":Lcom/samsung/groupcast/application/App;
    const/4 v10, 0x0

    aget-object v10, v4, v10

    invoke-virtual {v5, v10}, Lcom/samsung/groupcast/application/App;->startLaunchActivity(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_2

    .line 292
    .end local v5    # "app":Lcom/samsung/groupcast/application/App;
    :catch_0
    move-exception v8

    .line 293
    .local v8, "e":Landroid/content/ActivityNotFoundException;
    const-string v10, "GroupPlayBand"

    const-string v11, "[GP] Error : startActivity()"

    invoke-static {v10, v11}, Lcom/samsung/groupcast/application/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 297
    .end local v8    # "e":Landroid/content/ActivityNotFoundException;
    :cond_f
    array-length v10, v4

    const/4 v11, 0x2

    if-le v10, v11, :cond_4

    .line 298
    array-length v10, v2

    const/4 v11, 0x2

    if-le v10, v11, :cond_10

    .line 299
    const/4 v10, 0x0

    goto/16 :goto_0

    .line 301
    :cond_10
    iget-object v10, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity$1;->this$0:Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;

    const/4 v11, 0x2

    aget-object v11, v4, v11

    iput-object v11, v10, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mServiceType:Ljava/lang/String;

    .line 302
    iget-object v10, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity$1;->this$0:Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;

    iget-object v11, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity$1;->this$0:Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;

    # getter for: Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mManifestGenerator:Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;
    invoke-static {v11}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->access$200(Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;)Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->onManifestUpdated(Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;)V

    goto/16 :goto_2
.end method

.method public onNetworkStatus(Ljava/lang/String;)I
    .locals 1
    .param p1, "connectionStatus"    # Ljava/lang/String;

    .prologue
    .line 164
    const/4 v0, 0x0

    return v0
.end method

.method public onPreNdefDiscovered()I
    .locals 5

    .prologue
    const/4 v1, -0x1

    const/4 v2, 0x0

    .line 584
    iget-object v3, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity$1;->this$0:Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;

    # getter for: Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mCurrentActivityName:Ljava/lang/String;
    invoke-static {v3}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->access$000(Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/samsung/groupcast/application/App;->isTopActivity(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 585
    const-string v2, "GroupPlayBand"

    const-string v3, "isTopActivity false!"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 640
    :cond_0
    :goto_0
    return v1

    .line 588
    :cond_1
    invoke-static {}, Lcom/samsung/groupcast/application/App;->HAS_EXTERNAL_APP()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 589
    const-string v3, "GroupPlayBand"

    const-string v4, "HAS_EXTERNAL_APP!"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 590
    invoke-static {v2}, Lcom/samsung/groupcast/application/App;->SET_EXTERNAL_APP(Z)V

    .line 591
    iget-object v3, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity$1;->this$0:Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;

    # getter for: Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mCurrentActivityName:Ljava/lang/String;
    invoke-static {v3}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->access$000(Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/samsung/groupcast/application/App;->isTopActivity(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    move v1, v2

    .line 592
    goto :goto_0

    .line 597
    :cond_2
    iget-object v3, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity$1;->this$0:Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;

    invoke-virtual {v3}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/samsung/groupcast/application/App;

    .line 603
    .local v0, "app":Lcom/samsung/groupcast/application/App;
    const-string v3, "com.sec.android.app.mediasync"

    invoke-virtual {v0, v3}, Lcom/samsung/groupcast/application/App;->isRunning(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 605
    const-string v2, "GroupPlayBand"

    const-string v3, "MLS is running!"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 606
    const-string v2, "GroupPlayBand"

    const-string v3, "for MLS Externl app issue (MusicServerActivity is not External app.)"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 612
    :cond_3
    iget-object v1, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity$1;->this$0:Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;

    # getter for: Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->access$100(Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/groupcast/application/App;->isForeground(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 613
    const-string v1, "GroupPlayBand"

    const-string v3, "I\'m in foreground!"

    invoke-static {v1, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move v1, v2

    .line 614
    goto :goto_0

    .line 618
    :cond_4
    invoke-static {}, Lcom/samsung/groupcast/application/App;->IsSbeamOrGalleryForeground()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 619
    const-string v1, "GroupPlayBand"

    const-string v3, "sbeam or gallery is foreground!"

    invoke-static {v1, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move v1, v2

    .line 620
    goto :goto_0

    .line 624
    :cond_5
    const-string v1, "GroupPlayBand"

    const-string v3, "I\'m in background!"

    invoke-static {v1, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 626
    iget-object v1, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity$1;->this$0:Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;

    # getter for: Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->access$100(Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/groupcast/application/App;->moveToTaskForeGround(Ljava/lang/String;)V

    move v1, v2

    .line 640
    goto :goto_0
.end method

.method public onRequestNdefMessage()Landroid/nfc/NdefMessage;
    .locals 1

    .prologue
    .line 578
    const/4 v0, 0x0

    return-object v0
.end method
