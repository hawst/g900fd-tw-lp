.class public Lcom/samsung/groupcast/application/SideWorkingQueue;
.super Ljava/lang/Object;
.source "SideWorkingQueue.java"


# static fields
.field private static sHandler:Landroid/os/Handler;

.field private static sInstance:Lcom/samsung/groupcast/application/SideWorkingQueue;


# instance fields
.field private mHT:Landroid/os/HandlerThread;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    new-instance v0, Lcom/samsung/groupcast/application/SideWorkingQueue;

    invoke-direct {v0}, Lcom/samsung/groupcast/application/SideWorkingQueue;-><init>()V

    sput-object v0, Lcom/samsung/groupcast/application/SideWorkingQueue;->sInstance:Lcom/samsung/groupcast/application/SideWorkingQueue;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "GP_SIDE_WORKER"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/samsung/groupcast/application/SideWorkingQueue;->mHT:Landroid/os/HandlerThread;

    .line 13
    iget-object v0, p0, Lcom/samsung/groupcast/application/SideWorkingQueue;->mHT:Landroid/os/HandlerThread;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/HandlerThread;->setDaemon(Z)V

    .line 14
    iget-object v0, p0, Lcom/samsung/groupcast/application/SideWorkingQueue;->mHT:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 15
    new-instance v0, Landroid/os/Handler;

    iget-object v1, p0, Lcom/samsung/groupcast/application/SideWorkingQueue;->mHT:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sput-object v0, Lcom/samsung/groupcast/application/SideWorkingQueue;->sHandler:Landroid/os/Handler;

    .line 16
    return-void
.end method

.method public static cancel(Ljava/lang/Runnable;)V
    .locals 1
    .param p0, "runnable"    # Ljava/lang/Runnable;

    .prologue
    .line 31
    sget-object v0, Lcom/samsung/groupcast/application/SideWorkingQueue;->sHandler:Landroid/os/Handler;

    invoke-virtual {v0, p0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 32
    return-void
.end method

.method public static post(Ljava/lang/Runnable;)V
    .locals 1
    .param p0, "runnable"    # Ljava/lang/Runnable;

    .prologue
    .line 23
    sget-object v0, Lcom/samsung/groupcast/application/SideWorkingQueue;->sHandler:Landroid/os/Handler;

    invoke-virtual {v0, p0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 24
    return-void
.end method

.method public static postDelayed(Ljava/lang/Runnable;J)V
    .locals 1
    .param p0, "runnable"    # Ljava/lang/Runnable;
    .param p1, "delayMilliseconds"    # J

    .prologue
    .line 27
    sget-object v0, Lcom/samsung/groupcast/application/SideWorkingQueue;->sHandler:Landroid/os/Handler;

    invoke-virtual {v0, p0, p1, p2}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 28
    return-void
.end method


# virtual methods
.method public getInstance()Lcom/samsung/groupcast/application/SideWorkingQueue;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/samsung/groupcast/application/SideWorkingQueue;->sInstance:Lcom/samsung/groupcast/application/SideWorkingQueue;

    return-object v0
.end method
