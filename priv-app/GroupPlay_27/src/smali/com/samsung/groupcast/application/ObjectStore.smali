.class public Lcom/samsung/groupcast/application/ObjectStore;
.super Ljava/lang/Object;
.source "ObjectStore.java"


# static fields
.field private static final INSTANCE:Lcom/samsung/groupcast/application/ObjectStore;

.field private static final NULL_OBJECT_ID:I


# instance fields
.field private mCounter:I

.field private final mObjects:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 6
    new-instance v0, Lcom/samsung/groupcast/application/ObjectStore;

    invoke-direct {v0}, Lcom/samsung/groupcast/application/ObjectStore;-><init>()V

    sput-object v0, Lcom/samsung/groupcast/application/ObjectStore;->INSTANCE:Lcom/samsung/groupcast/application/ObjectStore;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/groupcast/application/ObjectStore;->mObjects:Ljava/util/HashMap;

    .line 10
    const/4 v0, 0x1

    iput v0, p0, Lcom/samsung/groupcast/application/ObjectStore;->mCounter:I

    .line 12
    return-void
.end method

.method public static declared-synchronized getHash()I
    .locals 2

    .prologue
    .line 53
    const-class v1, Lcom/samsung/groupcast/application/ObjectStore;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/samsung/groupcast/application/ObjectStore;->INSTANCE:Lcom/samsung/groupcast/application/ObjectStore;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit v1

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized isValid(I)Z
    .locals 2
    .param p0, "hash"    # I

    .prologue
    .line 49
    const-class v1, Lcom/samsung/groupcast/application/ObjectStore;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/samsung/groupcast/application/ObjectStore;->INSTANCE:Lcom/samsung/groupcast/application/ObjectStore;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-ne v0, p0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized release(I)Ljava/lang/Object;
    .locals 8
    .param p0, "id"    # I

    .prologue
    .line 29
    const-class v2, Lcom/samsung/groupcast/application/ObjectStore;

    monitor-enter v2

    if-nez p0, :cond_0

    .line 30
    :try_start_0
    const-class v1, Lcom/samsung/groupcast/application/ObjectStore;

    const-string v3, "release"

    const-string v4, "id is NULL_OBJECT_ID, returning null"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    const-string v7, "count"

    aput-object v7, v5, v6

    const/4 v6, 0x1

    sget-object v7, Lcom/samsung/groupcast/application/ObjectStore;->INSTANCE:Lcom/samsung/groupcast/application/ObjectStore;

    iget-object v7, v7, Lcom/samsung/groupcast/application/ObjectStore;->mObjects:Ljava/util/HashMap;

    invoke-virtual {v7}, Ljava/util/HashMap;->size()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v1, v3, v4, v5}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 32
    const/4 v0, 0x0

    .line 45
    :goto_0
    monitor-exit v2

    return-object v0

    .line 35
    :cond_0
    :try_start_1
    sget-object v1, Lcom/samsung/groupcast/application/ObjectStore;->INSTANCE:Lcom/samsung/groupcast/application/ObjectStore;

    iget-object v1, v1, Lcom/samsung/groupcast/application/ObjectStore;->mObjects:Ljava/util/HashMap;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 37
    .local v0, "object":Ljava/lang/Object;
    if-eqz v0, :cond_1

    .line 38
    const-class v1, Lcom/samsung/groupcast/application/ObjectStore;

    const-string v3, "release"

    const/4 v4, 0x0

    const/4 v5, 0x6

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    const-string v7, "id"

    aput-object v7, v5, v6

    const/4 v6, 0x1

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x2

    const-string v7, "count"

    aput-object v7, v5, v6

    const/4 v6, 0x3

    sget-object v7, Lcom/samsung/groupcast/application/ObjectStore;->INSTANCE:Lcom/samsung/groupcast/application/ObjectStore;

    iget-object v7, v7, Lcom/samsung/groupcast/application/ObjectStore;->mObjects:Ljava/util/HashMap;

    invoke-virtual {v7}, Ljava/util/HashMap;->size()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x4

    const-string v7, "object"

    aput-object v7, v5, v6

    const/4 v6, 0x5

    aput-object v0, v5, v6

    invoke-static {v1, v3, v4, v5}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 29
    .end local v0    # "object":Ljava/lang/Object;
    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1

    .line 41
    .restart local v0    # "object":Ljava/lang/Object;
    :cond_1
    :try_start_2
    const-class v1, Lcom/samsung/groupcast/application/ObjectStore;

    const-string v3, "release"

    const-string v4, "object not found"

    const/4 v5, 0x4

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    const-string v7, "id"

    aput-object v7, v5, v6

    const/4 v6, 0x1

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x2

    const-string v7, "count"

    aput-object v7, v5, v6

    const/4 v6, 0x3

    sget-object v7, Lcom/samsung/groupcast/application/ObjectStore;->INSTANCE:Lcom/samsung/groupcast/application/ObjectStore;

    iget-object v7, v7, Lcom/samsung/groupcast/application/ObjectStore;->mObjects:Ljava/util/HashMap;

    invoke-virtual {v7}, Ljava/util/HashMap;->size()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v1, v3, v4, v5}, Lcom/samsung/groupcast/application/Logger;->wx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public static declared-synchronized retain(Ljava/lang/Object;)I
    .locals 8
    .param p0, "object"    # Ljava/lang/Object;

    .prologue
    const/4 v0, 0x0

    .line 15
    const-class v2, Lcom/samsung/groupcast/application/ObjectStore;

    monitor-enter v2

    if-nez p0, :cond_0

    .line 16
    :try_start_0
    const-class v1, Lcom/samsung/groupcast/application/ObjectStore;

    const-string v3, "retain"

    const-string v4, "object is null, returning null"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    const-string v7, "count"

    aput-object v7, v5, v6

    const/4 v6, 0x1

    sget-object v7, Lcom/samsung/groupcast/application/ObjectStore;->INSTANCE:Lcom/samsung/groupcast/application/ObjectStore;

    iget-object v7, v7, Lcom/samsung/groupcast/application/ObjectStore;->mObjects:Ljava/util/HashMap;

    invoke-virtual {v7}, Ljava/util/HashMap;->size()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v1, v3, v4, v5}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 25
    :goto_0
    monitor-exit v2

    return v0

    .line 21
    :cond_0
    :try_start_1
    sget-object v1, Lcom/samsung/groupcast/application/ObjectStore;->INSTANCE:Lcom/samsung/groupcast/application/ObjectStore;

    iget v0, v1, Lcom/samsung/groupcast/application/ObjectStore;->mCounter:I

    add-int/lit8 v3, v0, 0x1

    iput v3, v1, Lcom/samsung/groupcast/application/ObjectStore;->mCounter:I

    .line 22
    .local v0, "id":I
    sget-object v1, Lcom/samsung/groupcast/application/ObjectStore;->INSTANCE:Lcom/samsung/groupcast/application/ObjectStore;

    iget-object v1, v1, Lcom/samsung/groupcast/application/ObjectStore;->mObjects:Ljava/util/HashMap;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v3, p0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 23
    const-class v1, Lcom/samsung/groupcast/application/ObjectStore;

    const-string v3, "retain"

    const/4 v4, 0x0

    const/4 v5, 0x6

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    const-string v7, "id"

    aput-object v7, v5, v6

    const/4 v6, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x2

    const-string v7, "count"

    aput-object v7, v5, v6

    const/4 v6, 0x3

    sget-object v7, Lcom/samsung/groupcast/application/ObjectStore;->INSTANCE:Lcom/samsung/groupcast/application/ObjectStore;

    iget-object v7, v7, Lcom/samsung/groupcast/application/ObjectStore;->mObjects:Ljava/util/HashMap;

    invoke-virtual {v7}, Ljava/util/HashMap;->size()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x4

    const-string v7, "object"

    aput-object v7, v5, v6

    const/4 v6, 0x5

    aput-object p0, v5, v6

    invoke-static {v1, v3, v4, v5}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 15
    .end local v0    # "id":I
    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1
.end method
