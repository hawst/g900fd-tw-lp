.class Lcom/samsung/groupcast/application/fragment/LicenseDialogFragment$1;
.super Landroid/webkit/WebViewClient;
.source "LicenseDialogFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/groupcast/application/fragment/LicenseDialogFragment;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/groupcast/application/fragment/LicenseDialogFragment;


# direct methods
.method constructor <init>(Lcom/samsung/groupcast/application/fragment/LicenseDialogFragment;)V
    .locals 0

    .prologue
    .line 84
    iput-object p1, p0, Lcom/samsung/groupcast/application/fragment/LicenseDialogFragment$1;->this$0:Lcom/samsung/groupcast/application/fragment/LicenseDialogFragment;

    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    return-void
.end method


# virtual methods
.method public onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 4
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "url"    # Ljava/lang/String;

    .prologue
    .line 112
    :try_start_0
    iget-object v2, p0, Lcom/samsung/groupcast/application/fragment/LicenseDialogFragment$1;->this$0:Lcom/samsung/groupcast/application/fragment/LicenseDialogFragment;

    iget-object v1, p0, Lcom/samsung/groupcast/application/fragment/LicenseDialogFragment$1;->this$0:Lcom/samsung/groupcast/application/fragment/LicenseDialogFragment;

    invoke-virtual {v1}, Lcom/samsung/groupcast/application/fragment/LicenseDialogFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v3, "LICENSE_PROGRESS_DIALOG_FRAGMENT"

    invoke-virtual {v1, v3}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;

    # setter for: Lcom/samsung/groupcast/application/fragment/LicenseDialogFragment;->mProgressDialogFragment:Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;
    invoke-static {v2, v1}, Lcom/samsung/groupcast/application/fragment/LicenseDialogFragment;->access$002(Lcom/samsung/groupcast/application/fragment/LicenseDialogFragment;Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;)Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;

    .line 115
    iget-object v1, p0, Lcom/samsung/groupcast/application/fragment/LicenseDialogFragment$1;->this$0:Lcom/samsung/groupcast/application/fragment/LicenseDialogFragment;

    # getter for: Lcom/samsung/groupcast/application/fragment/LicenseDialogFragment;->mProgressDialogFragment:Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;
    invoke-static {v1}, Lcom/samsung/groupcast/application/fragment/LicenseDialogFragment;->access$000(Lcom/samsung/groupcast/application/fragment/LicenseDialogFragment;)Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 116
    iget-object v1, p0, Lcom/samsung/groupcast/application/fragment/LicenseDialogFragment$1;->this$0:Lcom/samsung/groupcast/application/fragment/LicenseDialogFragment;

    # getter for: Lcom/samsung/groupcast/application/fragment/LicenseDialogFragment;->mProgressDialogFragment:Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;
    invoke-static {v1}, Lcom/samsung/groupcast/application/fragment/LicenseDialogFragment;->access$000(Lcom/samsung/groupcast/application/fragment/LicenseDialogFragment;)Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;->dismiss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 122
    :cond_0
    :goto_0
    invoke-super {p0, p1, p2}, Landroid/webkit/WebViewClient;->onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V

    .line 123
    return-void

    .line 118
    :catch_0
    move-exception v0

    .line 119
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 4
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "url"    # Ljava/lang/String;
    .param p3, "favicon"    # Landroid/graphics/Bitmap;

    .prologue
    .line 94
    iget-object v1, p0, Lcom/samsung/groupcast/application/fragment/LicenseDialogFragment$1;->this$0:Lcom/samsung/groupcast/application/fragment/LicenseDialogFragment;

    # getter for: Lcom/samsung/groupcast/application/fragment/LicenseDialogFragment;->mProgressDialogFragment:Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;
    invoke-static {v1}, Lcom/samsung/groupcast/application/fragment/LicenseDialogFragment;->access$000(Lcom/samsung/groupcast/application/fragment/LicenseDialogFragment;)Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;

    move-result-object v1

    if-nez v1, :cond_0

    .line 96
    :try_start_0
    iget-object v1, p0, Lcom/samsung/groupcast/application/fragment/LicenseDialogFragment$1;->this$0:Lcom/samsung/groupcast/application/fragment/LicenseDialogFragment;

    new-instance v2, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;

    invoke-direct {v2}, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;-><init>()V

    # setter for: Lcom/samsung/groupcast/application/fragment/LicenseDialogFragment;->mProgressDialogFragment:Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;
    invoke-static {v1, v2}, Lcom/samsung/groupcast/application/fragment/LicenseDialogFragment;->access$002(Lcom/samsung/groupcast/application/fragment/LicenseDialogFragment;Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;)Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;

    .line 97
    iget-object v1, p0, Lcom/samsung/groupcast/application/fragment/LicenseDialogFragment$1;->this$0:Lcom/samsung/groupcast/application/fragment/LicenseDialogFragment;

    # getter for: Lcom/samsung/groupcast/application/fragment/LicenseDialogFragment;->mProgressDialogFragment:Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;
    invoke-static {v1}, Lcom/samsung/groupcast/application/fragment/LicenseDialogFragment;->access$000(Lcom/samsung/groupcast/application/fragment/LicenseDialogFragment;)Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;->setTitle(Ljava/lang/String;)V

    .line 98
    iget-object v1, p0, Lcom/samsung/groupcast/application/fragment/LicenseDialogFragment$1;->this$0:Lcom/samsung/groupcast/application/fragment/LicenseDialogFragment;

    # getter for: Lcom/samsung/groupcast/application/fragment/LicenseDialogFragment;->mProgressDialogFragment:Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;
    invoke-static {v1}, Lcom/samsung/groupcast/application/fragment/LicenseDialogFragment;->access$000(Lcom/samsung/groupcast/application/fragment/LicenseDialogFragment;)Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/groupcast/application/fragment/LicenseDialogFragment$1;->this$0:Lcom/samsung/groupcast/application/fragment/LicenseDialogFragment;

    const v3, 0x7f080022

    invoke-virtual {v2, v3}, Lcom/samsung/groupcast/application/fragment/LicenseDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;->setText(Ljava/lang/String;)V

    .line 99
    iget-object v1, p0, Lcom/samsung/groupcast/application/fragment/LicenseDialogFragment$1;->this$0:Lcom/samsung/groupcast/application/fragment/LicenseDialogFragment;

    # getter for: Lcom/samsung/groupcast/application/fragment/LicenseDialogFragment;->mProgressDialogFragment:Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;
    invoke-static {v1}, Lcom/samsung/groupcast/application/fragment/LicenseDialogFragment;->access$000(Lcom/samsung/groupcast/application/fragment/LicenseDialogFragment;)Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;->setCancelable(Z)V

    .line 100
    iget-object v1, p0, Lcom/samsung/groupcast/application/fragment/LicenseDialogFragment$1;->this$0:Lcom/samsung/groupcast/application/fragment/LicenseDialogFragment;

    # getter for: Lcom/samsung/groupcast/application/fragment/LicenseDialogFragment;->mProgressDialogFragment:Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;
    invoke-static {v1}, Lcom/samsung/groupcast/application/fragment/LicenseDialogFragment;->access$000(Lcom/samsung/groupcast/application/fragment/LicenseDialogFragment;)Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/groupcast/application/fragment/LicenseDialogFragment$1;->this$0:Lcom/samsung/groupcast/application/fragment/LicenseDialogFragment;

    invoke-virtual {v2}, Lcom/samsung/groupcast/application/fragment/LicenseDialogFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-string v3, "LICENSE_PROGRESS_DIALOG_FRAGMENT"

    invoke-virtual {v1, v2, v3}, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 106
    :cond_0
    :goto_0
    invoke-super {p0, p1, p2, p3}, Landroid/webkit/WebViewClient;->onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V

    .line 107
    return-void

    .line 102
    :catch_0
    move-exception v0

    .line 103
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 1
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "url"    # Ljava/lang/String;

    .prologue
    .line 87
    invoke-virtual {p1, p2}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 88
    const/4 v0, 0x1

    return v0
.end method
