.class Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity$2;
.super Ljava/lang/Object;
.source "SharedExperienceActivity.java"

# interfaces
.implements Lcom/samsung/groupcast/GroupPlayManager$ExternalEventHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;


# direct methods
.method constructor <init>(Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;)V
    .locals 0

    .prologue
    .line 647
    iput-object p1, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity$2;->this$0:Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGpRegister(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "Capability_info"    # Ljava/lang/String;

    .prologue
    .line 684
    iget-object v0, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity$2;->this$0:Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;

    # getter for: Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mCurrentActivityName:Ljava/lang/String;
    invoke-static {v0}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->access$000(Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/groupcast/application/App;->isTopActivity(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 693
    :goto_0
    return-void

    .line 687
    :cond_0
    const-string v0, "GroupPlayBand"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "receive onGpRegister:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity$2;->this$0:Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;

    # getter for: Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->access$100(Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/groupcast/application/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 689
    const-string v0, "GroupPlayBand"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "     PACKAGE_NAME :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/groupcast/application/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 690
    const-string v0, "GroupPlayBand"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "     CAPABILITY_INFO :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/groupcast/application/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 692
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/samsung/groupcast/application/App;->setRegister_Extra_App(Z)V

    goto :goto_0
.end method

.method public onGpUpdate(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "status_info"    # Ljava/lang/String;
    .param p3, "screenId"    # Ljava/lang/String;

    .prologue
    .line 651
    iget-object v2, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity$2;->this$0:Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;

    # getter for: Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mCurrentActivityName:Ljava/lang/String;
    invoke-static {v2}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->access$000(Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/groupcast/application/App;->isTopActivity(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 679
    :cond_0
    :goto_0
    return-void

    .line 654
    :cond_1
    if-eqz p2, :cond_2

    const-string v2, "null"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 655
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    .line 657
    :cond_2
    invoke-static {p3}, Lcom/samsung/groupcast/application/App;->setScreenId(Ljava/lang/String;)V

    .line 658
    const-string v2, "GroupPlayBand"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "receive onGpUpdate:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity$2;->this$0:Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;

    # getter for: Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->access$100(Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/groupcast/application/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 660
    const-string v2, "GroupPlayBand"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "     PACKAGE_NAME :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/groupcast/application/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 661
    const-string v2, "GroupPlayBand"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "     STATUS_INFO :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/groupcast/application/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 662
    const-string v2, "GroupPlayBand"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "     SCREEN_ID :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/groupcast/application/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 663
    const-string v2, "GroupPlayBand"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "     isRegister_Extra_App :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Lcom/samsung/groupcast/application/App;->isRegister_Extra_App()Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/groupcast/application/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 666
    iget-object v2, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity$2;->this$0:Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;

    iget-object v2, v2, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mBandInterface:Lcom/sec/android/band/BandInterface;

    invoke-virtual {v2, p3}, Lcom/sec/android/band/BandInterface;->update(Ljava/lang/String;)Z

    .line 668
    invoke-static {}, Lcom/samsung/groupcast/application/App;->getPeerScreenId()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 669
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 670
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v2, "com.samsung.groupcast.extra.SCREEN_ID"

    invoke-static {}, Lcom/samsung/groupcast/application/App;->getPeerScreenId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 671
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.samsung.groupcast.action.GP_SERVICE"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 672
    .local v1, "in":Landroid/content/Intent;
    invoke-virtual {v1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 673
    iget-object v2, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity$2;->this$0:Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;

    # getter for: Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->access$100(Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 674
    const-string v2, "GroupPlayBand"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[GP]send : GP_SERVICE:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity$2;->this$0:Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;

    # getter for: Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->access$100(Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/groupcast/application/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 676
    const-string v2, "GroupPlayBand"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "     SCREEN_ID :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Lcom/samsung/groupcast/application/App;->getPeerScreenId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/groupcast/application/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 677
    const/4 v2, 0x0

    invoke-static {v2}, Lcom/samsung/groupcast/application/App;->setPeerScreenId(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public onGpunRegister()V
    .locals 3

    .prologue
    .line 698
    iget-object v0, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity$2;->this$0:Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;

    # getter for: Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mCurrentActivityName:Ljava/lang/String;
    invoke-static {v0}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->access$000(Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/groupcast/application/App;->isTopActivity(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 705
    :goto_0
    return-void

    .line 701
    :cond_0
    const-string v0, "GroupPlayBand"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "receive onGpunRegister:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity$2;->this$0:Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;

    # getter for: Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->access$100(Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/groupcast/application/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 703
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/samsung/groupcast/application/App;->setRegister_Extra_App(Z)V

    .line 704
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/samsung/groupcast/application/App;->setScreenId(Ljava/lang/String;)V

    goto :goto_0
.end method
