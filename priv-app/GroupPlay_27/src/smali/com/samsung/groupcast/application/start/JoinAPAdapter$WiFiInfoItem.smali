.class public Lcom/samsung/groupcast/application/start/JoinAPAdapter$WiFiInfoItem;
.super Ljava/lang/Object;
.source "JoinAPAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/groupcast/application/start/JoinAPAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "WiFiInfoItem"
.end annotation


# instance fields
.field private isDummy:Z

.field private mBSSID:Ljava/lang/String;

.field private mIcon:Landroid/graphics/Bitmap;

.field private mParticipants:I

.field private mSSID:Ljava/lang/String;

.field private mWiFiSecurity:Z


# direct methods
.method public constructor <init>(Ljava/lang/CharSequence;Ljava/lang/String;Z)V
    .locals 2
    .param p1, "name"    # Ljava/lang/CharSequence;
    .param p2, "bssid"    # Ljava/lang/String;
    .param p3, "security"    # Z

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object v0, p0, Lcom/samsung/groupcast/application/start/JoinAPAdapter$WiFiInfoItem;->mSSID:Ljava/lang/String;

    .line 28
    iput-object v0, p0, Lcom/samsung/groupcast/application/start/JoinAPAdapter$WiFiInfoItem;->mBSSID:Ljava/lang/String;

    .line 30
    iput-boolean v1, p0, Lcom/samsung/groupcast/application/start/JoinAPAdapter$WiFiInfoItem;->mWiFiSecurity:Z

    .line 32
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/groupcast/application/start/JoinAPAdapter$WiFiInfoItem;->mParticipants:I

    .line 34
    iput-boolean v1, p0, Lcom/samsung/groupcast/application/start/JoinAPAdapter$WiFiInfoItem;->isDummy:Z

    .line 37
    if-nez p1, :cond_0

    .line 45
    .end local p1    # "name":Ljava/lang/CharSequence;
    :goto_0
    return-void

    .line 40
    .restart local p1    # "name":Ljava/lang/CharSequence;
    :cond_0
    if-eqz p2, :cond_1

    .line 41
    iput-object p2, p0, Lcom/samsung/groupcast/application/start/JoinAPAdapter$WiFiInfoItem;->mBSSID:Ljava/lang/String;

    .line 43
    :cond_1
    check-cast p1, Ljava/lang/String;

    .end local p1    # "name":Ljava/lang/CharSequence;
    iput-object p1, p0, Lcom/samsung/groupcast/application/start/JoinAPAdapter$WiFiInfoItem;->mSSID:Ljava/lang/String;

    .line 44
    iput-boolean p3, p0, Lcom/samsung/groupcast/application/start/JoinAPAdapter$WiFiInfoItem;->mWiFiSecurity:Z

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/CharSequence;Ljava/lang/String;ZILandroid/graphics/Bitmap;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/CharSequence;
    .param p2, "bssid"    # Ljava/lang/String;
    .param p3, "security"    # Z
    .param p4, "participants"    # I
    .param p5, "bmp"    # Landroid/graphics/Bitmap;

    .prologue
    .line 63
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/groupcast/application/start/JoinAPAdapter$WiFiInfoItem;-><init>(Ljava/lang/CharSequence;Ljava/lang/String;Z)V

    .line 64
    iput-object p5, p0, Lcom/samsung/groupcast/application/start/JoinAPAdapter$WiFiInfoItem;->mIcon:Landroid/graphics/Bitmap;

    .line 65
    iput p4, p0, Lcom/samsung/groupcast/application/start/JoinAPAdapter$WiFiInfoItem;->mParticipants:I

    .line 66
    return-void
.end method

.method public constructor <init>(Ljava/lang/CharSequence;Ljava/lang/String;ZZ)V
    .locals 0
    .param p1, "name"    # Ljava/lang/CharSequence;
    .param p2, "bssid"    # Ljava/lang/String;
    .param p3, "security"    # Z
    .param p4, "dummy"    # Z

    .prologue
    .line 48
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/groupcast/application/start/JoinAPAdapter$WiFiInfoItem;-><init>(Ljava/lang/CharSequence;Ljava/lang/String;Z)V

    .line 49
    iput-boolean p4, p0, Lcom/samsung/groupcast/application/start/JoinAPAdapter$WiFiInfoItem;->isDummy:Z

    .line 50
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/groupcast/application/start/JoinAPAdapter$WiFiInfoItem;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/application/start/JoinAPAdapter$WiFiInfoItem;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/JoinAPAdapter$WiFiInfoItem;->mBSSID:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public getBSSID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/JoinAPAdapter$WiFiInfoItem;->mBSSID:Ljava/lang/String;

    return-object v0
.end method

.method public getIcon()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/JoinAPAdapter$WiFiInfoItem;->mIcon:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getParticipants()I
    .locals 1

    .prologue
    .line 89
    iget v0, p0, Lcom/samsung/groupcast/application/start/JoinAPAdapter$WiFiInfoItem;->mParticipants:I

    return v0
.end method

.method public getSSID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/JoinAPAdapter$WiFiInfoItem;->mSSID:Ljava/lang/String;

    return-object v0
.end method

.method public getWiFiSecurity()Z
    .locals 1

    .prologue
    .line 73
    iget-boolean v0, p0, Lcom/samsung/groupcast/application/start/JoinAPAdapter$WiFiInfoItem;->mWiFiSecurity:Z

    return v0
.end method

.method public isDummy()Z
    .locals 1

    .prologue
    .line 54
    iget-boolean v0, p0, Lcom/samsung/groupcast/application/start/JoinAPAdapter$WiFiInfoItem;->isDummy:Z

    return v0
.end method

.method public setDummy(Z)V
    .locals 0
    .param p1, "Dummy"    # Z

    .prologue
    .line 58
    iput-boolean p1, p0, Lcom/samsung/groupcast/application/start/JoinAPAdapter$WiFiInfoItem;->isDummy:Z

    .line 59
    return-void
.end method

.method public setIcon(Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1, "icon"    # Landroid/graphics/Bitmap;

    .prologue
    .line 77
    iput-object p1, p0, Lcom/samsung/groupcast/application/start/JoinAPAdapter$WiFiInfoItem;->mIcon:Landroid/graphics/Bitmap;

    .line 78
    return-void
.end method

.method public setParticipants(I)V
    .locals 0
    .param p1, "participants"    # I

    .prologue
    .line 93
    iput p1, p0, Lcom/samsung/groupcast/application/start/JoinAPAdapter$WiFiInfoItem;->mParticipants:I

    .line 94
    return-void
.end method
