.class public Lcom/samsung/groupcast/application/fragment/MessageDialogFragment;
.super Landroid/app/DialogFragment;
.source "MessageDialogFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/groupcast/application/fragment/MessageDialogFragment$MessageDialogFragmentDelegate;
    }
.end annotation


# static fields
.field private static final KEY_BUTTON_PRESENT:Ljava/lang/String; = "BUTTON_PRESENT"

.field private static final KEY_TEXT_RESOURCE_ID:Ljava/lang/String; = "TEXT_RESOURCE_ID"


# instance fields
.field private mButtonPresent:Z

.field private mMessageDialogFragmentDelegate:Lcom/samsung/groupcast/application/fragment/MessageDialogFragment$MessageDialogFragmentDelegate;

.field private mTextResourceId:I

.field private mWasCancelled:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 14
    return-void
.end method

.method private getDelegate()Lcom/samsung/groupcast/application/fragment/MessageDialogFragment$MessageDialogFragmentDelegate;
    .locals 2

    .prologue
    .line 92
    iget-object v0, p0, Lcom/samsung/groupcast/application/fragment/MessageDialogFragment;->mMessageDialogFragmentDelegate:Lcom/samsung/groupcast/application/fragment/MessageDialogFragment$MessageDialogFragmentDelegate;

    if-eqz v0, :cond_0

    .line 93
    iget-object v0, p0, Lcom/samsung/groupcast/application/fragment/MessageDialogFragment;->mMessageDialogFragmentDelegate:Lcom/samsung/groupcast/application/fragment/MessageDialogFragment$MessageDialogFragmentDelegate;

    .line 100
    :goto_0
    return-object v0

    .line 96
    :cond_0
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/fragment/MessageDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-class v1, Lcom/samsung/groupcast/application/fragment/MessageDialogFragment$MessageDialogFragmentDelegate;

    invoke-static {v0, v1}, Lcom/samsung/groupcast/misc/utility/Verify;->instanceOf(Ljava/lang/Object;Ljava/lang/Class;)V

    .line 97
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/fragment/MessageDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/samsung/groupcast/application/fragment/MessageDialogFragment$MessageDialogFragmentDelegate;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 98
    :catch_0
    move-exception v0

    .line 100
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static newInstance(I)Lcom/samsung/groupcast/application/fragment/MessageDialogFragment;
    .locals 4
    .param p0, "textResourceId"    # I

    .prologue
    .line 27
    new-instance v1, Lcom/samsung/groupcast/application/fragment/MessageDialogFragment;

    invoke-direct {v1}, Lcom/samsung/groupcast/application/fragment/MessageDialogFragment;-><init>()V

    .line 28
    .local v1, "fragment":Lcom/samsung/groupcast/application/fragment/MessageDialogFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 29
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "TEXT_RESOURCE_ID"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 30
    const-string v2, "BUTTON_PRESENT"

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 31
    invoke-virtual {v1, v0}, Lcom/samsung/groupcast/application/fragment/MessageDialogFragment;->setArguments(Landroid/os/Bundle;)V

    .line 32
    return-object v1
.end method

.method public static newInstance(IZ)Lcom/samsung/groupcast/application/fragment/MessageDialogFragment;
    .locals 3
    .param p0, "textResourceId"    # I
    .param p1, "withButton"    # Z

    .prologue
    .line 36
    new-instance v1, Lcom/samsung/groupcast/application/fragment/MessageDialogFragment;

    invoke-direct {v1}, Lcom/samsung/groupcast/application/fragment/MessageDialogFragment;-><init>()V

    .line 37
    .local v1, "fragment":Lcom/samsung/groupcast/application/fragment/MessageDialogFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 38
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "TEXT_RESOURCE_ID"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 39
    const-string v2, "BUTTON_PRESENT"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 40
    invoke-virtual {v1, v0}, Lcom/samsung/groupcast/application/fragment/MessageDialogFragment;->setArguments(Landroid/os/Bundle;)V

    .line 41
    return-object v1
.end method


# virtual methods
.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    const/4 v1, 0x1

    .line 67
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onCancel(Landroid/content/DialogInterface;)V

    .line 68
    iput-boolean v1, p0, Lcom/samsung/groupcast/application/fragment/MessageDialogFragment;->mWasCancelled:Z

    .line 69
    invoke-direct {p0}, Lcom/samsung/groupcast/application/fragment/MessageDialogFragment;->getDelegate()Lcom/samsung/groupcast/application/fragment/MessageDialogFragment$MessageDialogFragmentDelegate;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 70
    invoke-direct {p0}, Lcom/samsung/groupcast/application/fragment/MessageDialogFragment;->getDelegate()Lcom/samsung/groupcast/application/fragment/MessageDialogFragment$MessageDialogFragmentDelegate;

    move-result-object v0

    invoke-interface {v0, p0, v1}, Lcom/samsung/groupcast/application/fragment/MessageDialogFragment$MessageDialogFragmentDelegate;->onMessageDialogDismissed(Lcom/samsung/groupcast/application/fragment/MessageDialogFragment;Z)V

    .line 71
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 46
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 47
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/fragment/MessageDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "TEXT_RESOURCE_ID"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/samsung/groupcast/application/fragment/MessageDialogFragment;->mTextResourceId:I

    .line 48
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/fragment/MessageDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "BUTTON_PRESENT"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/samsung/groupcast/application/fragment/MessageDialogFragment;->mButtonPresent:Z

    .line 49
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 53
    const/4 v0, 0x0

    .line 54
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    new-instance v0, Landroid/app/AlertDialog$Builder;

    .end local v0    # "builder":Landroid/app/AlertDialog$Builder;
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/fragment/MessageDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 56
    .restart local v0    # "builder":Landroid/app/AlertDialog$Builder;
    const v2, 0x7f080001

    invoke-virtual {p0, v2}, Lcom/samsung/groupcast/application/fragment/MessageDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 57
    iget v2, p0, Lcom/samsung/groupcast/application/fragment/MessageDialogFragment;->mTextResourceId:I

    invoke-static {v2}, Lcom/samsung/groupcast/application/App;->getSanitizedString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 58
    iget-boolean v2, p0, Lcom/samsung/groupcast/application/fragment/MessageDialogFragment;->mButtonPresent:Z

    if-eqz v2, :cond_0

    .line 59
    const v2, 0x7f08000e

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 60
    :cond_0
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    .line 61
    .local v1, "dialog":Landroid/app/AlertDialog;
    return-object v1
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    .line 76
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onDismiss(Landroid/content/DialogInterface;)V

    .line 78
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/fragment/MessageDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_1

    .line 85
    :cond_0
    :goto_0
    return-void

    .line 82
    :cond_1
    iget-boolean v0, p0, Lcom/samsung/groupcast/application/fragment/MessageDialogFragment;->mWasCancelled:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/samsung/groupcast/application/fragment/MessageDialogFragment;->getDelegate()Lcom/samsung/groupcast/application/fragment/MessageDialogFragment$MessageDialogFragmentDelegate;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 83
    invoke-direct {p0}, Lcom/samsung/groupcast/application/fragment/MessageDialogFragment;->getDelegate()Lcom/samsung/groupcast/application/fragment/MessageDialogFragment$MessageDialogFragmentDelegate;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, p0, v1}, Lcom/samsung/groupcast/application/fragment/MessageDialogFragment$MessageDialogFragmentDelegate;->onMessageDialogDismissed(Lcom/samsung/groupcast/application/fragment/MessageDialogFragment;Z)V

    goto :goto_0
.end method

.method public setDelegate(Lcom/samsung/groupcast/application/fragment/MessageDialogFragment$MessageDialogFragmentDelegate;)V
    .locals 0
    .param p1, "delegate"    # Lcom/samsung/groupcast/application/fragment/MessageDialogFragment$MessageDialogFragmentDelegate;

    .prologue
    .line 88
    iput-object p1, p0, Lcom/samsung/groupcast/application/fragment/MessageDialogFragment;->mMessageDialogFragmentDelegate:Lcom/samsung/groupcast/application/fragment/MessageDialogFragment$MessageDialogFragmentDelegate;

    .line 89
    return-void
.end method
