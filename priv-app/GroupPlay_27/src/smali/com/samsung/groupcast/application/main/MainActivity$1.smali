.class Lcom/samsung/groupcast/application/main/MainActivity$1;
.super Landroid/content/BroadcastReceiver;
.source "MainActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/groupcast/application/main/MainActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/groupcast/application/main/MainActivity;


# direct methods
.method constructor <init>(Lcom/samsung/groupcast/application/main/MainActivity;)V
    .locals 0

    .prologue
    .line 159
    iput-object p1, p0, Lcom/samsung/groupcast/application/main/MainActivity$1;->this$0:Lcom/samsung/groupcast/application/main/MainActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 162
    const-string v2, "GAME NAME"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 164
    .local v1, "pkgName":Ljava/lang/String;
    const-string v2, "com.samsung.groupcast.action.APP_CLOSE_NOTIFICATION_TO_GP2"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 165
    iget-object v2, p0, Lcom/samsung/groupcast/application/main/MainActivity$1;->this$0:Lcom/samsung/groupcast/application/main/MainActivity;

    invoke-virtual {v2}, Lcom/samsung/groupcast/application/main/MainActivity;->endGroupPlay()V

    .line 168
    :cond_0
    if-nez v1, :cond_2

    .line 200
    :cond_1
    :goto_0
    return-void

    .line 170
    :cond_2
    iget-object v2, p0, Lcom/samsung/groupcast/application/main/MainActivity$1;->this$0:Lcom/samsung/groupcast/application/main/MainActivity;

    invoke-virtual {v2}, Lcom/samsung/groupcast/application/main/MainActivity;->getSharedExperienceManager()Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 173
    const-string v2, "GROUPPLAY GAMER JOINED"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 174
    sget-object v2, Lcom/samsung/groupcast/application/main/MainActivity;->PreLoadedApps:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 175
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "GAME_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 177
    :cond_3
    iget-object v2, p0, Lcom/samsung/groupcast/application/main/MainActivity$1;->this$0:Lcom/samsung/groupcast/application/main/MainActivity;

    invoke-virtual {v2}, Lcom/samsung/groupcast/application/main/MainActivity;->getSharedExperienceManager()Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->getHybridChannel()Lcom/samsung/groupcast/core/messaging/HybridChannel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/groupcast/core/messaging/HybridChannel;->getUserInfo()Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;->joinActivity(Ljava/lang/String;)V

    goto :goto_0

    .line 180
    :cond_4
    const-string v2, "GROUPPLAY GAMER LEFT"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 181
    sget-object v2, Lcom/samsung/groupcast/application/main/MainActivity;->PreLoadedApps:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 182
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "GAME_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 184
    :cond_5
    iget-object v2, p0, Lcom/samsung/groupcast/application/main/MainActivity$1;->this$0:Lcom/samsung/groupcast/application/main/MainActivity;

    invoke-virtual {v2}, Lcom/samsung/groupcast/application/main/MainActivity;->getSharedExperienceManager()Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->getHybridChannel()Lcom/samsung/groupcast/core/messaging/HybridChannel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/groupcast/core/messaging/HybridChannel;->getUserInfo()Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;->leaveActivity(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 187
    :cond_6
    const-string v2, "GP_REMOTE_DISCONNECT"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 188
    iget-object v2, p0, Lcom/samsung/groupcast/application/main/MainActivity$1;->this$0:Lcom/samsung/groupcast/application/main/MainActivity;

    invoke-virtual {v2}, Lcom/samsung/groupcast/application/main/MainActivity;->showDisconnectAlert()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 190
    iget-object v2, p0, Lcom/samsung/groupcast/application/main/MainActivity$1;->this$0:Lcom/samsung/groupcast/application/main/MainActivity;

    invoke-virtual {v2}, Lcom/samsung/groupcast/application/main/MainActivity;->stopGroupPlayService()V

    .line 192
    const-string v2, "remote disconnect....showDisconnectAlert()"

    invoke-static {v2}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 194
    new-instance v0, Landroid/content/Intent;

    invoke-static {}, Lcom/samsung/groupcast/application/App;->getInstance()Lcom/samsung/groupcast/application/App;

    move-result-object v2

    const-class v3, Lcom/samsung/groupcast/application/main/MainActivity;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 195
    .local v0, "noWifiIntent":Landroid/content/Intent;
    const/high16 v2, 0x4000000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 196
    const-string v2, "ObjectStoreHash"

    invoke-static {}, Lcom/samsung/groupcast/application/ObjectStore;->getHash()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 197
    iget-object v2, p0, Lcom/samsung/groupcast/application/main/MainActivity$1;->this$0:Lcom/samsung/groupcast/application/main/MainActivity;

    invoke-virtual {v2, v0}, Lcom/samsung/groupcast/application/main/MainActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0
.end method
