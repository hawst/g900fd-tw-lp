.class public Lcom/samsung/groupcast/application/start/WifiConnectDialogStaticHandler;
.super Landroid/os/Handler;
.source "WifiConnectDialogStaticHandler.java"


# instance fields
.field private mDelegate:Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 1
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 11
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/WifiConnectDialogStaticHandler;->mDelegate:Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment;

    if-eqz v0, :cond_0

    .line 12
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/WifiConnectDialogStaticHandler;->mDelegate:Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment;

    invoke-virtual {v0, p0, p1}, Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment;->handleMessage(Landroid/os/Handler;Landroid/os/Message;)V

    .line 14
    :cond_0
    return-void
.end method

.method setDelegate(Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment;)V
    .locals 0
    .param p1, "delegate"    # Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment;

    .prologue
    .line 17
    iput-object p1, p0, Lcom/samsung/groupcast/application/start/WifiConnectDialogStaticHandler;->mDelegate:Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment;

    .line 18
    return-void
.end method
