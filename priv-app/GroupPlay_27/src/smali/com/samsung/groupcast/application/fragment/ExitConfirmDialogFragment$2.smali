.class Lcom/samsung/groupcast/application/fragment/ExitConfirmDialogFragment$2;
.super Ljava/lang/Object;
.source "ExitConfirmDialogFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/groupcast/application/fragment/ExitConfirmDialogFragment;->createAlertDialog(Landroid/app/Activity;)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/groupcast/application/fragment/ExitConfirmDialogFragment;

.field final synthetic val$act:Landroid/app/Activity;


# direct methods
.method constructor <init>(Lcom/samsung/groupcast/application/fragment/ExitConfirmDialogFragment;Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 95
    iput-object p1, p0, Lcom/samsung/groupcast/application/fragment/ExitConfirmDialogFragment$2;->this$0:Lcom/samsung/groupcast/application/fragment/ExitConfirmDialogFragment;

    iput-object p2, p0, Lcom/samsung/groupcast/application/fragment/ExitConfirmDialogFragment$2;->val$act:Landroid/app/Activity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 98
    invoke-static {}, Lcom/samsung/groupcast/application/StatBigDataLog;->getInstance()Lcom/samsung/groupcast/application/StatBigDataLog;

    move-result-object v0

    sget-object v1, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;->END:Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

    invoke-virtual {v0, v1}, Lcom/samsung/groupcast/application/StatBigDataLog;->pushLog(Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;)V

    .line 100
    invoke-static {}, Lcom/samsung/groupcast/application/StatLog;->getInstance()Lcom/samsung/groupcast/application/StatLog;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/groupcast/application/StatLog;->saveData()V

    .line 101
    invoke-static {}, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->clearConfirms()V

    .line 102
    iget-object v0, p0, Lcom/samsung/groupcast/application/fragment/ExitConfirmDialogFragment$2;->val$act:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 103
    return-void
.end method
