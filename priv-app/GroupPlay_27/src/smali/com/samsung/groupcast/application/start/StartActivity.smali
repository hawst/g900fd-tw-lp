.class public Lcom/samsung/groupcast/application/start/StartActivity;
.super Lcom/sec/android/band/BandActivity;
.source "StartActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment$Delegate;
.implements Lcom/samsung/groupcast/application/fragment/FlightModeDialogFragment$FlightModeDialogFragmentDelegate;
.implements Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment$NicknameInputDialogFragmentDelegate;
.implements Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment$PinCodeInputDialogFragmentDelegate;
.implements Lcom/samsung/groupcast/application/start/ApListView$Delegate;
.implements Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment$JoinSessionDialogFragmentDelegate;
.implements Lcom/samsung/groupcast/application/start/StartFragment$DisclaimerFragmentCheckedListener;
.implements Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment$WifiConnectDialogFragmentDelegate;
.implements Lcom/samsung/groupcast/net/wifi/WifiAPUtils$OnRvfHotspotChangeListener;
.implements Lcom/samsung/groupcast/net/wifi/WifiP2PUtils$OnP2PEventListener;
.implements Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiMotherFatherOxygenDelegate;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/groupcast/application/start/StartActivity$WifiCancelReceiver;,
        Lcom/samsung/groupcast/application/start/StartActivity$StartPagerAdapter;,
        Lcom/samsung/groupcast/application/start/StartActivity$WHandler;
    }
.end annotation


# static fields
.field private static final AUTOCONNECTION_IN_PROGRESS:Ljava/lang/String; = "AUTOCONNECTION_IN_PROGRESS"

.field public static final EXTERNAL_APP_TYPE:Ljava/lang/String; = "ExternalAppContentType"

.field private static final LAST_CHECK_TIME_ID:Ljava/lang/String; = "LAST_CHECK_TIME"

.field private static final MODE_JOIN:I = 0x2

.field private static final NICKNAME_INPUT_DIALOG_FRAGMENT:Ljava/lang/String; = "TAG_NICKNAME_DIALOG"

.field private static final PIN_CODE_INPUT_DIALOG_FRAGMENT_TAG:Ljava/lang/String; = "TAG_PIN_DIALOG"

.field private static final REQUEST_ENABLE_BT:I = 0x1

.field private static final SEARCH_TIMEOUT_MS:I = 0x2710

.field private static final TAG:Ljava/lang/String;

.field private static final TAG_AIRPLAINMODE_FRAGMENT:Ljava/lang/String; = "TAG_AIRPLANEMODE_FRAGMENT"

.field private static final TAG_CONNECT_DIALOG_FRAGMENT:Ljava/lang/String; = "TAG_CONNECT_DIALOG_FRAGMENT"

.field private static final TAG_IS_CONNECT_ONGOING:Ljava/lang/String; = "IS_CONNECT_ONGOING"

.field private static final TAG_IS_JOINING:Ljava/lang/String; = "TAG_IS_JOINING"

.field private static final TAG_IS_PIN_OK:Ljava/lang/String; = "IS_PIN_OK"

.field private static final TAG_IS_SEARCH_ONGOING:Ljava/lang/String; = "TAG_IS_SEARCH_ONGOING"

.field private static final TAG_JOIN_SESSION_DIALOG_FRAGMENT:Ljava/lang/String; = "TAG_JOIN_SESSION_DIALOG_FRAGMENT"

.field private static final TAG_NFC_MSG:Ljava/lang/String; = "TAG_NFC_MSG"

.field private static final TAG_PROGRESS_DIALOG_FRAGMENT:Ljava/lang/String; = "AUTOCONNECT_PROGRESS_DIALOG_FRAGMENT"

.field private static final TAG_REMOTE_INVITATION_PROGRESS_DIALOG_FRAGMENT:Ljava/lang/String; = "TAG_REMOTE_INVITATION_PROGRESS_DIALOG_FRAGMENT"

.field private static final TAG_SCAN_RESULT:Ljava/lang/String; = "TAG_SCAN_RESULT"

.field private static final TAG_SSID:Ljava/lang/String; = "TAG_SSID"

.field public static final TAG_VERIFICATIONLOG:Ljava/lang/String; = "VerificationLog"

.field private static final UPDATE_DIALOG_FRAGMENT_TAG:Ljava/lang/String; = "TAG_UPDATE_DIALOG"

.field private static mNfcConnectOngoing:Z

.field public static start_Activity:Landroid/app/Activity;


# instance fields
.field private final MODE_START:I

.field protected contentView:Landroid/view/View;

.field private currentUri:Landroid/net/Uri;

.field private dummyCount:I

.field private final mAPHandler:Lcom/samsung/groupcast/application/start/StartActivity$WHandler;

.field private mActive:Z

.field private mAlreadyEn:Z

.field private mApLeftArrow:Landroid/widget/ImageView;

.field private mApNumber:Landroid/widget/TextView;

.field private mApRightArrow:Landroid/widget/ImageView;

.field private mApScanStop:Landroid/widget/ImageView;

.field private mBTAdapter:Landroid/bluetooth/BluetoothAdapter;

.field private mBTManager:Landroid/bluetooth/BluetoothManager;

.field private final mBandEventHandler:Lcom/sec/android/band/BandInterface$BandEventHandler;

.field private mBandPassphrase:Ljava/lang/String;

.field private mBandSessionPin:Ljava/lang/String;

.field private mBandSessionSummery:Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;

.field private mBandTargetSSID:Ljava/lang/String;

.field private mBssID:Ljava/lang/String;

.field private mDisclaimerChecked:Z

.field private mExternalEventHandler:Lcom/samsung/groupcast/GroupPlayManager$ExternalEventHandler;

.field private mGPSDKNextAction:Ljava/lang/String;

.field private mGPSDKType:I

.field private mGp:Lcom/samsung/groupcast/GroupPlayManager;

.field private mHandler:Landroid/os/Handler;

.field protected mHowManyDisclaimerShowing:I

.field private mImageView:Landroid/widget/ImageView;

.field private mIsCheckedNearbyFrientInput:Z

.field private mIsCheckedPasswordInput:Z

.field private mIsConnectOngoing:Z

.field private mIsResumed:Z

.field private mIsSearchOngoing:Z

.field private mIsStartFromExternal:Z

.field private mIsStartFromGPSDK:Z

.field private mLastCheckTime:J

.field private mMenuType:Ljava/lang/String;

.field private mMobileAPAdapter:Lcom/samsung/groupcast/application/start/JoinAPAdapter;

.field private mMobileAPListView:Lcom/samsung/groupcast/application/start/ApListView;

.field private mModeStartOrJoin:I

.field private mNearbyFrientCheckBox:Landroid/widget/CheckBox;

.field private mNfcMsg:Lcom/samsung/groupcast/core/messaging/NfcMsg;

.field private mNickName:Landroid/widget/TextView;

.field private mPager:Lcom/samsung/groupcast/view/NoSwipableViewPager;

.field private mPagerAdapter:Lcom/samsung/groupcast/application/start/StartActivity$StartPagerAdapter;

.field private mPasswordCheckBox:Landroid/widget/CheckBox;

.field private mPin:Ljava/lang/String;

.field private mPinFromBand:Ljava/lang/String;

.field private mPinOK:Z

.field private mProgressDialogFragment:Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;

.field private mSSID:Ljava/lang/String;

.field private mSavedInstanceState:Landroid/os/Bundle;

.field private mSavedInstanceStateCalled:Z

.field public mScanResult:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/net/wifi/ScanResult;",
            ">;"
        }
    .end annotation
.end field

.field private mScanningLayout:Landroid/widget/RelativeLayout;

.field private mServiceType:Ljava/lang/String;

.field private mStartBtn:Landroid/widget/Button;

.field private mStartNextBtn:Landroid/widget/LinearLayout;

.field private mSummittingPincode:Ljava/lang/String;

.field private final mWiFiInfoItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/groupcast/application/start/JoinAPAdapter$WiFiInfoItem;",
            ">;"
        }
    .end annotation
.end field

.field private mWifiCancelReceiver:Lcom/samsung/groupcast/application/start/StartActivity$WifiCancelReceiver;

.field private mWifiManager:Landroid/net/wifi/WifiManager;

.field private final mWifiUtils:Lcom/samsung/groupcast/net/wifi/WifiUtils;

.field private mlicenseView:Landroid/widget/TextView;

.field private final showNicknameInputDialog:Landroid/view/View$OnClickListener;

.field startStop:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 137
    const-class v0, Lcom/samsung/groupcast/application/start/StartActivity;

    invoke-static {v0}, Lcom/samsung/groupcast/application/Logger;->getTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/groupcast/application/start/StartActivity;->TAG:Ljava/lang/String;

    .line 222
    const/4 v0, 0x0

    sput-boolean v0, Lcom/samsung/groupcast/application/start/StartActivity;->mNfcConnectOngoing:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 120
    invoke-direct {p0}, Lcom/sec/android/band/BandActivity;-><init>()V

    .line 138
    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiUtils;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mWifiUtils:Lcom/samsung/groupcast/net/wifi/WifiUtils;

    .line 145
    new-instance v0, Lcom/samsung/groupcast/GroupPlayManager;

    invoke-static {}, Lcom/samsung/groupcast/application/App;->getInstance()Lcom/samsung/groupcast/application/App;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/groupcast/application/App;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/groupcast/GroupPlayManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mGp:Lcom/samsung/groupcast/GroupPlayManager;

    .line 168
    iput-boolean v2, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mIsConnectOngoing:Z

    .line 169
    iput-boolean v2, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mIsSearchOngoing:Z

    .line 170
    iput-boolean v2, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mActive:Z

    .line 171
    iput-boolean v2, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mPinOK:Z

    .line 173
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mLastCheckTime:J

    .line 178
    iput-object v3, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mServiceType:Ljava/lang/String;

    .line 179
    iput-object v3, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mMenuType:Ljava/lang/String;

    .line 193
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mWiFiInfoItems:Ljava/util/ArrayList;

    .line 194
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mScanResult:Ljava/util/List;

    .line 195
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mSSID:Ljava/lang/String;

    .line 196
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mBssID:Ljava/lang/String;

    .line 200
    iput-object v3, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mProgressDialogFragment:Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;

    .line 203
    iput-boolean v2, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mIsStartFromExternal:Z

    .line 204
    iput-boolean v2, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mIsCheckedPasswordInput:Z

    .line 205
    iput-boolean v2, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mIsCheckedNearbyFrientInput:Z

    .line 224
    iput v2, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mModeStartOrJoin:I

    .line 225
    const/4 v0, 0x1

    iput v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->MODE_START:I

    .line 228
    iput-boolean v2, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mSavedInstanceStateCalled:Z

    .line 1140
    new-instance v0, Lcom/samsung/groupcast/application/start/StartActivity$8;

    invoke-direct {v0, p0}, Lcom/samsung/groupcast/application/start/StartActivity$8;-><init>(Lcom/samsung/groupcast/application/start/StartActivity;)V

    iput-object v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->startStop:Ljava/lang/Runnable;

    .line 1233
    new-instance v0, Lcom/samsung/groupcast/application/start/StartActivity$WHandler;

    invoke-direct {v0, p0}, Lcom/samsung/groupcast/application/start/StartActivity$WHandler;-><init>(Lcom/samsung/groupcast/application/start/StartActivity;)V

    iput-object v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mAPHandler:Lcom/samsung/groupcast/application/start/StartActivity$WHandler;

    .line 1990
    new-instance v0, Lcom/samsung/groupcast/application/start/StartActivity$10;

    invoke-direct {v0, p0}, Lcom/samsung/groupcast/application/start/StartActivity$10;-><init>(Lcom/samsung/groupcast/application/start/StartActivity;)V

    iput-object v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mExternalEventHandler:Lcom/samsung/groupcast/GroupPlayManager$ExternalEventHandler;

    .line 2061
    new-instance v0, Lcom/samsung/groupcast/application/start/StartActivity$11;

    invoke-direct {v0, p0}, Lcom/samsung/groupcast/application/start/StartActivity$11;-><init>(Lcom/samsung/groupcast/application/start/StartActivity;)V

    iput-object v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mBandEventHandler:Lcom/sec/android/band/BandInterface$BandEventHandler;

    .line 2310
    new-instance v0, Lcom/samsung/groupcast/application/start/StartActivity$12;

    invoke-direct {v0, p0}, Lcom/samsung/groupcast/application/start/StartActivity$12;-><init>(Lcom/samsung/groupcast/application/start/StartActivity;)V

    iput-object v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->showNicknameInputDialog:Landroid/view/View$OnClickListener;

    .line 2598
    iput-boolean v2, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mAlreadyEn:Z

    .line 2676
    return-void
.end method

.method static synthetic access$002(Lcom/samsung/groupcast/application/start/StartActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/groupcast/application/start/StartActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 120
    iput-boolean p1, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mIsCheckedPasswordInput:Z

    return p1
.end method

.method static synthetic access$100(Lcom/samsung/groupcast/application/start/StartActivity;)Landroid/widget/CheckBox;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/application/start/StartActivity;

    .prologue
    .line 120
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mPasswordCheckBox:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$1002(Lcom/samsung/groupcast/application/start/StartActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/groupcast/application/start/StartActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 120
    iput-object p1, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mPinFromBand:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1100(Lcom/samsung/groupcast/application/start/StartActivity;)Lcom/samsung/groupcast/net/wifi/WifiUtils;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/application/start/StartActivity;

    .prologue
    .line 120
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mWifiUtils:Lcom/samsung/groupcast/net/wifi/WifiUtils;

    return-object v0
.end method

.method static synthetic access$1200()Ljava/lang/String;
    .locals 1

    .prologue
    .line 120
    sget-object v0, Lcom/samsung/groupcast/application/start/StartActivity;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/samsung/groupcast/application/start/StartActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/application/start/StartActivity;

    .prologue
    .line 120
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mBandTargetSSID:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1302(Lcom/samsung/groupcast/application/start/StartActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/groupcast/application/start/StartActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 120
    iput-object p1, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mBandTargetSSID:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1400(Lcom/samsung/groupcast/application/start/StartActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/application/start/StartActivity;

    .prologue
    .line 120
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mBandPassphrase:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1402(Lcom/samsung/groupcast/application/start/StartActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/groupcast/application/start/StartActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 120
    iput-object p1, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mBandPassphrase:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1500(Lcom/samsung/groupcast/application/start/StartActivity;)Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/application/start/StartActivity;

    .prologue
    .line 120
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mBandSessionSummery:Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;

    return-object v0
.end method

.method static synthetic access$1502(Lcom/samsung/groupcast/application/start/StartActivity;Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;)Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/groupcast/application/start/StartActivity;
    .param p1, "x1"    # Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;

    .prologue
    .line 120
    iput-object p1, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mBandSessionSummery:Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;

    return-object p1
.end method

.method static synthetic access$1600(Lcom/samsung/groupcast/application/start/StartActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/application/start/StartActivity;

    .prologue
    .line 120
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mBandSessionPin:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1602(Lcom/samsung/groupcast/application/start/StartActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/groupcast/application/start/StartActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 120
    iput-object p1, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mBandSessionPin:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1700(Lcom/samsung/groupcast/application/start/StartActivity;Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/groupcast/application/start/StartActivity;
    .param p1, "x1"    # Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 120
    invoke-direct {p0, p1, p2}, Lcom/samsung/groupcast/application/start/StartActivity;->doJoinFromBand(Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1802(Lcom/samsung/groupcast/application/start/StartActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/groupcast/application/start/StartActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 120
    iput-object p1, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mServiceType:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1902(Lcom/samsung/groupcast/application/start/StartActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/groupcast/application/start/StartActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 120
    iput-object p1, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mMenuType:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$200(Lcom/samsung/groupcast/application/start/StartActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/groupcast/application/start/StartActivity;

    .prologue
    .line 120
    invoke-direct {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->saveCheckPasswordState()V

    return-void
.end method

.method static synthetic access$2000(Lcom/samsung/groupcast/application/start/StartActivity;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/application/start/StartActivity;

    .prologue
    .line 120
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mNickName:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$2200(Lcom/samsung/groupcast/application/start/StartActivity;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/application/start/StartActivity;

    .prologue
    .line 120
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mScanningLayout:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$2300(Lcom/samsung/groupcast/application/start/StartActivity;)Lcom/samsung/groupcast/application/start/ApListView;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/application/start/StartActivity;

    .prologue
    .line 120
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mMobileAPListView:Lcom/samsung/groupcast/application/start/ApListView;

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/groupcast/application/start/StartActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/application/start/StartActivity;

    .prologue
    .line 120
    iget-boolean v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mIsSearchOngoing:Z

    return v0
.end method

.method static synthetic access$400(Lcom/samsung/groupcast/application/start/StartActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/groupcast/application/start/StartActivity;

    .prologue
    .line 120
    invoke-direct {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->stopSearch()V

    return-void
.end method

.method static synthetic access$500(Lcom/samsung/groupcast/application/start/StartActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/groupcast/application/start/StartActivity;

    .prologue
    .line 120
    invoke-direct {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->startSearch()V

    return-void
.end method

.method static synthetic access$600(Lcom/samsung/groupcast/application/start/StartActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/application/start/StartActivity;

    .prologue
    .line 120
    iget-boolean v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mDisclaimerChecked:Z

    return v0
.end method

.method static synthetic access$700(Lcom/samsung/groupcast/application/start/StartActivity;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/application/start/StartActivity;

    .prologue
    .line 120
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mStartNextBtn:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$800(Lcom/samsung/groupcast/application/start/StartActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/application/start/StartActivity;

    .prologue
    .line 120
    iget-boolean v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mSavedInstanceStateCalled:Z

    return v0
.end method

.method static synthetic access$902(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 120
    sput-boolean p0, Lcom/samsung/groupcast/application/start/StartActivity;->mNfcConnectOngoing:Z

    return p0
.end method

.method private addExtraInfoFromExternalApp(Landroid/content/Intent;)V
    .locals 8
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v5, 0x0

    .line 2563
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->getIntent()Landroid/content/Intent;

    move-result-object v6

    invoke-static {v6}, Lcom/samsung/groupcast/application/IntentTools;->getSharedUrisFromIntent(Landroid/content/Intent;)Ljava/util/ArrayList;

    move-result-object v4

    .line 2564
    .local v4, "uris":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    if-eqz v4, :cond_3

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-lez v6, :cond_3

    .line 2565
    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/Uri;

    .line 2566
    .local v2, "singleFileUri":Landroid/net/Uri;
    const-string v6, "URIS_TO_SHARE_FROM_EXTERNAL"

    invoke-static {v4}, Lcom/samsung/groupcast/misc/utility/FileTools;->getCheckUri(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v7

    invoke-virtual {p1, v6, v7}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 2568
    invoke-virtual {v2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/samsung/groupcast/misc/utility/FileTools;->getFileType(Ljava/lang/String;)I

    move-result v6

    const/4 v7, -0x1

    if-eq v6, v7, :cond_4

    .line 2569
    const-string v6, "ExternalAppContentType"

    invoke-virtual {v2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/samsung/groupcast/misc/utility/FileTools;->getFileType(Ljava/lang/String;)I

    move-result v7

    invoke-virtual {p1, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2579
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->getIntent()Landroid/content/Intent;

    move-result-object v6

    invoke-static {v6}, Lcom/samsung/groupcast/application/IntentTools;->isSendDJModeIntent(Landroid/content/Intent;)Z

    move-result v6

    if-nez v6, :cond_1

    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->getIntent()Landroid/content/Intent;

    move-result-object v6

    invoke-static {v6}, Lcom/samsung/groupcast/application/IntentTools;->isSendMultipleDJModeIntent(Landroid/content/Intent;)Z

    move-result v6

    if-eqz v6, :cond_2

    :cond_1
    const/4 v5, 0x1

    :cond_2
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 2582
    .local v0, "intentFromMusicPlayer":Ljava/lang/Boolean;
    const-string v5, "DJ_MODE_VIA_GROUP_PLAY_FROM_MUSIC_PLAYER"

    invoke-virtual {p1, v5, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 2585
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    invoke-static {v5}, Lcom/samsung/groupcast/application/IntentTools;->isSendVideoIntent(Landroid/content/Intent;)Z

    move-result v5

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 2586
    .local v1, "intentFromVideoPlayer":Ljava/lang/Boolean;
    const-string v5, "SHARE_VIDEO_VIA_GROUP_PLAY_FROM_VIDEO_PLAYER"

    invoke-virtual {p1, v5, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 2589
    .end local v0    # "intentFromMusicPlayer":Ljava/lang/Boolean;
    .end local v1    # "intentFromVideoPlayer":Ljava/lang/Boolean;
    .end local v2    # "singleFileUri":Landroid/net/Uri;
    :cond_3
    return-void

    .line 2571
    .restart local v2    # "singleFileUri":Landroid/net/Uri;
    :cond_4
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    invoke-static {v6, v2}, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItemFactory;->resolveLocalContentPath(Landroid/content/ContentResolver;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v3

    .line 2574
    .local v3, "tmpPath":Ljava/lang/String;
    if-eqz v3, :cond_0

    .line 2575
    const-string v6, "ExternalAppContentType"

    invoke-static {v3}, Lcom/samsung/groupcast/misc/utility/FileTools;->getFileType(Ljava/lang/String;)I

    move-result v7

    invoke-virtual {p1, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_0
.end method

.method private addExtraInfoFromGPSDK(Landroid/content/Intent;)V
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 2592
    sget-object v0, Lcom/samsung/groupcast/application/start/StartActivity;->TAG:Ljava/lang/String;

    const-string v1, "[addExtraInfoFromGPSDK]"

    invoke-static {v0, v1}, Lcom/samsung/groupcast/application/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2593
    const-string v0, "APPLICATION_INTERFACE_IS_START_FROM_GPSDK"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2594
    const-string v0, "APPLICATION_INTERFACE_NEXT_ACTION"

    iget-object v1, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mGPSDKNextAction:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2595
    const-string v0, "APPLICATION_INTERFACE_TYPE"

    iget v1, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mGPSDKType:I

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2596
    return-void
.end method

.method private apCount(I)V
    .locals 4
    .param p1, "listSize"    # I

    .prologue
    .line 1280
    iget v1, p0, Lcom/samsung/groupcast/application/start/StartActivity;->dummyCount:I

    sub-int v0, p1, v1

    .line 1282
    .local v0, "apCount":I
    if-lez v0, :cond_0

    .line 1283
    iget-object v1, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mApNumber:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1287
    :goto_0
    return-void

    .line 1285
    :cond_0
    iget-object v1, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mApNumber:Landroid/widget/TextView;

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private checkAvailablityToConnect()Z
    .locals 10

    .prologue
    const/4 v5, 0x0

    const/4 v6, 0x1

    .line 1729
    sget-object v7, Lcom/samsung/groupcast/application/start/StartActivity;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "isCmccMode: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static {}, Lcom/samsung/groupcast/application/Environment;->isCmccModel()Z

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", isAirPlaneModeOn : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static {}, Lcom/samsung/groupcast/application/App;->getInstance()Lcom/samsung/groupcast/application/App;

    move-result-object v9

    invoke-virtual {v9}, Lcom/samsung/groupcast/application/App;->getApplicationContext()Landroid/content/Context;

    move-result-object v9

    invoke-static {v9}, Lcom/samsung/groupcast/application/Environment;->isAirplaneModeOn(Landroid/content/Context;)Z

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/samsung/groupcast/application/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1731
    invoke-static {}, Lcom/samsung/groupcast/application/Environment;->isCmccModel()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 1732
    invoke-static {}, Lcom/samsung/groupcast/application/App;->getInstance()Lcom/samsung/groupcast/application/App;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/groupcast/application/App;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Lcom/samsung/groupcast/application/Environment;->isAirplaneModeOn(Landroid/content/Context;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 1733
    const v7, 0x7f080046

    invoke-virtual {p0, v7}, Lcom/samsung/groupcast/application/start/StartActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {p0, v7, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/Toast;->show()V

    .line 1765
    :goto_0
    return v5

    .line 1739
    :cond_0
    invoke-static {}, Lcom/samsung/groupcast/application/App;->getInstance()Lcom/samsung/groupcast/application/App;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/groupcast/application/App;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Lcom/samsung/groupcast/application/Environment;->isAirplaneModeOn(Landroid/content/Context;)Z

    move-result v7

    if-eqz v7, :cond_2

    iget-object v7, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mWifiUtils:Lcom/samsung/groupcast/net/wifi/WifiUtils;

    invoke-virtual {v7}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getWifiManager()Landroid/net/wifi/WifiManager;

    move-result-object v7

    invoke-virtual {v7}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z

    move-result v7

    if-nez v7, :cond_2

    .line 1745
    iget v7, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mModeStartOrJoin:I

    if-ne v7, v6, :cond_1

    .line 1746
    const v1, 0x7f080048

    .line 1747
    .local v1, "filghtStringId":I
    const/4 v2, 0x0

    .line 1752
    .local v2, "flightFragmentType":I
    :goto_1
    invoke-static {v1, v2}, Lcom/samsung/groupcast/application/fragment/FlightModeDialogFragment;->newInstance(II)Lcom/samsung/groupcast/application/fragment/FlightModeDialogFragment;

    move-result-object v3

    .line 1755
    .local v3, "fragment":Lcom/samsung/groupcast/application/fragment/FlightModeDialogFragment;
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v6

    const-string v7, "TAG_AIRPLANEMODE_FRAGMENT"

    invoke-virtual {v3, v6, v7}, Lcom/samsung/groupcast/application/fragment/FlightModeDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 1756
    :catch_0
    move-exception v4

    .line 1757
    .local v4, "ie":Ljava/lang/IllegalStateException;
    invoke-virtual {v4}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0

    .line 1749
    .end local v1    # "filghtStringId":I
    .end local v2    # "flightFragmentType":I
    .end local v3    # "fragment":Lcom/samsung/groupcast/application/fragment/FlightModeDialogFragment;
    .end local v4    # "ie":Ljava/lang/IllegalStateException;
    :cond_1
    const v1, 0x7f080047

    .line 1750
    .restart local v1    # "filghtStringId":I
    const/4 v2, 0x1

    .restart local v2    # "flightFragmentType":I
    goto :goto_1

    .line 1758
    .restart local v3    # "fragment":Lcom/samsung/groupcast/application/fragment/FlightModeDialogFragment;
    :catch_1
    move-exception v0

    .line 1759
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .end local v0    # "e":Ljava/lang/Exception;
    .end local v1    # "filghtStringId":I
    .end local v2    # "flightFragmentType":I
    .end local v3    # "fragment":Lcom/samsung/groupcast/application/fragment/FlightModeDialogFragment;
    :cond_2
    move v5, v6

    .line 1765
    goto :goto_0
.end method

.method private clearWifiP2pListners()V
    .locals 1

    .prologue
    .line 1858
    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->removeOnP2PEventListener(Lcom/samsung/groupcast/net/wifi/WifiP2PUtils$OnP2PEventListener;)V

    .line 1859
    return-void
.end method

.method private doJoin(Lcom/samsung/groupcast/core/messaging/NfcMsg;)V
    .locals 2
    .param p1, "msg"    # Lcom/samsung/groupcast/core/messaging/NfcMsg;

    .prologue
    .line 1503
    iget-object v0, p1, Lcom/samsung/groupcast/core/messaging/NfcMsg;->session_summary:Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/samsung/groupcast/core/messaging/NfcMsg;->session_pin:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1504
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Joining session! "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p1, Lcom/samsung/groupcast/core/messaging/NfcMsg;->wifi_ssid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 1505
    iget-object v0, p1, Lcom/samsung/groupcast/core/messaging/NfcMsg;->session_summary:Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;

    iget-object v1, p1, Lcom/samsung/groupcast/core/messaging/NfcMsg;->session_pin:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/samsung/groupcast/application/start/StartActivity;->doJoin(Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;Ljava/lang/String;)V

    .line 1509
    :goto_0
    return-void

    .line 1507
    :cond_0
    const-string v0, "NULL session in message"

    invoke-static {v0}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private doJoin(Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;Ljava/lang/String;)V
    .locals 3
    .param p1, "summary"    # Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;
    .param p2, "pin"    # Ljava/lang/String;

    .prologue
    .line 1480
    const-string v1, "---"

    invoke-static {v1}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 1482
    new-instance v0, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;

    invoke-direct {v0}, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;-><init>()V

    .line 1483
    .local v0, "joinDialog":Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;
    invoke-virtual {v0, p1, p2}, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->doJoin(Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;Ljava/lang/String;)V

    .line 1484
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "TAG_JOIN_SESSION_DIALOG_FRAGMENT"

    invoke-virtual {v0, v1, v2}, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 1485
    const/4 v0, 0x0

    .line 1486
    return-void
.end method

.method private doJoinFromBand(Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;Ljava/lang/String;)V
    .locals 3
    .param p1, "summary"    # Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;
    .param p2, "pin"    # Ljava/lang/String;

    .prologue
    .line 1489
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "TAG_JOIN_SESSION_DIALOG_FRAGMENT"

    invoke-virtual {v1, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1490
    sget-object v1, Lcom/samsung/groupcast/application/start/StartActivity;->TAG:Ljava/lang/String;

    const-string v2, "Already progressing"

    invoke-static {v1, v2}, Lcom/samsung/groupcast/application/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1500
    :goto_0
    return-void

    .line 1494
    :cond_0
    const/4 v1, 0x0

    sput-boolean v1, Lcom/samsung/groupcast/application/start/StartActivity;->mNfcConnectOngoing:Z

    .line 1496
    new-instance v0, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;

    invoke-direct {v0}, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;-><init>()V

    .line 1497
    .local v0, "joinDialog":Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;
    const/4 v1, 0x1

    invoke-virtual {v0, v1, p2}, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->setNfcAutoConnectionInfo(ZLjava/lang/String;)V

    .line 1498
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "TAG_JOIN_SESSION_DIALOG_FRAGMENT"

    invoke-virtual {v0, v1, v2}, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 1499
    const/4 v0, 0x0

    .line 1500
    goto :goto_0
.end method

.method private doUpdate()V
    .locals 6

    .prologue
    .line 479
    iget-object v4, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mSavedInstanceState:Landroid/os/Bundle;

    if-nez v4, :cond_0

    .line 480
    const/4 v1, 0x0

    .line 483
    .local v1, "localAppVerCode":I
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 484
    .local v2, "pm":Landroid/content/pm/PackageManager;
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->getPackageName()Ljava/lang/String;

    move-result-object v4

    const/16 v5, 0x40

    invoke-virtual {v2, v4, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v4

    iget v1, v4, Landroid/content/pm/PackageInfo;->versionCode:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 490
    .end local v2    # "pm":Landroid/content/pm/PackageManager;
    :goto_0
    new-instance v3, Lcom/samsung/groupcast/application/start/StartActivity$4;

    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, p0, v4, v1}, Lcom/samsung/groupcast/application/start/StartActivity$4;-><init>(Lcom/samsung/groupcast/application/start/StartActivity;Landroid/content/Context;I)V

    .line 502
    .local v3, "task":Lcom/samsung/groupcast/misc/update/UpdateCheckTask;
    sget-object v4, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Void;

    invoke-virtual {v3, v4, v5}, Lcom/samsung/groupcast/misc/update/UpdateCheckTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 510
    .end local v1    # "localAppVerCode":I
    .end local v3    # "task":Lcom/samsung/groupcast/misc/update/UpdateCheckTask;
    :cond_0
    invoke-static {}, Lcom/samsung/groupcast/application/StatLog;->getInstance()Lcom/samsung/groupcast/application/StatLog;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/groupcast/application/StatLog;->openAndSendData()V

    .line 511
    return-void

    .line 486
    .restart local v1    # "localAppVerCode":I
    :catch_0
    move-exception v0

    .line 487
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private forwardStubStartActivity()Z
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2738
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-static {v3}, Lcom/samsung/groupcast/application/IntentTools;->isSendDJModeIntent(Landroid/content/Intent;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-static {v3}, Lcom/samsung/groupcast/application/IntentTools;->isSendMultipleDJModeIntent(Landroid/content/Intent;)Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    move v3, v5

    :goto_0
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 2742
    .local v0, "intentFromMusicPlayer":Ljava/lang/Boolean;
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-static {v3}, Lcom/samsung/groupcast/application/IntentTools;->isSendVideoIntent(Landroid/content/Intent;)Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 2743
    .local v2, "intentFromVideoPlayer":Ljava/lang/Boolean;
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-static {v3}, Lcom/samsung/groupcast/application/IntentTools;->isSendSconnectIntent(Landroid/content/Intent;)Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 2745
    .local v1, "intentFromSconnect":Ljava/lang/Boolean;
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 2748
    :goto_1
    return v5

    .end local v0    # "intentFromMusicPlayer":Ljava/lang/Boolean;
    .end local v1    # "intentFromSconnect":Ljava/lang/Boolean;
    .end local v2    # "intentFromVideoPlayer":Ljava/lang/Boolean;
    :cond_1
    move v3, v4

    .line 2738
    goto :goto_0

    .restart local v0    # "intentFromMusicPlayer":Ljava/lang/Boolean;
    .restart local v1    # "intentFromSconnect":Ljava/lang/Boolean;
    .restart local v2    # "intentFromVideoPlayer":Ljava/lang/Boolean;
    :cond_2
    move v5, v4

    .line 2748
    goto :goto_1
.end method

.method private getCheckPasswordState()V
    .locals 2

    .prologue
    .line 1586
    invoke-static {}, Lcom/samsung/groupcast/application/Preferences;->getCheckPasswordPreference()Z

    move-result v0

    iput-boolean v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mIsCheckedPasswordInput:Z

    .line 1587
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mPasswordCheckBox:Landroid/widget/CheckBox;

    iget-boolean v1, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mIsCheckedPasswordInput:Z

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 1589
    return-void
.end method

.method public static getDownloadListFromXML(Ljava/io/InputStream;)Ljava/util/ArrayList;
    .locals 24
    .param p0, "input"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/InputStream;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/groupcast/application/start/MoreGameItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1943
    new-instance v21, Ljava/util/ArrayList;

    invoke-direct/range {v21 .. v21}, Ljava/util/ArrayList;-><init>()V

    .line 1944
    .local v21, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/groupcast/application/start/MoreGameItem;>;"
    const/16 v18, 0x0

    .line 1945
    .local v18, "doc":Lorg/w3c/dom/Document;
    const/16 v22, 0x0

    .line 1948
    .local v22, "nl":Lorg/w3c/dom/NodeList;
    invoke-static/range {p0 .. p0}, Lcom/samsung/groupcast/misc/utility/XmlTools;->getDom(Ljava/io/InputStream;)Lorg/w3c/dom/Document;

    move-result-object v18

    .line 1949
    if-eqz v18, :cond_1

    .line 1950
    const-string v2, "appInfo"

    move-object/from16 v0, v18

    invoke-interface {v0, v2}, Lorg/w3c/dom/Document;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v22

    .line 1955
    if-eqz v22, :cond_1

    .line 1956
    const/16 v20, 0x0

    .local v20, "i":I
    :goto_0
    invoke-interface/range {v22 .. v22}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v2

    move/from16 v0, v20

    if-ge v0, v2, :cond_1

    .line 1957
    move-object/from16 v0, v22

    move/from16 v1, v20

    invoke-interface {v0, v1}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v19

    check-cast v19, Lorg/w3c/dom/Element;

    .line 1958
    .local v19, "elem":Lorg/w3c/dom/Element;
    const-string v2, "productID"

    move-object/from16 v0, v19

    invoke-static {v0, v2}, Lcom/samsung/groupcast/misc/utility/XmlTools;->getValue(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1959
    .local v3, "productID":Ljava/lang/String;
    const-string v4, ""

    .line 1960
    .local v4, "productName":Ljava/lang/String;
    const-string v2, "productName"

    move-object/from16 v0, v19

    invoke-interface {v0, v2}, Lorg/w3c/dom/Element;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v23

    .line 1961
    .local v23, "productNameNodes":Lorg/w3c/dom/NodeList;
    invoke-interface/range {v23 .. v23}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v2

    if-lez v2, :cond_0

    .line 1962
    const/4 v2, 0x0

    move-object/from16 v0, v23

    invoke-interface {v0, v2}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v2

    invoke-interface {v2}, Lorg/w3c/dom/Node;->getTextContent()Ljava/lang/String;

    move-result-object v4

    .line 1964
    :cond_0
    const-string v2, "appId"

    move-object/from16 v0, v19

    invoke-static {v0, v2}, Lcom/samsung/groupcast/misc/utility/XmlTools;->getValue(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 1965
    .local v5, "appId":Ljava/lang/String;
    const-string v2, "screenShotImgURL"

    move-object/from16 v0, v19

    invoke-static {v0, v2}, Lcom/samsung/groupcast/misc/utility/XmlTools;->getValue(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 1966
    .local v6, "screenShotImgURL":Ljava/lang/String;
    const-string v2, "productIconImgURL"

    move-object/from16 v0, v19

    invoke-static {v0, v2}, Lcom/samsung/groupcast/misc/utility/XmlTools;->getValue(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 1967
    .local v7, "productIconImgURL":Ljava/lang/String;
    const-string v2, "currencyUnit"

    move-object/from16 v0, v19

    invoke-static {v0, v2}, Lcom/samsung/groupcast/misc/utility/XmlTools;->getValue(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 1968
    .local v8, "currencyUnit":Ljava/lang/String;
    const-string v2, "price"

    move-object/from16 v0, v19

    invoke-static {v0, v2}, Lcom/samsung/groupcast/misc/utility/XmlTools;->getValue(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 1969
    .local v9, "price":Ljava/lang/String;
    const-string v2, "discountPrice"

    move-object/from16 v0, v19

    invoke-static {v0, v2}, Lcom/samsung/groupcast/misc/utility/XmlTools;->getValue(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 1970
    .local v10, "discountPrice":Ljava/lang/String;
    const-string v2, "discountFlag"

    move-object/from16 v0, v19

    invoke-static {v0, v2}, Lcom/samsung/groupcast/misc/utility/XmlTools;->getValue(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 1971
    .local v11, "discountFlag":Ljava/lang/String;
    const-string v2, "versionName"

    move-object/from16 v0, v19

    invoke-static {v0, v2}, Lcom/samsung/groupcast/misc/utility/XmlTools;->getValue(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 1972
    .local v12, "versionName":Ljava/lang/String;
    const-string v2, "versionCode"

    move-object/from16 v0, v19

    invoke-static {v0, v2}, Lcom/samsung/groupcast/misc/utility/XmlTools;->getValue(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 1973
    .local v13, "versionCode":Ljava/lang/String;
    const-string v2, "realContentSize"

    move-object/from16 v0, v19

    invoke-static {v0, v2}, Lcom/samsung/groupcast/misc/utility/XmlTools;->getValue(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 1975
    .local v14, "realContentSize":Ljava/lang/String;
    const-string v2, "sellerName"

    move-object/from16 v0, v19

    invoke-static {v0, v2}, Lcom/samsung/groupcast/misc/utility/XmlTools;->getValue(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 1977
    .local v15, "sellerName":Ljava/lang/String;
    const-string v2, "categoryName"

    move-object/from16 v0, v19

    invoke-static {v0, v2}, Lcom/samsung/groupcast/misc/utility/XmlTools;->getValue(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 1979
    .local v16, "categoryName":Ljava/lang/String;
    const-string v2, "averageRating"

    move-object/from16 v0, v19

    invoke-static {v0, v2}, Lcom/samsung/groupcast/misc/utility/XmlTools;->getValue(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    .line 1981
    .local v17, "averageRating":Ljava/lang/String;
    new-instance v2, Lcom/samsung/groupcast/application/start/MoreGameItem;

    invoke-direct/range {v2 .. v17}, Lcom/samsung/groupcast/application/start/MoreGameItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1956
    add-int/lit8 v20, v20, 0x1

    goto/16 :goto_0

    .line 1987
    .end local v3    # "productID":Ljava/lang/String;
    .end local v4    # "productName":Ljava/lang/String;
    .end local v5    # "appId":Ljava/lang/String;
    .end local v6    # "screenShotImgURL":Ljava/lang/String;
    .end local v7    # "productIconImgURL":Ljava/lang/String;
    .end local v8    # "currencyUnit":Ljava/lang/String;
    .end local v9    # "price":Ljava/lang/String;
    .end local v10    # "discountPrice":Ljava/lang/String;
    .end local v11    # "discountFlag":Ljava/lang/String;
    .end local v12    # "versionName":Ljava/lang/String;
    .end local v13    # "versionCode":Ljava/lang/String;
    .end local v14    # "realContentSize":Ljava/lang/String;
    .end local v15    # "sellerName":Ljava/lang/String;
    .end local v16    # "categoryName":Ljava/lang/String;
    .end local v17    # "averageRating":Ljava/lang/String;
    .end local v19    # "elem":Lorg/w3c/dom/Element;
    .end local v20    # "i":I
    .end local v23    # "productNameNodes":Lorg/w3c/dom/NodeList;
    :cond_1
    return-object v21
.end method

.method private getNicknameState()V
    .locals 4

    .prologue
    .line 2297
    invoke-static {}, Lcom/samsung/groupcast/application/Preferences;->getNicknamePreference()Ljava/lang/String;

    move-result-object v1

    .line 2299
    .local v1, "name":Ljava/lang/String;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2300
    :cond_0
    invoke-static {}, Lcom/samsung/groupcast/application/Environment;->getUserName()Ljava/lang/String;

    move-result-object v1

    .line 2301
    invoke-static {v1}, Lcom/samsung/groupcast/application/Preferences;->setNicknamePreference(Ljava/lang/String;)V

    .line 2304
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]\'s group"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2305
    .local v0, "fullName":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mNickName:Landroid/widget/TextView;

    if-eqz v2, :cond_2

    .line 2306
    iget-object v2, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mNickName:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2308
    :cond_2
    return-void
.end method

.method private hidePinCodeInputDialog()V
    .locals 4

    .prologue
    .line 2381
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->isFinishing()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2396
    :cond_0
    :goto_0
    return-void

    .line 2385
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "TAG_PIN_DIALOG"

    invoke-virtual {v1, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2388
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-string v3, "TAG_PIN_DIALOG"

    invoke-virtual {v2, v3}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2391
    :catch_0
    move-exception v0

    .line 2392
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private hideScanningLayout()V
    .locals 3

    .prologue
    .line 2716
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 2717
    .local v0, "animation":Landroid/view/animation/Animation;
    const-wide/16 v1, 0x12c

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 2718
    new-instance v1, Lcom/samsung/groupcast/application/start/StartActivity$13;

    invoke-direct {v1, p0}, Lcom/samsung/groupcast/application/start/StartActivity$13;-><init>(Lcom/samsung/groupcast/application/start/StartActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 2732
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setFillBefore(Z)V

    .line 2733
    iget-object v1, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mScanningLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 2734
    iget-object v1, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mApScanStop:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->clearAnimation()V

    .line 2735
    return-void
.end method

.method private initSetupLayout()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1628
    new-instance v0, Lcom/samsung/groupcast/application/start/StartActivity$StartPagerAdapter;

    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/groupcast/application/start/StartActivity$StartPagerAdapter;-><init>(Landroid/app/FragmentManager;)V

    iput-object v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mPagerAdapter:Lcom/samsung/groupcast/application/start/StartActivity$StartPagerAdapter;

    .line 1629
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->contentView:Landroid/view/View;

    const v1, 0x7f070037

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/samsung/groupcast/view/NoSwipableViewPager;

    iput-object v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mPager:Lcom/samsung/groupcast/view/NoSwipableViewPager;

    .line 1630
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mPager:Lcom/samsung/groupcast/view/NoSwipableViewPager;

    iget-object v1, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mPagerAdapter:Lcom/samsung/groupcast/application/start/StartActivity$StartPagerAdapter;

    invoke-virtual {v0, v1}, Lcom/samsung/groupcast/view/NoSwipableViewPager;->setAdapter(Landroid/support/v4/view/FixedPagerAdapter;)V

    .line 1632
    invoke-static {}, Lcom/samsung/groupcast/application/StatBigDataLog;->getInstance()Lcom/samsung/groupcast/application/StatBigDataLog;

    move-result-object v0

    sget-object v1, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;->START:Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

    invoke-virtual {v0, v1}, Lcom/samsung/groupcast/application/StatBigDataLog;->pushLog(Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;)V

    .line 1664
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mPager:Lcom/samsung/groupcast/view/NoSwipableViewPager;

    new-instance v1, Lcom/samsung/groupcast/application/start/StartActivity$9;

    invoke-direct {v1, p0}, Lcom/samsung/groupcast/application/start/StartActivity$9;-><init>(Lcom/samsung/groupcast/application/start/StartActivity;)V

    invoke-virtual {v0, v1}, Lcom/samsung/groupcast/view/NoSwipableViewPager;->setOnPageChangeListener(Landroid/support/v4/view/FixedViewPager$OnPageChangeListener;)V

    .line 1702
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->contentView:Landroid/view/View;

    const v1, 0x7f070036

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1703
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->contentView:Landroid/view/View;

    const v1, 0x7f070038

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1704
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->contentView:Landroid/view/View;

    const v1, 0x7f07003a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1709
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1710
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ActionBar;->show()V

    .line 1712
    :cond_0
    return-void
.end method

.method private initStartTextView()V
    .locals 6

    .prologue
    .line 540
    iget-object v3, p0, Lcom/samsung/groupcast/application/start/StartActivity;->contentView:Landroid/view/View;

    const v4, 0x7f070036

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    .line 546
    .local v2, "setupLyout":Landroid/widget/LinearLayout;
    new-instance v0, Lcom/samsung/groupcast/application/start/StartActivity$5;

    invoke-direct {v0, p0}, Lcom/samsung/groupcast/application/start/StartActivity$5;-><init>(Lcom/samsung/groupcast/application/start/StartActivity;)V

    .line 553
    .local v0, "disabledAccessibilityDelegate":Landroid/view/View$AccessibilityDelegate;
    new-instance v1, Lcom/samsung/groupcast/application/start/StartActivity$6;

    invoke-direct {v1, p0}, Lcom/samsung/groupcast/application/start/StartActivity$6;-><init>(Lcom/samsung/groupcast/application/start/StartActivity;)V

    .line 563
    .local v1, "enabledAccessibilityDelegate":Landroid/view/View$AccessibilityDelegate;
    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    .line 565
    new-instance v3, Lcom/samsung/groupcast/application/start/StartActivity$7;

    invoke-direct {v3, p0, v2, v1}, Lcom/samsung/groupcast/application/start/StartActivity$7;-><init>(Lcom/samsung/groupcast/application/start/StartActivity;Landroid/widget/LinearLayout;Landroid/view/View$AccessibilityDelegate;)V

    const-wide/16 v4, 0x7d0

    invoke-static {v3, v4, v5}, Lcom/samsung/groupcast/application/MainQueue;->postDelayed(Ljava/lang/Runnable;J)V

    .line 613
    return-void
.end method

.method private joinGroupAfterPasswordSubmitted(Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;Ljava/lang/String;Z)V
    .locals 3
    .param p1, "dialogFragment"    # Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;
    .param p2, "pin"    # Ljava/lang/String;
    .param p3, "dialogCancelled"    # Z

    .prologue
    .line 2440
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2441
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "TAG_JOIN_SESSION_DIALOG_FRAGMENT"

    invoke-virtual {v1, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;

    .line 2445
    .local v0, "joinDialog":Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;
    if-eqz v0, :cond_0

    .line 2446
    if-eqz p3, :cond_2

    .line 2447
    invoke-virtual {v0}, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->dismiss()V

    .line 2448
    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiAPUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->isRvfHotspotEnabled()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2449
    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiAPUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->releaseRvfHotspot()V

    .line 2459
    .end local v0    # "joinDialog":Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;
    :cond_0
    :goto_0
    return-void

    .line 2450
    .restart local v0    # "joinDialog":Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;
    :cond_1
    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->isWifiEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getWifiSSID()Ljava/lang/String;

    move-result-object v1

    const-string v2, "GroupPlay-"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2453
    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->disconnectFromCurrentAP()V

    goto :goto_0

    .line 2456
    :cond_2
    invoke-virtual {v0, p2}, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->doJoinAfterPasswordInput(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private makeWiFiInfoItem(Landroid/net/wifi/ScanResult;)Lcom/samsung/groupcast/application/start/JoinAPAdapter$WiFiInfoItem;
    .locals 4
    .param p1, "device"    # Landroid/net/wifi/ScanResult;

    .prologue
    .line 1212
    new-instance v0, Lcom/samsung/groupcast/application/start/JoinAPAdapter$WiFiInfoItem;

    iget-object v1, p1, Landroid/net/wifi/ScanResult;->SSID:Ljava/lang/String;

    iget-object v2, p1, Landroid/net/wifi/ScanResult;->BSSID:Ljava/lang/String;

    iget-object v3, p1, Landroid/net/wifi/ScanResult;->SSID:Ljava/lang/String;

    invoke-virtual {p0, v3}, Lcom/samsung/groupcast/application/start/StartActivity;->IsProtected(Ljava/lang/String;)Z

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/groupcast/application/start/JoinAPAdapter$WiFiInfoItem;-><init>(Ljava/lang/CharSequence;Ljava/lang/String;Z)V

    .line 1214
    .local v0, "item":Lcom/samsung/groupcast/application/start/JoinAPAdapter$WiFiInfoItem;
    return-object v0
.end method

.method private onWiFiSelected(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "SSID"    # Ljava/lang/String;
    .param p2, "bssid"    # Ljava/lang/String;

    .prologue
    .line 1102
    const-string v2, "---"

    invoke-static {v2}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 1104
    iget-boolean v2, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mActive:Z

    if-nez v2, :cond_0

    .line 1105
    const-string v2, "activity paused"

    invoke-static {v2}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 1121
    :goto_0
    return-void

    .line 1109
    :cond_0
    iget-object v2, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mWifiUtils:Lcom/samsung/groupcast/net/wifi/WifiUtils;

    invoke-virtual {v2}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getWifiSSID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1110
    invoke-direct {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->onWifiAPConnectCompleted()V

    goto :goto_0

    .line 1114
    :cond_1
    new-instance v1, Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment;

    invoke-direct {v1}, Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment;-><init>()V

    .line 1115
    .local v1, "connectDialog":Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment;
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-string v3, "AUTOCONNECT_PROGRESS_DIALOG_FRAGMENT"

    invoke-virtual {v1, v2, v3}, Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 1117
    const-string v0, "GroupCast"

    .line 1118
    .local v0, "PASS":Ljava/lang/String;
    invoke-virtual {v1, p1, p2, v0}, Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment;->connect(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1120
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mIsConnectOngoing:Z

    goto :goto_0
.end method

.method private onWifiAPConnectCompleted()V
    .locals 1

    .prologue
    .line 1355
    iget-boolean v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mActive:Z

    if-nez v0, :cond_0

    .line 1356
    const-string v0, "activity paused"

    invoke-static {v0}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 1361
    :goto_0
    return-void

    .line 1359
    :cond_0
    invoke-direct {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->showJoinSessionDialog()V

    goto :goto_0
.end method

.method private openJoinDialog()V
    .locals 3

    .prologue
    .line 643
    new-instance v0, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;

    invoke-direct {v0}, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;-><init>()V

    .line 644
    .local v0, "joinDialog":Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "TAG_JOIN_SESSION_DIALOG_FRAGMENT"

    invoke-virtual {v0, v1, v2}, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 645
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentManager;->executePendingTransactions()Z

    .line 646
    const/4 v0, 0x0

    .line 647
    return-void
.end method

.method private processNfcIntent(Landroid/content/Intent;)V
    .locals 10
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 1512
    const-string v4, "---"

    invoke-static {v4}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 1513
    const-string v4, "android.nfc.extra.NDEF_MESSAGES"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->getParcelableArrayExtra(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v3

    .line 1514
    .local v3, "rawMsgs":[Landroid/os/Parcelable;
    if-nez v3, :cond_0

    .line 1563
    :goto_0
    return-void

    .line 1518
    :cond_0
    const-string v4, "android.nfc.extra.NDEF_MESSAGES"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 1520
    sget-boolean v4, Lcom/samsung/groupcast/application/start/StartActivity;->mNfcConnectOngoing:Z

    if-eqz v4, :cond_1

    .line 1521
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const-string v5, "WiFi Connection already in progress"

    invoke-static {v4, v5, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 1526
    :cond_1
    sput-boolean v9, Lcom/samsung/groupcast/application/start/StartActivity;->mNfcConnectOngoing:Z

    .line 1528
    aget-object v2, v3, v8

    check-cast v2, Landroid/nfc/NdefMessage;

    .line 1530
    .local v2, "ndefMsg":Landroid/nfc/NdefMessage;
    :try_start_0
    new-instance v4, Lcom/samsung/groupcast/core/messaging/NfcMsg;

    invoke-direct {v4, v2}, Lcom/samsung/groupcast/core/messaging/NfcMsg;-><init>(Landroid/nfc/NdefMessage;)V

    iput-object v4, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mNfcMsg:Lcom/samsung/groupcast/core/messaging/NfcMsg;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1538
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "processing nfc: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mNfcMsg:Lcom/samsung/groupcast/core/messaging/NfcMsg;

    iget-object v5, v5, Lcom/samsung/groupcast/core/messaging/NfcMsg;->wifi_bssid:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " / "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mNfcMsg:Lcom/samsung/groupcast/core/messaging/NfcMsg;

    iget-object v5, v5, Lcom/samsung/groupcast/core/messaging/NfcMsg;->wifi_ssid:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mWifiUtils:Lcom/samsung/groupcast/net/wifi/WifiUtils;

    invoke-virtual {v5}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getWifiBSSID()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 1540
    iget-object v4, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mWifiUtils:Lcom/samsung/groupcast/net/wifi/WifiUtils;

    invoke-virtual {v4}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getWifiStatus()Ljava/lang/String;

    move-result-object v4

    const-string v5, "CONNECTED"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mNfcMsg:Lcom/samsung/groupcast/core/messaging/NfcMsg;

    iget-object v4, v4, Lcom/samsung/groupcast/core/messaging/NfcMsg;->wifi_bssid:Ljava/lang/String;

    iget-object v5, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mWifiUtils:Lcom/samsung/groupcast/net/wifi/WifiUtils;

    invoke-virtual {v5}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getWifiBSSID()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_4

    .line 1542
    :cond_2
    iget-object v4, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mNfcMsg:Lcom/samsung/groupcast/core/messaging/NfcMsg;

    iget-object v4, v4, Lcom/samsung/groupcast/core/messaging/NfcMsg;->wifi_passwd:Ljava/lang/String;

    if-nez v4, :cond_3

    .line 1543
    sget-object v4, Lcom/samsung/groupcast/application/start/StartActivity;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Not connected to WiFi network: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mNfcMsg:Lcom/samsung/groupcast/core/messaging/NfcMsg;

    iget-object v6, v6, Lcom/samsung/groupcast/core/messaging/NfcMsg;->wifi_ssid:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " ("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mNfcMsg:Lcom/samsung/groupcast/core/messaging/NfcMsg;

    iget-object v6, v6, Lcom/samsung/groupcast/core/messaging/NfcMsg;->wifi_bssid:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ") [connected to: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mWifiUtils:Lcom/samsung/groupcast/net/wifi/WifiUtils;

    invoke-virtual {v6}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getWifiBSSID()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/groupcast/application/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1546
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f080017

    new-array v6, v9, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mNfcMsg:Lcom/samsung/groupcast/core/messaging/NfcMsg;

    iget-object v7, v7, Lcom/samsung/groupcast/core/messaging/NfcMsg;->wifi_ssid:Ljava/lang/String;

    aput-object v7, v6, v8

    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/groupcast/application/App;->getSanitizedString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {p0, v4, v9}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    .line 1551
    sput-boolean v8, Lcom/samsung/groupcast/application/start/StartActivity;->mNfcConnectOngoing:Z

    goto/16 :goto_0

    .line 1531
    :catch_0
    move-exception v1

    .line 1532
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f080039

    invoke-virtual {p0, v5}, Lcom/samsung/groupcast/application/start/StartActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v9}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    .line 1534
    sput-boolean v8, Lcom/samsung/groupcast/application/start/StartActivity;->mNfcConnectOngoing:Z

    goto/16 :goto_0

    .line 1555
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_3
    new-instance v0, Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment;

    invoke-direct {v0}, Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment;-><init>()V

    .line 1556
    .local v0, "connectDialog":Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment;
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v4

    const-string v5, "TAG_CONNECT_DIALOG_FRAGMENT"

    invoke-virtual {v0, v4, v5}, Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 1557
    iget-object v4, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mNfcMsg:Lcom/samsung/groupcast/core/messaging/NfcMsg;

    iget-object v4, v4, Lcom/samsung/groupcast/core/messaging/NfcMsg;->wifi_ssid:Ljava/lang/String;

    iget-object v5, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mNfcMsg:Lcom/samsung/groupcast/core/messaging/NfcMsg;

    iget-object v5, v5, Lcom/samsung/groupcast/core/messaging/NfcMsg;->wifi_bssid:Ljava/lang/String;

    iget-object v6, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mNfcMsg:Lcom/samsung/groupcast/core/messaging/NfcMsg;

    iget-object v6, v6, Lcom/samsung/groupcast/core/messaging/NfcMsg;->wifi_passwd:Ljava/lang/String;

    invoke-virtual {v0, v4, v5, v6}, Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment;->connect(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1560
    .end local v0    # "connectDialog":Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment;
    :cond_4
    iget-object v4, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mNfcMsg:Lcom/samsung/groupcast/core/messaging/NfcMsg;

    invoke-direct {p0, v4}, Lcom/samsung/groupcast/application/start/StartActivity;->doJoin(Lcom/samsung/groupcast/core/messaging/NfcMsg;)V

    .line 1561
    sput-boolean v8, Lcom/samsung/groupcast/application/start/StartActivity;->mNfcConnectOngoing:Z

    goto/16 :goto_0
.end method

.method private registerWifiDialogCancelReceiver()V
    .locals 3

    .prologue
    .line 2670
    new-instance v1, Lcom/samsung/groupcast/application/start/StartActivity$WifiCancelReceiver;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/samsung/groupcast/application/start/StartActivity$WifiCancelReceiver;-><init>(Lcom/samsung/groupcast/application/start/StartActivity;Lcom/samsung/groupcast/application/start/StartActivity$1;)V

    iput-object v1, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mWifiCancelReceiver:Lcom/samsung/groupcast/application/start/StartActivity$WifiCancelReceiver;

    .line 2671
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 2672
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "android.net.wifi.WIFI_DIALOG_CANCEL_ACTION"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 2673
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mWifiCancelReceiver:Lcom/samsung/groupcast/application/start/StartActivity$WifiCancelReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 2674
    return-void
.end method

.method private saveCheckNearbyCheckBox()V
    .locals 1

    .prologue
    .line 1607
    iget-boolean v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mIsCheckedNearbyFrientInput:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/groupcast/application/Preferences;->setNearByFriendsAdvertisePreference(Ljava/lang/Boolean;)V

    .line 1608
    return-void
.end method

.method private saveCheckPasswordState()V
    .locals 1

    .prologue
    .line 1598
    iget-boolean v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mIsCheckedPasswordInput:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/groupcast/application/Preferences;->setCheckPasswordPreference(Ljava/lang/Boolean;)V

    .line 1599
    return-void
.end method

.method private setWifiP2pListners()V
    .locals 1

    .prologue
    .line 1854
    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->addOnP2PEventListener(Lcom/samsung/groupcast/net/wifi/WifiP2PUtils$OnP2PEventListener;)V

    .line 1855
    return-void
.end method

.method private showJoinSessionDialog()V
    .locals 3

    .prologue
    .line 1364
    new-instance v0, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;

    invoke-direct {v0}, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;-><init>()V

    .line 1365
    .local v0, "joinDialog":Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;
    iget-object v1, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mSummittingPincode:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mSummittingPincode:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1366
    iget-object v1, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mSummittingPincode:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->setSummittingPincode(Ljava/lang/String;)V

    .line 1368
    :cond_0
    iget-boolean v1, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mIsCheckedPasswordInput:Z

    invoke-virtual {v0, v1}, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->setCheckedShowPasswordCheckBox(Z)V

    .line 1369
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "TAG_JOIN_SESSION_DIALOG_FRAGMENT"

    invoke-virtual {v0, v1, v2}, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 1370
    return-void
.end method

.method private showPasswordInputDialog()V
    .locals 4

    .prologue
    .line 2362
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->isFinishing()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2378
    :cond_0
    :goto_0
    return-void

    .line 2366
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-string v3, "TAG_PIN_DIALOG"

    invoke-virtual {v2, v3}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v2

    if-nez v2, :cond_0

    .line 2370
    :cond_2
    const/4 v2, 0x0

    :try_start_0
    invoke-static {v2}, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->newInstance(I)Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;

    move-result-object v1

    .line 2372
    .local v1, "fragment":Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;
    invoke-virtual {v1, p0}, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->setDelegate(Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment$PinCodeInputDialogFragmentDelegate;)V

    .line 2373
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-string v3, "TAG_PIN_DIALOG"

    invoke-virtual {v1, v2, v3}, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2374
    .end local v1    # "fragment":Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;
    :catch_0
    move-exception v0

    .line 2375
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private showScanningLayout()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 2699
    iget-object v2, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mMobileAPListView:Lcom/samsung/groupcast/application/start/ApListView;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Lcom/samsung/groupcast/application/start/ApListView;->setVisibility(I)V

    .line 2700
    iget-object v2, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mScanningLayout:Landroid/widget/RelativeLayout;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 2701
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/4 v2, 0x0

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-direct {v0, v2, v3}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 2703
    .local v0, "animation":Landroid/view/animation/Animation;
    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 2704
    invoke-virtual {v0, v4}, Landroid/view/animation/Animation;->setFillEnabled(Z)V

    .line 2705
    invoke-virtual {v0, v4}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    .line 2706
    iget-object v2, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mScanningLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v4}, Landroid/widget/RelativeLayout;->setClipChildren(Z)V

    .line 2707
    iget-object v2, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mScanningLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v0}, Landroid/widget/RelativeLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 2709
    const/high16 v2, 0x7f050000

    invoke-static {p0, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    .line 2710
    .local v1, "apScanAnimation":Landroid/view/animation/Animation;
    const-wide/16 v2, 0x3e8

    invoke-virtual {v1, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 2711
    iget-object v2, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mApScanStop:Landroid/widget/ImageView;

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 2712
    return-void
.end method

.method private startSearch()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1236
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->isChangingConfigurations()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1237
    iput-boolean v4, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mIsSearchOngoing:Z

    .line 1239
    :cond_0
    iget-boolean v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mIsSearchOngoing:Z

    if-eqz v0, :cond_1

    .line 1257
    :goto_0
    return-void

    .line 1242
    :cond_1
    invoke-direct {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->showScanningLayout()V

    .line 1244
    iput-boolean v5, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mIsSearchOngoing:Z

    .line 1246
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mAPHandler:Lcom/samsung/groupcast/application/start/StartActivity$WHandler;

    iget-object v1, p0, Lcom/samsung/groupcast/application/start/StartActivity;->startStop:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/samsung/groupcast/application/start/StartActivity$WHandler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1247
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mAPHandler:Lcom/samsung/groupcast/application/start/StartActivity$WHandler;

    iget-object v1, p0, Lcom/samsung/groupcast/application/start/StartActivity;->startStop:Ljava/lang/Runnable;

    const-wide/16 v2, 0x2710

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/groupcast/application/start/StartActivity$WHandler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1249
    sget-object v0, Lcom/samsung/groupcast/core/messaging/MessagingClient;->DefaultInterface:Lcom/samsung/groupcast/core/messaging/MessagingClient$MAGNET_CONNECTION_TYPE;

    invoke-virtual {v0}, Lcom/samsung/groupcast/core/messaging/MessagingClient$MAGNET_CONNECTION_TYPE;->value()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 1254
    :pswitch_0
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mWifiUtils:Lcom/samsung/groupcast/net/wifi/WifiUtils;

    invoke-virtual {v0, v4, v5}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->startScan(ZZ)V

    goto :goto_0

    .line 1251
    :pswitch_1
    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;

    move-result-object v0

    invoke-virtual {v0, v4, v5}, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->startScan(ZZ)V

    goto :goto_0

    .line 1249
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private stopSearch()V
    .locals 1

    .prologue
    .line 1291
    iget-boolean v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mIsSearchOngoing:Z

    if-nez v0, :cond_0

    .line 1296
    :goto_0
    return-void

    .line 1294
    :cond_0
    invoke-direct {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->hideScanningLayout()V

    .line 1295
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mIsSearchOngoing:Z

    goto :goto_0
.end method


# virtual methods
.method BandCacheRelease()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 2400
    iput-object v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mServiceType:Ljava/lang/String;

    .line 2401
    iput-object v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mMenuType:Ljava/lang/String;

    .line 2402
    invoke-static {v1}, Lcom/samsung/groupcast/application/App;->SET_EXTERNAL_APP(Z)V

    .line 2403
    invoke-static {v1}, Lcom/samsung/groupcast/application/App;->setRegister_Extra_App(Z)V

    .line 2404
    invoke-static {v0}, Lcom/samsung/groupcast/application/App;->setScreenId(Ljava/lang/String;)V

    .line 2405
    invoke-static {v0}, Lcom/samsung/groupcast/application/App;->setPeerScreenId(Ljava/lang/String;)V

    .line 2406
    return-void
.end method

.method protected BandInterfaceRelease()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1567
    const-string v0, "GroupPlayBand"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "BandInterfaceRelease [StartActivity] : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->getBandInterface()Lcom/sec/android/band/BandInterface;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1569
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->getBandInterface()Lcom/sec/android/band/BandInterface;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1570
    const-string v0, "GroupPlayBand"

    const-string v1, "unregister()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1571
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->getBandInterface()Lcom/sec/android/band/BandInterface;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/band/BandInterface;->unregister()Z

    .line 1574
    :cond_0
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mGp:Lcom/samsung/groupcast/GroupPlayManager;

    if-eqz v0, :cond_1

    .line 1575
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mGp:Lcom/samsung/groupcast/GroupPlayManager;

    invoke-virtual {v0}, Lcom/samsung/groupcast/GroupPlayManager;->unExtRegisterListener()V

    .line 1576
    iput-object v3, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mExternalEventHandler:Lcom/samsung/groupcast/GroupPlayManager$ExternalEventHandler;

    .line 1577
    iput-object v3, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mGp:Lcom/samsung/groupcast/GroupPlayManager;

    .line 1579
    :cond_1
    return-void
.end method

.method public IsProtected(Ljava/lang/String;)Z
    .locals 3
    .param p1, "ssid"    # Ljava/lang/String;

    .prologue
    .line 1373
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    const-string v2, "GroupPlay-"

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-le v1, v2, :cond_1

    .line 1374
    const-string v1, "GroupPlay-"

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "GroupPlay-"

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1376
    :cond_0
    const-string v1, "GroupPlay-"

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {p1, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 1378
    .local v0, "ssidForCheckingProtected":Ljava/lang/String;
    const-string v1, "P-"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1379
    const/4 v1, 0x1

    .line 1383
    .end local v0    # "ssidForCheckingProtected":Ljava/lang/String;
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected closeRemoteInviationProgress()V
    .locals 4

    .prologue
    .line 745
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-string v3, "TAG_REMOTE_INVITATION_PROGRESS_DIALOG_FRAGMENT"

    invoke-virtual {v2, v3}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;

    .line 749
    .local v1, "fragment":Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;
    if-eqz v1, :cond_0

    .line 750
    invoke-virtual {v1}, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;->dismiss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 756
    .end local v1    # "fragment":Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;
    :cond_0
    :goto_0
    return-void

    .line 753
    :catch_0
    move-exception v0

    .line 754
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method protected createGroup()V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 1769
    sget-object v0, Lcom/samsung/groupcast/core/messaging/MessagingClient;->DefaultInterface:Lcom/samsung/groupcast/core/messaging/MessagingClient$MAGNET_CONNECTION_TYPE;

    invoke-virtual {v0}, Lcom/samsung/groupcast/core/messaging/MessagingClient$MAGNET_CONNECTION_TYPE;->value()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 1812
    :goto_0
    :pswitch_0
    return-void

    .line 1771
    :pswitch_1
    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->isOxygenConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1772
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->startNextActivity()V

    goto :goto_0

    .line 1775
    :cond_0
    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getIBSSStatus()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    .line 1784
    :pswitch_2
    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiUtils;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->turnOxygenOnOff(Z)V

    goto :goto_0

    .line 1777
    :pswitch_3
    iput-boolean v1, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mAlreadyEn:Z

    .line 1778
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mWifiUtils:Lcom/samsung/groupcast/net/wifi/WifiUtils;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "OX_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getWifiSSID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->createOxygen(Ljava/lang/String;)V

    goto :goto_0

    .line 1792
    :pswitch_4
    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->getLastNetInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->getLastNetInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1794
    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->removeSession()V

    .line 1796
    :cond_1
    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->stopScan()V

    .line 1797
    invoke-virtual {p0, v1}, Lcom/samsung/groupcast/application/start/StartActivity;->setAutoConnectEnabled(Z)Z

    goto :goto_0

    .line 1803
    :pswitch_5
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mWifiUtils:Lcom/samsung/groupcast/net/wifi/WifiUtils;

    invoke-virtual {v0}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->stopScan()V

    .line 1804
    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiAPUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->isRvfHotspotEnabled()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1805
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->startNextActivity()V

    goto/16 :goto_0

    .line 1807
    :cond_2
    invoke-virtual {p0, v1}, Lcom/samsung/groupcast/application/start/StartActivity;->setAutoConnectEnabled(Z)Z

    goto/16 :goto_0

    .line 1769
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_5
        :pswitch_4
        :pswitch_1
    .end packed-switch

    .line 1775
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_2
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method protected createGroupHandler()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 277
    iput v1, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mModeStartOrJoin:I

    .line 279
    invoke-direct {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->checkAvailablityToConnect()Z

    move-result v0

    if-nez v0, :cond_0

    .line 280
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mModeStartOrJoin:I

    .line 291
    :goto_0
    return-void

    .line 284
    :cond_0
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mPasswordCheckBox:Landroid/widget/CheckBox;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mPasswordCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 285
    invoke-direct {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->showPasswordInputDialog()V

    goto :goto_0

    .line 287
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mPin:Ljava/lang/String;

    .line 288
    const v0, 0x7f080056

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 289
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->createGroup()V

    goto :goto_0
.end method

.method protected dismissProgressAndStartMainActivity()V
    .locals 3

    .prologue
    .line 1840
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    .line 1841
    .local v0, "fm":Landroid/app/FragmentManager;
    if-nez v0, :cond_1

    .line 1851
    :cond_0
    :goto_0
    return-void

    .line 1844
    :cond_1
    const-string v2, "AUTOCONNECT_PROGRESS_DIALOG_FRAGMENT"

    invoke-virtual {v0, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;

    .line 1847
    .local v1, "progressDialog":Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;
    if-eqz v1, :cond_0

    .line 1848
    invoke-virtual {v1}, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;->dismiss()V

    .line 1849
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->startNextActivity()V

    goto :goto_0
.end method

.method public doUpdateFromDisclaimer()V
    .locals 0

    .prologue
    .line 2771
    invoke-direct {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->doUpdate()V

    .line 2772
    return-void
.end method

.method public getPinOk()Z
    .locals 1

    .prologue
    .line 2559
    iget-boolean v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mPinOK:Z

    return v0
.end method

.method public handleScanResults(Ljava/util/List;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/net/wifi/ScanResult;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "scanResult":Ljava/util/List;, "Ljava/util/List<Landroid/net/wifi/ScanResult;>;"
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 1148
    iput v4, p0, Lcom/samsung/groupcast/application/start/StartActivity;->dummyCount:I

    .line 1149
    iget-object v5, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mMobileAPAdapter:Lcom/samsung/groupcast/application/start/JoinAPAdapter;

    monitor-enter v5

    .line 1150
    :try_start_0
    iget-object v2, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mWiFiInfoItems:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 1152
    iget-object v2, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mScanResult:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 1153
    iget-object v2, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mScanResult:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1155
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/wifi/ScanResult;

    .line 1156
    .local v1, "result":Landroid/net/wifi/ScanResult;
    iget-object v2, v1, Landroid/net/wifi/ScanResult;->SSID:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 1160
    sget-object v2, Lcom/samsung/groupcast/core/messaging/MessagingClient;->DefaultInterface:Lcom/samsung/groupcast/core/messaging/MessagingClient$MAGNET_CONNECTION_TYPE;

    invoke-virtual {v2}, Lcom/samsung/groupcast/core/messaging/MessagingClient$MAGNET_CONNECTION_TYPE;->value()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    .line 1174
    :pswitch_0
    iget-object v2, v1, Landroid/net/wifi/ScanResult;->SSID:Ljava/lang/String;

    const-string v6, "GroupPlay-"

    invoke-virtual {v2, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1175
    iget-object v2, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mWiFiInfoItems:Ljava/util/ArrayList;

    invoke-direct {p0, v1}, Lcom/samsung/groupcast/application/start/StartActivity;->makeWiFiInfoItem(Landroid/net/wifi/ScanResult;)Lcom/samsung/groupcast/application/start/JoinAPAdapter$WiFiInfoItem;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1207
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "result":Landroid/net/wifi/ScanResult;
    :catchall_0
    move-exception v2

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 1162
    .restart local v0    # "i$":Ljava/util/Iterator;
    .restart local v1    # "result":Landroid/net/wifi/ScanResult;
    :pswitch_1
    :try_start_1
    iget-object v6, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mWiFiInfoItems:Ljava/util/ArrayList;

    new-instance v7, Lcom/samsung/groupcast/application/start/JoinAPAdapter$WiFiInfoItem;

    iget-object v8, v1, Landroid/net/wifi/ScanResult;->SSID:Ljava/lang/String;

    iget-object v9, v1, Landroid/net/wifi/ScanResult;->BSSID:Ljava/lang/String;

    iget-object v2, v1, Landroid/net/wifi/ScanResult;->capabilities:Ljava/lang/String;

    if-eqz v2, :cond_1

    move v2, v3

    :goto_1
    invoke-direct {v7, v8, v9, v2}, Lcom/samsung/groupcast/application/start/JoinAPAdapter$WiFiInfoItem;-><init>(Ljava/lang/CharSequence;Ljava/lang/String;Z)V

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    move v2, v4

    goto :goto_1

    .line 1167
    :pswitch_2
    iget-object v2, v1, Landroid/net/wifi/ScanResult;->SSID:Ljava/lang/String;

    invoke-static {v2}, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->IsGroupPlayP2P(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1168
    iget-object v6, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mWiFiInfoItems:Ljava/util/ArrayList;

    new-instance v7, Lcom/samsung/groupcast/application/start/JoinAPAdapter$WiFiInfoItem;

    iget-object v8, v1, Landroid/net/wifi/ScanResult;->SSID:Ljava/lang/String;

    iget-object v9, v1, Landroid/net/wifi/ScanResult;->BSSID:Ljava/lang/String;

    iget-object v2, v1, Landroid/net/wifi/ScanResult;->capabilities:Ljava/lang/String;

    if-eqz v2, :cond_2

    move v2, v3

    :goto_2
    invoke-direct {v7, v8, v9, v2}, Lcom/samsung/groupcast/application/start/JoinAPAdapter$WiFiInfoItem;-><init>(Ljava/lang/CharSequence;Ljava/lang/String;Z)V

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    move v2, v4

    goto :goto_2

    .line 1189
    .end local v1    # "result":Landroid/net/wifi/ScanResult;
    :cond_3
    iget-object v2, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mMobileAPAdapter:Lcom/samsung/groupcast/application/start/JoinAPAdapter;

    iget-object v3, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mWiFiInfoItems:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Lcom/samsung/groupcast/application/start/JoinAPAdapter;->setItems(Ljava/util/ArrayList;)V

    .line 1191
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    if-eqz v2, :cond_5

    .line 1192
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-string v3, "TAG_JOIN_SESSION_DIALOG_FRAGMENT"

    invoke-virtual {v2, v3}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v2

    if-nez v2, :cond_4

    iget-boolean v2, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mIsConnectOngoing:Z

    if-eqz v2, :cond_5

    .line 1194
    :cond_4
    monitor-exit v5

    .line 1208
    :goto_3
    return-void

    .line 1206
    :cond_5
    invoke-direct {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->stopSearch()V

    .line 1207
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_3

    .line 1160
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public isDisclaimerFragmentChecked(Z)V
    .locals 2
    .param p1, "mChecked"    # Z

    .prologue
    .line 1718
    iput-boolean p1, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mDisclaimerChecked:Z

    .line 1719
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mStartNextBtn:Landroid/widget/LinearLayout;

    iget-boolean v1, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mDisclaimerChecked:Z

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 1720
    iget-boolean v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mDisclaimerChecked:Z

    if-nez v0, :cond_0

    .line 1721
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mStartNextBtn:Landroid/widget/LinearLayout;

    const v1, 0x3ecccccd    # 0.4f

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setAlpha(F)V

    .line 1726
    :goto_0
    return-void

    .line 1723
    :cond_0
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mStartNextBtn:Landroid/widget/LinearLayout;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setAlpha(F)V

    goto :goto_0
.end method

.method protected makeNextActivityIntent(Landroid/app/Activity;)Landroid/content/Intent;
    .locals 3
    .param p1, "callAct"    # Landroid/app/Activity;

    .prologue
    .line 1387
    const/4 v0, 0x0

    .line 1392
    .local v0, "intent":Landroid/content/Intent;
    new-instance v0, Landroid/content/Intent;

    .end local v0    # "intent":Landroid/content/Intent;
    const-class v1, Lcom/samsung/groupcast/application/main/MainActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1395
    .restart local v0    # "intent":Landroid/content/Intent;
    const-string v1, "ObjectStoreHash"

    invoke-static {}, Lcom/samsung/groupcast/application/ObjectStore;->getHash()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1398
    sget-object v1, Lcom/samsung/groupcast/core/messaging/MessagingClient;->DefaultInterface:Lcom/samsung/groupcast/core/messaging/MessagingClient$MAGNET_CONNECTION_TYPE;

    invoke-virtual {v1}, Lcom/samsung/groupcast/core/messaging/MessagingClient$MAGNET_CONNECTION_TYPE;->value()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 1421
    :goto_0
    iget-boolean v1, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mIsStartFromExternal:Z

    if-eqz v1, :cond_0

    .line 1422
    iget-boolean v1, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mIsStartFromGPSDK:Z

    if-eqz v1, :cond_3

    .line 1423
    invoke-direct {p0, v0}, Lcom/samsung/groupcast/application/start/StartActivity;->addExtraInfoFromGPSDK(Landroid/content/Intent;)V

    .line 1429
    :cond_0
    :goto_1
    return-object v0

    .line 1401
    :pswitch_0
    const-string v1, "GROUP_NAME"

    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->getP2pName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1403
    const-string v2, "PASSWORD"

    iget-object v1, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mPin:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mPin:Ljava/lang/String;

    :goto_2
    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1404
    const-string v1, "AP_NAME"

    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->getP2pName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1406
    const-string v1, "GC_HOTSPOT_TURNED_ON"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto :goto_0

    .line 1403
    :cond_1
    const-string v1, ""

    goto :goto_2

    .line 1410
    :pswitch_1
    const-string v1, "GROUP_NAME"

    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getWifiSSID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1412
    const-string v2, "PASSWORD"

    iget-object v1, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mPin:Ljava/lang/String;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mPin:Ljava/lang/String;

    :goto_3
    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1413
    const-string v1, "AP_NAME"

    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getWifiBSSID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1415
    const-string v1, "GC_HOTSPOT_TURNED_ON"

    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiAPUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->isRvfHotspotEnabled()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto :goto_0

    .line 1412
    :cond_2
    const-string v1, ""

    goto :goto_3

    .line 1425
    :cond_3
    invoke-direct {p0, v0}, Lcom/samsung/groupcast/application/start/StartActivity;->addExtraInfoFromExternalApp(Landroid/content/Intent;)V

    goto :goto_1

    .line 1398
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 2
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 651
    sget-object v0, Lcom/samsung/groupcast/application/start/StartActivity;->TAG:Ljava/lang/String;

    const-string v1, "StartActivity onActivityResult"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 652
    if-nez p1, :cond_1

    .line 653
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 654
    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiAPUtils;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->setNATModeForRVFMode(I)V

    .line 658
    :goto_0
    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiAPUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->setRvfHotspotEnable()Z

    .line 661
    :goto_1
    return-void

    .line 656
    :cond_0
    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiAPUtils;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->setNATModeForRVFMode(I)V

    goto :goto_0

    .line 660
    :cond_1
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/band/BandActivity;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_1
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    .line 665
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 667
    sget-object v0, Lcom/samsung/groupcast/core/messaging/MessagingClient;->DefaultInterface:Lcom/samsung/groupcast/core/messaging/MessagingClient$MAGNET_CONNECTION_TYPE;

    invoke-virtual {v0}, Lcom/samsung/groupcast/core/messaging/MessagingClient$MAGNET_CONNECTION_TYPE;->value()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 678
    :goto_0
    invoke-super {p0}, Lcom/sec/android/band/BandActivity;->onBackPressed()V

    .line 679
    return-void

    .line 669
    :pswitch_0
    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiUtils;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->turnOxygenOnOff(Z)V

    goto :goto_0

    .line 673
    :pswitch_1
    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->removeSession()V

    .line 674
    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->cancelConnect()V

    goto :goto_0

    .line 667
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 295
    iput-boolean v3, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mDisableNotificationBar:Z

    .line 296
    invoke-super {p0, p1}, Lcom/sec/android/band/BandActivity;->onCreate(Landroid/os/Bundle;)V

    .line 298
    sget-object v0, Lcom/samsung/groupcast/application/start/StartActivity;->TAG:Ljava/lang/String;

    const-string v1, "GroupPlay_platfrom :[onCreate] fix ver. 2.7(2014/02/07)"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 299
    sget-object v0, Lcom/samsung/groupcast/application/start/StartActivity;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[onCreate] version:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/samsung/groupcast/application/App;->getVersionName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/groupcast/application/Logger;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 307
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "EXTRA_STARTED_BY_NOTIFICATION"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 310
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->finish()V

    .line 475
    :cond_0
    :goto_0
    return-void

    .line 314
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 315
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "IsStartFromExternal"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mIsStartFromExternal:Z

    .line 320
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "EXIT"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 321
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->onEndGroupPlay()V

    goto :goto_0

    .line 325
    :cond_3
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "APPLICATION_INTERFACE_IS_START_FROM_GPSDK"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_4

    .line 326
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ActionBar;->hide()V

    .line 328
    :cond_4
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 329
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ActionBar;->show()V

    .line 331
    :cond_5
    const-string v0, "GroupPlayBand"

    const-string v1, "StartActivity onCreate"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 332
    const-string v0, "VerificationLog"

    const-string v1, "onCreate"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 336
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->BandCacheRelease()V

    .line 337
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->getBandInterface()Lcom/sec/android/band/BandInterface;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/band/BandInterface;->register()Z

    .line 339
    instance-of v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;

    if-eqz v0, :cond_0

    .line 342
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040019

    invoke-virtual {v0, v1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->contentView:Landroid/view/View;

    .line 344
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->contentView:Landroid/view/View;

    const v1, 0x7f07003c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mApScanStop:Landroid/widget/ImageView;

    .line 345
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->contentView:Landroid/view/View;

    const v1, 0x7f07003b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mApNumber:Landroid/widget/TextView;

    .line 346
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->contentView:Landroid/view/View;

    const v1, 0x7f07003f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/samsung/groupcast/application/start/ApListView;

    iput-object v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mMobileAPListView:Lcom/samsung/groupcast/application/start/ApListView;

    .line 347
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mMobileAPListView:Lcom/samsung/groupcast/application/start/ApListView;

    invoke-virtual {v0, p0}, Lcom/samsung/groupcast/application/start/ApListView;->setDelegate(Lcom/samsung/groupcast/application/start/ApListView$Delegate;)V

    .line 348
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mWiFiInfoItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 349
    new-instance v0, Lcom/samsung/groupcast/application/start/JoinAPAdapter;

    invoke-direct {v0, p0}, Lcom/samsung/groupcast/application/start/JoinAPAdapter;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mMobileAPAdapter:Lcom/samsung/groupcast/application/start/JoinAPAdapter;

    .line 350
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mMobileAPAdapter:Lcom/samsung/groupcast/application/start/JoinAPAdapter;

    iget-object v1, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mMobileAPListView:Lcom/samsung/groupcast/application/start/ApListView;

    invoke-virtual {v0, v1}, Lcom/samsung/groupcast/application/start/JoinAPAdapter;->setListView(Lcom/samsung/groupcast/application/start/ApListView;)V

    .line 351
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mMobileAPListView:Lcom/samsung/groupcast/application/start/ApListView;

    invoke-virtual {v0, p0}, Lcom/samsung/groupcast/application/start/ApListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 353
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->contentView:Landroid/view/View;

    const v1, 0x7f07003e

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mApLeftArrow:Landroid/widget/ImageView;

    .line 354
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->contentView:Landroid/view/View;

    const v1, 0x7f07003d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mApRightArrow:Landroid/widget/ImageView;

    .line 356
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mWifiUtils:Lcom/samsung/groupcast/net/wifi/WifiUtils;

    invoke-virtual {v0}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getWifiManager()Landroid/net/wifi/WifiManager;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mWifiManager:Landroid/net/wifi/WifiManager;

    .line 357
    invoke-direct {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->initSetupLayout()V

    .line 358
    invoke-direct {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->initStartTextView()V

    .line 365
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->contentView:Landroid/view/View;

    const v1, 0x7f070041

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mStartBtn:Landroid/widget/Button;

    .line 368
    invoke-direct {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->getNicknameState()V

    .line 369
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->contentView:Landroid/view/View;

    const v1, 0x7f070042

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mPasswordCheckBox:Landroid/widget/CheckBox;

    .line 370
    invoke-direct {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->getCheckPasswordState()V

    .line 371
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mPasswordCheckBox:Landroid/widget/CheckBox;

    new-instance v1, Lcom/samsung/groupcast/application/start/StartActivity$1;

    invoke-direct {v1, p0}, Lcom/samsung/groupcast/application/start/StartActivity$1;-><init>(Lcom/samsung/groupcast/application/start/StartActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 390
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mStartBtn:Landroid/widget/Button;

    new-instance v1, Lcom/samsung/groupcast/application/start/StartActivity$2;

    invoke-direct {v1, p0}, Lcom/samsung/groupcast/application/start/StartActivity$2;-><init>(Lcom/samsung/groupcast/application/start/StartActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 422
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->contentView:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/samsung/groupcast/application/start/StartActivity;->setContentView(Landroid/view/View;)V

    .line 424
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->getBandInterface()Lcom/sec/android/band/BandInterface;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mBandEventHandler:Lcom/sec/android/band/BandInterface$BandEventHandler;

    invoke-virtual {v0, v1}, Lcom/sec/android/band/BandInterface;->setBandCallback(Lcom/sec/android/band/BandInterface$BandEventHandler;)V

    .line 427
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mGp:Lcom/samsung/groupcast/GroupPlayManager;

    iget-object v1, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mExternalEventHandler:Lcom/samsung/groupcast/GroupPlayManager$ExternalEventHandler;

    invoke-virtual {v0, v1}, Lcom/samsung/groupcast/GroupPlayManager;->extRegisterListener(Lcom/samsung/groupcast/GroupPlayManager$ExternalEventHandler;)V

    .line 429
    iput-object v4, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mServiceType:Ljava/lang/String;

    .line 430
    iput-object v4, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mMenuType:Ljava/lang/String;

    .line 432
    iput-object p1, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mSavedInstanceState:Landroid/os/Bundle;

    .line 433
    invoke-static {}, Lcom/samsung/groupcast/application/Environment;->isRunningOnChinaProduct()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 434
    invoke-static {}, Lcom/samsung/groupcast/application/Environment;->isWifiConnected()Z

    move-result v0

    if-eqz v0, :cond_6

    const/4 v0, 0x7

    invoke-static {p0, v0}, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->showIfNeeded(Landroid/app/Activity;I)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 437
    iget v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mHowManyDisclaimerShowing:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mHowManyDisclaimerShowing:I

    .line 450
    :goto_1
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mApScanStop:Landroid/widget/ImageView;

    new-instance v1, Lcom/samsung/groupcast/application/start/StartActivity$3;

    invoke-direct {v1, p0}, Lcom/samsung/groupcast/application/start/StartActivity$3;-><init>(Lcom/samsung/groupcast/application/start/StartActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 463
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->contentView:Landroid/view/View;

    const v1, 0x7f070040

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mScanningLayout:Landroid/widget/RelativeLayout;

    .line 466
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "APPLICATION_INTERFACE_IS_START_FROM_GPSDK"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mIsStartFromGPSDK:Z

    .line 468
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "APPLICATION_INTERFACE_NEXT_ACTION"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mGPSDKNextAction:Ljava/lang/String;

    .line 469
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "APPLICATION_INTERFACE_TYPE"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mGPSDKType:I

    .line 474
    invoke-direct {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->registerWifiDialogCancelReceiver()V

    goto/16 :goto_0

    .line 438
    :cond_6
    invoke-static {}, Lcom/samsung/groupcast/application/Environment;->isMobileConnected()Z

    move-result v0

    if-eqz v0, :cond_7

    const/16 v0, 0x8

    invoke-static {p0, v0}, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->showIfNeeded(Landroid/app/Activity;I)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 441
    iget v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mHowManyDisclaimerShowing:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mHowManyDisclaimerShowing:I

    goto :goto_1

    .line 443
    :cond_7
    invoke-direct {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->doUpdate()V

    goto :goto_1

    .line 446
    :cond_8
    invoke-direct {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->doUpdate()V

    goto :goto_1
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 936
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->contentView:Landroid/view/View;

    invoke-static {v0}, Lcom/samsung/groupcast/misc/utility/RecycleUtils;->recursiveRecycle(Landroid/view/View;)V

    .line 938
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 939
    sget-object v0, Lcom/samsung/groupcast/core/messaging/MessagingClient;->DefaultInterface:Lcom/samsung/groupcast/core/messaging/MessagingClient$MAGNET_CONNECTION_TYPE;

    invoke-virtual {v0}, Lcom/samsung/groupcast/core/messaging/MessagingClient$MAGNET_CONNECTION_TYPE;->value()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 957
    :cond_0
    :goto_0
    iput-object v1, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mProgressDialogFragment:Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;

    .line 958
    iput-object v1, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mImageView:Landroid/widget/ImageView;

    .line 959
    iput-object v1, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mHandler:Landroid/os/Handler;

    .line 960
    iput-object v1, p0, Lcom/samsung/groupcast/application/start/StartActivity;->currentUri:Landroid/net/Uri;

    .line 964
    :cond_1
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 966
    invoke-super {p0}, Lcom/sec/android/band/BandActivity;->onDestroy()V

    .line 967
    return-void

    .line 941
    :pswitch_0
    const-string v0, "null RVF manager!"

    invoke-static {v0}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 942
    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiAPUtils;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->setOnRvfHotspotChangeListener(Lcom/samsung/groupcast/net/wifi/WifiAPUtils$OnRvfHotspotChangeListener;)V

    .line 943
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mWifiUtils:Lcom/samsung/groupcast/net/wifi/WifiUtils;

    if-eqz v0, :cond_0

    .line 944
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mWifiUtils:Lcom/samsung/groupcast/net/wifi/WifiUtils;

    invoke-virtual {v0}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->stopScan()V

    goto :goto_0

    .line 949
    :pswitch_1
    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiUtils;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->setWifiMotherFatherOxygenDelegate(Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiMotherFatherOxygenDelegate;)V

    goto :goto_0

    .line 953
    :pswitch_2
    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->stopScan()V

    goto :goto_0

    .line 939
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public onDisclaimerDialogDismissed(Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;Z)V
    .locals 3
    .param p1, "fragment"    # Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;
    .param p2, "cancelled"    # Z

    .prologue
    .line 2754
    sget-object v0, Lcom/samsung/groupcast/application/start/StartActivity;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[onDisclaimerDialogDismissed] cancelled:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/groupcast/application/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2755
    iget v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mHowManyDisclaimerShowing:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mHowManyDisclaimerShowing:I

    .line 2756
    if-eqz p2, :cond_1

    .line 2757
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->finish()V

    .line 2767
    :cond_0
    :goto_0
    return-void

    .line 2759
    :cond_1
    sget-boolean v0, Lcom/samsung/groupcast/application/start/StartActivity;->mNfcConnectOngoing:Z

    if-eqz v0, :cond_0

    .line 2760
    iget v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mHowManyDisclaimerShowing:I

    const/4 v1, 0x1

    if-ge v0, v1, :cond_0

    .line 2761
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mBandTargetSSID:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 2762
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mBandTargetSSID:Ljava/lang/String;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mBandPassphrase:Ljava/lang/String;

    invoke-virtual {p0, v0, v1, v2}, Lcom/samsung/groupcast/application/start/StartActivity;->startJoinProcess(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onEndGroupPlay()V
    .locals 2

    .prologue
    .line 248
    const-string v0, "onEndGroupPlay"

    invoke-static {v0}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 249
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->BandInterfaceRelease()V

    .line 250
    invoke-static {}, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->ClearLastSession()V

    .line 252
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 253
    const-string v0, "clears oldIntent"

    invoke-static {v0}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 254
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const/4 v0, 0x0

    check-cast v0, Landroid/os/Bundle;

    invoke-virtual {v1, v0}, Landroid/content/Intent;->replaceExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 258
    :cond_0
    sget-object v0, Lcom/samsung/groupcast/core/messaging/MessagingClient;->DefaultInterface:Lcom/samsung/groupcast/core/messaging/MessagingClient$MAGNET_CONNECTION_TYPE;

    invoke-virtual {v0}, Lcom/samsung/groupcast/core/messaging/MessagingClient$MAGNET_CONNECTION_TYPE;->value()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 270
    :goto_0
    invoke-static {}, Lcom/samsung/groupcast/core/messaging/MessagingClient;->releaseChordDir()V

    .line 271
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->finish()V

    .line 272
    const-string v0, "set gc!"

    invoke-static {v0}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 273
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 274
    return-void

    .line 260
    :pswitch_0
    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->removeSession()V

    .line 261
    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->cancelConnect()V

    .line 262
    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->stopScan()V

    goto :goto_0

    .line 266
    :pswitch_1
    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiUtils;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->turnOxygenOnOff(Z)V

    goto :goto_0

    .line 258
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onFlightModeDialogDismissed(Lcom/samsung/groupcast/application/fragment/FlightModeDialogFragment;ZI)V
    .locals 2
    .param p1, "fragment"    # Lcom/samsung/groupcast/application/fragment/FlightModeDialogFragment;
    .param p2, "isAgree"    # Z
    .param p3, "type"    # I

    .prologue
    .line 2647
    if-eqz p2, :cond_0

    .line 2648
    packed-switch p3, :pswitch_data_0

    .line 2667
    :cond_0
    :goto_0
    return-void

    .line 2650
    :pswitch_0
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mPasswordCheckBox:Landroid/widget/CheckBox;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mPasswordCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2651
    invoke-direct {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->showPasswordInputDialog()V

    goto :goto_0

    .line 2653
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mPin:Ljava/lang/String;

    .line 2654
    const v0, 0x7f080056

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2656
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->createGroup()V

    goto :goto_0

    .line 2648
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 2463
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v1, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mMobileAPAdapter:Lcom/samsung/groupcast/application/start/JoinAPAdapter;

    invoke-virtual {v1, p3}, Lcom/samsung/groupcast/application/start/JoinAPAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/groupcast/application/start/JoinAPAdapter$WiFiInfoItem;

    .line 2464
    .local v0, "itm":Lcom/samsung/groupcast/application/start/JoinAPAdapter$WiFiInfoItem;
    if-eqz v0, :cond_0

    .line 2465
    const-string v1, "--- join --"

    invoke-static {v1}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 2466
    invoke-static {}, Lcom/samsung/groupcast/application/StatLog;->getInstance()Lcom/samsung/groupcast/application/StatLog;

    move-result-object v1

    sget-object v2, Lcom/samsung/groupcast/application/StatLog$Menu;->JOIN:Lcom/samsung/groupcast/application/StatLog$Menu;

    invoke-virtual {v1, v2}, Lcom/samsung/groupcast/application/StatLog;->countMenu(Lcom/samsung/groupcast/application/StatLog$Menu;)V

    .line 2469
    invoke-virtual {v0}, Lcom/samsung/groupcast/application/start/JoinAPAdapter$WiFiInfoItem;->isDummy()Z

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 2488
    :cond_0
    :goto_0
    return-void

    .line 2473
    :cond_1
    const/4 v1, 0x0

    invoke-static {v1}, Lcom/samsung/groupcast/application/App;->setPeerScreenId(Ljava/lang/String;)V

    .line 2474
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "TAG_CONNECT_DIALOG_FRAGMENT"

    invoke-virtual {v1, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    if-nez v1, :cond_0

    .line 2477
    const/4 v1, 0x2

    iput v1, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mModeStartOrJoin:I

    .line 2479
    invoke-direct {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->checkAvailablityToConnect()Z

    move-result v1

    if-nez v1, :cond_2

    .line 2480
    const/4 v1, 0x0

    iput v1, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mModeStartOrJoin:I

    goto :goto_0

    .line 2484
    :cond_2
    invoke-virtual {v0}, Lcom/samsung/groupcast/application/start/JoinAPAdapter$WiFiInfoItem;->getSSID()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mSSID:Ljava/lang/String;

    .line 2485
    invoke-virtual {v0}, Lcom/samsung/groupcast/application/start/JoinAPAdapter$WiFiInfoItem;->getBSSID()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mBssID:Ljava/lang/String;

    .line 2486
    iget-object v1, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mSSID:Ljava/lang/String;

    iget-object v2, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mBssID:Ljava/lang/String;

    invoke-direct {p0, v1, v2}, Lcom/samsung/groupcast/application/start/StartActivity;->onWiFiSelected(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onJoinSessionFinished(Lcom/samsung/groupcast/legacy/gp2/session/model/Session;)V
    .locals 3
    .param p1, "session"    # Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    .prologue
    .line 2493
    if-nez p1, :cond_1

    .line 2494
    const v1, 0x7f08003a

    const/4 v2, 0x1

    invoke-static {p0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 2495
    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->isWifiEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getWifiSSID()Ljava/lang/String;

    move-result-object v1

    const-string v2, "GroupPlay-"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2498
    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->disconnectFromCurrentAP()V

    .line 2549
    :cond_0
    :goto_0
    return-void

    .line 2504
    :cond_1
    sget-object v1, Lcom/samsung/groupcast/core/messaging/MessagingClient;->DefaultInterface:Lcom/samsung/groupcast/core/messaging/MessagingClient$MAGNET_CONNECTION_TYPE;

    invoke-virtual {v1}, Lcom/samsung/groupcast/core/messaging/MessagingClient$MAGNET_CONNECTION_TYPE;->value()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 2510
    :goto_1
    const/4 v0, 0x0

    .line 2514
    .local v0, "intent":Landroid/content/Intent;
    new-instance v0, Landroid/content/Intent;

    .end local v0    # "intent":Landroid/content/Intent;
    const-class v1, Lcom/samsung/groupcast/application/main/MainActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2517
    .restart local v0    # "intent":Landroid/content/Intent;
    const-string v1, "GROUP_NAME"

    invoke-virtual {p1}, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2518
    const-string v1, "SessionExtra"

    invoke-static {p1}, Lcom/samsung/groupcast/application/ObjectStore;->retain(Ljava/lang/Object;)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2519
    const-string v1, "ObjectStoreHash"

    invoke-static {}, Lcom/samsung/groupcast/application/ObjectStore;->getHash()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2521
    iget-object v1, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mServiceType:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 2523
    const-string v1, "TAG_SERVICE_TYPE"

    iget-object v2, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mServiceType:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2524
    const-string v1, "GroupPlayBand"

    const-string v2, "StartActivity set ServiceType"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2526
    :cond_2
    iget-object v1, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mMenuType:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 2528
    const-string v1, "TAG_MENU_TYPE"

    iget-object v2, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mMenuType:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2529
    const-string v1, "GroupPlayBand"

    const-string v2, "StartActivity set MenuType"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2532
    :cond_3
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    if-eqz v1, :cond_5

    .line 2533
    iget-boolean v1, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mIsStartFromExternal:Z

    if-eqz v1, :cond_5

    .line 2534
    iget-boolean v1, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mIsStartFromGPSDK:Z

    if-eqz v1, :cond_4

    .line 2535
    invoke-direct {p0, v0}, Lcom/samsung/groupcast/application/start/StartActivity;->addExtraInfoFromGPSDK(Landroid/content/Intent;)V

    .line 2537
    :cond_4
    invoke-direct {p0, v0}, Lcom/samsung/groupcast/application/start/StartActivity;->addExtraInfoFromExternalApp(Landroid/content/Intent;)V

    .line 2542
    :cond_5
    invoke-virtual {p0, v0}, Lcom/samsung/groupcast/application/start/StartActivity;->startActivity(Landroid/content/Intent;)V

    .line 2543
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->finish()V

    .line 2545
    const/4 v0, 0x0

    .line 2546
    const/4 v1, 0x0

    sput-boolean v1, Lcom/samsung/groupcast/application/start/StartActivity;->mNfcConnectOngoing:Z

    .line 2547
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mNfcMsg:Lcom/samsung/groupcast/core/messaging/NfcMsg;

    goto :goto_0

    .line 2506
    .end local v0    # "intent":Landroid/content/Intent;
    :pswitch_0
    invoke-direct {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->clearWifiP2pListners()V

    goto :goto_1

    .line 2504
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onNetworkStatusChanged(Landroid/net/NetworkInfo;)V
    .locals 1
    .param p1, "info"    # Landroid/net/NetworkInfo;

    .prologue
    .line 2626
    iget-boolean v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mSavedInstanceStateCalled:Z

    if-eqz v0, :cond_1

    .line 2641
    :cond_0
    :goto_0
    return-void

    .line 2629
    :cond_1
    invoke-virtual {p1}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2630
    iget v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mModeStartOrJoin:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 2635
    :pswitch_0
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->startNextActivity()V

    goto :goto_0

    .line 2632
    :pswitch_1
    invoke-direct {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->openJoinDialog()V

    goto :goto_0

    .line 2630
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 971
    const-string v0, "---"

    invoke-static {v0}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 972
    const-string v0, "GroupPlayBand"

    const-string v1, "StartActivity onNewIntent"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 973
    const-string v0, "EXIT"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 974
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->onEndGroupPlay()V

    .line 978
    :goto_0
    return-void

    .line 977
    :cond_0
    invoke-direct {p0, p1}, Lcom/samsung/groupcast/application/start/StartActivity;->processNfcIntent(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public onNicknameSubmitted(Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;Ljava/lang/String;Z)V
    .locals 4
    .param p1, "dialogfragment"    # Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;
    .param p2, "mNickname"    # Ljava/lang/String;
    .param p3, "cancel"    # Z

    .prologue
    .line 2343
    if-eqz p2, :cond_0

    const-string v1, ""

    if-eq p2, v1, :cond_0

    .line 2344
    iget-object v1, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mNickName:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]\'s group"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2345
    invoke-static {p2}, Lcom/samsung/groupcast/application/Preferences;->setNicknamePreference(Ljava/lang/String;)V

    .line 2347
    :cond_0
    if-eqz p1, :cond_1

    .line 2348
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "TAG_NICKNAME_DIALOG"

    invoke-virtual {v1, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 2351
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-string v3, "TAG_NICKNAME_DIALOG"

    invoke-virtual {v2, v3}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2359
    :cond_2
    :goto_0
    return-void

    .line 2354
    :catch_0
    move-exception v0

    .line 2355
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public onP2PChanged(IZI)V
    .locals 2
    .param p1, "event"    # I
    .param p2, "result"    # Z
    .param p3, "code"    # I

    .prologue
    .line 1864
    sparse-switch p1, :sswitch_data_0

    .line 1885
    :goto_0
    return-void

    .line 1866
    :sswitch_0
    if-eqz p2, :cond_0

    .line 1867
    const-string v0, "P2P created"

    invoke-static {v0}, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->showToast(Ljava/lang/String;)V

    goto :goto_0

    .line 1869
    :cond_0
    const-string v0, "Please retry create"

    invoke-static {v0}, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->showToast(Ljava/lang/String;)V

    goto :goto_0

    .line 1873
    :sswitch_1
    if-eqz p2, :cond_1

    .line 1874
    const-string v0, "p2p join connection ok"

    invoke-static {v0}, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->showToast(Ljava/lang/String;)V

    goto :goto_0

    .line 1876
    :cond_1
    const-string v0, "Please retry join"

    invoke-static {v0}, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->showToast(Ljava/lang/String;)V

    goto :goto_0

    .line 1881
    :sswitch_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "scan result:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->showToast(Ljava/lang/String;)V

    goto :goto_0

    .line 1864
    nop

    :sswitch_data_0
    .sparse-switch
        0xa -> :sswitch_0
        0x14 -> :sswitch_1
        0x1e -> :sswitch_2
    .end sparse-switch
.end method

.method public onP2PConnectionInfoAvailable(Landroid/net/wifi/p2p/WifiP2pInfo;)V
    .locals 0
    .param p1, "info"    # Landroid/net/wifi/p2p/WifiP2pInfo;

    .prologue
    .line 1911
    return-void
.end method

.method public onP2PEventReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 1916
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 1918
    .local v0, "action":Ljava/lang/String;
    invoke-static {v0}, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->showToast(Ljava/lang/String;)V

    .line 1919
    sget-object v2, Lcom/samsung/groupcast/application/start/StartActivity;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "receive:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1921
    const-string v2, "android.net.wifi.p2p.CONNECTION_STATE_CHANGE"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1922
    const-string v2, "networkInfo"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/net/NetworkInfo;

    .line 1924
    .local v1, "netInfo":Landroid/net/NetworkInfo;
    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1925
    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->getLastCmd()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 1940
    .end local v1    # "netInfo":Landroid/net/NetworkInfo;
    :cond_0
    :goto_0
    return-void

    .line 1928
    .restart local v1    # "netInfo":Landroid/net/NetworkInfo;
    :pswitch_0
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->onRvfHotspotEnabled()V

    goto :goto_0

    .line 1925
    nop

    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_0
    .end packed-switch
.end method

.method public onP2PScaned(Landroid/net/wifi/p2p/WifiP2pDeviceList;)V
    .locals 0
    .param p1, "peers"    # Landroid/net/wifi/p2p/WifiP2pDeviceList;

    .prologue
    .line 1905
    return-void
.end method

.method protected onPause()V
    .locals 3

    .prologue
    .line 898
    invoke-super {p0}, Lcom/sec/android/band/BandActivity;->onPause()V

    .line 900
    invoke-static {}, Landroid/net/http/HttpResponseCache;->getInstalled()Landroid/net/http/HttpResponseCache;

    move-result-object v0

    .line 901
    .local v0, "cache":Landroid/net/http/HttpResponseCache;
    if-eqz v0, :cond_0

    .line 902
    invoke-virtual {v0}, Landroid/net/http/HttpResponseCache;->flush()V

    .line 905
    :cond_0
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mIsResumed:Z

    .line 907
    sget-object v1, Lcom/samsung/groupcast/core/messaging/MessagingClient;->DefaultInterface:Lcom/samsung/groupcast/core/messaging/MessagingClient$MAGNET_CONNECTION_TYPE;

    invoke-virtual {v1}, Lcom/samsung/groupcast/core/messaging/MessagingClient$MAGNET_CONNECTION_TYPE;->value()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 920
    :goto_0
    iget-boolean v1, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mIsStartFromExternal:Z

    if-eqz v1, :cond_1

    .line 931
    :goto_1
    return-void

    .line 909
    :pswitch_0
    iget-object v1, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mWifiUtils:Lcom/samsung/groupcast/net/wifi/WifiUtils;

    iget-object v2, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mAPHandler:Lcom/samsung/groupcast/application/start/StartActivity$WHandler;

    invoke-virtual {v1, v2}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->unregisterFromScanningResultsAvailable(Landroid/os/Handler;)V

    .line 911
    :pswitch_1
    iget-object v1, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mWifiUtils:Lcom/samsung/groupcast/net/wifi/WifiUtils;

    invoke-virtual {v1}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->stopScan()V

    goto :goto_0

    .line 914
    :pswitch_2
    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->stopScan()V

    .line 915
    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mAPHandler:Lcom/samsung/groupcast/application/start/StartActivity$WHandler;

    invoke-virtual {v1, v2}, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->unregisterFromScanningResultsAvailable(Landroid/os/Handler;)V

    goto :goto_0

    .line 924
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->isFinishing()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 925
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->BandInterfaceRelease()V

    .line 926
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->BandCacheRelease()V

    .line 929
    :cond_2
    const-string v1, "GroupPlayBand"

    const-string v2, "StartActivity onPause"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 907
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public onPinSubmitted(Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;Ljava/lang/String;Z)V
    .locals 2
    .param p1, "dialogFragment"    # Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;
    .param p2, "pin"    # Ljava/lang/String;
    .param p3, "dialogCancelled"    # Z

    .prologue
    .line 2412
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->getType()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 2414
    invoke-direct {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->hidePinCodeInputDialog()V

    .line 2415
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/groupcast/application/start/StartActivity;->joinGroupAfterPasswordSubmitted(Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;Ljava/lang/String;Z)V

    .line 2435
    :cond_0
    :goto_0
    return-void

    .line 2419
    :cond_1
    if-eqz p3, :cond_3

    .line 2420
    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiAPUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->isRvfHotspotEnabled()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2421
    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiAPUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->releaseRvfHotspot()V

    goto :goto_0

    .line 2422
    :cond_2
    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->isWifiEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getWifiSSID()Ljava/lang/String;

    move-result-object v0

    const-string v1, "GroupPlay-"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2425
    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->disconnectFromCurrentAP()V

    goto :goto_0

    .line 2430
    :cond_3
    invoke-direct {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->hidePinCodeInputDialog()V

    .line 2432
    iput-object p2, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mPin:Ljava/lang/String;

    .line 2433
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->createGroup()V

    goto :goto_0
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 1002
    const-string v0, "IS_PIN_OK"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mPinOK:Z

    .line 1004
    const-string v0, "AUTOCONNECTION_IN_PROGRESS"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1005
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const-string v1, "AUTOCONNECT_PROGRESS_DIALOG_FRAGMENT"

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;

    iput-object v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mProgressDialogFragment:Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;

    .line 1010
    :cond_0
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mProgressDialogFragment:Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mProgressDialogFragment:Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;

    invoke-virtual {v0}, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiAPUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->isRvfHotspotEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1012
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->dismissProgressAndStartMainActivity()V

    .line 1015
    :cond_1
    const-string v0, "TAG_NFC_MSG"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1016
    const-string v0, "TAG_NFC_MSG"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/samsung/groupcast/core/messaging/NfcMsg;

    iput-object v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mNfcMsg:Lcom/samsung/groupcast/core/messaging/NfcMsg;

    .line 1018
    :cond_2
    const-string v0, "LAST_CHECK_TIME"

    const-wide/16 v1, 0x0

    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mLastCheckTime:J

    .line 1020
    invoke-super {p0, p1}, Lcom/sec/android/band/BandActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 1021
    return-void
.end method

.method protected onResume()V
    .locals 11

    .prologue
    const/4 v10, 0x2

    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 760
    invoke-super {p0}, Lcom/sec/android/band/BandActivity;->onResume()V

    .line 762
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->isFinishing()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 763
    sget-object v4, Lcom/samsung/groupcast/application/start/StartActivity;->TAG:Ljava/lang/String;

    const-string v5, "onStart():finish()"

    invoke-static {v4, v5}, Lcom/samsung/groupcast/application/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 887
    :cond_0
    :goto_0
    return-void

    .line 767
    :cond_1
    invoke-direct {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->getNicknameState()V

    .line 768
    invoke-direct {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->getCheckPasswordState()V

    .line 771
    iput-boolean v8, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mIsResumed:Z

    .line 772
    iput-boolean v9, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mSavedInstanceStateCalled:Z

    .line 773
    iget-object v4, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mPasswordCheckBox:Landroid/widget/CheckBox;

    if-eqz v4, :cond_2

    .line 774
    iget-object v4, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mPasswordCheckBox:Landroid/widget/CheckBox;

    const v5, 0x7f080040

    invoke-virtual {v4, v5}, Landroid/widget/CheckBox;->setText(I)V

    .line 776
    :cond_2
    const-class v4, Lcom/samsung/groupcast/application/start/StartActivity;

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/groupcast/application/App;->setTopActivity(Ljava/lang/String;)V

    .line 778
    iget-boolean v4, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mIsStartFromExternal:Z

    if-eqz v4, :cond_3

    invoke-static {}, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->HasSession()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 779
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->startNextActivity()V

    .line 782
    :cond_3
    iget-boolean v4, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mIsStartFromExternal:Z

    if-eqz v4, :cond_7

    invoke-static {}, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->HasSession()Z

    move-result v4

    if-nez v4, :cond_7

    .line 783
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 784
    .local v1, "intent":Landroid/content/Intent;
    invoke-static {v1}, Lcom/samsung/groupcast/application/IntentTools;->isSendSconnectIntent(Landroid/content/Intent;)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 785
    const-string v4, "BSSID"

    invoke-virtual {v1, v4}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 787
    const/4 v4, 0x0

    invoke-static {v4}, Lcom/samsung/groupcast/application/App;->setPeerScreenId(Ljava/lang/String;)V

    .line 788
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v4

    const-string v5, "TAG_CONNECT_DIALOG_FRAGMENT"

    invoke-virtual {v4, v5}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v4

    if-nez v4, :cond_0

    .line 791
    iput v10, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mModeStartOrJoin:I

    .line 793
    invoke-direct {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->checkAvailablityToConnect()Z

    move-result v4

    if-nez v4, :cond_4

    .line 794
    iput v9, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mModeStartOrJoin:I

    goto :goto_0

    .line 798
    :cond_4
    const-string v4, "BSSID"

    invoke-virtual {v1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mBssID:Ljava/lang/String;

    .line 799
    const-string v4, "GroupPlay-"

    iput-object v4, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mSSID:Ljava/lang/String;

    .line 802
    iget-object v4, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mWifiUtils:Lcom/samsung/groupcast/net/wifi/WifiUtils;

    invoke-virtual {v4}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getWifiManager()Landroid/net/wifi/WifiManager;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/wifi/WifiManager;->getScanResults()Ljava/util/List;

    move-result-object v2

    .line 803
    .local v2, "list":Ljava/util/List;, "Ljava/util/List<Landroid/net/wifi/ScanResult;>;"
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_5
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_6

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/net/wifi/ScanResult;

    .line 804
    .local v3, "s":Landroid/net/wifi/ScanResult;
    sget-object v4, Lcom/samsung/groupcast/application/start/StartActivity;->TAG:Ljava/lang/String;

    const-string v5, "SCONNECT:%s - %s(%s)"

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mBssID:Ljava/lang/String;

    aput-object v7, v6, v9

    iget-object v7, v3, Landroid/net/wifi/ScanResult;->BSSID:Ljava/lang/String;

    aput-object v7, v6, v8

    iget-object v7, v3, Landroid/net/wifi/ScanResult;->SSID:Ljava/lang/String;

    aput-object v7, v6, v10

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 806
    iget-object v4, v3, Landroid/net/wifi/ScanResult;->BSSID:Ljava/lang/String;

    iget-object v5, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mBssID:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 807
    iget-object v4, v3, Landroid/net/wifi/ScanResult;->SSID:Ljava/lang/String;

    iput-object v4, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mSSID:Ljava/lang/String;

    .line 811
    .end local v3    # "s":Landroid/net/wifi/ScanResult;
    :cond_6
    sget-object v4, Lcom/samsung/groupcast/application/start/StartActivity;->TAG:Ljava/lang/String;

    const-string v5, "SCONNECT:%s,%s"

    new-array v6, v10, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mSSID:Ljava/lang/String;

    aput-object v7, v6, v9

    iget-object v7, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mBssID:Ljava/lang/String;

    aput-object v7, v6, v8

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 813
    const-string v4, "GroupPlay-"

    iget-object v5, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mSSID:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_7

    .line 814
    iput-boolean v8, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mActive:Z

    .line 815
    iget-object v4, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mSSID:Ljava/lang/String;

    iget-object v5, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mBssID:Ljava/lang/String;

    invoke-direct {p0, v4, v5}, Lcom/samsung/groupcast/application/start/StartActivity;->onWiFiSelected(Ljava/lang/String;Ljava/lang/String;)V

    .line 834
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "intent":Landroid/content/Intent;
    .end local v2    # "list":Ljava/util/List;, "Ljava/util/List<Landroid/net/wifi/ScanResult;>;"
    :cond_7
    :goto_1
    sget-object v4, Lcom/samsung/groupcast/core/messaging/MessagingClient;->DefaultInterface:Lcom/samsung/groupcast/core/messaging/MessagingClient$MAGNET_CONNECTION_TYPE;

    if-eqz v4, :cond_0

    .line 838
    sget-object v4, Lcom/samsung/groupcast/core/messaging/MessagingClient;->DefaultInterface:Lcom/samsung/groupcast/core/messaging/MessagingClient$MAGNET_CONNECTION_TYPE;

    invoke-virtual {v4}, Lcom/samsung/groupcast/core/messaging/MessagingClient$MAGNET_CONNECTION_TYPE;->value()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    .line 864
    :goto_2
    iget-object v4, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mWifiManager:Landroid/net/wifi/WifiManager;

    if-eqz v4, :cond_8

    .line 866
    invoke-direct {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->startSearch()V

    .line 868
    sget-object v4, Lcom/samsung/groupcast/core/messaging/MessagingClient;->DefaultInterface:Lcom/samsung/groupcast/core/messaging/MessagingClient$MAGNET_CONNECTION_TYPE;

    invoke-virtual {v4}, Lcom/samsung/groupcast/core/messaging/MessagingClient$MAGNET_CONNECTION_TYPE;->value()I

    move-result v4

    packed-switch v4, :pswitch_data_1

    .line 880
    :cond_8
    :goto_3
    iput-boolean v8, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mActive:Z

    goto/16 :goto_0

    .line 821
    .restart local v1    # "intent":Landroid/content/Intent;
    :cond_9
    iget-object v4, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mStartBtn:Landroid/widget/Button;

    invoke-virtual {v4}, Landroid/widget/Button;->callOnClick()Z

    goto :goto_1

    .line 824
    :cond_a
    iget-boolean v4, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mIsStartFromGPSDK:Z

    if-nez v4, :cond_7

    .line 827
    iget-object v4, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mStartBtn:Landroid/widget/Button;

    invoke-virtual {v4}, Landroid/widget/Button;->callOnClick()Z

    goto :goto_1

    .line 840
    .end local v1    # "intent":Landroid/content/Intent;
    :pswitch_0
    invoke-direct {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->setWifiP2pListners()V

    goto :goto_2

    .line 843
    :pswitch_1
    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiAPUtils;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->isRvfHotspotEnabled()Z

    move-result v4

    if-eqz v4, :cond_b

    .line 844
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->dismissProgressAndStartMainActivity()V

    goto/16 :goto_0

    .line 847
    :cond_b
    iget-object v4, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mWifiUtils:Lcom/samsung/groupcast/net/wifi/WifiUtils;

    invoke-virtual {v4, v8, v9}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->startScan(ZZ)V

    goto :goto_2

    .line 870
    :pswitch_2
    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mAPHandler:Lcom/samsung/groupcast/application/start/StartActivity$WHandler;

    invoke-virtual {v4, v5}, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->registerForScanningResultsAvailable(Landroid/os/Handler;)V

    .line 871
    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;

    move-result-object v4

    invoke-virtual {v4, v9, v8}, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->startScan(ZZ)V

    goto :goto_3

    .line 874
    :pswitch_3
    iget-object v4, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mWifiUtils:Lcom/samsung/groupcast/net/wifi/WifiUtils;

    iget-object v5, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mAPHandler:Lcom/samsung/groupcast/application/start/StartActivity$WHandler;

    invoke-virtual {v4, v5}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->registerForScanningResultsAvailable(Landroid/os/Handler;)V

    .line 875
    iget-object v4, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mWifiUtils:Lcom/samsung/groupcast/net/wifi/WifiUtils;

    invoke-virtual {v4, v9, v8}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->startScan(ZZ)V

    goto :goto_3

    .line 838
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch

    .line 868
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method public onRvfHotspotDisabled()V
    .locals 2

    .prologue
    .line 1462
    sget-object v0, Lcom/samsung/groupcast/application/start/StartActivity;->TAG:Ljava/lang/String;

    const-string v1, "onRvfHotspotDisabled "

    invoke-static {v0, v1}, Lcom/samsung/groupcast/application/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1464
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mStartBtn:Landroid/widget/Button;

    if-eqz v0, :cond_0

    .line 1465
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mStartBtn:Landroid/widget/Button;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 1471
    :cond_0
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mProgressDialogFragment:Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;

    if-eqz v0, :cond_1

    .line 1472
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mProgressDialogFragment:Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;

    invoke-virtual {v0}, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1473
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mProgressDialogFragment:Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;

    invoke-virtual {v0}, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;->forceDismiss()V

    .line 1477
    :cond_1
    return-void
.end method

.method public onRvfHotspotDisabling()V
    .locals 2

    .prologue
    .line 1456
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mStartBtn:Landroid/widget/Button;

    if-eqz v0, :cond_0

    .line 1457
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mStartBtn:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 1458
    :cond_0
    return-void
.end method

.method public onRvfHotspotEnabled()V
    .locals 2

    .prologue
    .line 1442
    sget-object v0, Lcom/samsung/groupcast/application/start/StartActivity;->TAG:Ljava/lang/String;

    const-string v1, "onRvfHotspotEnabled"

    invoke-static {v0, v1}, Lcom/samsung/groupcast/application/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1444
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mProgressDialogFragment:Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;

    if-eqz v0, :cond_0

    .line 1445
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mProgressDialogFragment:Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;

    invoke-virtual {v0}, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;->stopTimer()V

    .line 1448
    :cond_0
    iget-boolean v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mSavedInstanceStateCalled:Z

    if-eqz v0, :cond_1

    .line 1452
    :goto_0
    return-void

    .line 1451
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->dismissProgressAndStartMainActivity()V

    goto :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 982
    sget-object v0, Lcom/samsung/groupcast/application/start/StartActivity;->TAG:Ljava/lang/String;

    const-string v1, "onSaveInstanceState() "

    invoke-static {v0, v1}, Lcom/samsung/groupcast/application/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 983
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mProgressDialogFragment:Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;

    if-eqz v0, :cond_0

    .line 984
    const-string v0, "AUTOCONNECTION_IN_PROGRESS"

    iget-object v1, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mProgressDialogFragment:Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;

    invoke-virtual {v1}, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;->isVisible()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 986
    :cond_0
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mNfcMsg:Lcom/samsung/groupcast/core/messaging/NfcMsg;

    if-eqz v0, :cond_1

    .line 987
    const-string v0, "TAG_NFC_MSG"

    iget-object v1, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mNfcMsg:Lcom/samsung/groupcast/core/messaging/NfcMsg;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 990
    :cond_1
    const-string v0, "IS_PIN_OK"

    iget-boolean v1, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mPinOK:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 992
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mSavedInstanceStateCalled:Z

    .line 994
    const-string v0, "LAST_CHECK_TIME"

    iget-wide v1, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mLastCheckTime:J

    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 996
    invoke-super {p0, p1}, Lcom/sec/android/band/BandActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 997
    return-void
.end method

.method protected onStart()V
    .locals 2

    .prologue
    .line 683
    sget-object v0, Lcom/samsung/groupcast/application/start/StartActivity;->TAG:Ljava/lang/String;

    const-string v1, "onStart() "

    invoke-static {v0, v1}, Lcom/samsung/groupcast/application/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 684
    invoke-super {p0}, Lcom/sec/android/band/BandActivity;->onStart()V

    .line 686
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 687
    sget-object v0, Lcom/samsung/groupcast/application/start/StartActivity;->TAG:Ljava/lang/String;

    const-string v1, "onStart():finish()"

    invoke-static {v0, v1}, Lcom/samsung/groupcast/application/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 714
    :goto_0
    return-void

    .line 692
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->invalidateOptionsMenu()V

    .line 695
    sget-object v0, Lcom/samsung/groupcast/core/messaging/MessagingClient;->DefaultInterface:Lcom/samsung/groupcast/core/messaging/MessagingClient$MAGNET_CONNECTION_TYPE;

    invoke-virtual {v0}, Lcom/samsung/groupcast/core/messaging/MessagingClient$MAGNET_CONNECTION_TYPE;->value()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 713
    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->invalidateOptionsMenu()V

    goto :goto_0

    .line 697
    :pswitch_1
    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiUtils;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->setWifiMotherFatherOxygenDelegate(Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiMotherFatherOxygenDelegate;)V

    goto :goto_1

    .line 703
    :pswitch_2
    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiAPUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->isRvfHotspotEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 704
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->dismissProgressAndStartMainActivity()V

    goto :goto_0

    .line 708
    :cond_1
    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiAPUtils;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->setOnRvfHotspotChangeListener(Lcom/samsung/groupcast/net/wifi/WifiAPUtils$OnRvfHotspotChangeListener;)V

    goto :goto_1

    .line 695
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onWifiConnectionFinished(ZLjava/lang/String;Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectToFail;)V
    .locals 5
    .param p1, "connected"    # Z
    .param p2, "SSID"    # Ljava/lang/String;
    .param p3, "fail_reason"    # Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectToFail;

    .prologue
    const v4, 0x7f08003a

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1302
    iput-boolean v2, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mIsConnectOngoing:Z

    .line 1304
    iget-boolean v1, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mSavedInstanceStateCalled:Z

    if-eqz v1, :cond_0

    .line 1305
    const-string v1, "Ignoring due to savedInstanceState"

    invoke-static {v1}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 1352
    :goto_0
    return-void

    .line 1308
    :cond_0
    sget-boolean v1, Lcom/samsung/groupcast/application/start/StartActivity;->mNfcConnectOngoing:Z

    if-eqz v1, :cond_3

    .line 1309
    if-nez p1, :cond_1

    .line 1310
    sput-boolean v2, Lcom/samsung/groupcast/application/start/StartActivity;->mNfcConnectOngoing:Z

    .line 1311
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mNfcMsg:Lcom/samsung/groupcast/core/messaging/NfcMsg;

    .line 1313
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 1316
    sget-object v1, Lcom/samsung/groupcast/application/start/StartActivity;->TAG:Ljava/lang/String;

    invoke-virtual {p3}, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectToFail;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/groupcast/application/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1318
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->BandCacheRelease()V

    goto :goto_0

    .line 1324
    :cond_1
    iget-object v1, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mNfcMsg:Lcom/samsung/groupcast/core/messaging/NfcMsg;

    if-eqz v1, :cond_2

    .line 1325
    sget-object v1, Lcom/samsung/groupcast/application/start/StartActivity;->TAG:Ljava/lang/String;

    const-string v2, "onWifiConnectionFinished dojoin()"

    invoke-static {v1, v2}, Lcom/samsung/groupcast/application/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1326
    iget-object v1, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mNfcMsg:Lcom/samsung/groupcast/core/messaging/NfcMsg;

    invoke-direct {p0, v1}, Lcom/samsung/groupcast/application/start/StartActivity;->doJoin(Lcom/samsung/groupcast/core/messaging/NfcMsg;)V

    goto :goto_0

    .line 1328
    :cond_2
    sget-object v1, Lcom/samsung/groupcast/application/start/StartActivity;->TAG:Ljava/lang/String;

    const-string v2, "onWifiConnectionFinished JoinSession()"

    invoke-static {v1, v2}, Lcom/samsung/groupcast/application/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1329
    new-instance v0, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;

    invoke-direct {v0}, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;-><init>()V

    .line 1330
    .local v0, "joinDialog":Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;
    sget-boolean v1, Lcom/samsung/groupcast/application/start/StartActivity;->mNfcConnectOngoing:Z

    iget-object v2, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mPinFromBand:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->setNfcAutoConnectionInfo(ZLjava/lang/String;)V

    .line 1331
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "TAG_JOIN_SESSION_DIALOG_FRAGMENT"

    invoke-virtual {v0, v1, v2}, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 1332
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentManager;->executePendingTransactions()Z

    goto :goto_0

    .line 1339
    .end local v0    # "joinDialog":Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;
    :cond_3
    if-eqz p1, :cond_4

    .line 1340
    invoke-direct {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->onWifiAPConnectCompleted()V

    goto :goto_0

    .line 1344
    :cond_4
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 1347
    iget-object v1, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mSummittingPincode:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 1348
    const-string v1, ""

    iput-object v1, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mSummittingPincode:Ljava/lang/String;

    .line 1351
    :cond_5
    sget-object v1, Lcom/samsung/groupcast/application/start/StartActivity;->TAG:Ljava/lang/String;

    invoke-virtual {p3}, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectToFail;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/groupcast/application/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public onWifiStateChanged(I)V
    .locals 1
    .param p1, "state"    # I

    .prologue
    .line 2603
    iget-boolean v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mSavedInstanceStateCalled:Z

    if-eqz v0, :cond_1

    .line 2621
    :cond_0
    :goto_0
    return-void

    .line 2606
    :cond_1
    const/4 v0, 0x3

    if-ne p1, v0, :cond_0

    .line 2607
    iget v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mModeStartOrJoin:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 2609
    :pswitch_0
    iget-boolean v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mAlreadyEn:Z

    if-nez v0, :cond_0

    .line 2610
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->createGroup()V

    goto :goto_0

    .line 2607
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method protected setAutoConnectEnabled(Z)Z
    .locals 1
    .param p1, "enable"    # Z

    .prologue
    .line 1049
    sget-object v0, Lcom/samsung/groupcast/core/messaging/MessagingClient;->DefaultInterface:Lcom/samsung/groupcast/core/messaging/MessagingClient$MAGNET_CONNECTION_TYPE;

    invoke-virtual {v0}, Lcom/samsung/groupcast/core/messaging/MessagingClient$MAGNET_CONNECTION_TYPE;->value()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 1056
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 1051
    :pswitch_0
    invoke-virtual {p0, p1}, Lcom/samsung/groupcast/application/start/StartActivity;->setAutoConnectEnabledP2P(Z)Z

    move-result v0

    goto :goto_0

    .line 1054
    :pswitch_1
    invoke-virtual {p0, p1}, Lcom/samsung/groupcast/application/start/StartActivity;->setAutoConnectEnabledWifi(Z)Z

    move-result v0

    goto :goto_0

    .line 1049
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method protected setAutoConnectEnabledP2P(Z)Z
    .locals 5
    .param p1, "enable"    # Z

    .prologue
    const/4 v2, 0x0

    .line 1815
    sget-object v1, Lcom/samsung/groupcast/application/start/StartActivity;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setAutoConnectEnabled: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/samsung/groupcast/application/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1817
    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;

    move-result-object v1

    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->getP2PSSID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->createSession(Ljava/lang/String;)V

    .line 1819
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v3, "AUTOCONNECT_PROGRESS_DIALOG_FRAGMENT"

    invoke-virtual {v1, v3}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;

    iput-object v1, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mProgressDialogFragment:Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;

    .line 1822
    iget-object v1, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mProgressDialogFragment:Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;

    if-nez v1, :cond_0

    .line 1824
    :try_start_0
    new-instance v1, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;

    invoke-direct {v1}, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;-><init>()V

    iput-object v1, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mProgressDialogFragment:Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;

    .line 1825
    iget-object v1, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mProgressDialogFragment:Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;->setTitle(Ljava/lang/String;)V

    .line 1826
    iget-object v1, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mProgressDialogFragment:Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;

    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f080009

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;->setText(Ljava/lang/String;)V

    .line 1828
    iget-object v1, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mProgressDialogFragment:Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;->setCancelable(Z)V

    .line 1829
    sget-object v1, Lcom/samsung/groupcast/application/start/StartActivity;->TAG:Ljava/lang/String;

    const-string v3, "ProgressDialog is showed by setAutoConnectEnabled"

    invoke-static {v1, v3}, Lcom/samsung/groupcast/application/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1830
    iget-object v1, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mProgressDialogFragment:Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;

    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    const-string v4, "AUTOCONNECT_PROGRESS_DIALOG_FRAGMENT"

    invoke-virtual {v1, v3, v4}, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1836
    :cond_0
    const/4 v1, 0x1

    :goto_0
    return v1

    .line 1831
    :catch_0
    move-exception v0

    .line 1832
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    move v1, v2

    .line 1833
    goto :goto_0
.end method

.method protected setAutoConnectEnabledWifi(Z)Z
    .locals 6
    .param p1, "enable"    # Z

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1060
    sget-object v1, Lcom/samsung/groupcast/application/start/StartActivity;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "setAutoConnectEnabled: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Lcom/samsung/groupcast/application/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1062
    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiAPUtils;

    move-result-object v1

    if-eqz v1, :cond_5

    .line 1063
    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiAPUtils;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->setOnRvfHotspotChangeListener(Lcom/samsung/groupcast/net/wifi/WifiAPUtils$OnRvfHotspotChangeListener;)V

    .line 1065
    if-nez p1, :cond_0

    .line 1066
    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiAPUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->releaseRvfHotspot()V

    .line 1097
    :goto_0
    return v2

    .line 1069
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v4, "AUTOCONNECT_PROGRESS_DIALOG_FRAGMENT"

    invoke-virtual {v1, v4}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;

    iput-object v1, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mProgressDialogFragment:Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;

    .line 1072
    iget-object v1, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mProgressDialogFragment:Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;

    if-nez v1, :cond_1

    .line 1074
    :try_start_0
    new-instance v1, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;

    invoke-direct {v1}, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;-><init>()V

    iput-object v1, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mProgressDialogFragment:Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;

    .line 1075
    iget-object v1, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mProgressDialogFragment:Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;

    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;->setTitle(Ljava/lang/String;)V

    .line 1076
    iget-object v4, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mProgressDialogFragment:Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;

    if-eqz p1, :cond_2

    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v5, 0x7f080031

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    :goto_1
    invoke-virtual {v4, v1}, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;->setText(Ljava/lang/String;)V

    .line 1079
    iget-object v1, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mProgressDialogFragment:Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;

    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;->setCancelable(Z)V

    .line 1080
    sget-object v1, Lcom/samsung/groupcast/application/start/StartActivity;->TAG:Ljava/lang/String;

    const-string v4, "ProgressDialog is showed by setAutoConnectEnabled"

    invoke-static {v1, v4}, Lcom/samsung/groupcast/application/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1081
    iget-object v1, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mProgressDialogFragment:Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;

    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v4

    const-string v5, "AUTOCONNECT_PROGRESS_DIALOG_FRAGMENT"

    invoke-virtual {v1, v4, v5}, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1088
    :cond_1
    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiAPUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->isProvisioningNeeded()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1089
    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiAPUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->startProvisioning()V

    goto :goto_0

    .line 1076
    :cond_2
    :try_start_1
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v5, 0x7f080032

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v1

    goto :goto_1

    .line 1082
    :catch_0
    move-exception v0

    .line 1083
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    move v2, v3

    .line 1084
    goto :goto_0

    .line 1091
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_3
    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiAPUtils;

    move-result-object v4

    iget-object v1, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mPin:Ljava/lang/String;

    if-eqz v1, :cond_4

    move v1, v2

    :goto_2
    invoke-virtual {v4, v1}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->setProtected(Z)V

    .line 1092
    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiAPUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->setRvfHotspotEnable()Z

    goto/16 :goto_0

    :cond_4
    move v1, v3

    .line 1091
    goto :goto_2

    .line 1096
    :cond_5
    sget-object v1, Lcom/samsung/groupcast/application/start/StartActivity;->TAG:Ljava/lang/String;

    const-string v2, "WifiAPUtils is null"

    invoke-static {v1, v2}, Lcom/samsung/groupcast/application/Logger;->w(Ljava/lang/String;Ljava/lang/String;)V

    move v2, v3

    .line 1097
    goto/16 :goto_0
.end method

.method public setPinOK(Z)V
    .locals 0
    .param p1, "bOk"    # Z

    .prologue
    .line 2553
    iput-boolean p1, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mPinOK:Z

    .line 2555
    return-void
.end method

.method public showArrow(ZZI)V
    .locals 3
    .param p1, "left"    # Z
    .param p2, "right"    # Z
    .param p3, "count"    # I

    .prologue
    const/4 v2, 0x4

    .line 1262
    iget-object v1, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mMobileAPListView:Lcom/samsung/groupcast/application/start/ApListView;

    invoke-virtual {v1}, Lcom/samsung/groupcast/application/start/ApListView;->getVisibility()I

    move-result v0

    .line 1264
    .local v0, "mMobileAPListVisible":I
    if-eqz p1, :cond_0

    .line 1265
    iget-object v1, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mApLeftArrow:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1269
    :goto_0
    if-eqz p2, :cond_1

    .line 1270
    iget-object v1, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mApRightArrow:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1275
    :goto_1
    invoke-direct {p0, p3}, Lcom/samsung/groupcast/application/start/StartActivity;->apCount(I)V

    .line 1277
    return-void

    .line 1267
    :cond_0
    iget-object v1, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mApLeftArrow:Landroid/widget/ImageView;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 1272
    :cond_1
    iget-object v1, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mApRightArrow:Landroid/widget/ImageView;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1
.end method

.method protected showRemoteInvitationProgress()V
    .locals 3

    .prologue
    .line 721
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->isFinishing()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 722
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "showAddFilesProgressDialog ending..."

    invoke-static {v1, v2}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 741
    :cond_0
    :goto_0
    return-void

    .line 726
    :cond_1
    const/4 v0, 0x0

    .line 727
    .local v0, "fragment":Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "TAG_REMOTE_INVITATION_PROGRESS_DIALOG_FRAGMENT"

    invoke-virtual {v1, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    .end local v0    # "fragment":Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;
    check-cast v0, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;

    .line 730
    .restart local v0    # "fragment":Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;
    if-nez v0, :cond_2

    .line 731
    new-instance v0, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;

    .end local v0    # "fragment":Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;
    invoke-direct {v0}, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;-><init>()V

    .line 734
    .restart local v0    # "fragment":Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;
    :cond_2
    if-eqz v0, :cond_0

    .line 735
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0800ed

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;->setTitle(Ljava/lang/String;)V

    .line 737
    invoke-virtual {v0}, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;->isAdded()Z

    move-result v1

    if-nez v1, :cond_0

    .line 738
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "TAG_REMOTE_INVITATION_PROGRESS_DIALOG_FRAGMENT"

    invoke-virtual {v0, v1, v2}, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected startJoinProcess(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "ssid"    # Ljava/lang/String;
    .param p2, "bssid"    # Ljava/lang/String;
    .param p3, "passphrase"    # Ljava/lang/String;

    .prologue
    .line 1124
    iget-boolean v1, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mSavedInstanceStateCalled:Z

    if-nez v1, :cond_0

    .line 1125
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Connecting to: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 1126
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Connecting to: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 1128
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "TAG_CONNECT_DIALOG_FRAGMENT"

    invoke-virtual {v1, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 1129
    sget-object v1, Lcom/samsung/groupcast/application/start/StartActivity;->TAG:Ljava/lang/String;

    const-string v2, "Now progressing"

    invoke-static {v1, v2}, Lcom/samsung/groupcast/application/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1138
    :cond_0
    :goto_0
    return-void

    .line 1133
    :cond_1
    new-instance v0, Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment;

    invoke-direct {v0}, Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment;-><init>()V

    .line 1134
    .local v0, "connectDialog":Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment;
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "TAG_CONNECT_DIALOG_FRAGMENT"

    invoke-virtual {v0, v1, v2}, Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 1135
    invoke-virtual {v0, p1, p2, p3}, Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment;->connect(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected startNextActivity()V
    .locals 1

    .prologue
    .line 1433
    invoke-direct {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->saveCheckPasswordState()V

    .line 1434
    iget-boolean v0, p0, Lcom/samsung/groupcast/application/start/StartActivity;->mIsCheckedNearbyFrientInput:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/groupcast/application/Preferences;->setNearByFriendsAdvertisePreference(Ljava/lang/Boolean;)V

    .line 1435
    invoke-virtual {p0, p0}, Lcom/samsung/groupcast/application/start/StartActivity;->makeNextActivityIntent(Landroid/app/Activity;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/groupcast/application/start/StartActivity;->startActivity(Landroid/content/Intent;)V

    .line 1437
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->finish()V

    .line 1438
    return-void
.end method

.method protected startWiFiHotspotForExternalAppSharing()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1041
    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiAPUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->isRvfHotspotEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1045
    :goto_0
    return v0

    .line 1044
    :cond_0
    invoke-virtual {p0, v0}, Lcom/samsung/groupcast/application/start/StartActivity;->setAutoConnectEnabled(Z)Z

    .line 1045
    const/4 v0, 0x0

    goto :goto_0
.end method
