.class public Lcom/samsung/groupcast/application/start/MoreGameItem;
.super Ljava/lang/Object;
.source "MoreGameItem.java"


# instance fields
.field private final mAppId:Ljava/lang/String;

.field private final mAverageRating:Ljava/lang/String;

.field private final mCategoryName:Ljava/lang/String;

.field private final mCurrencyUnit:Ljava/lang/String;

.field private final mDiscountFlag:Ljava/lang/String;

.field private final mDiscountPrice:Ljava/lang/String;

.field private final mPrice:Ljava/lang/String;

.field private final mProductID:Ljava/lang/String;

.field private mProductIconImgLocalPath:Ljava/lang/String;

.field private final mProductIconImgURL:Ljava/lang/String;

.field private final mProductName:Ljava/lang/String;

.field private final mRealContentSize:Ljava/lang/String;

.field private final mScreenShotImgURL:Ljava/lang/String;

.field private final mSellerName:Ljava/lang/String;

.field private final mVersionCode:Ljava/lang/String;

.field private final mVersionName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "productID"    # Ljava/lang/String;
    .param p2, "productName"    # Ljava/lang/String;
    .param p3, "appId"    # Ljava/lang/String;
    .param p4, "screenShotImgURL"    # Ljava/lang/String;
    .param p5, "productIconImgURL"    # Ljava/lang/String;
    .param p6, "currencyUnit"    # Ljava/lang/String;
    .param p7, "price"    # Ljava/lang/String;
    .param p8, "discountPrice"    # Ljava/lang/String;
    .param p9, "discountFlag"    # Ljava/lang/String;
    .param p10, "versionName"    # Ljava/lang/String;
    .param p11, "versionCode"    # Ljava/lang/String;
    .param p12, "realContentSize"    # Ljava/lang/String;
    .param p13, "sellerName"    # Ljava/lang/String;
    .param p14, "categoryName"    # Ljava/lang/String;
    .param p15, "averageRating"    # Ljava/lang/String;

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/samsung/groupcast/application/start/MoreGameItem;->mProductID:Ljava/lang/String;

    .line 28
    iput-object p2, p0, Lcom/samsung/groupcast/application/start/MoreGameItem;->mProductName:Ljava/lang/String;

    .line 29
    iput-object p3, p0, Lcom/samsung/groupcast/application/start/MoreGameItem;->mAppId:Ljava/lang/String;

    .line 30
    iput-object p4, p0, Lcom/samsung/groupcast/application/start/MoreGameItem;->mScreenShotImgURL:Ljava/lang/String;

    .line 31
    iput-object p5, p0, Lcom/samsung/groupcast/application/start/MoreGameItem;->mProductIconImgURL:Ljava/lang/String;

    .line 32
    iput-object p6, p0, Lcom/samsung/groupcast/application/start/MoreGameItem;->mCurrencyUnit:Ljava/lang/String;

    .line 33
    iput-object p7, p0, Lcom/samsung/groupcast/application/start/MoreGameItem;->mPrice:Ljava/lang/String;

    .line 34
    iput-object p8, p0, Lcom/samsung/groupcast/application/start/MoreGameItem;->mDiscountPrice:Ljava/lang/String;

    .line 35
    iput-object p9, p0, Lcom/samsung/groupcast/application/start/MoreGameItem;->mDiscountFlag:Ljava/lang/String;

    .line 36
    iput-object p10, p0, Lcom/samsung/groupcast/application/start/MoreGameItem;->mVersionName:Ljava/lang/String;

    .line 37
    iput-object p11, p0, Lcom/samsung/groupcast/application/start/MoreGameItem;->mVersionCode:Ljava/lang/String;

    .line 38
    iput-object p12, p0, Lcom/samsung/groupcast/application/start/MoreGameItem;->mRealContentSize:Ljava/lang/String;

    .line 39
    iput-object p13, p0, Lcom/samsung/groupcast/application/start/MoreGameItem;->mSellerName:Ljava/lang/String;

    .line 40
    iput-object p14, p0, Lcom/samsung/groupcast/application/start/MoreGameItem;->mCategoryName:Ljava/lang/String;

    .line 41
    iput-object p15, p0, Lcom/samsung/groupcast/application/start/MoreGameItem;->mAverageRating:Ljava/lang/String;

    .line 42
    return-void
.end method


# virtual methods
.method public getAppId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/MoreGameItem;->mAppId:Ljava/lang/String;

    return-object v0
.end method

.method public getAverageRating()Ljava/lang/String;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/MoreGameItem;->mAverageRating:Ljava/lang/String;

    return-object v0
.end method

.method public getCategoryName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/MoreGameItem;->mCategoryName:Ljava/lang/String;

    return-object v0
.end method

.method public getCurrencyUnit()Ljava/lang/String;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/MoreGameItem;->mCurrencyUnit:Ljava/lang/String;

    return-object v0
.end method

.method public getDiscountFlag()Ljava/lang/String;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/MoreGameItem;->mDiscountFlag:Ljava/lang/String;

    return-object v0
.end method

.method public getDiscountPrice()Ljava/lang/String;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/MoreGameItem;->mDiscountPrice:Ljava/lang/String;

    return-object v0
.end method

.method public getPrice()Ljava/lang/String;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/MoreGameItem;->mPrice:Ljava/lang/String;

    return-object v0
.end method

.method public getProductID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/MoreGameItem;->mProductID:Ljava/lang/String;

    return-object v0
.end method

.method public getProductIconImgLocalPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/MoreGameItem;->mProductIconImgLocalPath:Ljava/lang/String;

    return-object v0
.end method

.method public getProductIconImgURL()Ljava/lang/String;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/MoreGameItem;->mProductIconImgURL:Ljava/lang/String;

    return-object v0
.end method

.method public getProductName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/MoreGameItem;->mProductName:Ljava/lang/String;

    return-object v0
.end method

.method public getRealContentSize()Ljava/lang/String;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/MoreGameItem;->mRealContentSize:Ljava/lang/String;

    return-object v0
.end method

.method public getScreenShotImgURL()Ljava/lang/String;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/MoreGameItem;->mScreenShotImgURL:Ljava/lang/String;

    return-object v0
.end method

.method public getSellerName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/MoreGameItem;->mSellerName:Ljava/lang/String;

    return-object v0
.end method

.method public getVersionCode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/MoreGameItem;->mVersionCode:Ljava/lang/String;

    return-object v0
.end method

.method public getVersionName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/MoreGameItem;->mVersionName:Ljava/lang/String;

    return-object v0
.end method

.method public setProductIconImgLocalPath(Ljava/lang/String;)V
    .locals 0
    .param p1, "mProductIconImgLocalPath"    # Ljava/lang/String;

    .prologue
    .line 69
    iget-object p1, p0, Lcom/samsung/groupcast/application/start/MoreGameItem;->mProductIconImgLocalPath:Ljava/lang/String;

    .line 70
    return-void
.end method
