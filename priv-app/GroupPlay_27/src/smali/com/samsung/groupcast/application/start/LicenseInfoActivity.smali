.class public Lcom/samsung/groupcast/application/start/LicenseInfoActivity;
.super Landroid/app/Activity;
.source "LicenseInfoActivity.java"


# static fields
.field private static final LOCAL_LICENSE:Ljava/lang/String; = "file:///android_asset/licenses/license_groupplay.html"

.field private static final TAG_PROGRESS_DIALOG_FRAGMENT:Ljava/lang/String; = "LICENSE_PROGRESS_DIALOG_FRAGMENT"


# instance fields
.field private intent:Landroid/content/Intent;

.field private mProgressDialogFragment:Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;

.field private title:Landroid/widget/RelativeLayout;

.field private webview:Landroid/webkit/WebView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 29
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/groupcast/application/start/LicenseInfoActivity;->intent:Landroid/content/Intent;

    return-void
.end method

.method static synthetic access$000(Lcom/samsung/groupcast/application/start/LicenseInfoActivity;)Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/application/start/LicenseInfoActivity;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/LicenseInfoActivity;->mProgressDialogFragment:Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;

    return-object v0
.end method

.method static synthetic access$002(Lcom/samsung/groupcast/application/start/LicenseInfoActivity;Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;)Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/groupcast/application/start/LicenseInfoActivity;
    .param p1, "x1"    # Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;

    .prologue
    .line 23
    iput-object p1, p0, Lcom/samsung/groupcast/application/start/LicenseInfoActivity;->mProgressDialogFragment:Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;

    return-object p1
.end method


# virtual methods
.method public onBackPressed()V
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/LicenseInfoActivity;->webview:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->canGoBack()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 141
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/LicenseInfoActivity;->webview:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->goBack()V

    .line 145
    :goto_0
    return-void

    .line 143
    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onBackPressed()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v3, 0x1

    .line 36
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 39
    const v1, 0x7f040008

    invoke-virtual {p0, v1}, Lcom/samsung/groupcast/application/start/LicenseInfoActivity;->setContentView(I)V

    .line 41
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/LicenseInfoActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "LICENSE_PROGRESS_DIALOG_FRAGMENT"

    invoke-virtual {v1, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;

    iput-object v1, p0, Lcom/samsung/groupcast/application/start/LicenseInfoActivity;->mProgressDialogFragment:Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;

    .line 49
    const v1, 0x7f070016

    invoke-virtual {p0, v1}, Lcom/samsung/groupcast/application/start/LicenseInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/samsung/groupcast/application/start/LicenseInfoActivity;->title:Landroid/widget/RelativeLayout;

    .line 50
    iget-object v1, p0, Lcom/samsung/groupcast/application/start/LicenseInfoActivity;->title:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v3}, Landroid/widget/RelativeLayout;->setClickable(Z)V

    .line 51
    iget-object v1, p0, Lcom/samsung/groupcast/application/start/LicenseInfoActivity;->title:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v3}, Landroid/widget/RelativeLayout;->setFocusable(Z)V

    .line 52
    iget-object v1, p0, Lcom/samsung/groupcast/application/start/LicenseInfoActivity;->title:Landroid/widget/RelativeLayout;

    new-instance v2, Lcom/samsung/groupcast/application/start/LicenseInfoActivity$1;

    invoke-direct {v2, p0}, Lcom/samsung/groupcast/application/start/LicenseInfoActivity$1;-><init>(Lcom/samsung/groupcast/application/start/LicenseInfoActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 65
    const v1, 0x7f070019

    invoke-virtual {p0, v1}, Lcom/samsung/groupcast/application/start/LicenseInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/webkit/WebView;

    iput-object v1, p0, Lcom/samsung/groupcast/application/start/LicenseInfoActivity;->webview:Landroid/webkit/WebView;

    .line 68
    iget-object v1, p0, Lcom/samsung/groupcast/application/start/LicenseInfoActivity;->webview:Landroid/webkit/WebView;

    invoke-virtual {v1}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    .line 69
    .local v0, "webSettings":Landroid/webkit/WebSettings;
    invoke-virtual {v0, v3}, Landroid/webkit/WebSettings;->setLoadWithOverviewMode(Z)V

    .line 70
    invoke-virtual {v0, v3}, Landroid/webkit/WebSettings;->setUseWideViewPort(Z)V

    .line 71
    sget-object v1, Landroid/webkit/WebSettings$LayoutAlgorithm;->NORMAL:Landroid/webkit/WebSettings$LayoutAlgorithm;

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setLayoutAlgorithm(Landroid/webkit/WebSettings$LayoutAlgorithm;)V

    .line 74
    iget-object v1, p0, Lcom/samsung/groupcast/application/start/LicenseInfoActivity;->webview:Landroid/webkit/WebView;

    const-string v2, "file:///android_asset/licenses/license_groupplay.html"

    invoke-virtual {v1, v2}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 75
    iget-object v1, p0, Lcom/samsung/groupcast/application/start/LicenseInfoActivity;->webview:Landroid/webkit/WebView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/webkit/WebView;->setScrollBarStyle(I)V

    .line 76
    iget-object v1, p0, Lcom/samsung/groupcast/application/start/LicenseInfoActivity;->webview:Landroid/webkit/WebView;

    invoke-virtual {v1, v3}, Landroid/webkit/WebView;->setVerticalScrollBarEnabled(Z)V

    .line 79
    iget-object v1, p0, Lcom/samsung/groupcast/application/start/LicenseInfoActivity;->webview:Landroid/webkit/WebView;

    new-instance v2, Lcom/samsung/groupcast/application/start/LicenseInfoActivity$2;

    invoke-direct {v2, p0}, Lcom/samsung/groupcast/application/start/LicenseInfoActivity$2;-><init>(Lcom/samsung/groupcast/application/start/LicenseInfoActivity;)V

    invoke-virtual {v1, v2}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 110
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 129
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 135
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 131
    :pswitch_0
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/LicenseInfoActivity;->finish()V

    .line 132
    const/4 v0, 0x1

    goto :goto_0

    .line 129
    nop

    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 116
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 118
    const-string v1, "power"

    invoke-virtual {p0, v1}, Lcom/samsung/groupcast/application/start/LicenseInfoActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 119
    .local v0, "powerManager":Landroid/os/PowerManager;
    invoke-virtual {v0}, Landroid/os/PowerManager;->isScreenOn()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 120
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/LicenseInfoActivity;->finish()V

    .line 123
    :cond_0
    return-void
.end method
