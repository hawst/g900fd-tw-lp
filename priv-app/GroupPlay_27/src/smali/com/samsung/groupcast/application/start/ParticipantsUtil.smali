.class public Lcom/samsung/groupcast/application/start/ParticipantsUtil;
.super Ljava/lang/Object;
.source "ParticipantsUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/groupcast/application/start/ParticipantsUtil$MappingTable;
    }
.end annotation


# static fields
.field public static final DOCUMENTS_COLLECTION_NAME:Ljava/lang/String; = "MyDocumentPages"

.field public static final GROUP_CAMCODER_PARTICIPANT_ID:Ljava/lang/String; = "GroupCamcoder"

.field public static final ICON_APP:Ljava/lang/Byte;

.field public static final ICON_DEFAULT:Ljava/lang/Byte;

.field public static final ICON_DOCUMENT:Ljava/lang/Byte;

.field public static final ICON_GAME:Ljava/lang/Byte;

.field public static final ICON_GROUPCAMCORDER:Ljava/lang/Byte;

.field public static final ICON_IMAGE:Ljava/lang/Byte;

.field public static final ICON_MUSIC:Ljava/lang/Byte;

.field public static final ICON_VIDEO:Ljava/lang/Byte;

.field public static final IMAGE_COLLECTION_NAME:Ljava/lang/String; = "MyImages"

.field public static final MAIN_COLLECTION_NAME:Ljava/lang/String; = "MyCollagePages"

.field public static final MUSIC_LIVE_SHARE_PARTICIPANT_ID:Ljava/lang/String; = "MusicLiveShare"

.field public static final SHARE_VIDEO_PARTICIPANT_ID:Ljava/lang/String; = "ShareVideo"

.field public static instance:Lcom/samsung/groupcast/application/start/ParticipantsUtil;


# instance fields
.field private final mHashmapMappingTable:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Byte;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v0

    sput-object v0, Lcom/samsung/groupcast/application/start/ParticipantsUtil;->ICON_DEFAULT:Ljava/lang/Byte;

    .line 23
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v0

    sput-object v0, Lcom/samsung/groupcast/application/start/ParticipantsUtil;->ICON_MUSIC:Ljava/lang/Byte;

    .line 24
    const/4 v0, 0x2

    invoke-static {v0}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v0

    sput-object v0, Lcom/samsung/groupcast/application/start/ParticipantsUtil;->ICON_IMAGE:Ljava/lang/Byte;

    .line 25
    const/4 v0, 0x4

    invoke-static {v0}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v0

    sput-object v0, Lcom/samsung/groupcast/application/start/ParticipantsUtil;->ICON_VIDEO:Ljava/lang/Byte;

    .line 26
    const/16 v0, 0x8

    invoke-static {v0}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v0

    sput-object v0, Lcom/samsung/groupcast/application/start/ParticipantsUtil;->ICON_DOCUMENT:Ljava/lang/Byte;

    .line 27
    const/16 v0, 0x10

    invoke-static {v0}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v0

    sput-object v0, Lcom/samsung/groupcast/application/start/ParticipantsUtil;->ICON_GROUPCAMCORDER:Ljava/lang/Byte;

    .line 28
    const/16 v0, 0x20

    invoke-static {v0}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v0

    sput-object v0, Lcom/samsung/groupcast/application/start/ParticipantsUtil;->ICON_GAME:Ljava/lang/Byte;

    .line 29
    const/16 v0, 0x40

    invoke-static {v0}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v0

    sput-object v0, Lcom/samsung/groupcast/application/start/ParticipantsUtil;->ICON_APP:Ljava/lang/Byte;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/groupcast/application/start/ParticipantsUtil;->mHashmapMappingTable:Ljava/util/HashMap;

    .line 68
    return-void
.end method

.method public static getIconFromJoinedActivity(Ljava/lang/String;)Ljava/lang/Byte;
    .locals 4
    .param p0, "joinedActivity"    # Ljava/lang/String;

    .prologue
    .line 72
    if-nez p0, :cond_1

    .line 73
    sget-object v2, Lcom/samsung/groupcast/application/start/ParticipantsUtil;->ICON_DEFAULT:Ljava/lang/Byte;

    .line 89
    :cond_0
    :goto_0
    return-object v2

    .line 76
    :cond_1
    invoke-static {}, Lcom/samsung/groupcast/application/start/ParticipantsUtil$MappingTable;->values()[Lcom/samsung/groupcast/application/start/ParticipantsUtil$MappingTable;

    move-result-object v0

    .line 77
    .local v0, "array":[Lcom/samsung/groupcast/application/start/ParticipantsUtil$MappingTable;
    sget-object v2, Lcom/samsung/groupcast/application/start/ParticipantsUtil;->ICON_DEFAULT:Ljava/lang/Byte;

    .line 79
    .local v2, "icon":Ljava/lang/Byte;
    const-string v3, "GAME_"

    invoke-virtual {p0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 80
    sget-object v2, Lcom/samsung/groupcast/application/start/ParticipantsUtil;->ICON_GAME:Ljava/lang/Byte;

    goto :goto_0

    .line 83
    :cond_2
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    array-length v3, v0

    if-ge v1, v3, :cond_0

    .line 84
    aget-object v3, v0, v1

    # getter for: Lcom/samsung/groupcast/application/start/ParticipantsUtil$MappingTable;->mID:Ljava/lang/String;
    invoke-static {v3}, Lcom/samsung/groupcast/application/start/ParticipantsUtil$MappingTable;->access$000(Lcom/samsung/groupcast/application/start/ParticipantsUtil$MappingTable;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 85
    aget-object v3, v0, v1

    # getter for: Lcom/samsung/groupcast/application/start/ParticipantsUtil$MappingTable;->mIcon:Ljava/lang/Byte;
    invoke-static {v3}, Lcom/samsung/groupcast/application/start/ParticipantsUtil$MappingTable;->access$100(Lcom/samsung/groupcast/application/start/ParticipantsUtil$MappingTable;)Ljava/lang/Byte;

    move-result-object v2

    .line 86
    goto :goto_0

    .line 83
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public static getIconResID(Ljava/lang/Byte;)I
    .locals 4
    .param p0, "icon"    # Ljava/lang/Byte;

    .prologue
    .line 94
    sget-object v3, Lcom/samsung/groupcast/application/start/ParticipantsUtil$MappingTable;->Default:Lcom/samsung/groupcast/application/start/ParticipantsUtil$MappingTable;

    # getter for: Lcom/samsung/groupcast/application/start/ParticipantsUtil$MappingTable;->mResID:I
    invoke-static {v3}, Lcom/samsung/groupcast/application/start/ParticipantsUtil$MappingTable;->access$200(Lcom/samsung/groupcast/application/start/ParticipantsUtil$MappingTable;)I

    move-result v2

    .line 95
    .local v2, "resID":I
    invoke-static {}, Lcom/samsung/groupcast/application/start/ParticipantsUtil$MappingTable;->values()[Lcom/samsung/groupcast/application/start/ParticipantsUtil$MappingTable;

    move-result-object v0

    .line 97
    .local v0, "array":[Lcom/samsung/groupcast/application/start/ParticipantsUtil$MappingTable;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v3, v0

    if-ge v1, v3, :cond_0

    .line 98
    aget-object v3, v0, v1

    # getter for: Lcom/samsung/groupcast/application/start/ParticipantsUtil$MappingTable;->mIcon:Ljava/lang/Byte;
    invoke-static {v3}, Lcom/samsung/groupcast/application/start/ParticipantsUtil$MappingTable;->access$100(Lcom/samsung/groupcast/application/start/ParticipantsUtil$MappingTable;)Ljava/lang/Byte;

    move-result-object v3

    if-ne v3, p0, :cond_1

    .line 99
    aget-object v3, v0, v1

    # getter for: Lcom/samsung/groupcast/application/start/ParticipantsUtil$MappingTable;->mResID:I
    invoke-static {v3}, Lcom/samsung/groupcast/application/start/ParticipantsUtil$MappingTable;->access$200(Lcom/samsung/groupcast/application/start/ParticipantsUtil$MappingTable;)I

    move-result v2

    .line 103
    :cond_0
    return v2

    .line 97
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public static getInstance()Lcom/samsung/groupcast/application/start/ParticipantsUtil;
    .locals 1

    .prologue
    .line 13
    sget-object v0, Lcom/samsung/groupcast/application/start/ParticipantsUtil;->instance:Lcom/samsung/groupcast/application/start/ParticipantsUtil;

    if-nez v0, :cond_0

    .line 14
    new-instance v0, Lcom/samsung/groupcast/application/start/ParticipantsUtil;

    invoke-direct {v0}, Lcom/samsung/groupcast/application/start/ParticipantsUtil;-><init>()V

    sput-object v0, Lcom/samsung/groupcast/application/start/ParticipantsUtil;->instance:Lcom/samsung/groupcast/application/start/ParticipantsUtil;

    .line 15
    sget-object v0, Lcom/samsung/groupcast/application/start/ParticipantsUtil;->instance:Lcom/samsung/groupcast/application/start/ParticipantsUtil;

    .line 17
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/samsung/groupcast/application/start/ParticipantsUtil;->instance:Lcom/samsung/groupcast/application/start/ParticipantsUtil;

    goto :goto_0
.end method
