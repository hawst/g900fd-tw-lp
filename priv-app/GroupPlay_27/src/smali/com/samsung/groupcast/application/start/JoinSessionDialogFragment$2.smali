.class Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment$2;
.super Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchFileListener;
.source "JoinSessionDialogFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->fetchManifestForSession(Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;

.field final synthetic val$pin:Ljava/lang/String;

.field final synthetic val$summary:Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;


# direct methods
.method constructor <init>(Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 282
    iput-object p1, p0, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment$2;->this$0:Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;

    iput-object p2, p0, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment$2;->val$summary:Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;

    iput-object p3, p0, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment$2;->val$pin:Ljava/lang/String;

    invoke-direct {p0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchFileListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onComplete(Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchFileRequest;Lcom/samsung/groupcast/misc/requests/Result;Ljava/lang/String;)V
    .locals 6
    .param p1, "request"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchFileRequest;
    .param p2, "result"    # Lcom/samsung/groupcast/misc/requests/Result;
    .param p3, "path"    # Ljava/lang/String;

    .prologue
    .line 285
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment$2;->val$summary:Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;

    if-eqz v0, :cond_1

    .line 286
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "summary:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment$2;->val$summary:Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 287
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment$2;->val$summary:Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;

    invoke-virtual {v0}, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;->isPinProtected()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 288
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "PIN:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment$2;->val$summary:Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;

    invoke-virtual {v1}, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;->isPinProtected()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",d:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment$2;->this$0:Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;

    # invokes: Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->getDelegate()Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment$JoinSessionDialogFragmentDelegate;
    invoke-static {v1}, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->access$000(Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;)Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment$JoinSessionDialogFragmentDelegate;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",P:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, p0, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment$2;->this$0:Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;

    # invokes: Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->getDelegate()Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment$JoinSessionDialogFragmentDelegate;
    invoke-static {v0}, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->access$000(Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;)Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment$JoinSessionDialogFragmentDelegate;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment$2;->this$0:Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;

    # invokes: Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->getDelegate()Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment$JoinSessionDialogFragmentDelegate;
    invoke-static {v0}, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->access$000(Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;)Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment$JoinSessionDialogFragmentDelegate;

    move-result-object v0

    invoke-interface {v0}, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment$JoinSessionDialogFragmentDelegate;->getPinOk()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 290
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment$2;->this$0:Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;

    # invokes: Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->getDelegate()Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment$JoinSessionDialogFragmentDelegate;
    invoke-static {v0}, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->access$000(Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;)Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment$JoinSessionDialogFragmentDelegate;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment$2;->this$0:Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;

    # invokes: Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->getDelegate()Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment$JoinSessionDialogFragmentDelegate;
    invoke-static {v0}, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->access$000(Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;)Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment$JoinSessionDialogFragmentDelegate;

    move-result-object v0

    invoke-interface {v0}, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment$JoinSessionDialogFragmentDelegate;->getPinOk()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment$2;->this$0:Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;

    # getter for: Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->mNfcAutoConnectionGoingOn:Z
    invoke-static {v0}, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->access$100(Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 292
    :cond_0
    const-string v0, "joining..."

    invoke-static {v0}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 293
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment$2;->this$0:Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;

    iget-object v1, p0, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment$2;->val$summary:Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;

    iget-object v2, p0, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment$2;->val$pin:Ljava/lang/String;

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    # invokes: Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->onFetchManifestRequestComplete(Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchFileRequest;Lcom/samsung/groupcast/misc/requests/Result;Ljava/lang/String;)V
    invoke-static/range {v0 .. v5}, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->access$200(Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchFileRequest;Lcom/samsung/groupcast/misc/requests/Result;Ljava/lang/String;)V

    .line 302
    :cond_1
    :goto_1
    return-void

    .line 288
    :cond_2
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 295
    :cond_3
    const-string v0, "joining rejected"

    invoke-static {v0}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    goto :goto_1

    .line 298
    :cond_4
    const-string v0, "joining... no pin"

    invoke-static {v0}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 299
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment$2;->this$0:Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;

    iget-object v1, p0, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment$2;->val$summary:Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;

    iget-object v2, p0, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment$2;->val$pin:Ljava/lang/String;

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    # invokes: Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->onFetchManifestRequestComplete(Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchFileRequest;Lcom/samsung/groupcast/misc/requests/Result;Ljava/lang/String;)V
    invoke-static/range {v0 .. v5}, Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;->access$200(Lcom/samsung/groupcast/application/start/JoinSessionDialogFragment;Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchFileRequest;Lcom/samsung/groupcast/misc/requests/Result;Ljava/lang/String;)V

    goto :goto_1
.end method
