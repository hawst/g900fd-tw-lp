.class Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment$1;
.super Ljava/lang/Object;
.source "NicknameInputDialogFragment.java"

# interfaces
.implements Landroid/widget/TextView$OnEditorActionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;


# direct methods
.method constructor <init>(Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;)V
    .locals 0

    .prologue
    .line 93
    iput-object p1, p0, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment$1;->this$0:Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 5
    .param p1, "v"    # Landroid/widget/TextView;
    .param p2, "actionId"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 96
    iget-object v2, p0, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment$1;->this$0:Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;

    # invokes: Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;->updateOkButton()V
    invoke-static {v2}, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;->access$000(Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;)V

    .line 98
    iget-object v2, p0, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment$1;->this$0:Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;

    # invokes: Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;->isNicknameEntered()Z
    invoke-static {v2}, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;->access$100(Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 115
    :cond_0
    :goto_0
    return v0

    .line 102
    :cond_1
    invoke-static {p2, p3}, Lcom/samsung/groupcast/view/ViewTools;->isEditorActionForSubmitting(ILandroid/view/KeyEvent;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 103
    iget-object v2, p0, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment$1;->this$0:Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;

    # getter for: Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;->mDelegate:Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment$NicknameInputDialogFragmentDelegate;
    invoke-static {v2}, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;->access$200(Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;)Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment$NicknameInputDialogFragmentDelegate;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 104
    iget-object v2, p0, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment$1;->this$0:Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;

    # getter for: Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;->mDelegate:Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment$NicknameInputDialogFragmentDelegate;
    invoke-static {v2}, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;->access$200(Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;)Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment$NicknameInputDialogFragmentDelegate;

    move-result-object v2

    # getter for: Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;->mInstance:Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;
    invoke-static {}, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;->access$300()Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment$1;->this$0:Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;

    # getter for: Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;->mNickEditText:Landroid/widget/EditText;
    invoke-static {v4}, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;->access$400(Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;)Landroid/widget/EditText;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4, v1}, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment$NicknameInputDialogFragmentDelegate;->onNicknameSubmitted(Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;Ljava/lang/String;Z)V

    .line 108
    :cond_2
    iget-object v1, p0, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment$1;->this$0:Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;

    # getter for: Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;->mNickEditText:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;->access$400(Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;)Landroid/widget/EditText;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 109
    iget-object v1, p0, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment$1;->this$0:Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;

    # getter for: Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;->mNickEditText:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;->access$400(Lcom/samsung/groupcast/application/fragment/NicknameInputDialogFragment;)Landroid/widget/EditText;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_3
    move v0, v1

    .line 115
    goto :goto_0
.end method
