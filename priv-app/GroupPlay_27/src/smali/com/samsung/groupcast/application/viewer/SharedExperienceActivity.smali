.class public abstract Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;
.super Lcom/sec/android/band/BandActivity;
.source "SharedExperienceActivity.java"

# interfaces
.implements Landroid/nfc/NfcAdapter$CreateNdefMessageCallback;
.implements Landroid/os/Handler$Callback;
.implements Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker$ContentBrokerDelegate;
.implements Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentDeliveryManagerDelegate;
.implements Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator$ManifestGeneratorDelegate;
.implements Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$SharedExperienceManagerDelegate;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity$6;
    }
.end annotation


# static fields
.field public static final GAME_PREFIX_FOR_ACTIVITY:Ljava/lang/String; = "GAME_"

.field public static final HELP_DISABLED:I = 0x0

.field public static final HELP_ENABLED_ALL:I = 0x1

.field public static final HELP_ENABLED_FLAG:Ljava/lang/String; = "HELP"

.field public static final HELP_ENABLED_MUSIC_ONLY:I = 0x2

.field public static final HELP_ENABLED_VIDEO_ONLY:I = 0x3

.field protected static final KEY_CONTENT_BROKER_STORE_ID:Ljava/lang/String; = "CONTENT_BROKER_STORE_ID"

.field protected static final KEY_MANIFEST_GENERATOR_STORE_ID:Ljava/lang/String; = "MANIFEST_GENERATOR_STORE_ID"

.field protected static final KEY_OBJECT_STORE_HASH:Ljava/lang/String; = "OBJECT_STORE_HASH"

.field protected static final KEY_SESSION_STORE_ID:Ljava/lang/String; = "SESSION_STORE_ID"

.field protected static final KEY_SHARED_EXPERIENCE_MANAGER_STORE_ID:Ljava/lang/String; = "SHARED_EXPERIENCE_MANAGER_STORE_ID"

.field public static final MUSIC_LIVE_SHARE_PKG_NAME:Ljava/lang/String; = "com.sec.android.app.mediasync"

.field public static final MUSIC_LIVE_SHARE_SCREEN_ID:Ljava/lang/String; = "MUSIC_LIVE_SHARE_SCREEN"

.field public static final SHARE_VIDEO_PKG_NAME:Ljava/lang/String; = "com.sec.android.app.mv.player"

.field private static final TAG:Ljava/lang/String;


# instance fields
.field public landscapeEnable:Z

.field protected mBandInterface:Lcom/sec/android/band/BandInterface;

.field private mContentBroker:Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;

.field private final mContentDeliveryListeners:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/LinkedList",
            "<",
            "Lcom/samsung/groupcast/application/viewer/ContentDeliveryListener;",
            ">;>;"
        }
    .end annotation
.end field

.field private final mContentDeliveryLock:Ljava/lang/Object;

.field private final mContentListener:Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentListener;

.field private mContentPrefetcher:Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentPrefetcher;

.field private final mContentRequests:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentRequest;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field private mCurrentActivityName:Ljava/lang/String;

.field private mExternalEventHandler:Lcom/samsung/groupcast/GroupPlayManager$ExternalEventHandler;

.field protected mGp:Lcom/samsung/groupcast/GroupPlayManager;

.field private mHelpState:I

.field protected mIsHelpEnabled:Z

.field protected mIsStartedByExternalApp:Z

.field private mLastSave:Landroid/os/Bundle;

.field private mManifestGenerator:Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;

.field protected mMenuType:Ljava/lang/String;

.field private mPaused:Z

.field protected mServiceType:Ljava/lang/String;

.field private mSession:Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

.field protected mSessionName:Ljava/lang/String;

.field private mSharedExperienceManager:Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;

.field private mStoreManifestRequest:Lcom/samsung/groupcast/legacy/gp2/session/storage/StoreManifestRequest;

.field protected mWifiUtils:Lcom/samsung/groupcast/net/wifi/WifiUtils;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 80
    const-class v0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;

    invoke-static {v0}, Lcom/samsung/groupcast/application/Logger;->getTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 71
    invoke-direct {p0}, Lcom/sec/android/band/BandActivity;-><init>()V

    .line 99
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mPaused:Z

    .line 114
    iput-object v2, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mServiceType:Ljava/lang/String;

    .line 115
    iput-object v2, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mMenuType:Ljava/lang/String;

    .line 119
    iput-boolean v1, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mIsHelpEnabled:Z

    .line 120
    iput v1, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mHelpState:I

    .line 121
    iput-boolean v1, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->landscapeEnable:Z

    .line 1071
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mContentDeliveryLock:Ljava/lang/Object;

    .line 1072
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mContentDeliveryListeners:Ljava/util/HashMap;

    .line 1074
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mContentRequests:Ljava/util/HashMap;

    .line 1076
    new-instance v0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity$4;

    invoke-direct {v0, p0}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity$4;-><init>(Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;)V

    iput-object v0, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mContentListener:Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentListener;

    return-void
.end method

.method static synthetic access$000(Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mCurrentActivityName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$1002(Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;Lcom/samsung/groupcast/legacy/gp2/session/storage/StoreManifestRequest;)Lcom/samsung/groupcast/legacy/gp2/session/storage/StoreManifestRequest;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;
    .param p1, "x1"    # Lcom/samsung/groupcast/legacy/gp2/session/storage/StoreManifestRequest;

    .prologue
    .line 71
    iput-object p1, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mStoreManifestRequest:Lcom/samsung/groupcast/legacy/gp2/session/storage/StoreManifestRequest;

    return-object p1
.end method

.method static synthetic access$200(Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;)Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mManifestGenerator:Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;

    return-object v0
.end method

.method static synthetic access$202(Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;)Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;
    .param p1, "x1"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;

    .prologue
    .line 71
    iput-object p1, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mManifestGenerator:Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;

    return-object p1
.end method

.method static synthetic access$300(Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mContentDeliveryLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$400(Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;)Ljava/util/HashMap;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mContentDeliveryListeners:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$500(Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;)Ljava/util/HashMap;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mContentRequests:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$600(Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;)Lcom/samsung/groupcast/legacy/gp2/session/model/Session;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mSession:Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    return-object v0
.end method

.method static synthetic access$700(Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;)Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mContentBroker:Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;

    return-object v0
.end method

.method static synthetic access$800(Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;)Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mContentListener:Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentListener;

    return-object v0
.end method

.method static synthetic access$900(Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;

    .prologue
    .line 71
    iget-boolean v0, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mPaused:Z

    return v0
.end method

.method private onStartCollectionsPrefetching()V
    .locals 6

    .prologue
    .line 1265
    iget-object v4, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mContentBroker:Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;

    if-nez v4, :cond_1

    .line 1282
    :cond_0
    return-void

    .line 1268
    :cond_1
    iget-object v4, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mContentPrefetcher:Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentPrefetcher;

    if-eqz v4, :cond_2

    .line 1269
    iget-object v4, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mContentPrefetcher:Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentPrefetcher;

    invoke-virtual {v4}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentPrefetcher;->invalidate()V

    .line 1270
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mContentPrefetcher:Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentPrefetcher;

    .line 1274
    :cond_2
    new-instance v4, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentPrefetcher;

    iget-object v5, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mContentBroker:Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;

    invoke-direct {v4, v5, p0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentPrefetcher;-><init>(Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentDeliveryManagerDelegate;)V

    iput-object v4, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mContentPrefetcher:Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentPrefetcher;

    .line 1277
    iget-object v4, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mSession:Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    invoke-virtual {v4}, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->getManifest()Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;->getCollections()Ljava/util/HashMap;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1278
    .local v0, "collectionName":Ljava/lang/String;
    iget-object v4, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mSession:Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    invoke-virtual {v4}, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->getManifest()Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;

    move-result-object v4

    invoke-virtual {v4, v0}, Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;->getCollection(Ljava/lang/String;)Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;

    move-result-object v4

    if-eqz v4, :cond_3

    .line 1279
    iget-object v4, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mSession:Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    invoke-virtual {v4}, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->getManifest()Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;

    move-result-object v4

    invoke-virtual {v4, v0}, Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;->getCollection(Ljava/lang/String;)Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->getItems()Ljava/util/Map;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1280
    .local v1, "contentId":Ljava/lang/String;
    iget-object v4, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mContentPrefetcher:Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentPrefetcher;

    invoke-virtual {v4, v1}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentPrefetcher;->addRequest(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private onStopCollectionsPrefetching()V
    .locals 1

    .prologue
    .line 1285
    iget-object v0, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mContentPrefetcher:Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentPrefetcher;

    if-eqz v0, :cond_0

    .line 1286
    iget-object v0, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mContentPrefetcher:Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentPrefetcher;

    invoke-virtual {v0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentPrefetcher;->invalidate()V

    .line 1287
    :cond_0
    return-void
.end method

.method private processNfcIntent(Landroid/content/Intent;)V
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 893
    sget-object v0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->TAG:Ljava/lang/String;

    const-string v1, "Received NFC intent - ignoring..."

    invoke-static {v0, v1}, Lcom/samsung/groupcast/application/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 896
    return-void
.end method

.method private stopContentDelivery()V
    .locals 9

    .prologue
    .line 1159
    iget-object v7, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mContentDeliveryLock:Ljava/lang/Object;

    monitor-enter v7

    .line 1160
    :try_start_0
    iget-object v6, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mContentRequests:Ljava/util/HashMap;

    invoke-virtual {v6}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 1162
    .local v1, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentRequest;>;>;"
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 1164
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/Map$Entry;

    .line 1165
    .local v5, "pair":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentRequest;>;"
    invoke-interface {v5}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentRequest;

    invoke-virtual {v6}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentRequest;->isCompleted()Z

    move-result v0

    .line 1166
    .local v0, "isCompleted":Z
    invoke-interface {v5}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentRequest;

    invoke-virtual {v6}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentRequest;->invalidate()V

    .line 1167
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    .line 1169
    if-nez v0, :cond_0

    .line 1173
    iget-object v6, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mContentDeliveryListeners:Ljava/util/HashMap;

    invoke-interface {v5}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/LinkedList;

    .line 1175
    .local v3, "list":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/samsung/groupcast/application/viewer/ContentDeliveryListener;>;"
    if-eqz v3, :cond_0

    .line 1176
    invoke-virtual {v3}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 1177
    .local v2, "iterator2":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/groupcast/application/viewer/ContentDeliveryListener;>;"
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 1178
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/groupcast/application/viewer/ContentDeliveryListener;

    .line 1179
    .local v4, "listener":Lcom/samsung/groupcast/application/viewer/ContentDeliveryListener;
    invoke-interface {v5}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-interface {v4, v6}, Lcom/samsung/groupcast/application/viewer/ContentDeliveryListener;->onContentDeliveryAbort(Ljava/lang/String;)V

    goto :goto_0

    .line 1183
    .end local v0    # "isCompleted":Z
    .end local v1    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentRequest;>;>;"
    .end local v2    # "iterator2":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/groupcast/application/viewer/ContentDeliveryListener;>;"
    .end local v3    # "list":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/samsung/groupcast/application/viewer/ContentDeliveryListener;>;"
    .end local v4    # "listener":Lcom/samsung/groupcast/application/viewer/ContentDeliveryListener;
    .end local v5    # "pair":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentRequest;>;"
    :catchall_0
    move-exception v6

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v6

    .restart local v1    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentRequest;>;>;"
    :cond_1
    :try_start_1
    monitor-exit v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1184
    return-void
.end method


# virtual methods
.method protected IntentGroupCamcoder(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "mPath"    # Ljava/lang/String;
    .param p2, "mUri"    # Ljava/lang/String;

    .prologue
    .line 1372
    const-string v1, " IntentGroupCamcoder >>>>>>"

    invoke-static {v1}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 1374
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.samsung.groupcamcoder.ACTION_START_CAMCORDER"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1377
    .local v0, "intent":Landroid/content/Intent;
    :try_start_0
    invoke-static {v0}, Lcom/samsung/groupcast/application/IntentTools;->AddChordIfIntent(Landroid/content/Intent;)V

    .line 1378
    const-string v1, "DeviceName"

    invoke-static {}, Lcom/samsung/groupcast/application/Environment;->getUserName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1379
    const-string v1, "filePath"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1380
    const-string v1, "uri"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1381
    invoke-virtual {p0, v0}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->startActivity(Landroid/content/Intent;)V

    .line 1382
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mIsStartedByExternalApp:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1386
    :goto_0
    return-void

    .line 1383
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method protected IntentMusicLiveShareReceiver(Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 6
    .param p1, "playFrom"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1291
    .local p2, "playList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    const-string v3, ""

    .line 1293
    .local v3, "userName":Ljava/lang/String;
    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiUtils;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mWifiUtils:Lcom/samsung/groupcast/net/wifi/WifiUtils;

    .line 1296
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->getSession()Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {p0}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->getSession()Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->getSessionId()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 1297
    invoke-static {}, Lcom/samsung/groupcast/application/App;->getMessagingClient()Lcom/samsung/groupcast/core/messaging/MessagingClient;

    move-result-object v4

    invoke-virtual {p0}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->getSession()Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->getSessionId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/samsung/groupcast/core/messaging/MessagingClient;->acquireLocalChannel(Ljava/lang/String;)Lcom/samsung/groupcast/core/messaging/LocalChannel;

    move-result-object v2

    .line 1299
    .local v2, "mChannel":Lcom/samsung/groupcast/core/messaging/LocalChannel;
    invoke-virtual {v2}, Lcom/samsung/groupcast/core/messaging/LocalChannel;->getUserInfo()Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;->getName()Ljava/lang/String;

    move-result-object v3

    .line 1326
    .end local v2    # "mChannel":Lcom/samsung/groupcast/core/messaging/LocalChannel;
    :goto_0
    :try_start_0
    iget-object v4, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mSessionName:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {v3, v4, p1, p2, v5}, Lcom/samsung/groupcast/application/IntentTools;->getMusicLiveShareIntent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Lcom/samsung/groupcast/core/messaging/NfcMsg;)Landroid/content/Intent;

    move-result-object v1

    .line 1330
    .local v1, "intent":Landroid/content/Intent;
    const-string v4, "music_channel_id"

    iget-object v5, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mMenuType:Ljava/lang/String;

    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_1

    .line 1333
    const v4, 0x10008

    :try_start_1
    invoke-virtual {p0, v1, v4}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_1
    .catch Landroid/content/ActivityNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    .line 1342
    .end local v1    # "intent":Landroid/content/Intent;
    :goto_1
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mIsStartedByExternalApp:Z

    .line 1343
    return-void

    .line 1301
    :cond_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/samsung/groupcast/application/Environment;->getUserName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mWifiUtils:Lcom/samsung/groupcast/net/wifi/WifiUtils;

    invoke-virtual {v5}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getWifiIPLastNum()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 1334
    .restart local v1    # "intent":Landroid/content/Intent;
    :catch_0
    move-exception v0

    .line 1335
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    :try_start_2
    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->printStackTrace()V
    :try_end_2
    .catch Landroid/content/ActivityNotFoundException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_1

    .line 1338
    .end local v0    # "e":Landroid/content/ActivityNotFoundException;
    .end local v1    # "intent":Landroid/content/Intent;
    :catch_1
    move-exception v0

    .line 1339
    .restart local v0    # "e":Landroid/content/ActivityNotFoundException;
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    const-string v5, "#### is not MusicServerActivity.apk install ####"

    invoke-static {v4, v5}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;)V

    goto :goto_1
.end method

.method protected IntentShareVideo(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "mPath"    # Ljava/lang/String;
    .param p2, "mUri"    # Ljava/lang/String;

    .prologue
    .line 1347
    const-string v1, " IntentShareVideo >>>>>>"

    invoke-static {v1}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 1349
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.samsung.sharevideo.ACTION_START_MVPLAYER"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1360
    .local v0, "intent":Landroid/content/Intent;
    :try_start_0
    invoke-static {v0}, Lcom/samsung/groupcast/application/IntentTools;->AddChordIfIntent(Landroid/content/Intent;)V

    .line 1361
    const-string v1, "DeviceName"

    invoke-static {}, Lcom/samsung/groupcast/application/Environment;->getUserName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1362
    const-string v1, "filePath"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1363
    const-string v1, "uri"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1364
    invoke-virtual {p0, v0}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->startActivity(Landroid/content/Intent;)V

    .line 1365
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mIsStartedByExternalApp:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1369
    :goto_0
    return-void

    .line 1366
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public createNdefMessage(Landroid/nfc/NfcEvent;)Landroid/nfc/NdefMessage;
    .locals 8
    .param p1, "event"    # Landroid/nfc/NfcEvent;

    .prologue
    .line 900
    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiUtils;

    move-result-object v7

    .line 901
    .local v7, "mWifiUtils":Lcom/samsung/groupcast/net/wifi/WifiUtils;
    invoke-virtual {v7}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getWifiBSSID()Ljava/lang/String;

    move-result-object v1

    .line 902
    .local v1, "bssid":Ljava/lang/String;
    invoke-virtual {v7}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getWifiSSID()Ljava/lang/String;

    move-result-object v3

    .line 904
    .local v3, "ssid":Ljava/lang/String;
    if-nez v1, :cond_0

    .line 905
    new-instance v2, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity$3;

    invoke-direct {v2, p0}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity$3;-><init>(Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;)V

    invoke-static {v2}, Lcom/samsung/groupcast/application/MainQueue;->post(Ljava/lang/Runnable;)V

    .line 914
    sget-object v2, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->TAG:Ljava/lang/String;

    const-string v4, "Not connected to any WiFi network"

    invoke-static {v2, v4}, Lcom/samsung/groupcast/application/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 915
    const/4 v2, 0x0

    .line 924
    :goto_0
    return-object v2

    .line 921
    :cond_0
    new-instance v0, Lcom/samsung/groupcast/core/messaging/NfcMsg;

    invoke-virtual {v7}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getWifiPassword()Ljava/lang/String;

    move-result-object v2

    new-instance v4, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;

    invoke-virtual {p0}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->getSession()Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;-><init>(Lcom/samsung/groupcast/legacy/gp2/session/model/Session;)V

    invoke-virtual {p0}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->getSession()Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->getPin()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->getCurrentActivityName()Ljava/lang/String;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, Lcom/samsung/groupcast/core/messaging/NfcMsg;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;Ljava/lang/String;Ljava/lang/String;)V

    .line 924
    .local v0, "message":Lcom/samsung/groupcast/core/messaging/NfcMsg;
    invoke-virtual {v0}, Lcom/samsung/groupcast/core/messaging/NfcMsg;->getNdefMessage()Landroid/nfc/NdefMessage;

    move-result-object v2

    goto :goto_0
.end method

.method protected disableShareExperienceObjects()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 970
    iget-object v0, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mSharedExperienceManager:Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;

    if-eqz v0, :cond_0

    .line 971
    iget-object v0, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mSharedExperienceManager:Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;

    invoke-virtual {v0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->disable()V

    .line 972
    iget-object v0, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mSharedExperienceManager:Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;

    invoke-virtual {v0, v1, p0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->setDelegate(Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$SharedExperienceManagerDelegate;Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$SharedExperienceManagerDelegate;)V

    .line 973
    iput-object v1, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mSharedExperienceManager:Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;

    .line 976
    :cond_0
    iget-object v0, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mContentBroker:Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;

    if-eqz v0, :cond_1

    .line 977
    iget-object v0, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mContentBroker:Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;

    invoke-virtual {v0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;->disable()V

    .line 978
    iget-object v0, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mContentBroker:Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;

    invoke-virtual {v0, v1}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;->setDelegate(Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker$ContentBrokerDelegate;)V

    .line 979
    iget-object v0, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mContentBroker:Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;

    invoke-virtual {v0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;->deleteCachedContent()V

    .line 980
    iput-object v1, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mContentBroker:Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;

    .line 982
    :cond_1
    return-void
.end method

.method public getContentBroker()Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;
    .locals 1

    .prologue
    .line 1000
    iget-object v0, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mContentBroker:Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;

    return-object v0
.end method

.method public getCurrentActivityName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 884
    const/4 v0, 0x0

    return-object v0
.end method

.method protected getHelpState()I
    .locals 1

    .prologue
    .line 880
    iget v0, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mHelpState:I

    return v0
.end method

.method public getManifest()Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;
    .locals 1

    .prologue
    .line 990
    iget-object v0, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mSession:Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    if-nez v0, :cond_0

    .line 991
    const/4 v0, 0x0

    .line 992
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mSession:Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    invoke-virtual {v0}, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->getManifest()Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;

    move-result-object v0

    goto :goto_0
.end method

.method protected getManifestGenerator()Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;
    .locals 1

    .prologue
    .line 1008
    iget-object v0, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mManifestGenerator:Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;

    return-object v0
.end method

.method protected getParticipantsJoinedToActivity(Ljava/lang/String;)I
    .locals 9
    .param p1, "activityName"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    .line 1613
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->getSharedExperienceManager()Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;

    move-result-object v8

    if-nez v8, :cond_1

    .line 1640
    :cond_0
    return v5

    .line 1616
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->getSharedExperienceManager()Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->getHybridChannel()Lcom/samsung/groupcast/core/messaging/HybridChannel;

    move-result-object v8

    if-eqz v8, :cond_0

    .line 1620
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->getSharedExperienceManager()Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->getHybridChannel()Lcom/samsung/groupcast/core/messaging/HybridChannel;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/groupcast/core/messaging/HybridChannel;->getUsersName()Ljava/util/Collection;

    move-result-object v7

    .line 1623
    .local v7, "usersName":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    const/4 v5, 0x0

    .line 1624
    .local v5, "participants":I
    invoke-interface {v7}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .line 1625
    .local v6, "userNamesIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :cond_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 1626
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 1627
    .local v3, "name":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->getSharedExperienceManager()Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->getHybridChannel()Lcom/samsung/groupcast/core/messaging/HybridChannel;

    move-result-object v8

    invoke-virtual {v8, v3}, Lcom/samsung/groupcast/core/messaging/HybridChannel;->getParticipantInfo(Ljava/lang/String;)Lcom/samsung/groupcast/legacy/gp2/session/model/ParticipantInfo;

    move-result-object v4

    .line 1629
    .local v4, "participantInfo":Lcom/samsung/groupcast/legacy/gp2/session/model/ParticipantInfo;
    if-eqz v4, :cond_2

    .line 1630
    invoke-virtual {v4}, Lcom/samsung/groupcast/legacy/gp2/session/model/ParticipantInfo;->getJoinedActivities()Ljava/util/HashSet;

    move-result-object v0

    .line 1631
    .local v0, "activities":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 1632
    .local v1, "activitiesIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :cond_3
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_2

    .line 1633
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 1634
    .local v2, "activity":Ljava/lang/String;
    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 1635
    add-int/lit8 v5, v5, 0x1

    goto :goto_0
.end method

.method protected getParticipantsJoinedToGame()I
    .locals 9

    .prologue
    const/4 v5, 0x0

    .line 1644
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->getSharedExperienceManager()Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;

    move-result-object v8

    if-nez v8, :cond_1

    .line 1672
    :cond_0
    return v5

    .line 1647
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->getSharedExperienceManager()Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->getHybridChannel()Lcom/samsung/groupcast/core/messaging/HybridChannel;

    move-result-object v8

    if-eqz v8, :cond_0

    .line 1651
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->getSharedExperienceManager()Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->getHybridChannel()Lcom/samsung/groupcast/core/messaging/HybridChannel;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/groupcast/core/messaging/HybridChannel;->getUsersName()Ljava/util/Collection;

    move-result-object v7

    .line 1654
    .local v7, "usersName":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    const/4 v5, 0x0

    .line 1655
    .local v5, "participants":I
    invoke-interface {v7}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .line 1656
    .local v6, "userNamesIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :cond_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 1657
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 1658
    .local v3, "name":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->getSharedExperienceManager()Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->getHybridChannel()Lcom/samsung/groupcast/core/messaging/HybridChannel;

    move-result-object v8

    invoke-virtual {v8, v3}, Lcom/samsung/groupcast/core/messaging/HybridChannel;->getParticipantInfo(Ljava/lang/String;)Lcom/samsung/groupcast/legacy/gp2/session/model/ParticipantInfo;

    move-result-object v4

    .line 1660
    .local v4, "participantInfo":Lcom/samsung/groupcast/legacy/gp2/session/model/ParticipantInfo;
    if-eqz v4, :cond_2

    .line 1661
    invoke-virtual {v4}, Lcom/samsung/groupcast/legacy/gp2/session/model/ParticipantInfo;->getJoinedActivities()Ljava/util/HashSet;

    move-result-object v0

    .line 1662
    .local v0, "activities":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 1663
    .local v1, "activitiesIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :cond_3
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_2

    .line 1664
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 1665
    .local v2, "activity":Ljava/lang/String;
    const-string v8, "GAME_"

    invoke-virtual {v2, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 1666
    add-int/lit8 v5, v5, 0x1

    goto :goto_0
.end method

.method protected getParticipantsJoinedToGame(Ljava/lang/String;)I
    .locals 10
    .param p1, "pkgName"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    .line 1676
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->getSharedExperienceManager()Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;

    move-result-object v8

    if-nez v8, :cond_1

    .line 1707
    :cond_0
    return v5

    .line 1679
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->getSharedExperienceManager()Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->getHybridChannel()Lcom/samsung/groupcast/core/messaging/HybridChannel;

    move-result-object v8

    if-eqz v8, :cond_0

    .line 1683
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->getSharedExperienceManager()Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->getHybridChannel()Lcom/samsung/groupcast/core/messaging/HybridChannel;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/groupcast/core/messaging/HybridChannel;->getUsersName()Ljava/util/Collection;

    move-result-object v7

    .line 1686
    .local v7, "usersName":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    const/4 v5, 0x0

    .line 1687
    .local v5, "participants":I
    invoke-interface {v7}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .line 1688
    .local v6, "userNamesIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :cond_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 1689
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 1690
    .local v3, "name":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->getSharedExperienceManager()Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->getHybridChannel()Lcom/samsung/groupcast/core/messaging/HybridChannel;

    move-result-object v8

    invoke-virtual {v8, v3}, Lcom/samsung/groupcast/core/messaging/HybridChannel;->getParticipantInfo(Ljava/lang/String;)Lcom/samsung/groupcast/legacy/gp2/session/model/ParticipantInfo;

    move-result-object v4

    .line 1692
    .local v4, "participantInfo":Lcom/samsung/groupcast/legacy/gp2/session/model/ParticipantInfo;
    if-eqz v4, :cond_2

    .line 1693
    invoke-virtual {v4}, Lcom/samsung/groupcast/legacy/gp2/session/model/ParticipantInfo;->getJoinedActivities()Ljava/util/HashSet;

    move-result-object v0

    .line 1694
    .local v0, "activities":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 1695
    .local v1, "activitiesIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :cond_3
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_2

    .line 1696
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 1697
    .local v2, "activity":Ljava/lang/String;
    const-string v8, "GAME_"

    invoke-virtual {v2, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_3

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v8

    const-string v9, "GAME_"

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    if-le v8, v9, :cond_3

    const-string v8, "GAME_"

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    invoke-virtual {v2, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 1701
    add-int/lit8 v5, v5, 0x1

    goto :goto_0
.end method

.method public getSession()Lcom/samsung/groupcast/legacy/gp2/session/model/Session;
    .locals 1

    .prologue
    .line 996
    iget-object v0, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mSession:Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    return-object v0
.end method

.method public getSharedExperienceManager()Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;
    .locals 1

    .prologue
    .line 1004
    iget-object v0, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mSharedExperienceManager:Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;

    return-object v0
.end method

.method protected getUsersCount()I
    .locals 3

    .prologue
    .line 1730
    const/4 v0, 0x1

    .line 1731
    .local v0, "returnCount":I
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->getSharedExperienceManager()Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1732
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->getSharedExperienceManager()Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->getHybridChannel()Lcom/samsung/groupcast/core/messaging/HybridChannel;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 1733
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->getSharedExperienceManager()Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->getHybridChannel()Lcom/samsung/groupcast/core/messaging/HybridChannel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/groupcast/core/messaging/HybridChannel;->getUsersName()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->size()I

    move-result v1

    add-int/lit8 v0, v1, 0x1

    .line 1735
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "hybrid returnCount:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 1743
    :cond_0
    :goto_0
    return v0

    .line 1736
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->getSharedExperienceManager()Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->getLocalSessionChannel()Lcom/samsung/groupcast/core/messaging/LocalChannel;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1737
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->getSharedExperienceManager()Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->getLocalSessionChannel()Lcom/samsung/groupcast/core/messaging/LocalChannel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/groupcast/core/messaging/LocalChannel;->getUsersName()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->size()I

    move-result v1

    add-int/lit8 v0, v1, 0x1

    .line 1739
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "local returnCount:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public handleMessage(Landroid/os/Message;)Z
    .locals 1
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 930
    const/4 v0, 0x0

    return v0
.end method

.method protected isManifestGenerated()Z
    .locals 1

    .prologue
    .line 1012
    iget-object v0, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mManifestGenerator:Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected isSessionOpenned(Ljava/lang/String;)Z
    .locals 1
    .param p1, "activityName"    # Ljava/lang/String;

    .prologue
    .line 1712
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->getManifest()Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->getManifest()Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;->getCollection(Ljava/lang/String;)Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->getManifest()Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;->getCollection(Ljava/lang/String;)Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->getPageCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 1714
    const/4 v0, 0x1

    .line 1716
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected isUpdatingManifest()Z
    .locals 1

    .prologue
    .line 1723
    iget-object v0, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mManifestGenerator:Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;

    if-eqz v0, :cond_0

    .line 1724
    const/4 v0, 0x1

    .line 1725
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public joinActivity(Ljava/lang/String;)V
    .locals 2
    .param p1, "activity"    # Ljava/lang/String;

    .prologue
    .line 939
    new-instance v0, Landroid/content/Intent;

    const-string v1, "GROUPPLAY GAMER JOINED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 940
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "GAME NAME"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 941
    invoke-virtual {p0, v0}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 942
    return-void
.end method

.method public leftActivity(Ljava/lang/String;)V
    .locals 2
    .param p1, "activity"    # Ljava/lang/String;

    .prologue
    .line 945
    new-instance v0, Landroid/content/Intent;

    const-string v1, "GROUPPLAY GAMER LEFT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 946
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "GAME NAME"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 947
    invoke-virtual {p0, v0}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 948
    return-void
.end method

.method public onChangeContentDeliveryPriority(Ljava/lang/String;I)V
    .locals 3
    .param p1, "contentId"    # Ljava/lang/String;
    .param p2, "priority"    # I

    .prologue
    .line 1241
    iget-object v2, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mContentDeliveryLock:Ljava/lang/Object;

    monitor-enter v2

    .line 1242
    :try_start_0
    iget-object v1, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mContentRequests:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentRequest;

    .line 1243
    .local v0, "contentRequest":Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentRequest;
    if-eqz v0, :cond_0

    .line 1244
    invoke-virtual {v0, p2}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentRequest;->setPriority(I)V

    .line 1245
    :cond_0
    monitor-exit v2

    .line 1246
    return-void

    .line 1245
    .end local v0    # "contentRequest":Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentRequest;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public onContentAvailable(Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "broker"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;
    .param p2, "contentId"    # Ljava/lang/String;
    .param p3, "path"    # Ljava/lang/String;

    .prologue
    .line 1058
    iget-boolean v0, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mPaused:Z

    if-eqz v0, :cond_1

    .line 1059
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "paused - notification dropped"

    invoke-static {v0, v1}, Lcom/samsung/groupcast/application/Logger;->wx(Ljava/lang/Class;Ljava/lang/String;)V

    .line 1065
    :cond_0
    :goto_0
    return-void

    .line 1063
    :cond_1
    iget-object v0, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mSharedExperienceManager:Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;

    if-eqz v0, :cond_0

    .line 1064
    iget-object v0, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mSharedExperienceManager:Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;

    invoke-virtual {v0, p2}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->onContentAvailable(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v4, 0x1

    .line 130
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const-string v3, "onCreate"

    invoke-static {v2, v3}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;)V

    .line 131
    invoke-super {p0, p1}, Lcom/sec/android/band/BandActivity;->onCreate(Landroid/os/Bundle;)V

    .line 133
    iget-boolean v2, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->landscapeEnable:Z

    if-nez v2, :cond_0

    invoke-static {}, Lcom/samsung/groupcast/application/App;->isLandscapeEnabled()Z

    move-result v2

    if-nez v2, :cond_0

    .line 134
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->getRequestedOrientation()I

    move-result v2

    if-eq v4, v2, :cond_0

    .line 135
    invoke-virtual {p0, v4}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->setRequestedOrientation(I)V

    .line 139
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 140
    .local v0, "intent":Landroid/content/Intent;
    const-string v2, "HELP"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mHelpState:I

    .line 141
    iget v2, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mHelpState:I

    packed-switch v2, :pswitch_data_0

    .line 149
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "WRONG help state!!!"

    invoke-static {v2, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 150
    const-string v2, "WRONG HELP"

    invoke-static {v2}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 153
    :pswitch_0
    iput-object p0, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mContext:Landroid/content/Context;

    .line 155
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->getComponentName()Landroid/content/ComponentName;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v1

    .line 156
    .local v1, "str":Ljava/lang/String;
    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mCurrentActivityName:Ljava/lang/String;

    .line 158
    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiUtils;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mWifiUtils:Lcom/samsung/groupcast/net/wifi/WifiUtils;

    .line 159
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->getBandInterface()Lcom/sec/android/band/BandInterface;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mBandInterface:Lcom/sec/android/band/BandInterface;

    .line 160
    iget-object v2, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mBandInterface:Lcom/sec/android/band/BandInterface;

    new-instance v3, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity$1;

    invoke-direct {v3, p0}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity$1;-><init>(Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;)V

    invoke-virtual {v2, v3}, Lcom/sec/android/band/BandInterface;->setBandCallback(Lcom/sec/android/band/BandInterface$BandEventHandler;)V

    .line 646
    new-instance v2, Lcom/samsung/groupcast/GroupPlayManager;

    invoke-virtual {p0}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/samsung/groupcast/GroupPlayManager;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mGp:Lcom/samsung/groupcast/GroupPlayManager;

    .line 647
    new-instance v2, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity$2;

    invoke-direct {v2, p0}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity$2;-><init>(Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;)V

    iput-object v2, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mExternalEventHandler:Lcom/samsung/groupcast/GroupPlayManager$ExternalEventHandler;

    .line 708
    iget-object v2, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mGp:Lcom/samsung/groupcast/GroupPlayManager;

    iget-object v3, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mExternalEventHandler:Lcom/samsung/groupcast/GroupPlayManager$ExternalEventHandler;

    invoke-virtual {v2, v3}, Lcom/samsung/groupcast/GroupPlayManager;->extRegisterListener(Lcom/samsung/groupcast/GroupPlayManager$ExternalEventHandler;)V

    .line 710
    return-void

    .line 141
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public onCurrentPageChanged(Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;Lcom/samsung/groupcast/legacy/gp2/session/model/Session;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/session/model/CurrentPage;Lcom/samsung/groupcast/legacy/gp2/session/model/CurrentPage;)V
    .locals 0
    .param p1, "manager"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;
    .param p2, "session"    # Lcom/samsung/groupcast/legacy/gp2/session/model/Session;
    .param p3, "collectionName"    # Ljava/lang/String;
    .param p4, "previousPage"    # Lcom/samsung/groupcast/legacy/gp2/session/model/CurrentPage;
    .param p5, "page"    # Lcom/samsung/groupcast/legacy/gp2/session/model/CurrentPage;

    .prologue
    .line 1022
    return-void
.end method

.method protected onDestroy()V
    .locals 6

    .prologue
    .line 779
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    const-string v5, "onDestroy"

    invoke-static {v4, v5}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;)V

    .line 781
    iget v4, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mHelpState:I

    if-nez v4, :cond_1

    .line 782
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->isChangingConfigurations()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {p0}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->isFinishing()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 783
    :cond_0
    invoke-direct {p0}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->stopContentDelivery()V

    .line 784
    iget-object v4, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mLastSave:Landroid/os/Bundle;

    if-eqz v4, :cond_1

    .line 785
    iget-object v4, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mLastSave:Landroid/os/Bundle;

    const-string v5, "SESSION_STORE_ID"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 786
    .local v2, "sessionStoreId":I
    invoke-static {v2}, Lcom/samsung/groupcast/application/ObjectStore;->release(I)Ljava/lang/Object;

    .line 787
    iget-object v4, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mLastSave:Landroid/os/Bundle;

    const-string v5, "CONTENT_BROKER_STORE_ID"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 788
    .local v0, "contentBrokerId":I
    invoke-static {v0}, Lcom/samsung/groupcast/application/ObjectStore;->release(I)Ljava/lang/Object;

    .line 789
    iget-object v4, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mLastSave:Landroid/os/Bundle;

    const-string v5, "SHARED_EXPERIENCE_MANAGER_STORE_ID"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    .line 791
    .local v3, "sharedExterienceManagerId":I
    invoke-static {v3}, Lcom/samsung/groupcast/application/ObjectStore;->release(I)Ljava/lang/Object;

    .line 792
    iget-object v4, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mLastSave:Landroid/os/Bundle;

    const-string v5, "MANIFEST_GENERATOR_STORE_ID"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 793
    .local v1, "manifestGeneratorId":I
    invoke-static {v1}, Lcom/samsung/groupcast/application/ObjectStore;->release(I)Ljava/lang/Object;

    .line 798
    .end local v0    # "contentBrokerId":I
    .end local v1    # "manifestGeneratorId":I
    .end local v2    # "sessionStoreId":I
    .end local v3    # "sharedExterienceManagerId":I
    :cond_1
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 799
    invoke-super {p0}, Lcom/sec/android/band/BandActivity;->onDestroy()V

    .line 800
    return-void
.end method

.method public onManifestChanged(Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;Lcom/samsung/groupcast/legacy/gp2/session/model/Session;Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;)V
    .locals 2
    .param p1, "manager"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;
    .param p2, "session"    # Lcom/samsung/groupcast/legacy/gp2/session/model/Session;
    .param p3, "previousManifest"    # Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;
    .param p4, "manifest"    # Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;

    .prologue
    .line 1034
    iget-boolean v0, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mPaused:Z

    if-eqz v0, :cond_0

    .line 1035
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "paused - notification dropped"

    invoke-static {v0, v1}, Lcom/samsung/groupcast/application/Logger;->wx(Ljava/lang/Class;Ljava/lang/String;)V

    .line 1043
    :goto_0
    return-void

    .line 1039
    :cond_0
    iget-object v0, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mContentBroker:Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;

    if-eqz v0, :cond_1

    .line 1040
    iget-object v0, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mContentBroker:Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;

    invoke-virtual {v0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;->getContentMap()Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentMap;

    move-result-object v0

    invoke-virtual {v0, p4}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentMap;->setManifest(Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;)V

    .line 1042
    :cond_1
    invoke-direct {p0}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->onStartCollectionsPrefetching()V

    goto :goto_0
.end method

.method protected onManifestCreationFailed()V
    .locals 0

    .prologue
    .line 1605
    return-void
.end method

.method public onManifestGenerationComplete(Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;)V
    .locals 7
    .param p1, "generator"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;

    .prologue
    .line 1525
    iget-boolean v1, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mPaused:Z

    if-eqz v1, :cond_0

    .line 1532
    :goto_0
    return-void

    .line 1528
    :cond_0
    iget-object v1, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mManifestGenerator:Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;

    invoke-virtual {v1}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->getManifest()Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;

    move-result-object v0

    .line 1529
    .local v0, "newManifest":Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-string v2, "onManifestGenerationComplete"

    const/4 v3, 0x0

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const-string v6, "manifest"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    aput-object v0, v4, v5

    invoke-static {v1, v2, v3, v4}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 1531
    invoke-virtual {p0, v0}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->storeNewManifest(Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;)V

    goto :goto_0
.end method

.method public onManifestGenerationProgress(Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;)V
    .locals 0
    .param p1, "generator"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;

    .prologue
    .line 1521
    return-void
.end method

.method protected onManifestUpdated(Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;)V
    .locals 0
    .param p1, "manifestGenerator"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;

    .prologue
    .line 1603
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 0
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 889
    invoke-direct {p0, p1}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->processNfcIntent(Landroid/content/Intent;)V

    .line 890
    return-void
.end method

.method public onPageCommand(Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageCommandV1;Lcom/samsung/groupcast/legacy/gp2/session/model/PageState;)V
    .locals 0
    .param p1, "manager"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;
    .param p2, "contentId"    # Ljava/lang/String;
    .param p3, "command"    # Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageCommandV1;
    .param p4, "state"    # Lcom/samsung/groupcast/legacy/gp2/session/model/PageState;

    .prologue
    .line 1051
    return-void
.end method

.method public onPageEdited(Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;Lcom/samsung/groupcast/legacy/gp2/session/model/Session;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/session/model/PageStateNode;Lcom/samsung/groupcast/legacy/gp2/session/model/PageEditNode;)Z
    .locals 1
    .param p1, "manager"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;
    .param p2, "session"    # Lcom/samsung/groupcast/legacy/gp2/session/model/Session;
    .param p3, "contentId"    # Ljava/lang/String;
    .param p4, "pageState"    # Lcom/samsung/groupcast/legacy/gp2/session/model/PageStateNode;
    .param p5, "pageEdit"    # Lcom/samsung/groupcast/legacy/gp2/session/model/PageEditNode;

    .prologue
    .line 1027
    const/4 v0, 0x0

    return v0
.end method

.method protected onPause()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 744
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "onPause"

    invoke-static {v0, v1}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;)V

    .line 745
    const-string v0, "GroupPlayBand"

    const-string v1, "SharedExperience onPause"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 747
    iget v0, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mHelpState:I

    if-nez v0, :cond_2

    .line 748
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mPaused:Z

    .line 750
    invoke-direct {p0}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->onStopCollectionsPrefetching()V

    .line 752
    iget-object v0, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mManifestGenerator:Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;

    if-eqz v0, :cond_0

    .line 753
    iget-object v0, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mManifestGenerator:Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;

    invoke-virtual {v0, v2}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->setDelegate(Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator$ManifestGeneratorDelegate;)V

    .line 757
    :cond_0
    iget-object v0, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mStoreManifestRequest:Lcom/samsung/groupcast/legacy/gp2/session/storage/StoreManifestRequest;

    if-eqz v0, :cond_1

    .line 758
    iget-object v0, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mStoreManifestRequest:Lcom/samsung/groupcast/legacy/gp2/session/storage/StoreManifestRequest;

    invoke-virtual {v0}, Lcom/samsung/groupcast/legacy/gp2/session/storage/StoreManifestRequest;->invalidate()V

    .line 759
    iput-object v2, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mStoreManifestRequest:Lcom/samsung/groupcast/legacy/gp2/session/storage/StoreManifestRequest;

    .line 762
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mContentBroker:Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;

    if-eqz v0, :cond_2

    .line 763
    iget-object v0, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mContentBroker:Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;

    invoke-virtual {v0, v2}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;->setDelegate(Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker$ContentBrokerDelegate;)V

    .line 767
    :cond_2
    invoke-super {p0}, Lcom/sec/android/band/BandActivity;->onPause()V

    .line 769
    return-void
.end method

.method protected onRefreshSharedExperienceObjects(Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;)V
    .locals 0
    .param p1, "manifestGenerator"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;

    .prologue
    .line 1601
    return-void
.end method

.method public onRegisterForContentDelivery(Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;Ljava/lang/String;)V
    .locals 0
    .param p1, "manager"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;
    .param p2, "contentId"    # Ljava/lang/String;

    .prologue
    .line 1251
    invoke-virtual {p0, p2, p1}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->onRegisterForContentDelivery(Ljava/lang/String;Lcom/samsung/groupcast/application/viewer/ContentDeliveryListener;)V

    .line 1252
    return-void
.end method

.method public onRegisterForContentDelivery(Ljava/lang/String;Lcom/samsung/groupcast/application/viewer/ContentDeliveryListener;)V
    .locals 5
    .param p1, "contentId"    # Ljava/lang/String;
    .param p2, "listener"    # Lcom/samsung/groupcast/application/viewer/ContentDeliveryListener;

    .prologue
    .line 1188
    iget-object v4, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mContentDeliveryLock:Ljava/lang/Object;

    monitor-enter v4

    .line 1189
    :try_start_0
    iget-object v3, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mContentDeliveryListeners:Ljava/util/HashMap;

    invoke-virtual {v3, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/LinkedList;

    .line 1190
    .local v1, "list":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/samsung/groupcast/application/viewer/ContentDeliveryListener;>;"
    if-nez v1, :cond_0

    .line 1191
    new-instance v1, Ljava/util/LinkedList;

    .end local v1    # "list":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/samsung/groupcast/application/viewer/ContentDeliveryListener;>;"
    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    .line 1192
    .restart local v1    # "list":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/samsung/groupcast/application/viewer/ContentDeliveryListener;>;"
    iget-object v3, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mContentDeliveryListeners:Ljava/util/HashMap;

    invoke-virtual {v3, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1194
    :cond_0
    invoke-virtual {v1, p2}, Ljava/util/LinkedList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 1195
    invoke-virtual {v1, p2}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 1197
    :cond_1
    iget-object v3, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mContentRequests:Ljava/util/HashMap;

    invoke-virtual {v3, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentRequest;

    .line 1204
    .local v0, "contentRequest":Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentRequest;
    if-nez v0, :cond_3

    .line 1205
    iget-object v3, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mContentBroker:Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;

    if-eqz v3, :cond_2

    .line 1206
    iget-object v3, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mContentBroker:Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;

    invoke-virtual {v3, p1}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;->createContentRequest(Ljava/lang/String;)Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentRequest;

    move-result-object v0

    .line 1207
    if-eqz v0, :cond_2

    .line 1208
    iget-object v3, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mContentListener:Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentListener;

    invoke-virtual {v0, v3}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentRequest;->addListener(Lcom/samsung/groupcast/misc/requests/RequestListener;)V

    .line 1209
    iget-object v3, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mContentRequests:Ljava/util/HashMap;

    invoke-virtual {v3, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1210
    invoke-virtual {v0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentRequest;->start()V

    .line 1213
    :cond_2
    monitor-exit v4

    .line 1225
    :goto_0
    return-void

    .line 1215
    :cond_3
    invoke-virtual {v0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentRequest;->isCompleted()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 1216
    iget-object v3, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mContentBroker:Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;

    if-eqz v3, :cond_4

    .line 1217
    iget-object v3, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mContentBroker:Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;

    invoke-virtual {v3}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;->getContentMap()Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentMap;

    move-result-object v3

    invoke-virtual {v3, p1}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentMap;->getContentPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1218
    .local v2, "path":Ljava/lang/String;
    if-eqz v2, :cond_4

    .line 1219
    sget-object v3, Lcom/samsung/groupcast/misc/requests/Result;->SUCCESS:Lcom/samsung/groupcast/misc/requests/Result;

    invoke-interface {p2, p1, v3, v2}, Lcom/samsung/groupcast/application/viewer/ContentDeliveryListener;->onContentDeliveryComplete(Ljava/lang/String;Lcom/samsung/groupcast/misc/requests/Result;Ljava/lang/String;)V

    .line 1224
    .end local v2    # "path":Ljava/lang/String;
    :cond_4
    monitor-exit v4

    goto :goto_0

    .end local v0    # "contentRequest":Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentRequest;
    .end local v1    # "list":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/samsung/groupcast/application/viewer/ContentDeliveryListener;>;"
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 7
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v6, 0x1

    .line 837
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    const-string v5, "onRestoreInstanceState"

    invoke-static {v4, v5}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;)V

    .line 838
    invoke-super {p0, p1}, Lcom/sec/android/band/BandActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 841
    const-string v4, "OBJECT_STORE_HASH"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 842
    const-string v4, "GroupCast session closed"

    invoke-static {p0, v4, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    .line 843
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->finish()V

    .line 845
    :cond_0
    const-string v4, "OBJECT_STORE_HASH"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    invoke-static {v4}, Lcom/samsung/groupcast/application/ObjectStore;->isValid(I)Z

    move-result v4

    if-nez v4, :cond_1

    .line 846
    const-string v4, "GroupCast session closed"

    invoke-static {p0, v4, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    .line 847
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->finish()V

    .line 850
    :cond_1
    const-string v4, "SESSION_STORE_ID"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 851
    const-string v4, "SESSION_STORE_ID"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 852
    .local v2, "sessionStoreId":I
    invoke-static {v2}, Lcom/samsung/groupcast/application/ObjectStore;->release(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    iput-object v4, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mSession:Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    .line 855
    .end local v2    # "sessionStoreId":I
    :cond_2
    const-string v4, "CONTENT_BROKER_STORE_ID"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 856
    const-string v4, "CONTENT_BROKER_STORE_ID"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 857
    .local v0, "contentBrokerId":I
    invoke-static {v0}, Lcom/samsung/groupcast/application/ObjectStore;->release(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;

    iput-object v4, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mContentBroker:Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;

    .line 860
    .end local v0    # "contentBrokerId":I
    :cond_3
    const-string v4, "SHARED_EXPERIENCE_MANAGER_STORE_ID"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 861
    const-string v4, "SHARED_EXPERIENCE_MANAGER_STORE_ID"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    .line 863
    .local v3, "sharedExterienceManagerId":I
    invoke-static {v3}, Lcom/samsung/groupcast/application/ObjectStore;->release(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;

    iput-object v4, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mSharedExperienceManager:Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;

    .line 867
    .end local v3    # "sharedExterienceManagerId":I
    :cond_4
    const-string v4, "MANIFEST_GENERATOR_STORE_ID"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 868
    const-string v4, "MANIFEST_GENERATOR_STORE_ID"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 869
    .local v1, "manifestGeneratorId":I
    invoke-static {v1}, Lcom/samsung/groupcast/application/ObjectStore;->release(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;

    iput-object v4, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mManifestGenerator:Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;

    .line 872
    .end local v1    # "manifestGeneratorId":I
    :cond_5
    const-string v4, "HELP"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 873
    const-string v4, "HELP"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    iput v4, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mHelpState:I

    .line 876
    :cond_6
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mLastSave:Landroid/os/Bundle;

    .line 877
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 714
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "onResume"

    invoke-static {v0, v1}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;)V

    .line 715
    const-string v0, "GroupPlayBand"

    const-string v1, "SharedExperience onResume"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 716
    invoke-super {p0}, Lcom/sec/android/band/BandActivity;->onResume()V

    .line 719
    iget-object v0, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mCurrentActivityName:Ljava/lang/String;

    invoke-static {v0}, Lcom/samsung/groupcast/application/App;->setTopActivity(Ljava/lang/String;)V

    .line 722
    iget v0, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mHelpState:I

    if-nez v0, :cond_1

    .line 723
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mPaused:Z

    .line 725
    iget-object v0, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mManifestGenerator:Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;

    if-eqz v0, :cond_0

    .line 726
    iget-object v0, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mManifestGenerator:Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;

    invoke-virtual {v0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->isCompleted()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 727
    iget-object v0, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mManifestGenerator:Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;

    invoke-virtual {p0, v0}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->onManifestGenerationComplete(Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;)V

    .line 736
    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->onStartCollectionsPrefetching()V

    .line 740
    :cond_1
    return-void

    .line 729
    :cond_2
    iget-object v0, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mManifestGenerator:Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;

    invoke-virtual {v0, p0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->setDelegate(Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator$ManifestGeneratorDelegate;)V

    .line 730
    iget-object v0, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mManifestGenerator:Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;

    invoke-virtual {v0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->start()V

    goto :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 804
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    const-string v5, "onSaveInstanceState"

    invoke-static {v4, v5}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;)V

    .line 805
    invoke-super {p0, p1}, Lcom/sec/android/band/BandActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 807
    iget-object v4, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mLastSave:Landroid/os/Bundle;

    if-eqz v4, :cond_0

    .line 808
    iget-object v4, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mLastSave:Landroid/os/Bundle;

    const-string v5, "SESSION_STORE_ID"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 809
    .local v2, "sessionStoreId":I
    invoke-static {v2}, Lcom/samsung/groupcast/application/ObjectStore;->release(I)Ljava/lang/Object;

    .line 810
    iget-object v4, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mLastSave:Landroid/os/Bundle;

    const-string v5, "CONTENT_BROKER_STORE_ID"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 811
    .local v0, "contentBrokerId":I
    invoke-static {v0}, Lcom/samsung/groupcast/application/ObjectStore;->release(I)Ljava/lang/Object;

    .line 812
    iget-object v4, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mLastSave:Landroid/os/Bundle;

    const-string v5, "SHARED_EXPERIENCE_MANAGER_STORE_ID"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    .line 814
    .local v3, "sharedExterienceManagerId":I
    invoke-static {v3}, Lcom/samsung/groupcast/application/ObjectStore;->release(I)Ljava/lang/Object;

    .line 815
    iget-object v4, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mLastSave:Landroid/os/Bundle;

    const-string v5, "MANIFEST_GENERATOR_STORE_ID"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 816
    .local v1, "manifestGeneratorId":I
    invoke-static {v1}, Lcom/samsung/groupcast/application/ObjectStore;->release(I)Ljava/lang/Object;

    .line 819
    .end local v0    # "contentBrokerId":I
    .end local v1    # "manifestGeneratorId":I
    .end local v2    # "sessionStoreId":I
    .end local v3    # "sharedExterienceManagerId":I
    :cond_0
    const-string v4, "OBJECT_STORE_HASH"

    invoke-static {}, Lcom/samsung/groupcast/application/ObjectStore;->getHash()I

    move-result v5

    invoke-virtual {p1, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 821
    iget-object v4, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mSession:Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    if-eqz v4, :cond_1

    .line 822
    const-string v4, "SESSION_STORE_ID"

    iget-object v5, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mSession:Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    invoke-static {v5}, Lcom/samsung/groupcast/application/ObjectStore;->retain(Ljava/lang/Object;)I

    move-result v5

    invoke-virtual {p1, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 823
    :cond_1
    iget-object v4, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mContentBroker:Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;

    if-eqz v4, :cond_2

    .line 824
    const-string v4, "CONTENT_BROKER_STORE_ID"

    iget-object v5, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mContentBroker:Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;

    invoke-static {v5}, Lcom/samsung/groupcast/application/ObjectStore;->retain(Ljava/lang/Object;)I

    move-result v5

    invoke-virtual {p1, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 825
    :cond_2
    iget-object v4, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mSharedExperienceManager:Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;

    if-eqz v4, :cond_3

    .line 826
    const-string v4, "SHARED_EXPERIENCE_MANAGER_STORE_ID"

    iget-object v5, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mSharedExperienceManager:Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;

    invoke-static {v5}, Lcom/samsung/groupcast/application/ObjectStore;->retain(Ljava/lang/Object;)I

    move-result v5

    invoke-virtual {p1, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 828
    :cond_3
    iget-object v4, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mManifestGenerator:Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;

    if-eqz v4, :cond_4

    .line 829
    const-string v4, "MANIFEST_GENERATOR_STORE_ID"

    iget-object v5, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mManifestGenerator:Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;

    invoke-static {v5}, Lcom/samsung/groupcast/application/ObjectStore;->retain(Ljava/lang/Object;)I

    move-result v5

    invoke-virtual {p1, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 831
    :cond_4
    const-string v4, "HELP"

    iget v5, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mHelpState:I

    invoke-virtual {p1, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 832
    iput-object p1, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mLastSave:Landroid/os/Bundle;

    .line 833
    return-void
.end method

.method protected onStop()V
    .locals 2

    .prologue
    .line 773
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "onPause"

    invoke-static {v0, v1}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;)V

    .line 774
    invoke-super {p0}, Lcom/sec/android/band/BandActivity;->onStop()V

    .line 775
    return-void
.end method

.method protected onStoreManifestForAddedFilesCompleted()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 1554
    iget-boolean v6, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mPaused:Z

    if-eqz v6, :cond_1

    .line 1599
    :cond_0
    :goto_0
    return-void

    .line 1558
    :cond_1
    iget-object v6, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mSession:Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    if-eqz v6, :cond_0

    .line 1562
    iget-object v6, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mManifestGenerator:Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;

    invoke-virtual {p0, v6}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->onRefreshSharedExperienceObjects(Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;)V

    .line 1564
    iget-object v6, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mManifestGenerator:Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;

    if-eqz v6, :cond_6

    .line 1565
    iget-object v6, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mContentBroker:Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;

    if-eqz v6, :cond_2

    .line 1566
    iget-object v6, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mManifestGenerator:Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;

    invoke-virtual {v6}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->getMappedPaths()Ljava/util/Map;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 1567
    .local v2, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 1568
    .local v4, "id":Ljava/lang/String;
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 1569
    .local v5, "path":Ljava/lang/String;
    iget-object v6, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mContentBroker:Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;

    invoke-virtual {v6}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;->getContentMap()Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentMap;

    move-result-object v6

    invoke-virtual {v6, v4, v5}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentMap;->addItemMapping(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 1572
    .end local v2    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v4    # "id":Ljava/lang/String;
    .end local v5    # "path":Ljava/lang/String;
    :cond_2
    iget-object v6, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mSharedExperienceManager:Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;

    if-eqz v6, :cond_6

    .line 1573
    iget-object v6, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mSharedExperienceManager:Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;

    iget-object v7, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mManifestGenerator:Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;

    invoke-virtual {v7}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->getManifest()Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->setManifest(Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;)V

    .line 1574
    iget-object v6, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mManifestGenerator:Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;

    invoke-virtual {v6}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->getInsertionIndex()I

    move-result v1

    .line 1576
    .local v1, "currentPage":I
    if-gez v1, :cond_3

    .line 1577
    const/4 v1, 0x0

    .line 1579
    :cond_3
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->getManifest()Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;

    move-result-object v6

    iget-object v7, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mManifestGenerator:Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;

    invoke-virtual {v7}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->getCollectionName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;->getCollection(Ljava/lang/String;)Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;

    move-result-object v6

    if-eqz v6, :cond_6

    .line 1580
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->getManifest()Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;

    move-result-object v6

    iget-object v7, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mManifestGenerator:Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;

    invoke-virtual {v7}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->getCollectionName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;->getCollection(Ljava/lang/String;)Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->getPageCount()I

    move-result v0

    .line 1582
    .local v0, "collectionSize":I
    if-nez v0, :cond_4

    .line 1583
    const/4 v1, 0x0

    .line 1584
    :cond_4
    if-lt v1, v0, :cond_5

    .line 1585
    add-int/lit8 v1, v0, -0x1

    .line 1586
    :cond_5
    iget-object v6, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mSharedExperienceManager:Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;

    iget-object v7, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mManifestGenerator:Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;

    invoke-virtual {v7}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->getCollectionName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7, v1}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->setCurrentPage(Ljava/lang/String;I)V

    .line 1592
    .end local v0    # "collectionSize":I
    .end local v1    # "currentPage":I
    :cond_6
    iget-object v6, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mContentBroker:Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;

    if-eqz v6, :cond_7

    .line 1593
    invoke-direct {p0}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->onStartCollectionsPrefetching()V

    .line 1595
    :cond_7
    iget-object v6, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mManifestGenerator:Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;

    invoke-virtual {p0, v6}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->onManifestUpdated(Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;)V

    .line 1597
    iput-object v8, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mManifestGenerator:Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;

    .line 1598
    iput-object v8, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mStoreManifestRequest:Lcom/samsung/groupcast/legacy/gp2/session/storage/StoreManifestRequest;

    goto/16 :goto_0
.end method

.method public onUnregisterFromContentDelivery(Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;Ljava/lang/String;)V
    .locals 0
    .param p1, "manager"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;
    .param p2, "contentId"    # Ljava/lang/String;

    .prologue
    .line 1257
    invoke-virtual {p0, p2, p1}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->onUnregisterFromContentDelivery(Ljava/lang/String;Lcom/samsung/groupcast/application/viewer/ContentDeliveryListener;)V

    .line 1258
    return-void
.end method

.method public onUnregisterFromContentDelivery(Ljava/lang/String;Lcom/samsung/groupcast/application/viewer/ContentDeliveryListener;)V
    .locals 3
    .param p1, "contentId"    # Ljava/lang/String;
    .param p2, "listener"    # Lcom/samsung/groupcast/application/viewer/ContentDeliveryListener;

    .prologue
    .line 1229
    iget-object v2, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mContentDeliveryLock:Ljava/lang/Object;

    monitor-enter v2

    .line 1230
    :try_start_0
    iget-object v1, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mContentDeliveryListeners:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/LinkedList;

    .line 1231
    .local v0, "list":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/samsung/groupcast/application/viewer/ContentDeliveryListener;>;"
    if-eqz v0, :cond_0

    .line 1232
    invoke-virtual {v0, p2}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    .line 1233
    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1234
    iget-object v1, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mContentDeliveryListeners:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1236
    :cond_0
    monitor-exit v2

    .line 1237
    return-void

    .line 1236
    .end local v0    # "list":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/samsung/groupcast/application/viewer/ContentDeliveryListener;>;"
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public onUserJoined(Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;Lcom/samsung/groupcast/legacy/gp2/session/model/Session;Ljava/lang/String;)V
    .locals 0
    .param p1, "sharedExperienceManager"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;
    .param p2, "mSession"    # Lcom/samsung/groupcast/legacy/gp2/session/model/Session;
    .param p3, "contentId"    # Ljava/lang/String;

    .prologue
    .line 1047
    return-void
.end method

.method protected setContentBrokerDelegate(Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker$ContentBrokerDelegate;)V
    .locals 1
    .param p1, "delegate"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker$ContentBrokerDelegate;

    .prologue
    .line 985
    iget-object v0, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mContentBroker:Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;

    if-eqz v0, :cond_0

    .line 986
    iget-object v0, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mContentBroker:Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;

    invoke-virtual {v0, p1}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;->setDelegate(Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker$ContentBrokerDelegate;)V

    .line 987
    :cond_0
    return-void
.end method

.method protected setShareExperienceObjects(Lcom/samsung/groupcast/legacy/gp2/session/model/Session;Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;)V
    .locals 1
    .param p1, "session"    # Lcom/samsung/groupcast/legacy/gp2/session/model/Session;
    .param p2, "contentBroker"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;
    .param p3, "shareExperienceManager"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;

    .prologue
    .line 954
    const-string v0, "session"

    invoke-static {v0, p1}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 955
    iput-object p1, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mSession:Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    .line 956
    if-eqz p2, :cond_0

    .line 957
    iput-object p2, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mContentBroker:Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;

    .line 958
    iget-object v0, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mContentBroker:Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;

    invoke-virtual {v0, p0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;->setDelegate(Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker$ContentBrokerDelegate;)V

    .line 959
    iget-object v0, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mContentBroker:Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;

    invoke-virtual {v0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;->enable()V

    .line 961
    :cond_0
    if-eqz p3, :cond_1

    .line 962
    iput-object p3, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mSharedExperienceManager:Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;

    .line 963
    iget-object v0, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mSharedExperienceManager:Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;

    invoke-virtual {v0, p0, p0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->setDelegate(Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$SharedExperienceManagerDelegate;Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$SharedExperienceManagerDelegate;)V

    .line 964
    iget-object v0, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mSharedExperienceManager:Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;

    invoke-virtual {v0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->enable()V

    .line 967
    :cond_1
    return-void
.end method

.method protected startCollageManifestClear(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 1
    .param p1, "srcCollectionName"    # Ljava/lang/String;
    .param p2, "collectionName"    # Ljava/lang/String;
    .param p3, "bShowProgress"    # Z

    .prologue
    .line 1502
    iget-object v0, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mManifestGenerator:Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;

    if-eqz v0, :cond_0

    .line 1516
    :goto_0
    return-void

    .line 1506
    :cond_0
    invoke-static {p0, p1, p2, p3}, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;->collagePageManifestGeneratorAsCleared(Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;Ljava/lang/String;Ljava/lang/String;Z)Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mManifestGenerator:Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;

    .line 1510
    iget-object v0, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mManifestGenerator:Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;

    if-nez v0, :cond_1

    .line 1511
    const-string v0, "mManifestGenerator is null"

    invoke-static {v0}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 1514
    :cond_1
    iget-object v0, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mManifestGenerator:Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;

    invoke-virtual {v0, p0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->setDelegate(Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator$ManifestGeneratorDelegate;)V

    .line 1515
    iget-object v0, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mManifestGenerator:Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;

    invoke-virtual {v0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->start()V

    goto :goto_0
.end method

.method protected startCollageManifestUpdate(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 1
    .param p1, "srcCollectionName"    # Ljava/lang/String;
    .param p2, "collectionName"    # Ljava/lang/String;
    .param p3, "bShowProgress"    # Z

    .prologue
    .line 1483
    iget-object v0, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mManifestGenerator:Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;

    if-eqz v0, :cond_0

    .line 1496
    :goto_0
    return-void

    .line 1486
    :cond_0
    invoke-static {p0, p1, p2, p3}, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;->collagePageManifestGeneratorAsNew(Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;Ljava/lang/String;Ljava/lang/String;Z)Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mManifestGenerator:Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;

    .line 1490
    iget-object v0, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mManifestGenerator:Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;

    if-nez v0, :cond_1

    .line 1491
    const-string v0, "mManifestGenerator is null"

    invoke-static {v0}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 1494
    :cond_1
    iget-object v0, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mManifestGenerator:Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;

    invoke-virtual {v0, p0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->setDelegate(Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator$ManifestGeneratorDelegate;)V

    .line 1495
    iget-object v0, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mManifestGenerator:Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;

    invoke-virtual {v0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->start()V

    goto :goto_0
.end method

.method protected startManifestUpdate(Ljava/lang/String;ILjava/util/ArrayList;)V
    .locals 7
    .param p1, "collectionName"    # Ljava/lang/String;
    .param p2, "insertionIndex"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1416
    .local p3, "uris":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-string v2, "startManifestUpdate"

    const/4 v3, 0x0

    const/4 v4, 0x6

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const-string v6, "collectionName"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    aput-object p1, v4, v5

    const/4 v5, 0x2

    const-string v6, "index"

    aput-object v6, v4, v5

    const/4 v5, 0x3

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x4

    const-string v6, "uris"

    aput-object v6, v4, v5

    const/4 v5, 0x5

    aput-object p3, v4, v5

    invoke-static {v1, v2, v3, v4}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 1419
    iget-object v1, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mManifestGenerator:Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;

    if-eqz v1, :cond_0

    .line 1447
    :goto_0
    return-void

    .line 1426
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->getManifest()Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;

    move-result-object v1

    if-nez v1, :cond_1

    .line 1427
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Manifest should not be null here: session="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->getSession()Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " sharedExperienceManager="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->getSharedExperienceManager()Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1433
    :cond_1
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-virtual {p0}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->getManifest()Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;

    move-result-object v2

    invoke-static {v1, p3, v2, p2, p1}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->manifestGeneratorForInsertingPages(Landroid/content/ContentResolver;Ljava/util/List;Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;ILjava/lang/String;)Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mManifestGenerator:Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1441
    iget-object v1, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mManifestGenerator:Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;

    if-nez v1, :cond_2

    .line 1442
    const-string v1, "mManifestGenerator is null"

    invoke-static {v1}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 1436
    :catch_0
    move-exception v0

    .line 1437
    .local v0, "e":Ljava/lang/RuntimeException;
    invoke-virtual {v0}, Ljava/lang/RuntimeException;->printStackTrace()V

    goto :goto_0

    .line 1445
    .end local v0    # "e":Ljava/lang/RuntimeException;
    :cond_2
    iget-object v1, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mManifestGenerator:Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;

    invoke-virtual {v1, p0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->setDelegate(Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator$ManifestGeneratorDelegate;)V

    .line 1446
    iget-object v1, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mManifestGenerator:Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;

    invoke-virtual {v1}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->start()V

    goto :goto_0
.end method

.method protected startManifestUpdateForRemove(Ljava/lang/String;I)V
    .locals 1
    .param p1, "collectionName"    # Ljava/lang/String;
    .param p2, "iCurrentPage"    # I

    .prologue
    .line 1450
    iget-object v0, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mManifestGenerator:Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;

    if-eqz v0, :cond_1

    .line 1466
    :cond_0
    :goto_0
    return-void

    .line 1453
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->getManifest()Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->getSession()Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->getManifest()Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;->getCollection(Ljava/lang/String;)Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1457
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->getManifest()Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;

    move-result-object v0

    invoke-static {v0, p1, p2}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->manifestGeneratorForDelete(Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;Ljava/lang/String;I)Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mManifestGenerator:Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;

    .line 1460
    iget-object v0, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mManifestGenerator:Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;

    if-nez v0, :cond_2

    .line 1461
    const-string v0, "mManifestGenerator is null"

    invoke-static {v0}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 1464
    :cond_2
    iget-object v0, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mManifestGenerator:Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;

    invoke-virtual {v0, p0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->setDelegate(Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator$ManifestGeneratorDelegate;)V

    .line 1465
    iget-object v0, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mManifestGenerator:Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;

    invoke-virtual {v0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->start()V

    goto :goto_0
.end method

.method protected startNewManifestGeneration(Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 6
    .param p1, "collectionName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1396
    .local p2, "uris":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "startManifestUpdate"

    const/4 v2, 0x0

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string v5, "collectionName"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    aput-object p1, v3, v4

    const/4 v4, 0x2

    const-string v5, "uris"

    aput-object v5, v3, v4

    const/4 v4, 0x3

    aput-object p2, v3, v4

    invoke-static {v0, v1, v2, v3}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 1399
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->getManifest()Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mManifestGenerator:Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;

    if-eqz v0, :cond_1

    .line 1412
    :cond_0
    :goto_0
    return-void

    .line 1402
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v0, p2, p1}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->manifestGeneratorForFreshManifest(Landroid/content/ContentResolver;Ljava/util/List;Ljava/lang/String;)Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mManifestGenerator:Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;

    .line 1405
    iget-object v0, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mManifestGenerator:Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;

    if-nez v0, :cond_2

    .line 1406
    const-string v0, "mManifestGenerator is null"

    invoke-static {v0}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 1410
    :cond_2
    iget-object v0, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mManifestGenerator:Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;

    invoke-virtual {v0, p0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->setDelegate(Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator$ManifestGeneratorDelegate;)V

    .line 1411
    iget-object v0, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mManifestGenerator:Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;

    invoke-virtual {v0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->start()V

    goto :goto_0
.end method

.method protected stopManifestUpdate()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1470
    iget-object v0, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mManifestGenerator:Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;

    if-eqz v0, :cond_0

    .line 1471
    iget-object v0, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mManifestGenerator:Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;

    invoke-virtual {v0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->stop()V

    .line 1472
    iput-object v1, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mManifestGenerator:Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;

    .line 1474
    :cond_0
    iget-object v0, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mStoreManifestRequest:Lcom/samsung/groupcast/legacy/gp2/session/storage/StoreManifestRequest;

    if-eqz v0, :cond_1

    .line 1475
    iget-object v0, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mStoreManifestRequest:Lcom/samsung/groupcast/legacy/gp2/session/storage/StoreManifestRequest;

    invoke-virtual {v0}, Lcom/samsung/groupcast/legacy/gp2/session/storage/StoreManifestRequest;->invalidate()V

    .line 1476
    iput-object v1, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mStoreManifestRequest:Lcom/samsung/groupcast/legacy/gp2/session/storage/StoreManifestRequest;

    .line 1478
    :cond_1
    return-void
.end method

.method protected storeNewManifest(Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;)V
    .locals 3
    .param p1, "newManifest"    # Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;

    .prologue
    .line 1535
    new-instance v0, Lcom/samsung/groupcast/legacy/gp2/session/storage/StorageAgent;

    invoke-virtual {p0}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->getSession()Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->getSessionId()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/groupcast/legacy/gp2/session/storage/StorageAgent;-><init>(Ljava/lang/String;)V

    .line 1536
    .local v0, "storage":Lcom/samsung/groupcast/legacy/gp2/session/storage/StorageAgent;
    invoke-virtual {v0, p1}, Lcom/samsung/groupcast/legacy/gp2/session/storage/StorageAgent;->createStoreManifestRequest(Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;)Lcom/samsung/groupcast/legacy/gp2/session/storage/StoreManifestRequest;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mStoreManifestRequest:Lcom/samsung/groupcast/legacy/gp2/session/storage/StoreManifestRequest;

    .line 1537
    iget-object v1, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mStoreManifestRequest:Lcom/samsung/groupcast/legacy/gp2/session/storage/StoreManifestRequest;

    new-instance v2, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity$5;

    invoke-direct {v2, p0}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity$5;-><init>(Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;)V

    invoke-virtual {v1, v2}, Lcom/samsung/groupcast/legacy/gp2/session/storage/StoreManifestRequest;->addListener(Lcom/samsung/groupcast/misc/requests/RequestListener;)V

    .line 1550
    iget-object v1, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mStoreManifestRequest:Lcom/samsung/groupcast/legacy/gp2/session/storage/StoreManifestRequest;

    invoke-virtual {v1}, Lcom/samsung/groupcast/legacy/gp2/session/storage/StoreManifestRequest;->start()V

    .line 1551
    return-void
.end method
