.class Lcom/samsung/groupcast/application/stub/StubStartActivity$1;
.super Landroid/os/Handler;
.source "StubStartActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/groupcast/application/stub/StubStartActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/groupcast/application/stub/StubStartActivity;


# direct methods
.method constructor <init>(Lcom/samsung/groupcast/application/stub/StubStartActivity;)V
    .locals 0

    .prologue
    .line 76
    iput-object p1, p0, Lcom/samsung/groupcast/application/stub/StubStartActivity$1;->this$0:Lcom/samsung/groupcast/application/stub/StubStartActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 13
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 80
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v5

    const-string v6, "MESSAGE_CMD"

    const-string v7, "DOWNLOAD_STATUS"

    invoke-virtual {v5, v6, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 82
    .local v2, "cmd":Ljava/lang/String;
    iget v5, p1, Landroid/os/Message;->what:I

    packed-switch v5, :pswitch_data_0

    .line 141
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 84
    :pswitch_1
    const-string v5, "DOWNLOAD_STATUS"

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 85
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v5

    const-string v6, "DOWNLOAD_STATUS"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    packed-switch v5, :pswitch_data_1

    goto :goto_0

    .line 88
    :pswitch_2
    iget-object v5, p0, Lcom/samsung/groupcast/application/stub/StubStartActivity$1;->this$0:Lcom/samsung/groupcast/application/stub/StubStartActivity;

    # getter for: Lcom/samsung/groupcast/application/stub/StubStartActivity;->mProgressBarInstall:Landroid/widget/ProgressBar;
    invoke-static {v5}, Lcom/samsung/groupcast/application/stub/StubStartActivity;->access$000(Lcom/samsung/groupcast/application/stub/StubStartActivity;)Landroid/widget/ProgressBar;

    move-result-object v5

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    goto :goto_0

    .line 92
    :pswitch_3
    const/4 v5, 0x0

    # setter for: Lcom/samsung/groupcast/application/stub/StubStartActivity;->isDownloading:Z
    invoke-static {v5}, Lcom/samsung/groupcast/application/stub/StubStartActivity;->access$102(Z)Z

    goto :goto_0

    .line 95
    :pswitch_4
    iget-object v5, p0, Lcom/samsung/groupcast/application/stub/StubStartActivity$1;->this$0:Lcom/samsung/groupcast/application/stub/StubStartActivity;

    # getter for: Lcom/samsung/groupcast/application/stub/StubStartActivity;->mProgressBarInstall:Landroid/widget/ProgressBar;
    invoke-static {v5}, Lcom/samsung/groupcast/application/stub/StubStartActivity;->access$000(Lcom/samsung/groupcast/application/stub/StubStartActivity;)Landroid/widget/ProgressBar;

    move-result-object v5

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    .line 97
    const/4 v5, 0x0

    # setter for: Lcom/samsung/groupcast/application/stub/StubStartActivity;->isDownloading:Z
    invoke-static {v5}, Lcom/samsung/groupcast/application/stub/StubStartActivity;->access$102(Z)Z

    goto :goto_0

    .line 109
    :cond_1
    const-string v5, "DOWNLOA_RATIO"

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 110
    iget-object v5, p0, Lcom/samsung/groupcast/application/stub/StubStartActivity$1;->this$0:Lcom/samsung/groupcast/application/stub/StubStartActivity;

    # getter for: Lcom/samsung/groupcast/application/stub/StubStartActivity;->mProgressBarInstall:Landroid/widget/ProgressBar;
    invoke-static {v5}, Lcom/samsung/groupcast/application/stub/StubStartActivity;->access$000(Lcom/samsung/groupcast/application/stub/StubStartActivity;)Landroid/widget/ProgressBar;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    .line 111
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v5

    const-string v6, "DOWNLOA_RATIO"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    # setter for: Lcom/samsung/groupcast/application/stub/StubStartActivity;->ratio:I
    invoke-static {v5}, Lcom/samsung/groupcast/application/stub/StubStartActivity;->access$202(I)I

    .line 112
    iget-object v5, p0, Lcom/samsung/groupcast/application/stub/StubStartActivity$1;->this$0:Lcom/samsung/groupcast/application/stub/StubStartActivity;

    # getter for: Lcom/samsung/groupcast/application/stub/StubStartActivity;->mProgressBarInstall:Landroid/widget/ProgressBar;
    invoke-static {v5}, Lcom/samsung/groupcast/application/stub/StubStartActivity;->access$000(Lcom/samsung/groupcast/application/stub/StubStartActivity;)Landroid/widget/ProgressBar;

    move-result-object v5

    # getter for: Lcom/samsung/groupcast/application/stub/StubStartActivity;->ratio:I
    invoke-static {}, Lcom/samsung/groupcast/application/stub/StubStartActivity;->access$200()I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 113
    iget-object v5, p0, Lcom/samsung/groupcast/application/stub/StubStartActivity$1;->this$0:Lcom/samsung/groupcast/application/stub/StubStartActivity;

    # getter for: Lcom/samsung/groupcast/application/stub/StubStartActivity;->mTextViewDownlodingSize:Landroid/widget/TextView;
    invoke-static {v5}, Lcom/samsung/groupcast/application/stub/StubStartActivity;->access$400(Lcom/samsung/groupcast/application/stub/StubStartActivity;)Landroid/widget/TextView;

    move-result-object v5

    const-string v6, "%.1fMB/%.1fMB"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    iget-object v9, p0, Lcom/samsung/groupcast/application/stub/StubStartActivity$1;->this$0:Lcom/samsung/groupcast/application/stub/StubStartActivity;

    # getter for: Lcom/samsung/groupcast/application/stub/StubStartActivity;->mAppSize:D
    invoke-static {v9}, Lcom/samsung/groupcast/application/stub/StubStartActivity;->access$300(Lcom/samsung/groupcast/application/stub/StubStartActivity;)D

    move-result-wide v9

    const-wide/high16 v11, 0x4059000000000000L    # 100.0

    div-double/2addr v9, v11

    # getter for: Lcom/samsung/groupcast/application/stub/StubStartActivity;->ratio:I
    invoke-static {}, Lcom/samsung/groupcast/application/stub/StubStartActivity;->access$200()I

    move-result v11

    int-to-double v11, v11

    mul-double/2addr v9, v11

    invoke-static {v9, v10}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x1

    iget-object v9, p0, Lcom/samsung/groupcast/application/stub/StubStartActivity$1;->this$0:Lcom/samsung/groupcast/application/stub/StubStartActivity;

    # getter for: Lcom/samsung/groupcast/application/stub/StubStartActivity;->mAppSize:D
    invoke-static {v9}, Lcom/samsung/groupcast/application/stub/StubStartActivity;->access$300(Lcom/samsung/groupcast/application/stub/StubStartActivity;)D

    move-result-wide v9

    invoke-static {v9, v10}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 118
    :pswitch_5
    iget-object v5, p0, Lcom/samsung/groupcast/application/stub/StubStartActivity$1;->this$0:Lcom/samsung/groupcast/application/stub/StubStartActivity;

    const v6, 0x7f08006c

    # invokes: Lcom/samsung/groupcast/application/stub/StubStartActivity;->showErrorDialog(I)V
    invoke-static {v5, v6}, Lcom/samsung/groupcast/application/stub/StubStartActivity;->access$500(Lcom/samsung/groupcast/application/stub/StubStartActivity;I)V

    goto/16 :goto_0

    .line 121
    :pswitch_6
    iget-object v5, p0, Lcom/samsung/groupcast/application/stub/StubStartActivity$1;->this$0:Lcom/samsung/groupcast/application/stub/StubStartActivity;

    const v6, 0x7f080073

    # invokes: Lcom/samsung/groupcast/application/stub/StubStartActivity;->showErrorDialog(I)V
    invoke-static {v5, v6}, Lcom/samsung/groupcast/application/stub/StubStartActivity;->access$500(Lcom/samsung/groupcast/application/stub/StubStartActivity;I)V

    goto/16 :goto_0

    .line 124
    :pswitch_7
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v5

    const-string v6, "appSize"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    .line 125
    .local v0, "appByteSize":J
    iget-object v5, p0, Lcom/samsung/groupcast/application/stub/StubStartActivity$1;->this$0:Lcom/samsung/groupcast/application/stub/StubStartActivity;

    long-to-double v6, v0

    const-wide/high16 v8, 0x4130000000000000L    # 1048576.0

    div-double/2addr v6, v8

    # setter for: Lcom/samsung/groupcast/application/stub/StubStartActivity;->mAppSize:D
    invoke-static {v5, v6, v7}, Lcom/samsung/groupcast/application/stub/StubStartActivity;->access$302(Lcom/samsung/groupcast/application/stub/StubStartActivity;D)D

    .line 127
    iget-object v5, p0, Lcom/samsung/groupcast/application/stub/StubStartActivity$1;->this$0:Lcom/samsung/groupcast/application/stub/StubStartActivity;

    # getter for: Lcom/samsung/groupcast/application/stub/StubStartActivity;->mTextViewTranslatorInformation:Landroid/widget/TextView;
    invoke-static {v5}, Lcom/samsung/groupcast/application/stub/StubStartActivity;->access$600(Lcom/samsung/groupcast/application/stub/StubStartActivity;)Landroid/widget/TextView;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v7, p0, Lcom/samsung/groupcast/application/stub/StubStartActivity$1;->this$0:Lcom/samsung/groupcast/application/stub/StubStartActivity;

    const v8, 0x7f08006f

    invoke-virtual {v7, v8}, Lcom/samsung/groupcast/application/stub/StubStartActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ":"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "%.1fMB"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    iget-object v9, p0, Lcom/samsung/groupcast/application/stub/StubStartActivity$1;->this$0:Lcom/samsung/groupcast/application/stub/StubStartActivity;

    # getter for: Lcom/samsung/groupcast/application/stub/StubStartActivity;->mAppSize:D
    invoke-static {v9}, Lcom/samsung/groupcast/application/stub/StubStartActivity;->access$300(Lcom/samsung/groupcast/application/stub/StubStartActivity;)D

    move-result-wide v9

    invoke-static {v9, v10}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 129
    iget-object v5, p0, Lcom/samsung/groupcast/application/stub/StubStartActivity$1;->this$0:Lcom/samsung/groupcast/application/stub/StubStartActivity;

    # getter for: Lcom/samsung/groupcast/application/stub/StubStartActivity;->mTextViewDownlodingSize:Landroid/widget/TextView;
    invoke-static {v5}, Lcom/samsung/groupcast/application/stub/StubStartActivity;->access$400(Lcom/samsung/groupcast/application/stub/StubStartActivity;)Landroid/widget/TextView;

    move-result-object v5

    const-string v6, "%.1fMB/%.1fMB"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    iget-object v9, p0, Lcom/samsung/groupcast/application/stub/StubStartActivity$1;->this$0:Lcom/samsung/groupcast/application/stub/StubStartActivity;

    # getter for: Lcom/samsung/groupcast/application/stub/StubStartActivity;->mAppSize:D
    invoke-static {v9}, Lcom/samsung/groupcast/application/stub/StubStartActivity;->access$300(Lcom/samsung/groupcast/application/stub/StubStartActivity;)D

    move-result-wide v9

    const-wide/high16 v11, 0x4059000000000000L    # 100.0

    div-double/2addr v9, v11

    # getter for: Lcom/samsung/groupcast/application/stub/StubStartActivity;->ratio:I
    invoke-static {}, Lcom/samsung/groupcast/application/stub/StubStartActivity;->access$200()I

    move-result v11

    int-to-double v11, v11

    mul-double/2addr v9, v11

    invoke-static {v9, v10}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x1

    iget-object v9, p0, Lcom/samsung/groupcast/application/stub/StubStartActivity$1;->this$0:Lcom/samsung/groupcast/application/stub/StubStartActivity;

    # getter for: Lcom/samsung/groupcast/application/stub/StubStartActivity;->mAppSize:D
    invoke-static {v9}, Lcom/samsung/groupcast/application/stub/StubStartActivity;->access$300(Lcom/samsung/groupcast/application/stub/StubStartActivity;)D

    move-result-wide v9

    invoke-static {v9, v10}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 132
    iget-object v5, p0, Lcom/samsung/groupcast/application/stub/StubStartActivity$1;->this$0:Lcom/samsung/groupcast/application/stub/StubStartActivity;

    # getter for: Lcom/samsung/groupcast/application/stub/StubStartActivity;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/samsung/groupcast/application/stub/StubStartActivity;->access$700(Lcom/samsung/groupcast/application/stub/StubStartActivity;)Landroid/content/Context;

    move-result-object v5

    const-string v6, "stub_pref"

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v4

    .line 134
    .local v4, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    .line 135
    .local v3, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v5, "appByteSize"

    invoke-interface {v3, v5, v0, v1}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 136
    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto/16 :goto_0

    .line 82
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch

    .line 85
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
