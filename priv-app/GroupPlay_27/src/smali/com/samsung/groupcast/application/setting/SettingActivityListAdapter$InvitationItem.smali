.class public Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$InvitationItem;
.super Ljava/lang/Object;
.source "SettingActivityListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "InvitationItem"
.end annotation


# instance fields
.field private final mChecked:Z

.field private final mContext:Ljava/lang/String;

.field private final mItemId:Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$InvitationEnum;

.field private final mTitle:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$InvitationEnum;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 0
    .param p1, "itemId"    # Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$InvitationEnum;
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "context"    # Ljava/lang/String;
    .param p4, "checked"    # Z

    .prologue
    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    iput-object p1, p0, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$InvitationItem;->mItemId:Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$InvitationEnum;

    .line 66
    iput-object p2, p0, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$InvitationItem;->mTitle:Ljava/lang/String;

    .line 67
    iput-object p3, p0, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$InvitationItem;->mContext:Ljava/lang/String;

    .line 68
    iput-boolean p4, p0, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$InvitationItem;->mChecked:Z

    .line 69
    return-void
.end method


# virtual methods
.method public getChecked()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 84
    iget-boolean v0, p0, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$InvitationItem;->mChecked:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public getContext()Ljava/lang/String;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$InvitationItem;->mContext:Ljava/lang/String;

    return-object v0
.end method

.method public getItemId()Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$InvitationEnum;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$InvitationItem;->mItemId:Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$InvitationEnum;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$InvitationItem;->mTitle:Ljava/lang/String;

    return-object v0
.end method
