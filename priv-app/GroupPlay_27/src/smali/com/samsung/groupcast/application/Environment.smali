.class public Lcom/samsung/groupcast/application/Environment;
.super Ljava/lang/Object;
.source "Environment.java"


# static fields
.field private static final CHINA_SALES_CODES:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final CSC_PATH:Ljava/lang/String; = "/system/csc/sales_code.dat"

.field public static final SCREEN_CONFIGURATION_QUALIFIER_SW320DP_HDPI:Ljava/lang/String; = "sw320dp-hdpi"

.field public static final SCREEN_CONFIGURATION_QUALIFIER_SW360DP_HDPI:Ljava/lang/String; = "sw360dp-hdpi"

.field public static final SCREEN_CONFIGURATION_QUALIFIER_SW360DP_XDPI:Ljava/lang/String; = "sw360dp-xdpi"

.field public static final SCREEN_CONFIGURATION_QUALIFIER_SW360DP_XXDPI:Ljava/lang/String; = "sw360dp-xxdpi"

.field public static final SCREEN_CONFIGURATION_QUALIFIER_SW480DP_HDPI:Ljava/lang/String; = "sw480dp-hdpi"

.field private static USING_TABLET_INTERFACE_IDIOM:Ljava/lang/Boolean;

.field private static sFrequencyRequest:Landroid/os/CustomFrequencyManager$FrequencyRequest;

.field private static sLockForFR:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 36
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/samsung/groupcast/application/Environment;->CHINA_SALES_CODES:Ljava/util/ArrayList;

    .line 37
    sget-object v0, Lcom/samsung/groupcast/application/Environment;->CHINA_SALES_CODES:Ljava/util/ArrayList;

    const-string v1, "CHN"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 38
    sget-object v0, Lcom/samsung/groupcast/application/Environment;->CHINA_SALES_CODES:Ljava/util/ArrayList;

    const-string v1, "CHU"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 39
    sget-object v0, Lcom/samsung/groupcast/application/Environment;->CHINA_SALES_CODES:Ljava/util/ArrayList;

    const-string v1, "CHM"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 40
    sget-object v0, Lcom/samsung/groupcast/application/Environment;->CHINA_SALES_CODES:Ljava/util/ArrayList;

    const-string v1, "CHZ"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 41
    sget-object v0, Lcom/samsung/groupcast/application/Environment;->CHINA_SALES_CODES:Ljava/util/ArrayList;

    const-string v1, "CTC"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 42
    sget-object v0, Lcom/samsung/groupcast/application/Environment;->CHINA_SALES_CODES:Ljava/util/ArrayList;

    const-string v1, "CHC"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 517
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/samsung/groupcast/application/Environment;->sLockForFR:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static askDvfsLock()V
    .locals 9

    .prologue
    .line 521
    sget-object v8, Lcom/samsung/groupcast/application/Environment;->sLockForFR:Ljava/lang/Object;

    monitor-enter v8

    .line 522
    :try_start_0
    sget-object v1, Lcom/samsung/groupcast/application/Environment;->sFrequencyRequest:Landroid/os/CustomFrequencyManager$FrequencyRequest;

    if-nez v1, :cond_0

    .line 523
    invoke-static {}, Lcom/samsung/groupcast/application/App;->getInstance()Lcom/samsung/groupcast/application/App;

    move-result-object v1

    const-string v5, "CustomFrequencyManagerService"

    invoke-virtual {v1, v5}, Lcom/samsung/groupcast/application/App;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/CustomFrequencyManager;

    .line 527
    .local v0, "cfm":Landroid/os/CustomFrequencyManager;
    const-wide/16 v3, 0xbb8

    .line 529
    .local v3, "timeOut":J
    if-eqz v0, :cond_0

    .line 530
    invoke-virtual {v0}, Landroid/os/CustomFrequencyManager;->getSupportedCPUFrequency()[I

    move-result-object v7

    .line 531
    .local v7, "list":[I
    array-length v1, v7

    if-lez v1, :cond_0

    .line 532
    array-length v1, v7

    add-int/lit8 v1, v1, -0x1

    aget v2, v7, v1

    .line 533
    .local v2, "max":I
    const/4 v1, 0x7

    invoke-static {}, Lcom/samsung/groupcast/application/App;->getInstance()Lcom/samsung/groupcast/application/App;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/groupcast/application/App;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-static {}, Lcom/samsung/groupcast/application/App;->getInstance()Lcom/samsung/groupcast/application/App;

    move-result-object v6

    invoke-virtual/range {v0 .. v6}, Landroid/os/CustomFrequencyManager;->newFrequencyRequest(IIJLjava/lang/String;Landroid/content/Context;)Landroid/os/CustomFrequencyManager$FrequencyRequest;

    move-result-object v1

    sput-object v1, Lcom/samsung/groupcast/application/Environment;->sFrequencyRequest:Landroid/os/CustomFrequencyManager$FrequencyRequest;

    .line 543
    .end local v2    # "max":I
    .end local v7    # "list":[I
    :cond_0
    sget-object v1, Lcom/samsung/groupcast/application/Environment;->sFrequencyRequest:Landroid/os/CustomFrequencyManager$FrequencyRequest;

    invoke-virtual {v1}, Landroid/os/CustomFrequencyManager$FrequencyRequest;->doFrequencyRequest()V

    .line 544
    monitor-exit v8

    .line 545
    return-void

    .line 544
    :catchall_0
    move-exception v1

    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public static dipsToPixels(F)I
    .locals 2
    .param p0, "dips"    # F

    .prologue
    .line 46
    const/4 v0, 0x1

    invoke-static {}, Lcom/samsung/groupcast/application/App;->getInstance()Lcom/samsung/groupcast/application/App;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/groupcast/application/App;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    invoke-static {v0, p0, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    return v0
.end method

.method public static generateScreenConfigurationQualifier()Ljava/lang/String;
    .locals 3

    .prologue
    .line 487
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 488
    .local v0, "sb":Ljava/lang/StringBuffer;
    const-string v1, "sw"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 489
    invoke-static {}, Lcom/samsung/groupcast/application/App;->getInstance()Lcom/samsung/groupcast/application/App;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/groupcast/application/App;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v1, v1

    invoke-static {}, Lcom/samsung/groupcast/application/App;->getInstance()Lcom/samsung/groupcast/application/App;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/groupcast/application/App;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->density:F

    div-float/2addr v1, v2

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 490
    const-string v1, "dp-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 492
    invoke-static {}, Lcom/samsung/groupcast/application/App;->getInstance()Lcom/samsung/groupcast/application/App;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/groupcast/application/App;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->densityDpi:I

    const/16 v2, 0x78

    if-ne v1, v2, :cond_1

    .line 493
    const-string v1, "ldpi"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 505
    :cond_0
    :goto_0
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 494
    :cond_1
    invoke-static {}, Lcom/samsung/groupcast/application/App;->getInstance()Lcom/samsung/groupcast/application/App;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/groupcast/application/App;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->densityDpi:I

    const/16 v2, 0xa0

    if-ne v1, v2, :cond_2

    .line 495
    const-string v1, "mdpi"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 496
    :cond_2
    invoke-static {}, Lcom/samsung/groupcast/application/App;->getInstance()Lcom/samsung/groupcast/application/App;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/groupcast/application/App;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->densityDpi:I

    const/16 v2, 0xf0

    if-ne v1, v2, :cond_3

    .line 497
    const-string v1, "hdpi"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 498
    :cond_3
    invoke-static {}, Lcom/samsung/groupcast/application/App;->getInstance()Lcom/samsung/groupcast/application/App;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/groupcast/application/App;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->densityDpi:I

    const/16 v2, 0x140

    if-ne v1, v2, :cond_4

    .line 499
    const-string v1, "xdpi"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 500
    :cond_4
    invoke-static {}, Lcom/samsung/groupcast/application/App;->getInstance()Lcom/samsung/groupcast/application/App;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/groupcast/application/App;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->densityDpi:I

    const/16 v2, 0x1e0

    if-ne v1, v2, :cond_5

    .line 501
    const-string v1, "xxdpi"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 502
    :cond_5
    invoke-static {}, Lcom/samsung/groupcast/application/App;->getInstance()Lcom/samsung/groupcast/application/App;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/groupcast/application/App;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->densityDpi:I

    const/16 v2, 0xd5

    if-ne v1, v2, :cond_0

    .line 503
    const-string v1, "tvdpi"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0
.end method

.method public static getBuildModel()Ljava/lang/String;
    .locals 2

    .prologue
    .line 368
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    .line 369
    .local v0, "buildModel":Ljava/lang/String;
    if-eqz v0, :cond_0

    const-string v1, "SAMSUNG-"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 370
    const-string v1, "SAMSUNG-"

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 373
    .end local v0    # "buildModel":Ljava/lang/String;
    :cond_0
    return-object v0
.end method

.method public static getCSC()Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v4, 0x0

    .line 418
    const/4 v2, 0x0

    .line 422
    .local v2, "inputStream":Ljava/io/InputStream;
    :try_start_0
    new-instance v2, Ljava/io/FileInputStream;

    .end local v2    # "inputStream":Ljava/io/InputStream;
    new-instance v5, Ljava/io/File;

    const-string v6, "/system/csc/sales_code.dat"

    invoke-direct {v5, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-direct {v2, v5}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 429
    .restart local v2    # "inputStream":Ljava/io/InputStream;
    const/16 v5, 0x14

    new-array v0, v5, [B

    .line 432
    .local v0, "buffer":[B
    :try_start_1
    invoke-virtual {v2, v0}, Ljava/io/InputStream;->read([B)I

    move-result v5

    if-eqz v5, :cond_0

    .line 433
    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, v0}, Ljava/lang/String;-><init>([B)V

    .line 434
    .local v3, "version":Ljava/lang/String;
    const/4 v5, 0x0

    const/4 v6, 0x3

    invoke-virtual {v3, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v4

    .line 442
    :try_start_2
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 449
    .end local v0    # "buffer":[B
    .end local v2    # "inputStream":Ljava/io/InputStream;
    .end local v3    # "version":Ljava/lang/String;
    :goto_0
    return-object v4

    .line 423
    :catch_0
    move-exception v1

    .line 424
    .local v1, "e":Ljava/io/FileNotFoundException;
    const-class v5, Lcom/samsung/groupcast/application/Environment;

    const-string v6, "[getCSC] CSC file does not exist or is not file"

    invoke-static {v5, v6}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;)V

    goto :goto_0

    .line 443
    .end local v1    # "e":Ljava/io/FileNotFoundException;
    .restart local v0    # "buffer":[B
    .restart local v2    # "inputStream":Ljava/io/InputStream;
    .restart local v3    # "version":Ljava/lang/String;
    :catch_1
    move-exception v1

    .line 445
    .local v1, "e":Ljava/lang/Exception;
    const-class v5, Lcom/samsung/groupcast/application/Environment;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "[getCSC] exception:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;)V

    goto :goto_0

    .line 442
    .end local v1    # "e":Ljava/lang/Exception;
    .end local v3    # "version":Ljava/lang/String;
    :cond_0
    :try_start_3
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_0

    .line 443
    :catch_2
    move-exception v1

    .line 445
    .restart local v1    # "e":Ljava/lang/Exception;
    const-class v5, Lcom/samsung/groupcast/application/Environment;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "[getCSC] exception:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;)V

    goto :goto_0

    .line 438
    .end local v1    # "e":Ljava/lang/Exception;
    :catch_3
    move-exception v1

    .line 439
    .restart local v1    # "e":Ljava/lang/Exception;
    :try_start_4
    const-class v5, Lcom/samsung/groupcast/application/Environment;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "[getCSC] exception:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 442
    :try_start_5
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_4

    goto :goto_0

    .line 443
    :catch_4
    move-exception v1

    .line 445
    const-class v5, Lcom/samsung/groupcast/application/Environment;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "[getCSC] exception:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;)V

    goto :goto_0

    .line 441
    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v4

    .line 442
    :try_start_6
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_5

    .line 446
    :goto_1
    throw v4

    .line 443
    :catch_5
    move-exception v1

    .line 445
    .restart local v1    # "e":Ljava/lang/Exception;
    const-class v5, Lcom/samsung/groupcast/application/Environment;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "[getCSC] exception:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static getCSCCountry()Ljava/lang/String;
    .locals 9

    .prologue
    .line 222
    const/4 v2, 0x0

    .line 223
    .local v2, "countryCode":Ljava/lang/String;
    const/4 v1, 0x0

    .line 226
    .local v1, "c":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :try_start_0
    const-string v5, "android.os.SystemProperties"

    invoke-static {v5}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    .line 227
    const/4 v4, 0x0

    .line 228
    .local v4, "m":Ljava/lang/reflect/Method;
    const-string v5, "get"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Class;

    const/4 v7, 0x0

    const-class v8, Ljava/lang/String;

    aput-object v8, v6, v7

    invoke-virtual {v1, v5, v6}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v4

    .line 229
    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    const-string v7, "ro.csc.country_code"

    aput-object v7, v5, v6

    invoke-virtual {v4, v1, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    move-object v0, v5

    check-cast v0, Ljava/lang/String;

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 235
    .end local v4    # "m":Ljava/lang/reflect/Method;
    :goto_0
    return-object v2

    .line 230
    :catch_0
    move-exception v3

    .line 231
    .local v3, "e":Ljava/lang/NoSuchMethodException;
    invoke-virtual {v3}, Ljava/lang/NoSuchMethodException;->printStackTrace()V

    goto :goto_0

    .line 232
    .end local v3    # "e":Ljava/lang/NoSuchMethodException;
    :catch_1
    move-exception v3

    .line 233
    .local v3, "e":Ljava/lang/Exception;
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static getCSCSalesCode()Ljava/lang/String;
    .locals 9

    .prologue
    .line 205
    const/4 v2, 0x0

    .line 206
    .local v2, "countryCode":Ljava/lang/String;
    const/4 v1, 0x0

    .line 209
    .local v1, "c":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :try_start_0
    const-string v5, "android.os.SystemProperties"

    invoke-static {v5}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    .line 210
    const/4 v4, 0x0

    .line 211
    .local v4, "m":Ljava/lang/reflect/Method;
    const-string v5, "get"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Class;

    const/4 v7, 0x0

    const-class v8, Ljava/lang/String;

    aput-object v8, v6, v7

    invoke-virtual {v1, v5, v6}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v4

    .line 212
    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    const-string v7, "ro.csc.sales_code"

    aput-object v7, v5, v6

    invoke-virtual {v4, v1, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    move-object v0, v5

    check-cast v0, Ljava/lang/String;

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 218
    .end local v4    # "m":Ljava/lang/reflect/Method;
    :goto_0
    return-object v2

    .line 213
    :catch_0
    move-exception v3

    .line 214
    .local v3, "e":Ljava/lang/NoSuchMethodException;
    invoke-virtual {v3}, Ljava/lang/NoSuchMethodException;->printStackTrace()V

    goto :goto_0

    .line 215
    .end local v3    # "e":Ljava/lang/NoSuchMethodException;
    :catch_1
    move-exception v3

    .line 216
    .local v3, "e":Ljava/lang/Exception;
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static getDeviceId()Ljava/lang/String;
    .locals 3

    .prologue
    .line 111
    invoke-static {}, Lcom/samsung/groupcast/application/App;->getInstance()Lcom/samsung/groupcast/application/App;

    move-result-object v1

    const-string v2, "phone"

    invoke-virtual {v1, v2}, Lcom/samsung/groupcast/application/App;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 113
    .local v0, "telephonyManager":Landroid/telephony/TelephonyManager;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getDeviceId()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    .line 114
    :cond_0
    const-string v1, ""

    .line 116
    :goto_0
    return-object v1

    :cond_1
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getDeviceId()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public static getDeviceName()Ljava/lang/String;
    .locals 4

    .prologue
    .line 63
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    .line 64
    .local v0, "device":Ljava/lang/String;
    invoke-static {}, Lcom/samsung/groupcast/application/Environment;->getObscuredPhoneNumber()Ljava/lang/String;

    move-result-object v1

    .line 66
    .local v1, "obscuredPhoneNumber":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 67
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 69
    .end local v0    # "device":Ljava/lang/String;
    :cond_0
    return-object v0
.end method

.method public static getDisplayDpi()I
    .locals 1

    .prologue
    .line 59
    invoke-static {}, Lcom/samsung/groupcast/application/App;->getInstance()Lcom/samsung/groupcast/application/App;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/groupcast/application/App;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    return v0
.end method

.method public static getDisplayHeight()I
    .locals 1

    .prologue
    .line 55
    invoke-static {}, Lcom/samsung/groupcast/application/App;->getInstance()Lcom/samsung/groupcast/application/App;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/groupcast/application/App;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    return v0
.end method

.method public static getDisplayWidth()I
    .locals 1

    .prologue
    .line 51
    invoke-static {}, Lcom/samsung/groupcast/application/App;->getInstance()Lcom/samsung/groupcast/application/App;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/groupcast/application/App;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    return v0
.end method

.method public static getMCC(Landroid/content/Context;)Ljava/lang/String;
    .locals 6
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x0

    .line 377
    const-string v3, "phone"

    invoke-virtual {p0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/telephony/TelephonyManager;

    .line 379
    .local v2, "telMgr":Landroid/telephony/TelephonyManager;
    const/4 v0, 0x0

    .line 380
    .local v0, "mcc":Ljava/lang/String;
    if-nez v2, :cond_0

    .line 381
    const/4 v3, 0x0

    .line 399
    :goto_0
    return-object v3

    .line 383
    :cond_0
    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getSimOperator()Ljava/lang/String;

    move-result-object v1

    .line 384
    .local v1, "networkOperator":Ljava/lang/String;
    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 393
    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getSimOperator()Ljava/lang/String;

    move-result-object v1

    .line 394
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_1

    .line 395
    invoke-virtual {v1, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    :cond_1
    :goto_1
    move-object v3, v0

    .line 399
    goto :goto_0

    .line 386
    :pswitch_0
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_1

    .line 387
    invoke-virtual {v1, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 388
    goto :goto_1

    .line 384
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public static getMNC(Landroid/content/Context;)Ljava/lang/String;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 403
    const-string v3, "phone"

    invoke-virtual {p0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/TelephonyManager;

    .line 405
    .local v1, "telMgr":Landroid/telephony/TelephonyManager;
    if-nez v1, :cond_1

    .line 413
    :cond_0
    :goto_0
    return-object v2

    .line 409
    :cond_1
    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getSimOperator()Ljava/lang/String;

    move-result-object v0

    .line 410
    .local v0, "networkOperator":Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_0

    .line 411
    const/4 v2, 0x3

    invoke-virtual {v0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method public static getObscuredCountryIso()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 92
    invoke-static {}, Lcom/samsung/groupcast/application/App;->getInstance()Lcom/samsung/groupcast/application/App;

    move-result-object v4

    const-string v5, "phone"

    invoke-virtual {v4, v5}, Lcom/samsung/groupcast/application/App;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/telephony/TelephonyManager;

    .line 94
    .local v2, "telephonyManager":Landroid/telephony/TelephonyManager;
    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getSimCountryIso()Ljava/lang/String;

    move-result-object v0

    .line 96
    .local v0, "countryIso":Ljava/lang/String;
    if-nez v0, :cond_1

    move-object v0, v3

    .line 106
    .end local v0    # "countryIso":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object v0

    .line 100
    .restart local v0    # "countryIso":Ljava/lang/String;
    :cond_1
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    .line 102
    .local v1, "length":I
    const/4 v4, 0x2

    if-ge v1, v4, :cond_0

    move-object v0, v3

    .line 103
    goto :goto_0
.end method

.method public static getObscuredPhoneNumber()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 74
    invoke-static {}, Lcom/samsung/groupcast/application/App;->getInstance()Lcom/samsung/groupcast/application/App;

    move-result-object v4

    const-string v5, "phone"

    invoke-virtual {v4, v5}, Lcom/samsung/groupcast/application/App;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/telephony/TelephonyManager;

    .line 76
    .local v2, "telephonyManager":Landroid/telephony/TelephonyManager;
    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getLine1Number()Ljava/lang/String;

    move-result-object v1

    .line 78
    .local v1, "phoneNumber":Ljava/lang/String;
    if-nez v1, :cond_1

    .line 88
    :cond_0
    :goto_0
    return-object v3

    .line 82
    :cond_1
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    .line 84
    .local v0, "length":I
    const/4 v4, 0x2

    if-lt v0, v4, :cond_0

    .line 88
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "**"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    add-int/lit8 v4, v0, -0x2

    invoke-virtual {v1, v4, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method

.method public static getSystemCountryCode()Ljava/lang/String;
    .locals 9

    .prologue
    const/4 v4, 0x0

    .line 334
    const/4 v0, 0x0

    .line 337
    .local v0, "c":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :try_start_0
    const-string v5, "android.os.SystemProperties"

    invoke-static {v5}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 344
    const/4 v3, 0x0

    .line 347
    .local v3, "m":Ljava/lang/reflect/Method;
    :try_start_1
    const-string v5, "get"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Class;

    const/4 v7, 0x0

    const-class v8, Ljava/lang/String;

    aput-object v8, v6, v7

    invoke-virtual {v0, v5, v6}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v3

    .line 354
    const/4 v1, 0x0

    .line 357
    .local v1, "countryCode":Ljava/lang/String;
    const/4 v5, 0x1

    :try_start_2
    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    const-string v7, "ro.csc.country_code"

    aput-object v7, v5, v6

    invoke-virtual {v3, v0, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "countryCode":Ljava/lang/String;
    check-cast v1, Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    .line 364
    .end local v3    # "m":Ljava/lang/reflect/Method;
    :goto_0
    return-object v1

    .line 338
    :catch_0
    move-exception v2

    .line 339
    .local v2, "e":Ljava/lang/ClassNotFoundException;
    const-class v5, Lcom/samsung/groupcast/application/Environment;

    const-string v6, "Could not get class android.os.SystemProperties, assuming non-ro.csc.country_code"

    invoke-static {v5, v6}, Lcom/samsung/groupcast/application/Logger;->wx(Ljava/lang/Class;Ljava/lang/String;)V

    move-object v1, v4

    .line 341
    goto :goto_0

    .line 348
    .end local v2    # "e":Ljava/lang/ClassNotFoundException;
    .restart local v3    # "m":Ljava/lang/reflect/Method;
    :catch_1
    move-exception v2

    .line 349
    .local v2, "e":Ljava/lang/NoSuchMethodException;
    const-class v5, Lcom/samsung/groupcast/application/Environment;

    const-string v6, "Could not get method android.os.SystemProperties.get(), assuming non-ro.csc.country_code"

    invoke-static {v5, v6}, Lcom/samsung/groupcast/application/Logger;->wx(Ljava/lang/Class;Ljava/lang/String;)V

    move-object v1, v4

    .line 351
    goto :goto_0

    .line 358
    .end local v2    # "e":Ljava/lang/NoSuchMethodException;
    :catch_2
    move-exception v2

    .line 359
    .local v2, "e":Ljava/lang/Exception;
    const-class v5, Lcom/samsung/groupcast/application/Environment;

    const-string v6, "Could not get invoke android.os.SystemProperties.get(), assuming non-ro.csc.country_code"

    invoke-static {v5, v6}, Lcom/samsung/groupcast/application/Logger;->wx(Ljava/lang/Class;Ljava/lang/String;)V

    move-object v1, v4

    .line 361
    goto :goto_0
.end method

.method public static getUserName()Ljava/lang/String;
    .locals 13

    .prologue
    const/4 v12, 0x0

    .line 251
    invoke-static {}, Lcom/samsung/groupcast/application/Preferences;->getNicknamePreference()Ljava/lang/String;

    move-result-object v8

    .line 252
    .local v8, "nickName":Ljava/lang/String;
    if-eqz v8, :cond_0

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v10

    if-lez v10, :cond_0

    const-string v10, ""

    invoke-virtual {v8, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_0

    .line 301
    .end local v8    # "nickName":Ljava/lang/String;
    .local v1, "accounts":[Landroid/accounts/Account;
    .local v2, "arr$":[Landroid/accounts/Account;
    .local v4, "i$":I
    .local v5, "len$":I
    .local v6, "manager":Landroid/accounts/AccountManager;
    :goto_0
    return-object v8

    .line 257
    .end local v1    # "accounts":[Landroid/accounts/Account;
    .end local v2    # "arr$":[Landroid/accounts/Account;
    .end local v4    # "i$":I
    .end local v5    # "len$":I
    .end local v6    # "manager":Landroid/accounts/AccountManager;
    .restart local v8    # "nickName":Ljava/lang/String;
    :cond_0
    invoke-static {}, Lcom/samsung/groupcast/application/App;->getInstance()Lcom/samsung/groupcast/application/App;

    move-result-object v10

    invoke-virtual {v10}, Lcom/samsung/groupcast/application/App;->getApplicationContext()Landroid/content/Context;

    move-result-object v10

    invoke-static {v10}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v6

    .line 258
    .restart local v6    # "manager":Landroid/accounts/AccountManager;
    const-string v10, "com.osp.app.signin"

    invoke-virtual {v6, v10}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v1

    .line 259
    .restart local v1    # "accounts":[Landroid/accounts/Account;
    move-object v2, v1

    .restart local v2    # "arr$":[Landroid/accounts/Account;
    array-length v5, v2

    .restart local v5    # "len$":I
    const/4 v4, 0x0

    .restart local v4    # "i$":I
    :goto_1
    if-ge v4, v5, :cond_2

    aget-object v0, v2, v4

    .line 260
    .local v0, "account":Landroid/accounts/Account;
    iget-object v10, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    const-string v11, "@"

    invoke-virtual {v10, v11}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    .line 264
    .local v9, "parts":[Ljava/lang/String;
    array-length v10, v9

    if-lez v10, :cond_1

    aget-object v10, v9, v12

    if-eqz v10, :cond_1

    .line 265
    const-class v10, Lcom/samsung/groupcast/application/Environment;

    const-string v11, "use userinfo"

    invoke-static {v10, v11}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;)V

    .line 266
    aget-object v8, v9, v12

    goto :goto_0

    .line 259
    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 269
    .end local v0    # "account":Landroid/accounts/Account;
    .end local v9    # "parts":[Ljava/lang/String;
    :cond_2
    const-string v10, "com.google"

    invoke-virtual {v6, v10}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v1

    .line 270
    array-length v10, v1

    if-eqz v10, :cond_3

    .line 271
    aget-object v10, v1, v12

    iget-object v10, v10, Landroid/accounts/Account;->name:Ljava/lang/String;

    const-string v11, "@"

    invoke-virtual {v10, v11}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    .line 275
    .restart local v9    # "parts":[Ljava/lang/String;
    array-length v10, v9

    if-lez v10, :cond_3

    aget-object v10, v9, v12

    if-eqz v10, :cond_3

    .line 276
    const-class v10, Lcom/samsung/groupcast/application/Environment;

    const-string v11, "use userinfo"

    invoke-static {v10, v11}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;)V

    .line 277
    aget-object v8, v9, v12

    goto :goto_0

    .line 281
    .end local v9    # "parts":[Ljava/lang/String;
    :cond_3
    invoke-static {}, Lcom/samsung/groupcast/application/App;->getInstance()Lcom/samsung/groupcast/application/App;

    move-result-object v10

    invoke-virtual {v10}, Lcom/samsung/groupcast/application/App;->getApplicationContext()Landroid/content/Context;

    move-result-object v10

    invoke-virtual {v10}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v10

    const-string v11, "device_name"

    invoke-static {v10, v11}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 289
    .local v3, "deviceName":Ljava/lang/String;
    if-eqz v3, :cond_4

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v10

    if-nez v10, :cond_4

    sget-object v10, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v3, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_4

    .line 290
    const-class v10, Lcom/samsung/groupcast/application/Environment;

    const-string v11, "use deviceName"

    invoke-static {v10, v11}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;)V

    move-object v8, v3

    .line 291
    goto/16 :goto_0

    .line 295
    :cond_4
    invoke-static {}, Lcom/samsung/groupcast/application/Environment;->getBuildModel()Ljava/lang/String;

    move-result-object v7

    .line 296
    .local v7, "modelName":Ljava/lang/String;
    if-eqz v7, :cond_5

    const-string v10, ""

    invoke-virtual {v7, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_5

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v10

    if-nez v10, :cond_6

    .line 298
    :cond_5
    const-string v7, "GroupPlay"

    :cond_6
    move-object v8, v7

    .line 301
    goto/16 :goto_0
.end method

.method public static hasHardwareOptionMenuKey()Z
    .locals 1

    .prologue
    .line 556
    invoke-static {}, Lcom/samsung/groupcast/application/App;->getInstance()Lcom/samsung/groupcast/application/App;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/groupcast/application/App;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->hasPermanentMenuKey()Z

    move-result v0

    return v0
.end method

.method public static isAirplaneModeOn(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 306
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "airplane_mode_on"

    invoke-static {v1, v2, v0}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public static isCheckForInternetServiceOn(Landroid/content/Context;)Z
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 321
    const/4 v1, 0x0

    .line 323
    .local v1, "ret":I
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "wifi_watchdog_poor_network_test_enabled"

    const/4 v6, 0x0

    invoke-static {v4, v5, v6}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 329
    :goto_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "isCheckForInternetServiceOn ret = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 330
    if-ne v1, v2, :cond_0

    :goto_1
    return v2

    .line 326
    :catch_0
    move-exception v0

    .line 327
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    const-string v4, "isCheckForInternetServiceOn : IllegalArgumentException thrown"

    invoke-static {v4}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    goto :goto_0

    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :cond_0
    move v2, v3

    .line 330
    goto :goto_1
.end method

.method public static isCmccModel()Z
    .locals 2

    .prologue
    .line 195
    invoke-static {}, Lcom/samsung/groupcast/application/Environment;->getCSCSalesCode()Ljava/lang/String;

    move-result-object v0

    .line 196
    .local v0, "csc":Ljava/lang/String;
    if-eqz v0, :cond_1

    const-string v1, "CHM"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "CHC"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 197
    :cond_0
    const/4 v1, 0x1

    .line 199
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isMobileConnected()Z
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 474
    invoke-static {}, Lcom/samsung/groupcast/application/App;->getInstance()Lcom/samsung/groupcast/application/App;

    move-result-object v3

    const-string v4, "connectivity"

    invoke-virtual {v3, v4}, Lcom/samsung/groupcast/application/App;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 477
    .local v0, "connectivityManager":Landroid/net/ConnectivityManager;
    invoke-virtual {v0, v2}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v1

    .line 478
    .local v1, "mobile":Landroid/net/NetworkInfo;
    if-nez v1, :cond_0

    .line 481
    :goto_0
    return v2

    :cond_0
    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v2

    goto :goto_0
.end method

.method public static isNetworkAvailable()Z
    .locals 4

    .prologue
    .line 454
    invoke-static {}, Lcom/samsung/groupcast/application/App;->getInstance()Lcom/samsung/groupcast/application/App;

    move-result-object v2

    const-string v3, "connectivity"

    invoke-virtual {v2, v3}, Lcom/samsung/groupcast/application/App;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/ConnectivityManager;

    .line 457
    .local v1, "connectivityManager":Landroid/net/ConnectivityManager;
    invoke-virtual {v1}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 459
    .local v0, "activeNetworkInfo":Landroid/net/NetworkInfo;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static isRunningOnChinaProduct()Z
    .locals 9

    .prologue
    const/4 v4, 0x0

    .line 161
    const/4 v0, 0x0

    .line 164
    .local v0, "c":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :try_start_0
    const-string v5, "android.os.SystemProperties"

    invoke-static {v5}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 171
    const/4 v2, 0x0

    .line 174
    .local v2, "m":Ljava/lang/reflect/Method;
    :try_start_1
    const-string v5, "get"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Class;

    const/4 v7, 0x0

    const-class v8, Ljava/lang/String;

    aput-object v8, v6, v7

    invoke-virtual {v0, v5, v6}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v2

    .line 181
    const/4 v3, 0x0

    .line 184
    .local v3, "salesCode":Ljava/lang/String;
    const/4 v5, 0x1

    :try_start_2
    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    const-string v7, "ro.csc.sales_code"

    aput-object v7, v5, v6

    invoke-virtual {v2, v0, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "salesCode":Ljava/lang/String;
    check-cast v3, Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    .line 191
    .restart local v3    # "salesCode":Ljava/lang/String;
    sget-object v4, Lcom/samsung/groupcast/application/Environment;->CHINA_SALES_CODES:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v4

    .end local v2    # "m":Ljava/lang/reflect/Method;
    .end local v3    # "salesCode":Ljava/lang/String;
    :goto_0
    return v4

    .line 165
    :catch_0
    move-exception v1

    .line 166
    .local v1, "e":Ljava/lang/ClassNotFoundException;
    const-class v5, Lcom/samsung/groupcast/application/Environment;

    const-string v6, "doesDeviceHaveChinaSalesCode"

    const-string v7, "could not get class android.os.SystemProperties, assuming non-China sales code"

    invoke-static {v5, v6, v7}, Lcom/samsung/groupcast/application/Logger;->wx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    .line 175
    .end local v1    # "e":Ljava/lang/ClassNotFoundException;
    .restart local v2    # "m":Ljava/lang/reflect/Method;
    :catch_1
    move-exception v1

    .line 176
    .local v1, "e":Ljava/lang/NoSuchMethodException;
    const-class v5, Lcom/samsung/groupcast/application/Environment;

    const-string v6, "doesDeviceHaveChinaSalesCode"

    const-string v7, "could not get method android.os.SystemProperties.get(), assuming non-China sales code"

    invoke-static {v5, v6, v7}, Lcom/samsung/groupcast/application/Logger;->wx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    .line 185
    .end local v1    # "e":Ljava/lang/NoSuchMethodException;
    :catch_2
    move-exception v1

    .line 186
    .local v1, "e":Ljava/lang/Exception;
    const-class v5, Lcom/samsung/groupcast/application/Environment;

    const-string v6, "doesDeviceHaveChinaSalesCode"

    const-string v7, "could not get invoke android.os.SystemProperties.get(), assuming non-China sales code"

    invoke-static {v5, v6, v7}, Lcom/samsung/groupcast/application/Logger;->wx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static isRunningOnShippingProduct()Z
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 137
    const/4 v1, 0x0

    .line 140
    .local v1, "isProductShipMethod":Ljava/lang/reflect/Method;
    :try_start_0
    const-class v4, Landroid/os/Debug;

    const-string v5, "isProductShip"

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Class;

    invoke-virtual {v4, v5, v6}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 145
    if-nez v1, :cond_1

    .line 157
    :cond_0
    :goto_0
    return v3

    .line 141
    :catch_0
    move-exception v0

    .line 142
    .local v0, "e":Ljava/lang/NoSuchMethodException;
    goto :goto_0

    .line 149
    .end local v0    # "e":Ljava/lang/NoSuchMethodException;
    :cond_1
    const/4 v2, 0x0

    .line 152
    .local v2, "isProductShipResult":Ljava/lang/Integer;
    :try_start_1
    const-class v4, Landroid/os/Debug;

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {v1, v4, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "isProductShipResult":Ljava/lang/Integer;
    check-cast v2, Ljava/lang/Integer;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 157
    .restart local v2    # "isProductShipResult":Ljava/lang/Integer;
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-eqz v4, :cond_0

    const/4 v3, 0x1

    goto :goto_0

    .line 153
    .end local v2    # "isProductShipResult":Ljava/lang/Integer;
    :catch_1
    move-exception v0

    .line 154
    .local v0, "e":Ljava/lang/Exception;
    goto :goto_0
.end method

.method public static isUsingTabletInterfaceIdiom()Z
    .locals 2

    .prologue
    .line 120
    sget-object v0, Lcom/samsung/groupcast/application/Environment;->USING_TABLET_INTERFACE_IDIOM:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    .line 121
    invoke-static {}, Lcom/samsung/groupcast/application/App;->getInstance()Lcom/samsung/groupcast/application/App;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/groupcast/application/App;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->smallestScreenWidthDp:I

    const/16 v1, 0x258

    if-le v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/samsung/groupcast/application/Environment;->USING_TABLET_INTERFACE_IDIOM:Ljava/lang/Boolean;

    .line 133
    :cond_0
    sget-object v0, Lcom/samsung/groupcast/application/Environment;->USING_TABLET_INTERFACE_IDIOM:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0

    .line 121
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isVenderSpecificIdFeatureAvailable()Z
    .locals 3

    .prologue
    .line 312
    :try_start_0
    const-class v1, Landroid/net/wifi/WifiConfiguration;

    const-string v2, "vendorIE"

    invoke-virtual {v1, v2}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_0

    .line 316
    const/4 v1, 0x1

    .local v0, "e":Ljava/lang/NoSuchFieldException;
    :goto_0
    return v1

    .line 313
    .end local v0    # "e":Ljava/lang/NoSuchFieldException;
    :catch_0
    move-exception v0

    .line 314
    .restart local v0    # "e":Ljava/lang/NoSuchFieldException;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isWifiConnected()Z
    .locals 4

    .prologue
    .line 463
    invoke-static {}, Lcom/samsung/groupcast/application/App;->getInstance()Lcom/samsung/groupcast/application/App;

    move-result-object v2

    const-string v3, "connectivity"

    invoke-virtual {v2, v3}, Lcom/samsung/groupcast/application/App;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 466
    .local v0, "connectivityManager":Landroid/net/ConnectivityManager;
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v1

    .line 467
    .local v1, "wifi":Landroid/net/NetworkInfo;
    if-nez v1, :cond_0

    .line 468
    const/4 v2, 0x0

    .line 470
    :goto_0
    return v2

    :cond_0
    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v2

    goto :goto_0
.end method

.method public static releaseDvfsLock()V
    .locals 2

    .prologue
    .line 548
    sget-object v1, Lcom/samsung/groupcast/application/Environment;->sLockForFR:Ljava/lang/Object;

    monitor-enter v1

    .line 549
    :try_start_0
    sget-object v0, Lcom/samsung/groupcast/application/Environment;->sFrequencyRequest:Landroid/os/CustomFrequencyManager$FrequencyRequest;

    if-eqz v0, :cond_0

    .line 550
    sget-object v0, Lcom/samsung/groupcast/application/Environment;->sFrequencyRequest:Landroid/os/CustomFrequencyManager$FrequencyRequest;

    invoke-virtual {v0}, Landroid/os/CustomFrequencyManager$FrequencyRequest;->cancelFrequencyRequest()V

    .line 552
    :cond_0
    monitor-exit v1

    .line 553
    return-void

    .line 552
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
