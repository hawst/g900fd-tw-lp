.class public Lcom/samsung/groupcast/application/start/JoinAPAdapter;
.super Landroid/widget/BaseAdapter;
.source "JoinAPAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/groupcast/application/start/JoinAPAdapter$WiFiInfoItem;
    }
.end annotation


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mWiFiInfoItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/groupcast/application/start/JoinAPAdapter$WiFiInfoItem;",
            ">;"
        }
    .end annotation
.end field

.field private final mWifiUtils:Lcom/samsung/groupcast/net/wifi/WifiUtils;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 122
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 22
    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiUtils;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/groupcast/application/start/JoinAPAdapter;->mWifiUtils:Lcom/samsung/groupcast/net/wifi/WifiUtils;

    .line 23
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/groupcast/application/start/JoinAPAdapter;->mWiFiInfoItems:Ljava/util/ArrayList;

    .line 123
    iput-object p1, p0, Lcom/samsung/groupcast/application/start/JoinAPAdapter;->mContext:Landroid/content/Context;

    .line 124
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/JoinAPAdapter;->mWiFiInfoItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 104
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/JoinAPAdapter;->mWiFiInfoItems:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "arg0"    # I

    .prologue
    .line 119
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 16
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 128
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/groupcast/application/start/JoinAPAdapter;->mContext:Landroid/content/Context;

    const-string v15, "layout_inflater"

    invoke-virtual {v14, v15}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/view/LayoutInflater;

    .line 131
    .local v7, "inflater":Landroid/view/LayoutInflater;
    move-object/from16 v10, p2

    .line 133
    .local v10, "row":Landroid/view/View;
    if-nez v10, :cond_0

    .line 134
    const/high16 v14, 0x7f040000

    const/4 v15, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v7, v14, v0, v15}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v10

    .line 137
    :cond_0
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/groupcast/application/start/JoinAPAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/groupcast/application/start/JoinAPAdapter$WiFiInfoItem;

    .line 138
    .local v8, "item":Lcom/samsung/groupcast/application/start/JoinAPAdapter$WiFiInfoItem;
    invoke-virtual {v8}, Lcom/samsung/groupcast/application/start/JoinAPAdapter$WiFiInfoItem;->getSSID()Ljava/lang/String;

    move-result-object v11

    .line 139
    .local v11, "ssid":Ljava/lang/String;
    invoke-virtual {v8}, Lcom/samsung/groupcast/application/start/JoinAPAdapter$WiFiInfoItem;->getWiFiSecurity()Z

    move-result v2

    .line 140
    .local v2, "IsSecured":Z
    invoke-virtual {v8}, Lcom/samsung/groupcast/application/start/JoinAPAdapter$WiFiInfoItem;->getParticipants()I

    move-result v9

    .line 141
    .local v9, "participants":I
    invoke-virtual {v8}, Lcom/samsung/groupcast/application/start/JoinAPAdapter$WiFiInfoItem;->getIcon()Landroid/graphics/Bitmap;

    move-result-object v4

    .line 142
    .local v4, "icon":Landroid/graphics/Bitmap;
    invoke-virtual {v8}, Lcom/samsung/groupcast/application/start/JoinAPAdapter$WiFiInfoItem;->isDummy()Z

    move-result v1

    .line 150
    .local v1, "IsDummy":Z
    const v14, 0x7f070004

    invoke-virtual {v10, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/TextView;

    .line 151
    .local v12, "txtv_ap_name":Landroid/widget/TextView;
    const v14, 0x7f070003

    invoke-virtual {v10, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    .line 152
    .local v5, "imgv_icon":Landroid/widget/ImageView;
    const v14, 0x7f070006

    invoke-virtual {v10, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageView;

    .line 153
    .local v6, "imgv_secured":Landroid/widget/ImageView;
    const v14, 0x7f070005

    invoke-virtual {v10, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/TextView;

    .line 157
    .local v13, "txtv_participants":Landroid/widget/TextView;
    if-eqz v4, :cond_1

    .line 158
    invoke-virtual {v8}, Lcom/samsung/groupcast/application/start/JoinAPAdapter$WiFiInfoItem;->getIcon()Landroid/graphics/Bitmap;

    move-result-object v14

    invoke-virtual {v5, v14}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 161
    :cond_1
    if-eqz v2, :cond_4

    .line 162
    const/4 v14, 0x0

    invoke-virtual {v6, v14}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 163
    const-string v14, "GroupPlay-P-"

    const-string v15, ""

    invoke-virtual {v11, v14, v15}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v11

    .line 169
    :goto_0
    invoke-virtual {v12, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 171
    if-lez v9, :cond_5

    .line 172
    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setVisibility(I)V

    .line 173
    invoke-static {v9}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 178
    :goto_1
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/groupcast/application/start/JoinAPAdapter;->mWifiUtils:Lcom/samsung/groupcast/net/wifi/WifiUtils;

    invoke-virtual {v14}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getWifiSSID()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v8, v14}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_3

    .line 179
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/groupcast/application/start/JoinAPAdapter;->mWifiUtils:Lcom/samsung/groupcast/net/wifi/WifiUtils;

    invoke-virtual {v14}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getWifiBSSID()Ljava/lang/String;

    move-result-object v14

    if-eqz v14, :cond_2

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/groupcast/application/start/JoinAPAdapter;->mWifiUtils:Lcom/samsung/groupcast/net/wifi/WifiUtils;

    invoke-virtual {v14}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getWifiBSSID()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/groupcast/application/start/JoinAPAdapter;->mWiFiInfoItems:Ljava/util/ArrayList;

    move/from16 v0, p1

    invoke-virtual {v14, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/samsung/groupcast/application/start/JoinAPAdapter$WiFiInfoItem;

    # getter for: Lcom/samsung/groupcast/application/start/JoinAPAdapter$WiFiInfoItem;->mBSSID:Ljava/lang/String;
    invoke-static {v14}, Lcom/samsung/groupcast/application/start/JoinAPAdapter$WiFiInfoItem;->access$000(Lcom/samsung/groupcast/application/start/JoinAPAdapter$WiFiInfoItem;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v15, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_3

    .line 181
    :cond_2
    const/4 v14, 0x1

    invoke-virtual {v10, v14}, Landroid/view/View;->setActivated(Z)V

    .line 182
    const/4 v14, 0x1

    invoke-virtual {v12, v14}, Landroid/widget/TextView;->setActivated(Z)V

    .line 187
    :cond_3
    const/4 v14, 0x1

    if-ne v1, v14, :cond_6

    .line 188
    const/4 v14, 0x4

    invoke-virtual {v10, v14}, Landroid/view/View;->setVisibility(I)V

    .line 194
    :goto_2
    new-instance v3, Lcom/samsung/groupcast/application/start/JoinAPAdapter$1;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lcom/samsung/groupcast/application/start/JoinAPAdapter$1;-><init>(Lcom/samsung/groupcast/application/start/JoinAPAdapter;)V

    .line 200
    .local v3, "disabledAccessibilityDelegate":Landroid/view/View$AccessibilityDelegate;
    invoke-virtual {v12, v3}, Landroid/widget/TextView;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    .line 201
    invoke-virtual {v12}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v14

    invoke-virtual {v10, v14}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 202
    return-object v10

    .line 165
    .end local v3    # "disabledAccessibilityDelegate":Landroid/view/View$AccessibilityDelegate;
    :cond_4
    const/16 v14, 0x8

    invoke-virtual {v6, v14}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 166
    const-string v14, "GroupPlay-"

    const-string v15, ""

    invoke-virtual {v11, v14, v15}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v11

    goto :goto_0

    .line 175
    :cond_5
    const/16 v14, 0x8

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    .line 190
    :cond_6
    const/4 v14, 0x0

    invoke-virtual {v10, v14}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2
.end method

.method public setItems(Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/groupcast/application/start/JoinAPAdapter$WiFiInfoItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 108
    .local p1, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/groupcast/application/start/JoinAPAdapter$WiFiInfoItem;>;"
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/JoinAPAdapter;->mWiFiInfoItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 109
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/JoinAPAdapter;->mWiFiInfoItems:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 110
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/JoinAPAdapter;->notifyDataSetChanged()V

    .line 111
    return-void
.end method

.method public setListView(Lcom/samsung/groupcast/application/start/ApListView;)V
    .locals 0
    .param p1, "mMobileAPListView"    # Lcom/samsung/groupcast/application/start/ApListView;

    .prologue
    .line 114
    invoke-virtual {p1, p0}, Lcom/samsung/groupcast/application/start/ApListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 115
    return-void
.end method
