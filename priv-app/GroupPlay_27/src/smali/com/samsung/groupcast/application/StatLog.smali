.class public Lcom/samsung/groupcast/application/StatLog;
.super Ljava/lang/Object;
.source "StatLog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/groupcast/application/StatLog$1;,
        Lcom/samsung/groupcast/application/StatLog$RequestTask;,
        Lcom/samsung/groupcast/application/StatLog$Menu;
    }
.end annotation


# static fields
.field private static final REQUEST_URL:Ljava/lang/String; = "http://bss.allshareplay.com/groupcast/one.jpg"

.field private static final STAT_FILE_NAME:Ljava/lang/String; = "com.samsung.groupcast.application.stat"

.field private static final STAT_GAME_KEY:Ljava/lang/String; = "game"

.field private static final STAT_VERSION_CODE:Ljava/lang/String; = "V"

.field private static final STAT_VERSION_KEY:Ljava/lang/String; = "version"

.field private static final TAG:Ljava/lang/String;

.field private static final URL_PREFIX_GAME:Ljava/lang/String; = "?logtype=gp27g&"

.field private static final URL_PREFIX_MENU:Ljava/lang/String; = "?logtype=gp27m&"

.field private static instance:Lcom/samsung/groupcast/application/StatLog;


# instance fields
.field private final mBuildModel:Ljava/lang/String;

.field private final mCSC:Ljava/lang/String;

.field private final mGameCounter:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final mImei:Ljava/lang/String;

.field private final mMCC:Ljava/lang/String;

.field private final mMNC:Ljava/lang/String;

.field private final mMenuCounter:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<",
            "Lcom/samsung/groupcast/application/StatLog$Menu;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mStartTime:J

.field private mVersion:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const-class v0, Lcom/samsung/groupcast/application/StatLog;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/groupcast/application/StatLog;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    .line 91
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    invoke-static {}, Lcom/samsung/groupcast/application/Environment;->getBuildModel()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/groupcast/application/StatLog;->mBuildModel:Ljava/lang/String;

    .line 43
    invoke-static {}, Lcom/samsung/groupcast/application/App;->getInstance()Lcom/samsung/groupcast/application/App;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/groupcast/application/Environment;->getMCC(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/groupcast/application/StatLog;->mMCC:Ljava/lang/String;

    .line 44
    invoke-static {}, Lcom/samsung/groupcast/application/App;->getInstance()Lcom/samsung/groupcast/application/App;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/groupcast/application/Environment;->getMNC(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/groupcast/application/StatLog;->mMNC:Ljava/lang/String;

    .line 45
    invoke-static {}, Lcom/samsung/groupcast/application/Environment;->getCSC()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/groupcast/application/StatLog;->mCSC:Ljava/lang/String;

    .line 46
    invoke-static {}, Lcom/samsung/groupcast/application/Environment;->getDeviceId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/groupcast/misc/utility/StringTools;->getSHA1Digest(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/groupcast/application/StatLog;->mImei:Ljava/lang/String;

    .line 92
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/groupcast/application/StatLog;->mMenuCounter:Ljava/util/LinkedHashMap;

    .line 93
    invoke-direct {p0}, Lcom/samsung/groupcast/application/StatLog;->initializeMenuCounter()V

    .line 94
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/groupcast/application/StatLog;->mGameCounter:Ljava/util/HashMap;

    .line 95
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/groupcast/application/StatLog;->mStartTime:J

    .line 96
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/groupcast/application/StatLog;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/groupcast/application/StatLog;

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/samsung/groupcast/application/StatLog;->clearData()V

    return-void
.end method

.method private clearData()V
    .locals 2

    .prologue
    .line 265
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/groupcast/application/StatLog;->mStartTime:J

    .line 266
    invoke-direct {p0}, Lcom/samsung/groupcast/application/StatLog;->initializeMenuCounter()V

    .line 267
    iget-object v0, p0, Lcom/samsung/groupcast/application/StatLog;->mGameCounter:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 268
    invoke-direct {p0}, Lcom/samsung/groupcast/application/StatLog;->getSharedPreferencesEditor()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 269
    return-void
.end method

.method public static getInstance()Lcom/samsung/groupcast/application/StatLog;
    .locals 2

    .prologue
    .line 99
    sget-object v0, Lcom/samsung/groupcast/application/StatLog;->instance:Lcom/samsung/groupcast/application/StatLog;

    if-nez v0, :cond_0

    .line 100
    sget-object v0, Lcom/samsung/groupcast/application/StatLog;->TAG:Ljava/lang/String;

    const-string v1, "StatLog - new instance"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 101
    new-instance v0, Lcom/samsung/groupcast/application/StatLog;

    invoke-direct {v0}, Lcom/samsung/groupcast/application/StatLog;-><init>()V

    sput-object v0, Lcom/samsung/groupcast/application/StatLog;->instance:Lcom/samsung/groupcast/application/StatLog;

    .line 103
    :cond_0
    sget-object v0, Lcom/samsung/groupcast/application/StatLog;->instance:Lcom/samsung/groupcast/application/StatLog;

    return-object v0
.end method

.method private getRequestUrl()[Ljava/lang/String;
    .locals 10

    .prologue
    const/16 v9, 0x26

    const/16 v8, 0x3d

    .line 280
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 282
    .local v5, "urls":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    .line 283
    .local v4, "sb":Ljava/lang/StringBuffer;
    const-string v6, "http://bss.allshareplay.com/groupcast/one.jpg"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    const-string v7, "?logtype=gp27m&"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 284
    iget-object v6, p0, Lcom/samsung/groupcast/application/StatLog;->mMenuCounter:Ljava/util/LinkedHashMap;

    invoke-virtual {v6}, Ljava/util/LinkedHashMap;->entrySet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 285
    .local v0, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lcom/samsung/groupcast/application/StatLog$Menu;Ljava/lang/Integer;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    if-lez v6, :cond_0

    .line 286
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/groupcast/application/StatLog$Menu;

    invoke-virtual {v6}, Lcom/samsung/groupcast/application/StatLog$Menu;->getCode()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6, v8}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6, v9}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 289
    .end local v0    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lcom/samsung/groupcast/application/StatLog$Menu;Ljava/lang/Integer;>;"
    :cond_1
    const-string v6, "V"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6, v8}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v6

    iget-object v7, p0, Lcom/samsung/groupcast/application/StatLog;->mVersion:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 291
    const-string v6, "&deviceId="

    invoke-virtual {v4, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    iget-object v6, p0, Lcom/samsung/groupcast/application/StatLog;->mBuildModel:Ljava/lang/String;

    if-eqz v6, :cond_3

    iget-object v6, p0, Lcom/samsung/groupcast/application/StatLog;->mBuildModel:Ljava/lang/String;

    :goto_1
    invoke-virtual {v7, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 292
    const-string v6, "&mcc="

    invoke-virtual {v4, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    iget-object v6, p0, Lcom/samsung/groupcast/application/StatLog;->mMCC:Ljava/lang/String;

    if-eqz v6, :cond_4

    iget-object v6, p0, Lcom/samsung/groupcast/application/StatLog;->mMCC:Ljava/lang/String;

    :goto_2
    invoke-virtual {v7, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 293
    const-string v6, "&mnc="

    invoke-virtual {v4, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    iget-object v6, p0, Lcom/samsung/groupcast/application/StatLog;->mMNC:Ljava/lang/String;

    if-eqz v6, :cond_5

    iget-object v6, p0, Lcom/samsung/groupcast/application/StatLog;->mMNC:Ljava/lang/String;

    :goto_3
    invoke-virtual {v7, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 294
    const-string v6, "&csc="

    invoke-virtual {v4, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    iget-object v6, p0, Lcom/samsung/groupcast/application/StatLog;->mCSC:Ljava/lang/String;

    if-eqz v6, :cond_6

    iget-object v6, p0, Lcom/samsung/groupcast/application/StatLog;->mCSC:Ljava/lang/String;

    :goto_4
    invoke-virtual {v7, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 295
    const-string v6, "&hid="

    invoke-virtual {v4, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    iget-object v7, p0, Lcom/samsung/groupcast/application/StatLog;->mImei:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 296
    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 299
    iget-object v6, p0, Lcom/samsung/groupcast/application/StatLog;->mGameCounter:Ljava/util/HashMap;

    invoke-virtual {v6}, Ljava/util/HashMap;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_8

    .line 300
    new-instance v4, Ljava/lang/StringBuffer;

    .end local v4    # "sb":Ljava/lang/StringBuffer;
    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    .line 301
    .restart local v4    # "sb":Ljava/lang/StringBuffer;
    const-string v6, "http://bss.allshareplay.com/groupcast/one.jpg"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    const-string v7, "?logtype=gp27g&"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 302
    iget-object v6, p0, Lcom/samsung/groupcast/application/StatLog;->mGameCounter:Ljava/util/HashMap;

    invoke-virtual {v6}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_5
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 303
    .local v1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    if-lez v6, :cond_2

    .line 304
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-virtual {v4, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6, v8}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6, v9}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_5

    .line 291
    .end local v1    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;"
    :cond_3
    const-string v6, ""

    goto/16 :goto_1

    .line 292
    :cond_4
    const-string v6, ""

    goto/16 :goto_2

    .line 293
    :cond_5
    const-string v6, ""

    goto/16 :goto_3

    .line 294
    :cond_6
    const-string v6, ""

    goto :goto_4

    .line 307
    :cond_7
    invoke-virtual {v4}, Ljava/lang/StringBuffer;->length()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    invoke-virtual {v4, v6}, Ljava/lang/StringBuffer;->deleteCharAt(I)Ljava/lang/StringBuffer;

    .line 308
    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 310
    :cond_8
    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_6
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_9

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 311
    .local v3, "s":Ljava/lang/String;
    sget-object v6, Lcom/samsung/groupcast/application/StatLog;->TAG:Ljava/lang/String;

    invoke-static {v6, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_6

    .line 313
    .end local v3    # "s":Ljava/lang/String;
    :cond_9
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v6

    new-array v6, v6, [Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [Ljava/lang/String;

    return-object v6
.end method

.method private getSharedPreferences()Landroid/content/SharedPreferences;
    .locals 3

    .prologue
    .line 272
    invoke-static {}, Lcom/samsung/groupcast/application/App;->getInstance()Lcom/samsung/groupcast/application/App;

    move-result-object v0

    const-string v1, "com.samsung.groupcast.application.stat"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/samsung/groupcast/application/App;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    return-object v0
.end method

.method private getSharedPreferencesEditor()Landroid/content/SharedPreferences$Editor;
    .locals 1

    .prologue
    .line 276
    invoke-direct {p0}, Lcom/samsung/groupcast/application/StatLog;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    return-object v0
.end method

.method private initializeMenuCounter()V
    .locals 6

    .prologue
    .line 193
    invoke-static {}, Lcom/samsung/groupcast/application/StatLog$Menu;->values()[Lcom/samsung/groupcast/application/StatLog$Menu;

    move-result-object v0

    .local v0, "arr$":[Lcom/samsung/groupcast/application/StatLog$Menu;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 194
    .local v3, "menu":Lcom/samsung/groupcast/application/StatLog$Menu;
    iget-object v4, p0, Lcom/samsung/groupcast/application/StatLog;->mMenuCounter:Ljava/util/LinkedHashMap;

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v3, v5}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 193
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 196
    .end local v3    # "menu":Lcom/samsung/groupcast/application/StatLog$Menu;
    :cond_0
    return-void
.end method

.method private printGameStat()V
    .locals 5

    .prologue
    .line 204
    iget-object v2, p0, Lcom/samsung/groupcast/application/StatLog;->mGameCounter:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 205
    .local v0, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;"
    sget-object v3, Lcom/samsung/groupcast/application/StatLog;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Key: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "; Counter: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 207
    .end local v0    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;"
    :cond_0
    return-void
.end method

.method private setStartTime(J)V
    .locals 4
    .param p1, "l"    # J

    .prologue
    .line 199
    iput-wide p1, p0, Lcom/samsung/groupcast/application/StatLog;->mStartTime:J

    .line 200
    sget-object v0, Lcom/samsung/groupcast/application/StatLog;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "start time: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/samsung/groupcast/application/StatLog;->mStartTime:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 201
    return-void
.end method


# virtual methods
.method public countGame(Ljava/lang/String;)V
    .locals 3
    .param p1, "pkgName"    # Ljava/lang/String;

    .prologue
    .line 184
    const/4 v0, 0x0

    .line 185
    .local v0, "i":I
    iget-object v1, p0, Lcom/samsung/groupcast/application/StatLog;->mGameCounter:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 186
    iget-object v1, p0, Lcom/samsung/groupcast/application/StatLog;->mGameCounter:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 188
    :cond_0
    iget-object v1, p0, Lcom/samsung/groupcast/application/StatLog;->mGameCounter:Ljava/util/HashMap;

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, p1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 189
    invoke-direct {p0}, Lcom/samsung/groupcast/application/StatLog;->printGameStat()V

    .line 190
    return-void
.end method

.method public countMaxPage(Lcom/samsung/groupcast/application/StatLog$Menu;I)V
    .locals 4
    .param p1, "menuItem"    # Lcom/samsung/groupcast/application/StatLog$Menu;
    .param p2, "currentPages"    # I

    .prologue
    .line 175
    iget-object v1, p0, Lcom/samsung/groupcast/application/StatLog;->mMenuCounter:Ljava/util/LinkedHashMap;

    invoke-virtual {v1, p1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 176
    .local v0, "i":I
    if-ge v0, p2, :cond_0

    .line 177
    iget-object v1, p0, Lcom/samsung/groupcast/application/StatLog;->mMenuCounter:Ljava/util/LinkedHashMap;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, p1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 179
    :cond_0
    sget-object v1, Lcom/samsung/groupcast/application/StatLog;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "MAX "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 180
    return-void
.end method

.method public countMenu(I)V
    .locals 0
    .param p1, "key"    # I

    .prologue
    .line 127
    return-void
.end method

.method public countMenu(Lcom/samsung/groupcast/application/StatLog$Menu;)V
    .locals 3
    .param p1, "menuItem"    # Lcom/samsung/groupcast/application/StatLog$Menu;

    .prologue
    .line 139
    iget-object v1, p0, Lcom/samsung/groupcast/application/StatLog;->mMenuCounter:Ljava/util/LinkedHashMap;

    invoke-virtual {v1, p1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 141
    .local v0, "i":I
    sget-object v1, Lcom/samsung/groupcast/application/StatLog$1;->$SwitchMap$com$samsung$groupcast$application$StatLog$Menu:[I

    invoke-virtual {p1}, Lcom/samsung/groupcast/application/StatLog$Menu;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 170
    :goto_0
    iget-object v1, p0, Lcom/samsung/groupcast/application/StatLog;->mMenuCounter:Ljava/util/LinkedHashMap;

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, p1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 172
    return-void

    .line 144
    :pswitch_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-direct {p0, v1, v2}, Lcom/samsung/groupcast/application/StatLog;->setStartTime(J)V

    .line 168
    :pswitch_1
    invoke-static {}, Lcom/samsung/groupcast/application/StatBigDataLog;->getInstance()Lcom/samsung/groupcast/application/StatBigDataLog;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/samsung/groupcast/application/StatBigDataLog;->pushLog(Lcom/samsung/groupcast/application/StatLog$Menu;)V

    goto :goto_0

    .line 141
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public openAndSendData()V
    .locals 10

    .prologue
    const/4 v9, 0x0

    .line 238
    invoke-direct {p0}, Lcom/samsung/groupcast/application/StatLog;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v6

    .line 239
    .local v6, "sharedPreferences":Landroid/content/SharedPreferences;
    const-string v7, "version"

    const-string v8, ""

    invoke-interface {v6, v7, v8}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/samsung/groupcast/application/StatLog;->mVersion:Ljava/lang/String;

    .line 240
    iget-object v7, p0, Lcom/samsung/groupcast/application/StatLog;->mVersion:Ljava/lang/String;

    invoke-virtual {v7}, Ljava/lang/String;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 241
    invoke-static {}, Lcom/samsung/groupcast/application/App;->getVersionName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, Lcom/samsung/groupcast/application/StatLog;->setVersion(Ljava/lang/String;)V

    .line 262
    :goto_0
    return-void

    .line 244
    :cond_0
    iget-object v7, p0, Lcom/samsung/groupcast/application/StatLog;->mMenuCounter:Ljava/util/LinkedHashMap;

    invoke-virtual {v7}, Ljava/util/LinkedHashMap;->entrySet()Ljava/util/Set;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 245
    .local v0, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lcom/samsung/groupcast/application/StatLog$Menu;Ljava/lang/Integer;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/groupcast/application/StatLog$Menu;

    .line 246
    .local v4, "key":Lcom/samsung/groupcast/application/StatLog$Menu;
    iget-object v7, p0, Lcom/samsung/groupcast/application/StatLog;->mMenuCounter:Ljava/util/LinkedHashMap;

    invoke-virtual {v4}, Lcom/samsung/groupcast/application/StatLog$Menu;->getCode()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v6, v8, v9}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v7, v4, v8}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 249
    .end local v0    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lcom/samsung/groupcast/application/StatLog$Menu;Ljava/lang/Integer;>;"
    .end local v4    # "key":Lcom/samsung/groupcast/application/StatLog$Menu;
    :cond_1
    const-string v7, "game"

    const/4 v8, 0x0

    invoke-interface {v6, v7, v8}, Landroid/content/SharedPreferences;->getStringSet(Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v5

    .line 250
    .local v5, "set":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    if-eqz v5, :cond_2

    invoke-interface {v5}, Ljava/util/Set;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_2

    .line 251
    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 253
    .local v3, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    .line 254
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 255
    .local v1, "gameName":Ljava/lang/String;
    iget-object v7, p0, Lcom/samsung/groupcast/application/StatLog;->mGameCounter:Ljava/util/HashMap;

    invoke-interface {v6, v1, v9}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v7, v1, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 258
    .end local v1    # "gameName":Ljava/lang/String;
    .end local v3    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :cond_2
    invoke-direct {p0}, Lcom/samsung/groupcast/application/StatLog;->printGameStat()V

    .line 260
    new-instance v7, Lcom/samsung/groupcast/application/StatLog$RequestTask;

    invoke-direct {v7, p0}, Lcom/samsung/groupcast/application/StatLog$RequestTask;-><init>(Lcom/samsung/groupcast/application/StatLog;)V

    sget-object v8, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    invoke-direct {p0}, Lcom/samsung/groupcast/application/StatLog;->getRequestUrl()[Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Lcom/samsung/groupcast/application/StatLog$RequestTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method public saveData()V
    .locals 9

    .prologue
    .line 210
    invoke-direct {p0}, Lcom/samsung/groupcast/application/StatLog;->getSharedPreferencesEditor()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    .line 211
    .local v4, "sharedPreferencesEditor":Landroid/content/SharedPreferences$Editor;
    iget-object v5, p0, Lcom/samsung/groupcast/application/StatLog;->mMenuCounter:Ljava/util/LinkedHashMap;

    invoke-virtual {v5}, Ljava/util/LinkedHashMap;->entrySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 212
    .local v1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lcom/samsung/groupcast/application/StatLog$Menu;Ljava/lang/Integer;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    sget-object v6, Lcom/samsung/groupcast/application/StatLog$Menu;->TIME:Lcom/samsung/groupcast/application/StatLog$Menu;

    if-eq v5, v6, :cond_0

    .line 213
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/groupcast/application/StatLog$Menu;

    invoke-virtual {v5}, Lcom/samsung/groupcast/application/StatLog$Menu;->getCode()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-interface {v4, v6, v5}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    invoke-interface {v5}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0

    .line 215
    :cond_0
    const/4 v0, 0x0

    .line 216
    .local v0, "currentTime":I
    iget-wide v5, p0, Lcom/samsung/groupcast/application/StatLog;->mStartTime:J

    const-wide/16 v7, 0x0

    cmp-long v5, v5, v7

    if-eqz v5, :cond_1

    .line 217
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    iget-wide v7, p0, Lcom/samsung/groupcast/application/StatLog;->mStartTime:J

    sub-long/2addr v5, v7

    const-wide/16 v7, 0x3e8

    div-long/2addr v5, v7

    long-to-int v0, v5

    .line 218
    sget-object v5, Lcom/samsung/groupcast/application/StatLog;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "time diff "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 221
    :cond_1
    sget-object v5, Lcom/samsung/groupcast/application/StatLog$Menu;->TIME:Lcom/samsung/groupcast/application/StatLog$Menu;

    invoke-virtual {v5}, Lcom/samsung/groupcast/application/StatLog$Menu;->getCode()Ljava/lang/String;

    move-result-object v6

    iget-object v5, p0, Lcom/samsung/groupcast/application/StatLog;->mMenuCounter:Ljava/util/LinkedHashMap;

    sget-object v7, Lcom/samsung/groupcast/application/StatLog$Menu;->TIME:Lcom/samsung/groupcast/application/StatLog$Menu;

    invoke-virtual {v5, v7}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    add-int/2addr v5, v0

    invoke-interface {v4, v6, v5}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    goto :goto_0

    .line 226
    .end local v0    # "currentTime":I
    .end local v1    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lcom/samsung/groupcast/application/StatLog$Menu;Ljava/lang/Integer;>;"
    :cond_2
    const-string v5, "version"

    iget-object v6, p0, Lcom/samsung/groupcast/application/StatLog;->mVersion:Ljava/lang/String;

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    invoke-interface {v5}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 229
    iget-object v5, p0, Lcom/samsung/groupcast/application/StatLog;->mGameCounter:Ljava/util/HashMap;

    invoke-virtual {v5}, Ljava/util/HashMap;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_3

    .line 230
    iget-object v5, p0, Lcom/samsung/groupcast/application/StatLog;->mGameCounter:Ljava/util/HashMap;

    invoke-virtual {v5}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 231
    .local v2, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;"
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    invoke-interface {v5}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_1

    .line 234
    .end local v2    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;"
    :cond_3
    const-string v5, "game"

    iget-object v6, p0, Lcom/samsung/groupcast/application/StatLog;->mGameCounter:Ljava/util/HashMap;

    invoke-virtual {v6}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences$Editor;->putStringSet(Ljava/lang/String;Ljava/util/Set;)Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    invoke-interface {v5}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 235
    return-void
.end method

.method public setVersion(Ljava/lang/String;)V
    .locals 0
    .param p1, "version"    # Ljava/lang/String;

    .prologue
    .line 107
    iput-object p1, p0, Lcom/samsung/groupcast/application/StatLog;->mVersion:Ljava/lang/String;

    .line 108
    return-void
.end method

.method public updateMaxUser(I)V
    .locals 4
    .param p1, "count"    # I

    .prologue
    .line 130
    if-lez p1, :cond_0

    .line 131
    :cond_0
    iget-object v1, p0, Lcom/samsung/groupcast/application/StatLog;->mMenuCounter:Ljava/util/LinkedHashMap;

    sget-object v2, Lcom/samsung/groupcast/application/StatLog$Menu;->MAX_USER:Lcom/samsung/groupcast/application/StatLog$Menu;

    invoke-virtual {v1, v2}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 132
    .local v0, "i":I
    if-ge v0, p1, :cond_1

    .line 133
    iget-object v1, p0, Lcom/samsung/groupcast/application/StatLog;->mMenuCounter:Ljava/util/LinkedHashMap;

    sget-object v2, Lcom/samsung/groupcast/application/StatLog$Menu;->MAX_USER:Lcom/samsung/groupcast/application/StatLog$Menu;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 135
    :cond_1
    sget-object v1, Lcom/samsung/groupcast/application/StatLog;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Menu.MAX_USER "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 136
    return-void
.end method
