.class Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment$1;
.super Ljava/lang/Object;
.source "PinCodeInputDialogFragment.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;


# direct methods
.method constructor <init>(Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;)V
    .locals 0

    .prologue
    .line 109
    iput-object p1, p0, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment$1;->this$0:Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 5
    .param p1, "buttonView"    # Landroid/widget/CompoundButton;
    .param p2, "isChecked"    # Z

    .prologue
    .line 115
    iget-object v2, p0, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment$1;->this$0:Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;

    invoke-virtual {v2}, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v2

    if-nez v2, :cond_1

    .line 135
    :cond_0
    :goto_0
    return-void

    .line 119
    :cond_1
    iget-object v2, p0, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment$1;->this$0:Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;

    # getter for: Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->mPinEditText:Landroid/widget/EditText;
    invoke-static {v2}, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->access$000(Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;)Landroid/widget/EditText;

    move-result-object v2

    if-nez v2, :cond_2

    .line 120
    iget-object v3, p0, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment$1;->this$0:Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;

    iget-object v2, p0, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment$1;->this$0:Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;

    invoke-virtual {v2}, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v2

    const v4, 0x7f070024

    invoke-virtual {v2, v4}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/EditText;

    # setter for: Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->mPinEditText:Landroid/widget/EditText;
    invoke-static {v3, v2}, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->access$002(Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;Landroid/widget/EditText;)Landroid/widget/EditText;

    .line 123
    :cond_2
    iget-object v2, p0, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment$1;->this$0:Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;

    # getter for: Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->mPinEditText:Landroid/widget/EditText;
    invoke-static {v2}, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->access$000(Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;)Landroid/widget/EditText;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 124
    iget-object v2, p0, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment$1;->this$0:Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;

    # getter for: Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->mPinEditText:Landroid/widget/EditText;
    invoke-static {v2}, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->access$000(Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;)Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/EditText;->getSelectionStart()I

    move-result v1

    .line 125
    .local v1, "selectionStart":I
    iget-object v2, p0, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment$1;->this$0:Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;

    # getter for: Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->mPinEditText:Landroid/widget/EditText;
    invoke-static {v2}, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->access$000(Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;)Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/EditText;->getSelectionEnd()I

    move-result v0

    .line 127
    .local v0, "selectionEnd":I
    if-eqz p2, :cond_3

    .line 128
    iget-object v2, p0, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment$1;->this$0:Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;

    # getter for: Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->mPinEditText:Landroid/widget/EditText;
    invoke-static {v2}, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->access$000(Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;)Landroid/widget/EditText;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    .line 133
    :goto_1
    iget-object v2, p0, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment$1;->this$0:Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;

    # getter for: Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->mPinEditText:Landroid/widget/EditText;
    invoke-static {v2}, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->access$000(Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;)Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v2, v1, v0}, Landroid/widget/EditText;->setSelection(II)V

    goto :goto_0

    .line 130
    :cond_3
    iget-object v2, p0, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment$1;->this$0:Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;

    # getter for: Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->mPinEditText:Landroid/widget/EditText;
    invoke-static {v2}, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->access$000(Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;)Landroid/widget/EditText;

    move-result-object v2

    new-instance v3, Landroid/text/method/PasswordTransformationMethod;

    invoke-direct {v3}, Landroid/text/method/PasswordTransformationMethod;-><init>()V

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    goto :goto_1
.end method
