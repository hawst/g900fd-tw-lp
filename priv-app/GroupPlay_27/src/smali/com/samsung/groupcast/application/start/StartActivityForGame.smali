.class public Lcom/samsung/groupcast/application/start/StartActivityForGame;
.super Lcom/samsung/groupcast/application/start/StartActivity;
.source "StartActivityForGame.java"


# static fields
.field private static final TAG_LICENSE_DIALOG_FRAGMENT:Ljava/lang/String; = "LICENS_DIALOG_FRAGMENT"


# instance fields
.field private licenses:Landroid/widget/TextView;

.field private mLicenseDialogFragment:Lcom/samsung/groupcast/application/fragment/LicenseDialogFragment;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/samsung/groupcast/application/start/StartActivity;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/samsung/groupcast/application/start/StartActivityForGame;)Lcom/samsung/groupcast/application/fragment/LicenseDialogFragment;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/application/start/StartActivityForGame;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/StartActivityForGame;->mLicenseDialogFragment:Lcom/samsung/groupcast/application/fragment/LicenseDialogFragment;

    return-object v0
.end method

.method static synthetic access$002(Lcom/samsung/groupcast/application/start/StartActivityForGame;Lcom/samsung/groupcast/application/fragment/LicenseDialogFragment;)Lcom/samsung/groupcast/application/fragment/LicenseDialogFragment;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/groupcast/application/start/StartActivityForGame;
    .param p1, "x1"    # Lcom/samsung/groupcast/application/fragment/LicenseDialogFragment;

    .prologue
    .line 27
    iput-object p1, p0, Lcom/samsung/groupcast/application/start/StartActivityForGame;->mLicenseDialogFragment:Lcom/samsung/groupcast/application/fragment/LicenseDialogFragment;

    return-object p1
.end method


# virtual methods
.method public mOnClick(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 114
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 115
    .local v0, "id":I
    packed-switch v0, :pswitch_data_0

    .line 123
    :goto_0
    return-void

    .line 117
    :pswitch_0
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivityForGame;->createGroupHandler()V

    goto :goto_0

    .line 115
    :pswitch_data_0
    .packed-switch 0x7f070011
        :pswitch_0
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v5, 0x0

    .line 36
    invoke-super {p0, p1}, Lcom/samsung/groupcast/application/start/StartActivity;->onCreate(Landroid/os/Bundle;)V

    .line 38
    invoke-static {}, Lcom/samsung/groupcast/application/Preferences;->getDisclimerPreference()Z

    move-result v3

    if-nez v3, :cond_0

    .line 39
    const/4 v3, 0x1

    invoke-static {p0, v3}, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->showIfNeeded(Landroid/app/Activity;I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 41
    iget v3, p0, Lcom/samsung/groupcast/application/start/StartActivityForGame;->mHowManyDisclaimerShowing:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/samsung/groupcast/application/start/StartActivityForGame;->mHowManyDisclaimerShowing:I

    .line 45
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivityForGame;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "APPLICATION_INTERFACE_IS_START_FROM_GPSDK"

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 46
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivityForGame;->getWindow()Landroid/view/Window;

    move-result-object v2

    .line 48
    .local v2, "window":Landroid/view/Window;
    const/16 v3, 0x11

    invoke-virtual {v2, v3}, Landroid/view/Window;->setGravity(I)V

    .line 50
    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Landroid/view/Window;->addFlags(I)V

    .line 51
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/view/Window;->setDimAmount(F)V

    .line 53
    iget-object v3, p0, Lcom/samsung/groupcast/application/start/StartActivityForGame;->contentView:Landroid/view/View;

    const v4, 0x7f070036

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    .line 54
    iget-object v3, p0, Lcom/samsung/groupcast/application/start/StartActivityForGame;->contentView:Landroid/view/View;

    const v4, 0x7f070038

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/view/View;->setVisibility(I)V

    .line 56
    iget-object v3, p0, Lcom/samsung/groupcast/application/start/StartActivityForGame;->contentView:Landroid/view/View;

    const v4, 0x7f07003a

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/view/View;->setVisibility(I)V

    .line 58
    iget-object v3, p0, Lcom/samsung/groupcast/application/start/StartActivityForGame;->contentView:Landroid/view/View;

    const v4, 0x7f070043

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/samsung/groupcast/application/start/StartActivityForGame;->licenses:Landroid/widget/TextView;

    .line 60
    :try_start_0
    new-instance v1, Landroid/text/SpannableString;

    iget-object v3, p0, Lcom/samsung/groupcast/application/start/StartActivityForGame;->licenses:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-direct {v1, v3}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 61
    .local v1, "mySpannableString":Landroid/text/SpannableString;
    new-instance v3, Landroid/text/style/UnderlineSpan;

    invoke-direct {v3}, Landroid/text/style/UnderlineSpan;-><init>()V

    const/4 v4, 0x0

    invoke-virtual {v1}, Landroid/text/SpannableString;->length()I

    move-result v5

    const/4 v6, 0x0

    invoke-virtual {v1, v3, v4, v5, v6}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 62
    iget-object v3, p0, Lcom/samsung/groupcast/application/start/StartActivityForGame;->licenses:Landroid/widget/TextView;

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 67
    .end local v1    # "mySpannableString":Landroid/text/SpannableString;
    :goto_0
    iget-object v3, p0, Lcom/samsung/groupcast/application/start/StartActivityForGame;->licenses:Landroid/widget/TextView;

    new-instance v4, Lcom/samsung/groupcast/application/start/StartActivityForGame$1;

    invoke-direct {v4, p0}, Lcom/samsung/groupcast/application/start/StartActivityForGame$1;-><init>(Lcom/samsung/groupcast/application/start/StartActivityForGame;)V

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 81
    iget-object v3, p0, Lcom/samsung/groupcast/application/start/StartActivityForGame;->contentView:Landroid/view/View;

    invoke-virtual {v2, v3}, Landroid/view/Window;->setContentView(Landroid/view/View;)V

    .line 84
    .end local v2    # "window":Landroid/view/Window;
    :cond_1
    return-void

    .line 63
    .restart local v2    # "window":Landroid/view/Window;
    :catch_0
    move-exception v0

    .line 64
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 100
    invoke-super {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->onPause()V

    .line 102
    const-string v1, "power"

    invoke-virtual {p0, v1}, Lcom/samsung/groupcast/application/start/StartActivityForGame;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 103
    .local v0, "powerManager":Landroid/os/PowerManager;
    invoke-virtual {v0}, Landroid/os/PowerManager;->isScreenOn()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivityForGame;->isFinishing()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 104
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivityForGame;->finish()V

    .line 105
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 112
    :cond_0
    :goto_0
    return-void

    .line 109
    :cond_1
    iget-object v1, p0, Lcom/samsung/groupcast/application/start/StartActivityForGame;->mLicenseDialogFragment:Lcom/samsung/groupcast/application/fragment/LicenseDialogFragment;

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivityForGame;->isFinishing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 110
    iget-object v1, p0, Lcom/samsung/groupcast/application/start/StartActivityForGame;->mLicenseDialogFragment:Lcom/samsung/groupcast/application/fragment/LicenseDialogFragment;

    invoke-virtual {v1}, Lcom/samsung/groupcast/application/fragment/LicenseDialogFragment;->dismiss()V

    goto :goto_0
.end method

.method protected onResume()V
    .locals 3

    .prologue
    .line 89
    invoke-super {p0}, Lcom/samsung/groupcast/application/start/StartActivity;->onResume()V

    .line 90
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivityForGame;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "EXIT"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 91
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/StartActivityForGame;->finish()V

    .line 95
    :cond_0
    return-void
.end method
