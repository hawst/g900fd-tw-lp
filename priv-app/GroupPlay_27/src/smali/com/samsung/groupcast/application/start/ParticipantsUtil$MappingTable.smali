.class public final enum Lcom/samsung/groupcast/application/start/ParticipantsUtil$MappingTable;
.super Ljava/lang/Enum;
.source "ParticipantsUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/groupcast/application/start/ParticipantsUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "MappingTable"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/groupcast/application/start/ParticipantsUtil$MappingTable;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/groupcast/application/start/ParticipantsUtil$MappingTable;

.field public static final enum App:Lcom/samsung/groupcast/application/start/ParticipantsUtil$MappingTable;

.field public static final enum Default:Lcom/samsung/groupcast/application/start/ParticipantsUtil$MappingTable;

.field public static final enum Document:Lcom/samsung/groupcast/application/start/ParticipantsUtil$MappingTable;

.field public static final enum Game:Lcom/samsung/groupcast/application/start/ParticipantsUtil$MappingTable;

.field public static final enum GroupCamcorder:Lcom/samsung/groupcast/application/start/ParticipantsUtil$MappingTable;

.field public static final enum Image:Lcom/samsung/groupcast/application/start/ParticipantsUtil$MappingTable;

.field public static final enum Music:Lcom/samsung/groupcast/application/start/ParticipantsUtil$MappingTable;

.field public static final enum Video:Lcom/samsung/groupcast/application/start/ParticipantsUtil$MappingTable;


# instance fields
.field private mID:Ljava/lang/String;

.field private mIcon:Ljava/lang/Byte;

.field private mResID:I


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .prologue
    const/4 v15, 0x3

    const/4 v14, 0x2

    const/4 v7, 0x1

    const/4 v2, 0x0

    const v4, 0x7f020015

    .line 40
    new-instance v0, Lcom/samsung/groupcast/application/start/ParticipantsUtil$MappingTable;

    const-string v1, "Default"

    sget-object v3, Lcom/samsung/groupcast/application/start/ParticipantsUtil;->ICON_DEFAULT:Ljava/lang/Byte;

    const-string v5, "Defualt"

    invoke-direct/range {v0 .. v5}, Lcom/samsung/groupcast/application/start/ParticipantsUtil$MappingTable;-><init>(Ljava/lang/String;ILjava/lang/Byte;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/groupcast/application/start/ParticipantsUtil$MappingTable;->Default:Lcom/samsung/groupcast/application/start/ParticipantsUtil$MappingTable;

    .line 41
    new-instance v5, Lcom/samsung/groupcast/application/start/ParticipantsUtil$MappingTable;

    const-string v6, "Music"

    sget-object v8, Lcom/samsung/groupcast/application/start/ParticipantsUtil;->ICON_MUSIC:Ljava/lang/Byte;

    const-string v10, "MusicLiveShare"

    move v9, v4

    invoke-direct/range {v5 .. v10}, Lcom/samsung/groupcast/application/start/ParticipantsUtil$MappingTable;-><init>(Ljava/lang/String;ILjava/lang/Byte;ILjava/lang/String;)V

    sput-object v5, Lcom/samsung/groupcast/application/start/ParticipantsUtil$MappingTable;->Music:Lcom/samsung/groupcast/application/start/ParticipantsUtil$MappingTable;

    .line 42
    new-instance v8, Lcom/samsung/groupcast/application/start/ParticipantsUtil$MappingTable;

    const-string v9, "Image"

    sget-object v11, Lcom/samsung/groupcast/application/start/ParticipantsUtil;->ICON_IMAGE:Ljava/lang/Byte;

    const-string v13, "MyImages"

    move v10, v14

    move v12, v4

    invoke-direct/range {v8 .. v13}, Lcom/samsung/groupcast/application/start/ParticipantsUtil$MappingTable;-><init>(Ljava/lang/String;ILjava/lang/Byte;ILjava/lang/String;)V

    sput-object v8, Lcom/samsung/groupcast/application/start/ParticipantsUtil$MappingTable;->Image:Lcom/samsung/groupcast/application/start/ParticipantsUtil$MappingTable;

    .line 43
    new-instance v8, Lcom/samsung/groupcast/application/start/ParticipantsUtil$MappingTable;

    const-string v9, "Video"

    sget-object v11, Lcom/samsung/groupcast/application/start/ParticipantsUtil;->ICON_VIDEO:Ljava/lang/Byte;

    const-string v13, "ShareVideo"

    move v10, v15

    move v12, v4

    invoke-direct/range {v8 .. v13}, Lcom/samsung/groupcast/application/start/ParticipantsUtil$MappingTable;-><init>(Ljava/lang/String;ILjava/lang/Byte;ILjava/lang/String;)V

    sput-object v8, Lcom/samsung/groupcast/application/start/ParticipantsUtil$MappingTable;->Video:Lcom/samsung/groupcast/application/start/ParticipantsUtil$MappingTable;

    .line 44
    new-instance v8, Lcom/samsung/groupcast/application/start/ParticipantsUtil$MappingTable;

    const-string v9, "Document"

    const/4 v10, 0x4

    sget-object v11, Lcom/samsung/groupcast/application/start/ParticipantsUtil;->ICON_DOCUMENT:Ljava/lang/Byte;

    const-string v13, "MyDocumentPages"

    move v12, v4

    invoke-direct/range {v8 .. v13}, Lcom/samsung/groupcast/application/start/ParticipantsUtil$MappingTable;-><init>(Ljava/lang/String;ILjava/lang/Byte;ILjava/lang/String;)V

    sput-object v8, Lcom/samsung/groupcast/application/start/ParticipantsUtil$MappingTable;->Document:Lcom/samsung/groupcast/application/start/ParticipantsUtil$MappingTable;

    .line 45
    new-instance v8, Lcom/samsung/groupcast/application/start/ParticipantsUtil$MappingTable;

    const-string v9, "GroupCamcorder"

    const/4 v10, 0x5

    sget-object v11, Lcom/samsung/groupcast/application/start/ParticipantsUtil;->ICON_GROUPCAMCORDER:Ljava/lang/Byte;

    const-string v13, "GroupCamcoder"

    move v12, v4

    invoke-direct/range {v8 .. v13}, Lcom/samsung/groupcast/application/start/ParticipantsUtil$MappingTable;-><init>(Ljava/lang/String;ILjava/lang/Byte;ILjava/lang/String;)V

    sput-object v8, Lcom/samsung/groupcast/application/start/ParticipantsUtil$MappingTable;->GroupCamcorder:Lcom/samsung/groupcast/application/start/ParticipantsUtil$MappingTable;

    .line 46
    new-instance v8, Lcom/samsung/groupcast/application/start/ParticipantsUtil$MappingTable;

    const-string v9, "Game"

    const/4 v10, 0x6

    sget-object v11, Lcom/samsung/groupcast/application/start/ParticipantsUtil;->ICON_GAME:Ljava/lang/Byte;

    const-string v13, "MusicLiveShare"

    move v12, v4

    invoke-direct/range {v8 .. v13}, Lcom/samsung/groupcast/application/start/ParticipantsUtil$MappingTable;-><init>(Ljava/lang/String;ILjava/lang/Byte;ILjava/lang/String;)V

    sput-object v8, Lcom/samsung/groupcast/application/start/ParticipantsUtil$MappingTable;->Game:Lcom/samsung/groupcast/application/start/ParticipantsUtil$MappingTable;

    .line 47
    new-instance v8, Lcom/samsung/groupcast/application/start/ParticipantsUtil$MappingTable;

    const-string v9, "App"

    const/4 v10, 0x7

    sget-object v11, Lcom/samsung/groupcast/application/start/ParticipantsUtil;->ICON_APP:Ljava/lang/Byte;

    const v12, 0x7f020013

    const-string v13, "MusicLiveShare"

    invoke-direct/range {v8 .. v13}, Lcom/samsung/groupcast/application/start/ParticipantsUtil$MappingTable;-><init>(Ljava/lang/String;ILjava/lang/Byte;ILjava/lang/String;)V

    sput-object v8, Lcom/samsung/groupcast/application/start/ParticipantsUtil$MappingTable;->App:Lcom/samsung/groupcast/application/start/ParticipantsUtil$MappingTable;

    .line 38
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/samsung/groupcast/application/start/ParticipantsUtil$MappingTable;

    sget-object v1, Lcom/samsung/groupcast/application/start/ParticipantsUtil$MappingTable;->Default:Lcom/samsung/groupcast/application/start/ParticipantsUtil$MappingTable;

    aput-object v1, v0, v2

    sget-object v1, Lcom/samsung/groupcast/application/start/ParticipantsUtil$MappingTable;->Music:Lcom/samsung/groupcast/application/start/ParticipantsUtil$MappingTable;

    aput-object v1, v0, v7

    sget-object v1, Lcom/samsung/groupcast/application/start/ParticipantsUtil$MappingTable;->Image:Lcom/samsung/groupcast/application/start/ParticipantsUtil$MappingTable;

    aput-object v1, v0, v14

    sget-object v1, Lcom/samsung/groupcast/application/start/ParticipantsUtil$MappingTable;->Video:Lcom/samsung/groupcast/application/start/ParticipantsUtil$MappingTable;

    aput-object v1, v0, v15

    const/4 v1, 0x4

    sget-object v2, Lcom/samsung/groupcast/application/start/ParticipantsUtil$MappingTable;->Document:Lcom/samsung/groupcast/application/start/ParticipantsUtil$MappingTable;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, Lcom/samsung/groupcast/application/start/ParticipantsUtil$MappingTable;->GroupCamcorder:Lcom/samsung/groupcast/application/start/ParticipantsUtil$MappingTable;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/samsung/groupcast/application/start/ParticipantsUtil$MappingTable;->Game:Lcom/samsung/groupcast/application/start/ParticipantsUtil$MappingTable;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/samsung/groupcast/application/start/ParticipantsUtil$MappingTable;->App:Lcom/samsung/groupcast/application/start/ParticipantsUtil$MappingTable;

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/groupcast/application/start/ParticipantsUtil$MappingTable;->$VALUES:[Lcom/samsung/groupcast/application/start/ParticipantsUtil$MappingTable;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/Byte;I)V
    .locals 0
    .param p3, "icon"    # Ljava/lang/Byte;
    .param p4, "resID"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Byte;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 55
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 56
    iput-object p3, p0, Lcom/samsung/groupcast/application/start/ParticipantsUtil$MappingTable;->mIcon:Ljava/lang/Byte;

    .line 57
    iput p4, p0, Lcom/samsung/groupcast/application/start/ParticipantsUtil$MappingTable;->mResID:I

    .line 58
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/Byte;ILjava/lang/String;)V
    .locals 0
    .param p3, "icon"    # Ljava/lang/Byte;
    .param p4, "resID"    # I
    .param p5, "ID"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Byte;",
            "I",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 61
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/samsung/groupcast/application/start/ParticipantsUtil$MappingTable;-><init>(Ljava/lang/String;ILjava/lang/Byte;I)V

    .line 62
    iput-object p5, p0, Lcom/samsung/groupcast/application/start/ParticipantsUtil$MappingTable;->mID:Ljava/lang/String;

    .line 63
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/groupcast/application/start/ParticipantsUtil$MappingTable;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/application/start/ParticipantsUtil$MappingTable;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/ParticipantsUtil$MappingTable;->mID:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/groupcast/application/start/ParticipantsUtil$MappingTable;)Ljava/lang/Byte;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/application/start/ParticipantsUtil$MappingTable;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/ParticipantsUtil$MappingTable;->mIcon:Ljava/lang/Byte;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/groupcast/application/start/ParticipantsUtil$MappingTable;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/application/start/ParticipantsUtil$MappingTable;

    .prologue
    .line 38
    iget v0, p0, Lcom/samsung/groupcast/application/start/ParticipantsUtil$MappingTable;->mResID:I

    return v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/groupcast/application/start/ParticipantsUtil$MappingTable;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 38
    const-class v0, Lcom/samsung/groupcast/application/start/ParticipantsUtil$MappingTable;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/groupcast/application/start/ParticipantsUtil$MappingTable;

    return-object v0
.end method

.method public static values()[Lcom/samsung/groupcast/application/start/ParticipantsUtil$MappingTable;
    .locals 1

    .prologue
    .line 38
    sget-object v0, Lcom/samsung/groupcast/application/start/ParticipantsUtil$MappingTable;->$VALUES:[Lcom/samsung/groupcast/application/start/ParticipantsUtil$MappingTable;

    invoke-virtual {v0}, [Lcom/samsung/groupcast/application/start/ParticipantsUtil$MappingTable;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/groupcast/application/start/ParticipantsUtil$MappingTable;

    return-object v0
.end method
