.class public Lcom/samsung/groupcast/application/stub/StubStartActivity;
.super Landroid/app/Activity;
.source "StubStartActivity.java"


# static fields
.field public static final EXIT_CONFIRM_FRAGMENT_TAG:Ljava/lang/String; = "EXIT_CONFIRM_FRAGMENT"

.field static final MESSAGE_CMD:Ljava/lang/String; = "MESSAGE_CMD"

.field private static final NETWORKERR_DATAROAMING_OFF:I = 0x2

.field private static final NETWORKERR_FLIGHTMODE_ON:I = 0x0

.field private static final NETWORKERR_MOBILEDATA_OFF:I = 0x1

.field private static final NETWORKERR_NO_SIGNAL:I = 0x4

.field private static final NETWORKERR_REACHED_DATALIMIT:I = 0x3

.field public static final PREFERENCES_NAME:Ljava/lang/String; = "stub_pref"

.field static final RATIO:Ljava/lang/String; = "DOWNLOA_RATIO"

.field static final STATUS:Ljava/lang/String; = "DOWNLOAD_STATUS"

.field static final STATUS_DOWNLOADED:I = 0x1

.field static final STATUS_DOWNLOADING:I = 0x0

.field static final STATUS_DOWNLOAD_FAIL:I = 0x4

.field static final STATUS_INSTALLED:I = 0x3

.field static final STATUS_INSTALLING:I = 0x2

.field static final STATUS_INSTALL_FAIL:I = 0x5

.field public static final TAG:Ljava/lang/String; = "GroupCastStub"

.field private static isDownloading:Z

.field private static ratio:I


# instance fields
.field private mAppSize:D

.field private mContext:Landroid/content/Context;

.field private final mHandler:Landroid/os/Handler;

.field private mInfoTextView:Landroid/widget/TextView;

.field private mLayoutInstall:Landroid/widget/RelativeLayout;

.field private mLayoutProgressing:Landroid/widget/LinearLayout;

.field private mProgressBarInstall:Landroid/widget/ProgressBar;

.field private mTextViewDownlodInformation:Landroid/widget/TextView;

.field private mTextViewDownlodingSize:Landroid/widget/TextView;

.field private mTextViewTranslatorInformation:Landroid/widget/TextView;

.field private mThread:Lcom/samsung/groupcast/application/stub/UpdateCheckThread;

.field private mWelcomeImg1:Landroid/widget/ImageView;

.field private mWelcomeImg2:Landroid/widget/ImageView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 71
    sput v0, Lcom/samsung/groupcast/application/stub/StubStartActivity;->ratio:I

    .line 72
    sput-boolean v0, Lcom/samsung/groupcast/application/stub/StubStartActivity;->isDownloading:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 41
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 62
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/groupcast/application/stub/StubStartActivity;->mThread:Lcom/samsung/groupcast/application/stub/UpdateCheckThread;

    .line 70
    const-wide/high16 v0, 0x4040000000000000L    # 32.0

    iput-wide v0, p0, Lcom/samsung/groupcast/application/stub/StubStartActivity;->mAppSize:D

    .line 76
    new-instance v0, Lcom/samsung/groupcast/application/stub/StubStartActivity$1;

    invoke-direct {v0, p0}, Lcom/samsung/groupcast/application/stub/StubStartActivity$1;-><init>(Lcom/samsung/groupcast/application/stub/StubStartActivity;)V

    iput-object v0, p0, Lcom/samsung/groupcast/application/stub/StubStartActivity;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/samsung/groupcast/application/stub/StubStartActivity;)Landroid/widget/ProgressBar;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/application/stub/StubStartActivity;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/samsung/groupcast/application/stub/StubStartActivity;->mProgressBarInstall:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/samsung/groupcast/application/stub/StubStartActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/groupcast/application/stub/StubStartActivity;

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/samsung/groupcast/application/stub/StubStartActivity;->checkNetworkState()V

    return-void
.end method

.method static synthetic access$102(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 41
    sput-boolean p0, Lcom/samsung/groupcast/application/stub/StubStartActivity;->isDownloading:Z

    return p0
.end method

.method static synthetic access$1100(Lcom/samsung/groupcast/application/stub/StubStartActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/groupcast/application/stub/StubStartActivity;

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/samsung/groupcast/application/stub/StubStartActivity;->cancelDownload()V

    return-void
.end method

.method static synthetic access$200()I
    .locals 1

    .prologue
    .line 41
    sget v0, Lcom/samsung/groupcast/application/stub/StubStartActivity;->ratio:I

    return v0
.end method

.method static synthetic access$202(I)I
    .locals 0
    .param p0, "x0"    # I

    .prologue
    .line 41
    sput p0, Lcom/samsung/groupcast/application/stub/StubStartActivity;->ratio:I

    return p0
.end method

.method static synthetic access$300(Lcom/samsung/groupcast/application/stub/StubStartActivity;)D
    .locals 2
    .param p0, "x0"    # Lcom/samsung/groupcast/application/stub/StubStartActivity;

    .prologue
    .line 41
    iget-wide v0, p0, Lcom/samsung/groupcast/application/stub/StubStartActivity;->mAppSize:D

    return-wide v0
.end method

.method static synthetic access$302(Lcom/samsung/groupcast/application/stub/StubStartActivity;D)D
    .locals 0
    .param p0, "x0"    # Lcom/samsung/groupcast/application/stub/StubStartActivity;
    .param p1, "x1"    # D

    .prologue
    .line 41
    iput-wide p1, p0, Lcom/samsung/groupcast/application/stub/StubStartActivity;->mAppSize:D

    return-wide p1
.end method

.method static synthetic access$400(Lcom/samsung/groupcast/application/stub/StubStartActivity;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/application/stub/StubStartActivity;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/samsung/groupcast/application/stub/StubStartActivity;->mTextViewDownlodingSize:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$500(Lcom/samsung/groupcast/application/stub/StubStartActivity;I)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/groupcast/application/stub/StubStartActivity;
    .param p1, "x1"    # I

    .prologue
    .line 41
    invoke-direct {p0, p1}, Lcom/samsung/groupcast/application/stub/StubStartActivity;->showErrorDialog(I)V

    return-void
.end method

.method static synthetic access$600(Lcom/samsung/groupcast/application/stub/StubStartActivity;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/application/stub/StubStartActivity;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/samsung/groupcast/application/stub/StubStartActivity;->mTextViewTranslatorInformation:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$700(Lcom/samsung/groupcast/application/stub/StubStartActivity;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/application/stub/StubStartActivity;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/samsung/groupcast/application/stub/StubStartActivity;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$800(Lcom/samsung/groupcast/application/stub/StubStartActivity;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/groupcast/application/stub/StubStartActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 41
    invoke-direct {p0, p1}, Lcom/samsung/groupcast/application/stub/StubStartActivity;->setInstallLayout(Z)V

    return-void
.end method

.method static synthetic access$900(Lcom/samsung/groupcast/application/stub/StubStartActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/groupcast/application/stub/StubStartActivity;

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/samsung/groupcast/application/stub/StubStartActivity;->exitPoupupStubActivity()V

    return-void
.end method

.method private cancelDownload()V
    .locals 1

    .prologue
    .line 321
    const/4 v0, 0x0

    sput-boolean v0, Lcom/samsung/groupcast/application/stub/StubStartActivity;->isDownloading:Z

    .line 322
    iget-object v0, p0, Lcom/samsung/groupcast/application/stub/StubStartActivity;->mThread:Lcom/samsung/groupcast/application/stub/UpdateCheckThread;

    if-eqz v0, :cond_0

    .line 323
    iget-object v0, p0, Lcom/samsung/groupcast/application/stub/StubStartActivity;->mThread:Lcom/samsung/groupcast/application/stub/UpdateCheckThread;

    invoke-virtual {v0}, Lcom/samsung/groupcast/application/stub/UpdateCheckThread;->cancel()V

    .line 324
    iget-object v0, p0, Lcom/samsung/groupcast/application/stub/StubStartActivity;->mThread:Lcom/samsung/groupcast/application/stub/UpdateCheckThread;

    invoke-virtual {v0}, Lcom/samsung/groupcast/application/stub/UpdateCheckThread;->interrupt()V

    .line 326
    :cond_0
    return-void
.end method

.method private checkNetworkState()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 334
    invoke-static {p0}, Lcom/samsung/groupcast/application/stub/StubStartActivity;->isNetworkConnected(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 335
    const/4 v1, 0x0

    sput v1, Lcom/samsung/groupcast/application/stub/StubStartActivity;->ratio:I

    .line 336
    new-instance v1, Lcom/samsung/groupcast/application/stub/UpdateCheckThread;

    invoke-virtual {p0}, Lcom/samsung/groupcast/application/stub/StubStartActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/groupcast/application/stub/StubStartActivity;->mHandler:Landroid/os/Handler;

    invoke-direct {v1, v2, v3}, Lcom/samsung/groupcast/application/stub/UpdateCheckThread;-><init>(Landroid/content/Context;Landroid/os/Handler;)V

    iput-object v1, p0, Lcom/samsung/groupcast/application/stub/StubStartActivity;->mThread:Lcom/samsung/groupcast/application/stub/UpdateCheckThread;

    .line 337
    iget-object v1, p0, Lcom/samsung/groupcast/application/stub/StubStartActivity;->mThread:Lcom/samsung/groupcast/application/stub/UpdateCheckThread;

    invoke-virtual {v1}, Lcom/samsung/groupcast/application/stub/UpdateCheckThread;->start()V

    .line 338
    sput-boolean v4, Lcom/samsung/groupcast/application/stub/StubStartActivity;->isDownloading:Z

    .line 340
    invoke-direct {p0, v4}, Lcom/samsung/groupcast/application/stub/StubStartActivity;->setInstallLayout(Z)V

    .line 356
    :goto_0
    return-void

    .line 342
    :cond_0
    const/4 v0, -0x1

    .line 343
    .local v0, "networkStatus":I
    invoke-direct {p0}, Lcom/samsung/groupcast/application/stub/StubStartActivity;->isFligtMode()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 344
    const/4 v0, 0x0

    .line 354
    :goto_1
    invoke-direct {p0, v0}, Lcom/samsung/groupcast/application/stub/StubStartActivity;->sendBroadcastForNetworkErrorPopup(I)V

    goto :goto_0

    .line 345
    :cond_1
    invoke-direct {p0}, Lcom/samsung/groupcast/application/stub/StubStartActivity;->isMobileDataOff()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 346
    const/4 v0, 0x1

    goto :goto_1

    .line 347
    :cond_2
    invoke-direct {p0}, Lcom/samsung/groupcast/application/stub/StubStartActivity;->isRoamingOff()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 348
    const/4 v0, 0x2

    goto :goto_1

    .line 349
    :cond_3
    invoke-direct {p0}, Lcom/samsung/groupcast/application/stub/StubStartActivity;->isReachToDataLimit()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 350
    const/4 v0, 0x3

    goto :goto_1

    .line 352
    :cond_4
    const/4 v0, 0x4

    goto :goto_1
.end method

.method private exitPoupupStubActivity()V
    .locals 3

    .prologue
    .line 506
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/stub/StubStartActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "EXIT_CONFIRM_FRAGMENT"

    invoke-virtual {v1, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    if-nez v1, :cond_0

    .line 507
    const/4 v1, 0x0

    invoke-static {v1}, Lcom/samsung/groupcast/application/fragment/ExitConfirmDialogFragment;->newInstance(I)Lcom/samsung/groupcast/application/fragment/ExitConfirmDialogFragment;

    move-result-object v0

    .line 509
    .local v0, "fragment":Lcom/samsung/groupcast/application/fragment/ExitConfirmDialogFragment;
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/stub/StubStartActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "EXIT_CONFIRM_FRAGMENT"

    invoke-virtual {v0, v1, v2}, Lcom/samsung/groupcast/application/fragment/ExitConfirmDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 511
    .end local v0    # "fragment":Lcom/samsung/groupcast/application/fragment/ExitConfirmDialogFragment;
    :cond_0
    return-void
.end method

.method private init_Load_view()V
    .locals 13

    .prologue
    const v9, 0x7f0c0003

    const v8, 0x7f080071

    const/4 v12, 0x0

    .line 233
    const v5, 0x7f070049

    invoke-virtual {p0, v5}, Lcom/samsung/groupcast/application/stub/StubStartActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Button;

    .line 234
    .local v4, "update":Landroid/widget/Button;
    const v5, 0x7f07004e

    invoke-virtual {p0, v5}, Lcom/samsung/groupcast/application/stub/StubStartActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    .line 235
    .local v2, "cancel":Landroid/widget/Button;
    const v5, 0x7f070048

    invoke-virtual {p0, v5}, Lcom/samsung/groupcast/application/stub/StubStartActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, p0, Lcom/samsung/groupcast/application/stub/StubStartActivity;->mTextViewTranslatorInformation:Landroid/widget/TextView;

    .line 237
    const v5, 0x7f07004c

    invoke-virtual {p0, v5}, Lcom/samsung/groupcast/application/stub/StubStartActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, p0, Lcom/samsung/groupcast/application/stub/StubStartActivity;->mTextViewDownlodingSize:Landroid/widget/TextView;

    .line 238
    const v5, 0x7f070051

    invoke-virtual {p0, v5}, Lcom/samsung/groupcast/application/stub/StubStartActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, p0, Lcom/samsung/groupcast/application/stub/StubStartActivity;->mTextViewDownlodInformation:Landroid/widget/TextView;

    .line 239
    const v5, 0x7f07004d

    invoke-virtual {p0, v5}, Lcom/samsung/groupcast/application/stub/StubStartActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ProgressBar;

    iput-object v5, p0, Lcom/samsung/groupcast/application/stub/StubStartActivity;->mProgressBarInstall:Landroid/widget/ProgressBar;

    .line 240
    const v5, 0x7f07004a

    invoke-virtual {p0, v5}, Lcom/samsung/groupcast/application/stub/StubStartActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/LinearLayout;

    iput-object v5, p0, Lcom/samsung/groupcast/application/stub/StubStartActivity;->mLayoutProgressing:Landroid/widget/LinearLayout;

    .line 241
    const v5, 0x7f070047

    invoke-virtual {p0, v5}, Lcom/samsung/groupcast/application/stub/StubStartActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/RelativeLayout;

    iput-object v5, p0, Lcom/samsung/groupcast/application/stub/StubStartActivity;->mLayoutInstall:Landroid/widget/RelativeLayout;

    .line 243
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/stub/StubStartActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v9}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 244
    const v5, 0x7f07004f

    invoke-virtual {p0, v5}, Lcom/samsung/groupcast/application/stub/StubStartActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    iput-object v5, p0, Lcom/samsung/groupcast/application/stub/StubStartActivity;->mWelcomeImg1:Landroid/widget/ImageView;

    .line 245
    const v5, 0x7f070050

    invoke-virtual {p0, v5}, Lcom/samsung/groupcast/application/stub/StubStartActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    iput-object v5, p0, Lcom/samsung/groupcast/application/stub/StubStartActivity;->mWelcomeImg2:Landroid/widget/ImageView;

    .line 246
    const v5, 0x7f070046

    invoke-virtual {p0, v5}, Lcom/samsung/groupcast/application/stub/StubStartActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, p0, Lcom/samsung/groupcast/application/stub/StubStartActivity;->mInfoTextView:Landroid/widget/TextView;

    .line 256
    :cond_0
    invoke-static {}, Lcom/samsung/groupcast/application/App;->isGroupCamcoderInstalled()Z

    move-result v5

    if-nez v5, :cond_1

    .line 257
    iget-object v5, p0, Lcom/samsung/groupcast/application/stub/StubStartActivity;->mWelcomeImg2:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/samsung/groupcast/application/stub/StubStartActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f02000a

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 260
    :cond_1
    iget-object v5, p0, Lcom/samsung/groupcast/application/stub/StubStartActivity;->mTextViewDownlodInformation:Landroid/widget/TextView;

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/samsung/groupcast/application/stub/StubStartActivity;->mTextViewTranslatorInformation:Landroid/widget/TextView;

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/samsung/groupcast/application/stub/StubStartActivity;->mProgressBarInstall:Landroid/widget/ProgressBar;

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/samsung/groupcast/application/stub/StubStartActivity;->mTextViewDownlodingSize:Landroid/widget/TextView;

    if-nez v5, :cond_3

    .line 297
    :cond_2
    :goto_0
    return-void

    .line 264
    :cond_3
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/stub/StubStartActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v9}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 265
    iget-object v5, p0, Lcom/samsung/groupcast/application/stub/StubStartActivity;->mTextViewDownlodInformation:Landroid/widget/TextView;

    invoke-virtual {p0, v8}, Lcom/samsung/groupcast/application/stub/StubStartActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/samsung/groupcast/application/stub/StubStartActivity;->setTextStyleItalic(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 268
    :goto_1
    const-string v5, "stub_pref"

    invoke-virtual {p0, v5, v12}, Lcom/samsung/groupcast/application/stub/StubStartActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    .line 269
    .local v3, "pref":Landroid/content/SharedPreferences;
    const-string v5, "appByteSize"

    const-wide/32 v6, 0x2033333

    invoke-interface {v3, v5, v6, v7}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    .line 271
    .local v0, "appByteSize":J
    long-to-double v5, v0

    const-wide/high16 v7, 0x4130000000000000L    # 1048576.0

    div-double/2addr v5, v7

    iput-wide v5, p0, Lcom/samsung/groupcast/application/stub/StubStartActivity;->mAppSize:D

    .line 272
    const v5, 0x7f08006a

    invoke-virtual {p0, v5}, Lcom/samsung/groupcast/application/stub/StubStartActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v5

    iput-wide v5, p0, Lcom/samsung/groupcast/application/stub/StubStartActivity;->mAppSize:D

    .line 273
    iget-object v5, p0, Lcom/samsung/groupcast/application/stub/StubStartActivity;->mTextViewTranslatorInformation:Landroid/widget/TextView;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const v7, 0x7f08006f

    invoke-virtual {p0, v7}, Lcom/samsung/groupcast/application/stub/StubStartActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ": "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-wide v7, p0, Lcom/samsung/groupcast/application/stub/StubStartActivity;->mAppSize:D

    invoke-virtual {v6, v7, v8}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const v7, 0x7f080070

    invoke-virtual {p0, v7}, Lcom/samsung/groupcast/application/stub/StubStartActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 275
    iget-object v5, p0, Lcom/samsung/groupcast/application/stub/StubStartActivity;->mTextViewDownlodingSize:Landroid/widget/TextView;

    const-string v6, "%.1fMB/%.1fMB"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    iget-wide v8, p0, Lcom/samsung/groupcast/application/stub/StubStartActivity;->mAppSize:D

    const-wide/high16 v10, 0x4059000000000000L    # 100.0

    div-double/2addr v8, v10

    sget v10, Lcom/samsung/groupcast/application/stub/StubStartActivity;->ratio:I

    int-to-double v10, v10

    mul-double/2addr v8, v10

    invoke-static {v8, v9}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v8

    aput-object v8, v7, v12

    const/4 v8, 0x1

    iget-wide v9, p0, Lcom/samsung/groupcast/application/stub/StubStartActivity;->mAppSize:D

    invoke-static {v9, v10}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 277
    new-instance v5, Lcom/samsung/groupcast/application/stub/StubStartActivity$6;

    invoke-direct {v5, p0}, Lcom/samsung/groupcast/application/stub/StubStartActivity$6;-><init>(Lcom/samsung/groupcast/application/stub/StubStartActivity;)V

    invoke-virtual {v4, v5}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 284
    new-instance v5, Lcom/samsung/groupcast/application/stub/StubStartActivity$7;

    invoke-direct {v5, p0}, Lcom/samsung/groupcast/application/stub/StubStartActivity$7;-><init>(Lcom/samsung/groupcast/application/stub/StubStartActivity;)V

    invoke-virtual {v2, v5}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 292
    iget-object v5, p0, Lcom/samsung/groupcast/application/stub/StubStartActivity;->mProgressBarInstall:Landroid/widget/ProgressBar;

    const/16 v6, 0x64

    invoke-virtual {v5, v6}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 294
    sget-boolean v5, Lcom/samsung/groupcast/application/stub/StubStartActivity;->isDownloading:Z

    if-nez v5, :cond_2

    .line 295
    invoke-direct {p0, v12}, Lcom/samsung/groupcast/application/stub/StubStartActivity;->setInstallLayout(Z)V

    goto/16 :goto_0

    .line 267
    .end local v0    # "appByteSize":J
    .end local v3    # "pref":Landroid/content/SharedPreferences;
    :cond_4
    iget-object v5, p0, Lcom/samsung/groupcast/application/stub/StubStartActivity;->mTextViewDownlodInformation:Landroid/widget/TextView;

    invoke-virtual {p0, v8}, Lcom/samsung/groupcast/application/stub/StubStartActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1
.end method

.method private isFligtMode()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 386
    iget-object v2, p0, Lcom/samsung/groupcast/application/stub/StubStartActivity;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "airplane_mode_on"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method private isKitKat()Z
    .locals 2

    .prologue
    .line 474
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 476
    .local v0, "version":I
    const/16 v1, 0x13

    if-lt v0, v1, :cond_0

    .line 477
    const/4 v1, 0x1

    .line 479
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private isMobileDataOff()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 391
    const-string v2, "connectivity"

    invoke-virtual {p0, v2}, Lcom/samsung/groupcast/application/stub/StubStartActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 392
    .local v0, "connService":Landroid/net/ConnectivityManager;
    if-nez v0, :cond_1

    .line 398
    :cond_0
    :goto_0
    return v1

    .line 395
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/stub/StubStartActivity;->isWifiOnlyModel()Z

    move-result v2

    if-nez v2, :cond_0

    .line 398
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getMobileDataEnabled()Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method private static isNetworkConnected(Landroid/content/Context;)Z
    .locals 7
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 367
    const/4 v0, 0x0

    .line 368
    .local v0, "bIsConnected":Z
    const-string v5, "connectivity"

    invoke-virtual {p0, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/ConnectivityManager;

    .line 370
    .local v2, "manager":Landroid/net/ConnectivityManager;
    const/4 v5, 0x0

    invoke-virtual {v2, v5}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v3

    .line 371
    .local v3, "mobile":Landroid/net/NetworkInfo;
    const/4 v5, 0x1

    invoke-virtual {v2, v5}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v4

    .line 372
    .local v4, "wifi":Landroid/net/NetworkInfo;
    const/16 v5, 0x9

    invoke-virtual {v2, v5}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v1

    .line 374
    .local v1, "ethernet":Landroid/net/NetworkInfo;
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v5

    if-nez v5, :cond_2

    :cond_0
    if-eqz v4, :cond_1

    invoke-virtual {v4}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v5

    if-nez v5, :cond_2

    :cond_1
    if-eqz v1, :cond_3

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 378
    :cond_2
    const/4 v0, 0x1

    .line 382
    :goto_0
    return v0

    .line 380
    :cond_3
    const-string v5, "GroupCastStub"

    const-string v6, "isNetworkConnected : network error"

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private isReachToDataLimit()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 412
    const-string v2, "connectivity"

    invoke-virtual {p0, v2}, Lcom/samsung/groupcast/application/stub/StubStartActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 414
    .local v0, "cm":Landroid/net/ConnectivityManager;
    if-nez v0, :cond_1

    .line 416
    :cond_0
    :goto_0
    return v1

    :cond_1
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->isMobilePolicyDataEnable()Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method private isRoamingOff()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 402
    const-string v2, "phone"

    invoke-virtual {p0, v2}, Lcom/samsung/groupcast/application/stub/StubStartActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 403
    .local v0, "tm":Landroid/telephony/TelephonyManager;
    if-nez v0, :cond_1

    .line 408
    :cond_0
    :goto_0
    return v1

    .line 405
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/stub/StubStartActivity;->isWifiOnlyModel()Z

    move-result v2

    if-nez v2, :cond_0

    .line 408
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getDataRoamingEnabled()Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method private preLoadShareVideoCheck()Z
    .locals 4

    .prologue
    .line 460
    const-string v0, "com.sec.android.app.mv.player"

    .line 461
    .local v0, "PRELOAD_SHARE_VIDEO_PACKAGE":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/stub/StubStartActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 464
    .local v2, "pm":Landroid/content/pm/PackageManager;
    const/16 v3, 0x80

    :try_start_0
    invoke-virtual {v2, v0, v3}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 465
    const/4 v2, 0x0

    .line 470
    const/4 v3, 0x1

    :goto_0
    return v3

    .line 466
    :catch_0
    move-exception v1

    .line 467
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v1}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    .line 468
    const/4 v3, 0x0

    goto :goto_0
.end method

.method private sendBroadcastForNetworkErrorPopup(I)V
    .locals 3
    .param p1, "status"    # I

    .prologue
    .line 359
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 360
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.sec.android.app.popupuireceiver"

    const-string v2, "com.sec.android.app.popupuireceiver.popupNetworkError"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 362
    const-string v1, "network_err_type"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 363
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/samsung/groupcast/application/stub/StubStartActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 364
    return-void
.end method

.method private setInstallLayout(Z)V
    .locals 4
    .param p1, "bIsInstalling"    # Z

    .prologue
    const/4 v2, 0x4

    const/4 v1, 0x0

    .line 421
    sput-boolean p1, Lcom/samsung/groupcast/application/stub/StubStartActivity;->isDownloading:Z

    .line 422
    sput v1, Lcom/samsung/groupcast/application/stub/StubStartActivity;->ratio:I

    .line 424
    iget-object v3, p0, Lcom/samsung/groupcast/application/stub/StubStartActivity;->mLayoutProgressing:Landroid/widget/LinearLayout;

    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 425
    iget-object v0, p0, Lcom/samsung/groupcast/application/stub/StubStartActivity;->mLayoutInstall:Landroid/widget/RelativeLayout;

    if-eqz p1, :cond_1

    :goto_1
    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 426
    return-void

    :cond_0
    move v0, v2

    .line 424
    goto :goto_0

    :cond_1
    move v2, v1

    .line 425
    goto :goto_1
.end method

.method public static setTextStyleItalic(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    .locals 4
    .param p0, "text"    # Ljava/lang/CharSequence;

    .prologue
    const/4 v3, 0x0

    .line 452
    new-instance v1, Landroid/text/style/StyleSpan;

    const/4 v2, 0x2

    invoke-direct {v1, v2}, Landroid/text/style/StyleSpan;-><init>(I)V

    .line 453
    .local v1, "style":Landroid/text/style/StyleSpan;
    new-instance v0, Landroid/text/SpannableString;

    invoke-direct {v0, p0}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 454
    .local v0, "str":Landroid/text/SpannableString;
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v2

    invoke-virtual {v0, v1, v3, v2, v3}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 455
    return-object v0
.end method

.method private showErrorDialog(I)V
    .locals 3
    .param p1, "stringId"    # I

    .prologue
    .line 154
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/stub/StubStartActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 170
    :goto_0
    return-void

    .line 157
    :cond_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f080074

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x104000a

    new-instance v2, Lcom/samsung/groupcast/application/stub/StubStartActivity$2;

    invoke-direct {v2, p0}, Lcom/samsung/groupcast/application/stub/StubStartActivity$2;-><init>(Lcom/samsung/groupcast/application/stub/StubStartActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 164
    new-instance v0, Lcom/samsung/groupcast/application/stub/StubStartActivity$3;

    invoke-direct {v0, p0}, Lcom/samsung/groupcast/application/stub/StubStartActivity$3;-><init>(Lcom/samsung/groupcast/application/stub/StubStartActivity;)V

    invoke-virtual {p0, v0}, Lcom/samsung/groupcast/application/stub/StubStartActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method private showToast(Ljava/lang/String;)V
    .locals 2
    .param p1, "szMessage"    # Ljava/lang/String;

    .prologue
    .line 329
    const/4 v1, 0x0

    invoke-static {p0, p1, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 330
    .local v0, "toast":Landroid/widget/Toast;
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 331
    return-void
.end method

.method public static startActivityTakingOwnership(Landroid/app/Activity;)V
    .locals 3
    .param p0, "caller"    # Landroid/app/Activity;

    .prologue
    .line 146
    const-string v1, "caller"

    invoke-static {v1, p0}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 148
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/samsung/groupcast/application/stub/StubStartActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 149
    .local v0, "stubStartIntent":Landroid/content/Intent;
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 150
    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 151
    return-void
.end method


# virtual methods
.method public isWifiOnlyModel()Z
    .locals 5

    .prologue
    .line 483
    const/4 v2, 0x0

    .line 486
    .local v2, "isWifiOnly":Z
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/stub/StubStartActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    .line 487
    .local v3, "pm":Landroid/content/pm/PackageManager;
    const-string v4, "android.hardware.telephony"

    invoke-virtual {v3, v4}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 488
    .local v1, "isAPhone":Z
    if-nez v1, :cond_0

    const/4 v2, 0x1

    .line 495
    .end local v1    # "isAPhone":Z
    .end local v3    # "pm":Landroid/content/pm/PackageManager;
    :goto_0
    return v2

    .line 488
    .restart local v1    # "isAPhone":Z
    .restart local v3    # "pm":Landroid/content/pm/PackageManager;
    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    .line 491
    .end local v1    # "isAPhone":Z
    .end local v3    # "pm":Landroid/content/pm/PackageManager;
    :catch_0
    move-exception v0

    .line 492
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public onBackPressed()V
    .locals 0

    .prologue
    .line 502
    invoke-direct {p0}, Lcom/samsung/groupcast/application/stub/StubStartActivity;->exitPoupupStubActivity()V

    .line 503
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    const v1, 0x7f04001b

    .line 431
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 434
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    packed-switch v0, :pswitch_data_0

    .line 449
    :goto_0
    return-void

    .line 436
    :pswitch_0
    invoke-virtual {p0, v1}, Lcom/samsung/groupcast/application/stub/StubStartActivity;->setContentView(I)V

    .line 437
    invoke-direct {p0}, Lcom/samsung/groupcast/application/stub/StubStartActivity;->init_Load_view()V

    .line 438
    sget-boolean v0, Lcom/samsung/groupcast/application/stub/StubStartActivity;->isDownloading:Z

    invoke-direct {p0, v0}, Lcom/samsung/groupcast/application/stub/StubStartActivity;->setInstallLayout(Z)V

    goto :goto_0

    .line 441
    :pswitch_1
    invoke-virtual {p0, v1}, Lcom/samsung/groupcast/application/stub/StubStartActivity;->setContentView(I)V

    .line 442
    invoke-direct {p0}, Lcom/samsung/groupcast/application/stub/StubStartActivity;->init_Load_view()V

    .line 443
    sget-boolean v0, Lcom/samsung/groupcast/application/stub/StubStartActivity;->isDownloading:Z

    invoke-direct {p0, v0}, Lcom/samsung/groupcast/application/stub/StubStartActivity;->setInstallLayout(Z)V

    goto :goto_0

    .line 434
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 9
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const v8, 0x7f08006b

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 178
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 179
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/stub/StubStartActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0c0001

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v4

    if-nez v4, :cond_0

    .line 180
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/stub/StubStartActivity;->getRequestedOrientation()I

    move-result v4

    if-eq v6, v4, :cond_0

    .line 181
    invoke-virtual {p0, v6}, Lcom/samsung/groupcast/application/stub/StubStartActivity;->setRequestedOrientation(I)V

    .line 184
    :cond_0
    const v4, 0x7f04001b

    invoke-virtual {p0, v4}, Lcom/samsung/groupcast/application/stub/StubStartActivity;->setContentView(I)V

    .line 185
    iput-object p0, p0, Lcom/samsung/groupcast/application/stub/StubStartActivity;->mContext:Landroid/content/Context;

    .line 186
    invoke-direct {p0}, Lcom/samsung/groupcast/application/stub/StubStartActivity;->init_Load_view()V

    .line 188
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/stub/StubStartActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/app/ActionBar;->setHomeButtonEnabled(Z)V

    .line 189
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/stub/StubStartActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 191
    const-string v4, "groupplay_stub"

    invoke-virtual {p0, v4, v7}, Lcom/samsung/groupcast/application/stub/StubStartActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    .line 192
    .local v3, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 194
    .local v1, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v4, "isfirst"

    invoke-interface {v3, v4, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    .line 196
    .local v2, "isfirst":Z
    if-eqz v2, :cond_1

    .line 197
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 198
    .local v0, "dialog":Landroid/app/AlertDialog$Builder;
    invoke-virtual {v0, v7}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 199
    const v4, 0x7f08006e

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 200
    const-string v4, "ro.csc.country_code"

    invoke-static {v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "KOREA"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 201
    const v4, 0x7f080076

    invoke-virtual {p0, v4}, Lcom/samsung/groupcast/application/stub/StubStartActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-array v5, v6, [Ljava/lang/Object;

    invoke-virtual {p0, v8}, Lcom/samsung/groupcast/application/stub/StubStartActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 207
    :goto_0
    const/high16 v4, 0x1040000

    new-instance v5, Lcom/samsung/groupcast/application/stub/StubStartActivity$4;

    invoke-direct {v5, p0}, Lcom/samsung/groupcast/application/stub/StubStartActivity$4;-><init>(Lcom/samsung/groupcast/application/stub/StubStartActivity;)V

    invoke-virtual {v0, v4, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 217
    const v4, 0x104000a

    new-instance v5, Lcom/samsung/groupcast/application/stub/StubStartActivity$5;

    invoke-direct {v5, p0, v1}, Lcom/samsung/groupcast/application/stub/StubStartActivity$5;-><init>(Lcom/samsung/groupcast/application/stub/StubStartActivity;Landroid/content/SharedPreferences$Editor;)V

    invoke-virtual {v0, v4, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 226
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 230
    .end local v0    # "dialog":Landroid/app/AlertDialog$Builder;
    :cond_1
    return-void

    .line 204
    .restart local v0    # "dialog":Landroid/app/AlertDialog$Builder;
    :cond_2
    const v4, 0x7f080077

    invoke-virtual {p0, v4}, Lcom/samsung/groupcast/application/stub/StubStartActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-array v5, v6, [Ljava/lang/Object;

    invoke-virtual {p0, v8}, Lcom/samsung/groupcast/application/stub/StubStartActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 312
    const-string v0, "GroupCastStub"

    const-string v1, "onDestroy"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 313
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 315
    sget-boolean v0, Lcom/samsung/groupcast/application/stub/StubStartActivity;->isDownloading:Z

    if-eqz v0, :cond_0

    .line 316
    invoke-direct {p0}, Lcom/samsung/groupcast/application/stub/StubStartActivity;->cancelDownload()V

    .line 318
    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 303
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 304
    invoke-direct {p0}, Lcom/samsung/groupcast/application/stub/StubStartActivity;->init_Load_view()V

    .line 306
    sget-boolean v0, Lcom/samsung/groupcast/application/stub/StubStartActivity;->isDownloading:Z

    invoke-direct {p0, v0}, Lcom/samsung/groupcast/application/stub/StubStartActivity;->setInstallLayout(Z)V

    .line 307
    iget-object v0, p0, Lcom/samsung/groupcast/application/stub/StubStartActivity;->mProgressBarInstall:Landroid/widget/ProgressBar;

    sget v1, Lcom/samsung/groupcast/application/stub/StubStartActivity;->ratio:I

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 308
    return-void
.end method
