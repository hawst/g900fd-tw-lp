.class public Lcom/samsung/groupcast/application/fragment/LicenseDialogFragment;
.super Landroid/app/DialogFragment;
.source "LicenseDialogFragment.java"


# static fields
.field private static final TAG_PROGRESS_DIALOG_FRAGMENT:Ljava/lang/String; = "LICENSE_PROGRESS_DIALOG_FRAGMENT"


# instance fields
.field private mContentView:Landroid/view/View;

.field private mProgressDialogFragment:Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;

.field private webview:Landroid/webkit/WebView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/samsung/groupcast/application/fragment/LicenseDialogFragment;)Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/application/fragment/LicenseDialogFragment;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/samsung/groupcast/application/fragment/LicenseDialogFragment;->mProgressDialogFragment:Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;

    return-object v0
.end method

.method static synthetic access$002(Lcom/samsung/groupcast/application/fragment/LicenseDialogFragment;Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;)Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/groupcast/application/fragment/LicenseDialogFragment;
    .param p1, "x1"    # Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;

    .prologue
    .line 36
    iput-object p1, p0, Lcom/samsung/groupcast/application/fragment/LicenseDialogFragment;->mProgressDialogFragment:Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;

    return-object p1
.end method

.method public static newInstance(I)Lcom/samsung/groupcast/application/fragment/LicenseDialogFragment;
    .locals 1
    .param p0, "type"    # I

    .prologue
    .line 46
    new-instance v0, Lcom/samsung/groupcast/application/fragment/LicenseDialogFragment;

    invoke-direct {v0}, Lcom/samsung/groupcast/application/fragment/LicenseDialogFragment;-><init>()V

    .line 47
    .local v0, "fragment":Lcom/samsung/groupcast/application/fragment/LicenseDialogFragment;
    return-object v0
.end method


# virtual methods
.method public onAttach(Landroid/app/Activity;)V
    .locals 0
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 52
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onAttach(Landroid/app/Activity;)V

    .line 53
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v5, 0x1

    .line 67
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/fragment/LicenseDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f04001d

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/groupcast/application/fragment/LicenseDialogFragment;->mContentView:Landroid/view/View;

    .line 71
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/fragment/LicenseDialogFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-string v3, "LICENSE_PROGRESS_DIALOG_FRAGMENT"

    invoke-virtual {v2, v3}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v2

    check-cast v2, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;

    iput-object v2, p0, Lcom/samsung/groupcast/application/fragment/LicenseDialogFragment;->mProgressDialogFragment:Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;

    .line 76
    iget-object v2, p0, Lcom/samsung/groupcast/application/fragment/LicenseDialogFragment;->mContentView:Landroid/view/View;

    const v3, 0x7f070052

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/webkit/WebView;

    iput-object v2, p0, Lcom/samsung/groupcast/application/fragment/LicenseDialogFragment;->webview:Landroid/webkit/WebView;

    .line 77
    iget-object v2, p0, Lcom/samsung/groupcast/application/fragment/LicenseDialogFragment;->webview:Landroid/webkit/WebView;

    const-string v3, "file:///android_asset/licenses/license_groupplay.html"

    invoke-virtual {v2, v3}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 79
    iget-object v2, p0, Lcom/samsung/groupcast/application/fragment/LicenseDialogFragment;->webview:Landroid/webkit/WebView;

    invoke-virtual {v2}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v1

    .line 80
    .local v1, "webSettings":Landroid/webkit/WebSettings;
    invoke-virtual {v1, v5}, Landroid/webkit/WebSettings;->setLoadWithOverviewMode(Z)V

    .line 81
    invoke-virtual {v1, v5}, Landroid/webkit/WebSettings;->setUseWideViewPort(Z)V

    .line 82
    sget-object v2, Landroid/webkit/WebSettings$LayoutAlgorithm;->NORMAL:Landroid/webkit/WebSettings$LayoutAlgorithm;

    invoke-virtual {v1, v2}, Landroid/webkit/WebSettings;->setLayoutAlgorithm(Landroid/webkit/WebSettings$LayoutAlgorithm;)V

    .line 84
    iget-object v2, p0, Lcom/samsung/groupcast/application/fragment/LicenseDialogFragment;->webview:Landroid/webkit/WebView;

    new-instance v3, Lcom/samsung/groupcast/application/fragment/LicenseDialogFragment$1;

    invoke-direct {v3, p0}, Lcom/samsung/groupcast/application/fragment/LicenseDialogFragment$1;-><init>(Lcom/samsung/groupcast/application/fragment/LicenseDialogFragment;)V

    invoke-virtual {v2, v3}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 126
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/samsung/groupcast/application/fragment/LicenseDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 127
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    const v2, 0x7f080079

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 128
    iget-object v2, p0, Lcom/samsung/groupcast/application/fragment/LicenseDialogFragment;->mContentView:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 130
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    return-object v2
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 57
    invoke-super {p0}, Landroid/app/DialogFragment;->onResume()V

    .line 58
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/fragment/LicenseDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    if-nez v0, :cond_0

    .line 61
    :cond_0
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 135
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 136
    return-void
.end method
