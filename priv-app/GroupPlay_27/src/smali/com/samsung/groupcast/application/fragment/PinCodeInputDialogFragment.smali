.class public Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;
.super Landroid/app/DialogFragment;
.source "PinCodeInputDialogFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment$PinCodeInputDialogFragmentDelegate;
    }
.end annotation


# static fields
.field public static final MODE_PIN_INPUT_ON_CREATE:I = 0x0

.field public static final MODE_PIN_INPUT_ON_JOIN:I = 0x1

.field private static mInstance:Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;

.field private static mType:I


# instance fields
.field private mContentView:Landroid/view/View;

.field private mDelegate:Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment$PinCodeInputDialogFragmentDelegate;

.field private mIsCheckedShowPasswordCheckBox:Z

.field private mOkClickListener:Landroid/view/View$OnClickListener;

.field private final mOnCheckedChangeListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

.field private mOnEditorActionListener:Landroid/widget/TextView$OnEditorActionListener;

.field private mPinEditText:Landroid/widget/EditText;

.field private mShowPasswordCheckBox:Landroid/widget/CheckBox;

.field private mSummittingPincode:Ljava/lang/String;

.field private mTextWatcher:Landroid/text/TextWatcher;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    const/4 v0, -0x1

    sput v0, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->mType:I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 109
    new-instance v0, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment$1;

    invoke-direct {v0, p0}, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment$1;-><init>(Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;)V

    iput-object v0, p0, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->mOnCheckedChangeListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    .line 138
    new-instance v0, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment$2;

    invoke-direct {v0, p0}, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment$2;-><init>(Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;)V

    iput-object v0, p0, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->mOnEditorActionListener:Landroid/widget/TextView$OnEditorActionListener;

    .line 165
    new-instance v0, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment$3;

    invoke-direct {v0, p0}, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment$3;-><init>(Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;)V

    iput-object v0, p0, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->mTextWatcher:Landroid/text/TextWatcher;

    .line 269
    new-instance v0, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment$8;

    invoke-direct {v0, p0}, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment$8;-><init>(Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;)V

    iput-object v0, p0, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->mOkClickListener:Landroid/view/View$OnClickListener;

    return-void
.end method

.method static synthetic access$000(Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;)Landroid/widget/EditText;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->mPinEditText:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$002(Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;Landroid/widget/EditText;)Landroid/widget/EditText;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;
    .param p1, "x1"    # Landroid/widget/EditText;

    .prologue
    .line 30
    iput-object p1, p0, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->mPinEditText:Landroid/widget/EditText;

    return-object p1
.end method

.method static synthetic access$100(Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->updateOkButton()V

    return-void
.end method

.method static synthetic access$200(Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->isPinEntered()Z

    move-result v0

    return v0
.end method

.method static synthetic access$300(Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;)Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment$PinCodeInputDialogFragmentDelegate;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->mDelegate:Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment$PinCodeInputDialogFragmentDelegate;

    return-object v0
.end method

.method static synthetic access$400()Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;
    .locals 1

    .prologue
    .line 30
    sget-object v0, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->mInstance:Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;

    return-object v0
.end method

.method static synthetic access$500(Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->onDialogCancel()V

    return-void
.end method

.method private isPinEntered()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 296
    const-string v0, ""

    .line 298
    .local v0, "pin":Ljava/lang/String;
    iget-object v1, p0, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->mPinEditText:Landroid/widget/EditText;

    if-nez v1, :cond_1

    .line 299
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v1

    if-nez v1, :cond_0

    move v1, v2

    .line 310
    :goto_0
    return v1

    .line 302
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v1

    const v3, 0x7f070024

    invoke-virtual {v1, v3}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->mPinEditText:Landroid/widget/EditText;

    .line 305
    :cond_1
    iget-object v1, p0, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->mPinEditText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 307
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_3

    :cond_2
    move v1, v2

    .line 308
    goto :goto_0

    .line 310
    :cond_3
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public static newInstance(I)Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;
    .locals 1
    .param p0, "type"    # I

    .prologue
    .line 48
    sput p0, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->mType:I

    .line 49
    new-instance v0, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;

    invoke-direct {v0}, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;-><init>()V

    .line 50
    .local v0, "fragment":Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;
    sput-object v0, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->mInstance:Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;

    .line 51
    return-object v0
.end method

.method private onDialogCancel()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 264
    sget v0, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->mType:I

    if-ne v0, v3, :cond_0

    iget-object v0, p0, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->mDelegate:Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment$PinCodeInputDialogFragmentDelegate;

    if-eqz v0, :cond_0

    .line 265
    iget-object v0, p0, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->mDelegate:Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment$PinCodeInputDialogFragmentDelegate;

    sget-object v1, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->mInstance:Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment$PinCodeInputDialogFragmentDelegate;->onPinSubmitted(Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;Ljava/lang/String;Z)V

    .line 266
    :cond_0
    return-void
.end method

.method private updateOkButton()V
    .locals 3

    .prologue
    .line 285
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v1

    check-cast v1, Landroid/app/AlertDialog;

    .line 286
    .local v1, "dialog":Landroid/app/AlertDialog;
    if-nez v1, :cond_0

    .line 293
    :goto_0
    return-void

    .line 288
    :cond_0
    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    .line 289
    .local v0, "button":Landroid/widget/Button;
    invoke-direct {p0}, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->isPinEntered()Z

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 291
    iget-object v2, p0, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->mOkClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method


# virtual methods
.method public getSummittingPincode()Ljava/lang/String;
    .locals 2

    .prologue
    .line 351
    iget-object v0, p0, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->mPinEditText:Landroid/widget/EditText;

    if-nez v0, :cond_1

    .line 352
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    if-nez v0, :cond_0

    .line 353
    const-string v0, ""

    .line 363
    :goto_0
    return-object v0

    .line 355
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    const v1, 0x7f070024

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->mPinEditText:Landroid/widget/EditText;

    .line 359
    :cond_1
    iget-object v0, p0, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->mPinEditText:Landroid/widget/EditText;

    if-nez v0, :cond_2

    .line 360
    const-string v0, ""

    goto :goto_0

    .line 363
    :cond_2
    iget-object v0, p0, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->mPinEditText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getType()I
    .locals 1

    .prologue
    .line 254
    sget v0, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->mType:I

    return v0
.end method

.method public isCheckedShowPasswordCheckBox()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 367
    iget-object v0, p0, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->mShowPasswordCheckBox:Landroid/widget/CheckBox;

    if-nez v0, :cond_1

    .line 368
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    if-nez v0, :cond_0

    move v0, v1

    .line 379
    :goto_0
    return v0

    .line 371
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    const v2, 0x7f070025

    invoke-virtual {v0, v2}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->mShowPasswordCheckBox:Landroid/widget/CheckBox;

    .line 375
    :cond_1
    iget-object v0, p0, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->mShowPasswordCheckBox:Landroid/widget/CheckBox;

    if-nez v0, :cond_2

    move v0, v1

    .line 376
    goto :goto_0

    .line 379
    :cond_2
    iget-object v0, p0, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->mShowPasswordCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    goto :goto_0
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 61
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onAttach(Landroid/app/Activity;)V

    .line 62
    const-class v0, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment$PinCodeInputDialogFragmentDelegate;

    invoke-static {p1, v0}, Lcom/samsung/groupcast/misc/utility/Verify;->instanceOf(Ljava/lang/Object;Ljava/lang/Class;)V

    .line 63
    check-cast p1, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment$PinCodeInputDialogFragmentDelegate;

    .end local p1    # "activity":Landroid/app/Activity;
    invoke-virtual {p0, p1}, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->setDelegate(Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment$PinCodeInputDialogFragmentDelegate;)V

    .line 64
    return-void
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 0
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    .line 259
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onCancel(Landroid/content/DialogInterface;)V

    .line 260
    invoke-direct {p0}, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->onDialogCancel()V

    .line 261
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const v4, 0x7f08000e

    const/4 v3, 0x0

    .line 181
    const/4 v0, 0x0

    .line 183
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    new-instance v0, Landroid/app/AlertDialog$Builder;

    .end local v0    # "builder":Landroid/app/AlertDialog$Builder;
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 186
    .restart local v0    # "builder":Landroid/app/AlertDialog$Builder;
    iput-object v3, p0, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->mContentView:Landroid/view/View;

    .line 189
    sget v1, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->mType:I

    if-nez v1, :cond_1

    .line 190
    const v1, 0x7f080040

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 191
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f04000c

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->mContentView:Landroid/view/View;

    .line 193
    iget-object v1, p0, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->mContentView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 202
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->mContentView:Landroid/view/View;

    if-nez v1, :cond_2

    .line 203
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "contentView is null "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->mType:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 204
    const v1, 0x7f08003b

    invoke-virtual {p0, v1}, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 205
    new-instance v1, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment$4;

    invoke-direct {v1, p0}, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment$4;-><init>(Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;)V

    invoke-virtual {v0, v4, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 212
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    .line 246
    :goto_1
    return-object v1

    .line 194
    :cond_1
    sget v1, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->mType:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 195
    const v1, 0x7f080043

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 196
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f04000d

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->mContentView:Landroid/view/View;

    .line 199
    iget-object v1, p0, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->mContentView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    goto :goto_0

    .line 215
    :cond_2
    iget-object v1, p0, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->mContentView:Landroid/view/View;

    const v2, 0x7f070024

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->mPinEditText:Landroid/widget/EditText;

    .line 217
    iget-object v1, p0, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->mContentView:Landroid/view/View;

    const v2, 0x7f070025

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    iput-object v1, p0, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->mShowPasswordCheckBox:Landroid/widget/CheckBox;

    .line 218
    iget-object v1, p0, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->mShowPasswordCheckBox:Landroid/widget/CheckBox;

    new-instance v2, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment$5;

    invoke-direct {v2, p0}, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment$5;-><init>(Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;)V

    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 224
    iget-object v1, p0, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->mShowPasswordCheckBox:Landroid/widget/CheckBox;

    iget-object v2, p0, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->mOnCheckedChangeListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 225
    iget-object v1, p0, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->mPinEditText:Landroid/widget/EditText;

    iget-object v2, p0, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->mOnEditorActionListener:Landroid/widget/TextView$OnEditorActionListener;

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 226
    iget-object v1, p0, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->mPinEditText:Landroid/widget/EditText;

    iget-object v2, p0, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->mTextWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 227
    iget-object v1, p0, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->mPinEditText:Landroid/widget/EditText;

    const/4 v2, 0x6

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setImeOptions(I)V

    .line 230
    new-instance v1, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment$6;

    invoke-direct {v1, p0}, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment$6;-><init>(Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;)V

    invoke-virtual {v0, v4, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 238
    const v1, 0x7f08000f

    new-instance v2, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment$7;

    invoke-direct {v2, p0}, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment$7;-><init>(Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 246
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    goto :goto_1
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 341
    invoke-super {p0}, Landroid/app/DialogFragment;->onDestroy()V

    .line 342
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->mPinEditText:Landroid/widget/EditText;

    invoke-static {v0, v1}, Lcom/samsung/groupcast/view/ViewTools;->hideSoftKeyboard(Landroid/app/Activity;Landroid/view/View;)V

    .line 343
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 326
    iget-object v0, p0, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->mPinEditText:Landroid/widget/EditText;

    if-eqz v0, :cond_0

    .line 327
    iget-object v0, p0, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->mPinEditText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->clearFocus()V

    .line 330
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 331
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 333
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->mPinEditText:Landroid/widget/EditText;

    invoke-static {v0, v1}, Lcom/samsung/groupcast/view/ViewTools;->hideSoftKeyboard(Landroid/app/Activity;Landroid/view/View;)V

    .line 336
    :cond_1
    invoke-super {p0}, Landroid/app/DialogFragment;->onPause()V

    .line 337
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 68
    invoke-super {p0}, Landroid/app/DialogFragment;->onResume()V

    .line 69
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    if-nez v0, :cond_1

    .line 107
    :cond_0
    :goto_0
    return-void

    .line 73
    :cond_1
    iget-object v0, p0, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->mPinEditText:Landroid/widget/EditText;

    if-nez v0, :cond_2

    .line 74
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    const v1, 0x7f070024

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->mPinEditText:Landroid/widget/EditText;

    .line 76
    :cond_2
    iget-object v0, p0, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->mPinEditText:Landroid/widget/EditText;

    if-eqz v0, :cond_0

    .line 79
    iget-object v0, p0, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->mPinEditText:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->mOnEditorActionListener:Landroid/widget/TextView$OnEditorActionListener;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 80
    iget-object v0, p0, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->mPinEditText:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->mTextWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 81
    iget-object v0, p0, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->mPinEditText:Landroid/widget/EditText;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setImeOptions(I)V

    .line 82
    iget-object v0, p0, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->mPinEditText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 84
    iget-object v0, p0, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->mSummittingPincode:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->mSummittingPincode:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 85
    iget-object v0, p0, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->mPinEditText:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->mSummittingPincode:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 86
    iget-object v0, p0, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->mPinEditText:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->mSummittingPincode:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    .line 89
    :cond_3
    iget-object v0, p0, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->mShowPasswordCheckBox:Landroid/widget/CheckBox;

    if-nez v0, :cond_4

    .line 90
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    const v1, 0x7f070025

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->mShowPasswordCheckBox:Landroid/widget/CheckBox;

    .line 92
    :cond_4
    iget-object v0, p0, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->mShowPasswordCheckBox:Landroid/widget/CheckBox;

    const v1, 0x7f080041

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setText(I)V

    .line 93
    iget-boolean v0, p0, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->mIsCheckedShowPasswordCheckBox:Z

    if-eqz v0, :cond_5

    .line 94
    iget-object v0, p0, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->mShowPasswordCheckBox:Landroid/widget/CheckBox;

    iget-boolean v1, p0, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->mIsCheckedShowPasswordCheckBox:Z

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 97
    :cond_5
    iget-object v0, p0, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->mDelegate:Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment$PinCodeInputDialogFragmentDelegate;

    if-nez v0, :cond_6

    invoke-virtual {p0}, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    instance-of v0, v0, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment$PinCodeInputDialogFragmentDelegate;

    if-eqz v0, :cond_6

    .line 98
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment$PinCodeInputDialogFragmentDelegate;

    invoke-virtual {p0, v0}, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->setDelegate(Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment$PinCodeInputDialogFragmentDelegate;)V

    .line 101
    :cond_6
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->mPinEditText:Landroid/widget/EditText;

    invoke-static {v0, v1}, Lcom/samsung/groupcast/view/ViewTools;->showSowftKeyboard(Landroid/app/Activity;Landroid/view/View;)V

    .line 102
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 105
    invoke-direct {p0}, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->updateOkButton()V

    goto/16 :goto_0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 347
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 348
    return-void
.end method

.method public onStop()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 316
    invoke-super {p0}, Landroid/app/DialogFragment;->onStop()V

    .line 317
    iput-object v0, p0, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->mDelegate:Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment$PinCodeInputDialogFragmentDelegate;

    .line 318
    sput-object v0, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->mInstance:Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;

    .line 319
    iput-object v0, p0, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->mPinEditText:Landroid/widget/EditText;

    .line 320
    iput-object v0, p0, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->mShowPasswordCheckBox:Landroid/widget/CheckBox;

    .line 321
    return-void
.end method

.method public setCheckedShowPasswordCheckBox(Z)V
    .locals 0
    .param p1, "isCheckedShowPasswordCheckBox"    # Z

    .prologue
    .line 387
    iput-boolean p1, p0, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->mIsCheckedShowPasswordCheckBox:Z

    .line 388
    return-void
.end method

.method public setDelegate(Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment$PinCodeInputDialogFragmentDelegate;)V
    .locals 0
    .param p1, "mDelegate"    # Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment$PinCodeInputDialogFragmentDelegate;

    .prologue
    .line 250
    iput-object p1, p0, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->mDelegate:Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment$PinCodeInputDialogFragmentDelegate;

    .line 251
    return-void
.end method

.method public setSummittingPincode(Ljava/lang/String;)V
    .locals 0
    .param p1, "summittingPincode"    # Ljava/lang/String;

    .prologue
    .line 383
    iput-object p1, p0, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->mSummittingPincode:Ljava/lang/String;

    .line 384
    return-void
.end method
