.class public Lcom/samsung/groupcast/application/App;
.super Landroid/app/Application;
.source "App.java"


# static fields
.field private static final CHINA_WIFI_REPLACEMENT_STRING:Ljava/lang/String; = "WLAN"

.field private static final DEVICE_CAMERA_M:Ljava/lang/String; = "mproject"

.field private static final DEVICE_CAMERA_U:Ljava/lang/String; = "u0"

.field public static final DEVICE_SOLD_IN_CHINA:Z

.field public static final FEATURE_CLEARS_IMG_DOC_SESSION_WHEN_ITS_EMPTY:Z = true

.field public static final FEATURE_SUPPORT_GIF:Z = false

.field public static final FEATURE_SUPPORT_SSP:Z = false

.field public static final FIRST_SESSION_VERSION:I = 0x0

.field private static HAS_EXTERNAL_APP_MODE:Z = false

.field public static final MIN_SESSION_VERSION:I = 0x1

.field public static final SUPPORTED_AUDIO_TYPE_STRING:Ljava/lang/String; = "audio/*"

.field public static final SUPPORTED_DOCUMENT_TYPE_STRING:Ljava/lang/String; = "application/msword;application/vnd.openxmlformats-officedocument.wordprocessingml.document;application/vnd.ms-powerpoint;application/vnd.openxmlformats-officedocument.presentationml.presentation;application/pdf"

.field public static final SUPPORTED_IMAGE_TYPE_STRING:Ljava/lang/String; = "image/*"

.field public static final THIS_SESSION_VERSION:I = 0x1

.field private static final WIFI_STRING_PATTERN:Ljava/util/regex/Pattern;

.field private static ext_screenId:Ljava/lang/String;

.field private static mGameXmlDownloadTime:J

.field private static mIsGameInstalled:Z

.field private static mIsGroupCamcoderInstalled:Z

.field private static mIsMusicLiveInstalled:Z

.field private static mIsShareVideoInstalled:Z

.field private static mScreenConfigurationQualifier:Ljava/lang/String;

.field private static mTopActivity:Ljava/lang/String;

.field public static mVendorSpecificAPAvaliable:Z

.field private static register_extra_app:Z

.field private static sInstance:Lcom/samsung/groupcast/application/App;

.field private static screenId:Ljava/lang/String;


# instance fields
.field private final MAX_GETTASK:I

.field private mMessagingClient:Lcom/samsung/groupcast/core/messaging/MessagingClient;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 61
    invoke-static {}, Lcom/samsung/groupcast/application/Environment;->isRunningOnChinaProduct()Z

    move-result v0

    sput-boolean v0, Lcom/samsung/groupcast/application/App;->DEVICE_SOLD_IN_CHINA:Z

    .line 62
    const-string v0, "\\bwifi\\b|\\bwi-fi\\b|\\bwi-fi"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/samsung/groupcast/application/App;->WIFI_STRING_PATTERN:Ljava/util/regex/Pattern;

    .line 76
    sput-boolean v3, Lcom/samsung/groupcast/application/App;->register_extra_app:Z

    .line 80
    sput-boolean v3, Lcom/samsung/groupcast/application/App;->HAS_EXTERNAL_APP_MODE:Z

    .line 87
    sput-boolean v2, Lcom/samsung/groupcast/application/App;->mIsGameInstalled:Z

    .line 88
    sput-boolean v2, Lcom/samsung/groupcast/application/App;->mIsMusicLiveInstalled:Z

    .line 89
    sput-boolean v2, Lcom/samsung/groupcast/application/App;->mIsShareVideoInstalled:Z

    .line 90
    sput-boolean v2, Lcom/samsung/groupcast/application/App;->mIsGroupCamcoderInstalled:Z

    .line 93
    const-wide/32 v0, -0x337f9800

    sput-wide v0, Lcom/samsung/groupcast/application/App;->mGameXmlDownloadTime:J

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0}, Landroid/app/Application;-><init>()V

    .line 79
    const/16 v0, 0x32

    iput v0, p0, Lcom/samsung/groupcast/application/App;->MAX_GETTASK:I

    return-void
.end method

.method public static HAS_EXTERNAL_APP()Z
    .locals 1

    .prologue
    .line 386
    sget-boolean v0, Lcom/samsung/groupcast/application/App;->HAS_EXTERNAL_APP_MODE:Z

    return v0
.end method

.method public static IsSbeamOrGalleryForeground()Z
    .locals 9

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 441
    :try_start_0
    invoke-static {}, Lcom/samsung/groupcast/application/App;->getInstance()Lcom/samsung/groupcast/application/App;

    move-result-object v5

    const-string v8, "activity"

    invoke-virtual {v5, v8}, Lcom/samsung/groupcast/application/App;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 443
    .local v0, "am":Landroid/app/ActivityManager;
    const/4 v5, 0x1

    invoke-virtual {v0, v5}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v3

    .line 444
    .local v3, "taskInfo":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    const/4 v5, 0x0

    invoke-interface {v3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/ActivityManager$RunningTaskInfo;

    iget-object v4, v5, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    .line 445
    .local v4, "topActivity":Landroid/content/ComponentName;
    invoke-virtual {v4}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v2

    .line 446
    .local v2, "pkg":Ljava/lang/String;
    const-string v5, "com.sec.android.directshare"

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 447
    const-string v5, "sbeam is foreground!!"

    invoke-static {v5}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    move v5, v6

    .line 457
    .end local v2    # "pkg":Ljava/lang/String;
    .end local v3    # "taskInfo":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    .end local v4    # "topActivity":Landroid/content/ComponentName;
    :goto_0
    return v5

    .line 449
    .restart local v2    # "pkg":Ljava/lang/String;
    .restart local v3    # "taskInfo":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    .restart local v4    # "topActivity":Landroid/content/ComponentName;
    :cond_0
    const-string v5, "com.sec.android.gallery3d"

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 450
    const-string v5, "gallery is foreground!!"

    invoke-static {v5}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move v5, v6

    .line 451
    goto :goto_0

    .line 454
    .end local v2    # "pkg":Ljava/lang/String;
    .end local v3    # "taskInfo":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    .end local v4    # "topActivity":Landroid/content/ComponentName;
    :catch_0
    move-exception v1

    .line 455
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .end local v1    # "e":Ljava/lang/Exception;
    :cond_1
    move v5, v7

    .line 457
    goto :goto_0
.end method

.method public static SET_EXTERNAL_APP(Z)V
    .locals 0
    .param p0, "mode"    # Z

    .prologue
    .line 390
    sput-boolean p0, Lcom/samsung/groupcast/application/App;->HAS_EXTERNAL_APP_MODE:Z

    .line 391
    return-void
.end method

.method public static deserialize([B)Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;
    .locals 5
    .param p0, "data"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 184
    new-instance v0, Ljava/io/ByteArrayInputStream;

    invoke-direct {v0, p0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 185
    .local v0, "b":Ljava/io/ByteArrayInputStream;
    new-instance v1, Ljava/io/ObjectInputStream;

    invoke-direct {v1, v0}, Ljava/io/ObjectInputStream;-><init>(Ljava/io/InputStream;)V

    .line 186
    .local v1, "o":Ljava/io/ObjectInputStream;
    invoke-virtual {v1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v2

    .line 188
    .local v2, "obj":Ljava/lang/Object;
    instance-of v3, v2, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;

    if-nez v3, :cond_0

    .line 189
    new-instance v3, Ljava/io/IOException;

    const-string v4, "SessionSummary error"

    invoke-direct {v3, v4}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 192
    :cond_0
    check-cast v2, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;

    .end local v2    # "obj":Ljava/lang/Object;
    return-object v2
.end method

.method public static enableNotificationBarWhileFullScreen(Landroid/app/Activity;)V
    .locals 4
    .param p0, "act"    # Landroid/app/Activity;

    .prologue
    .line 470
    :try_start_0
    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    .line 471
    .local v1, "lp":Landroid/view/WindowManager$LayoutParams;
    if-eqz v1, :cond_0

    .line 472
    iget v2, v1, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    or-int/lit16 v2, v2, 0x200

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    .line 474
    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 475
    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 482
    .end local v1    # "lp":Landroid/view/WindowManager$LayoutParams;
    :cond_0
    :goto_0
    return-void

    .line 478
    :catch_0
    move-exception v0

    .line 479
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 480
    const-string v2, "GroupPlay"

    const-string v3, "WTF framework team!"

    invoke-static {v2, v3}, Lcom/samsung/groupcast/application/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static getGameXmlDownloadTime()J
    .locals 2

    .prologue
    .line 382
    sget-wide v0, Lcom/samsung/groupcast/application/App;->mGameXmlDownloadTime:J

    return-wide v0
.end method

.method public static getInstance()Lcom/samsung/groupcast/application/App;
    .locals 1

    .prologue
    .line 100
    sget-object v0, Lcom/samsung/groupcast/application/App;->sInstance:Lcom/samsung/groupcast/application/App;

    return-object v0
.end method

.method public static getMessagingClient()Lcom/samsung/groupcast/core/messaging/MessagingClient;
    .locals 1

    .prologue
    .line 113
    invoke-static {}, Lcom/samsung/groupcast/core/messaging/MessagingClient;->GetInstance()Lcom/samsung/groupcast/core/messaging/MessagingClient;

    move-result-object v0

    return-object v0
.end method

.method public static getPeerScreenId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 424
    sget-object v0, Lcom/samsung/groupcast/application/App;->ext_screenId:Ljava/lang/String;

    return-object v0
.end method

.method public static getPromotionImgUrlEachDevice()Ljava/util/ArrayList;
    .locals 24
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 485
    invoke-static {}, Lcom/samsung/groupcast/application/Environment;->getBuildModel()Ljava/lang/String;

    move-result-object v14

    .line 486
    .local v14, "model":Ljava/lang/String;
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    .line 488
    .local v13, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-static {}, Lcom/samsung/groupcast/application/App;->getInstance()Lcom/samsung/groupcast/application/App;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/groupcast/application/App;->getResources()Landroid/content/res/Resources;

    move-result-object v21

    const v22, 0x7f060001

    invoke-virtual/range {v21 .. v22}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v9

    .line 490
    .local v9, "is":Ljava/io/InputStream;
    const/4 v3, 0x0

    .line 491
    .local v3, "doc":Lorg/w3c/dom/Document;
    const/16 v18, 0x0

    .line 493
    .local v18, "nl":Lorg/w3c/dom/NodeList;
    invoke-static {v9}, Lcom/samsung/groupcast/misc/utility/XmlTools;->getDom(Ljava/io/InputStream;)Lorg/w3c/dom/Document;

    move-result-object v3

    .line 495
    if-eqz v3, :cond_0

    .line 496
    const-string v21, "banner"

    move-object/from16 v0, v21

    invoke-interface {v3, v0}, Lorg/w3c/dom/Document;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v18

    .line 498
    :cond_0
    if-eqz v18, :cond_4

    .line 499
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    invoke-interface/range {v18 .. v18}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v21

    move/from16 v0, v21

    if-ge v5, v0, :cond_4

    .line 500
    move-object/from16 v0, v18

    invoke-interface {v0, v5}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v4

    check-cast v4, Lorg/w3c/dom/Element;

    .line 501
    .local v4, "elem":Lorg/w3c/dom/Element;
    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    .line 502
    .local v15, "modelList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v21, "models"

    move-object/from16 v0, v21

    invoke-interface {v4, v0}, Lorg/w3c/dom/Element;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v17

    .line 504
    .local v17, "modelNodes":Lorg/w3c/dom/NodeList;
    invoke-interface/range {v17 .. v17}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v21

    if-lez v21, :cond_3

    .line 505
    const/16 v21, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v21

    invoke-interface {v0, v1}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v21

    invoke-interface/range {v21 .. v21}, Lorg/w3c/dom/Node;->getTextContent()Ljava/lang/String;

    move-result-object v21

    const-string v22, ","

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v20

    .line 506
    .local v20, "str":[Ljava/lang/String;
    move-object/from16 v2, v20

    .local v2, "arr$":[Ljava/lang/String;
    array-length v12, v2

    .local v12, "len$":I
    const/4 v6, 0x0

    .local v6, "i$":I
    :goto_1
    if-ge v6, v12, :cond_3

    aget-object v16, v2, v6

    .line 507
    .local v16, "modelName":Ljava/lang/String;
    move-object/from16 v0, v16

    invoke-virtual {v0, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_2

    .line 508
    const-string v21, "promotionImages"

    move-object/from16 v0, v21

    invoke-interface {v4, v0}, Lorg/w3c/dom/Element;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v8

    .line 509
    .local v8, "imgNodes":Lorg/w3c/dom/NodeList;
    const/4 v10, 0x0

    .local v10, "j":I
    :goto_2
    invoke-interface {v8}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v21

    move/from16 v0, v21

    if-ge v10, v0, :cond_4

    .line 510
    invoke-interface {v8, v10}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v7

    check-cast v7, Lorg/w3c/dom/Element;

    .line 511
    .local v7, "imageurl":Lorg/w3c/dom/Element;
    const-string v21, "imageurl"

    move-object/from16 v0, v21

    invoke-interface {v7, v0}, Lorg/w3c/dom/Element;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v19

    .line 512
    .local v19, "node":Lorg/w3c/dom/NodeList;
    const/4 v11, 0x0

    .local v11, "k":I
    :goto_3
    invoke-interface/range {v19 .. v19}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v21

    move/from16 v0, v21

    if-ge v11, v0, :cond_1

    .line 513
    move-object/from16 v0, v19

    invoke-interface {v0, v11}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v21

    invoke-interface/range {v21 .. v21}, Lorg/w3c/dom/Node;->getTextContent()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v13, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 512
    add-int/lit8 v11, v11, 0x1

    goto :goto_3

    .line 509
    :cond_1
    add-int/lit8 v10, v10, 0x1

    goto :goto_2

    .line 506
    .end local v7    # "imageurl":Lorg/w3c/dom/Element;
    .end local v8    # "imgNodes":Lorg/w3c/dom/NodeList;
    .end local v10    # "j":I
    .end local v11    # "k":I
    .end local v19    # "node":Lorg/w3c/dom/NodeList;
    :cond_2
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 499
    .end local v2    # "arr$":[Ljava/lang/String;
    .end local v6    # "i$":I
    .end local v12    # "len$":I
    .end local v16    # "modelName":Ljava/lang/String;
    .end local v20    # "str":[Ljava/lang/String;
    :cond_3
    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_0

    .line 523
    .end local v4    # "elem":Lorg/w3c/dom/Element;
    .end local v5    # "i":I
    .end local v15    # "modelList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v17    # "modelNodes":Lorg/w3c/dom/NodeList;
    :cond_4
    const-string v21, "lhs"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "model : "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Lcom/samsung/groupcast/application/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 524
    return-object v13
.end method

.method public static getSanitizedString(I)Ljava/lang/String;
    .locals 1
    .param p0, "resId"    # I

    .prologue
    .line 162
    invoke-static {}, Lcom/samsung/groupcast/application/App;->getInstance()Lcom/samsung/groupcast/application/App;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/samsung/groupcast/application/App;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/groupcast/application/App;->getSanitizedString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getSanitizedString(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "string"    # Ljava/lang/String;

    .prologue
    .line 166
    sget-boolean v1, Lcom/samsung/groupcast/application/App;->DEVICE_SOLD_IN_CHINA:Z

    if-nez v1, :cond_0

    .line 172
    :goto_0
    return-object p0

    .line 170
    :cond_0
    sget-object v1, Lcom/samsung/groupcast/application/App;->WIFI_STRING_PATTERN:Ljava/util/regex/Pattern;

    invoke-virtual {v1, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 171
    .local v0, "matcher":Ljava/util/regex/Matcher;
    const-string v1, "WLAN"

    invoke-virtual {v0, v1}, Ljava/util/regex/Matcher;->replaceAll(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 172
    goto :goto_0
.end method

.method public static getScreenId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 416
    sget-object v0, Lcom/samsung/groupcast/application/App;->screenId:Ljava/lang/String;

    return-object v0
.end method

.method public static getVersionName()Ljava/lang/String;
    .locals 5

    .prologue
    .line 105
    :try_start_0
    invoke-static {}, Lcom/samsung/groupcast/application/App;->getInstance()Lcom/samsung/groupcast/application/App;

    move-result-object v0

    .line 106
    .local v0, "app":Lcom/samsung/groupcast/application/App;
    invoke-virtual {v0}, Lcom/samsung/groupcast/application/App;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-virtual {v0}, Lcom/samsung/groupcast/application/App;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v2

    iget-object v2, v2, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 108
    :goto_0
    return-object v2

    .line 107
    :catch_0
    move-exception v1

    .line 108
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v2, "unknown"

    goto :goto_0
.end method

.method public static isGroupCamcoderInstalled()Z
    .locals 1

    .prologue
    .line 286
    sget-boolean v0, Lcom/samsung/groupcast/application/App;->mIsGroupCamcoderInstalled:Z

    return v0
.end method

.method public static isLandscapeEnabled()Z
    .locals 2

    .prologue
    .line 351
    sget-object v0, Lcom/samsung/groupcast/application/App;->sInstance:Lcom/samsung/groupcast/application/App;

    invoke-virtual {v0}, Lcom/samsung/groupcast/application/App;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c0001

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 352
    const/4 v0, 0x1

    .line 355
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isMusicLiveInstalled()Z
    .locals 1

    .prologue
    .line 278
    sget-boolean v0, Lcom/samsung/groupcast/application/App;->mIsMusicLiveInstalled:Z

    return v0
.end method

.method public static isRegister_Extra_App()Z
    .locals 1

    .prologue
    .line 432
    sget-boolean v0, Lcom/samsung/groupcast/application/App;->register_extra_app:Z

    return v0
.end method

.method public static isShareVideoInstalled()Z
    .locals 1

    .prologue
    .line 282
    sget-boolean v0, Lcom/samsung/groupcast/application/App;->mIsShareVideoInstalled:Z

    return v0
.end method

.method public static isTablet()Z
    .locals 1

    .prologue
    .line 359
    const/4 v0, 0x0

    return v0
.end method

.method public static isTopActivity(Ljava/lang/String;)Z
    .locals 3
    .param p0, "ActivityName"    # Ljava/lang/String;

    .prologue
    .line 364
    const-string v0, "GroupPlayBand"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/samsung/groupcast/application/App;->mTopActivity:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 365
    sget-object v0, Lcom/samsung/groupcast/application/App;->mTopActivity:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/samsung/groupcast/application/App;->mTopActivity:Ljava/lang/String;

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 366
    const/4 v0, 0x1

    .line 368
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isWifiOnlyModel()Z
    .locals 7

    .prologue
    .line 394
    const/4 v2, 0x0

    .line 397
    .local v2, "isWifiOnly":Z
    :try_start_0
    invoke-static {}, Lcom/samsung/groupcast/application/App;->getInstance()Lcom/samsung/groupcast/application/App;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/groupcast/application/App;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    .line 398
    .local v3, "pm":Landroid/content/pm/PackageManager;
    const-string v4, "android.hardware.telephony"

    invoke-virtual {v3, v4}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v1

    .line 399
    .local v1, "isAPhone":Z
    if-nez v1, :cond_0

    const/4 v2, 0x1

    .line 401
    :goto_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "isAPhone:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", type :"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {}, Lcom/samsung/groupcast/application/App;->getInstance()Lcom/samsung/groupcast/application/App;

    move-result-object v4

    const-string v6, "phone"

    invoke-virtual {v4, v6}, Lcom/samsung/groupcast/application/App;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/telephony/TelephonyManager;

    invoke-virtual {v4}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    move-result v4

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 411
    .end local v1    # "isAPhone":Z
    .end local v3    # "pm":Landroid/content/pm/PackageManager;
    :goto_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "isWifiOnly:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 412
    return v2

    .line 399
    .restart local v1    # "isAPhone":Z
    .restart local v3    # "pm":Landroid/content/pm/PackageManager;
    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    .line 407
    .end local v1    # "isAPhone":Z
    .end local v3    # "pm":Landroid/content/pm/PackageManager;
    :catch_0
    move-exception v0

    .line 408
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method private static preLoadGroupCamcoderCheck()Z
    .locals 4

    .prologue
    .line 335
    const-string v0, "com.sec.android.app.ma.recorder"

    .line 336
    .local v0, "PRELOAD_GROUP_CAMCORDER_PACKAGE":Ljava/lang/String;
    invoke-static {}, Lcom/samsung/groupcast/application/App;->getInstance()Lcom/samsung/groupcast/application/App;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/groupcast/application/App;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 339
    .local v2, "pm":Landroid/content/pm/PackageManager;
    const/16 v3, 0x80

    :try_start_0
    invoke-virtual {v2, v0, v3}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 340
    const/4 v2, 0x0

    .line 345
    const/4 v3, 0x1

    :goto_0
    return v3

    .line 341
    :catch_0
    move-exception v1

    .line 342
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v1}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    .line 343
    const/4 v3, 0x0

    goto :goto_0
.end method

.method private preLoadMusicLiveCheck()Z
    .locals 10

    .prologue
    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 292
    const-string v5, "samsung.musicliveshare.intent.action.ACTION_LAUNCH_SLAVE"

    .line 293
    .local v5, "mlsNfcAction":Ljava/lang/String;
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 294
    .local v3, "intent":Landroid/content/Intent;
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/App;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v9

    invoke-virtual {v9, v3, v8}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v1

    .line 296
    .local v1, "apps":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    const/4 v4, 0x0

    .line 297
    .local v4, "isExsistNewMls":Z
    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v9

    if-lez v9, :cond_0

    .line 298
    const/4 v4, 0x1

    .line 301
    :cond_0
    if-eqz v4, :cond_1

    .line 315
    :goto_0
    return v7

    .line 305
    :cond_1
    const-string v0, "com.sec.android.app.mediasync"

    .line 306
    .local v0, "PRELOAD_MUSIC_LIVE_PACKAGE":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/App;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    .line 309
    .local v6, "pm":Landroid/content/pm/PackageManager;
    const/16 v9, 0x80

    :try_start_0
    invoke-virtual {v6, v0, v9}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 310
    const/4 v6, 0x0

    goto :goto_0

    .line 311
    :catch_0
    move-exception v2

    .line 312
    .local v2, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v2}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    move v7, v8

    .line 313
    goto :goto_0
.end method

.method private preLoadShareVideoCheck()Z
    .locals 4

    .prologue
    .line 321
    const-string v0, "com.sec.android.app.mv.player"

    .line 322
    .local v0, "PRELOAD_SHARE_VIDEO_PACKAGE":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/App;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 325
    .local v2, "pm":Landroid/content/pm/PackageManager;
    const/16 v3, 0x80

    :try_start_0
    invoke-virtual {v2, v0, v3}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 326
    const/4 v2, 0x0

    .line 331
    const/4 v3, 0x1

    :goto_0
    return v3

    .line 327
    :catch_0
    move-exception v1

    .line 328
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v1}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    .line 329
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public static serialize(Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;)[B
    .locals 3
    .param p0, "ss"    # Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 176
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 177
    .local v0, "b":Ljava/io/ByteArrayOutputStream;
    new-instance v1, Ljava/io/ObjectOutputStream;

    invoke-direct {v1, v0}, Ljava/io/ObjectOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 178
    .local v1, "o":Ljava/io/ObjectOutputStream;
    invoke-virtual {v1, p0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 179
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v2

    return-object v2
.end method

.method public static setPeerScreenId(Ljava/lang/String;)V
    .locals 0
    .param p0, "ext_id"    # Ljava/lang/String;

    .prologue
    .line 428
    sput-object p0, Lcom/samsung/groupcast/application/App;->ext_screenId:Ljava/lang/String;

    .line 429
    return-void
.end method

.method public static setRegister_Extra_App(Z)V
    .locals 0
    .param p0, "register"    # Z

    .prologue
    .line 436
    sput-boolean p0, Lcom/samsung/groupcast/application/App;->register_extra_app:Z

    .line 437
    return-void
.end method

.method public static setScreenId(Ljava/lang/String;)V
    .locals 0
    .param p0, "id"    # Ljava/lang/String;

    .prologue
    .line 420
    sput-object p0, Lcom/samsung/groupcast/application/App;->screenId:Ljava/lang/String;

    .line 421
    return-void
.end method

.method public static setTopActivity(Ljava/lang/String;)V
    .locals 3
    .param p0, "ActivityName"    # Ljava/lang/String;

    .prologue
    .line 372
    const-string v0, "GroupPlayBand"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/samsung/groupcast/application/App;->mTopActivity:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "->"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 374
    sput-object p0, Lcom/samsung/groupcast/application/App;->mTopActivity:Ljava/lang/String;

    .line 375
    return-void
.end method

.method public static updateGameXmlDownloadTime()V
    .locals 2

    .prologue
    .line 378
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    sput-wide v0, Lcom/samsung/groupcast/application/App;->mGameXmlDownloadTime:J

    .line 379
    return-void
.end method


# virtual methods
.method public isForeground(Ljava/lang/String;)Z
    .locals 8
    .param p1, "pkgname"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 217
    const-string v6, "activity"

    invoke-virtual {p0, v6}, Lcom/samsung/groupcast/application/App;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 219
    .local v0, "am":Landroid/app/ActivityManager;
    invoke-virtual {v0, v4}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v6

    invoke-interface {v6, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/ActivityManager$RunningTaskInfo;

    .line 220
    .local v3, "info":Landroid/app/ActivityManager$RunningTaskInfo;
    iget-object v6, v3, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    invoke-virtual {v6}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v2

    .line 221
    .local v2, "currentTopPkgName":Ljava/lang/String;
    iget-object v6, v3, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    invoke-virtual {v6}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v1

    .line 223
    .local v1, "currentTopClassName":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 224
    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 227
    :goto_0
    return v4

    :cond_0
    move v4, v5

    goto :goto_0
.end method

.method public isRunning(Ljava/lang/String;)Z
    .locals 8
    .param p1, "pkgname"    # Ljava/lang/String;

    .prologue
    .line 198
    const-string v6, "activity"

    invoke-virtual {p0, v6}, Lcom/samsung/groupcast/application/App;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 200
    .local v0, "am":Landroid/app/ActivityManager;
    const/16 v6, 0x32

    invoke-virtual {v0, v6}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v5

    .line 202
    .local v5, "taskInfos":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/app/ActivityManager$RunningTaskInfo;

    .line 204
    .local v4, "info":Landroid/app/ActivityManager$RunningTaskInfo;
    iget-object v6, v4, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    invoke-virtual {v6}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v2

    .line 205
    .local v2, "currentTopPkgName":Ljava/lang/String;
    iget-object v6, v4, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    invoke-virtual {v6}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v1

    .line 207
    .local v1, "currentTopClassName":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 208
    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 209
    const/4 v6, 0x1

    .line 212
    .end local v1    # "currentTopClassName":Ljava/lang/String;
    .end local v2    # "currentTopPkgName":Ljava/lang/String;
    .end local v4    # "info":Landroid/app/ActivityManager$RunningTaskInfo;
    :goto_0
    return v6

    :cond_1
    const/4 v6, 0x0

    goto :goto_0
.end method

.method public moveToTaskForeGround(Ljava/lang/String;)V
    .locals 7
    .param p1, "pkgName"    # Ljava/lang/String;

    .prologue
    .line 234
    const-string v4, "activity"

    invoke-virtual {p0, v4}, Lcom/samsung/groupcast/application/App;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 236
    .local v0, "am":Landroid/app/ActivityManager;
    const/16 v4, 0x32

    invoke-virtual {v0, v4}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v3

    .line 238
    .local v3, "taskInfos":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/ActivityManager$RunningTaskInfo;

    .line 240
    .local v2, "info":Landroid/app/ActivityManager$RunningTaskInfo;
    const-string v4, "GroupPlayBand"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, v2, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    invoke-virtual {v6}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 241
    iget-object v4, v2, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    invoke-virtual {v4}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 242
    const-string v4, "GroupPlayBand"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Move to foreground"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 243
    iget v4, v2, Landroid/app/ActivityManager$RunningTaskInfo;->id:I

    const/4 v5, 0x0

    invoke-virtual {v0, v4, v5}, Landroid/app/ActivityManager;->moveTaskToFront(II)V

    goto :goto_0

    .line 246
    .end local v2    # "info":Landroid/app/ActivityManager$RunningTaskInfo;
    :cond_1
    const/4 v3, 0x0

    .line 247
    const/4 v0, 0x0

    .line 248
    return-void
.end method

.method public onCreate()V
    .locals 11

    .prologue
    const/4 v10, 0x0

    const/4 v9, 0x2

    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 118
    sput-object p0, Lcom/samsung/groupcast/application/App;->sInstance:Lcom/samsung/groupcast/application/App;

    .line 122
    invoke-super {p0}, Landroid/app/Application;->onCreate()V

    .line 123
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/App;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const/high16 v4, 0x7f0c0000

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v2

    .line 124
    .local v2, "forceLogging":Z
    invoke-static {v2}, Lcom/samsung/groupcast/application/Logger;->setForceLoggingEnabled(Z)V

    .line 125
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    const-string v4, "onCreate"

    new-array v5, v9, [Ljava/lang/Object;

    const-string v6, "version"

    aput-object v6, v5, v8

    invoke-static {}, Lcom/samsung/groupcast/application/App;->getVersionName()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v3, v4, v10, v5}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 127
    invoke-static {}, Lcom/samsung/groupcast/application/Environment;->isVenderSpecificIdFeatureAvailable()Z

    move-result v3

    sput-boolean v3, Lcom/samsung/groupcast/application/App;->mVendorSpecificAPAvaliable:Z

    .line 128
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    const-string v4, "onCreate"

    new-array v5, v9, [Ljava/lang/Object;

    const-string v6, "vender specific ID feature "

    aput-object v6, v5, v8

    sget-boolean v6, Lcom/samsung/groupcast/application/App;->mVendorSpecificAPAvaliable:Z

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v3, v4, v10, v5}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 131
    invoke-static {v7}, Lcom/samsung/groupcast/misc/utility/FileTools;->getDirectoryForImageResized(Z)Ljava/lang/String;

    .line 133
    invoke-static {}, Ljava/lang/Thread;->getDefaultUncaughtExceptionHandler()Ljava/lang/Thread$UncaughtExceptionHandler;

    move-result-object v1

    .line 134
    .local v1, "defaultHandler":Ljava/lang/Thread$UncaughtExceptionHandler;
    new-instance v0, Lcom/samsung/groupcast/application/App$1;

    invoke-direct {v0, p0, v1}, Lcom/samsung/groupcast/application/App$1;-><init>(Lcom/samsung/groupcast/application/App;Ljava/lang/Thread$UncaughtExceptionHandler;)V

    .line 142
    .local v0, "appHandler":Ljava/lang/Thread$UncaughtExceptionHandler;
    invoke-static {v0}, Ljava/lang/Thread;->setDefaultUncaughtExceptionHandler(Ljava/lang/Thread$UncaughtExceptionHandler;)V

    .line 144
    invoke-direct {p0}, Lcom/samsung/groupcast/application/App;->preLoadMusicLiveCheck()Z

    move-result v3

    sput-boolean v3, Lcom/samsung/groupcast/application/App;->mIsMusicLiveInstalled:Z

    .line 145
    invoke-direct {p0}, Lcom/samsung/groupcast/application/App;->preLoadShareVideoCheck()Z

    move-result v3

    sput-boolean v3, Lcom/samsung/groupcast/application/App;->mIsShareVideoInstalled:Z

    .line 146
    invoke-static {}, Lcom/samsung/groupcast/application/App;->preLoadGroupCamcoderCheck()Z

    move-result v3

    sput-boolean v3, Lcom/samsung/groupcast/application/App;->mIsGroupCamcoderInstalled:Z

    .line 148
    invoke-static {}, Lcom/samsung/groupcast/debug/DebugFlags;->getInstance()Lcom/samsung/groupcast/debug/DebugFlags;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/groupcast/debug/DebugFlags;->logMemStatus()V

    .line 153
    return-void
.end method

.method public startLaunchActivity(Ljava/lang/String;)V
    .locals 9
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    const/4 v8, 0x0

    .line 251
    new-instance v2, Landroid/content/Intent;

    const-string v6, "android.intent.action.MAIN"

    invoke-direct {v2, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 252
    .local v2, "intentToResolve":Landroid/content/Intent;
    const-string v6, "android.intent.category.LAUNCHER"

    invoke-virtual {v2, v6}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 253
    invoke-virtual {v2, p1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 254
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/App;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    .line 255
    .local v3, "manager":Landroid/content/pm/PackageManager;
    invoke-virtual {v3, v2, v8}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v5

    .line 256
    .local v5, "ris":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    if-eqz v5, :cond_0

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v6

    if-gtz v6, :cond_2

    .line 257
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/App;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    .line 258
    .local v4, "pm":Landroid/content/pm/PackageManager;
    invoke-virtual {v4, p1}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 259
    .local v1, "in":Landroid/content/Intent;
    if-eqz v1, :cond_1

    .line 260
    const-string v6, "StartActivity by getLaunchIntentForPackage"

    invoke-static {v6}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 261
    invoke-virtual {p0, v1}, Lcom/samsung/groupcast/application/App;->startActivity(Landroid/content/Intent;)V

    .line 274
    .end local v1    # "in":Landroid/content/Intent;
    .end local v4    # "pm":Landroid/content/pm/PackageManager;
    :cond_1
    :goto_0
    const-string v6, "startLaunchActivity end"

    invoke-static {v6}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 275
    return-void

    .line 266
    :cond_2
    new-instance v0, Landroid/content/Intent;

    const-string v6, "android.intent.action.MAIN"

    invoke-direct {v0, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 267
    .local v0, "i":Landroid/content/Intent;
    const-string v6, "android.intent.category.LAUNCHER"

    invoke-virtual {v0, v6}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 268
    const/high16 v6, 0x10200000

    invoke-virtual {v0, v6}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 269
    invoke-interface {v5, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/content/pm/ResolveInfo;

    iget-object v6, v6, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v7, v6, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-interface {v5, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/content/pm/ResolveInfo;

    iget-object v6, v6, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v6, v6, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-virtual {v0, v7, v6}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 270
    invoke-virtual {p0, v0}, Lcom/samsung/groupcast/application/App;->startActivity(Landroid/content/Intent;)V

    .line 271
    const-string v6, "StartActivity by setClassName"

    invoke-static {v6}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    goto :goto_0
.end method
