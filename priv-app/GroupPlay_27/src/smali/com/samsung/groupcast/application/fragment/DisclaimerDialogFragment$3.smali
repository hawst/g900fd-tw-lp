.class Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment$3;
.super Ljava/lang/Object;
.source "DisclaimerDialogFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;


# direct methods
.method constructor <init>(Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;)V
    .locals 0

    .prologue
    .line 308
    iput-object p1, p0, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment$3;->this$0:Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 1
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 311
    iget-object v0, p0, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment$3;->this$0:Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;

    # getter for: Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->mCheckBox:Landroid/widget/CheckBox;
    invoke-static {v0}, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->access$100(Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 312
    iget-object v0, p0, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment$3;->this$0:Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;

    # getter for: Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->mType:I
    invoke-static {v0}, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->access$300(Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;)I

    move-result v0

    invoke-static {v0}, Lcom/samsung/groupcast/application/Preferences;->setDisclaimerConfirmed(I)V

    .line 314
    :cond_0
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/samsung/groupcast/application/Preferences;->setDisclimerPreference(Z)V

    .line 315
    iget-object v0, p0, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment$3;->this$0:Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;

    # invokes: Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->getDelegate()Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment$Delegate;
    invoke-static {v0}, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->access$200(Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;)Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment$Delegate;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 316
    iget-object v0, p0, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment$3;->this$0:Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;

    # invokes: Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->getDelegate()Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment$Delegate;
    invoke-static {v0}, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->access$200(Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;)Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment$Delegate;

    move-result-object v0

    instance-of v0, v0, Lcom/samsung/groupcast/application/start/StartActivity;

    if-eqz v0, :cond_1

    .line 317
    iget-object v0, p0, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment$3;->this$0:Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;

    # invokes: Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->getDelegate()Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment$Delegate;
    invoke-static {v0}, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->access$200(Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;)Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment$Delegate;

    move-result-object v0

    invoke-interface {v0}, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment$Delegate;->doUpdateFromDisclaimer()V

    .line 320
    :cond_1
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 321
    return-void
.end method
