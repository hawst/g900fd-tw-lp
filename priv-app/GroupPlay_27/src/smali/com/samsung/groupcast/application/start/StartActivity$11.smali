.class Lcom/samsung/groupcast/application/start/StartActivity$11;
.super Ljava/lang/Object;
.source "StartActivity.java"

# interfaces
.implements Lcom/sec/android/band/BandInterface$BandEventHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/groupcast/application/start/StartActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/groupcast/application/start/StartActivity;


# direct methods
.method constructor <init>(Lcom/samsung/groupcast/application/start/StartActivity;)V
    .locals 0

    .prologue
    .line 2061
    iput-object p1, p0, Lcom/samsung/groupcast/application/start/StartActivity$11;->this$0:Lcom/samsung/groupcast/application/start/StartActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onConfiguration(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)I
    .locals 6
    .param p1, "ssid"    # Ljava/lang/String;
    .param p2, "bssid"    # Ljava/lang/String;
    .param p3, "PeerScreenId"    # Ljava/lang/String;
    .param p4, "defSsid"    # Ljava/lang/String;
    .param p5, "defBssid"    # Ljava/lang/String;
    .param p6, "defPassphrase"    # Ljava/lang/String;
    .param p7, "defEncryption"    # Ljava/lang/String;
    .param p8, "defAuthentication"    # Ljava/lang/String;
    .param p9, "newConnection"    # Z

    .prologue
    const/4 v5, 0x0

    .line 2181
    invoke-static {v5}, Lcom/samsung/groupcast/application/App;->SET_EXTERNAL_APP(Z)V

    .line 2182
    const-string v2, "GroupPlayBand"

    const-string v3, "onConfiguration"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2184
    const-class v2, Lcom/samsung/groupcast/application/start/StartActivity;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/groupcast/application/App;->isTopActivity(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 2236
    :cond_0
    :goto_0
    return v5

    .line 2188
    :cond_1
    const-string v2, "GroupPlayBand"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onConfiguration ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiUtils;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getWifiBSSID()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2190
    if-eqz p2, :cond_2

    const-string v2, "00:00:00:00:00:00"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getWifiBSSID()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getWifiBSSID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 2193
    const-string v2, "GroupPlayBand"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "other Group return ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiUtils;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getWifiBSSID()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2195
    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiAPUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->isRvfHotspotEnabled()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2204
    :cond_2
    invoke-static {}, Lcom/samsung/groupcast/application/App;->isRegister_Extra_App()Z

    move-result v2

    if-nez v2, :cond_4

    .line 2205
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/samsung/groupcast/application/App;->getInstance()Lcom/samsung/groupcast/application/App;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/groupcast/application/App;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-class v3, Lcom/samsung/groupcast/application/start/StartActivity;

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2211
    .local v0, "MyScreenId":Ljava/lang/String;
    :goto_1
    const-string v2, "GroupPlayBand"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "MyScreenId:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2213
    const-string v2, ":"

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 2215
    .local v1, "MyScreenId_splited":[Ljava/lang/String;
    if-eqz v1, :cond_3

    aget-object v2, v1, v5

    iget-object v3, p0, Lcom/samsung/groupcast/application/start/StartActivity$11;->this$0:Lcom/samsung/groupcast/application/start/StartActivity;

    invoke-virtual {v3}, Lcom/samsung/groupcast/application/start/StartActivity;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 2216
    const/4 v2, 0x1

    invoke-static {v2}, Lcom/samsung/groupcast/application/App;->SET_EXTERNAL_APP(Z)V

    .line 2218
    :cond_3
    const-string v2, "GroupPlayBand"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "CONFIGURATION RECEIVE SERVICE_INFO:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2220
    if-nez p3, :cond_5

    .line 2222
    const-string v2, "GroupPlayBand"

    const-string v3, "peer is idle(GroupPlay is not launched)"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2228
    const-string v2, "GroupPlayBand"

    const-string v3, "I have no session. peer is idle."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2229
    const-string v2, "GroupPlayBand"

    const-string v3, "This scenario is not supported."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 2209
    .end local v0    # "MyScreenId":Ljava/lang/String;
    .end local v1    # "MyScreenId_splited":[Ljava/lang/String;
    :cond_4
    invoke-static {}, Lcom/samsung/groupcast/application/App;->getScreenId()Ljava/lang/String;

    move-result-object v0

    .restart local v0    # "MyScreenId":Ljava/lang/String;
    goto :goto_1

    .line 2232
    .restart local v1    # "MyScreenId_splited":[Ljava/lang/String;
    :cond_5
    const-string v2, "GroupPlayBand"

    const-string v3, "peer is launched(GP or with external app)"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2233
    const-string v2, "GroupPlayBand"

    const-string v3, "I will do nothing."

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method public onNdefDiscovered(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    .locals 11
    .param p1, "ssid"    # Ljava/lang/String;
    .param p2, "bssid"    # Ljava/lang/String;
    .param p3, "passphrase"    # Ljava/lang/String;
    .param p4, "chunk"    # Ljava/lang/String;

    .prologue
    .line 2068
    const/4 v8, 0x0

    invoke-static {v8}, Lcom/samsung/groupcast/application/App;->SET_EXTERNAL_APP(Z)V

    .line 2069
    const-string v8, "GroupPlayBand"

    const-string v9, "onNdefDiscovered"

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2075
    const-class v8, Lcom/samsung/groupcast/application/start/StartActivity;

    invoke-virtual {v8}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/samsung/groupcast/application/App;->isTopActivity(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 2076
    const/4 v8, 0x0

    .line 2172
    :goto_0
    return v8

    .line 2077
    :cond_0
    iget-object v8, p0, Lcom/samsung/groupcast/application/start/StartActivity$11;->this$0:Lcom/samsung/groupcast/application/start/StartActivity;

    # getter for: Lcom/samsung/groupcast/application/start/StartActivity;->mSavedInstanceStateCalled:Z
    invoke-static {v8}, Lcom/samsung/groupcast/application/start/StartActivity;->access$800(Lcom/samsung/groupcast/application/start/StartActivity;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 2078
    const/4 v8, 0x0

    goto :goto_0

    .line 2080
    :cond_1
    const/4 v8, 0x1

    # setter for: Lcom/samsung/groupcast/application/start/StartActivity;->mNfcConnectOngoing:Z
    invoke-static {v8}, Lcom/samsung/groupcast/application/start/StartActivity;->access$902(Z)Z

    .line 2082
    const/4 v1, 0x0

    .line 2083
    .local v1, "PeerScreenId":Ljava/lang/String;
    const/4 v7, 0x0

    .line 2084
    .local v7, "ss":Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;
    const/4 v6, 0x0

    .line 2085
    .local v6, "sessionPin":Ljava/lang/String;
    const-string v2, ""

    .line 2087
    .local v2, "SessionSummaryString":Ljava/lang/String;
    const-string v8, "GroupPlayBand"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "chunk["

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "]"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2088
    if-eqz p4, :cond_4

    .line 2089
    const-string v8, ";"

    invoke-virtual {p4, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 2090
    .local v4, "chunk_splitted":[Ljava/lang/String;
    const/4 v8, 0x0

    aget-object v2, v4, v8

    .line 2091
    if-eqz v2, :cond_2

    .line 2092
    const-string v8, "GroupPlayBand"

    const-string v9, "String to SessinSummary"

    invoke-static {v8, v9}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 2093
    invoke-static {v2}, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;->BandStringtoSessionSummary(Ljava/lang/String;)Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;

    move-result-object v7

    .line 2094
    if-nez v7, :cond_2

    .line 2095
    const-string v8, "GroupPlayBand"

    const-string v9, "SessinSummary is null"

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2099
    :cond_2
    array-length v8, v4

    const/4 v9, 0x3

    if-lt v8, v9, :cond_3

    .line 2100
    const/4 v8, 0x1

    aget-object v6, v4, v8

    .line 2101
    iget-object v8, p0, Lcom/samsung/groupcast/application/start/StartActivity$11;->this$0:Lcom/samsung/groupcast/application/start/StartActivity;

    # setter for: Lcom/samsung/groupcast/application/start/StartActivity;->mPinFromBand:Ljava/lang/String;
    invoke-static {v8, v6}, Lcom/samsung/groupcast/application/start/StartActivity;->access$1002(Lcom/samsung/groupcast/application/start/StartActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 2104
    :cond_3
    array-length v8, v4

    add-int/lit8 v8, v8, -0x1

    aget-object v1, v4, v8

    .line 2105
    invoke-static {v1}, Lcom/samsung/groupcast/application/App;->setPeerScreenId(Ljava/lang/String;)V

    .line 2108
    .end local v4    # "chunk_splitted":[Ljava/lang/String;
    :cond_4
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "processing nfc: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " / "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "("

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/samsung/groupcast/application/start/StartActivity$11;->this$0:Lcom/samsung/groupcast/application/start/StartActivity;

    # getter for: Lcom/samsung/groupcast/application/start/StartActivity;->mWifiUtils:Lcom/samsung/groupcast/net/wifi/WifiUtils;
    invoke-static {v9}, Lcom/samsung/groupcast/application/start/StartActivity;->access$1100(Lcom/samsung/groupcast/application/start/StartActivity;)Lcom/samsung/groupcast/net/wifi/WifiUtils;

    move-result-object v9

    invoke-virtual {v9}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getWifiBSSID()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 2111
    iget-object v8, p0, Lcom/samsung/groupcast/application/start/StartActivity$11;->this$0:Lcom/samsung/groupcast/application/start/StartActivity;

    # getter for: Lcom/samsung/groupcast/application/start/StartActivity;->mSavedInstanceStateCalled:Z
    invoke-static {v8}, Lcom/samsung/groupcast/application/start/StartActivity;->access$800(Lcom/samsung/groupcast/application/start/StartActivity;)Z

    move-result v8

    if-nez v8, :cond_7

    .line 2112
    iget-object v8, p0, Lcom/samsung/groupcast/application/start/StartActivity$11;->this$0:Lcom/samsung/groupcast/application/start/StartActivity;

    # getter for: Lcom/samsung/groupcast/application/start/StartActivity;->mWifiUtils:Lcom/samsung/groupcast/net/wifi/WifiUtils;
    invoke-static {v8}, Lcom/samsung/groupcast/application/start/StartActivity;->access$1100(Lcom/samsung/groupcast/application/start/StartActivity;)Lcom/samsung/groupcast/net/wifi/WifiUtils;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getWifiStatus()Ljava/lang/String;

    move-result-object v8

    const-string v9, "CONNECTED"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_5

    if-eqz p2, :cond_5

    iget-object v8, p0, Lcom/samsung/groupcast/application/start/StartActivity$11;->this$0:Lcom/samsung/groupcast/application/start/StartActivity;

    # getter for: Lcom/samsung/groupcast/application/start/StartActivity;->mWifiUtils:Lcom/samsung/groupcast/net/wifi/WifiUtils;
    invoke-static {v8}, Lcom/samsung/groupcast/application/start/StartActivity;->access$1100(Lcom/samsung/groupcast/application/start/StartActivity;)Lcom/samsung/groupcast/net/wifi/WifiUtils;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getWifiBSSID()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p2, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_9

    .line 2115
    :cond_5
    if-nez p3, :cond_6

    .line 2116
    # getter for: Lcom/samsung/groupcast/application/start/StartActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/groupcast/application/start/StartActivity;->access$1200()Ljava/lang/String;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Not connected to WiFi network: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " ("

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ") [connected to: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/samsung/groupcast/application/start/StartActivity$11;->this$0:Lcom/samsung/groupcast/application/start/StartActivity;

    # getter for: Lcom/samsung/groupcast/application/start/StartActivity;->mWifiUtils:Lcom/samsung/groupcast/net/wifi/WifiUtils;
    invoke-static {v10}, Lcom/samsung/groupcast/application/start/StartActivity;->access$1100(Lcom/samsung/groupcast/application/start/StartActivity;)Lcom/samsung/groupcast/net/wifi/WifiUtils;

    move-result-object v10

    invoke-virtual {v10}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getWifiBSSID()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "]"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/samsung/groupcast/application/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2118
    const/4 v8, 0x0

    # setter for: Lcom/samsung/groupcast/application/start/StartActivity;->mNfcConnectOngoing:Z
    invoke-static {v8}, Lcom/samsung/groupcast/application/start/StartActivity;->access$902(Z)Z

    .line 2119
    const/4 v8, 0x0

    goto/16 :goto_0

    .line 2122
    :cond_6
    const-string v8, "GroupPlayBand"

    const-string v9, "mSavedInstanceStateCalled"

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2123
    iget-object v8, p0, Lcom/samsung/groupcast/application/start/StartActivity$11;->this$0:Lcom/samsung/groupcast/application/start/StartActivity;

    # setter for: Lcom/samsung/groupcast/application/start/StartActivity;->mBandTargetSSID:Ljava/lang/String;
    invoke-static {v8, p1}, Lcom/samsung/groupcast/application/start/StartActivity;->access$1302(Lcom/samsung/groupcast/application/start/StartActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 2124
    iget-object v8, p0, Lcom/samsung/groupcast/application/start/StartActivity$11;->this$0:Lcom/samsung/groupcast/application/start/StartActivity;

    # setter for: Lcom/samsung/groupcast/application/start/StartActivity;->mBandPassphrase:Ljava/lang/String;
    invoke-static {v8, p3}, Lcom/samsung/groupcast/application/start/StartActivity;->access$1402(Lcom/samsung/groupcast/application/start/StartActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 2126
    iget-object v8, p0, Lcom/samsung/groupcast/application/start/StartActivity$11;->this$0:Lcom/samsung/groupcast/application/start/StartActivity;

    iget v8, v8, Lcom/samsung/groupcast/application/start/StartActivity;->mHowManyDisclaimerShowing:I

    const/4 v9, 0x1

    if-ge v8, v9, :cond_7

    .line 2127
    iget-object v8, p0, Lcom/samsung/groupcast/application/start/StartActivity$11;->this$0:Lcom/samsung/groupcast/application/start/StartActivity;

    iget-object v9, p0, Lcom/samsung/groupcast/application/start/StartActivity$11;->this$0:Lcom/samsung/groupcast/application/start/StartActivity;

    # getter for: Lcom/samsung/groupcast/application/start/StartActivity;->mBandTargetSSID:Ljava/lang/String;
    invoke-static {v9}, Lcom/samsung/groupcast/application/start/StartActivity;->access$1300(Lcom/samsung/groupcast/application/start/StartActivity;)Ljava/lang/String;

    move-result-object v9

    iget-object v10, p0, Lcom/samsung/groupcast/application/start/StartActivity$11;->this$0:Lcom/samsung/groupcast/application/start/StartActivity;

    # getter for: Lcom/samsung/groupcast/application/start/StartActivity;->mBandPassphrase:Ljava/lang/String;
    invoke-static {v10}, Lcom/samsung/groupcast/application/start/StartActivity;->access$1400(Lcom/samsung/groupcast/application/start/StartActivity;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v9, p2, v10}, Lcom/samsung/groupcast/application/start/StartActivity;->startJoinProcess(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2138
    :cond_7
    :goto_1
    const-string v8, "GroupPlayBand"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/samsung/groupcast/application/App;->getInstance()Lcom/samsung/groupcast/application/App;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " NDEF_DISCOVERD RECEIVED SCREENID:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2141
    invoke-static {}, Lcom/samsung/groupcast/application/App;->isRegister_Extra_App()Z

    move-result v8

    const/4 v9, 0x1

    if-ne v8, v9, :cond_a

    .line 2142
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 2143
    .local v3, "bundle":Landroid/os/Bundle;
    const-string v8, "com.samsung.groupcast.extra.SCREEN_ID"

    invoke-virtual {v3, v8, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2144
    new-instance v5, Landroid/content/Intent;

    const-string v8, "com.samsung.groupcast.action.GP_SERVICE"

    invoke-direct {v5, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2145
    .local v5, "in":Landroid/content/Intent;
    invoke-virtual {v5, v3}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 2146
    iget-object v8, p0, Lcom/samsung/groupcast/application/start/StartActivity$11;->this$0:Lcom/samsung/groupcast/application/start/StartActivity;

    invoke-virtual {v8, v5}, Lcom/samsung/groupcast/application/start/StartActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 2147
    const/4 v3, 0x0

    .line 2148
    const-string v8, "GroupPlayBand"

    const-string v9, "[GP] send GroupPlayManager.GP_SERVICE"

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2149
    const-string v8, "GroupPlayBand"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "     SCREENID : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2172
    .end local v3    # "bundle":Landroid/os/Bundle;
    .end local v5    # "in":Landroid/content/Intent;
    :cond_8
    :goto_2
    const/4 v8, 0x0

    goto/16 :goto_0

    .line 2130
    :cond_9
    if-eqz v7, :cond_7

    .line 2131
    iget-object v8, p0, Lcom/samsung/groupcast/application/start/StartActivity$11;->this$0:Lcom/samsung/groupcast/application/start/StartActivity;

    # setter for: Lcom/samsung/groupcast/application/start/StartActivity;->mBandSessionSummery:Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;
    invoke-static {v8, v7}, Lcom/samsung/groupcast/application/start/StartActivity;->access$1502(Lcom/samsung/groupcast/application/start/StartActivity;Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;)Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;

    .line 2132
    iget-object v8, p0, Lcom/samsung/groupcast/application/start/StartActivity$11;->this$0:Lcom/samsung/groupcast/application/start/StartActivity;

    # setter for: Lcom/samsung/groupcast/application/start/StartActivity;->mBandSessionPin:Ljava/lang/String;
    invoke-static {v8, v6}, Lcom/samsung/groupcast/application/start/StartActivity;->access$1602(Lcom/samsung/groupcast/application/start/StartActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 2133
    iget-object v8, p0, Lcom/samsung/groupcast/application/start/StartActivity$11;->this$0:Lcom/samsung/groupcast/application/start/StartActivity;

    iget-object v9, p0, Lcom/samsung/groupcast/application/start/StartActivity$11;->this$0:Lcom/samsung/groupcast/application/start/StartActivity;

    # getter for: Lcom/samsung/groupcast/application/start/StartActivity;->mBandSessionSummery:Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;
    invoke-static {v9}, Lcom/samsung/groupcast/application/start/StartActivity;->access$1500(Lcom/samsung/groupcast/application/start/StartActivity;)Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;

    move-result-object v9

    iget-object v10, p0, Lcom/samsung/groupcast/application/start/StartActivity$11;->this$0:Lcom/samsung/groupcast/application/start/StartActivity;

    # getter for: Lcom/samsung/groupcast/application/start/StartActivity;->mBandSessionPin:Ljava/lang/String;
    invoke-static {v10}, Lcom/samsung/groupcast/application/start/StartActivity;->access$1600(Lcom/samsung/groupcast/application/start/StartActivity;)Ljava/lang/String;

    move-result-object v10

    # invokes: Lcom/samsung/groupcast/application/start/StartActivity;->doJoinFromBand(Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;Ljava/lang/String;)V
    invoke-static {v8, v9, v10}, Lcom/samsung/groupcast/application/start/StartActivity;->access$1700(Lcom/samsung/groupcast/application/start/StartActivity;Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2151
    :cond_a
    if-nez v1, :cond_b

    .line 2152
    const-string v8, "PeerScreenId is null"

    invoke-static {v8}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 2153
    const/4 v8, 0x0

    goto/16 :goto_0

    .line 2156
    :cond_b
    const-string v8, ":"

    invoke-virtual {v1, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 2157
    .local v0, "PeerScreeId_splited":[Ljava/lang/String;
    const-string v8, "GroupPlayBand"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "["

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-static {}, Lcom/samsung/groupcast/application/App;->getInstance()Lcom/samsung/groupcast/application/App;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "] PeerScreeId_splited[0] :"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const/4 v10, 0x0

    aget-object v10, v0, v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2159
    const-string v8, "GroupPlayBand"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "["

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-static {}, Lcom/samsung/groupcast/application/App;->getInstance()Lcom/samsung/groupcast/application/App;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "] PeerScreeId_splited[1] :"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const/4 v10, 0x1

    aget-object v10, v0, v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2161
    array-length v8, v0

    const/4 v9, 0x3

    if-ne v8, v9, :cond_8

    .line 2162
    const-string v8, "GroupPlayBand"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "["

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-static {}, Lcom/samsung/groupcast/application/App;->getInstance()Lcom/samsung/groupcast/application/App;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "] split_data[2] :"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const/4 v10, 0x2

    aget-object v10, v0, v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2165
    const/4 v8, 0x0

    aget-object v8, v0, v8

    const-string v9, "com.sec.android.app.mediasync"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_c

    .line 2166
    iget-object v8, p0, Lcom/samsung/groupcast/application/start/StartActivity$11;->this$0:Lcom/samsung/groupcast/application/start/StartActivity;

    const/4 v9, 0x2

    aget-object v9, v0, v9

    # setter for: Lcom/samsung/groupcast/application/start/StartActivity;->mServiceType:Ljava/lang/String;
    invoke-static {v8, v9}, Lcom/samsung/groupcast/application/start/StartActivity;->access$1802(Lcom/samsung/groupcast/application/start/StartActivity;Ljava/lang/String;)Ljava/lang/String;

    goto/16 :goto_2

    .line 2168
    :cond_c
    iget-object v8, p0, Lcom/samsung/groupcast/application/start/StartActivity$11;->this$0:Lcom/samsung/groupcast/application/start/StartActivity;

    const/4 v9, 0x2

    aget-object v9, v0, v9

    # setter for: Lcom/samsung/groupcast/application/start/StartActivity;->mMenuType:Ljava/lang/String;
    invoke-static {v8, v9}, Lcom/samsung/groupcast/application/start/StartActivity;->access$1902(Lcom/samsung/groupcast/application/start/StartActivity;Ljava/lang/String;)Ljava/lang/String;

    goto/16 :goto_2
.end method

.method public onNetworkStatus(Ljava/lang/String;)I
    .locals 2
    .param p1, "connectionStatus"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 2243
    const-class v0, Lcom/samsung/groupcast/application/start/StartActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/groupcast/application/App;->isTopActivity(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2248
    :goto_0
    return v1

    .line 2246
    :cond_0
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/StartActivity$11;->this$0:Lcom/samsung/groupcast/application/start/StartActivity;

    invoke-virtual {v0}, Lcom/samsung/groupcast/application/start/StartActivity;->startNextActivity()V

    goto :goto_0
.end method

.method public onPreNdefDiscovered()I
    .locals 5

    .prologue
    const/4 v1, -0x1

    const/4 v2, 0x0

    .line 2259
    const-string v3, "GroupPlayBand"

    const-string v4, "onPreNdefDiscovered"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2261
    const-class v3, Lcom/samsung/groupcast/application/start/StartActivity;

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/samsung/groupcast/application/App;->isTopActivity(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 2262
    const-string v2, "GroupPlayBand"

    const-string v3, "isTopActivity : false return"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2287
    :goto_0
    return v1

    .line 2266
    :cond_0
    invoke-static {}, Lcom/samsung/groupcast/application/App;->HAS_EXTERNAL_APP()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2267
    const-string v3, "GroupPlayBand"

    const-string v4, "HAS_EXTERNAL_APP!"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2268
    invoke-static {v2}, Lcom/samsung/groupcast/application/App;->SET_EXTERNAL_APP(Z)V

    goto :goto_0

    .line 2272
    :cond_1
    iget-object v1, p0, Lcom/samsung/groupcast/application/start/StartActivity$11;->this$0:Lcom/samsung/groupcast/application/start/StartActivity;

    invoke-virtual {v1}, Lcom/samsung/groupcast/application/start/StartActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/samsung/groupcast/application/App;

    .line 2273
    .local v0, "app":Lcom/samsung/groupcast/application/App;
    invoke-static {}, Lcom/samsung/groupcast/application/App;->getInstance()Lcom/samsung/groupcast/application/App;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/groupcast/application/App;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/groupcast/application/App;->isForeground(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2274
    const-string v1, "GroupPlayBand"

    const-string v3, "I\'m in foreground!"

    invoke-static {v1, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move v1, v2

    .line 2275
    goto :goto_0

    .line 2278
    :cond_2
    invoke-static {}, Lcom/samsung/groupcast/application/App;->IsSbeamOrGalleryForeground()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2279
    const-string v1, "GroupPlayBand"

    const-string v3, "sbeam or gallery is foreground!"

    invoke-static {v1, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move v1, v2

    .line 2280
    goto :goto_0

    .line 2283
    :cond_3
    const-string v1, "GroupPlayBand"

    const-string v3, "I\'m in background!"

    invoke-static {v1, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2285
    invoke-static {}, Lcom/samsung/groupcast/application/App;->getInstance()Lcom/samsung/groupcast/application/App;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/groupcast/application/App;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/groupcast/application/App;->moveToTaskForeGround(Ljava/lang/String;)V

    move v1, v2

    .line 2287
    goto :goto_0
.end method

.method public onRequestNdefMessage()Landroid/nfc/NdefMessage;
    .locals 1

    .prologue
    .line 2254
    const/4 v0, 0x0

    return-object v0
.end method
