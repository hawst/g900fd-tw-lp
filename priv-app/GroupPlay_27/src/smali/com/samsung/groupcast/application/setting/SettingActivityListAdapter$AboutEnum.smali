.class public final enum Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$AboutEnum;
.super Ljava/lang/Enum;
.source "SettingActivityListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "AboutEnum"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$AboutEnum;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$AboutEnum;

.field public static final enum ABOUT_FAQ:Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$AboutEnum;

.field public static final enum ABOUT_NOTICE:Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$AboutEnum;

.field public static final enum ABOUT_VERSION:Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$AboutEnum;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 41
    new-instance v0, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$AboutEnum;

    const-string v1, "ABOUT_FAQ"

    invoke-direct {v0, v1, v2}, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$AboutEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$AboutEnum;->ABOUT_FAQ:Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$AboutEnum;

    .line 42
    new-instance v0, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$AboutEnum;

    const-string v1, "ABOUT_NOTICE"

    invoke-direct {v0, v1, v3}, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$AboutEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$AboutEnum;->ABOUT_NOTICE:Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$AboutEnum;

    .line 43
    new-instance v0, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$AboutEnum;

    const-string v1, "ABOUT_VERSION"

    invoke-direct {v0, v1, v4}, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$AboutEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$AboutEnum;->ABOUT_VERSION:Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$AboutEnum;

    .line 40
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$AboutEnum;

    sget-object v1, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$AboutEnum;->ABOUT_FAQ:Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$AboutEnum;

    aput-object v1, v0, v2

    sget-object v1, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$AboutEnum;->ABOUT_NOTICE:Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$AboutEnum;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$AboutEnum;->ABOUT_VERSION:Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$AboutEnum;

    aput-object v1, v0, v4

    sput-object v0, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$AboutEnum;->$VALUES:[Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$AboutEnum;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 40
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$AboutEnum;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 40
    const-class v0, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$AboutEnum;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$AboutEnum;

    return-object v0
.end method

.method public static values()[Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$AboutEnum;
    .locals 1

    .prologue
    .line 40
    sget-object v0, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$AboutEnum;->$VALUES:[Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$AboutEnum;

    invoke-virtual {v0}, [Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$AboutEnum;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$AboutEnum;

    return-object v0
.end method
