.class public final enum Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$InvitationEnum;
.super Ljava/lang/Enum;
.source "SettingActivityListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "InvitationEnum"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$InvitationEnum;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$InvitationEnum;

.field public static final enum CHECK_PASSWORD:Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$InvitationEnum;

.field public static final enum HIDDEN_DISCLAIMER:Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$InvitationEnum;

.field public static final enum NEAR_BY_FRIEND_LISTEN:Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$InvitationEnum;

.field public static final enum REMOTE_FRIEND:Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$InvitationEnum;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 34
    new-instance v0, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$InvitationEnum;

    const-string v1, "NEAR_BY_FRIEND_LISTEN"

    invoke-direct {v0, v1, v2}, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$InvitationEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$InvitationEnum;->NEAR_BY_FRIEND_LISTEN:Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$InvitationEnum;

    .line 35
    new-instance v0, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$InvitationEnum;

    const-string v1, "REMOTE_FRIEND"

    invoke-direct {v0, v1, v3}, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$InvitationEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$InvitationEnum;->REMOTE_FRIEND:Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$InvitationEnum;

    .line 36
    new-instance v0, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$InvitationEnum;

    const-string v1, "CHECK_PASSWORD"

    invoke-direct {v0, v1, v4}, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$InvitationEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$InvitationEnum;->CHECK_PASSWORD:Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$InvitationEnum;

    .line 37
    new-instance v0, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$InvitationEnum;

    const-string v1, "HIDDEN_DISCLAIMER"

    invoke-direct {v0, v1, v5}, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$InvitationEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$InvitationEnum;->HIDDEN_DISCLAIMER:Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$InvitationEnum;

    .line 33
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$InvitationEnum;

    sget-object v1, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$InvitationEnum;->NEAR_BY_FRIEND_LISTEN:Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$InvitationEnum;

    aput-object v1, v0, v2

    sget-object v1, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$InvitationEnum;->REMOTE_FRIEND:Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$InvitationEnum;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$InvitationEnum;->CHECK_PASSWORD:Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$InvitationEnum;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$InvitationEnum;->HIDDEN_DISCLAIMER:Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$InvitationEnum;

    aput-object v1, v0, v5

    sput-object v0, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$InvitationEnum;->$VALUES:[Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$InvitationEnum;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 33
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$InvitationEnum;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 33
    const-class v0, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$InvitationEnum;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$InvitationEnum;

    return-object v0
.end method

.method public static values()[Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$InvitationEnum;
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$InvitationEnum;->$VALUES:[Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$InvitationEnum;

    invoke-virtual {v0}, [Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$InvitationEnum;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$InvitationEnum;

    return-object v0
.end method
