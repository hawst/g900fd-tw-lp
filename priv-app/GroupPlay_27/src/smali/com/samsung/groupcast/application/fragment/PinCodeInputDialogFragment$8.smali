.class Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment$8;
.super Ljava/lang/Object;
.source "PinCodeInputDialogFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;


# direct methods
.method constructor <init>(Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;)V
    .locals 0

    .prologue
    .line 269
    iput-object p1, p0, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment$8;->this$0:Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 272
    iget-object v0, p0, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment$8;->this$0:Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;

    # getter for: Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->mDelegate:Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment$PinCodeInputDialogFragmentDelegate;
    invoke-static {v0}, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->access$300(Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;)Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment$PinCodeInputDialogFragmentDelegate;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 273
    iget-object v0, p0, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment$8;->this$0:Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;

    # getter for: Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->mDelegate:Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment$PinCodeInputDialogFragmentDelegate;
    invoke-static {v0}, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->access$300(Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;)Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment$PinCodeInputDialogFragmentDelegate;

    move-result-object v0

    # getter for: Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->mInstance:Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;
    invoke-static {}, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->access$400()Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment$8;->this$0:Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;

    # getter for: Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->mPinEditText:Landroid/widget/EditText;
    invoke-static {v2}, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->access$000(Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;)Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment$PinCodeInputDialogFragmentDelegate;->onPinSubmitted(Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;Ljava/lang/String;Z)V

    .line 276
    :cond_0
    iget-object v0, p0, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment$8;->this$0:Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;

    # getter for: Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->mPinEditText:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->access$000(Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;)Landroid/widget/EditText;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 277
    iget-object v0, p0, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment$8;->this$0:Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;

    # getter for: Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->mPinEditText:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;->access$000(Lcom/samsung/groupcast/application/fragment/PinCodeInputDialogFragment;)Landroid/widget/EditText;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 280
    :cond_1
    return-void
.end method
