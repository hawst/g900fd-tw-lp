.class public Lcom/samsung/groupcast/application/Preferences;
.super Ljava/lang/Object;
.source "Preferences.java"


# static fields
.field private static final CHECK_PASSWORD:Ljava/lang/String; = "CHECK_PASSWORD"

.field public static final CLEAR_CONFIRMS:I = -0x1

.field private static final DAYS_OF_YEAR:I = 0x164

.field private static final DISCLIMER_CHECK:Ljava/lang/String; = "DISCLIMER_CHECK"

.field private static final DOCUMENT_PICKER_START_FOLDER:Ljava/lang/String; = "DOCUMENT_PICKER_START_FOLDER"

.field private static final GAME_XML_VERSION:Ljava/lang/String; = "GAME_XML_VERSION"

.field public static final MODE_FOR_DONT_AGAIN:I = -0x1

.field private static final NEARBY_FRIEND_ADVERTISE:Ljava/lang/String; = "NEARBY_FRIEND_ADVERTISE"

.field public static final NEARBY_FRIEND_LISTEN:Ljava/lang/String; = "NEARBY_FRIEND_LISTEN"

.field private static final NICK_NAME:Ljava/lang/String; = "NICK_NAME"

.field public static final PREFERENCES_FILE_NAME:Ljava/lang/String; = "com.samsung.groupcast.application.preferences"

.field private static final REMOTE_FRIEND:Ljava/lang/String; = "CHECKED_PASSWORD"

.field private static final SKIP_DISCLAIMER_NOTIFICATION_DEFAULT:I = -0x2

.field private static final SKIP_DISCLAIMER_NOTIFICATION_KEY:Ljava/lang/String; = "SKIP_DISCLAIMER_NOTIFICATION"

.field private static final TAG:Ljava/lang/String;

.field private static mDisclaimerConfirmed:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    const-class v0, Lcom/samsung/groupcast/application/Preferences;

    invoke-static {v0}, Lcom/samsung/groupcast/application/Logger;->getTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/groupcast/application/Preferences;->TAG:Ljava/lang/String;

    .line 34
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/samsung/groupcast/application/Preferences;->mDisclaimerConfirmed:Ljava/util/ArrayList;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static clearPreferences()V
    .locals 1

    .prologue
    .line 37
    invoke-static {}, Lcom/samsung/groupcast/application/Preferences;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 38
    return-void
.end method

.method public static getCheckPasswordPreference()Z
    .locals 3

    .prologue
    .line 162
    invoke-static {}, Lcom/samsung/groupcast/application/Preferences;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "CHECK_PASSWORD"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static getDefaultDocumentPickerFolderPreference()Ljava/lang/String;
    .locals 4

    .prologue
    .line 117
    invoke-static {}, Lcom/samsung/groupcast/application/Preferences;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "DOCUMENT_PICKER_START_FOLDER"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/Documents"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getDisclaimerConfirmed(I)Z
    .locals 2
    .param p0, "type"    # I

    .prologue
    .line 95
    sget-object v0, Lcom/samsung/groupcast/application/Preferences;->mDisclaimerConfirmed:Ljava/util/ArrayList;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getDisclimerPreference()Z
    .locals 3

    .prologue
    .line 53
    invoke-static {}, Lcom/samsung/groupcast/application/Preferences;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "DISCLIMER_CHECK"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static getGameXmlVersion()Ljava/lang/String;
    .locals 3

    .prologue
    .line 122
    invoke-static {}, Lcom/samsung/groupcast/application/Preferences;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "GAME_XML_VERSION"

    const-string v2, "0"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getNearByFriendsAdvertisePreference()Z
    .locals 3

    .prologue
    .line 154
    invoke-static {}, Lcom/samsung/groupcast/application/Preferences;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "NEARBY_FRIEND_ADVERTISE"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static getNearByFriendsListenPreference()Z
    .locals 3

    .prologue
    .line 150
    invoke-static {}, Lcom/samsung/groupcast/application/Preferences;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "NEARBY_FRIEND_LISTEN"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static getNicknamePreference()Ljava/lang/String;
    .locals 3

    .prologue
    .line 45
    invoke-static {}, Lcom/samsung/groupcast/application/Preferences;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "NICK_NAME"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getRemoteFriendPreference()Z
    .locals 3

    .prologue
    .line 158
    invoke-static {}, Lcom/samsung/groupcast/application/Preferences;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "CHECKED_PASSWORD"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method private static getSharedPreferences()Landroid/content/SharedPreferences;
    .locals 3

    .prologue
    .line 179
    invoke-static {}, Lcom/samsung/groupcast/application/App;->getInstance()Lcom/samsung/groupcast/application/App;

    move-result-object v0

    const-string v1, "com.samsung.groupcast.application.preferences"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/samsung/groupcast/application/App;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    return-object v0
.end method

.method private static getSharedPreferencesEditor()Landroid/content/SharedPreferences$Editor;
    .locals 1

    .prologue
    .line 183
    invoke-static {}, Lcom/samsung/groupcast/application/Preferences;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    return-object v0
.end method

.method public static getSkipDisclaimerNotificationPreferenceDynamically(Ljava/lang/String;)Z
    .locals 8
    .param p0, "key"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 58
    const/4 v3, -0x2

    .line 60
    .local v3, "recordedDate":I
    :try_start_0
    invoke-static {}, Lcom/samsung/groupcast/application/Preferences;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v6

    const/4 v7, -0x2

    invoke-interface {v6, p0, v7}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    .line 65
    :goto_0
    packed-switch v3, :pswitch_data_0

    .line 72
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 73
    .local v0, "cal":Ljava/util/Calendar;
    invoke-virtual {v0, v4}, Ljava/util/Calendar;->get(I)I

    move-result v6

    mul-int/lit16 v6, v6, 0x164

    const/4 v7, 0x6

    invoke-virtual {v0, v7}, Ljava/util/Calendar;->get(I)I

    move-result v7

    add-int v1, v6, v7

    .line 76
    .local v1, "currentDate":I
    sub-int v6, v3, v1

    if-lez v6, :cond_0

    .end local v0    # "cal":Ljava/util/Calendar;
    .end local v1    # "currentDate":I
    :goto_1
    :pswitch_0
    return v4

    .line 61
    :catch_0
    move-exception v2

    .line 62
    .local v2, "e":Ljava/lang/ClassCastException;
    invoke-static {}, Lcom/samsung/groupcast/application/Preferences;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v6

    invoke-interface {v6}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    invoke-interface {v6, p0}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    goto :goto_0

    .end local v2    # "e":Ljava/lang/ClassCastException;
    :pswitch_1
    move v4, v5

    .line 69
    goto :goto_1

    .restart local v0    # "cal":Ljava/util/Calendar;
    .restart local v1    # "currentDate":I
    :cond_0
    move v4, v5

    .line 76
    goto :goto_1

    .line 65
    :pswitch_data_0
    .packed-switch -0x2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static setCheckPasswordPreference(Ljava/lang/Boolean;)V
    .locals 3
    .param p0, "vale"    # Ljava/lang/Boolean;

    .prologue
    .line 142
    invoke-static {}, Lcom/samsung/groupcast/application/Preferences;->getSharedPreferencesEditor()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "CHECK_PASSWORD"

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 143
    return-void
.end method

.method public static setDisclaimerConfirmed(I)V
    .locals 2
    .param p0, "type"    # I

    .prologue
    .line 99
    const/4 v0, -0x1

    if-ne p0, v0, :cond_1

    .line 100
    sget-object v0, Lcom/samsung/groupcast/application/Preferences;->mDisclaimerConfirmed:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 106
    :cond_0
    :goto_0
    return-void

    .line 102
    :cond_1
    sget-object v0, Lcom/samsung/groupcast/application/Preferences;->mDisclaimerConfirmed:Ljava/util/ArrayList;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 105
    sget-object v0, Lcom/samsung/groupcast/application/Preferences;->mDisclaimerConfirmed:Ljava/util/ArrayList;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public static setDisclimerPreference(Z)V
    .locals 2
    .param p0, "checked"    # Z

    .prologue
    .line 49
    invoke-static {}, Lcom/samsung/groupcast/application/Preferences;->getSharedPreferencesEditor()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "DISCLIMER_CHECK"

    invoke-interface {v0, v1, p0}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 51
    return-void
.end method

.method public static setGameXmlVersion(Ljava/lang/String;)V
    .locals 2
    .param p0, "version"    # Ljava/lang/String;

    .prologue
    .line 126
    invoke-static {}, Lcom/samsung/groupcast/application/Preferences;->getSharedPreferencesEditor()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "GAME_XML_VERSION"

    invoke-interface {v0, v1, p0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 127
    return-void
.end method

.method public static setNearByFriendsAdvertisePreference(Ljava/lang/Boolean;)V
    .locals 3
    .param p0, "vale"    # Ljava/lang/Boolean;

    .prologue
    .line 134
    invoke-static {}, Lcom/samsung/groupcast/application/Preferences;->getSharedPreferencesEditor()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "NEARBY_FRIEND_ADVERTISE"

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 135
    return-void
.end method

.method public static setNearByFriendsListenPreference(Ljava/lang/Boolean;)V
    .locals 3
    .param p0, "vale"    # Ljava/lang/Boolean;

    .prologue
    .line 131
    invoke-static {}, Lcom/samsung/groupcast/application/Preferences;->getSharedPreferencesEditor()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "NEARBY_FRIEND_LISTEN"

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 132
    return-void
.end method

.method public static setNicknamePreference(Ljava/lang/String;)V
    .locals 2
    .param p0, "nickName"    # Ljava/lang/String;

    .prologue
    .line 41
    invoke-static {}, Lcom/samsung/groupcast/application/Preferences;->getSharedPreferencesEditor()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "NICK_NAME"

    invoke-interface {v0, v1, p0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 43
    return-void
.end method

.method public static setRemoteFriendPreference(Ljava/lang/Boolean;)V
    .locals 3
    .param p0, "vale"    # Ljava/lang/Boolean;

    .prologue
    .line 138
    invoke-static {}, Lcom/samsung/groupcast/application/Preferences;->getSharedPreferencesEditor()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "CHECKED_PASSWORD"

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 139
    return-void
.end method

.method public static setSkipDisclaimerNotificationPreference(ILjava/lang/String;)V
    .locals 5
    .param p0, "value"    # I
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 82
    const/4 v3, -0x1

    if-ne p0, v3, :cond_0

    .line 83
    invoke-static {}, Lcom/samsung/groupcast/application/Preferences;->getSharedPreferencesEditor()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    invoke-interface {v3, p1, p0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 92
    :goto_0
    return-void

    .line 87
    :cond_0
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 88
    .local v0, "cal":Ljava/util/Calendar;
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Ljava/util/Calendar;->get(I)I

    move-result v3

    mul-int/lit16 v3, v3, 0x164

    const/4 v4, 0x6

    invoke-virtual {v0, v4}, Ljava/util/Calendar;->get(I)I

    move-result v4

    add-int v1, v3, v4

    .line 89
    .local v1, "currentDate":I
    add-int v2, v1, p0

    .line 91
    .local v2, "targetDate":I
    invoke-static {}, Lcom/samsung/groupcast/application/Preferences;->getSharedPreferencesEditor()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    invoke-interface {v3, p1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0
.end method

.method public static setStartingFolderDocumentPickerPreference(Ljava/lang/String;)V
    .locals 3
    .param p0, "documentPickerDefaultLocation"    # Ljava/lang/String;

    .prologue
    .line 110
    sget-object v0, Lcom/samsung/groupcast/application/Preferences;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[setStartingFolderDocumentPickerPreference] value: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/groupcast/application/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    invoke-static {}, Lcom/samsung/groupcast/application/Preferences;->getSharedPreferencesEditor()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "DOCUMENT_PICKER_START_FOLDER"

    invoke-interface {v0, v1, p0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 114
    return-void
.end method
