.class Lcom/samsung/groupcast/application/start/StartActivity$WHandler;
.super Lcom/samsung/groupcast/misc/utility/WeakReferenceHandler;
.source "StartActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/groupcast/application/start/StartActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "WHandler"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/samsung/groupcast/misc/utility/WeakReferenceHandler",
        "<",
        "Lcom/samsung/groupcast/application/start/StartActivity;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/samsung/groupcast/application/start/StartActivity;)V
    .locals 0
    .param p1, "activity"    # Lcom/samsung/groupcast/application/start/StartActivity;

    .prologue
    .line 1219
    invoke-direct {p0, p1}, Lcom/samsung/groupcast/misc/utility/WeakReferenceHandler;-><init>(Ljava/lang/Object;)V

    .line 1220
    return-void
.end method


# virtual methods
.method public handleMessage(Lcom/samsung/groupcast/application/start/StartActivity;Landroid/os/Message;)V
    .locals 1
    .param p1, "activity"    # Lcom/samsung/groupcast/application/start/StartActivity;
    .param p2, "msg"    # Landroid/os/Message;

    .prologue
    .line 1225
    iget v0, p2, Landroid/os/Message;->what:I

    if-nez v0, :cond_0

    .line 1230
    :goto_0
    return-void

    .line 1229
    :cond_0
    iget-object v0, p2, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/util/List;

    invoke-virtual {p1, v0}, Lcom/samsung/groupcast/application/start/StartActivity;->handleScanResults(Ljava/util/List;)V

    goto :goto_0
.end method

.method public bridge synthetic handleMessage(Ljava/lang/Object;Landroid/os/Message;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Landroid/os/Message;

    .prologue
    .line 1217
    check-cast p1, Lcom/samsung/groupcast/application/start/StartActivity;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/samsung/groupcast/application/start/StartActivity$WHandler;->handleMessage(Lcom/samsung/groupcast/application/start/StartActivity;Landroid/os/Message;)V

    return-void
.end method
