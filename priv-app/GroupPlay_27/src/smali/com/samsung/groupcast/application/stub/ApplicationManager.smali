.class public Lcom/samsung/groupcast/application/stub/ApplicationManager;
.super Ljava/lang/Object;
.source "ApplicationManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/groupcast/application/stub/ApplicationManager$PackageDeleteObserver;,
        Lcom/samsung/groupcast/application/stub/ApplicationManager$PackageInstallObserver;
    }
.end annotation


# static fields
.field public static final INSTALL_FAILED_ALREADY_EXISTS:I = -0x1

.field public static final INSTALL_FAILED_CONFLICTING_PROVIDER:I = -0xd

.field public static final INSTALL_FAILED_CONTAINER_ERROR:I = -0x12

.field public static final INSTALL_FAILED_CPU_ABI_INCOMPATIBLE:I = -0x10

.field public static final INSTALL_FAILED_DEXOPT:I = -0xb

.field public static final INSTALL_FAILED_DUPLICATE_PACKAGE:I = -0x5

.field public static final INSTALL_FAILED_INSUFFICIENT_STORAGE:I = -0x4

.field public static final INSTALL_FAILED_INTERNAL_ERROR:I = -0x6e

.field public static final INSTALL_FAILED_INVALID_APK:I = -0x2

.field public static final INSTALL_FAILED_INVALID_INSTALL_LOCATION:I = -0x13

.field public static final INSTALL_FAILED_INVALID_URI:I = -0x3

.field public static final INSTALL_FAILED_MEDIA_UNAVAILABLE:I = -0x14

.field public static final INSTALL_FAILED_MISSING_FEATURE:I = -0x11

.field public static final INSTALL_FAILED_MISSING_SHARED_LIBRARY:I = -0x9

.field public static final INSTALL_FAILED_NEWER_SDK:I = -0xe

.field public static final INSTALL_FAILED_NO_SHARED_USER:I = -0x6

.field public static final INSTALL_FAILED_OLDER_SDK:I = -0xc

.field public static final INSTALL_FAILED_REPLACE_COULDNT_DELETE:I = -0xa

.field public static final INSTALL_FAILED_SHARED_USER_INCOMPATIBLE:I = -0x8

.field public static final INSTALL_FAILED_TEST_ONLY:I = -0xf

.field public static final INSTALL_FAILED_UPDATE_INCOMPATIBLE:I = -0x7

.field public static final INSTALL_PARSE_FAILED_BAD_MANIFEST:I = -0x65

.field public static final INSTALL_PARSE_FAILED_BAD_PACKAGE_NAME:I = -0x6a

.field public static final INSTALL_PARSE_FAILED_BAD_SHARED_USER_ID:I = -0x6b

.field public static final INSTALL_PARSE_FAILED_CERTIFICATE_ENCODING:I = -0x69

.field public static final INSTALL_PARSE_FAILED_INCONSISTENT_CERTIFICATES:I = -0x68

.field public static final INSTALL_PARSE_FAILED_MANIFEST_EMPTY:I = -0x6d

.field public static final INSTALL_PARSE_FAILED_MANIFEST_MALFORMED:I = -0x6c

.field public static final INSTALL_PARSE_FAILED_NOT_APK:I = -0x64

.field public static final INSTALL_PARSE_FAILED_NO_CERTIFICATES:I = -0x67

.field public static final INSTALL_PARSE_FAILED_UNEXPECTED_EXCEPTION:I = -0x66

.field public static final INSTALL_SUCCEEDED:I = 0x1


# instance fields
.field public final INSTALL_REPLACE_EXISTING:I

.field private deleteHandler:Landroid/os/Handler;

.field private deleteObserver:Lcom/samsung/groupcast/application/stub/ApplicationManager$PackageDeleteObserver;

.field private handler:Landroid/os/Handler;

.field private method:Ljava/lang/reflect/Method;

.field private methodForExistingPackage:Ljava/lang/reflect/Method;

.field private observer:Lcom/samsung/groupcast/application/stub/ApplicationManager$PackageInstallObserver;

.field private onInstalledPackaged:Lcom/samsung/groupcast/application/stub/OnInstalledPackaged;

.field private onPackageDeleted:Lcom/samsung/groupcast/application/stub/OnPackageDeleted;

.field pkgname:Ljava/lang/String;

.field private pm:Landroid/content/pm/PackageManager;

.field returncode:I

.field private uninstallmethod:Ljava/lang/reflect/Method;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/SecurityException;,
            Ljava/lang/NoSuchMethodException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 361
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput v6, p0, Lcom/samsung/groupcast/application/stub/ApplicationManager;->INSTALL_REPLACE_EXISTING:I

    .line 340
    new-instance v3, Lcom/samsung/groupcast/application/stub/ApplicationManager$1;

    invoke-direct {v3, p0}, Lcom/samsung/groupcast/application/stub/ApplicationManager$1;-><init>(Lcom/samsung/groupcast/application/stub/ApplicationManager;)V

    iput-object v3, p0, Lcom/samsung/groupcast/application/stub/ApplicationManager;->handler:Landroid/os/Handler;

    .line 347
    new-instance v3, Lcom/samsung/groupcast/application/stub/ApplicationManager$2;

    invoke-direct {v3, p0}, Lcom/samsung/groupcast/application/stub/ApplicationManager$2;-><init>(Lcom/samsung/groupcast/application/stub/ApplicationManager;)V

    iput-object v3, p0, Lcom/samsung/groupcast/application/stub/ApplicationManager;->deleteHandler:Landroid/os/Handler;

    .line 362
    new-instance v3, Lcom/samsung/groupcast/application/stub/ApplicationManager$PackageInstallObserver;

    invoke-direct {v3, p0}, Lcom/samsung/groupcast/application/stub/ApplicationManager$PackageInstallObserver;-><init>(Lcom/samsung/groupcast/application/stub/ApplicationManager;)V

    iput-object v3, p0, Lcom/samsung/groupcast/application/stub/ApplicationManager;->observer:Lcom/samsung/groupcast/application/stub/ApplicationManager$PackageInstallObserver;

    .line 363
    new-instance v3, Lcom/samsung/groupcast/application/stub/ApplicationManager$PackageDeleteObserver;

    invoke-direct {v3, p0}, Lcom/samsung/groupcast/application/stub/ApplicationManager$PackageDeleteObserver;-><init>(Lcom/samsung/groupcast/application/stub/ApplicationManager;)V

    iput-object v3, p0, Lcom/samsung/groupcast/application/stub/ApplicationManager;->deleteObserver:Lcom/samsung/groupcast/application/stub/ApplicationManager$PackageDeleteObserver;

    .line 364
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/groupcast/application/stub/ApplicationManager;->pm:Landroid/content/pm/PackageManager;

    .line 366
    const/4 v3, 0x4

    new-array v1, v3, [Ljava/lang/Class;

    const-class v3, Landroid/net/Uri;

    aput-object v3, v1, v4

    const-class v3, Landroid/content/pm/IPackageInstallObserver;

    aput-object v3, v1, v5

    sget-object v3, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v3, v1, v6

    const-class v3, Ljava/lang/String;

    aput-object v3, v1, v7

    .line 367
    .local v1, "types":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;"
    iget-object v3, p0, Lcom/samsung/groupcast/application/stub/ApplicationManager;->pm:Landroid/content/pm/PackageManager;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    const-string v4, "installPackage"

    invoke-virtual {v3, v4, v1}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/groupcast/application/stub/ApplicationManager;->method:Ljava/lang/reflect/Method;

    .line 370
    :try_start_0
    iget-object v3, p0, Lcom/samsung/groupcast/application/stub/ApplicationManager;->pm:Landroid/content/pm/PackageManager;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    const-string v4, "installExistingPackage"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Class;

    const/4 v6, 0x0

    const-class v7, Ljava/lang/String;

    aput-object v7, v5, v6

    invoke-virtual {v3, v4, v5}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/groupcast/application/stub/ApplicationManager;->methodForExistingPackage:Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0

    .line 376
    :goto_0
    const/4 v3, 0x3

    :try_start_1
    new-array v2, v3, [Ljava/lang/Class;

    const/4 v3, 0x0

    const-class v4, Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-class v4, Landroid/content/pm/IPackageDeleteObserver;

    aput-object v4, v2, v3

    const/4 v3, 0x2

    sget-object v4, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v4, v2, v3

    .line 377
    .local v2, "uninstalltypes":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;"
    iget-object v3, p0, Lcom/samsung/groupcast/application/stub/ApplicationManager;->pm:Landroid/content/pm/PackageManager;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    const-string v4, "deletePackage"

    invoke-virtual {v3, v4, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/groupcast/application/stub/ApplicationManager;->uninstallmethod:Ljava/lang/reflect/Method;
    :try_end_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_1 .. :try_end_1} :catch_1

    .line 381
    .end local v2    # "uninstalltypes":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;"
    :goto_1
    return-void

    .line 371
    :catch_0
    move-exception v0

    .line 372
    .local v0, "e":Ljava/lang/NoSuchMethodException;
    iput-object v8, p0, Lcom/samsung/groupcast/application/stub/ApplicationManager;->methodForExistingPackage:Ljava/lang/reflect/Method;

    goto :goto_0

    .line 378
    .end local v0    # "e":Ljava/lang/NoSuchMethodException;
    :catch_1
    move-exception v0

    .line 379
    .restart local v0    # "e":Ljava/lang/NoSuchMethodException;
    iput-object v8, p0, Lcom/samsung/groupcast/application/stub/ApplicationManager;->uninstallmethod:Ljava/lang/reflect/Method;

    goto :goto_1
.end method

.method static synthetic access$000(Lcom/samsung/groupcast/application/stub/ApplicationManager;)Lcom/samsung/groupcast/application/stub/OnInstalledPackaged;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/application/stub/ApplicationManager;

    .prologue
    .line 16
    iget-object v0, p0, Lcom/samsung/groupcast/application/stub/ApplicationManager;->onInstalledPackaged:Lcom/samsung/groupcast/application/stub/OnInstalledPackaged;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/groupcast/application/stub/ApplicationManager;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/application/stub/ApplicationManager;

    .prologue
    .line 16
    iget-object v0, p0, Lcom/samsung/groupcast/application/stub/ApplicationManager;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/groupcast/application/stub/ApplicationManager;)Lcom/samsung/groupcast/application/stub/OnPackageDeleted;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/application/stub/ApplicationManager;

    .prologue
    .line 16
    iget-object v0, p0, Lcom/samsung/groupcast/application/stub/ApplicationManager;->onPackageDeleted:Lcom/samsung/groupcast/application/stub/OnPackageDeleted;

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/groupcast/application/stub/ApplicationManager;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/application/stub/ApplicationManager;

    .prologue
    .line 16
    iget-object v0, p0, Lcom/samsung/groupcast/application/stub/ApplicationManager;->deleteHandler:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method public installExistingPackage(Ljava/lang/String;)V
    .locals 10
    .param p1, "packageName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v9, 0x1

    .line 413
    iget-object v5, p0, Lcom/samsung/groupcast/application/stub/ApplicationManager;->methodForExistingPackage:Ljava/lang/reflect/Method;

    if-nez v5, :cond_0

    .line 414
    new-instance v5, Ljava/lang/Exception;

    const-string v6, "Can not find installExistingPackage method."

    invoke-direct {v5, v6}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v5

    .line 416
    :cond_0
    const/4 v4, 0x1

    .line 417
    .local v4, "result":Z
    const/4 v1, 0x0

    .line 420
    .local v1, "error":Ljava/lang/Exception;
    :try_start_0
    iget-object v5, p0, Lcom/samsung/groupcast/application/stub/ApplicationManager;->methodForExistingPackage:Ljava/lang/reflect/Method;

    iget-object v6, p0, Lcom/samsung/groupcast/application/stub/ApplicationManager;->pm:Landroid/content/pm/PackageManager;

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object p1, v7, v8

    invoke-virtual {v5, v6, v7}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    .line 421
    .local v3, "output":Ljava/lang/Integer;
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v5

    iput v5, p0, Lcom/samsung/groupcast/application/stub/ApplicationManager;->returncode:I

    .line 422
    iget v5, p0, Lcom/samsung/groupcast/application/stub/ApplicationManager;->returncode:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-ne v5, v9, :cond_3

    .line 423
    const/4 v4, 0x1

    .line 434
    .end local v3    # "output":Ljava/lang/Integer;
    :goto_0
    iput-object p1, p0, Lcom/samsung/groupcast/application/stub/ApplicationManager;->pkgname:Ljava/lang/String;

    .line 436
    iget-object v5, p0, Lcom/samsung/groupcast/application/stub/ApplicationManager;->onInstalledPackaged:Lcom/samsung/groupcast/application/stub/OnInstalledPackaged;

    if-eqz v5, :cond_2

    .line 437
    iget v5, p0, Lcom/samsung/groupcast/application/stub/ApplicationManager;->returncode:I

    if-eq v5, v9, :cond_1

    .line 440
    :cond_1
    iget-object v5, p0, Lcom/samsung/groupcast/application/stub/ApplicationManager;->handler:Landroid/os/Handler;

    invoke-virtual {v5}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v2

    .line 441
    .local v2, "msg":Landroid/os/Message;
    iget-object v5, p0, Lcom/samsung/groupcast/application/stub/ApplicationManager;->handler:Landroid/os/Handler;

    invoke-virtual {v5, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 444
    .end local v2    # "msg":Landroid/os/Message;
    :cond_2
    if-nez v4, :cond_4

    if-eqz v1, :cond_4

    .line 445
    throw v1

    .line 425
    .restart local v3    # "output":Ljava/lang/Integer;
    :cond_3
    const/4 v4, 0x0

    .line 426
    :try_start_1
    new-instance v1, Ljava/lang/Exception;

    .end local v1    # "error":Ljava/lang/Exception;
    const-string v5, "Failed to install existing package."

    invoke-direct {v1, v5}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .restart local v1    # "error":Ljava/lang/Exception;
    goto :goto_0

    .line 428
    .end local v1    # "error":Ljava/lang/Exception;
    .end local v3    # "output":Ljava/lang/Integer;
    :catch_0
    move-exception v0

    .line 429
    .local v0, "e":Ljava/lang/Exception;
    const/16 v5, -0x6e

    iput v5, p0, Lcom/samsung/groupcast/application/stub/ApplicationManager;->returncode:I

    .line 430
    const/4 v4, 0x0

    .line 431
    move-object v1, v0

    .restart local v1    # "error":Ljava/lang/Exception;
    goto :goto_0

    .line 447
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_4
    return-void
.end method

.method public installPackage(Landroid/net/Uri;)V
    .locals 6
    .param p1, "apkFile"    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/IllegalAccessException;,
            Ljava/lang/reflect/InvocationTargetException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x2

    .line 409
    iget-object v0, p0, Lcom/samsung/groupcast/application/stub/ApplicationManager;->method:Ljava/lang/reflect/Method;

    iget-object v1, p0, Lcom/samsung/groupcast/application/stub/ApplicationManager;->pm:Landroid/content/pm/PackageManager;

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/samsung/groupcast/application/stub/ApplicationManager;->observer:Lcom/samsung/groupcast/application/stub/ApplicationManager$PackageInstallObserver;

    aput-object v4, v2, v3

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    const/4 v3, 0x3

    const/4 v4, 0x0

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 410
    return-void
.end method

.method public installPackage(Ljava/io/File;)V
    .locals 2
    .param p1, "apkFile"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/IllegalAccessException;,
            Ljava/lang/reflect/InvocationTargetException;
        }
    .end annotation

    .prologue
    .line 400
    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    .line 401
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v1

    .line 404
    :cond_0
    invoke-static {p1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    .line 405
    .local v0, "packageURI":Landroid/net/Uri;
    invoke-virtual {p0, v0}, Lcom/samsung/groupcast/application/stub/ApplicationManager;->installPackage(Landroid/net/Uri;)V

    .line 406
    return-void
.end method

.method public installPackage(Ljava/lang/String;)V
    .locals 1
    .param p1, "apkFile"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/IllegalAccessException;,
            Ljava/lang/reflect/InvocationTargetException;
        }
    .end annotation

    .prologue
    .line 396
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/samsung/groupcast/application/stub/ApplicationManager;->installPackage(Ljava/io/File;)V

    .line 397
    return-void
.end method

.method public setOnDeletePackage(Lcom/samsung/groupcast/application/stub/OnPackageDeleted;)V
    .locals 0
    .param p1, "onPackageDeleted"    # Lcom/samsung/groupcast/application/stub/OnPackageDeleted;

    .prologue
    .line 388
    iput-object p1, p0, Lcom/samsung/groupcast/application/stub/ApplicationManager;->onPackageDeleted:Lcom/samsung/groupcast/application/stub/OnPackageDeleted;

    .line 389
    return-void
.end method

.method public setOnInstalledPackaged(Lcom/samsung/groupcast/application/stub/OnInstalledPackaged;)V
    .locals 0
    .param p1, "onInstalledPackaged"    # Lcom/samsung/groupcast/application/stub/OnInstalledPackaged;

    .prologue
    .line 384
    iput-object p1, p0, Lcom/samsung/groupcast/application/stub/ApplicationManager;->onInstalledPackaged:Lcom/samsung/groupcast/application/stub/OnInstalledPackaged;

    .line 385
    return-void
.end method

.method public uninstallPackage(Ljava/lang/String;)V
    .locals 6
    .param p1, "packagename"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/IllegalAccessException;,
            Ljava/lang/reflect/InvocationTargetException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 392
    iget-object v0, p0, Lcom/samsung/groupcast/application/stub/ApplicationManager;->uninstallmethod:Ljava/lang/reflect/Method;

    iget-object v1, p0, Lcom/samsung/groupcast/application/stub/ApplicationManager;->pm:Landroid/content/pm/PackageManager;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v5

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/samsung/groupcast/application/stub/ApplicationManager;->deleteObserver:Lcom/samsung/groupcast/application/stub/ApplicationManager$PackageDeleteObserver;

    aput-object v4, v2, v3

    const/4 v3, 0x2

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 393
    return-void
.end method
