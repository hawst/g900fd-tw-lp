.class public Lcom/samsung/groupcast/application/GPActivity;
.super Landroid/app/Activity;
.source "GPActivity.java"


# static fields
.field private static final DISABLE_NOTIFICATION_BAR_ID:Ljava/lang/String; = "DISABLE_NOTIFICATION_BAR"


# instance fields
.field protected mDisableNotificationBar:Z

.field private mOnSaveInstanceStateCalled:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 6
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 11
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/groupcast/application/GPActivity;->mDisableNotificationBar:Z

    return-void
.end method


# virtual methods
.method public isOnSaveInstanceStateCalled()Z
    .locals 1

    .prologue
    .line 42
    iget-boolean v0, p0, Lcom/samsung/groupcast/application/GPActivity;->mOnSaveInstanceStateCalled:Z

    return v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 16
    if-eqz p1, :cond_0

    .line 17
    const-string v0, "DISABLE_NOTIFICATION_BAR"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/samsung/groupcast/application/GPActivity;->mDisableNotificationBar:Z

    .line 18
    :cond_0
    iget-boolean v0, p0, Lcom/samsung/groupcast/application/GPActivity;->mDisableNotificationBar:Z

    if-eqz v0, :cond_1

    .line 19
    invoke-static {p0}, Lcom/samsung/groupcast/application/App;->enableNotificationBarWhileFullScreen(Landroid/app/Activity;)V

    .line 20
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/groupcast/application/GPActivity;->mOnSaveInstanceStateCalled:Z

    .line 21
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 22
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 28
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/groupcast/application/GPActivity;->mOnSaveInstanceStateCalled:Z

    .line 29
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 30
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 35
    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 36
    const-string v0, "DISABLE_NOTIFICATION_BAR"

    iget-boolean v1, p0, Lcom/samsung/groupcast/application/GPActivity;->mDisableNotificationBar:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 37
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/groupcast/application/GPActivity;->mOnSaveInstanceStateCalled:Z

    .line 38
    return-void
.end method
