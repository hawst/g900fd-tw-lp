.class Lcom/samsung/groupcast/application/main/MainActivity$2;
.super Landroid/content/BroadcastReceiver;
.source "MainActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/groupcast/application/main/MainActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/groupcast/application/main/MainActivity;


# direct methods
.method constructor <init>(Lcom/samsung/groupcast/application/main/MainActivity;)V
    .locals 0

    .prologue
    .line 270
    iput-object p1, p0, Lcom/samsung/groupcast/application/main/MainActivity$2;->this$0:Lcom/samsung/groupcast/application/main/MainActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/high16 v6, 0x4000000

    .line 273
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 274
    .local v0, "action":Ljava/lang/String;
    const-string v4, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 276
    iget-object v4, p0, Lcom/samsung/groupcast/application/main/MainActivity$2;->this$0:Lcom/samsung/groupcast/application/main/MainActivity;

    # getter for: Lcom/samsung/groupcast/application/main/MainActivity;->mWifiUtils:Lcom/samsung/groupcast/net/wifi/WifiUtils;
    invoke-static {v4}, Lcom/samsung/groupcast/application/main/MainActivity;->access$000(Lcom/samsung/groupcast/application/main/MainActivity;)Lcom/samsung/groupcast/net/wifi/WifiUtils;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getWifiStatus()Ljava/lang/String;

    move-result-object v4

    const-string v5, "DISCONNECTED"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/samsung/groupcast/application/main/MainActivity$2;->this$0:Lcom/samsung/groupcast/application/main/MainActivity;

    invoke-virtual {v4}, Lcom/samsung/groupcast/application/main/MainActivity;->showDisconnectAlert()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 278
    iget-object v4, p0, Lcom/samsung/groupcast/application/main/MainActivity$2;->this$0:Lcom/samsung/groupcast/application/main/MainActivity;

    const/4 v5, 0x0

    # setter for: Lcom/samsung/groupcast/application/main/MainActivity;->mIsStartFromGPSDK:Z
    invoke-static {v4, v5}, Lcom/samsung/groupcast/application/main/MainActivity;->access$102(Lcom/samsung/groupcast/application/main/MainActivity;Z)Z

    .line 280
    iget-object v4, p0, Lcom/samsung/groupcast/application/main/MainActivity$2;->this$0:Lcom/samsung/groupcast/application/main/MainActivity;

    invoke-virtual {v4}, Lcom/samsung/groupcast/application/main/MainActivity;->stopGroupPlayService()V

    .line 282
    const-string v4, "WifiUtils.DISCONNECTED) && showDisconnectAlert()"

    invoke-static {v4}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 284
    new-instance v3, Landroid/content/Intent;

    invoke-static {}, Lcom/samsung/groupcast/application/App;->getInstance()Lcom/samsung/groupcast/application/App;

    move-result-object v4

    const-class v5, Lcom/samsung/groupcast/application/main/MainActivity;

    invoke-direct {v3, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 285
    .local v3, "noWifiIntent":Landroid/content/Intent;
    invoke-virtual {v3, v6}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 286
    const-string v4, "ObjectStoreHash"

    invoke-static {}, Lcom/samsung/groupcast/application/ObjectStore;->getHash()I

    move-result v5

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 288
    iget-object v4, p0, Lcom/samsung/groupcast/application/main/MainActivity$2;->this$0:Lcom/samsung/groupcast/application/main/MainActivity;

    invoke-virtual {v4, v3}, Lcom/samsung/groupcast/application/main/MainActivity;->startActivity(Landroid/content/Intent;)V

    .line 325
    .end local v3    # "noWifiIntent":Landroid/content/Intent;
    :cond_0
    :goto_0
    return-void

    .line 290
    :cond_1
    const-string v4, "android.net.wifi.WIFI_AP_STATE_CHANGED"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 291
    const-string v4, "wifi_state"

    const/16 v5, 0xe

    invoke-virtual {p2, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 297
    .local v1, "apState":I
    iget-object v4, p0, Lcom/samsung/groupcast/application/main/MainActivity$2;->this$0:Lcom/samsung/groupcast/application/main/MainActivity;

    # getter for: Lcom/samsung/groupcast/application/main/MainActivity;->mGroupCastHotspotTurnedOn:Z
    invoke-static {v4}, Lcom/samsung/groupcast/application/main/MainActivity;->access$200(Lcom/samsung/groupcast/application/main/MainActivity;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/16 v4, 0xb

    if-ne v1, v4, :cond_0

    iget-object v4, p0, Lcom/samsung/groupcast/application/main/MainActivity$2;->this$0:Lcom/samsung/groupcast/application/main/MainActivity;

    invoke-virtual {v4}, Lcom/samsung/groupcast/application/main/MainActivity;->showDisconnectAlert()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 300
    const-string v4, "mGroupCastHotspotTurnedOn && WifiManager.WIFI_AP_STATE_DISABLED && showDisconnectAlert()"

    invoke-static {v4}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 302
    new-instance v3, Landroid/content/Intent;

    invoke-static {}, Lcom/samsung/groupcast/application/App;->getInstance()Lcom/samsung/groupcast/application/App;

    move-result-object v4

    const-class v5, Lcom/samsung/groupcast/application/main/MainActivity;

    invoke-direct {v3, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 303
    .restart local v3    # "noWifiIntent":Landroid/content/Intent;
    invoke-virtual {v3, v6}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 304
    const-string v4, "ObjectStoreHash"

    invoke-static {}, Lcom/samsung/groupcast/application/ObjectStore;->getHash()I

    move-result v5

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 306
    iget-object v4, p0, Lcom/samsung/groupcast/application/main/MainActivity$2;->this$0:Lcom/samsung/groupcast/application/main/MainActivity;

    invoke-virtual {v4, v3}, Lcom/samsung/groupcast/application/main/MainActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 308
    .end local v1    # "apState":I
    .end local v3    # "noWifiIntent":Landroid/content/Intent;
    :cond_2
    const-string v4, "android.net.wifi.p2p.CONNECTION_STATE_CHANGE"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 309
    const-string v4, "networkInfo"

    invoke-virtual {p2, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Landroid/net/NetworkInfo;

    .line 311
    .local v2, "networkInfo":Landroid/net/NetworkInfo;
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/samsung/groupcast/application/main/MainActivity$2;->this$0:Lcom/samsung/groupcast/application/main/MainActivity;

    invoke-virtual {v4}, Lcom/samsung/groupcast/application/main/MainActivity;->showDisconnectAlert()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 313
    iget-object v4, p0, Lcom/samsung/groupcast/application/main/MainActivity$2;->this$0:Lcom/samsung/groupcast/application/main/MainActivity;

    invoke-virtual {v4}, Lcom/samsung/groupcast/application/main/MainActivity;->stopGroupPlayService()V

    .line 314
    invoke-static {}, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->ClearLastSession()V

    .line 316
    const-string v4, "WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION) && showDisconnectAlert()"

    invoke-static {v4}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 318
    new-instance v3, Landroid/content/Intent;

    invoke-static {}, Lcom/samsung/groupcast/application/App;->getInstance()Lcom/samsung/groupcast/application/App;

    move-result-object v4

    const-class v5, Lcom/samsung/groupcast/application/main/MainActivity;

    invoke-direct {v3, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 319
    .restart local v3    # "noWifiIntent":Landroid/content/Intent;
    invoke-virtual {v3, v6}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 320
    const-string v4, "ObjectStoreHash"

    invoke-static {}, Lcom/samsung/groupcast/application/ObjectStore;->getHash()I

    move-result v5

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 322
    iget-object v4, p0, Lcom/samsung/groupcast/application/main/MainActivity$2;->this$0:Lcom/samsung/groupcast/application/main/MainActivity;

    invoke-virtual {v4, v3}, Lcom/samsung/groupcast/application/main/MainActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0
.end method
