.class Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment$1;
.super Ljava/lang/Object;
.source "ProgressDialogFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;


# direct methods
.method constructor <init>(Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;)V
    .locals 0

    .prologue
    .line 49
    iput-object p1, p0, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment$1;->this$0:Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 52
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Timed out with duration :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment$1;->this$0:Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;

    # getter for: Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;->mTimeoutDuration:I
    invoke-static {v1}, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;->access$000(Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 54
    iget-object v0, p0, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment$1;->this$0:Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;

    invoke-virtual {v0}, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 55
    iget-object v0, p0, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment$1;->this$0:Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;

    invoke-virtual {v0}, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f080018

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 59
    :cond_0
    iget-object v0, p0, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment$1;->this$0:Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;

    invoke-virtual {v0}, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;->dismissAllowingStateLoss()V

    .line 60
    return-void
.end method
