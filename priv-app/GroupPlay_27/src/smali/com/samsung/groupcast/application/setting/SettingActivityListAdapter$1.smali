.class Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$1;
.super Ljava/lang/Object;
.source "SettingActivityListAdapter.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter;->attachListView(Landroid/widget/ListView;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter;


# direct methods
.method constructor <init>(Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter;)V
    .locals 0

    .prologue
    .line 141
    iput-object p1, p0, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$1;->this$0:Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 144
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v1, p0, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$1;->this$0:Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter;

    # getter for: Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter;->mDelegate:Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$Delegate;
    invoke-static {v1}, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter;->access$000(Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter;)Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$Delegate;

    move-result-object v1

    if-nez v1, :cond_1

    .line 159
    :cond_0
    :goto_0
    return-void

    .line 148
    :cond_1
    iget-object v1, p0, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$1;->this$0:Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter;

    # invokes: Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter;->isInvotationItem(I)Z
    invoke-static {v1, p3}, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter;->access$100(Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter;I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 149
    iget-object v1, p0, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$1;->this$0:Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter;

    invoke-virtual {v1, p3}, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$InvitationItem;

    .line 150
    .local v0, "item":Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$InvitationItem;
    iget-object v1, p0, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$1;->this$0:Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter;

    # getter for: Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter;->mDelegate:Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$Delegate;
    invoke-static {v1}, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter;->access$000(Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter;)Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$Delegate;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$Delegate;->onInvotationItemSelected(Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$InvitationItem;)V

    goto :goto_0

    .line 154
    .end local v0    # "item":Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$InvitationItem;
    :cond_2
    iget-object v1, p0, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$1;->this$0:Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter;

    # invokes: Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter;->isAboutItem(I)Z
    invoke-static {v1, p3}, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter;->access$200(Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 155
    iget-object v1, p0, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$1;->this$0:Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter;

    invoke-virtual {v1, p3}, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$AboutItem;

    .line 156
    .local v0, "item":Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$AboutItem;
    iget-object v1, p0, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$1;->this$0:Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter;

    # getter for: Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter;->mDelegate:Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$Delegate;
    invoke-static {v1}, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter;->access$000(Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter;)Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$Delegate;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$Delegate;->onAboutItemSelected(Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$AboutItem;)V

    goto :goto_0
.end method
