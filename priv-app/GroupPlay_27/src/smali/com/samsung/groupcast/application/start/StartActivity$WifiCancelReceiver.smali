.class Lcom/samsung/groupcast/application/start/StartActivity$WifiCancelReceiver;
.super Landroid/content/BroadcastReceiver;
.source "StartActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/groupcast/application/start/StartActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "WifiCancelReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/groupcast/application/start/StartActivity;


# direct methods
.method private constructor <init>(Lcom/samsung/groupcast/application/start/StartActivity;)V
    .locals 0

    .prologue
    .line 2676
    iput-object p1, p0, Lcom/samsung/groupcast/application/start/StartActivity$WifiCancelReceiver;->this$0:Lcom/samsung/groupcast/application/start/StartActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/groupcast/application/start/StartActivity;Lcom/samsung/groupcast/application/start/StartActivity$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/groupcast/application/start/StartActivity;
    .param p2, "x1"    # Lcom/samsung/groupcast/application/start/StartActivity$1;

    .prologue
    .line 2676
    invoke-direct {p0, p1}, Lcom/samsung/groupcast/application/start/StartActivity$WifiCancelReceiver;-><init>(Lcom/samsung/groupcast/application/start/StartActivity;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 2680
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 2681
    .local v0, "action":Ljava/lang/String;
    const-string v1, "android.net.wifi.WIFI_DIALOG_CANCEL_ACTION"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2682
    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiAPUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->isRvfHotspotEnabled()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {}, Lcom/samsung/groupcast/application/Environment;->isRunningOnChinaProduct()Z

    move-result v1

    if-nez v1, :cond_1

    .line 2693
    :cond_0
    :goto_0
    return-void

    .line 2689
    :cond_1
    iget-object v1, p0, Lcom/samsung/groupcast/application/start/StartActivity$WifiCancelReceiver;->this$0:Lcom/samsung/groupcast/application/start/StartActivity;

    # invokes: Lcom/samsung/groupcast/application/start/StartActivity;->stopSearch()V
    invoke-static {v1}, Lcom/samsung/groupcast/application/start/StartActivity;->access$400(Lcom/samsung/groupcast/application/start/StartActivity;)V

    .line 2690
    iget-object v1, p0, Lcom/samsung/groupcast/application/start/StartActivity$WifiCancelReceiver;->this$0:Lcom/samsung/groupcast/application/start/StartActivity;

    # getter for: Lcom/samsung/groupcast/application/start/StartActivity;->mWifiUtils:Lcom/samsung/groupcast/net/wifi/WifiUtils;
    invoke-static {v1}, Lcom/samsung/groupcast/application/start/StartActivity;->access$1100(Lcom/samsung/groupcast/application/start/StartActivity;)Lcom/samsung/groupcast/net/wifi/WifiUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getWifiManager()Landroid/net/wifi/WifiManager;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/net/wifi/WifiManager;->setWifiEnabled(Z)Z

    .line 2691
    iget-object v1, p0, Lcom/samsung/groupcast/application/start/StartActivity$WifiCancelReceiver;->this$0:Lcom/samsung/groupcast/application/start/StartActivity;

    invoke-virtual {v1}, Lcom/samsung/groupcast/application/start/StartActivity;->finish()V

    goto :goto_0
.end method
