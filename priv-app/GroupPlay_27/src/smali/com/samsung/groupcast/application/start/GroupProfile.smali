.class public Lcom/samsung/groupcast/application/start/GroupProfile;
.super Ljava/lang/Object;
.source "GroupProfile.java"


# static fields
.field public static final AP_NAME_CHOOSEN:Ljava/lang/String; = "AP_NAME"

.field public static final GC_HOTSPOT_TURNED_ON:Ljava/lang/String; = "GC_HOTSPOT_TURNED_ON"

.field public static final GP_RAW_LAND_VIDEO:Ljava/lang/String; = "/raw/group_play_land"

.field public static final GP_RAW_LAND_VIDEO_BLUR:Ljava/lang/String; = "/raw/group_play_land_blur"

.field public static final GP_RAW_VIDEO:Ljava/lang/String; = "/raw/group_play_welcome"

.field public static final GP_RAW_VIDEO_BLUR:Ljava/lang/String; = "/raw/group_play_welcome"

.field public static final GP_TABLET_RAW_LAND_VIDEO:Ljava/lang/String; = "/raw/group_play_n1_land"

.field public static final GP_TABLET_RAW_LAND_VIDEO_BLUR:Ljava/lang/String; = "/raw/group_play_n1_land_blur"

.field public static final GP_TABLET_RAW_VIDEO:Ljava/lang/String; = "/raw/group_play_n1_portrait"

.field public static final GP_TABLET_RAW_VIDEO_BLUR:Ljava/lang/String; = "/raw/group_play_n1_portrait_blur"

.field public static final GROUP_NAME_ENTERED:Ljava/lang/String; = "GROUP_NAME"

.field public static final PASSWORD_ENTERED:Ljava/lang/String; = "PASSWORD"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
