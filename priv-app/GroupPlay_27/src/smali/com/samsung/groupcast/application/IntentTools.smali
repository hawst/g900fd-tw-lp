.class public Lcom/samsung/groupcast/application/IntentTools;
.super Ljava/lang/Object;
.source "IntentTools.java"


# static fields
.field public static final ACTION_GROUP_CAMCORDER:Ljava/lang/String; = "com.samsung.groupcamcoder.ACTION_START_CAMCORDER"

.field public static final ACTION_LEGACY_PRIVATE_SEND:Ljava/lang/String; = "com.samsung.allshare.presenter.action.SEND"

.field public static final ACTION_LEGACY_PRIVATE_SEND_MULTIPLE:Ljava/lang/String; = "com.samsung.allshare.presenter.action.SEND_MULTIPLE"

.field public static final ACTION_MUSIC_SHARE_RECEIVER:Ljava/lang/String; = "com.sec.musicliveshare.receiver"

.field public static final ACTION_MUSIC_SHARE_SERVER:Ljava/lang/String; = "com.sec.android.app.mediasync.Launch"

.field public static final ACTION_MYFILES_MULTIPLE_SELECTION:Ljava/lang/String; = "com.sec.android.app.myfiles.PICK_DATA_MULTIPLE"

.field public static final ACTION_MYFILES_SINGLE_SELECTION:Ljava/lang/String; = "com.sec.android.app.myfiles.PICK_DATA"

.field public static final ACTION_PACKAGE_NAME:Ljava/lang/String; = "com.samsung.groupcast"

.field public static final ACTION_PREFERRED_GALLERY_MULTIPLE_SELECTION:Ljava/lang/String; = "android.intent.action.MULTIPLE_PICK"

.field public static final ACTION_PRIVATE_SEND:Ljava/lang/String; = "com.samsung.groupcast.SEND"

.field public static final ACTION_PRIVATE_SEND_MULTIPLE:Ljava/lang/String; = "com.samsung.groupcast.SEND_MULTIPLE"

.field public static final ACTION_SAMSUNGAPPS_SELECTION:Ljava/lang/String; = "com.sec.android.app.samsungapps"

.field public static final ACTION_SEND_FROM_SCONNECT:Ljava/lang/String; = "com.samsung.groupcast.action.SEND_SCONNECT"

.field public static final ACTION_SEND_MULTIPE_MUSIC_FROM_MUSIC_PLAYER:Ljava/lang/String; = "com.samsung.groupcast.action.SEND_MULTIPLE_MUSIC_FOR_DJ_MODE"

.field public static final ACTION_SEND_MUSIC_FROM_MUSIC_PLAYER:Ljava/lang/String; = "com.samsung.groupcast.action.SEND_MUSIC_FOR_DJ_MODE"

.field public static final ACTION_SEND_VIDEO_FROM_VIDEO_PLAYER:Ljava/lang/String; = "com.samsung.groupcast.action.SEND_VIDEO"

.field public static final ACTION_SHARE_VIDEO:Ljava/lang/String; = "com.samsung.sharevideo.ACTION_START_MVPLAYER"

.field public static final ACTION_SHARE_VIDEO_MAIN_CLS:Ljava/lang/String; = "com.samsung.multivision.video.VideoMain"

.field public static final ACTION_SHARE_VIDEO_MAIN_PKG:Ljava/lang/String; = "com.samsung.multivision.video"

.field public static final ACTION_SHARE_VIDEO_SERVER_CLS:Ljava/lang/String; = "com.sec.android.app.mv.player.activity.MoviePlayer"

.field public static final ACTION_SHARE_VIDEO_SERVER_PKG:Ljava/lang/String; = "com.sec.android.app.mv.player"

.field public static final ACTION_SHARE_VIDEO_SERVER_URI:Ljava/lang/String; = "rtsp://multivision"

.field public static final ACTION_SNOTE_SINGLE_SELECTION:Ljava/lang/String; = "android.intent.action.SNOTE_GET_CONTENTS"

.field public static final EXTRA_ACTION_SNOTE_APP_NAME_KEY:Ljava/lang/String; = "app_name"

.field public static final EXTRA_CHINAURL_URL:Ljava/lang/String; = "http://www.allshareplay.cn/groupplay/android/"

.field public static final EXTRA_GROBAL_URL:Ljava/lang/String; = "http://m.allshareplay.com/groupplay/android/"

.field public static final EXTRA_GROUP_CAMCORDER_CHANNEL_ID:Ljava/lang/String; = "camcorder_channel_id"

.field public static final EXTRA_INFO_FROM_GROUPPLAY_NETIF:Ljava/lang/String; = "netIF"

.field public static final EXTRA_MUSIC_SHARE_CHANNEL_ID:Ljava/lang/String; = "music_channel_id"

.field public static final EXTRA_MUSIC_SHARE_PLAY_DEVICENAME:Ljava/lang/String; = "DeviceName"

.field public static final EXTRA_MUSIC_SHARE_PLAY_FROM:Ljava/lang/String; = "From"

.field public static final EXTRA_MUSIC_SHARE_PLAY_GROUPID:Ljava/lang/String; = "GroupId"

.field public static final EXTRA_MUSIC_SHARE_PLAY_LIST:Ljava/lang/String; = "PlayList"

.field public static final EXTRA_MUSIC_SHARE_PLAY_PACKAGE:Ljava/lang/String; = "com.sec.android.app.musicplayer"

.field public static final EXTRA_MUSIC_SHARE_PLAY_WIFI_BSSID:Ljava/lang/String; = "wifi_bssid"

.field public static final EXTRA_MUSIC_SHARE_PLAY_WIFI_PASSWD:Ljava/lang/String; = "wifi_passwd"

.field public static final EXTRA_MUSIC_SHARE_PLAY_WIFI_SSID:Ljava/lang/String; = "wifi_ssid"

.field public static final EXTRA_MYFILES_CONTENT_TYPE_KEY:Ljava/lang/String; = "CONTENT_TYPE"

.field public static final EXTRA_MYFILES_INITIAL_FOLDER:Ljava/lang/String;

.field public static final EXTRA_MYFILES_INITIAL_FOLDER_KEY:Ljava/lang/String; = "FOLDERPATH"

.field public static final EXTRA_MYFILES_JUST_SELECT_MODE:Ljava/lang/String; = "JUST_SELECT_MODE"

.field public static final EXTRA_PREFERRED_FALLBACK_GALLERY_IDENTIFIER_KEY:Ljava/lang/String; = "from-photowall"

.field public static final EXTRA_PREFERRED_FALLBACK_GALLERY_PICK_TYPE_KEY:Ljava/lang/String; = "multi-pick"

.field public static final EXTRA_PREFERRED_GALLERY_CONTENT_TYPE_KEY:Ljava/lang/String; = "local-only"

.field public static final EXTRA_PREFERRED_GALLERY_IDENTIFIER_KEY:Ljava/lang/String; = "from-groupCast"

.field public static final EXTRA_SAMSUNG_APP_INSTALL_URL:Ljava/lang/String; = "http://apps.samsung.com/mw/apps311.as"

.field public static final EXTRA_SHARE_VIDEO_PACKAGE:Ljava/lang/String; = "com.sec.android.app.mv.player"

.field public static final EXTRA_SNOTE_APP_NAME:Ljava/lang/String; = "All_Share"

.field public static final EXTRA_SNOTE_RETURN_TYPE:Ljava/lang/String; = "Imageonly"

.field public static final EXTRA_SNOTE_RETURN_TYPE_KEY:Ljava/lang/String; = "ReturnType"

.field public static final PREFERRED_ENHANCED_GALLERY_CLASS_NAME:Ljava/lang/String; = "com.sec.android.gallery3d.app.Gallery"

.field public static final PREFERRED_ENHANCED_GALLERY_PACKAGE_NAME:Ljava/lang/String; = "com.sec.android.gallery3d"

.field public static final PREFERRED_FALLBACK_GALLERY_PACKAGE_NAME:Ljava/lang/String; = "com.android.sec.gallery3d"

.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    const-class v0, Lcom/samsung/groupcast/application/IntentTools;

    invoke-static {v0}, Lcom/samsung/groupcast/application/Logger;->getTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/groupcast/application/IntentTools;->TAG:Ljava/lang/String;

    .line 35
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/groupcast/application/IntentTools;->EXTRA_MYFILES_INITIAL_FOLDER:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static AddChordIfIntent(Landroid/content/Intent;)V
    .locals 2
    .param p0, "intent"    # Landroid/content/Intent;

    .prologue
    .line 231
    const-string v0, "chordIfType"

    invoke-static {}, Lcom/samsung/groupcast/core/messaging/MessagingClient;->GetInstance()Lcom/samsung/groupcast/core/messaging/MessagingClient;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/groupcast/core/messaging/MessagingClient;->getChordInterface()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 233
    return-void
.end method

.method public static ClearShareViaExtrasFromIntent(Landroid/content/Intent;)V
    .locals 1
    .param p0, "intent"    # Landroid/content/Intent;

    .prologue
    .line 135
    if-eqz p0, :cond_0

    const-string v0, "android.intent.extra.STREAM"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 136
    const-string v0, "android.intent.extra.STREAM"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 138
    :cond_0
    return-void
.end method

.method public static getFirstImagePickerIntent()Landroid/content/Intent;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 236
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.MULTIPLE_PICK"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 237
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v1, 0x20000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 238
    const-string v1, "image/*"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 239
    const-string v1, "android.intent.extra.LOCAL_ONLY"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 240
    const-string v1, "from-groupCast"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 241
    return-object v0
.end method

.method public static getMusicLiveShareIntent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Lcom/samsung/groupcast/core/messaging/NfcMsg;)Landroid/content/Intent;
    .locals 4
    .param p0, "userName"    # Ljava/lang/String;
    .param p1, "sessionName"    # Ljava/lang/String;
    .param p2, "playFrom"    # Ljava/lang/String;
    .param p4, "nfcMsg"    # Lcom/samsung/groupcast/core/messaging/NfcMsg;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Lcom/samsung/groupcast/core/messaging/NfcMsg;",
            ")",
            "Landroid/content/Intent;"
        }
    .end annotation

    .prologue
    .line 143
    .local p3, "playList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    sget-object v1, Lcom/samsung/groupcast/application/IntentTools;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getMusicLiveShareIntent userName:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 144
    sget-object v1, Lcom/samsung/groupcast/application/IntentTools;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getMusicLiveShareIntent sessionName:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.app.mediasync.Launch"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 147
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "From"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 148
    const-string v1, "GroupId"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 149
    const-string v1, "DeviceName"

    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 150
    const-string v1, "wifi_bssid"

    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getWifiBSSID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 152
    const-string v1, "wifi_ssid"

    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getWifiSSID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 154
    const-string v1, "wifi_passwd"

    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getWifiPassword()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 156
    const-string v1, "PlayList"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 158
    if-eqz p4, :cond_0

    .line 159
    const-string v1, "music_channel_id"

    iget-object v2, p4, Lcom/samsung/groupcast/core/messaging/NfcMsg;->music_live_channel_id:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 162
    :cond_0
    invoke-static {v0}, Lcom/samsung/groupcast/application/IntentTools;->AddChordIfIntent(Landroid/content/Intent;)V

    .line 164
    return-object v0
.end method

.method public static getSecondImagePickerIntent()Landroid/content/Intent;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 246
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.GET_CONTENT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 247
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v1, 0x20000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 248
    const-string v1, "image/*"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 249
    const-string v1, "android.intent.extra.LOCAL_ONLY"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 250
    const-string v1, "multi-pick"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 251
    const-string v1, "from-photowall"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 252
    return-object v0
.end method

.method public static getSharedUrisFromIntent(Landroid/content/Intent;)Ljava/util/ArrayList;
    .locals 5
    .param p0, "intent"    # Landroid/content/Intent;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Intent;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation

    .prologue
    .line 108
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 110
    .local v2, "uris":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    if-nez p0, :cond_1

    .line 131
    :cond_0
    :goto_0
    return-object v2

    .line 114
    :cond_1
    invoke-static {p0}, Lcom/samsung/groupcast/application/IntentTools;->isSendIntent(Landroid/content/Intent;)Z

    move-result v3

    if-nez v3, :cond_2

    invoke-static {p0}, Lcom/samsung/groupcast/application/IntentTools;->isSendDJModeIntent(Landroid/content/Intent;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 115
    :cond_2
    const-string v3, "android.intent.extra.STREAM"

    invoke-virtual {p0, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/net/Uri;

    .line 117
    .local v1, "uri":Landroid/net/Uri;
    if-eqz v1, :cond_0

    .line 118
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 120
    .end local v1    # "uri":Landroid/net/Uri;
    :cond_3
    invoke-static {p0}, Lcom/samsung/groupcast/application/IntentTools;->isSendMultipleIntent(Landroid/content/Intent;)Z

    move-result v3

    if-nez v3, :cond_4

    invoke-static {p0}, Lcom/samsung/groupcast/application/IntentTools;->isSendMultipleDJModeIntent(Landroid/content/Intent;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 122
    :cond_4
    const-string v3, "android.intent.extra.STREAM"

    invoke-virtual {p0, v3}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 124
    .local v0, "intentUris":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    if-eqz v0, :cond_0

    .line 125
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 128
    .end local v0    # "intentUris":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    :cond_5
    sget-object v3, Lcom/samsung/groupcast/application/IntentTools;->TAG:Ljava/lang/String;

    const-string v4, "Intent action is not a send or send multiple action. No content found."

    invoke-static {v3, v4}, Lcom/samsung/groupcast/application/Logger;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static isSendDJModeIntent(Landroid/content/Intent;)Z
    .locals 2
    .param p0, "intent"    # Landroid/content/Intent;

    .prologue
    .line 199
    if-nez p0, :cond_0

    .line 200
    const/4 v1, 0x0

    .line 203
    :goto_0
    return v1

    .line 202
    :cond_0
    invoke-virtual {p0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 203
    .local v0, "action":Ljava/lang/String;
    const-string v1, "com.samsung.groupcast.action.SEND_MUSIC_FOR_DJ_MODE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method

.method public static isSendIntent(Landroid/content/Intent;)Z
    .locals 3
    .param p0, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v1, 0x0

    .line 172
    if-nez p0, :cond_1

    .line 178
    :cond_0
    :goto_0
    return v1

    .line 176
    :cond_1
    invoke-virtual {p0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 178
    .local v0, "action":Ljava/lang/String;
    const-string v2, "com.samsung.groupcast.SEND"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "com.samsung.allshare.presenter.action.SEND"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "android.intent.action.SEND"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_2
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public static isSendMultipleDJModeIntent(Landroid/content/Intent;)Z
    .locals 2
    .param p0, "intent"    # Landroid/content/Intent;

    .prologue
    .line 207
    if-nez p0, :cond_0

    .line 208
    const/4 v1, 0x0

    .line 211
    :goto_0
    return v1

    .line 210
    :cond_0
    invoke-virtual {p0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 211
    .local v0, "action":Ljava/lang/String;
    const-string v1, "com.samsung.groupcast.action.SEND_MULTIPLE_MUSIC_FOR_DJ_MODE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method

.method public static isSendMultipleIntent(Landroid/content/Intent;)Z
    .locals 3
    .param p0, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v1, 0x0

    .line 187
    if-nez p0, :cond_1

    .line 193
    :cond_0
    :goto_0
    return v1

    .line 191
    :cond_1
    invoke-virtual {p0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 193
    .local v0, "action":Ljava/lang/String;
    const-string v2, "com.samsung.groupcast.SEND_MULTIPLE"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "com.samsung.allshare.presenter.action.SEND_MULTIPLE"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "android.intent.action.SEND_MULTIPLE"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_2
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public static isSendSconnectIntent(Landroid/content/Intent;)Z
    .locals 2
    .param p0, "intent"    # Landroid/content/Intent;

    .prologue
    .line 223
    if-nez p0, :cond_0

    .line 224
    const/4 v1, 0x0

    .line 227
    :goto_0
    return v1

    .line 226
    :cond_0
    invoke-virtual {p0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 227
    .local v0, "action":Ljava/lang/String;
    const-string v1, "com.samsung.groupcast.action.SEND_SCONNECT"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method

.method public static isSendVideoIntent(Landroid/content/Intent;)Z
    .locals 2
    .param p0, "intent"    # Landroid/content/Intent;

    .prologue
    .line 215
    if-nez p0, :cond_0

    .line 216
    const/4 v1, 0x0

    .line 219
    :goto_0
    return v1

    .line 218
    :cond_0
    invoke-virtual {p0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 219
    .local v0, "action":Ljava/lang/String;
    const-string v1, "com.samsung.groupcast.action.SEND_VIDEO"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method
