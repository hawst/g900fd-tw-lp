.class public final enum Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;
.super Ljava/lang/Enum;
.source "StatBigDataLog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/groupcast/application/StatBigDataLog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "BigDataMenu"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

.field public static final enum ADD:Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

.field public static final enum ALPHABET_SORT:Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

.field public static final enum CAMCODER:Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

.field public static final enum COLLAGE:Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

.field public static final enum CREATE:Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

.field public static final enum DOC:Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

.field public static final enum DRAWING_MODE:Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

.field public static final enum END:Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

.field public static final enum ERASER_SETTINGS:Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

.field public static final enum ERASE_ALL:Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

.field public static final enum GAME:Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

.field public static final enum HELP:Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

.field public static final enum IMAGE:Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

.field public static final enum JOIN:Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

.field public static final enum MUSIC:Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

.field public static final enum NEW_ARRIVAL_SORT:Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

.field public static final enum NICK_NAME_SETTING:Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

.field public static final enum PEN_SETTING:Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

.field public static final enum PRIVATE_MODE:Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

.field public static final enum REMOTE_INVITATION:Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

.field public static final enum REMOVE:Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

.field public static final enum SETTING:Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

.field public static final enum SORT_BY:Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

.field public static final enum START:Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

.field public static final enum TOP_RATED_SORT:Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

.field public static final enum UNKNOW:Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

.field public static final enum VIDEO:Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

.field public static final enum VIEW_MODE:Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;


# instance fields
.field private final code:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 20
    new-instance v0, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

    const-string v1, "UNKNOW"

    const-string v2, "GP00"

    invoke-direct {v0, v1, v4, v2}, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;->UNKNOW:Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

    .line 21
    new-instance v0, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

    const-string v1, "START"

    const-string v2, "GP01"

    invoke-direct {v0, v1, v5, v2}, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;->START:Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

    .line 22
    new-instance v0, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

    const-string v1, "END"

    const-string v2, "GP02"

    invoke-direct {v0, v1, v6, v2}, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;->END:Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

    .line 23
    new-instance v0, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

    const-string v1, "CREATE"

    const-string v2, "GP03"

    invoke-direct {v0, v1, v7, v2}, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;->CREATE:Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

    .line 24
    new-instance v0, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

    const-string v1, "JOIN"

    const-string v2, "GP04"

    invoke-direct {v0, v1, v8, v2}, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;->JOIN:Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

    .line 25
    new-instance v0, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

    const-string v1, "HELP"

    const/4 v2, 0x5

    const-string v3, "GP05"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;->HELP:Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

    .line 26
    new-instance v0, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

    const-string v1, "SETTING"

    const/4 v2, 0x6

    const-string v3, "GP06"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;->SETTING:Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

    .line 27
    new-instance v0, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

    const-string v1, "SORT_BY"

    const/4 v2, 0x7

    const-string v3, "GP07"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;->SORT_BY:Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

    .line 28
    new-instance v0, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

    const-string v1, "MUSIC"

    const/16 v2, 0x8

    const-string v3, "GP08"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;->MUSIC:Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

    .line 29
    new-instance v0, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

    const-string v1, "IMAGE"

    const/16 v2, 0x9

    const-string v3, "GP09"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;->IMAGE:Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

    .line 30
    new-instance v0, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

    const-string v1, "DOC"

    const/16 v2, 0xa

    const-string v3, "GP10"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;->DOC:Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

    .line 31
    new-instance v0, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

    const-string v1, "GAME"

    const/16 v2, 0xb

    const-string v3, "GP11"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;->GAME:Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

    .line 32
    new-instance v0, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

    const-string v1, "VIDEO"

    const/16 v2, 0xc

    const-string v3, "GP12"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;->VIDEO:Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

    .line 33
    new-instance v0, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

    const-string v1, "CAMCODER"

    const/16 v2, 0xd

    const-string v3, "GP13"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;->CAMCODER:Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

    .line 34
    new-instance v0, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

    const-string v1, "ALPHABET_SORT"

    const/16 v2, 0xe

    const-string v3, "GP14"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;->ALPHABET_SORT:Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

    .line 35
    new-instance v0, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

    const-string v1, "TOP_RATED_SORT"

    const/16 v2, 0xf

    const-string v3, "GP15"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;->TOP_RATED_SORT:Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

    .line 36
    new-instance v0, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

    const-string v1, "NEW_ARRIVAL_SORT"

    const/16 v2, 0x10

    const-string v3, "GP16"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;->NEW_ARRIVAL_SORT:Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

    .line 37
    new-instance v0, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

    const-string v1, "NICK_NAME_SETTING"

    const/16 v2, 0x11

    const-string v3, "GP17"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;->NICK_NAME_SETTING:Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

    .line 40
    new-instance v0, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

    const-string v1, "DRAWING_MODE"

    const/16 v2, 0x12

    const-string v3, "GP20"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;->DRAWING_MODE:Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

    .line 41
    new-instance v0, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

    const-string v1, "VIEW_MODE"

    const/16 v2, 0x13

    const-string v3, "GP21"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;->VIEW_MODE:Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

    .line 42
    new-instance v0, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

    const-string v1, "PEN_SETTING"

    const/16 v2, 0x14

    const-string v3, "GP22"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;->PEN_SETTING:Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

    .line 43
    new-instance v0, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

    const-string v1, "ERASER_SETTINGS"

    const/16 v2, 0x15

    const-string v3, "GP23"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;->ERASER_SETTINGS:Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

    .line 44
    new-instance v0, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

    const-string v1, "ERASE_ALL"

    const/16 v2, 0x16

    const-string v3, "GP24"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;->ERASE_ALL:Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

    .line 45
    new-instance v0, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

    const-string v1, "ADD"

    const/16 v2, 0x17

    const-string v3, "GP25"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;->ADD:Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

    .line 46
    new-instance v0, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

    const-string v1, "REMOVE"

    const/16 v2, 0x18

    const-string v3, "GP26"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;->REMOVE:Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

    .line 47
    new-instance v0, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

    const-string v1, "PRIVATE_MODE"

    const/16 v2, 0x19

    const-string v3, "GP27"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;->PRIVATE_MODE:Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

    .line 48
    new-instance v0, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

    const-string v1, "REMOTE_INVITATION"

    const/16 v2, 0x1a

    const-string v3, "GP28"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;->REMOTE_INVITATION:Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

    .line 49
    new-instance v0, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

    const-string v1, "COLLAGE"

    const/16 v2, 0x1b

    const-string v3, "GP29"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;->COLLAGE:Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

    .line 19
    const/16 v0, 0x1c

    new-array v0, v0, [Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

    sget-object v1, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;->UNKNOW:Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;->START:Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;->END:Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

    aput-object v1, v0, v6

    sget-object v1, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;->CREATE:Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

    aput-object v1, v0, v7

    sget-object v1, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;->JOIN:Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;->HELP:Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;->SETTING:Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;->SORT_BY:Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;->MUSIC:Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;->IMAGE:Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;->DOC:Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;->GAME:Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;->VIDEO:Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;->CAMCODER:Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;->ALPHABET_SORT:Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;->TOP_RATED_SORT:Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;->NEW_ARRIVAL_SORT:Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;->NICK_NAME_SETTING:Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;->DRAWING_MODE:Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;->VIEW_MODE:Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;->PEN_SETTING:Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;->ERASER_SETTINGS:Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;->ERASE_ALL:Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;->ADD:Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;->REMOVE:Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;->PRIVATE_MODE:Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;->REMOTE_INVITATION:Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;->COLLAGE:Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;->$VALUES:[Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "code"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 55
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 56
    iput-object p3, p0, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;->code:Ljava/lang/String;

    .line 57
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;->getCode()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getCode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;->code:Ljava/lang/String;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 19
    const-class v0, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

    return-object v0
.end method

.method public static values()[Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;->$VALUES:[Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

    invoke-virtual {v0}, [Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

    return-object v0
.end method
