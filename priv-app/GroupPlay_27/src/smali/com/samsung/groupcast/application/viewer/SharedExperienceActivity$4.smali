.class Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity$4;
.super Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentListener;
.source "SharedExperienceActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;


# direct methods
.method constructor <init>(Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;)V
    .locals 0

    .prologue
    .line 1076
    iput-object p1, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity$4;->this$0:Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;

    invoke-direct {p0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onComplete(Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentRequest;Lcom/samsung/groupcast/misc/requests/Result;Ljava/lang/String;)V
    .locals 9
    .param p1, "r"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentRequest;
    .param p2, "result"    # Lcom/samsung/groupcast/misc/requests/Result;
    .param p3, "path"    # Ljava/lang/String;

    .prologue
    .line 1107
    invoke-virtual {p1}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentRequest;->getContentId()Ljava/lang/String;

    move-result-object v0

    .line 1108
    .local v0, "contentId":Ljava/lang/String;
    new-instance v5, Ljava/util/LinkedList;

    invoke-direct {v5}, Ljava/util/LinkedList;-><init>()V

    .line 1111
    .local v5, "listeners":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/samsung/groupcast/application/viewer/ContentDeliveryListener;>;"
    iget-object v6, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity$4;->this$0:Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;

    # getter for: Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mContentDeliveryLock:Ljava/lang/Object;
    invoke-static {v6}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->access$300(Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;)Ljava/lang/Object;

    move-result-object v7

    monitor-enter v7

    .line 1112
    :try_start_0
    iget-object v6, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity$4;->this$0:Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;

    # getter for: Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mContentDeliveryListeners:Ljava/util/HashMap;
    invoke-static {v6}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->access$400(Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;)Ljava/util/HashMap;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/LinkedList;

    .line 1113
    .local v3, "list":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/samsung/groupcast/application/viewer/ContentDeliveryListener;>;"
    if-eqz v3, :cond_0

    .line 1114
    invoke-virtual {v3}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 1115
    .local v2, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/groupcast/application/viewer/ContentDeliveryListener;>;"
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 1116
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/groupcast/application/viewer/ContentDeliveryListener;

    .line 1117
    .local v4, "listener":Lcom/samsung/groupcast/application/viewer/ContentDeliveryListener;
    invoke-virtual {v5, v4}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1146
    .end local v2    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/groupcast/application/viewer/ContentDeliveryListener;>;"
    .end local v3    # "list":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/samsung/groupcast/application/viewer/ContentDeliveryListener;>;"
    .end local v4    # "listener":Lcom/samsung/groupcast/application/viewer/ContentDeliveryListener;
    :catchall_0
    move-exception v6

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v6

    .line 1121
    .restart local v3    # "list":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/samsung/groupcast/application/viewer/ContentDeliveryListener;>;"
    :cond_0
    :try_start_1
    sget-object v6, Lcom/samsung/groupcast/misc/requests/Result;->SUCCESS:Lcom/samsung/groupcast/misc/requests/Result;

    if-eq v6, p2, :cond_4

    .line 1122
    iget-object v6, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity$4;->this$0:Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;

    # getter for: Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mContentRequests:Ljava/util/HashMap;
    invoke-static {v6}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->access$500(Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;)Ljava/util/HashMap;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1123
    iget-object v6, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity$4;->this$0:Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;

    # getter for: Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mSession:Lcom/samsung/groupcast/legacy/gp2/session/model/Session;
    invoke-static {v6}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->access$600(Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;)Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->getManifest()Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;

    move-result-object v6

    invoke-virtual {v6, v0}, Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;->getItem(Ljava/lang/String;)Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;

    move-result-object v6

    if-nez v6, :cond_2

    .line 1124
    monitor-exit v7

    .line 1155
    :cond_1
    :goto_1
    return-void

    .line 1126
    :cond_2
    sget-object v6, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity$6;->$SwitchMap$com$samsung$groupcast$misc$requests$Result:[I

    invoke-virtual {p2}, Lcom/samsung/groupcast/misc/requests/Result;->ordinal()I

    move-result v8

    aget v6, v6, v8

    packed-switch v6, :pswitch_data_0

    .line 1144
    :cond_3
    :goto_2
    :pswitch_0
    monitor-exit v7

    goto :goto_1

    .line 1132
    :pswitch_1
    iget-object v6, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity$4;->this$0:Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;

    # getter for: Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mContentBroker:Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;
    invoke-static {v6}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->access$700(Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;)Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;

    move-result-object v6

    if-eqz v6, :cond_3

    .line 1133
    iget-object v6, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity$4;->this$0:Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;

    # getter for: Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mContentBroker:Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;
    invoke-static {v6}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->access$700(Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;)Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;

    move-result-object v6

    invoke-virtual {v6, v0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;->createContentRequest(Ljava/lang/String;)Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentRequest;

    move-result-object v1

    .line 1135
    .local v1, "contentRequest":Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentRequest;
    if-eqz v1, :cond_3

    .line 1136
    iget-object v6, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity$4;->this$0:Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;

    # getter for: Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mContentListener:Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentListener;
    invoke-static {v6}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->access$800(Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;)Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentListener;

    move-result-object v6

    invoke-virtual {v1, v6}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentRequest;->addListener(Lcom/samsung/groupcast/misc/requests/RequestListener;)V

    .line 1137
    iget-object v6, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity$4;->this$0:Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;

    # getter for: Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mContentRequests:Ljava/util/HashMap;
    invoke-static {v6}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->access$500(Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;)Ljava/util/HashMap;

    move-result-object v6

    invoke-virtual {v6, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1138
    invoke-virtual {v1}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentRequest;->start()V

    goto :goto_2

    .line 1146
    .end local v1    # "contentRequest":Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentRequest;
    :cond_4
    monitor-exit v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1149
    invoke-virtual {v5}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 1150
    .restart local v2    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/groupcast/application/viewer/ContentDeliveryListener;>;"
    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 1151
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/groupcast/application/viewer/ContentDeliveryListener;

    .line 1152
    .restart local v4    # "listener":Lcom/samsung/groupcast/application/viewer/ContentDeliveryListener;
    invoke-interface {v4, v0, p2, p3}, Lcom/samsung/groupcast/application/viewer/ContentDeliveryListener;->onContentDeliveryComplete(Ljava/lang/String;Lcom/samsung/groupcast/misc/requests/Result;Ljava/lang/String;)V

    .line 1153
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    goto :goto_3

    .line 1126
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onProgress(Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentRequest;Lcom/samsung/groupcast/misc/requests/Progress;)V
    .locals 8
    .param p1, "r"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentRequest;
    .param p2, "progress"    # Lcom/samsung/groupcast/misc/requests/Progress;

    .prologue
    .line 1079
    invoke-virtual {p1}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentRequest;->getContentId()Ljava/lang/String;

    move-result-object v0

    .line 1080
    .local v0, "contentId":Ljava/lang/String;
    new-instance v4, Ljava/util/LinkedList;

    invoke-direct {v4}, Ljava/util/LinkedList;-><init>()V

    .line 1083
    .local v4, "listeners":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/samsung/groupcast/application/viewer/ContentDeliveryListener;>;"
    iget-object v5, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity$4;->this$0:Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;

    # getter for: Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mContentDeliveryLock:Ljava/lang/Object;
    invoke-static {v5}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->access$300(Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;)Ljava/lang/Object;

    move-result-object v6

    monitor-enter v6

    .line 1084
    :try_start_0
    iget-object v5, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity$4;->this$0:Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;

    # getter for: Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mContentDeliveryListeners:Ljava/util/HashMap;
    invoke-static {v5}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->access$400(Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;)Ljava/util/HashMap;

    move-result-object v5

    invoke-virtual {p1}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentRequest;->getContentId()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/LinkedList;

    .line 1086
    .local v2, "list":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/samsung/groupcast/application/viewer/ContentDeliveryListener;>;"
    if-eqz v2, :cond_0

    .line 1087
    invoke-virtual {v2}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 1088
    .local v1, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/groupcast/application/viewer/ContentDeliveryListener;>;"
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1089
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/groupcast/application/viewer/ContentDeliveryListener;

    .line 1091
    .local v3, "listener":Lcom/samsung/groupcast/application/viewer/ContentDeliveryListener;
    invoke-virtual {v4, v3}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1094
    .end local v1    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/groupcast/application/viewer/ContentDeliveryListener;>;"
    .end local v2    # "list":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/samsung/groupcast/application/viewer/ContentDeliveryListener;>;"
    .end local v3    # "listener":Lcom/samsung/groupcast/application/viewer/ContentDeliveryListener;
    :catchall_0
    move-exception v5

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v5

    .restart local v2    # "list":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/samsung/groupcast/application/viewer/ContentDeliveryListener;>;"
    :cond_0
    :try_start_1
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1097
    invoke-virtual {v4}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 1098
    .restart local v1    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/groupcast/application/viewer/ContentDeliveryListener;>;"
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1099
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/groupcast/application/viewer/ContentDeliveryListener;

    .line 1100
    .restart local v3    # "listener":Lcom/samsung/groupcast/application/viewer/ContentDeliveryListener;
    invoke-interface {v3, v0, p2}, Lcom/samsung/groupcast/application/viewer/ContentDeliveryListener;->onContentDeliveryProgress(Ljava/lang/String;Lcom/samsung/groupcast/misc/requests/Progress;)V

    .line 1101
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_1

    .line 1103
    .end local v3    # "listener":Lcom/samsung/groupcast/application/viewer/ContentDeliveryListener;
    :cond_1
    return-void
.end method
