.class Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity$5;
.super Lcom/samsung/groupcast/legacy/gp2/session/storage/StoreManifestListener;
.source "SharedExperienceActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->storeNewManifest(Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;


# direct methods
.method constructor <init>(Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;)V
    .locals 0

    .prologue
    .line 1537
    iput-object p1, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity$5;->this$0:Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;

    invoke-direct {p0}, Lcom/samsung/groupcast/legacy/gp2/session/storage/StoreManifestListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onComplete(Lcom/samsung/groupcast/legacy/gp2/session/storage/StoreManifestRequest;Lcom/samsung/groupcast/misc/requests/Result;)V
    .locals 2
    .param p1, "request"    # Lcom/samsung/groupcast/legacy/gp2/session/storage/StoreManifestRequest;
    .param p2, "result"    # Lcom/samsung/groupcast/misc/requests/Result;

    .prologue
    const/4 v1, 0x0

    .line 1540
    iget-object v0, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity$5;->this$0:Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;

    # getter for: Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mPaused:Z
    invoke-static {v0}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->access$900(Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1548
    :goto_0
    return-void

    .line 1542
    :cond_0
    sget-object v0, Lcom/samsung/groupcast/misc/requests/Result;->SUCCESS:Lcom/samsung/groupcast/misc/requests/Result;

    if-ne v0, p2, :cond_1

    .line 1543
    iget-object v0, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity$5;->this$0:Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;

    invoke-virtual {v0}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->onStoreManifestForAddedFilesCompleted()V

    .line 1546
    :goto_1
    iget-object v0, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity$5;->this$0:Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;

    # setter for: Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mManifestGenerator:Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;
    invoke-static {v0, v1}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->access$202(Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;)Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;

    .line 1547
    iget-object v0, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity$5;->this$0:Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;

    # setter for: Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->mStoreManifestRequest:Lcom/samsung/groupcast/legacy/gp2/session/storage/StoreManifestRequest;
    invoke-static {v0, v1}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->access$1002(Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;Lcom/samsung/groupcast/legacy/gp2/session/storage/StoreManifestRequest;)Lcom/samsung/groupcast/legacy/gp2/session/storage/StoreManifestRequest;

    goto :goto_0

    .line 1545
    :cond_1
    iget-object v0, p0, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity$5;->this$0:Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;

    invoke-virtual {v0}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->onManifestCreationFailed()V

    goto :goto_1
.end method
