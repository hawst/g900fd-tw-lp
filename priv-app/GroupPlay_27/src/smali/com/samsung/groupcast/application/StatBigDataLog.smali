.class public Lcom/samsung/groupcast/application/StatBigDataLog;
.super Ljava/lang/Object;
.source "StatBigDataLog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/groupcast/application/StatBigDataLog$1;,
        Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;

.field private static instance:Lcom/samsung/groupcast/application/StatBigDataLog;


# instance fields
.field private final mMatchMenu:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    const-class v0, Lcom/samsung/groupcast/application/StatBigDataLog;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/groupcast/application/StatBigDataLog;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    invoke-static {}, Lcom/samsung/groupcast/application/StatLog;->getInstance()Lcom/samsung/groupcast/application/StatLog;

    .line 66
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/groupcast/application/StatBigDataLog;->mMatchMenu:Ljava/util/LinkedHashMap;

    .line 67
    invoke-direct {p0}, Lcom/samsung/groupcast/application/StatBigDataLog;->initializeMenuCounter()V

    .line 68
    return-void
.end method

.method public static getInstance()Lcom/samsung/groupcast/application/StatBigDataLog;
    .locals 2

    .prologue
    .line 109
    sget-object v0, Lcom/samsung/groupcast/application/StatBigDataLog;->instance:Lcom/samsung/groupcast/application/StatBigDataLog;

    if-nez v0, :cond_0

    .line 110
    sget-object v0, Lcom/samsung/groupcast/application/StatBigDataLog;->TAG:Ljava/lang/String;

    const-string v1, "StatBigDataLog - new instance"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 111
    new-instance v0, Lcom/samsung/groupcast/application/StatBigDataLog;

    invoke-direct {v0}, Lcom/samsung/groupcast/application/StatBigDataLog;-><init>()V

    sput-object v0, Lcom/samsung/groupcast/application/StatBigDataLog;->instance:Lcom/samsung/groupcast/application/StatBigDataLog;

    .line 113
    :cond_0
    sget-object v0, Lcom/samsung/groupcast/application/StatBigDataLog;->instance:Lcom/samsung/groupcast/application/StatBigDataLog;

    return-object v0
.end method

.method private initializeMenuCounter()V
    .locals 7

    .prologue
    .line 72
    invoke-static {}, Lcom/samsung/groupcast/application/StatLog$Menu;->values()[Lcom/samsung/groupcast/application/StatLog$Menu;

    move-result-object v0

    .local v0, "arr$":[Lcom/samsung/groupcast/application/StatLog$Menu;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 73
    .local v3, "menu":Lcom/samsung/groupcast/application/StatLog$Menu;
    sget-object v4, Lcom/samsung/groupcast/application/StatBigDataLog$1;->$SwitchMap$com$samsung$groupcast$application$StatLog$Menu:[I

    invoke-virtual {v3}, Lcom/samsung/groupcast/application/StatLog$Menu;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 103
    iget-object v4, p0, Lcom/samsung/groupcast/application/StatBigDataLog;->mMatchMenu:Ljava/util/LinkedHashMap;

    invoke-virtual {v3}, Lcom/samsung/groupcast/application/StatLog$Menu;->getCode()Ljava/lang/String;

    move-result-object v5

    sget-object v6, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;->UNKNOW:Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

    invoke-virtual {v4, v5, v6}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 72
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 74
    :pswitch_0
    iget-object v4, p0, Lcom/samsung/groupcast/application/StatBigDataLog;->mMatchMenu:Ljava/util/LinkedHashMap;

    invoke-virtual {v3}, Lcom/samsung/groupcast/application/StatLog$Menu;->getCode()Ljava/lang/String;

    move-result-object v5

    sget-object v6, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;->CREATE:Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

    invoke-virtual {v4, v5, v6}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 75
    :pswitch_1
    iget-object v4, p0, Lcom/samsung/groupcast/application/StatBigDataLog;->mMatchMenu:Ljava/util/LinkedHashMap;

    invoke-virtual {v3}, Lcom/samsung/groupcast/application/StatLog$Menu;->getCode()Ljava/lang/String;

    move-result-object v5

    sget-object v6, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;->JOIN:Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

    invoke-virtual {v4, v5, v6}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 76
    :pswitch_2
    iget-object v4, p0, Lcom/samsung/groupcast/application/StatBigDataLog;->mMatchMenu:Ljava/util/LinkedHashMap;

    invoke-virtual {v3}, Lcom/samsung/groupcast/application/StatLog$Menu;->getCode()Ljava/lang/String;

    move-result-object v5

    sget-object v6, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;->HELP:Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

    invoke-virtual {v4, v5, v6}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 77
    :pswitch_3
    iget-object v4, p0, Lcom/samsung/groupcast/application/StatBigDataLog;->mMatchMenu:Ljava/util/LinkedHashMap;

    invoke-virtual {v3}, Lcom/samsung/groupcast/application/StatLog$Menu;->getCode()Ljava/lang/String;

    move-result-object v5

    sget-object v6, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;->SETTING:Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

    invoke-virtual {v4, v5, v6}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 78
    :pswitch_4
    iget-object v4, p0, Lcom/samsung/groupcast/application/StatBigDataLog;->mMatchMenu:Ljava/util/LinkedHashMap;

    invoke-virtual {v3}, Lcom/samsung/groupcast/application/StatLog$Menu;->getCode()Ljava/lang/String;

    move-result-object v5

    sget-object v6, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;->SORT_BY:Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

    invoke-virtual {v4, v5, v6}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 79
    :pswitch_5
    iget-object v4, p0, Lcom/samsung/groupcast/application/StatBigDataLog;->mMatchMenu:Ljava/util/LinkedHashMap;

    invoke-virtual {v3}, Lcom/samsung/groupcast/application/StatLog$Menu;->getCode()Ljava/lang/String;

    move-result-object v5

    sget-object v6, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;->MUSIC:Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

    invoke-virtual {v4, v5, v6}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 80
    :pswitch_6
    iget-object v4, p0, Lcom/samsung/groupcast/application/StatBigDataLog;->mMatchMenu:Ljava/util/LinkedHashMap;

    invoke-virtual {v3}, Lcom/samsung/groupcast/application/StatLog$Menu;->getCode()Ljava/lang/String;

    move-result-object v5

    sget-object v6, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;->IMAGE:Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

    invoke-virtual {v4, v5, v6}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 81
    :pswitch_7
    iget-object v4, p0, Lcom/samsung/groupcast/application/StatBigDataLog;->mMatchMenu:Ljava/util/LinkedHashMap;

    invoke-virtual {v3}, Lcom/samsung/groupcast/application/StatLog$Menu;->getCode()Ljava/lang/String;

    move-result-object v5

    sget-object v6, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;->DOC:Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

    invoke-virtual {v4, v5, v6}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 82
    :pswitch_8
    iget-object v4, p0, Lcom/samsung/groupcast/application/StatBigDataLog;->mMatchMenu:Ljava/util/LinkedHashMap;

    invoke-virtual {v3}, Lcom/samsung/groupcast/application/StatLog$Menu;->getCode()Ljava/lang/String;

    move-result-object v5

    sget-object v6, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;->GAME:Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

    invoke-virtual {v4, v5, v6}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 83
    :pswitch_9
    iget-object v4, p0, Lcom/samsung/groupcast/application/StatBigDataLog;->mMatchMenu:Ljava/util/LinkedHashMap;

    invoke-virtual {v3}, Lcom/samsung/groupcast/application/StatLog$Menu;->getCode()Ljava/lang/String;

    move-result-object v5

    sget-object v6, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;->VIDEO:Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

    invoke-virtual {v4, v5, v6}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 84
    :pswitch_a
    iget-object v4, p0, Lcom/samsung/groupcast/application/StatBigDataLog;->mMatchMenu:Ljava/util/LinkedHashMap;

    invoke-virtual {v3}, Lcom/samsung/groupcast/application/StatLog$Menu;->getCode()Ljava/lang/String;

    move-result-object v5

    sget-object v6, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;->CAMCODER:Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

    invoke-virtual {v4, v5, v6}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 85
    :pswitch_b
    iget-object v4, p0, Lcom/samsung/groupcast/application/StatBigDataLog;->mMatchMenu:Ljava/util/LinkedHashMap;

    invoke-virtual {v3}, Lcom/samsung/groupcast/application/StatLog$Menu;->getCode()Ljava/lang/String;

    move-result-object v5

    sget-object v6, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;->ALPHABET_SORT:Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

    invoke-virtual {v4, v5, v6}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 86
    :pswitch_c
    iget-object v4, p0, Lcom/samsung/groupcast/application/StatBigDataLog;->mMatchMenu:Ljava/util/LinkedHashMap;

    invoke-virtual {v3}, Lcom/samsung/groupcast/application/StatLog$Menu;->getCode()Ljava/lang/String;

    move-result-object v5

    sget-object v6, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;->TOP_RATED_SORT:Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

    invoke-virtual {v4, v5, v6}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 87
    :pswitch_d
    iget-object v4, p0, Lcom/samsung/groupcast/application/StatBigDataLog;->mMatchMenu:Ljava/util/LinkedHashMap;

    invoke-virtual {v3}, Lcom/samsung/groupcast/application/StatLog$Menu;->getCode()Ljava/lang/String;

    move-result-object v5

    sget-object v6, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;->NEW_ARRIVAL_SORT:Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

    invoke-virtual {v4, v5, v6}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 88
    :pswitch_e
    iget-object v4, p0, Lcom/samsung/groupcast/application/StatBigDataLog;->mMatchMenu:Ljava/util/LinkedHashMap;

    invoke-virtual {v3}, Lcom/samsung/groupcast/application/StatLog$Menu;->getCode()Ljava/lang/String;

    move-result-object v5

    sget-object v6, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;->NICK_NAME_SETTING:Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

    invoke-virtual {v4, v5, v6}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 91
    :pswitch_f
    iget-object v4, p0, Lcom/samsung/groupcast/application/StatBigDataLog;->mMatchMenu:Ljava/util/LinkedHashMap;

    invoke-virtual {v3}, Lcom/samsung/groupcast/application/StatLog$Menu;->getCode()Ljava/lang/String;

    move-result-object v5

    sget-object v6, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;->DRAWING_MODE:Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

    invoke-virtual {v4, v5, v6}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 92
    :pswitch_10
    iget-object v4, p0, Lcom/samsung/groupcast/application/StatBigDataLog;->mMatchMenu:Ljava/util/LinkedHashMap;

    invoke-virtual {v3}, Lcom/samsung/groupcast/application/StatLog$Menu;->getCode()Ljava/lang/String;

    move-result-object v5

    sget-object v6, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;->VIEW_MODE:Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

    invoke-virtual {v4, v5, v6}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 93
    :pswitch_11
    iget-object v4, p0, Lcom/samsung/groupcast/application/StatBigDataLog;->mMatchMenu:Ljava/util/LinkedHashMap;

    invoke-virtual {v3}, Lcom/samsung/groupcast/application/StatLog$Menu;->getCode()Ljava/lang/String;

    move-result-object v5

    sget-object v6, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;->PEN_SETTING:Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

    invoke-virtual {v4, v5, v6}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 94
    :pswitch_12
    iget-object v4, p0, Lcom/samsung/groupcast/application/StatBigDataLog;->mMatchMenu:Ljava/util/LinkedHashMap;

    invoke-virtual {v3}, Lcom/samsung/groupcast/application/StatLog$Menu;->getCode()Ljava/lang/String;

    move-result-object v5

    sget-object v6, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;->ERASER_SETTINGS:Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

    invoke-virtual {v4, v5, v6}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 95
    :pswitch_13
    iget-object v4, p0, Lcom/samsung/groupcast/application/StatBigDataLog;->mMatchMenu:Ljava/util/LinkedHashMap;

    invoke-virtual {v3}, Lcom/samsung/groupcast/application/StatLog$Menu;->getCode()Ljava/lang/String;

    move-result-object v5

    sget-object v6, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;->ERASE_ALL:Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

    invoke-virtual {v4, v5, v6}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 96
    :pswitch_14
    iget-object v4, p0, Lcom/samsung/groupcast/application/StatBigDataLog;->mMatchMenu:Ljava/util/LinkedHashMap;

    invoke-virtual {v3}, Lcom/samsung/groupcast/application/StatLog$Menu;->getCode()Ljava/lang/String;

    move-result-object v5

    sget-object v6, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;->ADD:Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

    invoke-virtual {v4, v5, v6}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 97
    :pswitch_15
    iget-object v4, p0, Lcom/samsung/groupcast/application/StatBigDataLog;->mMatchMenu:Ljava/util/LinkedHashMap;

    invoke-virtual {v3}, Lcom/samsung/groupcast/application/StatLog$Menu;->getCode()Ljava/lang/String;

    move-result-object v5

    sget-object v6, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;->REMOVE:Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

    invoke-virtual {v4, v5, v6}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 98
    :pswitch_16
    iget-object v4, p0, Lcom/samsung/groupcast/application/StatBigDataLog;->mMatchMenu:Ljava/util/LinkedHashMap;

    invoke-virtual {v3}, Lcom/samsung/groupcast/application/StatLog$Menu;->getCode()Ljava/lang/String;

    move-result-object v5

    sget-object v6, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;->PRIVATE_MODE:Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

    invoke-virtual {v4, v5, v6}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 99
    :pswitch_17
    iget-object v4, p0, Lcom/samsung/groupcast/application/StatBigDataLog;->mMatchMenu:Ljava/util/LinkedHashMap;

    invoke-virtual {v3}, Lcom/samsung/groupcast/application/StatLog$Menu;->getCode()Ljava/lang/String;

    move-result-object v5

    sget-object v6, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;->REMOTE_INVITATION:Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

    invoke-virtual {v4, v5, v6}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 100
    :pswitch_18
    iget-object v4, p0, Lcom/samsung/groupcast/application/StatBigDataLog;->mMatchMenu:Ljava/util/LinkedHashMap;

    invoke-virtual {v3}, Lcom/samsung/groupcast/application/StatLog$Menu;->getCode()Ljava/lang/String;

    move-result-object v5

    sget-object v6, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;->COLLAGE:Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

    invoke-virtual {v4, v5, v6}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 106
    .end local v3    # "menu":Lcom/samsung/groupcast/application/StatLog$Menu;
    :cond_0
    return-void

    .line 73
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
    .end packed-switch
.end method


# virtual methods
.method public bigdataLog(Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;)V
    .locals 7
    .param p1, "menu"    # Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

    .prologue
    .line 117
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "insertLog :: insertLog "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    # invokes: Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;->getCode()Ljava/lang/String;
    invoke-static {p1}, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;->access$000(Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p0}, Lcom/samsung/groupcast/application/StatBigDataLog;->getVersionOfContextProviders()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;)V

    .line 121
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/StatBigDataLog;->getVersionOfContextProviders()I

    move-result v4

    const/4 v5, 0x2

    if-ge v4, v5, :cond_0

    .line 138
    :goto_0
    return-void

    .line 124
    :cond_0
    const-string v4, "content://com.samsung.android.providers.context.log.use_app_feature_survey"

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 128
    .local v3, "uri":Landroid/net/Uri;
    :try_start_0
    invoke-static {}, Lcom/samsung/groupcast/application/App;->getInstance()Lcom/samsung/groupcast/application/App;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/groupcast/application/App;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 129
    .local v0, "cr":Landroid/content/ContentResolver;
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 130
    .local v2, "row":Landroid/content/ContentValues;
    const-string v4, "app_id"

    const-string v5, "com.samsung.groupcast"

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    const-string v4, "feature"

    # invokes: Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;->getCode()Ljava/lang/String;
    invoke-static {p1}, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;->access$000(Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    invoke-virtual {v0, v3, v2}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 133
    sget-object v4, Lcom/samsung/groupcast/application/StatBigDataLog;->TAG:Ljava/lang/String;

    const-string v5, "ContextProvider insertion operation is performed."

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 134
    .end local v0    # "cr":Landroid/content/ContentResolver;
    .end local v2    # "row":Landroid/content/ContentValues;
    :catch_0
    move-exception v1

    .line 135
    .local v1, "ex":Ljava/lang/Exception;
    sget-object v4, Lcom/samsung/groupcast/application/StatBigDataLog;->TAG:Ljava/lang/String;

    const-string v5, "Error while using the ContextProvider"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 136
    sget-object v4, Lcom/samsung/groupcast/application/StatBigDataLog;->TAG:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method getVersionOfContextProviders()I
    .locals 6

    .prologue
    .line 141
    const/4 v3, -0x1

    .line 142
    .local v3, "version":I
    invoke-static {}, Lcom/samsung/groupcast/application/App;->getInstance()Lcom/samsung/groupcast/application/App;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/groupcast/application/App;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 144
    .local v2, "packageManager":Landroid/content/pm/PackageManager;
    :try_start_0
    const-string v4, "com.samsung.android.providers.context"

    const/16 v5, 0x80

    invoke-virtual {v2, v4, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 147
    .local v1, "pInfo":Landroid/content/pm/PackageInfo;
    iget v3, v1, Landroid/content/pm/PackageInfo;->versionCode:I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 151
    .end local v1    # "pInfo":Landroid/content/pm/PackageInfo;
    :goto_0
    return v3

    .line 148
    :catch_0
    move-exception v0

    .line 149
    .local v0, "e1":Landroid/content/pm/PackageManager$NameNotFoundException;
    sget-object v4, Lcom/samsung/groupcast/application/StatBigDataLog;->TAG:Ljava/lang/String;

    const-string v5, "[SW] Could not find ContextProvider"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public pushLog(Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;)V
    .locals 1
    .param p1, "menu"    # Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

    .prologue
    .line 155
    invoke-static {}, Lcom/samsung/groupcast/application/StatBigDataLog;->getInstance()Lcom/samsung/groupcast/application/StatBigDataLog;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/samsung/groupcast/application/StatBigDataLog;->bigdataLog(Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;)V

    .line 156
    return-void
.end method

.method public pushLog(Lcom/samsung/groupcast/application/StatLog$Menu;)V
    .locals 3
    .param p1, "menu"    # Lcom/samsung/groupcast/application/StatLog$Menu;

    .prologue
    .line 159
    invoke-static {}, Lcom/samsung/groupcast/application/StatBigDataLog;->getInstance()Lcom/samsung/groupcast/application/StatBigDataLog;

    move-result-object v1

    iget-object v0, p0, Lcom/samsung/groupcast/application/StatBigDataLog;->mMatchMenu:Ljava/util/LinkedHashMap;

    invoke-virtual {p1}, Lcom/samsung/groupcast/application/StatLog$Menu;->getCode()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;

    invoke-virtual {v1, v0}, Lcom/samsung/groupcast/application/StatBigDataLog;->bigdataLog(Lcom/samsung/groupcast/application/StatBigDataLog$BigDataMenu;)V

    .line 160
    return-void
.end method
