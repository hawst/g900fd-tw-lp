.class public Lcom/samsung/groupcast/application/stub/UpdateCheckThread;
.super Ljava/lang/Thread;
.source "UpdateCheckThread.java"


# static fields
.field private static final APK_NAME:Ljava/lang/String; = "GroupPlay.apk"

.field private static final CSC_PATH:Ljava/lang/String; = "/system/csc/sales_code.dat"

.field private static final PD_TEST_PATH:Ljava/lang/String; = "/sdcard/go_to_andromeda.test"

.field private static final SERVER_URL_CHECK:Ljava/lang/String; = "http://vas.samsungapps.com/stub/stubUpdateCheck.as"

.field private static final SERVER_URL_DOWNLOAD:Ljava/lang/String; = "https://vas.samsungapps.com/stub/stubDownload.as"

.field private static final TAG:Ljava/lang/String; = "GroupCastStub"


# instance fields
.field private final _HFileUtil:Lcom/samsung/groupcast/application/stub/HFileUtil;

.field private final mContext:Landroid/content/Context;

.field private mFlagCancel:Z

.field private final mHandler:Landroid/os/Handler;

.field private final mPackageName:Ljava/lang/String;


# direct methods
.method constructor <init>(Landroid/content/Context;Landroid/os/Handler;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "handler"    # Landroid/os/Handler;

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 62
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/groupcast/application/stub/UpdateCheckThread;->mFlagCancel:Z

    .line 67
    iput-object p1, p0, Lcom/samsung/groupcast/application/stub/UpdateCheckThread;->mContext:Landroid/content/Context;

    .line 68
    iget-object v0, p0, Lcom/samsung/groupcast/application/stub/UpdateCheckThread;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/groupcast/application/stub/UpdateCheckThread;->mPackageName:Ljava/lang/String;

    .line 69
    iput-object p2, p0, Lcom/samsung/groupcast/application/stub/UpdateCheckThread;->mHandler:Landroid/os/Handler;

    .line 71
    new-instance v0, Lcom/samsung/groupcast/application/stub/HFileUtil;

    const-string v1, "stub.apk"

    invoke-direct {v0, p1, v1}, Lcom/samsung/groupcast/application/stub/HFileUtil;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/samsung/groupcast/application/stub/UpdateCheckThread;->_HFileUtil:Lcom/samsung/groupcast/application/stub/HFileUtil;

    .line 72
    iget-object v0, p0, Lcom/samsung/groupcast/application/stub/UpdateCheckThread;->_HFileUtil:Lcom/samsung/groupcast/application/stub/HFileUtil;

    invoke-virtual {v0}, Lcom/samsung/groupcast/application/stub/HFileUtil;->prepare()Z

    .line 73
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/groupcast/application/stub/UpdateCheckThread;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/application/stub/UpdateCheckThread;

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/samsung/groupcast/application/stub/UpdateCheckThread;->installApk()Z

    move-result v0

    return v0
.end method

.method private checkDownload(Ljava/net/URL;)Z
    .locals 17
    .param p1, "url"    # Ljava/net/URL;

    .prologue
    .line 277
    :try_start_0
    invoke-static {}, Lorg/xmlpull/v1/XmlPullParserFactory;->newInstance()Lorg/xmlpull/v1/XmlPullParserFactory;

    move-result-object v4

    .line 278
    .local v4, "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    invoke-virtual {v4}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v9

    .line 280
    .local v9, "parser":Lorg/xmlpull/v1/XmlPullParser;
    new-instance v5, Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-direct {v5}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>()V

    .line 283
    .local v5, "httpclient":Lorg/apache/http/client/HttpClient;
    new-instance v6, Lorg/apache/http/client/methods/HttpGet;

    invoke-virtual/range {p1 .. p1}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-direct {v6, v15}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/net/SocketException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/net/UnknownHostException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_6

    .line 288
    .local v6, "httpget":Lorg/apache/http/client/methods/HttpGet;
    :try_start_1
    invoke-interface {v5, v6}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v11

    .line 290
    .local v11, "response":Lorg/apache/http/HttpResponse;
    const-string v15, "GroupCastStub"

    invoke-interface {v11}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 293
    invoke-interface {v11}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/net/SocketException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/net/UnknownHostException; {:try_start_1 .. :try_end_1} :catch_5
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_6

    move-result-object v3

    .line 296
    .local v3, "entity":Lorg/apache/http/HttpEntity;
    if-eqz v3, :cond_0

    .line 301
    :try_start_2
    invoke-interface {v3}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v8

    .line 303
    .local v8, "instream":Ljava/io/InputStream;
    const/4 v15, 0x0

    invoke-interface {v9, v8, v15}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/InputStream;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/IllegalStateException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/net/SocketException; {:try_start_2 .. :try_end_2} :catch_4
    .catch Ljava/net/UnknownHostException; {:try_start_2 .. :try_end_2} :catch_5

    .line 314
    .end local v3    # "entity":Lorg/apache/http/HttpEntity;
    .end local v8    # "instream":Ljava/io/InputStream;
    .end local v11    # "response":Lorg/apache/http/HttpResponse;
    :cond_0
    :goto_0
    :try_start_3
    invoke-interface {v9}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v10

    .line 316
    .local v10, "parserEvent":I
    const-string v7, ""

    .local v7, "id":Ljava/lang/String;
    const-string v12, ""

    .line 317
    .local v12, "result":Ljava/lang/String;
    const-string v1, ""

    .line 319
    .local v1, "DownloadURI":Ljava/lang/String;
    :goto_1
    const/4 v15, 0x1

    if-eq v10, v15, :cond_6

    .line 320
    const/4 v15, 0x2

    if-ne v10, v15, :cond_3

    .line 321
    invoke-interface {v9}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v13

    .line 322
    .local v13, "tag":Ljava/lang/String;
    const-string v15, "appId"

    invoke-virtual {v13, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_1

    .line 323
    invoke-interface {v9}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v14

    .line 324
    .local v14, "type":I
    const/4 v15, 0x4

    if-ne v14, v15, :cond_1

    .line 325
    invoke-interface {v9}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v7

    .line 328
    .end local v14    # "type":I
    :cond_1
    const-string v15, "resultCode"

    invoke-virtual {v13, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_2

    .line 329
    invoke-interface {v9}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v14

    .line 330
    .restart local v14    # "type":I
    const/4 v15, 0x4

    if-ne v14, v15, :cond_2

    .line 331
    invoke-interface {v9}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v12

    .line 334
    .end local v14    # "type":I
    :cond_2
    const-string v15, "downloadURI"

    invoke-virtual {v13, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_3

    .line 335
    invoke-interface {v9}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v14

    .line 336
    .restart local v14    # "type":I
    const/4 v15, 0x4

    if-ne v14, v15, :cond_3

    .line 337
    invoke-interface {v9}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v1

    .line 341
    .end local v13    # "tag":Ljava/lang/String;
    .end local v14    # "type":I
    :cond_3
    const/4 v15, 0x3

    if-ne v10, v15, :cond_5

    .line 342
    invoke-interface {v9}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v13

    .line 343
    .restart local v13    # "tag":Ljava/lang/String;
    const-string v15, "downloadURI"

    invoke-virtual {v13, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_5

    .line 344
    move-object/from16 v0, p0

    invoke-direct {v0, v7, v12, v1}, Lcom/samsung/groupcast/application/stub/UpdateCheckThread;->downloadApk(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_4

    .line 345
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/groupcast/application/stub/UpdateCheckThread;->mHandler:Landroid/os/Handler;

    new-instance v16, Lcom/samsung/groupcast/application/stub/UpdateCheckThread$1;

    invoke-direct/range {v16 .. v17}, Lcom/samsung/groupcast/application/stub/UpdateCheckThread$1;-><init>(Lcom/samsung/groupcast/application/stub/UpdateCheckThread;)V

    invoke-virtual/range {v15 .. v16}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 355
    :cond_4
    const-string v7, ""

    .line 356
    const-string v12, ""

    .line 360
    .end local v13    # "tag":Ljava/lang/String;
    :cond_5
    invoke-interface {v9}, Lorg/xmlpull/v1/XmlPullParser;->next()I
    :try_end_3
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/net/SocketException; {:try_start_3 .. :try_end_3} :catch_4
    .catch Ljava/net/UnknownHostException; {:try_start_3 .. :try_end_3} :catch_5
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_6

    move-result v10

    goto :goto_1

    .line 304
    .end local v1    # "DownloadURI":Ljava/lang/String;
    .end local v7    # "id":Ljava/lang/String;
    .end local v10    # "parserEvent":I
    .end local v12    # "result":Ljava/lang/String;
    .restart local v3    # "entity":Lorg/apache/http/HttpEntity;
    .restart local v11    # "response":Lorg/apache/http/HttpResponse;
    :catch_0
    move-exception v2

    .line 305
    .local v2, "e":Ljava/lang/IllegalStateException;
    :try_start_4
    invoke-virtual {v2}, Ljava/lang/IllegalStateException;->printStackTrace()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/net/SocketException; {:try_start_4 .. :try_end_4} :catch_4
    .catch Ljava/net/UnknownHostException; {:try_start_4 .. :try_end_4} :catch_5
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_6

    goto :goto_0

    .line 310
    .end local v2    # "e":Ljava/lang/IllegalStateException;
    .end local v3    # "entity":Lorg/apache/http/HttpEntity;
    .end local v11    # "response":Lorg/apache/http/HttpResponse;
    :catch_1
    move-exception v2

    .line 311
    .local v2, "e":Ljava/lang/Exception;
    :try_start_5
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_5
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_5 .. :try_end_5} :catch_2
    .catch Ljava/net/SocketException; {:try_start_5 .. :try_end_5} :catch_4
    .catch Ljava/net/UnknownHostException; {:try_start_5 .. :try_end_5} :catch_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_6

    goto/16 :goto_0

    .line 362
    .end local v2    # "e":Ljava/lang/Exception;
    .end local v4    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .end local v5    # "httpclient":Lorg/apache/http/client/HttpClient;
    .end local v6    # "httpget":Lorg/apache/http/client/methods/HttpGet;
    .end local v9    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    :catch_2
    move-exception v2

    .line 363
    .local v2, "e":Lorg/xmlpull/v1/XmlPullParserException;
    const-string v15, "GroupCastStub"

    const-string v16, "xml parsing error"

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 364
    invoke-virtual {v2}, Lorg/xmlpull/v1/XmlPullParserException;->printStackTrace()V

    .line 365
    const/4 v15, 0x0

    .line 379
    .end local v2    # "e":Lorg/xmlpull/v1/XmlPullParserException;
    :goto_2
    return v15

    .line 306
    .restart local v3    # "entity":Lorg/apache/http/HttpEntity;
    .restart local v4    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .restart local v5    # "httpclient":Lorg/apache/http/client/HttpClient;
    .restart local v6    # "httpget":Lorg/apache/http/client/methods/HttpGet;
    .restart local v9    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .restart local v11    # "response":Lorg/apache/http/HttpResponse;
    :catch_3
    move-exception v2

    .line 307
    .local v2, "e":Ljava/io/IOException;
    :try_start_6
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_6 .. :try_end_6} :catch_2
    .catch Ljava/net/SocketException; {:try_start_6 .. :try_end_6} :catch_4
    .catch Ljava/net/UnknownHostException; {:try_start_6 .. :try_end_6} :catch_5
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_6

    goto/16 :goto_0

    .line 366
    .end local v2    # "e":Ljava/io/IOException;
    .end local v3    # "entity":Lorg/apache/http/HttpEntity;
    .end local v4    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .end local v5    # "httpclient":Lorg/apache/http/client/HttpClient;
    .end local v6    # "httpget":Lorg/apache/http/client/methods/HttpGet;
    .end local v9    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .end local v11    # "response":Lorg/apache/http/HttpResponse;
    :catch_4
    move-exception v2

    .line 367
    .local v2, "e":Ljava/net/SocketException;
    invoke-virtual {v2}, Ljava/net/SocketException;->printStackTrace()V

    .line 368
    const-string v15, "GroupCastStub"

    const-string v16, "network is unavailable"

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 369
    const/4 v15, 0x0

    goto :goto_2

    .line 370
    .end local v2    # "e":Ljava/net/SocketException;
    :catch_5
    move-exception v2

    .line 371
    .local v2, "e":Ljava/net/UnknownHostException;
    invoke-virtual {v2}, Ljava/net/UnknownHostException;->printStackTrace()V

    .line 372
    const-string v15, "GroupCastStub"

    const-string v16, "server is not response"

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 379
    .end local v2    # "e":Ljava/net/UnknownHostException;
    :cond_6
    const/4 v15, 0x1

    goto :goto_2

    .line 373
    :catch_6
    move-exception v2

    .line 374
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    .line 375
    const-string v15, "GroupCastStub"

    const-string v16, "network error"

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 376
    const/4 v15, 0x0

    goto :goto_2
.end method

.method private checkUpdate(Ljava/net/URL;)Z
    .locals 14
    .param p1, "url"    # Ljava/net/URL;

    .prologue
    const/4 v13, 0x4

    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 150
    :try_start_0
    invoke-static {}, Lorg/xmlpull/v1/XmlPullParserFactory;->newInstance()Lorg/xmlpull/v1/XmlPullParserFactory;

    move-result-object v1

    .line 151
    .local v1, "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    invoke-virtual {v1}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v3

    .line 152
    .local v3, "parser":Lorg/xmlpull/v1/XmlPullParser;
    invoke-virtual {p1}, Ljava/net/URL;->openStream()Ljava/io/InputStream;

    move-result-object v10

    const/4 v11, 0x0

    invoke-interface {v3, v10, v11}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/InputStream;Ljava/lang/String;)V

    .line 153
    invoke-interface {v3}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v4

    .line 154
    .local v4, "parserEvent":I
    invoke-interface {v3}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v4

    .line 156
    const-string v2, ""

    .local v2, "id":Ljava/lang/String;
    const-string v5, ""

    .line 157
    .local v5, "result":Ljava/lang/String;
    :goto_0
    if-eq v4, v8, :cond_3

    .line 158
    const/4 v10, 0x2

    if-ne v4, v10, :cond_1

    .line 159
    invoke-interface {v3}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v6

    .line 160
    .local v6, "tag":Ljava/lang/String;
    const-string v10, "appId"

    invoke-virtual {v6, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 161
    invoke-interface {v3}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v7

    .line 162
    .local v7, "type":I
    if-ne v7, v13, :cond_0

    .line 163
    invoke-interface {v3}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v2

    .line 164
    const-string v10, "GroupCastStub"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "appId : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 167
    .end local v7    # "type":I
    :cond_0
    const-string v10, "resultCode"

    invoke-virtual {v6, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 168
    invoke-interface {v3}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v7

    .line 169
    .restart local v7    # "type":I
    if-ne v7, v13, :cond_1

    .line 170
    invoke-interface {v3}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v5

    .line 171
    const-string v10, "GroupCastStub"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "StubUpdateCheck result : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 175
    .end local v6    # "tag":Ljava/lang/String;
    .end local v7    # "type":I
    :cond_1
    const/4 v10, 0x3

    if-ne v4, v10, :cond_2

    .line 176
    invoke-interface {v3}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v6

    .line 177
    .restart local v6    # "tag":Ljava/lang/String;
    const-string v10, "resultCode"

    invoke-virtual {v6, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 178
    invoke-direct {p0, v2, v5}, Lcom/samsung/groupcast/application/stub/UpdateCheckThread;->getResultUpdateCheck(Ljava/lang/String;Ljava/lang/String;)V

    .line 179
    const-string v2, ""

    .line 180
    const-string v5, ""

    .line 184
    .end local v6    # "tag":Ljava/lang/String;
    :cond_2
    invoke-interface {v3}, Lorg/xmlpull/v1/XmlPullParser;->next()I
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/net/SocketException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/net/UnknownHostException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3

    move-result v4

    goto :goto_0

    .line 186
    .end local v1    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .end local v2    # "id":Ljava/lang/String;
    .end local v3    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .end local v4    # "parserEvent":I
    .end local v5    # "result":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 187
    .local v0, "e":Lorg/xmlpull/v1/XmlPullParserException;
    const-string v8, "GroupCastStub"

    const-string v10, "xml parsing error"

    invoke-static {v8, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 188
    invoke-virtual {v0}, Lorg/xmlpull/v1/XmlPullParserException;->printStackTrace()V

    move v8, v9

    .line 203
    .end local v0    # "e":Lorg/xmlpull/v1/XmlPullParserException;
    :cond_3
    :goto_1
    return v8

    .line 190
    :catch_1
    move-exception v0

    .line 191
    .local v0, "e":Ljava/net/SocketException;
    invoke-virtual {v0}, Ljava/net/SocketException;->printStackTrace()V

    .line 192
    const-string v8, "GroupCastStub"

    const-string v10, "network is unavailable"

    invoke-static {v8, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v8, v9

    .line 193
    goto :goto_1

    .line 194
    .end local v0    # "e":Ljava/net/SocketException;
    :catch_2
    move-exception v0

    .line 195
    .local v0, "e":Ljava/net/UnknownHostException;
    invoke-virtual {v0}, Ljava/net/UnknownHostException;->printStackTrace()V

    .line 196
    const-string v9, "GroupCastStub"

    const-string v10, "server is not response"

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 197
    .end local v0    # "e":Ljava/net/UnknownHostException;
    :catch_3
    move-exception v0

    .line 198
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 199
    const-string v8, "GroupCastStub"

    const-string v10, "network error"

    invoke-static {v8, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v8, v9

    .line 200
    goto :goto_1
.end method

.method private downloadApk(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 18
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "code"    # Ljava/lang/String;
    .param p3, "URI"    # Ljava/lang/String;

    .prologue
    .line 383
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/groupcast/application/stub/UpdateCheckThread;->mPackageName:Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_3

    const-string v14, "1"

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_3

    const-string v14, ""

    move-object/from16 v0, p3

    invoke-virtual {v0, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-nez v14, :cond_3

    .line 384
    const/4 v5, 0x0

    .line 386
    .local v5, "file":Ljava/io/File;
    new-instance v7, Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-direct {v7}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>()V

    .line 387
    .local v7, "httpclient":Lorg/apache/http/client/HttpClient;
    new-instance v6, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v6}, Lorg/apache/http/client/methods/HttpGet;-><init>()V

    .line 389
    .local v6, "httpGet":Lorg/apache/http/client/methods/HttpGet;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/groupcast/application/stub/UpdateCheckThread;->_HFileUtil:Lcom/samsung/groupcast/application/stub/HFileUtil;

    invoke-virtual {v14}, Lcom/samsung/groupcast/application/stub/HFileUtil;->getFile()Ljava/io/File;

    move-result-object v5

    .line 391
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v14

    if-eqz v14, :cond_0

    .line 392
    const-string v14, "Range"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "bytes="

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v5}, Ljava/io/File;->length()J

    move-result-wide v16

    invoke-virtual/range {v15 .. v17}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "-"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v6, v14, v15}, Lorg/apache/http/client/methods/HttpGet;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 396
    :cond_0
    const-wide/16 v12, 0x0

    .line 397
    .local v12, "sizeTotal":J
    :try_start_0
    invoke-virtual {v5}, Ljava/io/File;->length()J

    move-result-wide v10

    .line 398
    .local v10, "sizeDownload":J
    new-instance v14, Ljava/net/URI;

    move-object/from16 v0, p3

    invoke-direct {v14, v0}, Ljava/net/URI;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v14}, Lorg/apache/http/client/methods/HttpGet;->setURI(Ljava/net/URI;)V

    .line 400
    invoke-interface {v7, v6}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v9

    .line 401
    .local v9, "response":Lorg/apache/http/HttpResponse;
    invoke-interface {v9}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v14

    invoke-interface {v14}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v2

    .line 403
    .local v2, "InStream":Ljava/io/InputStream;
    invoke-interface {v9}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v14

    invoke-interface {v14}, Lorg/apache/http/HttpEntity;->getContentLength()J

    move-result-wide v14

    add-long v12, v14, v10

    .line 405
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/groupcast/application/stub/UpdateCheckThread;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/groupcast/application/stub/UpdateCheckThread;->_HFileUtil:Lcom/samsung/groupcast/application/stub/HFileUtil;

    invoke-virtual {v15}, Lcom/samsung/groupcast/application/stub/HFileUtil;->getFileName()Ljava/lang/String;

    move-result-object v15

    const v16, 0x8001

    invoke-virtual/range {v14 .. v16}, Landroid/content/Context;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    move-result-object v1

    .line 408
    .local v1, "Fout":Ljava/io/FileOutputStream;
    const/16 v14, 0x400

    new-array v3, v14, [B

    .line 409
    .local v3, "buffer":[B
    const/4 v8, 0x0

    .line 411
    .local v8, "len1":I
    const-string v14, "DOWNLOAD_STATUS"

    const/4 v15, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v14, v15}, Lcom/samsung/groupcast/application/stub/UpdateCheckThread;->sendMessage(Ljava/lang/String;I)V

    .line 413
    :goto_0
    invoke-virtual {v2, v3}, Ljava/io/InputStream;->read([B)I

    move-result v8

    if-lez v8, :cond_2

    .line 414
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/samsung/groupcast/application/stub/UpdateCheckThread;->mFlagCancel:Z

    if-eqz v14, :cond_1

    .line 415
    const-string v14, "GroupCastStub"

    const-string v15, "Cancel Download"

    invoke-static {v14, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 416
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V

    .line 417
    const/4 v14, 0x0

    .line 438
    .end local v1    # "Fout":Ljava/io/FileOutputStream;
    .end local v2    # "InStream":Ljava/io/InputStream;
    .end local v3    # "buffer":[B
    .end local v5    # "file":Ljava/io/File;
    .end local v6    # "httpGet":Lorg/apache/http/client/methods/HttpGet;
    .end local v7    # "httpclient":Lorg/apache/http/client/HttpClient;
    .end local v8    # "len1":I
    .end local v9    # "response":Lorg/apache/http/HttpResponse;
    .end local v10    # "sizeDownload":J
    .end local v12    # "sizeTotal":J
    :goto_1
    return v14

    .line 420
    .restart local v1    # "Fout":Ljava/io/FileOutputStream;
    .restart local v2    # "InStream":Ljava/io/InputStream;
    .restart local v3    # "buffer":[B
    .restart local v5    # "file":Ljava/io/File;
    .restart local v6    # "httpGet":Lorg/apache/http/client/methods/HttpGet;
    .restart local v7    # "httpclient":Lorg/apache/http/client/HttpClient;
    .restart local v8    # "len1":I
    .restart local v9    # "response":Lorg/apache/http/HttpResponse;
    .restart local v10    # "sizeDownload":J
    .restart local v12    # "sizeTotal":J
    :cond_1
    const/4 v14, 0x0

    invoke-virtual {v1, v3, v14, v8}, Ljava/io/FileOutputStream;->write([BII)V

    .line 422
    int-to-long v14, v8

    add-long/2addr v10, v14

    .line 424
    const-string v14, "DOWNLOA_RATIO"

    const-wide/16 v15, 0x64

    mul-long/2addr v15, v10

    div-long/2addr v15, v12

    long-to-int v15, v15

    move-object/from16 v0, p0

    invoke-direct {v0, v14, v15}, Lcom/samsung/groupcast/application/stub/UpdateCheckThread;->sendMessage(Ljava/lang/String;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 427
    .end local v1    # "Fout":Ljava/io/FileOutputStream;
    .end local v2    # "InStream":Ljava/io/InputStream;
    .end local v3    # "buffer":[B
    .end local v8    # "len1":I
    .end local v9    # "response":Lorg/apache/http/HttpResponse;
    .end local v10    # "sizeDownload":J
    :catch_0
    move-exception v4

    .line 428
    .local v4, "e":Ljava/lang/Exception;
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V

    .line 429
    const-string v14, "GroupCastStub"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Download fail - "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v4}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 430
    const-string v14, "DOWNLOAD_STATUS"

    const/4 v15, 0x4

    move-object/from16 v0, p0

    invoke-direct {v0, v14, v15}, Lcom/samsung/groupcast/application/stub/UpdateCheckThread;->sendMessage(Ljava/lang/String;I)V

    .line 431
    const/4 v14, 0x0

    goto :goto_1

    .line 426
    .end local v4    # "e":Ljava/lang/Exception;
    .restart local v1    # "Fout":Ljava/io/FileOutputStream;
    .restart local v2    # "InStream":Ljava/io/InputStream;
    .restart local v3    # "buffer":[B
    .restart local v8    # "len1":I
    .restart local v9    # "response":Lorg/apache/http/HttpResponse;
    .restart local v10    # "sizeDownload":J
    :cond_2
    :try_start_1
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 434
    const-string v14, "GroupCastStub"

    const-string v15, "Download complete."

    invoke-static {v14, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 435
    const-string v14, "DOWNLOAD_STATUS"

    const/4 v15, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v14, v15}, Lcom/samsung/groupcast/application/stub/UpdateCheckThread;->sendMessage(Ljava/lang/String;I)V

    .line 436
    const/4 v14, 0x1

    goto :goto_1

    .line 438
    .end local v1    # "Fout":Ljava/io/FileOutputStream;
    .end local v2    # "InStream":Ljava/io/InputStream;
    .end local v3    # "buffer":[B
    .end local v5    # "file":Ljava/io/File;
    .end local v6    # "httpGet":Lorg/apache/http/client/methods/HttpGet;
    .end local v7    # "httpclient":Lorg/apache/http/client/HttpClient;
    .end local v8    # "len1":I
    .end local v9    # "response":Lorg/apache/http/HttpResponse;
    .end local v10    # "sizeDownload":J
    .end local v12    # "sizeTotal":J
    :cond_3
    const/4 v14, 0x0

    goto :goto_1
.end method

.method private getCSC()Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 545
    const-string v0, ""

    .line 546
    .local v0, "cscVersion":Ljava/lang/String;
    const/4 v1, 0x0

    .line 549
    .local v1, "value":Ljava/lang/String;
    invoke-direct {p0}, Lcom/samsung/groupcast/application/stub/UpdateCheckThread;->isCSCExistFile()Z

    move-result v2

    if-eq v2, v3, :cond_0

    .line 568
    :goto_0
    return-object v0

    .line 553
    :cond_0
    invoke-direct {p0}, Lcom/samsung/groupcast/application/stub/UpdateCheckThread;->getCSCVersion()Ljava/lang/String;

    move-result-object v1

    .line 554
    if-nez v1, :cond_1

    .line 555
    const-string v2, "GroupCastStub"

    const-string v3, "getCSC::getCSCVersion::value is null"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 559
    :cond_1
    const-string v2, "FAIL"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-ne v2, v3, :cond_2

    .line 560
    const-string v2, "GroupCastStub"

    const-string v3, "getCSC::getCSCVersion::Fail to read CSC Version"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 564
    :cond_2
    const/4 v2, 0x0

    const/4 v3, 0x3

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private getCSCVersion()Ljava/lang/String;
    .locals 7

    .prologue
    .line 572
    const/4 v4, 0x0

    .line 573
    .local v4, "s":Ljava/lang/String;
    new-instance v3, Ljava/io/File;

    const-string v6, "/system/csc/sales_code.dat"

    invoke-direct {v3, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 574
    .local v3, "mFile":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->isFile()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 575
    const/16 v6, 0x14

    new-array v0, v6, [B

    .line 576
    .local v0, "buffer":[B
    const/4 v1, 0x0

    .line 579
    .local v1, "in":Ljava/io/InputStream;
    :try_start_0
    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, v3}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 581
    .end local v1    # "in":Ljava/io/InputStream;
    .local v2, "in":Ljava/io/InputStream;
    :try_start_1
    invoke-virtual {v2, v0}, Ljava/io/InputStream;->read([B)I

    move-result v6

    if-eqz v6, :cond_1

    .line 582
    new-instance v5, Ljava/lang/String;

    invoke-direct {v5, v0}, Ljava/lang/String;-><init>([B)V

    .end local v4    # "s":Ljava/lang/String;
    .local v5, "s":Ljava/lang/String;
    move-object v4, v5

    .line 587
    .end local v5    # "s":Ljava/lang/String;
    .restart local v4    # "s":Ljava/lang/String;
    :goto_0
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    .line 595
    .end local v0    # "buffer":[B
    .end local v2    # "in":Ljava/io/InputStream;
    :cond_0
    :goto_1
    return-object v4

    .line 584
    .restart local v0    # "buffer":[B
    .restart local v2    # "in":Ljava/io/InputStream;
    :cond_1
    new-instance v5, Ljava/lang/String;

    const-string v6, "FAIL"

    invoke-direct {v5, v6}, Ljava/lang/String;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .end local v4    # "s":Ljava/lang/String;
    .restart local v5    # "s":Ljava/lang/String;
    move-object v4, v5

    .end local v5    # "s":Ljava/lang/String;
    .restart local v4    # "s":Ljava/lang/String;
    goto :goto_0

    .line 590
    .end local v2    # "in":Ljava/io/InputStream;
    .restart local v1    # "in":Ljava/io/InputStream;
    :catch_0
    move-exception v6

    goto :goto_1

    .end local v1    # "in":Ljava/io/InputStream;
    .restart local v2    # "in":Ljava/io/InputStream;
    :catch_1
    move-exception v6

    move-object v1, v2

    .end local v2    # "in":Ljava/io/InputStream;
    .restart local v1    # "in":Ljava/io/InputStream;
    goto :goto_1

    .line 588
    :catch_2
    move-exception v6

    goto :goto_1

    .end local v1    # "in":Ljava/io/InputStream;
    .restart local v2    # "in":Ljava/io/InputStream;
    :catch_3
    move-exception v6

    move-object v1, v2

    .end local v2    # "in":Ljava/io/InputStream;
    .restart local v1    # "in":Ljava/io/InputStream;
    goto :goto_1
.end method

.method private getIMEI()Ljava/lang/String;
    .locals 9

    .prologue
    const/16 v8, 0x10

    .line 615
    const-string v3, ""

    .line 616
    .local v3, "imei":Ljava/lang/String;
    iget-object v6, p0, Lcom/samsung/groupcast/application/stub/UpdateCheckThread;->mContext:Landroid/content/Context;

    const-string v7, "phone"

    invoke-virtual {v6, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/telephony/TelephonyManager;

    .line 618
    .local v5, "telMgr":Landroid/telephony/TelephonyManager;
    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    .line 620
    .local v4, "md5":Ljava/lang/StringBuffer;
    if-eqz v5, :cond_0

    .line 623
    const/4 v0, 0x0

    .line 625
    .local v0, "digest":[B
    :try_start_0
    const-string v6, "MD5"

    invoke-static {v6}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v6

    const-string v7, "000000000000000"

    invoke-virtual {v7}, Ljava/lang/String;->getBytes()[B

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/security/MessageDigest;->digest([B)[B
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 631
    :goto_0
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    array-length v6, v0

    if-ge v2, v6, :cond_0

    .line 632
    aget-byte v6, v0, v2

    and-int/lit16 v6, v6, 0xf0

    shr-int/lit8 v6, v6, 0x4

    invoke-static {v6, v8}, Ljava/lang/Integer;->toString(II)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 633
    aget-byte v6, v0, v2

    and-int/lit8 v6, v6, 0xf

    shr-int/lit8 v6, v6, 0x0

    invoke-static {v6, v8}, Ljava/lang/Integer;->toString(II)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 631
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 626
    .end local v2    # "i":I
    :catch_0
    move-exception v1

    .line 628
    .local v1, "e":Ljava/security/NoSuchAlgorithmException;
    invoke-virtual {v1}, Ljava/security/NoSuchAlgorithmException;->printStackTrace()V

    goto :goto_0

    .line 641
    .end local v0    # "digest":[B
    .end local v1    # "e":Ljava/security/NoSuchAlgorithmException;
    :cond_0
    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->getBytes()[B

    move-result-object v6

    const/4 v7, 0x0

    invoke-static {v6, v7}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v3

    .line 642
    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    return-object v6
.end method

.method private getMCC()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v5, 0x3

    .line 516
    const-string v0, ""

    .line 517
    .local v0, "mcc":Ljava/lang/String;
    iget-object v3, p0, Lcom/samsung/groupcast/application/stub/UpdateCheckThread;->mContext:Landroid/content/Context;

    const-string v4, "phone"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/telephony/TelephonyManager;

    .line 520
    .local v2, "telMgr":Landroid/telephony/TelephonyManager;
    if-eqz v2, :cond_0

    .line 521
    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getSimOperator()Ljava/lang/String;

    move-result-object v1

    .line 523
    .local v1, "networkOperator":Ljava/lang/String;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    if-lt v3, v5, :cond_0

    .line 524
    const/4 v3, 0x0

    invoke-virtual {v1, v3, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 527
    .end local v1    # "networkOperator":Ljava/lang/String;
    :cond_0
    return-object v0
.end method

.method private getMNC()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v5, 0x3

    .line 531
    const-string v0, "00"

    .line 532
    .local v0, "mnc":Ljava/lang/String;
    iget-object v3, p0, Lcom/samsung/groupcast/application/stub/UpdateCheckThread;->mContext:Landroid/content/Context;

    const-string v4, "phone"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/telephony/TelephonyManager;

    .line 535
    .local v2, "telMgr":Landroid/telephony/TelephonyManager;
    if-eqz v2, :cond_0

    .line 536
    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getSimOperator()Ljava/lang/String;

    move-result-object v1

    .line 537
    .local v1, "networkOperator":Ljava/lang/String;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    if-le v3, v5, :cond_0

    .line 538
    invoke-virtual {v1, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 541
    .end local v1    # "networkOperator":Ljava/lang/String;
    :cond_0
    return-object v0
.end method

.method private getPD()Ljava/lang/String;
    .locals 4

    .prologue
    .line 646
    const-string v2, "0"

    .line 647
    .local v2, "rtn_str":Ljava/lang/String;
    const/4 v1, 0x0

    .line 649
    .local v1, "result":Z
    new-instance v0, Ljava/io/File;

    const-string v3, "/sdcard/go_to_andromeda.test"

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 652
    .local v0, "file":Ljava/io/File;
    :try_start_0
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    .line 653
    const/4 v3, 0x1

    if-ne v1, v3, :cond_0

    .line 654
    const-string v2, "1"
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 657
    :cond_0
    :goto_0
    return-object v2

    .line 656
    :catch_0
    move-exception v3

    goto :goto_0
.end method

.method private getResultUpdateCheck(Ljava/lang/String;Ljava/lang/String;)V
    .locals 18
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "code"    # Ljava/lang/String;

    .prologue
    .line 207
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/groupcast/application/stub/UpdateCheckThread;->mPackageName:Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_5

    const-string v14, "2"

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_5

    .line 209
    sget-object v10, Landroid/os/Build;->MODEL:Ljava/lang/String;

    .line 210
    .local v10, "szModel":Ljava/lang/String;
    const-string v11, "SAMSUNG-"

    .line 212
    .local v11, "szPrefix":Ljava/lang/String;
    invoke-virtual {v10, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v14

    if-eqz v14, :cond_0

    .line 213
    const-string v14, ""

    invoke-virtual {v10, v11, v14}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 217
    :cond_0
    const-string v8, ""

    .line 218
    .local v8, "szMCC":Ljava/lang/String;
    const-string v9, ""

    .line 224
    .local v9, "szMNC":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/groupcast/application/stub/UpdateCheckThread;->mContext:Landroid/content/Context;

    const-string v15, "connectivity"

    invoke-virtual {v14, v15}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/ConnectivityManager;

    .line 225
    .local v1, "cManager":Landroid/net/ConnectivityManager;
    const/4 v14, 0x0

    invoke-virtual {v1, v14}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v4

    .line 226
    .local v4, "mobile":Landroid/net/NetworkInfo;
    const/4 v14, 0x1

    invoke-virtual {v1, v14}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v13

    .line 227
    .local v13, "wifi":Landroid/net/NetworkInfo;
    const/16 v14, 0x9

    invoke-virtual {v1, v14}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v3

    .line 229
    .local v3, "ethernet":Landroid/net/NetworkInfo;
    if-eqz v13, :cond_1

    invoke-virtual {v13}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v14

    if-nez v14, :cond_2

    :cond_1
    if-eqz v3, :cond_3

    invoke-virtual {v3}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v14

    if-eqz v14, :cond_3

    .line 231
    :cond_2
    const-string v8, "505"

    .line 232
    const-string v9, "00"

    .line 241
    :goto_0
    const-string v7, "https://vas.samsungapps.com/stub/stubDownload.as"

    .line 242
    .local v7, "server_url":Ljava/lang/String;
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v14, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "?appId="

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/groupcast/application/stub/UpdateCheckThread;->mPackageName:Ljava/lang/String;

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 243
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v14, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "&encImei="

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-direct/range {p0 .. p0}, Lcom/samsung/groupcast/application/stub/UpdateCheckThread;->getIMEI()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 244
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v14, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "&deviceId="

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 245
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v14, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "&mcc="

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 246
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v14, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "&mnc="

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 247
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v14, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "&csc="

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-direct/range {p0 .. p0}, Lcom/samsung/groupcast/application/stub/UpdateCheckThread;->getCSC()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 248
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v14, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "&sdkVer="

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    sget v15, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-static {v15}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 249
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v14, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "&pd="

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-direct/range {p0 .. p0}, Lcom/samsung/groupcast/application/stub/UpdateCheckThread;->getPD()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 251
    const/4 v6, 0x0

    .line 254
    .local v6, "result":Z
    :try_start_0
    const-string v14, "GroupCastStub"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "HTTP :: StubDownload url : "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 255
    new-instance v12, Ljava/net/URL;

    invoke-direct {v12, v7}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 256
    .local v12, "url":Ljava/net/URL;
    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/samsung/groupcast/application/stub/UpdateCheckThread;->checkDownload(Ljava/net/URL;)Z
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v6

    .line 260
    const-string v14, "GroupCastStub"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "end StubDownload : "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 271
    .end local v1    # "cManager":Landroid/net/ConnectivityManager;
    .end local v3    # "ethernet":Landroid/net/NetworkInfo;
    .end local v4    # "mobile":Landroid/net/NetworkInfo;
    .end local v6    # "result":Z
    .end local v7    # "server_url":Ljava/lang/String;
    .end local v8    # "szMCC":Ljava/lang/String;
    .end local v9    # "szMNC":Ljava/lang/String;
    .end local v10    # "szModel":Ljava/lang/String;
    .end local v11    # "szPrefix":Ljava/lang/String;
    .end local v12    # "url":Ljava/net/URL;
    .end local v13    # "wifi":Landroid/net/NetworkInfo;
    :goto_1
    return-void

    .line 233
    .restart local v1    # "cManager":Landroid/net/ConnectivityManager;
    .restart local v3    # "ethernet":Landroid/net/NetworkInfo;
    .restart local v4    # "mobile":Landroid/net/NetworkInfo;
    .restart local v8    # "szMCC":Ljava/lang/String;
    .restart local v9    # "szMNC":Ljava/lang/String;
    .restart local v10    # "szModel":Ljava/lang/String;
    .restart local v11    # "szPrefix":Ljava/lang/String;
    .restart local v13    # "wifi":Landroid/net/NetworkInfo;
    :cond_3
    if-eqz v4, :cond_4

    invoke-virtual {v4}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v14

    if-eqz v14, :cond_4

    .line 234
    invoke-direct/range {p0 .. p0}, Lcom/samsung/groupcast/application/stub/UpdateCheckThread;->getMCC()Ljava/lang/String;

    move-result-object v8

    .line 235
    invoke-direct/range {p0 .. p0}, Lcom/samsung/groupcast/application/stub/UpdateCheckThread;->getMNC()Ljava/lang/String;

    move-result-object v9

    goto/16 :goto_0

    .line 237
    :cond_4
    const-string v14, "GroupCastStub"

    const-string v15, "Connection failed"

    invoke-static {v14, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 257
    .restart local v6    # "result":Z
    .restart local v7    # "server_url":Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 258
    .local v2, "e1":Ljava/net/MalformedURLException;
    :try_start_1
    invoke-virtual {v2}, Ljava/net/MalformedURLException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 260
    const-string v14, "GroupCastStub"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "end StubDownload : "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .end local v2    # "e1":Ljava/net/MalformedURLException;
    :catchall_0
    move-exception v14

    const-string v15, "GroupCastStub"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "end StubDownload : "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    throw v14

    .line 264
    .end local v1    # "cManager":Landroid/net/ConnectivityManager;
    .end local v3    # "ethernet":Landroid/net/NetworkInfo;
    .end local v4    # "mobile":Landroid/net/NetworkInfo;
    .end local v6    # "result":Z
    .end local v7    # "server_url":Ljava/lang/String;
    .end local v8    # "szMCC":Ljava/lang/String;
    .end local v9    # "szMNC":Ljava/lang/String;
    .end local v10    # "szModel":Ljava/lang/String;
    .end local v11    # "szPrefix":Ljava/lang/String;
    .end local v13    # "wifi":Landroid/net/NetworkInfo;
    :cond_5
    const-string v14, "GroupCastStub"

    const-string v15, "Not found GroupPlay Update"

    invoke-static {v14, v15}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 265
    const-string v14, "GroupCastStub"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "[Package] "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " [ResultCode] "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p2

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 267
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/groupcast/application/stub/UpdateCheckThread;->mHandler:Landroid/os/Handler;

    invoke-virtual {v14}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v5

    .line 268
    .local v5, "msg":Landroid/os/Message;
    const/4 v14, 0x2

    iput v14, v5, Landroid/os/Message;->what:I

    .line 269
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/groupcast/application/stub/UpdateCheckThread;->mHandler:Landroid/os/Handler;

    invoke-virtual {v14, v5}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_1
.end method

.method private installApk()Z
    .locals 7

    .prologue
    .line 442
    new-instance v3, Ljava/io/File;

    iget-object v5, p0, Lcom/samsung/groupcast/application/stub/UpdateCheckThread;->_HFileUtil:Lcom/samsung/groupcast/application/stub/HFileUtil;

    invoke-virtual {v5}, Lcom/samsung/groupcast/application/stub/HFileUtil;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 444
    .local v3, "folder":Ljava/io/File;
    const/4 v0, 0x0

    .line 445
    .local v0, "bIsSuccess":Z
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v5

    if-nez v5, :cond_0

    .line 446
    const-string v5, "DOWNLOAD_STATUS"

    const/4 v6, 0x5

    invoke-direct {p0, v5, v6}, Lcom/samsung/groupcast/application/stub/UpdateCheckThread;->sendMessage(Ljava/lang/String;I)V

    move v1, v0

    .line 488
    .end local v0    # "bIsSuccess":Z
    .local v1, "bIsSuccess":I
    :goto_0
    return v1

    .line 450
    .end local v1    # "bIsSuccess":I
    .restart local v0    # "bIsSuccess":Z
    :cond_0
    :try_start_0
    new-instance v4, Lcom/samsung/groupcast/application/stub/ApplicationManager;

    iget-object v5, p0, Lcom/samsung/groupcast/application/stub/UpdateCheckThread;->mContext:Landroid/content/Context;

    invoke-direct {v4, v5}, Lcom/samsung/groupcast/application/stub/ApplicationManager;-><init>(Landroid/content/Context;)V

    .line 451
    .local v4, "mgr":Lcom/samsung/groupcast/application/stub/ApplicationManager;
    new-instance v5, Lcom/samsung/groupcast/application/stub/UpdateCheckThread$2;

    invoke-direct {v5, p0}, Lcom/samsung/groupcast/application/stub/UpdateCheckThread$2;-><init>(Lcom/samsung/groupcast/application/stub/UpdateCheckThread;)V

    invoke-virtual {v4, v5}, Lcom/samsung/groupcast/application/stub/ApplicationManager;->setOnInstalledPackaged(Lcom/samsung/groupcast/application/stub/OnInstalledPackaged;)V

    .line 460
    const-string v5, "DOWNLOAD_STATUS"

    const/4 v6, 0x2

    invoke-direct {p0, v5, v6}, Lcom/samsung/groupcast/application/stub/UpdateCheckThread;->sendMessage(Ljava/lang/String;I)V

    .line 461
    iget-object v5, p0, Lcom/samsung/groupcast/application/stub/UpdateCheckThread;->_HFileUtil:Lcom/samsung/groupcast/application/stub/HFileUtil;

    invoke-virtual {v5}, Lcom/samsung/groupcast/application/stub/HFileUtil;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/samsung/groupcast/application/stub/ApplicationManager;->installPackage(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_4

    .line 470
    const/4 v0, 0x1

    .end local v4    # "mgr":Lcom/samsung/groupcast/application/stub/ApplicationManager;
    :goto_1
    move v1, v0

    .line 488
    .restart local v1    # "bIsSuccess":I
    goto :goto_0

    .line 472
    .end local v1    # "bIsSuccess":I
    :catch_0
    move-exception v2

    .line 474
    .local v2, "e":Ljava/lang/SecurityException;
    invoke-virtual {v2}, Ljava/lang/SecurityException;->printStackTrace()V

    goto :goto_1

    .line 475
    .end local v2    # "e":Ljava/lang/SecurityException;
    :catch_1
    move-exception v2

    .line 477
    .local v2, "e":Ljava/lang/NoSuchMethodException;
    invoke-virtual {v2}, Ljava/lang/NoSuchMethodException;->printStackTrace()V

    goto :goto_1

    .line 478
    .end local v2    # "e":Ljava/lang/NoSuchMethodException;
    :catch_2
    move-exception v2

    .line 480
    .local v2, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v2}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_1

    .line 481
    .end local v2    # "e":Ljava/lang/IllegalArgumentException;
    :catch_3
    move-exception v2

    .line 483
    .local v2, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v2}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_1

    .line 484
    .end local v2    # "e":Ljava/lang/IllegalAccessException;
    :catch_4
    move-exception v2

    .line 486
    .local v2, "e":Ljava/lang/reflect/InvocationTargetException;
    invoke-virtual {v2}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V

    goto :goto_1
.end method

.method private isCSCExistFile()Z
    .locals 4

    .prologue
    .line 599
    const/4 v1, 0x0

    .line 600
    .local v1, "result":Z
    new-instance v0, Ljava/io/File;

    const-string v2, "/system/csc/sales_code.dat"

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 603
    .local v0, "file":Ljava/io/File;
    :try_start_0
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    .line 604
    const/4 v2, 0x1

    if-eq v1, v2, :cond_0

    .line 605
    const-string v2, "GroupCastStub"

    const-string v3, "CSC is not exist"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 611
    :cond_0
    :goto_0
    return v1

    .line 607
    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method private sendMessage(Ljava/lang/String;I)V
    .locals 3
    .param p1, "cmd"    # Ljava/lang/String;
    .param p2, "arg"    # I

    .prologue
    .line 492
    iget-object v2, p0, Lcom/samsung/groupcast/application/stub/UpdateCheckThread;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    .line 493
    .local v1, "msg":Landroid/os/Message;
    const/4 v2, 0x0

    iput v2, v1, Landroid/os/Message;->what:I

    .line 494
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 495
    .local v0, "b":Landroid/os/Bundle;
    const-string v2, "MESSAGE_CMD"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 497
    const-string v2, "DOWNLOAD_STATUS"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 498
    const-string v2, "DOWNLOAD_STATUS"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 503
    :cond_0
    :goto_0
    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 504
    iget-object v2, p0, Lcom/samsung/groupcast/application/stub/UpdateCheckThread;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 505
    return-void

    .line 499
    :cond_1
    const-string v2, "DOWNLOA_RATIO"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 500
    const-string v2, "DOWNLOA_RATIO"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_0
.end method


# virtual methods
.method public cancel()V
    .locals 1

    .prologue
    .line 76
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/groupcast/application/stub/UpdateCheckThread;->mFlagCancel:Z

    .line 77
    return-void
.end method

.method public getDownLoadPath()Ljava/lang/String;
    .locals 2

    .prologue
    .line 661
    iget-object v0, p0, Lcom/samsung/groupcast/application/stub/UpdateCheckThread;->mContext:Landroid/content/Context;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/content/Context;->getFileStreamPath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public run()V
    .locals 21

    .prologue
    .line 82
    const-string v17, "DOWNLOAD_STATUS"

    const/16 v18, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    move/from16 v2, v18

    invoke-direct {v0, v1, v2}, Lcom/samsung/groupcast/application/stub/UpdateCheckThread;->sendMessage(Ljava/lang/String;I)V

    .line 84
    sget-object v13, Landroid/os/Build;->MODEL:Ljava/lang/String;

    .line 85
    .local v13, "szModel":Ljava/lang/String;
    const-string v14, "SAMSUNG-"

    .line 87
    .local v14, "szPrefix":Ljava/lang/String;
    invoke-virtual {v13, v14}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v17

    if-eqz v17, :cond_0

    .line 88
    const-string v17, ""

    move-object/from16 v0, v17

    invoke-virtual {v13, v14, v0}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 92
    :cond_0
    const-string v11, ""

    .line 93
    .local v11, "szMCC":Ljava/lang/String;
    const-string v12, ""

    .line 99
    .local v12, "szMNC":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/groupcast/application/stub/UpdateCheckThread;->mContext:Landroid/content/Context;

    move-object/from16 v17, v0

    const-string v18, "connectivity"

    invoke-virtual/range {v17 .. v18}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/net/ConnectivityManager;

    .line 100
    .local v3, "cManager":Landroid/net/ConnectivityManager;
    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-virtual {v3, v0}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v7

    .line 101
    .local v7, "mobile":Landroid/net/NetworkInfo;
    const/16 v17, 0x1

    move/from16 v0, v17

    invoke-virtual {v3, v0}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v16

    .line 102
    .local v16, "wifi":Landroid/net/NetworkInfo;
    const/16 v17, 0x9

    move/from16 v0, v17

    invoke-virtual {v3, v0}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v6

    .line 104
    .local v6, "ethernet":Landroid/net/NetworkInfo;
    if-eqz v16, :cond_1

    invoke-virtual/range {v16 .. v16}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v17

    if-nez v17, :cond_2

    :cond_1
    if-eqz v6, :cond_4

    invoke-virtual {v6}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v17

    if-eqz v17, :cond_4

    .line 105
    :cond_2
    const-string v11, "505"

    .line 106
    const-string v12, "00"

    .line 115
    :goto_0
    const-string v10, "http://vas.samsungapps.com/stub/stubUpdateCheck.as"

    .line 116
    .local v10, "server_url":Ljava/lang/String;
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v17

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, "?appId="

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/groupcast/application/stub/UpdateCheckThread;->mPackageName:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 117
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v17

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, "&versionCode=0"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 118
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v17

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, "&deviceId="

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 119
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v17

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, "&mcc="

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 120
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v17

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, "&mnc="

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 121
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v17

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, "&csc="

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-direct/range {p0 .. p0}, Lcom/samsung/groupcast/application/stub/UpdateCheckThread;->getCSC()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 122
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v17

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, "&sdkVer="

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    sget v18, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-static/range {v18 .. v18}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 123
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v17

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, "&pd="

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-direct/range {p0 .. p0}, Lcom/samsung/groupcast/application/stub/UpdateCheckThread;->getPD()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 125
    const/4 v9, 0x0

    .line 127
    .local v9, "result":Z
    :try_start_0
    const-string v17, "GroupCastStub"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "HTTP :: StubUpdateCheck url : "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 129
    new-instance v15, Ljava/net/URL;

    invoke-direct {v15, v10}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 130
    .local v15, "url":Ljava/net/URL;
    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lcom/samsung/groupcast/application/stub/UpdateCheckThread;->checkUpdate(Ljava/net/URL;)Z
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v9

    .line 137
    const-string v17, "GroupCastStub"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "end StubUpdateCheck : "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 138
    if-nez v9, :cond_3

    .line 139
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/groupcast/application/stub/UpdateCheckThread;->mHandler:Landroid/os/Handler;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v8

    .line 140
    .local v8, "msg":Landroid/os/Message;
    const/16 v17, 0x1

    move/from16 v0, v17

    iput v0, v8, Landroid/os/Message;->what:I

    .line 141
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/groupcast/application/stub/UpdateCheckThread;->mHandler:Landroid/os/Handler;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v8}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 144
    .end local v8    # "msg":Landroid/os/Message;
    .end local v9    # "result":Z
    .end local v10    # "server_url":Ljava/lang/String;
    .end local v15    # "url":Ljava/net/URL;
    :cond_3
    :goto_1
    return-void

    .line 107
    :cond_4
    if-eqz v7, :cond_5

    invoke-virtual {v7}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v17

    if-eqz v17, :cond_5

    .line 108
    invoke-direct/range {p0 .. p0}, Lcom/samsung/groupcast/application/stub/UpdateCheckThread;->getMCC()Ljava/lang/String;

    move-result-object v11

    .line 109
    invoke-direct/range {p0 .. p0}, Lcom/samsung/groupcast/application/stub/UpdateCheckThread;->getMNC()Ljava/lang/String;

    move-result-object v12

    goto/16 :goto_0

    .line 111
    :cond_5
    const-string v17, "GroupCastStub"

    const-string v18, "Connection failed"

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 132
    .restart local v9    # "result":Z
    .restart local v10    # "server_url":Ljava/lang/String;
    :catch_0
    move-exception v5

    .line 133
    .local v5, "e1":Ljava/net/MalformedURLException;
    :try_start_1
    invoke-virtual {v5}, Ljava/net/MalformedURLException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 137
    const-string v17, "GroupCastStub"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "end StubUpdateCheck : "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 138
    if-nez v9, :cond_3

    .line 139
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/groupcast/application/stub/UpdateCheckThread;->mHandler:Landroid/os/Handler;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v8

    .line 140
    .restart local v8    # "msg":Landroid/os/Message;
    const/16 v17, 0x1

    move/from16 v0, v17

    iput v0, v8, Landroid/os/Message;->what:I

    .line 141
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/groupcast/application/stub/UpdateCheckThread;->mHandler:Landroid/os/Handler;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v8}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_1

    .line 134
    .end local v5    # "e1":Ljava/net/MalformedURLException;
    .end local v8    # "msg":Landroid/os/Message;
    :catch_1
    move-exception v4

    .line 135
    .local v4, "e":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 137
    const-string v17, "GroupCastStub"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "end StubUpdateCheck : "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 138
    if-nez v9, :cond_3

    .line 139
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/groupcast/application/stub/UpdateCheckThread;->mHandler:Landroid/os/Handler;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v8

    .line 140
    .restart local v8    # "msg":Landroid/os/Message;
    const/16 v17, 0x1

    move/from16 v0, v17

    iput v0, v8, Landroid/os/Message;->what:I

    .line 141
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/groupcast/application/stub/UpdateCheckThread;->mHandler:Landroid/os/Handler;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v8}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_1

    .line 137
    .end local v4    # "e":Ljava/lang/Exception;
    .end local v8    # "msg":Landroid/os/Message;
    :catchall_0
    move-exception v17

    const-string v18, "GroupCastStub"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "end StubUpdateCheck : "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 138
    if-nez v9, :cond_6

    .line 139
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/groupcast/application/stub/UpdateCheckThread;->mHandler:Landroid/os/Handler;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v8

    .line 140
    .restart local v8    # "msg":Landroid/os/Message;
    const/16 v18, 0x1

    move/from16 v0, v18

    iput v0, v8, Landroid/os/Message;->what:I

    .line 141
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/groupcast/application/stub/UpdateCheckThread;->mHandler:Landroid/os/Handler;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v8}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 142
    .end local v8    # "msg":Landroid/os/Message;
    :cond_6
    throw v17
.end method
