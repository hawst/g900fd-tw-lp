.class public Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter;
.super Landroid/widget/BaseAdapter;
.source "SettingActivityListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$ViewHolder;,
        Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$AboutItem;,
        Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$InvitationItem;,
        Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$NicknameItem;,
        Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$AboutEnum;,
        Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$InvitationEnum;,
        Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$Delegate;
    }
.end annotation


# instance fields
.field private TAG:Ljava/lang/String;

.field private final mAboutItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$AboutItem;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field private mDelegate:Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$Delegate;

.field private final mInvotationItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$InvitationItem;",
            ">;"
        }
    .end annotation
.end field

.field private final mNicknameItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$NicknameItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 112
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 24
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter;->TAG:Ljava/lang/String;

    .line 108
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter;->mInvotationItems:Ljava/util/ArrayList;

    .line 109
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter;->mNicknameItems:Ljava/util/ArrayList;

    .line 110
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter;->mAboutItems:Ljava/util/ArrayList;

    .line 113
    iput-object p1, p0, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter;->mContext:Landroid/content/Context;

    .line 114
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter;)Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$Delegate;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter;->mDelegate:Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$Delegate;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter;I)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter;
    .param p1, "x1"    # I

    .prologue
    .line 22
    invoke-direct {p0, p1}, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter;->isInvotationItem(I)Z

    move-result v0

    return v0
.end method

.method static synthetic access$200(Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter;I)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter;
    .param p1, "x1"    # I

    .prologue
    .line 22
    invoke-direct {p0, p1}, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter;->isAboutItem(I)Z

    move-result v0

    return v0
.end method

.method private getAboutHeaderIndex()I
    .locals 2

    .prologue
    .line 345
    iget-object v0, p0, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter;->mInvotationItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-direct {p0}, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter;->getInvotationHeaderIndex()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method private getInvotationHeaderIndex()I
    .locals 1

    .prologue
    .line 341
    iget-object v0, p0, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter;->mNicknameItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method private getNickNameHeaderIndex()I
    .locals 1

    .prologue
    .line 337
    const/4 v0, 0x0

    return v0
.end method

.method private isAboutHeaderIndex(I)Z
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 321
    invoke-direct {p0}, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter;->getAboutHeaderIndex()I

    move-result v0

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isAboutItem(I)Z
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 333
    invoke-direct {p0}, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter;->getAboutHeaderIndex()I

    move-result v0

    if-le p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isInvotationHeaderIndex(I)Z
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 317
    invoke-direct {p0}, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter;->getInvotationHeaderIndex()I

    move-result v0

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isInvotationItem(I)Z
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 329
    invoke-direct {p0}, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter;->getInvotationHeaderIndex()I

    move-result v0

    if-le p1, v0, :cond_0

    invoke-direct {p0}, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter;->getAboutHeaderIndex()I

    move-result v0

    if-ge p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isNickNameHeaderIndex(I)Z
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 313
    invoke-direct {p0}, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter;->getNickNameHeaderIndex()I

    move-result v0

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isNicknameItem(I)Z
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 325
    invoke-direct {p0, p1}, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter;->isNickNameHeaderIndex(I)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter;->getInvotationHeaderIndex()I

    move-result v0

    if-ge p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public attachListView(Landroid/widget/ListView;)V
    .locals 1
    .param p1, "listView"    # Landroid/widget/ListView;

    .prologue
    .line 139
    invoke-virtual {p1, p0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 141
    new-instance v0, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$1;

    invoke-direct {v0, p0}, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$1;-><init>(Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter;)V

    invoke-virtual {p1, v0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 161
    return-void
.end method

.method public getCount()I
    .locals 2

    .prologue
    .line 166
    iget-object v0, p0, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter;->mNicknameItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v0, v0, 0x1

    iget-object v1, p0, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter;->mInvotationItems:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x1

    iget-object v1, p0, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter;->mAboutItems:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 171
    invoke-direct {p0, p1}, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter;->isNicknameItem(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 172
    add-int/lit8 v0, p1, -0x1

    .line 173
    .local v0, "index":I
    iget-object v1, p0, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter;->mNicknameItems:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    .line 182
    .end local v0    # "index":I
    :goto_0
    return-object v1

    .line 174
    :cond_0
    invoke-direct {p0, p1}, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter;->isInvotationItem(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 175
    invoke-direct {p0}, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter;->getInvotationHeaderIndex()I

    move-result v1

    sub-int v1, p1, v1

    add-int/lit8 v0, v1, -0x1

    .line 176
    .restart local v0    # "index":I
    iget-object v1, p0, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter;->mInvotationItems:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    goto :goto_0

    .line 177
    .end local v0    # "index":I
    :cond_1
    invoke-direct {p0, p1}, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter;->isAboutItem(I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 178
    invoke-direct {p0}, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter;->getAboutHeaderIndex()I

    move-result v1

    sub-int v1, p1, v1

    add-int/lit8 v0, v1, -0x1

    .line 179
    .restart local v0    # "index":I
    iget-object v1, p0, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter;->mAboutItems:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    goto :goto_0

    .line 182
    .end local v0    # "index":I
    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 188
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const v9, 0x7f070029

    const v8, 0x7f040010

    const/4 v7, 0x0

    .line 200
    move-object v3, p2

    .line 201
    .local v3, "view":Landroid/view/View;
    const/4 v4, 0x0

    .line 203
    .local v4, "viewHolder":Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$ViewHolder;
    if-eqz v3, :cond_0

    .line 204
    invoke-virtual {v3}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v4

    .end local v4    # "viewHolder":Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$ViewHolder;
    check-cast v4, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$ViewHolder;

    .line 208
    .restart local v4    # "viewHolder":Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$ViewHolder;
    :cond_0
    invoke-direct {p0, p1}, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter;->isNickNameHeaderIndex(I)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 209
    if-eqz v4, :cond_1

    const-string v5, "nickNameHeader"

    iget-object v6, v4, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$ViewHolder;->type:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 210
    :cond_1
    iget-object v5, p0, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter;->mContext:Landroid/content/Context;

    const-string v6, "layout_inflater"

    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/LayoutInflater;

    .line 212
    .local v2, "layoutInflater":Landroid/view/LayoutInflater;
    invoke-virtual {v2, v8, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 213
    new-instance v4, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$ViewHolder;

    .end local v4    # "viewHolder":Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$ViewHolder;
    invoke-direct {v4, p0, v7}, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$ViewHolder;-><init>(Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter;Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$1;)V

    .line 214
    .restart local v4    # "viewHolder":Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$ViewHolder;
    const-string v5, "nickNameHeader"

    iput-object v5, v4, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$ViewHolder;->type:Ljava/lang/String;

    .line 215
    invoke-virtual {v3, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, v4, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$ViewHolder;->title:Landroid/widget/TextView;

    .line 216
    invoke-virtual {v3, v4}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 218
    .end local v2    # "layoutInflater":Landroid/view/LayoutInflater;
    :cond_2
    iget-object v5, v4, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$ViewHolder;->title:Landroid/widget/TextView;

    const v6, 0x7f0800da

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(I)V

    .line 222
    :cond_3
    invoke-direct {p0, p1}, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter;->isInvotationHeaderIndex(I)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 223
    if-eqz v4, :cond_4

    const-string v5, "invotationHeader"

    iget-object v6, v4, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$ViewHolder;->type:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_5

    .line 224
    :cond_4
    iget-object v5, p0, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter;->mContext:Landroid/content/Context;

    const-string v6, "layout_inflater"

    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/LayoutInflater;

    .line 226
    .restart local v2    # "layoutInflater":Landroid/view/LayoutInflater;
    invoke-virtual {v2, v8, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 227
    new-instance v4, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$ViewHolder;

    .end local v4    # "viewHolder":Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$ViewHolder;
    invoke-direct {v4, p0, v7}, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$ViewHolder;-><init>(Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter;Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$1;)V

    .line 228
    .restart local v4    # "viewHolder":Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$ViewHolder;
    const-string v5, "invotationHeader"

    iput-object v5, v4, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$ViewHolder;->type:Ljava/lang/String;

    .line 229
    invoke-virtual {v3, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, v4, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$ViewHolder;->title:Landroid/widget/TextView;

    .line 230
    invoke-virtual {v3, v4}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 233
    .end local v2    # "layoutInflater":Landroid/view/LayoutInflater;
    :cond_5
    iget-object v5, v4, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$ViewHolder;->title:Landroid/widget/TextView;

    const v6, 0x7f0800dd

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(I)V

    .line 237
    :cond_6
    invoke-direct {p0, p1}, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter;->isAboutHeaderIndex(I)Z

    move-result v5

    if-eqz v5, :cond_9

    .line 238
    if-eqz v4, :cond_7

    const-string v5, "aboutHeader"

    iget-object v6, v4, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$ViewHolder;->type:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_8

    .line 239
    :cond_7
    iget-object v5, p0, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter;->mContext:Landroid/content/Context;

    const-string v6, "layout_inflater"

    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/LayoutInflater;

    .line 241
    .restart local v2    # "layoutInflater":Landroid/view/LayoutInflater;
    invoke-virtual {v2, v8, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 242
    new-instance v4, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$ViewHolder;

    .end local v4    # "viewHolder":Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$ViewHolder;
    invoke-direct {v4, p0, v7}, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$ViewHolder;-><init>(Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter;Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$1;)V

    .line 243
    .restart local v4    # "viewHolder":Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$ViewHolder;
    const-string v5, "aboutHeader"

    iput-object v5, v4, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$ViewHolder;->type:Ljava/lang/String;

    .line 244
    invoke-virtual {v3, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, v4, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$ViewHolder;->title:Landroid/widget/TextView;

    .line 245
    invoke-virtual {v3, v4}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 248
    .end local v2    # "layoutInflater":Landroid/view/LayoutInflater;
    :cond_8
    iget-object v5, v4, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$ViewHolder;->title:Landroid/widget/TextView;

    const v6, 0x7f0800e5

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(I)V

    .line 252
    :cond_9
    invoke-direct {p0, p1}, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter;->isNicknameItem(I)Z

    move-result v5

    if-eqz v5, :cond_c

    .line 253
    if-eqz v4, :cond_a

    const-string v5, "nickNameItem"

    iget-object v6, v4, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$ViewHolder;->type:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_b

    .line 254
    :cond_a
    iget-object v5, p0, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter;->mContext:Landroid/content/Context;

    const-string v6, "layout_inflater"

    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/LayoutInflater;

    .line 256
    .restart local v2    # "layoutInflater":Landroid/view/LayoutInflater;
    const v5, 0x7f040013

    invoke-virtual {v2, v5, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 257
    new-instance v4, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$ViewHolder;

    .end local v4    # "viewHolder":Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$ViewHolder;
    invoke-direct {v4, p0, v7}, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$ViewHolder;-><init>(Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter;Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$1;)V

    .line 258
    .restart local v4    # "viewHolder":Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$ViewHolder;
    const-string v5, "nickNameItem"

    iput-object v5, v4, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$ViewHolder;->type:Ljava/lang/String;

    .line 259
    const v5, 0x7f07002d

    invoke-virtual {v3, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, v4, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$ViewHolder;->title:Landroid/widget/TextView;

    .line 262
    .end local v2    # "layoutInflater":Landroid/view/LayoutInflater;
    :cond_b
    invoke-virtual {p0, p1}, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$NicknameItem;

    .line 263
    .local v0, "item":Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$NicknameItem;
    invoke-virtual {v0}, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$NicknameItem;->getTitle()Ljava/lang/String;

    move-result-object v1

    .line 265
    .local v1, "itemNickName":Ljava/lang/String;
    iget-object v5, v4, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$ViewHolder;->title:Landroid/widget/TextView;

    invoke-virtual {v5, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 266
    const v5, 0x7f07002e

    invoke-virtual {v3, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/Button;

    iput-object v5, v4, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$ViewHolder;->button:Landroid/widget/Button;

    .line 268
    iget-object v5, v4, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$ViewHolder;->button:Landroid/widget/Button;

    new-instance v6, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$2;

    invoke-direct {v6, p0, v1}, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$2;-><init>(Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter;Ljava/lang/String;)V

    invoke-virtual {v5, v6}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 277
    .end local v0    # "item":Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$NicknameItem;
    .end local v1    # "itemNickName":Ljava/lang/String;
    :cond_c
    invoke-direct {p0, p1}, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter;->isInvotationItem(I)Z

    move-result v5

    if-eqz v5, :cond_f

    .line 278
    if-eqz v4, :cond_d

    const-string v5, "invotationItem"

    iget-object v6, v4, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$ViewHolder;->type:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_e

    .line 279
    :cond_d
    iget-object v5, p0, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter;->mContext:Landroid/content/Context;

    const-string v6, "layout_inflater"

    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/LayoutInflater;

    .line 281
    .restart local v2    # "layoutInflater":Landroid/view/LayoutInflater;
    const v5, 0x7f040012

    invoke-virtual {v2, v5, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 282
    new-instance v4, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$ViewHolder;

    .end local v4    # "viewHolder":Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$ViewHolder;
    invoke-direct {v4, p0, v7}, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$ViewHolder;-><init>(Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter;Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$1;)V

    .line 283
    .restart local v4    # "viewHolder":Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$ViewHolder;
    const-string v5, "invotationItem"

    iput-object v5, v4, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$ViewHolder;->type:Ljava/lang/String;

    .line 284
    const v5, 0x7f07002b

    invoke-virtual {v3, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, v4, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$ViewHolder;->title:Landroid/widget/TextView;

    .line 285
    const v5, 0x7f07002c

    invoke-virtual {v3, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/CheckBox;

    iput-object v5, v4, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$ViewHolder;->checkBox:Landroid/widget/CheckBox;

    .line 288
    .end local v2    # "layoutInflater":Landroid/view/LayoutInflater;
    :cond_e
    invoke-virtual {p0, p1}, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$InvitationItem;

    .line 289
    .local v0, "item":Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$InvitationItem;
    iget-object v5, v4, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$ViewHolder;->title:Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$InvitationItem;->getTitle()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 290
    iget-object v5, v4, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$ViewHolder;->checkBox:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$InvitationItem;->getChecked()Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    invoke-virtual {v5, v6}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 294
    .end local v0    # "item":Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$InvitationItem;
    :cond_f
    invoke-direct {p0, p1}, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter;->isAboutItem(I)Z

    move-result v5

    if-eqz v5, :cond_12

    .line 295
    if-eqz v4, :cond_10

    const-string v5, "aboutItem"

    iget-object v6, v4, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$ViewHolder;->type:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_11

    .line 296
    :cond_10
    iget-object v5, p0, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter;->mContext:Landroid/content/Context;

    const-string v6, "layout_inflater"

    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/LayoutInflater;

    .line 298
    .restart local v2    # "layoutInflater":Landroid/view/LayoutInflater;
    const v5, 0x7f040011

    invoke-virtual {v2, v5, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 299
    new-instance v4, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$ViewHolder;

    .end local v4    # "viewHolder":Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$ViewHolder;
    invoke-direct {v4, p0, v7}, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$ViewHolder;-><init>(Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter;Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$1;)V

    .line 300
    .restart local v4    # "viewHolder":Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$ViewHolder;
    const-string v5, "aboutItem"

    iput-object v5, v4, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$ViewHolder;->type:Ljava/lang/String;

    .line 301
    const v5, 0x7f07002a

    invoke-virtual {v3, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, v4, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$ViewHolder;->title:Landroid/widget/TextView;

    .line 304
    .end local v2    # "layoutInflater":Landroid/view/LayoutInflater;
    :cond_11
    invoke-virtual {p0, p1}, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$AboutItem;

    .line 305
    .local v0, "item":Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$AboutItem;
    iget-object v5, v4, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$ViewHolder;->title:Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$AboutItem;->getTitle()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 309
    .end local v0    # "item":Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$AboutItem;
    :cond_12
    return-object v3
.end method

.method public setAboutItems(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$AboutItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 133
    .local p1, "items":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$AboutItem;>;"
    iget-object v0, p0, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter;->mAboutItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 134
    iget-object v0, p0, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter;->mAboutItems:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 135
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter;->notifyDataSetChanged()V

    .line 136
    return-void
.end method

.method public setDelegate(Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$Delegate;)V
    .locals 0
    .param p1, "delegate"    # Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$Delegate;

    .prologue
    .line 117
    iput-object p1, p0, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter;->mDelegate:Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$Delegate;

    .line 118
    return-void
.end method

.method public setInvotationItems(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$InvitationItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 121
    .local p1, "items":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$InvitationItem;>;"
    iget-object v0, p0, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter;->mInvotationItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 122
    iget-object v0, p0, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter;->mInvotationItems:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 123
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter;->notifyDataSetChanged()V

    .line 124
    return-void
.end method

.method public setNickNameItems(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$NicknameItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 127
    .local p1, "items":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter$NicknameItem;>;"
    iget-object v0, p0, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter;->mNicknameItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 128
    iget-object v0, p0, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter;->mNicknameItems:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 129
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/setting/SettingActivityListAdapter;->notifyDataSetChanged()V

    .line 130
    return-void
.end method
